#!/usr/bin/env bash
set -x -e
if [ "$#" -ne 5 ] && [ "$#" -ne 7 ]; then
  echo 'Usage for PLSA/LDA: '$0' <input_directory> <output_directory> <plsa|lda> <language> <topics_number> [ <number_bigrams> <word_association_measure> ]'
  echo 'Usage for PLSA-SIM: '$0' <input_directory> <output_directory> <plsa-sim|lda-sim>  <language> <topics_number> <number_bigrams> <word_association_measure>'
  echo 'Usage for PLSA-ITER: '$0' <input_directory> <output_directory> <plsa-iter|lda-iter> <language> <topics_number> <top_unigrams_number> <number_iterations>'
  exit 1
fi

if [ ! -d $1 ]; then
  echo 'Wrong input directory'
  exit 1
fi

mkdir -p $2

if [ $3 != 'plsa' ] && [ $3 != 'lda' ] && [ $3 != 'plsa-iter' ] && [ $3 != 'lda-iter' ] && [ $3 != 'plsa-sim' ] && [ $3 != 'lda-sim' ]; then
  echo 'Wrong topic model algorithm'
  exit 1
fi

if [ $4 != 'russian' ] && [ $4 != 'english' ]; then
  echo 'Wrong language of the text corpus'
  exit 1
fi

positive_number="^[0-9]+$"
if ! [[ $5 =~ $positive_number ]]; then
    echo 'Wrong topics number'
    exit 1
fi

if [ $3 == 'plsa' ] || [ $3 == 'lda' ]; then
  if [ "$#" -eq 5 ]; then
    word_association_measure='no'
  else
    word_association_measure=$7
    if ! [[ $6 =~ $positive_number ]]; then
      echo 'Wrong number of bigrams to add'
      exit 1
    fi
  fi
elif [ $3 == 'plsa-sim' ] || [ $3 == 'lda-sim' ]; then
  word_association_measure=$7
  if ! [[ $6 =~ $positive_number ]]; then
    echo 'Wrong number of bigrams to add'
    exit 1
  fi
else
  word_association_measure='no'
  if ! [[ $6 =~ $positive_number ]]; then
    echo 'Wrong number of top unigrams to consider'
    exit 1
  fi
  if ! [[ $7 =~ $positive_number ]]; then
    echo 'Wrong number of iterations in PLSA-ITER algorithm'
    exit 1
  fi
fi

InputConverter/input_converter --language=$4 --minimum_frequency=5 --input_directory_name=$1 \
--unigram_topic_input_filename=$2/unigram_topic_input.txt \
--unigram_topic_vocabulary_filename=$2/unigram_topic_vocabulary.txt \
--bigram_topic_input_filename=$2/bigram_topic_input.txt \
--bigram_topic_vocabulary_filename=$2/bigram_topic_vocabulary.txt \
--word_association_measures=$word_association_measure --word_association_directory_name=$2/Bigrams

cat $2/unigram_topic_vocabulary.txt $2/bigram_topic_vocabulary.txt > $2/topic_vocabulary.txt
wikipedia_directory=`realpath $0`
wikipedia_directory=`dirname $wikipedia_directory`/../Data/Wikipedia/$4/lemmatized

if [ ! -d ${wikipedia_directory} ]; then
  pushd . > /dev/null
  cd InvertedIndex
  ./download_wikipedia.sh $4
  popd > /dev/null
fi

InvertedIndex/IndexCreator/inverted_index_creator --language=$4 --source_directory_name=$wikipedia_directory \
--vocabulary_filename=$2/topic_vocabulary.txt --inverted_index_filename=$2/inverted_index.txt

alpha=0
beta=0
if [[ $3 == *'lda'* ]]; then
  alpha=0.5
  beta=0.01
fi

if [ $3 == 'plsa' ] || [ $3 == 'lda' ]; then
  if [ "$#" -eq 5 ]; then
    TopicModels/topic_model --input_filename=$2/unigram_topic_input.txt \
    --vocabulary_filename=$2/unigram_topic_vocabulary.txt --inverted_index_filename=$2/inverted_index.txt \
    --topics_number=$5 --alpha=$alpha --beta=$beta --maximum_iterations_number=100 --evaluation_measure=ALL \
    --topic_words_filename=$2/topic_words.txt --logging_directory_name=$2/topic_logs
  else
    BigramsAdder/add_bigrams_to_plsa_sim.py $2/unigram_topic_input.txt \
    $2/unigram_topic_vocabulary.txt $2/bigram_topic_input.txt $2/bigram_topic_vocabulary.txt \
    $2/Bigrams/${word_association_measure}.txt $2/topic_input.txt $2/topic_vocabulary.txt -n $6
    TopicModels/topic_model --input_filename=$2/topic_input.txt \
    --vocabulary_filename=$2/topic_vocabulary.txt --inverted_index_filename=$2/inverted_index.txt \
    --topics_number=$5 --alpha=$alpha --beta=$beta --maximum_iterations_number=100 --evaluation_measure=ALL \
    --topic_words_filename=$2/topic_words.txt --logging_directory_name=$2/topic_logs
  fi
elif [[ $3 == *'sim'* ]]; then
  BigramsAdder/add_bigrams_to_plsa_sim.py $2/unigram_topic_input.txt \
  $2/unigram_topic_vocabulary.txt $2/bigram_topic_input.txt $2/bigram_topic_vocabulary.txt \
  $2/Bigrams/${word_association_measure}.txt $2/topic_sim_input.txt $2/topic_sim_vocabulary.txt \
  -n $6 -s $2/topic_sim_similar_sets.txt
  TopicModels/topic_model --input_filename=$2/topic_sim_input.txt \
  --vocabulary_filename=$2/topic_sim_vocabulary.txt --inverted_index_filename=$2/inverted_index.txt \
  --similar_sets_filename=$2/topic_sim_similar_sets.txt --topics_number=$5 \
  --alpha=$alpha --beta=$beta --maximum_iterations_number=100 --evaluation_measure=ALL \
  --topic_words_filename=$2/topic_words.txt --logging_directory_name=$2/topic_logs
else
  prev_iteration=0
  TopicModels/topic_model --input_filename=$2/unigram_topic_input.txt \
  --vocabulary_filename=$2/unigram_topic_vocabulary.txt --inverted_index_filename=$2/inverted_index.txt \
  --topics_number=$5 --alpha=$alpha --beta=$beta --maximum_iterations_number=100 --evaluation_measure=ALL \
  --topic_words_filename=$2/iter_topic_words_${prev_iteration}.txt \
  --logging_directory_name=$2/topic_logs
  for iteration in `seq 1 $7`;
  do
    BigramsAdder/add_bigrams_to_plsa_iter.py $2/unigram_topic_input.txt \
    $2/unigram_topic_vocabulary.txt $2/bigram_topic_input.txt $2/bigram_topic_vocabulary.txt \
    $2/iter_topic_words_${prev_iteration}.txt $2/iter_similar_sets.txt \
    $2/iter_input.txt $2/iter_vocabulary.txt -n $6 -l $4
    TopicModels/topic_model --input_filename=$2/iter_input.txt \
    --vocabulary_filename=$2/iter_vocabulary.txt --inverted_index_filename=$2/inverted_index.txt \
    --similar_sets_filename=$2/iter_similar_sets.txt --topics_number=$5 \
    --alpha=$alpha --beta=$beta --maximum_iterations_number=100 --evaluation_measure=ALL \
    --topic_words_filename=$2/iter_topic_words_${iteration}.txt \
    --logging_directory_name=$2/topic_logs
    ((++prev_iteration))
  done
fi
