// Copyright 2013 Michael Nokel
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <iostream>
#include <set>
#include <stdexcept>
#include <string>
#include <vector>
#include "./auxiliary.h"
#include "./program_options_parser.h"

using namespace boost::program_options;
using boost::algorithm::to_lower;
using boost::filesystem::create_directories;
using boost::filesystem::path;
using boost::is_any_of;
using boost::split;
using std::cout;
using std::runtime_error;
using std::set;
using std::string;
using std::vector;

void ProgramOptionsParser::Parse(int argc, char** argv) {
  variables_map program_options_storage;
  string configuration_filename = "";
  string algorithm_string = "";
  string algorithm_options_string = "";
  string evaluation_measure_string = "";
  options_description program_options_description("Options for program:");
  program_options_description.add_options()
      ("help,h", value<bool>(&help_message_printed_)->implicit_value(true),
          "Print help message and exit")
      ("config_filename,c", value<string>(&configuration_filename),
          "Configuration file")
      ("input_filename", value<string>(&input_filename_), "Input file")
      ("vocabulary_filename", value<string>(&vocabulary_filename_),
          "Vocabulary file")
      ("similar_sets_filename", value<string>(&similar_sets_filename_),
          "File containing sets of similar words")
      ("inverted_index_filename",
          value<string>(&inverted_index_filename_),
          "File containing pre-computed inverted index")
      ("topics_number", value<unsigned int>(&topics_number_),
          "Number of topics")
      ("alpha", value<double>(&alpha_), "Alpha hyperparameter")
      ("beta", value<double>(&beta_), "Beta hyperparameter")
      ("maximum_iterations_number",
          value<unsigned int>(&maximum_iterations_number_),
          "Maximum number of iterations")
      ("evaluation_measure", value<string>(&evaluation_measure_string),
          "Evaluation measure to use")
      ("topic_words_filename", value<string>(&topic_words_filename_),
          "File for printing words for each topic")
      ("logging_directory_name", value<string>(&logging_directory_name_),
          "Directory for logging");
  store(parse_command_line(argc, argv, program_options_description),
        program_options_storage);
  notify(program_options_storage);
  if (!configuration_filename.empty()) {
    store(parse_config_file<char>(configuration_filename.data(),
                                  program_options_description),
          program_options_storage);
    notify(program_options_storage);
  }
  if (help_message_printed_) {
    cout << program_options_description << "\n";
  }
  evaluation_measures_ = ParseEvaluationMeasures(evaluation_measure_string);
  if (evaluation_measures_.empty()) {
    evaluation_measures_.insert(EvaluationMeasure::NO_MEASURE);
  }
  CorrectDirectoryName(&logging_directory_name_);
}


set<EvaluationMeasure> ProgramOptionsParser::ParseEvaluationMeasures(
    const string& measure_string) const {
  set<EvaluationMeasure> evaluation_measures;
  string measures = measure_string;
  to_lower(measures);
  vector<string> measures_vector;
  split(measures_vector, measures, is_any_of(","));
  for (const auto& measure : measures_vector) {
    if (measure == "perplexity") {
      evaluation_measures.insert(EvaluationMeasure::PERPLEXITY);
    } else if (measure == "tc-pmi") {
      evaluation_measures.insert(EvaluationMeasure::TC_PMI);
    } else if (measure == "tc-pmi-nsim") {
      evaluation_measures.insert(EvaluationMeasure::TC_PMI_NSIM);
    } else if (measure == "all") {
      evaluation_measures.insert(EvaluationMeasure::PERPLEXITY);
      evaluation_measures.insert(EvaluationMeasure::TC_PMI);
      evaluation_measures.insert(EvaluationMeasure::TC_PMI_NSIM);
    } else if (measure == "no") {
      evaluation_measures.insert(EvaluationMeasure::NO_MEASURE);
    } else {
      throw runtime_error("Wrong evaluation measure");
    }
  }
  return evaluation_measures;
}

void ProgramOptionsParser::CorrectDirectoryName(string* directory_name)
    const {
  path path_separator("/");
  string path_separator_string = path_separator.make_preferred().native();
  if (directory_name->back() != path_separator_string[0]) {
    directory_name->push_back(path_separator_string[0]);
  }
  create_directories(*directory_name);
}
