// Copyright 2014 Michael Nokel
#pragma once

#include <string>
#include <vector>


/**
  @brief Class for working with similar sets

  This class should be used for parsing file containing similar sets and
  detecting similar bigrams and unigrams among all words
*/
class SimilarNGramsFinder {
 public:
  /**
    Parses file containing sets with similar words
    @param filename file containing sets with similar words
    @param vocabulary vector containing vocabulary
    @throw std::runtime_error in case of any occurred error
  */
  void ParseSimilarSetsFile(const std::string& filename,
                            const std::vector<std::string>& vocabulary);

 private:
  /** Vector containing sets of similar words */
  std::vector<std::vector<unsigned int>> similar_sets_;

 public:
  /**
    Returns index of the similar set containing given word
    @param word word to get index of the similar set to
    @return index of the similar set containing given word or -1 otherwise
  */
  int GetSimilarSetIndex(unsigned int word) const;

  /**
    Returns iterator to the beginning of the vector containing similar sets
    @return iterator to the beginning of the vector containing similar sets
  */
  std::vector<std::vector<unsigned int>>::const_iterator GetStartIterator()
      const {
    return similar_sets_.cbegin();
  }

  /**
    Returns iterator to the end of the vector containing similar sets
    @return iterator to the end of the vector containing similar sets
  */
  std::vector<std::vector<unsigned int>>::const_iterator GetEndIterator()
      const {
    return similar_sets_.cend();
  }
};