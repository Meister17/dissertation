// Copyright 2013 Michael Nokel
#include <boost/thread.hpp>
#include <algorithm>
#include <iostream>
#include <map>
#include <memory>
#include <set>
#include <stdexcept>
#include <string>
#include <thread>
#include <vector>
#include "./topic_model_analyzer.h"
#include "./topic_model.h"

using boost::mutex;
using std::accumulate;
using std::cout;
using std::find;
using std::map;
using std::numeric_limits;
using std::pair;
using std::runtime_error;
using std::set;
using std::string;
using std::thread;
using std::unique_ptr;
using std::vector;

void TopicModelAnalyzer::Init(const string& input_filename,
                              const string& vocabulary_filename,
                              const string& similar_sets_filename) {
  file_loader_.ParseVocabularyFile(vocabulary_filename, &vocabulary_);
  if (!similar_sets_filename.empty()) {
    file_loader_.ParseSimilarSetsFile(similar_sets_filename, vocabulary_);
  }
  file_loader_.ParseInputFile(input_filename, vocabulary_, &source_data_);
}

void TopicModelAnalyzer::EstimateModel(
    const set<EvaluationMeasure>& evaluation_measures,
    const string& inverted_index_filename) {
  cout << "Starting topic model algorithm\n";
  bool compute_perplexity =
      evaluation_measures.find(EvaluationMeasure::PERPLEXITY) !=
      evaluation_measures.end();
  bool compute_train_set_measures = !compute_perplexity ||
      evaluation_measures.size() > 1;
  if (compute_perplexity && compute_train_set_measures) {
    thread perplexity_thread(&TopicModelAnalyzer::CalculatePerplexity,
                             this,
                             false);
    thread train_set_thread(&TopicModelAnalyzer::CalculateTrainSetMeasures,
                            this,
                            evaluation_measures,
                            inverted_index_filename);
    perplexity_thread.join();
    train_set_thread.join();
  } else if (compute_perplexity) {
    CalculatePerplexity(true);
  } else {
    CalculateTrainSetMeasures(evaluation_measures, inverted_index_filename);
  }
}

void TopicModelAnalyzer::CalculatePerplexity(bool store_new_vocabulary) {
  vector<string> train_vocabulary;
  vector<map<unsigned int, unsigned int>> train_data;
  vector<map<unsigned int, unsigned int>> test_train_data;
  vector<map<unsigned int, unsigned int>> test_data;
  file_loader_.SplitToTrainAndTest(vocabulary_,
                                   source_data_,
                                   TEST_PART_,
                                   &train_vocabulary,
                                   &train_data,
                                   &test_train_data,
                                   &test_data);
  if (store_new_vocabulary) {
    // only we are writing -> no race condition
    vocabulary_ = train_vocabulary;
    source_data_ = train_data;
  }
  perplexity_topic_model_.EstimateModel(train_data,
                                        train_vocabulary.size(),
                                        TOPICS_NUMBER_);
  perplexity_topic_model_.EstimateModelOnTestTrainPart(test_train_data,
                                                       TOPICS_NUMBER_);
  perplexity_topic_model_.CalculatePerplexity(test_data);
}

void TopicModelAnalyzer::CalculateTrainSetMeasures(
    const set<EvaluationMeasure>& evaluation_measures,
    const string& inverted_index_filename) {
  train_set_topic_model_.EstimateModel(source_data_,
                                       vocabulary_.size(),
                                       TOPICS_NUMBER_);
  for (const auto& measure : evaluation_measures) {
    switch (measure) {
      case EvaluationMeasure::TC_PMI:
        train_set_topic_model_.CalculateTCPMI(file_loader_,
                                              vocabulary_,
                                              false,
                                              inverted_index_filename);
        break;
      case EvaluationMeasure::TC_PMI_NSIM:
        train_set_topic_model_.CalculateTCPMI(file_loader_,
                                              vocabulary_,
                                              true,
                                              inverted_index_filename);
        break;
      case EvaluationMeasure::PERPLEXITY: case EvaluationMeasure::NO_MEASURE:
        break;
      default:
        throw runtime_error("Wrong evaluation measure to compute");
    }
  }
}

void TopicModelAnalyzer::PrintResults(const string& topic_words_filename)
                                      const {
  cout << "Printing results\n";
  vector<vector<double>> phi = train_set_topic_model_.phi();
  if (phi.empty()) {
    phi = perplexity_topic_model_.phi();
  }
  file_loader_.PrintTopicWords(topic_words_filename, vocabulary_, phi);
}
