// Copyright 2014 Michael Nokel
#include <boost/algorithm/string.hpp>
#include <algorithm>
#include <cctype>
#include <iostream>
#include <fstream>
#include <functional>
#include <stdexcept>
#include <string>
#include <vector>
#include "./similar_ngrams_finder.h"

using boost::is_any_of;
using boost::split;
using std::cout;
using std::find_if;
using std::getline;
using std::ifstream;
using std::isspace;
using std::lower_bound;
using std::not1;
using std::ptr_fun;
using std::runtime_error;
using std::sort;
using std::string;
using std::vector;

void SimilarNGramsFinder::ParseSimilarSetsFile(
    const string& filename,
    const vector<string>& vocabulary) {
  cout << "Parsing file with similar words\n";
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to parse file with similar sets");
  }
  while (!file.eof()) {
    string line;
    getline(file, line);
    line.erase(find_if(line.rbegin(),
                       line.rend(),
                       not1(ptr_fun<int, int>(isspace))).base(),
               line.end());
    if (line.empty()) {
      continue;
    }
    vector<string> tokens;
    split(tokens, line, is_any_of("\t"));
    vector<unsigned int> similar_words;
    for (const auto& token : tokens) {
      auto iterator = lower_bound(vocabulary.begin(), vocabulary.end(), token);
      if (iterator == vocabulary.end() || *iterator != token) {
        std::cerr << token << std::endl;
        throw runtime_error("Failed to parse file with similar sets");
      }
      similar_words.push_back(iterator - vocabulary.begin());
    }
    sort(similar_words.begin(), similar_words.end());
    similar_sets_.push_back(similar_words);
  }
  file.close();
  if (similar_sets_.empty()) {
    throw runtime_error("Failed to parse file with similar sets");
  }
  std::cerr << similar_sets_.size() << " similar sets loaded\n";
}

int SimilarNGramsFinder::GetSimilarSetIndex(unsigned int word) const {
  for (unsigned int index = 0; index < similar_sets_.size(); ++index) {
    auto iterator = lower_bound(similar_sets_[index].begin(),
                                similar_sets_[index].end(),
                                word);
    if (iterator != similar_sets_[index].end() && *iterator == word) {
      return index;
    }
  }
  return -1;
}
