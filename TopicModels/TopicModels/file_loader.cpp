// Copyright 2013 Michael Nokel
#include <boost/algorithm/string.hpp>
#include <boost/iterator/counting_iterator.hpp>
#include <algorithm>
#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <random>
#include <set>
#include <stdexcept>
#include <string>
#include <vector>
#include "./file_loader.h"
#include "./auxiliary.h"

using boost::counting_iterator;
using boost::is_any_of;
using boost::split;
using std::cout;
using std::getline;
using std::find_if;
using std::ifstream;
using std::isspace;
using std::lower_bound;
using std::map;
using std::max;
using std::min;
using std::mt19937;
using std::not1;
using std::ofstream;
using std::ptr_fun;
using std::runtime_error;
using std::set;
using std::set_intersection;
using std::shuffle;
using std::sort;
using std::stoi;
using std::string;
using std::vector;


void FileLoader::ParseVocabularyFile(const string& filename,
                                     vector<string>* vocabulary) {
  cout << "Parsing vocabulary\n";
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to parse vocabulary file");
  }
  while (!file.eof()) {
    string line;
    getline(file, line);
    line.erase(find_if(line.rbegin(),
                       line.rend(),
                       not1(ptr_fun<int, int>(isspace))).base(),
               line.end());
    vector<string> tokens;
    split(tokens, line, is_any_of("\t"));
    if (tokens.size() <= 2) {
      continue;
    }
    vocabulary->push_back(tokens[0]);
  }
  file.close();
  if (vocabulary->empty()) {
    throw runtime_error("Failed to parse vocabulary");
  }
}

void FileLoader::ParseInputFile(
    const string& filename,
    const vector<string>& vocabulary,
    vector<map<unsigned int, unsigned int>>* source_data) const {
  cout << "Parsing input file\n";
  set<unsigned int> ngrams;
  for (auto iterator = similar_ngrams_finder_.GetStartIterator(); iterator !=
       similar_ngrams_finder_.GetEndIterator(); ++iterator) {
    for (const auto& similar_word : *iterator) {
      string ngram = vocabulary[similar_word];
      if (count(ngram.begin(), ngram.end(), ' ') > 0) {
        ngrams.insert(similar_word);
      }
    }
  }
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to parse input file");
  }
  while (!file.eof()) {
    string line;
    getline(file, line);
    line.erase(find_if(line.rbegin(),
                       line.rend(),
                       not1(ptr_fun<int, int>(isspace))).base(),
               line.end());
    if (line.empty()) {
      continue;
    }
    vector<string> tokens;
    split(tokens, line, is_any_of(" \t"));
    if (tokens.size() > 0) {
      StoreNewDocument(tokens, ngrams, source_data);
    }
  }
  file.close();
}

void FileLoader::StoreNewDocument(
    const vector<string>& tokens,
    const set<unsigned int>& ngrams,
    vector<map<unsigned int, unsigned int>>* source_data) const {
  map<unsigned int, unsigned int> ndw;
  map<unsigned int, unsigned int> ngrams_ndw;
  for (const auto& ngram : ngrams) {
    ngrams_ndw[ngram] = 0;
  }
  vector<unsigned int> words;
  for (const auto& token : tokens) {
    vector<string> elements;
    split(elements, token, is_any_of(":"));
    if (elements.size() != 2) {
      throw runtime_error("Failed to parse input file");
    }
    ndw[stoi(elements.front())] = stoi(elements.back());
    words.push_back(stoi(elements.front()));
  }
  source_data->push_back(ndw);
  sort(words.begin(), words.end());
  for (auto iterator = similar_ngrams_finder_.GetStartIterator(); iterator !=
       similar_ngrams_finder_.GetEndIterator(); ++iterator) {
    vector<unsigned int> similar_words_in_document(min(words.size(),
                                                       iterator->size()));
    similar_words_in_document.resize(
        set_intersection(words.begin(),
                         words.end(),
                         iterator->begin(),
                         iterator->end(),
                         similar_words_in_document.begin()) -
        similar_words_in_document.begin());
    if (similar_words_in_document.size() > 1) {
      for (const auto& source_word : similar_words_in_document) {
        auto found_iterator = ngrams_ndw.find(source_word);
        if (found_iterator != ngrams_ndw.end()) {
          for (const auto& similar_word : similar_words_in_document) {
            if (source_word != similar_word) {
              found_iterator->second += ndw[similar_word];
            }
          }
        } else {
          for (const auto& similar_word : similar_words_in_document) {
            if (source_word != similar_word) {
              source_data->back()[source_word] += ndw[similar_word];
            }
          }
        }
      }
    }
  }
  for (const auto& ngram_ndw : ngrams_ndw) {
    if (ngram_ndw.second > 0) {
      source_data->back()[ngram_ndw.first] += ngram_ndw.second;
    }
  }
}

void FileLoader::SplitToTrainAndTest(
    const vector<string>& vocabulary,
    const vector<map<unsigned int, unsigned int>>& source_data,
    double test_part,
    vector<string>* train_vocabulary,
    vector<map<unsigned int, unsigned int>>* train_data,
    vector<map<unsigned int, unsigned int>>* test_train_data,
    vector<map<unsigned int, unsigned int>>* test_data) const {
  unsigned int test_index = (1.0 - test_part) * source_data.size();
  vector<unsigned int> indices(
      counting_iterator<unsigned int>(0),
      counting_iterator<unsigned int>(source_data.size()));
  mt19937 generator(SEED_NUMBER_);
  shuffle(indices.begin(), indices.end(), generator);
  for (unsigned int index = 0; index < test_index; ++index) {
    train_data->push_back(source_data[indices[index]]);
  }
  map<unsigned int, unsigned int> train_vocabulary_map;
  for (const auto& document : *train_data) {
    for (const auto& word : document) {
      train_vocabulary_map.insert({word.first, train_vocabulary_map.size()});
    }
  }
  train_vocabulary->resize(train_vocabulary_map.size(), "");
  for (const auto& word_pair : train_vocabulary_map) {
    train_vocabulary->at(word_pair.second) = vocabulary[word_pair.first];
  }
  for (auto& document : *train_data) {
    map<unsigned int, unsigned int> new_data;
    for (const auto& word : document) {
      new_data.insert({train_vocabulary_map[word.first], word.second});
    }
    document = new_data;
  }
  for (unsigned int index = test_index; index < source_data.size(); ++index) {
    vector<unsigned int> words_in_document;
    for (const auto& element : source_data[indices[index]]) {
      auto found_iterator = train_vocabulary_map.find(element.first);
      if (found_iterator != train_vocabulary_map.end()) {
        for (unsigned int number = 0; number < element.second; ++number) {
          words_in_document.push_back(found_iterator->second);
        }
      }
    }
    if (words_in_document.size() > 1) {
      shuffle(words_in_document.begin(), words_in_document.end(), generator);
      unsigned int split_index = words_in_document.size() / 2;
      AppendToTestData(words_in_document, 0, split_index, test_train_data);
      AppendToTestData(words_in_document,
                       split_index,
                       words_in_document.size(),
                       test_data);
    }
  }
}

void FileLoader::AppendToTestData(
    const vector<unsigned int>& words_in_document,
    unsigned int start_index,
    unsigned int end_index,
    vector<map<unsigned int, unsigned int>>* test_data) const {
  test_data->push_back(map<unsigned int, unsigned int>());
  for (unsigned int index = start_index; index < end_index; ++index) {
    auto result = test_data->back().insert({words_in_document[index], 1});
    if (!result.second) {
      ++result.first->second;
    }
  }
}

void FileLoader::PrintTopicWords(const string& filename,
                                 const vector<string>& vocabulary,
                                 const vector<vector<double>>& phi) const {
  ofstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to open file for printing top words");
  }
  for (unsigned int topic = 0; topic < phi.front().size(); ++topic) {
    file << "Topic " << topic << ":\n";
    map<double, vector<unsigned int>> words_in_topic;
    for (unsigned int word = 0; word < phi.size(); ++word) {
      auto result = words_in_topic.insert({phi[word][topic],
                                           vector<unsigned int>()});
      result.first->second.push_back(word);
    }
    for (auto iterator = words_in_topic.rbegin();
         iterator != words_in_topic.rend(); ++iterator) {
      for (const auto& word : iterator->second) {
        file << "\t" << vocabulary[word] << "\t" << iterator->first << "\n";
      }
    }
  }
  file.close();
}