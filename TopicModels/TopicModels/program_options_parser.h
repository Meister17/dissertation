// Copyright 2013 Michael Nokel
#pragma once

#include <set>
#include <string>
#include <vector>
#include "./auxiliary.h"

/**
  @brief Class for working with program options

  This class should be used for parsing program options from command line and/or
  from configuration file. After what one can access each necessary program
  option.
*/
class ProgramOptionsParser {
 public:
  /**
    Parses program options from command line and/or from configuration file
    @param argc number of command line arguments (from main function)
    @param argv command line arguments (from main function)
    @throw std::exception in case of any occurred error
  */
  void Parse(int argc, char** argv);

 private:
  /** Flag indicating whether help message has been printed or not */
  bool help_message_printed_ = false;

  /** File containing input data for topic model algorithm */
  std::string input_filename_ = "";

  /** File containing vocabulary for topic model algorithm */
  std::string vocabulary_filename_ = "";

  /** File containing sets of similar n-grams */
  std::string similar_sets_filename_ = "";

  /** File containing pre-computed inverted index (via Wikipedia) */
  std::string inverted_index_filename_ = "";

  /** Desired number of topics to reveal */
  unsigned int topics_number_ = 100;

  /** Alpha hyperparameter */
  double alpha_ = 0.0;

  /** Beta hyperparameter */
  double beta_ = 0.0;

  /** Maximum number of iterations */
  unsigned int maximum_iterations_number_ = 0;

  /** Evaluation measures to compute */
  std::set<EvaluationMeasure> evaluation_measures_;

  /** File for printing words that form each topic */
  std::string topic_words_filename_ = "";

  /** Directory for logging */
  std::string logging_directory_name_ = "";

 public:
  bool help_message_printed() const noexcept {
    return help_message_printed_;
  }

  std::string input_filename() const noexcept {
    return input_filename_;
  }

  std::string vocabulary_filename() const noexcept {
    return vocabulary_filename_;
  }

  std::string similar_sets_filename() const noexcept {
    return similar_sets_filename_;
  }

  std::string inverted_index_filename() const noexcept {
    return inverted_index_filename_;
  }

  unsigned int topics_number() const noexcept {
    return topics_number_;
  }

  double alpha() const noexcept {
    return alpha_;
  }

  double beta() const noexcept {
    return beta_;
  }

  unsigned int maximum_iterations_number() const noexcept {
    return maximum_iterations_number_;
  }

  std::set<EvaluationMeasure> evaluation_measure() const noexcept {
    return evaluation_measures_;
  }

  std::string topic_words_filename() const noexcept {
    return topic_words_filename_;
  }

  std::string logging_directory_name() const noexcept {
    return logging_directory_name_;
  }

 private:
  /**
    Parse evaluaton measures from their string representations
    @param measure_string string representation of evaluation measures
    @return set containing enum values corresponding to given evaluation
    measures
    @throw std::runtime_error in case of wrong evaluation measures
  */
  std::set<EvaluationMeasure> ParseEvaluationMeasures(
      const std::string& measure_string) const;

  /**
    Parses topic model algorithm options from their string representation
    @param algorithm_options string representation of topic model algorithm
    options
    @return vector containing topic model algorithm options
  */
  std::vector<std::string> ParseAlgorithmOptions(
      const std::string& algorithm_options) const noexcept;

  /**
    Corrects and creates given directory
    @param[out] directory_name name of directory to correct and create
    @throw std::exception in case of any occurred error
  */
  void CorrectDirectoryName(std::string* directory_name) const;
};
