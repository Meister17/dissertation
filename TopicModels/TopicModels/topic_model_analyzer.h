// Copyright 2013 Michael Nokel
#pragma once

#include <boost/thread/condition_variable.hpp>
#include <boost/thread/mutex.hpp>
#include <map>
#include <memory>
#include <set>
#include <string>
#include <vector>
#include "./auxiliary.h"
#include "./file_loader.h"
#include "./similar_ngrams_finder.h"
#include "./topic_model.h"

/**
  Main class for revealing and evaluating topics in text collection.

  This class should be used to parse input files, reveal and evaluate topics,
  and print words within each topic.
*/
class TopicModelAnalyzer {
 public:
  /**
    Initializes object
    @param logging_directory_name directory for logging information
    @param topics_number desired number of topics
    @param alpha alpha hyperparameter
    @param beta beta hyperparameter
    @param maximum_iterations_number maximum number of iterations
  */
  TopicModelAnalyzer(const std::string& logging_directory_name,
                     unsigned int topics_number,
                     double alpha,
                     double beta,
                     unsigned int maximum_iterations_number)
      : TOPICS_NUMBER_(topics_number),
        perplexity_topic_model_(logging_directory_name + "perplexity.log",
                                alpha,
                                beta,
                                maximum_iterations_number),
        train_set_topic_model_(logging_directory_name + "train_set.log",
                               alpha,
                               beta,
                               maximum_iterations_number) {
  }

  /**
    Parses files with input data and vocabulary
    @param input_filename file with input data
    @param vocabulary_filename file with vocabulary
    @param similar_sets_filename file with sets of similar words
    @throw std::runtime_error in case of any occurred error
  */
  void Init(const std::string& input_filename,
            const std::string& vocabulary_filename,
            const std::string& similar_sets_filename);

 protected:
  /** Number for seeding random numbers */
  const unsigned int SEED_NUMBER_ = 239;

  /** Part of text collection to use as a test */
  const double TEST_PART_ = 0.1;

  /** Desired number of topics to reveal */
  const unsigned int TOPICS_NUMBER_ = 100;

  /** Mutex for synchronization */
  boost::mutex mutex_;

  /** Conditional variable for synchronization */
  boost::condition_variable condition_variable_;

  /** Flag indicating whether perplexity thread is prepared for working */
  bool prepared_for_perplexity_ = false;

  /** Object for working with files */
  FileLoader file_loader_;

  /** Vocabulary */
  std::vector<std::string> vocabulary_;

  /** Vector containing words and their frequencies within each document */
  std::vector<std::map<unsigned int, unsigned int>> source_data_;

  /** Object representing topic model algorithm (for training on the subset of
      the whole collection) */
  TopicModel perplexity_topic_model_;

  /** Object representing topic model algorithm (for training on the whole
      collection) */
  TopicModel train_set_topic_model_;

 public:
  /**
    Estimates and evaluates topic model
    @param evaluation_measures vector containing evaluation measures to compute
    @param inverted_index_filename file containing inverted index for the given
    text collection from Wikipedia
  */
  void EstimateModel(const std::set<EvaluationMeasure>& evaluation_measures,
                     const std::string& inverted_index_filename);

  /**
    Prints results of revealing topics in the text collection
    @param topic_words_filename file where words within each topic will be
    printed
    @throw std::runtime_error in case of any occurred error
  */
  void PrintResults(const std::string& topic_words_filename) const;

 private:
  /**
    Calculates Perplexity on the test set
    @param store_new_vocabulary flag indicating whether new train vocabulary
    should be stored
  */
  void CalculatePerplexity(bool store_new_vocabulary);

  /**
    Calculates given evaluation measures on the whole train set
    @param evaluation_measures set containing evaluation measures to compute
    @param inverted_index_filename file containing inverted index of the text
    collection based on Wikipedia
  */
  void CalculateTrainSetMeasures(
      const std::set<EvaluationMeasure>& evaluation_measures,
      const std::string& inverted_index_filename);
};
