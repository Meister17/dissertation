// Copyright 2013 Michael Nokel
#pragma once

#include <string>
#include <vector>

/**
  @brief Enum class containing all possible evaluation measures

  Currently it supports only Perplexity, TC-PMI, TC_PMI_NSIM, while
  NO_MEASURE indicates just constructing topic model without any measuring
*/
enum class EvaluationMeasure {
  NO_MEASURE,
  PERPLEXITY,
  TC_PMI,
  TC_PMI_NSIM
};

/**
  @brief Structure representing bigram

  This structure contains indices of the first and second unigrams forming
  bigram
*/
struct Bigram {
  Bigram(unsigned int first_word, unsigned int second_word)
      : first_unigram(first_word),
        second_unigram(second_word) {
  }

  /** First unigram in the bigram */
  unsigned int first_unigram;

  /** Second unigram in the bigram */
  unsigned int second_unigram;

  /**
    Comparison operator for organizing a set
    @param other other bigram to compare with
    @return true if current bigram is less than given another one
  */
  bool operator<(const Bigram& other) const noexcept {
    if (first_unigram == other.first_unigram) {
      return second_unigram < other.second_unigram;
    }
    return first_unigram < other.first_unigram;
  }
};