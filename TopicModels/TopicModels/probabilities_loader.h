// Copyright 2014 Michael Nokel
#pragma once

#include <map>
#include <set>
#include <string>
#include <vector>
#include "./auxiliary.h"

/**
  @brief Class for working with files containing pre-computed single word and
  word co-occurrence probabilities

  This class should be used for parsing file containing pre-computed single-word
  and word co-occurrence probabilities, after what one can obtain desired
  probabilities
*/
class ProbabilitiesLoader {
 public:
  /**
    Parses file containing pre-computed single word and word co-occurrence
    probabilities
    @param filename file containing pre-compured single word and word
    co-occurrence probabilities
    @param top_words set containing top words from each topic
    @param vocabulary vocabulary containing all words
    @throw std::runtime_error in case of any occurred error
  */
  void LoadFile(const std::string& filename,
                const std::set<unsigned int>& top_words,
                const std::vector<std::string>& vocabulary);

 protected:
  /** Map containing unigram probabilities */
  std::map<unsigned int, double> unigram_probabilities_;

  /** Map containing bigram probabilities */
  std::map<Bigram, double> bigram_probabilities_;

 public:
  /**
    Returns probability of the given unigram
    @param unigram unigram to get probability for
    @return probability of the given unigram
  */
  double GetUnigramProbability(unsigned int unigram) const {
    auto iterator = unigram_probabilities_.find(unigram);
    if (iterator != unigram_probabilities_.end()) {
      return iterator->second;
    }
    return 0.0;
  }

  /**
    Returns probability of the given bigram
    @param first_unigram first unigram forming bigram
    @param second_unigram second unigram forming bigram
    @return probability of the given bigram
  */
  double GetBigramProbability(unsigned int first_unigram,
                              unsigned int second_unigram) const {
    auto iterator = bigram_probabilities_.find(Bigram(first_unigram,
                                                      second_unigram));
    if (iterator != bigram_probabilities_.end()) {
      return iterator->second;
    }
    return 0.0;
  }
};