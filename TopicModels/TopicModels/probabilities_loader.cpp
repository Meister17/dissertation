// Copyright 2014 Michael Nokel
#include <boost/algorithm/string.hpp>
#include <algorithm>
#include <fstream>
#include <functional>
#include <map>
#include <set>
#include <string>
#include <vector>
#include "./probabilities_loader.h"
#include "./auxiliary.h"

using boost::is_any_of;
using boost::replace_all;
using boost::split;
using std::find_if;
using std::getline;
using std::ifstream;
using std::isspace;
using std::map;
using std::not1;
using std::ptr_fun;
using std::runtime_error;
using std::set;
using std::set_intersection;
using std::string;
using std::vector;

void ProbabilitiesLoader::LoadFile(const string& filename,
                                   const set<unsigned int>& top_ngrams,
                                   const vector<string>& vocabulary) {
  ifstream file(filename.data(), ifstream::app);
  if (!file) {
    throw runtime_error("Failed to load file with probabilities");
  }
  map<string, unsigned int> top_ngrams_map;
  for (auto ngram : top_ngrams) {
    top_ngrams_map[vocabulary[ngram]] = ngram;
  }
  unsigned int maximum_frequency;
  file.read(reinterpret_cast<char *>(&maximum_frequency), sizeof(unsigned int));
  file.get();
  map<unsigned int, vector<unsigned int>> documents_ngrams;
  string ngram;
  while (!file.eof()) {
    file >> ngram;
    replace_all(ngram, "_", " ");
    file.get();
    unsigned int document, frequency;
    file.read(reinterpret_cast<char *>(&frequency), sizeof(unsigned int));
    auto found_iterator = top_ngrams_map.find(ngram);
    if (found_iterator != top_ngrams_map.end()) {
      unsigned int ngram_index = found_iterator->second;
      unigram_probabilities_[ngram_index] =
          static_cast<double>(frequency) /
          static_cast<double>(maximum_frequency);
      documents_ngrams[ngram_index] = vector<unsigned int>(frequency, 0);
      for (unsigned int index = 0; index < frequency; ++index) {
        file.read(reinterpret_cast<char *>(&document), sizeof(unsigned int));
        documents_ngrams[ngram_index][index] = document;
      }
    } else {
      for (unsigned int index = 0; index < frequency; ++index) {
        file.read(reinterpret_cast<char *>(&document), sizeof(unsigned int));
      }
    }
    file.get();
  }
  file.close();
  if (documents_ngrams.size() != top_ngrams.size()) {
    throw runtime_error("New words found, regenerate inverted index");
  }
  vector<unsigned int> intersection_vector(maximum_frequency);
  for (auto first_iterator = documents_ngrams.begin(); first_iterator !=
       documents_ngrams.end(); ++first_iterator) {
    auto second_iterator = first_iterator;
    for (++second_iterator; second_iterator != documents_ngrams.end();
         ++second_iterator) {
      unsigned int cooccurrence_frequency =
          set_intersection(first_iterator->second.begin(),
                           first_iterator->second.end(),
                           second_iterator->second.begin(),
                           second_iterator->second.end(),
                           intersection_vector.begin()) -
          intersection_vector.begin();
      double probability = static_cast<double>(cooccurrence_frequency) /
          static_cast<double>(maximum_frequency);
      bigram_probabilities_[Bigram(first_iterator->first,
                                   second_iterator->first)] = probability;
      bigram_probabilities_[Bigram(second_iterator->first,
                                   first_iterator->first)] = probability;
    }
  }
}
