// Copyright 2013 Michael Nokel
#pragma once

#include <map>
#include <random>
#include <set>
#include <string>
#include <utility>
#include <vector>
#include "./auxiliary.h"

/**
  @brief Class implementing all topic model algorithms (PLSA, LDA, PLSA-SIM,
  PLSA-ITER)
*/
class TopicModel {
 public:
  /**
    Initializes object
    @param logging_filename file for logging information
    @param alpha alpha hyperparameter
    @param beta beta hyperparameter
    @param maximum_iterations_number maximum number of iterations
  */
  TopicModel(const std::string& logging_filename,
             double alpha,
             double beta,
             unsigned int maximum_iterations_number);

 private:
  /** Seed number for generating random numbers */
  const unsigned int SEED_NUMBER_ = 239;

  /** Number of top words to consider for Topic Coherence measures */
  const unsigned int TOP_WORDS_TC_NUMBER_ = 10;

  /** File for logging information */
  const std::string LOGGING_FILENAME_ = "";

  /** Alpha hyperparameter */
  const double ALPHA_ = 0.0;

  /** Beta hyperparameter */
  const double BETA_ = 0.0;

  /** Maximum number of iterations to perform */
  const unsigned int MAXIMUM_ITERATIONS_NUMBER_ = 1000;

  /** Tolerance to achieve between successive iterations */
  const double TOLERANCE_ = 1e-6;

  /** Vector containing P(w|t) probabilities */
  std::vector<std::vector<double>> phi_;

  /** Vector containing P(t|d) probabilities */
  std::vector<std::vector<double>> theta_;

 public:
  std::vector<std::vector<double>> phi() const {
    return phi_;
  }

  /**
    Estimates model by revealing topics in the given text collection
    @param source_data vector containing words with their frequencies within
    each document
    @param vocabulary_size size of vocabulary in the collection
    @param topics_number desired number of topics
  */
  void EstimateModel(
      const std::vector<std::map<unsigned int, unsigned int>>& source_data,
      unsigned int vocabulary_size,
      unsigned int topics_number);

  /**
    Estimates P(t|d) probabilities on the given test set (for perplexity)
    @param source_data vector containing words with their frequencies within
    each document (train part of the test set)
    @param topics_number desired number of topics
  */
  void EstimateModelOnTestTrainPart(
      const std::vector<std::map<unsigned int, unsigned int>>& source_data,
      unsigned int topics_number);

  /**
    Calculates Perplexity on the test set
    @param source_data vector containing words with their frequencies within
    each document (test set)
    @return Perplexity
  */
  void CalculatePerplexity(
      const std::vector<std::map<unsigned int, unsigned int>>& source_data)
      const;

  /**
    Calculates TC-PMI on the resulted topics
    @param file_loader object for determining similar words
    @param vocabulary vocabulary containing all words
    @param use_similarity flag indicating whether similarity should be used or
    not
    @param inverted_index_filename
    @return TC-PMI
  */
  void CalculateTCPMI(const FileLoader& file_loader,
                      const std::vector<std::string>& vocabulary,
                      bool use_similarity,
                      const std::string& inverted_index_filename) const;

 private:
  /**
    Initializes matrices P(w|t), P(t|d) and P(w|t) for each document (from the
    uniform distribution)
    @param documents_number number of documents in the collection
    @param words_number number of words in the collection
    @param topics_number number of topics in the collection
    @param[out] generator generator of random numbers
  */
  void InitializeProbabilities(unsigned int documents_number,
                               unsigned int words_number,
                               unsigned int topics_number,
                               std::mt19937* generator);

  /**
    Initializes matrix with random numbers generated from the given uniform
    distribution
    @param[out] generator of random numbers
    @param[out] matrix matrix to initialize
  */
  void InitializeMatrix(std::vector<std::vector<double>>* matrix,
                        std::mt19937* generator) const;

  /**
    Calculates Euclidean norm of the given matrix
    @param matrix matrix to calculate norm for
    @return calculated Euclidean norm of the given matrix
  */
  double CalculateNorm(const std::vector<std::vector<double>>& matrix) const;

  /**
    Calculates word probability in the given document
    @param word word to calculate probability for
    @param document document to calculate probability in
    @return word probability in the given document
  */
  double GetWordProbabilityInDocument(unsigned int word,
                                      unsigned int document) const;

  /**
    Forms vector containing top words for given topic
    @param file_loader object for determining similar words
    @param topic topic for which top words should be returned
    @param use_similarity flag indicating whether similarity should be used or
    not
    @return vector where top words for given topic will be stored
  */
  std::vector<unsigned int> GetTopWordsForTopic(const FileLoader& file_loader,
                                                unsigned int topic,
                                                bool use_similarity) const;

  /**
    Computes variant of TC-PMI measure
    @param top_words top words from each topic
    @param vocabulary vocabulary containing all words
    @param filename file containing pre-computed single word and word
    co-occurrence probabilities
    @param use_similarity flag indicating whether similarity should be used or
    not
  */
  void ComputeTCPMIVariant(
      const std::vector<std::vector<unsigned int>>& top_words,
      const std::vector<std::string>& vocabulary,
      const std::string& filename,
      bool use_similarity) const;
};