// Copyright 2013 Michael Nokel
#include <cmath>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <limits>
#include <map>
#include <memory>
#include <random>
#include <set>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>
#include "./auxiliary.h"
#include "./file_loader.h"
#include "./probabilities_loader.h"
#include "./topic_model.h"

using std::accumulate;
using std::cout;
using std::map;
using std::max;
using std::min;
using std::mt19937;
using std::numeric_limits;
using std::ofstream;
using std::pair;
using std::runtime_error;
using std::set;
using std::string;
using std::uniform_real_distribution;
using std::unique_ptr;
using std::vector;

TopicModel::TopicModel(const string& logging_filename,
                       double alpha,
                       double beta,
                       unsigned int maximum_iterations_number)
    : LOGGING_FILENAME_(logging_filename),
      ALPHA_(alpha),
      BETA_(beta),
      MAXIMUM_ITERATIONS_NUMBER_(maximum_iterations_number) {
  ofstream log_file(LOGGING_FILENAME_.data());
  if (!log_file) {
    throw runtime_error("Failed to log information for PLSA");
  }
  log_file.close();
}

void TopicModel::EstimateModel(
    const vector<map<unsigned int, unsigned int>>& source_data,
    unsigned int vocabulary_size,
    unsigned int topics_number) {
  ofstream log_file(LOGGING_FILENAME_.data(), ofstream::out | ofstream::app);
  if (!log_file) {
    throw runtime_error("Failed to log information for PLSA");
  }
  log_file << "Starting PLSA\n";
  mt19937 generator(SEED_NUMBER_);
  InitializeProbabilities(source_data.size(),
                          vocabulary_size,
                          topics_number,
                          &generator);
  vector<vector<double>> nwt(vocabulary_size,
                             vector<double>(topics_number, 0.0));
  vector<vector<double>> ntd(topics_number,
                             vector<double>(source_data.size(), 0.0));
  vector<double> nt(topics_number, 0.0);
  vector<double> nd(source_data.size(), 0.0);
  for (unsigned int document = 0; document < source_data.size(); ++document) {
    for (const auto& word : source_data[document]) {
      nd[document] += word.second;
    }
  }
  double phi_norm = CalculateNorm(phi_);
  double theta_norm = CalculateNorm(theta_);
  double delta_phi_norm = phi_norm;
  double delta_theta_norm = theta_norm;
  for (unsigned int iteration = 1; iteration <= MAXIMUM_ITERATIONS_NUMBER_ &&
       (delta_phi_norm >= TOLERANCE_ || delta_theta_norm >= TOLERANCE_);
       ++iteration) {
    log_file << "Iteration:" << iteration << "; delta phi:" << delta_phi_norm <<
        "; delta theta:" << delta_theta_norm << "\n";
    log_file.flush();
    nwt.assign(nwt.size(), vector<double>(topics_number, 0.0));
    ntd.assign(ntd.size(), vector<double>(source_data.size(), 0.0));
    nt.assign(nt.size(), 0.0);
    for (unsigned int document = 0; document < source_data.size(); ++document) {
      for (const auto& element : source_data[document]) {
        unsigned int word = element.first;
        double ndw = element.second;
        double denominator = 0.0;
        for (unsigned int topic = 0; topic < topics_number; ++topic) {
          denominator += phi_[word][topic] * theta_[topic][document];
        }
        for (unsigned int topic = 0; topic < topics_number; ++topic) {
          double value = phi_[word][topic] * theta_[topic][document];
          if (value > 0.0) {
            value *= ndw / denominator;
            nwt[word][topic] += value;
            ntd[topic][document] += value;
            nt[topic] += value;
          }
        }
      }
    }
    for (unsigned int topic = 0; topic < topics_number; ++topic) {
      for (unsigned int word = 0; word < vocabulary_size; ++word) {
        phi_[word][topic] = (nwt[word][topic] + BETA_) /
            (nt[topic] + BETA_ * vocabulary_size);
      }
      for (unsigned int document = 0; document < source_data.size();
           ++document) {
        theta_[topic][document] = (ntd[topic][document] + ALPHA_) /
            (nd[document] + ALPHA_ * topics_number);
      }
    }
    double prev_norm = phi_norm;
    phi_norm = CalculateNorm(phi_);
    delta_phi_norm = fabs(phi_norm - prev_norm);
    prev_norm = theta_norm;
    theta_norm = CalculateNorm(theta_);
    delta_theta_norm = fabs(theta_norm - prev_norm);
  }
  log_file.close();
}

void TopicModel::EstimateModelOnTestTrainPart(
    const vector<map<unsigned int, unsigned int>>& source_data,
    unsigned int topics_number) {
  ofstream log_file(LOGGING_FILENAME_.data(), ofstream::out | ofstream::app);
  if (!log_file) {
    throw runtime_error("Failed to log information for topic model");
  }
  log_file << "Starting topic model on test part for perplexity\n";
  mt19937 generator(SEED_NUMBER_);
  theta_ = vector<vector<double>>(topics_number,
                                  vector<double>(source_data.size(), 0.0));
  InitializeMatrix(&theta_, &generator);
  vector<vector<double>> ntd(theta_.size(),
                             vector<double>(source_data.size(), 0.0));
  vector<double> nd(source_data.size(), 0.0);
  for (unsigned int document = 0; document < source_data.size(); ++document) {
    for (const auto& word : source_data[document]) {
      nd[document] += word.second;
    }
  }
  double theta_norm = CalculateNorm(theta_);
  double delta_norm = theta_norm;
  for (unsigned int iteration = 1; iteration <= MAXIMUM_ITERATIONS_NUMBER_ &&
       delta_norm >= TOLERANCE_; ++iteration) {
    log_file << "Iteration: " << iteration << "; delta theta:" << delta_norm <<
        "\n";
    log_file.flush();
    ntd.assign(ntd.size(), vector<double>(source_data.size(), 0.0));
    for (unsigned int document = 0; document < source_data.size(); ++document) {
      for (const auto& element : source_data[document]) {
        unsigned int word = element.first;
        double ndw = element.second;
        double denominator = 0.0;
        for (unsigned int topic = 0; topic < topics_number; ++topic) {
          denominator += phi_[word][topic] * theta_[topic][document];
        }
        for (unsigned int topic = 0; topic < topics_number; ++topic) {
          double value = phi_[word][topic] * theta_[topic][document];
          if (value > 0.0) {
            ntd[topic][document] += value * ndw / denominator;
          }
        }
      }
    }
    for (unsigned int topic = 0; topic < topics_number; ++topic) {
      for (unsigned int document = 0; document < source_data.size();
           ++document) {
        theta_[topic][document] = (ntd[topic][document] + ALPHA_) /
            (nd[document] + ALPHA_ * topics_number);
      }
    }
    double prev_norm = theta_norm;
    theta_norm = CalculateNorm(theta_);
    delta_norm = fabs(theta_norm - prev_norm);
  }
  log_file.close();
}

void TopicModel::CalculatePerplexity(
    const vector<map<unsigned int, unsigned int>>& source_data) const {
  cout << "Calculating Perplexity\n";
  double nominator = 0.0;
  double denominator = 0.0;
  for (unsigned int document = 0; document < source_data.size(); ++document) {
    for (const auto& element : source_data[document]) {
      double pwd = max(GetWordProbabilityInDocument(element.first, document),
                       numeric_limits<double>::min());
      nominator += element.second * log(pwd);
      denominator += element.second;
    }
  }
  double perplexity = exp(-nominator / denominator);
  cout << "Perplexity: " << perplexity << "\n";
}

void TopicModel::CalculateTCPMI(const FileLoader& file_loader,
                                const vector<string>& vocabulary,
                                bool use_similarity,
                                const string& inverted_index_filename) const {
  if (use_similarity) {
    cout << "Calculating TC-PMI-nSIM\n";
  } else {
    cout << "Calculating TC-PMI\n";
  }
  vector<vector<unsigned int>> top_words;
  for (unsigned int topic = 0; topic < phi_.front().size(); ++topic) {
    top_words.push_back(
        GetTopWordsForTopic(file_loader, topic, use_similarity));
  }
  ComputeTCPMIVariant(top_words,
                      vocabulary,
                      inverted_index_filename,
                      use_similarity);
}

vector<unsigned int> TopicModel::GetTopWordsForTopic(
    const FileLoader& file_loader,
    unsigned int topic,
    bool use_similarity) const {
  vector<unsigned int> top_words;
  map<double, vector<unsigned int>> words_in_topic;
  for (unsigned int word = 0; word < phi_.size(); ++word) {
    auto result = words_in_topic.insert({phi_[word][topic],
                                         vector<unsigned int>()});
    result.first->second.push_back(word);
  }
  set<unsigned int> similar_sets_indices;
  for (auto iterator = words_in_topic.rbegin(); top_words.size() <
       TOP_WORDS_TC_NUMBER_ && iterator != words_in_topic.rend();
       ++iterator) {
    for (const auto& word : iterator->second) {
      if (top_words.size() < TOP_WORDS_TC_NUMBER_) {
        int index = file_loader.GetSimilarSetIndex(word);
        if (!use_similarity ||
            similar_sets_indices.find(index) == similar_sets_indices.end()) {
          top_words.push_back(word);
          if (index != -1) {
            similar_sets_indices.insert(index);
          }
        }
      }
    }
  }
  return top_words;
}

void TopicModel::ComputeTCPMIVariant(
    const vector<vector<unsigned int>>& top_words,
    const vector<string>& vocabulary,
    const string& filename,
    bool use_similarity) const {
  vector<double> tcpmi(phi_.front().size(), 0.0);
  set<unsigned int> top_words_set;
  for (const auto& topic : top_words) {
    for (auto word : topic) {
      top_words_set.insert(word);
    }
  }
  ProbabilitiesLoader probabilities_loader;
  probabilities_loader.LoadFile(filename, top_words_set, vocabulary);
  for (unsigned int topic = 0; topic < top_words.size(); ++topic) {
    for (unsigned int first_index = 0; first_index < top_words[topic].size();
         ++first_index) {
      double first_probability = probabilities_loader.GetUnigramProbability(
          top_words[topic][first_index]);
      for (unsigned int second_index = first_index + 1; second_index <
           top_words[topic].size(); ++second_index) {
        double bigram_probability = probabilities_loader.GetBigramProbability(
            top_words[topic][first_index],
            top_words[topic][second_index]);
        double second_probability = probabilities_loader.GetUnigramProbability(
            top_words[topic][second_index]);
	      if (bigram_probability > 0) {
          tcpmi[topic] += log(bigram_probability) - log(first_probability) -
              log(second_probability);
	      }
      }
    }
  }
  double tc_pmi = accumulate(tcpmi.begin(), tcpmi.end(), 0.0) / tcpmi.size();
  if (use_similarity) {
    cout << "TC-PMI-nSIM: " << tc_pmi << "\n";
  } else {
    cout << "TC-PMI: " << tc_pmi << "\n";
  }
}

void TopicModel::InitializeProbabilities(unsigned int documents_number,
                                         unsigned int words_number,
                                         unsigned int topics_number,
                                         mt19937* generator) {
  phi_ = vector<vector<double>>(words_number,
                                vector<double>(topics_number, 0.0));
  theta_ = vector<vector<double>>(topics_number,
                                  vector<double>(documents_number, 0.0));
  InitializeMatrix(&phi_, generator);
  InitializeMatrix(&theta_, generator);
}

void TopicModel::InitializeMatrix(vector<vector<double>>* matrix,
                                  mt19937* generator) const {
  uniform_real_distribution<> distribution(numeric_limits<double>::min(), 1);
  for (auto& row : *matrix) {
    for (auto& element : row) {
      element = distribution(*generator);
    }
  }
}

double TopicModel::CalculateNorm(const vector<vector<double>>& matrix) const {
  double norm = 0.0;
  for (const auto& row : matrix) {
    for (const auto& element : row) {
      norm += element * element;
    }
  }
  return sqrt(norm);
}

double TopicModel::GetWordProbabilityInDocument(unsigned int word,
                                                unsigned int document)
    const {
  double probability = 0.0;
  for (unsigned int topic = 0; topic < theta_.size(); ++topic) {
    probability += phi_[word][topic] * theta_[topic][document];
  }
  return probability;
}