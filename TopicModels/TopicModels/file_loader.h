// Copyright 2013 Michael Nokel
#pragma once

#include <map>
#include <set>
#include <string>
#include <vector>
#include "./auxiliary.h"
#include "./similar_ngrams_finder.h"

/**
  @brief Class for parsing, loading input files and printing output files

  This class should be used for parsing and loading input and vocabulary files
  and printing output files
*/
class FileLoader {
 public:
  /**
    Parses and loads file with vocabulary
    @param filename file with vocabulary
    @param[out] vocabulary vector where words will be placed
    @throw std::runtime_error in case of any occurred error
  */
  void ParseVocabularyFile(const std::string& filename,
                           std::vector<std::string>* vocabulary);

  /**
    Parses file containing sets of similar words
    @param filename file containing sets of similar words
    @param vocabulary vector containing vocabulary
    @throw std::runtime_error in case of any occurred error
  */
  void ParseSimilarSetsFile(const std::string& filename,
                            const std::vector<std::string>& vocabulary) {
    similar_ngrams_finder_.ParseSimilarSetsFile(filename, vocabulary);
  }

  /**
    Parses and loads file with input data
    @param filename file with input data
    @param vocabulary already parsed vocabulary
    @param[out] source_data vector containing hash map of words with their
    frequencies in the documents
    @throw std::runtime_error in case of any occurred error
  */
  void ParseInputFile(
      const std::string& filename,
      const std::vector<std::string>& vocabulary,
      std::vector<std::map<unsigned int, unsigned int>>* source_data) const;

  /**
    Splits data to train and test parts
    @param vocabulary vector containing source data vocabulary
    @param source_data vector containing source data
    @param test_part part of the text collection to be used in the test
    @param[out] train_vocabulary vector where new vocabulary with words only in
    the train part will be stored
    @param[out] train_data vector where train data will be stored on which
    P(w|t) will be estimated
    @param[out] test_train_data vector where part of the test data will be
    placed on which P(t|d) will be estimated
    @param[out] test_data vector where part of the test data will be placed on
    which perplexity will be further calculated
  */
  void SplitToTrainAndTest(
      const std::vector<std::string>& vocabulary,
      const std::vector<std::map<unsigned int, unsigned int>>& source_data,
      double test_part,
      std::vector<std::string>* train_vocabulary,
      std::vector<std::map<unsigned int, unsigned int>>* train_data,
      std::vector<std::map<unsigned int, unsigned int>>* test_train_data,
      std::vector<std::map<unsigned int, unsigned int>>* test_data) const;

  /**
    Returns index of the similar set containing given word
    @param word word to get index of the similar set to
    @return index of the similar set containing given word or -1 otherwise
  */
  int GetSimilarSetIndex(unsigned int word) const {
    return similar_ngrams_finder_.GetSimilarSetIndex(word);
  }

  /**
    Prints words within each revealed topic in the descending order of their
    probabilities P(w|t)
    @param filename file where words within each revealed topic will be printed
    @param vocabulary vector containing words
    @param phi vector containing P(w|t) probabilities
    @throw std::runtime_error in case of any occurred error
  */
  void PrintTopicWords(const std::string& filename,
                       const std::vector<std::string>& vocabulary,
                       const std::vector<std::vector<double>>& phi) const;

 private:
  /** Seeding number for generating random numbers */
  const int SEED_NUMBER_ = 239;

  /** Object used for finding similar ngrams */
  SimilarNGramsFinder similar_ngrams_finder_;

 private:
  /**
    Stores new document into the vector of documents
    @param tokens vector containing parsed tokens word:number_occurrences
    @param ngrams set containing ngrams from similar words
    @param[out] source_data vector with words in documents
    @throw std::runtime_error in case of any occurred error
  */
  void StoreNewDocument(
      const std::vector<std::string>& tokens,
      const std::set<unsigned int>& ngrams,
      std::vector<std::map<unsigned int, unsigned int>>* source_data) const;

  /**
    Appends given vector to the forming vector of test data
    @param words_in_document vector containing words in document
    @param start_index index of the first word to add
    @param end_index index of the last word (+1) to add
    @param[out] test_data forming vector of test data where given words should
    be appended
  */
  void AppendToTestData(
      const std::vector<unsigned int>& words_in_document,
      unsigned int start_index,
      unsigned int end_index,
      std::vector<std::map<unsigned int, unsigned int>>* test_data) const;
};