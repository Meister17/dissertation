// Copyright 2014 Michael Nokel
#pragma once

#include <string>

/**
  @brief Enumerator containing all supported languages of the text corpus

  This enumerator contains only Russian and English languages, while NO_LANGUAGE
  indicates an error
*/
enum class Language {
  NO_LANGUAGE,
  RUSSIAN,
  ENGLISH
};

/**
  @brief Structure representing found occurrence via Aho-Corasick algorithm

  This structure contains index of the found ngram and number of spaces between
  words forming found ngram
*/
struct AhoCorasickElement {
  AhoCorasickElement(unsigned int element_index, unsigned int number_of_spaces)
      : ngram_index(element_index),
        spaces_number(number_of_spaces) {
  }

  /** Index of the found ngram */
  unsigned int ngram_index;

  /** Number of spaces between words forming found ngram */
  unsigned int spaces_number;

  /**
    Comparison operator that is necessary for organizing a set
    @param other other element to compare with
  */
  bool operator<(const AhoCorasickElement& other) const {
    return ngram_index < other.ngram_index;
  }
};