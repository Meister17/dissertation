// Copyright 2014 Michael Nokel
#pragma once

#include <boost/filesystem.hpp>
#include <boost/thread.hpp>
#include <boost/threadpool.hpp>
#include <iostream>
#include <memory>
#include <string>
#include "./auxiliary.h"
#include "./file_processors/file_processor.h"
#include "./file_processors/english_file_processor.h"
#include "./file_processors/russian_file_processor.h"

/**
  @brief Main class that parses Wikipedia lemmatized texts and counts desired
  probabilities of given words

  This class should be used for parsing file containing vocabulary and parsing
  source directory for counting word probabilities via sliding window and
  documents. After what one can print results
*/
class WikipediaParser {
 public:
  /**
    Initializes object
    @param language language of the text collection
  */
  explicit WikipediaParser(const Language& language) {
    switch (language) {
      case Language::RUSSIAN:
        file_processor_.reset(new RussianFileProcessor());
        break;
      case Language::ENGLISH:
        file_processor_.reset(new EnglishFileProcessor());
        break;
      case Language::NO_LANGUAGE: default:
        break;
    }
  }

  /**
    Parses file containing vocabulary
    @param vocabulary_filename file containing vocabulary
    @param inverted_index_filename file containing inverted index
    @throw std::runtime_error in case of any occurred error
  */
  void ParseVocabulary(const std::string& vocabulary_filename,
                       const std::string& inverted_index_filename) {
    std::cout << "Parsing vocabulary\n";
    file_processor_->ParseVocabulary(vocabulary_filename,
                                     inverted_index_filename);
  }

  /**
    Checks whether there are some new ngrams to process
    @return true if there are some new ngrams to process
  */
  bool HasNewNGrams() const {
    return file_processor_->HasNewNGrams();
  }

  /**
    Processes source directory for counting word probabilities via sliding
    window and documents
    @param directory_name name of directory for processing
    @param language language of the text corpus
    @throw std::runtime_error in case of any occurred error
  */
  void ProcessDirectory(const std::string& directory_name) {
    std::cout << "Processing Wikipedia lemmatized directory\n";
    boost::threadpool::pool thread_pool(boost::thread::hardware_concurrency());
    for (boost::filesystem::directory_iterator iterator(directory_name);
         iterator != boost::filesystem::directory_iterator(); ++iterator) {
      thread_pool.schedule(boost::bind(&FileProcessor::ProcessFile,
                                       file_processor_.get(),
                                       iterator->path().string()));
    }
  }

  /**
    Prints computed inverted index
    @param output_filename name of file where inverted index will be written
    @throw std::runtime_error in case of any occurred error
  */
  void PrintInvertedIndex(const std::string& output_filename) const {
    std::cout << "Printing inverted index\n";
    file_processor_->PrintInvertedIndex(output_filename);
  }

 private:
  /** Pointer to the object processing files */
  std::unique_ptr<FileProcessor> file_processor_;
};