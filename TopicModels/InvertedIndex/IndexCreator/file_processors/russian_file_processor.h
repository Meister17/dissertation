// Copyright 2014 Michael Nokel
#pragma once

#include <fstream>
#include <set>
#include <string>
#include "./file_processor.h"

/**
  @brief Class for processing Russian files

  This class should be used for processing Russian files and counting single
  word and word co-occurrence frequencies in it
*/
class RussianFileProcessor : public FileProcessor {
 private:
  /**
    Processes given Russian file and counts various word frequencies in it
    @param filename name of Russian file for processing
    @throw std::runtime_error in case of any occurred error
  */
  void ProcessFile(const std::string& filename);

  /**
    Extracts words that have same word forms from the file
    @param[out] file pointer to file from where words should be extracted
    @param[out] words set where new extracted words will be stored
    @param[out] is_end_article flag indicating that end of article was met
    was encountered or not
    @throw std::runtime_error in case of any occurred errors
  */
  void ExtractSameWordForms(std::ifstream* file,
                            std::set<std::string>* words,
                            bool& is_end_article) const;

  /**
    Converts string from UTF8 encoding to CP1251
    @param string_cp1251 string in CP1251 encoding
    @return equivalent string in UTF8 encoding
  */
  std::string ConvertFromCP1251ToUTF8(const std::string& string_cp1251) const;
};