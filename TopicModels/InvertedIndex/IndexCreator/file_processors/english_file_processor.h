// Copyright 2014 Michael Nokel
#pragma once

#include <string>
#include "./file_processor.h"

/**
  @brief Class for processing English files

  This class should be used for processing English files and counting single
  word and word co-occurrence frequencies in it
*/
class EnglishFileProcessor : public FileProcessor {
 private:
  /**
    Processes given English file and counts various word frequencies in it
    @param filename name of English file for processing
    @throw std::runtime_error in case of any occurred error
  */
  void ProcessFile(const std::string& filename);
};