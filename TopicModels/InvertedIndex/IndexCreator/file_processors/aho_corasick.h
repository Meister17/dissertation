// Copyright 2014 Michael Nokel
#pragma once

#include <map>
#include <set>
#include <string>
#include <vector>
#include "../auxiliary.h"

/**
  @brief Class that implements Aho-Corasick algorithm

  This class implements Aho-Corasick algorithm. It should be used for
  identifying unigrams and bigrams from the text collection
*/
class AhoCorasick {
 public:
  AhoCorasick();

  /**
    Adds new ngrams to the trie for further finding them
    @param ngrams ngram to add to the trie
  */
  void AddNGrams(const std::set<std::string>& ngrams);

 private:
  /** Empty transition in the trie */
  const int kEmptyTransition_ = -1;

  /** Vector containing ngrams from vocabulary */
  std::vector<std::string> ngrams_;

  /** Vector containing transition functions */
  std::vector<std::map<std::string, int>> transition_functions_;

  /** Vector containing terminal vertices in the trie */
  std::vector<std::vector<int>> terminal_vertices_;

  /** Vector containing fault functions */
  std::vector<int> fault_functions_;

  /** Vector containing compressed fault functions */
  std::vector<int> compressed_fault_functions_;

 public:
  /**
    Reads words and tries to find vocabulary ngrams among all given words
    @param words set containing new words
    @param[out] current_vertices vector containing current vertices in trie
    @return set containing new found vocabulary ngrams
  */
  std::set<AhoCorasickElement> ReadWords(
      const std::set<std::string>& words,
      std::vector<int>* current_vertices) const;

  /**
    Returns ngram based on the given index
    @param index index of the ngram to return
    @throw std::runtime_error in case of wrong index
  */
  std::string GetNGram(unsigned int index) const;

 private:
  /**
    Adds new ngram to the file
    @param ngram new ngram to add to the file
  */
  void AddNGram(const std::string& ngram);

  /**
    Initializes fault functions for Aho-Corasick algorithm
  */
  void InitializeFaultFunctions();

  /**
    Compresses fault functions for Aho-Corasick algorithm
  */
  void CompressFaultFunctions();
};