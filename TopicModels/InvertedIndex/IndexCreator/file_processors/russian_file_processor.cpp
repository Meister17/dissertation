// Copyright 2014 Michael Nokel
#include <boost/algorithm/string.hpp>
#include <iconv.h>
#include <algorithm>
#include <fstream>
#include <functional>
#include <set>
#include <stdexcept>
#include <string>
#include <vector>
#include "./russian_file_processor.h"

using boost::is_any_of;
using boost::split;
using boost::token_compress_on;
using std::find_if;
using std::getline;
using std::ifstream;
using std::isspace;
using std::not1;
using std::ptr_fun;
using std::runtime_error;
using std::set;
using std::string;
using std::vector;

void RussianFileProcessor::ProcessFile(const string& filename) {
  ifstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to process Russian file");
  }
  vector<int> current_trie_vertices;
  set<unsigned int> ngrams_in_article;
  bool is_end_article = false;
  string line;
  getline(file, line);
  while (!file.eof()) {
    set<string> words;
    ExtractSameWordForms(&file, &words, is_end_article);
    if (is_end_article) {
      FinishProcessingArticle(&current_trie_vertices, &ngrams_in_article);
      is_end_article = false;
    } else if (!words.empty()) {
      set<AhoCorasickElement> new_ngrams =
          aho_corasick_.ReadWords(words, &current_trie_vertices);
      if (!new_ngrams.empty()) {
        for (const auto& element : new_ngrams) {
          ngrams_in_article.insert(element.ngram_index);
        }
      }
    }
  }
  file.close();
}

void RussianFileProcessor::ExtractSameWordForms(ifstream* file,
                                                set<string>* words,
                                                bool& is_end_article) const {
  int first_letter = 0;
  do {
    string line;
    getline(*file, line);
    line.erase(find_if(line.rbegin(),
                       line.rend(),
                       not1(ptr_fun<int, int>(isspace))).base(),
               line.end());
    line.erase(line.begin(),
               find_if(line.begin(),
                       line.end(),
                       not1(ptr_fun<int, int>(isspace))));
    if (line.empty()) {
      continue;
    }
    line = ConvertFromCP1251ToUTF8(line);
    vector<string> tokens;
    split(tokens, line, is_any_of(" \t"), token_compress_on);
    if (tokens[0] == kEndOfArticle_) {
      is_end_article = true;
    } else if (tokens[4] == "ЦК" || tokens[4].find("ЛЕ") != string::npos) {
      words->insert(tokens[tokens.size() - 2]);
    }
    first_letter = file->get();
  } while (!file->eof() && isspace(first_letter));
  if (!file->eof()) {
    file->unget();
  }
}

string RussianFileProcessor::ConvertFromCP1251ToUTF8(
    const string& string_cp1251) const {
  iconv_t cd = iconv_open("utf-8", "cp1251");
  if (cd == (iconv_t) - 1) {
    return "";
  }
  size_t input_size = string_cp1251.length();
  size_t output_size = 3*input_size + 1;
  size_t total_output_size = output_size;
#ifdef _WIN32
  const char* input_string_pointer = string_cp1251.data();
#else
  char* input_string_pointer = const_cast<char*>(string_cp1251.data());
#endif
  char* output_string = new char[output_size];
  char* output_string_pointer = output_string;
  memset(output_string, 0, output_size);
  if ((iconv(cd, &input_string_pointer, &input_size, &output_string_pointer,
             &output_size)) == (size_t) - 1) {
    iconv_close(cd);
    delete[] output_string;
    return "";
  }
  iconv_close(cd);
  string string_utf8 = string(output_string, total_output_size - output_size);
  delete[] output_string;
  return string_utf8;
}
