// Copyright 2015 Michael Nokel
#include <boost/algorithm/string.hpp>
#include <boost/thread/mutex.hpp>
#include <algorithm>
#include <fstream>
#include <functional>
#include <iostream>
#include <set>
#include <stdexcept>
#include <string>
#include <vector>
#include "./file_processor.h"

using boost::is_any_of;
using boost::replace_all;
using boost::split;
using std::cout;
using std::find_if;
using std::getline;
using std::ifstream;
using std::isspace;
using std::not1;
using std::ofstream;
using std::ptr_fun;
using std::runtime_error;
using std::set;
using std::string;
using std::vector;

void FileProcessor::ParseVocabulary(const string& vocabulary_filename,
                                    const string& inverted_index_filename) {
  set<string> ngrams;
  ifstream file(vocabulary_filename.data());
  if (!file) {
    throw runtime_error("Failed to parse vocabulary file");
  }
  string line;
  while (!file.eof()) {
    getline(file, line);
    line.erase(find_if(line.rbegin(),
                       line.rend(),
                       not1(ptr_fun<int, int>(isspace))).base(),
               line.end());
    if (line.empty()) {
      continue;
    }
    vector<string> tokens;
    split(tokens, line, is_any_of("\t"));
    ngrams.insert(tokens[0]);
  }
  file.close();
  file.open(inverted_index_filename.data(), ifstream::binary);
  if (file) {
    unsigned int number;
    file.read(reinterpret_cast<char*>(&number), sizeof(unsigned int));
    file.get();
    string ngram;
    while (!file.eof()) {
      file >> ngram;
      replace_all(ngram, "_", " ");
      file.get();
      unsigned int frequency;
      file.read(reinterpret_cast<char*>(&frequency), sizeof(unsigned int));
      for (unsigned int index = 0; index < frequency; ++index) {
        file.read(reinterpret_cast<char*>(&number), sizeof(unsigned int));
      }
      file.get();
      if (ngrams.find(ngram) != ngrams.end()) {
        ngrams.erase(ngram);
      }
    }
  }
  file.close();
  cout << ngrams.size() << " new ngrams will be processed\n";
  aho_corasick_.AddNGrams(ngrams);
  ngrams_.resize(ngrams.size(), vector<unsigned int>());
}

void FileProcessor::PrintInvertedIndex(const string& filename) const {
  ofstream file(filename.data(), ofstream::app | ofstream::binary);
  if (!file) {
    throw runtime_error("Failed to print inverted index");
  }
  file.write(reinterpret_cast<const char*>(&document_number_),
             sizeof(unsigned int));
  file << '\n';
  for (unsigned int index = 0; index < ngrams_.size(); ++index) {
    string ngram = aho_corasick_.GetNGram(index);
    replace_all(ngram, " ", "_");
    file << ngram << ' ';
    unsigned int frequency = ngrams_[index].size();
    file.write(reinterpret_cast<const char*>(&frequency), sizeof(unsigned int));
    for (auto document : ngrams_[index]) {
      file.write(reinterpret_cast<const char*>(&document),
                 sizeof(unsigned int));
    }
    file << '\n';
  }
  file.close();
}

void FileProcessor::FinishProcessingArticle(
    vector<int>* current_trie_vertices,
      set<unsigned int>* ngrams_in_article) {
  current_trie_vertices->clear();
  boost::mutex::scoped_lock lock(mutex_);
  for (auto ngram : *ngrams_in_article) {
    ngrams_[ngram].push_back(document_number_);
  }
  ngrams_in_article->clear();
  ++document_number_;
}