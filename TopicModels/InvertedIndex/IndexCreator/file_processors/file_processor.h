// Copyright 2014 Michael Nokel
#pragma once

#include <boost/thread/mutex.hpp>
#include <set>
#include <string>
#include <vector>
#include "./aho_corasick.h"

/**
  @brief Base class for processing files

  This class should be used as base for Russian and English file processors.
  After processing files one can obtain inverted index
*/
class FileProcessor {
 public:
  /**
    Parses file containing vocabulary
    @param vocabulary_filename file containing vocabulary
    @param inverted_index_filename file containing inverted index
    @throw std::runtime_error in case of any occurred error
  */
  virtual void ParseVocabulary(const std::string& vocabulary_filename,
                               const std::string& inverted_index_filename);

  /**
    Checks whether there are some new ngrams to process
    @return true if there are some new ngrams to process
  */
  virtual bool HasNewNGrams() const {
    return !ngrams_.empty();
  }

  /**
    Processes given file and extracts all found unique ngrams in it
    @param filename file for processing
  */
  virtual void ProcessFile(const std::string& filename) = 0;

  /**
    Prints extracted inverted index for the given vocabulary
    @param filename file for printing extracted inverted index
    @throw std:::runtime_error in case of any occurred error
  */
  virtual void PrintInvertedIndex(const std::string& filename) const;

 protected:
  /** Label of the end of article */
  const std::string kEndOfArticle_ = "ARTICLESEPARATOR";

  /** Mutex for synchronization between threads */
  boost::mutex mutex_;

  /** Object for parsing vocabulary file and finding occurrences in the text */
  AhoCorasick aho_corasick_;

  /** Current number of processing document */
  unsigned int document_number_ = 0;

  /** Vector containing vector of unique ngrams found in documents */
  std::vector<std::vector<unsigned int>> ngrams_;

  /**
    Finishes processing article
    @param[out] current_trie_vertices current vertices in trie (will be cleared)
    @param[out] ngrams_in_article set containing all found ngrams in article
    (will be cleared)
  */
  virtual void FinishProcessingArticle(
      std::vector<int>* current_trie_vertices,
      std::set<unsigned int>* ngrams_in_article);
};