#!/usr/bin/env bash
if [ '$#' -ne $3 ]; then
  echo 'Usage: '$0' <input_directory> <inverted_index_file> <language>'
  exit 1
fi

if [ ! -d $1 ]; then
  echo 'Wrong input directory'
  exit 1
fi

if [ $3 != 'russian' ] && [ $3 != 'english' ]; then
  echo 'Wrong language specified'
  exit 1
fi

mkdir -p tmp

../InputConverter/input_converter --language=$3 --minimum_frequency=5 --input_directory_name=$1 \
--unigram_topic_input_filename=tmp/unigram_topic_input.txt \
--unigram_topic_vocabulary_filename=tmp/unigram_topic_vocabulary.txt \
--bigram_topic_input_filename=tmp/bigram_topic_input.txt \
--bigram_topic_vocabulary_filename=tmp/bigram_topic_vocabulary.txt \
--word_association_measures=no --word_association_directory_name=tmp

cat tmp/unigram_topic_vocabulary.txt tmp/bigram_topic_vocabulary.txt > tmp/topic_vocabulary.txt

../InvertedIndex/inverted_index_creator --language $3 --source_directory_name=../../../Data/Wikipedia/$3 \
--vocabulary_filename=tmp/topic_vocabulary.txt --inverted_index_filename=$2

rm -rf tmp