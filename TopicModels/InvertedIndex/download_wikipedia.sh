#!/usr/bin/env bash
set -x -e

if [ "$#" -ne 1 ]; then
  echo 'Usage: '$0' <language>'
  exit 1
fi

if [ $1 != 'russian' ] && [ $1 != 'english' ]; then
  echo 'Wrong language: only "russian" and "english" are supported'
  exit 1
fi

base_dir=`realpath $0`
base_dir=`dirname $base_dir`
wikipedia_dir=${base_dir}/../../Data/Wikipedia/$1
mkdir -p $wikipedia_dir
mkdir -p ${wikipedia_dir}/extracted
mkdir -p ${wikipedia_dir}/lemmatized

if [ $1 == 'russian' ]; then
  src_url='https://dumps.wikimedia.org/ruwiki/latest/'
  prefix='ruwiki-latest-pages-articles'
else
  src_url='https://dumps.wikimedia.org/enwiki/latest/'
  prefix='enwiki-latest-pages-articles'
fi

number=1
for i in `curl -s ${src_url} | \grep -P 'href="'${prefix}'\d'`;
do
  if [[ $i == "href"* ]] && [[ $i != *"rss"* ]]; then
    dump=`echo $i | awk -F '"' '{print $2}'`
    wget ${src_url}$dump
    mv $dump ${wikipedia_dir}/$dump
    python ./WikiExtractor.py -o ${wikipedia_dir}/extracted/$number ${wikipedia_dir}/$dump
    rm ${wikipedia_dir}/$dump
    ((++number))
  fi
done

python move_wiki_files.py ${wikipedia_dir}/extracted

if [ $1 == 'russian' ]; then
  python reencode_dir.py ${wikipedia_dir}/extracted cp1251
else
  python reencode_dir.py ${wikipedia_dir}/extracted utf-8
fi

bash ${base_dir}/../../MorphologicalAnalyzer/run_morphological_analyzer.sh ${wikipedia_dir}/extracted ${wikipedia_dir}/lemmatized $1 wiki
rm -rf ${wikipedia_dir}/extracted

echo 'Wikipedia dump downloaded and lemmatized, see '${wikipedia_dir}'/lemmatized'
