#!/usr/bin/env python
from argparse import ArgumentParser
import os

SEPARATOR='ARTICLESEPARATOR\n\n'

def reencode_dir(directory_name, new_encoding):
  for filename in os.listdir(directory_name):
    lines = []
    with open(os.path.join(directory_name, filename), 'r') as file_input:
      for line in file_input:
        if line.startswith('<doc'):
          continue
        elif line.startswith('</doc>'):
          line = SEPARATOR
        lines.append(unicode(line, 'utf-8', errors='ignore').encode(new_encoding, errors='ignore'))
    with open(os.path.join(directory_name, filename), 'w') as file_output:
      for line in lines:
        file_output.write(line)


if __name__ == '__main__':
  parser = ArgumentParser(description='Directory UTF-8 re-encoder')
  parser.add_argument('directory_name', help='Name of directory to reencode')
  parser.add_argument('new_encoding', help='New encoding of files')
  args = parser.parse_args()
  reencode_dir(args.directory_name, args.new_encoding)