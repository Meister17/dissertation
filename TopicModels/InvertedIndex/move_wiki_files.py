#!/usr/bin/env python
from argparse import ArgumentParser
import os
import shutil


def move_wikipedia_articles(directory_name):
  number = 1
  for root_directory in os.listdir(directory_name):
    root_directory = os.path.join(directory_name, root_directory)
    if os.path.isdir(root_directory):
      for root, dirs, files in os.walk(root_directory):
        for filename in files:
          shutil.move(os.path.join(root, filename),
                      os.path.join(directory_name, str(number) + '.txt'))
          number += 1
      shutil.rmtree(root_directory)


if __name__ == '__main__':
  parser = ArgumentParser(description='Moves Wikipedia articles to one directory')
  parser.add_argument('directory_name')
  args = parser.parse_args()
  move_wikipedia_articles(args.directory_name)
