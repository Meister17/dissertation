#!/usr/bin/env python
from nltk.stem import LancasterStemmer, SnowballStemmer

def load_vocabulary(filename):
  vocabulary = {}
  vocabulary_indices = {}
  with open(filename, 'r') as file_input:
    for index, line in enumerate(file_input):
      tokens = line.strip().split('\t')
      vocabulary_indices[len(vocabulary)] = tokens[0]
      vocabulary[tokens[0]] = '\t'.join(tokens[1:])
  return vocabulary, vocabulary_indices


def load_input_data(filename, vocabulary_indices):
  input_data = []
  with open(filename, 'r') as file_input:
    for line in file_input:
      input_data.append({})
      for token in line.strip().split():
        word, frequency = [int(x) for x in token.split(':')]
        input_data[-1][vocabulary_indices[word]] = frequency
  return input_data


def load_topic_input(input_filename, vocabulary_filename):
  print('Loading topic input')
  vocabulary, vocabulary_indices = load_vocabulary(vocabulary_filename)
  return load_input_data(input_filename, vocabulary_indices), vocabulary


def print_vocabulary(vocabulary, filename):
  vocabulary_indices = {}
  with open(filename, 'w') as file_output:
    for index, (word, item_data) in enumerate(sorted(vocabulary.iteritems(), key=lambda x: x[0])):
      vocabulary_indices[word] = index
      file_output.write(word + '\t' + item_data + '\n')
  return vocabulary_indices


def print_input_data(input_data, vocabulary_indices, filename):
  with open(filename, 'w') as file_output:
    for document in input_data:
      for word, frequency in sorted(document.iteritems(), key=lambda x: x[0]):
        file_output.write(str(vocabulary_indices[word]) + ':' + str(frequency) + '\t')
      file_output.write('\n')


def print_topic_input(input_data, vocabulary, input_filename, vocabulary_filename):
  print('Printing topic input with bigrams')
  vocabulary_indices = print_vocabulary(vocabulary, vocabulary_filename)
  print_input_data(input_data, vocabulary_indices, input_filename)


class NoStemmer(object):
  def stem(self, word):
    return word


def init_stemmer(stemmer):
  if stemmer is None:
    return NoStemmer()
  elif stemmer == 'snowball':
    return SnowballStemmer('russian')
  elif stemmer == 'lancaster':
    return LancasterStemmer()
  else:
    raise Exception('Unsupported stemmer was specified')


def stem_ngram(ngram, stem_algorithm):
  stem_words = []
  for word in ngram.split():
    if word == 'of':
      continue
    stem_words.append(stem_algorithm.stem(word))
  return ' '.join(stem_words)


def create_similar_sets(vocabulary, filename, stemmer=None):
  print('Creating sets of similar unigrams and bigrams')
  stem_algorithm = init_stemmer(stemmer)
  stem_dict = {}
  for item, item_data in vocabulary.iteritems():
    parts = item_data.split('\t')
    good_item = False
    for part in parts:
      if part == 'A' or part == 'N' or part == 'B':
        good_item = True
    if good_item:
      stemmed_ngram = stem_ngram(unicode(item, 'utf-8'), stem_algorithm)
      for stemmed_word in stemmed_ngram.split():
        if stemmed_word not in stem_dict:
          stem_dict[stemmed_word] = set()
        stem_dict[stemmed_word].add(item)
  with open(filename, 'w') as file_output:
    for stemmed_word, similar_set in sorted(stem_dict.iteritems()):
      if len(similar_set) > 1 and len(stemmed_word) > 0:
        file_output.write('\t'.join(sorted(similar_set)) + '\n')