#!/usr/bin/env python
from argparse import ArgumentParser
import topic_loader
from add_bigrams_to_plsa_sim import add_bigrams


def load_topics(filename, top_unigrams_number, bigram_vocabulary):
  print('Loading topics')
  bigrams_to_add = set()
  top_unigrams = set()
  with open(filename, 'r') as file_input:
    words = []
    for line in file_input:
      if line.startswith('Topic'):
        words = []
      elif len(words) < top_unigrams_number:
        word = line.strip().split('\t')[0]
        if word.count(' ') == 0:
          words.append(word)
          top_unigrams.add(word)
      elif len(words) == top_unigrams_number:
        for first_index in xrange(top_unigrams_number):
          for second_index in xrange(first_index + 1, top_unigrams_number):
            first_bigram = words[first_index] + ' ' + words[second_index]
            second_bigram = words[second_index] + ' ' + words[first_index]
            if first_bigram in bigram_vocabulary:
              if second_bigram in bigram_vocabulary:
                first_frequency = float(bigram_vocabulary[first_bigram].split('\t')[0])
                second_frequency = float(bigram_vocabulary[second_bigram].split('\t')[0])
                if first_frequency < second_frequency:
                  bigrams_to_add.add(second_bigram)
                else:
                  bigrams_to_add.add(first_bigram)
              else:
                bigrams_to_add.add(first_bigram)
            elif second_bigram in bigram_vocabulary:
              bigrams_to_add.add(second_bigram)
        words.append(line.strip().split('\t')[0])
  return bigrams_to_add, top_unigrams


def create_similar_sets_vocabulary(bigrams, unigrams, vocabulary):
  similar_sets_vocabulary = {}
  for bigram in bigrams:
    if bigram in vocabulary:
      similar_sets_vocabulary[bigram] = 'B'
  for unigram in unigrams:
    if unigram in vocabulary:
      similar_sets_vocabulary[unigram] = 'N'
  return similar_sets_vocabulary


if __name__ == '__main__':
  parser = ArgumentParser(description='Bigrams adder to PLSA-ITER algorithm')
  parser.add_argument('unigram_input_file',
                      help='File containing unigram input for topic modeling')
  parser.add_argument('unigram_vocabulary_file',
                      help='File containing unigram vocabulary for topic modeling')
  parser.add_argument('bigram_input_file',
                      help='File containing bigram input for topic modeling')
  parser.add_argument('bigram_vocabulary_file',
                      help='File containing bigram vocabulary for topic modeling')
  parser.add_argument('topics_file', help='File containing topics')
  parser.add_argument('similar_sets_file',
                      help='File where sets of similar unigrams and bigrams will be written')
  parser.add_argument('result_input_file',
                      help='File where result input will be written')
  parser.add_argument('result_vocabulary_file',
                      help='File where result vocabulary will be written')
  parser.add_argument('-n', '--top_unigrams_number', type=int, default=10,
                      help='Number of unigrams to consider in each topic')
  parser.add_argument('-l', '--language', choices=['russian', 'english'], required=True)
  args = parser.parse_args()
  unigram_input_data, unigram_vocabulary = topic_loader.load_topic_input(args.unigram_input_file,
                                                                         args.unigram_vocabulary_file)
  bigram_input_data, bigram_vocabulary = topic_loader.load_topic_input(args.bigram_input_file,
                                                                       args.bigram_vocabulary_file)
  bigrams_to_add, top_unigrams = load_topics(args.topics_file, args.top_unigrams_number, bigram_vocabulary)
  result_input_data, result_vocabulary = add_bigrams(unigram_input_data, unigram_vocabulary,
                                                     bigram_input_data, bigram_vocabulary, bigrams_to_add)
  similar_sets_vocabulary = create_similar_sets_vocabulary(bigrams_to_add, top_unigrams, result_vocabulary)
  if args.language == 'russian':
    topic_loader.create_similar_sets(similar_sets_vocabulary, args.similar_sets_file, 'snowball')
  else:
    topic_loader.create_similar_sets(similar_sets_vocabulary, args.similar_sets_file, 'lancaster')
  topic_loader.print_topic_input(result_input_data, result_vocabulary,
                                 args.result_input_file, args.result_vocabulary_file)