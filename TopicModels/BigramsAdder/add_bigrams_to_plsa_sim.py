#!/usr/bin/env python
from argparse import ArgumentParser
from collections import defaultdict
import topic_loader

def load_top_bigrams(filename, bigrams_number):
  print('Loading top bigrams to add')
  bigrams = set()
  with open(filename, 'r') as file_input:
    for index, line in enumerate(file_input):
      if index >= bigrams_number:
        break
      tokens = line.strip().split('\t')
      bigrams.add(tokens[0])
  return bigrams


def add_bigrams(unigram_input_data, unigram_vocabulary, bigram_input_data,
                bigram_vocabulary, top_bigrams):
  print('Adding bigrams')
  assert(len(unigram_input_data) == len(bigram_input_data))
  result_input_data = unigram_input_data
  result_vocabulary = unigram_vocabulary
  for bigram in top_bigrams:
    result_vocabulary[bigram] = bigram_vocabulary[bigram]
  for index in xrange(len(unigram_input_data)):
    for bigram in top_bigrams.intersection(bigram_input_data[index]):
      result_input_data[index][bigram] = bigram_input_data[index][bigram]
      for unigram in [bigram.split()[0], bigram.split()[-1]]:
        result_input_data[index][unigram] -= result_input_data[index][bigram]
        result_input_data[index][unigram] = max(result_input_data[index][unigram], 0)
    document = {}
    for ngram, frequency in result_input_data[index].iteritems():
      if frequency > 0:
        document[ngram] = frequency
    result_input_data[index] = document
  item_frequencies = defaultdict(int)
  for document in result_input_data:
    for item, frequency in document.iteritems():
      item_frequencies[item] += frequency
  zero_items = set(result_vocabulary).difference(item_frequencies)
  for item in zero_items:
    result_vocabulary.pop(item)
  return result_input_data, result_vocabulary


if __name__ == '__main__':
  parser = ArgumentParser(description='Bigrams adder to PLSA-SIM algorithm')
  parser.add_argument('unigram_input_file',
                      help='File containing unigram input for topic modeling')
  parser.add_argument('unigram_vocabulary_file',
                      help='File containing unigram vocabulary for topic modeling')
  parser.add_argument('bigram_input_file',
                      help='File containing bigram input for topic modeling')
  parser.add_argument('bigram_vocabulary_file',
                      help='File containing bigram vocabulary for topic modeling')
  parser.add_argument('bigrams_file', help='File containing bigrams to add')
  parser.add_argument('result_input_file',
                      help='File where result input will be written')
  parser.add_argument('result_vocabulary_file',
                      help='File where result vocabulary will be written')
  parser.add_argument('-s', '--similar_sets_file', dest='similar_sets_file',
                      help='File where sets of similar unigrams and bigrams will be written')
  parser.add_argument('-n', '--bigrams_number', type=int, default=1000,
                      help='Number of bigrams to add')
  args = parser.parse_args()
  unigram_input_data, unigram_vocabulary = topic_loader.load_topic_input(args.unigram_input_file,
                                                                         args.unigram_vocabulary_file)
  bigram_input_data, bigram_vocabulary = topic_loader.load_topic_input(args.bigram_input_file,
                                                                       args.bigram_vocabulary_file)
  top_bigrams = load_top_bigrams(args.bigrams_file, args.bigrams_number)
  result_input_data, result_vocabulary = add_bigrams(unigram_input_data, unigram_vocabulary,
                                                     bigram_input_data, bigram_vocabulary, top_bigrams)
  if args.similar_sets_file is not None:
    topic_loader.create_similar_sets(result_vocabulary, args.similar_sets_file)
  topic_loader.print_topic_input(result_input_data, result_vocabulary,
                                 args.result_input_file, args.result_vocabulary_file)