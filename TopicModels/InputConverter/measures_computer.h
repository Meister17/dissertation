// Copyright 2014 Michael Nokel
#pragma once

#include <map>
#include <string>
#include <vector>
#include "./auxiliary.h"
#include "./file_processors/file_processor.h"

class MeasuresComputer {
 public:
  /**
    Creates and initializes object
    @param file_processor pointer to the object that is used to work with
    accumulated data from files
  */
  explicit MeasuresComputer(const FileProcessor* file_processor)
      : file_processor_(file_processor) {
  }

 private:
  /** Object used to work with accumulated data from files */
  const FileProcessor* file_processor_;

 public:
  /**
    Computes given word association measures and prints results in separate
    text files in the given directory
    @param measures vector containing word association measures to compute
    @param directory_name directory where results will be written
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeMeasures(const std::vector<WordAssociationMeasure>& measures,
                       const std::string& directory_name) const;

 private:
  /**
    Computes Term Frequency for each extracted bigram and prints them ordered by
    descending of values
    @param filename file where bigrams ordered by Term Frequency will be printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeTermFrequency(const std::string& filename) const;

  /**
    Computes Mutual Information for the given bigram
    @param bigram_frequency frequency of the bigram to compute MI for
    @param left_unigram_frequency frequency of the left unigram forming bigram
    @param right_unigram_frequency frequency of the right unigram forming bigram
    @return Mutual Information of the given bigram
  */
  double ComputeMIForBigram(unsigned int bigram_frequency,
                            unsigned int left_unigram_frequency,
                            unsigned int right_unigram_frequency) const;

  /**
    Computes Mutual Information for each extracted bigram and prints them
    ordered by descending of values
    @param filename file where bigrams ordered by Mutual Information will be
    printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeMutualInformation(const std::string& filename) const;

  /**
    Computes Augmeneted Mutual Information for each extracted bigram and prints
    them ordered by descending of values
    @param filename file where bigrams ordered by Augmented Mutual Information
    will be printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeAugmentedMutualInformation(const std::string& filename) const;

  /**
    Computes Normalized Mutual Information for each extracted bigram and prints
    them ordered by descending of values
    @param filename file where bigrams ordered by Normalized Mutual Information
    will be printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeNormalizedMutualInformation(const std::string& filename) const;

  /**
    Computes True Mutual Information for each extracted bigram and prints them
    ordered by descending of values
    @param filename file where bigrams ordered by True Mutual Information will
    be printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeTrueMutualInformation(const std::string& filename) const;

  /**
    Computes Cubic Mutual Information for each extracted bigram and prints them
    ordered by descending of values
    @param filename file where bigrams ordered by Cubic Mutual Information will
    be printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeCubicMutualInformation(const std::string& filename) const;

  /**
    Computes Symmetrical Conditional Probability for each extracted bigram and
    prints them ordered by descending of values
    @param filename file where bigrams ordered by Symmetrical Conditional
    Probability will be printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeSymmetricalConditionalProbability(const std::string& filename)
      const;

  /**
    Computes Gravity Count for each extracted bigram and prints them ordered by
    descending of values
    @param filename file where bigrams ordered by Gravity Count will be printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeGravityCount(const std::string& filename) const;

  /**
    Computes T-Score for each extracted bigram and prints them ordered by
    descending of values
    @param filename file where bigrams ordered by T-Score will be printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeTScore(const std::string& filename) const;

  /**
    Computes Dice Coefficient for each extracted bigram and prints them ordered
    by descending of values
    @param filename file where bigrams ordered by Dice Coefficient will be
    printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeDiceCoefficient(const std::string& filename) const;

  /**
    Computes Modified Dice Coefficient for each extracted bigram and prints them
    ordered by descending of values
    @param filename file where bigrams ordered by Modified Dice Coefficient will
    be printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeModifiedDiceCoefficient(const std::string& filename) const;

  /**
    Computes Simple Matching Coefficient for each extracted bigram and prints
    them ordered by descending of values
    @param filename file where bigrams ordered by Simple Matching Coefficient
    will be printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeSimpleMatchingCoefficient(const std::string& filename) const;

  /**
    Computes Kulczinsky Coefficient for each extracted bigram and prints them
    ordered by descending of values
    @param filename file where bigrams ordered by Loglikelihood Ratio will be
    printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeKulczinskyCoefficient(const std::string& filename) const;

  /**
    Computes Yule Coefficient for each extracted bigram and prints them ordered
    by descending of values
    @param filename file where bigrams ordered by Yule Coefficient will be
    printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeYuleCoefficient(const std::string& filename) const;

  /**
    Computes Jaccard Coefficient for each extracted bigram and prints them
    ordered by descending of values
    @param filename file where bigrams ordered by Jaccard Coefficient will be
    printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeJaccardCoefficient(const std::string& filename) const;

  /**
    Computes Chi-Square for each extracted bigram and prints them ordered by
    descending of values
    @param filename file where bigram ordered by Chi-Square will be printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeChiSquare(const std::string& filename) const;

  /**
    Computes LogLikelihood Ratio part for given bigram
    @param bigram_frequency frequency of the given bigram
    @param left_unigram_frequency frequency of the left unigram forming given
    bigram
    @param right_unigram_frequency frequency of the right unigram forming given
    bigram
    @return LogLikelihood Ratio part for given bigram
  */
  double ComputeLogLikelihoodRatioPart(unsigned int bigram_frequency,
                                       unsigned int left_unigram_frequency,
                                       unsigned int right_unigram_frequency)
                                       const;

  /**
    Computes LogLikelihood Ratio for each extracted bigram and prints them
    ordered by descending of values
    @param filename file where bigrams ordered by LogLikelihood Ratio will be
    printed
    @throw std::runtime_error in case of any occurred error
  */
  void ComputeLogLikelihoodRatio(const std::string& filename) const;

  /**
    Returns first and last unigram forming given bigram
    @param bigram bigram to get unigrams for
    @param[out] left_unigram first unigram forming given bigram
    @param[out] right_unigram last unigram forming given bigram
  */
  void GetUnigrams(const std::string& bigram,
                   std::string* left_unigram,
                   std::string* right_unigram) const;

  /**
    Returns frequencies of both unigrams that form the whole bigram
    @param bigram bigram to get unigram frequencies for
    @param[out] left_unigram_frequency frequency of the left unigram forming
    bigram
    @param[out] right_unigram_frequency frequency of the right unigram forming
    bigram
  */
  void GetUnigramFrequencies(const std::string& bigram,
                             unsigned int& left_unigram_frequency,
                             unsigned int& right_unigram_frequency) const;

  /**
    Returns pre and post frequencies of the unigrams forming given bigram
    @param bigram bigram to get pre- and post- frequencies for
    @param[out] left_post_frequency number of different words occurred to the
    right of the first unigram forming given bigram
    @param[out] right_pre_frequency number of different words occurred to the
    left of the last unigram forming given bigram
  */
  void GetPrePostFrequencies(const std::string& bigram,
                             unsigned int& left_post_frequency,
                             unsigned int& right_pre_frequency) const;
  /**
    Inserts computed weight for given bigram in the given storage
    @param bigram bigram to insert weight for
    @param weight computed weight to insert
    @param[out] weights storage containing all bigrams with their weights
  */
  void InsertWeight(const std::string& bigram,
                    double weight,
                    std::map<double, std::vector<std::string>>* weights) const;

  /**
    Prints bigrams in descending order by given weights
    @param filename file where bigrams ordered by descending order of weights
    will be printed
    @param[out] weights storage containing all bigrams with their weights (each
    item will be sorted)
    @throw std::runtime_error in case of any occurred error
  */
  void PrintBigrams(const std::string& filename,
                    std::map<double, std::vector<std::string>>* weights) const;
};
