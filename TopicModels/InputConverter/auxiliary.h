// Copyright 2014 Michael Nokel
#pragma once

#include <set>
#include <string>

/**
  @brief Enumerator containing all possible languages of the text corpus

  This enumerator currently contains Russian and English languages, while
  NO_LANGUAGE indicates an error
*/
enum class Language {
  NO_LANGUAGE,
  RUSSIAN,
  ENGLISH
};

/**
  @brief Enumerator containing all possible word association measures

  This enumerator currently contains Term Frequency, Mutual Information,
  Augmented Mutual Information, Normalized Mutual Information, True Mutual
  Information, Cubic Mutual Information, Symmetrical Conditional Probability,
  Lexical Cohesion, Gravity Count, T-Score, Dice Coefficient, Modified Dice
  Coefficient, Simple Matching Coefficient, Kulczinsky Coefficient, Yule
  Coefficient, Jaccard Coefficient, Chi-Square, Loglikelihood Ratio, while
  NO_MEASURE indicates an error
*/
enum class WordAssociationMeasure {
  NO_MEASURE,
  TERM_FREQUENCY,
  MUTUAL_INFORMATION,
  AUGMENTED_MUTUAL_INFORMATION,
  NORMALIZED_MUTUAL_INFORMATION,
  TRUE_MUTUAL_INFORMATION,
  CUBIC_MUTUAL_INFORMATION,
  SYMMETRICAL_CONDITIONAL_PROBABILITY,
  GRAVITY_COUNT,
  T_SCORE,
  DICE_COEFFICIENT,
  MODIFIED_DICE_COEFFICIENT,
  SIMPLE_MATCHING_COEFFICIENT,
  KULCZINSKY_COEFFICIENT,
  YULE_COEFFICIENT,
  JACCARD_COEFFICIENT,
  CHI_SQUARE,
  LOGLIKELIHOOD_RATIO
};

/**
  @brief Enumerator containing all possible parts of speech that we consider

  It contains Adjectives, Adverbs, Nouns, Verbs and Bigrams, while
  NO_PART_OF_SPEECH indicates an error
*/
enum class PartOfSpeech {
  NO_PART_OF_SPEECH,
  ADJECTIVE,
  ADVERB,
  NOUN,
  GENITIVE_NOUN,
  VERB,
  PREPOSITION,
  DETERMINER,
  BIGRAM
};

/**
  @brief Structure containing ngram's frequency and all its parts of speech

  This structure contains set of all possible parts of speech for the given
  ngram and its frequency
*/
struct NGramItem {
  NGramItem(unsigned int frequency, const std::set<PartOfSpeech> & pos)
      : frequency(frequency),
        parts_of_speech(pos) {
  }

  /** Frequency of the word */
  unsigned int frequency;

  /** Set containing all possible parts of speech for the word */
  std::set<PartOfSpeech> parts_of_speech;
};

/**
  @brief Structure containing all necessary information about extracted ngram

  This structure contains set of all possible parts of speech for the given
  ngram, its frequency and index in vocabulary
*/
struct NGramIndexItem : public NGramItem {
  NGramIndexItem(unsigned int index,
                 unsigned int frequency,
                 const std::set<PartOfSpeech>& pos)
      : NGramItem(frequency, pos),
        index(index) {
  }

  /** Index of the ngram in the vocabulary */
  unsigned int index;
};

/**
  @brief Structure containing number of different words occurred to the left or
  to the right of unigram (in extracted bigrams)
*/
struct PrePostOccurrences {
  /** Number of different words occurred to the left of the unigram */
  unsigned int pre_occurrences_number = 0;

  /** Number of different words occurred to the right of the unigram */
  unsigned int post_occurrences_number = 0;
};
