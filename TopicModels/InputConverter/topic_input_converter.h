// Copyright 2014 Michael Nokel
#pragma once

#include <memory>
#include <string>
#include <vector>
#include "./auxiliary.h"
#include "./measures_computer.h"
#include "./file_processors/file_processor.h"


/**
  @brief Class for creating input for topic modeling from the given directory

  This class should be used for processing input directory and forming input and
  vocabulary for topic modeling
*/
class TopicInputConverter {
 public:
  /**
    Initializes object
    @param language using language of the text corpus
    @param minimum_frequency minimum frequency of the ngram to be considered
  */
  TopicInputConverter(const Language& language,
                      unsigned int minimum_frequency);

 private:

  /** Minimum frequency of ngram to be considered */
  const unsigned int kMinimumFrequency_;

  /** Pointer to the object that processes files */
  std::unique_ptr<FileProcessor> file_processor_;

  /** Pointer to the Object used for computing measures */
  std::unique_ptr<MeasuresComputer> measures_computer_;

 public:
  /**
    Processes given directory and creates result input and vocabulary for topic
    modeling
    @param directory_name input directory that should be processed
    @throw std::runtime_error in case of any occurred error
  */
  void CreateTopicInput(const std::string& directory_name);

  /**
    Prints unigram and bigram result input and vocabulary for topic modeling to
    the given files
    @param unigram_topic_input_filename file where unigram result input for
    topic will be printed
    @param unigram_topic_vocabulary_filename file where unigram result
    vocabulary for topic modeling will be printed
    @param bigram_topic_input_filename file where bigram result LDA input will
    be printed
    @param bigram_topic_vocabulary_filename file where bigram result topic
    vocabulary will be printed
    @throw std::runtime_error in case of any occurred error
  */
  void PrintResults(const std::string& unigram_topic_input_filename,
                    const std::string& unigram_topic_vocabulary_filename,
                    const std::string& bigram_topic_input_filename,
                    const std::string& bigram_topic_vocabulary_filename) const;

  /**
    Calculates given word association measures and prints result to the text
    files inside given directory
    @param measures vector containing word association measures to compute
    @param directory_name name of directory for printing
    @throw std::runtime_error in case of any occurred error
  */
  void CalculateWordAssociationMeasures(
      const std::vector<WordAssociationMeasure>& measures,
      const std::string& directory_name) const;

 private:
  /**
    Scans source directory and processes all files found there in a thread pool
    @param directory_name name of directory for processing
  */
  void ScanSourceDirectory(const std::string& directory_name);
};
