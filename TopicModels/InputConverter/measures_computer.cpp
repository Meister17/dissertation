// Copyright 2014 Michael Nokel
#include <boost/algorithm/string.hpp>
#include <boost/threadpool.hpp>
#include <boost/thread.hpp>
#include <algorithm>
#include <cmath>
#include <fstream>
#include <map>
#include <stdexcept>
#include <string>
#include <vector>
#include "./auxiliary.h"
#include "./measures_computer.h"

using boost::is_any_of;
using boost::split;
using boost::threadpool::pool;
using std::map;
using std::max;
using std::ofstream;
using std::runtime_error;
using std::sort;
using std::string;
using std::vector;

void MeasuresComputer::ComputeMeasures(
    const vector<WordAssociationMeasure>& measures,
    const string& directory_name) const {
  pool thread_pool(boost::thread::hardware_concurrency());
  for (const auto& measure : measures) {
    switch (measure) {
      case WordAssociationMeasure::TERM_FREQUENCY:
        thread_pool.schedule(
            boost::bind(&MeasuresComputer::ComputeTermFrequency,
                        this,
                        directory_name + "term_frequency.txt"));
        break;
      case WordAssociationMeasure::MUTUAL_INFORMATION:
        thread_pool.schedule(
            boost::bind(&MeasuresComputer::ComputeMutualInformation,
                        this,
                        directory_name + "mutual_information.txt"));
        break;
      case WordAssociationMeasure::AUGMENTED_MUTUAL_INFORMATION:
        thread_pool.schedule(
            boost::bind(&MeasuresComputer::ComputeAugmentedMutualInformation,
                        this,
                        directory_name + "augmented_mutual_information.txt"));
        break;
      case WordAssociationMeasure::NORMALIZED_MUTUAL_INFORMATION:
        thread_pool.schedule(
            boost::bind(&MeasuresComputer::ComputeNormalizedMutualInformation,
                        this,
                        directory_name + "normalized_mutual_infromation.txt"));
        break;
      case WordAssociationMeasure::TRUE_MUTUAL_INFORMATION:
        thread_pool.schedule(
            boost::bind(&MeasuresComputer::ComputeTrueMutualInformation,
                        this,
                        directory_name + "true_mutual_information.txt"));
        break;
      case WordAssociationMeasure::CUBIC_MUTUAL_INFORMATION:
        thread_pool.schedule(
            boost::bind(&MeasuresComputer::ComputeCubicMutualInformation,
                        this,
                        directory_name + "cubic_mutual_information.txt"));
        break;
      case WordAssociationMeasure::SYMMETRICAL_CONDITIONAL_PROBABILITY:
        thread_pool.schedule(boost::bind(
            &MeasuresComputer::ComputeSymmetricalConditionalProbability,
            this,
            directory_name + "symmetrical_conditional_probability.txt"));
        break;
      case WordAssociationMeasure::GRAVITY_COUNT:
        thread_pool.schedule(boost::bind(&MeasuresComputer::ComputeGravityCount,
                                         this,
                                         directory_name + "gravity_count.txt"));
        break;
      case WordAssociationMeasure::T_SCORE:
        thread_pool.schedule(boost::bind(&MeasuresComputer::ComputeTScore,
                                         this,
                                         directory_name + "t_score.txt"));
        break;
      case WordAssociationMeasure::DICE_COEFFICIENT:
        thread_pool.schedule(
            boost::bind(&MeasuresComputer::ComputeDiceCoefficient,
                        this,
                        directory_name + "dice_coefficient.txt"));
        break;
      case WordAssociationMeasure::MODIFIED_DICE_COEFFICIENT:
        thread_pool.schedule(
            boost::bind(&MeasuresComputer::ComputeModifiedDiceCoefficient,
                        this,
                        directory_name + "modified_dice_coefficient.txt"));
        break;
      case WordAssociationMeasure::SIMPLE_MATCHING_COEFFICIENT:
        thread_pool.schedule(
            boost::bind(&MeasuresComputer::ComputeSimpleMatchingCoefficient,
                        this,
                        directory_name + "simple_matching_coefficient.txt"));
        break;
      case WordAssociationMeasure::KULCZINSKY_COEFFICIENT:
        thread_pool.schedule(
            boost::bind(&MeasuresComputer::ComputeKulczinskyCoefficient,
                        this,
                        directory_name + "kulczinsky_coefficient.txt"));
        break;
      case WordAssociationMeasure::YULE_COEFFICIENT:
        thread_pool.schedule(
            boost::bind(&MeasuresComputer::ComputeYuleCoefficient,
                        this,
                        directory_name + "yule_coefficient.txt"));
        break;
      case WordAssociationMeasure::JACCARD_COEFFICIENT:
        thread_pool.schedule(
            boost::bind(&MeasuresComputer::ComputeJaccardCoefficient,
                        this,
                        directory_name + "jaccard_coefficient.txt"));
        break;
      case WordAssociationMeasure::CHI_SQUARE:
        thread_pool.schedule(
            boost::bind(&MeasuresComputer::ComputeChiSquare,
                        this,
                        directory_name + "chi_square.txt"));
        break;
      case WordAssociationMeasure::LOGLIKELIHOOD_RATIO:
        thread_pool.schedule(
            boost::bind(&MeasuresComputer::ComputeLogLikelihoodRatio,
                        this,
                        directory_name + "loglikelihood_ratio.txt"));
        break;
      case WordAssociationMeasure::NO_MEASURE:
        break;
      default:
        throw runtime_error("Failed to compute word association measures");
    }
  }
}

void MeasuresComputer::ComputeTermFrequency(const string& filename) const {
  map<double, vector<string>> weights;
  for (auto iterator = file_processor_->GetStartBigramIterator(); iterator !=
       file_processor_->GetEndBigramIterator(); ++iterator) {
    InsertWeight(iterator->first, iterator->second.frequency, &weights);
  }
  PrintBigrams(filename, &weights);
}

double MeasuresComputer::ComputeMIForBigram(
    unsigned int bigram_frequency,
    unsigned int left_unigram_frequency,
    unsigned int right_unigram_frequency) const {
  return log(static_cast<double>(file_processor_->total_number_words()) *
      static_cast<double>(bigram_frequency) /
      (static_cast<double>(left_unigram_frequency) *
       static_cast<double>(right_unigram_frequency)));
}

void MeasuresComputer::ComputeMutualInformation(const string& filename) const {
  map<double, vector<string>> weights;
  for (auto iterator = file_processor_->GetStartBigramIterator(); iterator !=
       file_processor_->GetEndBigramIterator(); ++iterator) {
    unsigned int left_frequency, right_frequency;
    GetUnigramFrequencies(iterator->first, left_frequency, right_frequency);
    double weight = ComputeMIForBigram(iterator->second.frequency,
                                       left_frequency,
                                       right_frequency);
    InsertWeight(iterator->first, weight, &weights);
  }
  PrintBigrams(filename, &weights);
}

void MeasuresComputer::ComputeAugmentedMutualInformation(const string& filename)
    const {
  map<double, vector<string>> weights;
  for (auto iterator = file_processor_->GetStartBigramIterator(); iterator !=
       file_processor_->GetEndBigramIterator(); ++iterator) {
    unsigned int left_frequency, right_frequency;
    GetUnigramFrequencies(iterator->first, left_frequency, right_frequency);
    double left_multiplier = left_frequency - iterator->second.frequency;
    double right_multiplier = right_frequency - iterator->second.frequency;
    if (left_multiplier == 0) {
      left_multiplier = 1;
    }
    if (right_multiplier == 0) {
      right_multiplier = 1;
    }
    double weight = ComputeMIForBigram(iterator->second.frequency,
                                       left_multiplier,
                                       right_multiplier);
    InsertWeight(iterator->first, weight, &weights);
  }
  PrintBigrams(filename, &weights);
}

void MeasuresComputer::ComputeNormalizedMutualInformation(
    const string& filename) const {
  map<double, vector<string>> weights;
  for (auto iterator = file_processor_->GetStartBigramIterator(); iterator !=
       file_processor_->GetEndBigramIterator(); ++iterator) {
    unsigned int left_frequency, right_frequency;
    GetUnigramFrequencies(iterator->first, left_frequency, right_frequency);
    double weight = ComputeMIForBigram(iterator->second.frequency,
                                       left_frequency,
                                       right_frequency) /
        log(static_cast<double>(file_processor_->total_number_words()) /
            static_cast<double>(iterator->second.frequency));
    InsertWeight(iterator->first, weight, &weights);
  }
  PrintBigrams(filename, &weights);
}

void MeasuresComputer::ComputeCubicMutualInformation(
    const string& filename) const {
  map<double, vector<string>> weights;
  for (auto iterator = file_processor_->GetStartBigramIterator(); iterator !=
       file_processor_->GetEndBigramIterator(); ++iterator) {
    unsigned int left_frequency, right_frequency;
    GetUnigramFrequencies(iterator->first, left_frequency, right_frequency);
    double weight = ComputeMIForBigram(iterator->second.frequency,
                                       left_frequency,
                                       right_frequency) +
        log(static_cast<double>(iterator->second.frequency) /
            static_cast<double>(file_processor_->total_number_words())) +
        log(static_cast<double>(iterator->second.frequency) /
            static_cast<double>(file_processor_->total_number_words()));
    InsertWeight(iterator->first, weight, &weights);
  }
  PrintBigrams(filename, &weights);
}

void MeasuresComputer::ComputeTrueMutualInformation(const string& filename)
    const {
  map<double, vector<string>> weights;
  for (auto iterator = file_processor_->GetStartBigramIterator(); iterator !=
       file_processor_->GetEndBigramIterator(); ++iterator) {
    unsigned int left_frequency, right_frequency;
    GetUnigramFrequencies(iterator->first, left_frequency, right_frequency);
    double weight = iterator->second.frequency *
        ComputeMIForBigram(iterator->second.frequency,
                           left_frequency,
                           right_frequency);
    InsertWeight(iterator->first, weight, &weights);
  }
  PrintBigrams(filename, &weights);
}

void MeasuresComputer::ComputeSymmetricalConditionalProbability(
    const string& filename) const {
  map<double, vector<string>> weights;
  for (auto iterator = file_processor_->GetStartBigramIterator(); iterator !=
       file_processor_->GetEndBigramIterator(); ++iterator) {
    unsigned int left_frequency, right_frequency;
    GetUnigramFrequencies(iterator->first, left_frequency, right_frequency);
    double weight = static_cast<double>(iterator->second.frequency) *
        static_cast<double>(iterator->second.frequency) /
        (static_cast<double>(left_frequency) *
         static_cast<double>(right_frequency));
    InsertWeight(iterator->first, weight, &weights);
  }
  PrintBigrams(filename, &weights);
}

void MeasuresComputer::ComputeGravityCount(const string& filename) const {
  map<double, vector<string>> weights;
  for (auto iterator = file_processor_->GetStartBigramIterator(); iterator !=
       file_processor_->GetEndBigramIterator(); ++iterator) {
    unsigned int left_frequency, right_frequency;
    GetUnigramFrequencies(iterator->first, left_frequency, right_frequency);
    unsigned int left_post_frequency, right_pre_frequency;
    GetPrePostFrequencies(iterator->first,
                          left_post_frequency,
                          right_pre_frequency);
    double weight = log(static_cast<double>(iterator->second.frequency) *
                        max(static_cast<double>(left_post_frequency), 1.0) /
                        static_cast<double>(left_frequency)) +
        log(static_cast<double>(iterator->second.frequency) *
            max(static_cast<double>(right_pre_frequency), 1.0) /
            static_cast<double>(right_frequency));
    InsertWeight(iterator->first, weight, &weights);
  }
  PrintBigrams(filename, &weights);
}

void MeasuresComputer::ComputeTScore(const string& filename) const {
  map<double, vector<string>> weights;
  for (auto iterator = file_processor_->GetStartBigramIterator(); iterator !=
       file_processor_->GetEndBigramIterator(); ++iterator) {
    unsigned int left_frequency, right_frequency;
    GetUnigramFrequencies(iterator->first, left_frequency, right_frequency);
    double weight = (static_cast<double>(iterator->second.frequency) -
        static_cast<double>(left_frequency) *
        static_cast<double>(right_frequency) /
        static_cast<double>(file_processor_->total_number_words())) /
        sqrt(iterator->second.frequency);
    InsertWeight(iterator->first, weight, &weights);
  }
  PrintBigrams(filename, &weights);
}

void MeasuresComputer::ComputeDiceCoefficient(const string& filename) const {
  map<double, vector<string>> weights;
  for (auto iterator = file_processor_->GetStartBigramIterator(); iterator !=
       file_processor_->GetEndBigramIterator(); ++iterator) {
    unsigned int left_frequency, right_frequency;
    GetUnigramFrequencies(iterator->first, left_frequency, right_frequency);
    double weight = 2.0 * iterator->second.frequency /
        static_cast<double>(left_frequency + right_frequency);
    InsertWeight(iterator->first, weight, &weights);
  }
  PrintBigrams(filename, &weights);
}

void MeasuresComputer::ComputeModifiedDiceCoefficient(const string& filename)
    const {
  map<double, vector<string>> weights;
  for (auto iterator = file_processor_->GetStartBigramIterator(); iterator !=
       file_processor_->GetEndBigramIterator(); ++iterator) {
    unsigned int left_frequency, right_frequency;
    GetUnigramFrequencies(iterator->first, left_frequency, right_frequency);
    double weight = log(static_cast<double>(iterator->second.frequency)) *
        2 * static_cast<double>(iterator->second.frequency) /
        static_cast<double>(left_frequency + right_frequency);
    InsertWeight(iterator->first, weight, &weights);
  }
  PrintBigrams(filename, &weights);
}

void MeasuresComputer::ComputeSimpleMatchingCoefficient(const string& filename)
    const {
  map<double, vector<string>> weights;
  for (auto iterator = file_processor_->GetStartBigramIterator(); iterator !=
       file_processor_->GetEndBigramIterator(); ++iterator) {
    unsigned int left_frequency, right_frequency;
    GetUnigramFrequencies(iterator->first, left_frequency, right_frequency);
    double weight = (2.0 * static_cast<double>(iterator->second.frequency) +
        static_cast<double>(file_processor_->total_number_words() -
                            left_frequency - right_frequency)) /
        static_cast<double>(file_processor_->total_number_words());
    InsertWeight(iterator->first, weight, &weights);
  }
  PrintBigrams(filename, &weights);
}

void MeasuresComputer::ComputeKulczinskyCoefficient(const string& filename)
    const {
  map<double, vector<string>> weights;
  for (auto iterator = file_processor_->GetStartBigramIterator(); iterator !=
       file_processor_->GetEndBigramIterator(); ++iterator) {
    unsigned int left_frequency, right_frequency;
    GetUnigramFrequencies(iterator->first, left_frequency, right_frequency);
    double weight = 0.5 * static_cast<double>(iterator->second.frequency) *
        (1.0 / static_cast<double>(left_frequency) +
         1.0 / static_cast<double>(right_frequency));
    InsertWeight(iterator->first, weight, &weights);
  }
  PrintBigrams(filename, &weights);
}

void MeasuresComputer::ComputeYuleCoefficient(const string& filename) const {
  map<double, vector<string>> weights;
  for (auto iterator = file_processor_->GetStartBigramIterator(); iterator !=
       file_processor_->GetEndBigramIterator(); ++iterator) {
    unsigned int left_frequency, right_frequency;
    GetUnigramFrequencies(iterator->first, left_frequency, right_frequency);
    double weight = (static_cast<double>(iterator->second.frequency) *
        static_cast<double>(file_processor_->total_number_words() +
            iterator->second.frequency - left_frequency - right_frequency) -
        static_cast<double>(left_frequency - iterator->second.frequency) *
        static_cast<double>(right_frequency - iterator->second.frequency)) /
        (static_cast<double>(iterator->second.frequency) *
         static_cast<double>(file_processor_->total_number_words() +
            iterator->second.frequency - left_frequency - right_frequency) +
         static_cast<double>(left_frequency - iterator->second.frequency) *
         static_cast<double>(right_frequency - iterator->second.frequency));
    InsertWeight(iterator->first, weight, &weights);
  }
  PrintBigrams(filename, &weights);
}

void MeasuresComputer::ComputeJaccardCoefficient(const string& filename) const {
  map<double, vector<string>> weights;
  for (auto iterator = file_processor_->GetStartBigramIterator(); iterator !=
       file_processor_->GetEndBigramIterator(); ++iterator) {
    unsigned int left_frequency, right_frequency;
    GetUnigramFrequencies(iterator->first, left_frequency, right_frequency);
    double weight = static_cast<double>(iterator->second.frequency) /
        static_cast<double>(left_frequency + right_frequency -
                            iterator->second.frequency);
    InsertWeight(iterator->first, weight, &weights);
  }
  PrintBigrams(filename, &weights);
}

void MeasuresComputer::ComputeChiSquare(const string& filename) const {
  map<double, vector<string>> weights;
  for (auto iterator = file_processor_->GetStartBigramIterator(); iterator !=
       file_processor_->GetEndBigramIterator(); ++iterator) {
    unsigned int left_frequency, right_frequency;
    GetUnigramFrequencies(iterator->first, left_frequency, right_frequency);
    double weight = static_cast<double>(iterator->second.frequency) -
        static_cast<double>(left_frequency) *
        static_cast<double>(right_frequency) /
        static_cast<double>(file_processor_->total_number_words());
    weight *= weight / (static_cast<double>(left_frequency) *
                        static_cast<double>(right_frequency));
    InsertWeight(iterator->first, weight, &weights);
  }
  PrintBigrams(filename, &weights);
}

double MeasuresComputer::ComputeLogLikelihoodRatioPart(
    unsigned int bigram_frequency,
    unsigned int left_unigram_frequency,
    unsigned int right_unigram_frequency) const {
  return static_cast<double>(bigram_frequency) *
      log(static_cast<double>(file_processor_->total_number_words()) *
          static_cast<double>(bigram_frequency) /
          (static_cast<double>(left_unigram_frequency) *
           static_cast<double>(right_unigram_frequency)));
}

void MeasuresComputer::ComputeLogLikelihoodRatio(const string& filename) const {
  map<double, vector<string>> weights;
  for (auto iterator = file_processor_->GetStartBigramIterator(); iterator !=
       file_processor_->GetEndBigramIterator(); ++iterator) {
    unsigned int left_frequency, right_frequency;
    GetUnigramFrequencies(iterator->first, left_frequency, right_frequency);
    double weight = 2.0 * (
        ComputeLogLikelihoodRatioPart(iterator->second.frequency,
                                      left_frequency,
                                      right_frequency) +
        ComputeLogLikelihoodRatioPart(
            left_frequency - iterator->second.frequency,
            left_frequency,
            file_processor_->total_number_words() - right_frequency) +
        ComputeLogLikelihoodRatioPart(
            right_frequency - iterator->second.frequency,
            file_processor_->total_number_words() - left_frequency,
            right_frequency) +
        ComputeLogLikelihoodRatioPart(
            file_processor_->total_number_words() +
            iterator->second.frequency - left_frequency - right_frequency,
            file_processor_->total_number_words() - left_frequency,
            file_processor_->total_number_words() - right_frequency));
    InsertWeight(iterator->first, weight, &weights);
  }
  PrintBigrams(filename, &weights);
}

void MeasuresComputer::GetUnigrams(const string& bigram,
                                   string* left_unigram,
                                   string* right_unigram) const {
  vector<string> unigrams;
  split(unigrams, bigram, is_any_of(" "));
  if (unigrams.size() != 2 && unigrams.size() != 3) {
    throw runtime_error("Failed to compute word association measures");
  }
  *left_unigram = unigrams.front();
  *right_unigram = unigrams.back();
}

void MeasuresComputer::GetUnigramFrequencies(
    const string& bigram,
    unsigned int& left_unigram_frequency,
    unsigned int& right_unigram_frequency) const {
  string left_unigram, right_unigram;
  GetUnigrams(bigram, &left_unigram, &right_unigram);
  left_unigram_frequency = file_processor_->GetWordFrequency(left_unigram);
  right_unigram_frequency = file_processor_->GetWordFrequency(right_unigram);
}

void MeasuresComputer::GetPrePostFrequencies(const string& bigram,
                                             unsigned int& left_post_frequency,
                                             unsigned int& right_pre_frequency)
                                             const {
  string left_unigram, right_unigram;
  GetUnigrams(bigram, &left_unigram, &right_unigram);
  left_post_frequency = file_processor_->GetPostOccurrencesNumber(left_unigram);
  right_pre_frequency = file_processor_->GetPreOccurrencesNumber(right_unigram);
}

void MeasuresComputer::InsertWeight(const string& bigram,
                                    double weight,
                                    map<double, vector<string>>* weights)
                                    const {
  auto insert_result = weights->insert({weight, vector<string>()});
  insert_result.first->second.push_back(bigram);
}

void MeasuresComputer::PrintBigrams(const string& filename,
                                    map<double, vector<string>>* weights)
                                    const {
  ofstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to print bigrams");
  }
  for (auto iterator = weights->rbegin(); iterator != weights->rend();
       ++iterator) {
    sort(iterator->second.begin(), iterator->second.end());
    for (const auto& bigram : iterator->second) {
      file << bigram << "\t" << iterator->first << "\n";
    }
  }
  file.close();
}
