// Copyright 2014 Michael Nokel
#pragma once

#include <algorithm>
#include <map>
#include <memory>
#include <set>
#include <string>
#include <vector>
#include "../auxiliary.h"
#include "./ngrams_accumulators/bigrams_accumulator.h"
#include "./ngrams_accumulators/ngrams_accumulator.h"

/**
  @brief Abstract lass for processing files

  This class should be derived by each class implementing processing files of
  any particular language
*/
class FileProcessor {
 public:
  FileProcessor() {
  }

  /**
    Reserves space for processing files
    @param files_number number of files for processing
  */
  virtual void ReserveForFiles(unsigned int files_number) {
    unigrams_accumulator_.ReserveForFiles(files_number);
    bigrams_accumulator_.ReserveForFiles(files_number);
  }

  /**
    Processes given file and extracts all found here unigrams and bigrams with
    some statistical information about them
    @param file_index index of processing file
    @param filename file for processing and extracting unigrams and bigrams
  */
  virtual void ProcessFile(unsigned int file_index,
                           const std::string& filename) = 0;

  /**
    Filters low-frequent topic words and calculates pre and post occurrences
    numbers for unigrams
    @param minimum_frequency minimum frequency to consider words
  */
  virtual void FilterTopicVocabulary(unsigned int minimum_frequency) {
    std::set<std::string> filtered_ngrams;
    filtered_ngrams = unigrams_accumulator_.FilterTopicVocabulary(
        filtered_ngrams,
        minimum_frequency);
    filtered_ngrams = bigrams_accumulator_.FilterTopicVocabulary(
        filtered_ngrams,
        minimum_frequency);
    bigrams_accumulator_.FillPrePostOccurrences();
  }

  /**
    Returns indexes of empty files
    @return vector containing indexes of empty files
  */
  virtual std::vector<unsigned int> GetEmptyFiles() {
    std::vector<unsigned int> empty_unigrams =
        unigrams_accumulator_.GetEmptyFiles();
    std::vector<unsigned int> empty_bigrams =
        bigrams_accumulator_.GetEmptyFiles();
    std::vector<unsigned int> empty_files(
        std::min(empty_unigrams.size(), empty_bigrams.size()));
    empty_files.resize(std::set_intersection(empty_unigrams.begin(),
                                             empty_unigrams.end(),
                                             empty_bigrams.begin(),
                                             empty_bigrams.end(),
                                             empty_files.begin()) -
                       empty_files.begin());
    return empty_files;
  }

  /**
    Prints input for topic modeling for unigrams
    @param vocabulary_filename file for printing vocabulary
    @param input_filename file for printing input data
    @param empty_files vector containing indexes of empty files
    @throw std::runtime_error if printing failed
  */
  virtual void PrintUnigramInput(const std::string& vocabulary_filename,
                                 const std::string& input_filename,
                                 const std::vector<unsigned int>& empty_files)
                                 const {
    unigrams_accumulator_.PrintVocabulary(vocabulary_filename);
    unigrams_accumulator_.PrintInput(input_filename, empty_files);
  }

  /**
    Prints input for topic modeling for bigrams
    @param vocabulary_filename file for printing vocabulary
    @param input_filename file for printing input data
    @param empty_files vector containing indexes of empty files
    @throw std::runtime_error if printing failed
  */
  virtual void PrintBigramInput(const std::string& vocabulary_filename,
                                const std::string& input_filename,
                                const std::vector<unsigned int>& empty_files)
                                const {
    bigrams_accumulator_.PrintVocabulary(vocabulary_filename);
    bigrams_accumulator_.PrintInput(input_filename, empty_files);
  }

  /**
    Returns word's frequency
    @param word word to return frequency for
    @return word's frequency
  */
  virtual unsigned int GetWordFrequency(const std::string& word) const {
    return unigrams_accumulator_.GetWordFrequency(word);
  }

  /**
    Returns number of unique words following the given word
    @param word word to get number of unique words for
    @return number of unique words following the given word
  */
  virtual unsigned int GetPostOccurrencesNumber(const std::string& word) const {
    return bigrams_accumulator_.GetPostOccurrencesNumber(word);
  }

  /**
    Returns number of unique words preceding the given word
    @param word word to get number of unique words for
    @return number of unique words preceding the given word
  */
  virtual unsigned int GetPreOccurrencesNumber(const std::string& word) const {
    return bigrams_accumulator_.GetPreOccurrencesNumber(word);
  }

  /**
    Returns iterator to the start of the map containing extracted bigrams with
    their indexes and frequencies
    @return iterator to the start of the map containing extracted bigrams with
    their indexes and frequencies
  */
  virtual std::map<std::string, NGramIndexItem>::const_iterator
      GetStartBigramIterator() const {
    return bigrams_accumulator_.GetStartIterator();
  }

  /**
    Returns iterator to the end of the map containing extracted bigrams with
    their indexes and frequencies
    @return iterator to the end of the map containing extracted bigrams with
    their indexes and frequencies
  */
  virtual std::map<std::string, NGramIndexItem>::const_iterator
      GetEndBigramIterator() const {
    return bigrams_accumulator_.GetEndIterator();
  }

  virtual unsigned int total_number_words() const {
    return unigrams_accumulator_.total_number_ngrams();
  }

  /**
    Destructor for the purposes of proper deriving
  */
  virtual ~FileProcessor() {
  }

 protected:
  /** Unigram accumulator */
  NGramsAccumulator unigrams_accumulator_;

  /** Bigram accumulator */
  BigramsAccumulator bigrams_accumulator_;
};
