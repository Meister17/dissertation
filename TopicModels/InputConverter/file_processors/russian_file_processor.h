// Copyright 2014 Michael Nokel
#pragma once

#include <fstream>
#include <map>
#include <set>
#include <string>
#include "./file_processor.h"
#include "../auxiliary.h"

/**
  @brief Class for extracting Russian unigrams and bigrams from processing files

  This class should be used for extracting unigrams and bigrams from Russian
  files
*/
class RussianFileProcessor : public FileProcessor {
 public:
  /**
    Processes Russian file and extracts unirams and bigrams from it
    @param file_index index of the processing file
    @param filename file for processing and extracting unigrams and bigrams
    @throw std::runtime_error in case of any occurred error
  */
  void ProcessFile(unsigned int file_index, const std::string& filename);

 private:
  /**
    Extracts words that have same form from the given file
    @param[out] file pointer to file from where to extract words
    @return map containing extracted words with their parts of speech
    @throw std::runtime_error in case of any occurred error
  */
  std::map<std::string, std::set<PartOfSpeech>> ExtractSameWordForms(
      std::ifstream* file) const;

  /**
    Converts string from its CP-1251 encoding to UTF-8 encoding
    @param string_cp1251 string in CP-1251 encoding
    @return string in UTF-8 encoding
  */
  std::string ConvertFromCP1251ToUTF8(const std::string& string_cp1251) const;
};