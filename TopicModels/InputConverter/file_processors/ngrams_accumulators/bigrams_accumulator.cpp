// Copyright 2015 Michael Nokel
#include "./bigrams_accumulator.h"
#include <boost/algorithm/string.hpp>
#include <string>
#include <vector>

using boost::is_any_of;
using boost::split;
using std::string;
using std::vector;

unsigned int BigramsAccumulator::GetPreOccurrencesNumber(const string& word)
    const {
  auto iterator = pre_post_occurrences_.find(word);
  if (iterator == pre_post_occurrences_.end()) {
    return 0;
  }
  return iterator->second.pre_occurrences_number;
}

unsigned int BigramsAccumulator::GetPostOccurrencesNumber(const string& word)
    const {
  auto iterator = pre_post_occurrences_.find(word);
  if (iterator == pre_post_occurrences_.end()) {
    return 0;
  }
  return iterator->second.post_occurrences_number;
}

void BigramsAccumulator::FillPrePostOccurrences() {
  for (const auto& item : topic_vocabulary_) {
    vector<string> words;
    split(words, item.first, is_any_of(" "));
    if (words.size() ==  2 || words.size() == 3) {
      auto insert_result = pre_post_occurrences_.insert({words.front(),
                                                         PrePostOccurrences()});
      ++insert_result.first->second.post_occurrences_number;
      insert_result = pre_post_occurrences_.insert({words.back(),
                                                    PrePostOccurrences()});
      ++insert_result.first->second.pre_occurrences_number;
    }
  }
}