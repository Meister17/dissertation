// Copyright 2015 Michael Nokel
#include "./ngrams_accumulator.h"
#include <boost/algorithm/string.hpp>
#include <boost/thread/mutex.hpp>
#include <algorithm>
#include <fstream>
#include <map>
#include <set>
#include <stdexcept>
#include <string>
#include <vector>

using boost::is_any_of;
using boost::split;
using std::map;
using std::ofstream;
using std::runtime_error;
using std::set;
using std::sort;
using std::string;
using std::vector;

void NGramsAccumulator::AccumulateNGrams(
    unsigned int file_index,
    map<string, NGramItem>::const_iterator start_iterator,
    map<string, NGramItem>::const_iterator end_iterator) {
  boost::mutex::scoped_lock lock(mutex_);
  if (start_iterator != end_iterator) {
    for (auto iterator = start_iterator; iterator != end_iterator; ++iterator) {
      unsigned int item_index = topic_vocabulary_.size();
      NGramIndexItem item(item_index,
                          iterator->second.frequency,
                          iterator->second.parts_of_speech);
      auto inserted_result = topic_vocabulary_.insert({iterator->first, item});
      if (!inserted_result.second) {
        inserted_result.first->second.frequency += iterator->second.frequency;
        inserted_result.first->second.parts_of_speech.insert(
            iterator->second.parts_of_speech.cbegin(),
            iterator->second.parts_of_speech.cend());
        item_index = inserted_result.first->second.index;
      }
      topic_input_[file_index][item_index] += iterator->second.frequency;
      total_number_ngrams_ += iterator->second.frequency;
    }
  }
}

set<string> NGramsAccumulator::FilterTopicVocabulary(
    const set<string>& filtering_ngrams,
    unsigned int minimum_frequency) {
  set<string> filtered_ngrams;
  vector<string> vocabulary;
  for (auto iterator = topic_vocabulary_.begin(); iterator !=
       topic_vocabulary_.end(); ) {
    vector<string> unigrams;
    split(unigrams, iterator->first, is_any_of(" "));
    bool filter = false;
    for (const auto& unigram: unigrams) {
      if (filtering_ngrams.find(unigram) != filtering_ngrams.end()) {
        filter = true;
        break;
      }
    }
    if (!filter && iterator->second.parts_of_speech.size() < 5 &&
        iterator->second.frequency >= minimum_frequency) {
      vocabulary.push_back(iterator->first);
      ++iterator;
    } else {
      filtered_ngrams.insert(iterator->first);
      iterator = topic_vocabulary_.erase(iterator);
    }
  }
  sort(vocabulary.begin(), vocabulary.end());
  map<unsigned int, unsigned int> reindex;
  for (unsigned int index = 0; index < vocabulary.size(); ++index) {
    auto iterator = topic_vocabulary_.find(vocabulary[index]);
    reindex[iterator->second.index] = index;
    iterator->second.index = index;
  }
  for (auto& document : topic_input_) {
    map<unsigned int, unsigned int> new_document;
    for (const auto& element : document) {
      auto found_iterator = reindex.find(element.first);
      if (found_iterator != reindex.end()) {
        new_document[found_iterator->second] = element.second;
      }
    }
    document = new_document;
  }
  return filtered_ngrams;
}

void NGramsAccumulator::PrintVocabulary(const string& filename) const {
  ofstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to print topic vocabulary");
  }
  for (const auto& item : topic_vocabulary_) {
    file << item.first << "\t" << item.second.frequency;
    for (const auto& pos : item.second.parts_of_speech) {
      switch (pos) {
        case PartOfSpeech::ADJECTIVE:
          file << "\tA";
          break;
        case PartOfSpeech::NOUN: case PartOfSpeech::GENITIVE_NOUN:
          file << "\tN";
          break;
        case PartOfSpeech::ADVERB:
          file << "\tD";
          break;
        case PartOfSpeech::VERB:
          file << "\tV";
          break;
        case PartOfSpeech::BIGRAM:
          file << "\tB";
          break;
        case PartOfSpeech::NO_PART_OF_SPEECH: case PartOfSpeech::PREPOSITION:
        case PartOfSpeech::DETERMINER: default:
          throw runtime_error("Failed to print topic vocabulary");
      }
    }
    file << "\n";
  }
  file.close();
}

void NGramsAccumulator::PrintInput(const string& filename,
                                   const vector<unsigned int>& empty_files)
                                   const {
  ofstream file(filename.data());
  if (!file) {
    throw runtime_error("Failed to print topic input");
  }
  unsigned int file_index = 0;
  for (unsigned int index = 0; index < topic_input_.size(); ++index) {
    if (file_index < empty_files.size() && empty_files[file_index] == index) {
      ++file_index;
    } else {
      for (const auto& element : topic_input_[index]) {
        file << element.first << ":" << element.second << "\t";
      }
      file << "\n";
    }
  }
  file.close();
}