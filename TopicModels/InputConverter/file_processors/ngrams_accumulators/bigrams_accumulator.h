// Copyright 2015 Michael Nokel
#pragma once

#include "./ngrams_accumulator.h"
#include "../../auxiliary.h"
#include <map>
#include <string>

/**
  @brief Class for accumulating bigrams from parsed files and printing them

  This class should be used for accumulating bigrams and necessary data about
  them extracted from parsed files. After what one can print accumulated data
  in the desired files or use it for other purposes
*/
class BigramsAccumulator : public NGramsAccumulator {
 public:
  /**
    Returns number of different words occurred to the right of the given word
    @param word word to get post words number
    @return number of different words occurred to the right of the given word
  */

  virtual unsigned int GetPostOccurrencesNumber(const std::string& word) const;

  /**
    Returns number of different words occurred to the left of the given word
    @param word word to get pre words number
    @return number of different words occurred to the left of the given word
  */
  virtual unsigned int GetPreOccurrencesNumber(const std::string& word) const;

  /**
    Fill pre and post occurrences for unigrams
  */
  virtual void FillPrePostOccurrences();

  /**
    Returns start iterator to the map containing extracted bigrams with their
    frequencies and indexes
    @return start iterator to the map containing extracted bigrams with their
    frequencies and indexes
  */
  virtual std::map<std::string, NGramIndexItem>::const_iterator
      GetStartIterator() const {
    return topic_vocabulary_.cbegin();
  }

  /**
    Returns end iterator to the map containing extracted bigrams with their
    frequencies and indexes
    @return end iterator to the map containing extracted bigrams with their
    frequencies and indexes
  */
  virtual std::map<std::string, NGramIndexItem>::const_iterator GetEndIterator()
      const {
    return topic_vocabulary_.cend();
  }

 private:
  /** Map containing all extracted words with pre and post occurrences number
      in extracted bigrams */
  std::map<std::string, PrePostOccurrences> pre_post_occurrences_;
};