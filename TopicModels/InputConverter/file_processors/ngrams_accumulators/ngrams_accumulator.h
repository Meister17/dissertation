// Copyright Michael Nokel 2015
#pragma once

#include "../../auxiliary.h"
#include <boost/thread/mutex.hpp>
#include <map>
#include <set>
#include <string>
#include <vector>

/**
  @brief Class for accumulating information about extracted ngrams

  This class should be used for accumulating information about ngrams extracted
  from various files. After what one can print all accumulated information
*/
class NGramsAccumulator {
 public:
  /**
    Reserves space for the files to process
    @param files_number number of files that will be processed
  */
  virtual void ReserveForFiles(unsigned int files_number) {
    topic_input_.resize(files_number, std::map<unsigned int, unsigned int>());
  }

  /**
    Accumulates extracted ngrams from the parsed file
    @param file_index index of the parsed file
    @param start_iterator start iterator to the map containing extracted ngrams
    and necessary data about them
    @param end_iterator end iterator to the map containing extracted ngrams and
    necessary data about them
  */
  virtual void AccumulateNGrams(
      unsigned int file_index,
      std::map<std::string, NGramItem>::const_iterator start_iterator,
      std::map<std::string, NGramItem>::const_iterator end_iterator);

  /**
    Filters vocabulary created for topic modeling by removing low frequent items
    @param filtering_words hash set containing ngrams to filter
    @param minimum_frequency minimum frequency to remain item in vocabulary
    @return hash set containing filtered ngrams
  */
  virtual std::set<std::string> FilterTopicVocabulary(
      const std::set<std::string>& filtered_ngrams,
      unsigned int minimum_frequency);

  /**
    Returns indexes of empty files
    @return indexes of empty files
  */
  virtual std::vector<unsigned int> GetEmptyFiles() const {
    std::vector<unsigned int> empty_files;
    for (unsigned int index = 0; index < topic_input_.size(); ++index) {
      if (topic_input_[index].empty()) {
        empty_files.push_back(index);
      }
    }
    return empty_files;
  }

  /**
    Returns frequency to the given word
    @param word word to get frequency of
    @return frequency of the given word
  */
  virtual unsigned int GetWordFrequency(const std::string& word) const {
    auto iterator = topic_vocabulary_.find(word);
    if (iterator == topic_vocabulary_.end()) {
      return 0;
    }
    return iterator->second.frequency;
  }

  /***
    Prints vocabulary containing ngrams for topic modeling
    @param filename file for printing vocabulary
    @throw std::runtime_error in case of any occurred error
  */
  virtual void PrintVocabulary(const std::string& filename) const;

  /**
    Prints input data for topic modeling
    @param filename file for printing input data
    @param empty_files vector containing indexes of empty files
    @throw std::runtime_error in case of any occurred error
  */
  virtual void PrintInput(const std::string& filename,
                          const std::vector<unsigned int>& empty_files) const;

  virtual unsigned int total_number_ngrams() const {
    return total_number_ngrams_;
  }

 protected:
  /** Mutex for synchronization */
  boost::mutex mutex_;

  /** Total number of extracted ngrams */
  unsigned int total_number_ngrams_ = 0;

  /** Map containing all extracted ngrams with indexes and frequencies in
      vocabulary */
  std::map<std::string, NGramIndexItem> topic_vocabulary_;

  /** Vector containing topic input */
  std::vector<std::map<unsigned int, unsigned int>> topic_input_;
};