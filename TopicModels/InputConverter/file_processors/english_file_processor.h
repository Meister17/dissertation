// Copyright 2014 Michael Nokel
#pragma once

#include <string>
#include "./file_processor.h"

/**
  @brief Class for extracting English unigrams and bigrams during parsing files

  This class should be used for extracting unigrams and bigrams from English
  files
*/
class EnglishFileProcessor : public FileProcessor {
 public:
  /**
    Processes file and extracts unigrams and bigrams from it
    @param file_index index of the processing file
    @param filename file for processing and extracting unigrams and bigrams
    @throw std::runtime_error in case of any occurred error
  */
  void ProcessFile(unsigned int file_index, const std::string& filename);
};