// Copyright 2015 Michael Nokel
#pragma once

#include <map>
#include <set>
#include <string>
#include "../../auxiliary.h"

/**
  @brief Abstract class for extracting and printing ngrams

  This class should be used for extracting ngrams and printing resulted
  vocabulary and input data for topic modeling
*/
class NGramsExtractor {
 public:
  /**
    Returns start iterator to the map containing extracted ngrams and their
    frequencies
    @return start iterator to the map containing extracted ngrams and their
    frequencies
  */
  virtual std::map<std::string, NGramItem>::const_iterator
      GetStartIterator() const {
    return ngrams_.cbegin();
  }

  /**
    Returns end iterator to the map containing extracted ngrams and their
    frequencies
    @return end iterator to the map containing extracted ngrams and their
    frequencies
  */
  virtual std::map<std::string, NGramItem>::const_iterator GetEndIterator()
      const {
    return ngrams_.cend();
  }

  /**
    Destructor for the purposes of proper deriving
  */
  virtual ~NGramsExtractor() {
  }

 protected:
  /** Map containing extracted ngrams and necessary data about them */
  std::map<std::string, NGramItem> ngrams_;

 protected:
  /**
    Inserts new extracted ngram with its parts of speech
    @param ngram new extracted ngram to add
    @param pos ngram's parts of speech to add
  */
  virtual void InsertNewNGram(const std::string& ngram,
                              const std::set<PartOfSpeech>& pos) {
    NGramItem item(1, pos);
    auto result = ngrams_.insert({ngram, item});
    if (!result.second) {
      ++result.first->second.frequency;
      result.first->second.parts_of_speech.insert(pos.begin(), pos.end());
    }
  }
};