// Copyright 2014 Michael Nokel
#pragma once

#include <set>
#include <string>
#include "../ngrams_extractor.h"
#include "../../../auxiliary.h"

/**
  @brief Class that should be used for extracting English unigrams from the
  text corpus

  This class should be used for forming statistics about extracted unigrams
  during parsing English files
*/
class EnglishUnigramsExtractor : public NGramsExtractor {
 public:
  /**
    Adds newly extracted word to hash map containing all extracted unigrams
    @param word newly extracted word to add
    @param part_of_speech part of speech of the newly extracted word
  */
  void AddWord(const std::string& word, const PartOfSpeech& part_of_speech) {
    if (part_of_speech == PartOfSpeech::NOUN ||
        part_of_speech == PartOfSpeech::GENITIVE_NOUN ||
        part_of_speech == PartOfSpeech::ADJECTIVE ||
        part_of_speech == PartOfSpeech::ADVERB ||
        part_of_speech == PartOfSpeech::VERB) {
      std::set<PartOfSpeech> pos;
      if (part_of_speech == PartOfSpeech::GENITIVE_NOUN) {
        pos.insert(PartOfSpeech::NOUN);
      } else {
        pos.insert(part_of_speech);
      }
      InsertNewNGram(word, pos);
    }
  }
};