// Copyright 2014 Michael Nokel
#pragma once

#include <string>
#include "../ngrams_extractor.h"
#include "../../../auxiliary.h"

/**
  @brief Class that should be used for extracting English bigrams from the
  text corpus

  This class should be used for forming statistics about extracted bigrams
  during parsing English files
*/
class EnglishBigramsExtractor : public NGramsExtractor {
 public:
  /**
    Adds newly extracted word to hash map containing all extracted bigrams
    @param word newly extracted word to add
    @param part_of_speech part of speech of the newly extracted word
  */
  void AddWord(const std::string& word, const PartOfSpeech& part_of_speech);

 private:
  /** Set containing bigram's part of speech */
  const std::set<PartOfSpeech> bigram_part_of_speech_ = {PartOfSpeech::BIGRAM};

  /** Previous extracted word */
  std::string previous_word_;

  /** Part of speech of the previous word */
  PartOfSpeech previous_part_of_speech_;
};
