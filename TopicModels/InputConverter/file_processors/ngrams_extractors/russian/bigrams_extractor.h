// Copyright 2014 Michael Nokel
#pragma once

#include <map>
#include <set>
#include <string>
#include "../ngrams_extractor.h"
#include "../../../auxiliary.h"

/**
  @brief Class that should be used for extracting Russian bigrams from the text
  corpus

  This class should be used to form statistics for all Russian bigrams extracted
  during parsing files
*/
class RussianBigramsExtractor : public NGramsExtractor {
 public:
  /**
    Adds newly extracted words that have same form to the hash map containing
    all extracted bigrams
    @param words map containing newly extracted words and their parts of speech
    to add
  */
  void AddWords(const std::map<std::string, std::set<PartOfSpeech>>& words);

 private:
  /** Set containing bigram's part of speech */
  const std::set<PartOfSpeech> bigram_part_of_speech_ = {PartOfSpeech::BIGRAM};

  /** Map containing previously extracted words and their parts of speech */
  std::map<std::string, std::set<PartOfSpeech>> previous_words_;
};