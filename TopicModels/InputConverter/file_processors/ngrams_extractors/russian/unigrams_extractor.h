// Copyright 2014 Michael Nokel
#pragma once

#include <map>
#include <set>
#include <string>
#include "../ngrams_extractor.h"
#include "../../../auxiliary.h"

/**
  @brief Class that should be used for extracting Russian unigrams from the text
  corpus

  This class should be used for forming statistics about extracted Russian
  unigrams during parsing files
*/
class RussianUnigramsExtractor : public NGramsExtractor {
 public:
  /**
    Adds given words that have same form to hash map containing all extracted
    unigrams
    @param words map containing newly extracted words and their parts of speech
    to add
  */
  void AddWords(const std::map<std::string, std::set<PartOfSpeech>>& words) {
    std::set<std::string> words_in_bigrams;
    for (const auto& element : words) {
      if (!element.second.empty() && (element.second.size() > 1 ||
          element.second.find(PartOfSpeech::NO_PART_OF_SPEECH) ==
          element.second.end())) {
        std::set<PartOfSpeech> pos;
        for (const auto& part_of_speech : element.second) {
          if (part_of_speech == PartOfSpeech::GENITIVE_NOUN) {
            pos.insert(PartOfSpeech::NOUN);
          } else {
            pos.insert(part_of_speech);
          }
        }
        InsertNewNGram(element.first, pos);
      }
    }
  }
};