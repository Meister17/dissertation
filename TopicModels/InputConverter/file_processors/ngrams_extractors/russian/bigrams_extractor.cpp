// Copyright 2014 Michael Nokel
#include <map>
#include <set>
#include <string>
#include "./bigrams_extractor.h"
#include "../../../auxiliary.h"

using std::map;
using std::set;
using std::string;

void RussianBigramsExtractor::AddWords(
    const map<string, set<PartOfSpeech>>& words) {
  for (const auto& new_element : words) {
    bool is_genitive = new_element.second.find(PartOfSpeech::GENITIVE_NOUN) !=
        new_element.second.end();
    if (is_genitive || new_element.second.find(PartOfSpeech::NOUN) !=
        new_element.second.end()) {
      for (const auto& previous_element : previous_words_) {
        if (is_genitive ||
            previous_element.second.find(PartOfSpeech::ADJECTIVE) !=
            previous_element.second.end()) {
          InsertNewNGram(previous_element.first + " " + new_element.first,
                         bigram_part_of_speech_);
        }
      }
    }
  }
  previous_words_.clear();
  for (const auto& element : words) {
    set<PartOfSpeech> pos;
    for (const auto& part_of_speech : element.second) {
      if (part_of_speech == PartOfSpeech::GENITIVE_NOUN) {
        pos.insert(PartOfSpeech::NOUN);
      } else if (part_of_speech == PartOfSpeech::NOUN ||
                 part_of_speech == PartOfSpeech::ADJECTIVE) {
        pos.insert(part_of_speech);
      }
    }
    if (!pos.empty()) {
      previous_words_[element.first] = pos;
    }
  }
}
