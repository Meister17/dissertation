// Copyright 2014 Michael Nokel
#include <boost/filesystem.hpp>
#include <boost/threadpool.hpp>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>
#include "./topic_input_converter.h"
#include "./file_processors/english_file_processor.h"
#include "./file_processors/russian_file_processor.h"

using boost::filesystem::directory_iterator;
using boost::filesystem::is_regular_file;
using boost::threadpool::pool;
using std::cout;
using std::runtime_error;
using std::string;
using std::vector;

TopicInputConverter::TopicInputConverter(const Language& language,
                                         unsigned int minimum_frequency)
    : kMinimumFrequency_(minimum_frequency) {
  switch (language) {
    case Language::RUSSIAN:
      file_processor_.reset(new RussianFileProcessor());
      break;
    case Language::ENGLISH:
      file_processor_.reset(new EnglishFileProcessor());
      break;
    case Language::NO_LANGUAGE: default:
      break;
  }
  measures_computer_.reset(new MeasuresComputer(file_processor_.get()));
}

void TopicInputConverter::CreateTopicInput(const string& directory_name) {
  ScanSourceDirectory(directory_name);
  cout << "Filtering topic vocabulary\n";
  file_processor_->FilterTopicVocabulary(kMinimumFrequency_);
}

void TopicInputConverter::PrintResults(
    const string& unigram_topic_input_filename,
    const string& unigram_topic_vocabulary_filename,
    const string& bigram_topic_input_filename,
    const string& bigram_topic_vocabulary_filename) const {
  cout << "Printing results\n";
  vector<unsigned int> empty_files = file_processor_->GetEmptyFiles();
  pool thread_pool(boost::thread::hardware_concurrency());
  thread_pool.schedule(boost::bind(&FileProcessor::PrintUnigramInput,
                                   file_processor_.get(),
                                   unigram_topic_vocabulary_filename,
                                   unigram_topic_input_filename,
                                   empty_files));
  thread_pool.schedule(boost::bind(&FileProcessor::PrintBigramInput,
                                   file_processor_.get(),
                                   bigram_topic_vocabulary_filename,
                                   bigram_topic_input_filename,
                                   empty_files));
}

void TopicInputConverter::CalculateWordAssociationMeasures(
    const vector<WordAssociationMeasure>& measures,
    const string& directory_name) const {
  cout << "Calculating word association measures\n";
  measures_computer_->ComputeMeasures(measures, directory_name);
}

void TopicInputConverter::ScanSourceDirectory(const string& directory_name) {
  cout << "Scanning input directory\n";
  unsigned int files_number = 0;
  for (directory_iterator iterator(directory_name); iterator !=
       directory_iterator(); ++iterator) {
    if (is_regular_file(iterator->status())) {
      ++files_number;
    }
  }
  file_processor_->ReserveForFiles(files_number);
  files_number = 0;
  pool thread_pool(boost::thread::hardware_concurrency());
  for (directory_iterator iterator(directory_name); iterator !=
       directory_iterator(); ++iterator) {
    if (is_regular_file(iterator->status())) {
      thread_pool.schedule(boost::bind(&FileProcessor::ProcessFile,
                                       file_processor_.get(),
                                       files_number,
                                       iterator->path().string()));
      ++files_number;
    }
  }
}