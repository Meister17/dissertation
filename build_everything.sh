#!/usr/bin/env bash
pushd . > /dev/null
echo 'Building FeaturesCalculator module'
cd TermsExtractor/FeaturesCalculator
scons
popd > /dev/null

pushd . > /dev/null
echo 'Building TermsExtractor module'
cd TermsExtractor/TermsExtractor
scons
popd > /dev/null

pushd . > /dev/null
echo 'Building InputConverter module'
cd TopicModels/InputConverter
scons
popd > /dev/null

pushd . > /dev/null
echo 'Building InvertedIndex module'
cd TopicModels/InvertedIndex/IndexCreator
scons
popd > /dev/null

pushd . > /dev/null
echo 'Building TopicModels module'
cd TopicModels/TopicModels
scons
popd > /dev/null