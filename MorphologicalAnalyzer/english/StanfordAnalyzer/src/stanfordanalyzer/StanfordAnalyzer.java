/*
 * StanfordAnalyzer.java
 *
 * Created on January 14, 2013, 00:38 AM
 *
 *
 * Copyright (c) 2013 Michael Nokel
 */
package stanfordanalyzer;

import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import java.io.File;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

/**
 * @brief Class for performing morphological analysis using StanfordCoreNLP
 * library
 *
 * This class should be used for scanning given source directory, processing
 * each found there file and storing results in the destination directory
 */
public class StanfordAnalyzer {
  public StanfordAnalyzer() {
  }

  /**
   * Analyzes given source directory and places processed files in the
   * destination directory
   * @param source_directory_name name of source directory for scanning
   * @param destination_directory_name name of destination directory for
   * placing processed files
   * @throws Exception if analyzing directory has failed
   */
  public void AnalyzeDirectory(String source_directory_name,
                               String destination_directory_name)
                               throws Exception {
    // creates a StanfordCoreNLP object, with POS tagging and lemmatization
    Properties properties = new Properties();
    properties.put("annotators", "tokenize, ssplit, pos, lemma");
    StanfordCoreNLP pipeline = new StanfordCoreNLP(properties);
    // get list of all files in the directory
    File directory = new File(source_directory_name);
    File[] files = directory.listFiles();
    // process each file in the source directory separately
    ExecutorService executor = Executors.newFixedThreadPool(
      Runtime.getRuntime().availableProcessors());
    for (File file: files) {
      executor.submit(new AnalyzerThread(file,
                                         destination_directory_name,
                                         pipeline));
    }
    executor.shutdown();
  }
}
