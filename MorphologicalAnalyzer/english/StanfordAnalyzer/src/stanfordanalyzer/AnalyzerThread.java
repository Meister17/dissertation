/*
 * AnalyzerThread.java
 *
 * Created on June 06, 2014, 17:05 PM
 *
 *
 * Copyright (c) 2014 Michael Nokel
 */

package stanfordanalyzer;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import java.io.FileNotFoundException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.List;

/**
 * @brief Class for performing morphological analysis using StanfordCoreNLP
 * library on the given file
 * 
 * This class should be used to perform morphological analysis of the given
 * file in the separate thread in the thread pool
 */
public class AnalyzerThread implements Runnable {
  /**
   * Creates object
   * @param file file for processing
   * @param directory_name name of directory for printing results of processing
   * @param pipeline StanfordCoreNLP pipeline for processing files
   */
  public AnalyzerThread(File file,
                        String directory_name,
                        StanfordCoreNLP pipeline) {
    this.kFile_ = file;
    this.kDirectoryName_ = directory_name;
    this.kPipeline_ = pipeline;
  }
 
  /**
   * Main function that starts the thread
   */
  @Override
  public void run() {
    // list files in source directory
    StringBuilder text = new StringBuilder();
    try (BufferedReader reader = new BufferedReader(new FileReader(kFile_))) {
      String line;
      while ((line = reader.readLine()) != null) {
        text.append(line).append("\n");
      }
    } catch (FileNotFoundException exception) {
      System.out.println(exception.getMessage());
      return;
    } catch (IOException exception) {
      System.out.println(exception.getMessage());
      return;
    }
    // annotate read text
    Annotation document = new Annotation(text.toString());
    kPipeline_.annotate(document);
    List<CoreMap> sentences = document.get(
        CoreAnnotations.SentencesAnnotation.class);
    try (BufferedWriter writer = new BufferedWriter(new FileWriter(
         new File(kDirectoryName_, kFile_.getName())))) {
      // write all processed sentences to the output file
      for (CoreMap sentence: sentences) {
        // traverse the tokens in the processed sentence
        for (CoreLabel token: sentence.get(
             CoreAnnotations.TokensAnnotation.class)) {
          // get word, lemma and POS
          writer.write(token.get(CoreAnnotations.TextAnnotation.class));
          writer.write("\t");
          writer.write(token.get(CoreAnnotations.LemmaAnnotation.class));
          writer.write("\t");
          writer.write(
              token.get(CoreAnnotations.PartOfSpeechAnnotation.class));
          writer.write("\n");
        }
      }
    } catch (IOException exception) {
      System.out.println(exception.getMessage());
    }
  }
 
  /** File for processing */
  private final File kFile_;
  
  /** Name of directory for printing results */
  private final String kDirectoryName_;
 
  /** Pipeline for performing morphological analysis */
  private final StanfordCoreNLP kPipeline_;
}
