/*
 * Main.java
 * 
 * Created on January 13, 2013, 11:30 PM
 * 
 * 
 * Copyright (c) 2013 Michael Nokel
 */
package stanfordanalyzer;

import com.martiansoftware.jsap.JSAPException;

/**
 * Main class for starting the program
 * 
 * This class contains only one function which is the main function of the
 * whole program
 */
public class Main {
  /**
   * Main function to start the program
   * @param args command line arguments of program
   */
  public static void main(String[] args) {
    try {
      // parse program options
      ProgramOptionsParser program_options_parser = new ProgramOptionsParser();
      program_options_parser.Parse(args);
      if (program_options_parser.help_message_printed()) {
        return;
      }
      // check program options for correctness
      program_options_parser.CheckProgramOptions();
      // perform morphological analysis of source directory
      StanfordAnalyzer stanford_analyzer = new StanfordAnalyzer();
      stanford_analyzer.AnalyzeDirectory(
          program_options_parser.source_directory_name(),
          program_options_parser.destination_directory_name());
    } catch (JSAPException exception) {
      System.err.println("Wrong program options: " + exception.getMessage());
    } catch (Exception exception) {
      System.err.println("Fatal error occurred: " + exception.getMessage());
      exception.printStackTrace();
    }
  }
}
