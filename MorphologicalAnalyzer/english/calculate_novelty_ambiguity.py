#!/usr/bin/env python
from argparse import ArgumentParser
import en
from enchant import Dict
import multiprocessing as mp
from nltk.stem.wordnet import WordNetLemmatizer
import os


def process_file(input_filename, output_filename, compute_novelty):
    lemmatizer = WordNetLemmatizer()
    dictionary = Dict('en-US')
    with open(output_filename, 'w') as file_output:
        with open(input_filename, 'r') as file_input:
            for line in file_input:
                line = line.strip().split('\t')
                if len(line) != 3:
                    continue
                try:
                    lemma = lemmatizer.lemmatize(line[1].lower())
                except UnicodeDecodeError:
                    lemma = line[1].lower()
                if (line[2] == 'NN' or line[2] == 'NNS' or line[2] == 'JJ' or
                    line[2] == 'NNP' or line[2] == 'NNPS'):
                    word = line[0].lower()
                    if en.is_adverb(word) or en.is_number(word):
                        line[2] = 'RB'
                if compute_novelty:
                    novelty = '1' if dictionary.check(lemma) else '0'
                    ambiguity = ((1 if en.is_noun(lemma) else 0) +
                        (1 if en.is_adjective(lemma) else 0) +
                        (1 if en.is_adverb(lemma) else 0) +
                        (1 if en.is_number(lemma) else 0) +
                        (1 if en.is_verb(lemma) else 0))
                    ambiguity = '1' if ambiguity > 1 else '0'
                    file_output.write(line[0] + '\t' + lemma + '\t' + line[2] +
                                      '\t' + novelty + '\t' + ambiguity + '\n')
                else:
                    file_output.write(line[0] + '\t' + lemma + '\t' + line[2] + '\n')


def calculate_novelty_ambiguity(input_directory, output_directory, compute_novelty):
    pool = mp.Pool(processes=mp.cpu_count())
    for filename in os.listdir(input_directory):
        pool.apply_async(process_file,
                         (os.path.join(input_directory, filename),
                          os.path.join(output_directory, filename),
                          compute_novelty))
    pool.close()
    pool.join()


if __name__ == '__main__':
    parser = ArgumentParser(description='Novelty and Ambiguity calculator')
    parser.add_argument('input_directory',
                        help='Source directory for processing')
    parser.add_argument('output_directory',
                        help='Destination directory for printing results')
    parser.add_argument('-n', '--novelty', action='store_true',
                        help='Compute novelty and ambiguity or not')
    args = parser.parse_args()
    calculate_novelty_ambiguity(args.input_directory, args.output_directory, args.novelty)