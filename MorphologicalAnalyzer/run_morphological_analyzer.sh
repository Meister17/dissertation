#!/usr/bin/env bash
set -x -e

if [ "$#" -ne 3 ] && [ "$#" -ne 4 ]; then
 	echo 'Usage: $0 <input_directory> <output_directory> <language> [ wiki ]'
  exit 1
fi

if [ ! -d $1 ]; then
  	echo 'Wrong input directory'
  	exit 1
fi

if [ ! -d $2 ]; then
	mkdir -p $2
fi

if [ $3 != 'russian' ] && [ $3 != 'english' ]; then
  echo 'Wrong language'
  exit 1
fi

if [ "$#" -eq 4 ] && [ $4 != 'wiki' ]; then
  echo 'For parsing Wikipedia specify wiki'
  exit 1
fi

if [ $3 == 'russian' ]; then
  morphological_analyzer_dir=`realpath $0`
  morphological_analyzer_dir=`dirname $morphological_analyzer_dir`/$3
  mkdir -p $morphological_analyzer_dir/tmp
  cp $1/* $morphological_analyzer_dir/tmp
  pushd . >> /dev/null
  cd $morphological_analyzer_dir
  wine CIR_RE1.exe ww tmp\\*.txt -p
  popd >> /dev/null
  mv $morphological_analyzer_dir/tmp/*.plm $2
  rm -rf $morphological_analyzer_dir/tmp
else
  stanfordcorenlp_dir=`realpath $0`
  auxiliary_script=`dirname $stanfordcorenlp_dir`/$3/calculate_novelty_ambiguity.py
  stanfordcorenlp_dir=`dirname $stanfordcorenlp_dir`/$3/StanfordAnalyzer/dist
  tmp_dir=$stanfordcorenlp_dir/tmp
  echo 'source_directory_name='`realpath $1` > $stanfordcorenlp_dir/stanfordanalyzer.conf
  echo 'destination_directory_name='`realpath $tmp_dir` >> $stanfordcorenlp_dir/stanfordanalyzer.conf
  pushd . > /dev/null
  cd $stanfordcorenlp_dir
  java -Xms2048m -Xmx4096m -jar $stanfordcorenlp_dir/StanfordAnalyzer.jar
  popd > /dev/null
  if [ "$#" -eq 4 ]; then
    python $auxiliary_script $stanfordcorenlp_dir/tmp $2
  else
    python $auxiliary_script $stanfordcorenlp_dir/tmp $2 -n
  fi
  find $2 -size 0 -delete
  rm -rf $stanfordcorenlp_dir/tmp
fi
