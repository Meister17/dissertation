Commission Regulation (EC) No 1189/2006
of 3 August 2006
amending for the 66th time Council Regulation (EC) No 881/2002 imposing certain specific restrictive measures directed against certain persons and entities associated with Usama bin Laden, the Al-Qaida network and the Taliban, and repealing Council Regulation (EC) No 467/2001
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 881/2002 of 27 May 2002 imposing certain specific restrictive measures directed against certain persons and entities associated with Usama bin Laden, the Al-Qaida network and the Taliban, and repealing Council Regulation (EC) No 467/2001 prohibiting the export of certain goods and services to Afghanistan, strengthening the flight ban and extending the freeze of funds and other financial resources in respect of the Taliban of Afghanistan [1], and in particular Article 7(1), first indent, thereof,
Whereas:
(1) Annex I to Regulation (EC) No 881/2002 lists the persons, groups and entities covered by the freezing of funds and economic resources under that Regulation.
(2) On 25 July 2006, the Sanctions Committee of the United Nations Security Council decided to amend the list of persons, groups and entities to whom the freezing of funds and economic resources should apply. Annex I should therefore be amended accordingly,
HAS ADOPTED THIS REGULATION:
Article 1
Annex I to Regulation (EC) No 881/2002 is hereby amended as set out in the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 3 August 2006.
For the Commission
Eneko Landáburu
Director-General for External Relations
[1] OJ L 139, 29.5.2002, p. 9. Regulation as last amended by Commission Regulation (EC) No 674/2006 (OJ L 116, 29.4.2006, p. 58).
--------------------------------------------------
ANNEX
Annex I to Regulation (EC) No 881/2002 is amended as follows:
(1) The entry "Al Rashid Trust (a.k.a. Al Rasheed Trust, Al-Rasheed Trust, Al-Rashid Trust, The Aid Organisation of The Ulema):
- Kitas Ghar, Nazimabad 4, Dahgel-Iftah, Karachi, Pakistan,
- Jamia Maajid, Sulalman Park, Melgium Pura, Lahore, Pakistan,
- Kitab Ghar, Darul Ifta Wal Irshad, Nazimabad No 4, Karachi, Pakistan, tel. 668 33 01; tel. 0300-820 91 99; Fax 662 38 14,
- Jamia Masjid, Sulaiman Park, Begum Pura, Lahore, Pakistan; tel. 042-681 20 81,
- 302b-40, Good Earth Court, Opposite Pia Planitarium, Block 13a, Gulshan -I Iqbal, Karachi; tel. 497 92 63,
- 617 Clifton Center, Block 5, 6th Floor, Clifton, Karachi; tel. 587-25 45,
- 605 Landmark Plaza, 11 Chundrigar Road, Opposite Jang Building, Karachi, Pakistan; tel. 262 38 18-19,
- Office Dha'rbi M'unin, Opposite Khyber Bank, Abbottabad Road, Mansehra, Pakistan,
- Office Dhar'bi M'unin ZR Brothers, Katcherry Road, Chowk Yadgaar, Peshawar, Pakistan,
- Office Dha'rbi-M'unin, Rm No 3 Moti Plaza, Near Liaquat Bagh, Muree Road, Rawalpindi, Pakistan,
- Office Dha'rbi-M'unin, Top floor, Dr Dawa Khan Dental Clinic Surgeon, Main Baxae, Mingora, Swat, Pakistan,
- Operations in Afghanistan: Herat, Jalalabad, Kabul, Kandahar, Mazar Sherif,
- Also operations in Kosovo, Chechnya"
under the heading "Legal persons, groups and entities" shall be replaced by the following:
"Aid Organisation of The Ulema (alias (a) Al Rashid Trust, (b) Al Rasheed Trust, (c) Al-Rasheed Trust, (d) Al-Rashid Trust). Address:
(a) Kitab Ghar, Darul Ifta Wal Irshad, Nazimabad No 4, Karachi, Pakistan (Tel. (a) 668 33 01; (b) 0300-820 91 99; Fax 662 38 14),
(b) 302b-40, Good Earth Court, Opposite Pia Planitarium, Block 13a, Gulshan -I Iqbal, Karachi (Tel. 497 92 63),
(c) 617 Clifton Center, Block 5, 6th Floor, Clifton, Karachi (Tel. 587 25 45),
(d) 605 Landmark Plaza, 11 Chundrigar Road, Opposite Jang Building, Karachi, Pakistan (Tel. 262 38 18-19),
(e) Jamia Masjid, Sulaiman Park, Begum Pura, Lahore, Pakistan (Tel. 042-681 20 81).
Other information: (a) Headquarters in Pakistan, (b) Account numbers in Habib Bank Ltd., Foreign Exchange Branch: 05501741 and 06500138."
(2) The entry "Al-Nur Honey Press Shops (aka Al-Nur Honey Center), Sanaa, Yemen" under the heading "Legal persons, groups and entities" shall be replaced by the following:
A"l-Nur Honey Press Shops (alias Al-Nur Honey Center). Address: Sanaa, Yemen. Other information: established by Mohamed Mohamed A-Hamati from Hufash District, El Mahweet Governate, Yemen."
(3) The entry "Eastern Turkistan Islamic Movement or East Turkistan Islamic Movement (ETIM) (aka Eastern Turkistan Islamic Party or Eastern Turkistan Islamic Party of Allah)" under the heading "Legal persons, groups and entities" shall be replaced by the following:
"Eastern Turkistan Islamic Movement (alias (a) The Eastern Turkistan Islamic Party, (b) The Eastern Turkistan Islamic Party of Allah)."
(4) The entry "Global Relief Foundation (alias (a) GRF, (b) Fondation Secours Mondial, (c) Secours mondial de France, (d) SEMONDE, (e) Fondation Secours Mondial — Belgique a.s.b.l., (f) Fondation Secours Mondial v.z.w, (g) FSM, (h) Stichting Wereldhulp — Belgie, v.z.w., (i) Fondation Secours Mondial — Kosova, (j) Fondation Secours Mondial "World Relief". Address:
(a) 9935 South 76th Avenue, Unit 1, Bridgeview, Illinois 60455, U.S.A.,
(b) PO Box 1406, Bridgeview, Illinois 60455, U.S.A.,
(c) 49 rue du Lazaret, 67100 Strasbourg, France,
(d) Vaatjesstraat 29, 2580 Putte, Belgium,
(e) Rue des Bataves 69, 1040 Etterbeek (Brussels), Belgium,
(f) PO Box 6, 1040 Etterbeek 2 (Brussels), Belgium,
(g) Mula Mustafe Baseskije Street No. 72, Sarajevo, Bosnia and Herzegovina,
(h) Put Mladih Muslimana Street 30/A, Sarajevo, Bosnia and Herzegovina,
(i) Rr. Skenderbeu 76, Lagjja Sefa, Gjakova, Kosovo,
(j) Ylli Morina Road, Djakovica, Kosovo,
(k) Rruga e Kavajes, Building No. 3, Apartment No. 61, PO Box 2892, Tirana, Albania,
(l) House 267 Street No. 54, Sector F — 11/4, Islamabad, Pakistan.
Other information:
(a) Other Foreign Locations: Afghanistan, Azerbaijan, Bangladesh, Chechnya (Russia), China, Eritrea, Ethiopia, Georgia, India, Ingushetia (Russia), Iraq, Jordan, Kashmir, Lebanon, West Bank and Gaza, Sierra Leone, Somalia and Syria.
(b) U.S. Federal Employer Identification: 36-3804626.
(c) V.A.T. Number: BE 454 419 759.
(d) Belgian addresses are those of Fondation Secours Mondial — Belgique a.s.b.l and Fondation Secours Mondial vzw. since 1998"
under the heading "Legal persons, groups and entities" shall be replaced by the following:
"Global Relief Foundation (alias (a) GRF, (b) Fondation Secours Mondial, (c) Secours mondial de France, (d) SEMONDE, (e) Fondation Secours Mondial — Belgique a.s.b.l., (f) Fondation Secours Mondial v.z.w, (g) FSM, (h) Stichting Wereldhulp — Belgie, v.z.w., (i) Fondation Secours Mondial — Kosova, (j) Fondation Secours Mondial "World Relief". Address:
(a) 9935 South 76th Avenue, Unit 1, Bridgeview, Illinois 60455, U.S.A.,
(b) PO Box 1406, Bridgeview, Illinois 60455, U.S.A.,
(c) 49 rue du Lazaret, 67100 Strasbourg, France,
(d) Vaatjesstraat 29, 2580 Putte, Belgium,
(e) Rue des Bataves 69, 1040 Etterbeek (Brussels), Belgium,
(f) PO Box 6, 1040 Etterbeek 2 (Brussels), Belgium,
(g) Mula Mustafe Baseskije Street No. 72, Sarajevo, Bosnia and Herzegovina,
(h) Put Mladih Muslimana Street 30/A, Sarajevo, Bosnia and Herzegovina,
(i) Rr. Skenderbeu 76, Lagjja Sefa, Gjakova, Kosovo,
(j) Ylli Morina Road, Djakovica, Kosovo,
(k) Rruga e Kavajes, Building No. 3, Apartment No. 61, PO Box 2892, Tirana, Albania,
(l) House 267 Street No. 54, Sector F — 11/4, Islamabad, Pakistan.
Other information:
(a) Other Foreign Locations: Afghanistan, Azerbaijan, Bangladesh, Chechnya (Russia), China, Eritrea, Ethiopia, Georgia, India, Ingushetia (Russia), Iraq, Jordan, Lebanon, West Bank and Gaza, Sierra Leone, Somalia and Syria.
(b) U.S. Federal Employer Identification: 36-3804626.
(c) V.A.T. Number: BE 454 419 759.
(d) Belgian addresses are those of Fondation Secours Mondial — Belgique a.s.b.l and Fondation Secours Mondial vzw. since 1998."
(5) The entry "Revival of Islamic Heritage Society (RIHS), aka Jamiat Ihia Al-Turath Al-Islamiya, Revival of Islamic Society Heritage on the African Continent, Jamia Ihya Ul Turath; office locations: Pakistan and Afghanistan. NB: only the Pakistan and Afghanistan offices of this entity will be designated" under the heading "Legal persons, groups and entities" shall be replaced by the following:
"Revival of Islamic Heritage Society (alias (a) Jamiat Ihia Al-Turath Al-Islamiya, (b) Revival of Islamic Society Heritage on the African Continent, (c) Jamia Ihya Ul Turath, (d) RIHS). Office locations: Pakistan and Afghanistan. Other information: only the Pakistan and Afghanistan offices of this entity are designated."
(6) The entry "Riyadus-Salikhin Reconnaissance and Sabotage Battalion of Chechen Martyrs (alias Riyadus-Salikhin Reconnaissance and Sabotage Battalion, Riyadh-as-Saliheen, the Sabotage and Military Surveillance Groupof the Riyadh al-Salihin Martyrs, Firqat al-Takhrib wa al-Istitla al-Askariyah li Shuhada Riyadh al-Salihin)" under the heading "Legal persons, groups and entities" shall be replaced by the following:
"Riyadus-Salikhin Reconnaissance and Sabotage Battalion of Chechen Martyrs (alias (a) Riyadus-Salikhin Reconnaissance and Sabotage Battalion, (b) Riyadh-as-Saliheen, (c) The Sabotage and Military Surveillance Group of the Riyadh al-Salihin Martyrs, (d) Firqat al-Takhrib wa al-Istitla al-Askariyah li Shuhada Riyadh al-Salihin, (e) Riyadu-Salikhin Reconnaissance and Sabotage battalion of Shahids (Martyrs), (f) RSRSBCM)."
(7) The entry "Special Purpose Islamic Regiment (alias the Islamic Special Purpose Regiment, the al-Jihad-Fisi-Sabililah Special Islamic Regiment)" under the heading "Legal persons, groups and entities" shall be replaced by the following:
"Special Purpose Islamic Regiment (alias (a) The Islamic Special Purpose Regiment, (b) The al-Jihad-Fisi-Sabililah Special Islamic Regiment, (c) Islamic Regiment of Special Meaning, (d) SPIR)."
(8) The entry "Youssef M. Nada, Via Riasc 4, CH-6911 Campione d'Italia I, Switzerland" under the heading "Legal persons, groups and entities" shall be replaced by the following:
"Youssef M. Nada, Via Riasc 4, CH-6911 Campione d'Italia I, Italy".
(9) The entry "Anafi, Nazirullah, Maulavi (Commercial Attaché, Taliban "Embassy", Islamabad)" under the heading "Natural persons" shall be replaced by the following:
"Nazirullah Aanafi. Title: Maulavi. Function: Commercial Attaché, Taliban "Embassy", Islamabad, Pakistan. Date of birth: 1962. Place of birth: Kandahar, Afghanistan. Nationality: Afghan. Passport No: D 000912 (issued on 30.6.1998)."
(10) The entry "Qadeer, Abdul, General (Military Attaché, Taliban "Embassy", Islamabad)" under the heading "Natural persons" shall be replaced by the following:
"Abdul Qadeer. Title: General. Function: Military Attaché, Taliban "Embassy", Islamabad, Pakistan. Date of birth: 1967. Place of birth: Nangarhar, Afghanistan. Nationality: Afghan. Passport No: D 000974."
(11) The entry "Shafiq Ben Mohamed Ben Mohamed Al-Ayadi (alias (a) Bin Muhammad, Ayadi Chafiq, (b) Ayadi Chafik, Ben Muhammad, (c) Aiadi, Ben Muhammad, (d) Aiady, Ben Muhammad, (e) Ayadi Shafig Ben Mohamed, (f) Ben Mohamed, Ayadi Chafig, (g) Abou El Baraa). Address: (a) Helene Meyer Ring 10-1415-80809, Munich, Germany, (b) 129 Park Road, NW8, London, England, (c) 28 Chaussée De Lille, Mouscron, Belgium, (d) Street of Provare 20, Sarajevo, Bosnia and Herzegovina (last registered address in Bosnia and Herzegovina). Date of birth: (a) 21.3.1963, (b) 21.1.1963. Place of birth: Sfax, Tunisia. Nationality: (a) Tunisian, (b) Bosnia and Herzegovina. Passport No: (a) E 423362 delivered in Islamabad on 15.5.1988, (b) 0841438 (Bosnia and Herzegovina passport issued on 30.12.1998 which expired on 30.12.2003). National identification No: 1292931. Other information: (a) address in Belgium is a PO box, (b) his father’s name is Mohamed, mother's name is Medina Abid; (c) reportedly living in Dublin, Ireland" under the heading "Natural persons" shall be replaced by the following:
"Shafiq Ben Mohamed Ben Mohamed Al-Ayadi (alias (a) Bin Muhammad, Ayadi Chafiq, (b) Ayadi Chafik, Ben Muhammad, (c) Aiadi, Ben Muhammad, (d) Aiady, Ben Muhammad, (e) Ayadi Shafig Ben Mohamed, (f) Ben Mohamed, Ayadi Chafig, (g) Chafiq Ayadi, (h) Chafik Ayadi, (i) Ayadi Chafiq, (j) Ayadi Chafik, (k) Abou El Baraa). Address: (a) Helene Meyer Ring 10-1415-80809, Munich, Germany, (b) 129 Park Road, London NW8, England, (c) 28 Chaussée De Lille, Mouscron, Belgium, (d) Street of Provare 20, Sarajevo, Bosnia and Herzegovina (last registered address in Bosnia and Herzegovina). Date of birth: (a) 21.3.1963, (b) 21.1.1963. Place of birth: Sfax, Tunisia. Nationality: (a) Tunisian, (b) Bosnia and Herzegovina. Passport No: (a) E 423362 (delivered in Islamabad on 15.5.1988), (b) 0841438 (Bosnia and Herzegovina passport issued on 30.12.1998 which expired on 30.12.2003). National identification No: 1292931. Other information: (a) address in Belgium is a PO box, (b) his father’s name is Mohamed, mother's name is Medina Abid; (c) reportedly living in Dublin, Ireland."
(12) The entry "Ahmed Mohammed Hamed Ali (aka Abdurehman, Ahmed Mohammed; aka Abu Fatima; aka Abu Islam; aka Abu Khadiijah; aka Ahmed Hamed; aka Ahmed The Egyptian; aka Ahmed, Ahmed; aka Al-Masri, Ahmad; aka Al-Surir, Abu Islam; aka Ali, Ahmed Mohammed; aka Ali, Hamed; aka Hemed, Ahmed; aka Shieb, Ahmed; aka Shuaib), Afghanistan; born 1965, Egypt; citizen Egypt" under the heading "Natural persons" shall be replaced by the following:
"Ahmed Mohammed Hamed Ali (alias (a) Abdurehman, Ahmed Mohammed, (b) Ahmed Hamed, (c) Ali, Ahmed Mohammed, (d) Ali, Hamed, (e) Hemed, Ahmed, (f) Shieb, Ahmed, (g) Abu Fatima, (h) Abu Islam, (i) Abu Khadiijah, (j) Ahmed The Egyptian, (k) Ahmed, Ahmed, (l) Al-Masri, Ahmad, (m) Al-Surir, Abu Islam, (n) Shuaib. Date of birth: 1965. Place of birth: Egypt. Nationality: Egyptian."
(13) The entry "Al-Jadawi, Saqar; Born c. 1965; thought to be a Yemeni and Saudi national; aide to Usama Bin Laden" under the heading "Natural persons" shall be replaced by the following:
"Saqar Al-Jadawi (alias Saqr Al-Jaddawi). Address: Shari Tunis, Sana’a, Yemen. Date of birth: 1965. Place of birth: Al-Mukalla, Yemen. Nationality: Yemeni. Passport No: 00385937. Other information: (a) address is previous address, (b) driver and private bodyguard to Usama Bin Laden from 1996 until 2001."
(14) The entry "Shaykh Abd-al-Majid AL-ZINDANI (alias (a) Abdelmajid AL-ZINDANI; (b) Shaykh Abd Al-Majid AL-ZINDANI). Date of birth: 1950. Place of birth: Yemen. Nationality: Yemeni. Passport No: A005487 (Yemen) issued 13 August 1995" under the heading "Natural persons" shall be replaced by the following:
"Abd-al-Majid Aziz Al-Zindani (alias (a) Abdelmajid Al-Zindani, (b) Abd Al-Majid Al-Zindani, (c) Abd Al-Meguid Al-Zandani). Title: Sheikh. Address: Sanaa, Yemen. Date of birth: (a) 1942, (b) circa 1950. Place of birth: Yemen. Nationality: Yemeni. Passport No: A005487 (issued 13.8.1995)."
(15) The entry "Allamuddin, Syed (Second Secretary, Taliban "Consulate General", Peshawar)" under the heading "Natural persons" shall be replaced by the following:
"Sayed Allamuddin Athear. Function: Second Secretary, Taliban "Consulate General", Peshawar, Pakistan. Date of birth: 1955. Place of birth: Badakshan. Nationality: Afghan. Passport No: D 000994."
(16) The entry "Huda bin Abdul HAQ (alias (a) Ali Gufron, (b) Ali Ghufron, (c) Ali Gufron al Mukhlas, (d) Mukhlas, (e) Muklas, (f) Muchlas, (g) Sofwan); date of birth: (a) 9 February 1960 (b) 2 February 1960; place of birth: Solokuro subdistrict in Lamongan district, East Java province, Indonesia; nationality: Indonesian" under the heading "Natural persons" shall be replaced by the following:
"Huda bin Abdul Haq (alias (a) Ali Gufron, (b) Ali Ghufron, (c) Ali Gufron al Mukhlas, (d) Mukhlas, (e) Muklas, (f) Muchlas, (g) Sofwan). Date of birth: (a) 9.2.1960 (b) 2.2.1960. Place of birth: Solokuro subdistrict in Lamongan district, East Java province, Indonesia. Nationality: Indonesian."
(17) The entry "Ramzi Mohamed Abdullah Binalshibh (alias (a) Binalsheidah, Ramzi Mohamed Abdullah, (b) Bin al Shibh, Ramzi, (c) Omar, Ramzi Mohamed Abdellah). Date of birth: 1.5.1972 or 16.9.1973. Place of birth: (a) Hadramawt, Yemen, (b) Khartoum, Sudan. Nationality: (a) Sudan, (b) Yemen. Passport of Yemen No 00 085 243 issued on 12.11.1997 in Sanaa, Yemen" under the heading "Natural persons" shall be replaced by the following:
"Ramzi Mohamed Abdullah Binalshibh (alias (a) Binalsheidah, Ramzi Mohamed Abdullah, (b) Bin al Shibh, Ramzi, (c) Omar, Ramzi Mohamed Abdellah, (d) Mohamed Ali Abdullah Bawazir, (e) Ramzi Omar). Date of birth: (a) 1.5.1972, (b) 16.9.1973. Place of birth: (a) Gheil Bawazir, Hadramawt, Yemen, (b) Khartoum, Sudan. Nationality: (a) Yemen, (b) Sudan. Passport No: 00085243 (issued on 17.11.1997 in Sanaa, Yemen). Other information: arrested in Karachi, Pakistan, on 30.9.2002."
(18) The entry "Daud, Mohammad (Administrative Attaché, Taliban "Embassy", Islamabad)" under the heading "Natural persons" shall be replaced by the following:
"Mohammad Daud. Function: Administrative Attaché, Taliban "Embassy", Islamabad, Pakistan. Date of birth: 1956. Place of birth: Kabul, Afghanistan. Nationality: Afghan. Passport No: D 00732."
(19) The entry "Fauzi, Habibullah (First Secretary/Deputy Head of Mission, Taliban "Embassy", Islamabad)" under the heading "Natural persons" shall be replaced by the following:
"Habibullah Faizi. Function: Second secretary. Date of birth: 1961. Place of birth: Ghazni, Afghanistan. Nationality: Afghan. Passport No: D 010678 (issued on 19.12.1993)."
(20) The entry "Murad, Abdullah, Maulavi (Consul General, Taliban "Consulate General", Quetta)" under the heading "Natural persons" shall be replaced by the following:
"Abdullah Hamad. Title: Maulavi. Function: Consul General, Taliban "Consulate General", Quetta, Pakistan. Date of birth: 1972. Place of birth: Helmand, Afghanistan. Nationality: Afghan. Passport No: D 00857 (issued on 20.11.1997)."
(21) The entry "Aazem, Abdul Haiy, Maulavi (First Secretary, Taliban "Consulate General", Quetta)" under the heading "Natural persons" shall be replaced by the following:
"Abdul Hai Hazem. Title: Maulavi. Function: First Secretary, Taliban "Consulate General", Quetta, Pakistan. Date of birth: 1971. Place of birth: Ghazni, Afghanistan. Nationality: Afghan. Passport No: D 0001203."
(22) The entry "Zayn al-Abidin Muhammad HUSAYN (alias (a) Abu Zubaida (b) Abd Al-Hadi Al-Wahab (c) Zain Al-Abidin Muhahhad Husain (d) Zain Al-Abidin Muhahhad Husain (e) Abu Zubaydah (f) Tariq); date of birth: 12 March 1971; place of birth: Riyadh, Saudi Arabia: nationality: thought to be a Saudi and Palestinian national; passport No: bearer of Egyptian passport No 484824 issued on 18 January 1984 at the Egyptian embassy in Riyadh; other information: close associate of Usama bin Laden and facilitator of terrorist travel" under the heading "Natural persons" shall be replaced by the following:
"Zayn al-Abidin Muhammad Hussein (alias (a) Abu Zubaida, (b) Abd Al-Hadi Al-Wahab, (c) Zain Al-Abidin Muhahhad Husain, (d) Zain Al-Abidin Muhahhad Husain, (e) Abu Zubaydah, (f) Tariq). Date of birth: 12.3.1971. Place of birth: Riyadh, Saudi Arabia. Nationality: Palestinian. Passport No: 484824 (Egyptian passport issued on 18.1.1984 at the Egyptian Embassy in Riyadh). Other information: close associate of Usama bin Laden and facilitator of terrorist travel."
(23) The entry "Kakazada, Rahamatullah, Maulavi (Consul General, Taliban "Consulate General", Karachi)" under the heading "Natural persons" shall be replaced by the following:
"Rahamatullah Kakazada. Title: Maulavi. Function: Consul General, Taliban "Consulate General", Karachi, Pakistan. Date of birth: 1968. Place of birth: Ghazni, Afghanistan. Nationality: Afghan. Passport No: D 000952 (issued on 7.1.1999)."
(24) The entry "Dawood Ibrahim Kaskar (alias (a) Dawood Ebrahim, (b) Sheikh Dawood Hassan). Date of birth: 1955. Place of birth: Ratnagiri, India. Nationality: Indian. Passport No: A-333602, issued in Bombay, India, on 6 April 1985" under the heading "Natural persons" shall be replaced by the following:
"Dawood Ibrahim Kaskar (alias (a) Dawood Ebrahim, (b) Sheikh Dawood Hassan, (c) Sheikh Ibrahim, (d) Hizrat). Date of birth: 26.12.1955. Place of birth: (a) Bombay, (b) Ratnagiri, India. Nationality: Indian. Passport No: A-333602 (issued on 4.6.1985 in Bombay, India). Other information: (a) passport revoked by the Government of India, (b) international arrest warrant issued by India."
(25) The entry "Mostafa Kamel Mostafa Ibrahim (alias (a) Mustafa Kamel Mustafa, (b) Adam Ramsey Eaman, (c) Kamel Mustapha Mustapha, (d) Mustapha Kamel Mustapha, (e) Abu Hamza, (f) Abu Hamza Al-Masri, (g) Al-Masri, Abu Hamza, (h) Al-Misri, Abu Hamza). Address: (a) 9 Albourne Road, Shepherds Bush, London W12 OLW, United Kingdom; (b) 8 Adie Road, Hammersmith, London W6 OPW, United Kingdom. Date of birth: 15.4.1958. Place of birth: Alexandria, Egypt. Other information: under investigation in the United Kingdom" under the heading "Natural persons" shall be replaced by the following:
"Mostafa Kamel Mostafa Ibrahim (alias (a) Mustafa Kamel Mustafa, (b) Adam Ramsey Eaman, (c) Kamel Mustapha Mustapha, (d) Mustapha Kamel Mustapha, (e) Abu Hamza, (f) Mostafa Kamel Mostafa, (g) Abu Hamza Al-Masri, (h) Al-Masri, Abu Hamza, (i) Al-Misri, Abu Hamza). Address: (a) 9 Aldbourne Road, Shepherds Bush, London W12 OLW, United Kingdom; (b) 8 Adie Road, Hammersmith, London W6 OPW, United Kingdom. Date of birth: 15.4.1958. Place of birth: Alexandria, Egypt. Nationality: British. Other information: under investigation in the United Kingdom."
(26) The entry "Mohammad, Akhtar, Maulavi (Education Attaché, Taliban "Consulate General", Peshawar)" under the heading "Natural persons" shall be replaced by the following:
"Akhtar Mohammad Maz-Hari. Title: Maulavi. Function: Education Attaché, Taliban "Consulate General", Peshawar, Pakistan. Date of birth: 1970. Place of birth: Kunduz, Afghanistan. Nationality: Afghan. Passport No: SE 012820 (issued on 4.11.2000)."
(27) The entry "Saddiq, Alhaj Mohammad, Maulavi (Trade Representative, Taliban "Consulate General", Peshawar)" under the heading "Natural persons" shall be replaced by the following:
"Mohammad Sadiq (alias Maulavi Amir Mohammad) Title: (a) Alhaj, (b) Maulavi. Function: Head of Afghan Trade Agency, Peshawar, Pakistan. Date of birth: 1934. Place of birth: Ghazni, Afghanistan. Nationality: Afghan. Passport No: SE 011252."
(28) The entry "Nedal Mahmoud Saleh (alias (a) Nedal Mahmoud N. Saleh, (b) Hitem). Address: (a) Via Milano 105, Casal di Principe (Caserta), Italy, (b) Via di Saliceto 51/9, Bologna, Italy. Place of birth: Taiz (Yemen). Date of birth: 1 March 1970. Other information: arrested in Italy on 19.8.2003" under the heading "Natural persons" shall be replaced by the following:
"Nedal Mahmoud Saleh (alias (a) Nedal Mahmoud N. Saleh, (b) Salah Nedal, (c) Hitem). Address: (a) Via Milano 105, Casal di Principe (Caserta), Italy, (b) Via di Saliceto 51/9, Bologna, Italy. Date of birth: (a) 1.3.1970, (b) 26.3.1972. Place of birth: Taiz, Yemen. Nationality: Yemeni. Other information: arrested in Italy on 19.8.2003."
(29) The entry "Wali, Qari Abdul (First Secretary, Taliban "Consulate General", Peshawar)" under the heading "Natural persons" shall be replaced by the following:
"Qari Abdul Wali Seddiqi. Function: Third Secretary. Date of birth: 1974. Place of birth: Ghazni, Afghanistan. Nationality: Afghan. Passport No: D 000769 (issued on 2.2.1997)."
(30) The entry "Shenwary, Haji Abdul Ghafar (Third Secretary, Taliban "Consulate General", Karachi)" under the heading "Natural persons" shall be replaced by the following:
"Abdul Ghafar Shinwari. Title: Haji. Function: Third Secretary, Taliban "Consulate General", Karachi, Pakistan. Date of birth: 29.3.1965. Place of birth: Kandahar, Afghanistan. Nationality: Afghan. Passport No: D 000763 (issued on 9.1.1997)."
(31) The entry "Najibullah, Maulavi (Consul General, Taliban "Consulate General", Peshawar)" under the heading "Natural persons" shall be replaced by the following:
"Najib Ullah (alias Maulvi Muhammad Juma). Title: Maulavi. Function: Consul General, Taliban "Consulate General", Peshawar, Pakistan. Date of birth: 1954. Place of birth: Farah. Nationality: Afghan. Passport No: 00737 (issued on 20.10.1996)."
(32) The entry "Zelimkhan Ahmedovic (Abdul-Muslimovich) YANDARBIEV. Place of birth: village of Vydriha, Eastern Kazakhstan region, USSR. Date of birth: 12 September 1952. Nationality: Russian Federation. Passports: Russian passport 43 No 1600453" under the heading "Natural persons" shall be replaced by the following:
"Zelimkhan Ahmedovich Yandarbiev (alias Abdul-Muslimovich). Address: Derzhavina street 281-59, Grozny, Chechen Republic, Russian Federation. Date of birth: 12.9.1952. Place of birth: village of Vydrikh, Shemonaikhinsk (Verkhubinsk) district, (Soviet Socialist Republic of) Kazakhstan. Nationality: Russian. Passport No: (a) 43 No 1600453, (b) 535884942 (Russian foreign passport), (c) 35388849 (Russian foreign passport). Other information: (a) address is former address, (b) killed on 19.2.2004."
(33) The entries "Zaeef, Abdul Salam, Mullah (Ambassador Extraordinary and Plenipotentiary, Taliban "Embassy", Islamabad)". "Zaeef, Abdul Salam (Taliban Ambassador to Pakistan)" and "Zaief, Abdul Salam, Mullah (Deputy Minister of Mines and Industries)" under the heading "Natural persons" shall be replaced by the following:
"Abdul Salam Zaeef. Title: Mullah. Function: (a) Deputy Minister of Mines and Industries, (b) Ambassador Extraordinary and Plenipotentiary, Taliban "Embassy", Islamabad, Pakistan. Date of birth: 1968. Place of birth: Kandahar, Afghanistan. Nationality: Afghan. Passport No: D 001215 (issued on 29.8.2000)."
(34) The entry "Zahid, Mohammad, Mullah (Third Secretary, Taliban "Embassy", Islamabad)" under the heading "Natural persons" shall be replaced by the following:
"Mohammad Zahid. Title: Mullah. Function: Third Secretary, Taliban "Embassy", Islamabad, Pakistan. Date of birth: 1971. Place of birth: Logar, Afghanistan. Nationality: Afghan. Passport No: D 001206 (issued on 17.7.2000)."
--------------------------------------------------
