COMMISSION DECISION of 18 December 1996 laying down provisions for the implementation of Council Directive 96/16/EC on statistical surveys of milk and milk products (Text with EEA relevance) (97/80/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 96/16/EC of 19 March 1996 on statistical surveys of milk and milk products (1), and in particular Article 3 (2), Article 4 (2) and Article 6 (1) thereof,
Whereas, following the replacement by Directive 96/16/EC of Directive 72/280/EEC, a parallel re-casting of Commission Decision 72/356/EEC of 18 October 1972 laying down implementing provisions for the statistical surveys on milk and milk products (2), as last amended by Decision 86/180/EEC (3), is appropriate as a means of guaranteeing the continuous development of statistics in harmony with prevailing economic conditions;
Whereas experience acquired in applying Decision 72/356/EEC has shown that it is necessary to adopt more detailed explanatory notes concerning the products and tables;
Whereas, with a view to improved integration of Community statistics, it is opportune to look forward to complete coordination with the Prodcom list referred to in Council Regulation (EEC) No 3924/91 of 19 December 1991 on the establishment of a Community survey of industrial production (4);
Whereas the measures provided for in this Decision are in accordance with the opinion of the Standing Committee for Agricultural Statistics,
HAS ADOPTED THIS DECISION:
Article 1
The list of milk products covered by the surveys referred to in Article 3 (2) of Directive 96/16/EC is set out in Annex I to this Decision.
Article 2
Specimens of the tables to be used for transmitting the data referred to in Article 6 (1) of Directive 96/16/EC are set out in Annex II to this Decision.
Article 3
The work programme referred to in Article 4 (2) of Directive 96/16/EC is set out in Annex III to this Decision.
Article 4
Decision 72/356/EEC is repealed with effect from 1 January 1997.
References to the repealed Decision shall be construed as references to this Decision.
Article 5
This Decision is addressed to the Member States.
Done at Brussels, 18 December 1996.
For the Commission
Yves-Thibault DE SILGUY
Member of the Commission
(1) OJ No L 78, 28. 3. 1996, p. 27.
(2) OJ No L 246, 30. 10. 1972, p. 1.
(3) OJ No L 138, 24. 5. 1986, p. 49.
(4) OJ No L 374, 31. 12. 1991, p. 1.
ANNEX I
LIST OF MILK PRODUCTS
>TABLE>
EXPLANATORY NOTES
DRINKING MILK (11)
Drinking milk: raw milk, whole milk, semi-skimmed and skimmed milk containing no additives.
- Relates only to milk directly intended for consumption, normally in containers of 2 l or less,
- Also includes milk with vitamin additives.
Raw milk (111): milk produced by the secretion of the mammary glands of one or more cows, ewes, goats or buffalos, which has not been heated beyond 40 °C or undergone any treatment that has an equivalent effect (Council Directive 92/46/EEC of 16 June 1992, OJ No L 268, 14. 9. 1992, p. 3).
Whole milk (112): milk which has been subject to one heat treatment or an authorized treatment of equivalent effect by a milk processor and whose fat content is either at least 3,50 % naturally or has been brought to at least 3,5 % (Council Regulation (EEC) No 1411/71 of 29 June 1971, OJ No L 148, 3. 7. 1971, p. 4).
- Also includes Swedish drinking milk types designated as 'Gammaldags mjoelk` and 'Standardmjoelk` whose milk fat contents are 4,2 % and 3 % respectively.
Semi-skimmed milk (113): milk which has been subject to at least one heat treatment or an authorized treatment of equivalent effect by a milk processor and whose fat content has been brought to at least 1,50 % and at most 1,80 % (Council Regulation (EEC) No 1411/71 of 29 June 1971, OJ No L 148, 3. 7. 1971, p. 4).
- Also includes Finnish drinking milk designated as 'ykkoesmaito/ettans mjoelk` whose milk fat content is 1 %,
- Also includes the Swedish drinking milk types designated as 'Ekologisk mjoelk` and 'Mellanmjoelk` whose milk fat contents are 2 % and 1,5 % respectively,
- Also includes Austrian drinking milk whose milk fat content is between 2 % and 2,5 %.
Skimmed milk (114): milk which has been subject to at least one heat treatment or an authorized treatment of equivalent effect by a milk processor and whose fat content has been brought to not more than 0,30 % (Council Regulation (EEC) No 1411/71 of 29 June 1971, OJ No L 148, 3. 7. 1971, p. 4).
- Also includes the Swedish drinking milk types designated as 'Laettmjoelk` and 'Minimjoelk` whose milk fat contents are 0,5 % and 0,07 % respectively,
- Also includes Austrian drinking milk whose milk fat content is 0,5 %.
Pasteurized: pasteurized milk must have been obtained by means of a treatment involving a high temperature for a short time (at least 71,7 °C for 15 seconds or any equivalent combination) or pasteurization process using different time and temperature combinations to obtain an equivalent effect (Council Directive 92/46/EEC of 16 June 1992, OJ No L 268, 14. 9. 1992, p. 24).
Sterilized: sterilized milk must:
- have been heated and sterilized in hermetically sealed wrappings or containers, the seal of which must remain intact,
- in the event of random sampling, be of preservability such that no deterioration can be observed after it has spent 15 days in a closed container at a temperature of +30 °C (Council Directive 92/46/EEC of 16 June 1992, OJ No L 268, 14. 9. 1992, p. 25).
Uperized: uperized milk (or UHT milk) must be produced by applying a continuous flow of heat using a high temperature for a short time (not less than 135 °C for not less than 1 second) (Council Directive 92/46/EEC of 16 June 1992, OJ No L 268, 14. 9. 1992, p. 24).
- Member States which make no distinction between sterilized and uperized milk may group them together.
BUTTERMILK (12)
Buttermilk: Residual product (may even be acid or acidified) of the processing of milk or cream into butter (by continuous churning and separation of the solid fats).
- Buttermilk with additives must be included in drinks with a milk base.
CREAM (13)
Cream: a film of fat which forms naturally on the surface of the milk by slow agglomeration of emulsifying fat globules. If it is removed by skimming it from the surface of the milk or extracted from the milk by centrifuging in a cream separator, it has, in addition to the other components of the milk, a relatively high fat content (usually exceeding 10 % of the weight of the product).
Cream (13): cream which has been processed and is available for delivery outside dairies (i.e. for human consumption, as raw material for manufacturers of chocolate, ice cream, etc.). In the same way as for other products, does not include intermediate production intended for the manufacture of other dairy products.
Cream of a fat content by weight not exceeding 29 % (131).
Cream of a fat content by weight over 29 % (132).
- Table A/'Collection`: raw material (in milk equivalent) delivered to dairies by agricultural holdings.
- Table B/'Availabilities`: cream separated at the farm and delivered to a dairy.
- Table A/'Products obtained` and Table B/'Utilization`:
- pasteurized, sterilized or uperized;
- also includes acidified cream;
- also includes cream in cartons or tins.
ACIDIFIED MILK (14)
Acidified milk: milk products with a pH of between 3,8 and 5,5.
- Relates to yoghurts, drinkable yoghurts, prepared yoghurts, heat-treated fermented milk and others,
- Also includes products based on or containing bifidus.
Acidified milk with additives (141): sweetened acidified milk should be included under heading 142.
Acidified milk without additives (142): also includes acidified milk with the addition of sugar and/or sweeteners.
DRINKS WITH A MILK BASE (15)
Drinks with a milk base: other liquid products containing at least 50 % milk products, including products based on whey.
- Includes chocolate milk, buttermilk with additives or flavoured, etc.
OTHER FRESH PRODUCTS (16)
- Relates to fresh milk products not elsewhere specified, mainly milk-based desserts (jellied milks, custard tarts, cream desserts, mousses, etc.) and ice cream (and similar products) manufactured in the reporting enterprises,
- Also includes desserts in tins,
- Also includes fresh farm products collected from agricultural holdings (under heading Availability/III.4) and placed on the market without processing (excluding packaging).
CONCENTRATED MILK (21)
Concentrated milk: a product obtained by partial elimination of water, from whole milk, semi-skimmed or skimmed milk only.
- Also includes evaporated milk (heat-treated) and concentrated milk with added sugar,
- Also includes concentrated milk used for the manufacture of 'Chocolate crumb`; dried product consisting of milk, sugar and cocoa paste in the following proportions:
- milkfat: more than 6,5 % (content by weight) but less than 11 % (content by weight),
- cocoa: more than 6,5 % (content by weight) but less than 15 % (content by weight),
- sucrose (including invert sugar calculated as sucrose) more than 50 % (content by weight) but less than 60 % (content by weight),
- non-fat dry matter of milk: more than 17 % (content by weight) but less than 30 % (content by weight),
- water: more than 0,5 % (content by weight) but less than 3,5 % (content by weight).
Its composition is as given in Annex I to Commission Regulation (EEC) No 380/84 of 15 February 1984 (OJ No L 46 16. 2. 1984, p. 26).
POWDERED DAIRY PRODUCTS (22)
Powdered dairy products: product obtained by eliminating water from cream, whole milk, semi-skimmed milk, skimmed milk, buttermilk and acidified milk.
- Also includes additives to the raw material before the product is made into powder,
- Also includes milk powder manufactured in dairies and contained in powders for infants and in animal feeds.
Cream milk powder (221): milk powder with a milk fat content of not less than 42 % by weight of the product.
Whole milk powder (222): milk powder with a milk fat content of not less than 26 % and less than 42 % by weight of the product.
Partly skimmed-milk powder (223): milk powder with a milk fat content of more than 1,5 % and less than 26 % by weight of the product.
Skimmed-milk powder (224): milk powder with a maximum milk fat content of 1,5 % by weight of the product.
Buttermilk powder (225): powder product made from buttermilk.
Other powdered products (226): curdled milk and cream, kephir and other fermented or acidified milk and cream, whether or not containing added sugar or other sweetening matter or flavoured or containing added fruit or cocoa.
- Also includes mixtures of powdered cream, milk, buttermilk and/or whey,
- Also includes protein-based powdered products.
BUTTER (23)
Butter, total and other yellow fat dairy products (23): includes butter, rendered butter and butteroil, and other yellow fat products, expressed in butter equivalent with a milk fat content equal to 82 % by weight of the product.
- Table A: Denmark: includes only butter (231),
- Table B: the headings 231 (butter), 232 (rendered butter and butteroil) and 233 (other yellow fat dairy products) must be recorded in product weight. Only item 23 must be given in its butter equivalent.
Butter (231): a product with a milk fat content of not less than 80 % and less than 90 %, a maximum water content of 16 % and a maximum dry non-fat milk-material content of 2 %.
- Includes also butter which contains small amounts of herbs, spices, aromatic substances, etc. on the condition that the product retains the characteristics of butter.
Rendered butter and butteroil (232):
Rendered butter: rendered butter has a milk fat content exceeding 85 % by weight of the product. The term covers, in addition to rendered butter as such, a number of other similar dehydrated butters which are known generically under various names, such as 'dehydrated butter`, 'anhydrous butter`, 'butteroil`, 'butyric fat` (milk fat) and 'concentrated butter`.
Butteroil: a product which may be obtained from milk, cream or butter by processes which eliminate the water and the non-fat dry extract with a minimum content of milk fat of 99,3 % of the total weight and a maximum water content of 0,5 % of the total weight.
- Also includes 'ghee`,
- To avoid double counting, relates only to direct production from cream.
Other yellow fat products (233):
Reduced-fat butter: product similar to butter with a milk-fat content of less than 80 % by weight (excluding all other fat) (Sales description: three quarter-fat butter, half-fat butter and dairy spread).
Fats composed of plant and/or animal products: products in the form of a solid, malleable emulsion principally of the water-in-oil type, derived from solid and/or liquid vegetable and/or animal fats suitable for human consumption, with a milk-fat content of between 10 % and 80 % of the fat content.
CHEESE (24)
Cheese: shall be a fresh or matured, solid or semi-solid product, obtained by coagulating milk, skimmed milk, partly skimmed milk, cream, whey cream or buttermilk, alone or in combination, by the action of rennet or other suitable coagulating agents, and by partly draining the whey resulting from such coagulation. (Codex Alimentarius - FAO, Volume XVI, Standard A-6).
>TABLE>
Soft cheese (2421): cheese in which the MFFB when refined is in general not less than 68 %.
Semi-soft cheese (2422): cheese in which the MFFB when refined is in general not less than 62 % and less than 68 %.
Semi-hard cheese (2423): cheese in which the MFFB when refined is in general not less than 55 % and less than 62 %.
Hard cheese (2424): cheese in which the MFFB when refined is in general not less than 47 % and less than 55 %.
Very hard cheese (2425): cheese in which the MFFB when refined is in general less than 47 %.
Fresh cheese (2426): product obtained from sour milk from which most of the serum has been removed (e.g. by draining or pressing). Also includes curds (other than in powder form) containing up to 30 % by weight in the form of sugar and added fruits.
- Includes fresh whey cheese (obtained by concentrating whey and adding milk or milk fat).
PROCESSED CHEESE (25)
Processed cheese: product obtained by grinding, mixing, melting and emulsifying under the action of heat and with the aid of emulsifying agents one or more varieties of cheese, with or without the addition of milk components and/or other foodstuffs. (Codex Alimentarius - FAO, Volume XVI, Standard A-8 (b)).
CASEIN AND CASEINATES (26)
Casein: is the main protein constituent of milk. It is obtained from skimmed milk by precipitation (curdling), generally with acids or rennet. The heading covers various types of casein which differ according to the method of curdling, e.g. acid casein and rennet casein (paracasein). (Explanatory notes to the harmonized system - SectionVI, Chapter 35 (No 35.01)).
Caseinates: (salts of casein) include the sodium and ammonium salts known as 'soluble caseins`; these salts are normally used to prepare concentrated foods and pharmaceutical products. Calcium caseinate is used in the preparation of foodstuffs or as a glue, depending on its character. (Explanatory notes to the harmonized system - SectionVI, Chapter 35 (No 35.01)).
WHEY (27)
Whey: by-product obtained during the manufacture of cheese or casein. In the liquid state, whey contains natural constituents (on average 4,8 % lactose, 0,8 % protein and 0,2 % fats by weight of the product) which remain when the casein and the majority of the fat have been removed from the milk.
Total whey (27): also includes the whey used in the dairy for manufacturing animal feedingstuffs.
- Items 271 (whey delivered in the liquid state), 272 (whey used in the concentrated state), 273 (whey in powder or block form), 274 (lactose), 275 (lactalbumin) are to be given in their effective weight. Only item 27 (total whey) is to be given in its liquid whey equivalent and must in no event be the sum of the abovementioned quantities.
Whey delivered in the liquid state (271): whey delivered to be used mainly for animal feeds. Quantities used as raw materials for other processes must be excluded.
Whey delivered in the concentrated state (272).
Whey in powdered or block form (273).
Lactose (milk sugar) (274).
Lactalbumin (275): one of the main components of whey protein.
OTHER MANUFACTURED PRODUCTS (28)
- This heading relates to manufactured milk products (to be specified) not designated elsewhere, mainly lactoferrins.
- Also includes manufactured farm products collected from agricultural holdings (under heading Availability/III.4) and placed on the market without processing (excluding packaging or maturing).
ANNEX II
TABLE A Monthly return of milk collected (cow's milk) and products obtained
>
START OF GRAPHIC>
Country: Month: Year: A. COLLECTION
Quantites (1 000 t) Fat content
(%) Protein content (%)
1. Cow's milk from farms:
2. Cream from farms:
Product code B. PRODUCTS OBTAINED (1 000 t)
11 Drinking milk
13 Cream
14 Acidified milk
21 Concentrated milk
221+222 +223 Cream milk powder, whole milk powder and partly skimmed-milk powder
224 Skimmed-milk powder
23 Butter (total - in butter equivalent)
2411 Cheese made from cow's milk only
>END OF GRAPHIC>
Table B Annual production and utilization of milk (all milk) in dairies
>START OF GRAPHIC>
Country: Year: A. AVAILABILITIES Quantities (1 000 t) Milk fat content (t) Milk proteins (t)
1 2 3
I.Cows' milk collected from farms:
II.Other availabilities collected:
1.Ewe's milk
2.Goats' milk
3.Buffalo milk
4.Cream
5.Skimmed milk and buttermilk
6.Other products (specifiy)
III.Imports and intra-Community arrivals from dairies outside the national territory:
1.Whole milk, including raw milk
11.Of which Member States:
2.Skimmed milk
21.Of which Member States:
3.Cream
31.Of which Member States:
4.Other products (specify)
41.Of which Member States
>END OF GRAPHIC>
>START OF GRAPHIC>
Country: Year: Code
B. UTILIZATION
Quantities (1 000 t)
Milk fat content (t)
Input of:
Whole milk (1 000 t)
Skimmed milk (1 000 t)
1
2
3
4
1 Fresh products
11 Drinking milk
111 Raw milk
112 Whole milk
1121 Pasteurized
1122 Sterilized
1123 Uperized
113 Semi-skimmed milk
1131 Pasteurized
1132 Sterilized
1133 Uperized
114 Skimmed milk
1141 Pasteurized
1142 Sterilized
1143 Uperized
12 Buttermilk
13 Cream Of fat content by weight
131 Not exceeding 29 % 132 Over 29 %
14 Acidified milk (Yoghurts, drinking yoghurts and other)
141 With additives
142 Without additives
15 Drinks with a milk base
16 Other fresh products (Milk jelly and others) >END OF GRAPHIC>
>START OF GRAPHIC>
Country: Year: Code
B. UTILIZATION
Quantities (1 000 t)
Milk fat content (t)
Input of:
Whole milk (1 000 t)
Skimmed milk (1 000 t)
1
2
3
4
2 Manufactured products
21 Concentrated milk
211 Unsweetened
212 Sweetened
22 Powdered dairy products:
221 Cream milk powder
222 Whole milk powder
223 Partly skimmed-milk powder
224 Skimmed-milk powder
225 Buttermilk
226 Other powder products
23 Butter and other yellow fat dairy products
231 Butter
232 Rendered butter and butteroil
233 Other yellow fat dairy products
24 Cheese
241 Cheese by milk cagegory:
2411 Cheese from cows' milk (pure)
2412 Cheese from ewes' milk (pure)
2413 Cheese from goats' milk (pure)
2414 Others (mixed or cheese from buffalos' milk (pure))
242 Cheese (all milks) by category:
2421 Soft cheese
2422 Medium-soft cheese
2423 Medium-hard cheese
2424 Hard cheese
2425 Extra hard cheese
2426 Fresh cheese
25 Processed cheese
26 Casein and caseinates
27 Whey, total
271 Whey delivered in the liquid state
272 Whey delivered in the concentrated state
273 Whey in powder or block
274 Lactose (milk sugar)
275 Lactalbumin
28 Other manufactured products (specify) >END OF GRAPHIC>
>START OF GRAPHIC>
Country:
Year: Code
B. UTILIZATION
Quantities (1 000 t)
Milk fat content (t)
Input of:
Whole milk (1 000 t)
Skimmed milk (1 000 t)
1
2
3
4
3 Skimmed-milk and buttermilk returned to farms
4 Exports and intra-Community dispatches of milk and cream in bulk 41 Of which Member States
5 Other uses (specify)
6 Differences
Total
>END OF GRAPHIC>
TABLE C Annual production and utilization of milk (all milk) on farms
>START OF GRAPHIC>
Country: Year: A. AVAILABILITIES (1 000 t)
Whole milk
Skimmed milk and buttermilk
1.Cows' milk
1.1.Of which dairy cows' milk
2.Ewes' milk
3.Goats' milk
4.Buffalo milk
1.Returned by dairies
2.Balance of cream deliveries
3.Farm butter and cream productionTotal
Total
B. UTILIZATION (1 000 t)
Whole milk
Skimmed milk or buttermilk
1.Drinking milk
(a)Home consumption
(b)Direct sales
2.Farm butter and cream
3.Farm cheese
4.Other products
5.Feed
6.Delivered to dairies
(a)Milk
(b)Cream (in milk equivalent)
(c)Other products (specify)
7.Difference and losses
1.Drinking milk
2.Farm cheese
3.Feed
4.Delivered to dairiesTotal
Total
C. PRODUCTS OBTAINED (1 000 t)
1.Drinking milk
(a)Home consumption
(b)Direct sales
2.Farm creamof which delivered to dairies
3.Farm butterof which delivered to dairies
4.Farm cheeseof which delivered to dairies
5.Other products (specify)of which delivered to dairies>END OF GRAPHIC>
TABLE D Enterprises (1) by volume of annual milk collection
>START OF GRAPHIC>
Country: Situation on 31 December: Classes by volume (tonnes/year) of collection Number of enterprises Collection in 1 000 t
5 000 and under 5 001 to 20 000 20 001 to 50 000 50 001 to 100 000 100 001 to 300 000 over 300 000 Total
(1)Referred to in Article 2 (1) of Council Directive 96/16/EC.
>END OF GRAPHIC>
TABLE E Collection centres (1) by volume of annual milk collection
>START OF GRAPHIC>
Country: Situation on 31 December: Classes by volume (tonnes/year) of collection Number Collection in 1 000 t
1 000 and under 1 001 to 5 000 over 5 000 Total
(1)Referred to in Article 2 (2) of Council Directive 96/16/EC.
>END OF GRAPHIC>
TABLE F Enterprises by volume of milk treated
>START OF GRAPHIC>
Country: Situation on 31 December: Classes by volume (tonnes/year) of milk treated Number of enterprises Volume in 1 000 t
5 000 and under 5 001 to 20 000 20 001 to 50 000 50 001 to 100 000 100 001 to 300 000 over 300 000 Total
>END OF GRAPHIC>
TABLE G.1 Enterprises by annual production of certain groups of milk products
>
START OF GRAPHIC>
Country: Situation on 31 December: Groups of products: FRESH PRODUCTS (1)
Classes by volume (tonnes/year) of production Number of enterprises Annual production (in 1 000 t)
1 000 and under 1 001 to 10 000 10 001 to 30 000 30 001 to 50 000 50 001 to 100 000 over 100 000 Total
>END OF GRAPHIC>
TABLE G.2 Enterprises by annual production of certain groups of milk products
>
START OF GRAPHIC>
Country: Situation on 31 December: Groups of products: DRINKING MILK (11)
Classes by volume (tonnes/year) of production Number of enterprises Annual production (1 000 t)
1 000 and under 1 001 to 10 000 10 001 to 30 000 30 001 to 100 000 over 100 000 Total
>END OF GRAPHIC>
TABLE G.3 Enterprises by annual production of certain groups of milk products
>
START OF GRAPHIC>
Country: Situation on 31 December: Groups of products: POWDERED DAIRY PRODUCTS (22)
Classes by volume (tonnes/year) of production Number of enterprises Annual production in 1 000 t
1 000 and under 1 001 to 5 000 5 001 to 20 000 over 20 000 Total
>END OF GRAPHIC>
TABLE G.4 Enterprises by annual production of certain groups of milk products
>
START OF GRAPHIC>
Country: Situation on 31 December: Groups of products: BUTTER (23)
Classes by volume (tonnes/year) of production Number of enterprises Annual production in 1 000 t
100 and under 101 to 1 000 1 001 to 5 000 5 001 to 10 000 over 10 000 Total
>END OF GRAPHIC>
TABLE G.5 Enterprises by annual production of certain groups of milk products
>
START OF GRAPHIC>
Country: Situation on 31 December: Groups of products: CHEESE (all types) (24)
Classes by volume (tonnes/year) of production Number of enterprises Annual production in 1 000 t
100 and under 101 to 1 000 1 001 to 4 000 4 001 to 10 000 over 10 000 Total
>END OF GRAPHIC>
EXPLANATORY NOTES
TABLE A
The data concerning this Table refer only to cows' milk, for both collection and the products obtained (mixtures are therefore excluded).
In the case of weekly returns, the data for the weeks running into the next month must be divided and broken down in accordance with the number of working days in each of the two months.
>TABLE>
Products obtained: quantities of processed fresh milk products shall be understood to be available for delivery outside dairies.
TABLE B
To avoid double counting, milk products used within the same dairy for the manufacture of other milk products are not taken into account.
Likewise, the data in this table refer to the concept of the 'national dairy`. Accordingly, all trade in raw materials or products between dairies in the Member State concerned must be excluded from national production figures.
Availability/I and II: collected milk: relates to purchases of all types of whole milk (cows', ewes', goats' and buffalos' milk) and milk products collected directly from agricultural holdings.
Availability/II.6: relates to other milk products (such as cheese, butter and yoghurt) collected from agricultural holdings. These products, irrespective of whether or not they are processed, are intended for input in the dairies' manufacturing process and must be included in part B (Use) of the table.
Availability/III.4: relates to other milk products (such as cheese, butter and yoghurt) imported from other countries. These products, irrespective of whether or not they are intended for input in the dairies' manufacturing process, must be included in part B (Use) of the table.
If they are intended to be placed on the market without processing (excluding packaging or maturing), these products must be entered under heading 16 (in the case of fresh farm products) or 28 (in the case of manufactured farm products).
Availability/III-Imports and intra-Community arrivals: in bulk or in containers of 2 l or more.
Column 1 - Quantities: unless otherwise indicated, the quantities to be recorded refer to the net weight of the raw material/finished product (in 1 000 tonnes).
Quantities of processed fresh milk products shall be understood to be available for delivery outside dairies.
Column 2/B. Use - milk fat content: quantities (in tonnes) of milk fat used to manufacture the product concerned, including possible losses occurring during the manufacturing process.
Column 3/A. Availabilities - milk protein: quantities (in tonnes) of milk protein contained in the cows' milk collected.
Column 3/B. Use - input of whole milk: quantities (in 1 000 tonnes) of whole milk used in the manufacture of the product in question, including possible losses occurring during the manufacturing process.
Column 4/B. Use - input of skimmed milk:
- positive: quantities (in 1 000 tonnes) of skimmed milk used in the manufacture of the product in question, including possible losses occurring during the manufacturing process,
- negative: quantities (in 1 000 tonnes) of skimmed milk recovered during the manufacturing process for the product in question (for example: skimmed milk recovered during the manufacture of butter from whole milk or cream).
Other uses (codes 3-6):
Skimmed milk and buttermilk returned to the farm (3): the skimmed milk and buttermilk returned to farms.
Exports and consignments of milk and cream in bulk (4): exports and Community dispatches of whole milk, skimmed milk and liquid cream in bulk or in containers of 2 l or more by the dairies.
Other uses (5): includes whole and skimmed milk in bulk or in containers of more than 2 l delivered to food industries (e.g. for ice cream) or intended for use as animal feedingstuffs in all forms, except item 3.
Differences (6): relates to the statistical differences.
TABLE C
Agricultural holding: a techno-economic unit under a single management producing agricultural products.
A: Availabilities:
Cows' milk: relates to all cows' milk, excluding milk directly suckled but including that obtained by milking (including colostrum) used for animal feedingstuffs (for example in buckets or by other means).
Dairy cows: cows which are used exclusively or mainly for the production of milk for human consumption and/or processing into dairy products, including cull dairy cows (whether or not fattened between their last lactation and their slaughter).
Whole milk column: relates to the quantities of milk obtained.
Skimmed milk and buttermilk column:
- returned by the dairies (1): see definition Table B/3.
- balance of cream deliveries (2).
B. Use:
Whole milk/home consumption: whole milk consumed by the holder's household (therefore for human consumption only).
Whole milk/direct sales: whole milk for human consumption sold direct to consumers.
Whole milk/farm butter and cream: whole milk (all milks) used to produce farm butter and cream.
Whole milk/farm cheese: whole milk (all milks) used to produce farm cheese.
Whole milk/other products: whole milk (all milks) used for the manufacture of milk products for human consumption (e.g. yoghurt).
Whole milk/animal feedingstuffs: whole milk used on the farm for animal feedingstuffs, in whatever form (as they are or as compound animal feedingstuffs manufactured on the farm).
Whole milk/delivered to dairies:
- includes deliveries:
- of all types of whole milk (from cows, ewes, goats and buffalos) to dairies (within or outside the Member State) and to the agricultural holdings referred to in Article 2 of Directive 96/16/EC;
- other products (specify), in milk equivalent,
- deliveries of cream must be expressed in milk equivalent.
Whole milk/differences and losses:
- refers to the statistical difference and the quantities lost during manufacturing,
- the total for the 'whole milk use column` should be equal to the total available.
Skimmed milk and buttermilk/drinking milk: skimmed milk and buttermilk used on agricultural holdings for human consumption, in particular home consumption on holdings and direct sales to consumers.
Skimmed milk and buttermilk/farm cheese: quantity of skimmed milk and buttermilk used to produce farm cheese.
C. Products obtained:
The quantities reported shall refer to the net weight of the finished product (in 1 000 tonnes).
Drinking milk: home consumption and direct sales.
Farm cream: cream produced on agricultural holdings.
Farm butter: butter produced on agricultural holdings.
Farm cheese: cheese produced on the agricultural holding.
Other products: other products (to be specified) produced on the agricultural holding.
Of which delivered to dairies: relates to deliveries of cream, butter, cheese and other farm products to dairies (within or outside the Member State).
TABLES D AND E
Milk collected: quantities of milk and cream (in milk equivalent) collected direct from farms.
TABLE E
Collection centres: relates only to those enterprises which purchase milk from agricultural holdings and sell it in their own name to dairies. Collection centres which are local units dependent on dairies are thus excluded.
The accounting centres for national accounting of quantities of milk collected in the national territory by an enterprise (dairy) from another Member State must therefore also be included in this table.
TABLE F
Volume: total volume of raw material processed = quantities of whole milk (or whole milk equivalent) used for the manufacture of milk products in the enterprise.
TABLES D, E, F AND G
Enterprise: the enterprise is the smallest combination of legal units that is an organizational unit producing goods or services, which benefits from a certain degree of autonomy in decision-making, especially for the allocation of its current resources. An enterprise carries out one or more activities at one or more locations. An enterprise may be a sole legal unit. (Council Regulation (EEC) No 696/93 of 15 March 1993, OJ No L 76, 30. 3. 1993).
Transmitted data which are subject to statistical confidentiality must be clearly identified as such.
The tables must be completed for all enterprises in operation on 31 December of the reference year. They relate to the dairies' activities, including the activities of dairies taken over by them in the course of the year.
ANNEX III
Work programme for 1997
1. Member States shall forward to the Commission by 1 May 1997, as far as possible:
(a) the data provided for in column 1 and columns 2 and/or 3 of the table below, together with a detailed report describing the methods used to obtain these data;
(b) a report on the regional differences in the protein content of the cows' milk collected;
(c) a report on the relation between the use of protein in the manufacture of milk products and the protein content of milk products.
2. Member States shall at the same time forward a report to the Commission describing the technical facilities for recording the data referred to in paragraph 1, and their proposals for the 1998 programme.
DAIRIES ACTIVITIES Milk protein of cows' milk by group of milk products
>START OF GRAPHIC>
Country: Period: Code
Product
Quantity produced (1) (1000 t) Input (2) Content (3)
1 2 3
1
Fresh products
11
Drinking milk
12
Buttermilk
13
Cream
14
Acidified milk (Yoghurts and other)
15
Drinks with a milk base
16
Other milk products (Milk jelly and others)
2
Manufactured products
21
Concentrated milk
22
Powdered dairy products
221
Cream milk powder
222
Whole milk powder
223
Partly skimmed-milk powder
224
Skimmed-milk powder
225
Buttermilk powder
226
Other powder products
23
Butter and other yellow fat dairy products
231
Butter
232
Rendered butter and butteroil
233
Other yellow fat dairy products
24
Cheese
2411
Cheese from cows' milk only
26
Caseins and caseinates
27
Whey, total
28
Other manufactured products (to specify)
3
Skimmed-milk and buttermilk: Returned to farms
4
Exports and intra-Community dispatches of milk and cream in bulk
41
Of which Member States
5
Other uses (to specify)
6
Differences
Total
(1)Column 1: produced quantities (in 1 000 tonnes) during the reference period (year or month).
Definition: see Annex II/Table B/Column 1.
(2)Column 2: quantities of cows' milk protein (in tonnes) used to manufacture the product concerned, including possible losses occuring during the manufacturing process.
(3)Column 3: quantities of cows' milk protein (in tonnes) contained in the product.
>END OF GRAPHIC>
