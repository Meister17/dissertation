Council Decision
of 5 October 2006
on the establishment of a mutual information mechanism concerning Member States' measures in the areas of asylum and immigration
(2006/688/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 66 thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament [1],
Whereas:
(1) On 4 November 2004 the European Council endorsed a multi-annual programme, known as the Hague Programme, for strengthening the area of freedom, security and justice, which calls for the development of the second phase of a common policy in the field of asylum, migration, visas and borders, which started on 1 May 2004, based, inter alia, on closer practical cooperation between Member States and an improved exchange of information.
(2) The development of common asylum and immigration policies since the entry into force of the Treaty of Amsterdam has resulted in closer interdependency between Member States' policies in these areas, making the need for a more coordinated approach of national policies essential for strengthening the area of freedom, security and justice.
(3) In conclusions adopted at its meeting of 14 April 2005, the Justice and Home Affairs Council called for the establishment of a system of mutual information between those in charge of migration and asylum policy in the Member States, based on the necessity to communicate information on measures considered likely to have a significant impact on several Member States or on the European Union as a whole and allowing for an exchange of views between Member States and the Commission at the request of any one of the Member States or the Commission.
(4) The information mechanism should be based on solidarity, transparency and mutual confidence and should provide a flexible, rapid and non-bureaucratic channel for exchanging information and views on national asylum and immigration measures at European Union level.
(5) For the purposes of the application of this Decision, national asylum and immigration measures which are likely to have a significant impact on several Member States or on the European Union as a whole may comprise policy intentions, long-term programming, draft and adopted legislation, final decisions of the highest courts or tribunals which apply or interpret measures of national law and administrative decisions affecting a significant number of persons.
(6) Communication of the relevant information should take place at the latest when the measures concerned become publicly available. Member States are however encouraged to transmit it as soon as possible.
(7) For reasons of efficiency and accessibility, a web-based network should be an essential element of the information mechanism concerning national measures in the areas of asylum and immigration.
(8) The exchange of information on national measures through a web-based network should be complemented by the possibility of exchanging views on such measures.
(9) The information mechanism established by this Decision should be without prejudice to the right of Member States to request ad-hoc discussions in the Council on national measures at any time, in accordance with the Council's rules of procedure.
(10) Since the objectives of this Decision, namely secure information exchange and consultation between Member States, cannot be sufficiently achieved by the Member States and can therefore, by reason of the effects of this Decision, be better achieved at Community level, the Community may adopt measures, in accordance with the principle of subsidiary as set out in Article 5 of the Treaty. In accordance with the principle of proportionality, as set out in that Article, this Decision does not go beyond what is necessary in order to achieve those objectives.
(11) The United Kingdom and Ireland, in accordance with Article 3 of the Protocol on the position of the United Kingdom and Ireland, annexed to the Treaty on European Union and to the Treaty establishing the European Community, have given notice of their wish to take part in the adoption and application of this Decision.
(12) In accordance with Articles 1 and 2 of the Protocol on the position of Denmark annexed to the Treaty on European Union and to the Treaty establishing the European Community, Denmark does not take part in the adoption of this Decision and is therefore not bound by it or subject to its application,
HAS ADOPTED THIS DECISION:
Article 1
Subject matter and scope
1. This Decision establishes a mechanism for the mutual exchange of information concerning national measures in the areas of asylum and immigration that are likely to have a significant impact on several Member States or on the European Union as a whole.
2. The mechanism referred to in paragraph 1 allows for the preparation of exchanges of views and debates on such measures.
Article 2
Information to be submitted
1. Member States shall communicate to the Commission and the other Member States information on the measures which they intend to take, or have recently taken, in the areas of asylum and immigration, where these measures are publicly available and are likely to have a significant impact on several Member States or on the European Union as a whole.
Such information shall be transmitted as soon as possible and at the latest when it becomes publicly available. This paragraph is subject to any confidentiality and data protection requirements that may apply to a particular measure.
Each Member State shall be responsible for evaluating whether its national measures are likely to have a significant impact on several Member States or on the European Union as a whole.
2. The information pursuant to paragraph 1 shall be communicated through the network referred to in Article 3, using the reporting form annexed to this Decision.
3. The Commission or a Member State may request additional information concerning the information communicated by another Member State through the network. In such a case, the Member State concerned shall provide additional information within one month.
Information on final decisions of the highest Courts which apply or interpret measures of national law shall not be the subject of a request for additional information under this paragraph.
4. The possibility for providing additional information referred to in paragraph 3 may also be used by the Member States to provide information on measures not covered by the obligation referred to in paragraph 1, on their own initiative or upon request of the Commission or another Member State.
Article 3
The network
1. The network for the exchange of information in accordance with this Decision shall be web-based.
2. The Commission shall be responsible for the development and management of the network, including the structure and content of the network and access to it. The network shall include appropriate measures to guarantee the confidentiality of all or part of the information in the network.
3. For the practical set up of the network, the Commission shall make use of the existing technical platform within the Community framework of the trans-European telematic network for the interchange of information between the Member States authorities.
4. A specific functionality of the network shall be provided in order to allow the Commission and the Member States to request from one or more Member States additional information on communicated measures, as indicated in Article 2(3), and other information, as indicated in Article 2(4).
5. Member States shall designate national contact points having access to the network and notify the Commission thereof.
6. When necessary for the development of the network the Commission may conclude agreements with Institutions of the European Community, as well as with bodies governed by public law established under the Treaties establishing the European Communities or established within the framework of the European Union.
The Commission shall inform the Council whenever a request for such access is submitted and when access to such Institutions and/or bodies is granted.
Article 4
Exchanges of views, the general report and discussions at ministerial level
1. The Commission shall, once a year, prepare a general report summarizing the most relevant information transmitted by the Member States. With a view to preparing such a report and identifying issues of common interest, Member States shall be associated with the Commission for this preparatory work, which may include technical meetings throughout the reporting period consisting in an exchange of views with Member States' experts on information submitted under Article 2.
The general report shall be transmitted to the European Parliament and to the Council.
2. Without prejudice to the possibility of holding ad-hoc consultations within the Council, the general report prepared by the Commission shall constitute the basis for a debate on national asylum and immigration policies at ministerial level.
Article 5
Evaluation and Review
The Commission shall evaluate the functioning of the mechanism two years after the entry into force of this Decision and regularly thereafter. If appropriate, the Commission shall propose amendments to it.
Article 6
Entry into force
This Decision shall enter into force on the 20th day following its publication in the Official Journal of the European Union.
Article 7
Addressees
This Decision is addressed to the Member States in accordance with the Treaty establishing the European Community.
Done at Luxembourg, 5 October 2006.
For the Council
The President
K. Rajamäki
[1] Opinion delivered on 3 May 2006. Not yet published in the Official Journal.
--------------------------------------------------
ANNEX
+++++ TIFF +++++
--------------------------------------------------
