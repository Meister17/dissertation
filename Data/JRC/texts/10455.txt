Euro exchange rates [1]
20 April 2006
(2006/C 93/02)
| Currency | Exchange rate |
USD | US dollar | 1,2346 |
JPY | Japanese yen | 145,27 |
DKK | Danish krone | 7,4606 |
GBP | Pound sterling | 0,69240 |
SEK | Swedish krona | 9,2882 |
CHF | Swiss franc | 1,5723 |
ISK | Iceland króna | 97,51 |
NOK | Norwegian krone | 7,7900 |
BGN | Bulgarian lev | 1,9558 |
CYP | Cyprus pound | 0,5761 |
CZK | Czech koruna | 28,460 |
EEK | Estonian kroon | 15,6466 |
HUF | Hungarian forint | 266,23 |
LTL | Lithuanian litas | 3,4528 |
LVL | Latvian lats | 0,6960 |
MTL | Maltese lira | 0,4293 |
PLN | Polish zloty | 3,8945 |
RON | Romanian leu | 3,4814 |
SIT | Slovenian tolar | 239,61 |
SKK | Slovak koruna | 37,235 |
TRY | Turkish lira | 1,6415 |
AUD | Australian dollar | 1,6594 |
CAD | Canadian dollar | 1,4039 |
HKD | Hong Kong dollar | 9,5742 |
NZD | New Zealand dollar | 1,9535 |
SGD | Singapore dollar | 1,9722 |
KRW | South Korean won | 1171,14 |
ZAR | South African rand | 7,3860 |
CNY | Chinese yuan renminbi | 9,8924 |
HRK | Croatian kuna | 7,2975 |
IDR | Indonesian rupiah | 10969,42 |
MYR | Malaysian ringgit | 4,520 |
PHP | Philippine peso | 63,576 |
RUB | Russian rouble | 33,9020 |
THB | Thai baht | 46,608 |
[1] Source: reference exchange rate published by the ECB.
--------------------------------------------------
