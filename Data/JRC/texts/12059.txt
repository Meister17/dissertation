Decision of the EEA Joint Committee
No 26/2006
of 10 March 2006
amending Annex II (Technical regulations, standards, testing and certification) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex II to the Agreement was amended by Decision of the EEA Joint Committee No 144/2005 of 2 December 2005 [1].
(2) Commission Decision 2005/618/EC of 18 August 2005 amending Directive 2002/95/EC of the European Parliament and of the Council for the purpose of establishing the maximum concentration values for certain hazardous substances in electrical and electronic equipment [2] is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
The following shall be added in point 12q (Directive 2002/95/EC of the European Parliament and of the Council) of Chapter XV of Annex II to the Agreement:
", as amended by:
- 32005 D 0618: Commission Decision 2005/618/EC of 18 August 2005 (OJ L 214, 19.8.2005, p. 65)."
Article 2
The texts of Decision 2005/618/EC in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 11 March 2006, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [3].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 10 March 2006.
For the EEA Joint Committee
The President
R. Wright
[1] OJ L 53, 23.2.2006, p. 40.
[2] OJ L 214, 19.8.2005, p. 65.
[3] No constitutional requirements indicated.
--------------------------------------------------
