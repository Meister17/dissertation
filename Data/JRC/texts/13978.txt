Order of the Court
(Fourth Chamber)
of 13 December 2005
in Case C-177/05: Reference for a preliminary ruling from the Juzgado de lo Social Único de Algeciras in María Cristina Guerrero Pecino
v Fondo de Garantía Salarial (Fogasa) [1]
In Case C-177/05: reference for a preliminary ruling under Article 234 EC from the Juzgado de lo Social Único de Algeciras (Spain), made by decision of 30 March 2005, received at the Court on 20 April 2005, in the proceedings between María Cristina Guerrero Pecino and Fondo de Garantía Salarial (Fogasa) — the Court (Fourth Chamber), composed of K. Schiemann, President of the Chamber, N. Colneric (Rapporteur), and E. Juhász, Judges; A. Tizzano, Advocate General; R. Grass, Registrar, made an order on 13 December 2005, the operative part of which is as follows:
[1] OJ C 155 of 25.06.2005.
--------------------------------------------------
