COMMISSION DIRECTIVE 96/18/EC of 19 March 1996 amending various Council Directives on the marketing of seeds and propagating materials (Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 66/401/EEC of 14 June 1966 on the marketing of fodder plant seed (1), as last amended by the Act of Accession of Austria, Finland and Sweden, and in particular Article 21 (a) thereof,
Having regard to Council Directive 69/208/EEC of 30 June 1969 on the marketing of seed of oil and fibre plants (2), as last amended by the Act of Accession of Austria, Finland and Sweden, and in particular Article 20 (a) thereof,
Having regard to Council Directive 70/458/EEC of 29 September 1970 on the marketing of vegetable seed (3), as last amended by the Act of Accession of Austria, Finland and Sweden, and in particular Article 40 (a) thereof,
Whereas, in the light of the development of scientific and technical knowledge, Directives 66/401/EEC, 69/208/EEC and 70/458/EEC should be amended for the reasons set out hereafter;
Whereas current international rules, namely those of the International Seed Testing Association and the Organization for Economic Cooperation and Development, have been revised in respect of maximum weights for seed lots of certain species; whereas that revision has been shown to be suitable for adoption by the Community;
Whereas the maximum weights in the Community rules for seed lots of those species should therefore be revised accordingly;
Whereas the measures provided for in this Directive are in accordance with the opinion of the Standing Committee on Seeds and Propagating Material for Agriculture, Horticulture and Forestry,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Directive 66/401/EEC is hereby amended as follows:
in column 2 of the Table in Annex III '20` is replaced by '25` in the case of
- Lupinus albus
- Lupinus angustifolius
- Lupinus luteus
- Pisum sativum
- Vicia faba,
and
- Vicia sativa.
Article 2
Directive 69/208/EEC is hereby amended as follows:
in column 2 of the Table in Annex III '20` is replaced by '25` in all the cases where it appears and '10` is replaced by '25` in the case of Carthamus tinctorius.
Article 3
Directive 70/458/EEC is hereby amended as follows:
point 1 of Annex III is replaced by the following:
'1. Maximum weight of a seed lot:
>TABLE>
The maximum lot weight shall not be exceeded by more than 5 %.`
Article 4
1. Member States shall bring into force the laws, regulations or administrative provisions necessary to comply with this Directive not later than 1 July 1996. They shall immediately inform the Commission thereof.
When Member States adopt these provisions, these shall contain a reference to this Directive or shall be accompanied by such reference at the time of their official publication. The procedure for such reference shall be adopted by Member States.
2. Member States shall communicate to the Commission the text of the main provisions of national law they adopt in the field covered by this Directive.
Article 5
This Directive shall enter into force on the day after its publication in the Official Journal of the European Communities.
Article 6
This Directive is addressed to the Member States.
Done at Brussels, 19 March 1996.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ No 125, 11. 7. 1966, p. 2298/66.
(2) OJ No L 169, 10. 7. 1969, p. 3.
(3) OJ No L 225, 12. 10. 1970, p. 7.
