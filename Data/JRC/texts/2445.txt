State aid
(Articles 87 to 89 of the Treaty establishing the European Community)
Communication from the Commission, pursuant to Article 88(2) of the EC Treaty, to the other Member States and other interested parties
State aid C 31/2002 (ex N 149/2000) — Transitional arrangements for the electricity market — Belgium
(2005/C 175/06)
(Text with EEA relevance)
By the following letter, dated 27 April 2005, the Commission informed Belgium of its decision to terminate the procedure provided for in Article 88(2) of the EC Treaty.
"The Belgian authorities notified the Transitional arrangements for the electricity market to the Commission under Article 88(3) of the EC Treaty by letter dated 3 March 2000, registered in the Commission on 15 March 2000.
The Commission asked the Belgian authorities for further information about the measure on several occasions. The Belgian authorities replied to these questions, the last time by letter dated 26 February 2002, registered in the Commission on 28 February 2002.
By letter sent to the Belgian authorities on 24 April 2002 the Commission initiated the procedure laid down in Article 88(2) of the EC Treaty in order to examine in more detail the part of the Transitional arrangements for the electricity market dealing with the pensions of electricity industry workers. The Commission approved the two other parts of the arrangements in the same decision. The decision was published in the Official Journal of the European Union on 18 September 2002 [1].
By letter dated 18 February 2005, registered in the Commission on 23 February 2005, the Belgian authorities withdrew their notification of the part of the Transitional arrangements for the electricity market dealing with the pensions of electricity industry workers. They stated that the undertakings would continue to be responsible for funding these workers' pensions.
The Commission would point out that, under Article 8 of Council Regulation (EC) No 659/1999 of 22 March 1999 laying down detailed rules for the application of Article 93 of the EC Treaty [2], the Member State concerned may withdraw its notification in due time before the Commission has taken a decision about the aid. In cases where it has already initiated the formal investigation procedure, the Commission closes that procedure.
Consequently, the Commission has decided to close the formal investigation procedure provided for in Article 88(2) of the EC Treaty in respect of the aid in question, having noted that Belgium has withdrawn the notification."
[1] OJ C 222, 18.9.2002, p. 2.
[2] OJ L 83, 27.3.1999, p. 1.
--------------------------------------------------
