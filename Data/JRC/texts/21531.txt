Council Directive 2002/92/EC
of 3 December 2002
amending Directive 77/388/EEC to extend the facility allowing Member States to apply reduced rates of VAT to certain labour-intensive services
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 93 thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament(1),
Having regard to the opinion of the Economic and Social Committee(2),
Whereas:
(1) Article 28(6) of Council Directive 77/388/EC of 17 May 1977 on the harmonisation of the laws of the Member States relating to turnover taxes - common system of value added tax: uniform basis of assessment(3), allows the reduced rates provided for in the third subparagraph of Article 12(3)(a) also to be applied to the labour-intensive services listed in the categories set out in Annex K to that Directive for a maximum period of three years from 1 January 2000 to 31 December 2002.
(2) Council Decision 2000/185/EC of 28 February 2000 authorising Member States to apply a reduced rate of VAT to certain labour-intensive services in accordance with the procedure provided for in Article 28(6) of Directive 77/388/EEC(4) authorised certain Member States to apply, up to 31 December 2002, a reduced rate of VAT to those labour-intensive services for which they had submitted an application.
(3) Based on the reports to be drawn up by 1 October 2002 by the Member States that have applied such reduced rates, the Commission is required to submit a global evaluation report to the Council and the European Parliament by 31 December 2002, accompanied if necessary by a proposal for a final decision on the rate to be applied to labour-intensive services.
(4) In view of the time needed to produce a thorough global evaluation of such reports to extend the maximum period of application set for this measure in Directive 77/388/EEC.
(5) Directive 77/388/EEC should therefore be amended accordingly,
HAS ADOPTED THIS DIRECTIVE:
Article 1
In the first subparagraph of Article 28(6) of Directive 77/388/EEC the words "three years between 1 January 2000 and 31 December 2002" shall be replaced by the words "four years between 1 January 2000 and 31 December 2003".
Article 2
This Directive shall enter into force on the day of its publication in the Official Journal of the European Communities.
Article 3
This Directive is addressed to the Member States.
Done at Brussels, 3 December 2002.
For the Council
The President
T. Pedersen
(1) Opinion delivered on 20 November 2002 (not yet published in the Official Journal).
(2) Opinion delivered on 24 October 2002 (not yet published in the Official Journal).
(3) OJ L 145, 13.6.1977, p. 1. Directive as last amended by Directive 2002/38/EC (OJ L 128, 15.5.2002, p. 41).
(4) OJ L 59, 4.3.2000, p. 10.
