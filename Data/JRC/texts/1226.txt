Commission Regulation (EC) No 272/2001
of 9 February 2001
amending Regulation (EC) No 2808/2000 opening Community tariff quotas for 2001 for sheep, goats, sheepmeat and goatmeat falling within CN codes 0104 10 30, 0104 10 80, 0104 20 10, 0104 20 90 and 0204 and derogating from Regulation (EC) No 1439/95 laying down detailed rules for the application of Council Regulation (EEC) No 3013/89 as regards the import and export of products in the sheepmeat and goatmeat sector and amending Regulation (EC) No 1439/95
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 2467/98 of 3 November 1998 on the common organisation of the market in sheepmeat and goatmeat(1), as amended by Regulation (EC) No 1669/2000(2), and in particular Article 17(1) thereof,
Having regard to Council Regulation (EC) No 2851/2000 of 22 December 2000 establishing certain concessions in the form of Community tariff quotas for certain agricultural products and providing for an adjustment, as an autonomous and transitional measure, of certain agricultural concessions provided for in the Europe Agreement with the Republic of Poland and repealing Regulation (EC) No 3066/95(3), and in particular Article 1(4) thereof,
Whereas:
(1) Article 1 of Council Regulation (EC) No 2007/2000 of 18 September 2000 introducing exceptional trade measures for countries and territories participating in or linked to the European Union's stabilisation and association process, amending Regulation (EC) No 2820/98, and repealing Regulations (EC) No 1763/1999 and (EC) No 6/2000(4), as amended by Regulation (EC) No 2563/2000(5), establishes access to the Community without quantitative restrictions for products originating in the Republics of Albania, Bosnia and Herzegovina, Croatia, the Former Yugoslav Republic of Macedonia and the Federal Republic of Yugoslavia including Kosovo as defined by UNSC Resolution 1244 of 10 June 1999.
(2) Annex A(a) of Regulation (EC) No 2851/2000 abolishes from 1 January 2001 customs duties applicable on imports of certain agricultural products originating in Poland, in particular those falling under CN code 0104 20 10.
(3) Annex A(b) of Regulation (EC) No 2851/2000 lays down the quantities of certain agricultural products that may be imported from Poland with a total exemption or reduction of customs duty subject to tariff quotas, ceilings or reference quantities from 1 January 2001.
(4) Accordingly it is necessary to amend Commission Regulation (EC) No 2808/2000 of 21 December 2000 establishing Community tariff quotas for 2001 for sheep, goats, sheepmeat and goatmeat falling within CN codes 0104 10 30, 0104 10 80, 0104 20 10, 0104 20 90 and 0204 and derogating from Regulation (EC) No 1439/95 laying down detailed rules for the application of Council Regulation (EEC) No 3013/89 as regards the import and export of products in the sheepmeat and goatmeat sector(6) and to amend Commission Regulation (EC) No 1439/95(7), as last amended by Regulation (EC) No 2534/2000(8), in order to take account of these concessions.
(5) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for sheepmeat and goatmeat,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 1439/95 is amended as follows:
1. Article 2, first subparagraph, is replaced by the following text:"Notwithstanding the conditions laid down in Title II of this Regulation and with the exception of imports with exemption from customs duties and without quantitative restrictions the importation into the Community of any of the products listed under points (a), (c) and (d) of Article 1 of Council Regulation (EC) No 2467/98(9) shall be subject to the submission of an import licence issued by the Member State to any applicant who so requests, irrespective of the place of his establishment in the Community."
2. Article 14(1) is replaced by the following:
"1. Licence applications and licences shall bear in box 8 the name of the country of origin. In the case of products falling within CN codes 0104 10 30, 0104 10 80 and 0104 20 90, licence applications and licences shall bear in boxes 17 and 18 particulars of the net mass and where appropriate the number of animals to be imported.
A licence shall make it compulsory to import the products from the country indicated."
3. Article 14(3) is replaced by the following text:
"3. Import licences issued in respect of the quantities referred to in Part 1 of the Annex to Commission Regulation (EC) No 2808/2000(10) and in subsequent annual tariff quota regulations shall bear in box 24 at least one of the following entries:
- Derecho limitado a 0 [aplicación de la parte 1 del anexo del Reglamento (CE) n° 2808/2000 y de posteriores Reglamentos por los que se establecen contingentes arancelarios anuales]
- Told nedsat til 0 (jf. del 1 i bilaget til forordning (EF) nr. 2808/2000 og efterfølgende forordninger om årlige toldkontingenter)
- Beschränkung des Zollsatzes auf Null (Anwendung von Teil 1 des Anhangs der Verordnung (EG) Nr. 2808/2000 und der späteren jährlichen Verordnungen über die Zollkontingente)
- Μηδενικός δασμός [εφαρμογή του μέρους 1 του παραρτήματος του κανονισμού (ΕΚ) αριθ. 2808/2000 και των μεταγενέστερων κανονισμών για τις δασμολογικές ποσοστώσεις]
- Duty limited to zero (application of Part 1 of the Annex to Regulation (EC) No 2808/2000 and subsequent annual tariff quota regulations)
- Droit de douane nul [application de la partie 1 de l'annexe du règlement (CE) n° 2808/2000 et des règlements ultérieurs sur les contingents tarifaires]
- Dazio limitato a zero [applicazione della parte 1 dell'allegato del regolamento (CE) n. 2808/2000 e dei successivi regolamenti relativi ai contingenti tariffari annuali]
- Invoerrecht beperkt tot 0 (toepassing van deel 1 van de bijlage bij Verordening (EG) nr. 2808/2000 en van de latere verordeningen tot vaststelling van de jaarlijkse tariefcontingenten)
- Direito limitado a zero [aplicação da parte 1 do anexo do Regulamento (CE) n.o 2808/2000 e regulamentos subsequentes relativos aos contingentes pautais anuais]
- Tulli rajoitettu 0 prosenttiin (asetuksen (EY) N:o 2808/2000 liitteessä olevan 1 osan ja sen jälkeen annettujen vuotuisia tariffikiintiötä koskevien asetusten soveltaminen)
- Tull begränsad till noll procent (tillämpning av del 1 i bilagan till förordning (EG) nr 2808/2000 och i senare förordningar om årliga tullkvoter)"
4. Article 14(4) is replaced by the following text:
"4. Import licences issued in respect of the quantities referred to in Part 2 of the Annex to Regulation (EC) No 2808/2000 and in subsequent annual tariff quota regulations shall bear in box 24 at least one of the following entries:
- Derecho limitado a 0 [aplicación de la parte 2 del anexo del Reglamento (CE) n° 2808/2000 y de posteriores Reglamentos por los que se establecen contingentes arancelarios anuales]
- Told nedsat til 0 (jf. del 2 i bilaget i forordning (EF) nr. 2808/2000 og efterfølgende forordninger om årlige toldkontingenter)
- Beschränkung des Zollsatzes auf Null (Anwendung von Teil 2 des Anhangs der Verordnung (EG) Nr. 2808/2000 und der späteren jährlichen Verordnungen über die Zollkontingente)
- Μηδενικός δασμός [εφαρμογή του μέρους 2 του παραρτήματος του κανονισμού (ΕΚ) αριθ. 2808/2000 και των μεταγενέστερων κανονισμών για τις δασμολογικές ποσοστώσεις]
- Duty limited to zero (application of Part 2 of the Annex to Regulation (EC) No 2808/2000 and subsequent annual tariff quota regulations)
- Droit de douane nul [application de la partie 2 de l'annexe du règlement (CE) n° 2808/2000 et des règlements ultérieurs sur les contingents tarifaires]
- Dazio limitato a zero [applicazione della parte 2 dell'allegato del regolamento (CE) n. 2808/2000 e dei successivi regolamenti relativi ai contingenti tariffari annuali]
- Invoerrecht beperkt tot 0 (toepassing van deel 2 van de bijlage bij Verordening (EG) nr. 2808/2000 en van de latere verordeningen tot vaststelling van de jaarlijkse tariefcontingenten)
- Direito limitado a zero [aplicação da parte 2 do anexo do Regulamento (CE) n.o 2808/2000 e regulamentos subsequentes relativos aos contingentes pautais anuais]
- Tulli rajoitettu 0 prosenttiin (asetuksen (EY) N:o 2808/2000 liitteessä olevan 2 osan ja sen jälkeen annettujen vuotuisia tariffikiintiötä koskevien asetusten soveltaminen)
- Tull begränsad till noll procent (tillämpning av del 2 i bilagan till förordning (EG) nr 2808/2000 och i senare förordningar om årliga tullkvoter)".
5. Article 14(5) is deleted.
6. Article 15, second subparagraph, is replaced by the following text:"During each of the first three quarters of each year, such import licences shall be issued within the limits of one quarter of the quantities, expressed in tonnes liveweight and referred to in Part 3 of the Annex, and expressed in tonnes of carcase equivalent and referred to in Part 4 of the Annex to Regulation (EC) No 2808/2000 and in subsequent annual tariff quota regulations."
7. Article 16(1) is replaced by the following text:
"1. The maximum overall quantity for which any one party may apply by lodging one or more licence applications shall be that laid down in Part 3 of the Annex to Regulation (EC) No 2808/2000 and in subsequent annual tariff quota regulations for the quarter in which the licence application(s) concerned is (are) lodged."
8. Article 17(4) is replaced by the following text:
"4. Import licences issued in respect of the quantities referred to in Part 3 of the Annex to Regulation (EC) No 2808/2000 and in subsequent annual tariff quota regulations shall bear in box 24 at least one of the following entries:
- Derecho limitado a 10 % [aplicación de la parte 3 del anexo del Reglamento (CE) n° 2808/2000 y de posteriores Reglamentos por los que se establecen contingentes arancelarios anuales]
- Told nedsat til 10 % (jf. del 3 i bilaget til forordning (EF) nr. 2808/2000 og efterfølgende forordninger om årlige toldkontingenter)
- Beschränkung des Zollsatzes auf 10 % (Anwendung von Teil 3 des Anhangs der Verordnung (EG) Nr. 2808/2000 und der späteren jährlichen Verordnungen über die Zollkontingente)
- Μηδενικός 10 % [εφαρμογή του μέρους 3 του παραρτήματος του κανονισμού (ΕΚ) αριθ. 2808/2000 και των μεταγενέστερων κανονισμών για τις δασμολογικές ποσοστώσεις]
- Duty limited to 10 % (application of Part 3 of the Annex to Regulation (EC) No 2808/2000 and subsequent annual tariff quota regulations)
- Droit de douane 10 % [application de la partie 3 de l'annexe du règlement (CE) n° 2808/2000 et des règlements ultérieurs sur les contingents tarifaires]
- Dazio limitato a 10 % [applicazione della parte 3 dell'allegato del regolamento (CE) n. 2808/2000 e dei successivi regolamenti relativi ai contingenti tariffari annuali]
- Invoerrecht beperkt tot 10 % (toepassing van deel 3 van de bijlage bij Verordening (EG) nr. 2808/2000 en van de latere verordeningen tot vaststelling van de jaarlijkse tariefcontingenten)
- Direito limitado a 10 % [aplicação da parte 3 do anexo do Regulamento (CE) n.o 2808/2000 e regulamentos subsequentes relativos aos contingentes pautais anuais]
- Tulli rajoitettu 10 prosenttiin (asetuksen (EY) N:o 2808/2000 liitteessä olevan 3 osan ja sen jälkeen annettujen vuotuisia tariffikiintiötä koskevien asetusten soveltaminen)
- Tull begränsad till 10 % (tillämpning av del 3 i bilagan till förordning (EG) nr 2808/2000 och i senare förordningar om årliga tullkvoter)".
9. Article 17(5) is replaced by the following text:
"5. Import licences issued in respect of the quantities referred to in Part 4 of the Annex to Regulation (EC) No 2808/2000 and in subsequent annual tariff quota regulations shall bear in box 24 at least one of the following entries:
- Derecho limitado a 0 [aplicación de la parte 4 del anexo del Reglamento (CE) n° 2808/2000 y de posteriores Reglamentos por los que se establecen contingentes arancelarios anuales]
- Told nedsat til 0 (jf. del 4 i bilaget til forordning (EF) nr. 2808/2000 og efterfølgende forordninger om årlige toldkontingenter)
- Beschränkung des Zollsatzes auf Null (Anwendung von Teil 4 des Anhangs der Verordnung (EG) Nr. 2808/2000 und der späteren jährlichen Verordnungen über die Zollkontingente)
- Μηδενικός δασμός [εφαρμογή του μέρους 4 του παραρτήματος του κανονισμού (ΕΚ) αριθ. 2808/2000 και των μεταγενέστερων κανονισμών για τις δασμολογικές ποσοστώσεις]
- Duty limited to zero (application of Part 4 of the Annex to Regulation (EC) No 2808/2000 and subsequent annual tariff quota regulations)
- Droit de douane nul [application de la partie 4 de l'annexe du règlement (CE) n° 2808/2000 et des règlements ultérieurs sur les contingents tarifaires]
- Dazio limitato a zero [applicazione della parte 4 dell'allegato del regolamento (CE) n. 2808/2000 e dei successivi regolamenti relativi ai contingenti tariffari annuali]
- Invoerrecht beperkt tot 0 (toepassing van deel 4 van de bijlage bij Verordening (EG) nr. 2808/2000 en van de latere verordeningen tot vaststelling van de jaarlijkse tariefcontingenten)
- Direito limitado a zero [aplicação da parte 4 do anexo do Regulamento (CE) n.o 2808/2000 e regulamentos subsequentes relativos aos contingentes pautais anuais]
- Tulli rajoitettu 0 prosenttiin (asetuksen (EY) N:o 2808/2000 liitteessä olevan 4 osan ja sen jälkeen annettujen vuotuisia tariffikiintiötä koskevien asetusten soveltaminen)
- Tull begränsad till noll procent (tillämpning av del 4 i bilagan till förordning (EG) nr 2808/2000 och i senare förordningar om årliga tullkvoter)".
Article 2
Regulation (EC) No 2808/2000 is amended as follows:
1. The title is replaced by the following text:
"Commission Regulation (EC) No 2808/2000 of 21 December 2000 opening Community tariff quotas for 2001 for sheep, goats, sheepmeat and goatmeat falling within CN codes 0104 10 30, 0104 10 80, 0104 20 90 and 0204".
2. The text of Article 1 is replaced by the following:
"Article 1
This Regulation opens Community tariff quotas for the sheepmeat and goatmeat sectors for the period 1 January to 31 December 2001."
3. The text of Article 2 is replaced by the following:
"Article 2
The customs duties applicable to imports into the Community of sheep, goats, sheepmeat and goatmeat falling within CN codes 0104 10 30, 0104 10 80, 0104 20 90 and 0204 originating in the countries indicated in the Annex shall be suspended or reduced during the periods, at the levels and within the limits of the tariff quotas laid down in this Regulation."
4. Article 3(2) is replaced by the following:
"2. The quantities of live animals and meat expressed as carcase-weight equivalent, falling within CN codes 0104 10 30, 0104 10 80, 0104 20 90 and 0204 and, for which the customs duty, applicable to imports originating in specific supplying countries, is reduced to zero for the period between 1 January and 31 December 2001, shall be those laid down in Part 2 of the Annex."
5. Article 6 is deleted.
Article 3
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
It shall apply with effect from 1 January 2001.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 9 February 2001.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 312, 20.11.1998, p. 1.
(2) OJ L 193, 29.7.2000, p. 8.
(3) OJ L 332, 28.12.2000, p. 7.
(4) OJ L 240, 23.9.2000, p. 1.
(5) OJ L 295, 23.11.2000, p. 1.
(6) OJ L 326, 22.12.2000, p. 12.
(7) OJ L 143, 27.6.1995, p. 7.
(8) OJ L 291, 18.11.2000, p. 6.
(9) OJ L 312, 20.11.1998, p. 1.
(10) OJ L 326, 22.12.2000, p. 1.
