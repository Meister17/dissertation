Commission Decision
of 25 March 2002
amending Decision 93/198/EEC laying down a model for the animal health conditions and veterinary certification for the import of domestic ovine and caprine animals from third countries and amending Annex E of Council Directive 91/68/EEC laying down the animal health conditions governing intra-Community trade in ovine and caprine animals
(notified under document number C(2002) 1178)
(Text with EEA relevance)
(2002/261/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 72/462/EEC(1) of 12 December 1972 on health and veterinary inspection problems upon importation of bovine, ovine and caprine animals, and swine, fresh meat and meat products from third countries, as last amended by Regulation (EC) No 1452/2001(2) and in particular Article 8 and 11 thereof,
Having regard to Council Directive 91/68/EEC, laying down the animal health conditions governing intra-Community trade in ovine and caprine animals(3), as last amended by Council Directive 2001/10/EC(4) and in particular Article 14 thereof,
Whereas:
(1) Commission Decision 93/198/EEC(5), as last amended by Decision 97/231/EC(6), lays down the animal health conditions and veterinary certification for imports of domestic ovine and caprine animals.
(2) Regulation (EC) No 999/2001 of the European Parliament and the Council of 22 May 2001 laying down rules for the prevention, control and eradication of certain transmissible spongiform encephalopathies(7), as last amended by Commission Regulation (EC) No 1326/2001(8) requires that ovine and caprine animals for breeding to be imported from a third country must satisfy similar conditions as required inside the Community.
(3) Therefore, the conditions laid down in the health certificates for intra-Community trade and imports from third countries of breeding sheep and goats must be amended to reflect these new Community requirements.
(4) It is opportune to update and harmonise with the requirements laid down for other species the certificates for imports of all categories of sheep and goats.
(5) The annexes to Council Directive 91/68/EEC and Decision 93/198/EEC must be amended accordingly.
(6) The measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
Annex I, Parts 1(a) and 1(b), and Annex II, Parts 1(a) and 1(b), to Decision 93/198/EEC are replaced by the corresponding parts in Annex I to this Decision.
Article 2
Model III in Annex E to Directive 91/68/EEC is replaced by Annex II to this Decision.
Article 3
This Decision shall come into effect after 30 days from its publication in the Official Journal of the European Communities.
Article 4
This Decision is addressed to the Member States.
Done at Brussels, 25 March 2002.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 302, 31.12.1972, p. 28.
(2) OJ L 198, 21.7.2001, p. 11.
(3) OJ L 46, 19.2.1991, p. 19.
(4) OJ L 147, 31.5.2001, p. 41.
(5) OJ L 86, 6.4.1993, p. 34.
(6) OJ L 93, 8.4.1997, p. 22.
(7) OJ L 147, 31.5.2001, p. 1.
(8) OJ L 177, 29.6.2001, p. 60.
ANNEX I
"ANNEX I
>PIC FILE= "L_2002091EN.003203.TIF">
>PIC FILE= "L_2002091EN.003301.TIF">
>PIC FILE= "L_2002091EN.003401.TIF">
>PIC FILE= "L_2002091EN.003501.TIF">
>PIC FILE= "L_2002091EN.003601.TIF">
>PIC FILE= "L_2002091EN.003701.TIF">
>PIC FILE= "L_2002091EN.003801.TIF">
ANNEX II
>PIC FILE= "L_2002091EN.003902.TIF">
>PIC FILE= "L_2002091EN.004001.TIF">
>PIC FILE= "L_2002091EN.004101.TIF">
>PIC FILE= "L_2002091EN.004201.TIF">
>PIC FILE= "L_2002091EN.004301.TIF">
>PIC FILE= "L_2002091EN.004401.TIF">
>PIC FILE= "L_2002091EN.004501.TIF">
>PIC FILE= "L_2002091EN.004601.TIF">
>PIC FILE= "L_2002091EN.004701.TIF">"
ANNEX II
"ANNEX E
>PIC FILE= "L_2002091EN.004803.TIF">
>PIC FILE= "L_2002091EN.004901.TIF">
>PIC FILE= "L_2002091EN.005001.TIF">
>PIC FILE= "L_2002091EN.005101.TIF">"
