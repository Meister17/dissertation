COUNCIL DIRECTIVE 94/71/EC of 13 December 1994 amending Directive 92/46/EEC laying down the health rules for the production and placing on the market of raw milk, heat-treated milk and milk-based products
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 92/46/EEC of 16 June 1992 laying down the health rules for the production and placing on the market of raw milk, heat-treated milk and milk-based products (1), and in particular Article 21 thereof,
Having regard to the proposal from the Commission,
Whereas, following a detailed study of certain provisions in the Annexes to Directive 92/46/EEC, it has been found necessary to make certain technical adjustments in order to ensure better application; whereas such amendments concern, in particular, the collection temperatures for raw milk, the rules on equipment in treatment or processing establishments and the manufacture of heat-treated milk and milk-based products,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Directive 92/46/EEC is hereby amended as follows:
1. in Annex A, Chapter I (1):
(i) the following phrase shall be added to point (b) (i):
'except where the milk is intended for the manufacture of cheese with a maturation period of at least two months';
(ii) the following subparagraph shall be added:
'Milk and milk-based products must not come from a surveillance zone established under Directive 85/511/EEC (2)(), unless the milk has undergone, under the supervision of the competent authority, initial pasteurization (71,7 °C for 15 seconds) followed by:
(a) a second heat treatment resulting in a negative reaction to the peroxidase test; or
(b) a drying procedure including heating having an effect equivalent to the heat treatment provided for in (a); or
(c) a second treatment whereby pH is reduced and kept for at least one hour at less than 6.
(*) Council Directive 85/511/EEC of 18 November 1985 introducing Community measures for the control of foot-and-mouth disease (OJ No L 315, 26. 11. 1985, p. 11). Directive as last amended by Commission Decision 92/380/EEC (OJ No L 198, 17. 7. 1992).';
2. in Annex A, Chapter III (A) (2) shall be replaced by the following:
'2. Immediately after milking, the milk must be placed in a clean place which is so equipped as to avoid adverse effects on the quality of the milk. If the milk is not collected within two hours of milking, it must be cooled to a temperature of 8 °C or lower in the case of daily collection or 6 °C if collection is not daily. While the milk is being transported to the treatment and/or processing establishment, the temperature of the cooled milk must not exceed 10 °C unless the milk has been collected within two hours of milking.
For technological reasons concerning the manufacture of certain milk-based products, the competent authorities may authorize derogations from the temperatures laid down in the first subparagraph provided the end product meets the standards set out in Chapter II of Annex C.';
3. in Annex A, Chapter IV:
(a) the title shall be replaced by the following:
'Standards to be met at the time of collection from the production holding for acceptance of raw milk at treatment or processing establishments';
(b) the following introductory sentence shall be inserted before Section A:
'For compliance with these standards, a separate test shall be carried out on a representative sample of the raw milk collected from each production holding.';
(c) in points A.1 and A.2, note (b) shall be replaced by the following:
'(b) Geometric average over a period of three months, with at least one sample a month. Where production levels vary considerably according to season, Member States may be authorized, in accordance with the procedure laid down in Article 31 of this Directive, to apply a different method of calculating results during the low lactation period';
4. in Annex A, Chapter IV, section C shall be replaced by the following:
'C. Raw goat's, sheep's and buffalo's milk must meet the following standards:
1. if it is intended for the manufacture of heat-treated drinking milk or heat-treated milk-based products:
"" ID="1">Plate count at 30 °C (per ml)> ID="2">& le; 3 000 000> ID="3">< 1 500 000 (3)()"">
'
2. if it is intended for the manufacture of products made with raw milk by a process which does not involve any heat treatment:
"" ID="1">Plate count at 30 °C (per ml)> ID="2">& le; 1 000 000> ID="3">< 500 000">
5. In Annex B, Chapter 1, the introductory wording of 3 shall be replaced by the following:
'3. in rooms where the raw materials and the products covered by this Directive are stored, the same conditions as those at 2 (a) to (f), except:';
6. in Annex B, Chapter II (A) (3) shall be replaced by the following:
'3. Working areas, instruments and working equipment must be used only for work on products for which approval has been granted.
However, following authorization by the competent authority, they may be used at the same time or other times for work on other foodstuffs fit for human consumption or other products based on milk for human consumption but intended for use other than human consumption, provided they do not create contamination of the products for which approval has been given.';
7. in Annex B, the title of Chapter III shall be replaced by:
'Special requirements for approval of collection centres';
8. In Annex B, the title of Chapter IV shall be replaced by:
'Special requirements for approval of standardization centres'
9. in Annex B, Chapter V(a) shall be replaced by the following:
'(a) equipment for the mechanical filling and proper automatic sealing of containers which are to be used for packaging heat-treated drinking milk and milk-based products in liquid form, after filling, in so far as such operations are carried out there. This requirement does not apply to churns, tanks and bulk packaging of more than 4 litres.
However, in the case of limited production of liquid milk intended for drinking, the competent authorities may authorize alternative methods using means of filling and sealing which are not automatic, provided that such methods carry equal assurances with regard to hygiene.';
10. in Annex B, Chapter V(b), the reference: 'in the cases provided for in Annex A, Chapters III and IV', shall be replaced by the reference: 'in the cases provided for in Chapters II and IV.';
11. in Annex B, Chapter V(f) shall be replaced by the following:
'(f) 1. in the case of treatment establishments, heat-treatment equipment approved or authorized by the competent authority, fitted with:
- an automatic temperature control,
- a recording thermometer,
- an automatic safety device preventing insufficient heating,
- an adequate safety system preventing the mixture of heat-treated milk with incompletely heated milk,
- an automatic recording device for the safety system referred to in the preceding indent or a procedure for monitoring the system's effectiveness.
However, when approving establishments, the competent authorities may authorize different equipment with equivalent performance guarantees and equal assurances with regard to hygiene.
2. in the case of processing establishments, in so far as such operations are carried out there, equipment and methods for heating, thermization or heat treatment, meeting the hygiene requirements.';
12. in Annex B, Chapter VI (3) shall be replaced by the following:
'3. Equipment, containers and installations which come into contact with milk or milk-based products or other perishable raw materials during production must be cleaned and if necessary disinfected according to a frequency and procedures consistent with the principles referred to in Article 14 (1).';
13. in Annex B, Chapter VI (4) shall be replaced by the following:
'4. The treatment premises must be cleaned according to a frequency and procedures consistent with the principles referred to in Article 14 (1).';
14. in Annex C, in the first sentence of the second subparagraph of Chapter I (A) (2), the word 'cow's' shall be inserted between 'raw' and 'milk';
15. in Annex C, the first indent of Chapter I (A) (3) (a) shall be replaced by the following:
- raw milk, if it is not treated within 36 hours of acceptance, does not exceed immediately before heat treatment a plate count at 30 °C of 300 000 per ml in the case of cow's milk,';
16. in Annex C, the following shall be added to Chapter I (A) (4) (d):
'Pasteurized milk may be produced in the same conditions from raw milk which has undergone only initial thermization.';
17. in Annex C, Chapter I (B) (1) shall be replaced by the following:
'1. The operator or manager of the processing establishment must take all necessary steps to ensure that the raw milk is heat treated or used, in the case of products made with raw milk':
- as soon as possible after acceptance if the milk has not been refrigerated,
- within 36 hours of acceptance if the milk is kept at a temperature not exceeding 6 °C,
- within 48 hours of acceptance if the milk is kept at a temperature not exceeding 4 °C,
- within 72 hours for buffalo's, sheep's and goat's milk.
However, for technological reasons concerning the manufacture of certain milk-based products, the competent authorities may authorize the times and temperatures referred to in the above indents to be exceeded.
They shall inform the Commission of these derogations, and of the technical reasons for them.';
18. in Annex C, Chapter I (B) (3) (a) (i) shall be replaced by the following:
'(i) have been obtained from raw milk which, if it is not treated within 36 hours of acceptance by the establishment, has a plate count at 30 °C prior to thermization which does not exceed 300 000 per ml in the case of cow's milk;';
19. in Annex C, Chapter II(A):
- in the table in paragraph 1, opposite heading 'Salmonella spp.,' in the 'Standard' column, the words 'Absent in 25 g(c)' shall be replaced in both cases where they occur by 'Absent in 1g',
- the last two subparagraphs of point 2 shall be replaced by the following:
'In addition, whenever the standard M is exceeded in the case of cheese made from raw milk and from thermized milk and soft cheese, testing must be carried out for the possible presence of strains of enterotoxinogenic S. aureus or E. coli that are presumed to be pathogenic and also, if necessary, the possible presence of staphylococcal toxins in such products by means of methods to be determined in accordance with the procedure laid down in Article 31 of this Directive. If the abovementioned strains are identified and/or staphylococcal enterotoxins are found, all the batches involved shall be withdrawn from the market. In this case, the competent authority shall be informed of the findings, pursuant to the fifth indent of the second subparagraph of Article 14 (1) of this Directive, and of the action taken to withdraw the suspect batches and the corrective procedures introduced into the production monitoring system.';
20. in Annex C, the first sentence of Chapter II (A) (4) shall be replaced by the following:
'In addition, milk-based products in liquid or gel form which have undergone UHT treatment or sterilization which are intended for conservation at room temperature must meet the following standards after incubation for 15 days at 30 °C:';
21. in Annex C, the following shall be added to Chapter III (3):
'However, in the case of limited production, the competent authorities may authorize non-automatic sealing methods provided that these provide equal assurances with regard to hygiene.';
22. in Annex C, the second subparagraph of Chapter III (4) shall be replaced by the following:
'Sealing must be carried out in the establishment in which the last heat treatment of drinking milk and/or milk-based products in liquid form has been carried out, immediately after filling, by means of sealing devices which ensure that the milk is protected from any adverse effects of external origin on its characteristics. The sealing system must be so designed that once the container has been opened, the evidence that it has been opened remains clear and easy to check.';
23. in Annex C, Chapter III (5) shall be replaced by the following:
'5. The operator or manager of the establishment must ensure for control purposes that, in addition to the information required by Chapter IV, the following information is visibly and legibly displayed on the packaging of the heat-treated milk and milk-based products in liquid form:
- the nature of the heat treatment which the milk has undergone,
- an indication, in code or not, whereby the date of the last heat treatment may be established,
- in the case of pasteurized milk, the temperature at which the product must be stored.
However, these details need not appear on the reusable glass bottles referred to in Article 11 (6) of Directive 79/112/EEC.';
24. in Annex C, the last sentence of Chapter IV (A) (1) shall be replaced by the following:
'However, where small products are individually wrapped and then packaged together or where such small individually wrapped portions are supplied to the final consumer, it will suffice for the health mark to be applied to their collective packaging.';
25. in Annex C, Chapter IV, Section A, (3) (a):
(a) the following paragraph shall be added:
'(iii) or
- above: the name or initial letter or letters of the consigning country in capitals, i.e. for the Community, the letters,
B - DK - D - EL - E - F - IRL - I - L - NL - P - UK,
- in the centre: a reference to where the approval number of the establishment is shown,
- below: one of the following sets of initials:
CEE - EOEF - EWG - EOK - EEC - EEG';
(b) the following sentence shall be added as second subparagraph:
'In the case of the bottles, packaging and containers referred to in Article 11 (4) and (6) of Directive 79/112/EEC, the health mark may indicate only the initials of the consigning country and the approval number of the establishment.';
26. in Annex C, the last sentence of Chapter IV (A) (3) (b) shall be deleted;
27. in Annex C, the following shall be added to Chapter IV (A):
'4. To take account of the disposal of existing packaging, application of the health mark on packaging shall be compulsory only from 1 January 1996. However, the information to be given on the health mark must be shown on the accompanying commercial document provided for in Article 5 (8) and the last subparagraph of Article 7 (A) (9) of this Directive';
28. in Annex C, the following shall be added to Chapter V (7):
'and authorize a tolerance of +2 °C during deliveries to retail establishments'.
Article 2
1. Member States shall bring into force the laws, regulations and administrative provision necessary to comply with this Directive before 1 July 1995. They shall forthwith inform the Commission thereof.
Member States which have opted for a somatic cell content control when raw milk enters the treatment or processing establishment shall have an additional period of 24 months in order to comply with the requirement introduced by Article 1 point 3 (b) of this Directive.
When Member States adopt these measures, they shall contain a reference to this Directive or shall be accompanied by such reference on the occasion of their official publication. The methods of making such reference shall be laid down by Member States.
2. Member States shall communicate to the Commission the text of the main provisions of national law which they adopt in the field governed by this Directive.
Article 3
This Directive shall enter into force on the day of its publication in the Official Journal of the European Communities.
Article 4
This Directive is addressed to the Member States.
Done at Brussels, 13 December 1994.
For the Council
The President
T. BORCHERT
(1) OJ No L 268, 14. 9. 1992, p. 1. Directive as last amended by Commission Decision 94/330/EC (OJ No L 146, 11. 6. 1994, p. 23).(2)() without prejudice to the result of the re-examination to be carried out in accordance with Article 21.
