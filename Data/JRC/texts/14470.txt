Commission Regulation (EC) No 852/2006
of 9 June 2006
amending Regulation (EC) No 793/2006 laying down certain detailed rules for applying Council Regulation (EC) No 247/2006 laying down specific measures for agriculture in the outermost regions of the Union
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 247/2006 of 30 January 2006 laying down specific measures for agriculture in the outermost regions of the Union [1], and in particular Article 30 thereof,
Whereas:
(1) Regulation (EC) No 247/2006 replaces the existing arrangements in the outermost regions, which are laid down by Council Regulations (EC) No 1452/2001 [2], (EC) No 1453/2001 [3] and (EC) No 1454/2001 [4], and repeals those Regulations. Under Article 33 thereof, it is to apply for each Member State concerned as from the date on which the Commission notifies its approval of the overall programme referred to in Article 24(1).
(2) Article 33 of Regulation (EC) No 247/2006 provides that Regulations (EC) No 1452/2001, (EC) No 1453/2001 and (EC) No 1454/2001 are to apply until the date on which the Commission notifies the Member State concerned of its approval of the overall programme referred to in Article 24(1) of Regulation (EC) No 247/2006. It should be specified, therefore, that the measures adopted to implement Regulations (EC) No 1452/2001, (EC) No 1453/2001 and (EC) No 1454/2001 are to apply only until that date.
(3) In this connection, provision should be made for applications lodged under the measures adopted to implement Regulations (EC) No 1452/2001, (EC) No 1453/2001 and (EC) No 1454/2001 and pending on the date of notification of the overall programme referred to in Article 24(1) of Regulation (EC) No 247/2006 to be dealt with under the arrangements introduced by that Regulation and, in particular, in the context of the overall programme referred to in Article 24(1) thereof.
(4) Commission Regulation (EC) No 793/2006 [5] should therefore be amended in order to introduce the transitional measures required to ensure a smooth transition from the arrangements in force for 2005 to the measures introduced by Regulation (EC) No 247/2006.
(5) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Direct Payments,
HAS ADOPTED THIS REGULATION:
Article 1
The following Article 52a is inserted in Regulation (EC) No 793/2006:
"Article 52a
Transitional measures
1. The measures adopted to implement Regulations (EC) No 1452/2001, (EC) No 1453/2002 and (EC) No 1454/2001 which are valid beyond 31 December 2005 shall remain applicable until the date on which the Commission notifies the Member State concerned of its approval of the overall programme referred to in Article 24(1) of Regulation (EC) No 247/2006.
2. This Regulation shall cover applications which are lodged under the measures adopted to implement, in respect of 2006, Regulations (EC) No 1452/2001, (EC) No 1453/2001 and (EC) No 1454/2001 and are pending on the date of notification referred to in paragraph 1 of this Article, or are lodged after that date."
Article 2
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Union.
It shall apply from 1 January 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 9 June 2006.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 42, 14.2.2006, p. 1.
[2] OJ L 198, 21.7.2001, p. 11. Regulation last amended by Regulation (EC) No 1690/2004 (OJ L 305, 1.10.2004, p. 1).
[3] OJ L 198, 21.7.2001, p. 26. Regulation last amended by Regulation (EC) No 1690/2004.
[4] OJ L 198, 21.7.2001, p. 45. Regulation last amended by Regulation (EC) No 1690/2004.
[5] OJ L 145, 31.5.2006, p. 1.
--------------------------------------------------
