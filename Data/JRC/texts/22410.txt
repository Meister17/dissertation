COUNCIL DECISION of 13 March 1997 concerning the conclusion of the Agreement on customs cooperation in the form of an Exchange of Letters between the European Community and the Kingdom of Norway (97/269/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 113 thereof, in conjunction with the first sentence of Article 228 (2) and the first subparagraph of Article 228 (3),
Having regard to the proposal from the Commission,
Whereas the national arrangements on customs cooperation concluded between the Kingdom of Norway and the Republic of Finland, on the one hand, and between the Kingdom of Norway and the Kingdom of Sweden, on the other hand, should, for the matters falling within the Community's jurisdiction, be replaced by a Community system;
Whereas the frontier cooperation agreements help to facilitate trade and the efficient allocation of resources over a limited number of frontier posts situated in outermost regions, in particular for the Republic of Finland and the Kingdom of Sweden; whereas such regions have a number of peculiarities relating to their geography (extremely harsh climatic conditions, extremely long borders, long internal distances, great difficulty in gaining access to certain areas) and to their very low density of population and traffic and these peculiarities are new in the Community context and require special attention if the regions and economic operators concerned are not to be penalized;
Whereas on 25 October 1996 the Council authorized the Commission to negotiate, on behalf of the Community, an agreement on customs cooperation in the form of an Exchange of Letters between the European Community and the Kingdom of Norway;
Whereas the Republic of Finland and the Kingdom of Sweden should assume full responsibility, including financial liability, towards the Community for all acts performed or to be performed on their behalf by the Norwegian customs authorities;
Whereas the Finnish and Swedish customs authorities respectively should conclude with the Norwegian customs authorities an administrative arrangement for the implementation of the Agreement; whereas such arrangements should be notified to the Commission; whereas the Finnish and Swedish customs authorities should be accountable to the Commission for the implementation of the Agreement;
Whereas the Agreement on customs cooperation in the form of an Exchange of Letters negotiated between the European Community and the Kingdom of Norway should be approved,
HAS DECIDED AS FOLLOWS:
Article 1
The Agreement on customs cooperation in the form of an Exchange of Letters between the European Community and the Kingdom of Norway is hereby approved on behalf of the Community.
The text of the Agreement is attached to this Decision.
Article 2
The Republic of Finland and the Kingdom of Sweden shall assume full responsibility, including financial liability, towards the Community for all acts performed or to be performed on their behalf by the Norwegian customs authorities.
Article 3
1. The Finnish and the Swedish customs authorities respectively shall conclude with the Norwegian customs authorities an administrative arrangement for the implementation of the Agreement. These arrangements shall be notified to the Commission of the European Communities.
2. The Finnish and the Swedish customs authorities respectively shall be accountable to the Commission for the implementation of the Agreement. To this end, they shall present a yearly report to the Commission, unless special circumstances were to require additional reports.
Article 4
The Community shall be represented on the Joint Committee set up under Article 7 of the Agreement by the Commission assisted by the representatives of the Member States.
Article 5
The President of the Council is hereby authorized to designate the persons empowered to sign the Agreement in order to bind the Community and to give the notification provided for in Article 11 of the Agreement (1).
Article 6
This Decision shall be published in the Official Journal of the European Communities.
Done at Brussels, 13 March 1997.
For the Council
The President
M. PATIJN
(1) The date of entry into force of the Agreement will be published in the Official Journal of the European Communities by the General Secretariat of the Council.
