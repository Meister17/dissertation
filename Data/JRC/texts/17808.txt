Decision of the EEA Joint Committee
No 83/2004
of 8 June 2004
amending Annex XXI (Statistics) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex XXI to the Agreement was amended by the Agreement on the participation of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Hungary, the Republic of Latvia, the Republic of Lithuania, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic in the European Economic Area signed on 14 October 2003 in Luxembourg [1].
(2) Regulation (EC) No 2257/2003 of the European Parliament and of the Council of 25 November 2003 amending Council Regulation (EC) No 577/98 on the organisation of a labour force sample survey in the Community to adapt the list of survey characteristics [2], is to be incorporated into the Agreement.
(3) This Decision is not to apply to Liechtenstein,
HAS DECIDED AS FOLLOWS:
Article 1
Point 18a (Council Regulation (EC) No 577/98) of Annex XXI to the Agreement shall be amended as follows:
1. The following indent shall be added:
"— 32003 R 2257: Regulation (EC) No 2257/2003 of the European Parliament and of the Council of 25 November 2003 (OJ L 336, 23.12.2003, p. 6)."
2. The introductory phrase to the adaptation and the adaptation shall be replaced by the following:
"The provisions of the Regulation shall, for the purposes of the present Agreement, be read with the following adaptations:
(a) This Regulation shall not apply to Liechtenstein.
(b) The text of the last sentence in Article 4(4) shall be replaced by:
"Norway, Spain, Finland and the United Kingdom may survey the structural variables with reference to a single quarter during a transition period until the end of 2007.""
Article 2
The texts of Regulation (EC) No 2257/2003 in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 9 June 2004, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [3].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 8 June 2004.
For the EEA Joint Committee
The President
S. Gillespie
--------------------------------------------------
[1] OJ L 130, 29.4.2004, p. 3.
[2] OJ L 336, 23.12.2003, p. 6.
[3] No constitutional requirements indicated.
