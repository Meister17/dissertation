COMMISSION DECISION of 12 January 1996 laying down the specimen pedigree certificates for the ova of breeding animals of the bovine species and the particulars to be entered on those certificates (Text with EEA relevance) (96/80/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 77/504/EEC of 25 July 1977 on pure-bred breeding animals of the bovine species (1), as last amended by Accession Treaty of Austria, Finland and Sweden, and in particular Article 5 and the fifth indent of Article 6 (1) thereof,
Whereas, under the fifth indent of Article 6 (1) of the abovementioned Directive, the Commission shall determine, in accordance with the procedure laid down in Article 8 of that Directive, the particulars to be shown on the pedigree certificate that may accompany ova of breeding animals of the bovine species when they enter into intra-Community trade;
Whereas those certificates themselves may be dispensed with provided that the particulars mentioned in this Decision are already present in reference documentation referring to the ova of breeding animals of the bovine species that enter into intra-Community trade;
Whereas the specimen and the particulars to be shown on the pedigree certificate of pure-bred breeding animals of the bovine species are already laid down in Commission Decision 86/404/EEC (2) and those for semen and embryos are already laid down in Commission Decision 88/124/EEC (3); whereas the information on breeding animals of the bovine species should be included in the certificate on ova;
Whereas the measures provided for in this Decision are in accordance with the opinion of the Standing Committee on Zootechnics,
HAS ADOPTED THIS DECISION:
Article 1
The following particulars must be mentioned in the certificate for intra-Community trade of the ova of breeding animals of the bovine species:
- up-to-date data as listed in Article 1 of Decision 86/404/EEC concerning the donor cow as well as its blood group,
- information allowing identification of the ovum, the date of its collection and the names and addresses of the ovum collection centre and of the consignee.
If there is more than one ovum in a single straw, this must be clearly stated and furthermore the ova must all have the same dam.
Article 2
The particulars provided for in Article 1 may be indicated:
1. in the form of a certificate conforming to the specimen in the Annex;
2. in documentation accompanying the bovine ova. In this event the competent authorities must certify that the particulars set out in Article 1 are indicated in those documents, by the following formula:
'The undersigned certifies that these documents, contain the particulars mentioned in Article 1 of Commission Decision 96/80/EC`.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 12 January 1996.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ No L 206, 12. 8. 1977, p. 8.
(2) OJ No L 233, 20. 8. 1986, p. 19.
(3) OJ No L 62, 8. 3. 1988, p. 32.
ANNEX
>REFERENCE TO A FILM>
>REFERENCE TO A FILM>
