Decision 9/2006/GB
of the Governing Board of the European Police college concerning the rules regarding the selection of the candidates for the post of Director
(Adopted by the Governing Board on 24 February 2006)
(2006/C 107/01)
THE GOVERNING BOARD OF CEPOL,
Having regard to Council Decision 2005/681/JHA of 20 September 2005 establishing the European Police College (CEPOL) and repealing Decision 2000/820/JHA [1], and in particular Article 11(1) thereof,
Whereas it is for the Governing Board of the European Police College to lay down rules regarding the selection of the candidates for the post of Director,
HAS ADOPTED THIS DECISION:
Article 1
The selection of candidates for the post of Director of the European Police College (hereinafter referred to as "CEPOL") shall take place in accordance with the provisions of this Decision.
Article 2
1. The post of Director of CEPOL shall be deemed to be vacant for the purpose of the application of the provisions of this Decision:
- as from 9 months before the end of the term of office of the Director;
- upon receipt by the Governing Board of a letter of resignation of the Director;
- upon a decision by the Governing Board on compulsory resignation, retirement in the interest of the service or dismissal;
- as from 9 months before the date on which the Director reaches the age of 65;
- upon the death of the Director.
2. For the vacant post an advertisement shall be drawn up by the Governing Board describing in detail the nature of the post, including remuneration, the duties to be performed, and the qualifications, skills and experience required.
The advertisement shall indicate that applicants must submit their applications in writing, accompanied by a curriculum vitae, to the Chairman of the Governing Board within 60 days of the date of publication in the Official Journal of the European Union of the advertisement, referred to in the first subparagraph, as specified in the advertisement.
Article 3
1. The Governing Board shall ensure that the advertisement referred to in Article 2(2) shall be published in the Official Journal of the European Union.
2. CEPOL shall inform the Contact Points and the European Commission of a vacancy for a post of Director. The Contact Points shall inform the relevant agencies within their Member State of the vacancy. The competent national authorities shall be responsible for ensuring that the vacancy is brought to the attention of the agencies and all the personnel concerned.
3. Both internal and external applications shall be considered.
4. CEPOL shall send an acknowledgement of receipt to the applicants.
Article 4
1. The Governing Board shall set up a Selection Committee (hereinafter referred to as "the Committee") to prepare the Governing Board's decision.
2. Four Member States indicated to that end by drawing lots by the Governing Board and the Commission shall assign a representative to serve as a member of the Committee.
3. The members of the Committee assigned to serve pursuant to paragraphs 2 shall serve as members of the Committee until the selection procedure has been completed.
4. Where there is reason to believe that a member of the Committee has a personal relationship with one of the applicants for the post, he/she shall not participate in the selection process. In such cases, the Member State which presented the member shall propose to the Governing Board that he/she be replaced and shall assign another representative to serve as a member of the Committee.
5. The secretariat of CEPOL shall provide the secretariat of the Committee.
Article 5
1. At the first meeting of the Committee, the members shall indicate one of its members as Chairman.
2. The Committee may request the assistance of one or more assessors for the performance of its tasks. Such request shall be addressed to the Chairman of the Governing Board who shall decide on the matter. Assessors shall not have the status of members of the Committee.
3. The tasks of the Committee shall include:
(a) making an initial assessment of the applicants, taking into account their professional qualifications, skills and experience;
(b) interviewing applicants;
(c) reporting to the Governing Board.
Article 6
1. Where it is deemed necessary, the Governing Board may decide to organise a specific post-related assessment procedure. The Governing Board shall decide on the specific necessities.
In that case, the assessment procedure shall be conducted by the Committee in order to appraise the specific qualifications and expertise of the applicants for the post concerned.
2. The Committee shall interview all applicants whose application is admissible and who meet the requirements set out in the advertisement, with a view to assessing their qualifications, skills and experience required and their ability to perform the duties inherent to the post to be filled. The interviews shall also be used to test the applicants' knowledge of the official languages of the institutions of the European Union.
3. Where considered necessary by the Committee, a second round of interviews for all or some of the applicants may be held.
Article 7
The tests and interviews shall be held in the United Kingdom. Travel expenses and any subsistence and hotel accommodation expenses shall be reimbursed to the applicants, the members of the Committee and the assessors.
Article 8
Once the interviews have been completed, the Committee shall draw up a duly reasoned report on the applications received and the procedure followed by it. The decision of the Committee establishing the report shall be taken by simple majority. The said report shall be forwarded to the Governing Board as soon as possible after the interviews have been held, together with the Curriculum Vitae of the applicants whose application is admissible and who meet the requirements set out in the advertisement.
Article 9
1. On the basis of the report of the Committee and any further information it may wish to obtain from the Committee, the Governing Board shall decide on the appointment of the Director of CEPOL [2].
2. The Governing Board may, if it deems this necessary, hear some or all of the applicants before taking a decision. If it so happens that a member of the Governing Board is also on the list of applicants, he/she shall not be present when the decision of the Governing Board is taken.
Article 10
Members of the Committee, assessors, as well as members of the Governing Board and CEPOL officials involved, shall maintain the strictest confidentiality with regard to the applicants and the results of the selection procedure.
Article 11
Where the term of office of the Director can be renewed in accordance with Article 11(2) of Council Decision 2005/681/JHA, the Governing Board may decide, by derogation from the procedure established in this Decision, to renew the appointment.
The decision to renew the appointment shall be taken at the latest 9 months before the end of the term of office of the Director.
Article 12
This Decision shall enter into force on the day after it has been approved by the Council [3]. It shall be published in the Official Journal of the European Union.
Done at Vienna, 24 February 2006
For the Governing Board
János Fehérváry
Chair of the Governing Board
[1] OJ L 256, 1.10.2005, p. 63.
[2] Article 10(7) of Council Decision 2005/681/JHA of 20 September 2005 reads as follows:"Except where otherwise indicated in this Decision, the Governing Board shall act by a two-thirds majority of its members".
[3] Approved by the Council on 27 April 2006.
--------------------------------------------------
