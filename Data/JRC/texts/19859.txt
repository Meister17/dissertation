Commission Decision
of 21 May 2003
allowing Member States to extend provisional authorisations granted for the new active substances iodosulfuron-methyl-sodium, indoxacarb, S-metolachlor, Spodoptera exigua nuclear polyhedrosis virus, tepraloxydim and dimethenamid-P
(notified under document number C(2003) 1583)
(Text with EEA relevance)
(2003/370/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/414/EEC of 15 July 1991 concerning the placing of plant protection products on the market(1), as last amended by Commission Directive 2003/31/EC(2) and in particular the fourth subparagraph of Article 8(1) thereof,
Whereas:
(1) In accordance with Article 6(2) of Directive 91/414/EEC, in December 1998 Germany received an application from AgrEvo GmbH (now Bayer CropScience) for the inclusion of the active substance iodosulfuron-methyl-sodium in Annex I to Directive 91/414/EEC. Commission Decision 1999/392/EC(3) confirmed that the dossier was complete and could be considered as satisfying, in principle, the data and information requirements of Annex II and Annex III to the Directive.
(2) The authorities of the Netherlands received a similar application in October 1997 from Du Pont de Nemours concerning indoxacarb. This dossier was declared complete by Commission Decision 98/398/EC(4).
(3) Belgium received a similar application in August 1997 from Novartis NV (now Syngenta) concerning S-metolachlor. This dossier was declared complete by Commission Decision 98/512/EC(5).
(4) The Netherlands received a similar application in July 1996 from Biosys concerning Spodoptera exigua nuclear polyhedrosis virus. This dossier was declared complete by Commission Decision 97/865/EC(6).
(5) Spain received a similar application in September 1997 from BASF AG concerning tepraloxydim. This dossier was declared complete by Decision 98/512/EC.
(6) Germany received a similar application in April 1999 from BASF AG concerning dimethenamid-P. This dossier was declared complete by Commission Decision 1999/555/EC(7).
(7) Confirmation of the completeness of the dossiers was necessary in order to allow them to be examined in detail and to allow Member States the possibility of granting provisional authorisations, for periods up to three years, for plant protection products containing the active substances concerned, while complying with the conditions laid down in Article 8(1) of Directive 91/414/EEC and, in particular, the condition relating to the detailed assessment of the active substance and the plant protection product in the light of the requirements laid down by the Directive.
(8) For these active substances, the effects on human health and the environment have been assessed, in accordance with the provisions of Article 6(2) and (4) of Directive 91/414/EEC, for the uses proposed by the respective applicants. The rapporteur Member States submitted the draft assessment reports to the Commission on 30 May 2000 (iodosulfuron-methyl-sodium), 7 February 2000 (indoxacarb), 3 May 1999 (S-metolachlor), 9 November 1999 (Spodoptera exigua NPV), 21 January 2001 (tepraloxydim) and 26 September 2000 (dimethenamid-P).
(9) The examination of the dossiers is still ongoing after submission of the draft assessment reports by the respective rapporteur Member States and it will not be possible to complete the evaluation within the timeframe foreseen by Directive 91/414/EEC.
(10) As the evaluation so far has not identified any reason for immediate concern, Member States should be given the possibility of prolonging provisional authorisations granted for plant protection products containing the active substances concerned for a period of 24 months in accordance with the provisions of Article 8 of Directive 91/414/EEC so as to enable the examination of the dossiers to continue. It is expected that the evaluation and decision-making process with respect to a decision on possible Annex I inclusion for each of the active substances concerned will have been completed within 24 months.
(11) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
Member States may extend provisional authorisations for plant protection products containing iodosulfuron-methyl-sodium, indoxacarb, S-metolachlor, Spodoptera exigua nuclear polyhedrosis virus, tepraloxydim and dimethenamid-P for a period not exceeding 24 months from the date of adoption of this Decision.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 21 May 2003.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 230, 19.8.1991, p. 1.
(2) OJ L 101, 23.4.2003, p. 3.
(3) OJ L 148, 15.6.1999, p. 44.
(4) OJ L 176, 20.6.1998, p. 34.
(5) OJ L 228, 15.8.1998, p. 35.
(6) OJ L 351, 23.12.1997, p. 67.
(7) OJ L 210, 10.8.1999, p. 22.
