COMMISSION REGULATION (EC) No 46/1999 of 8 January 1999 amending Regulation (EEC) No 2454/93 laying down provisions for the implementation of Council Regulation (EEC) No 2913/92 establishing the Community Customs Code
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2913/92 of 12 October 1992 establishing the Community Customs Code (1), as last amended by Regulation (EC) No 82/97 of the European Parliament and of the Council (2), and in particular Article 249 thereof,
Whereas, in accordance with the conclusions of the European Council in Essen on 9 and 10 December 1994, the Community has taken measures to unify the preferential rules of origin in order to facilitate trade; whereas, under those measures, a single list of working or processing, accompanied by introductory notes, should be gradually substituted for the lists of working or processing and their corresponding introductory notes which are currently annexed to the Protocols on rules of origin provided for each of the preferential agreements signed by the Community; whereas, accordingly, it is essential that that single list, accompanied by its introductory notes, should be used also for the generalised system of preferences as established in Commission Regulation (EEC) No 2454/93 (3), as last amended by Regulation (EC) No 1677/98 (4);
Whereas it is necessary to ensure that the definition of related persons in Article 143 of Regulation (EC) No 2454/93 embraces all the provisions of Regulation (EEC) No 2913/92 which refer to that term;
Whereas it is for the decision-making customs authority within the meaning of Article 877(1)(b) of Regulation (EC) No 2454/93 to decide on an application for repayment or remission; whereas Article 890 of that Regulation should be reworded to make this clear;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Customs Code Committee,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EEC) No 2454/93 is hereby amended as follows:
1. The following subparagraph is inserted after the second subparagraph of Article 67(4):
'The provisions of the first subparagraph shall not apply to products falling within Chapters 1 to 24 of the Harmonised System.`;
2. Article 69 is replaced by the following:
'Article 69
For the purposes of Article 67, products which are not wholly obtained in a beneficiary country or in the Community are considered to be sufficiently worked or processed when the conditions set out in the list in Annex 15 are fulfilled.
Those conditions indicate, for all products covered by this Section, the working or processing which must be carried out on non-originating materials used in manufacturing and apply only in relation to such materials.
If a product which has acquired originating status by fulfilling the conditions set out in the list is used in the manufacture of another product, the conditions applicable to the product in which it is incorporated do not apply to it, and no account shall be taken of the non-originating materials which may have been used in its manufacture.`;
3. in the introductory part of Article 70, 'Article 69(1)` is replaced by 'Article 69`;
4. the following Article 70a is inserted:
'Article 70a
1. The unit of qualification for the application of the provisions of this Section shall be the particular product which is considered as the basic unit when determining classification using the nomenclature of the Harmonised System.
When a product composed of a group or assembly of articles is classified under the terms of the Harmonised System in a single heading, the whole constitutes the unit of qualification.
When a consignment consists of a number of identical products classified under the same heading of the Harmonised System, each product must be taken individually when applying the provisions of this Section.
2. Where, under General Rule 5 of the Harmonised System, packaging is included with the product for classification purposes, it shall be included for the purposes of determining origin.`;
5. Article 71(1) is replaced by the following:
'1. By way of derogation from the provisions of Article 69, non-originating materials may be used in the manufacture of a given product, provided that their total value does not exceed 5 % of the ex-works price of the product.
Where, in the list, one or several percentages are given for the maximum value of non-originating materials, such percentages must not be exceeded through the application of the first subparagraph.`;
6. Article 72(3)(a) is replaced by the following:
'(a) the Association of South-East Asian Nations (ASEAN) (Brunei Darussalam, Indonesia, Laos, Malaysia, Philippines, Singapore, Thailand, Vietnam);`
7. in Article 102(1), 'to Annex 14` is replaced by 'of Part B of Annex 14`;
8. the introductory clause of Article 143(1) is replaced by the following:
'For the purposes of Title II, Chapter 3 of the Code and of this Title, persons shall be deemed to be related only if:`;
9. the second paragraph of Article 890 is replaced by the following:
'Repayment or remission shall take place upon presentation of the goods. Where the goods cannot be presented to the implementing customs office, the decision-making customs authority shall grant repayment or remission only where it has information showing unequivocally that the certificate or document produced post-clearance applies to the said goods.`;
10. Annex 14 is amended in accordance with Annex I to this Regulation;
11. Annex 15 is replaced by the text in Annex II to this Regulation.
Article 2
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 8 January 1999.
For the Commission
Mario MONTI
Member of the Commission
(1) OJ L 302, 19.10.1992, p. 1.
(2) OJ L 17, 21.1.1997, p. 1.
(3) OJ L 253, 11.10.1993, p. 1.
(4) OJ L 212, 30.7.1998, p. 18.
ANNEX I
Annex 14 to Regulation (EEC) No 2454/93 is hereby amended as follows:
(a) the following text is inserted at the beginning of the Annex:
'PART A
INTRODUCTORY NOTES TO THE LIST IN ANNEX 15
Note 1:
The list sets out the conditions required for all products to be considered as sufficiently worked or processed within the meaning of Article 69.
Note 2:
2.1. The first two columns in the list describe the product obtained. The first column gives the heading number or chapter number used in the Harmonised System and the second column gives the description of goods used in that system for that heading or chapter. For each entry in the first two columns a rule is specified in column 3 or 4. Where, in some cases, the entry in the first column is preceded by an "ex", this signifies that the rules in column 3 or 4 apply only to the part of that heading as described in column 2.
2.2. Where several heading numbers are grouped together in column 1 or a chapter number is given and the description of products in column 2 is therefore given in general terms, the adjacent rules in column 3 or 4 apply to all products which, under the Harmonised System, are classified in headings of the chapter or in any of the headings grouped together in column 1.
2.3. Where there are different rules in the list applying to different products within a heading, each indent contains the description of that part of the heading covered by the adjacent rules in column 3 or 4.
2.4. Where, for an entry in the first two columns, a rule is specified in both columns 3 and 4, the exporter may opt, as an alternative, to apply either the rule set out in column 3 or that set out in column 4. If no origin rule is given in column 4, the rule set out in column 3 has to be applied.
Note 3:
3.1. The provisions of Article 69, concerning products having acquired originating status which are used in the manufacture of other products, shall apply, regardless of whether this status has been acquired inside the factory where these products are used or in another factory in the beneficiary country or in the Community.
Example:
An engine of heading No 8407, for which the rule states that the value of the non-originating materials which may be incorporated may not exceed 40 % of the ex-works price, is made from "other alloy steel roughly shaped by forging" of heading No ex 7224.
If this forging has been forged in the beneficiary country from a non-originating ingot, it has already acquired originating status by virtue of the rule for heading No ex 7224 in the list. The forging can then count as originating in the value calculation for the engine regardless of whether it was produced in the same factory or in another factory in the beneficiary country. The value of the non-originating ingot is thus not taken into account when adding up the value of the non-originating materials used.
3.2. The rule in the list represents the minimum amount of working or processing required and the carrying out of more working or processing also confers originating status; conversely, the carrying out of less working or processing cannot confer originating status. Thus if a rule provides that non-originating material at a certain level of manufacture may be used, the use of such material at an earlier stage of manufacture is allowed and the use of such material at a later stage is not.
3.3. Without prejudice to Note 3.2 where a rule states that "materials of any heading" may be used, materials of the same heading as the product may also be used, subject, however, to any specific limitations which may also be contained in the rule. However, the expression "manufacture from materials of any heading, including other materials of heading No . . ." means that only materials classified in the same heading as the product and of a different description than that of the product as given in column 2 of the list may be used.
3.4. When a rule in the list specifies that a product may be manufactured from more than one material, this means that one or more of these materials may be used. It does not require that all be used.
Example:
The rule for fabrics of heading Nos 5208 to 5212 provides that natural fibres may be used and that chemical materials, among other materials, may also be used. This does not mean that both have to be used; it is possible to use one or the other, or both.
3.5. Where a rule in the list specifies that a product must be manufactured from a particular material, the condition obviously does not prevent the use of other materials which, because of their inherent nature, cannot satisfy the rule. (See also Note 6.2 below in relation to textiles.)
Example:
The rule for prepared foods of heading No 1904, which specifically excludes the use of cereals and their derivatives does not prevent the use of mineral salts, chemicals and other additives which are not products from cereals.
However, this does not apply to products which, although they cannot be manufactured from the particular materials specified in the list, can be produced from a material of the same nature at an earlier stage of manufacture.
Example:
In the case of an article of apparel of ex Chapter 62 made from non-woven materials, if the use of only non-originating yarn is allowed for this class of article, it is not possible to start from non-woven cloth - even if non-woven cloths cannot normally be made from yarn. In such cases, the starting material would normally be at the stage before yarn - that is the fibre stage.
3.6. Where, in a rule in the list, two percentages are given for the maximum value of non-originating materials that can be used, then these percentages may not be added together. In other words, the maximum value of all the non-originating materials used may never exceed the higher of the percentages given. Furthermore, the individual percentages must not be exceeded, in relation to the particular materials to which they apply.
Note 4:
4.1. The term "natural fibres" is used in the list to refer to fibres other than artificial or synthetic fibres. It is restricted to the stages before spinning takes place, including waste, and, unless otherwise specified, includes fibres that have been carded, combed or otherwise processed but not spun.
4.2. The term "natural fibres" includes horsehair of heading No 0503, silk of heading Nos 5002 and 5003, as well as the wool fibres, fine or coarse animal hair of heading Nos 5101 to 5105, cotton fibres of heading Nos 5201 to 5203, and other vegetable fibres of heading Nos 5301 to 5305.
4.3. The terms "textile pulp", "chemical materials" and "paper-making materials" are used in the list to describe the materials not classified in Chapters 50 to 63, which can be used to manufacture artificial, synthetic or paper fibres or yarns.
4.4. The term "man-made staple fibres" is used in the list to refer to synthetic or artificial filament tow, staple fibres or waste, of heading Nos 5501 to 5507.
Note 5:
5.1. Where for a given product in the list a reference is made to this note, the conditions set out in column 3 shall not be applied to any basic textile materials, used in the manufacture of this product, which, taken together, represent 10 % or less of the total weight of all the basic textile materials used. (See also Notes 5.3 and 5.4 below.)
5.2. However, the tolerance mentioned in Note 5.1 may only be applied to mixed products which have been made from two or more basic textile materials.
The following are the basic textile materials:
- silk,
- wool,
- coarse animal hair,
- fine animal hair,
- horsehair,
- cotton,
- paper-making materials and paper,
- flax,
- true hemp,
- jute and other textile bast fibres,
- sisal and other textile fibres of the genus Agave,
- coconut, abaca, ramie and other vegetable textile fibres,
- synthetic man-made filaments,
- artificial man-made filaments,
- current-conducting filaments,
- synthetic man-made staple fibres of polypropylene,
- synthetic man-made staple fibres of polyester,
- synthetic man-made staple fibres of polyamide,
- synthetic man-made staple fibres of polyacrylonitrile,
- synthetic man-made staple fibres of polyimide,
- synthetic man-made staple fibres of polytetrafluoroethylene,
- synthetic man-made staple fibres of polyphenylene sulphide,
- synthetic man-made staple fibres of polyvinyl chloride,
- other synthetic man-made staple fibres,
- artificial man-made staple fibres of viscose,
- other artificial man-made staple fibres,
- yarn made of polyurethane segmented with flexible segments of polyether, whether or not gimped,
- yarn made of polyurethane segmented with flexible segments of polyester whether or not gimped,
- products of heading No 5605 (metallised yarn) incorporating strip consisting of a core of aluminium foil or of a core of plastic film whether or not coated with aluminium powder, of a width not exceeding 5 mm, sandwiched by means of a transparent or coloured adhesive between two layers of plastic film,
- other products of heading No 5605.
Example:
A yarn of heading No 5205 made from cotton fibres of heading No 5203 and synthetic staple fibres of heading No 5506 is a mixed yarn. Therefore, non-originating synthetic staple fibres which do not satisfy the origin rules (which require manufacture from chemical materials or textile pulp) may be used up to a weight of 10 % of the yarn.
Example:
A woollen fabric of heading No 5112 made from woollen yarn of heading No 5107 and synthetic yarn of staple fibres of heading No 5509 is a mixed fabric. Therefore, synthetic yarn which does not satisfy the origin rules (which require manufacture from chemical materials or textile pulp), or woollen yarn that does not satisfy the origin rules (which require manufacture from natural fibres, not carded or combed or otherwise prepared for spinning), or a combination of the two may be used provided their total weight does not exceed 10 % of the weight of the fabric.
Example:
Tufted textile fabric of heading No 5802 made from cotton yarn of heading No 5205 and cotton fabric of heading No 5210 is only a mixed product if the cotton fabric is itself a mixed fabric made from yarns classified in two separate headings or if the cotton yarns used are themselves mixtures.
Example:
If the tufted textile fabric concerned had been made from cotton yarn of heading No 5205 and synthetic fabric of heading No 5407, then, obviously, the yarns used are two separate basic textile materials and the tufted textile fabric is, accordingly, a mixed product.
5.3. In the case of products incorporating "yarn made of polyurethane segmented with flexible segments of polyether, whether or not gimped" this tolerance is 20 % in respect of this yarn.
5.4. In the case of products incorporating "strip consisting of a core of aluminium foil or of a core of plastic film whether or not coated with aluminium powder, of a width not exceeding 5 mm, sandwiched by means of an adhesive between two layers of plastic film", this tolerance is 30 % in respect of this strip.
Note 6:
6.1. Where, in the list, reference is made to this note, textile materials (with the exception of linings and interlinings), which do not satisfy the rule set out in the list in column 3 for the made-up product concerned may be used, provided that they are classified in a heading other than that of the product and that their value does not exceed 8 % of the ex-works price of the product.
6.2. Without prejudice to Note 6.3, materials which are not classified within Chapters 50 to 63 may be used freely in the manufacture of textile products, whether or not they contain textiles.
Example:
If a rule in the list provides that for a particular textile item (such as trousers), yarn must be used, this does not prevent the use of metal items, such as buttons, because buttons are not classified within Chapters 50 to 63. For the same reason, it does not prevent the use of slide-fasteners even though slide-fasteners normally contain textiles.
6.3. Where a percentage rules applies, the value of materials which are not classified within Chapters 50 to 63 must be taken into account when calculating the value of the non-originating materials incorporated.
Note 7:
7.1. For the purposes of heading Nos ex 2707, 2713 to 2715, ex 2901, ex 2902 and ex 3403, the "specific processes" are the following:
(a) vacuum distillation;
(b) redistillation by a very thorough fractionation process (1);
(c) cracking;
(d) reforming;
(e) extraction by means of selective solvents;
(f) the process comprising all the following operations: processing with concentrated sulphuric acid, oleum or sulphuric anhydride; neutralisation with alkaline agents; decolorisation and purification with naturally-active earth, activated earth, activated charcoal or bauxite;
(g) polymerisation;
(h) alkylation;
(i) isomerisation.
7.2. For the purposes of heading Nos 2710, 2711 and 2712, the "specific processes" are the following:
(a) vacuum distillation;
(b) redistillation by a very thorough fractionation process (1);
(c) cracking;
(d) reforming;
(e) extraction by means of selective solvents;
(f) the process comprising all the following operations: processing with concentrated sulphuric acid, oleum or sulphuric anhydride; neutralisation with alkaline agents; decolorisation and purification with naturally-active earth, activated earth, activated charcoal or bauxite;
(g) polymerisation;
(h) alkylation;
(ij) isomerisation;
(k) in respect of heavy oils falling within heading No ex 2710 only, desulphurisation with hydrogen resulting in a reduction of at least 85 % of the sulphur content of the products processed (ASTM D 1266-59 T method);
(l) in respect of products falling within heading No 2710 only, deparaffining by a process other than filtering;
(m) in respect of heavy oils falling within heading No ex 2710 only, treatment with hydrogen at a pressure of more than 20 bar and a temperature of more than 250 °C with the use of a catalyst, other than to effect desulphurisation, when the hydrogen constitutes an active element in a chemical reaction. The further treatment with hydrogen of lubricating oils of heading No ex 2710 (e.g. hydrofinishing or decolorisation) in order, more especially, to improve colour or stability shall not, however, be deemed to be a specific process;
(n) in respect of fuel oils falling within heading No ex 2710 only, atmospheric distillation, on condition that less than 30 % of these products distils, by volume, including losses, at 300 °C by the ASTM D 86 method;
(o) in respect of heavy oils other than gas oils and fuel oils falling within heading No ex 2710 only, treatment by means of a high-frequency electrical brush discharge.
7.3. For the purposes of heading Nos ex 2707, 2713 to 2715, ex 2901, ex 2902 and ex 3403, simple operations such as cleaning, decanting, desalting, water separation, filtering, colouring, marking, obtaining a sulphur content as a result of mixing products with different sulphur contents, any combination of these operations or like operations do not confer origin.
PART B
INTRODUCTORY NOTES TO THE LISTS IN ANNEXES 19 AND 20
(1) See Additional Explanatory Note 4(b) to Chapter 27 of the Combined Nomenclature.`
(b) in the foreword, the following sentence shall be deleted:
'Except where otherwise specified, these Notes apply to the three preferential regimes`;
(c) in the foreword and in Note 1.1, 'Annexes 15, 19 and 20` is replaced by 'Annexes 19 and 20`;
(d) in the foreword and in Note 2.1, 'in Article 69(1) and 100(1)` is replaced by 'in Article 100(1)`;
(e) in Note 2.5, 'within the meaning of Articles 70 and 101` is replaced by 'within the meaning of Article 101`;
(f) in Note 5, the following reference is deleted:
'(Territories of the West Bank and the Gaza Strip and beneficiary Republics)`;
(g) in Note 6, the following references is deleted:
'Territories of the West Bank and the Gaza Strip and beneficiary Republics`
and
'GSP, Territories of the West Bank and the Gaza Strip and beneficiary Republics`.
ANNEX II
'ANNEX 15
LIST OF WORKING OR PROCESSING REQUIRED TO BE CARRIED OUT ON NON-ORIGINATING MATERIALS IN ORDER THAT THE PRODUCT MANUFACTURED CAN OBTAIN ORIGINATING STATUS (GSP)
>TABLE>
`
