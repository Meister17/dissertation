Commission Regulation (EC) No 1240/2005
of 29 July 2005
amending Regulation (EC) No 1279/98 as regards certain tariff quotas for beef and veal products originating in Romania
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1254/1999 of 17 May 1999 on the common organisation of the market in beef and veal [1], and in particular the first subparagraph of Article 32(1) thereof,
Whereas,
(1) Council Decision 2003/18/EC of 19 December 2002 concerning the conclusion of a Protocol adjusting the trade aspects of the Europe Agreement establishing an association between the European Communities and their Member States, of the one part, and Romania, of the other part, to take account of the outcome of negotiations between the Parties on new mutual agricultural concessions [2] provided for concessions as regards the importation of beef and veal products under the tariff quota opened by this Agreement.
(2) Provisions implementing this tariff quota were adopted by Commission Regulation (EC) No 1279/98 of 19 June 1998 laying down detailed rules for applying the tariff quotas for beef and veal provided for in Council Decisions 2003/286/EC and 2003/18/EC for Bulgaria and Romania [3].
(3) Council and Commission Decision 2005/431/EC of 25 April 2005 on the conclusion of the Additional Protocol to the Europe Agreement establishing an association between the European Communities and their Member States, of the one part, and Romania, of the other part, to take account of the accession of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic to the European Union [4], provides for concessions as regards beef and veal products.
(4) The measures which are necessary to open the concessions concerning beef and veal products should be adopted and Regulation (EC) No 1279/98 be amended accordingly.
(5) Moreover, Article 4(1) of Regulation (EC) No 1279/98 provides that licence applications may only be submitted during the first ten days of each period referred to in Article 2 of that Regulation. In view of the date of entry into force of the Additional Protocol, it is necessary to derogate from this provision for the period from the entry into force of this Regulation until 31 December 2005.
(6) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Beef and Veal,
HAS ADOPTED THIS REGULATION:
Article 1
1. By way of derogation from Article 4(1) of Regulation (EC) No 1279/98, applications for import licences for the period from the entry into force of the present Regulation until 31 December 2005 shall be lodged during the first ten working days following the date of publication of this Regulation in the Official Journal of the European Union but before 13:00, Brussels time, on the 10th working day.
2. Licence applications submitted during the first 10 days of July 2005 in accordance with Article 4(1) of Regulation (EC) No 1279/98 shall be counted as applications under paragraph 1.
3. Annexes I and II of Regulation (EC) No 1279/98 are replaced by the text in the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Union.
It shall apply from 1 August 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 29 July 2005.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 160, 26.6.1999, p. 21. Regulation as last amended by Regulation (EC) No 1782/2003 (OJ L 270, 21.10.2003, p. 1).
[2] OJ L 8, 14.1.2003, p. 18.
[3] OJ L 176, 20.6.1998, p. 12. Regulation as last amended by Regulation (EC) No 1220/2005 (OJ L 199, 29.7.2005, p. 47).
[4] OJ L 155, 17.6.2005, p. 26.
--------------------------------------------------
ANNEX
--------------------------------------------------
