COMMISSION DECISION of 13 August 1997 amending Decisions 97/515/EC, 97/513/EC, 97/516/EC and 97/517/EC concerning protective measures with regard to certain products of animal origin originating in India, Bangladesh and Madagascar (Text with EEA relevance) (97/553/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 90/675/EEC of 10 December 1990 laying down the principles governing the organisation of veterinary checks on products entering the Community from third countries (1), as last amended by Directive 96/43/EC (2), and in particular Article 19 (7) thereof,
Whereas the Commission, in adopting Decisions 97/515/EC (3), 97/513/EC (4), 97/516/EC (5) and 97/517/EC (6), has established measures in order to ensure that possibly hazardous products of animal origin cannot enter the Community; whereas these measures tend to suspend all imports of fishery products from India, Bangladesh and Madagascar, as well as of other products of animal origin from Madagascar;
Whereas these measures include an opportunity for products which have been despatched to the Community before the entry into force of these requirements and presented for importation into the Community before 15 August 1997, to gain entry to the Community on condition that they are systematically submitted to a microbiological examination upon arrival;
Whereas it is necessary to extend this delay, whilst ensuring a high level of consumer protection;
Whereas the measures provided for in this Decision are in conformity with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
In Article 2 of Decisions 97/515/EC, 97/513/EC, 97/516/EC and 97/517/EC, the words 'before 15 August 1997` are replaced by the words 'before 15 September 1997`.
Article 2
The Member States shall alter the measures they apply in trade in order to bring them into line with this Decision. They shall immediately inform the Commission thereof.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 13 August 1997.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ No L 373, 31. 12. 1990, p. 1.
(2) OJ No L 162, 1. 7. 1996, p. 1.
(3) OJ No L 214, 6. 8. 1997, p. 52.
(4) OJ No L 214, 6. 8. 1997, p. 46.
(5) OJ No L 214, 6. 8. 1997, p. 53.
(6) OJ No L 214, 6. 8. 1997, p. 54.
