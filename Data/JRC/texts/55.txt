*****
COUNCIL DIRECTIVE
of 3 October 1989
on the coordination of certain provisions laid down by law, regulation or administrative action in Member States concerning the pursuit of television broadcasting activities
(89/552/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Articles 57 (2) and 66 thereof,
Having regard to the proposal from the Commission (1),
In cooperation with the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas the objectives of the Community as laid down in the Treaty include establishing an even closer union among the peoples of Europe, fostering closer relations between the States belonging to the Community, ensuring the economic and social progress of its countries by common action to eliminate the barriers which divide Europe, encouraging the constant improvement of the living conditions of its peoples as well as ensuring the preservation and strengthening of peace and liberty;
Whereas the Treaty provides for the establishment of a common market, including the abolition, as between Member States, of obstacles to freedom of movement for services and the institution of a system ensuring that competition in the common market is not distorted;
Whereas broadcasts transmitted across frontiers by means of various technologies are one of the ways of pursuing the objectives of the Community; whereas measures should be adopted to permit and ensure the transition from national markets to a common programme production and distribution market and to establish conditions of fair competition without prejudice to the public interest role to be discharged by the television broadcasting services;
Whereas the Council of Europe has adopted the European Convention on Transfrontier Television;
Whereas the Treaty provides for the issuing of directives for the coordination of provisions to facilitate the taking up of activities as self-employed persons;
Whereas television broadcasting constitutes, in normal circumstances, a service within the meaning of the Treaty;
Whereas the Treaty provides for free movement of all services normally provided against payment, without exclusion on grounds of their cultural or other content and without restriction of nationals of Member States established in a Community country other than that of the person for whom the services are intended;
Whereas this right as applied to the broadcasting and distribution of television services is also a specific manifestation in Community law of a more general principle, namely the freedom of expression as enshrined in Article 10 (1) of the Convention for the Protection of Human Rights and Fundamental Freedoms ratified by all Member States; whereas for this reason the issuing of directives on the broadcasting and distribution of television programmes must ensure their free movement in the light of the said Article and subject only to the limits set by paragraph 2 of that Article and by Article 56 (1) of the Treaty;
Whereas the laws, regulations and administrative measures in Member States concerning the pursuit of activities as television broadcasters and cable operators contain disparities, some of which may impede the free movement of broadcasts within the Community and may distort competition within the common market;
Whereas all such restrictions on freedom to provide broadcasting services within the Community must be abolished under the Treaty;
Whereas such abolition must go hand in hand with coordination of the applicable laws; whereas this coordination must be aimed at facilitating the pursuit of the professional activities concerned and, more generally, the free movement of information and ideas within the Community;
Whereas it is consequently necessary and sufficient that all broadcasts comply with the law of Member State from which they emanate;
Whereas this Directive lays down the minimum rules needed to guarantee freedom of transmission in broadcasting; whereas, therefore, it does not affect the responsibility of the Member States and their authorities with regard to the organization - including the systems of licensing, administrative authorization or taxation - financing and the content of programmes; whereas the independence of cultural developments in the Member States and the preservation of cultural diversity in the Community therefore remain unaffected;
Whereas it is necessary, in the common market, that all broadcasts emanating from and intended for reception within the Community and in particular those intended for reception in another Member State, should respect the law of the originating Member State applicable to broadcasts intended for reception by the public in that Member State and the provisions of this Directive;
Whereas the requirement that the originating Member State should verify that broadcasts comply with national law as coordinated by this Directive is sufficient under Community law to ensure free movement of broadcasts without secondary control on the same grounds in the receiving Member States; whereas, however, the receiving Member State may, exceptionally and under specific conditions provisionally suspend the retransmission of televised broadcasts;
Whereas it is essential for the Member States to ensure the prevention of any acts which may prove detrimental to freedom of movement and trade in television programmes or which may promote the creation of dominant positions which would lead to restrictions on pluralism and freedom of televised information and of the information sector as a whole;
Whereas this Directive, being confined specifically to television broadcasting rules, is without prejudice to existing or future Community acts of harmonization, in particular to satisfy mandatory requirements concerning the protection of consumers and the fairness of commercial transactions and competition;
Whereas co-ordination is nevertheless needed to make it easier for persons and industries producing programmes having a cultural objective to take up and pursue their activities;
Whereas minimum requirements in respect of all public or private Community television programmes for European audio-visual productions have been a means of promoting production, independent production and distribution in the abovementioned industries and are complementary to other instruments which are already or will be proposed to favour the same objective;
Whereas it is therefore necessary to promote markets of sufficient size for television productions in the Member States to recover necessary investments not only by establishing common rules opening up national markets but also by envisaging for European productions where practicable and by appropriate means a majority proportion in television programmes of all Member States; whereas, in order to allow the monitoring of the application of these rules and the pursuit of the objectives, Member States will provide the Commission with a report on the application of the proportions reserved for European works and independent productions in this Directive; whereas for the calculation of such proportions account should be taken of the specific situation of the Hellenic Republic and the Portuguese Republic; whereas the Commission must inform the other Member States of these reports accompanied, where appropriate by an opinion taking account of, in particular, progress achieved in relation to previous years, the share of first broadcasts in the programming, the particular circumstances of new television broadcasters and the specific situation of countries with a low audio-visual production capacity or restricted language area;
Whereas for these purposes 'European works' should be defined without prejudice to the possibility of Member States laying down a more detailed definition as regards television broadcasters under their jurisdiction in accordance with Article 3 (1) in compliance with Community law and account being taken of the objectives of this Directive; Whereas it is important to seek appropriate instruments and procedures in accordance with Community law in order to promote the implementation of these objectives with a view to adopting suitable measures to encourage the activity and development of European audio-visual production and distribution, particularly in countries with a low production capacity or restricted language area;
Whereas national support schemes for the development of European production may be applied in so far as they comply with Community law;
Whereas a commitment, where practicable, to a certain proportion of broadcasts for independent productions, created by producers who are independent of broadcasters, will stimulate new sources of television production, especially the creation of small and medium-sized enterprises; whereas it will offer new opportunities and outlets to the marketing of creative talents of employment of cultural professions and employees in the cultural field; whereas the definition of the concept of independent producer by the Member States should take account of that objective by giving due consideration to small and medium-sized producers and making it possible to authorize financial participation by the coproduction subsidiaries of television organizations;
Whereas measures are necessary for Member States to ensure that a certain period elapses between the first cinema showing of a work and the first television showing;
Whereas in order to allow for an active policy in favour of a specific language, Member States remain free to lay down more detailed or stricter rules in particular on the basis of language criteria, as long as these rules are in conformity with Community law, and in particular are not applicable to the retransmission of broadcasts originating in other Member States;
Whereas in order to ensure that the interests of consumers as television viewers are fully and properly protected, it is essential for television advertising to be subject to a certain number of minimum rules and standards and that the Member States must maintain the right to set more detailed or stricter rules and in certain circumstances to lay down different conditions for television broadcasters under their jurisdiction;
Whereas Member States, with due regard to Community law and in relation to broadcasts intended solely for the national territory which may not be received, directly or indirectly, in one or more Member States, must be able to lay down different conditions for the insertion of advertising and different limits for the volume of advertising in order to facilitate these particular broadcasts;
Whereas it is necessary to prohibit all television advertising promoting cigarettes and other tobacco products including indirect forms of advertising which, whilst not directly mentioning the tobacco product, seek to circumvent the ban on advertising by using brand names, symbols or other distinctive features of tobacco products or of undertakings whose known or main activities include the production or sale of such products;
Whereas it is equally necessary to prohibit all television advertising for medicinal products and medical treatment available only on prescription in the Member State within whose jurisdiction the broadcaster falls and to introduce strict criteria relating to the television advertising of alcoholic products;
Whereas in view of the growing importance of sponsorship in the financing of programmes, appropriate rules should be laid down;
Whereas it is, furthermore, necessary to introduce rules to protect the physical, mental and moral development of minors in programmes and in television advertising;
Whereas although television broadcasters are normally bound to ensure that programmes present facts and events fairly, it is nevertheless important that they should be subject to specific obligations with respect to the right of reply or equivalent remedies so that any person whose legitimate interests have been damaged by an assertion made in the course of a broadcast television programme may effectively exercise such right or remedy.
HAS ADOPTED THIS DIRECTIVE:
CHAPTER I
Definitions
Article 1
For the purpose of this Directive:
(a) 'television broadcasting' means the initial transmission by wire or over the air, including that by satellite, in unencoded or encoded form, of television programmes intended for reception by the public. It includes the communication of programmes between undertakings with a view to their being relayed to the public. It does not include communication services providing items of information or other messages on individual demand such as telecopying, electronic data banks and other similar services;
(b) 'television advertising' means any form of announcement broadcast in return for payment or for similar consideration by a public or private undertaking in connection with a trade, business, craft or profession in order to promote the supply of goods or services, including immovable property, or rights and obligations, in return for payment.
(1) OJ No C 179, 17. 7. 1986, p. 4.
(2) OJ No C 49, 22. 2. 1988, p. 53, and OJ No C 158, 26. 6. 1989.
(3) OJ No C 232, 31. 8. 1987, p. 29.
Except for the purposes of Article 18, this does not include direct offers to the public for the sale, purchase or rental of products or for the provision of services in return for payment;
(c) 'surreptitious advertising' means the representation in words or pictures of goods, services, the name, the trade mark or the activities of a producer of goods or a provider of services in programmes when such representation is intended by the broadcaster to serve advertising and might mislead the public as to its nature. Such representation is considered to be intentional in particular if it is done in return for payment or for similar consideration;
(d) 'sponsorship' means any contribution made by a public or private undertaking not engaged in television broadcasting activities or in the production of audio-visual works, to the financing of television programmes with a view to promoting its name, its trade mark, its image, its activities or its products.
CHAPTER II
General provisions
Article 2
1. Each Member State shall ensure that all television broadcasts transmitted
- by broadcasters under its jurisdiction, or
- by broadcasters who, while not being under the jurisdiction of any Member State, make use of a frequency or a satellite capacity granted by, or a satellite up-link situated in, that Member State,
comply with the law applicable to broadcasts intended for the public in that Member State.
2. Member States shall ensure freedom of reception and shall not restrict retransmission on their territory of television broadcasts from other Member States for reasons which fall within the fields coordinated by this Directive. Member States may provisonally suspend retransmissions of television broadcasts if the following conditions are fulfilled:
(a) a television broadcast coming from another Member State manifestly, seriously and gravely infringes Article 22;
(b) during the previous 12 months, the broadcaster has infringed the same provision on at least two prior occasions;
(c) the Member State concerned has notified the broadcaster and the Commission in writing of the alleged infringements and of its intention to restrict retransmission should any such infringement occur again;
(d) consultations with the transmitting State and the Commission have not produced an amicable settlement within 15 days of the notification provided for in point (c), and the alleged infringement persists.
The Commission shall ensure that the suspension is compatible with Community law. It may ask the Member State concerned to put an end to a suspension which is contrary to Community law, as a matter of urgency. This provision is without prejudice to the application of any procedure, remedy or sanction to the infringements in question in the Member State which has jurisdiction over the broadcaster concerned.
3. This Directive shall not apply to broadcasts intended exclusively for reception in States other than Member States, and which are not received directly or indirectly in one or more Member States.
Article 3
1. Member States shall remain free to require television broadcasters under their jurisdiction to lay down more detailed or stricter rules in the areas covered by this Directive.
2. Member States shall, by appropriate means, ensure, within the framework of their legislation, that television broadcasters under their jurisdiction comply with the provisions of this Directive.
CHAPTER III
Promotion of distribution and production of television programmes
Article 4
1. Member States shall ensure where practicable and by appropriate means, that broadcasters reserve for European works, within the meaning of Article 6, a majority proportion of their transmission time, excluding the time appointed to news, sports events, games, advertising and teletext services. This proportion, having regard to the broadcaster's informational, educational, cultural and entertainment responsibilities to its viewing public, should be achieved progressively, on the basis of suitable criteria.
2. Where the proportion laid down in paragraph 1 cannot be attained, it must not be lower than the average for 1988 in the Member State concerned.
However, in respect of the Hellenic Republic and the Portuguese Republic, the year 1988 shall be replaced by the year 1990. 3. From 3 October 1991, the Member States shall provide the Commission every two years with a report on the application of this Article and Article 5.
That report shall in particular include a statistical statement on the achievement of the proportion referred to in this Article and Article 5 for each of the television programmes falling within the jurisdiction of the Member State concerned, the reasons, in each case, for the failure to attain that proportion and the measures adopted or envisaged in order to achieve it.
The Commission shall inform the other Member States and the European Parliament of the reports, which shall be accompanied, where appropriate, by an opinion. The Commission shall ensure the application of this Article and Article 5 in accordance with the provisions of the Treaty. The Commission may take account in its opinion, in particular, of progress achieved in relation to previous years, the share of first broadcast works in the programming, the particular circumstances of new television broadcasters and the specific situation of countries with a low audiovisual production capacity or restricted language area.
4. The Council shall review the implementation of this Article on the basis of a report from the Commission accompanied by any proposals for revision that it may deem appropriate no later than the end of the fifth year from the adoption of the Directive.
To that end, the Commission report shall, on the basis of the information provided by Member States under paragraph 3, take account in particular of developments in the Community market and of the international context.
Article 5
Member States shall ensure, where practicable and by appropriate means, that broadcasters reserve at least 10 % of their transmission time, excluding the time appointed to news, sports events, games, advertising and teletext services, or alternately, at the discretion of the Member State, at least 10 % of their programming budget, for European works created by producers who are independent of broadcasters. This proportion, having regard to broadcasters' informational, educational, cultural and entertainment responsibilities to its viewing public, should be achieved progressively, on the basis of suitable criteria; it must be achieved by earmarking an adequate proportion for recent works, that is to say works transmitted within five years of their production.
Article 6
1. Within the meaning of this chapter, 'European works' means the following:
(a) works originating from Member States of the Community and, as regards television broadcasters falling within the jurisdiction of the Federal Republic of Germany, works from German territories where the Basic Law does not apply and fulfilling the conditions of paragraph 2;
(b) works originating from European third States party to the European Convention on Transfrontier Television of the Council of Europe and fulfilling the conditions of paragraph 2;
(c) works originating from other European third countries and fulfilling the conditions of paragraph 3.
2. The works referred to in paragraph 1 (a) and (b) are works mainly made with authors and workers residing in one or more States referred to in paragraph 1 (a) and (b) provided that they comply with one of the following three conditions:
(a) they are made by one or more producers established in one or more of those States; or
(b) production of the works is supervised and actually controlled by one or more producers established in one or more of those States; or
(c) the contribution of co-producers of those States to the total co-production costs is preponderant and the co-production is not controlled by one or more producers established outside those States.
3. The works referred to in paragraph 1 (c) are works made exclusively or in co-production with producers established in one or more Member State by producers established in one or more European third countries with which the Community will conclude agreements in accordance with the procedures of the Treaty, if those works are mainly made with authors and workers residing in one or more European States.
4. Works which are not European works within the meaning of paragraph 1, but made mainly with authors and workers residing in one or more Member States, shall be considered to be European works to an extent corresponding to the proportion of the contribution of Community co-producers to the total production costs.
Article 7
Member States shall ensure that the television broadcasters under their jurisdiction do not broadcast any cinematographic work, unless otherwise agreed between its rights holders and the broadcaster, until two years have elapsed since the work was first shown in cinemas in one of the Member States of the Community; in the case of cinematographic works co-produced by the broadcaster, this period shall be one year. Article 8
Where they consider it necessary for purposes of language policy, the Member States, whilst observing Community law, may as regards some or all programmes of television broadcasters under their jurisdiction, lay down more detailed or stricter rules in particular on the basis of language criteria.
Article 9
This chapter shall not apply to local television broadcasts not forming part of a national network.
CHAPTER IV
Television advertising and sponsorship
Article 10
1. Television advertising shall be readily recognizable as such and kept quite separate from other parts of the programme service by optical and/or acoustic means.
2. Isolated advertising spots shall remain the exception.
3. Advertising shall not use subliminal techniques.
4. Surreptitious advertising shall be prohibited.
Article 11
1. Advertisements shall be inserted between programmes. Provided the conditions contained in paragraphs 2 to 5 of this Article are fulfilled, advertisements may also be inserted during programmes in such a way that the integrity and value of the programme, taking into account natural breaks in and the duration and nature of the programme, and the rights of the rights holders are not prejudiced.
2. In programmes consisting of autonomous parts, or in sports programmes and similarly structured events and performances comprising intervals, advertisements shall only be inserted between the parts or in the intervals.
3. The transmission of audiovisual works such as feature films and films made for television (excluding series, serials, light entertainment programmes and documentaries), provided their programmed duration is more than 45 minutes, may be interrupted once for each complete period of 45 minutes. A further interruption is allowed if their programmed duration is at least 20 minutes longer than two or more complete periods of 45 minutes.
4. Where programmes, other than those covered by paragraph 2, are interrupted by advertisements, a period of at least 20 minutes should elapse between each successive advertising break within the programme.
5. Advertisements shall not be inserted in any broadcast of a religious service. News and current affairs programmes, documentaries, religious programmes, and children's programmes, when their programmed duration is less than 30 minutes shall not be interrupted by advertisements. If their programmed duration is of 30 minutes or longer, the provisions of the previous paragraphs shall apply.
Article 12
Television advertising shall not:
(a) prejudice respect for human dignity:
(b) include any discrimination on grounds of race, sex or nationality;
(c) be offensive to religious or political beliefs;
(d) encourage behaviour prejudicial to health or to safety;
(e) encourage behaviour prejudicial to the protection of the environment.
Article 13
All forms of television advertising for cigarettes and other tobacco products shall be prohibited.
Article 14
Television advertising for medicinal products and medical treatment available only on prescription in the Member State within whose jurisdiction the broadcaster falls shall be prohibited.
Article 15
Television advertising for alcoholic beverages shall comply with the following criteria:
(a) it may not be aimed specifically at minors or, in particular, depict minors consuming these beverages;
(b) it shall not link the consumption of alcohol to enhanced physical performance or to driving;
(c) it shall not create the impression that the consumption of alcohol contributes towards social or sexual success;
(d) it shall not claim that alcohol has therapeutic qualities or that it is a stimulant, a sedative or a means of resolving personal conflicts;
(e) it shall not encourage immoderate consumption of alcohol or present abstinence or moderation in a negative light;
(f) it shall not place emphasis on high alcoholic content as being a positive quality of the beverages.
Article 16
Television advertising shall not cause moral or physical detriment to minors, and shall therefore comply with the following criteria for their protection: (a) it shall not directly exhort minors to buy a product or a service by exploiting their inexperience or credulity;
(b) it shall not directly encourage minors to persuade their parents or others to purchase the goods or services being advertised;
(c) it shall not exploit the special trust minors place in parents, teachers or other persons;
(d) it shall not unreasonably show minors in dangerous situations.
Article 17
1. Sponsored television programmes shall meet the following requirements:
(a) the content and scheduling of sponsored programmes may in no circumstances be influenced by the sponsor in such a way as to affect the responsibility and editorial independence of the broadcaster in respect of programmes;
(b) they must be clearly identified as such by the name and/or logo of the sponsor at the beginning and/or the end of the programmes;
(c) they must not encourage the purchase or rental of the products or services of the sponsor or a third party, in particular by making special promotional references to those products or services.
2. Television programmes may not be sponsored by natural or legal persons whose principal activity is the manufacture or sale of products, or the provision of services, the advertising of which is prohibited by Article 13 or 14.
3. News and current affairs programmes may not be sponsored.
Article 18
1. The amount of advertising shall not exceed 15 % of the daily transmission time. However, this percentage may be increased to 20 % to include forms of advertisements such as direct offers to the public for the sale, purchase or rental of products or for the provision of services, provided the amount of spot advertising does not exceed 15 %.
2. The amount of spot advertising within a given one-hour period shall not exceed 20 %.
3. Without prejudice to the provisions of paragraph 1, forms of advertisements such as direct offers to the public for the sale, purchase or rental of products or for the provision of services shall not exceed one hour per day.
Article 19
Member States may lay down stricter rules than those in Article 18 for programming time and the procedures for television broadcasting for television broadcasters under their jurisdiction, so as to reconcile demand for televised advertising with the public interest, taking account in particular of:
(a) the role of television in providing information, education, culture and entertainment;
(b) the protection of pluralism of information and of the media.
Article 20
Without prejudice to Article 3, Member States may, with due regard for Community law, lay down conditions other than those laid down in Article 11 (2) to (5) and in Article 18 in respect of broadcasts intended solely for the national territory which may not be received, directly or indirectly, in one or more other Member States.
Article 21
Member States shall, within the framework of their laws, ensure that in the case of television broadcasts that do not comply with the provisions of this chapter, appropriate measures are applied to secure compliance with these provisions.
CHAPTER V
Protection of minors
Article 22
Member States shall take appropriate measures to ensure that television broadcasts by broadcasters under their jurisdiction do not include programmes which might seriously impair the physical, mental or moral development of minors, in particular those that involve pornography or gratuitous violence. This provision shall extend to other programmes which are likely to impair the physical, mental or moral development of minors, except where it is ensured, by selecting the time of the broadcast or by any technical measure, that minors in the area of transmission will not normally hear or see such broadcasts.
Member States shall also ensure that broadcasts do not contain any incitement to hatred on grounds of race, sex, religion or nationality.
CHAPTER VI
Right of reply
Article 23
1. Without prejudice to other provisions adopted by the Member States under civil, administrative or criminal law, any natural or legal person, regardless of nationality, whose legitimate interests, in particular reputation and good name, have been damaged by an assertion of incorrect facts in a television programme must have a right of reply or equivalent remedies.
2. A right of reply or equivalent remedies shall exist in relation to all broadcasters under the jurisdiction of a Member State.
3. Member States shall adopt the measures needed to establish the right of reply or the equivalent remedies and shall determine the procedure to be followed for the exercise thereof. In particular, they shall ensure that a sufficient time span is allowed and that the procedures are such that the right or equivalent remedies can be exercised appropriately by natural or legal persons resident or established in other Member States.
4. An application for exercise of the right of reply or the equivalent remedies may be rejected if such a reply is not justified according to the conditions laid down in paragraph 1, would involve a punishable act, would render the broadcaster liable to civil law proceedings or would transgress standards of public decency.
5. Provision shall be made for procedures whereby disputes as to the exercise of the right of reply or the equivalent remedies can be subject to judicial review.
CHAPTER VII
Final provisions
Article 24
In fields which this Directive does not coordinate, it shall not affect the rights and obligations of Member States resulting from existing conventions dealing with telecommunications or broadcasting.
Article 25
1. Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive not later than 3 October 1991. They shall forthwith inform the Commission thereof.
2. Member States shall communicate to the Commission the text of the main provisions of national law which they adopt in the fields governed by this Directive.
Article 26
Not later than the end of the fifth year after the date of adoption of this Directive and every two years thereafter, the Commission shall submit to the European Parliament, the Council, and the Economic and Social Committee a report on the application of this Directive and, if necessary, make further proposals to adapt it to developments in the field of television broadcasting.
Article 27
This Directive is addressed to the Member States.
Done at Luxembourg, 3 October 1989.
For the Council
The President
R. DUMAS
