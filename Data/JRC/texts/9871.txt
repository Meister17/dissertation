Judgment of the Court of First Instance (First Chamber) of 27 September 2006 — Ferriere Nord v Commission
(Case T-153/04) [1]
Parties
Applicant: Ferriere Nord SpA (Osoppo, Italy) (represented by: W. Viscardini and G. Donà, lawyers)
Defendant: Commission of the European Communities (represented by: A. Nijenhuis and A. Whelan, acting as Agents, and by A. Colabianchi, lawyer)
Re:
Annulment of the Commission decisions notified by letter of 5 February 2004 and by facsimile of 13 April 2004 concerning the outstanding balance of the fine imposed on the applicant by Commission Decision 89/515/EEC of 2 August 1989 relating to a proceeding under Article 85 of the EEC Treaty (IV/31.553 — Welded steel mesh) (OJ 1989 L 260, p. 1)
Operative part of the judgment
The Court:
1. Annuls the Commission decisions notified by letter of 5 February 2004 and by facsimile of 13 April 2004 concerning the outstanding balance of the fine imposed on the applicant by Commission Decision 89/515/EEC of 2 August 1989 relating to a proceeding under Article 85 of the EEC Treaty (IV/31.553 — Welded steel mesh);
2. Orders the Commission, in addition to bearing its own costs, to pay those of the applicant.
[1] OJ C 168, 26.6.2004.
--------------------------------------------------
