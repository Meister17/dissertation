Commission Regulation (EC) No 569/2003
of 28 March 2003
amending Regulation (EC) No 1238/95 establishing implementation rules for the application of Council Regulation (EC) No 2100/94 as regards the fees payable to the Community Plant Variety Office
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 2100/94 of 27 July 1994 on Community plant variety rights(1), as amended by Regulation (EC) No 2506/95(2), and in particular Article 113(4) thereof,
Whereas:
(1) Commission Regulation (EC) No 1238/95 of 31 May 1995 establishing implementing rules for the application of Council Regulation (EC) No 2100/94 as regards the fees payable to the Community Plant Variety Office(3), as amended by Regulation (EC) No 329/2000(4), sets out fees payable to the Community Plant Variety Office ("the Office"), and the levels of such fees.
(2) The Administrative Council of the Office has submitted to the Commission draft amendments relating to the fees payable to the Office under Regulation (EC) No 2100/94.
(3) The SWIFT electronic bank payment method should provide sufficient documentary evidence to show that an applicant has taken the necessary steps to pay the application fee into the account of the Office.
(4) Under Council Regulation (EC) No 2100/94, the application fee is intended to cover several stages of the processing of an application. Therefore the Office should refund a fixed proportion of the application fee where, following the initial examination of the application, it is apparent that the applications is not valid.
(5) To reflect the administration costs of the Community plant variety rights system that are not covered by other fees, the annual fee should not vary according to the species protected, nor increase over time.
(6) The financial reserve of the Office has reached a level which exceeds the level necessary to safeguard the continuity of its operations. Therefore, the amount of the annual fee should be linked to a reduction of the reserve for the period 2003 to 2005.
(7) The due date for payment of the annual fee should precede the beginning of the year of the protection of the plant variety right to which it relates in order to avoid conferring free protection in the event of non-payment of such fee.
(8) It is appropriate to remove the difference between the fees charged for making entries in the register of Community plant variety rights and the register of applications. In addition only one fee should be charged for making the same entry in a register in relation to a request covering more than one variety in the same ownership.
(9) Under Commission Regulation (EC) No 1239/95 of 31 May 1995 establishing implementing rules for the application of Council Regulation (EC) No 2100/94 as regards proceedings before the Community Plant Variety Office(5), as last amended by Regulation (EC) No 2181/2002(6), the Office is to pay for technical examinations. It is necessary to increase the fees charged to applicants for technical examinations and to introduce different fee groups. The fee increases should be achieved in two stages, due to the large amount of the increase.
(10) Regulation (EC) No 1238/95 should therefore be amended accordingly.
(11) The new measures shall apply in relation to fees that fall due from 1 April 2003.
(12) The Administrative Council has been consulted in accordance with Regulation (EC) No 2100/94.
(13) The provisions of this Regulation are in accordance with the opinion of the Standing Committee on Plant Variety Rights,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 1238/95 is amended as follows:
1. Article 4 is amended as follows:
(a) point 3 is replaced by the following:
"3. Where the payment is considered not to have been received by the Office within the requisite time limit, such time limit shall be considered to have been observed vis-à-vis the Office where sufficient documentary evidence is produced within that time limit to show that the person making the payment duly gave an order to a banking establishment or a post office to transfer the amount of the payment in euro to a bank account held by the Office within the time limit.";
(b) point 4 is deleted;
(c) point 5 is replaced by the following:
"5. Documentary evidence shall be regarded as sufficient within the meaning of point 3 where an acknowledgement of provision of the transfer order, issued by a banking establishment or a post office, is produced. However, where the transfer ordered was a transfer using the SWIFT electronic bank payment method, the acknowledgement of provision of the transfer order shall take the form of a copy of the SWIFT report, stamped and signed by a duly authorised official of the bank or post office.";
2. Article 7 is amended as follows:
(a) paragraph 5 is replaced by the following:
"5. Paragraph 4 shall not apply where the application is accompanied by sufficient documentary evidence showing that the person making the payment duly gave an order to a banking establishment or a post office to transfer the amount of the payment in euro to a bank account held by the Office; Article 4 (5) shall apply mutatis mutandis.";
(b) the following paragraph 7 is added:
"7. Where the application fee is received but the application is not valid under Article 50 of the basic Regulation, the Office shall retain EUR 300 of the application fee and refund the remainder when notifying the applicant of the deficiencies found in the application.";
3. Article 9(1) and (2) are replaced by the following:
"1. The Office shall charge a holder of a Community plant variety right (hereinafter referred to as 'the holder') a fee for each year of the duration of a Community plant variety right (annual fee) of EUR 300 for the years 2003 to 2005 and of EUR 435 for the year 2006 and the following years.
2. Payment of the annual fee shall be due:
(a) in relation to the first year of the term of the Community plant variety right, within 60 days of date of the grant of the right; and
(b) in relation to subsequent years of the term of the Community plant variety right, on the first day of the calendar month preceding the month in which the anniversary of the date grant falls.";
4. Article 10 is amended as follows:
(a) in the fifth indent of paragraph 1(b), "ECU 300" is replaced by "EUR 100";
(b) the following paragraph 3 is added:
"3. Where a request for an entry referred to in point (b) or (c) of paragraph 1 concerns more than one application or registered right, applied for or held by the same person, only one fee shall be charged.";
5. Annex I is amended in accordance with the Annex to this Regulation;
6. Annex II is deleted.
Article 2
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Union.
It shall apply in relation to fees that fall due from 1 April 2003.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 28 March 2003.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 227, 1.9.1994, p. 1.
(2) OJ L 258, 28.10.1995, p. 3.
(3) OJ L 121, 1.6.1995, p. 31.
(4) OJ L 37, 12.2.2000, p. 19.
(5) OJ L 121, 1.6.1995, p. 37.
(6) OJ L 331, 7.12.2002, p. 14.
ANNEX
Annex I is replaced by the following:
"ANNEX I
Fees relating to technical examinations as referred to in Article 8
The fee to be paid for the technical examination of a variety pursuant to Article 8 shall be determined, by reference to the year in which the growing period begins and to the species group to which the variety belongs, in accordance with the table:
>TABLE>"
