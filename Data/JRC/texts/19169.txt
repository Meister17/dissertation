Commission Regulation (EC) No 2035/2003
of 19 November 2003
amending Regulation (EC) No 296/96 on data to be forwarded by the Member States and the monthly booking of expenditure financed under the Guarantee Section of the Agricultural Guidance and Guarantee Fund (EAGGF)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1258/1999 of 17 May 1999 on the financing of the common agricultural policy(1), and in particular Article 5(3) and Article 7(5) thereof,
Whereas:
(1) Article 181 and Article 41(2) of Council Regulation (EC, Euratom) No 1605/2002 of 25 June 2002 on the Financial Regulation applicable to the general budget of the European Communities(2) make provision, starting in financial year 2004, for the classification of Commission expenditure by use. The effect of classifying expenditure in this manner is to make the chapters of the budget nomenclature less detailed.
(2) In order to maintain the same level of information and transparency as hitherto in the booking of expenditure financed under the Guarantee Section of the European Agricultural Guidance and Guarantee Fund (EAGGF), the financial information communicated by the Member States every month in accordance with Article 3(3) of Commission Regulation (EC) No 296/96(3), as last amended by Regulation (EC) No 1997/2002(4), should be broken down by budget article or item.
(3) Regulation (EC) No 296/96 should therefore be amended.
(4) The measures provided for in this Regulation are in accordance with the opinion of the EAGGF Committee,
HAS ADOPTED THIS REGULATION:
Article 1
In Article 3(4) of Regulation (EC) No 296/96, the first subparagraph is replaced by the following:"The notification referred to in paragraph 3 shall include a breakdown of expenditure by budget article in the nomenclature of the budget of the European Communities and, for the chapter on the audit of agricultural expenditure, an additional breakdown by budget item; however, for the chapter on fisheries, expenditure shall be given by chapter."
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Union.
It shall apply from 1 December 2003.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 19 November 2003.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 160, 26.6.1999, p. 103.
(2) OJ L 248, 16.9.2002, p. 1.
(3) OJ L 39, 17.2.1996, p. 5.
(4) OJ L 308, 9.11.2002, p. 9.
