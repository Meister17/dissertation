COMMISSION DECISION of 19 November 1992 on the database covering the Community's import requirements, envisaged by the Shift project (92/563/EEC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Decision 92/438/EEC of 13 July 1992, on computerization of veterinary import procedures (Shift project) amending Directives 90/675/EEC, 91/496/EEC, 92/628/EEC and Decision 90/424/EEC, and repealing Decision 88/192/EEC (1), and in particular Article 12 thereof,
Whereas, in order to set up and to make efficient use of the database referred to in Article 4 and in Annex II (1) of Decision 92/438/EEC, it is necessary to specify the characteristics, the content and the conditions for the setting-up and use of this database;
Whereas the development of the user system for the Community database shall be the responsibility of the Commission, and shall take into account the operating systems used in the Member States;
Whereas the measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
1. For the purpose of this Decision, 'Community database' shall mean the database covering the Community's import requirements for live animals and animal products coming from third countries.
2. The Community database must be relational. The user must be able to benefit from rapid and easy access to the information necessary for the checks.
Article 2
The Community database shall include the information referred to in Article 4 (1) of Decision 92/438/EEC. Furthermore, it shall include the special import conditions applying to a Member State or a part of a Member State and certain establishments.
Article 3
1. The Commission shall be responsible for the development of the user system for the Community database.
2. The development referred to in paragraph 1 shall include:
- the description of the Community database structure,
- the definition and realization of the technical application functions which are necessary for its use.
Article 4
This Decision is addressed to the Member States.
Done at Brussels, 19 November 1992. For the Commission
Ray MAC SHARRY
Member of the Commission
(1) OJ No L 243, 25. 8. 1992, p. 27.
