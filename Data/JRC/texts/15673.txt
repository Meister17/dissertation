Prior notification of a concentration
(Case COMP/M.4137 — Mittal/Arcelor)
(2006/C 90/03)
(Text with EEA relevance)
1. On 7 April 2006, the Commission received a notification of a proposed concentration pursuant to Article 4 of Council Regulation (EC) No 139/2004 [1] by which the undertaking Mittal Steel Company N.V. ("Mittal", The Netherlands) acquires within the meaning of Article 3(1)(b) of the Council Regulation control of the whole of the undertaking Arcelor S.A. ("Arcelor", Luxembourg) by way of public bid announced on 27 January 2006.
2. The business activities of the undertakings concerned are:
- for Mittal: manufacturing and sale of steel products at a world-wide level;
- for Arcelor: manufacturing and sale of steel products at a world-wide level.
3. On preliminary examination, the Commission finds that the notified transaction could fall within the scope of Regulation (EC) No 139/2004. However, the final decision on this point is reserved.
4. The Commission invites interested third parties to submit their possible observations on the proposed operation to the Commission.
Observations must reach the Commission not later than 10 days following the date of this publication. Observations can be sent to the Commission by fax (No (32-2) 296 43 01 or 296 72 44) or by post, under reference number COMP/M.4137 — Mittal/Arcelor, to the following address:
European Commission
Competition DG
Merger Registry
J-70
B-1049 Brussels
[1] OJ L 24, 29.1.2004, p. 1.
--------------------------------------------------
