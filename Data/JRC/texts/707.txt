Council Directive 2000/17/EC
of 30 March 2000
amending Directive 77/388/EEC on the common system of value added tax - transitional provisions granted to the Republic of Austria and the Portuguese Republic
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 93 thereof,
Having regard to the proposal from the Community,
Having regard to the opinion of the European Parliament(1),
Having regard to the opinion of the Economic and Social Committee(2),
Whereas:
(1) Point 2(e) of Part IX "Taxation" of Annex XV to the 1994 Act of Accession authorised the Republic of Austria to derogate from Article 28(2) of sixth Council Directive 77/388/EEC of 17 May 1977 on the harmonisation of the laws of the Member States relating to turnover taxes - common system of value added tax: uniform basis of assessment(3), (hereinafter referred to as the "sixth VAT Directive") and to apply a reduced rate to the letting of immovable property for residential use until 31 December 1998, provided that the rate was not lower than 10 %.
(2) Under Article 13(B)(b) of the sixth VAT Directive, the letting of immovable property for residential use in Austria has been exempt from VAT since 1 January 1999 without the right to deduct input tax. However, under Article 13(C)(a) of that Directive, Austria may allow taxpayers the right to opt for taxation. In that case, the normal VAT rate and the normal rules for the right to deduction apply.
(3) The Republic of Austria considers that the measure is still essential, mainly because the transitional VAT regime is still in force and the situation has not really changed since the negotiation of the 1994 Act of Accession.
(4) The Republic of Austria also considers that dispensing with the reduced rate of 10 % would inevitably lead to an increase in the price of immovable property rental for the final consumer.
(5) The Portuguese Republic applied a reduced rate of 8 % to restaurant services as at 1 January 1991. Under Article 28(2)(d) of the sixth VAT Directive, Portugal was permitted to continue applying that rate. However, after a comprehensive amendment of the rates and for political and budgetary reasons, restaurant services were made subject to the normal rate from 1992.
(6) The Portuguese Republic wishes to reintroduce a reduced rate on these services on the basis that maintaining the normal rate had adverse consequences, in particular job losses and an increase in undeclared employment, and that application of the normal rate increased the price of restaurant services for the final consumer.
(7) As the derogations in question concern supplies of services within a single Member State, the risk of distortion of competition can be considered non-existent.
(8) In these circumstances, return to the previous situation may be considered for both the Republic of Austria and the Portuguese Republic, provided that application of the derogations is limited to the transitional period referred to in Article 281 of the sixth VAT Directive. However, the Republic of Austria must take the necessary steps to ensure that the reduced rate has no adverse effects on the European Communities' own resources accruing from VAT, the basis of assessment for which must be reconstituted in accordance with Regulation (EEC, Euratom) No 1553/89(4),
HAS ADOPTED THIS DIRECTIVE:
Article 1
The following points shall be added to Article 28(2) of the sixth VAT Directive:
"j) the Republic of Austria may apply one of the two reduced rates provided for in the third subparagraph of Article 12(3)(a) to the letting of immovable property for residential use, provided that the rate is not lower than 10 %;
k) the Portuguese Republic may apply one of the two reduced rates provided for in the third subparagraph of Article 12(3)(a) to restaurant services, provided that the rate is not lower than 12 %."
Article 2
1. The Member States referred to in Article 1 shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive. They shall forthwith inform the Commission thereof.
When the Member States adopt these measures, they shall contain a reference to this Directive or shall be accompanied by such a reference on the occasion of their official publication. The methods for making such a reference shall be laid down by the Member States.
2. The Member States referred to in Article 1 shall communicate to the Commission the text of the provisions of national law which they adopt in the field governed by this Directive.
Article 3
This Directive shall enter into force on the day of its publication in the Official Journal of the European Communities.
It shall apply as from 1 January 1999 until the end of the transitional period referred to in Article 281 of the sixth VAT Directive.
Article 4
This Directive is addressed to the Member States.
Done at Brussels, 30 March 2000.
For the Council
The President
J. Sócrates
(1) Opinion delivered on 15 March 2000 (not yet published in the Official Journal).
(2) OJ C 75, 15.3.2000, p. 21.
(3) OJ L 145, 13.6.1977, p. 1. Directive as last amended by Directive 1999/85/EC (OJ L 277, 28.10.1999, p. 34).
(4) OJ L 155, 7.6.1989, p. 9.
