Action brought on 17 May 2006 — Commission of the European Communities v French Republic
Parties
Applicant: Commission of the European Communities (represented by: G. Rozet and I. Kaufmann-Bühler, acting as Agents)
Defendant: French Republic
Form of order sought
- declare that, by failing to adopt the laws, regulations and administrative provisions necessary to comply with Articles 2, 10(1) and 12(3) and (4) of Council Directive 89/391/EEC of 12 June 1989 on the introduction of measures to encourage improvements in the safety and health of workers at work [1], the French Republic has failed to fulfil its obligations under that directive and Articles 10 EC and 249 EC;
- order the French Republic to pay the costs.
Pleas in law and main arguments
The period for transposing Directive 89/391/EEC expired on 31 December 1992.
The Commission complains that the French Republic has failed to fulfil its obligations under Articles 2, 10(1) and 12(3) and (4) of Directive 83/391 by failing to adopt all the provisions necessary to transpose the directive correctly into French law.
[1] OJ 1989 L 183, p. 1.
--------------------------------------------------
