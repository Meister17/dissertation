COMMISSION REGULATION (EEC) No 1620/81 of 15 June 1981 on the classification of goods falling within subheading 87.01 B of the Common Customs Tariff
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 97/69 of 16 January 1969 on measures to be taken for uniform application of the nomenclature of the Common Customs Tariff (1), as last amended by the Act of Accession of Greece, and in particular Article 3 thereof,
Whereas, in order to ensure uniform application of the nomenclature of the Common Customs Tariff, provision must be made for the classification of self-propelled machines for forestry use which are capable of being fitted with various attachments enabling the machines to perform a number of operations on timber, such as lifting, handling, hauling, pushing, loading, unloading, debarking and cutting to length;
Whereas the Common Customs Tariff annexed to Council Regulation (EEC) No 950/68 (2), as last amended by Regulation (EEC) No 853/81 (3), includes under heading No 84.22 lifting, handling, loading or unloading machinery, under heading No 84.23 excavating, levelling and similar earth-moving machinery, under heading No 84.28 other agricultural machinery and under heading No 87.01 tractors other than those of the type used on railway station platforms;
Whereas these headings are relevant to the classification of the abovementioned machines;
Whereas, in accordance with Note 1 to Chapter 87, for the purposes of that Chapter and in particular of heading No 87.01, tractors are deemed to be vehicles constructed essentially for hauling or pushing another vehicle, appliance or load;
Whereas propelling bases forming an integral part of a machine designed to perform any of the functions described in heading Nos 84.22, 84.23 or 84.28, in addition to hauling or pushing, are excluded from heading No 87.01;
Whereas, however, the abovementioned machines, although capable of performing various operations according to the attachments fitted, are nevertheless constructed essentially for hauling and pushing loads (felled trees) and cannot therefore be classified as machines designed specifically to perform the functions described in heading Nos 84.22 or 84.23 or those covered by heading No 84.28;
Whereas the machines must thus be classified under heading No 87.01 ; whereas since they are designed to perform. the operations normally carried out by forestry tractors, they therefore must be classified under subheading 87.01 B;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Committee on Common Customs Tariff Nomenclature,
HAS ADOPTED THIS REGULATION:
Article 1
Self-propelled machines for forestry use which are capable of being fitted with various attachments enabling the machines to perform a number of operations on timber, such as lifting, handling, hauling, pushing, loading, unloading, debarking and cutting to length, shall be classified in the Common Customs Tariff under subheading:
87.01 Tractors (other than those falling within heading No 87.07), whether or not fitted with power take-offs, winches or pulleys:
B. Agricultural tractors (excluding walking tractors) and forestry tractors, wheeled.
Article 2
This Regulation shall enter into force on the 42nd day following its publication in the Official Journal of the European Communities.
(1) OJ No L 14, 21.1.1969, p. 1. (2) OJ No L 172, 22.7.1968, p. 1. (3) OJ No L 90, 4.4.1981, p. 8.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 15 June 1981.
For the Commission
Karl-Heinz NARJES
Member of the Commission
