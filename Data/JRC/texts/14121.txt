Council conclusions on women's health
(2006/C 146/02)
THE COUNCIL OF THE EUROPEAN UNION
1. NOTES that the citizens of the European Union, more than half of them being women, attach great importance to the highest possible levels of human health and consider it to be an essential prerequisite to a high quality of life.
2. RECALLS THAT:
- Article 3(2) of the EC Treaty and Article 23 of the Charter of Fundamental Rights of the European Union state that equality between women and men shall be ensured in all policy areas;
- Article 152 of the EC Treaty states that a high level of human health protection shall be ensured in the definition and implementation of all Community policies and activities, and provides that Community action is to complement national policies and be directed towards improving public health, preventing human illness and diseases, and obviating sources of danger to human health;
- On 4 December 1997, the Council adopted a Resolution concerning the report [1]on the state of women's health in the European Community [2];
- On 9 March 1997, the European Parliament adopted a Resolution on the report of the Commission on the state of women's health in the European Community [3];
- On 28 April 2005, the European Parliament adopted a Resolution on modernising social protection and developing good quality healthcare, calling on the Commission to submit a new report on the health situation of women in the European Union [4].
3. RECALLS the report on the progress made within the European Union as regards the implementation of the Beijing Platform for Action established in January 2005 by the Luxembourg Presidency that underlined that women's health is still an area of concern and stressed the importance of the collection of relevant data.
4. RECALLS the Strategic Action Plan for the Health of Women in Europe endorsed at the WHO meeting in Copenhagen, on 5- 7 February 2001.
5. ACKNOWLEDGES that social and health determinants, clinical manifestations, therapeutical approaches, effectiveness and side effects of treatment of disease and disorders may differ between women and men.
6. STRESSES the importance of raising awareness amongst the general public but also health care professionals that gender is a key determinant of health.
7. RECOGNISES the importance of addressing inequalities that may exist within and between Member States, by tackling social and economic health determinants.
8. WELCOMES the Commission Communication: A Roadmap for equality between women and men (2006-2010) [5] recognising the gender dimension in health, inter alia, aiming at strengthening gender mainstreaming in health policies.
9. NOTES that the Programme of Community Action in the Field of Public Health (2003-2008) [6] aims to protect human health and improve public health, thereby contributing towards tackling inequalities in health.
10. WELCOMES the fact that the proposal for a Council Decision concerning the Specific Programme "Cooperation" implementing the 7th Framework Programme (2007-2013) of the European Community for research, technological development and demonstration activities [7] intends to integrate gender aspects in health research.
11. ACKNOWLEDGES the need for gender-related biomedical research as well as research on socio-economic determinants.
12. RECOGNISES that although women live longer than men, they suffer a greater burden of unhealthy life years. The incidence and prevalence of certain diseases like osteoporosis are higher in women. Others such as cardiovascular disease, cancer and mental health problems affect men and women differently. Some diseases related to birth and reproductive organs like endometriosis and cervical cancer affect women exclusively.
13. EMPHASISES that cardiovascular disease is a major cause of death and of reduced quality of life for women in the European Union, despite still being perceived as predominantly male disease in some Member States.
14. NOTES WITH CONCERN that the rise in smoking among females in some Member States is causing a substantially increased risk of lung cancer and cardiovascular diseases.
15. NOTES WITH CONCERN that depression is predicted in some Member States to be the major burden of disease for women by 2020. Mental ill health has an impact on quality of life and can therefore influence morbidity and mortality.
16. RECOGNISES the major impact of unhealthy lifestyles on a significant number of diseases and therefore the potential which the promotion of inter alia healthy diets and physical activity has for reducing cardiovascular diseases and certain forms of cancer.
17. AGREES that gender sensitive prevention measures, health promotion and treatment contribute towards reducing morbidity and mortality from major diseases among women and consequently improve their quality of life.
18. NOTES that reliable, compatible, comparable data on the status of women's health is essential to improve information to the public and develop appropriate strategies, policies and actions to ensure a high level of health protection, and that gender-specific data and reporting are essential for policy making.
19. UNDERLINES that after almost a decade a new report on the health status of women in the enlarged European Union is needed.
20. INVITES the Member States to:
- Collect gender-specific data on health, and to break down and analyse statistics by gender;
- Take initiatives to enhance general and health professionals' knowledge on the relationship between gender and health;
- Promote health and prevent disease taking into account where appropriate gender difference;
- Promote research into the different effects of medicines on women and men, and gender- specific health research;
- Encourage gender mainstreaming in healthcare;
- Examine and tackle health inequalities which may exist accordingly in order to reduce the health gap and ensure equality of treatment and access to care.
21. INVITES the European Commission to:
- Integrate gender aspects in health research;
- Support the exchange of information and experience on good practice in gender-sensitive health promotion and prevention;
- Assist Member States in developing effective strategies to reduce health inequalities with a gender dimension;
- Promote and strengthen the comparability and compatibility of gender-specific information on health across Member States and at Community level through the development of appropriate data;
- Present a second report on the state of women's health in the European Union.
22. INVITES the European Commission to draw on the expertise of EUROSTAT and of the future European Institute for Gender Equality in order to contribute to data collection and analysis and the sharing of best practice.
23. INVITES the European Commission to continue to cooperate with the relevant international and intergovernmental organisations, in particular the WHO and OECD, to ensure effective coordination of activities.
[1] Doc. 8537/97; COM(97) 224 final
[2] OJ C 394 of 30.12.1997, p 1.
[3] OJ C 175 of 21.6.1999, p 68.
[4] A6-0085/2005
[5] OJ Doc. 7034/06; COM(2006) 92 final
[6] OJ L 271 of 9.10.2002, p. 1.
[7] Doc. 12736/05
--------------------------------------------------
