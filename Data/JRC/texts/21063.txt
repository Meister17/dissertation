Directive 2002/85/EC of the European Parliament and of the Council
of 5 November 2002
amending Council Directive 92/6/EEC on the installation and use of speed limitation devices for certain categories of motor vehicles in the Community
THE EUROPEAN PARLIAMENT AND THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 71 thereof,
Having regard to the proposal from the Commission(1),
Having regard to the opinion of the Economic and Social Committee(2),
Following consultation of the Committee of the Regions,
Acting in accordance with the procedure laid down in Article 251 of the Treaty(3),
Whereas:
(1) Transport safety and environmental issues connected with transport are vital in ensuring sustainable mobility.
(2) The use of speed limitation devices for heaviest-motor vehicle categories has had a positive effect on the improvement of road safety. It has also contributed to environmental protection.
(3) Council Directive 92/6/EEC(4) provides that, depending on technical possibilities and experiences in Member States, the requirements on installation and use of speed limitation devices could subsequently be extended to light goods vehicles.
(4) The extension of the scope of Directive 92/6/EEC to vehicles of more than 3,5 tonnes designed for transporting goods or passengers was one of the measures advocated by the Council in its resolution of 26 June 2000 on the improvement of road safety(5), in accordance with the Commission communication of 20 March 2000 on priorities in EU road safety.
(5) The scope of Directive 92/6/EEC should be extended to motor vehicles of category M2, to vehicles of category M3 having a maximum mass of more than 5 tonnes but not exceeding 10 tonnes and to vehicles of category N2.
(6) Since the objectives of the proposed action, namely the introduction of modifications to the Community-wide arrangements for the installation and use of speed limitation devices on certain heavy vehicle categories, cannot be sufficiently achieved by the Member States and can therefore, by reason of the scale or effects of the action, be better achieved at Community level, the Community may adopt measures, in accordance with the principle of subsidiarity as set out in Article 5 of the Treaty. In accordance with the principle of proportionality, as set out in that Article, this Directive does not go beyond what is necessary to achieve those objectives.
(7) Directive 92/6/EEC should therefore be amended accordingly,
HAVE ADOPTED THIS DIRECTIVE:
Article 1
Directive 92/6/EEC is hereby amended as follows:
1. Articles 1 to 5 shall be replaced by the following:
"Article 1
For the purposes of this Directive, 'motor vehicle' means any power-driven vehicle falling within category M2, M3, N2 or N3, intended for use on the road and having at least four wheels and a maximum design speed exceeding 25 km/h.
Categories M2, M3, N2 and N3 shall be understood to be those defined in Annex II to Directive 70/156/EEC(6).
Article 2
Member States shall take the necessary measures to ensure that motor vehicles of categories M2 and M3 referred to in Article 1 may be used on the road only if equipped with a speed limitation device set in such a way that their speed cannot exceed 100 kilometres per hour.
Category M3 vehicles registered before 1 January 2005 with a maximum mass exceeding 10 tonnes may continue to be equipped with devices on which the maximum speed is set at 100 kilometres per hour.
Article 3
1. Member States shall take the necessary measures to ensure that motor vehicles of categories N2 and N3 may be used on the road only if equipped with a speed limitation device set in such a way that their speed cannot exceed 90 kilometres per hour.
2. Member States shall be authorised to require that the speed limitation device in vehicles registered in their territory and used exclusively for the transport of dangerous goods is set in such a way that those vehicles cannot exceed a maximum speed of less than 90 kilometres per hour.
Article 4
1. For motor vehicles of category M3 having a maximum mass of more than 10 tonnes and motor vehicles of category N3, Articles 2 and 3 shall be applied:
(a) to vehicles registered as from 1 January 1994, from 1 January 1994;
(b) to vehicles registered between 1 January 1988 and 1 January 1994:
(i) from 1 January 1995, in the case of vehicles used for both national and international transport;
(ii) from 1 January 1996, in the case of vehicles used exclusively for national transport.
2. For motor vehicles of category M2, vehicles of category M3 having a maximum mass of more than 5 tonnes but not exceeding 10 tonnes and vehicles of category N2, Articles 2 and 3 shall apply at the latest:
(a) to vehicles registered as from 1 January 2005;
(b) to vehicles complying with the limit values set out in Directive 88/77/EEC(7) registered between 1 October 2001 and 1 January 2005:
(i) from 1 January 2006 in the case of vehicles used for both national and international transport operations;
(ii) from 1 January 2007 in the case of vehicles used solely for national transport operations.
3. For a period of no more than three years from 1 January 2005, any Member State may exempt from the provisions of Articles 2 and 3 category M2 vehicles and category N2 vehicles with a maximum mass of more than 3,5 tonnes but not exceeding 7,5 tonnes, registered in the national register and not travelling on the territory of another Member State.
Article 5
1. The speed limitation devices referred to in Articles 2 and 3 must satisfy the technical requirements laid down in the Annex to Directive 92/24/EEC(8). However, all vehicles covered by this Directive and registered before 1 January 2005 may continue to be equipped with speed limitation devices which satisfy the technical requirements laid down by the competent national authorities.
2. Speed limitation devices shall be installed by workshops or bodies approved by the Member States."
2. The following Article shall be inserted:
"Article 6a
As part of the road safety action programme for the period 2002 to 2010, the Commission shall assess the road safety and road traffic implications of adjusting the speed limitation devices used by category M2 vehicles and by category N2 vehicles with a maximum mass of 7,5 tonnes or less to the speeds laid down by this Directive.
If necessary, the Commission shall submit appropriate proposals."
Article 2
Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive by 1 January 2005 at the latest. They shall forthwith inform the Commission thereof.
When Member States adopt these measures, they shall contain a reference to this Directive or shall be accompanied by such reference on the occasion of their official publication. The methods of making such reference shall be laid down by Member States.
Article 3
This Directive shall enter into force on the day of its publication in the Official Journal of the European Communities.
Article 4
This Directive is addressed to the Member States.
Done at Brussels, 5 November 2002.
For the European Parliament
The President
P. Cox
For the Council
The President
T. Pedersen
(1) OJ C 270 E, 25.9.2001, p. 77.
(2) OJ C 48, 21.2.2002, p. 47.
(3) Opinion of the European Parliament of 7 February 2002 (not yet published in the Official Journal), Council Common Position of 25 June 2002 (OJ C 228 E, 25.9.2002, p. 14) and decision of the European Parliament of 24 September 2002 (not yet published in the Official Journal).
(4) OJ L 57, 2.3.1992, p. 27.
(5) OJ C 218, 31.7.2000, p. 1.
(6) Council Directive 70/156/EEC of 6 February 1970 on the approximation of the laws of the Member States relating to the type-approval of motor vehicles and their trailers (OJ L 42, 23.2.1970, p. 1). Directive as last amended by Commission Directive 2001/116/EC (OJ L 18, 21.1.2002, p. 1).
(7) Council Directive 88/77/EEC of 3 December 1987 on the approximation of the laws of the Member States relating to the measures to be taken against the emission of gaseous pollutants from diesel engines for use in vehicles (OJ L 36, 9.2.1988, p. 33). Directive as last amended by Commission Directive 2001/27/EC (OJ L 107, 18.4.2001, p. 10).
(8) Council Directive 92/24/EEC of 31 March 1992 relating to speed limitation devices or similar speed limitation on-board systems of certain categories of motor vehicles (OJ L 129, 14.5.1992, p. 154).
