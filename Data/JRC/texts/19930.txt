COMMISSION DIRECTIVE of 27 June 1979 amending Council Directives 66/401/EEC, 66/402/EEC, 69/208/EEC and 70/458/EEC on the marketing of fodder plant seed, cereal seed, seed of oil and fibre plants and vegetable seed (79/641/EEC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Directive 66/401/EEC of 14 June 1966 on the marketing of fodder plant seed (1), as last amended by Directive 78/1020/EEC (2), and in particular Articles 2 (1a) and 21a thereof,
Having regard to Council Directive 66/402/EEC of 14 June 1966 on the marketing of cereal seed (3), as last amended by Directive 78/1020/EEC, and in particular Articles 2 (1a) and 21a thereof,
Having regard to Council Directive 69/208/EEC of 30 June 1969 on the marketing of seed of oil and fibre plants (4), as last amended by Directive 78/1020/EEC, and in particular Articles 2 (1a) and 40a thereof,
Having regard to Council Directive 70/458/EEC of 29 September 1970 on the marketing of vegetable seed (5), as last amended by Directive 78/692/EEC (6), and in particular Articles 2 (1a) and 40a thereof,
Whereas, in the light of the development of scientific knowledge, a number of the botanical names used in the Directives on the marketing of seeds have been shown to be incorrect or of doubtful authenticity;
Whereas those names should be aligned with those normally accepted internationally;
Whereas Directives 66/401/EEC, 66/402/EEC, 69/208/EEC and 70/458/EEC on the marketing of fodder plant seed, cereal seed, seed of oil and fibre plants and vegetable seed should therefore be amended;
Whereas certain provisions of the abovementioned Directives and of Council Directive 70/457/EEC of 29 September 1970 on the common catalogue of varieties of agricultural plant species (7), as last amended by Directive 78/55/EEC (8), use the concept of species, thereby determining the scope of those provisions ; whereas the changes to the nomenclature made by this Directive should not affect the interpretation of those provisions;
Whereas the measures provided for in this Directive are in accordance with the opinion of the Standing Committee on Seeds and Propagating Material for Agriculture, Horticulture and Forestry,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Council Directive 66/401/EEC on the marketing of fodder plant seed is hereby amended as follows: 1. In Article 2 (1) (A) (a) the names of the following species:
Agrostis canina L. ssp. canina Hwd. - Velvet bent grass,
Arrhenatherum elatius (L.) J. et C. Presl. - Tall oatgrass,
Phleum bertolinii DC - Timothy,
Trisetum flavescens (L.) Pal. Beauv. - Golden oatgrass,
shall be replaced by the following:
Agrostis canina L. - Velvet bent,
Arrhenatherum elatius (L.) Beauv. ex J. et K. Presl. - Tall oatgrass, (1)OJ No 125, 11.7.1966, p. 2298/66. (2)OJ No L 350, 14.12.1978, p. 27. (3)OJ No 125, 11.7.1966, p. 2309/66. (4)OJ No L 169, 10.7.1969, p. 3. (5)OJ No L 225, 12.10.1970, p. 7. (6)OJ No L 236, 26.8.1978, p. 13. (7)OJ No L 225, 12.10.1970, p. 1. (8)OJ No L 16, 20.1.1978, p. 23.
Phleum bertolonii DC - Timothy,
Trisetum flavescens (L.) Beauv. - Golden oatgrass.
2. In Article 2 (1) (A) (b) the names of the following species:
Medicago varia Martyn - Lucerne,
Onobrychis sativa Lam. - Sainfoin,
Pisum arvense L. - Field pea,
Trigonella foenumgraecum L. - Fenugreek,
shall be replaced by the following:
Medicago x varia Martyn - Lucerne,
Onobrychis viciifolia Scop. - Sainfoin,
Pisum sativum L. (partim) - Field pea,
Trigonella foenum-graecum L. - Fenugreek.
3. In Article 2 (1) (A) (b) the names of the following species:
Vicia faba L. ssp. faba var. equina Pers. - Field beans,
Vicia faba L. var. minor (Peterm.) Bull - Field beans,
shall be replaced by the following:
Vicia faba L. (partim) - Field beans.
4. In Article 3 (1) the names of the following species:
Medicago varia Martyn,
Pisum arvense L.,
shall be replaced by the following:
Medicago x varia Martyn,
Pisum sativum L.
5. In Annex I (2), Annex I (4), Annex II (I) (1) and Annex II (II) (1) the name of the following species:
Pisum arvense,
shall be replaced by the following:
Pisum sativum.
6. In Annex II (1) (2) (A), Annex II (II) (2) (A) and Annex III the names of the following species:
Agrostis canina ssp. canina,
Medicago varia,
Onobrychis sativa,
Pisum arvense,
shall be replaced by the following:
Agrostis canina,
Medicago x varia,
Onobrychis viciifolia,
Pisum sativum.
7. In Annex II (I) (2) (A), Annex II (II) (2) (A) and Annex III the names of the following species:
Vicia faba ssp. var. equina,
Vicia faba var. minor,
shall be replaced by the following:
Vicia faba.
8. In Annex II (I) (2) (B) (e) the names of the following species:
Pisum arvense,
Vicia faba spp.,
shall be replaced by the following:
Pisum sativum,
Vicia faba.
Article 2
Council Directive 66/402/EEC on the marketing of cereal seed is hereby amended as follows: 1. In Article 2 (1) (A) the names of the two following species:
Hordeum distichum L. - 2-row barley,
Hordeum polystichum L. - 6-row barley,
shall be replaced by the following:
Hordeum vulgare L. - Barley.
2. In Article 2 (1) (A) the names of the following species:
Triticum aestivum L. - Common wheat,
Triticum durum L. - Durum wheat, shall be replaced by the following:
Triticum aestivum L. emend. Fiori et Paol. - Wheat,
Triticum durum Desf. - Durum wheat.
3. In Annex I (5) (B) (a), Annex II (1) (A), Annex II (1) (A) and Annex III the names of the following species:
Hordeum distichum,
Hordeum polystichum,
shall be replaced by the following:
Hordeum vulgare.
Article 3
Council Directive 69/208/EEC on the marketing of seed of oil and fibre plants is hereby amended as follows: 1. The following names shall be substituted for the names listed in Article 2 (1) (A):
Arachis hypogaea L. - Groundnut (peanut),
Brassica juncea (L.) Czern. et Coss. in Czern. - Brown mustard,
Brassica napus L. ssp. oleifera (Metzg.) Sinsk. - Swede rape,
Brassica nigra (L.) W. Koch - Black mustard,
Brassica rapa L. (partim) - Turnip rape,
Cannabis sativa L. - Hemp,
Carum carvi L. - Caraway,
Glycine max (L.) Merr. - Soya bean,
Gossypium spp. - Cotton,
Helianthus annuus L. - Sunflower,
Linum usitatissimum L. - Flax, linseed,
Papaver somniferum L. - Opium poppy,
Sinapis alba L. - White mustard.
2. In Article 3 (1) the name of the species Brassica campestris L. ssp. oleifera (Metzg.) Sinsk. shall be deleted.
The name Brassica rapa L. (partim) shall be inserted after the name Brassica napus L. ssp. oleifera (Metzg.) Sinsk.
3. In Annex I (2) the name of the species Brassica napus oleifera shall be replaced by Brassica napus ssp. oleifera.
4. In Annex II (I) (3) (A) the name of the species Brassica campestris ssp. oleifera shall be deleted.
The name Brassica rapa shall be inserted after the name Brassica napus ssp. oleifera.
5. In Annex III the name of the species Brassica campestris spp. oleifera shall be replaced by Brassica rapa.
Article 4
Council Directive 70/458/EEC on the marketing of vegetable seed is hereby amended as follows: 1. In Article 2 (1) (A) the names of the following species:
Citrullus vulgaris L. - Water melon,
Daucus carota L. ssp. sativus (Hoffm.) Hayek - Carrot,
Petroselinum hortense Hoffm. - Parsley,
Pisum sativum L. (excl. P. arvense L.) - Pea,
Valerianella locusta (L.) Betcke (V. olitoria Polt.) - Corn-salad or Lamb's lettuce,
Vicia faba major L. - Broad bean,
shall be replaced by the following:
Citrullus lanatus (Thunb.) Matsum. et Nakai - Water melon,
Daucus carota L. - Carrot,
Petroselinum crispum (Mill.) Nym. ex A. W. Hill - Parsley,
Pisum sativum L. (partim) - Pea, excluding Field pea, Valerianella locusta (L.) Laterr. - Corn salad or Lamb's lettuce,
Vicia faba L. (partim) - Broad bean.
2. In Article 2 (1) (A) the name of the species Solanum lycopersicum L. (Lycopersicum esculentum Mill.) - Tomato - shall be deleted.
The name Lycopersicon lycopersicum (L.) Karst. ex Farwell - Tomato - shall be inserted after the name Lactuca sativa L. - Lettuce.
3. In Annex II (3) (a) and in Annex III (2) the names of the species:
Citrullus vulgaris,
Petroselinum hortense,
Solanum lycopersicum,
shall be replaced by the following:
Citrullus lanatus,
Petroselinum crispum,
Lycopersicon lycopersicum.
Article 5
This Directive shall not affect the application of the other provisions of Directives 66/401/EEC, 66/402/EEC, 69/208/EEC, and 70/458/EEC or the provisions of Directive 70/457/EEC.
Article 6
Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with the provisions of this Directive by 1 July 1980 at the latest. They shall forthwith inform the Commission thereof.
Article 7
This Directive is addressed to the Member States.
Done at Brussels, 27 June 1979.
For the Commission
Finn GUNDELACH
Vice-President
