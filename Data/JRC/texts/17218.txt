Commission Regulation (EC) No 1931/2004
of 8 November 2004
amending Regulation (EEC) No 1609/88 as regards the latest date by which butter must have been taken into storage in order to be sold pursuant to Regulations (EEC) No 3143/85 and (EC) No 2571/97
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1255/1999 of 17 May 1999 on the common organisation of the market in milk and milk products [1], and in particular Article 10 thereof,
Whereas:
(1) Pursuant to Article 1 of Commission Regulation (EC) No 2571/97 of 15 December 1997 on the sale of butter at reduced prices and the grant of aid for cream, butter and concentrated butter for use in the manufacture of pastry products, ice cream and other foodstuffs [2], the butter put up for sale must have been taken into storage before a date to be determined.
(2) In view of the trends on the butter market and the quantities of stocks available, the date in Article 1 of Commission Regulation (EEC) No 1609/88 [3], relating to the butter referred to in Regulation (EC) No 2571/97, should be amended.
(3) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Milk and Milk Products,
HAS ADOPTED THIS REGULATION:
Article 1
In Article 1 of Regulation (EEC) No 1609/88, the second subparagraph is hereby replaced by the following:
"The butter referred to in Article 1(1)(a) of Regulation (EC) No 2571/97 must have been taken into storage before 1 January 2003."
Article 2
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 8 November 2004.
For the Commission
Franz Fischler
Member of the Commission
--------------------------------------------------
[1] OJ L 160, 26.6.1999, p. 48. Regulation as last amended by Commission Regulation (EC) No 186/2004 (OJ L 29, 3.2.2004, p. 6).
[2] OJ L 350, 20.12.1997, p. 3. Regulation as last amended by Regulation (EC) No 921/2004 (OJ L 163, 30.4.2004, p. 94).
[3] OJ L 143, 10.6.1988, p. 23. Regulation as last amended by Regulation (EC) No 1449/2004 (OJ L 267, 14.8.2004, p. 31).
