COUNCIL DIRECTIVE of 12 December 1991 on hazardous waste (91/689/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particluar Article 103s thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas Council Directive 78/319/EEC of 20 March 1978 on toxic and dangerous waste (4), established Community rules on the disposal of dangerous waste; whereas in order to take account of experience gained in the implementation of that Directive by the Member States, it is necessary to amend the rules and to replace Directive 78/319/EEC by this Directive;
Whereas the Council resolution of 7 May 1990 on waste policy (5) and the action programme of the European Communities on the environment, which was the subject of the resolution of the Council of the European Communities and of the representatives of the Government of the Member States, meeting within the Council, of 19 October 1987 on the continuation and implementation of a European Community policy and action programme on the environment (1987 to 1992) (6), envisage Community measures to improve the conditions under which hazardous wastes are disposed of and managed;
Whereas the general rules applying to waste management which are laid down by Council Directive 75/442/EEC of 15 July 1975 on waste (7), as amended by Directive 91/156/EEC (8), also apply to the management of hazardous waste;
Whereas the correct management of hazardous waste necessitates additional, more stringent rules to take account of the special nature of such waste;
Whereas it is necessary, in order to improve the effectiveness of the management of hazardous waste in the Community, to use a precise and uniform definition of hazardous waste based on experience;
Whereas it is necessary to ensure that disposal and recovery of hazardous waste is monitored in the fullest manner possible;
Whereas it must be possible rapidly to adapt the provisions of this Directive to scientific and technical progress; whereas the Committee set up by Directive 75/442/EEC must also empowered to adapt the provisions of this Directive to such progress,
HAS ADOPTED THIS DIRECTIVE:
Article 1
1. The object of this Directive, drawn up pursuant to Article 2 (2) of Directive 75/442/EEC, is to approximate the laws of the Member States on the controlled management of hazardous waste.
2. Subject ot this Directive, Directive 75/442/EEC shall apply to hazardous waste.
3. The definition of 'waste` and of the other terms used in this Directive shall be those in Directive 75/442/EEC.
4. For the purpose of this Directive 'hazardous waste` means:
- wastes featuring on a list to be drawn up in accordance with the procedure laid down in Article 18 of Directive 75/442/EEC on the basis of Annexes I and II to this Directive, not later than six months before the date of implementation of this Directive. These wastes must have one or more of the properties listed in Annex III. The list shall take into account the origin and composition of the waste and, where necessary, limit values of concentration. This list shall be periodically reviewed and if necessary by the same procedure,
- any other waste which is considered by a Member State to display any of the properties listed in Annex III. Such cases shall be notified to the Commission and reviewed in accordance with the procedure laid down in Article 18 of Directive 75/442/EEC with a view to adaptation of the list.
5. Domestic waste shall be exempted from the provisions of this Directive. The Council shall establish, upon a proposal from the Commission, specific rules taking into consideration the particular nature of domestic waste not later than the end of 1992.
Article 2
1. Member States shall take the necessary measures to require that on every site where tipping (discharge) of hazardous waste takes place the waste is recorded and identified.
2. Member States shall take the necessary measures to require that establishment and undertaking which dispose of, recover, collect or transport hazardous waste do not mix different categories of hazardous waste or mix hazardous waste with non-hazardous waste.
3. By way of derogation from paragraph 2, the mixing of hazardous waste with other hazardous waste or with other waste, substances or materials may be permitted only where the conditions laid down in Article 4 of Directive 75/442/EEC are complied with and in particular for the purpose of improving safety during disposal or recovery. Such an operation shall be subject to the permit requirement imposed in Articles 9, 10 and 11 of Directive 75/442/EEC.
4. Where waste is already mixed with other waste, substances or materials, separation must be effected, where technically and economically feasible, and where necessary in order to comply with Article 4 of Directive 75/442/EEC.
Article 3
1. The derogation referred to in Article 11 (1) (a) of Directive 75/442/EEC from the permit requirement for establishments or undertakings which carry out their own waste disposal shall not apply to hazardous waste covered by this Directive.
2. In accordance with Article 11 (1) (b) of Directive 75/442/EEC, a Member State may waive Article 10 of that Directive for establishments or undertakings which recover waste covered by this Directive:
- if the Member State adopts general rules listing the type and quantity of waste and laying down specific conditions (limit values for the content of hazardous substances in the waste, emission limit values, type of activity) and other necessary requirements for carrying out different forms of recovery, and - if the types or quantities of waste and methods of recovery are such that the conditions laid down in Article 4 of Directive 75/442/EEC are complied with.
3. The establishments or undertankings referred to in paragraph 2 shall be registered with the competent authorities.
4. If a Member State intends to make use of the provisions of paragraph 2, the rules referred to in that paragraph shall be sent to the Commission not later than three months prior to their coming into force. The Commission shall consult the Member States. In the light of these consultations the Commission shall propose that the rules be finally agreed upon in accordance with the procedure laid down in Article 18 of Directive 75/442/EEC.
Article 4
1. Article 13 of Directive 75/442/EEC shall also apply to producers of hazardous waste.
2. Article 14 of Directive 75/442/EEC shall also apply to producers of hazardous waste and to all establishments and undertakings transporting hazardous waste.
3. The records referred to in Article 14 of Directive 75/442/EEC must be preserved for at least three years except in the case of establishments and undertakings transporting hazardous waste which must keep such records for at least 12 months. Documentary evidence that the management operations have been carried out must be supplied at the request of the competent authorities or of a previous holder.
Article 5
1. Member States shall take the necessary measures to ensure that, in the course of collection, transport and temporary storage, waste is properly packaged and labelled in accordance with the international and Community standards in force.
2. In the case of hazardous waste, inspections concerning collection and transport operations made on the basis of Article 13 of Directive 75/442/EEC shall cover more particularly the origin and destination of such waste.
3. Where hazardous waste is transferred, it shall be accompanied by an identification form containing the details specified in Section A of Annex I to Council Directive 84/631/EEC of 6 December 1984 on the supervision and control within the European Community of the transfrontier shipment of hazardous waste (1), as last amended by Directive 86/279/EEC (2).
Article 6
1. As provided in Article 7 of Directive 75/442/EEC, the competent authorities shall draw up, either separately or in the framework of their general waste management plans, plans for the management of hazardous waste and shall make these plans public.
2. The Commission shall compare these plans, and in particular the methods of disposal and recovery. It shall make this information available to the competent authorities of the Member States which ask for it.
Article 7
In cases of emergency or grave danger, Member States shall take all necessary steps, including, where appropriate, temporary derogations from this Directive, to ensure that hazardous waste is so dealt with as not to constitute a threat to the population or the environment. The Member State shall inform the Commission of any such derogations.
Article 8
1. In the context of the report provided for in Article 16 (1) of Directive 75/442/EEC, and on the basis of a questionnaire drawn up in accordance with that Article, the Member States shall send the Commission a report on the implementation of this Directive.
2. In addition to the consolidated report referred to in Article 16 (2) of Directive 75/442/EEC, the Commission shall report to the European Parliament and the Council every three years on the implementation of this Directive.
3. In addition, by 12 December 1994, the Member States shall send the Commission the following information for every establishment or undertaking which carries out disposal and/or recovery of hazardous waste principally on behalf of third parties and which is likely to form part of the integrated network referred to in Article of Directive 75/442/EEC:
- name and address,
- the method used to treat waste,
- the types and quantities of waste which can be treated.
Once a year, Member States shall inform the Commission of any changes in this information.
The Commission shall make this information available on request to the competent authorities in the Member States.
The format in which this information will be supplied to the Commission shall be agreed upon in accordance with the procedure laid down in Article 18 of Directive 75/442/EEC.
Article 9
The amendments necessary for adapting the Annexes to this Directive to scientific and technical progress and for revising the list of wastes referred to in Article 1 (4) shall be adopted in accordance with the procedure laid down in Article 18 of Directive 74/442/EEC.
Article 10
1. The Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive before 12 December 1993. They shall forthwith inform the Comission thereof.
2. When Member States adopt these measures, they shall contain a reference to this Directive or shall be accompanied by such reference on the occasion of their official publication. The methods of making such a reference shall be laid down by the Member States.
3. Member States shall communicate to the Commission the texts of the main provisions of national law which they adopt in the field governed by this Directive.
Article 11
Directive 78/319/EEC is hereby repealed with effect from 12 December 1993.
Article 12
This Directive is addressed to the Member States.
Done at Brussels, 12 December 1991.
For the CouncilThe PresidentJ.G.M. ALDERS
(1)OJ N° C 295, 19. 11. 1988, p. 8, and OJ N° C 42, 22. 2. 1990, p. 19.
(2)OJ N° C 158, 26. 6. 1989, p. 238.
(3)OJ N° C 56, 6. 3. 1989, p. 2.
(4)OJ N° L 84, 31. 3. 1978, p. 43.
(5)OJ N° C 122, 18. 5. 1990, p. 2.
(6)OJ N° C 328, 7. 12. 1987, p. 1.
(7)OJ N° L 194, 25. 7. 1975, p. 39.
(8)OJ N° L 78, 26. 3. 1991, p. 32.
(1)OJ N° L 326, 13. 12. 1984, p. 31.
(2)OJ N° L 181, 4. 7. 1986, p. 13.
ANNEX I
CATEGORIES OR GENERIC TYPES OF HAZARDOUS WASTE LISTED ACCORDING TO THEIR NATURE OR THE ACTIVITY WHICH GENERATED THEM (*) (WASTE MAY BE LIQUID, SLUDGE OR SOLID IN FORM)
ANNEX I.A.
Wastes displaying any of the properties listed in Annex III and which consist of:
1. anatomical substances; hospital and other clinical wastes;
2. pharmaceuticals, medicines and veterinary compounds;
3. wood preservatives;
4. biocides and phyto-pharmaceutical substances;
5. residue from substances employed as solvents;
6. halogenated organic substances not employed as solvents excluding inert polymerized materials;
7. tempering salts containing cyanides;
8. mineral oils and oily substances (e.g. cutting sludges, etc.);
9. oil/water, hydrocarbon/water mixtures, emulsions;
10. substances containing PCBs and/or PCTs (e.g. dielectrics etc.);
11. tarry materials arising from refining, distillation and any pyrolytic treatment (e.g. still bottoms, etc.);
12. inks, dyes, pigments, paints, lacquers, varnishes;
13. resins, latex, plasticizers, glues/adhesives;
14. chemical substances arising from research and development or teaching activities which are not identified and/or are new and whose effects on man and/or the environment are not known (e.g. laboratory residues, etc.);
15. pyrotechnics and other explosive materials;
16. photographic chemicals and processing materials;
17. any material contaminated with any congener of polychlorinated dibenzo-furan;
18. any material contaminated with any congener of polychlorinated dibenzo-p-dioxin.
ANNEX I.B.
Wastes which contain any of the constituents listed in Annex II and having any of the properties listed in Annex III and consisting of:
19. animal or vegetable soaps, fats, waxes;
20. non-halogenated organic substances not employed as solvents;
21. inorganic substances without metals or metal compounds;
22. ashes and/or cinders;
23. soil, sand, clay including dredging spoils;
24. non-cyanidic tempering salts;
25. metallic dust, powder;
26. spent catalyst materials;
27. liquids or sludges containing metals or metal compounds;
28. residue from pollution control operations (e.g. baghouse dusts, etc.) except (29), (30) and (33);
29. scrubber sludges;
30. sludges from water purification plants;
31. decarbonization residue;
32. ion-exchange column residue;
33. sewage sludges, untreated or unsuitable for use in agriculture;
34. residue from cleaning of tanks and/or equipment;
35. contaminated equipment;
36. contaminated containers (e.g. packaging, gas cylinders, etc.) whose contents included one or more of the constituents listed in Annex II;
37. batteries and other electrical cells;
38. vegetable oils;
39. materials resulting from selective waste collections from households and which exhibit any of the characteristics listed in Annex III;
40. any other wastes which contain any of the constituents listed in Annex II and any of the properties listed in Annex III.
(*) Certain duplications of entries found in Annex II are intentional.
ANNEX II
CONSTITUENTS OF THE WASTES IN ANNEX I.B. WHICH RENDER THEM HAZARDOUS WHEN THEY HAVE THE PROPERTIES DESCRIBED IN ANNEX III (*)
Wastes having as constituents:
C1 beryllium; beryllium compounds;
C2 vanadium compounds;
C3 chromium (VI) compounds;
C4 cobalt compounds;
C5 nickel compounds;
C6 copper compounds;
C7 zinc compounds;
C8 arsenic; arsenic compounds;
C9 selenium; selenium compounds;
C10 silver compounds;
C11 cadmium; cadmium compounds;
C12 tin compounds;
C13 antimony; antimony compounds;
C14 tellurium; tellurium compounds;
C15 barium compounds; excluding barium sulfate;
C16 mercury; mercury compounds;
C17 thallium; thallium compounds;
C18 lead; lead compounds;
C19 inorganic sulphides;
C20 inorganic fluorine compounds, excluding calcium fluoride;
C21 inorganic cyanides;
C22 the following alkaline or alkaline earth metals: lithium, sodium, potassium, calcium, magnesium in uncombined form;
C23 acidic solutions or acids in solid form;
C24 basic solutions or bases in solid form;
C25 asbestos (dust and fibres);
C26 phosphorus: phosphorus compounds, excluding mineral phosphates;
C27 metal carbonyls;
C28 peroxides;
C29 chlorates;
C30 perchlorates;
C31 azides;
C32 PCBs and/or PCTs;
C33 pharmaceutical or veterinary coumpounds;
C34 biocides and phyto-pharmaceutical substances (e.g. pesticides, etc.);
C35 infectious substances;
C36 creosotes;
C37 isocyanates; thiocyanates;
C38 organic cyanides (e.g. nitriles, etc.);
C39 phenols; phenol compounds;
C40 halogenated solvents;
C41 organic solvents, excluding halogenated solvents;
C42 organohalogen compounds, excluding inert polymerized materials and other substances referred to in this Annex;
C43 aromatic compounds; polycyclic and heterocyclic organic compounds;
C44 aliphatic amines;
C45 aromatic amines C46 ethers;
C47 substances of an explosive character, excluding those listed elsewhere in this Annex;
C48 sulphur organic compounds;
C49 any congener of polychlorinated dibenzo-furan;
C50 any congener of polychlorinated dibenzo-p-dioxin;
C51 hydrocarbons and their oxygen; nitrogen and/or sulphur compounds not otherwise taken into account in this Annex.
(*)Certain duplications of generic types of hazardous wastes listed in Annex I are intentional.
Annex III
PROPERTIES OF WASTES WHICH RENDER THEM HAZARDOUS
H1 'Explosive`: substances and preparations which may explode under the effect of flame or which are more sensitive to shocks or friction than dinitrobenzene.
H2 'Oxidizing`: substances and preparations which exhibit highly exothermic reactions when in contact with other substances, particularly flammable substances.
H3-A 'Highly flammable`:
- liquid substances and preparations having a flash point below 21 °C (including extremely flammable liquids), or - substances and preparations which may become hot and finally catch fire in contact with air at ambient temperature without any application of energy, or - solid substances and preparations which may readily catch fire after brief contact with a source of ignition and which continue to burn or to be consumed after removal of the source of ignition, or - gaseous substances and preparations which are flammable in air at normal pressure, or - substances and preparations which, in contact with water or damp air, evolve highly flammable gases in dangerous quantities.
H3-B 'Flammable`: liquid substances and preparations having a flash point equal to or greater than 21 °C and less than or equal to 55 °C.
H4 'Irritant`: non-corrosive substances and preparations which, through immediate, prolonged or repeated contact with the skin or mucous membrane, can cause inflammation.
H5 'harmful`: substances and preparations which, if they are inhaled or ingested or if they penetrate the skin, may involve limited health risks.
H6 'Toxic`: substances and preparations (including very toxic substances and preparations) which, if they are inhaled or ingested or if they penetrate the skin, may involve serious, acute or chronic health risks and even death.
H7 'Carcinogenic`: substances and preparations which, if they are inhaled or ingested or if they penetrate the skin, may induce cancer or increase its incidence.
H8 'Corrosive`: substances and preparations which may destroy living tissue on contacts.
H9 'Infectious`: substances containing viable micro-organisms or their toxins which are known or reliably believed to cause disease in man or other living organisms.
H10 'Teratogenic`: substances and preparations which, if they are inhaled or ingested or if they penetrate the skin, may induce non-hereditary congenital malformations or increase their incidence.
H11 'Mutagenic`: substances and preparations which, if they are inhaled or ingested or if they penetrate the skin, may induce hereditary genetic defects or increase their incidence.
H12 Substances and preparations which release toxic or very toxic gases in contact with water, air or an acid.
H13 Substances and preparations capable by any means, after disposal, of yielding another substance, e.g. a leachate, which possesses any of the characteristics listed above.
H14 'Ecotoxic`: substances and preparations which present or may present immediate or delayed risks for one or more sectors of the environment.
Notes
1. Attribution of the hazard properties 'toxic` (and 'very toxic`), 'harmful`, 'corrosive` and 'irritant` is made on the basis of the criteria laid down by Annex VI, part I A and part II B, of Council Directive 67/548/EEC of 27 June 1967 of the approximation of laws, regulations and administrative provisions relating to the classification, packaging and labelling of dangerous substances (1), in the version as amended by Council Directive 79/831/EEC (2).
2. With regard to attribution of the properties 'carcinogenic`, 'teratogenic` and 'mutagenic`, and reflecting the most recent findings, additional criteria are contained in the Guide to the classification and labelling of dangerous substances and preparations of Annex VI (part II D) to Directive 67/548/EEC in the version as amended by Commission Directive 83/467/EEC (1).
Test methods
The test methods serve to give specific meaning to the definitions given in Annex III.
The methods to be used are those described in Annex V to Directive 67/548/EEC, in the version as amended by Commission Directive 84/449/EEC (2), or by subsequent Commission Directives adapting Directive 67/548/EEC to technical progress. These methods are themselves based on the work and recommendations of the competent international bodies, in particular the OECD.
(1)OJ N° L 196, 16. 8. 1967, p. 1.
(2)OJ N° L 259, 15. 10. 1979, p. 10.
(1)OJ N° L 257, 16. 9. 1983, p. 1.
(2)OJ N° L 251, 19. 9. 1984, p. 1.
