Invitation to submit comments on a draft Commission Regulation
(2006/C 172/04)
Interested parties may submit their comments within one month of the date of publication of this draft Regulation to:
European Commission
Directorate-General for Competition
Consultation (HT 364)
State aid Registry
B-1049 Brussels
Fax (32-2) 296 12 42
E-mail: stateaidgreffe@ec.europa.eu
Draft Commission Regulation (EC) No …/..
of […]
amending Regulations (EC) No 2204/2002, (EC) No 70/2001 and (EC) No 68/2001 as regards period of application
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 994/98 of 7 May 1998 on the application of Articles 92 and 93 of the Treaty establishing the European Community to certain categories of horizontal State aid [1]
and in particular points (a)(i), (ii),(iv) and b) of Article 1(1) thereof,
Having published a draft of this Regulation [2],
After consulting the Advisory Committee on State Aid,
Whereas:
(1) Commission Regulation (EC) No 2204/2002 of 12 December 2002 on the application of Articles 87 and 88 of the EC Treaty to State aid for employment [3], Commission Regulation (EC) No 70/2001 of 12 January 2001 on the application of Articles 87 and 88 of the EC Treaty to State aid for small and medium-sized enterprises [4] and Commission Regulation (EC) No 68/2001 on the application of Articles 87 and 88 of the EC Treaty to training aid [5] will expire on 31 December 2006. In its State Aid Action Plan [6] the Commission has proposed to regroup these Regulations, in one single block exemption Regulation and possibly add other areas mentioned in Article 1 and 2 of Regulation (EC) No 994/98.
(2) The content of the future block exemption Regulation depends in particular on the results of the public consultations initiated by the State Aid Action Plan and by the Commission consultation document on State aid and Innovation [7]. Discussions with representatives of Member States are also necessary in order to define the categories of aid which might be considered compatible with the Treaty. In order to proceed with the current consultations and the analysis of their results, it is appropriate to extend the validity of Regulations (EC) No 2204/2002, (EC) No 70/2001 and (EC) No 68/2001 until the end of 2007.
(3) Regulations (EC) No. 2204/2002, (EC) No 68/2001and (EC) No 70/2001 should therefore be amended accordingly,
(4) The measures provided for in this Regulation are in accordance with the opinion of the Advisory Committee on State Aids
HAS ADOPTED THIS REGULATION:
Article 1
In Article 11(1) of Regulation (EC) No 68/2001, the second sentence is replaced by the following:
"It shall apply until 31 December 2007"
Article 2
In Article 10(1) of Regulation (EC) No 70/2001, the second sentence is replaced by the following
"It shall apply until 31 December 2007".
Article 3
In Article 8(1) of Regulation (EC) No 2204/2002, the second sentence is replaced by the following:
"It shall apply until 31 December 2007".
Article 4
This Regulation shall enter into force on the twentieth day following that of its publication in the Official Journal of the European Union
This Regulation shall be binding in its entirety and directly applicable in all Member States
Done at Brussels, […]
For the Commission
[…]
Member of the Commission
[1] 0J L 142, 14.5.1998, p. 1.
[2] OJ C ...
[3] OJ L 337, 13.12.2002, p. 3.
[4] OJ L 10, 13.1.2001, p. 33. Regulation as amended by Regulation (EC) No 364/2004 (OJ L 63, 28.2.2004, p. 22).
[5] OJ L 10, 13.1.2001, p. 20. Regulation as amended by Regulation (EC) No 363/2004 (OJ L 63, 28.2.2004, p. 20).
[6] COM/2005/0107 final.
[7] COM/2005/0436 final.
--------------------------------------------------
