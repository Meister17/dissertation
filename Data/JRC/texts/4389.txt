Regulation (EC) No 932/2005 of the European Parliament and of the Council
of 8 June 2005
amending Regulation (EC) No 999/2001 laying down rules for the prevention, control and eradication of certain transmissible spongiform encephalopathies as regards the extension of the period for transitional measures
(Text with EEA relevance)
THE EUROPEAN PARLIAMENT AND THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 152(4)(b) thereof,
Having regard to the proposal from the Commission,
After consulting the European Economic and Social Committee,
After consulting the Committee of the Regions,
Acting in accordance with the procedure laid down in Article 251 of the Treaty [1],
Whereas:
(1) Regulation (EC) No 999/2001 [2] is intended to provide a single legal framework for transmissible spongiform encephalopathies (TSEs) in the Community.
(2) Regulation (EC) No 1128/2003 of the European Parliament and of the Council of 16 June 2003 amending Regulation (EC) No 999/2001 as regards the extension of the period for transitional measures [3] prolonged the period of application of the transitional measures provided for in Regulation (EC) No 999/2001 until 1 July 2005 at the latest.
(3) In order to ensure legal certainty beyond the expiry of the transitional measures provided for in Regulation (EC) No 999/2001 and pending the revision of permanent measures as well as the creation of an overall strategy on TSEs, it is appropriate to extend those transitional measures further until 1 July 2007.
(4) In the interests of legal certainty and in order to protect any legitimate expectations of economic operators, pending the substantive revision of Regulation (EC) No 999/2001, the present Regulation should enter into force on 1 July 2005.
(5) Regulation (EC) No 999/2001 should be amended accordingly,
HAVE ADOPTED THIS REGULATION:
Article 1
The second paragraph of Article 23 of Regulation (EC) No 999/2001 shall be replaced by the following:
Article 2
This Regulation shall enter into force on 1 July 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Strasbourg, 8 June 2005.
For the European Parliament
The President
J. P. Borrell Fontelles
For the Council
The President
N. Schmit
[1] Opinion of the European Parliament of 10 May 2005 (not yet published in the Official Journal) and Council Decision of 30 May 2005.
[2] OJ L 147, 31.5.2001, p. 1. Regulation as last amended by Commission Regulation (EC) No 260/2005 (OJ L 46, 17.2.2005, p. 31).
[3] OJ L 160, 28.6.2003, p. 1.
--------------------------------------------------
