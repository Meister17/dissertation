Information communicated by Member States regarding State aid granted under Commission Regulation (EC) No 68/2001 of 12 January 2001 on the application of Articles 87 and 88 of the EC Treaty to training aid
(2006/C 272/04)
(Text with EEA relevance)
Aid No | XT 15/06 |
Member State | Slovenia |
Title of aid scheme or name of company receiving individual aid | Training programmes No BE01-5022860-2006 (ex N 569/2005 and SI 1 2004) |
Legal basis | Pravilnik o izvajanju ukrepov "Aktivne politike zaposlovanja", v navezavi z Zakonom o zaposlovanju in zavarovanju za primer brezposelnosti |
Annual expenditure planned or overall amount of individual aid granted to the company | 2006: SIT 2003832972 |
Maximum aid intensity | Aid for general training covers up to 50 % of the eligible costs and may be increased by: 20 percentage points where the training is for employees of SMEs,10 percentage points where the training is for employees in areas falling under (a),5 percentage points where the training is for employees in areas falling under (b),10 percentage points where the training is for the hard-to-place unemployed within the meaning of the regulations.Aid for specific training covers up to 25 % of the eligible costs and may be increased by: 10 percentage points where the training is for employees of SMEs,10 percentage points where the training is for employees in areas falling under (a),5 percentage points where the training is for employees in areas falling under (b),10 percentage points where the training is for the hard-to-place unemployed. |
Date of implementation | 7.3.2006 |
Duration of scheme or individual aid award | To 31.12.2006 |
Objective of the aid | General training | Yes |
Specific training | Yes |
Economic sectors concerned | All sectors eligible for training aid | All sectors excl. coalmining |
Name and address of the granting authority | Ministrstvo za delo, družino in socialne zadeve |
Kotnikova 5 SLO-1000 Ljubljana |
Other information | The operator of the scheme is aware of the obligations arising from Articles 5 and 6 of Commission Regulation No 68/2001 on the application of Articles 87 and 88 of the EC Treaty to training aid. The operator of the scheme will notify the European Commission if the amount of aid granted to one enterprise for a single training project exceeds EUR 1000000. The above aid ceiling applies regardless of whether the project is financed entirely from State resources or is partly financed by the Community. |
Aid No | XT 20/05 |
Member State | Italian Republic |
Region | Veneto |
Title of aid scheme or name of company receiving individual aid: | L. 53/2000 art. 6 — D.I. n. 349/V/2004. Funding of projects drawn up on the basis of contractual agreements which provide for reductions in working time |
Legal basis | L. 53/2000 art. 6 — D.I. n. 349/V/2004 D.G.R. n. 706 del 4 marzo 2005 |
Annual expenditure planned or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount | EUR 1437373,56, excluding private-sector contribution. This amount also includes the contribution under the arrangements laid down in Regulation (EC) No 69/2001. |
Loans guaranteed | |
Individual aid | Overall aid amount | |
Loans guaranteed | |
Maximum aid intensity | In conformity with Article 4(2)-(6) of the Regulation | Yes |
Date of implementation | From 13.5.2005 |
Duration of scheme or individual aid award | Until 31.12.2007 |
Objective of aid | General training | Yes |
Specific training | Yes |
Economic sectors concerned | All sectors eligible for training aid (only for sectors not covered by the "de minimis" arrangements, as well as transport, agriculture, fisheries and aquaculture) | Yes |
Name and address of the granting authority | Regione del Veneto — Giunta Regionale |
Dorsoduro 3901 I-30100 Venezia |
Large individual aid grants | In conformity with Article 5 of the Regulation. The measure excludes awards of aid, or requires prior notification to the Commission of awards of aid, if the amount of aid granted to one enterprise for a single training project exceeds EUR 1 million. | Yes |
Aid No | XT 26/06 |
Member State | United Kingdom |
Region | Wales |
Title of aid scheme or name of company receiving individual aid | HCC Awareness Training Scheme (Hybu Cig Cymru — Meat Promotion Wales) |
Legal basis | Industrial Development Act 1982 |
Annual expenditure planned under the scheme or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount | 2006: GBP 0,030 million 2007: GBP 0,020 million |
Loans guaranteed | |
Individual aid | Overall aid amount | |
Loans guaranteed | |
Maximum aid intensity | In conformity with Article 4(2)-(7) of the Regulation | Yes |
Date of implementation | From 1.6.2006 |
Duration of scheme or individual aid award | Until 31.12.2007 — The grant was committed prior to 31 December 2006. Payments against this commitment will continue until 31 December 2007. |
Objective of aid | General training | Yes |
Specific training | |
Economic sectors concerned | Limited to specific sectors | Yes |
—Agriculture | Yes |
Name and address of the granting authority | Meat and Livestock Commission |
PO Box 44, Winterhill House Snowdon Drive Milton Keynes MK6 1AX United Kingdom |
Attention: Michael Fogden |
Large individual aid grants | In conformity with Article 5 of the Regulation | Yes |
Aid No | XT 39/06 |
Member State | Italy |
Title of aid scheme or name of company receiving individual aid | Call for proposals to carry out continuous training activities I-2006 |
Legal basis: | Art. 118 Legge 23 dicembre 2000, pag. 388 Art. 48 Legge 27 dicembre 2002, pag. 289 |
Annual expenditure planned or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount | EUR 7830288 for this invitation |
Loans guaranteed | |
Individual aid | Overall aid amount | |
Loans guaranteed | |
Maximum aid intensity | In conformity with Article 4(2)-(7) of the Regulation | Yes |
Date of implementation | 29.7.2006 |
Duration of scheme or individual aid award | Until approximately 30.6.2007 |
Objective of aid | General training | Yes |
Specific training | Yes |
Economic sectors concerned | All sectors eligible for training aid | Yes |
Name and address of the granting authority | Fondartigianato — Fondo Artigianato Formazione |
Via di Santa Croce in Gerusalemme, 63 I-00185 Roma |
Large individual aid grants | In conformity with Article 5 of the Regulation The measure excludes awards of aid or requires prior notification to the Commission of awards of aid, if the amount of aid granted to one enterprise for a single training project exceeds EUR 1 million. | Yes |
Aid No | XT 41/06 |
Member State | Germany |
Region | Bavaria |
Title of aid scheme or name of company receiving individual aid | Bildungszentrum der Stadt Nürnberg (Volkshochschule) Gewerbemuseumsplatz 1 D-90403 Nürnberg |
Legal basis | Bay HO, VO (EG) Nr. 1260/1999, VO (EG) Nr. 1784/1999, EPPD zu Ziel 2, Programmergänzung zu Ziel 2 |
Annual expenditure planned or overall amount of individual aid granted to the company | Aid scheme | Total | |
Loans guaranteed | |
Individual aid | Overall aid amount | EUR 127586 |
Loans guaranteed | |
Maximum aid intensity | In conformity with Article 4(2)-(7) of the Regulation | Yes |
Date of implementation | 27.7.2006 |
Duration of the scheme or individual aid award | 31.12.2007 |
Objective of aid | General training | Yes |
Specific training | |
Economic sectors concerned | All sectors eligible for training aid | Yes |
Name and address of the granting authority | Zentrum Bayern Familie und Soziales |
Hegelstrasse 2 D-95447 Bayreuth |
Large individual aid grants | In conformity with Article 5 of the Regulation | Yes |
--------------------------------------------------
