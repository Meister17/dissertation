COMMISSION REGULATION (EC) No 142/97 of 27 January 1997 concerning the delivery of information about certain existing substances as foreseen under Council Regulation (EEC) No 793/93 (Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 793/93 of 23 March 1993 on the evaluation and control of the risks of existing substances (1), and in particular Article 12 (2) thereof,
Whereas the Commission needs relevant information on certain substances in order to initiate the review procedure under Articles 69, 84 and 112 of the Accession Treaty of provisions not yet applicable in the new Member States, whereas this information must be available before all the information required by Articles 3 and 4 of Regulation (EEC) No 793/93 is available;
Whereas Article 12 foresees that for certain substances suspected of presenting serious risks to man or the environment, the manufacturers and importers may be obliged to deliver available information;
Whereas Commission Regulation (EEC) No 1488/94 (2) outlines the principles for the assessment of risks to man and the environment of existing substances in accordance with Regulation (EEC) No 793/93;
Whereas, the provisions of this Regulation are in accordance with the opinion of the Committee established pursuant to Article 15 of Regulation (EEC) No 793/93,
HAS ADOPTED THIS REGULATION:
Article 1
The manufacturer(s) and importer(s) of the substances listed in the Annex to this Regulation shall deliver all relevant and available information concerning exposure to man and the environment of these substances to the Commission within four months of the entry in force of this Regulation.
The information relevant to the exposure information concerns the emission of, or exposure to, the chemical to human populations or environmental spheres at various stages during the life cycle of the substance according Article 3 (3) and Annex 1A of Regulation (EC) No 1488/94 where:
- the human populations are workers, consumers and man exposed via the environment;
- the environmental spheres are aquatic, terrestrial and atmosphere, as well as information related to fate of the chemical in waste water treatment plants and it's accumulation in the food chain;
- the life cycle of a substance is seen as manufacture, transport, storage, formulation into a preparation or other processing, use and disposal or recovery.
Article 2
This Regulation shall enter into force on its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 27 January 1997.
For the Commission
Ritt BJERREGAARD
Member of the Commission
(1) OJ No L 84, 5. 4. 1993, p. 1.
(2) OJ No L 161, 29. 6. 1994, p. 3.
ANNEX
>TABLE>
