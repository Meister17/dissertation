[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 25.10.2006
COM(2006) 619 final
2006/0202 (CNS)
Proposal for a
COUNCIL DECISION
on the signature and provisional application of the Agreement between the European Community and the Government of Malaysia on certain aspects of air services
Proposal for a
COUNCIL DECISION
on the conclusion of the Agreement between the European Community and the Government of Malaysia on certain aspects of air services
(presented by the Commission)
EXPLANATORY MEMORANDUM
CONTEXT OF THE PROPOSAL |
110 | Grounds for and objectives of the proposal Following the judgements of the Court of Justice in the so-called “Open Skies” cases, on 5 June 2003 the Council granted the Commission a mandate to open negotiations with third countries on the replacement of certain provisions in existing agreements with a Community agreement[1] (the “horizontal mandate”). The objectives of such agreements are to give all EU air carriers non-discriminatory access to routes between the Community and third countries, and to bring bilateral air service agreements between Member States and third countries in line with Community law. |
120 | General context International aviation relations between Member States and third countries have traditionally been governed by bilateral air services agreements between Member States and third countries, the Annexes to such agreements and other related bilateral or multilateral arrangements. Traditional designation clauses in Member States’ bilateral air services agreements infringe Community law. They allow a third country to reject, withdraw or suspend the permissions or authorisations of an air carrier that has been designated by a Member State but that is not substantially owned and effectively controlled by that Member State or its nationals. This has been found to constitute discrimination against Community carriers established in the territory of a Member State but owned and controlled by nationals of other Member States. This is contrary to Article 43 of the Treaty which guarantees nationals of Member States who have exercised their freedom of establishment the same treatment in the host Member State as that accorded to nationals of that Member State. There are further issues, such as aviation fuel taxation or tariffs introduced by third country air carriers on intra-Community routes, where compliance with Community law should be ensured through amending or complementing existing provisions in bilateral air services agreements between Member States and third countries. |
130 | Existing provisions in the area of the proposal The provisions of the Agreement supersede or complement the existing provisions in the nineteen bilateral air services agreements between Member States and the Government of Malaysia. |
140 | Consistency with the other policies and objectives of the Union The Agreement will serve a fundamental objective of the Community external aviation policy by bringing existing bilateral air services agreements in line with Community law. |
CONSULTATION OF INTERESTED PARTIES AND IMPACT ASSESSMENT |
Consultation of interested parties |
211 | Consultation methods, main sectors targeted and general profile of respondents Member States as well as the industry were consulted throughout the negotiations. |
212 | Summary of responses and how they have been taken into account Comments made by Member States and the industry have been taken into account. |
LEGAL ELEMENTS OF THE PROPOSAL |
305 | Summary of the proposed action In accordance with the mechanisms and directives in the Annex to the ”horizontal mandate”, the Commission has negotiated an agreement with the Government of Malaysia that replaces certain provisions in the existing bilateral air services agreements between Member States and the Government of Malaysia. Article 2 of the Agreement replaces the traditional designation clauses with a Community designation clause, permitting all Community carriers to benefit from the right of establishment. Article 4 (Pricing) resolves conflicts between the existing bilateral air services agreements and Council Regulation No 2409/92 on fares and rates for air services which prohibits third country carriers from being price leaders on air services for carriage wholly within the Community. The services of the European Commission have indicated to the Malaysian authorities that provisions of air services agreements between Member States and the Government of Malaysia which may render ineffective the competition rules applicable to undertakings must be brought into conformity with competition law. To this effect, the Commission services have proposed to include an article in the Horizontal Agreement to ensure compatibility with competition law. The Malaysian authorities have informed the Commission services that they do not wish to include such a provision at this stage, and prefer to address these issues in bilateral negotiations with Member States, rather than in the context of a "horizontal" agreement. The European Commission has informed the Government of Malaysia that it has taken note of its decision and reiterated that issues of incompatibility with competition rules must be resolved. |
310 | Legal basis EC Treaty Art. 80(2), 300(2) |
329 | Subsidiarity principle The proposal is entirely based on the “horizontal mandate” granted by the Council taking into account the issues covered by Community law and bilateral air services agreements. |
Proportionality principle The Agreement will amend or complement provisions in bilateral air services agreements only to the extent necessary to ensure compliance with Community law. |
Choice of instruments |
342 | The Agreement between the Community and the Government of Malaysia is the most efficient instrument to bring all existing bilateral air services agreements between Member States and the Government of Malaysia into conformity with Community law. |
BUDGETARY IMPLICATION |
409 | The proposal has no implication for the Community budget. |
ADDITIONAL INFORMATION |
510 | Simplification |
511 | The proposal provides for simplification of legislation. |
512 | The relevant provisions of bilateral air services agreements between Member States and the Government of Malaysia will be superseded or complemented by provisions in one single Community agreement. |
570 | Detailed explanation of the proposal In accordance with the standard procedure for the signature and conclusion of international agreements, the Council is asked to approve the decisions on the signature and provisional application and on the conclusion of the Agreement between the European Community and the Government of Malaysia on certain aspects of air services and to designate the persons authorised to sign the Agreement on behalf of the Community. |
1. Proposal for a
COUNCIL DECISION
on the signature and provisional application of the Agreement between the European Community and the Government of Malaysia on certain aspects of air services
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 80(2) in conjunction with the first sentence of the first subparagraph of Article 300(2) thereof,
Having regard to the proposal from the Commission[2],
Whereas:
(1) The Council authorised the Commission on 5 June 2003 to open negotiations with third countries on the replacement of certain provisions in existing bilateral agreements with a Community agreement.
(2) On behalf of the Community, the Commission has negotiated an Agreement with the Government of Malaysia on certain aspects of air services in accordance with the mechanisms and directives in the Annex to the Council Decision authorising the Commission to open negotiations with third countries on the replacement of certain provisions in existing bilateral agreements with a Community agreement.
(3) Subject to its possible conclusion at a later date, the Agreement negotiated by the Commission should be signed and provisionally applied.
HAS DECIDED AS FOLLOWS:
Sole Article
1. The President of the Council is hereby authorised to designate the person(s) empowered to sign on behalf of the Community the Agreement between the European Community and the Government of Malaysia on certain aspects of air services subject to its conclusion at a later date.
2. Pending its entry into force, the Agreement shall be applied provisionally from the first day of the first month following the date on which the parties have notified each other of the completion of the necessary procedures for this purpose. The President of the Council is hereby authorised to make the notification provided for in Article 8(2) of the Agreement.
3. The text of the Agreement is attached to this Decision.
Done at Brussels,
For the Council
The President
2006/0202 (CNS)
Proposal for a
COUNCIL DECISION
on the conclusion of the Agreement between the European Community and the Government of Malaysia on certain aspects of air services
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 80(2) in conjunction with the first sentence of the first subparagraph of Article 300(2) and the first subparagraph of Article 300(3) thereof,
Having regard to the proposal from the Commission[3],
Having regard to the opinion of the European Parliament[4],
Whereas:
(1) The Council authorised the Commission on 5 June 2003 to open negotiations with third countries on the replacement of certain provisions in existing bilateral agreements with a Community agreement.
(2) On behalf of the Community, the Commission has negotiated an Agreement with the Government of Malaysia on certain aspects of air services in accordance with the mechanisms and directives in the Annex to the Council Decision authorising the Commission to open negotiations with third countries on the replacement of certain provisions in existing bilateral agreements with a Community agreement.
(3) The Agreement was signed on behalf of the Community on [...] subject to its possible conclusion at a later date, in conformity with Council Decision …/…/EC of [...][5].
(4) The Agreement should be approved,
HAS DECIDED AS FOLLOWS:
Article 1
1. The Agreement between the European Community and the Government of Malaysia on certain aspects of air services is hereby approved on behalf of the Community.
2. The text of the Agreement is attached to this Decision.
Article 2
The President of the Council is hereby authorised to designate the person(s) empowered to make the notification provided for in Article 8(1) of the Agreement.
Done at Brussels,
For the Council
The President
ANNEX
AGREEMENT
between the European Community and the Government of Malaysia
on certain aspects of air services
THE EUROPEAN COMMUNITY
of the one part, and
THE GOVERNMENT OF MALAYSIA (hereinafter Malaysia)
of the other part
(hereinafter referred to as ‘the Parties’)
RECOGNISING that certain provisions of the bilateral air service agreements between Member States of the European Community and Malaysia, which are contrary to European Community law, must be brought into conformity with it in order to establish a sound legal basis for air services between the European Community and Malaysia and to preserve the continuity of such air services,
NOTING that the European Community has exclusive competence with respect to several aspects that may be included in bilateral air service agreements between Member States of the European Community and third countries,
NOTING that under European Community law Community air carriers established in a Member State have the right to non-discriminatory access to air routes between the Member States of the European Community and third countries,
HAVING REGARD to the agreements between the European Community and certain third countries providing for the possibility for the nationals of such third countries to acquire ownership in air carriers licensed in accordance with European Community law,
NOTING that under European Community law air carriers may not, in principle, conclude agreements which may affect trade between Member States of the European Community and which have as their object or effect the prevention, restriction or distortion of competition,
RECOGNISING that provisions in bilateral air service agreements concluded between Member States of the European Community and the Malaysia which (i) require or favour the adoption of agreements between undertakings, decisions by associations of undertakings or concerted practices that prevent, distort or restrict competition between air carriers on the relevant routes; or (ii) reinforce the effects of any such agreement, decision or concerted practice; or (iii) delegate to air carriers or other private economic operators the responsibility for taking measures that prevent, distort or restrict competition between air carriers on the relevant routes, may render ineffective the competition rules applicable to undertakings,
NOTING that it is not a purpose of the European Community, as part of these negotiations, to increase the total volume of air traffic between the European Community and Malaysia, to affect the balance between Community air carriers and air carriers of Malaysia, or to negotiate amendments to the provisions of existing bilateral air service agreements concerning traffic rights.
HAVE AGREED AS FOLLOWS:
ARTICLE 1
General provisions
1. For the purposes of this Agreement, ‘Member States’ shall mean Member States of the European Community.
2. References in each of the agreements listed in Annex 1 to nationals of the Member State that is a party to that agreement shall be understood as referring to nationals of the Member States of the European Community.
3. References in each of the agreements listed in Annex 1 to air carriers or airlines of the Member State that is a party to that agreement shall be understood as referring to air carriers or airlines designated by that Member State.
ARTICLE 2
Designation by a Member State
1. The provisions in paragraphs 2 and 3 of this Article shall supersede the corresponding provisions in the articles listed in Annex 2 (a) and (b) respectively, in relation to the designation of an air carrier by the Member State concerned, its authorisations and permissions granted by Malaysia, and the refusal, revocation, suspension or limitation of the authorisations or permissions of the air carrier, respectively.
2. On receipt of a designation by a Member State, Malaysia shall grant the appropriate authorisations and permissions with minimum procedural delay, provided that:
i. the air carrier is established in the territory of the designating Member State under the Treaty establishing the European Community and has a valid Operating Licence in accordance with European Community law;
ii. effective regulatory control of the air carrier is exercised and maintained by the Member State responsible for issuing its Air Operator’s Certificate and the relevant aeronautical authority is clearly identified in the designation; and
iii. the air carrier has its principal place of business in the territory of the Member State from which it has received the valid Operating Licence; and
iv. the air carrier is owned, directly or through majority ownership, and it is effectively controlled by Member States and/or nationals of Member States, and/or by other States listed in Annex 3 and/or nationals of such other States.
3. Malaysia may refuse, revoke, suspend or limit the authorisations or permissions of an air carrier designated by a Member State where:
i. the air carrier is not established in the territory of the designating Member State under the Treaty establishing the European Community or does not have a valid Operating Licence in accordance with European Community law;
ii. effective regulatory control of the air carrier is not exercised or not maintained by the Member State responsible for issuing its Air Operator’s Certificate, or the relevant aeronautical authority is not clearly identified in the designation; or
iii. the air carrier is not owned, directly or through majority ownership, or it is not effectively controlled by Member States and/or nationals of Member States, and/or by other States listed in Annex 3 and/or nationals of such other States.
iv. the air carrier is already authorised to operate under a bilateral agreement between Malaysia and another Member State and Malaysia can demonstrate that, by exercising traffic rights under this Agreement on a route that includes a point in that other Member State, it would be circumventing restrictions on the traffic rights imposed by that other agreement; or
v. the air carrier designated holds an Air Operators Certificate issued by a Member State and no bilateral air services agreement is in force between Malaysia and that Member State and that Member State has denied traffic rights to the air carriers designated by Malaysia;
In exercising its right under this paragraph, Malaysia shall not discriminate between Community air carriers on the grounds of nationality.
ARTICLE 3
Safety
1. The provisions in paragraph 2 of this Article shall complement the corresponding provisions in the articles listed in Annex 2 (c).
2. Where a Member State (the first Member State) has designated an air carrier whose regulatory control is exercised and maintained by a second Member State, the rights of Malaysia under the safety provisions of the agreement between the first Member State that has designated the air carrier and Malaysia shall apply equally in respect of the adoption, exercise or maintenance of safety standards by that second Member State and in respect of the operating authorisation of that air carrier.
ARTICLE 4
Taxation of aviation fuel
1. The provisions in paragraph 2 of this Article shall complement the corresponding provisions in the articles listed in Annex 2 (d).
2. Notwithstanding any other provision to the contrary, nothing in each of the agreements listed in Annex 2 (d) shall prevent a Member State from imposing, on a non-discriminatory basis, taxes, levies, duties, fees or charges on fuel supplied in its territory for use in an aircraft of a designated air carrier of Malaysia that operates between a point in the territory of that Member State and another point in the territory of that Member State or in the territory of another Member State.
ARTICLE 5
Tariffs for carriage within the European Community
1. The provisions in paragraph 2 of this Article shall complement the corresponding provisions in the articles listed in Annex 2 (e).
2. The tariffs to be charged by the air carrier(s) designated by Malaysia under an agreement listed in Annex 1 containing a provision listed in Annex 2 (e) for carriage wholly within the European Community shall be subject to European Community law.
ARTICLE 6
Compatibility with competition rules
1. Notwithstanding any other provision to the contrary, nothing in each of the agreements listed in Annex 1 shall (i) favour the adoption of agreements between undertakings, decisions by associations of undertakings or concerted practices that prevent, distort or restrict competition; (ii) reinforce the effects of any such agreement, decision or concerted practice; or (iii) delegate to private economic operators the responsibility for taking measures that prevent, distort or restrict competition.
2. The provisions contained in the agreements listed in Annex 1 that are incompatible with paragraph 1 of this Article shall not be applied.
ARTICLE 7
Annexes to the Agreement
The Annexes to this Agreement shall form an integral part thereof.
ARTICLE 8
Revision or amendment
The Parties may, at any time, revise or amend this Agreement by mutual consent.
ARTICLE 9
Entry into force and provisional application
1. This Agreement shall enter into force when the Parties have notified each other in writing that their respective internal procedures necessary for its entry into force have been completed.
2. Notwithstanding paragraph 1, the Parties agree to provisionally apply this Agreement from the first day of the month following the date on which the Parties have notified each other of the completion of the procedures necessary for this purpose.
3. Agreements and other arrangements between Member States and Malaysia which, at the date of signature of this Agreement, have not yet entered into force and are not being applied provisionally are listed in Annex 1 (b). This Agreement shall apply to all such Agreements and arrangements upon their entry into force or provisional application.
ARTICLE 10
Termination
1. In the event that an agreement listed in Annex 1 is terminated, all provisions of this Agreement that relate to the agreement listed in Annex 1 concerned shall terminate at the same time.
2. In the event that all agreements listed in Annex 1 are terminated, this Agreement shall terminate at the same time.
IN WITNESS WHEREOF, the undersigned, being duly authorised, have signed this Agreement.
Done at [….] in duplicate, on this […] day of […, …] in the Czech, Danish, Dutch, English, Estonian, Finnish, French, German, Greek, Hungarian, Italian, Latvian, Lithuanian, Maltese, Polish, Portuguese, Slovak, Slovene, Spanish, Swedish and Bahasa Melayu languages.
FOR THE EUROPEAN COMMUNITY: FOR THE GOVERNMENT OF MALAYSIA:
Annex 1
List of agreements referred to in Article 1 of this Agreement
a) Air service agreements between Malaysia and Member States of the European Community which, at the date of signature of this Agreement, have been concluded, signed and/or are being applied provisionally
- Agreement between the Austrian Federal Government and the Government of Malaysia for air services between and beyond their respective territories, signed at Kuala Lumpur on 22 November 1976, hereinafter referred to “Malaysia – Austria Agreement” in Annex 2;
Modified by Memorandum of Understanding done at Vienna on 23 August 1990;
Last modified by Note Verbale done at Kuala Lumpur on 14 September 1994;
- Accord entre le Gouvernement du Royaume de Belgique et le Gouvernement de Malaisie , relatif aux services aériens entre leurs territoires respectifs et au-delà, signed at Kuala Lumpur on 26 February 1974, hereinafter referred to “Malaysia – Belgium Agreement” in Annex 2;
Modified by Agreed Minutes done at Brussels on 25 July 1978;
Last modified by Agreed Minutes done at Kuala Lumpur on 14 October 1993;
- Agreement between the Government of the Czechoslovak Socialist Republic and the Government of Malaysia for air services between and beyond their respective territories, signed at Prague on 2 May 1973, hereinafter referred to “Malaysia – Czech Republic Agreement” in Annex 2;
To be read together with Memorandum of Understanding signed at Prague on 2 May 1973;
- Agreement between the Government of the Kingdom of Denmark and the Government of Malaysia for air services between and beyond their respective territories, signed at Kuala Lumpur on 19 October 1967, hereinafter referred to “Malaysia – Denmark Agreement” in Annex 2;
- Agreement between the Government of the Republic of Finland and the Government of Malaysia for air services between and beyond their respective territories, signed at Kuala Lumpur on 6 November 1997, hereinafter referred to “Malaysia – Finland Agreement” in Annex 2;
To be read together with Memorandum of Understanding done at Kuala Lumpur on 15 September 1997;
- Accord entre le Gouvernement de la République française et le Gouvernement de Malaysia relatif aux transports aériens, signed at Kuala Lumpur on 22 May 1967, hereinafter referred to “Malaysia – France Agreement” in Annex 2;
- Agreement between the Federal Republic of Germany and Malaysia for Air Services between and beyond their respective territories, signed at Kuala Lumpur on 23 July 1968, hereinafter referred to “Malaysia – Germany Agreement” in Annex 2;
Modified by Confidential Memorandum of Understanding done at Bonn on 13 July 1994;
Modified by Confidential Memorandum of Understanding done at Kuala Lumpur on 10 August 1996;
Modified by Confidential Memorandum of Understanding done at Bonn on 26 May 1999;
Last modified by Confidential Memorandum of Understanding done at Bonn on 30 November 2001, hereinafter referred to “Malaysia-Germany MoU” in Annex 2;
- Agreement between the Government of the Republic of Hungary and the Government of Malaysia for air services between and beyond their territories, signed at Kuala Lumpur on 19 February 1993, hereinafter referred to “Malaysia – Hungary Agreement” in Annex 2;
- Agreement between the Government of Ireland and the Government of Malaysia on air transport, signed at Shannon on 17 February 1992, hereinafter referred to “Malaysia – Ireland Agreement” in Annex 2;
- Agreement between the Government of Malaysia and the Government of Republic of Italy concerning air services, signed at Kuala Lumpur on 23 March 1995, hereinafter referred to “Malaysia – Italy Agreement” in Annex 2;
To be read together with Confidential Memorandum of Understanding done at Rome on 30 November 1994;
Modified by Confidential Memorandum of Understanding done at Kuala Lumpur on 18 July1997;
Modified by Agreed Record of discussions between Malaysia and Italy, done at Rome on 18 May 2005;
Last modified by Memorandum of Understanding done at London on 18 July 2006.
- Air Services Agreement between the Government of Malaysia and the Government of the Grand Duchy of Luxembourg , initialled at Kuala Lumpur on 19 July 2002, as Attachment II of Confidential Memorandum of Understanding, signed at Kuala Lumpur on 19 July 2002; hereinafter referred to “Malaysia – Luxembourg Agreement” in Annex 2;
- Agreement between the Government of Malta and the Government of Malaysia for air services between and beyond their respective territories, signed at Malaysia on 12 October 1993, hereinafter referred to “Malaysia – Malta Agreement” in Annex 2;
To be read together with Memorandum of Understanding done at Valletta on 28 February 1984;
- Agreement between the Government of the Kingdom of the Netherlands and the Government of Malaysia for air services between and beyond their respective territories, signed Kuala Lumpur on 15 December 1966, hereinafter referred to “Malaysia – Netherlands Agreement” in Annex 2;
Modified by Exchange of Notes of 25 March 1988;
Modified by Confidential Memorandum of 23 October 1991;
Modified by Exchange of Notes done at Kuala Lumpur on 10 May 1993;
Last modified by Confidential Memorandum of Understanding attached as Appendix A to the Agreed Minutes done at Kuala Lumpur on 19 September 1995;
Last amended by Exchange of Notes done at Kuala Lumpur on 23 May 1996;
- Agreement between the Government of the Polish People’s Republic and the Government of Malaysia concerning civil air transport, signed at Kuala Lumpur on 24 March 1975, hereinafter referred to “Malaysia – Poland Agreement” in Annex 2;
- Agreement between the Government of Malaysia and the Portuguese Republic for air services between and beyond their respective territories, initialled and attached as Attachment II to the Memorandum of Understanding done at Kuala Lumpur on 19 May 1998, hereinafter referred to “Malaysia – Portugal Agreement” in Annex 2;
- Agreement between the Government of the Republic of Slovenia and the Government of Malaysia for air services between and beyond their respective territories, signed at Ljubljana on 28 October 1997, hereinafter referred to “Malaysia – Slovenia Agreement” in Annex 2;
- Air transport agreement between the Government of Spain and the Government of Malaysia , signed at Kuala Lumpur on 23 March 1993, hereinafter referred to “Malaysia – Spain Agreement” in Annex 2;
- Agreement between the Government of the Kingdom of Sweden and the Government of Malaysia for Air Services between and beyond their respective territories, signed at Kuala Lumpur on 19 October 1967, hereinafter referred to “Malaysia – Sweden Agreement” in Annex 2;
- Agreement between the Government of the United Kingdom of Great Britain and Northern Ireland and the Government of Malaysia for air services between and beyond their respective territories , signed at London on 24 May 1973, hereinafter referred to “Malaysia – UK Agreement” in Annex 2;
Modified by Exchange of Notes done at Kuala Lumpur on 14 September 1993;
Last modified by Memorandum of Understanding done at London on 9 March 2001;
b) Air service agreements and other arrangements initialled or signed between Malaysia and Member States of the European Community which, at the date of signature of this Agreement, have not yet entered into force and are not being applied provisionally
- Draft Air Services Agreement between the Government of the Kingdom of Denmark and the Government of Malaysia , initialled 1997 and 2002, hereinafter referred to “Draft Malaysia – Denmark Agreement” in Annex 2;
- Draft Air Services Agreement between the Government of the Kingdom of Sweden and the Government of Malaysia , initialled 1997 and 2002, hereinafter referred to “Draft Malaysia – Sweden Agreement” in Annex 2;
- Draft Memorandum of Understanding attached as Appendix 1 to the Agreed Record done in Kuala Lumpur on 15 December 2004, modifying the Malaysia-UK Agreement.
Annex 2
List of articles in the agreements listed in Annex 1 and referred to in Articles 2 to 6 of this Agreement
a) Designation by a Member State:
- Article 3 (1-3) of the Malaysia – Austria Agreement;
- Article 2 of the Malaysia – Belgium Agreement;
- Article 3 (1-3) of the Malaysia – Czech Republic Agreement;
- Article II of the Malaysia – Denmark Agreement;
- Article 3 (1-3) of the Malaysia – France Agreement;
- Article 3 (1-3) of the Malaysia – Germany Agreement;
- Article 3 of the Malaysia – Finland Agreement;
- Article 3 (1-3) of the Malaysia – Hungary Agreement;
- Article 3 (1-2) of the Malaysia – Ireland Agreement;
- Article 4 of the Malaysia – Italy Agreement;
- Article 3 of the Malaysia – Malta Agreement;
- Article 3 (1-3) of the Malaysia – Netherlands Agreement;
- Article 3 of the Malaysia – Poland Agreement;
- Article 3 (1-3) of the Malaysia – Portugal Agreement;
- Article 3 (1-3) of the Malaysia – Slovenia Agreement;
- Article 3 of the Malaysia – Spain Agreement;
- Article II of the Malaysia – Sweden Agreement;
- Article 3 (1-3) of the Malaysia – UK Agreement;
(b) Refusal, revocation, suspension or limitation of authorisations or permissions:
- Article 3 (4-7) of the Malaysia – Austria Agreement;
- Article 3 of the Malaysia – Belgium Agreement;
- Article 3 (4-6) of the Malaysia – Czech Republic Agreement;
- Article III of the Malaysia – Denmark Agreement;
- Article 4 of the Malaysia – Finland Agreement;
- Article 3 (4-6) of the Malaysia – France Agreement;
- Article 3 (4-6) of the Malaysia – Germany Agreement;
- Article 3 (4-6) of the Malaysia – Hungary Agreement;
- Article 3 (3-6) of the Malaysia – Ireland Agreement;
- Article 5 of the Malaysia – Italy Agreement;
- Article 4 of the Malaysia – Malta Agreement;
- Article 3 (4-6) of the Malaysia – Netherlands Agreement;
- Article 4 of the Malaysia – Poland Agreement;
- Article 3 (4-6) of the Malaysia – Portugal Agreement;
- Article 3 (4-6) of the Malaysia – Slovenia Agreement;
- Article 4 of the Malaysia – Spain Agreement;
- Article III of the Malaysia – Sweden Agreement;
- Article 3 (4-6) of the Malaysia – UK Agreement;
(c) Safety:
- Article 7 of the Malaysia – Belgium Agreement;
- Annex 3 of the Malaysia – Germany MoU;
- Article 9 of the Malaysia – Hungary Agreement;
- Article 10 of the Malaysia – Italy Agreement;
- Article 6 of the Malaysia – Luxembourg Agreement;
- Article 11 of the Malaysia – Portugal Agreement;
- Article 11 of the Malaysia – Spain Agreement;
- Article 9A of the Malaysia – UK Agreement;
(d) Taxation of aviation fuel:
- Article 4 of the Malaysia – Austria Agreement;
- Article 4 of the Malaysia – Belgium Agreement;
- Article 4 of the Malaysia – Czech Republic Agreement;
- Article IV of the Malaysia – Denmark Agreement;
- Article 5 of the Malaysia – Finland Agreement;
- Article 4 of the Malaysia – France Agreement;
- Article 4 of the Malaysia – Germany Agreement;
- Article 4 of the Malaysia – Hungary Agreement;
- Article 11 of the Malaysia – Ireland Agreement;
- Article 6 of the Malaysia – Italy Agreement;
- Article 9 of the Malaysia – Luxembourg Agreement;
- Article 5 of the Malaysia – Malta Agreement;
- Article 4 of the Malaysia – Netherlands Agreement;
- Article 6 of the Malaysia – Poland Agreement;
- Article 4 of the Malaysia – Portugal Agreement;
- Article 4 of the Malaysia – Slovenia Agreement;
- Article 5 of the Malaysia – Spain Agreement;
- Article IV of the Malaysia – Sweden Agreement;
- Article 6 of the Draft Malaysia – Sweden Agreement;
- Article 4 of the Malaysia – UK Agreement;
(e) Tariffs for carriage within the European Community:
- Article 7 of the Malaysia – Austria Agreement;
- Article 10 of the Malaysia – Belgium Agreement;
- Article 7 of the Malaysia – Czech Republic Agreement;
- Article 10 of the Malaysia – Finland Agreement;
- Article 7 of the Malaysia – France Agreement;
- Article 7 of the Malaysia – Germany Agreement;
- Article VII of the Malaysia – Denmark Agreement;
- Article 7 of the Malaysia – Spain Agreement;
- Article 8 of the Malaysia – Hungary Agreement;
- Article 6 of the Malaysia – Ireland Agreement;
- Article 8 of the Malaysia – Italy Agreement;
- Article 11 of the Malaysia – Luxembourg Agreement;
- Article 10 of the Malaysia – Malta Agreement;
- Article 7 of the Malaysia – Netherlands Agreement;
- Article 10 of the Malaysia – Poland Agreement;
- Article 9 of the Malaysia – Portugal Agreement;
- Article 8 of the Malaysia – Slovenia Agreement;
- Article VII of the Malaysia – Sweden Agreement;
- Article 7 of the Malaysia – UK Agreement;
Annex 3
List of other States referred to in Article 2 of this Agreement
(a) The Republic of Iceland (under the Agreement on the European Economic Area);
(b) The Principality of Liechtenstein (under the Agreement on the European Economic Area);
(c) The Kingdom of Norway (under the Agreement on the European Economic Area);
(d) The Swiss Confederation (under the Agreement between the European Community and the Swiss Confederation on Air Transport)
[1] Council Decision 11323/03 of 5 June 2003 (restricted document)
[2] OJ C , , p. .
[3] OJ C , , p. .
[4] OJ C , , p. .
[5] OJ C , , p. .
