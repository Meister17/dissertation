Commission Regulation (EC) No 975/2006
of 29 June 2006
amending Regulation (EC) No 581/2004 opening a standing invitation to tender for export refunds concerning certain types of butter and Regulation (EC) No 582/2004 opening a standing invitation to tender for exports refunds concerning skimmed milk powder
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1255/1999 of 17 May 1999 on the common organisation of the market in milk and milk products [1], and in particular Article 31(3)(b) and (14) thereof,
Whereas:
(1) According to Article 1(1) of Commission Regulation (EC) No 581/2004 [2] and to Article 1(1) of Commission Regulation (EC) No 582/2004 [3], certain destinations are excluded from the granting of an export refund.
(2) Commission Regulation (EC) No 786/2006 of 24 May 2006 fixing the export refunds for milk and milk products [4] has included Bulgaria and Romania, as from 25 May 2006, under the destination zones L 20 and L 21, listing the destinations not eligible for export refunds. It is therefore necessary to exclude Bulgaria and Romania from the export refunds fixed under Regulation (EC) No 581/2004 and Romania from the export refunds fixed under Regulation (EC) No 582/2004.
(3) Regulations (EC) No 581/2004 and (EC) No 582/2004 should be amended accordingly.
(4) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Milk and Milk Products,
HAS ADOPTED THIS REGULATION:
Article 1
In Article 1(1) of Regulation (EC) No 581/2004, the second subparagraph is replaced by the following:
"The products referred to in the first subparagraph are intended for export for all destinations except Andorra, Bulgaria, Ceuta and Melilla, Gibraltar, Romania, the United States of America and Vatican City."
Article 2
In Article 1 of Regulation (EC) No 582/2004, paragraph 1 is replaced by the following:
"1. A permanent tender is opened in order to determine the export refund on skimmed milk powder referred to in Section 9 of Annex I to Commission Regulation (EEC) No 3846/87 [5] in bags of at least 25 kilograms net weight and containing no more than 0,5 % by weight of added non-lactic matter falling under product code ex 0402 10 19 9000, intended for export to all destinations except Andorra, Bulgaria, Ceuta and Melilla, Gibraltar, Romania, the United States of America and Vatican City.
Article 3
This Regulation shall enter into force on the day following that of its publication in the Official Journal of the European Union.
It shall apply from 4 July 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 29 June 2006.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 160, 26.6.1999, p. 48. Regulation last amended by Regulation (EC) No 1913/2005 (OJ L 307, 25.11.2005, p. 2).
[2] OJ L 90, 27.3.2004, p. 64. Regulation last amended by Regulation (EC) No 409/2006 (OJ L 71, 10.3.2006, p. 5).
[3] OJ L 90, 27.3.2004, p. 67. Regulation last amended by Regulation (EC) No 409/2006.
[4] OJ L 138, 25.5.2006, p. 10.
[5] OJ L 366, 24.12.1987, p. 1."
--------------------------------------------------
