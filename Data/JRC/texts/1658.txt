Commission Regulation (EC) No 26/2005
of 7 January 2005
on the issue of licences for the import of garlic in the quarter from 1 December 2004 to 28 February 2005
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 2200/96 of 28 October 1996 on the common organisation of the market in fruit and vegetables [1],
Having regard to Commission Regulation (EC) No 565/2002 of 2 April 2002 establishing the method for managing the tariff quotas and introducing a system of certificates of origin for garlic imported from third countries [2], and in particular Article 8(2) thereof,
Whereas:
(1) The quantities for which licence applications have been lodged by new importers on 3 and 4 January 2005, pursuant to Article 5(2) of Regulation (EC) No 565/2002 exceed the quantities available for products originating in third countries other than China or Argentina.
(2) It is now necessary to establish the extent to which the licence applications sent to the Commission on 6 January 2005 can be met and to fix, for each category of importer and product origin, the dates until which the issue of certificates must be suspended,
HAS ADOPTED THIS REGULATION:
Article 1
Applications for import licences lodged pursuant to Article 3(1) of Regulation (EC) No 565/2002, on 3 and 4 January 2005 and sent to the Commission on 6 January 2005, shall be met at a percentage rate of the quantities applied for as set out in Annex I hereto.
Article 2
For each category of importer and the origin involved, applications for import licences pursuant to Article 3(1) of Regulation (EC) No 565/2002 relating to the quarter from 1 December 2004 to 28 February 2005 and lodged after 4 January 2005 but before the date in Annex II hereto, shall be rejected.
Article 3
This Regulation shall enter into force on 8 January 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 7 January 2005.
For the Commission
J. M. Silva Rodríguez
Director-General for Agriculture and Rural Development
--------------------------------------------------
[1] OJ L 297, 21.11.1996, p. 1. Regulation as last amended by Commission Regulation (EC) No 47/2003 (OJ L 7, 11.1.2003, p. 64).
[2] OJ L 86, 3.4.2002, p. 11. Regulation as last amended by Regulation (EC) No 537/2004 (OJ L 86, 24.3.2004, p. 9).
--------------------------------------------------
+++++ ANNEX 1 +++++</br>
+++++ ANNEX 2 +++++</br>
