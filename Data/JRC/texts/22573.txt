*****
COMMISSION REGULATION (EEC) No 2589/85
of 13 September 1985
amending Regulation (EEC) No 2264/69 on applications for reimbursement of aid granted by Member States to organizations of fruit and vegetable producers
THE COMMISSION OF THE EUROPEAN
COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 1035/72 of 18 May 1972 on the common organization of the market in fruit and vegetables (1), as last amended by Regulation (EEC) No 1332/84 (2),
Having regard to Council Regulation (EEC) No 449/69 of 11 March 1969 on the reimbursement of aid granted by Member States to organizations of fruit and vegetable producers (3), and in particular Article 7 (3) thereof,
Whereas Council Regulation (EEC) No 3284/83 (4) changed the arrangements under which aid is granted to organizations of fruit and vegetable producers;
Whereas the forms referred to in Commission Regulation (EEC) No 850/80 (5) should therefore be adapted in line with the new provisions;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Committee of the European Agricultural Guidance and Guarantee Fund,
HAS ADOPTED THIS REGULATION:
Sole Article
Annexes I and II to Commission Regulation (EEC) No 2264/69 (6) are hereby replaced by Annexes I and II to this Regulation.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 13 September 1985.
For the Commission
Frans ANDRIESSEN
Vice-President
(1) OJ No L 118, 20. 5. 1972, p. 1.
(2) OJ No L 130, 16. 5. 1984, p. 1.
(3) OJ No L 61, 12. 3. 1969, p. 2.
(4) OJ No L 325, 22. 11. 1983, p. 1.
(5) OJ No L 92, 9. 4. 1980, p. 5.
(6) OJ No L 287, 15. 11. 1969, p. 3.
ANNEX I
A. APPLICATION FOR REIMBURSEMENT IN ACCORDANCE WITH ARTICLE 36 (2) OF REGULATION (EEC) No 1035/72
Member State:
1.2.3 // // // // Serial No // Producer organization // Amount of aid granted in accordance with Article 14 (1), (2) or (3) of Regulation (EEC) No 1035/72 // // //
Aid granted for the first year following the date of recognition
1.2.3 // // // // // //
Aid granted for the second year following the date of recognition
1.2.3 // // // // // //
Aid granted for the third year following the date of recognition
1.2.3 // // // // // //
Aid granted for the fourth year following the date of recognition
1.2.3 // // // // // //
Aid granted for the fifth year following the date of recognition
1.2.3 // // // // // //
Subtotal
1.2.3 // // // // // //
Aid relating to Article 14 (3)
1.2.3 // // // // // // 1,2.3 // Total aid granted // // // // Amount to be reimbursed // // //
B. TABLES RELATING TO AID GRANTED TO PRODUCER ORGANIZATIONS UNDER ARTICLE 14 (1), (2) AND (3) OF REGULATION (EEC) No 1035/72
Serial number (1):
Producer organization:
Date of recognition in accordance with Article 13 (2) of Regulation (EEC) No 1035/72:
Number of members in accordance with Article 3 of Regulation (EEC) No 449/69:
Amount of aid:
Date of granting aid:
Aid granted for the . . . . . year following the date of recognition of the producer organization
B.1. Calculation of the value of production marketed in accordance with Article 14 (1) of Regulation (EEC) No 1035/72
1.2.3.4 // // // // // Products (2) // Production actually marketed in accordance with the first indent of point (b) of Article 13 (1) of Regulation (EEC) No 1035/72 // Average producer price // Value of production marketed // // // // // // 19 . . // 19 . . // // // // // // // // //
B.2. Calculation of the value of production marketed in accordance with Article 14 (2) of Regulation (EEC) No 1035/72
1.2,5.6,9.10 // // // // // Products (2) // Production marketed in accordance with Article 5 of Regulation (EEC) No 449/69 // Producer price in accordance with Article 6 of Regulation (EEC) No 449/69 // Value of production marketed in accordance with Article 4 of Regulation (EEC) No 449/69 // // // // // // 1.2.3.4.5.6.7.8.9.10 // // 19 . . // 19 . . // 19 . . // Average of three years // 19 . . // 19 . . // 19 . . // Average of three years // // // // // // // // // // // 1,9.10 // // // Total // // //
(1) Consecutive numbering.
(2) In this table product means all varieties of the one product: for example, apples, pears . . . .
B.3. Calculation of aid
1.2.3.4.5 // // // // // // Value of production marketed as calculated in form B. 1 and B. 2 // Maximum aid based on the value of production marketed // Amount of the actual cost of formation and administrative operation in accordance with Article 1 (1) of Regulation (EEC) No 2118/78 (1) (2) // Amount of aid granted in accordance with Article 14 (1) and (2) of Regulation (EEC) No 1035/72 // Observations // // // // // // // // (a) // // // // // (b) // // // // // (c) // // // // // (d) // // // // // (e) // // // // // (f) // // // // // (g) // // // // // (h) Total // // // // // // //
(1) OJ No L 246, 8. 9. 1978, p. 11.
(2) To be used only for the producer organizations which qualify for aid in accordance with Article 14 (1) of Regulation (EEC) No 1035/72.
B.4. Calculation of aid in accordance with Article 14 (3) of Regulation (EEC) No 1035/72
1.2 // // // Costs relating to preparatory work // // Costs relating to the drawing up of the memorandum and articles of association // // // // Total // // //
It is confirmed that:
- the organization mentioned above has proper standing as regards the duration and effectiveness of its activities, in accordance with Article 13 (2) of Regulation (EEC) No 1035/72,
- the organization mentioned above, from the date of recognition, keeps specific accounts in respect of the activities for which recognition is sought, in accordance with Article 13 (2) of Regulation (EEC) No 1035/72,
- the value of production marketed is calculated in accordance with the provisions of (EEC) No 449/69 in implementation of Article 14 (1) or (2) of Regulation (EEC) No 1035/72,
- the amount of the cost of formation and administrative operation referred to in Article 14 (1) of Regulation (EEC) No 1035/72 has been approved by the competent authorities of the Member State (1),
- in the event of a merger, the objectives referred to in Article 13 of Regulation (EEC) No 1035/72 will be achieved more effectively,
- the system under which aid is granted is in conformity with Article 12 (1) of Regulation (EEC) No 3284/83,
- the beneficiaries have been informed in a suitable manner of the Community contribution.
Stamp and signature
of the competent authority
(1) To be used only for producer organizations receiving aid for five years in accordance with Article 14 (1a) of Regulation (EEC) No 1035/72.
ANNEX II
INFORMATION PROVIDED BY MEMBER STATE RELATING TO PRODUCER ORGANIZATIONS IN ACCORDANCE WITH THE PROVISIONS OF ARTICLE 13 OF REGULATION (EEC) No 1035/72
Member State:
- Serial Number (1):
- Producer organization (name and address):
- It is confirmed that the date of recognition in accordance with Article 13 (2) of Regulation (EEC) No 1035/72 is the following:
1. By what means is stabilization of prices ensured at the production stage in order to harmonize supply with demand?
2. What are the technical facilities available to producers for packaging and marketing products?
3. How is the obligation imposed on the member producers to supply their total output to the producer organization provided for in the rules and applied in practice?
4. Where appropriate: What are the quantities which member producers themselves are authorized by the producer organization to market?
5. What are the production rules applied by the producer organization (Summary)?
6. What are the marketing rules applied by the producer organization (Summary)?
7. What are the rules compelling associate members to forward the information required by the organization as regards harvests and availability?
8. In the event of a merger, in which way are the objectives referred to in Article 13 of Regulation (EEC) No 1035/72 best achieved?
(1) Consecutive numbering.
