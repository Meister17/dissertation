Opinion of the Advisory Committee on restrictive practices and dominant positions given at its 345th meeting on 18 October 2002 concerning a preliminary draft decision in Case COMP/37.784 — Fine Art Auction Houses
(2005/C 187/13)
1. The Advisory Committee agrees with the Commission that the addressees of the Draft Decision have participated in a continuing agreement or/and a concerted practice concerning the sale by auction of fine art sector contrary to Article 81(1) EC Treaty and 53(1) of the EEA Agreement.
2. The Advisory Committee agrees with the Commission that the relevant product market is the sale through auction of "fine art objects" just as it has been defined in the Draft Decision.
3. The Advisory Committee agrees with the Commission that the geographical market is at least EEA wide.
4. The Advisory Committee agrees with the Commission that the continuing agreement had an appredable effect upon trade between EU Member States and between contracting parties of the EEA Agreement.
5. The Advisory Committee agrees with the Commission on the gravity of the infringement.
6. The Advisory Committee agrees with the Commission on the duration of the infringement.
7. The Advisory Committee agrees with the Commission that there are neither attenuating nor aggravating circumstances.
8. The Advisory Committee agrees with the Commission that Sotheby's has not shown inability to pay in a specific social context.
9. The Advisory Committee agrees with the Commission on the application of the Commission's Leniency Notice.
10. The Advisory Committee asks the Commission to take into account all the other points raised during the discussion.
11. The Advisory Committee recommends the publication of its opinion in the Official Journal of the European Communities.
--------------------------------------------------
