Commission Decision
of 1 December 2004
establishing a model health certificate for non-commercial movements of dogs, cats and ferrets from third countries into the Community
(notified under document number C(2004) 4421)
(Text with EEA relevance)
(2004/824/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to the Regulation (EC) No 998/2003 of the European Parliament and of the Council of 26 May 2003 on the animal health requirements applicable to the non-commercial movement of pet animals and amending Council Directive 92/65/EEC [1], and in particular Article 8(4) thereof,
Whereas:
(1) Regulation (EC) No 998/2003 establishes conditions for non-commercial movements of dogs, cats and ferrets from third countries into the Community. These conditions differ depending on the status of the third country of origin and of the Member State of destination.
(2) Commission Decision 2004/203/EC of 18 February 2004 establishing a model health certificate for non-commercial movements from third countries of dogs, cats and ferrets [2] establishes the model certificate to accompany such animals when entering the Community, for which a Corrigendum [3] has been published.
(3) Commission Decision 2004/539/EC of 1 July 2004 establishing a transitional measure for the implementation of Regulation (EC) No 998/2003 on the animal health requirements applicable to the non-commercial movement of pet animals [4] allows until 1 October 2004 the coexistence of certificates issued in conformity with Regulation (EC) No 998/2003 or with the national rules which were in force before 3 July 2004.
(4) By Council Decision 2004/650/EC of 13 September 2004 amending Regulation (EC) No 998/2003 of the European Parliament and of the Council on the animal health requirements applicable to the non-commercial movement of pet animals to take account of the accession of Malta [5], Malta was added to the list of countries in Part A of Annex II to the Regulation. Consequently specific provisions applying to entries of pet animals into Ireland, Sweden and the United Kingdom should be extended to Malta.
(5) For the sake of clarity Decision 2004/203/EC should be repealed and replaced by this Decision.
(6) In view of the very specific nature of the animals and movements concerned, it is appropriate to facilitate the drawing up and use of the certificate for the veterinarians and travelers concerned.
(7) Since Regulation (EC) No 998/2003 and Decision 2004/203/EC, as replaced by the present Decision, are to apply from 3 July 2004, this Decision should also apply without delay.
(8) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
1. This Decision establishes the model certificate and its conditions of use for non-commercial movements from third countries of pet animals of the species dogs, cats and ferrets, provided for in Article 8(4) of Regulation (EC) No 998/2003.
2. The model certificate is set out in the Annex to this Decision.
Article 2
1. The certificate referred to in Article 1(2) shall be required for non-commercial movements of pet animals of the species dogs, cats and ferrets (the pet animals) coming from:
(a) all third countries and entering into a Member State other than Ireland, Malta, Sweden and the United Kingdom, and
(b) third countries listed in Section 2 of Part B and in Part C of Annex II to Regulation (EC) No 998/2003 and entering into Ireland, Malta, Sweden and the United Kingdom. The certificate shall not be used for animals from or prepared in third countries not listed in Annex II to Regulation (EC) No 998/2003, when moving to Ireland, Malta, Sweden or the United Kingdom, in which case Article 8(1)(b)(ii) of the Regulation shall apply.
2. By way of derogation from paragraph 1, Member States shall authorise the non-commercial movement of dogs, cats and ferrets accompanied by a passport in accordance with the model established by Commission Decision 2003/803/EC [6] from those third countries listed in Section 2 of Part B of Annex II to Regulation (EC) No 998/2003 which have notified the Commission and the Member States of their intention to use the passport instead of the certificate.
3. Notwithstanding the rules applying to movements to Malta, Member States shall accept a certificate of the model in the Annex to Decision 2004/203/EC.
Article 3
1. The certificate referred to in Article 1 shall consist of a single sheet drawn up at least in the language of the Member State of entry and in English. It shall be completed in block letters in the language of the Member State of entry or in English.
2. The certificate referred to in Article 1 shall be issued as follows:
(a) Parts I to V of the certificate shall:
(i) either be completed and signed by an official veterinarian designated by the competent authority of the country of dispatch, or
(ii) be completed and signed by a veterinary surgeon authorised by the competent authority, and subsequently endorsed by the competent authority;
(b) Parts VI and VII, where applicable, shall be completed and signed by a veterinarian authorised to practice veterinary medicine in the country of dispatch.
3. The certificate shall be accompanied by supporting documentation, or a certified copy thereof, including the identification details of the animal concerned, vaccination details and the result of the serological test.
4. The certificate is valid for intra-Community movements for a period of four months from the date of issue or until the date of expiry of the vaccination shown in Part IV, whichever is earlier.
Article 4
The vaccination required in Part IV shall be carried out by use of an inactivated vaccine produced at least in accordance with the standards described in the Manual of Diagnostic Tests and Vaccines for Terrestrial Animals, latest edition, of the Office International des Epizooties.
Article 5
1. Member States shall ensure that the conditions provided for in Article 8(1)(a) of Regulation (EC) No 998/2003 are only applied to pet animals from a third country listed in Section 2 of Part B or in Part C of Annex II to that Regulation undertaking:
- either a direct journey to the Member State of entry, or
- a journey between the third country of dispatch and the Member State of entry including a residence exclusively in one or more countries listed in Section 2 of Part B or in Part C of Annex II to Regulation (EC) No 998/2003.
2. By way of derogation to paragraph 1, the journey may include transiting a third country not listed in Annex II to Regulation (EC) No 998/2003 by air or sea, if the pet animal remains within the perimeter of an international airport in such a country or remains secured within the vessel.
Article 6
Decision 2004/203/EC is repealed.
Article 7
This Decision shall apply from 6 December 2004.
Article 8
This Decision is addressed to the Member States.
Done at Brussels, 1 December 2004.
For the Commission
David Byrne
Member of the Commission
--------------------------------------------------
[1] OJ L 146, 13.6.2003, p. 1. Regulation as last amended by Commission Regulation (EC) No 592/2004 (OJ L 94, 31.3.2004, p. 7).
[2] OJ L 65, 3.3.2004, p. 13. Decision as amended by Decision 2004/301/EC (OJ L 98, 2.4.2004, p. 55).
[3] OJ L 111, 17.4.2004, p. 83.
[4] OJ L 237, 8.7.2004, p. 21.
[5] OJ L 298, 23.9.2004, p. 22.
[6] OJ L 312, 27.11.2003, p. 1.
--------------------------------------------------
+++++ ANNEX 1 +++++</br>
