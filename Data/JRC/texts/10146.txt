Commission Regulation (EC) No 1564/2006
of 19 October 2006
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 20 October 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 19 October 2006.
For the Commission
Jean-Luc Demarty
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 386/2005 (OJ L 62, 9.3.2005, p. 3).
--------------------------------------------------
ANNEX
to Commission Regulation of 19 October 2006 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code [1] | Standard import value |
07020000 | 052 | 70,9 |
096 | 36,7 |
204 | 41,3 |
999 | 49,6 |
07070005 | 052 | 111,8 |
096 | 30,8 |
999 | 71,3 |
07099070 | 052 | 100,9 |
204 | 51,8 |
999 | 76,4 |
08055010 | 052 | 63,6 |
388 | 66,0 |
524 | 57,9 |
528 | 58,6 |
999 | 61,5 |
08061010 | 052 | 95,7 |
066 | 54,3 |
400 | 172,2 |
999 | 107,4 |
08081080 | 388 | 82,4 |
400 | 104,0 |
404 | 100,0 |
800 | 148,2 |
804 | 140,1 |
999 | 114,9 |
08082050 | 052 | 114,0 |
388 | 102,9 |
720 | 57,7 |
999 | 91,5 |
[1] Country nomenclature as fixed by Commission Regulation (EC) No 750/2005 (OJ L 126, 19.5.2005, p. 12). Code "999" stands for "of other origin".
--------------------------------------------------
