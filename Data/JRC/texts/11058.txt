State aid — United Kingdom
(Articles 87 to 89 of the Treaty establishing the European Community)
Commission notice pursuant to Article 88(2) of the EC Treaty, addressed to the other Member States and interested parties
State aid C 16/2005 (ex N 232/2004) — Sale of the Tote
(2006/C 204/04)
(Text with EEA relevance)
By the following letter dated 5 July 2006, the Commission has informed the United Kingdom of its decision to close the procedure laid down in Article 88(2) of the Treaty.
"By decision of 1 June 2005, the Commission initiated the formal investigation procedure referred to in Article 88 (2) EC Treaty in respect of the above aid. The decision to initiate the procedure was communicated to the UK authorities on 1 June 2005 and published in the Official Journal on 8 July 2005 (OJ C 168/41). The UK withdrew its notification of the aid measure in a letter registered on 5 May 2006.
The Commission takes note of the UK Government's statement that it will not pursue the current sale proposal.
The Commission notes that, according to Article 8 of the Council Regulation 659/99 [1], the Member State concerned may withdraw the notification in due time before the Commission has taken a decision on the aid. In cases where the Commission has initiated the formal investigation procedure, the Commission shall close that procedure.
Consequently, the Commission has decided to close the formal investigation procedure under Article 88(2) of the EC Treaty in respect of the relevant aid, recording that the UK has withdrawn its notification."
[1] Council Regulation (EC) No 659/1999 of 22 March 1999 laying down detailed rules for application of Article 93 of the Treaty, OJ L 83, 27.3.1999, p. 1.
--------------------------------------------------
