Action brought on 18 April 2006 — Kingdom of Spain v Council of the European Union
Parties
Applicant: Kingdom of Spain (represented by: N. Díaz Abad, Agent)
Defendant: Council of the European Union
Form of order sought
- annulment of Council Regulation (EC) No 51/2006 of 22 December 2005 [1] fixing for 2006 the fishing opportunities and associated conditions for certain fish stocks and groups of fish stocks, applicable in Community waters and, for Community vessels, in waters where catch limitations are required, in so far as it does not allocate specific quotas to the Spanish fishing fleet in the Community waters of the North Sea and the Baltic Sea;
- order the Council of the European Union to pay the costs.
Pleas in law and main arguments
1. Infringement of the principle of non-discrimination
The Kingdom of Spain believes that, in so far as the contested regulation does not allocate to Spain quotas in the Community waters of the North Sea and the Baltic Sea, it infringes the principle of non-discrimination since, upon expiry of the transitional period laid down in the Act of Accession, the other Member States are granted the right of access to those waters and their resources, whereas the Kingdom of Spain is granted only the right of access to waters.
2. Incorrect interpretation of the Act of Accession of Spain
In regulating the transitional period for Spain concerning fisheries, the Act of Accession does not distinguish between access to waters and access to resources. Moreover, the provisions of the Act of Accession must be interpreted in the light of their context and purpose.
3. Infringement of Article 20(2) of Regulation No 2371/2002 [2]
The failure to allocate to Spain quotas which constitute new fishing opportunities distributed for the first time after expiry of the transitional period laid down in the Act of Accession infringes the above provision.
[1] OJ L 16 of 20.1.2006, p. 1.
[2] Council Regulation (EC) No 2371/2002 of 20 December 2002 on the conservation and sustainable exploitation of fisheries resources under the Common Fisheries Policy (OJ L 358 of 31.12.2002, p. 59).
--------------------------------------------------
