Council Regulation (EC) No 1920/2004
of 25 October 2004
amending Regulation (EC) No 992/95 opening and providing for the administration of Community tariff quotas for certain agricultural and fishery products originating in Norway
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 133 thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) Pursuant to Council Regulation (EC) No 992/95 [1], Community tariff quotas were opened for such products.
(2) Participation of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovakia and Slovenia (hereinafter "the acceding States") in the European Economic Area was agreed by means of the EEA Enlargement Agreement, signed between the Community and its Member States, Iceland, Liechtenstein, and Norway and the acceding States on 14 October 2003.
(3) Pending the completion of the procedures required for the adoption of the EEA Enlargement Agreement, an Agreement in the form of an Exchange of Letters was agreed, which provides for a provisional application of the EEA Enlargement Agreement. That Agreement in the form of an Exchange of Letters was approved by Council Decision 2004/368/EC [2].
(4) The EEA Enlargement Agreement provides for an Additional Protocol to the EC-Norway Free Trade Agreement of 1973 (hereinafter "the Protocol"), which provides for the opening of new Community tariff quotas for certain fishery products. Those new tariff quotas should be opened.
(5) The Protocol provides that drawings on two new tariff quotas are stopped on 15 October of each year from 2005, so that any unused balance is made available exclusively to imports at the end of the year.
(6) Regulation (EC) No 992/95 should therefore be amended accordingly.
(7) Since the EEA Enlargement Agreement takes effect from 1 May 2004, this Regulation should be applicable from the same date and should enter into force without delay,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 992/95 is hereby amended as follows:
1. The following Article 2a is inserted:
"Article 2a
From 2005, on 15 October of each year, drawings on the subquotas with order numbers 09.0760 and 09.0763 shall be stopped.
On the following working day, the unused balances of those quotas shall be made available for imports declared from 1 October of that year within the framework of the subquota with the order number 09.0778 for that year.
From 15 October onwards of each year, any drawings subsequently returned because they are unused shall be made available only to imports declared from 1 October of that year."
2. Annexes I and II are amended in accordance with the Annex hereto.
Article 2
1. For 2004, the annual volumes of the tariff quotas with order numbers 09.0752, 09.0756 and 09.0758 shall be reduced in proportion to the part of the quota period in whole weeks which has elapsed before 1 May 2004.
2. For 2004, the tariff quota with order number 09.0754 shall be opened, for the period from 15 June to 31 December, for a volume of 24800 tonnes.
Article 3
This Regulation shall enter into force on the day following that of its publication in the Official Journal of the European Union.
It shall apply from 1 May 2004.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Luxembourg, 25 October 2004.
For the Council
The President
R. Verdonk
--------------------------------------------------
[1] OJ L 101, 4.5.1995, p. 1. Regulation as last amended by Regulation (EC) No 1329/2003 (OJ L 187, 26.7.2003, p. 1).
[2] OJ L 130, 29.4.2004, p. 1.
--------------------------------------------------
+++++ ANNEX 1 +++++
