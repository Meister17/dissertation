COUNCIL DIRECTIVE of 23 November 1979 amending Directive 75/106/EEC on the approximation of the laws of the Member States relating to the making-up by volume of certain prepackaged liquids (79/1005/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 100 thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas the field of application of Council Directive 76/211/EEC of 20 January 1976 on the approximation of the laws of the Member States relating to the making-up by weight or by volume of certain prepackaged products (4) is different from that of Council Directive 75/106/EEC of 19 December 1974 on the approximation of the laws of the Member States relating to the making-up by volume of certain prepackaged liquids (5);
Whereas, therefore, the fields of application of the two Directives should be aligned as regards the volumes of the prepackages to which they relate;
Whereas at the time of the adoption of Directive 75/106/EEC, the Council, with a view to providing better protection for the consumer, requested the Commission to submit before 31 December 1980 a new proposal reducing the list of nominal volumes in Annex III by eliminating values that were too close to one another;
Whereas Council Directive 71/354/EEC of 18 October 1971 relating to the approximation of the laws of the Member States on units of measurement (6) was last amended by Directive 76/770/EEC (7);
Whereas pursuant to Article 7 (2) of Directive 75/106/EEC, Belgium, Ireland, the Netherlands and the United Kingdom have five years in which to implement that Directive ; whereas this Directive should therefore take account of that time limit;
Whereas certain Member States will find it difficult to reduce the number of nominal volumes ; whereas, therefore, provisions should be made for a transitional period for these Member States which does not, however, impede intra-Community trade in the products in question or jeopardize implementation of this Directive in the other Member States,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Article 1 of Directive 75/106/EEC shall be replaced by the following:
"Article 1
This Directive relates to prepackages containing the liquid products listed in Annex III measured by volume for the purpose of sale in individual quantities of between 5 ml and 10 litres inclusive."
Article 2
Article 2 (2) of Directive 75/106/EEC shall be replaced by the following:
"2. A product is prepacked when it is placed in a package of whatever nature without the purchaser being present and the quantity of product contained in the package has a predetermined value and cannot be altered without the package either being opened or undergoing a perceptible modification."
Article 3
Article 3 (1) of Directive 75/106/EEC shall be replaced by the following:
"1. The prepackages which may be marked with the EEC mark referred to in Section 3.3 of Annex I are those which comply with Annex I." (1)OJ No C 250, 19.10.1977, p. 7. (2)OJ No C 163, 10.7.1978, p. 72. (3)OJ No C 283, 27.11.1978, p. 40. (4)OJ No L 46, 21.2.1976, p. 1. (5)OJ No L 42, 15.2.1975, p. 1. (6)OJ No L 243, 29.10.1971, p. 29. (7)OJ No L 262, 27.9.1976, p. 204.
Article 4
1. Article 4 of Directive 75/106/EEC shall be replaced by the following:
Article 4
1. All prepackages referred to in Article 3 must in accordance with Annex I bear an indication of the volume of liquid, called the "nominal volume of the contents", which they are required to contain.
2. Until the expiry of the periods laid down in Council Directive 71/354/EEC of 18 October 1971 on the approximation of the laws of the Member States relating to units of measurement (1), as last amended by Directive 76/770/EEC (2), the indication of the nominal volume of the contents expressed in SI units of measurement in accordance with section 3.1 of Annex I to this Directive shall, if the United Kingdom or Ireland so desire and on their national territories, be accompanied by an indication of the nominal volume expressed in the equivalent imperial units of measurement, if they are given in Annex I to this Directive."
2. The following footnotes shall be added:
"(1)OJ No L 243, 29.10.1971, p. 29. (2)OJ No L 262, 27.9.1976, p. 204. "
Article 5
Article 5 of Directive 75/106/EEC shall be replaced by the following:
"Article 5
1. Member States may not refuse, prohibit or restrict the placing on the market of prepackages which satisfy the requirements of this Directive on grounds related to the determination of their volumes, the methods by which they have been checked or the nominal volumes where these are set forth in Annex III.
2. As from 1 January 1984, prepackages containing the products listed in Annex III 1 (a) may be marketed only if they have the nominal volumes laid down in Annex III.
Until 31 December 1983, Member States may allow all values allowed up to that date on their markets.
3. However, (a) for prepackages with the nominal volumes listed in Annex III, column II, paragraph 1 shall apply until 31 December 1988 only to those countries which allowed such prepackages on 31 December 1973, with the exception of prepackages of category 1 (a) "Wines" with a nominal volume of 0 773 l, for which the expiry date is 31 December 1985;
(b) with respect to the liquids specified in Sections 1 (a), 1 (b) and 4 of Annex III, paragraph 1 shall apply only when such liquids are presented in packages having one of the nominal volumes listed in the corresponding columns of that Annex and complying with the rules on trade practices of the Member State of origin of the liquid, regardless of whether filling took place in the Member State of origin or in another State.
4. The provisions of this Directive shall be no impediment to national laws governing on environmental grounds the use of packaging with regard to its recycling."
Article 6
Section 2.4 of Annex I to Directive 75/106/EEC shall be replaced by the following:
"2.4 The tolerable negative error shall be fixed in accordance with the following table: >PIC FILE= "T0015610">
When using the table, the values of the tolerable negative errors shown as percentages in the table, calculated in units of volume, shall be rounded up to the nearest one-tenth of a millilitre."
Article 7
The second paragraph of section 3.1 of Annex I to Directive 75/106/EEC shall be replaced by the following:
"Until the expiry of the periods laid down in Directive 71/354/EEC, as amended by Directive 76/770/EEC, the indication of the nominal volume of the contents expressed in SI units in accordance with the first paragraph may be accompanied by that of the equivalent value in imperial (UK) units of measurement, calculated by applying the following conversion factors:
one millilitre = 0 70352 fluid ounce
one litre = 1 7760 pints or 0 7220 gallon."
Article 8
Annex III to Directive 75/106/EEC shall be replaced by the Annex to this Directive.
Article 9
1. Member States shall adopt and publish before 1 January 1981 the laws, regulations and administrative provisions needed to comply with this Directive, and these shall enter into force on 1 January 1981.
2. Member States shall ensure that the texts of the main provisions of national law which they adopt in the field covered by this Directive are communicated to the Commission.
Article 10
This Directive is addressed to the Member States.
Done at Brussels, 23 November 1979.
For the Council
The President
R. Mac SHARRY
ANNEX
"ANNEX III
>PIC FILE= "T0015611"> >PIC FILE= "T0015612">
