Commission Decision
of 30 November 2006
repealing Commission Decision 2005/613/EC accepting an undertaking offered in connection with the anti-dumping proceeding concerning imports of polyester staple fibres originating, inter alia, in Saudi Arabia
(notified under document number C(2006) 5776)
(2006/864/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 384/96 of 22 December 1995 on protection against dumped imports from countries not members of the European Community [1], and in particular Articles 8 and 9 thereof,
After consulting the Advisory Committee,
Whereas:
A. PREVIOUS PROCEDURE
(1) In March 2005, the Council, by Regulation (EC) No 428/2005 [2], imposed a definitive anti-dumping duty on imports of polyester staple fibres originating in the People's Republic of China and Saudi Arabia, amended the definitive anti-dumping duties in force on imports of the same product originating in the Republic of Korea and terminated the anti-dumping proceeding in respect to imports of the same product originating in Taiwan.
(2) The Commission, by Decision 2005/613/EC [3], accepted a price undertaking offered by the Saudi Arabian company Saudi Basic Industries Corporation (Sabic) (the "Company") and all its related companies, including the related producer of the product concerned, Arabian Industrial Fibres Company (Ibn Rushd).
B. VOLUNTARY WITHDRAWAL OF AN UNDERTAKING
(3) The Company advised the Commission in August 2006 that it wished to withdraw its undertaking.
C. REPEAL OF DECISION 2005/613/EC
(4) In view of the above, the acceptance of the undertaking should be withdrawn and Commission Decision 2005/613/EC should be repealed,
HAS DECIDED AS FOLLOWS:
Article 1
Commission Decision 2005/613/EC is hereby repealed.
Article 2
This Decision shall take effect on the day following that of its publication in the Official Journal of the European Union.
Done at Brussels, 30 November 2006.
For the Commission
Peter Mandelson
Member of the Commission
[1] OJ L 56, 6.3.1996, p. 1. Regulation as last amended by Regulation (EC) No 2117/2005 (OJ L 340, 23.12.2005, p. 17).
[2] OJ L 71, 17.3.2005, p. 1. Regulation as amended by Regulation (EC) No 1333/2005 (OJ L 211, 13.8.2005, p. 1).
[3] OJ L 211, 13.8.2005, p. 20.
--------------------------------------------------
