Commission Regulation (EC) No 1228/2006
of 14 August 2006
amending for the 69th time Council Regulation (EC) No 881/2002 imposing certain specific restrictive measures directed against certain persons and entities associated with Usama bin Laden, the Al-Qaida network and the Taliban, and repealing Council Regulation (EC) No 467/2001
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 881/2002 of 27 May 2002 imposing certain specific restrictive measures directed against certain persons and entities associated with Usama bin Laden, the Al-Qaida network and the Taliban, and repealing Council Regulation (EC) No 467/2001 prohibiting the export of certain goods and services to Afghanistan, strengthening the flight ban and extending the freeze of funds and other financial resources in respect of the Taliban of Afghanistan [1], and in particular the first indent of Article 7(1), thereof,
Whereas:
(1) Annex I to Regulation (EC) No 881/2002 lists the persons, groups and entities covered by the freezing of funds and economic resources under that Regulation.
(2) On 2 August 2006, the Sanctions Committee of the United Nations Security Council decided to amend the list of persons, groups and entities to whom the freezing of funds and economic resources should apply. Annex I should therefore be amended accordingly.
(3) In order to ensure that the measures provided for in this Regulation are effective, this Regulation must enter into force immediately,
HAS ADOPTED THIS REGULATION:
Article 1
Annex I to Regulation (EC) No 881/2002 is hereby amended as set out in the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 14 August 2006.
For the Commission
Eneko Landáburu
Director-General for External Relations
[1] OJ L 139, 29.5.2002, p. 9. Regulation as last amended by Commission Regulation (EC) No 1217/2006 (OJ L 220, 11.8.2006, p. 9).
--------------------------------------------------
ANNEX
Annex I to Regulation (EC) No 881/2002 is amended as follows:
(1) The following entry shall be added under the heading "Legal persons, groups and entities":
"International Islamic Relief Organisation, Philippines, Branch offices (alias (a) International Islamic Relief Agency, (b) International Relief Organisation, (c) Islamic Relief Organization, (d) Islamic World Relief, (e) International Islamic Aid Organisation, (f) Islamic Salvation Committee, (g) The Human Relief Committee of the Muslim World League, (h) World Islamic Relief Organisation, (i) Al Igatha Al-Islamiya, (j) Hayat al-Aghatha al-Islamia al-Alamiya, (k) Hayat al-Igatha, (l) Hayat Al-'Igatha, (m) Ighatha, (n) Igatha, (o) Igassa, (p) Igasa, (q) Igase, (r) Egassa, (s) IIRO). Address: (a) International Islamic Relief Organisation, Philippines Office, 201 Heart Tower Building; 108 Valero Street; Salcedo Village, Makati City; Manila, Philippines, (b) Zamboanga City, Philippines, (c) Tawi Tawi, Philippines, (d) Marawi City, Philippines, (e) Basilan, Philippines, (e) Cotabato City, Philippines."
(2) The following entry shall be added under the heading "Natural persons":
"Abd Al Hamid Sulaiman Al-Mujil (alias (a) Dr Abd al-Hamid Al-Mujal, (b) Dr Abd Abdul-Hamid bin Sulaiman Al-Mu’jil, (c) Abd al-Hamid Sulaiman Al-Mu’jil, (d) Dr Abd Al-Hamid Al-Mu’ajjal, (e) Abd al-Hamid Mu’jil, (f) A.S. Mujel, (g) Abu Abdallah). Date of birth: 28.4.1949. Nationality: Saudi Arabian."
--------------------------------------------------
