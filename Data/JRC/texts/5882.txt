[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 3.8.2005
COM(2005) 361 final
2005/0147 (COD)
Proposal for a
DIRECTIVE OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL
repealing Council Directive 90/544/EEC on the frequency bands designated for the coordinated introduction of pan-European land-based public radio paging in the Community
(presented by the Commission)
EXPLANATORY MEMORANDUM
CONTEXT OF THE PROPOSAL |
110 | Grounds for and objectives of the proposal This proposal aims at repealing Directive 90/544/EEC of 9 October 1990 on the frequency bands designated for the coordinated introduction of pan-European land-based public radio paging in the Community (ERMES Directive). Upon such repeal, the Commission will adopt a new harmonised spectrum plan for this band to meet EC policy objectives pursuant to Decision 676/2002/EC of the European Parliament and the Council of 7 March 2002 on a regulatory framework for radio spectrum policy in the European Community (Radio Spectrum Decision). |
120 | General context The ERMES Directive specifying spectrum use for pan-European land-based public radio paging services (ERMES) no longer meets EC policy needs as it requires reservation in the 169.4 to 169.8 MHz band of four channels for ERMES and of the whole band according to commercial demand. Use of the band by ERMES has decreased or even ceased and ERMES has been replaced by other technologies such as short messaging systems (SMS) over GSM. As harmonised use of the band may support other applications of EC policy interest, the Commission issued in 2003 a mandate to the European Conference of Postal and Telecommunications Administrations (CEPT) pursuant to the Radio Spectrum Decision to collect information on possible uses of the band, identify options for EC policy needs, and propose technical and regulatory solutions, considering electronic communications and other EC objectives such as social inclusion of people with disabilities. |
130 | Existing provisions in the area of the proposal This matter is governed by the ERMES Directive to be repealed; a new harmonised plan should be adopted by the Commission. |
140 | Consistency with other policies and objectives of the Union Upon the ERMES directive repeal, the Commission will adopt a new frequency plan and channel arrangement allowing for six types of applications to share the band to meet EC needs in consistency with other EU policies and objectives. These applications include hearing aids for hearing impaired people, which would improve travelling conditions in the EC and benefit from economies of scale and price decreases; social alarms allowing elderly people to send alarm messages; asset tracking or tracing devices to track stolen goods in the EC; meter reading systems for water and electricity utility companies, and existing paging systems such as ERMES and private mobile radio systems. Framework Directive 2002/21/EC requires the needs of special social groups of citizens such as disabled users to be addressed; the Vitoria Informal Council of Ministers for telecommunications required that accessibility to all electronic services be ensured for disabled or older persons, and eEurope 2005 action plan adopted by European Council on 21-22 June 2002 in Sevilla underlined importance of access for all citizens to online public services, TV and mobile phones, in particular broadband access, spectrum policies and multi-platform content. |
CONSULTATION OF INTERESTED PARTIES AND IMPACT ASSESSMENT |
Consultation of interested parties |
211 | Consultation methods, main sectors targeted and general profile of respondents Use of the 169.4 to 169.8 MHz band has been constantly reviewed by the Commission. In 1998, a study was conducted for the Commission on radio paging markets in Europe, including 149 interviews with industry, users and authorities. Based on Commission’s mandate, CEPT inquired from 2002 to 2004 on current and future uses of the band; information and applications proposals were collected through interviews. Questionnaires were sent and opinions sought from national authorities, the European Telecommunications Standardisation Institute, industry, user groups, disabled people associations and other interested parties. |
212 | Summary of responses and how they have been taken into account The 1998 study showed that use of ERMES was not developing and had ceased to be developed by several manufacturers. In its 2004 report to the Commission, CEPT concluded that use of the band for ERMES had strongly diminished and should be reorganised for other applications. Retained applications are those most relevant from both a technical and EC policy point of view; these include social alarms, meter readers, tracking and asset tracing systems, paging systems, private mobile radio communications and hearing aids. |
Collection and use of expertise |
221 | Scientific/expertise domains concerned Spectrum management, electronic communications; technical expertise of CEPT. |
222 | Methodology used The Commission issued a mandate to CEPT who produced a report which the Commission, assisted by the Radio Spectrum Committee, has examined. |
223 | Main organisations/experts consulted National authorities, ETSI, ERMES users and potential users of alternative applications, hearing impaired people associations, communications operators, manufacturers and other interested parties. |
2249 | Summary of advice received and used Existence of potentially serious risks with irreversible consequences has not been mentioned. |
225 | Commission noted from CEPT report that use of the band for ERMES had considerably decreased and that six types of applications could share the band to meet EC policy needs. To use the 169.4 to 169.8 MHz band most efficiently, ERMES Directive should be repealed and a new harmonised plan be introduced by Commission decision. |
226 | Means used to make the expert advice publicly available CEPT report is at: https://forum.europa.eu.int/Members/irc/infso/radiospectrum/library?l=/members/documentsv2004/rscom04-69_ermesdoc/_EN_1.0_ |
230 | Impact assessment The Commission envisaged various solutions for spectrum harmonisation for EC policies. Self or co-regulation or regulation by national authorities could not repeal ERMES Directive to ensure timely and harmonised introduction of a new plan to meet EC policy objectives. A Council and EP Directive is necessary to repeal the ERMES Directive; a Commission decision would then be adopted with a new harmonisation plan prepared by the Commission with CEPT technical expertise, assisted by the Radio Spectrum Committee. Positive social impacts of new harmonised plan will include increased social inclusion and mobility of hearing impaired people and elderly in EC, protection against theft, safety of more efficient monitoring capabilities by utilities companies and appropriate electronic communications tools. The new plan would benefit various EC sectors and businesses including radio communications equipment, water and electricity sectors, communications services, theatres, exhibition and conference halls, schools and social care entities. Positive economic impacts include reduction in prices for hearing aids and social alarms, increased employment and travelling possibilities for impaired people, lower insurance costs and increased retrieval of stolen goods, lower monitoring and operating costs for utility companies, new manufacturing, investment and services opportunities, new types of applications, increased competitiveness and economies of scale with radio equipment operational throughout EC. There are no particular relevant environmental impacts. |
LEGAL ELEMENTS OF THE PROPOSAL |
305 | Summary of the proposed action The proposal is to repeal the ERMES Directive so that a new plan of the 169.4 to 169.8 MHz band could meet current and new EC policy needs, to be established by a Commission decision adopted pursuant to the Radio Spectrum Decision. |
310 | Legal basis Article 95 EC Treaty. |
320 | Subsidiarity principle The subsidiarity principle applies insofar as the proposal does not fall under the exclusive competence of EC. |
The objectives of the proposal cannot be sufficiently achieved by Member States for the following reasons. |
321 | Ensuring harmonised use of the 169.4 to 169.8 MHz band to fulfil EC policy needs cannot be done satisfactorily by Member States acting individually and can be better achieved at EC level by internal market measures under the Radio Spectrum Decision. Adoption of new plan requires repeal of ERMES directive. |
323 | Repeal of ERMES Directive can only be done at EC level by a directive. The new harmonised plan needs to be adopted at EC level to ensure that EC policy objectives are met in a timely and harmonised way. Without such new harmonised plan, new applications with social and economic advantages for Member States could not appear. |
EC action will better achieve the proposal objectives for following reasons. |
324 | Repeal of ERMES Directive and replacement of the plan by another EC binding measure are necessary to ensure timely and harmonised introduction of the new plan in Member States. Without such EC measure, no harmonised and timely solution can be guaranteed. |
325 | ERMES directive no longer meets EC needs. Harmonised use of 169.4 to 169.8 MHz band may support other applications fulfilling current EC policy objectives. |
327 | Member States cannot repeal the ERMES directive and cannot ensure that a new spectrum plan be introduced in a timely and harmonised way at EC level. |
The proposal therefore complies with the subsidiarity principle. |
Proportionality principle The proposal complies with the proportionality principle for the following reasons. |
331 | The ERMES directive will be repealed and new spectrum plan introduced by the Commission. Only spectrum strictly necessary for EC applications will be harmonised. Applications which do not need harmonised spectrum will not be harmonised at EC level. Considering evolution of technology and consumers needs, the band would be kept under review. |
332 | The new plan was elaborated with CEPT technical expertise and national experts without new unnecessary financial or administrative burden at EC, national or regional levels. The new plan will offer economies of scale as well as additional and more effective services in EC to operators and users. |
Choice of instruments |
341 | Proposed instruments: directive. |
342 | Other means would not be adequate for the following reason(s). Only a directive can repeal the ERMES directive. The plan of that directive will be replaced by a new effective and more appropriate plan based on a Commission decision under the Radio Spectrum Decision. |
BUDGETARY IMPLICATION |
409 | The proposal has no implication for EC budget. |
ADDITIONAL INFORMATION |
510 | Simplification |
511 | The proposal provides for simplification of legislation, of administrative procedures for public authorities (EU or national), and of administrative procedures for private parties. |
512 | Upon repeal of the Directive, adoption of the new plan by the Commission will relieve European Parliament and Council from technical work on spectrum management. |
513 | Spectrum harmonisation will ease the task of national authorities with a clear table for allocation of spectrum for harmonised applications. |
514 | Harmonised spectrum will simplify manufacture, sale and use of new applications in EC and open new opportunities for users with special needs. |
516 | The proposal is included in the Commission's rolling programme for up-date and simplification of the acquis communautaire. |
520 | Repeal of existing legislation Adoption of the proposal will lead to repeal of existing legislation. |
560 | European Economic Area The proposed act concerns an EEA matter and should therefore extend to the European Economic Area. |
1. 2005/0147 (COD)
Proposal for a
DIRECTIVE OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL
repealing Council Directive 90/544/EEC on the frequency bands designated for the coordinated introduction of pan-European land-based public radio paging in the Community (Text with EEA relevance)
THE EUROPEAN PARLIAMENT AND THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 95 thereof,
Having regard to the proposal from the Commission[1],
Having regard to the opinion of the European Economic and Social Committee[2],
Having regard to the opinion of the Committee of the Regions[3],
Acting in accordance with the procedure laid down in Article 251 of the Treaty[4],
Whereas:
(1) Council Directive 90/544/EEC of 9 October 1990 on the frequency bands designated for the coordinated introduction of pan-European land-based public radio paging in the Community[5] required Member States to designate, by 31 December 1992, in the 169.4 to 169.8 MHz radio spectrum band four channels for the pan-European land-based public radio paging service (hereinafter “ERMES”) and to prepare, as quickly as possible, plans to enable pan-European public radio paging service to occupy the whole band 169.4 to 169.8 MHz according to commercial demand.
(2) Use of the 169.4 to 169.8 MHz spectrum band for ERMES in the Community has decreased or even ceased, so that this band is not currently being efficiently utilised by ERMES and could be better used to fulfil other Community policy needs.
(3) Decision No 676/2002/EC of the European Parliament and the Council of 7 March 2002 on a regulatory framework for radio spectrum policy in the European Community (Radio Spectrum Decision)[6] established a Community policy and legal framework to ensure coordination of policy approaches and, where appropriate, harmonised conditions with regard to availability and efficient use of the spectrum band necessary for establishment and functioning of the internal market. This Decision allows the Commission to adopt technical implementing measures to ensure harmonised conditions for the availability and efficient use of the spectrum band.
(4) Since the 169.4 to 169.8 MHz band is appropriate for applications benefiting people with impairments or disabilities, and considering that promotion of such applications is a policy objective for the Community together with the general objective of ensuring the functioning of the internal market, the Commission pursuant to Article 4(2) of the Radio Spectrum Decision issued a mandate to the European Conference of Postal and Telecommunications Administration (hereinafter “the CEPT”) to examine among others applications related to assistance for people with disabilities.
(5) As mandated, the CEPT produced a new frequency plan, and a channel arrangement allowing six types of preferred applications to share the band in order to meet several Community policy needs.
(6) For these reasons and in accordance with the objectives of the Radio Spectrum Decision, Directive 90/544/EEC should be repealed.
HAVE ADOPTED THIS DIRECTIVE:
Article 1
Directive 90/544/EEC is repealed with effect from […].
Article 2
This Directive shall enter into force on the day of its publication in the Official Journal of the European Union .
Article 3
This Directive is addressed to the Member States.
Done at Brussels,
For the European Parliament For the Council
The President The President
[1] OJ C , , p. .
[2] OJ C , , p. .
[3] OJ C , , p. .
[4] OJ C , , p. .
[5] OJ L 310, 9.11.1990, p.28.
[6] OJ L 108, 24.4.2002, p. 1.
