COMMISSION REGULATION (EEC) No 936/79 of 11 May 1979 on the classification of meat of bovine animals under subheading 02.01 A II a) 4 aa) of the Common Customs Tariff
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 97/69 of 16 January 1969 on measures to be taken for uniform application of the nomenclature of the Common Customs Tariff (1), as last amended by Regulation (EEC) No 280/77 (2), and in particular Article 3 thereof,
Whereas, in order to ensure uniform application of the nomenclature of the Common Customs Tariff, provisions must be laid down concerning the tariff classification of separated forequarters of bovine animals, in a fresh or chilled state, from which the atlas bone has been removed;
Whereas subheading 02.01 A II a) 4 aa) of the Common Customs Tariff annexed to Council Regulation (EEC) No 950/68 (3), as last amended by Regulation (EEC) No 882/79 (4), refers to meat of bovine animals, fresh or chilled, unboned (bone-in);
Whereas Note 1 A e) to Chapter 2 of the Common Customs Tariff precisely defines separated forequarters, indicating in particular that they are the front part of a half-carcass comprising all the bones ; whereas such products which do not include the atlas bone cannot therefore be considered to be separated forequarters;
Whereas the products in question are to be classified as meat of bovine animals, fresh or chilled, other, unboned (bone-in) under subheading 02.01 A II a) 4 aa);
Whereas the measures proved for in this Regulation are in accordance with the opinion of the Committee on Common Customs Tariff Nomenclature,
HAS ADOPTED THIS REGULATION:
Article 1
Separated forequarters of bovine animals in a fresh or chilled state, from which the atlas bone has been removed, fall to be classified under the following subheading of the Common Customs Tariff:
02.01 Meat and edible offals of the animals falling within heading No 01.01, 01.02, 01.03 or 01.04, fresh, chilled or frozen:
A. Meat:
II. Of bone animals:
a) Fresh or chilled:
4. Other:
aa) Unboned (bone-in)
Article 2
This Regulation shall enter into force on the 21st day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 11 May 1979.
For the Commission
Étienne DAVIGNON
Member of the Commission (1)OJ No L 14, 21.1.1969, p. 1. (2)OJ No L 40, 11.2.1977, p. 1. (3)OJ No L 172, 22.7.1968, p. 1. (4)OJ No L 111, 4.5.1979, p. 14.
