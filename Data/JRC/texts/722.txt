Commission Regulation (EC) No 1904/2000
of 7 September 2000
supplementing the Annex to Regulation (EC) No 2400/96 on the entry of certain names in the "Register of protected designations of origin and protected geographical indications" provided for in Council Regulation (EEC) No 2081/92 on the protection of geographical indications and designations of origin for agricultural products and foodstuffs
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2081/92 of 14 July 1992 on the protection of geographical indications and designations of origin for agricultural products and foodstuffs(1), as last amended by Commission Regulation (EC) No 1509/2000(2), and in particular Article 6(3) and (4) thereof,
Whereas:
(1) Under Article 5 of Regulation (EEC) No 2081/92, Italy has sent the Commission applications for the registration of certain names as designations of origin and geographical indications.
(2) In accordance with Article 6(1) of that Regulation, the applications have been found to meet all the requirements laid down therein and in particular to contain all the information required in accordance with Article 4 thereof.
(3) No statements of objection have been received by the Commission under Article 7 of that Regulation in respect of the names given in the Annex to this Regulation following their publication in the Official Journal of the European Communities(3).
(4) The names should therefore be entered in the "Register of protected designations of origin and protected geographical indications" and hence be protected throughout the Community as protected designations of origin or protected geographical indications.
(5) The Annex to this Regulation supplements the Annex to Commission Regulation (EC) No 2400/96(4), as last amended by Regulation (EC) No 1651/2000(5),
HAS ADOPTED THIS REGULATION:
Article 1
The names in the Annex hereto are added to the Annex to Regulation (EC) No 2400/96 and entered as protected designations of origin (PDO) or protected geographical indications (PGI) in the "Register of protected designations of origin and protected geographical indications" provided for in Article 6(3) of Regulation (EEC) No 2081/92.
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 7 September 2000.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 208, 24.7.1992, p. 1.
(2) OJ L 174, 13.7.2000, p. 7.
(3) OJ C 347, 3.12.1999, p. 2 and
OJ C 358, 10.12.1999, p. 2.
(4) OJ L 327, 18.12.1996, p. 11.
(5) OJ L 189, 27.7.2000, p. 15.
ANNEX
PRODUCTS LISTED IN ANNEX I TO THE EC TREATY, INTENDED FOR HUMAN CONSUMPTION
Fruit, vegetables and cereals
ITALY
Castagna del Monte Amiata (PGI)
La Bella della Daunia (PDO)
