COUNCIL DECISION of 26 April 1997 concerning the conclusion of the Agreement between the European Community and the Republic of Korea on cooperation and mutual administrative assistance in customs matters (97/291/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 113 thereof, in conjunction with the first sentence of Article 228 (2) thereof,
Having regard to the proposal from the Commission,
Whereas on 5 April 1993 the Council authorized the Commission to negotiate, on behalf of the Community, customs cooperation agreements with some of the Community's main trading partners;
Whereas the Agreement between the European Community and the Republic of Korea on cooperation and mutual administrative assistance in customs matters should be approved,
HAS DECIDED AS FOLLOWS:
Article 1
The Agreement between the European Community and the Republic of Korea on cooperation and mutual administrative assistance in customs matters is hereby approved on behalf of the Community.
The text of the Agreement is attached to this Decision.
Article 2
The Commission, assisted by representatives of the Member States, shall represent the Community on the Joint Customs Cooperation Committee set up pursuant to Article 15 of the Agreement.
Article 3
The President of the Council is hereby authorized to designate the persons empowered to sign the Agreement on behalf of the Community.
Article 4
The President of the Council shall effect the notification provided for in Article 19 of the Agreement on behalf of the Community.
Article 5
This Decision shall be published in the Official Journal of the European Communities.
Done at Brussels, 26 April 1997.
For the Council
The President
E. KENNY
