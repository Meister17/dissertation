COMMISSION REGULATION (EC) No 1526/98 of 16 July 1998 amending Commission Regulation (EEC) No 752/93 laying down provisions for the implementation of Council Regulation (EEC) No 3911/92 on the export of cultural goods
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 3911/92 of 9 December 1992 on the export of cultural goods (1), as amended by Regulation (EC) No 2469/96 (2), and in particular Article 7 thereof,
After consulting the Advisory Committee on Cultural Goods,
Whereas it is advisable, in order to eliminate unnecessary administrative work, to introduce the concept of open licences for the temporary export of cultural goods by responsible persons or organisations for use and/or for exhibition in third countries;
Whereas the Member States that wish to take advantage of such facilities should be able to do so in relation to their own cultural goods, persons and organisations, whereas the conditions to be fulfilled will differ from Member State to Member State; whereas the Member States should be able to opt for the use of open licences or not and lay down the conditions to be met for their issue;
Whereas it is necessary to lay down provisions as to the appearance of such licences so that they can be readily recognised and used anywhere in the Community;
Whereas the provisions of Article 10 relating to Common transit are no longer necessary;
HAS ADOPTED THIS REGULATION:
Article 1
Commission Regulation (EEC) No 752/93 (3) shall be amended as follows:
1. Article 1 shall be replaced by the following:
'Article 1
1. There shall be three types of licences for the export of cultural goods which shall be issued and used in accordance with Council Regulation (EEC) No 3911/92, hereinafter called the "Basic Regulation", and with this implementing Regulation:
- the standard licence,
- the specific open licence,
- the general open licence.
2. The use of export licences shall in no way affect obligations connected with export formalities or related documents.`
2. Article 2 shall become Article 1(3).
3. A new Article 2 shall be introduced:
'Article 2
1. A standard licence shall normally be used for each export subject to the Basic Regulation. However each individual Member State concerned may decide whether or not it wishes to issue any specific or general open licences which may be used instead if the specific conditions relating to them are fulfilled as set out in Articles 10 and 13.
2. A specific open licence shall cover the repeated temporary export of a specific cultural good by a particular person or organisation as set out in Article 10.
3. A general open licence shall cover any temporary export of any of those cultural goods that form part of the permanent collection of a museum or other institution, as set out in Article 13.
4. A Member State may revoke any specific or general open licence at any time if the conditions under which it was issued are no longer met. It shall inform the Commission immediately if the licence issued is not recovered and could be used irregularly. The Commission shall immediately inform the other Member States.
5. Member States may introduce whatever reasonable measures they deem necessary in their national territory to monitor the use of their own open licences.`
4. A new Section II shall be introduced containing the existing Articles 3 to 9:
'SECTION II
The standard licence`
5. At the beginning of Article 3(1) the following shall be inserted:
'Standard licences shall be issued on the form, a model of which is in Annex I.`
6. A new Section III shall be added, the existing Article 10 shall be deleted and the existing Article 11 shall become Article 17:
'SECTION III
Open licences
CHAPTER 1
Specific open licences
Article 10
1. Specific open licences may be issued for a specific cultural good which is liable to be temporarily exported from the Community on a regular basis for use and/or exhibition in a third country. The cultural good must be owned by, or be in the legitimate possession of, the particular person or organisation that uses and or exhibits the good.
2. A licence may only be issued provided the authorities are convinced that the person or organisation concerned offers all the guarantees considered necessary for the good to be returned in good condition to the Community and that the good can be so described or marked that there will be no doubt at the moment of temporary export that the good being exported is that described in the specific open licence.
3. A licence may not be valid for a period that exceeds five years.
Article 11
The licence shall be presented in support of a written export declaration or be available in other cases for production with the cultural goods for examination upon request.
The competent authorities of the Member State in which the licence is presented may ask for it to be translated into the language, or one of the official languages, of that Member State. In this case, the translation costs shall be met by the licence holder.
Article 12
1. The customs office authorised to accept the export declaration shall ensure that the goods presented are those described on the export licence and that a reference is made to that licence in box 44 of the export declaration if a written declaration is required.
2. If a written declaration is required then the licence must be attached to copy 3 of the single administrative document and accompany the good to the customs office at the point of exit from the customs territory of the Community. Where copy 3 of the single administrative document is made available to the exporter or his representative, the licence shall also be made available to him for use on a subsequent occasion.
CHAPTER 2
General open licences
Article 13
1. General open licences may be issued to museums or other institutions to cover the temporary export of any of the goods that belong to their permanent collection that are liable to be temporarily exported from the Community on a regular basis for exhibition in a third country.
2. A licence may only be issued if the authorities are convinced that the institution offers all the guarantees considered necessary for the good to be returned in good condition to the Community. The licence may be used to cover any combination of goods in the permanent collection at any one occasion of temporary export. It can be used to cover a series of different combinations of goods either consecutively or concurrently.
3. A licence may not be valid for a period that exceeds five years.
Article 14
The licence shall be presented in support of the export declaration.
The competent authorities of the Member State in which the licence is presented may ask for it to be translated into the language, or one of the official languages, of that Member State. In this case, the translation costs shall be met by the licence holder.
Article 15
1. The customs office authorised to accept the export declaration shall ensure that the licence is presented together with a list of the goods being exported and which are also described in the export declaration. The list shall be on the headed paper of the institution and each page shall be signed by one of the persons from the institution and named on the licence. Each page shall also be stamped with the stamp of the institution as placed on the licence. A reference to the licence must be made in box 44 of the export declaration.
2. The licence shall be attached to copy 3 of the single administrative document and must accompany the consignment to the customs office at the point of exit from the customs territory of the Community. Where copy 3 of the single administrative document is made available to the exporter or his representative the licence shall also be made available to him for use on a subsequent occasion.
CHAPTER 3
Forms for the licences
Article 16
1. Specific open licences shall be issued on the form, a model of which is in Annex II.
2. General open licences shall be issued on the form a model of which is in Annex III.
3. The licence form shall be printed in one or more of the official languages of the Community.
4. The licence shall measure 210 × 297 mm. A tolerance of up to minus 5 mm or plus 8 mm in the length shall be allowed. The paper used shall be white, free of mechanical pulp, dressed for writing purposes and weigh at least 55 g/m2. It shall have a printed guilloche pattern background in light blue such as to reveal any falsification by mechanical or chemical means.
5. The second sheet of the licence, which shall not have a guilloche pattern background, is for the exporter's own use or records only.
The application form to be used shall be prescribed by the Member State concerned.
6. Member States may reserve the right to print the licence forms or may have them printed by approved printers. In the latter case, each must bear a reference to such approval. Each form must bear the name and address of the printer or a mark by which the printer can be identified. It shall also bear a serial number, either printed or stamped, by which it can be identified.
7. Member States shall be responsible for taking any measure necessary in order to avoid the forging of licences. The means of identification adopted by Member States for this purpose shall be notified to the Commission, for communication to the competent authorities of the other Member States.
8. Licences shall be made out by mechanical or electronic means. In exceptional circumstances they may be made out by black ball point pen in block capitals. They shall not contain erasures, overwritten words or other alterations.`
7. A new Section IV shall be added, containing Article 17:
'SECTION IV
General provisions`
8. The existing Annex shall become Annex I.
9. Annex I to this Regulation shall be inserted as Annex II.
10. Annex II to this Regulation shall be inserted as Annex III.
Article 2
This Regulation shall enter into force on the seventh day after its publication in the Official Journal of the European Communities.
It shall apply from 1 September 1998.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 16 July 1998.
For the Commission
Mario MONTI
Member of the Commission
(1) OJ L 395, 31. 12. 1992, p. 1.
(2) OJ L 335, 24. 12. 1996, p. 9.
(3) OJ L 77, 31. 3. 1993, p. 24.
ANNEX I
'ANNEX II
Model of form for specific open licences and copies thereof
>START OF GRAPHIC>
1
SPECIFIC OPEN LICENCE
1
EUROPEAN COMMUNITYEXPORT OF CULTURAL GOODS (Regulation (EEC) No 3911/92)1. Exporter
A. Identification number
B. Expiry date
This space should be used for pre-printing the name and address of the issuing authority. A national symbol or logo can also be placed here
2. Description of the goods
3. Commodity code
4. Photograph of cultural good (not more than 8 cm × 12 cm)
This space is available for pre-printed information at the discretion of the Member States, including any conditions
1
C. For completion by issuing authority
Signature:
Position:
Place:
Date:
Stamp
>END OF GRAPHIC>
>START OF GRAPHIC>
2
COPY FOR EXPORTER
2
EUROPEAN COMMUNITYEXPORT OF CULTURAL GOODS (Regulation (EEC) No 3911/92)1. Exporter
A. Identification number
B. Expiry date
This space should be used for pre-printing the name and address of the issuing authority. A national symbol or logo can also be placed here
2. Description of the goods
3. Commodity code
4. Photograph of cultural good (not more than 8 cm × 12 cm)
This space is available for pre-printed information at the discretion of the Member States, including any conditions
1
C. For completion by issuing authority
Signature:
Position:
Place:
Date:
Stamp'
>END OF GRAPHIC>
ANNEX II
'ANNEX III
Model of form for general open licences and copies thereof
>START OF GRAPHIC>
1
GENERAL OPEN LICENCE
1
EUROPEAN COMMUNITYEXPORT OF CULTURAL GOODS (Regulation (EEC) No 3911/92)1. Exporter
A. Identification number
B. Expiry date
This space should be used for pre-printing the name and address of the issuing authority. A national symbol or logo can also be placed here
This is a general open licence which allows for the temporary export of cultural goods which are part of the permanent collection of
It may be used to cover a number of different export consignments to different destinations during the period
to It is only valid provided that it is presented together with a list of the cultural goods to be temporarily exported in a particular shipment made out on their headed notepaper and marked with this stamp
and signed by one of the following.
NameSignature
This space is available for pre-printed information at the discretion of the Member States, including any conditions
1
C. For completion by issuing authority
Signature:
Position:
Place:
Date:
Stamp
>END OF GRAPHIC>
>START OF GRAPHIC>
2
COPY FOR EXPORTER
2
EUROPEAN COMMUNITYEXPORT OF CULTURAL GOODS (Regulation (EEC) No 3911/92)1. Exporter
A. Identification number
B. Expiry date
This space should be used for pre-printing the name and address of the issuing authority. A national symbol or logo can also be placed here
This is a general open licence which allows for the temporary export of cultural goods which are part of the permanent collection of
It may be used to cover a number of different export consignments to different destinations during the period
to It is only valid provided that it is presented together with a list of the cultural goods to be temporarily exported in a particular shipment made out on their headed notepaper and marked with this stamp
and signed by one of the following.
NameSignature
This space is available for pre-printed information at the discretion of the Member States, including any conditions
1
C. For completion by issuing authority
Signature:
Position:
Place:
Date:
Stamp'
>END OF GRAPHIC>
