Judgment of the Court
(Sixth Chamber)
of 8 September 2005
in Case C-57/05: Commission of the European Communities v French Republic [1]
In Case C-57/05 Commission of the European Communities (Agent: J.-P. Keppene)
v French Republic (Agent: G. de Bergues, E. Belliard and R. Loosli-Surrans) — action for failure to fulfil obligations under Article 226 EC, brought on 9 February 2005 — the Court (Sixth Chamber), composed of A. Borg Barthet, President of the Chamber, S. von Bahr and A. Ó Caoimh (Rapporteur), Judges; L.A. Geelhoed, Advocate General; R. Grass, Registrar, gave a judgment on 8 September 2005, in which it:
1. Declares that, By failing to adopt the laws, regulations and administrative provisions necessary to comply with Directive 2002/46/EC of the European Parliament and of the Council of 10 June 2002 on the approximation of the laws of the Member States relating to food supplements, the French Republic has failed to fulfil its obligations under that directive;
2. Orders the French Republic to pay the costs.
[1] OJ C 82 of 02.04.2005.
--------------------------------------------------
