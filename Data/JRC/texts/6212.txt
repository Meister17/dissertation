Final report of the Hearing Officer in Case COMP/M.3625 — Blackstone/Acetex
(pursuant to Articles 15 and 16 of Commission Decision 2001/462/EC, ECSC of 23 May 2001 on the terms of reference of Hearing Officers in certain competition proceedings — OJ L 162, 19.6.2001, p. 21)
(2005/C 297/02)
(Text with EEA relevance)
On 20 January 2005, the Commission received a notification of a proposed concentration pursuant to Article 4 of Council Regulation (EC) No 139/2004 [1] by which the undertaking Celanese Corporation (Celanese) controlled by Blackstone Crystal Holdings Capital Partners, Cayman Islands (Blackstone) acquires control within the meaning of Article 3(1)(b) of the Council Regulation of the whole of the undertaking Acetex Corporation, Canada (Acetex) by way of purchase of shares.
Having examined the information submitted by the parties to the proposed merger and conducted an initial market investigation, the Commission concluded that the notified operation raised serious doubts as to its compatibility with the common market and with the EEA Agreement. On 10 March 2005, the Commission therefore initiated proceedings in accordance with Article 6(1)(c) of the Merger Regulation.
Following a detailed market investigation, the Commission services concluded that the proposed concentration did not significantly impede effective competition on the markets concerned by the case. Accordingly, no statement of objections was sent to the parties.
In the course of the market investigation, the parties were granted access to key documents under section 7.2 of DG Competition's best practices on the conduct of merger control proceedings.
The case does not call for any particular comments as regards rights to be heard.
Brussels, 29 June 2005.
Karen Williams
[1] OJ L 24, 29.1.2004, p. 1.
--------------------------------------------------
