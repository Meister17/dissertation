Council Regulation (EC) No 1051/2001
of 22 May 2001
on production aid for cotton
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community,
Having regard to Protocol 4 on cotton(1), annexed to the Act of Accession of Greece, and in particular the paragraph 6 thereof,
Having regard to the proposal from the Commission(2),
Having regard to the opinion of the European Parliament(3),
Having regard to the opinion of the Economic and Social Committee(4),
Whereas:
(1) Examination of the operation of the aid scheme provided for in paragraph 11 of Protocol 4 annexed to the Act of Accession of Greece reveals a need to maintain the existing arrangements for cotton, but with certain adjustments.
(2) The measures relating to cotton are laid down in Protocol 4, Council Regulation (EC) No 1554/95 of 29 June 1995 laying down the general rules for the system of aid for cotton and repealing Regulation (EEC) No 2169/81(5) and Council Regulation (EEC) No 1964/87 of 2 July 1987 adjusting the system of aid for cotton introduced by Protocol 4 annexed to the Act of Accession of Greece(6). The arrangements provided for in Protocol 4 should be maintained, particularly the possibility for the Council to adjust the scheme, and, in the interests of simplification, all the implementing measures needed for the grant of the aid should be brought together in a single Council Regulation.
(3) Paragraph 6 of Protocol 4 stipulates that the necessary measures for implementing the scheme for cotton must be adopted. The production aid provided for in paragraph 3 of Protocol 4 is currently based on a system, in the framework of guaranteed national quantities, that guarantees a minimum price to producers and, through the aid paid to ginning undertakings, makes up the difference between the guide price and the world market price. In the light of experience, the foundations of the system and its various components should be maintained.
(4) The guide price, the minimum price payable to producers and the guaranteed national quantities should be fixed at levels that maintain the balance achieved between crops and enable operators to run medium-term production and processing programmes.
(5) The provisions for determining the world market price for unginned cotton should be maintained. This price may be established on the basis of the relationship between the price for ginned cotton and the price calculated for unginned cotton. When calculating the price of ginned cotton, account should be taken of offers made on the world market and of quotations on those exchanges that are important for international trade.
(6) The current mechanism, whereby when a certain production quantity is exceeded the guide price is reduced proportionately in the Member States responsible for the overrun, should be maintained in order to ensure that penalties are applied fairly. However, the reduction in the guide price may be limited provided that, taking account in particular of the average world market price, a certain level of expenditure is not exceeded. The consequences of the guaranteed national quantities mechanism must be applied to both the minimum price and the aid.
(7) The percentage reduction in the guide price which now applies, equal to half the overrun in the national guaranteed quantity, may under certain circumstances threaten budgetary discipline. That percentage should therefore be increased from a certain production threshold.
(8) To ensure that the system is balanced, the production aid should henceforth be paid in full to beneficiaries, without prejudice to the various reductions provided for by the Community rules. The current state of production structures is such that the aid has to be granted to ginning undertakings, which pay producers a price at least equal to the minimum price and an advance on that price, and which accept certain conditions regarding checks on the quantities eligible for aid.
(9) The aid varies in amount according to the world market price and needs to be applied to the quantities of cotton eligible for aid depending on the actual period when the aid application is made. The current scheme allows ginning undertakings to fix that amount, in their aid applications, on the basis, in particular, of the date of conclusion of sales contracts for the ginned cotton in their possession. In order better to facilitate the disposal of ginned cotton on the world market, it is advisable in future to allow contracts to be concluded before the harvesting period and, consequently, to extend the period during which aid applications may be submitted.
(10) It seems inadvisable to regulate contractual relations between producers and ginning undertakings at Community level. The current principle of common consent between the contracting parties should be maintained but spelt out in greater detail.
(11) The amount of aid to be granted cannot be known until actual production in each Member State has been determined. To offset the adverse effects of late payment of aid, provision should continue to be made for part of the aid to be paid in advance.
(12) Producer Member States must set up the control arrangements necessary to ensure that the aid system operates correctly, using where applicable the integrated administration and control system provided for in Council Regulation (EEC) No 3508/92 of 27 November 1992 establishing an integrated administration and control system for certain Community aid schemes(7).
(13) The cultivation of cotton in regions not suited to it is likely to have harmful effects on the environment as well as on the agricultural economy of those regions where this crop is important. In order to take account of environmental objectives, Member States should determine and adopt the environmental measures they consider suitable to regulate the use of agricultural land for cotton production. In future, Member States must introduce measures to restrict the crop under objective environmental criteria and remind producers of the need to comply with the legislation in force. The two main producer Member States should draw up a report on the impact on the cotton sector of national environmental measures at a time when an assessment is possible.
(14) In order to facilitate the implementation and proper administration of the aid system, provision should be made for a procedure establishing close cooperation between Member States and the Commission within a management committee. The Management Committee for natural fibres provided for in Council Regulation (EC) No 1673/2000 of 27 July 2000 on the common organisation of the market in flax and hemp(8), should be used for that purpose.
(15) The measures necessary for the implementation of this Regulation should be adopted in accordance with Council Decision 1999/468/EC of 28 June 1999 laying down the procedures for the exercise of implementing powers conferred on the Commission(9).
(16) In order to ensure that Community expenditure linked to the application of the measures provided for in this Regulation is subject to financial and monetary rules and adequate procedures, and in view of the specifically agricultural nature of unginned cotton, Council Regulation (EC) No 1258/1999 of 17 May 1999 on the financing of the common agricultural policy(10) and Council Regulation (EC) No 2799/98 of 15 December 1998 establishing agrimonetary arrangements for the euro should apply in this area(11).
(17) The adjustments to the scheme provided for in this Regulation should be implemented as smoothly as possible and transitional measures may therefore be required,
HAS ADOPTED THIS REGULATION:
CHAPTER I
GENERAL PRINCIPLES
Article 1
1. This Regulation lays down the measures needed for the grant of the production aid provided for in paragraph 3 of Protocol 4 annexed to the Act of Accession of Greece.
2. For the purposes of this Regulation:
(a) "unginned cotton" means the fruit of the cotton plant (Gossypium) which has reached maturity and been harvested and which contains pod and leaf fragments and soil matter;
(b) "ginned cotton" means cotton fibres (other than linters and waste), neither carded nor combed, from which the seeds and most of the pod and leaf fragments and soil matter have been removed.
3. The marketing year shall run from 1 September to 31 August.
Article 2
1. The amount of the production aid for unginned cotton shall be fixed by the Commission on the basis of the difference between:
- a guide price fixed for unginned cotton in accordance with Article 3(1) and Article 7, and
- the world market price determined in accordance with Article 4.
2. The aid shall be granted for unginned cotton purchased at a price at least equal to the minimum price, fixed in accordance with Article 3(2) and Article 9.
CHAPTER II
PRICE MECHANISM
Article 3
1. The guide price shall be set at EUR 106,30 per 100 kg of unginned cotton.
This price shall relate to cotton:
- of sound and fair merchantable quality,
- containing 10 % moisture and 3 % impurities,
- with the characteristics needed to produce, after ginning, 32 % grade 5 fibres (white middling) 28 mm in length (1-3/32").
2. The minimum price shall be set at EUR 100,99 per 100 kg of unginned cotton for the quality selected for the ex holding guide price.
Article 4
1. The world market price for unginned cotton shall be determined by taking account of the historical relationship between the world market price for ginned cotton and the price calculated for unginned cotton. It shall be fixed periodically by the Commission on the basis of the world market price for ginned cotton referred to in Article 5.
2. If the world market price for unginned cotton cannot be determined in accordance with paragraph 1, it shall be established on the basis of the most recent price determined.
Article 5
1. The world market price for ginned cotton shall be determined on the basis of a grade 5 (white middling) product with a fibre length of 28 mm (1-3/32"), account being taken of the offers made on the world market and of the quotations on one or more European exchanges that are important for international trade. This price shall be determined using the most favourable offers and quotations among those that are considered representative of the real market price cif for the product delivered to a Community port.
2. Where the offers and quotations recorded do not satisfy the requirements referred to in paragraph 1, the necessary adjustments shall be made.
CHAPTER III
STABILISER MECHANISM
Article 6
A guaranteed national quantity for unginned cotton shall be introduced, equal for each marketing year to:
- 782000 tonnes for Greece,
- 249000 tonnes for Spain,
- 1500 tonnes in each of the other Member States.
Article 7
1. The measures referred to in this Article shall apply without prejudice to Article 8.
2. If during a marketing year the sum of actual production in Spain and Greece exceeds 1031000 tonnes, the guide price referred to in Article 3(1) shall be reduced for that marketing year in any Member State where the actual production exceeds the guaranteed national quantity.
3. The guide price shall be reduced for the Member State concerned by a percentage based on the rate by which its guaranteed national quantity has been overshot. However if actual production in either Spain or Greece is lower than its guaranteed national quantity, the difference between the total actual production of the two Member States and 1031000 tonnes shall be expressed as a rate of the guaranteed national quantity that has been overshot, and the guide price shall be reduced on the basis of this rate.
4. The guide price reduction is equal to 50 % of the rate of overshot referred to in paragraph 3.
However if the sum of actual production in Spain and Greece reduced by 1031000 tonnes is higher than 469000 tonnes, the reduction in the guide price of 50 % shall be increased by 2 percentage points:
- in the case of Greece, for each step of 15170 tonnes in full or part thereof of which the production exceeds the guaranteed national quantity increased by 356000 tonnes,
- in the case of Spain, for each step of 4830 tonnes in full or part thereof of which the production exceeds the guaranteed national quantity increased by 113000 tonnes.
Article 8
If during a marketing year:
- Article 7 has been applied,
- the weighted average of the world market price adopted in order to fix the amount of aid is greater than EUR 30,20 per 100 kg, and
- the total budget expenditure on the aid system is less than EUR 770 million,
the budget remainder referred to in the third indent shall be used in order to increase aid in each Member State where actual production exceeds its guaranteed national quantity.
However, the amount of aid as increased under the first subparagraph may not exceed:
- either the amount of aid calculated without application of Article 7,
- or the amount of aid calculated after application of Article 7 on the basis of 1120000 tonnes of unginned cotton subdivided into a guaranteed national quantity of 270000 tonnes for Spain and 850000 tonnes for Greece.
Article 9
The minimum price referred to in Article 3(2) shall be reduced by the same amount as the guide price under Article 7.
CHAPTER IV
AID BENEFICIARIES
Article 10
The aid shall be paid in full to the beneficiaries referred to in Articles 11 and 12 who make an application.
Article 11
In order to receive aid, cotton ginning undertakings other than those referred to in Article 12 must:
(a) have submitted a signed contract stipulating, in particular, payment to the producer of a price at least equal to the minimum price and containing a clause specifying that:
- where Article 7 is applied, the agreed price will be adjusted in consequence of the effect of the application of that Article on the aid,
- if the quality of the cotton delivered differs from the quality referred to in Article 3(2), the price agreed will be adjusted by common consent between the contracting parties in proportion to the effect of that difference in quality on the price of the ginned product in relation to the price referred to in Article 5;
(b) have paid an advance on the minimum price of an amount to be determined by common consent between the contracting parties, subject to conditions to be laid down;
(c) keep stock accounts on ginned and unginned cotton that satisfy requirements to be laid down and provide the other supporting documents necessary so that entitlement to the aid can be checked;
(d) furnish proof that the cotton delivered under the contract is the subject of the declaration of area sown referred to in Article 16(2).
Article 12
1. In order to receive the aid, cotton ginning undertakings that gin cotton on behalf of individual producers or producer groups meeting the criteria laid down in paragraph 4 of Protocol 4 must:
(a) have submitted a statement, approved by the producer or producer group concerned, giving details of the conditions under which the ginning is carried out and how the aid is administered;
(b) undertake to pass on the full amount of the aid to the individual producers or, where applicable, the producer groups concerned;
(c) satisfy the conditions referred to in Article 11(c);
(d) furnish proof that the cotton referred to in the statement referred to in (a) has been the subject of the declaration of area sown referred to in Article 16(2);
(e) furnish proof that producer groups are obliged to comply with an undertaking equivalent to the clause in the contract referred to in Article 11(a) and an undertaking by such groups to keep and produce supporting documents concerning payment of the minimum price to their members.
2. Failure to comply with the clause or undertaking referred to in paragraph 1(e) by a producer group having ginning carried out on its behalf shall be considered a failure to meet the criteria referred to in paragraph 4 of Protocol 4.
CHAPTER V
GRANT OF THE AID
Article 13
The aid shall be paid by the producer Member State in whose territory the ginning takes place.
Article 14
1. The amount of the aid to be paid shall be the amount applicable on the day the application for aid is made.
Aid applications shall be lodged, together with a security where required, during a period to be determined, for a quantity of unginned cotton which must enter the cotton ginning undertaking after the beginning of the marketing year concerned and before a date to be specified.
2. Entitlement to the aid shall be acquired when the cotton is ginned. However, aid may be paid in advance, at the request of the interested party, from 16 October following the start of the marketing year, when the unginned cotton enters the cotton ginning undertaking, provided an adequate security has been lodged. The amount of the advance shall be calculated in accordance with paragraph 3.
The balance of the aid shall be paid before the end of the marketing year at the latest, once any adjustments to the aid resulting from application of Article 7 have been determined.
3. The amount of the advance shall be equal to the guide price referred to in Article 3(1), minus the world price and reduced further by an amount calculated in accordance with Article 7, but replacing actual production by the estimated production fixed in accordance with the first indent of Article 19(2), plus 15 %.
From 16 December following the start of the marketing year, the amount of the advance referred to in the first subparagraph shall be replaced by a revised amount calculated using the same method but based on the revised production estimate fixed in accordance with the second indent of Article 19(2), plus at least 7,5 %. Advances paid between 16 October and 15 December shall be increased accordingly, except where the difference between the two amounts is less than EUR 1/100 kg.
Article 15
1. The aid shall be paid only for products of sound and fair merchantable quality.
2. If the quantity of ginned cotton is less than or equal to 33 % of the quantity of unginned cotton that has entered the cotton ginning undertaking, aid shall be granted for the quantity of ginned cotton, multiplied by 100 and divided by 32.
If the quantity of ginned cotton is greater than 33 % of the quantity of unginned cotton that has entered the cotton ginning undertaking, aid shall be granted for the quantity of unginned cotton, multiplied by 33 and divided by 32.
3. The quantity of ginned cotton shall be equal to its weight, adjusted where necessary for any difference between:
- either the percentage of impurities recorded compared to the representative percentage for grade 5 or the grade recorded and grade No 5, and
- the percentage moisture content recorded compared to the level representative of commercial fibre.
Article 16
1. The producer Member States shall set up a system of penalties and controls enabling them, in particular, to ensure that the minimum price is complied with and to ascertain:
- the quantity of unginned Community cotton which has entered each cotton ginning undertaking,
- the quantity of unginned Community cotton which has been ginned,
- the quantity of ginned cotton obtained in each cotton ginning undertaking from the cotton referred to in the first indent.
2. The producer Member States shall establish a system of declarations of the areas sown, in particular to ensure that cotton covered by aid applications is of the origin stated.
Article 17
1. Member States shall determine, for the cotton sector:
- measures to improve the environment, in particular cultivation techniques to reduce the negative impact on the environment,
- research programmes to develop more environmentally-friendly growing methods,
- the means of informing producers of the results of such research and the advantages of using the techniques concerned.
2. Member States shall take the environmental measures they consider suitable in view of the specific situation of the agricultural areas used for cotton production. In addition, Member States shall take the necessary steps to remind producers that they are required to comply with environmental legislation.
3. Member States shall, where appropriate, restrict the areas eligible for production aid for unginned cotton on the basis of objective criteria relating to:
- the agricultural economy of those regions where cotton is a major crop,
- the soil and climate in the areas in question,
- the management of irrigation water,
- rotation systems and cultivation methods likely to improve the environment.
4. Before 31 December 2004, the Hellenic Republic and the Kingdom of Spain shall send the Commission a report on the environmental situation in the cotton sector and the impact of national measures adopted in accordance with paragraphs 1, 2 and 3.
CHAPTER VI
GENERAL
Article 18
1. The Commission shall be assisted by the Management Committee of natural fibres established by Article 10 of Regulation (EC) No 1673/2000 (hereinafter referred to as "the Committee").
2. Where reference is made to this paragraph, Articles 4 and 7 of Decision 1999/468/EC shall apply.
The period provided for in Article 4(3) of Decision 1999/468/EC shall be set at one month.
3. The Committee shall adopt its rules of procedure.
Article 19
1. The detailed rules for implementation of this Regulation shall be adopted according to the procedure laid down in Article 18(2).
Such rules shall concern, inter alia, the information to be forwarded by Member States to the Commission and all the necessary inspection measures to protect the European Community's financial interests against fraud and other irregularities. These measures may be based on the integrated administration and control system established by Regulation (EEC) No 3508/92.
2. In accordance with the procedure referred to in Article 18(2), the Commission shall fix for each Member State concerned, before dates to be specified:
- taking account of crop forecasts, the estimated production referred to in the first subparagraph of Article 14(3) and the resultant provisional reduction in the guide price,
- taking account of the progress of the harvest, the revised production estimate referred to in the second subparagraph of Article 14(3) and the new resultant provisional reduction in the guide price,
- taking account in particular of the quantities on which aid has been applied for, actual production in the current marketing year, together with the reduction in the guide price referred to in Article 7 and the increase in the aid referred to in Article 8.
Article 20
Regulations (EC) No 2799/98 and No 1258/1999 on the financing of the common agricultural policy shall apply, mutatis mutandis, to the arrangements provided for in this Regulation.
Article 21
If it proves necessary to introduce transitional measures to facilitate implementation of the adjustments to the system laid down by this Regulation, such measures shall be adopted in accordance with the procedure referred to in Article 18(2). They shall apply until the end of the 2001/2002 marketing year at the latest.
Article 22
Regulations (EEC) No 1964/87 and (EC) No 1554/95 are hereby repealed.
Article 23
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Communities.
It shall apply from 1 September 2001. However, Article 21 shall apply from the date of entry into force of this Regulation.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 22 May 2001.
For the Council
The President
M. Winberg
(1) OJ L 291, 19.11.1979, p. 174. Protocol as last amended by Regulation (EC) No 1050/2001. (see p. 1 of this Official Journal).
(2) Proposition of 13 December 1999 (not yet published in the Official Journal).
(3) Opinion delivered on 15 February 2001 (not yet published in the Official Journal).
(4) OJ C 140, 18.5.2000, p. 33.
(5) OJ L 148, 30.6.1995, p. 48. Regulation as last amended by Regulation (EC) No 1419/98 (OJ L 190, 4.7.1998, p. 4)
(6) OJ L 184, 30.6.1995, p. 14. Regulation as last amended by Regulation (EC) No 1553/95 (OJ L 148, 30.6.1995, p. 45).
(7) OJ L 355, 5.12.1992, p. 1. Regulation as last amended by Regulation (EC) No 820/97. (OJ L 117, 7.5.1997, p. 1).
(8) OJ L 193, 29.7.2000, p. 16.
(9) OJ L 184, 17.7.1999, p. 23.
(10) OJ L 160, 26.6.1999, p. 103.
(11) OJ L 349, 24.12.1998, p. 1.
