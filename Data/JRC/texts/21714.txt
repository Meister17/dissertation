COMMISSION REGULATION (EEC) No 509/92 of 28 February 1992 concerning the classification of certain goods in the combined nomenclature
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 2658/87 (1) on the tariff and statistical nomenclature and on the Common Customs Tariff, as last amended by Commission Regulation (EEC) No 396/92 (2), and in particular Article 9,
Whereas in order to ensure uniform application of the combined nomenclature annexed to the said Regulation, it is necessary to adopt measures concerning the classification of the goods referred to in the Annex to this Regulation;
Whereas Regulation (EEC) No 2658/87 has set down the general rules for the interpretation of the combined nomenclature and these rules also apply to any other nomenclature which is wholly or partly based on it or which adds any additional subdivisions to it and which is established by specific Community provisions, with a view to the application of tariff or other measures relating to trade in goods;
Whereas, pursuant to the said general rules, the goods described in column 1 of the table annexed to the present Regulation must be classified under the appropriate CN codes indicated in column 2, by virtue of the reasons set out in column 3;
Whereas it is appropriate that binding tariff information issued by the customs authorities of Member States in respect of the classification of goods in the combined nomenclature and which do not conform to the rights established by this Regulation, can continue to be invoked under the provisions in Article 6 of Commission Regulation (EEC) No 3896/90 (3), for a period of three months by the holder if a binding contract has been concluded such as is envisaged in Article 14 (3) (a) or (b) of Commission Regulation (EEC) No 1715/90 (4);
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Nomenclature Committee,
HAS ADOPTED THIS REGULATION:
Article 1
The goods described in column 1 of the annexed table are now classified within the combined nomenclature under the appropriate CN codes indicated in column 2 of the said table.
Article 2
Binding tariff information issued by the customs authorities of Member States which do not conform to the rights established by this Regulation can continue to be invoked under the provisions of Article 6 of Regulation (EEC) No 3796/90 for a period of three months by the holder if a binding contract has been concluded as envisaged in Article 14 (3) (a) or (b) of Regulation (EEC) No 1715/90.
Article 3
This Regulation shall enter into force on the 21st day following its publication in the Official Journal of the European Communities. This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 28 February 1992. For the Commission
Christiane SCRIVENER
Member of the Commission
(1) OJ No L 256, 7. 9. 1987, p. 1. (2) OJ No L 44, 20. 2. 1992, p. 9. (3) OJ No L 365, 28. 12. 1990, p. 17. (4) OJ No L 160, 26. 6. 1990, p. 1.
ANNEX
Description of goods Classification CN code Reasons (1) (2) (3) 1. Product, consisting of a mixture of residues from the manufacture of starch from maize (approximately 40 %), residues from the extraction of oil from maize germs by the wet method (approximately 30 %) and of residues of the distillation of alcohol from maize ('corn distillers') (approximately 30 %), presenting the following analytical characteristics, weight on dry matter: 2309 90 41 Classification is determined by the provisions of general rules 1 and 6 for the interpretation of the combined nomenclature, by additional note 1 to chapter 23 and by the wording of CN codees 2309, 2309 90 en 2309 90 41. - starch 18 % determined in accordance with the method contained in Annex I.1 to Commission Directive 72/199/EEC (1) - proteins (N × 6,25) 28 % determined in accordance with the method contained in Annex I.2 to Commission Directive 72/199/EEC - fats 4,4 % determined in accordance with method A contained in Annex I to Commission Directive 84/4/EEC (2) It is used in animal feed. 2. Preparation consisting mainly of a mixture of approximately 60 % calcium hydrogen-orthophosphate [dicalcium phosphate], by weight, and approximately 40 % calcium bis (dihydrogenorthophosphate) [monocalcium phosphate], by weight, used in animal feed. 2309 90 99 Classification is determined by the provisions of general rules 1 and 6 for the interpretation of the combined nomenclature and by the wording of CN codes 2309, 2309 90 and 2309 90 99 (see also the Explanatory Notes to the HS, heading 23.09, part II. C).
(1) OJ No L 123, 29. 5. 1972, p. 6.
(2) OJ No L 15, 18. 1. 1984, p. 28.
