Commission Decision
of 22 November 2001
amending Decision 95/454/EC laying down special conditions governing imports of fishery and aquaculture products originating in the Republic of Korea
(notified under document number C(2001) 3692)
(Text with EEA relevance)
(2001/818/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/493/EEC of 22 July 1991 laying down the health conditions for the production and the placing on the market of fishery products(1), as last amended by Directive 97/79/EC(2), and in particular Article 11(5) thereof,
Whereas:
(1) Annex A of Commission Decision 95/454/EC of 23 October 1995 laying down special conditions governing imports of fishery and aquaculture products originating in the Republic of Korea(3), as last amended by Decision 2001/641/EC(4), lays down the model of health certificate for fishery and aquaculture products originating in the Republic of Korea and intended for export to the European Community.
(2) Commission Decision 95/453/EC of 23 October 1995 laying down special conditions for the import of bivalve molluscs, echinoderms, tunicates and marine gastropods originating in the Republic of Korea(5), as last amended by Decision 2001/676/EC(6), authorizes the imports of frozen and processed bivalve molluscs, echinoderms, tunicates and marine gastropods from the Republic of Korea. It is, therefore, necessary to complete the health attestation of the health certificate laid down in Decision 95/454/EC with the relevant mentions to the requirements for bivalve molluscs.
(3) The measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
Decision 95/454/EC is modified as follows:
Point IV of the Annex A is replaced by the following: "IV. Health attestation
The official inspector hereby certifies that the fishery or aquaculture products specified above:
1. were caught and handled on board vessels in accordance with the health rules laid down by Directive 92/48/EEC;
2. were landed, handled and where appropriate packaged, prepared, processed, frozen, thawed and stored hygienically in compliance with the requirements laid down in Chapters II, III and IV of the Annex to Directive 91/493/EEC;
3. have undergone health controls in accordance with Chapter V of the Annex to Directive 91/493/EEC;
4. are packaged, marked, stored and transported in accordance with Chapters VI, VII and VIII of the Annex to Directive 91/493/EEC;
5. do not come from toxic species or species containing biotoxins;
6. have satisfactorily undergone the organoleptic, parasitological, chemical and microbiological checks laid down for certain categories of fishery products by Directive 91/493/EEC and in the implementing decisions thereto;
7. in addition, where the fishery products are frozen or processed bivalve molluscs: the molluscs were obtained from approved production areas laid down by the Annex to Decision 95/453/EC of 23 October 1995 laying down special conditions for the import of live bivalve molluscs, echinoderms, tunicates and marine gastropods originating in the Republic of Korea.
The undersigned official inspector hereby declares that he is aware of the provisions of Directives 91/492/EEC, 91/493/EEC and 92/48/EEC and Decisions 95/453/EC and 95/454/EC."
Article 2
This Decision shall apply 45 days after its publication in the Official Journal of the European Communities.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 22 November 2001.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 268, 24.9.1991, p. 15.
(2) OJ L 24, 30.1.1998, p. 31.
(3) OJ L 264, 7.11.1995, p. 37.
(4) OJ L 224, 21.8.2001, p. 10.
(5) OJ L 264, 7.11.1995, p. 35.
(6) OJ L 18, 5.9.2001, p. 18.
