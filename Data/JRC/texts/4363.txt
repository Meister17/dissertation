Removal from the register of Case C-450/04 [1]
(2005/C 330/26)
Language of the case: French
By order of 28 September 2005, the President of the Court of Justice (Fourth Chamber) of the European Communities has ordered the removal from the register of Case C-450/04: Commission of the European Communities
v French Republic.
[1] OJ C 314, 18.12.2004.
--------------------------------------------------
