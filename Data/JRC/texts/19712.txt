Commission Regulation (EC) No 247/2003
of 10 February 2003
adopting the specification of the 2004 ad hoc module on work organisation and working time arrangements provided by Council Regulation (EC) No 577/98
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 577/98 of 9 March 1998 on the organisation of a labour force sample survey in the Community(1), as last amended by Commission Regulation (EC) No 2104/2002(2), and in particular Article 4(2) thereof,
Whereas:
(1) Commission Regulation (EC) No 246/2003(3) adopting the programme of ad hoc modules, covering the years 2004 to 2006, to the labour force sample survey includes an ad hoc module on work organisation and working time arrangements.
(2) In accordance with Article 4(2) of Regulation (EC) No 577/98 the detailed list of information to be collected in an ad hoc module shall be drawn at least 12 months before the beginning of the reference period for that module.
(3) In order to evaluate adequately progress under the pillars of the employment policy guidelines and in particular under the third pillar on encouraging adaptability of businesses and their employees, including with regard to quality in work, comparable structural statistics are needed by the Member States and the Commission(4).
(4) The measures provided for in this Regulation are in accordance with the opinion of the Statistical Programme Committee,
HAS ADOPTED THIS REGULATION:
Article 1
The detailed list of information to be collected in 2004 by the ad hoc module, as laid down in the Annex, is hereby adopted.
Article 2
This Regulation shall enter into force on the seventh day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 10 February 2003.
For the Commission
Pedro Solbes Mira
Member of the Commission
(1) OJ L 77, 14.3.1998, p. 3.
(2) OJ L 324, 29.11.2002, p. 14.
(3) See page 3 of this Official Journal.
(4) OJ L 60, 1.3.2002, p. 60.
ANNEX
LABOUR FORCE SURVEY
Specification of the 2004 ad hoc module on work organisation and working time arrangements
1. Member States and regions concerned: all.
2. The data are collected only for the main job.
3. The variables will be coded as follows:
>TABLE>
4. The variables on shift work, evening work, Saturday and Sunday work which appear as columns 204 to 208 in the Annex to the Commission Regulation (EC) No 1575/2000 concerning the codification to be used for data transmission from 2001 onwards must be surveyed in 2004 for the same reference period as the ad hoc module referred to in Article 1 of the present Regulation.
5. The variable on shift work is to be coded as follows:
>TABLE>
