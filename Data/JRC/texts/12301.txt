Publication of an application pursuant to Article 6(2) of Council Regulation (EC) No 510/2006 on the protection of geographical indications and designations of origin for agricultural products and foodstuffs
(2006/C 230/02)
This publication confers the right to object to the application pursuant to Article 7 of Council Regulation (EC) No 510/2006. Statements of objection must reach the Commission within six months from the date of this publication.
SUMMARY
Council Regulation (EC) No 510/2006
Application for registration pursuant to articles 5 and 17(2)
"BRYNDZA PODHALAŃSKA"
EC No: PL/PDO/005/0450/ 18.02.2005
PDO ( X ) PGI ( )
This summary has been drawn up for information only. For full details, interested parties and in particular the producers of the products covered by the PDO or PGI in question are invited to consult the full version of the product specification obtainable at national level or from the European Commission. [1]
1. Responsible department in the Member State:
Name: | Ministry of Agriculture and Rural Development |
Address: | ul. Wspólna 30 PL-00-930 Warszawa |
Telephone: | (48-22) 623 27 07 |
Fax No: | (48-22) 623 25 03 |
E-mail: | Jakub.Jasinski@minrol.gov.pl |
2. Group:
Name: | Regionalny Związek Hodowców Owiec i Kóz |
Address: | ul. Szaflarska 93 d/7 PL-34-400 Nowy Targ |
Telephone: | (48-18) 266 46 21 |
Fax No: | (48-18) 266 46 21 |
E-mail: | rzhoik@kr.home.pl |
Composition: | Producers/processors ( X ) other ( ) |
3. Type of product:
Class: 1.3 Cheeses
4. Specification (summary of requirements under Article 4(2))
Name : "Bryndza Podhalańska"
Description :
The cheese "Bryndza Podhalańska" belongs to the group of soft, rennin cheeses. Its colour may be white, creamy-white or with a willow green shade. It is a product with a strong, salty or slightly salty taste, or sometimes slightly sour. Bryndza Podhalańska is produced only from May to September.
The cheese's chemical composition: water content not more than 60 %, dry matter content not less than 40 % and fat content in the dry matter not less than 38 %.
Geographical area : "Bryndza Podhalańska" is produced in the Nowotarski district, in the Tatrzański district and in the following six municipalities of the Żywiecki district: Milówka, Węgierska Górka, Rajcza, Ujsoły, Jeleśnia and Koszarawa. The traditional name for this region is "Podhale" from which is derived the name "Bryndza Podhalańska". The entire region enjoys favourable natural conditions and is also home to cheesemakers with specific skills associated with a knowledge of traditional production methods. Thanks to a combination of these factors, it is possible to produce in this region a product of appropriate quality.
Proof of origin : The system to check that the product is produced in accordance with the specifications guarantees the quality of the final product. A list of those engaged in producing Bryndza Podhalańska is kept. Only producers on this list are authorised to make this cheese. The producers keep appropriate records, which must make it possible to trace the product's manufacturing history. Each producer keeps a register at the place of manufacture, in which information on the manufacturing process is recorded. The inspection body monitors producers and undertakes appropriate controls in accordance with the approved plans.
Method of production :
Stage 1 — Obtaining the raw material — The ewes' milk used for production comes from ewes of the breed "Polska Owca Górska" (Polish Mountain Sheep). Cows' milk, if it is used for production at all, comes from cows of the breed "Polska Krowa Czerwona" (Polish Red). The cows' milk content, if used, must not exceed 40 % of the milk used in production.
Stage 2 — Renneting — the milk is subsequently subjected to a process in which protein is separated from the whey (klaganie).
Stage 3 — Curding — after the addition of rennet, the milk coagulates and turns into curd.
Stage 4 — Breaking up of the curd.
Stage 5 — Settlement of the curd — separation of the whey from the cheese curds.
Stage 6 — Decanting of the whey — whey collection.
Stage 7 — Dripping — the cheese mass is removed and is then allowed to drip.
Stage 8 — The cheese undergoes seasoning and fermentation.
Stage 9 — The fermented cheese is broken up into small chunks.
Stage 10 — Mixing — the broken-up cheese is mixed with salt — the result of this process is bryndza which, because of its unique ingredients and old method of production known and used exclusively in Podhale, is called Bryndza Podhalańska.
Link Historical
The tradition and importance of bryndza from Podhale and the surrounding areas is confirmed by literary references, in commands issued by rural landowners and royal decrees. The oldest references to bryndza date from 1527. There are also numerous references confirming that bryndza was used in this area as a method of payment or as one constituent of rents or duties paid. There are also many descriptions in literature of the way bryndza was produced, as well as information on its price over the years. The manufacture of cheese from sheep's milk was an essential part of the pasturing of sheep in Podhale over the centuries. The shepherds who went up into the mountains with their sheep would spend several months there. During that time they practically lived on sheep's milk and the products thereof alone. The know-how gained on the method and principles of producing Bryndza Podhalańska were passed down from generation to generation, to become an art that is particular to the producers in this region. Thanks to the combination of natural factors, specific skills and maintenance of the original formula, a unique product, "Bryndza Podhalańska" is created.
Natural
The milk from sheep of the Polska Owca Górska (Polish Mountain Sheep) breed is used to make Bryndza Podhalańska, to which cows' milk from the Polska Krowa Czerwona (Polish Red) breed may be added. The Polish Red is a native cattle breed, reared in Poland. This breed is closely connected with the history and tradition of Podhale and with the people living in this region. The very rich and varied vegetation to be found in this region also has an impact on the quality of the final product. There are many endemic plant species that grow only in Podhale and they constitute the flora of the meadows, pastures and mountain pastures.
This unique vegetation influences the quality of the milk used in production. This combination of the quality of the raw material and producers' know-how and experience help to give Bryndza Podhalańska a unique taste all of its own.
Name: | IJHARS — Agricultural and Food Quality Inspection. |
Address: | ul. Wspólna 30 PL-00-930 Warszawa |
Telephone: | (48-22) 623 29 00 |
Fax No: | (48-22) 623 29 98 (48-22) 623 29 99 |
E-mail: | — |
Inspection body Labelling : The registered product will bear the Protected Designation of Origin symbol. The packages of products intended for marketing, apart from the name "BRYNDZA PODHALAŃSKA", will also bear the logo or the logo together with an inscription "Protected Designation of Origin". The use of the abbreviation "P.D.O." on the package is allowed.
National requirements : —
[1] European Commission, Directorate-General for Agriculture and Rural Development, Agricultural Product Quality Policy, B-1049 Brussels.
--------------------------------------------------
