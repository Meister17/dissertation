COMMISSION REGULATION (EC) No 1122/96 of 21 June 1996 amending for the second time Regulation (EC) No 1370/95 laying down detailed rules for implementing the system of export licences in the pigmeat sector
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2759/75 of 29 October 1975 on the common organization of the market in pigmeat (1), as last amended by the Act of Accession of Austria, Finland and Sweden and by Regulation (EC) No 3290/94 (2), and in particular Articles 8 (2), 13 (12) and 22 thereof,
Whereas Commission Regulation (EC) No 1370/95 (3), as amended by Regulation (EC) No 2739/95 (4), lays down detailed rules for implementing the system of export licences in the pigmeat sector;
Whereas it is appropriate, in the light of experience, to improve the outline relative to the lodging of applications and to the issuing of licences with a prolongation of the period for lodging applications and an amendment of the day of issuing licences; whereas it is necessary to incorporate these changes into the various provisions of Regulation (EC) No 1370/95;
Whereas it is necessary to allow the Commission to fix another day for the issuing of export licences when it is not possible for administrative reasons to respect the Wednesday;
Whereas it is necessary to simplify the procedure relative to licences issued immediately referred to in Article 4 by abolishing the quantitative limitation of applications and allowing automatic validating of these licences where the Commission has not taken special measures; whereas it is justified to exclude these licences from the possibility of withdrawing the application at the time of setting a single percentage of acceptance in order to avoid administrative difficulties;
Whereas it is necessary for licences which are issued immediately, to allow for a waiting period relative to the granting of the refund, during which licences may be amended, if need be, according to the special measures taken by the Commission;
Whereas it is necessary to adjust the rates of security set in Annex I to meet the recent amendments in the refund rates;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Pigmeat,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 1370/95 is hereby amended as follows:
1. Article 3 is amended as follows:
(a) Paragraph 1 is replaced by the following text:
'1. Applications for export licences may be lodged with the competent authorities from Monday to Friday of each week.`;
(b) in paragraph 3, 'Monday` is replaced by 'Wednesday`;
(c) in paragraph 6 the second indent is replaced by the following text:
'- or request immediate issuing of the licence, in which case the competent authority shall issue it without delay but no sooner than the normal issue date for the relevant week.`
(d) the following paragraph is added:
'7. By way of derogation from paragraph 3, the Commission can set a day other than Wednesday for the issuing of export licences when it is not possible to respect this day.`
2. Article 4 is replaced by the following text:
'Article 4
1. Upon written request of the operator, at the time of lodging the application, the competent authority shall immediately issue the licence applied for by indicating in section 22 at least one of the following:
- Certificado de exportación expedido sin perjuicio de medidas especiales de conformidad con el apartado 4 del artículo 3 del Reglamento (CE) n° 1370/95; la restitución se concederá como mínimo quince días laborables después de la fecha de su espedición
- Eksportlicens udstedt med forbehold af særforanstaltninger i henhold til artikel 3, stk. 4, i forordning (EF) nr. 1370/95; restitution ydes tidligst 15 dage efter udstedelsesdagen
- Ausfuhrlizenz, erteilt unter Vorbehalt der besonderen Maßnahmen gemäß Artikel 3 Absatz 4 der Verordnung (EG) Nr. 1370/95; Erstattung frühestens fünfzehn Arbeitstage nach dem Tag der Erteilung zu gewähren
- Ðéóôïðïéçôéêü åîáãùãÞò ðïõ åêäßäåôáé ìå ôçí åðéöýëáîç ôùí åéäéêþí ìÝôñùí óýìöùíá ìå ôï Üñèñï 3 ðáñÜãñáöïò 4 ôïõ êáíïíéóìïý (ÅÊ) áñéè. 1370/95 7 ç åðéóôñïöÞ ðñÝðåé íá ÷ïñçãçèåß ôï åíùñßôåñï äåêáðÝíôå åñãÜóéìåò çìÝñåò ìåôÜ ôçí çìåñïìçíßá ÝêäïóÞò ôïõ
- Export licence issued subject to any particular measures taken under Article 3 (4) of Regulation (EC) No 1370/95; refund to be granted at the earliest fifteen working days after the date of issuing
- Certificat d'exportation délivré sous réserve de mesures particulières en vertu de l'article 3 paragraphe 4 du règlement (CE) n° 1370/95; restitution à octroyer au plus tôt quinze jours ouvrables après la date de sa délivrance
- Titolo d'esportazione rilasciato sotto riserva d'adozione di misure specifiche a norma dell'articolo 3, paragrafo 4, del regolamento (CE) n. 1370/95; restituzione da concedere non prima che siano trascorsi quindici giorni lavorativi dalla data di rilascio del titolo
- Uitvoercertificaat afgegeven onder voorbehoud van bijzondere maatregelen als bedoeld in artikel 3, lid 4, van Verordening (EG) nr. 1370/95; de restitutie wordt niet vroeger dan 15 werkdagen na de datum van afgifte van het certificaat toegekend
- Certificado de exportação emitido sem prejuízo de medidas especiais em conformidade com o nº 4 do artigo 3º do Regulamento (CE) nº 1370/95; restituição a conceder nunca antes de quinze dias úteis após a data da sua emissão
- Vientitodistus annettu, jollei asetuksen (EY) N:o 1370/95 3 artiklan 4 kohdan mukaisista erityistoimenpiteistä muuta johdu; tuki annetaan aikaisintaan viidentoista työpäivan kuluttua antamispäivästä
- Exportlicens utfärdad med förbehåll för särskilda åtgärder enligt artikel 3.4 i förordning (EG) nr 1370/95. Exportbidrag skall beviljas tidigast femton arbetsdagar efter dagen för utfärdandet.
2. Where the Commission has not taken special measures under the terms of Article 3 (4) for the week in question, the licence is validated without other formalities from the Wednesday following the week in question.
3. Where the Commission has taken special measures under the terms of Article 3 (4) for the week in question, the competent authority requires within five working days as from the date of their publication, the operator to return the licence in order to amend it on the basis of these measures.
To this end, it shall cancel the indication referred to in paragraph 1 and shall indicate in section 22 at least one of the following:
(a) if a single percentage of acceptance has been set:
- Certificado de exportación con fijación anticipada de la restitución por una cantidad de [ . . . ] toneladas de los productos que se indican en las casillas 17 y 18
- Eksportlicens med forudfastsættelse af eksportrestitution for en mængde på [ . . . ] tons af de i rubrik 17 og 18 anførte produkter
- Ausfuhrlizenz mit Vorausfestsetzung der Erstattung für eine Menge von . . . Tonnen der in Feld 17 und 18 genannten Erzeugnisse
- Ðéóôïðïéçôéêü åîáãùãÞò ðïõ ðåñéëáìâÜíåé ôïí ðñïêáèïñéóìü ôçò åðéóôñïöÞò ãéá ìßá ðïóüôçôá [ . . . ] ôüíùí ðñïúüíôùí ðïõ åìöáßíïíôáé óôá ôåôñáãùíßäéá 17 êáé 18
- Export licence with advance fixing of the refund for a quantity of . . . tonnes of the products shown in sections 17 and 18
- Certificat d'exportation comportant fixation à l'avance de la restitution pour une quantité de [ . . . ] tonnes de produits figurant aux cases 17 et 18
- Titolo d'esportazione recante fissazione anticipata della restituzione per un quantitativo di [ . . . ] t di prodotti indicati nelle caselle 17 e 18
- Uitvoercertificaat met vaststelling vooraf van de restitutie voor . . . ton produkt vermeld in de vakken 17 en 18
- Certificado de exportação com prefixação da restituição para uma quantidade de [ . . . ] toneladas de produtos constantes das casas 17 e 18
- Vientitodistus, johon sisältyy tuen ennakkovahvistus [ . . . ] tonnille kohdassa 17 ja 18 mainittuja tuotteita
- Exportlicens med förutfastställelse av exportbidrag för en kvantitet av [ . . . ] ton av de produkter som nämns i fält 17 och 18;
(b) if the applications for licences have been rejected:
- Certificado de exportación sin derecho a restitución,
- Eksportlicens, der ikke giver ret til eksportrestitution,
- Ausfuhrlizenz ohne Anspruch auf Erstattung,
- Ðéóôïðïéçôéêü åîáãùãÞò ÷ùñßò äéêáßùìá ãéá ïðïéáäÞðïôå åðéóôñïöÞ,
- Export licence without entitlement to any refund,
- Certificat d'exportation ne donnant droit à aucune restitution,
- Titolo d'esportazione che non da diritto ad alcuna restituzione,
- Uitvoercertificaat dat geen recht op een restitutie geeft,
- Certificado de exportação que não dá direito a qualquer restituição,
- Vientitodistus ei oikeuta tukeen,
- Exportlicens som inte ger rätt till exportbidrag.
4. The provisions laid down in Article 3 (6) do not apply to licences issued under the terms of the provisions of this Article.
5. The refund for licences issued pursuant to the provisions of this Article may be granted at the earliest fifteen working days after the date of their issuing.`
3. Article 7 (1) is replaced by the following text:
'1. Member States shall communicate to the Commission, each Friday from 1 p.m., by fax and for the preceding period:
(a) the applications for export licences with advance fixing of refunds referred to in Article 1 which were lodged from Monday to Friday of the same week;
(b) the quantities for which export licences have been issued on the preceding Wednesday;
(c) the quantities for which applications for export licences have been withdrawn pursuant to Article 3 (6) during the preceding week.`
4. Annex I is replaced by the Annex to this Regulation.
5. In Annex II, part B, 'Monday` is replaced by 'Wednesday`.
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
It shall apply to export licences applied for as from 1 July 1996.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 21 June 1996.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ No L 282, 1. 11. 1975, p. 1.
(2) OJ No L 349, 31. 12. 1994, p. 105.
(3) OJ No L 133, 17. 6. 1995, p. 9.
(4) OJ No L 285, 29. 11. 1995, p. 11.
ANNEX
'ANNEX I
>TABLE>
