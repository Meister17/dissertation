Judgment of the Court (Fifth Chamber) of 5 October 2006 — Commission of the European Communities v Republic of Austria
(Case C-226/05) [1]
Parties
Applicant: Commission of the European Communities (represented by: B. Schima, Agent)
Defendant: Republic of Austria (represented by: E. Riedl, Agent)
Re:
Failure of a Member State to fulfil obligations — Failure fully to transpose Articles 8(2)(b), 11, 12 and 24(1) of Council Directive 96/82/EC of 9 December 1996 on the control of major-accident hazards involving dangerous substances (OJ 1997 L 10, p. 13)
Operative part of the judgment
The Court (Fifth Chamber):
1. Declares that, by failing to transpose, within the period prescribed:
- the provisions of Council Directive 96/82/EC of 9 December 1996 on the control of major-accident hazards involving dangerous substances into the national legislation on blasting supplies and explosives and the legislation of the Land Salzburg on electricity;
- Article 11 of Directive 96/82 in the Länder of Salzburg, Styria and Tirol;
- Article 12 of Directive 96/82 in the Land of Upper Austria; and
- Article 8(2)(b) of Directive 96/82 in the Länder of Upper Austria, Salzburg and Tirol,
the Republic of Austria has failed to fulfil its obligations under that directive;
2. Orders the Republic of Austria to pay the costs.
[1] OJ C 171, 09.07.2005.
--------------------------------------------------
