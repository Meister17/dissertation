COMMISSION DIRECTIVE 1999/58/EC
of 7 June 1999
adapting to technical progress Council Directive 79/533/EEC relating to coupling and reversing devices for wheeled agricultural or forestry tractors
(Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 74/150/EEC of 4 March 1974 on the approximation of the laws of the Member States relating to the type-approval of wheeled agricultural or forestry tractors(1), as last amended by Directive 97/54/EC of the European Parliament and of the Council(2), and in particular Article 11 thereof,
Having regard to Council Directive 79/533/EEC of 17 May 1979 on the approximation of the laws of the Member States relating to the coupling devide and the reverse of wheeled agricultural or forestry tractors(3), as last amended by Directive 97/54/EC, and in particular Article 4 thereof,
(1) Whereas, in order to improve safety, it is now necessary to clarify the requirements for the coupling device;
(2) Whereas the measures provided for in this Directive are in accordance with the opinion of the Committee for Adaptation to Technical Progress established by Article 12 of Directive 74/150/EEC,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Item 3 of Annex I to Directive 79/533/EEC is hereby amended as follows:
1. the first paragraph is replaced by the following: "The device shall be of the slotted-jaw type. The opening at the centre of the locking pin shall be 60 mm + 0,5/- 1,5 mm and the depth of the jaw measured from the centre of the pin shall be 62 mm +- 0,5 mm.";
2. the diagram is deleted.
Article 2
1. From 1 July 2000, Member States may not:
- refuse to grant EC type-approval, to issue the document provided for in the third indent of Article 10(1) of Directive 74/150/EEC, or to grant national type-approval, in respect of a type of tractor, or
- prohibit the entry into service of tractors,
if the tractors in question comply with the requirements of Directive 79/533/EEC, as amended by this Directive.
2. From 1 January 2001, Member States:
- may no longer issue the document provided for in the third indent of Article 10(1) of Directive 74/150/EEC in respect of a type of tractor which does not meet the requirements of Directive 79/533/EEC, as amended by this Directive,
- may refuse to grant national type-approval in respect of a type of tractor which does not meet the requirements of Directive 79/533/EEC, as amended by this Directive.
Article 3
1. Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive by 30 June 2000 at the latest. They shall forthwith inform the Commission thereof.
When Member States adopt those provision, they shall contain a reference to this Directive or be accompanied by such a reference on the occasion of their official publication. Member States shall determine how such reference is to be made.
2. Member States shall communicate to the Commission the text of the main provisions of domestic law that they adopt in the field governed by this Directive.
Article 4
This Directive shall enter into force on the 20th day following its publication in the Official Journal of the European Communities.
Article 5
This Directive is addressed to the Member States.
Done at Brussels, 7 June 1999.
For the Commission
Martin BANGEMANN
Member of the Commission
(1) OJ L 84, 28.3.1974, p. 10.
(2) OJ L 277, 10.10.1997, p. 24.
(3) OJ L 145, 13.6.1979, p. 20.
