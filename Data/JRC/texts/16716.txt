Council Decision
of 15 November 2004
appointing two Czech members and three alternate Czech members of the Committee of the Regions
(2004/779/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 263 thereof,
Having regard to the proposal from the Czech Government,
Whereas:
(1) On 22 January 2002 [1] the Council adopted Decision 2002/60/EC appointing the members and alternate members of the Committee of the Regions.
(2) Two seats as members of the Committee of the Regions have become vacant following the resignations of Mr Jan BEZINA, notified to the Council on 22 July 2004, and of Mr Oldich VLASÁK, notified to the Council on 30 August 2004; one seat as an alternate member of the Committee of the Regions has become vacant following the resignation of Mr Petr DUCHO, notified to the Council on 30 August 2004. Two seats as alternate members of the Committee of the Regions have become vacant following the nomination of Mr Frantiek SLAVÍK and Mr Tomá ÚLEHLA to be full members,
HAS DECIDED AS FOLLOWS:
Sole Article
The following are appointed to the Committee of the Regions:
(a) as members:
1. Mr Frantiek SLAVÍK,
President of the Regional Council of Zlínský kraj
to replace M. Jan BEZINA
2. Mr Tomá ÚLEHLA
Mayor of the City of Zlín, Zlínský kraj
to replace M. Oldich VLASÁK;
(b) as alternate members:
1. Ms Ivana ERVINKOVÁ
Mayor of the Municipality of Kostelec nad Orlicí, Královehradecký kraj
to replace Mr Petr DUCHO
2. Mr Ivan KOSATÍK,
2nd Deputy of the President of the Regional Council of Olomoucký kraj
to replace Mr Frantiek SLAVÍK
3. Mr Petr OSVALD
Member of the local authority of the City of Plze, Plzeský kraj
to replace Mr Tomá ÚLEHLA;
for the remainder of their term of office, which runs until 25 January 2006.
Done at Brussels, 15 November 2004.
For the Council
The President
M. van der Hoeven
--------------------------------------------------
[1] OJ L 24, 26.1.2002, p. 38.
