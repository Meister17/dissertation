COMMISSION REGULATION (EC) No 1555/96 of 30 July 1996 on rules of application for additional import duties on fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 1035/72 of 18 May 1972 on the common organization of the market in fruit and vegetables (1), as last amended by Commission Regulation (EC) No 1363/95 (2), and in particular Article 24 (4) thereof,
Whereas under Regulation (EEC) No 1035/72 import duty additional to that provided for in the Common Customs Tariff ('additional duty`) can be imposed on certain products covered by that Regulation if the terms of Article 5 of the Agreement on Agriculture (3) are met, unless there is no risk of disturbance of the Community market or the impact would be disproportionate to the end sought;
Whereas additional duty may be imposed if import volumes of the products concerned, determined from import licences issued by Member States or by procedures introduced under a preferential agreement, exceed trigger levels determined by product and period of application as specified in Article 5 (4) of the Agreement on Agriculture;
Whereas additional duty may be imposed only on imports the tariff classification of which made in line with Article 5 of Commission Regulation (EC) No 3223/94 (4), as last amended by Regulation (EC) No 2933/95 (5), entails application of the highest specific duty and on imports made outside the tariff quotas set in the World Trade Organization framework; whereas no additional duty may be imposed on products which enjoy preferences in respect of the entry price, in so far as their tariff classification does not entail application of the highest specific duty;
Whereas, in the case of imports enjoying tariff preference as to ad valorem duty, calculation of the additional duty must take account of this;
Whereas goods en route to the Community are exempt from additional duty; whereas specific provisions on these should therefore be enacted;
Whereas the introduction of the import licence arrangements will not prevent their being replaced by a rapid computerized procedure for recording imports as soon as it is legally and practically possible to set one up; whereas there will be an evaluation in this respect by 31 December 1997;
Whereas the Management Committee for fresh Fruit and Vegetables has not delivered an opinion within the time limit set by its chairman,
HAS ADOPTED THIS REGULATION:
Article 1
Additional import duty as referred to in Article 24 (1) of Regulation (EEC) No 1035/72, termed 'additional duty` below, may be applied to the products listed in the Annex hereto on the conditions set out below.
Article 2
Trigger levels and the applicable periods for each of the products listed in the Annex shall be set each year.
Article 3
1. If it is found that the quantity imported of a product to which the special safeguard clause applies has in respect of a given period exceeded, as determined from the issued import licences drawn up in accordance with Article 22 (2) of Regulation (EEC) No 1035/72, or by procedure introduced under a preferential agreement, the trigger level set under Article 2, the Commission shall impose an additional duty.
2. It shall apply to goods covered by an import licence issued after the date of introduction of the duty and to imports effected after that date in the event of a procedure, within the meaning of paragraph 1, introduced under a preferential agreement provided that:
- their tariff classification determined in line with Article 5 of Regulation (EC) No 3223/94 entails application of the highest specific duties applicable to imports of the origin in question,
- importation is made during the period of application of the additional duty.
Article 4
1. The additional duty imposed under Article 3 shall be one third of the customs duty applicable to the product given in the Common Customs Tariff.
2. However, for imports enjoying tariff preference as to ad valorem duty the additional duty shall be one third of the specific duty on the product in so far as Article 3 (2) applies.
Article 5
1. The following are exempt from additional duty:
(a) goods imported against the tariff quotas listed in Annex 7 to the combined nomenclature;
(b) goods en route to the Community as defined in paragraph 2.
2. Goods shall be considered to be en route to the Community that:
- left the country of origin before the decision to impose additional duty, and
- are being transported under cover of a transport document valid from the place of loading in the country of origin to the place of unloading in the Community, drawn up before imposition of additional duty.
3. Interested parties shall provide evidence to the satisfaction of the customs authorities that the requirements of paragraph 2 are met.
However, these authorities may deem that goods left their country of origin before the date of imposition of additional duty if one of the following documents is provided:
- for sea transport, the bill of lading showing that loading took place before that date,
- for rail transport, the waybill accepted by the rail authorities of the country of origin before that date,
- for road transport, the road carriage contract (CMR) or another transit document made out in the country of origin before that date, if the conditions laid down in bilateral or multilateral arrangements concluded in the context of Community transit or common transit are observed,
- for air transport, the air way bill showing that the airline accepted the goods before that date.
Article 6
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 30 July 1996.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ No L 118, 20. 5. 1972, p. 1.
(2) OJ No L 132, 16. 6. 1995, p. 8.
(3) OJ No L 336, 23. 12. 1994, p. 22.
(4) OJ No L 337, 24. 12. 1994, p. 66.
(5) OJ No L 307, 20. 12. 1995, p. 21.
ANNEX
>TABLE>
