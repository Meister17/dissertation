Commission Regulation (EC) No 1343/2001
of 3 July 2001
amending Regulation (EC) No 449/2001 laying down detailed rules for applying Council Regulation (EC) No 2201/96 as regards the aid scheme for products processed from fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 2201/96 of 28 October 1996 on the common organisation of the markets in processed fruit and vegetable products(1), as last amended by Regulation (EC) No 2699/2000(2), and in particular Article 6(1) thereof,
Whereas:
(1) Article 3(4)(e) of Commission Regulation (EC) No 449/2001(3) provides that the terms governing the payment of the raw material by the processor to the producer organisation are to be laid down in the contracts and stipulates in particular that the payment deadline cannot exceed 60 days from the date of delivery of each consignment.
(2) In order to make provision for the requisite flexibility and to make the scheme easier to administer, that deadline should be extended to the end of the second month following the month of delivery. This provision should be available only for contracts concluded after the entry into force of this Regulation.
(3) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Processed Fruit and Vegetables,
HAS ADOPTED THIS REGULATION:
Article 1
Article 3(4)(e) is hereby replaced by the following: "(e) the price to be paid for the raw materials, which may vary by variety and/or quality and/or delivery period.
In the case of tomatoes, peaches and pears, the contract shall also indicate the delivery stage at which that price applies and the payment terms. Any payment deadline may not exceed two months from the end of the month of delivery of each consignment."
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 3 July 2001.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 297, 21.11.1996, p. 29.
(2) OJ L 311, 12.12.2000, p. 9.
(3) OJ L 64, 6.3.2001, p. 16.
