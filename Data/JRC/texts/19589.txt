Directive 2003/65/EC of the European Parliament and of the Council
of 22 July 2003
amending Council Directive 86/609/EEC on the approximation of laws, regulations and administrative provisions of the Member States regarding the protection of animals used for experimental and other scientific purposes
(Text with EEA relevance)
THE EUROPEAN PARLIAMENT AND THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 95 thereof,
Having regard to the proposal from the Commission(1),
Having regard to the opinion of the European Economic and Social Committee(2),
Acting in accordance with the procedure laid down in Article 251 of the Treaty(3),
Whereas:
(1) On 23 March 1998 the Council adopted Decision 1999/575/EC concerning the conclusion by the Community of the European Convention for the protection of vertebrate animals used for experimental and other scientific purposes(4) (hereinafter referred to as the Convention).
(2) Council Directive 86/609/EEC(5) is the implementing tool of the Convention incorporating the same objectives as the Convention.
(3) Annex II to Directive 86/609/EEC containing guidelines for accommodation and care of animals takes over Appendix A to the Convention. The provisions contained in Appendix A to the Convention and the Annexes to the said Directive are of a technical nature.
(4) It is necessary to ensure the consistency of the Annexes to Directive 86/609/EEC with the latest scientific and technical developments and results of research within the fields covered. Currently, changes to the Annexes can only be adopted by the long-drawn-out co-decision procedure with the consequence that their content is lagging behind the latest developments in the field.
(5) The measures necessary for the implementation of this Directive should be adopted in accordance with Council Decision 1999/468/EC of 28 June 1999 laying down the procedures for the exercise of implementing powers conferred on the Commission(6).
(6) Therefore Directive 86/609/EEC should be amended accordingly,
HAVE ADOPTED THIS DIRECTIVE:
Article 1
In Directive 86/609/EEC the following Articles shall be inserted:
"Article 24a
The measures necessary for the implementation of this Directive relating to the subject matters referred to below shall be adopted in accordance with the regulatory procedure set out in Article 24b(2):
- Annexes to this Directive.
Article 24b
1. The Commission shall be assisted by a Committee.
2. Where reference is made to this paragraph, Articles 5 and 7 of Decision 1999/468/EC shall apply, having regard to the provisions of Article 8 thereof.
The period laid down in Article 5(6) of Decision 1999/468/EC shall be set at three months.
3. The Committee shall adopt its rules of procedure."
Article 2
Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive before 16 September 2004. They shall forthwith inform the Commission thereof.
When Member States adopt these measures, they shall contain a reference to this Directive or be accompanied by such a reference on the occasion of their official publication. The methods of making such reference shall be laid down by the Member States.
Article 3
This Directive shall enter into force on the day of its publication in the Official Journal of the European Union.
Article 4
This Directive is addressed to the Member States.
Done at Brussels, 22 July 2003.
For the European Parliament
The President
P. Cox
For the Council
The President
G. Alemanno
(1) OJ C 25 E, 29.1.2002, p. 536.
(2) OJ C 94, 18.4.2002, p. 5.
(3) Opinion of the European Parliament of 2 July 2002 (not yet published in the Official Journal), Council Common Position of 17 March 2003 (OJ C 113 E, 13.5.2003, p. 59) and Decision of the European Parliament of 19 June 2003 (not yet published in the Official Journal).
(4) OJ L 222, 24.8.1999, p. 29.
(5) OJ L 358, 18.12.1986, p. 1.
(6) OJ L 184, 17.7.1999, p. 23.
