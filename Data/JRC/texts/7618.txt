*****
COMMISSION REGULATION (EEC) No 3425/86
of 10 November 1986
amending Regulation (EEC) No 2388/84 on special detailed rules on the application of export refunds in the case of certain preserved beef and veal products
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 805/68 of 27 June 1968 on the common organization of the market in beef and veal (1), as last amended by Regulation (EEC) No 3768/85 (2), and in particular Article 18 (6) thereof,
Whereas Commission Regulation (EEC) No 2388/84 (3), as amended by Regulation (EEC) No 1032/86 (4), laid down that certain preserved products which meet the conditions laid down in that Regulation and are exported to third countries will be eligible for a special refund where they are manufactured under the arrangements provided for in Article 4 of Council Regulation (EEC) No 565/80 (5); whereas, in the light of experience gained since the entry into force of this Regulation, the maximum weight of these preserved products should be raised from 500 to 2 500 grams;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Beef and Veal,
HAS ADOPTED THIS REGULATION:
Article 1
The third indent of Article 2 of Regulation (EEC) No 2388/84 is replaced by the following:
'- be put in tins of a unit weight of not more than 2 500 grams net'.
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Communities.
It shall apply with effect from 3 November 1986.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 10 November 1986.
For the Commission
Frans ANDRIESSEN
Vice-President
(1) OJ No L 148, 28. 6. 1968, p. 24.
(2) OJ No L 362, 31. 12. 1985, p. 8.
(3) OJ No L 221, 18. 8. 1984, p. 28.
(4) OJ No L 95, 10. 4. 1986, p. 17.
(5) OJ No L 62, 7. 3. 1980, p. 5.
