Commission Regulation (EC) No 1317/2006
of 4 September 2006
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 5 September 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 4 September 2006.
For the Commission
Jean-Luc Demarty
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 386/2005 (OJ L 62, 9.3.2005, p. 3).
--------------------------------------------------
ANNEX
to Commission Regulation of 4 September 2006 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code [1] | Standard import value |
07020000 | 052 | 87,3 |
999 | 87,3 |
07070005 | 052 | 79,0 |
999 | 79,0 |
07099070 | 052 | 74,2 |
999 | 74,2 |
08055010 | 388 | 68,2 |
524 | 49,5 |
528 | 54,2 |
999 | 57,3 |
08061010 | 052 | 79,6 |
220 | 178,5 |
400 | 181,8 |
624 | 120,1 |
999 | 140,0 |
08081080 | 388 | 85,8 |
400 | 93,2 |
508 | 87,9 |
512 | 94,9 |
528 | 59,3 |
720 | 81,1 |
800 | 173,9 |
804 | 108,3 |
999 | 98,1 |
08082050 | 052 | 121,7 |
388 | 91,7 |
720 | 88,3 |
999 | 100,6 |
08093010, 08093090 | 052 | 123,8 |
096 | 12,8 |
999 | 68,3 |
08094005 | 052 | 92,3 |
066 | 46,7 |
098 | 41,6 |
624 | 150,6 |
999 | 82,8 |
[1] Country nomenclature as fixed by Commission Regulation (EC) No 750/2005 (OJ L 126, 19.5.2005, p. 12). Code "999" stands for "of other origin".
--------------------------------------------------
