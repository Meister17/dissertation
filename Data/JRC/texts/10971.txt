Euro exchange rates [1]
31 May 2006
(2006/C 128/01)
| Currency | Exchange rate |
USD | US dollar | 1,2868 |
JPY | Japanese yen | 144,32 |
DKK | Danish krone | 7,4578 |
GBP | Pound sterling | 0,68590 |
SEK | Swedish krona | 9,2757 |
CHF | Swiss franc | 1,5600 |
ISK | Iceland króna | 92,33 |
NOK | Norwegian krone | 7,8155 |
BGN | Bulgarian lev | 1,9558 |
CYP | Cyprus pound | 0,5750 |
CZK | Czech koruna | 28,208 |
EEK | Estonian kroon | 15,6466 |
HUF | Hungarian forint | 262,09 |
LTL | Lithuanian litas | 3,4528 |
LVL | Latvian lats | 0,6960 |
MTL | Maltese lira | 0,4293 |
PLN | Polish zloty | 3,9367 |
RON | Romanian leu | 3,5400 |
SIT | Slovenian tolar | 239,65 |
SKK | Slovak koruna | 37,770 |
TRY | Turkish lira | 2,0039 |
AUD | Australian dollar | 1,6953 |
CAD | Canadian dollar | 1,4107 |
HKD | Hong Kong dollar | 9,9826 |
NZD | New Zealand dollar | 2,0141 |
SGD | Singapore dollar | 2,0284 |
KRW | South Korean won | 1216,93 |
ZAR | South African rand | 8,5459 |
CNY | Chinese yuan renminbi | 10,3186 |
HRK | Croatian kuna | 7,2650 |
IDR | Indonesian rupiah | 11909,33 |
MYR | Malaysian ringgit | 4,672 |
PHP | Philippine peso | 68,072 |
RUB | Russian rouble | 34,6750 |
THB | Thai baht | 49,051 |
[1] Source: reference exchange rate published by the ECB.
--------------------------------------------------
