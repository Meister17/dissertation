Decision of the EEA Joint Committee No 36/2005
of 11 March 2005
amending Annex II (Technical regulations, standards, testing and certification) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex II to the Agreement was amended by Decision of the EEA Joint Committee No 9/2005 of 8 February 2005 [1].
(2) Directive 2004/12/EC of the European Parliament and of the Council of 11 February 2004 amending Directive 94/62/EC on packaging and packaging waste [2] is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
Point 7 (Directive 94/62/EC of the European Parliament and of the Council) in Chapter XVII of Annex II to the Agreement shall be amended as follows:
1. The following shall be added before the transitional arrangements:
", as amended by:
- 32004 L 0012: Directive 2004/12/EC of the European Parliament and of the Council of 11 February 2004 (OJ L 47, 18.2.2004, p. 26)."
2. The following shall be added after the transitional arrangements:
"The provisions of the Directive shall, for the purposes of this Agreement, be read with the following adaptations:
In Article 6(7), the words ", Iceland" shall be inserted after the word "Ireland" and ", the presence of rural areas and low population density" shall be inserted after the word "areas"."
Article 2
The texts of Directive 2004/12/EC in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union , shall be authentic.
Article 3
This Decision shall enter into force on 12 March 2005, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [3].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 11 March 2005.
For the EEA Joint Committee
The President
Richard Wright
[1] OJ L 161, 23.6.2005, p. 20.
[2] OJ L 47, 18.2.2004, p. 26.
[3] No constitutional requirements indicated.
--------------------------------------------------
