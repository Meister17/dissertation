Commission Regulation (EC) No 1571/2003
of 5 September 2003
amending the specifications of a name appearing in the Annex to Regulation (EC) No 1107/96 (Parmigiano Reggiano)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2081/92 of 14 July 1992 on the protection of geographical indications and designations of origin for agricultural products and foodstuffs(1), as last amended by Regulation (EC) No 806/2003(2), and in particular Article 9 thereof,
Whereas:
(1) In accordance with Article 9 of Regulation (EEC) No 2081/92, the Italian authorities have requested in respect of the name "Parmigiano Reggiano" registered as a protected designation of origin by Commission Regulation (EC) No 1107/96 of 12 June 1996 on the registration of geographical indications and designations of origin under the procedure laid down in Article 17 of Council Regulation (EEC) No 2081/92(3), as last amended by Regulation (EC) No 1204/2003(4), the amendment of the description, method of production, labelling and rules on feed for cows.
(2) Following examination of this request for amendment, it has been decided that the amendments concerned are not minor.
(3) In accordance with the procedure laid down in Article 9 of Regulation (EEC) No 2081/92 and since the amendments are not minor, the Article 6 procedure applies mutatis mutandis.
(4) It has been decided that the amendments in this case comply with Regulation (EEC) No 2081/92. No statement of objection, within the meaning of Article 7 of the Regulation, has been sent to the Commission following the publication of these amendments in the Official Journal of the European Union(5).
(5) Consequently, these amendments must be registered and published in the Official Journal of the European Union,
HAS ADOPTED THIS REGULATION:
Article 1
The amendments set out in the Annex to this Regulation are hereby registered and published in accordance with Article 6(4) of Regulation (EEC) No 2081/92.
Article 2
This Regulation shall enter into force on the 20th day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 5 September 2003.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 208, 24.7.1992, p. 1.
(2) OJ L 122, 16.5.2003, p. 1.
(3) OJ L 148, 21.6.1996, p. 1.
(4) OJ L 168, 5.7.2003, p. 10.
(5) OJ C 275, 12.11.2002, p. 14 (Parmigiano Reggiano).
ANNEX
ITALY
Parmigiano Reggiano
- Description:
The mould ranges in height from not less than 20 cm to not more than 26 cm with resulting variations in weight, the minimum being set at 30 kg.
The oiling of the exterior of the mould, a possibility almost never availed of, has now been discontinued as it proved to be a drawback when marketing the product.
- Method of production:
The use of raw milk is specified. Heat treatment may not be applied and no additives may be used.
Milking operations, the maximum period for which milking takes place, conservation and the partial skimming of the milk by removal of the cream that rises to the top in open-air tanks, the adding of natural whey inoculum produced by spontaneous acidification of the whey residue from the previous day's processing, the coagulation of the milk, the dripping of the curds and the moulding of the cheese are explained as appropriate in accordance with local, trustworthy and established practice, as traditionally observed.
- Labelling:
The origin of the cheese is stencilled on the product and a casein nameplate is attached bearing the words "Parmigiano Reggiano" together with codes identifying the mould so that the product can be accurately traced.
Selection is marked by the Consorzio di tutela del Parmigiano Reggiano using indelible marks for the selection classes following verification by the approved inspection agency.
- Other - Rules on feed for cows:
A list of prohibited fodder and by-products is specified, reflecting currently available technical knowledge of animal nutrition.
Aspects of the rules on feed that have no impact on the link with the territory have been simplified and the rules themselves have been tightened as regards the use of the "unifeed technique" for administering feed.
