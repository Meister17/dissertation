Appeal brought on 14 December 2005 by Common Market Fertilizers SA (CMF) against the judgment given on 27 September 2005 by the First Chamber (Extended Composition) of the Court of First Instance of the European Communities in Joined Cases T-134/03 and T-135/03 between Common Market Fertilizers SA (CMF) and the Commission of the European Communities
An appeal against the judgment given on 27 September 2005 by the First Chamber (Extended Composition) of the Court of First Instance of the European Communities in Joined Cases T-134/03 and T-135/03 between Common Market Fertilizers SA (CMF) and the Commission of the European Communities was brought before the Court of Justice of the European Communities on 14 December 2005 by Common Market Fertilizers SA (CMF), represented by A. Sutton and N. Flandin, avocats.
The appellant claims that the Court should:
- set aside the judgment under appeal in its entirety;
- grant all of the claims submitted at first instance by the appellant;
- order the Commission to pay the costs both of the appeal and of the proceedings at first instance.
Pleas in law and main arguments:
In support of its appeal, the appellant points out four errors of law made by the Court of First Instance consisting in:
- an incomplete presentation of the legal context resulting in a misinterpretation of Regulation No 3319/94 [1] as to the conditions required to impose a specific duty, and an incorrect legal interpretation of the nature of the committee consulted;
- an incomplete presentation of the facts resulting in a clear distortion of those facts, and a misapplication of Regulation No 3319/94 as to whether there is a situation of indirect invoicing;
- an incorrect legal interpretation as regards the infringement of essential procedural requirements and specifically as regards the legal nature of the committee consulted; and
- an incorrect legal interpretation as regards the conditions for application of Article 239 of the Community Customs Code [2] and specifically as regards the application of the condition of no obvious negligence.
[1] Council Regulation (EC) No 3319/94 of 22 December 1994 imposing a definitive anti-dumping duty on imports of urea ammonium nitrate solution originating in Bulgaria and Poland, exported by companies not exempted from the duty, and collecting definitively the provisional duty imposed (OJ L 350, 31.12.1994, p. 20)
[2] Council Regulation (EEC) No 2913/92 of 12 October 1992 establishing the Community Customs Code (OJ L 302, 19.10.1992, p. 1)
--------------------------------------------------
