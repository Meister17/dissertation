COMMISSION REGULATION (EEC) No 762/92 of 27 March 1992 modifying Annex V to Council Regulation (EEC) No 2377/90 laying down a Community procedure for the establishment of maximum residue limits of veterinary medicinal products in foodstuffs of animal origin
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 2377/90 of 26 June 1990 laying down a Community procedure for the establishment of maximum residue limits of veterinary medicinal products in foodstuffs of animal origin (1), as amended by Commission Regulation (EEC) No 675/92 (2), and in particular Article 11 thereof,
Whereas it is desirable in the interests of administrative efficiency that the information and particulars to be included in an application for the establishment of a maximum residue limit for a pharmacologically active substance used in veterinary medicinal products in accordance with Regulation (EEC) No 2377/90 should correspond as closely as possible to the information and particulars to be submitted to Member States in an application for authorization to place a veterinary medicinal product on the market submitted in accordance with Article 5 of Council Directive 81/851/EEC of 28 September 1981 on the approximation of the laws of the Member States relating to veterinary medicinal products (3), as amended by Directive 90/676/EEC (4);
Whereas it is necessary to amend Annex V to Regulation (EEC) No 2377/90 to take account of the changes to the requirements for the testing of veterinary medicinal products introduced by Commission Directive 92/18/EEC of 20 March 1992 modifying the Annex to Council Directive 81/852/EEC on the approximation of laws of the Member States relating to analytical, pharmaco-toxicological and clinical standards and protocols in respect of the testing of veterinary medicinal products;
Whereas the provisions of this Regulation are in accordance with the opinion of the Committee on the Adaptation to Technical Progress of the Directives on the Removal of Technical Barriers to Trade in the Veterinary Medicinal Products Sector established under Article 2b of Council Directive 81/852/EEC (5), as amended by Directive 87/20/EEC (6),
HAS ADOPTED THIS REGULATION:
Article 1
Annex V to Regulation (EEC) No 2377/90 is hereby replaced by the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the date of its publication in the Official Journal of the European Communities. This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 27 March 1992.
For the Commission
Martin BANGEMANN
Vice-President
(1) OJ No L 224, 18. 8. 1990, p. 1.
(2) OJ No L 73, 19. 3. 1992, p. 8.
(3) OJ No L 317, 6. 11. 1981, p. 1.
(4) OJ No L 373, 31. 12. 1990, p. 15.
(5) OJ No L 317, 6. 11. 1981, p. 16.
(6) OJ No L 15, 17. 1. 1987, p. 34.
ANNEX
'ANNEX V
Information and particulars to be included in an application for the establishment of a maximum residue limit for a pharmacologically active substance used in veterinary medicinal products
Administrative particulars
1 Name or corporate name and permanent address of the applicant.
2 Name of the veterinary medicinal product.
3 Qualitative and quantitative composition in terms of active principles, with mention of the international non-proprietary name recommended by the World Health Organization, where such name exists.
4 Manufacturing authorization, if any.
5 Marketing authorization, if any.
6 Summary of the characteristics of the veterinary medicinal product(s) prepared in accordance with Article 5a of Directive 81/851/EEC.
A. Safety documentation
A.0. Expert report
A.1. Precise identification of the substance concerned by the application
1.1 International non-proprietary name (INN).
1.2 International Union of Pure and Applied Chemistry (IUPAC) name.
1.3 Chemical Abstract Service (CAS) name.
1.4 Classification:
- therapeutic;
- pharmacological.
1.5 Synonyms and abbreviations.
1.6 Structural formula.
1.7 Molecular formula.
1.8 Molecular weight.
1.9 Degree of impurity.
1.10 Qualitative and quantitative composition of impurities.
1.11 Description of physical properties:
- melting point;
- boiling point;
- vapour pressure;
- solubility in water and organic solvents, expressed in grams per litre, with indication of temperature;
- density;
- refractive index, rotation, etc.
A.2. Relevant pharmacological studies
2.1 Pharmacodynamics.
2.2 Pharmacokinetics.
A.3. Toxicological studies
3.1 Single dose toxicity.
3.2 Repeated dose toxicity.
3.3 Tolerance in the target species of animal.
3.4 Reproductive toxicity, including teratogenicity.
3.4.1 Study of the effects on reproduction.
3.4.2 Embryotoxicity / fetotoxicity, including teratogenicity.
3.5 Mutagenicity.
3.6 Carcinogenicity.
A.4. Studies of other effects
4.1 Immunotoxicity.
4.2 Microbiological properties of residues.
4.2.1 On the human gut flora;
4.2.2 On the organisms and microorganisms used for industrial food-processing.
4.3 Observations in humans.
B. Residue documentation
B.0 Expert report
B.1. Precise identification of the substance concerned by the application The substance concerned should be identified in accordance with point
A.1. However, where the application relates to one or more veterinary medicinal products, the product itself should be identified in detail, including: - qualitative and quantitative composition; - purity; - identification of the manufacturer's batch used in the studies; relationship to the final product; - specific activity and radio-purity of labelled substances; - position of labelled atoms on the molecule.
B.2. Residue studies
2.1 Pharmacokinetics
(absorption, distribution, biotransformation, excretion).
2.2 Depletion of residues.
2.3 Elaboration of maximum residue limits (MRLS).
B.3. Routine analytical method for the detection of residues
3.1 Description of the method.
3.2 Validation of the method.
3.2.1 specificity;
3.2.2 accuracy, including sensitivity;
3.2.3 precision;
3.2.4 limit of detection;
3.2.5 limit of quantitation;
3.2.6 practicability and applicability under normal laboratory conditions;
3.2.7 susceptibility to interference.'
