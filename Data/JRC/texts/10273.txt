Order of the Court of First Instance of 5 July 2006 — Comunidad Autónoma de Valencia/Commission
(Case T-357/05) [1]
Parties
Applicant: Comunidad Autónoma de Valencia — Generalitat Valenciana (Spain) (represented by J.-V. Sánchez-Tarazaga Marcelino)
Defendant: Commission of the European Communities (represented by: L. Escobar Guerrero and A. Weimar, Agents)
Re:
Annulment of Commission Decision C (2005) 1867 final of 27 June 2005, concerning the reduction of the assistance initially granted from the Cohesion Fund to Project Group No 97/11/61/028, concerning the collection and treatment of waste waters on the Mediterranean coast of the Comunidad Autónoma de Valencia (Spain).
Operative part of the order
(1) The action is dismissed as being manifestly inadmissible.
(2) There is no need to adjudicate on the application to intervene.
(3) The applicant shall bear its own costs, as well as those of the Commission, with the exception of the costs relating to the application to intervene.
(4) The applicant, the Commission and the Comunidad autónoma de Andalucia — Junta de Andalucia shall bear their own costs relating to their respective applications to intervene.
[1] OJ C 281 of 12.11.2005.
--------------------------------------------------
