Order of the Civil Service Tribunal of 30 June 2006 — Colombani
v Commission
(Case F-75/05) [1]
(2006/C 190/72)
Language of the case: French
The President of the First Chamber has ordered that the case be removed from the register, following amicable settlement.
[1] OJ C 229, 17.9.2005 (case initially registered before the Court of First Instance of the European Communities under number T-294/05 and transferred to the European Union Civil Service Tribunal by order of 15.12.2005).
--------------------------------------------------
