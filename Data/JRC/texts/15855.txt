Council Decision
of 15 May 2006
authorising the Kingdom of Spain to apply a measure derogating from Article 11 and Article 28e of the Sixth Directive 77/388/EEC on the harmonisation of the laws of the Member States relating to turnover taxes
(2006/387/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community,
Having regard to the Sixth Council Directive 77/388/EEC of 17 May 1977 on the harmonisation of the laws of the Member States in relation to turnover taxes — Common System of Value Added Tax; uniform basis of assessment [1], and in particular Article 27 thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) Under Article 27(1) of Directive 77/388/EEC, the Council, acting unanimously on a proposal from the Commission, may authorise any Member State to introduce special measures for derogation from that Directive, in order to simplify the procedure for charging the tax or to prevent certain types of tax evasion or avoidance.
(2) In a letter dated 21 June 2005 and received in the Secretariat-General of the Commission on 22 July 2005, the Kingdom of Spain sought authorisation to introduce a measure derogating from the provisions of Directive 77/388/EEC governing the taxable amount for value added tax (VAT) purposes.
(3) In accordance with Article 27(2) of Directive 77/388/EEC, the Commission informed the other Member States of the request made by the Kingdom of Spain in a letter dated 7 October 2005. In a letter dated 10 October 2005, the Commission notified the Kingdom of Spain that it had all the information it considered necessary for appraisal of the request.
(4) Article 11(A)(1)(a) of Directive 77/388/EEC establishes the taxable amount of a supply for VAT purposes to be everything which constitutes the consideration paid for the supply. Article 28e(1) of that Directive governs the taxable amount of intra-Community acquisitions, by reference to Article 11(A).
(5) The measure requiring a derogation is intended to counter tax losses arising from the manipulation of the taxable amount of supplies of goods, services and intra-Community acquisitions subject to VAT where a vendor charges a reduced price to a connected purchaser who does not have a right to full deduction.
(6) The measure should be targeted so that it applies only in cases of VAT avoidance or evasion and only when a number of conditions have been met. The measure is therefore proportionate to the aim pursued.
(7) Similar derogations have been granted to other Member States in order to counter tax avoidance or evasion and have been found to be effective.
(8) Derogations pursuant to Article 27 of Directive 77/388/EEC which counter VAT avoidance linked to the taxable amount of supplies between related parties are included in the Commission proposal of 16 March 2005 for a Directive rationalising some of the derogations pursuant to that Article [2]. It is therefore necessary to bring the application period of this derogation to an end when that Directive enters into force.
(9) This derogation will safeguard the amount of VAT due at the final consumption stage and has no negative impact on the Communities' own resources accruing from VAT,
HAS ADOPTED THIS DECISION:
Article 1
By way of derogation from Article 11(A)(1)(a) and Article 28e of Directive 77/388/EEC, the Kingdom of Spain is hereby authorised to provide that the taxable amount of a supply of goods or services or of an intra-Community acquisition of goods shall be the same as the open-market value, as defined in Article 11(A)(1)(d) of the said Directive where the consideration is significantly lower than the open-market value and the recipient of the supply, or in the case of an intra-Community acquisition, the acquirer, does not have a right to full deduction under Article 17 of Directive 77/388/EEC.
This measure may only be used in order to prevent tax avoidance or evasion and when the consideration on which the taxable amount would otherwise be based has been influenced by family, management, ownership, financial or legal ties as defined in national legislation. For these purposes, legal ties shall include the formal relationship between employer and employee.
Article 2
The authorisation granted under Article 1 shall expire on the date of entry into force of a Directive rationalising the derogations pursuant to Article 27 of Directive 77/388/EEC which counter avoidance or evasion of VAT through the valuation of supplies between connected persons, or on 31 December 2009, whichever is the earlier.
Article 3
This Decision is addressed to the Kingdom of Spain.
Done at Brussels, 15 May 2006.
For the Council
The President
U. Plassnik
[1] OJ L 145, 13.6.1977, p. 1. Directive as last amended by Directive 2006/18/EC (OJ L 51, 22.2.2006, p. 12).
[2] OJ C 125, 24.5.2005, p. 12.
--------------------------------------------------
