COM documents other than legislative proposals adopted by the Commission
(2005/C 123/02)
Document | Part | Date | Title |
COM(2004) 500 | | 7.7.2004 | Communication from the Commission to the Council and to the European Parliament on Commission Decisions of 7 July 2004 concerning national allocation plans for the allocation of greenhouse gas emission allowances of Austria, Denmark, Germany, Ireland, the Netherlands, Slovenia, Sweden, and the United Kingdom in accordance with Directive 2003/87/EC |
COM(2004) 503 | | 15.7.2004 | Communication from the Commission to the Council and to the European Parliament: A More Efficient Common European Asylum System: The Single Procedure as the Next Step |
COM(2004) 541 | | 30.7.2004 | Communication from the Commission to the Council, the European Parliament, the European Economic and Social Committee and the Committee of the Regions on interoperability of digital interactive television services |
COM(2004) 552 | | 11.8.2004 | Communication from the Commission to the Council, the European Parliament, the European Economic and Social Committee and the Committee of the Regions on the implementation of an information and communication strategy on the euro and Economic and Monetary Union |
COM(2004) 681 | | 20.10.2004 | Communication from the Commission to the Council and to the European Parliament on Commission Decisions of 20 October 2004 concerning national allocation plans for the allocation of greenhouse gas emission allowances of Belgium, Estonia, Finland, France, Latvia, Luxembourg, Portugal, and the Slovak Republic in accordance with Directive 2003/87/EC |
COM(2004) 719 | | 26.10.2004 | Communication from the Commission to the European Parliament and the Council Report on the use of financial resources earmarked for the decommissioning of nuclear power plants |
COM(2004) 740 | | 26.10.2004 | Communication from the Commission Annual Report to the Discharge Authority on Internal Audits Carried out in 2003 |
COM(2004) 813 | | 14.12.2004 | Communication from the Commission to the Council: The situation of Germany and France in relation to their obligations under the excessive deficit procedure following the judgement of the Court of Justice |
COM(2004) 836 | | 12.1.2005 | Report from the Commission to the budgetary authority on guarantees covered by the general budget situation at 30 June 2004 |
COM(2005) 16 | | 27.1.2005 | Communication from the Commission: Report on the implementation of the Environmental Technologies Action Plan in 2004 |
COM(2005) 35 | | 9.2.2005 | Communication from the Commission to the Council, the European Parliament, the European Economic and Social Committee and the Committee of the Regions: Winning the Battle Against Global Climate Change |
COM(2005) 44 | | 14.2.2005 | Report from the Commission to the Council, the European Parliament, the European Economic and Social Committee and the Committee of the Regions on equality between women and men, 2005 |
COM(2005) 46 | | 16.2.2005 | Communication from the Commission to the European Parliament and the Council Strengthening passenger rights within the European Union |
COM(2005) 59 | | 25.2.2005 | Draft Interinstitutional Agreement on the operating framework for the European regulatory agencies |
COM(2005) 74 | | 9.3.2005 | Communication from the Commission to the Council on risk and crisis management in agriculture |
COM(2005) 77 | | 14.3.2005 | Communication from the Commission to the European Parliament and the Council — A Framework for Developing Relations with the Russian Federation in the Field of Air Transport |
COM(2005) 77 | | 16.3.2005 | Communication from the Commission to the Council and the European Parliament Better Regulation for Growth and Jobs in the European Union |
COM(2005) 102 | | 23.3.2005 | Report from the Commission to the Council and the European Parliament on the application of the Postal Directive (Directive 97/67/EC as amended by Directive 2002/39/EC) |
These texts are available on EUR-Lex: http://europa.eu.int/eur-lex/lex/
--------------------------------------------------
