Commission Decision
of 15 April 2003
amending Commission Decision 93/13/EEC in respect of the certificate of veterinary checks on products from third countries
(notified under document number C(2003) 1229)
(Text with EEA relevance)
(2003/279/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Communities,
Having regard to Council Directive 97/78/EC of 18 December 1997 laying down the principles governing the organisation of veterinary checks on products entering the Community from third countries(1) and in particular Articles 5(4), 7(6), 12(12) and 33 thereof,
Whereas:
(1) The original requirements for veterinary checks were set down in, or adopted on the basis of Council Directive 90/675/EEC(2), which has been repealed and replaced by Directive 97/78/EC.
(2) The model certificate of veterinary checks on products introduced into the Community from third countries, as referred to in Article 5(1) of Directive 97/78/EC, is set out in Annex B to Commission Decision 93/13/EEC of 22 December 1992 laying down the procedures for veterinary checks at Community border inspection posts on products from third countries(3), as last amended by Decision 96/32/EC(4). It should be amended to take account of changes to procedures for all consignments introduced into one of the territories listed in Annex I Directive 97/78/EC.
(3) Detailed rules in respect of the use of this certificate in case of transit by road, and concerning additional checks that must be carried out on products not meeting Community rules that transit across or move within the Community under customs control, are the subject of Commission Decisions 2000/208/EC(5) and 2000/571/EC(6) respectively.
(4) Pending the later revision of other rules laid down in Commission Decision 93/13/EEC, there is an urgent need to amend and update firstly the certificate in Annex B to that Decision.
(5) For the proper functioning of the system of veterinary checks in the internal market all the information pertaining to a product introduced into the Community should be included in a single simplified document with a uniform format in order to reduce as far as possible the problems associated with the use of different languages in different Member States.
(6) Decision 93/13/EEC should therefore be amended accordingly.
(7) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
Annex B to Decision 93/13/EEC is replaced by the text in the Annex to this Decision.
Article 2
This Decision shall apply from 1 September 2003.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 15 April 2003.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 24, 30.1.1998, p. 9.
(2) OJ L 373, 31.12.1990, p. 1.
(3) OJ L 9, 15.1.1993, p. 33.
(4) OJ L 9, 12.1.1996, p. 9.
(5) OJ L 64, 11.3.2000, p. 20.
(6) OJ L 240, 23.9.2000, p. 14.
ANNEX
>PIC FILE= "L_2003101EN.001701.TIF">
>PIC FILE= "L_2003101EN.001801.TIF">
NOTES FOR GUIDANCE FOR THE ANNEX B CERTIFICATE(1)
Part 1: This section is for completion by the declarant or person responsible for the load as defined in Council Directive 97/78/EC Article 2(2)(e). Notes are shown against the relevant box number
General:
Complete the certificate in capitals. Where there is an option to delete a box or it is not relevant, clearly deface or cross out the whole numbered box. To positively indicate any option, tick or mark the
>PIC FILE= "L_2003101EN.001901.TIF">
sign.
This certificate is to be completed for all consignments presented to a border inspection post, whether they are for consignments presented as meeting EU requirements and are for free circulation, consignments that will be subject to channelling, or those consignments not meeting EU conditions and destined for transhipment, transit, or their placing in free zones, free warehouses or customs warehouses or for ship suppliers (chandlers). Channelling refers to consignments accepted under the conditions laid down in Article 8 of Directive 97/78/EC but that remain under veterinary control until a specified final destination is reached, usually for further treatment.
Iso codes where indicated refer to the international standard two letter code for any country.
Box 1. Consignor/exporter: Indicate the commercial organisation despatching the consignment (in the third country).
Box 2. Border inspection post. If this information is not pre-printed on the document, please complete. The CVED reference number is the unique reference number given by the border inspection post issuing the certificate (repeated in box 25). The ANIMO unit number is unique to the border inspection post and is listed against its name on the list of approved border inspection posts published in the Official Journal.
Box 3. Consignee: Indicate the address of the person or commercial organisation given on the third country certificate.
Box 4. Person responsible for the load (also agent or declarant): This is the person defined in Article 2(2)(e) of the Directive 97/78/EC, who is in charge of the consignment when presented to the border inspection post and makes the necessary declarations to the competent authorities on behalf of the importer: give the name, address.
Box 5. Importer: The importer may be remote from the actual border inspection post: give the name, address. If the importer and agent are the same indicate "As box 2".
Box 6. Country of origin: This refers to where the final product was produced, manufactured or packaged.
Box 7. Country from where consigned: This refers to the country where the consignment was placed aboard the means of final transport for the journey to the EU.
Box 8. Include the delivery address in the EU. This applies both to conforming (Box 19) and to non conforming (Box 22) products.
Box 9. Give the estimated date that consignments are expected to arrive at the border inspection post.
Box 10. Veterinary Certificate/document: date of issue: The date that the certificate/document was signed by the official veterinarian or the competent authority. Number: give the unique official number of the certificate. For products from an approved or registered establishment or vessel, indicate the name and approval/registration number where appropriate. For embryos, ova or semen straws give an identity number of the approved collection team.
Box 11. Give full details of the means of arrival transport: for aircraft the flight number and airway bill number, for vessels the ship name and bill of lading number, for road vehicles the registration number plate with trailer number if appropriate, for railways the train identity and wagon number.
Box 12. Nature of the goods: Indicate the species of animal, the treatment undergone by the products and the number and type of packages that comprise the load eg 50 boxes of 25 kg or the number of containers. Tick the appropriate transport temperature.
Box 13. CN code: Give as a minimum the first four digits of the relevant Combined Nomenclature, CN code, established under Council Regulation (EEC) No 2658/87 as last amended. These codes are also listed in Commission Decision 2002/349/EC (and are equivalent to the HS headings). Where there is one certificate with one consignment having contents with more than one commodity code, the additional codes may be annotated onto the CVED as appropriate.
Box 14. Gross weight: Overall weight in kg. This is defined as the aggregate mass of the products with immediate containers and all their packaging, but excluding transport containers and other transport equipment.
Box 15. Net weight: Weight of actual product excluding packaging in kg. This is defined as the mass of the products themselves without immediate containers or any packaging. Use Units where a weight is inappropriate eg 100 semen straws of X ml. or three biological strains/embryos.
Box 16. Give all seal and container identification numbers where relevant.
Box 17. Transhipment. Use where a consignment is not to be imported at this border inspection post but is to travel onward in another vessel or aircraft either for importation into the EU at a second and subsequent border inspection post in the Community/EEA, or for a third country destination. Animo unit number - see Box 2.
Box 18. Transit: For consignments that do not conform to EU requirements and are destined for a third country by movement across the EU/relevant EEA state by road, rail or waterway transport.
Exit BIP: Name of the border inspection post where the products are to leave the EU. Animo unit number - see Box 2.
Box 19. Conforming products: All products that will be presented for free circulation in the internal market including those that are acceptable but will be subjected to a "channelling procedure" and those that after receiving veterinary clearance as acceptable for free circulation, may be stored under customs control, and receive customs clearance at a later stage, either at the customs office on which the border inspection post is geographically dependent, or at another location.
Non conforming products: Those products not meeting EU requirements and that are for free zones, free warehouses, customs warehouses, ship chandlers or ships, or transit to a third country.
Box 20. Reimport refers to consignments of EU origin that have been refused acceptance or entry to a third country, and are being returned to the establishment of origin in the EU.
Box 21. Internal market: This is for consignments that are being presented for distribution in the single market. Tick the category for which the consignment is being presented. This also applies to those consignments that after receiving veterinary clearance as acceptable for free circulation, may be stored under customs control, and receive customs clearance at a later stage, either at the customs office on which the border inspection post is geographically dependent, or at another location.
Box 22. Complete this box for all non EU conforming products where the consignment will be delivered to and stored under veterinary control in a free zone, a free warehouse, a customs warehouse or a ship supplier (chandler).
NB
boxes 18 and 22 refer to veterinary procedures only.
Box 23. Signature. This commits the signatory also to accepting back consignments in transit that are refused entry by a third country.
Part 2. This section is for the completion by the official veterinarian or designated official agent (as in Commission Decision 93/352/EEC) only
For boxes 38 to 41 use a colour other than black
Box 24. Previous CVED: If there has been a previous CVED issued, indicate the serial number of this certificate.
Box 25. This refers to the unique reference number given by the border inspection post issuing the certificate and is as in Box 2.
Box 26. Documentary check. To be completed for all consignments.
Box 27. Tick "seal check" where containers are not opened and the seal only is checked according to Article 4(4)(a)(i) of Directive 97/78/EC.
Box 28. Physical checks:
Reduced checks refers to the regime laid down in Commission Decision 94/360/EEC where the consignment has not been selected for a physical check but is considered checked satisfactorily with documentary and identity check only.
"Other" refers to: re-import procedure, channelled goods, transhipment, transit or Article 12 and 13 procedures. These destinations can be deduced from other boxes.
Box 29. Complete with the category of substance or pathogen for which an investigation procedure is undertaken. "Random" indicates sampling where the consignment is not detained pending a result, in which case the competent authority of destination must be notified by ANIMO message (see Article 8 of Directive 97/78/EC). "Suspicion" includes cases where the consignment has been detained pending a favourable result, or tested because of a previous notification from the Rapid Alert System for Food and Feed (RASFF), or tested because of a safeguard measure in operation.
Box 30. Complete where relevant for acceptability for transhipment. Use where a consignment is not to be imported at this border inspection post but is to travel onward in another vessel or aircraft either for importation into the EU at a second and subsequent border inspection post in the Community/EEA, or for a third country destination. See Article 9 of Directive 97/78/EC and Commission Decision 2000/25/EC. ANIMO unit number - see Box 2.
Box 31. Transit: Complete when it is acceptable to send consignments that do not conform to EU requirements to a third country across the EU/relevant EEA State by road, rail or waterway transport. This must be carried out under veterinary control in accordance with the requirements of Article 11 of Directive 97/78/EC and Commission Decision 2000/208/EC.
Box 32. This box is to be used for all consignments approved for free circulation within the single market. (It should also be used for consignments that meet EU requirements but for financial reasons are not being customs cleared immediately at the border inspection post, but are being stored under customs control in a customs warehouse or will be customs cleared later and/or at a geographically separate destination.)
Boxes 33 and 34. Are to be used where consignments cannot be accepted for release for free circulation under veterinary rules, but are considered higher risk and are to be sent under veterinary and customs control to one of the controlled destinations foreseen in the Directive 97/78/EC. Acceptance for free zones, free warehouses and customs warehouses can only be granted when requirements laid down in Article 12(4) of Directive 97/78/EC are fulfilled.
Box 33. For use where consignments are accepted but must be channelled to a specific destination laid down in Articles 8 or 15 of the Directive 97/78/EC.
Box 34. Use for all non EU conforming consignments destined to be moved to or stored in warehouses approved in accordance with Article 12.4 or to operators authorised under Article 13 of the Directive 97/78/EC.
Box 35. Indicate clearly when import is refused, the subsequent process to be carried out. Give the date for completion of the action proposed. The address of any transformation establishment should be entered in Box 37. After rejection or a decision for transformation, the date for further action should be also recorded in the "follow up action register."
Box 36. Reasons for refusal: for use as appropriate to add relevant information. Tick the appropriate box. Item 7 is for hygiene failure not covered by 8, 9, including temperature control irregularities, putrefaction, or dirty product.
Box 37. Give approval number and address (or ship name and port) for all destinations where further veterinary control of the consignment is required ie for Boxes 33: Channelling, 34: Warehouse procedure, 35: Transformation or destruction.
Box 38. Use this box when the original seal recorded on a consignment is destroyed on opening the container. A consolidated list of all seals that have been used for this purpose should be kept.
Box 39. Put here the official stamp of the border inspection post or competent authority.
Box 40. Signature of the veterinarian, or in case of ports handling fish only, of the designated official agent as laid down in Commission Decision 93/352/EC.
Box 41. This box to be used by the transit border inspection post of exit from the EU when consignments are sent in transit across the EU and are checked outwards as laid down in Commission Decision 2000/208/EC.
Box 42. For use by customs services to add relevant information (eg for the number of the customs T1 or T5 certificate) where consignments remain under customs control for a period. This information is normally added after signature by the veterinarian.
Box 43. For use when the original CVED certificate must remain at any one location and further "daughter" CVED certificates must be issued.
(1) Notes for guidance may be printed and distributed separately from the certificate itself.
