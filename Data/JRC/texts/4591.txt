Commission Regulation (EC) No 1313/2005
of 11 August 2005
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 12 August 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 11 August 2005.
For the Commission
J. M. Silva Rodríguez
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 1947/2002 (OJ L 299, 1.11.2002, p. 17).
--------------------------------------------------
ANNEX
to Commission Regulation of 11 August 2005 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code | Standard import value |
07020000 | 052 | 54,4 |
096 | 18,0 |
999 | 36,2 |
07070005 | 052 | 44,5 |
999 | 44,5 |
07099070 | 052 | 47,7 |
999 | 47,7 |
08055010 | 382 | 66,8 |
388 | 61,7 |
524 | 62,7 |
528 | 59,4 |
999 | 62,7 |
08061010 | 052 | 108,0 |
204 | 57,3 |
220 | 115,0 |
624 | 180,9 |
999 | 115,3 |
08081080 | 388 | 77,7 |
400 | 70,0 |
404 | 81,8 |
508 | 64,6 |
512 | 55,9 |
528 | 75,4 |
720 | 62,2 |
804 | 73,5 |
999 | 70,1 |
08082050 | 052 | 107,2 |
388 | 74,4 |
512 | 13,7 |
528 | 37,8 |
999 | 58,3 |
08093010, 08093090 | 052 | 106,2 |
999 | 106,2 |
08094005 | 508 | 43,6 |
624 | 63,4 |
999 | 53,5 |
--------------------------------------------------
