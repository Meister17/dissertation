COMMISSION DIRECTIVE 93/11/EEC of 15 March 1993 concerning the release of the N-nitrosamines and N-nitrosatable substances from elastomer or rubber teats and soothers
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Directive 89/109/EEC of 21 December 1988 on the approximation of the laws of the Member States relating to materials and articles intended to come into contact with foodstuffs (1), and in particular Article 3 thereof,
Whereas the Community measures envisaged by this Directive are not only necessary but also indispensable for the attainment of the objectives of the internal market; whereas these objectives cannot be achieved by Member States individually; whereas furthermore their attainment at Community level is already provided for by Directive 89/109/EEC;
Whereas it has been shown that teats and soothers, made of elastomer or rubber, may release N-nitrosamines and substances capable of being converted into N-nitrosamines (N-nitrosatable substances);
Whereas the Scientific Committee for Food has given the opinion that N-nitrosamines and N-nitrosatable substances may endanger human health owing to their toxicity and has therefore recommended that migration of these substances from the abovementioned articles be kept below the detection limit of an appropriate sensitive method;
Whereas Article 2 of Directive 89/109/EEC lays down that materials and articles, in their finished state, must not transfer their constituents to foodstuffs in quantities which could endanger human health;
Whereas, in order to achieve this objective, the suitable instrument for teats is a specific directive within the meaning of Article 3 of Directive 89/109/EEC;
Whereas the use of soothers may produce the same type of risk and therefore it is convenient to adopt the same provisions for these articles too;
Whereas it is necessary to act immediately and therefore this Directive is limited to establishing specific rules regarding the release of N-nitrosamines and N-nitrosatable substances from elastomer or rubber teats and soothers, postponing to a more general directive regarding elastomers and rubber the solution of other problems concerning teats and soothers;
Whereas this Directive establishes the basic rules and general criteria for determining the release of N-nitrosamines and N-nitrosatable substances and postpones the definition of a detailed method of analysis;
Whereas the outline method of analysis given in the Annexes is adopted as a temporary measure until more results are available on the performance of this method and possible alternative methods;
Whereas the Commission has undertaken to promote further research on methods of analysis, to review the proposed methodology and to consider establishing analytical tolerances in the light of that research;
Whereas the measures provided for in this Directive are in accordance with the opinion of the Standing Committee on Foodstuffs,
HAS ADOPTED THIS DIRECTIVE:
Article 1
This Directive is a specific directive within the meaning of Article 3 of Directive 89/109/EEC.
It concerns the release of N-nitrosamines and of substances capable of being converted into N-nitrosamines, hereinafter called 'N-nitrosatable substances`, from teats and soothers, made of elastomer or rubber.
Article 2
The teats and soothers referred to in Article 1 must not pass on to release-test liquid (saliva test solution) under the conditions specified in Annex I any N-nitrosamine and N-nitrosatable substance detectable by a validated method which complies with the criteria laid down in Annex II and which can detect the following quantities:
- 0,01 mg in total of N-nitrosamines released/kg (of the parts of teat or soother made of elastomer or rubber),
- 0,1 mg in total of N-nitrosatable substances/kg (of the parts of teat or soother made of elastomer or rubber).
Article 3
1. Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive as from 1 April 1994. They shall immediately inform the Commission thereof.
Member States shall:
- permit, as from 1 April 1994, the trade in and use of teats and soothers complying with this Directive,
- prohibit, as from 1 April 1995, the trade in and use of teats and soothers which do not comply with this Directive.
2. When Member States adopt the measures referred to in paragraph 1, these shall contain a reference to this Directive or shall be accompanied by such reference to this Directive or shall be accompanied by such reference at the time of their official publication. The procedure for such reference shall be adopted by Member States.
Article 4
This Directive is addressed to the Member States.
Done at Brussels, 15 March 1993.
For the Commission Martin BANGEMANN Member of the Commission
ANNEX I
BASIC RULES FOR DETERMINING THE RELEASE OF N-NITROSAMINES AND N-NITROSATABLE SUBSTANCES
1. Release-test liquid (saliva test solution) To obtain the release-test liquid, dissolve 4,2 g of sodium bicarbonate (NaHCO3), 0,5 g of sodium chloride (NaCl), 0,2 g of potassium carbonate (K2CO3) and 30,0 mg of sodium nitrite (NaNO2) in one litre of distilled water or water of equivalent quality. The solution must have a pH value of 9.
2. Test conditions Samples of material obtained from an appropriate number of teats or soothers are immersed in the test-release liquid for 24 hours at a temperature of 40 ± 2 °C.
ANNEX II
CRITERIA APPLICABLE TO THE METHOD FOR DETERMINING THE RELEASE OF N-NITROSAMINES AND N-NITROSATABLE SUBSTANCES
1. The release of N-nitrosamines is determined in one aliquot of each solution obtained according to Annex I. The N-nitrosamines are extracted from the aliquot with nitrosamine-free dichloromethane (DCM) and determined by gas chromatography.
2. The release of N-nitrosatable substances is determined in another aliquot of each solution obtained according to Annex I. The nitrosatable substances are converted into nitrosamines by acidification of the aliquot with hydrochloric acid. Subsequently the nitrosamines are extracted from the solution with DCM and determined by gas chromatography.
