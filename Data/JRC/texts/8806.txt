COUNCIL DECISION of 21 December 1988 on the conclusion of the Agreement in the form of an Exchange of Letters between the European Economic Community and the Kingdom of Morocco on the import into the Community of preserved fruit salads originating in Morocco (88/649/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 113 thereof,
Having regard to the recommendation from the Commission,
Whereas the Cooperation Agreement between the European Economic Community and the Kingdom of Morocco(1) was signed on 25 April 1976 and entered into force on 1 November 1978;
Whereas the Agreement in the form of an Exchange of Letters between the European Economic Community and the Kingdom of Morocco on the import into the Community of preserved fruit salads originating in Morocco should be approved,
HAS DECIDED AS FOLLOWS:
Article 1 The Agreement in the form of an Exchange of Letters between the European Economic Community and the Kingdom of Morocco on the import into the Community of preserved fruit salads originating in Morocco is hereby approved on behalf of the Community.
The text of the Agreement is attached to this Decision.
Article 2 The President of the Council is hereby authorized to designate the person empowered to sign the Agreement for the purpose of binding the Community.
Article 3 This Decision shall enter into force on the day following its publication in the Official Journal of the European Communities.
Done at Brussels, 21 December 1988.
For the CouncilThe PresidentV. PAPANDREOU (1)OJ No L 264, 27. 9. 1978, p. 2.
