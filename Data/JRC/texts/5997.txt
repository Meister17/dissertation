Commission Regulation (EC) No 1662/2005
of 11 October 2005
amending Annex I of Council Regulation (EC) No 953/2003 to avoid trade diversion into the European Union of certain key medicines
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 953/2003 of 26 May 2003 to avoid trade diversion into the European Union of certain key medicines [1], and in particular Article 4(4) and (8) thereof,
Whereas:
(1) The Commission has received modified applications under Article 4 of Regulation (EC) No 953/2003 with respect to Epivir 150 mg × 60 and Combivir 300/150 mg × 60.
(2) The Commission has determined that the applications received fulfil the requirements set out in Regulation (EC) No 953/2003 in accordance with the procedure laid down in Article 5(2) of that Regulation.
(3) The applicants have been informed of the Commission decision to accept their applications.
(4) It is therefore necessary to replace Annex I to Regulation (EC) No 953/2003,
HAS ADOPTED THIS REGULATION:
Article 1
Annex I to Regulation (EC) No 953/2003 is replaced by the Annex to this Regulation.
Article 2
This regulation enters into force on the day following its publication in the Official Journal of the European Union.
It shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 11 October 2005.
For the Commission
Peter Mandelson
Member of the Commission
[1] OJ L 135, 3.6.2003, p. 5. Regulation as amended by Commission Regulation (EC) No 1876/2004 (OJ L 326, 29.10.2004, p. 22).
--------------------------------------------------
ANNEX
Product | Manufacturer/exporter | Country of destination | Distinctive features | Date of approval | CN/TARIC code |
"TRIZIVIR 750 mg × 60 | GLAXO SMITH KLINE GSK House 980 Great West Road BRENTFORD, MIDDX TW8 9GS United Kingdom | Afghanistan Angola Armenia Azerbaijan Bangladesh Benin Bhutan Botswana Burkina Faso Burundi Cambodia Cameroon Cape Verde Central African Republic Chad Comoros Congo Côte d’Ivoire Djibouti DR Congo East Timor Equatorial Guinea Eritrea Ethiopia Gambia Ghana Guinea Guinea Bissau Haiti Honduras India Indonesia Kenya Kiribati Korea (Dem Rep) Kyrgyz Republic Lao People’s DR Lesotho Liberia Madagascar Malawi Maldives Mali Mauritania Moldova Mongolia Mozambique Myanmar Namibia Nepal Nicaragua Niger Nigeria Pakistan Rwanda Samoa Sao Tome and Principe Senegal Sierra Leone Solomon Islands Somalia South Africa Sudan Swaziland Tajikistan Tanzania Togo Tuvalu Uganda Vanuatu Yemen Zambia Zimbabwe | Distinctive access pack — tri-lingual text | 19.4.2004 | 30049019 |
EPIVIR 150 mg × 60 | GLAXO SMITH KLINE GSK House 980 Great West Road BRENTFORD, MIDDX TW8 9GS United Kingdom | Distinctive access pack — tri-lingual text — red tablets | | 30049019 |
RETROVIR 250 mg × 40 | GLAXO SMITH KLINE GSK House 980 Great West Road BRENTFORD, MIDDX TW8 9GS United Kingdom | General export pack (blue) not used in EU. French hospital pack — Francophone markets | 19.4.2004 | 30049019 |
RETROVIR 300 mg × 60 | GLAXO SMITH KLINE GSK House 980 Great West Road BRENTFORD, MIDDX TW8 9GS United Kingdom | General export pack (blue) not used in EU. French hospital pack — Francophone markets | 19.4.2004 | 30049019 |
RETROVIR 100 mg × 100 | GLAXO SMITH KLINE GSK House 980 Great West Road BRENTFORD, MIDDX TW8 9GS United Kingdom | General export pack (blue) not used in EU. French hospital pack — Francophone markets | 19.4.2004 | 30049019 |
COMBIVIR 300/150 mg × 60 | GLAXO SMITH KLINE GSK House 980 Great West Road BRENTFORD, MIDDX TW8 9GS United Kingdom | Distinctive access pack — tri-lingual text Bottle (rather than blister pack) "A22" embossed red tablets | | 30049019 |
EPIVIR ORAL SOLUTION 10 mg/ml 240 ml | GLAXO SMITH KLINE GSK House 980 Great West Road BRENTFORD, MIDDX TW8 9GS United Kingdom | Distinctive access pack — tri-lingual text | 19.4.2004 | 30049019 |
ZIAGEN 300 mg × 60 | GLAXO SMITH KLINE GSK House 980 Great West Road BRENTFORD, MIDDX TW8 9GS United Kingdom | General export pack — not used in EU. French hospital pack — Francophone countries | 20.9.2004 | 30049019 |
RETROVIR ORAL SOLUTION 10 mg/ml 200 ml | GLAXO SMITH KLINE GSK House 980 Great West Road BRENTFORD, MIDDX TW8 9GS United Kingdom | Distinctive access pack Tri-lingual text | 20.9.2004 | 30049019 |
[1] Only if applicable."
--------------------------------------------------
