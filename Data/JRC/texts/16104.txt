Council Regulation (EC) No 1623/2006
of 17 October 2006
repealing Regulation (EC) No 7/2005 adopting autonomous and transitional measures to open a Community tariff quota for certain agricultural products originating in Switzerland
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 133 thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) Following the enlargement of the European Union on 1 May 2004, the Community and Switzerland agreed to adapt the tariff concessions laid down in the Agreement between the European Community and the Swiss Confederation of 21 June 1999 on trade in agricultural products [1], (hereinafter referred to as the Agreement), which entered into force on 1 June 2002. In particular, they agreed to amend Annexes 1 and 2 to the Agreement, which listed the concessions, in order to widen an existing duty-free Community tariff quota to cover a new product, witloof chicory of CN code 07052100.
(2) Pending the formal amendment, the Community and Switzerland agreed to provide for the application of the adapted concessions, as from 1 May 2004, on an autonomous and transitional basis.
(3) To ensure that quota benefit for products of CN code 07052100 would be available from 1 May 2004, a new autonomous Community tariff quota limited to those products was provided for during a transitional period by Council Regulation (EC) No 7/2005 of 13 December 2004 adopting autonomous and transitional measures to open a Community tariff quota for certain agricultural products originating in Switzerland [2].
(4) Annex 2 to the Agreement, as adapted by Decision No 3/2005 of the Joint Committee for Agriculture set up by the Agreement between the European Community and the Swiss Confederation on trade in agricultural products of 19 December 2005 on the adaptation, following the enlargement of the European Union, of Annexes 1 and 2 [3], sets out tariff quotas expanded to cover the products of CN code 07052100.
(5) Annex 2 to the Agreement is implemented by Commission Regulation (EC) No 1630/2006 of 31 October 2006 amending Regulation (EC) No 933/2002 opening and providing for the management of tariff quotas for certain agricultural products originating in Switzerland [4] with effect from 1 September 2006.
(6) Regulation (EC) No 7/2005 should therefore be repealed with effect from the same date,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 7/2005 is hereby repealed.
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Union.
It shall apply from 1 September 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Luxembourg, 17 October 2006.
For the Council
The President
E. Tuomioja
[1] OJ L 114, 30.4.2002, p. 132.
[2] OJ L 4, 6.1.2005, p. 1.
[3] OJ L 346, 29.12.2005, p. 33.
[4] OJ L 302, 1.11.2006, p. 43.
--------------------------------------------------
