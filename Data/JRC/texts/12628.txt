Guidelines for the allocation of limited air traffic rights
(2006/C 177/06)
1. These guidelines are based on Regulation (EC) No 847/2004 of the European Parliament and of the Council of 29 April 2004 on the negotiation and implementation of air service agreements between Member States and third countries. They shall apply in cases where more than one air carrier wishes to use air traffic rights which are limited under a bilateral air traffic agreement concluded between Sweden and a third country, and where the extent of the rights does not allow all the interested parties to operate air traffic services in accordance with the agreement. They shall also apply when only one air carrier may be designated (selected) under the agreement, but when more than one have expressed interest in being designated.
2. Where the limited traffic rights apply to the Scandinavian countries as a whole, the examination must be carried out in accordance with these guidelines if the route in question begins or ends in Sweden. The relevant authorities in Denmark and Norway must be consulted in such cases, however.
3. For the purposes of these guidelines, an air carrier means an airline established in Sweden with an operating licence issued pursuant to Council Regulation (EEC) No 2407/92.
4. Information on air traffic rights under bilateral air traffic agreements concluded by Sweden and decisions in accordance with these guidelines concerning the traffic rights granted is provided by the Swedish Civil Aviation Authority and published on its website (www.luftfartsstyrelsen.se). Information is also available on forthcoming air traffic negotiations. The Civil Aviation Authority shall inform the relevant authorities in Denmark and Norway of applications which are to be examined in accordance with these guidelines.
5. Questions concerning the allocation of traffic rights in accordance with these guidelines shall be examined by the Civil Aviation Authority. Its decisions shall be published. Appeals against decisions by the Civil Aviation Authority may be submitted to the Government (Ministry of Industry, Employment and Communications).
6. An airline wishing to use limited traffic rights shall apply in writing to the Civil Aviation Authority for a permit. The application shall be written in Swedish or English and contain the following information:
a) a copy of the air carrier's operating licence, unless it was issued by a Swedish authority;
b) a description of the planned air traffic (e.g. the type of aircraft, its nationality and registration marks, flight times and routes, and the days when flights will be operated);
c) information on any wet or dry aircraft leasing planned in connection with the air traffic service;
d) information on any cooperation with other air carriers and on the nature and extent of any such cooperation;
e) the planned start date for the air traffic service;
f) the type of air traffic service (e.g. passenger or freight);
g) any connecting service;
h) the pricing policy for the route.
The Civil Aviation Administration may ask applicants to include additional information in their applications.
The Civil Aviation Administration shall inform the relevant authorities in Denmark and Norway of the applications received. Information about the applications received shall also be published on the Civil Aviation Authority's website.
7. Applications shall be examined in a transparent and non-discriminatory manner. The selection process may give priority to traffic which:
- provides maximum consumer benefits;
- promotes the most effective possible competition between Community airlines;
- provides services to satisfy all the usual transport requirements at the lowest reasonable price;
- promotes the sound development of the Community air transport industry, trade and tourism, and/or;
- meets public transport policy objectives, e.g. with regard to regional development.
8. The following criteria shall be given special consideration in the examination:
- the services offered;
- the frequency of flights;
- the type of aircraft and its configuration;
- direct or indirect traffic;
- the start date;
- the level of traffic during the year;
- the type of traffic (e.g. passenger or freight);
- the accessibility of the service to consumers (booking and purchasing tickets);
- connecting flights;
- pricing;
- effect on the environment.
The Civil Aviation Administration may also take other factors into account if these are announced to the applicants before a final decision is taken.
9. Consideration shall be given to the need for continuity of supply of air services when examining applications.
10. Decisions on the allocation of traffic rights under these guidelines shall state the reasons on which the decision is based.
11. Allocated traffic rights may not be transferred without special permission.
12. An air carrier which had been given permission to use traffic rights in accordance with these guidelines shall start the traffic service in question within the next two programming periods. Failure to do so shall result in the permit being cancelled.
13. A permit shall also be cancelled if
a) the traffic is interrupted and not resumed again within six months, unless the interruption is due to circumstances beyond the control of the permit holder;
b) the permit holder informs the Civil Aviation Authority that he no longer plans to use the permit.
14. The Civil Aviation Authority may withdraw a permit in full or in part if
a) the traffic is not operated in accordance with the terms of the permit or the provisions in the bilateral agreement upon which the traffic is based;
b) the permit holder otherwise fails to observe provisions on air traffic, or;
c) there are other special reasons for doing so.
15. When a permit is cancelled or withdrawn in accordance with these guidelines, the Civil Aviation Administration may decide to reconsider the matter.
16. These guidelines shall not affect traffic rights which are being used effectively and in accordance with Community law and national competition legislation.
--------------------------------------------------
