Commission Regulation (EC) No 749/2000
of 11 April 2000
adapting the total quantities referred to in Article 3 of Council Regulation (EEC) No 3950/92 establishing an additional levy in the milk and milk products sector
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 3950/92 of 28 December 1992 establishing an additional levy in the milk and milk products sector(1), as last amended by Regulation (EC) No 1256/1999(2), and in particular Articles 3(2) and 4(2) thereof,
Whereas:
(1) Article 3(2) of Regulation (EEC) No 3950/92 lays down that the guaranteed total quantities for Finland may be increased to compensate "SLOM" producers, up to a maximum of 200000 tonnes. In accordance with Article 6 of Commission Regulation (EC) No 671/95(3), as amended by Regulation (EC) No 1390/95(4), Finland has notified the quantities concerned for the 1999/2000 marketing year.
(2) Article 4(2) of Regulation (EEC) No 3950/92 lays down that the individual reference quantities are increased or established at the duly justified request of producers to take account of changes affecting their deliveries and/or direct sales and that the increase or establishment of such a reference quantity is subject to a corresponding reduction or cancellation of the other reference quantity the producer owns.
(3) Such adjustments may not lead to an increase, for the Member State concerned, in the sum of the deliveries and direct sales referred to in Article 3 of Regulation (EEC) No 3950/92. Where the individual reference quantities undergo a definitive change, the quantities referred to in Article 3 are adjusted accordingly.
(4) In accordance with the third indent of Article 8 of Commission Regulation (EEC) No 536/93(5), as last amended by Regulation (EC) No 1255/98(6), Belgium, Denmark, Germany, Spain, France, Ireland, Italy, the Netherlands, Austria, Portugal, Finland and the United Kingdom have notified quantities which have undergone a definitive change in accordance with the second subparagraph of Article 4(2) of Regulation (EEC) No 3950/92.
(5) The total quantities applicable for the period from 1 April 1999 to 31 March 2000 laid down in the table given in Article 3(2) of Regulation (EEC) No 3950/92 and consequently those applicable for subsequent periods laid down in the Annex to that Regulation should therefore be adjusted.
(6) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Milk and Milk Products,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EEC) No 3950/92 is amended as follows:
1. The table in Article 3(2) is replaced by that given in Annex I hereto.
2. The Annex is replaced by Annex II hereto.
Article 2
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 11 April 2000.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 405, 31.12.1992, p. 1.
(2) OJ L 160, 26.6.1999, p. 73.
(3) OJ L 70, 30.3.1995, p. 2.
(4) OJ L 135, 21.6.1995, p. 4.
(5) OJ L 57, 10.3.1993, p. 12.
(6) OJ L 173, 18.6.1998, p. 14.
ANNEX I
"Total reference quantities as applicable from 1 April 1999 to 31 March 2000
>TABLE>"
ANNEX II
"ANNEX
a) Total reference quantities referred to in Article 3(2) as applicable from 1 April 2000 to 31 March 2001
>TABLE>
b) Total reference quantities referred to in Article 3(2) as applicable from 1 April 2001 to 31 March 2002
>TABLE>
c) Total reference quantities referred to in Article 3(2) as applicable from 1 April 2002 to 31 March 2005
>TABLE>
d) Total reference quantities referred to in Article 3(2) as applicable from 1 April 2005 to 31 March 2006
>TABLE>
e) Total reference quantities referred to in Article 3(2) as applicable from 1 April 2006 to 31 March 2007
>TABLE>
f) Total reference quantities referred to in Article 3(2) as applicable from 1 April 2007 to 31 March 2008
>TABLE>"
