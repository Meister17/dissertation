COMMISSION DECISION of 5 February 1993 on animal health conditions and veterinary certification for imports of equidae for slaughter
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Directive 90/426/EEC of 26 June 1990 on animal health conditions governing the movement and import from third countries of equidae(1) , as last amended by Directive 92/361/EEC(2) , and in particular Article 15 point (a),
Articles 16 and 18 thereof;
Having regard to Council Directive 91/496/EEC of 15 July 1991 laying down the principles governing the organization of veterinary checks on animals entering the Community from third countries and amending Directives 89/662/EEC, 90/425/EEC and 90/675/EEC(3) , as last amended by Decision 92/438/EEC(4) , and in particular Article 4 thereof,
Whereas by Council Decision 79/542/EEC(5) , as last amended by Commission Decision 93/100/EEC(6) , the list of third countries from which the Member States authorize imports of equidae in particular has been established;
Whereas it is also necessary to take into account the regionalization of certain third countries appearing on the abovementioned list, which is the subject of Commission Decision 92/160/EEC(7) , as amended by Decision 92/161/EEC(8) ;
Whereas the responsible national veterinary authorities have undertaken to notify the Commission and the Member States, by telegram, telex or telefax, within 24 hours of the confirmation of the occurrence of any infectious or contagious disease in equidae of lists A and B of the International Office of Epizootic Diseases or of the adoption of vaccination against any of them or, within an appropriate period, of any changes in the national import rules concerning equidae;
Whereas the conditions to be established for imports of equidae for slaughter apply without prejudice to the requirements of Council Directive 86/469/EEC(9) that no thyreostatic, estrogenic, androgenic or gestagenic substances are used for fattening purposes in equidae;
Whereas the certification for a consignment of equidae for slaughter has been provided for, provided the animals are properly marked and identified; whereas therefore it is necessary to establish a clear and indelible mark for equidae for slaughter;
Whereas the different categories of equidae have their own features and their imports are authorized for different purposes; whereas, consequently different health requirements must be established for equidae for slaughter being sent directly to the slaughterhouse of destination and, for equidae for slaughter, passing through a market or a marshalling centre;
Whereas the measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
Without prejudice to Decision 92/160/EEC, Member States shall authorize imports of equidae for slaughter from a third country appearing in Part I of the Annex to Decision 79/542/EEC and which are clearly and indelibly marked by a hot-branded 'S' of not less than 3 cm size on the hoof of the left front leg and
(i) which, if sent directly to a slaughterhouse to be slaughtered within five days after arrival at the slaughterhouse and not more than five days after arrival in the Community, meet the requirements of Annex I of the present Decision. However, where equidae are subjected to a sea-voyage of more than eight days, Member States may decide that such equidae may be slaughtered within 21 days of arrival at the slaughterhouse, provided they remain at the slaughterhouse under the daily supervision of the official veterinarian. Member States shall notify the Commission of such cases; or
(ii) which, if they are passing through a market or a marshalling centre before being slaughtered, meet the requirements of Annex II.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 5 February 1993.
For the CommissionRené STEICHENMember of the Commission
(1) OJ No L 224, 18. 8. 1990, p. 42.
(2) OJ No L 157, 10. 6. 1992, p. 28.
(3) OJ No L 268, 24. 9. 1991, p. 56.
(4) OJ No L 243, 25. 8. 1992, p. 27.
(5) OJ No L 146, 14. 6. 1979, p. 15.
(6) OJ No L 40, 17. 2. 1993, p. 23.
(7) OJ No L 71, 18. 3. 1992, p. 27.
(8) OJ No L 71, 18. 3. 1992, p. 29.
(9) OJ No L 275, 26. 9. 1986, p. 36.
ANNEX I
HEALTH CERTIFICATE for imports of equidae for slaughter sent directly to a slaughterhouse within the European Community
No of certificate: .
Third country of dispatch(1) :
.
Ministry responsible: .
Reference to accompanying welfare certificate: .
Number of animals: .
(in words)
I. Identification of the animal(s)
/* Tables: see OJ */
The animal(s) is/are to be sent from: .
(Place of export)
directly to: .
(Member State and slaughterhouse of destination)
by railway wagon/lorry/aircraft/ship(2) .
(indicate means of transport and registration marks, flight number or registered name, as appropriate)
Name and address of consignor: .
.
Name and address of consignee: .
.
III. Health information
I, the undersigned, certify that the animal(s) described above meet(s) the following requirements:
(a) it/they come(s) from a country where the following diseases are compulsorily notifiable: African horse sickness, dourine, glanders, equine encephalomyelitis (of all types including VEE), infectious anaemia, vesicular stomatitis, rabies, anthrax;
(b) it/they has/have been examined today and show(s) no clinical sign of disease(3) ;
(c) it/they is/are not intended for slaughter under a national programme of infectious or contagious disease eradication;
(d) during the 90 days immediately preceding the exportation (or since birth if less than 90 days old) it/they has/have been resident on holdings under veterinary supervision in the country of dispatch and 30 days prior to dispatch it/they has/have been isolated from equidae not of equivalent health status;
(e) it/they come(s) from the territory or in cases of official regionalization according to Community legislation from a part of the territory of a third country in which:
(i) Venezuelan equine encephalomyelitis has not occurred during the last two years;
(ii) dourine has not occurred during the last six months;
(iii) glanders has not occurred during the last six months;
(iv) - either vesicular stomatitis has not occurred during the last six months(4) ;
or
- it/they was/were tested on a sample of blood taken within 10 days of export on ........................................(5) by a virus neutralization test for vesicular stomatitis with negative result(s) at a dilution of 1 in 12(6) ;
(v) - in the case of (an) uncastrated male equine animal(s); either equine viral arteritis (EVA) has not been officially recorded during the last six months(7) ;
or
- it/they was/were tested on samples of blood taken within 10 days of export on .........................................(8) by a virus neutralization test for EVA, either with negative result(s) at a delution of 1 in 4(9) ;
or
- the semen of the animal(s) taken within 21 days of export on
.........................................(10) was tested by a virus isolation test for EVA with negative result(11) ;
(f) it/they do(es) not come from the territory or from a part of the territory of a third country considered, in accordance with Community legislation, as infected with African horse sickness and
- either it/they was/were not vaccinated against African horse sickness(12) ,
or
- it/they was/were vaccinated against African horse sickness on ..........................................(13) (14) ;
(g) it/they do(es) not come from a holding which was subject to prohibition for animal health reasons nor had contact with equidae from a holding which was subject to prohibition for animal health reasons:
(i) during six months in the case of equine encephalomyelitis, beginning on the date on which the equidae suffering from the disease are slaughtered;
(ii) in the case of infectious anaemia, until the date on which the infected animals having been slaughtered, the remaining animals have shown a negative reaction to two Coggins tests carried out three months apart;
(iii) during six months in the case of vesicular stomatitis;
(iv) during one month from the last recorded case, in the case of rabies;
(v) during 15 days from the last recorded case, in the case of anthrax;
If all animals of species susceptible to the disease located on the holding have been slaughtered and the premises disinfected, the period of prohibition shall be 30 days, beginning on the day on which the animals were destroyed and the premises disinfected, except in the case of anthrax, where the period of prohibition is 15 days;
(h) to the best of my knowledge, it/they has/have not been in contact with equidae suffering from an infectious or contagious disease in the 15 days prior to this declaration;
(i) to the best of my knowledge, it/they has/have received no thyreostatic, estrogenic, androgenic or gestagenic substances for fatting purposes;
(j) it/they was/were subjected to the following tests carried out with negative results on samples of blood taken within ten days of export on .........................................(15) :
- a Coggins test of infectious anaemia,
- a complement fixation test for glanders at a dilution of 1 in 10(16) ;
IV. The animal(s) will be sent in a vehicle cleansed and disinfected in advance with a disinfectant officially recognized in the country of dispatch and designed in a way that droppings, litter or fodder cannot escape during transportation.
The following declaration signed by the owner or representative is part of the certificate.
V. The certificate is valid for 10 days. In the case of transport by ship the time is prolonged by the time of the voyage.
/* Tables: see OJ */
I, the undersigned, . (insert name in block letters)
(owner or representative(17) of the animal(s) described above) declare:
1. the animal(s) will be sent directly from the premises of dispatch to the premises of destination without coming into contact with other equidae not accompanied by an equivalent certificate.
The transportation will be effected in such a way that health and wellbeing of the animal(s) can be protected effectively;
2. the animal(s) has/have either remained in .............................................. (exporting country) since birth or entered the exporting country at least 90 days prior to this declaration(18) .
.
.
(Place, date)
(Signature)
(1) Part of territory in accordance with Article 13 (2) of Council Directive 90/426/EEC.
(2) The certificate must be issued on the day of loading of the animal(s) for dispatch to the Member State of destination. It must accompany the consignment and covers only animals transported in the same railway wagon, lorry, aircraft or ship and taken directly to a slaughterhouse.
(3) Delete as appropriate.
(4) Insert date.
(5) The required test for glanders does not apply for the countries: Austria, Finland, Greenland, Iceland, Norway, Sweden, Switzerland, Australia, New Zealand, Canada and the United States of America.
ANNEX II
for imports of equidae for slaughter passing through a market or marshalling centre within the European Community No of certificate .
Third country of dispatch(1) : .
Ministry responsible: .
Reference to acccompanying welfare certificate: .
Number of animals: .
.
(In words)
I. Identification of the animals
/* Tables: see OJ */
The animal(s) is/are to be sent from: .
(Place of export)
directly to: .
(Member State and place of destination)
by railway wagon/lorry/aircraft/ship(2) : .
(indicate means of transport and registration marks, flight number or registered name, as appropriate)
Name and address of consignor: .
.
Name and address of consignee: .
.
III. Health information
I, the undersigned, certify that the animal(s) described above meets the following requirements:
(a) it/they come(s) from a country where the following diseases are compulsorily notifiable; African horse sickness, dourine, glanders, equine encephalomyelitis (of all types including VEE), infectious anaemia, vesicular stomatitis, rabies, anthrax;
(b) it/they has/have been examined today and show(s) no clinical sign of disease(3) ;
(c) it/they is/are not intended for slaughter under a national programme of infectious or contagious disease eradication;
(d) during the last three months immediately preceding the exportation (or since birth if less than three months old) it/they has/have been resident on holdings under veterinary supervision in the country of dispatch and
- either it/they come(s) from a country(4) listed in group A, B, C or D below(5) and has/have been isolated from equidae not of the same health status during 30 days prior to dispatch(6) ,
or
- it/they come(s) from a country(7) listed in Group E below(8) and has/have been in an approved isolation centre, protected from vector insects during 40 days prior to dispatch(9) ;
(e) it/they come(s) from the territory or in cases of official regionalization according to Community legislation from a part of the territory of a third country in which:
(i) Venezuelan equine encephalomyelitis has not occurred during the last two years;
(ii) dourine has not occurred during the last six months;
(iii) glanders has not occurred during the last six months;
(iv) - either vesicular stomatitis has not occurred during the last six months(10) ;
or
- it/they was/were tested on samples of blood taken within 10 days of export on ..........(11) , by a virus neutralization test for vesicular stomatitis with negative result(s) at a dilution of 1 in 12(12) ;
(v) - in the case of (an) uncastrated male equine animal(s), either equine viral arthritis (EVA) has not been officially recorded during the last six months(13) ;
or
- it/they was/were tested on samples of blood taken within 10 days of export on ..........(14) by a virus neutralization test for EVA, either with negative result(s) at a dilution of 1 in 4(15) ;
or
- the semen of the animal(s) taken within 21 days of export on ..........(16) , was tested by a virus isolation test for EVA with negative result(17) ;
(f) it/they do(es) not come from the territory or from a part of the territory of a third country considered, in accordance with Community legislation, as infected with African horse sickness
- either it/they was/were not vaccinated against African horse sickness(18)
or
- it/they was/were vaccinated against African horse sickness on ..........(19) (20) ;
(g) it/they do(es) not come from a holding which was subject to prohibition for animal health reasons nor had contact with equidae from a holding which was subject to prohibition for animal health reasons:
(i) during six months in the case of equine encephalomyelitis, beginning on the date on which the equidae suffering from the disease are slaughtered;
(ii) in the case of infectious anaemia, until the date on which, the infected animals having been slaughtered, the remaining animals have shown a negative reaction to two Coggins tests carried out three months apart;
(iii) during six months in the case of vesicular stomatitis;
(iv) during one month from the last recorded case, in the case of rabies;
(v) during 15 days from the last recorded case, in the case of anthrax.
If all the animals of species susceptible to the disease located on the holding have been slaughtered and the premises disinfected, the period of prohibition shall be 30 days, beginning on the day on which the animals were destroyed and the premises disinfected, except in the case of anthrax, where the period of prohibition is 15 days;
(h) to the best of my knowledge, it/they has/have not been in contact with equidae suffering from an infectious or contagious disease in the 15 days prior to this declaration;
(i) to the best of my knowledge, it/they has/have received no thyreostatic, estrogenic, androgenic or gestagenic substances for fattening purposes;
(j) it/they was/were subjected to the following tests carried out with negative results on samples of blood taken within 10 days of export on ..........(21) :
- a Coggins test for infectious anaemia,
- a complement fixation test for glanders at a dilution of 1 in 10,(22)
- a complement fixation test for dourine at a dilution of 1 in 10,(23)
- a complement fixation test for piroplasmosis (Babesia equi and Babesia caballi)(24) (25) at a dilution of 1 in 5;
(k) it/they was/were subjected to a test for African horse sickness as described in Annex D to Council Directive 90/426/EEC on two occasions, carried out on samples of blood taken with an interval of between 21 and 30 days, on ..........(26) and on ..........(27) , the second of which must have been taken within ten days of export(28) either with negative
- reactions if it/they has/have not been vaccinated(29) ,
or
- without increase in antibody count if it/they has/have been vaccinated(30) ;
(l) either it/they was/were not vaccinated against Venezulelan equine encephalomyelitis(31) (32)
or
it/they was/were vaccinated on ..........(33) , this being at least six months prior to pre-export isolation(34) ;
(m) either it/they was/were vaccinated against western and eastern equine encephalomyelitis with inactivated vaccine on ..........(35) , this being within six months and at least 30 days of export(36) (37) (38) ;
or
it/they was/were subjected to haemagglutination inhibition tests for western and eastern equine encephalomyelitis on two occasions, carried out on blood samples taken with an interval of 21 days on ..........(39) and on ..........(40) , the second of which must have been taken within 10 days of export either with negative reactions, if it/they has/have not been vaccinated(41) or without increase in antibody count, if it/they has/have been vaccinated more than six months ago(42) .
IV. The animal(s) will be sent in a vehicle cleansed and disinfected in advance with a disinfectant officially recognized in the country of dispatch and designed in a way droppings, litter or fodder cannot escape during transportation.
The following declaration signed by the owner or representative is part of the certificate.
V. The certificate is valid for 10 days. In the case of transport by ship the time is prolonged by the time of the voyage.
/* Tables: see OJ */
I, the undersigned, . (insert name in block letters)
(owner or representative(43) of the animal(s) described above) declare:
1. the animal(s) will be sent directly from the premises of dispatch to the premises of destination without coming into contact with other equidae not of the same health status.
The transportation will be effected in such a way that health and well-being of the animal(s) can be protected effectively;
2. the animal(s) has/have either remained in .......... (exporting country) since birth or entered the exporting country at least 90 days prior to this declaration.
.
.
(Place, date)
(Signature)
(1) Part of territory in accordance with Article 13 (2) of Council Directive 90/426/EEC.
(2) This certificate must be issued on the day of loading of the animal(s) for dispatch to the Member State of destination. It must accompany the consignment and covers only animals transported in the same railway wagon, lorry, aircraft or ship.
(3) Group A: Austria, Finland, Greenland, Iceland, Norway, Sweden, Switzerland; Group B: Australia, Belarus, Bulgaria, Croatia, Cyprus, the Czech Republic, Estonia, Hungary, Latvia, Lithuania, the ex-Yugoslav Republic of Macedonia, Montenegro, New Zealand, Poland, Romania, Russia (1), Serbia, the Slovak Republic, Slovenia, Ukraine; Group C: Canada, United States of America; Group D: Argentina, Brazil (1), Chile, Cuba, Mexico, Paraquay, Uruguay; Group E: Algeria, Israel, Malta, Mauritius, Tunisia.
(4) Delete as appropriate.
(5) Insert date.
(6) The required test for glanders and dourine don't apply for countries listed in Groups A and C, Australia und New Zealand.
(7) Applies only to countries listed in Group E above.
(8) Applies only to countries listed in Group D above.
(9) Applies only to countries listed in Group C above.
