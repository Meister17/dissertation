Prior notification of a concentration
(Case COMP/M.4434 — Ricoh/Danka)
(2006/C 275/08)
(Text with EEA relevance)
1. On 3 November 2006, the Commission received a notification of a proposed concentration pursuant to Article 4 of Council Regulation (EC) No 139/2004 [1] by which the undertaking Ricoh Europe B.V. ("Ricoh", the Netherlands), a subsidiary of Ricoh Company Ltd, Japan, acquires, within the meaning of Article 3(1)(b) of the Council Regulation, control of the undertakings Danka Austria GmbH (Austria), Danka Belgium NV/SA (Belgium), Danka Danmark A/S (Denmark), Danka Deutschland Holding GmbH (Germany), Danka Office Products B.V. (the Netherlands), Danka Netherlands B.V. (the Netherlands), Danka Holdings B.V. (the Netherlands), Danka Holding France Sarl (France), Danka Holdings Iberia SA (Spain), Danka Holdings S.A. (Switzerland), Danka Sverige AB (Sweden), Danka Italia SpA (Italy), Danka Norge AS (Norway) and Danka UK plc (United Kingdom) (together "Danka") by way of purchase of shares.
2. The business activities of the undertakings concerned are:
- for Ricoh: production and distribution of office automation products and related services,
- for Danka: distribution of office automation products and related services.
3. On preliminary examination, the Commission finds that the notified transaction could fall within the scope of Regulation (EC) No 139/2004. However, the final decision on this point is reserved.
4. The Commission invites interested third parties to submit their possible observations on the proposed operation to the Commission.
Observations must reach the Commission not later than 10 days following the date of this publication. Observations can be sent to the Commission by fax (fax No (32-2) 296 43 01 or 296 72 44) or by post, under reference number Case COMP/M.4434 — Ricoh/Danka, to the following address:
European Commission
Directorate-General for Competition
Merger Registry
J-70
B-1049 Bruxelles/Brussel
[1] OJ L 24, 29.1.2004, p. 1.
--------------------------------------------------
