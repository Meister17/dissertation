Action brought on 9 February 2006 by the Commission of the European Communities against the Grand-Duchy of Luxembourg
An action against the Grand-Duchy of Luxembourg was brought before the Court of Justice of the European Communities on 9 February 2006 by the Commission of the European Communities, represented by J. Hottiaux and F. Simonetti, acting as Agents, with an address for service in Luxembourg.
The Commission claims that the Court should:
1. declare that, by not adopting the laws, regulations and administrative provisions necessary to comply with Directive 2001/42/EC of the European Parliament and of the Council of 27 June 2001 on the assessment of the effects of certain plans and programmes on the environment [1] and, in any event, by not communicating those provisions to the Commission, the Grand Duchy of Luxembourg has failed to fulfil its obligations under that directive;
2. order the Grand-Duchy of Luxembourg to pay the costs.
Pleas in law and main arguments
The time-limit for the implementation of Directive 2001/42/EC expired on 21 July 2004.
[1] OJ L 197 of 21.7.2001, p. 30.
--------------------------------------------------
