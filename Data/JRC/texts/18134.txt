Commission Regulation (EC) No 1897/2004
of 29 October 2004
supplementing the Annex to Regulation (EC) No 2400/96 as regards the entry of a name in the "Register of protected designations of origin and protected geographical indications" (Cartoceto) (PDO)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2081/92 of 14 July 1992 on the protection of geographical indications and designations of origin for agricultural products and foodstuffs [1], and in particular Article 6(3) and (4) thereof,
Whereas:
(1) In accordance with Article 6(2) of Regulation (EEC) No 2081/92, the application submitted by Italy for registration of the name "Cartoceto" was published in the Official Journal of the European Union [2].
(2) Since no statement of objection within the meaning of Article 7 of Regulation (EEC) No 2081/92 has been sent to the Commission, the name should be entered in the "Register of protected designations of origin and protected geographical indications",
HAS ADOPTED THIS REGULATION:
Article 1
The name listed in the Annex to this Regulation is hereby added to the Annex to Regulation (EC) No 2400/96.
Article 2
This Regulation shall enter into force on the twentieth day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 29 October 2004.
For the Commission
Franz Fischler
Member of the Commission
--------------------------------------------------
[1] OJ L 208, 24.7.1992, p.1. Regulation as last amended by Regulation (EC) No 806/2003 (OJ L 232, 1.7.2004, p. 21).
[2] OJ C 41, 17.2.2004, p. 2 (Cartoceto).
--------------------------------------------------
+++++ ANNEX 1 +++++
