Euro exchange rates [1]
4 July 2006
(2006/C 156/01)
| Currency | Exchange rate |
USD | US dollar | 1,2791 |
JPY | Japanese yen | 146,57 |
DKK | Danish krone | 7,4605 |
GBP | Pound sterling | 0,69285 |
SEK | Swedish krona | 9,1883 |
CHF | Swiss franc | 1,5677 |
ISK | Iceland króna | 96,15 |
NOK | Norwegian krone | 7,9700 |
BGN | Bulgarian lev | 1,9558 |
CYP | Cyprus pound | 0,5750 |
CZK | Czech koruna | 28,443 |
EEK | Estonian kroon | 15,6466 |
HUF | Hungarian forint | 279,96 |
LTL | Lithuanian litas | 3,4528 |
LVL | Latvian lats | 0,6960 |
MTL | Maltese lira | 0,4293 |
PLN | Polish zloty | 4,0138 |
RON | Romanian leu | 3,5586 |
SIT | Slovenian tolar | 239,64 |
SKK | Slovak koruna | 38,385 |
TRY | Turkish lira | 1,9740 |
AUD | Australian dollar | 1,7150 |
CAD | Canadian dollar | 1,4205 |
HKD | Hong Kong dollar | 9,9360 |
NZD | New Zealand dollar | 2,0951 |
SGD | Singapore dollar | 2,0197 |
KRW | South Korean won | 1206,70 |
ZAR | South African rand | 8,9853 |
CNY | Chinese yuan renminbi | 10,2245 |
HRK | Croatian kuna | 7,2490 |
IDR | Indonesian rupiah | 11593,76 |
MYR | Malaysian ringgit | 4,658 |
PHP | Philippine peso | 67,357 |
RUB | Russian rouble | 34,3500 |
THB | Thai baht | 48,454 |
[1] Source: reference exchange rate published by the ECB.
--------------------------------------------------
