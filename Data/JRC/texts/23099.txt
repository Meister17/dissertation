COUNCIL DECISION of 5 October 1998 relating to the conclusion of a Protocol for the adaptation of the trade aspects of the Europe Agreement between the European Communities and their Member States, of the one part, and Romania, of the other part, to take into account the accession of the Republic of Austria, the Republic of Finland and the Kingdom of Sweden to the European Union and the results of the agricultural negotiations of the Uruguay Round, including the improvements to the existing preferential regime (98/626/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 113, in conjunction with the first sentence of Article 228(2) thereof,
Having regard to the proposal from the Commission,
Whereas the Protocol for the adaptation of the trade aspects of the Europe Agreement concluded between the European Communities and their Member States, of the one part, and Romania, of the other part, hereinafter referred to as 'the Protocol`, should be approved with a view to taking account of the accession of the Republic of Austria, the Republic of Finland and the Kingdom of Sweden to the European Union and the results of the agricultural negotiations of the Uruguay Round, including improvements to the existing preferential regime;
Whereas the Commission should be authorised to take measures to implement the Protocol, notably with regard to basic and processed agricultural products;
Whereas Regulations (EC) No 1595/97 (1) and (EC) No 656/98 (2) brought forward the implementation of certain provisions of the Protocol relating to basic and processed agricultural products respectively; whereas, therefore, appropriate measures should be taken to ensure a smooth transition between the preferential arrangements applied under those Regulations and those provided for by the Protocol;
Whereas the Additional Protocol to the Europe Agreement on trade in textile products between the European Community and Romania was last amended by Decision 96/224/EC (3),
HAS DECIDED AS FOLLOWS:
Article 1
The Protocol for the adaptation of the trade aspects of the Europe Agreement between the European Communities and their Member States, of the one part, and Romania, of the other part, to take into account the accession of the Republic of Austria, the Republic of Finland and the Kingdom of Sweden to the European Union and the results of the agricultural negotiations of the Uruguay Round, including improvements to the existing preferential regime, hereinafter referred to as 'the Protocol`, is hereby approved on behalf of the Community.
The text of the Protocol is attached to this Decision.
Article 2
1. Detailed rules for the application of this Decision shall be adopted by the Commission according to the procedure provided for in Article 23 of Regulation (EEC) No 1766/92 (4) or, as appropriate, the relevant provisions of other Regulations on the common organisation of the market, Regulation (EC) No 3448/93 (5) or Regulation (EC) No 3066/95 (6).
2. Upon this Decision taking effect, any rules adopted by the Commission pursuant to Article 8 of Regulation (EC) No 3066/95 and to Regulation (EC) No 656/98, implementing the concessions covered by the Protocol, shall be deemed to be governed by paragraph 1.
Article 3
The President of the Council shall, on behalf of the Community, give the notification provided for in Article 6 of the Protocol.
Done at Luxembourg, 5 October 1998.
For the Council
The President
W. SCHÜSSEL
(1) OJ L 216, 8.8.1997, p. 1.
(2) OJ L 90, 25.3.1998, p. 1.
(3) OJ L 81, 30.3.1996, p. 264.
(4) OJ L 181, 1.7.1992, p. 21. Regulation as last amended by Regulation (EC) No 923/96 (OJ L 126, 24.5.1996, p. 37).
(5) OJ L 318, 20.12.1993, p. 18.
(6) OJ L 328, 30.12.1995, p. 31. Regulation as last amended by Regulation (EC) No 1595/97.
