COMMISSION REGULATION (EC) No 2619/98 of 4 December 1998 amending Regulation (EC) No 2042/98 on special conditions for the granting of private storage aid for pigmeat
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2759/75 of 29 October 1975 on the common organisation of the market in pigmeat (1), as last amended by Regulation (EC) No 3290/94 (2), and in particular Articles 4(6) and 5(4) thereof,
Whereas Commission Regulation (EC) No 2042/98 (3) obliges the operators to export the products subject of a storage contract after the end of the storage period; whereas this provision reduces considerably the flexibility of this support measure and, as a consequence, the interest of the operators in this operation; whereas it is, therefore, appropriate to delete this obligation;
Whereas it is necessary that this Regulation applies from the beginning of the period for submission of the requests for contracts, i.e. from 28 September 1998 in order to ensure equal treatment of all the operators taking part in the aid for private storage scheme;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Pigmeat,
HAS ADOPTED THIS REGULATION:
Article 1
In Article 1 of Regulation (EC) No 2042/98, paragraphs 2 and 3 are deleted and paragraph 4 becomes paragraph 2.
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Communities.
It shall apply with effect from 28 September 1998.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 4 December 1998.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ L 282, 1. 11. 1975, p. 1.
(2) OJ L 349, 31. 12. 1994, p. 105.
(3) OJ L 263, 26. 9. 1998, p. 12.
