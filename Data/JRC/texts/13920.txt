Commission Regulation (EC) No 972/2006
of 29 June 2006
laying down special rules for imports of Basmati rice and a transitional control system for determining their origin
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1785/2003 of 29 September 2003 on the common organisation of the market in rice [1], and in particular Articles 10(2) and 11b thereof,
Whereas:
(1) Pursuant to the Agreement in the form of an Exchange of Letters between the European Community and India pursuant to Article XXVIII of the GATT 1994 relating to the modification of concessions with respect to rice provided for in EC Schedule CXL annexed to the GATT 1994 [2], approved by Council Decision 2004/617/EC [3], the duty applicable to imports of husked rice of certain Basmati varieties is fixed at zero.
(2) Pursuant to the Agreement in the form of an Exchange of Letters between the European Community and Pakistan pursuant to Article XXVIII of the GATT 1994 relating to the modification of concessions with respect to rice provided for in EC Schedule CXL annexed to the GATT 1994 [4], approved by Council Decision 2004/618/EC [5], the duty applicable to imports of husked rice of certain Basmati varieties originating in Pakistan is fixed at zero.
(3) Pursuant to Article 2(1) of Decision 2004/617/EC and Article 2(1) of Decision 2004/618/EC, the Commission adopted Regulation (EC) No 1549/2004 [6], which, until Regulation (EC) No 1785/2003 was amended, derogated therefrom as regards the arrangements for importing rice and laid down separate transitional rules for imports of Basmati rice. Given that Regulation (EC) No 1785/2003 has been amended to that end, for the sake of clarity, Regulation (EC) No 1549/2004 should be replaced by a new Regulation.
(4) The Agreements approved by Decisions 2004/617/EC and 2004/618/EC provide for the introduction of a Community control system based on DNA analysis at the border and transitional arrangements for imports of Basmati rice pending the entry into force of that control system. Since the definitive control system has not yet been introduced, special transitional rules should be laid down.
(5) To be eligible for zero import duty, Basmati rice must belong to a variety specified in the Agreements. In order to ascertain that Basmati rice imported at a zero rate of duty meets those characteristics, it should be covered by an authenticity certificate drawn up by the competent authorities.
(6) In order to prevent fraud, provision should be made for measures to check the variety of Basmati rice declared. To that end the provisions on sampling laid down in Commission Regulation (EEC) No 2454/93 of 2 July 1993 laying down provisions for the implementation of Council Regulation (EEC) No 2913/92 establishing the Community Customs Code [7] should be applied.
(7) The transitional import arrangements for Basmati rice provide for a procedure for consulting the exporting country in the event of a market disturbance and possibly applying the full rate of duty if a satisfactory solution has not been found at the end of the consultations. The point at which the market may be considered to be disturbed should be defined.
(8) To ensure sound administrative management of imports of Basmati rice, special rules, whether additional to or derogating from Commission Regulation (EC) No 1291/2000 of 9 June 2000, laying down common detailed rules for the application of the system of import and export licences and advance fixing certificates for agricultural products [8] and Commission Regulation (EC) No 1342/2003 of 28 July 2003 laying down special detailed rules for the application of the system of import and export licences for cereals and rice [9], should be adopted concerning the lodging of applications, the issue of licences and the use thereof.
(9) So as not to disrupt the continuity of imports of Basmati rice, it should be laid down that authenticity certificates and import licences issued before 1 July 2006 pursuant to Regulation (EC) No 1549/2004 remain valid for their entire period of validity and that the zero rate of duty applies to products imported under those licences.
(10) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Cereals,
HAS ADOPTED THIS REGULATION:
Article 1
This Regulation shall apply to husked Basmati rice of the varieties of Basmati rice falling within CN code 10062017 and CN code 10062098, as specified in Annex IIIa to Regulation (EC) No 1785/2003.
Article 2
1. Import licence applications for Basmati rice as referred to in Article 10(1) of Regulation (EC) No 1785/2003 shall contain the following details:
(a) in box 8, indication of the country of origin and the word "yes" marked with a cross;
(b) in box 20, one of the entries listed in Annex I.
2. Import licence applications for Basmati rice shall be accompanied by:
(a) proof that the applicant is a natural or legal person who has, for at least 12 months, carried on a commercial activity in the rice sector and is registered in the Member State where the application is made;
(b) a product authenticity certificate issued by a competent body in the exporting country, as listed in Annex II hereto.
Article 3
1. The authenticity certificate shall be drawn up on a form in accordance with the specimen given in Annex III hereto.
The form shall measure approximately 210 × 297 millimetres. The original shall be drawn up on such paper as shall show up any tampering by mechanical or chemical means.
The forms shall be printed and completed in English.
The original and the copies thereof shall be either typewritten or handwritten. In the latter case, they shall be completed in ink and in block capitals.
Each authenticity certificate shall contain a serial number in the top right-hand box. The copies shall bear the same number as the original.
The text of the form in the other Community languages shall be published in the C series of the Official Journal of the European Union.
2. The body issuing the import licence shall keep the original of the authenticity certificate and give the applicant a copy.
The authenticity certificate shall be valid for 90 days from the date of issue.
It shall be valid only if the boxes are duly completed and it is signed.
Article 4
1. Import licences for Basmati rice shall contain the following details:
(a) in box 8, indication of the country of origin and the word "yes" marked with a cross;
(b) in box 20, one of the entries listed in Annex IV hereto.
The copy of the authenticity certificate referred to in Article 3(2) shall be annexed to the import licence.
2. Notwithstanding Article 9 of Regulation (EC) No 1291/2000, rights deriving from import licences for Basmati rice shall not be transferable.
3. Notwithstanding Article 12 of Regulation (EC) No 1342/2003, the security relating to import licences for Basmati rice shall be EUR 70 per tonne.
Article 5
Member States shall e-mail the following information to the Commission:
(a) no later than two working days following a refusal, the quantities in respect of which applications for import licences for Basmati rice have been refused, with an indication of the date of refusal and the grounds, the CN code, the country of origin, the issuing body and the number of the authenticity certificate, as well as the holder's name and address;
(b) no later than two working days following their issue, the quantities in respect of which applications for import licences for Basmati rice have been issued, with an indication of the date, the CN code, the country of origin, the issuing body and the number of the authenticity certificate, as well as the holder's name and address;
(c) in the event of the cancellation of a licence, no later than two working days after cancellation, the quantities in respect of which licences have been cancelled and the names and addresses of the holders of the cancelled licences;
(d) on the last working day of each month following the month of release for free circulation, the quantities actually released for free circulation, with an indication of the CN code, the country of origin, the issuing body and the number of the authenticity certificate.
The information referred to in the first paragraph shall be communicated separately from that relating to other import licence applications for rice.
Article 6
1. In the context of random checks or checks targeted at operations entailing a risk of fraud, Member States shall take representative samples of imported Basmati rice as laid down in Article 242 of Regulation (EC) No 2454/93. The samples shall be sent to the competent body in the country of origin, as listed in Annex V, for a DNA-based variety test.
The Member States may also carry out variety tests on the same sample in a Community laboratory.
2. If the results of one of the tests referred to in paragraph 1 show that the product analysed does not correspond to what is indicated on the authenticity certificate, the import duty on husked rice falling within CN code 100620, provided for in Article 11a of Regulation (EC) No 1785/2003, shall apply.
3. If the tests referred to in paragraph 1 or other information available to the Commission indicate a serious and lasting problem as regards the control procedures applied by a competent body in the country of origin, the Commission may contact the competent authorities in the country concerned. If such contacts fail to yield a satisfactory solution, the Commission may decide to apply the rate of import duty for husked rice falling within CN code 100620, provided for in Article 11a of Regulation (EC) No 1785/2003 to the imports checked by the body in question, based on Article 11b of the said Regulation and under the conditions laid down therein.
Article 7
1. The rice market shall be considered to be disturbed when, inter alia, a substantial increase in Basmati rice imports is noted for one quarter of the year relative to the previous quarter and there is no satisfactory explanation.
2. If a disturbance of the rice market persists and if the Commission's consultations of the exporting countries concerned fail to yield a satisfactory solution, the import duty on husked rice falling within CN code 100620, provided for in Article 11a of Regulation (EC) No 1785/2003, may also be applied to imports of Basmati rice, by Commission Decision, on the basis of Article 11b of the said Regulation and subject to the conditions laid down in therein.
Article 8
The Commission shall keep Annexes II and V up to date.
Article 9
Authenticity certificates and import licences for Basmati rice issued prior to 1 July 2006 under Regulation (EC) No 1549/2004 shall remain valid and products imported under such licences shall be eligible for the import duty provided for in Article 11a of Regulation (EC) No 1785/2003.
Article 10
Regulation (EC) No 1549/2004 is hereby repealed.
All references to Articles 2 to 8 of Regulation (EC) No 1549/2004 and Annexes II to VI thereto shall be read as references to Articles 2 to 8 of this Regulation and Annexes I to V hereto.
All references to Annex I to Regulation (EC) No 1549/2004 shall be read as references to Annex IIIa to Regulation (EC) No 1785/2003.
Article 11
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Union.
It shall apply from 1 July 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 29 June 2006.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 270, 21.10.2003, p. 96. Regulation as last amended by Regulation (EC) No 797/2006 (OJ L 144, 31.5.2006, p. 1).
[2] OJ L 279, 28.8.2004, p. 19.
[3] OJ L 279, 28.8.2004, p. 17. Decision as amended by Decision 2005/476/EC (OJ L 170, 1.7.2005, p. 67).
[4] OJ L 279, 28.8.2004, p. 25.
[5] OJ L 279, 28.8.2004, p. 23. Decision as amended by Decision 2005/476/EC.
[6] OJ L 280, 31.8.2004, p. 13. Regulation as last amended by Commission Regulation (EC) No 2152/2005 (OJ L 342, 24.12.2005, p. 30).
[7] OJ L 253, 11.10.1993, p. 1. Regulation as last amended by Regulation (EC) No 402/2006 (OJ L 70, 9.3.2006, p. 35).
[8] OJ L 152, 24.6.2000, p. 1. Regulation as last amended by Regulation (EC) No 410/2006 (OJ L 71, 10.3.2006, p. 7).
[9] OJ L 189, 29.7.2003, p. 12. Regulation as last amended by Regulation (EC) No 830/2006 (OJ L 150, 3.6.2006, p. 3).
--------------------------------------------------
ANNEX I
Indications referred to in Article 2(1)(b)
in Spanish : Arroz Basmati del código NC 10062017 o 10062098 importado con derecho cero en aplicación del Reglamento (CE) no 972/2006, acompañado del certificado de autenticidad no … expedido por [nombre de la autoridad competente]
in Czech : rýže Basmati kódu KN 10062017 nebo 10062098, která se dováží za nulové clo na základě nařízení (ES) č. 972/2006, a ke které se připojí osvědčení o pravosti č. … vydané [název příslušného subjektu]
in Danish : Basmati-ris henhørende under KN-kode 10062017 eller 10062098 importeres med nultold i henhold til forordning (EF) nr. 972/2006, ledsaget af ægthedscertifikat nr. … udstedt af [den kompetente myndigheds navn]
in German : Basmati-Reis des KN-Codes 10062017 oder 10062098, eingeführt zum Zollsatz Null gemäß der Verordnung (EG) Nr. 972/2006 und begleitet von einem Echtheitszeugnis Nr. …, ausgestellt durch [Name der zuständigen Behörde]
in Estonian : basmati riis CN-koodiga 10062017 või 10062098, mis on imporditud tollimaksu nullmääraga vastavalt määrusele (EÜ) nr 972/2006 ning millega on kaasas [pädeva asutuse nimi] välja antud autentsussertifikaat nr …
in Greek : Ρύζι μπασμάτι του κωδικού 10062017 ή 10062098 εισαγόμενο με μηδενικό δασμό κατ’ εφαρμογή του κανονισμού (ΕΚ) αριθ. 972/2006, συνοδευόμενο με το πιστοποιητικό γνησιότητας αριθ. … που εκδόθηκε από [ονομασία της αρμόδιας αρχής]
in English : basmati rice falling within code of CN 10062017 or 10062098 and imported at a zero rate of duty under Regulation (EC) No 972/2006, accompanied by authenticity certificate No … drawn up by [name of the competent authority]
in French : riz Basmati du code NC 10062017 ou 10062098 importé à droit nul en application du règlement (CE) no 972/2006, accompagné du certificat d’authenticité no … établi par [nom de l’autorité compétente]
in Italian : Riso Basmati di cui al codice NC 10062017 o 10062098 importato a dazio zero ai sensi del regolamento (CE) n. 972/2006, corredato del certificato di autenticità n. … rilasciato da [nome dell’autorità competente]
in Latvian : Basmati rīsi ar KN kodu 10062017 vai 10062098, ko importē bez ievedmuitas nodokļa saskaņā ar Regulu (EK) Nr. 972/2006, kuriem pievienota autentiskuma apliecība Nr. …, ko izsniegusi [kompetentās iestādes nosaukums]
in Lithuanian : Basmati ryžiai klasifikuojami KN kodu 10062017 arba 10062098, įvežti pagal nulinį muito mokestį pagal Reglamentas (EB) Nr. 972/2006, prie kurio pridėtas autentiškumo sertifikatas Nr. …, išduotas [kompetentingos institucijos pavadinimas]
in Hungarian : az 10062017 vagy az 10062098 KN-kód alá sorolt, a/az 972/2006/EK rendelet alkalmazásában nulla vámtétel mellett behozott basmati rizs, a/az [illetékes hatóság neve] által kiállított, … számú eredetiségigazolással együtt
in Dutch : Basmati-rijst van GN-code 10062017 of 10062098, ingevoerd met nulrecht overeenkomstig Verordening (EG) nr. 972/2006, vergezeld van het echtheidscertificaat nr. …, opgesteld door [naam van de bevoegde instantie]
in Polish : Ryż Basmati objęty kodem CN 10062017 lub 10062098, do którego przywiezienia zastosowano zerową stawkę celną zgodnie z rozporządzeniem (WE) nr 972/2006, z załączonym do niego certyfikatem autentyczności nr … sporządzonym przez [nazwa właściwego organu]
in Portuguese : Arroz Basmati do código NC 10062017 ou 10062098 importado com direito nulo em aplicação do Regulamento (CE) n.o 972/2006, acompanhado do certificado de autenticidade n.o … estabelecido por [nome da autoridade competente]
in Slovak : ryža Basmati s kódom KN 10062017 alebo 10062098 dovážaná s nulovou sadzbou cla v súlade s nariadením (ES) č. 972/2006, sprevádzaná osvedčením o pravosti č. … vystavenom [názov príslušného orgánu]
in Slovenian : Riž basmati s kodo KN 10062017 ali 10062098, uvožen po stopnji nič ob uporabi Uredbe (ES) št. 972/2006, s priloženim potrdilom o pristnosti št. …, ki ga je izdal [naziv pristojnega organa]
in Finnish : Asetuksen (EY) N:o 972/2006 mukaisesti tullivapaasti tuotu CN-koodiin 10062017 tai 10062098 kuuluva Basmati-riisi, jonka mukana on ….:n [toimivaltaisen viranomaisen nimi] myöntämän aitoustodistuksen N:o …
in Swedish : Basmatiris med KN-nummer 10062017 eller 10062098 som importeras tullfritt i enlighet med förordning (EG) nr 972/2006, åtföljt av äkthetsintyg nr … som utfärdats av [den behöriga myndighetens namn]
--------------------------------------------------
ANNEX II
Bodies authorised to issue the authenticity certificates referred to in Article 2(2)(b) |
INDIA [1] | Export Inspection Council (Ministry of Commerce, Government of India) |
PAKISTAN [2] | Trading Corporation of Pakistan (Pvt) Ltd |
[1] In respect of the varieties Basmati 370, Basmati 386, Type-3 (Dhradun), Taraori Basmati (HBC-19), Basmati 217, Ranbir Basmati, Pusa Basmati and Super Basmati.
[2] In respect of the varieties Kernel (Basmati), Basmati 370, Pusa Basmati and Super Basmati.
--------------------------------------------------
ANNEX III
Specimen authenticity certificate referred to in Article 3(1)
+++++ TIFF +++++
--------------------------------------------------
ANNEX IV
Indications referred to in Article 4(1)(b)
in Spanish : Arroz Basmati del código NC 10062017 o 10062098 importado con derecho cero en aplicación del Reglamento (CE) no 972/2006, acompañado de una copia del certificado de autenticidad no … expedido por [nombre de la autoridad competente]
in Czech : rýže Basmati kódu KN 10062017 nebo 10062098, která se dováží za nulové clo na základě nařízení (ES) č. 972/2006, a ke které se připojí kopie osvědčení o pravosti č. … vydané [název příslušného subjektu]
in Danish : Basmati-ris henhørende under KN-kode 10062017 eller 10062098 importeres med nultold i henhold til forordning (EF) nr. 972/2006, ledsaget af en kopi af ægthedscertifikat nr. … udstedt af [den kompetente myndigheds navn]
in German : Basmati-Reis des KN-Codes 10062017 oder 10062098, eingeführt zum Zollsatz Null gemäß der Verordnung (EG) Nr. 972/2006 und begleitet von einer Kopie des Echtheitszeugnisses Nr. …, ausgestellt durch [Name der zuständigen Behörde]
in Estonian : basmati riis CN-koodiga 10062017 või 10062098, mis on imporditud tollimaksu nullmääraga vastavalt määrusele (EÜ) nr 972/2006 ning millega on kaasas [pädeva asutuse nimi] välja antud autentsussertifikaadi nr …koopia
in Greek : Ρύζι μπασμάτι του κωδικού 10062017 ή 10062098 εισαγόμενο με μηδενικό δασμό με εφαρμογή του κανονισμού (ΕΚ) αριθ. 972/2006, συνοδευόμενο με αντίγραφο του πιστοποιητικού γνησιότητας αριθ. … που εκδόθηκε από [ονομασία της αρμόδιας αρχής]
in English : basmati rice falling within code of CN 10062017 or 10062098 and imported at a zero rate of duty under Regulation (EC) No 972/2006, accompanied by a copy of authenticity certificate No … drawn up by [name of the competent authority]
in French : riz Basmati du code NC 10062017 ou 10062098 importé à droit nul en application du règlement (CE) no 972/2006, accompagné d’une copie du certificat d’authenticité no … établi par [nom de l’autorité compétente]
in Italian : Riso Basmati di cui al codice NC 10062017 o 10062098 importato a dazio zero ai sensi del regolamento (CE) n. 972/2006, corredato di una copia del certificato di autenticità n. … rilasciato da [nome dell’autorità competente]
in Latvian : Basmati rīsi ar KN kodu 10062017 vai 10062098, ko importē bez ievedmuitas nodokļa saskaņā ar Regulu (EK) Nr. 972/2006, kuriem pievienota autentiskuma apliecības Nr. … kopija, ko izsniegusi [kompetentās iestādes nosaukums]
in Lithuanian : Basmati ryžiai klasifikuojami KN kodu 10062017 arba 10062098, įvežti pagal nulinį muito mokestį pagal Reglament (EB) Nr. 972/2006, prie kurio pridėta autentiškumo sertifikato Nr. …, išduoto [kompetentingos institucijos pavadinimas], kopija
in Hungarian : az 10062017 vagy az 10062098 KN-kód alá sorolt, a 972/2006/EK rendelet alkalmazásában nulla vámtétel mellett behozott basmati rizs, a/az [illetékes hatóság neve] által kiállított, … számú eredetiségigazolás másolatával együtt
in Dutch : Basmati-rijst van GN-code 10062017 of 10062098, ingevoerd met nulrecht overeenkomstig Verordening (EG) nr. 972/2006, vergezeld van een kopie van het echtheidscertificaat nr. …, opgesteld door [naam van de bevoegde instantie]
in Polish : Ryż Basmati objęty kodem CN 10062017 lub 10062098, do którego przywiezienia zastosowano zerową stawkę celną zgodnie z rozporządzeniem (WE) nr 972/2006, z załączoną do niego kopią certyfikatu autentyczności nr … sporządzonego przez [nazwa właściwego organu]
in Portuguese : Arroz Basmati do código NC 10062017 ou 10062098 importado com direito nulo em aplicação do Regulamento (CE) n.o 972/2006, acompanhado de uma cópia do certificado de autenticidade n.o … estabelecido por [nome da autoridade competente]
in Slovak : ryža Basmati s kódom KN 10062017 alebo 10062098 dovážaná s nulovou sadzbou cla v súlade s nariadením (ES) č. 972/2006, sprevádzaná kópiou osvedčenia o pravosti č. … vystavenom [názov príslušného orgánu]
in Slovenian : Riž basmati s kodo KN 10062017 ali 10062098, uvožen po stopnji nič ob uporabi Uredbe (ES) št. 972/2006, s priloženo kopijo potrdila o pristnosti št. …, ki ga je izdal [naziv pristojnega organa]
in Finnish : Asetuksen (EY) N:o 972/2006 mukaisesti tullivapaasti tuotu CN-koodiin 10062017 tai 10062098 kuuluva Basmati-riisi, jonka mukana on ….:n [toimivaltaisen viranomaisen nimi] myöntämän aitoustodistuksen N:o … jäljennös
in Swedish : Basmatiris med KN-nummer 10062017 eller 10062098 som importeras tullfritt i enlighet med förordning (EG) nr 972/2006, åtföljt av en kopia av äkthetsintyg nr … som utfärdats av [den behöriga myndighetens namn]
--------------------------------------------------
ANNEX V
Bodies authorised to conduct the variety tests referred to in Article 6
INDIA
Export Inspection Council
Department of Commerce
Ministry of Commerce and Industry
3rd Floor
NDYMCA Cultural Central Bulk
1 Jaisingh Road
New Delhi 110 001
India
Tel: (91-11) 374 81 88/89, 336 55 40
Fax: (91-11) 374 80 24
e-mail: eic@eicindia.org
PAKISTAN:
Trading Corporation of Pakistan Limited
4th and 5th Floor
Finance & Trade Centre
Shahrah-e-Faisal
Karachi 75530
Pakistan
Tel: (92-21) 290 28 47
Fax (92-21) 920 27 22 and 920 27 31
--------------------------------------------------
