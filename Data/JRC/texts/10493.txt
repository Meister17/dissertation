Council Regulation (EC) No 838/2006
of 20 March 2006
concerning the implementation of the Agreement in the form of an Exchange of Letters between the European Community and the People's Republic of China pursuant to Article XXIV:6 and Article XXVIII of the General Agreement on Tariffs and Trade (GATT) 1994 relating to the modification of concessions in the schedules of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic in the course of their accession to the European Union, amending and supplementing Annex I to Regulation (EEC) No 2658/87 on the tariff and statistical nomenclature and on the Common Customs Tariff
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 133 thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) Council Regulation (EEC) No 2658/87 [1] established a goods nomenclature, hereinafter referred to as the "Combined Nomenclature", and set out the conventional duty rates of the Common Customs Tariff.
(2) By Decision 2006/398/EC of 20 March 2006 on the conclusion of an Agreement in the form of an Exchange of Letters between the European Community and the People's Republic of China pursuant to Article XXIV:6 and Article XXVIII of the General Agreement on Tariffs and Trade (GATT) 1994 relating to the modification of concessions in the schedules of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic in the course of their accession to the European Union [2], the Council approved the said Agreement on behalf of the Community, with a view to closing negotiations initiated pursuant to Article XXIV:6 of GATT 1994.
(3) Regulation (EEC) No 2658/87 should therefore be amended and supplemented accordingly,
HAS ADOPTED THIS REGULATION:
Article 1
Annex I to Regulation (EEC) No 2658/87 shall be amended as follows:
(a) in Part two, Schedule of customs duties, the duty rates shall be amended as shown in point (a) of the Annex to this Regulation;
(b) Annex 7 of Section III of Part three, WTO Tariff Quotas to be opened by the competent Community authorities, shall be amended with the duties and supplemented with the volumes following the terms and conditions shown in point (b) of the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Union.
Article 1, point (b) shall apply six weeks from the date of the publication of this Regulation.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 20 March 2006.
For the Council
The President
U. Plassnik
[1] OJ L 256, 7.9.1987, p. 1. Regulation as last amended by Regulation (EC) No 486/2006 (OJ L 88, 25.3.2006, p. 1).
[2] See page 22 of this Official Journal.
--------------------------------------------------
ANNEX
Notwithstanding the rules for the interpretation of the Combined Nomenclature, the wording for the description of the products is to be considered as having no more than an indicative value, the concessions being determined, within the context of this Annex, by the coverage of the CN codes as they exist at the time of adoption of the current regulation. Where ex CN codes are indicated, the concessions are to be determined by application of the CN code and corresponding description taken together.
(a) In Annex I to Regulation (EEC) No 2658/87, Part two, Schedule of customs duties, the duty rates are the following:
CN code | Description | Duty rate |
Tariff item number 03042085 | Frozen fillets of Alaska pollack (Theragra chalcogramma) | bound duty of 13,7 % |
Tariff item number 64021900 | Other footwear with outer soles and uppers of rubber or plastics | bound duty of 16,9 % |
Tariff item number 64029100 | Other footwear covering the ankle with outer soles and uppers of rubber or plastics | bound duty of 16,9 % |
Tariff item number 640299 | Other footwear with outer soles and uppers of rubber or plastics | bound duty of 16,8 % |
Tariff item number 64041100 | Sports footwear; tennis shoes, basketball shoes, gym shoes, training shoes and the like | bound duty of 16,9 % |
Tariff item number 64041910 | Slippers and other indoor footwear | bound duty of 16,9 % |
Tariff item number 84829190 | Other balls, needles and rollers | bound duty of 7,7 % |
Tariff item number 85219000 | Other video recording or reproducing apparatus, whether or not incorporating a video tuner | bound duty of 13,9 % |
Tariff item number 87120030 | Bicycles not motorised | bound duty of 14 % |
(b) Annex 7, WTO Tariff Quotas to be opened by the competent Community authorities, Part three, Section III of Annex I to Regulation (EEC) No 2658/87, the other terms and conditions are the following:
CN code | Description | Other terms and conditions |
Tariff item number 07032000 | Garlic, fresh or chilled | Add 20500 tonnes to the allocation for China under the EC tariff rate quota |
Tariff item number 100610 | Paddy rice | Implemented through Council Regulation (EC) No 683/2006 [1] |
Tariff item number 100620 | Husked rice | Implemented through Council Regulation (EC) No 683/2006 |
Tariff item number 100630 | Milled and semi-milled rice | Implemented through Council Regulation (EC) No 683/2006 |
Tariff item number 100640 | Broken rice | Implemented through Council Regulation (EC) No 683/2006 |
Tariff item number 20031030 | Mushroom of species agaricus, prepared or preserved otherwise than by vinegar | Add 5200 tonnes (drained net weight) in EC tariff rate quota, allocated to China |
Tariff item number 20031020 | Mushroom of species agaricus, provisionally preserved or preserved otherwise than by vinegar | |
Tariff item number 07115100 | | |
Tariff item numbers [2] | Preserved pineapples, citrus fruit, pears, apricots, cherries, peaches and strawberries | Open a tariff rate quota of 2838 tonnes (erga omnes), in quota rate 20 %. The existing out of quota rates of the EC shall apply |
[1] OJ L 120, 5.5.2006, p. 1.
[2]
- 20082011: EUR 25,6 + 2,5 100 kg/net
- 20082019: 25,6
- 20082031: EUR 25,6 + 2,5 100 kg/net
- 20082039: 25,6
- 20082071: 20,8
- 20083011: 25,6
- 20083019: EUR 25,6 + 4,2 100 kg/net
- 20083031: 24
- 20083039: 25,6
- 20083079: 20,8
- 20084011: 25,6
- 20084019: EUR 25,6 + 4,2 100 kg/net
- 20084021: 24
- 20084029: 25,6
- 20084031: EUR 25,6 + 4,2 100 kg/net
- 20084039: 25,6
- 20085011: 25,6
- 20085019: EUR 25,6 + 4,2 100 kg/net
- 20085031: 24
- 20085039: 25,6
- 20085051: EUR 25,6 + 4,2 100 kg/net
- 20085059: 25,6
- 20085071: 20,8
- 20086011: 25,6
- 20086019: EUR 25,6 + 4,2 100 kg/net
- 20086031: 24
- 20086039: 25,6
- 20086060: 20,8
- 20087011: 25,6
- 20087919: EUR 25,6 + 4,2 100 kg/net
- 20087031: 24
- 20087039: 25,6
- 20087051: EUR 25,6 + 4,2 100 kg/net
- 20087059: 25,6
- 20088011: 25,6
- 20088019: EUR 25,6 + 4,2 100 kg/net
- 20088031: 24
- 20088039: 25,6
- 20088070: 20,8
--------------------------------------------------
