Notice of initiation of a partial interim review of the anti-dumping measures applicable to imports of synthetic staple fibres of polyester originating in Thailand
(2005/C 307/02)
The Commission has received a request for a partial interim review pursuant to Article 11(3) of Council Regulation (EC) No 384/96 on protection against dumped imports from countries not members of the European Community ("the basic Regulation") [1], as last amended by Council Regulation (EC) No 461/2004 [2].
1. Request for review
The request was lodged by Tuntex (Thailand) Public Company Limited ("the applicant"), an exporter from Thailand.
The request is limited in scope to the examination of dumping as far as the applicant is concerned.
2. Product
The product under review is synthetic staple fibres of polyester, not carded, combed or otherwise processed for spinning originating in Thailand ("the product concerned"), currently classifiable within CN code 55032000. It is commonly referred to as polyester staple fibres or PSF. This CN code is given only for information.
3. Existing measures
The measures currently in force are a definitive anti-dumping duty imposed by Council Regulation (EC) No 1522/2000 [3] on imports of synthetic staple fibres of polyester originating in Thailand. On 14 July 2005, a review of the existing measures was initiated on the basis of Article 11(2) of the basic Regulation [4].
4. Grounds for the review
The request pursuant to Article 11(3) of the basic Regulation is based on the prima facie evidence, provided by the applicant, that the circumstances on the basis of which measures were established have changed and that these changes are of a lasting nature.
The applicant has provided evidence showing that a comparison of normal value based on its own cost/domestic prices and its export prices to a third country market comparable to the EU, would lead to a reduction of dumping significantly below the level of the current measure. Therefore, the continued imposition of measures at the existing levels, which were based on the level of dumping previously established, is no longer necessary to offset dumping.
5. Procedure for the determination of dumping
Having determined, after consulting the Advisory Committee, that sufficient evidence exists to justify the initiation of a partial interim review, the Commission hereby initiates a review in accordance with Article 11(3) of the basic Regulation.
The investigation will assess the need for the continuation, removal or amendment of the existing measures in respect of the sole applicant.
(a) Questionnaires
In order to obtain the information it deems necessary for its investigation, the Commission will send questionnaires to the applicant and to the authorities of the exporting country concerned. This information and supporting evidence should reach the Commission within the time limit set in point 6(a).
(b) Collection of information and holding of hearings
All interested parties are hereby invited to make their views known, submit information other than questionnaire replies and to provide supporting evidence. This information and supporting evidence must reach the Commission within the time limit set in point 6(a).
Furthermore, the Commission may hear interested parties, provided that they make a request showing that there are particular reasons why they should be heard. This request must be made within the time limit set in point 6(b).
6. Time limits
(a) For parties to make themselves known, to submit questionnaire replies and any other information
All interested parties, if their representations are to be taken into account during the investigation, must make themselves known by contacting the Commission, present their views and submit questionnaire replies or any other information within 40 days of the date of publication of this notice in the Official Journal of the European Union, unless otherwise specified. Attention is drawn to the fact that the exercise of most procedural rights set out in the basic Regulation depends on the party's making itself known within the aforementioned period.
(b) Hearings
All interested parties may also apply to be heard by the Commission within the same 40-day time limit.
7. Written submissions, questionnaire replies and correspondence
All submissions and requests made by interested parties must be made in writing (not in electronic format, unless otherwise specified) and must indicate the name, address, e-mail address, telephone and fax numbers of the interested party. All written submissions, including the information requested in this notice, questionnaire replies and correspondence provided by interested parties on a confidential basis shall be labelled as "Limited" [5] and, in accordance with Article 19(2) of the basic Regulation, shall be accompanied by a non-confidential version, which will be labelled "FOR INSPECTION BY INTERESTED PARTIES".
Commission address for correspondence:
European Commission
Directorate General for Trade
Directorate B
Office: J-79 5/16
BE-1049 Brussels
Fax (32-2) 295 65 05
8. Non-cooperation
In cases in which any interested party refuses access to or does not provide the necessary information within the time limits, or significantly impedes the investigation, provisional or final findings, affirmative or negative, may be made in accordance with Article 18 of the basic Regulation, on the basis of the facts available.
Where it is found that any interested party has supplied false or misleading information, the information shall be disregarded and use may be made of the facts available. If an interested party does not cooperate or cooperates only partially and findings are therefore based on facts available in accordance with Article 18 of the basic Regulation, the result may be less favourable to that party than if it had cooperated.
[1] OJ L 56, 6.3.1996, p. 1. Regulation as last amended by Regulation (EC) No 461/2004 (OJ L 77, 13.3.2004, p. 12).
[2] OJ L 77, 13.3.2004, p. 12.
[3] OJ L 175, 14.7.2000, p. 10.
[4] OJ C 174, 14.7.2005, p. 15.
[5] This means that the document is for internal use only. It is protected pursuant to Article 4 of Regulation (EC) No 1049/2001 of the European Parliament and of the Council regarding public access to European Parliament, Council and Commission documents (OJ L 145, 31.5.2001, p. 43). It is a confidential document pursuant to Article 19 of the basic Regulation and Article 6 of the WTO Agreement on Implementation of Article VI of the GATT 1994 (Anti-dumping Agreement).
--------------------------------------------------
