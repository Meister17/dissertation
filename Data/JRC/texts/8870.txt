COMMISSION DECISION of 17 September 1976 concluding the Framework Agreement for commercial and economic cooperation between the European Communities and Canada (76/753/Euratom)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Atomic Energy Community, and in particular the second paragraph of Article 101 thereof,
Having regard to the approval of the Council,
Whereas the Framework Agreement for commercial and economic cooperation between the European Communities and Canada, signed in Ottawa on 6 July 1976, should be concluded on behalf of the European Atomic Energy Community,
HAS DECIDED AS FOLLOWS:
Article 1
The Framework Agreement for commercial and economic cooperation between the European Communities and Canada is hereby concluded and approved on behalf of the European Atomic Energy Community.
The text of the Agreement is annexed to this Decision.
Article 2
The President of the Commission shall give, as regards the Community, the notification provided for in Article 8 of the Agreement (1).
Article 3
This Decision shall enter into force on the day following its publication in the Official Journal of the European Communities.
Done at Brussels, 17 September 1976.
For the Commission
Christopher SOAMES
Vice-President
(1) The date of entry into force of the Agreement, as regards the European Atomic Energy Community, will be published in the Official Journal of the European Communities.
