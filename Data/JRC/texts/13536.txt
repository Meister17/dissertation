Commission Regulation (EC) No 315/2006
of 22 February 2006
implementing Regulation (EC) No 1177/2003 of the European Parliament and of the Council concerning Community statistics on income and living conditions (EU-SILC) as regards the list of target secondary variables relating to housing conditions
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Regulation (EC) No 1177/2003 of the European Parliament and the Council of 16 June 2003 concerning Community statistics on income and living conditions (EU-SILC) [1], and in particular Article 15(2)(f) thereof,
Whereas:
(1) Regulation (EC) No 1177/2003 established a common framework for the systematic production of Community statistics on income and living conditions, encompassing comparable and timely cross-sectional and longitudinal data on income, and on the level and composition of poverty and social exclusion at national and European Union levels.
(2) Pursuant to Article 15(2)(f) of Regulation (EC) No 1177/2003, implementing measures are necessary for the list of target secondary areas and variables to be included every year in the cross-sectional component of EU-SILC. For the year 2007, the list of target secondary variables included in the module on housing conditions should be laid down. This should be accompanied by the provision of variable codes and definitions.
(3) The measures provided for in this Regulation are in accordance with the opinion of the Statistical Programme Committee,
HAS ADOPTED THIS REGULATION:
Article 1
The list of target secondary variables, the variable codes, and the definitions for the 2007 module on housing conditions to be included in the cross-sectional component of Community statistics on income and living conditions (EU-SILC) shall be as laid down in the Annex.
Article 2
This Regulation shall enter into force on the 20th day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 22 February 2006.
For the Commission
Joaquín Almunia
Member of the Commission
[1] OJ L 165, 3.7.2003, p. 1. Regulation as amended by Regulation (EC) No 1553/2005 (OJ L 255, 30.9.2005, p. 6).
--------------------------------------------------
ANNEX
For the purposes of this Regulation, the following units, modes of data collection, reference periods and definitions shall apply.
1. Units
The target variables relate to two types of units:
- household (all variables except Change of dwelling)
- the household respondent (change of dwelling)
2. Modes of data collection
For all target variables the mode of data collection is personal interview with the household respondent or register.
3. Reference periods
The target variables relate to three types of reference periods:
- usual: an ordinary winter/summer, in the area where the dwelling is located (dwelling comfortably warm during winter time. Dwelling comfortably cool during summer time)
- last two years (changed dwelling)
- current (all other variables)
4. Definitions
1. Shortage of space in dwelling
(a) Shortage of space: the variable refers to the respondent’s opinion/feeling about shortage of space in dwelling.
2. Dwelling installations and facilities
(a) Electrical installations: wiring, contacts, sockets and other permanent electrical installations in the dwelling.
(b) Plumbing/water installations: pipes, taps, drainage and outlets.
(c) Central heating or similar: a housing unit is considered as centrally heated if heating is provided either from a community heating centre or from an installation built in the building or in the housing unit, established for heating purposes, without regard to the source of energy. Fixed electrical radiators, fixed gas heaters and similar are included. The heating shall be available in most rooms.
(d) Other fixed heating: a housing unit is considered heated by "other fixed heating" when the heating is not considered as "central heating/or similar". It includes stoves, heaters, fireplaces and similar.
(e) No fixed heating: no fixed heating system or heating device. Portable heating.
(f) Air conditioning facilities: systems for controlling, especially lowering, the temperature and humidity of an enclosed space; systems that keep air cool and dry. Simple fans are not considered as air conditioning.
(g) Adequate: sufficient to satisfy the general requirements/needs of the household. An installation which is permanently out of order is considered as no installation. Inadequate installations can be: installations in bad condition, dangerous installations, installations which are regularly out of order, where there is not enough electrical power/pressure for the water to be used, the water is not drinkable, or there is limited availability. Minor temporary problems such as a blockage in the outlet do not mean that the installation is inadequate.
3. Accessibility of basic needs
(a) Accessibility: this shall relate to the services used by the household having regard to the financial, physical, technical and health conditions. The accessibility of the services is to be assessed in terms of physical and technical access, and opening hours, but not in terms of quality, price and similar aspects.
(b) Grocery services: services which can provide most of the daily needs.
(c) Banking services: withdraw cash, transfer money and pay bills.
(d) Postal services: send and receive ordinary and parcel post.
(e) Public transport: bus, metro, tram and similar.
(f) Primary health care services: general practitioner, primary health centre or similar.
(g) Compulsory schools: if more than one child in the household is in compulsory school the respondent should refer to the one with the most difficulty.
4. Overall satisfaction with dwelling
(a) Overall satisfaction with dwelling: the variable refers to the respondent’s opinion/feeling about the degree of satisfaction with the dwelling in terms of meeting the household needs/opinion on the price, space, neighbourhood, distance to work, quality and other aspects.
5. Change of dwelling
(a) Family-related reasons: change in marital/partnership status. To establish own household. To follow partner/parents. To obtain better school or care facilities for children or other dependants.
(b) Employment-related reasons: start new job or transfer of existing job. Looking for work or made redundant. To be closer to work/easier to commute. Retirement.
(c) Housing-related reasons: desire to change accommodation or tenure status. Wanting new or better house/apartment. Seeking better neighbourhood/less crime.
(d) Eviction/distraint: forced to move for legal reasons.
(e) Landlord did not prolong the contract: non renewal of contract, short-term contract.
(f) Financial reasons: problems paying rent/mortgage.
(g) Other reasons: to attend or leave college/university, health and other reasons.
(h) The reference period is "the last two years". If there have been several changes of dwelling, the main reason for the most recent change should be given.
5. Transmission of data to Eurostat
The target secondary variables on "housing conditions" will be sent to Eurostat in the household data file (H) after the target primary variables.
AREAS AND LIST OF TARGET VARIABLES
Module 2007 Housing Conditions
Variable name | Code | Target variable |
Shortage of space in dwelling
MH010 | | Shortage of space in dwelling |
1 | Yes |
2 | No |
MH010_F | 1 | Variable is filled |
-1 | Missing |
Dwelling installations and facilities
MH020 | | Adequate electrical installations |
1 | Yes |
2 | No |
MH020_F | 1 | Variable is filled |
-1 | Missing |
-2 | N/a (No electricity/installations) |
MH030 | | Adequate plumbing/water installations |
1 | Yes |
2 | No |
MH030_F | 1 | Variable is filled |
-1 | Missing |
-2 | N/a (No running water/installations) |
MH040 | | Dwelling equipped with heating facilities |
1 | Yes — Central heating or similar |
2 | Yes — Other fixed heating |
3 | No — No fixed heating |
MH040_F | 1 | Variable is filled |
-1 | Missing |
MH050 | | Dwelling comfortably warm during winter time |
1 | Yes |
2 | No |
MH050_F | 1 | Variable is filled |
-1 | Missing |
MH060 | | Dwelling equipped with air conditioning facilities |
1 | Yes |
2 | No |
MH060_F | 1 | Variable is filled |
-1 | Missing |
MH070 | | Dwelling comfortably cool during summer time |
1 | Yes |
2 | No |
MH070_F | 1 | Variable is filled |
-1 | Missing |
Overall satisfaction with dwelling
MH080 | | Overall satisfaction with dwelling |
1 | Very dissatisfied |
2 | Somewhat dissatisfied |
3 | Satisfied |
4 | Very satisfied |
MH080_F | 1 | Variable is filled |
-1 | Missing |
Accessibility of Basic Services
MH090 | | Accessibility of grocery services |
1 | With great difficulty |
2 | With some difficulty |
3 | Easily |
4 | Very easily |
MH090_F | 1 | Variable is filled |
-1 | Missing |
-2 | N/a (Not used by household) |
MH100 | | Accessibility of banking services |
1 | With great difficulty |
2 | With some difficulty |
3 | Easily |
4 | Very easily |
MH100_F | 1 | Variable is filled |
-1 | Missing |
-2 | N/a (Not used by household) |
MH110 | | Accessibility of postal services |
1 | With great difficulty |
2 | With some difficulty |
3 | Easily |
4 | Very easily |
MH110_F | 1 | Variable is filled |
-1 | Missing |
-2 | N/a (Not used by household) |
MH120 | | Accessibility of public transport |
1 | With great difficulty |
2 | With some difficulty |
3 | Easily |
4 | Very easily |
MH120_F | 1 | Variable is filled |
-1 | Missing |
-2 | N/a (Not used by household) |
Accessibility of basic services
MH130 | | Accessibility of primary heath care services |
1 | With great difficulty |
2 | With some difficulty |
3 | Easily |
4 | Very easily |
MH130_F | 1 | Variable is filled |
-1 | Missing |
-2 | N/a (Not used by household) |
MH140 | | Accessibility of compulsory school |
1 | With great difficulty |
2 | With some difficulty |
3 | Easily |
4 | Very easily |
MH140_F | 1 | Variable is filled |
-1 | Missing |
-2 | N/a (No child in compulsory school) |
Change of dwelling
MH150 | | Change of dwelling |
1 | Yes |
2 | No |
MH150_F | 1 | Variable is filled |
-1 | Missing |
MH160 | | Main reason for change of dwelling |
1 | Family related reasons |
2 | Employment related reasons |
3 | Housing related reasons |
4 | Eviction/distrain |
5 | Landlord did not prolong the contract |
6 | Financial reasons |
7 | Other |
MH160_F | 1 | Variable is filled |
-1 | Missing |
-2 | N/a (MH150 not = 1) |
--------------------------------------------------
