Action brought on 3 February 2006 — Tolios and Others v Court of Auditors
Parties
Applicants: Iraklis Tolios (Paris, France), François Muller (Strasbourg, France) and Odette Perron (La Rochelle, France) (represented by: G. Vandersanden and L. Levi, lawyers)
Defendant: Court of Auditors
Form of order sought
The applicants claim that the Court should:
- declare the action admissible and well founded, including the objection of illegality contained therein;
- in consequence, annul the applicants' pension slips for March 2005, the effect of which will be to apply a weighting at the level of the capital of their country of residence or, at the very least, a weighting capable of adequately reflecting the differences in the cost of living in the places where the applicants are deemed to incur their expenditure and therefore consistent with the principle of equivalence;
- order the defendant to pay the costs.
Pleas in law and main arguments
The applicants in the present case are all officials who retired before 1 May 2004. They contest the transitional regime put in place, pending the abolition of weightings, by Council Regulation (EC, Euratom) No 723/2004 of 22 March 2004 amending the Staff Regulations of officials of the European Communities and the Conditions of Employment of other servants of the European Communities [1], in so far as that regime is based on new "pension" weightings which are no longer calculated by reference to the capital but according to the average cost of living in the Member State in which the pensioner shows that he has established his principal residence.
In support of their claims, the applicants maintain, first of all, that the regulation is based on incorrect reasoning, in so far as neither the deepened integration of the Community, nor freedom of movement and residence, nor the difficulty in monitoring the actual place of residence of pensioners can serve as a basis for the transitional regime in question.
The applicants further maintain that there has been a breach in the present case of the principles of equal treatment, legal certainty, the retroactivity of acquired rights and the protection of legitimate expectations.
[1] OJEU 2004 L 124 of 27.04.2004, p. 1.
--------------------------------------------------
