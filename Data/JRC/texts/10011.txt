Commission Regulation (EC) No 1483/2006
of 6 October 2006
opening standing invitations to tender for the resale on the Community market of cereals held by the intervention agencies of the Member States
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1784/2003 of 29 September 2003 on the common organisation of the market in cereals [1], and in particular Article 6 and the second paragraph of Article 24 thereof,
Whereas:
(1) Under Commission Regulation (EEC) No 2131/93 of 28 July 1993 laying down the procedures and conditions for the sale of cereals held by intervention agencies [2], cereals held by intervention agencies are to be sold by tendering procedure at prices preventing market disturbance.
(2) The Member States have intervention stocks of maize, common wheat, barley and rye. To meet market needs, these stocks of cereals should be made available on the Community market. To this end, standing invitations to tender should be opened for the resale on the Community market of cereals held by the intervention agencies of the Member States. Each of them should be considered to be a separate invitation to tender.
(3) Derogations should be made from the terms laid down by Regulation (EEC) No 2131/93 as regards the level of the security required. This security should be set at a sufficiently high level.
(4) To take account of the situation on the Community market, provision should be made for the Commission to manage this invitation to tender. In addition, provision must be made for an award coefficient for tenders offering the minimum selling price.
(5) To permit the efficient administration of the system, the information required by the Commission should be sent by electronic mail. It is important for the notification by the intervention agencies to the Commission to maintain the anonymity of the tenderers.
(6) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Cereals,
HAS ADOPTED THIS REGULATION:
Article 1
The intervention agencies of the Member States listed in Annex I shall open standing invitations to tender for the sale on the Community market of cereals held by them. The maximum quantities of the different cereals covered by these invitations to tender are shown in Annex I.
Article 2
The sales referred to in Article 1 shall be carried out under the terms laid down by Regulation (EEC) No 2131/93. However, notwithstanding the second subparagraph of Article 13(4) of that Regulation, the tender security shall be set at EUR 10 per tonne.
Article 3
1. The closing date for the submission of tenders for the first partial invitation to tender shall be 13.00 (Brussels time) on 11 October 2006.
The closing dates for the submission of tenders for subsequent partial invitations to tender shall be each Wednesday at 13.00 (Brussels time), with the exception of 1 November 2006, 27 December 2006, 4 April 2007 and 16 May 2007, i.e. weeks when no invitation to tender shall be made.
The closing date for the submission of tenders for the last partial invitation to tender shall be 27 June 2007 at 13.00 (Brussels time).
2. Tenders must be lodged with the intervention agencies concerned at the addresses shown in Annex I.
Article 4
Within four hours of the expiry of the deadline for the submission of tenders laid down in Article 4(1), the intervention agencies concerned shall notify the Commission of tenders received. If no tenders are received, the Member State shall notify the Commission within the same time limits. If the Member State does not send a communication to the Commission within the given deadlines, the Commission shall consider that no tender has been submitted in the Member State concerned.
The communications referred to in the first subparagraph shall be sent electronically, in accordance with the model in Annex II. A separate form for each type of cereal shall be sent to the Commission for each invitation to tender. The tenderers shall not be identified.
Article 5
1. Under the procedure laid down in Article 25(2) of Regulation (EC) No 1784/2003, the Commission shall set the minimum selling price for each cereal or decide not to award any quantities.
2. If the fixing of a minimum price, in accordance with paragraph 1, would lead to an overrun on the maximum quantity available to a Member State, an award coefficient may be fixed at the same time for the quantities offered at the minimum price in order to comply with the maximum quantity available to that Member State.
Article 6
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 6 October 2006.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 270, 21.10.2003, p. 78. Regulation as amended by Commission Regulation (EC) No 1154/2005 (OJ L 187, 19.7.2005, p. 11).
[2] OJ L 191, 31.7.1993, p. 76. Regulation as last amended by Regulation (EC) No 749/2005 (OJ L 126, 19.5.2005, p. 10).
--------------------------------------------------
ANNEX I
LIST OF INVITATIONS TO TENDER
"—" means no intervention stock of this cereal in this Member State.
Member State | Quantities made available for sale on the Community market (tonnes) | Intervention Agency Name, address and contact details |
Common wheat | Barley | Maize | Rye |
Belgique/België | 0 | 0 | — | — | Bureau d'intervention et de restitution belge/Belgisch Interventie- en Restitutiebureau Rue de Trèves, 82/Trierstraat 82 B-1040 Bruxelles/Brussel Tél./Tel. (32-2) 287 24 78 Fax (32-2) 287 25 24 E-mail: webmaster@birb.be |
Česká republika | 0 | 0 | 0 | — | Státní zemědělský intervenční fond Odbor rostlinných komodit Ve Smečkách 33 CZ-110 00 Praha 1 Tel.: (420) 222 87 16 67/14 03 Fax: (420) 296 80 64 04 E-mail: dagmar.hejrovska@szif.cz |
Danmark | 0 | 0 | 0 | — | Direktoratet for FødevareErhverv Nyropsgade 30 DK-1780 København V Tlf. (45) 33 95 88 07 Fax (45) 33 95 80 34 E-mail: mij@dffe.dk og pah@dffe.dk |
Deutschland | 0 | 0 | 0 | 100000 | Bundesanstalt für Landwirtschaft und Ernährung Deichmanns Aue 29 D-53179 Bonn Tel.: (49-228) 68 45-37 04 Fax 1: (49-228) 68 45-39 85 Fax 2: (49-228) 68 45-32 76 E-Mail: pflanzlErzeugnisse@ble.de |
Eesti | 0 | 0 | 0 | — | Põllumajanduse Registrite ja Informatsiooni Amet Narva mnt 3, 51009 Tartu Tel: (372) 7371 200 Faks: (372) 7371 201 E-post: pria@pria.ee |
Elláda | — | — | — | — | Payment and Control Agency for Guidance and Guarantee Community Aids (O.P.E.K.E.P.E) 241, Archarnon str., GR-104 46 Athens Téléphone: (30-210) 212.4787 & 4754 Télécopieur: (30-210) 212.4791 e-mail: ax17u073@minagric.gr |
España | — | — | — | — | S. Gral. Intervención de Mercados (FEGA) Almagro 33 E-28010 Madrid Tel. (34) 913 47 47 65 Fax (34) 913 47 48 38 e-mail: sgintervencion@fega.mapa.es |
France | 0 | 0 | 0 | — | Office national interprofessionnel des grandes cultures (ONIGC) 21, avenue Bosquet F-75326 Paris Cedex 07 Tél. (33-1) 44 18 22 29 et 23 37 Fax (33-1) 44 18 20 08-33 1 44 18 20 80 e-mail: m.meizels@onigc.fr et f.abeasis@onigc.fr |
Ireland | — | 0 | 0 | — | Intervention Operations, OFI, Subsidies & Storage Division, Department of Agriculture & Food Johnstown Castle Estate, County Wexford Téléphone: 353 53 91 63400 Télécopieur: 353 53 91 42843 |
Italia | — | — | — | — | Agenzia per le erogazioni in agricoltura — AGEA Via Torino, 45 I-00184 Roma Tel. (39) 06 49 49 97 55 Fax (39) 06 49 49 97 61 E-mail: d.spampinato@agea.gov.it |
Kypros/Kibris | — | — | — | — | |
Latvija | 0 | 0 | 0 | — | Lauku atbalsta dienests Republikas laukums 2, Rīga, LV – 1981 Téléphone: (371) 702 7893 Télécopieur: (371) 702 7892 e-mail: lad@lad.gov.lv |
Lietuva | 0 | 0 | 0 | — | Lithuanian Agricultural and Food Products Market regulation Agency L. Stuokos-Gucevičiaus Str. 9-12, Vilnius, Lithuania Téléphone: (370-5) 268 50 49 Télécopieur: (370-5) 268 50 61 e-mail: info@litfood.lt |
Luxembourg | — | — | — | — | Office des licences 21, rue Philippe II Boîte postale 113 L-2011 Luxembourg Tél. (352) 478 23 70 Fax (352) 46 61 38 Télex 2 537 AGRIM LU |
Magyarország | 0 | 0 | 0 | — | Mezőgazdasági és Vidékfejlesztési Hivatal Soroksári út 22–24. H-1095 Budapest Tel.: (36-1) 219 45 76 Fax: (36-1) 219 89 05 E-mail: ertekesites@mvh.gov.hu |
Malta | — | — | — | — | |
Nederland | — | — | — | — | Dienst Regelingen Roermond Postbus 965 6040 AZ Roermond Nederland Tel. (31-475) 35 54 86 Fax (31-475) 31 89 39 E-mail: p.a.c.m.van.de.lindeloof@minlnv.nl |
Österreich | 0 | 0 | 0 | — | AMA (Agrarmarkt Austria) Dresdner Straße 70 A-1200 Wien Tel.: (43-1) 331 51-258 (43-1) 331 51-328 Fax: (43-1) 331 51-46 24 (43-1) 331 51-44 69 E-Mail: referat10@ama.gv.at |
Polska | 0 | 0 | 0 | — | Agencja Rynku Rolnego Biuro Produktów Roślinnych Ul. Nowy Świat 6/12 PL-00-400 Warszawa Tel.: (48) 22 661 78 10 Faks: (48) 22 661 78 26 e-mail: cereals-intervention@arr.gov.pl |
Portugal | — | — | — | — | Instituto Nacional de Intervenção e Garantia Agrícola (INGA) R. Castilho, n.o 45-51 1269-163 Lisboa Téléphone: (351) 21 751 85 00 (351) 21 384 60 00 Télécopieur: (351) 21 384 61 70 e-mail: inga@inga.min-agricultura.pt edalberto.santana@inga.min-agricultura.pt |
Slovenija | — | — | — | — | Agencija Republike Slovenije za kmetijske trge in razvoj podeželja Dunajska 160, 1000 Ljubjana Tel. (386-1) 580 76 52 Faks (386-1) 478 92 00 E-pošta: aktrp@gov.si |
Slovensko | 0 | 0 | 0 | — | Pôdohospodárska platobná agentúra Oddelenie obilnín a škrobu Dobrovičova 12 815 26 Bratislava Slovenská republika Tel.: (421-2) 58 24 32 71 Fax: (421-2) 53 41 26 65 e-mail: jvargova@apa.sk |
Suomi/Finland | 0 | 0 | 0 | — | Maa- ja metsätalousministeriö (MMM) Interventioyksikkö – Intervention Unit Malminkatu 16/Jord- och Skogsbruksministeriet – Intervention Unit FIN-00100 Helsinki/Helsingfors PL 30 FIN-00023 Valtioneuvosto/Statsrådet Puhelin/Telefon (358-9) 160 01 Faksi/Fax (358-9) 16 05 27 72 (358-9) 16 05 27 78 Sähköposti: intervention.unit@mmm.fi |
Sverige | 0 | 0 | 0 | — | Jordbruksverket S-551 82 Jönköping Tfn (46-36) 15 50 00 Fax (46-36) 19 05 46 E-post: jordbruksverket@sjv.se |
United Kingdom | — | 0 | 0 | — | Rural Payments Agency Lancaster House Hampshire Court Newcastle upon Tyne NE4 7YH Téléphone: (44) 191 226 5882 Télécopieur: (44)191 226 5824 e-mail: cerealsintervention@rpa.gov.uk |
--------------------------------------------------
ANNEX II
Communication to the Commission of tenders received under the standing invitation to tender for the resale on the Community market of cereals from intervention stocks
Model [1]
Article 5 of Regulation (EC) No 1483/2006
"TYPE OF CEREAL: CN code [2]"
"MEMBER STATE [3]"
Indicate the total quantities offered (including rejected offers made for the same lot): … tonnes.
1 | 2 | 3 | 4 |
Serial numbers of tenderers | Lot No | Quantity (t) | Tender price EUR/tonne |
1 | | | |
2 | | | |
3 | | | |
etc. | | | |
[1] To be sent to DG AGRI, Unit D.2.
[2] 100190 for common wheat, 100300 for barley, 10059000 for maize and 10020000 for rye.
[3] Indicate the Member State concerned.
--------------------------------------------------
