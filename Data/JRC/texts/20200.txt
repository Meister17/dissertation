COUNCIL DECISION of 27 March 1995 concerning the conclusion of the Cooperation Agreement between the European Community and the Democratic Socialist Republic of Sri Lanka on Partnership and Development (95/129/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Articles 113 and 130y, in conjunction with the first sentence of Article 228 (2) and the first subparagraph of paragraph 3 thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Whereas, under Article 130u of the Treaty, Community policy in the sphere of development cooperation should foster the sustainable economic and social development of the developing countries, their smooth and gradual integration into the world economy and the campaign against poverty in those countries;
Whereas the Community should approve, for the attainment of its aims in the sphere of external relations, the Agreement referred to in this Decision,
HAS DECIDED AS FOLLOWS:
Article 1
The Cooperation Agreement between the European Community and the Democratic Socialist Republic of Sri Lanka on Partnership and Development is hereby approved on behalf of the Community.
The text of the Agreement is attached to this Decision.
Article 2
The President of the Council shall, on behalf of the Community, give the notification provided for in Article 26 of the Agreement.
Article 3
The Commission, assisted by representatives of the Member States, shall represent the Community in the Joint Commission provided for in Article 20 of the Agreement.
Article 4
This Decision shall be published in the Official Journal of the European Communities.
Done at Brussels, 27 March 1995.
For the Council
The President
M. GIRAUD
(1) OJ No C 86, 23. 3. 1994, p. 5.
(2) OJ No C 18, 23. 1. 1995.
