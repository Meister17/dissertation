Commission Regulation (EC) No 1561/2005
of 23 September 2005
determining the extent to which applications lodged in September 2005 for import licences for certain poultrymeat products under the regime provided for in Council Regulation (EC) No 774/94 opening and providing for the administration of certain Community tariff quotas for poultrymeat and certain other agricultural products can be accepted
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 1431/94 of 22 June 1994, laying down detailed rules for the application in the poultrymeat sector of the import arrangements provided for in Council Regulation (EC) No 774/94 opening and providing for the administration of certain Community tariff quotas for poultrymeat and certain other agricultural products [1] and in particular Article 4(4) thereof,
Whereas:
HAS ADOPTED THIS REGULATION:
Article 1
Applications for import licences for the period 1 October to 31 December 2005 submitted pursuant to Regulation (EC) No 1431/94 shall be met as referred to in the Annex to this Regulation.
Article 2
This Regulation shall enter into force on 1 October 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 23 September 2005.
For the Commission
J. M. Silva Rodríguez
Director-General for Agriculture and Rural Development
[1] OJ L 156, 23.6.1994, p. 9. Regulation as last amended by Regulation (EC) No 1043/2001 (OJ L 145, 31.5.2001, p. 24).
--------------------------------------------------
ANNEX
Group No | Percentage of acceptance of import certificates submitted for the period 1 October to 31 December 2005 |
1 | 1,240707 |
2 | 100,0 |
3 | 1,273885 |
4 | 1,694915 |
5 | 2,392344 |
--------------------------------------------------
