COUNCIL DIRECTIVE 98/92/EC of 14 December 1998 amending Directive 70/524/EEC concerning additives in feedingstuffs and Directive 95/69/EC laying down the conditions and arrangements for approving and registering certain establishments and intermediaries operating in the animal feed sector
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 43 thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas Article 14 of Council Directive 95/69/EC (4) states that the Council is to adopt the amounts of the fees to be charged for the approval of establishments and their intermediaries;
Whereas Article 6 of Council Directive 70/524/EEC of 23 November 1970 concerning additives in feedingstuffs (5) states that a fee may be charged by the Member State acting as rapporteur for the examination of dossiers relating to Community authorisation of additives for feedingstuffs; whereas the Council is to fix the amount of that fee;
Whereas an examination of the funding of the services concerned in individual Member States has revealed that the fixing of the amount of the fees at Community level would be a disproportionately far-reaching intervention in the existing systems for levying fees operated by the Member States; whereas, in addition, the costs incurred by the Member States in providing such services vary very greatly, particularly because of the wide differences in labour costs;
Whereas, to avoid distortion of competition, provision should nevertheless be made that the Council has to decide on harmonised rules for the calculation of the level of the fees;
Whereas the relevant provisions in Directives 70/524/EEC and 95/69/EC should be amended accordingly,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Directive 70/524/EEC shall be amended as follows:
Article 6(2) shall be replaced by the following:
'2. Before 1 April 1999, the Council, acting by a qualified majority on a proposal from the Commission, shall adopt rules for the calculation of the level of the fee referred to in paragraph 1.`
Article 2
Directive 95/69/EC shall be amended as follows:
Article 14 shall be replaced by the following:
'Article 14
The Council, acting by a qualified majority on a proposal from the Commission, shall, before 1 April 1999, adopt rules for calculating the level of the fee to be charged for the approval of establishments and their intermediaries.`
Article 3
1. Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive by 31 March 1999. They shall forthwith inform the Commission thereof.
When Member States adopt these measures, they shall contain a reference to this Directive or shall be accompanied by such reference on the occasion of their official publication. The methods of making such reference shall be laid down by Member States.
2. Member States shall communicate to the Commission the text of the main provisions of domestic law which they adopt in the field governed by this Directive.
Article 4
This Directive shall enter into force on the day of its publication in the Official Journal of the European Communities.
Article 5
This Directive is addressed to the Member States.
Done at Brussels, 14 December 1998.
For the Council
The President
W. MOLTERER
(1) OJ C 155, 20. 5. 1998, p. 29.
(2) OJ C 292, 21. 9. 1998.
(3) OJ C 284, 14. 9. 1998, p. 91.
(4) OJ L 332, 30. 12. 1995, p. 15.
(5) OJ L 270, 14. 12. 1970, p. 1. Directive as last amended by Directive 98/19/EC (OJ L 96, 28. 3. 1998, p. 39).
