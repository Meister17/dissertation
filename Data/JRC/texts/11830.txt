Judgment of the Court
(Third Chamber)
of 12 January 2006
in Case C-311/04: Reference for a preliminary ruling from the Gerechtshof te Amsterdam in Algemene Scheeps Agentuur Dordrecht BV
v Inspecteur der Belastingdienst — Douanedistrict Rotterdam [1]
In Case C-311/04: Reference for a preliminary ruling under Article 234 EC from the Gerechtshof te Amsterdam (Amsterdam Regional Court of Appeal) (Netherlands), made by decision of 28 June 2004, received at the Court on 22 July 2004, in the proceedings Algemene Scheeps Agentuur Dordrecht BV v Inspecteur der Belastingdienst — Douanedistrict Rotterdam — the Court (Third Chamber), composed of A. Rosas, President of the Chamber, J. Malenovský (Rapporteur), J.-P. Puissochet, S. von Bahr and U. Lõhmus, Judges; J. Kokott, Advocate General; R. Grass, Registrar, gave a judgment on 12 January 2006, in which it ruled as follows:
[1] OJ C 239, 25.09.2004.
--------------------------------------------------
