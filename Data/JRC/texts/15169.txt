Council Decision
of 5 October 2006
amending Annex 2, Schedule A, to the Common Consular Instructions on the visa requirements for holders of Indonesian diplomatic and service passports
(2006/684/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to Council Regulation (EC) No 789/2001 of 24 April 2001 reserving to the Council implementing powers with regard to certain detailed provisions and practical procedures for examining visa applications [1],
Having regard to the initiative of Belgium, Luxembourg and the Netherlands,
Whereas:
(1) Annex 2, Schedule A, to the Common Consular Instructions contains the list of countries whose nationals are not subject to a visa requirement in one or more Schengen States when they are holders of diplomatic, official or service passports, but who are subject to this requirement when they are holders of ordinary passports.
(2) Belgium, Luxembourg and the Netherlands wish to exempt holders of Indonesian diplomatic and service passports from visa requirements. The Common Consular Instructions should therefore be amended accordingly.
(3) In accordance with Articles 1 and 2 of the Protocol on the position of Denmark annexed to the Treaty on European Union and to the Treaty establishing the European Community, Denmark is not taking part in the adoption of this Decision, and is not bound by it or subject to its application. Given that this Decision builds upon the Schengen acquis under the provisions of Title IV of Part Three of the Treaty establishing the European Community, Denmark shall, in accordance with Article 5 of the said Protocol, decide within a period of six months after the Council has adopted this Decision whether it will implement it in its national law.
(4) As regards Iceland and Norway, this Decision constitutes a development of provisions of the Schengen acquis within the meaning of the Agreement concluded by the Council of the European Union and the Republic of Iceland and the Kingdom of Norway concerning the association of those two States with the implementation, application and development of the Schengen acquis [2], which fall within the area referred to in Article 1(A) of Council Decision 1999/437/EC of 17 May 1999 [3] on certain arrangements for the application of that Agreement.
(5) This Decision constitutes a development of provisions of the Schengen acquis in which the United Kingdom does not take part, in accordance with Council Decision 2000/365/EC of 29 May 2000 concerning the request of the United Kingdom of Great Britain and Northern Ireland to take part in some of the provisions of the Schengen acquis [4]; the United Kingdom is therefore not taking part in its adoption and is not bound by it or subject to its application.
(6) This Decision constitutes a development of provisions of the Schengen acquis in which Ireland does not take part, in accordance with Council Decision 2002/192/EC of 28 February 2002 concerning Ireland’s request to take part in some of the provisions of the Schengen acquis [5]; Ireland is therefore not taking part in its adoption and is not bound by it or subject to its application.
(7) As regards Switzerland, this Decision constitutes a development of the provisions of the Schengen acquis within the meaning of the Agreement signed between the European Union, the European Community and the Swiss Confederation, concerning the Swiss Confederation’s association with the implementation, application and development of the Schengen acquis, which fall within the area referred to in Article 4(1) of Council Decisions 2004/849/EC [6] and 2004/860/EC [7] on the signing on behalf of the European Union, and on behalf of the European Community, and on the provisional application of certain provisions of that Agreement.
(8) This Decision constitutes an act building upon the Schengen acquis or otherwise related to it within the meaning of Article 3(2) of the 2003 Act of Accession,
HAS ADOPTED THIS DECISION:
Article 1
Annex 2, Schedule A, to the Common Consular Instructions is hereby amended as follows:
Indonesia shall be added and the letters "DS" shall be inserted against it in the "BNL" column.
Article 2
This Decision shall apply from 1 November 2006.
Article 3
This Decision is addressed to the Member States in accordance with the Treaty establishing the European Community.
Done at Luxembourg, 5 October 2006.
For the Council
The President
K. Rajamäki
[1] OJ L 116, 26.4.2001, p. 2.
[2] OJ L 176, 10.7.1999, p. 36.
[3] OJ L 176, 10.7.1999, p. 31.
[4] OJ L 131, 1.6.2000, p. 43.
[5] OJ L 64, 7.3.2002, p. 20.
[6] OJ L 368, 15.12.2004, p. 26.
[7] OJ L 370, 17.12.2004, p. 78.
--------------------------------------------------
