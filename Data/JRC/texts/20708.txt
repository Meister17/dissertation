COMMISSION DIRECTIVE 94/38/EC of 26 July 1994 amending Annexes C and D to Council Directive 92/51/EEC on a second general system for the recognition of professional education and training to supplement Directive 89/48/EEC
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 92/51/EEC of 18 June 1992 on a second general system for the recognition of professional education and training to supplement Directive 89/48/EEC (1), and in particular Article 15 thereof,
Whereas, when examining a reasoned request for adding an education or training course to the list in Annex C or D or for removing such a course from one of those Annexes, the Commission, pursuant to Article 15 (2) of Directive 92/51/EEC, is required to verify in particular whether the qualification resulting from the course in question confers on the holder a level of professional education or training of a comparably high level to that of the post-secondary course referred to in point (i) of the second indent of the first subparagraph of Article 1 (a), and a similar level of responsibility and activity;
Whereas the German Government has made reasoned requests for the amendment of Annexes C and D to the Directive and the Italian Government has made a reasoned request for the amendment of Annex C;
Whereas, in particular, the reference to the professional title of physiotherapist ('Krankengymnast(in)') in Germany needs to be amended following a change in national legislation which has introduced ane professional title without, however, altering the structure of the professional education and training;
Whereas, in particular, the training courses to be added to the list in Annex C in respect of Germany have the same structure as those already listed, in respect of Germany, Italy and Luxembourg, at point 1 ('Paramedical and childcare training courses') in that Annex;
Whereas Italy has altered its education and training courses for accountants ('regioniere') and accountancy experts ('perito commerciale'), with the result that those courses now come under Council Directive 89/48/EEC (2); whereas, in the case of work consultants ('consulenti del lavoro'), the course of education and training covered by Directive 89/48/EEC is now the principal form of education and training for the profession concerned; whereas, accordingly, education and training courses for those two professions should no longer be included in Annex C, since holders of qualifications covered by Directive 92/51/EEC could, by virtue of Article 1 (a) of Directive 89/48/EEC, apply to be treated in the same way;
Whereas in accordance with Article 2 of Directive 92/51/EEC the provisions of that Directive are not applicable to activities covered by any of the Directives listed in Annex A thereto, including the Directives made applicable to the pursuit as an employed person of the activities listed in Annex B, even if a national of a Member State has completed one of the 'courses having a special structure' referred to in Annex D;
Whereas, in particular, the training courses to be added to the list in Annex D in respect of Germany are similar in structure to certain training courses in Annex C and invariably feature a total duration of 13 years or more, including three years or more of vocational training;
Whereas, in accordance with Article 17 (2) of Directive 92/51/EEC and in order to increase the effectiveness of the general system, those Member States whose education and training courses are listed in Annex D should send a list of the diplomas concerned to the Commission;
Whereas, in order to make Annexes C and D easier to read, the lists as amended should be published;
Whereas the measures provided for in this Directive are in accordance with the opinion given by the Committee established by Article 15 of Directive 92/51/EEC,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Annexes C and D to Directive 92/51/EEC are hereby amended as shown in Annex I hereto.
Article 2
The amended lists of the courses of education and training which appear in Annexes C and D to Directive 92/51/EEC are shown in Annex II to this Directive.
Article 3
1. Member States shall adopt the laws, regulations and administrative provisions necessary for them to comply with this Directive before 1 October 1994. They shall forthwith inform the Commission thereof.
When Member States adopt these provisions, these shall contain a reference to this Directive or shall be accompanied by such reference at the time of their official publication. The procedure for such reference shall be adopted by Member States.
2. Member States shall communicate to the Commission the texts of the main provisions of national law which they adopt in the field governed by this Directive.
Article 4
This Directive shall enter into force on the 20th day following its publication in the Official Journal of the European Communities.
Done at Brussels, 26 July 1994.
For the Commission
Raniero VANNI D'ARCHIRAFI
Member of the Commission
(1) OJ No L 209, 24. 7. 1992, p. 25.
(2) OJ No L 19, 24. 1. 1989, p. 16.
ANNEX I
1. Annex C is amended as follows:
1. At '1. Paramedical and childcare training courses', under the heading 'In Germany':
(a) the second indent is amended to read:
'- physiotherapist ("Krankengymnast(in)/Physiotherapeut(in)" (1))';
(b) the following indents are added:
'- medical laboratory technician ("medizinisch-technische(r) Laboratoriums-Assistent(in)"),
- medical X-ray technician ("medizinisch-technische(r) Radiologie-Assistent(in)"),
- medical functional diagnostics technician ("medizinisch-technische(r) Assistent(in) fuer Funktionsdiagnostik"),
- veterinary technician ("veterinaermedizinisch-technische(r) Assistent(in)"),
- dietitian ("Diaetassistent(in)"),
- pharmacy technician ("Pharmazieingenieur") received prior to 31 March 1994 in the former German Democratic Republic or in the territory of the new Laender,
- psychiatric nurse ("Psychiatrische(r) Krankenschwester/Krankenpfleger"),
- speech therapist ("Sprachtherapeut(in)")'.
2. At '4. Technical sector', under 'In Italy':
- the third indent, 'accountant ("ragioniere"), and accountancy expert ("perito commerciale"),' is deleted,
- the fourth indent, 'work consultants ("consulente del lavoro"),' is deleted,
- the sixth indent is amended to read as follows:
'- for land surveyors, by the completion of a practical traineeship lasting at least two years.'
2. Annex D is supplemented by the addition of the following section:
'In Germany:
the following regulated courses,
- regulated courses preparatory to the pursuit of the professions of technical assistant ("technische(r) Assistent(in)"), commercial assistant ("kaufmaennische(r) Assistent(in)"), social professions ("soziale Berufe") and the profession of State-certified respiration and elocution instructor ("staatlich gepruefte(r) Atem-, Sprech- und Stimmlehrer(in)"), of a total duration of at least 13 years, which require successful completion of the secondary course of education ("mittlerer Bildungsabschluss") and which comprise:
- at least three years (2) of vocational training at a specialized school ("Fachschule") culminating in an examination and, where applicable, supplemented by a one-year or two-year specialization course also culminating in an examination,
- or at least two and a half years at a specialized school ("Fachschule") culminating in an examination and supplemented by work experience having a duration of not less than six months or a traineeship of not less than six months in an approved establishment,
- or at least two years at a specialized school ("Fachschule") culminating in an examination and supplemented by work experience having a duration of not less than one year or a traineeship of not less than one year in an approved establishment,
- regulated courses for the professions of State-certified ("staatlich gepruefte(r)") technician ("Techniker(in)"), business economist ("Betriebswirt(in)"), designer ("Gestalter(in)") and family assistant ("Familienpfleger(in)"), of a total duration of not less than 16 years, a prerequisite of which is successful completion of compulsory schooling or equivalent education and training (of a duration of not less than nine years) and successful completion of a course at a trade school ("Berufsschule") of not less than three years, and comprising, upon completion of at least two years of work experience, full-time education and training having a duration of not less than two years or part-time education and training of equivalent duration,
- regulated courses and regulated in-service training, of a total duration of not less than 15 years, a prerequisite of which is, generally speaking, successful completion of compulsory schooling (of a duration of not less than nine years) and of vocational training (noramlly three years) and generally comprising at least two years of work experience (three years in most cases) and an examination as part of in service training, preparation for which generally entails a training course which either runs concurrently with the work experience (at least 1 000 hours) or is attended on a full-tiume basis (at least one year).
The German authorities shall send to the Commission and to the other Member States a list of the training courses covered by this Annex.'
(1) As from 1 June 1994, the professional title 'Krankengymnast(in)' was replaced by that of 'Physiotherapeut(in)'. Nevertheless, the members of the profession who have obtained their diplomas before that date may, if they wish, continue to use the former title of 'Krankengymnast(in)'.
(2) The minimum duration may be reduced from three years if the person concerned has the qualification required to enter university ('Abitur'), i.e. 13 years of prior education and training, or the qualification needed to enter a 'Fachhochschule' (a 'Fachhochschulreife'), i.e. 12 years of prior education and training.
ANNEX II
LIST OF COURSES HAVING A SPECIAL STRUCTURE AS REFERRED TO IN POINT (ii) OF THE SECOND INDENT OF THE FIRST SUBPARAGRAPH OF ARTICLE 1 (a) 1. Paramedical and childcare training courses
Training for the following:
in Germany
- paediatric nurse ('Kinderkrankenschwester/Kinderkrankenpfleger'),
- physiotherapist ('Krankengymnast(in)/Physiotherapeut(in)') (1),
- occupational therapist ('Beschaeftigungs- und Arbeitstherapeut(in)'),
- speech therapist ('Logopaede/Logopaedin'),
- orthoptist ('Orthoptist(in)'),
- State-recognized childcare worker ('Staatlich anerkannte(r) Erzieher(in)'),
- State-recognized remedial teacher ('Staatlich anerkannte(r) Heilpaedagoge(-in)'),
- medical laboratory technician ('medizinisch-technische(r) Laboratoriums-Assistent(in)'),
- medical X-ray technician ('medizinisch-technische(r) Radiologie-Assistent(in)'),
- medical functional diagnostics technician ('medizinisch-technische(r) Assistent(in) fuer Funktionsdiagnostik'),
- veterinary technician ('veterinaermedizinisch-technische(r) Assistent(in)'),
- dietitian ('Diaetassistent(in)'),
- pharmacy technician ('Pharmazieingenieur') received prior to 31 March 1994 in the former German Democratic Republic or in the territory of the new Laender,
- psychiatric nurse ('Psychiatrische(r) Krankenschwester/Krankenpfleger'),
- speech therapist ('Sprachtherapeut(in)'),
in Italy
- dental technician ('odontotecnico'),
- optician ('ottico'),
- chiropodist ('podologo'),
in Luxembourg
- medical X-ray technician (assistant(e) technique médical(e) en radiologie),
- medical laboratory technician (assistant(e) technique médical(e) de laboratoire),
- psychiatric nurse (infirmier/ière psychiatrique),
- medical technician - surgery (assistant(e) technique médical(e) en chirurgie),
- paediatric nurse (infirmier/ière puériculteur/trice),
- nurse - anaesthetics (infirmier/ière anesthésiste),
- qualified masseur/masseuse (masseur/euse diplômé(e)),
- childcare worker (éducateur/trice),
which represent education and training courses of a total duration of at least 13 years, comprising:
- either at least three years of vocational training in a specialized school culminating in an examination, in some cases supplemented by a one or two-year specialization course culminating in an examination,
- or at least two and a half years of vocational training in a specialized school culminating in an examination and supplemented by work experience of at least six months or by a traineeship of at least six months in an approved establishment,
- or at least two years of vocational training in a specialized school culminating in an examination and supplemented by work experience of at least one year or by a traineeship of at least one year in an approved establishment.
2. Master craftsman sector ('Mester/Meister/Maître'), which represents education and training courses concerning skills not covered by the Directives listed in Annex A
Training for the following:
in Denmark
- optician ('optometrist'),
this course is of a total duration of 14 years, including five years' vocational training divided into two and a half years' theoretical training provided by the vocational training establishment and two and half years' practical training received in the workplace, and culminating in a recognized examination relating to the craft and conferring the right to use the title 'Mester',
- orthopaedic technician ('ortopaedimekaniker'),
this course is of a total duration of 12,5 years, including three and a half years' vocational training divided into six months' theoretical training, provided by the vocational training establishment and three years' practical training received in the workplace, and culminating in a recognized examination relating to the craft and conferring the right to use the title 'Mester',
- orthopaedic boot and shoemaker ('ortopaediskomager'),
this course is of a total duration of 13,5 years, including four and a half years' vocational training divided into two years' theoretical training provided by the vocational training establishment and two and a half years' practical training received in the workplace, and culminating in a recognized examination relating to the craft and conferring the right to use the title 'Mester'.
Training for the following:
in Germany
- optician ('Augenoptiker'),
- dental technician ('Zahntechniker'),
- surgical truss maker ('Bandagist'),
- hearing-aid maker ('Hoergeraete-Akustiker'),
- orthopaedic technician ('Orthopaediemechaniker'),
- orthopaedic bootmaker ('Orthopaedieschuhmacher'),
in Luxembourg
- dispensing optician ('opticien'),
- dental technician ('mécanicien dentaire'),
- hearing-aid maker ('audioprothésiste'),
- orthopaedic technician/surgical truss maker ('mécanicien orthopédiste/bandagiste'),
- orthopaedic bootmaker ('orthopédiste-cordonnier').
These courses are of a total duration of 14 years, including at least five years' training followed within a structured training framework, partly received in the workplace and partly provided by the vocational training establishment, and culminating in an examination which must be passed in order to be able to practise any activity considered as skilled, either independently or as an employee with a comparable level of responsibility.
3. Seafaring sector
(a) Sea transport
Training for the following:
in Denmark
- ship's captain ('skibsfoerer'),
- first mate ('overstyrmand'),
- quartermaster, deck officer ('enestyrmand, vagthavende styrmand'),
- deck officer ('vagthavende styrmand'),
- engineer ('maskinchef'),
- first engineer ('1. maskinmester'),
- first engineer/duty engineer ('1. maskinmester/vagthavende maskinmester'),
in Germany
- captain, large coastal vessel ('Kapitaen AM'),
- captain, coastal vessel ('Kapitaen AK'),
- deck officer, large coastal vessel ('Nautischer Schiffsoffizier AMW'),
- deck officer, coastal vessel ('Nautischer Schiffsoffizier AKW'),
- chief engineer, grade C ('Schiffsbetriebstechniker CT - Leiter von Maschinenanlagen'),
- ship's mechanic, grade C ('Schiffsmaschinist CMa - Leiter von Maschinenanlagen'),
- ship's engineer, grade C ('Schiffsbetriebstechniker CTW'),
- ship's mechanic, grade C - solo engineer officer ('Schiffsmaschinist CMaW - Technischer Alleinoffizier'),
in Italy
- deck officer ('ufficiale di coperta'),
- engineer officer ('ufficiale di macchina'),
in the Netherlands
- first mate (coastal vessel) (with supplementary training) ('stuurman kleine handelsvaart (met aanvulling)'),
- coaster engineer (with diploma) ('diploma motordrijver'),
which represents training:
- in Denmark, of nine years' primary schooling followed by a course of basic training and/or service at sea of between 17 and 36 months, supplemented by:
- for the deck officer, one year of specialized vocational training,
- for the others, three years of specialized vocational training,
- in Germany, of a total duration of between 14 and 18 years, including a three-year course of basic vocational training and one year's service at sea, followed by one or two years of specialized vocational training supplemented, where appropriate, by two years' work experience in navigation,
- in Italy, of a total duration of 13 years, of which at least five years consist of professional training culminating in an examination and are supplemented, where appropriate, by a traineeship,
- in the Netherlands, involving a course of 14 years, at least two years of which take place in a specialized vocational training establishment, supplemented by a twelve-month traineeship,
and which are recognized under the International STCW Convention (International Convention on Standards of Training, Certification and Watchkeeping for Seafarers, 1978).
(b) Sea fishing
Training for the following:
in Germany
- captain, deep-sea fishing ('Kapitaen BG/Fischerei'),
- captain, coastal fishing ('Kapitaen BK/Fischerei'),
- deck officer, deep-sea vessel ('Nautischer Schiffsoffizier BGW/Fischerei'),
- deck officer, coastal vessel ('Nautischer Schiffsoffizier BKW/Fischerei'),
in the Netherlands
- first mate/engineer V ('stuurman werktuigkundige V'),
- engineer IV (fishing vessel) ('werktuigkundige IV visvaart'),
- first mate IV (fishing vessel) ('stuurman IV visvaart'),
- first mate/engineer VI ('stuurman werktuigkundige VI'),
which represents training:
- in Germany, of a total duration of between 14 and 18 years, including a three-year course of basic vocational training and one year's service at sea, followed by one or two years of specialized vocational training supplemented, where appropriate, by two years' work experience in navigation,
- in the Netherlands, involving a course varying in duration between 13 and 15 years, at least two years of which are provided in a specialized vocational school, supplemented by a 12-month period of work experience,
and is recognized under the Torremolinos Convention (1977 International Convention for the Safety of Fishing Vessels).
4. Techmical sector
Training for the following:
in Italy
- building surveyor ('geometra'),
- land surveyor ('perito agrario'),
which represents secondary technical courses of a total duration of at least 13 years, comprising eight years' compulsory schooling followed by five years' secondary study, including three years' vocational study, culminating in the Technical Baccalaureat examination, and supplemented,
- for building surveyors by: either a traineeship lasting at least two years in a professional office, or five years' work experience,
- for land surveyors, by the completion of a practical traineeship lasting at least two years, followed by the State Examination.
Training for the following:
in the Netherlands
- bailiff ('gerechtsdeurwaarder'),
which represents a course of study and vocational training totalling 19 years, comprising eight years' compulsory schooling followed by eight years' secondary education including four years' technical education culminating in a State examination and supplemented by three years' theoretical and practical vocational training.
5. United Kingdom courses accredited as national vocational qualifications or Scottish vocational qualifications
Training for:
- medical laboratory scientific officer,
- mine electrical engineer,
- mine mechanical engineer,
- approved social worker - mental health,
- probation officer,
- dental therapist,
- dental hygienist,
- dispensing optician,
- mine deputy,
- insolvency practitioner,
- licensed conveyancer,
- prosthetist,
- first mate - freight/passenger ships - unrestricted,
- second mate - freight/passenger ships - unrestricted,
- third mate - freight/passenger ships - unrestricted,
- deck officer - freight/passenger ships - unrestricted,
- engineer officer - freight/passenger ships - unlimited trading area,
- trade mark agent,
leading to qualifications accredited as National Vocational Qualifications (NVQs) or approved or recognized as equivalent by the National Council for Vocational Qualifications or, in Scotland, accredited as Scottish Vocational Qualifications, at levels 3 and 4 of the United Kingdom National Framework of Vocational Qualifications.
These levels are defined as follows:
- level 3: competence in a broad range of varied work activities performed in a wide variety of contexts and most of which are complex and non-routine. There is considerable responsibility and autonomy, and control or guidance of others is often required,
- level 4: competence in a broad range of complex technical or professional work activities performed in a wide variety of contexts and with a substantial degreee of personal responsibility and autonomy. Responsibility for the work of others and the allocation of resources is often present.
LIST OF COURSES HAVING A SPECIAL STRUCTURE AS REFERRED TO IN THE THIRD INDENT OF POINT (b) OF THE FIRST SUBPARAGRAPH OF ARTICLE 3 In the United Kingdom
Regulated courses leading to qualifications accredited as National Vocational Qualifications (NVQs) by the National Council for Vocational Qualifications or, in Scotland, accredited as Scottish Vocational Qualifications, at levels 3 and 4 of the United Kingdom National Framework of Vocational Qualifications.
These levels are defined as follows:
- level 3: competence in a broad range of varied work activities performed in a wide variety of contexts and most of which are complex and non-routine. There is considerable responsibility and autonomy, and control or guidance of others is often required,
- level 4: competence in a broad range of complex technical or professional work activities performed in a wide variety of contexts and with a substantial degree of personal responsibility and autonomy. Responsibility for the work of others and the allocation of resources is often present.
In Germany
The following regulated courses:
- regulated courses preparatory to the pursuit of the professions of technical assistant ('technisch(r) Assistent(in)'), commercial assistant ('kaufmaennisch(r) Assistent(in)'), social professions ('soziale Berufe') and the profession of State-certified respiration and elocution instructor ('staatlich gepruefte(r) Atem-, Sprech- und Stimmlehrer(in)'), of a total duration of at least 13 years, which require successful completion of the secondary course of education ('mittlerer Bildungsabschluss') and which comprise:
- at least three years (2) of vocational training at a specialized school ('Fachschule') culminating in a examination and, where applicable, supplemented by a one-year or two-year specialization course also culminating in an examination,
- or at least two and a half years at a specialized school ('Fachschule') culminationg in an examination and supplemented by work experience of a duration of not less than six months or a traineeship of not less than six months in an approved establishment,
- or at least two years at a specialized school ('Fachschule') culminating in an examination and supplemented by work experience of a duration of not less than one year or a traineeship of not less than one year in an approved establishment,
- Regulated courses for the professions of State-certified ('staatlich gepruefte(r)') technician ('Techniker(in)'), business economist ('Betriebswirt(in)'), designer ('Gestalter(in)') and family assistant ('Familiepfleger(in)'), of a total duration not less than 16 years, a preresquisite of which is successful completion of compulsory schooling or equivalent education and training (of a duration of not less than nine years) and successful completion of a course at a trade school ('Berufsschule') of a duration of not less than three years and comprising, upon completion of at least two years of work experience, full-time education and training of a duration of not less than two years or part-time education and training of equivalent duration
- Regulated courses and regulated in-service training, of a total duration of not less than 15 years, a prerequisite of which is, generally speaking, successful completion of compulsory schooling (of a duration not less than nine years) and of vocational training (normally three years) and which generally comprise at least two years of work experience (three years in most cases) and an examination in the context of in-service training preparation for which generally comprises a training course which is either concurrent with the work experience (at least 1 000 hours) or is attended on a full-time basis (at least one year).
The German authorities shall send to the Commission and to the other Member States a list of the training courses covered by this Annex.
(1) As from 1 June 1994 the professional title 'Krankengymnast(in)' was replaced by that of 'Physiotherapeut(in)'. Nevertheless, the members of the profession who have obtained their diplomas before this date may, if they wish, continue to use the former title of 'Krankengymnast(in)'.
(2) The minimum duration may be reduced from three years to two years if the person concerned has the qualification required to enter university ('Abitur'), i.e. 13 years of prior education and training, or the qualisication needed to enter a 'Fachhochschule' (a 'Fechhochschulreife'), i.e. 12 years of prior education and training.
