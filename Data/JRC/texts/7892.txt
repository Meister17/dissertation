COUNCIL DIRECTIVE of 30 July 1964 concerning co-ordinated annual surveys of investment in industry (64/475/EEC)
THE COUNCIL OF THE EUROPEAN ECONOMIC COMMUNITY,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 213 thereof;
Having regard to the draft submitted by the Commission;
Whereas in order to carry out the tasks entrusted to it under the Treaty the Commission must have at its disposal annual statistics on the trend of investment in the industries of the Community;
Whereas in the industrial survey for 1962 comparable basic data on the essential characteristics of industry were collected for the first time ; whereas such a census cannot be carried out frequently ; whereas annual statistics concerning certain basic matters cannot however be dispensed with for the intervening years ; whereas, in this respect, investment is of such importance that the statistical offices of most Member States have been collecting data of that kind for several years;
Whereas, however, it is indispensable that such annual statistics be put at the disposal of the Commission by all Member States in co-ordinated form, as regards concept and method, so that comparable results for the six Member States may be obtained following a similar breakdown by industrial sector;
HAS ADOPTED THIS DIRECTIVE:
Article 1
The Member States, in technical co-operation with the Commission, shall take all necessary steps to carry out co-ordinated annual surveys on investment in industry and to ensure that the first survey, for the year 1964, is carried out in 1965.
Article 2
For the purposes of this Directive, an industry shall be defined by reference to the "Nomenclature of Industries in the European Communities" (NICE). 1 In the case of small undertakings the survey may be carried out by random sampling.
Article 3
The figures to be recorded shall be those in respect of annual investment expenditure (including plant constructed by the undertakings themselves), broken down as follows: (a) Machinery, plant, vehicles;
(b) Construction of buildings;
(c) Purchase of existing buildings and of land.
Investments of a social nature shall, where possible, be recorded globally in a separate return.
Article 4
Subject to any national legislation on the secrecy of statistics, the results of the surveys, broken down by industrial sector, shall be forwarded to the Commission.
Article 5
This Directive is addressed to the Member States.
Done at Brussels, 30 July 1964.
For the Council
The President
G. SCHRÖDER
1 The current edition of the Nomenclature was published in 1963 in the Bulletin of Industrial Statistics (Statistical Office of the European Communities). It is reproduced in the Annex for information.
ANNEX NOMENCLATURE OF INDUSTRIES IN THE EUROPEAN COMMUNITIES-NICE
Branches, Major Groups, Groups, Subgroups and Subdivisions
>PIC FILE= "T9001291"> >PIC FILE= "T9001292"> >PIC FILE= "T9001293"> >PIC FILE= "T9001294"> >PIC FILE= "T9001295"> >PIC FILE= "T9001296"> >PIC FILE= "T9001297"> >PIC FILE= "T9001298"> >PIC FILE= "T9001299"> >PIC FILE= "T9001300"> >PIC FILE= "T9001301"> >PIC FILE= "T9001302"> >PIC FILE= "T9001303"> >PIC FILE= "T9001304"> >PIC FILE= "T9001305"> >PIC FILE= "T9001306"> >PIC FILE= "T9001307"> >PIC FILE= "T9001308"> >PIC FILE= "T9001309"> >PIC FILE= "T9001310"> >PIC FILE= "T0019356">
