Judgment of the Court (Second Chamber) of 18 May 2006 — Commission of the European Communities
v Kingdom of Spain
(Case C-221/04) [1]
Parties
Applicant: Commission of the European Communities (represented by: G. Valero Jordana and M. van Beek, Agents)
Defendant: Kingdom of Spain (represented by: F. Díez Moreno, Agent)
Re:
Failure by a Member State to fulfil its obligations — Art. 12(1) of and Annex VI to Council Directive 92/43/EEC of 21 May 1992 on the conservation of natural habitats and of wild fauna and flora (OJ 1992 L 206, p. 7) — Authorisation by the authorities of Castilla y León of hunting with snares in private hunting areas
Operative part of the judgment
The Court:
1. Dismisses the action;
2. Orders the Commission of the European Communities to pay the costs.
[1] OJ C 179, 10.07.2004.
--------------------------------------------------
