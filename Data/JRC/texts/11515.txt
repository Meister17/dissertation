Reference for a preliminary ruling from the Juzgado de lo Social No 3 lodged on 14 February 2006 — Vicente Pascual García v Confederación Hidrográfica del Duero
Referring court
Juzgado de lo Social No 3
Parties to the main proceedings
Applicant: Vicente Pascual García
Defendant: Confederación Hidrográfica del Duero
Questions referred
1. Does the principle of equal treatment, which prohibits any discrimination whatsoever on grounds of age, laid down in Article 13 of the Treaty and Article 2 (1) of Directive 2000/78 [1], preclude a national law (specifically the first paragraph of the Single Transitional Provision of Law 14/2005 on clauses in collective agreements concerning the attainment of normal retirement age), pursuant to which compulsory retirement clauses contained in collective agreements are lawful, where such clauses provide as sole requirements that workers should have reached normal retirement age and have satisfied the conditions set out in the social security legislation of the Spanish State for entitlement to draw a retirement pension under their contribution regime, whereas, for future agreements to be able to provide for termination of the contract on grounds of age, the undertaking must also link such termination to an employment policy?
2. Does the principle of equal treatment, which prohibits any discrimination whatsoever on grounds of age, laid down in Article 13 of the Treaty and Article 2 (1) of Directive 2000/78, require this Court, as a national court, not to apply to this case the first paragraph of the Single Transitional Provision of Law 14/2005?
[1] Council Directive 2000/78/EC of 27 November 2000 establishing a general framework for equal treatment in employment and occupation (OJ 2000 L 303, p. 16).
--------------------------------------------------
