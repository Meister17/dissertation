*****
COMMISSION REGULATION (EEC) No 3893/88
of 14 December 1988
amending Regulation (EEC) No 2290/83 laying down provisions for the implementation of Articles 50 to 59 of Council Regulation (EEC) No 918/83 setting up a Community system of reliefs from customs duty
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 918/83 of 28 March setting up a Community system of reliefs from customs duty (1), as last amended by Regulation (EEC) No 1315/88 (2), and in particular Article 143 thereof,
Whereas Articles 63a and 63b of Regulation (EEC) No 918/83 introduced on a definitive basis into the Community system of reliefs from customs duty the previously optional provisions of Articles 137 and 138 of that Regulation in respect of instruments and apparatus used in medical research, establishing medical diagnoses or carrying out medical treatment;
Whereas such implementing provisions may to some extent be based on those of Commission Regulation (EEC) No 2290/83 (3), as amended by Regulation (EEC) No 1745/85 (4); whereas it would therefore seem appropriate to deal with all the analogous situations in a single instrument, through an amendment to extend the scope of Regulation (EEC) No 2290/83;
Whereas practical experience of the procedure laid down by present provisions shows that certain adaptations should be made;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Committee on Duty-free Arrangements,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EEC) No 2290/83 is hereby amended as follows:
1. The title is replaced by the following:
'Commission Regulation (EEC) No 2290/83 of 29 July 1983 laying down provisions for the implementation of Articles 50 to 59 and Articles 63a and 63b of Council Regulation (EEC) No 918/83 setting up a Community system of reliefs from customs duty';
2. Article 1 is replaced by the following:
'Article 1
This Regulation lays down provisions for the implementation of Articles 50 to 59 and Articles 63a and 63b of Regulation (EEC) No 918/83, hereinafter referred to as the ''basic Regulation".;
3. In Article 7 (2) the second subparagraph is replaced by the following:
'Pending a decision on the application for duty-free admission in accordance with this Article, the competent authority may authorize the provisional importation free of import duties of the instrument or apparatus concerned, subject to an undertaking by the establishment or organization to which the goods are consigned to pay the relevant import duties should admission free of import duties not be granted.';
4. The following sentence is added to Article 7 (7):
'The period may, however, be extended, provided that the period in total does not exceed nine months, where the Commission has found it necessary to seek further information from the Member State in order to reach a decision. In this case the Commission must inform the requesting competent authority before the initial six-month period expires.';
5. The following Title Va is inserted:
'TITLE Va
SPECIAL PROVISIONS RELATING TO THE ADMISSION FREE OF IMPORT DUTIES OF MEDICAL INSTRUMENTS OR APPARATUS UNDER ARTICLES 63a AND 63b OF THE BASIC REGULATION
Article 15a
1. In order to obtain admission free of import duties of instruments or apparatus under Articles 63a and 63b of the basic Regulation, the head of the establishment or organization to which the goods are consigned, or his authorized representative, must submit an application to the competent authority of the Member State in which the establishment or organization is situated.
2. The application referred to in paragraph 1 must contain the following information relating to the instrument or apparatus in question:
(a) the precise trade description of the instrument or apparatus used by the manufacturer, and its presumed classification in the tariff nomenclature;
(b) the name or business name and address of the manufacturer and, if available, of the supplier;
(c) the country of origin of the instrument or apparatus;
(d) the place where the instrument or apparatus is to be used;
(e) the use which the instrument or apparatus is to be put.
3. In the case of a gift, the application shall also include:
(a) the name of business name and address of the donor;
(b) a declaration by the applicant to the effect that:
(i) the donation of the instrument or apparatus in question does not conceal any commercial intent on the part of the donor;
(ii) the donor is no way associated with the manufacturer of the instruments or apparatus whose duty-free admission is requested.
Article 15b
1. Where the competent authority of a Member State is considering granting duty-free admission of apparatus or instruments as defined in Article 63a of the basic Regulation, it shall consult the other Member States to ascertain whether equivalent apparatus or instruments are being manufactured in the Community.
2. If no reply is received by the consulting authority within four months, it shall take the view that no instruments equivalent to that for which duty-free admission is requested are being manufactured in the Member States consulted.
3. Where the four-month period proves insufficient for the authority consulted, that authority shall inform the consulting authority accordingly and at the same time state the period within which a final reply may be expected from it. Such period shall not, however, exceed a further two months.
4. Where on completion of the consultation procedure provided for in paragraphs 1 to 3, the consulting authority finds that the conditions laid down in Article 63a (1) (a), (b) and (c) of the basic Regulation are met it shall grant the relief. Otherwise it shall refuse it.
Article 15c
Where the competent authority of the Member State in which the establishment or body to which the goods are consigned is situated is unable to take a decision as referred to in Article 15b, the provisions governing the procedure laid down in Article 7 (2) to (7) in respect of the duty-free admission of scientific instruments and apparatus shall apply mutatis mutandis.
Article 15d
The provisions of Articles 15a to 15c shall apply mutatis mutandis to spare parts, components, specific accessories and tools to be used for the maintenance, checking, calibration or repair of instruments or apparatus admitted duty-free pursuant to Article 63a (2) (a) and (b) of the basic Regulation.
Article 15e
The provisions of Article 8 shall apply, mutatis mutandis.';
6. The first subparagraph of Article 16 (1) is replaced by the following:
'Each Member State shall send the Commission a list of the instruments, apparatus, spare parts, components, accessories and tools of which the price or the value for customs purposes exceeds Ecu 5 000 and for which it has authorized or refused admission free of import duties under Article 7 (1), 14 (1) or 15b (4).'
Article 2
This Regulation shall enter into force on 1 January 1989.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 14 December 1988.
For the Commission
COCKFIELD
Vice-President
(1) OJ No L 105, 23. 4. 1983, p. 1.
(2) OJ No L 123, 17. 5. 1988, p. 2.
(3) OJ No L 220, 11. 8. 1983, p. 20.
(4) OJ No L 167, 27. 6. 1985, p. 21.
