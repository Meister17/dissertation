Council Regulation (EC) No 923/2002
of 30 May 2002
on the conclusion of the Protocol defining, for the period from 18 January 2002 to 17 January 2005, the fishing possibilities and the financial contribution provided for by the Agreement between the European Economic Community and the Republic of Seychelles on fishing off Seychelles
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 37 in conjunction with Article 300(2) and the first subparagraph of Article 300(3) thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament(1),
Whereas:
(1) In accordance with the Agreement between the European Economic Community and the Republic of Seychelles on fishing off Seychelles(2), signed in Brussels on 28 October 1987, the two Parties held negotiations with a view to determining amendments to be made to that Agreement at the end of the period of application of the Protocol attached to the said Agreement.
(2) As a result of these negotiations, a new Protocol defining for the period from 18 January 2002 to 17 January 2005 the fishing possibilities and the financial contribution provided for by the abovementioned Agreement was initialled on 28 September 2001.
(3) It is in the Community's interest to approve that Protocol.
(4) The allocation of fishing possibilities among the Member States should be determined on the basis of the traditional allocation of fishing possibilities under the Fisheries Agreement,
HAS ADOPTED THIS REGULATION:
Article 1
The Protocol defining, for the period from 18 January 2002 to 17 January 2005, the fishing possibilities and the financial contribution provided for by the Agreement between the European Economic Community and the Republic of Seychelles on fishing off Seychelles is hereby approved on behalf of the European Community.
The text of the Protocol is attached to this Regulation(3).
Article 2
The fishing possibilities provided for in the Protocol are allocated among the Member States as follows:
(a) Tuna seiners
>TABLE>
(b) Surface longliners
>TABLE>
If licence applications from these Member States do not exhaust the fishing possibilities provided for in the Protocol, the Commission may take into consideration licence applications from any other Member State.
Article 3
The Member States whose vessels are fishing under this Protocol shall notify the Commission of the quantities of each stock taken in Seychelles waters in accordance with the arrangements laid down in Commission Regulation (EC) No 500/2001(4).
Article 4
The President of the Council is hereby authorised to designate the persons empowered to sign the Protocol in order to bind the Community.
Article 5
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 30 May 2002.
For the Council
The President
M. A. Cortés Martín
(1) Opinion delivered on 14 May 2002 (not yet published in the Official Journal).
(2) OJ L 119, 7.5.1987, p. 26.
(3) OJ L 134, 22.5.2002, p. 40.
(4) OJ L 73, 15.3.2001, p. 8.
