Council Joint Action 2005/825/CFSP
of 24 November 2005
amending the mandate of the European Union Special Representative in Bosnia and Herzegovina
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty on European Union, and in particular Articles 14, 18(5) and 23(2) thereof,
Whereas:
(1) On 12 July 2004, the Council adopted Joint Action 2004/569/CFSP [1] on the mandate of the European Union Special Representative (EUSR) in Bosnia and Herzegovina.
(2) On 28 July 2005, the Council adopted Joint Action 2004/583/CFSP [2] extending the mandate of Lord ASHDOWN as the EUSR in Bosnia and Herzegovina until 28 February 2006.
(3) On 24 November 2005, the Council adopted Joint Action 2005/824/CFSP on the European Union Police Mission (EUPM) in Bosnia and Herzegovina [3], which provides for a continuation of the EUPM with an adjusted mandate and size.
(4) In view of the specific role for the EUSR in the chain of command for EUPM, the EUSR's mandate should be amended accordingly.
(5) The EUSR's mandate should be implemented in coordination with the Commission in order to ensure consistency with other relevant activities falling within Community competence.
(6) The EUSR is to implement his mandate in the context of a situation which may deteriorate and could harm the objectives of the CFSP as set out in Article 11 of the Treaty,
HAS ADOPTED THIS JOINT ACTION:
Article 1
Joint Action 2004/569/CFSP, as extended by Joint Action 2005/583/CFSP, is hereby amended as follows:
1. Article 3 shall read as follows:
"Article 3
In order to achieve the policy objectives of the EU in BiH, the mandate of the EUSR shall be to:
(a) offer the EU's advice and facilitation in the political process;
(b) promote overall EU political coordination in BiH;
(c) promote overall EU coordination of, and give local political direction to, EU efforts in tackling organised crime, without prejudice to the European Union Police Mission (EUPM)'s leading role in the coordination of policing aspects of these efforts and to the ALTHEA (EUFOR) military chain of command;
(d) provide the ALTHEA (EUFOR) Commander with local political advice, including with respect to the Integrated Police Unit style capability, on which the EUSR may draw, in agreement with the said Commander, without prejudice to the chain of command;
(e) contribute to reinforcement of internal EU coordination and coherence in BiH, including through briefings to EU Heads of Mission and through participation in, or representation at, their regular meetings, through chairing a coordination group composed of all EU actors present in the field with a view to coordinating the implementation aspects of the EU's action, and through providing them with guidance on relations with the BiH authorities;
(f) ensure consistency and coherence of EU action towards the public. The EUSR spokesperson shall be the main EU point of contact for BiH media on CFSP/ESDP issues;
(g) maintain an overview over the whole range of activities in the field of the rule of law and in this context provide the Secretary-General/High Representative and the Commission with advice as necessary;
(h) provide the Head of Mission of the EUPM with local political guidance as part of his wider responsibilities and his role in the chain of command for EUPM;
(i) as part of the international community's and the BiH authorities' broader approach to the rule of law, and drawing upon the EUPM's provision of technical policing expertise and assistance in this respect, support the preparation and implementation of police restructuring;
(j) provide support for a reinforced and more effective BIH criminal justice/police interface, in close liaison with EUPM;
(k) as far as activities under Title VI of the Treaty, including Europol, and related Community activities are concerned, provide the Secretary-General and the Commission with advice as necessary, and take part in the required local coordination;
(l) with a view to coherence and possible synergies, continue to be consulted on priorities for Community Assistance for Reconstruction, Development and Stabilisation.";
2. Article 6(1) shall be replaced by the following:
"1. The financial reference amount shall be EUR 160000.";
3. Article 7(1) shall be replaced by the following:
"1. An EU dedicated staff projecting an EU identity shall be assigned to assist the EUSR to implement his mandate and contribute to the coherence, visibility and effectiveness of the overall EU action in BiH, in particular in political, politico-military, rule of law, including the fight against organised crime, and security affairs, and with regard to communication and media relations. Within the limits of his mandate and the corresponding financial means made available, the EUSR shall be responsible for constituting his team in consultation with the Presidency, assisted by the Secretary-General/High Representative, and in full association with the Commission. The EUSR shall inform the Presidency and the Commission of the final composition of his team.".
Article 2
This Joint Action shall enter into force on the day of its adoption.
Article 3
This Joint Action shall be published in the Official Journal of the European Union.
Done at Brussels, 24 November 2005.
For the Council
The President
I. Lewis
[1] OJ L 252, 28.7.2004, p. 7.
[2] OJ L 199, 29.7.2005, p. 94.
[3] See page 55 of this Official Journal.
--------------------------------------------------
