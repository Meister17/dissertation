Corrigendum to Commission Regulation (EC) No 127/2005 of 27 January 2005 amending Regulation (EC) No 20/2002 laying down detailed rules for implementing the specific supply arrangements for the outermost regions introduced by Council Regulations (EC) No 1452/2001, (EC) No 1453/2001 and (EC) No 1454/2001
(Official Journal of the European Union L 25 of 28 January 2005)
On page 12, in Article 1(2):
for:
"2. The title of Chapter VI and Articles 16, 17 and 18 are replaced by the following:",
read:
"2. Chapter VI shall be replaced by the following:";
on page 13, in Article 16(1)(c):
for:
"quantities of products which have been exempt from import duties and which are re-dispatched shall be re-attributed to the forecast supply balance and the amount of the erga omnes import duties applicable is paid by the dispatcher at the latest at the time of re-dispatching;",
read:
"quantities of products which have been exempt from import duties and which are re-dispatched shall be re-attributed to the forecast supply balance and the amount of the erga omnes import duties applicable at the day of importation is paid by the dispatcher at the latest at the time of re-dispatching;".
--------------------------------------------------
