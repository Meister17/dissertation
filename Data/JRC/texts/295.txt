Commission Regulation (EC) No 1534/2000
of 13 July 2000
determining the sensitive production areas and/or the groups of high-quality varieties exempt from application of the quota buyback programme in raw tobacco
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2075/92 of 30 June 1992 on the common organisation of the market in raw tobacco(1), as last amended by Regulation (EC) No 1336/2000(2), and in particular Article 14a thereof,
Whereas:
(1) Under Article 34(2) of Commission Regulation (EC) No 2848/98 of 22 December 1998 laying down detailed rules for the application of Council Regulation (EEC) No 2075/92 as regards the premium scheme, production quotas and the specific aid to be granted to producer groups in the raw tobacco sector(3), as last amended by Regulation (EC) No 1249/2000(4), the Commission must determine, on the basis of proposals from the Member States, which sensitive production areas and/or groups of high-quality varieties, up to a maximum of 25 % of each Member State's guarantee threshold, are to be exempt from application of the quota buyback programme.
(2) At the request of certain Member States, these groups of high-quality varieties should be determined.
(3) Article 35(2) of Regulation (EC) No 2848/98 stipulates that the Member State must make public its intention to sell from 1 September so that other producers may buy the quota before it is actually bought back. Therefore this Regulation must apply from 31 August 2000.
(4) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Tobacco,
HAS ADOPTED THIS REGULATION:
Article 1
The quantities of groups of high-quality varieties exempt from quota buyback for the 2000 harvest are as follows:
in Portugal:
>TABLE>
in France:
>TABLE>
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Communities.
It shall apply from 31 August 2000.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 13 July 2000.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 215, 30.7.1992, p. 70.
(2) OJ L 154, 27.6.2000, p. 2.
(3) OJ L 358, 31.12.1998, p. 17.
(4) OJ L 142, 16.6.2000, p. 3.
