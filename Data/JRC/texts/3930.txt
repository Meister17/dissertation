Judgment of the Court of First Instance of 15 September 2005 — Citicorp v OHIM
(Case T-320/03) [1]
Parties
Applicant(s): Citicorp (New York, United States) (represented by: V. von Bomhard, A. Renck and A. Pohlmann, lawyers)
Defendant(s): Office for Harmonisation in the Internal Market (Trade Marks and Designs) (represented by: S. Laitinen, P. Bullock and A. von Mühlendahl, Agents)
Application for
annulment of the decision of the Third Board of Appeal of OHIM of 25 June 2003 (Case R 85/2002-3), concerning an application to register the word mark LIVE RICHLY as a Community trade mark
Operative part of the judgment
The Court:
1) Dismisses the application;
2) Orders the applicant to bear its own costs, in addition to one half of the costs incurred by the defendant;
3) Orders the defendant to bear one half of its own costs.
[1] OJ C 275, 15.11.2003.
--------------------------------------------------
