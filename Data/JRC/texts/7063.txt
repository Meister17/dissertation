Council Regulation (EC) No 851/2005
of 2 June 2005
amending Regulation (EC) No 539/2001 listing the third countries whose nationals must be in possession of visas when crossing the external borders and those whose nationals are exempt from that requirement as regards the reciprocity mechanism
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 62(2)(b)(i) thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament [1],
Whereas:
(1) The mechanism provided for in Article 1(4) of Council Regulation (EC) No 539/2001 [2] has proved unsuitable for dealing with situations of non-reciprocity in which a third country on the list in Annex II to that Regulation, i.e. a third country whose nationals are exempt from the visa requirement, maintains or introduces a visa requirement for nationals from one or more Member States. Solidarity with the Member States experiencing situations of non-reciprocity requires that the existing mechanism be adapted so as to make it effective.
(2) Given the seriousness of such situations of non-reciprocity, it is essential that they should be notified without fail by the Member State(s) concerned. To ensure that the third country in question again applies visa-free travel to nationals of the Member States concerned, a mechanism should be provided which will combine measures at variable levels and intensities that can be rapidly carried out. Thus the Commission should take steps with the third country without delay, report to the Council and be able at any moment to propose that the Council adopt a provisional decision restoring the visa requirement for nationals of the third country in question. Resorting to such a provisional decision should not make it impossible to transfer the third country in question to Annex I of Regulation (EC) No 539/2001. A temporal link should also be provided between the entry into force of the provisional measure and any proposal to transfer the country to Annex I.
(3) A decision by a third country to introduce or reintroduce visa-free travel for nationals of one or more Member States should automatically terminate the provisional restoration of a visa requirement decided by the Council.
(4) The amended solidarity mechanism aims at achieving full reciprocity in respect of all Member States and creating an effective and accountable mechanism in order to ensure it.
(5) Regulation (EC) No 539/2001 should be amended accordingly.
(6) Transitional arrangements should be provided for where, when this Regulation comes into force, Member States are subject to a visa requirement by third countries listed in Annex II to Regulation (EC) No 539/2001.
(7) As regards Iceland and Norway, this Regulation constitutes a development of the provisions of the Schengen acquis, within the meaning of the Agreement concluded by the Council of the European Union and the Republic of Iceland and the Kingdom of Norway concerning the association of those two States with the implementation, application and development of the Schengen acquis [3], which fall within the area referred to in Article 1(B) of Council Decision 1999/437/EC of 17 May 1999 on certain arrangements for the application of that Agreement [4].
(8) The United Kingdom and Ireland are not bound by Regulation (EC) No 539/2001. They are therefore not taking part in the adoption of this Regulation and are not bound by it or subject to its application.
(9) As regards Switzerland, this Regulation constitutes a development of the provisions of the Schengen acquis within the meaning of the Agreement signed between the European Union, the European Community and the Swiss Confederation on the Swiss Confederation’s association with the implementation, application and development of the Schengen acquis [5], which fall in the area referred to in Article 1(B) of Decision 1999/437/EC read in conjunction with Article 4(1) of Council Decision 2004/849/EC [6] and Article 4(1) of Council Decision 2004/860/EC [7],
HAS ADOPTED THIS REGULATION:
Article 1
Article 1 of Regulation (EC) No 539/2001 is amended as follows:
1. Paragraph 4 shall be replaced by the following:
"4. Where a third country listed in Annex II introduces a visa requirement for nationals of a Member State, the following provisions shall apply:
(a) within 90 days of such introduction, or its announcement, the Member State concerned shall notify the Council and the Commission in writing; the notification shall be published in the C series of the Official Journal of the European Union. The notification shall specify the date of implementation of the measure and the type of travel documents and visas concerned.
If the third country decides to lift the visa obligation before the expiry of this deadline, the notification becomes superfluous;
(b) the Commission shall immediately after publication of that notification and in consultation with the Member State concerned, take steps with the authorities of the third country in order to restore visa-free travel;
(c) within 90 days after publication of that notification, the Commission, in consultation with the Member State concerned, shall report to the Council. The report may be accompanied by a proposal providing for the temporary restoration of the visa requirement for nationals of the third country in question. The Commission may also present this proposal after deliberations in Council on its report. The Council shall act on such proposal by a qualified majority within three months;
(d) if it considers it necessary, the Commission may present a proposal for the temporary restoration of the visa requirement for nationals of the third country referred to in subparagraph (c) without a prior report. The procedure provided for in subparagraph (c) shall apply to that proposal. The Member State concerned may state whether it wishes the Commission to refrain from the temporary restoration of such visa requirement without a prior report;
(e) the procedure referred to in subparagraphs (c) and (d) does not affect the Commission’s right to present a proposal amending this Regulation in order to transfer the third country concerned to Annex I. Where a temporary measure as referred to in subparagraphs (c) and (d) has been decided, the proposal amending this Regulation shall be presented by the Commission at the latest nine months after the entry into force of the temporary measure. Such a proposal shall also include provisions for lifting of temporary measures, which may have been introduced pursuant to the procedures referred to in subparagraphs (c) and (d). In the meantime the Commission will continue its efforts in order to induce the authorities of the third country in question to reinstall visa-free travel for the nationals of the Member State concerned;
(f) where the third country in question abolishes the visa requirement, the Member State shall immediately notify the Council and the Commission to that effect. The notification shall be published in the C series of the Official Journal of the European Union. Any temporary measure decided upon under subparagraph (d) shall terminate seven days after the publication in the Official Journal. In case the third country in question has introduced a visa requirement for nationals of two or more Member States the termination of the temporary measure will only terminate after the last publication."
2. The following paragraph shall be added:
"5. As long as visa exemption reciprocity continues not to exist with any third country listed in Annex II in relation to any of the Member States, the Commission shall report to the European Parliament and the Council before the 1 July of every even-numbered year on the situation of non-reciprocity and shall, if necessary, submit appropriate proposals."
Article 2
Member States whose nationals on 24 June 2005 are subject to a visa requirement by a third country listed in Annex II to Regulation (EC) No 539/2001 shall notify the Council and the Commission in writing by 24 July 2005. The notification shall be published in the C series of the Official Journal of the European Union.
The provisions of Article 1(4)(b) to (f) of Regulation (EC) No 539/2001 shall apply.
Article 3
This Regulation shall enter into force on the twentieth day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in the Member States in accordance with the Treaty establishing the European Community.
Done at Luxembourg, 2 June 2005.
For the Council
The President
L. Frieden
[1] Opinion delivered on 28 April 2005 (not yet published in the Official Journal).
[2] OJ L 81, 21.3.2001, p. 1.
[3] OJ L 176, 10.7.1999, p. 36.
[4] OJ L 176, 10.7.1999, p. 31.
[5] Council doc. 13054/04 accessible on http://register.consilium.eu.int
[6] OJ L 368, 15.12.2004, p. 26.
[7] OJ L 370, 17.12.2004, p. 78.
--------------------------------------------------
