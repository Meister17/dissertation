Commission Decision
of 31 July 2003
amending Decisions 1999/283/EC and 2000/585/EC as regards imports of fresh meat from Botswana and Swaziland
(notified under document number C(2003) 2743)
(Text with EEA relevance)
(2003/571/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 72/462/EEC of 12 December 1972 on health and veterinary inspection problems upon importation of bovine, ovine and caprine animals and swine, fresh meat or meat products from third countries(1) as last amended by Regulation (EC) No 807/2003(2), and in particular Article 14(3) thereof,
Having regard to Council Directive 92/45/EC of 16 June 1992 on public health and animal health problems relating to the killing of wild game and the placing on the market of wild game meat(3), as last amended by Regulation (EC) No 806/2003(4), and in particular Article 16(3) thereof,
Having regard to Council Directive 92/118/EEC(5) of 17 December 1992 laying down animal health and public health requirements governing trade in and imports into the Community of products not subject to the said requirements laid down in specific Community rules referred to in Annex A(I) to Directive 89/662/EEC and, as regards pathogens, to Directive 90/425/EEC as last amended by Commission Decision 2003/42/EC(6), and in particular Article 10 thereof,
Whereas:
(1) The animal health and veterinary certification conditions for imports of fresh meat from certain African countries are laid down by Commission Decision 1999/283/EC(7), as last amended by Decision 2003/163/EC(8).
(2) The animal and public health veterinary certification conditions for import of wild and farmed game meat and rabbit meat from third countries are laid down by Commission Decision 2000/585/EC(9), as last amended by Decision 2003/163/EC.
(3) Following outbreaks of foot-and-mouth disease in Botswana, Decisions 1999/283/EC and 2000/585/EC were amended by Commission Decision 2003/74/EC(10) to suspend exports to the Community from the whole of the country of boned and matured fresh meat of bovine, ovine and caprine species and farmed and wild ungulates produced after the presumed date of first infection, pending further information required to support regionalisation.
(4) Additional information and guarantees were received from the Botswanan veterinary authorities and Decisions 1999/283/EC and 2000/585/EC were amended by Decision 2003/163/EC to regionalise the country to allow imports into the Community of such boned and matured meat from zones 10, 11, 12, 13 and 14.
(5) No further cases of foot-and-mouth disease have been observed throughout the country since January 2003, all vaccinated animals have been destroyed and intensive surveillance including serological surveillance is being carried out with negative results for foot-and-mouth disease.
(6) The Botswanan authorities have given the necessary guarantees to the Commission that the veterinary control zones 5, 6, 7, 8, 9, and 18 may again be considered free of foot-and-mouth disease without vaccination.
(7) Accordingly, the importation into the Community of boned and matured fresh meat (excluding offal) of bovine, ovine and caprine species and farmed and wild ungulates should be allowed from zones 5, 6, 7, 8, 9, and 18 provided that slaughter took place after 7 June 2003.
(8) Following outbreaks of foot-and-mouth disease in Swaziland, Decision 1999/283/EC was amended by Commission Decision 2001/297/EC(11) to suspend exports to the Community from the whole of the country of boned and matured fresh meat of bovine species produced after the presumed date of first infection, pending further information required to support regionalisation.
(9) Additional information and guarantees were received from the Swaziland veterinary authorities and Decision 2000/585/EC was amended by Decision 2001/661/EC(12) to regionalise the country to allow imports into the Community of such boned and matured meat from certain zones.
(10) No further cases of foot-and-mouth disease have been observed throughout the country, the last vaccination was carried out in May 2001, all vaccinated animals have been identified and intensive surveillance including serological surveillance is being carried out with negative results for foot-and-mouth disease.
(11) The Swaziland authorities have given the necessary guarantees to the Commission that the territory excluding the area west of the "red line" fences which extend northwards from the river Usutu to the frontier with South Africa west of Nkalashane may again be considered free of foot-and-mouth disease without vaccination.
(12) Accordingly, the importation into the Community of boned and matured fresh meat (excluding offal) of bovine species should be allowed from this zone.
(13) Decisions 1999/283/EC and 2000/585/EC should therefore be amended accordingly.
(14) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
Annex I to Decision 1999/283/EC is replaced by the text in Annex I to this Decision.
Article 2
Annex II to Decision 1999/283/EC is replaced by the text in Annex II to this Decision.
Article 3
Annex I to Decision 2000/585/EC is replaced by the text in Annex III to this Decision.
Article 4
Annex II to Decision 2000/585/EC is replaced by the text in Annex IV to this Decision.
Article 5
This Decision shall apply from 4 August 2003.
Article 6
This Decision is addressed to the Member States.
Done at Brussels, 31 July 2003.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 302, 31.12.1972, p. 28.
(2) OJ L 122, 16.5.2003, p. 36.
(3) OJ L 268, 14.9.1992, p. 35.
(4) OJ L 122, 16.5.2003, p. 1.
(5) OJ L 62, 15.3.1993, p. 49.
(6) OJ L 13, 18.1.2003, p. 24.
(7) OJ L 110, 28.4.1999, p. 16.
(8) OJ L 66, 11.3.2003, p. 41.
(9) OJ L 251, 6.10.2000, p. 1.
(10) OJ L 28, 4.2.2003, p. 45.
(11) OJ L 102, 12.4.2001, p. 61.
(12) OJ L 232, 30.8.2001, p. 25.
ANNEX I
"ANNEX I
Description of territories of certain African countries established for animal health certification purposes
>TABLE>"
ANNEX II
"ANNEX II
Models of animal health certificates to be requested
>TABLE>"
ANNEX III
"ANNEX I
Description of territories of certain third countries established for animal health certification purposes
>TABLE>"
ANNEX IV
"ANNEX II
Animal health guarantees to be requested on certification of wild and farmed game meat and rabbit meat
>TABLE>"
