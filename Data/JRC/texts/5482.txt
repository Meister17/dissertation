Commission Decision
of 22 August 2005
concerning a derogation on the marking of pigmeat and its subsequent use for certain holdings in a surveillance zone of African swine fever in Sardinia, Italy
(notified under document number C(2005) 3161)
(Only the Italian text is authentic)
(2005/624/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 2002/60/EC of 27 June 2002 laying down specific provisions for the control of African swine fever and amending Directive 92/119/EEC as regards Teschen disease and African swine fever [1], and in particular Article 11(1)(f) thereof,
Whereas:
(1) Commission Decision 2005/363/EC of 2 May 2005 concerning animal health protection measures against African swine fever in Sardinia, Italy [2] was adopted in response to the presence of African swine fever in Sardinia, Italy.
(2) Outbreaks of African swine fever still occur in Sardinia and Italy takes measures to combat African swine fever in Sardinia within the framework of Directive 2002/60/EC.
(3) Directive 2002/60/EC provides that the competent authority establishes, immediately after the diagnosis of African swine fever in pigs on a holding has been officially confirmed, a protection zone with a radius of at least three kilometres around the outbreak site, which shall itself be included in a surveillance zone of a radius of at least ten kilometres.
(4) Directive 2002/60/EC also provides that pigs may not be removed from the holding in which they are kept (holding of origin) in a protection and surveillance zone during a period of respectively 40 and 30 days after the completion of the preliminary cleansing, disinfection and, if necessary, disinsectisation of the infected holdings. After these periods, the competent authorities may authorise the removal of pigs from the holding of origin to a slaughterhouse if specific conditions are met. In particular, fresh meat from these pigs is either to be processed or marked with a special mark and processed at later stage.
(5) Directive 2002/60/EC allows for a derogation from the said conditions to be granted to Member States at their request and if appropriate justification is submitted.
(6) An outbreak of African swine fever was confirmed in the municipality of Anela in Sardinia on 25 May 2005. The competent authority established immediately a protection zone with a radius of three kilometres around the outbreak site, which is included in a surveillance zone of a radius of ten kilometres around this site. Another outbreak was confirmed in the established protection zone on 10 June 2005 in the municipality of Bultei.
(7) The Italian authorities have asked the Commission for derogation from the fresh meat to be marked with the foreseen special mark and the condition that fresh meat originating from holdings situated within the established surveillance zone has to be processed. The request was justified by demonstrating the serious difficulties to find a market for processed meat, the consequences for the welfare of the pigs in some holdings if they are not slaughtered in due time and the negligible additional animal health risk related to such derogation if also specific disease control measures are adopted. It is therefore appropriate to provide that, under certain conditions, pigmeat from holdings situated in the established surveillance zone does not have to be processed and marked with the foreseen special mark and processed at later stage. In order to guarantee the absence of African swine fever and any risk for spread of the disease, additional measures as regards the holding of origin and the movement of these pigs have to be laid down.
(8) The checking and sampling procedures as regards the removal of pigs from a holding in an established surveillance zone to a slaughterhouse in accordance with the diagnostic manual [3] have to be fully applied. In case the derogation provided for in Article 11(4) of Directive 2002/60/EC is used paragraph 6 of Chapter IV of the Annex of the diagnostic manual applies.
(9) It is also appropriate to provide that pigmeat, pigmeat products and any other products containing pigmeat of pigs originating from holdings for which such a derogation is granted, is marked with the special mark foreseen by Decision 2005/363/EC in order to ensure that such pigmeat, pigmeat products and other products containing pigmeat are not dispatched from Sardinia and to ensure the traceability of such pigmeat and products.
(10) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
Subject matter and scope
This Decision lays down a derogation from the condition relating to processing of fresh meat originating from pigs from holdings situated in the surveillance zone which has been established around the holdings in which the diagnosis of African swine fever has been officially confirmed in Sardinia, Italy in the municipalities of Anela on 25 May 2005 and Bultei on 10 June 2005.
Article 2
Definitions
For the purpose of this Decision, the definitions laid down in Article 2 of Directive 2002/60/EC and of Article 2 of Decision 2005/363/EC shall apply.
Article 3
Derogation on Article 10(3)(f), fourth indent, of Directive 2002/60/EC
The competent authority may authorise that fresh meat from pigs which have been directly transported to a slaughterhouse in accordance with Article 11(1)(f) of Directive 2002/60/EC is not processed, as foreseen in Article 10(3)(f), fourth indent, if the following conditions are met:
(a) the holding of origin fulfils the requirements of Article 4;
(b) the movement of the pigs fulfils all the relevant requirements laid down in Directive 2002/60/EC and in particular in Articles 11(1)(f) and (4) thereof as regards the period of respectively 30 or 21 days after the completion of the preliminary cleansing, disinfection and, if necessary, disinsectisation of the infected holdings, during which the pigs may not be removed from the holding of origin;
(c) the meat, pigmeat products and any other products containing pigmeat originating from these pigs are marked with a special health or identification mark provided for in Article 4 of Decision 2005/363/EC.
Article 4
Requirements as regards the holding of origin
The holding of origin referred to in Article 3 shall fulfil the following requirements:
(a) the holding of origin may not be located in a protection zone established following an outbreak of African swine fever;
(b) the appropriate bio-security measures to prevent the introduction of African swine fever as well as a self-control programme to detect African swine fever, both referred to in the eradication programme approved by Commission Decision 2005/362/EC [4], have been put in place in the holding of origin and have been approved by the competent authority before the establishment of the surveillance zone around an outbreak of African swine fever in which the holding is located;
(c) African swine fever may not have been diagnosed in the holding of origin for at least two years before the dispatch of the pigs from this holding.
Article 5
Communication to the Commission and the other Member States
Italy shall communicate to the Commission and the other Member States, every month from the date of this Decision, all relevant information on the application of this Decision.
Article 6
Application
This Decision shall apply until 30 September 2005.
Article 7
Addressee
This Decision is addressed to the Italian Republic.
Done at Brussels, 22 August 2005.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 192, 20.7.2002, p. 27. Directive as amended by the 2003 Act of Accession.
[2] OJ L 118, 5.5.2005, p. 39. Decision as amended by Decision 2005/494/EC (OJ L 182, 13.7.2005, p. 26).
[3] Commission Decision 2003/422/EC of 26 May 2003 approving an African swine fever diagnostic manual (OJ L 143, 11.6.2003, p. 35).
[4] Commission Decision 2005/362/EC of 2 May 2005 approving the plan for the eradication of African swine fever in feral pigs in Sardinia, Italy (OJ L 118, 5.5.2005, p. 37).
--------------------------------------------------
