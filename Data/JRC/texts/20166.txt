COMMISSION REGULATION (EC) No 267/95 of 9 February 1995 amending Regulation (EEC) No 1756/93 fixing the operative events for the agricultural conversion rate applicable to milk and milk products
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 3813/92 of 28 December 1992 on the unit of account and the conversion rates to be applied for the purposes of the common agricultural policy (1), as last amended by Regulation (EC) No 150/95 (2), and in particular Article 6 (2) thereof,
Whereas Commission Regulation (EEC) No 1756/93 (3), as last amended by Regulation (EC) No 180/94 (4), seeks to fix precisely the agricultural conversion rate to be applied in respect of all amounts fixed in ecus in the milk and milk products sector; whereas that Regulation does not fix the operative event for the agricultural conversion rate to be applied for the amount referred to in Article 8 (5) of Commission Regulation (EEC) No 429/90 of 20 February 1990 on the granting by invitation to tender of an aid for concentrated butter intended for direct consumption in the Community (5), as last amended by Regulation (EC) No 3337/94 (6); whereas, consequently, it should be supplemented;
Whereas the operative events for the amount of the aid and for the penalty in the event of failure to meet the deadline laid down referred to in Article 16 (3) (c) and in the third subparagraph of Article 22 (4) of Commission Regulation (EEC) No 570/88 of 16 February 1988 on the sale of butter at reduced prices and the granting of aid for butter and concentrated butter for use in the manufacture of pastry products, ice-cream and other foodstuffs (7), as last amended by Regulation (EC) No 3337/94, and for the amount of aid referred to in Article 8 (2) (a) of Regulation (EEC) No 429/90 as defined in points 4 and 5 of Part B.III of the Annex to Regulation (EEC) No 1756/93, could encourage failure to meet the deadlines laid down for speculative reasons; whereas, as a result, they should be supplemented to prevent such risk;
Whereas the Management Committee for Milk and Milk Products has not delivered an opinion within the time limit set by its chairman,
HAS ADOPTED THIS REGULATION:
Article 1
Points 4 and 5 of Part B.III of the Annex to Regulation (EEC) No 1756/93 are hereby replaced by the following:
>TABLE>
Article 2
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 9 February 1995.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ No L 387, 31. 12. 1992, p. 1.
(2) OJ No L 22, 31. 1. 1995, p. 1.
(3) OJ No L 161, 2. 7. 1993, p. 48.
(4) OJ No L 24, 29. 1. 1994, p. 38.
(5) OJ No L 45, 21. 2. 1990, p. 8.
(6) OJ No L 350, 31. 12. 1994, p. 66.
(7) OJ No L 55, 1. 3. 1988, p. 31.
