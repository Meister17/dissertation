Swearing in of the new Members of the Court
(2006/C 42/06)
Mr Bay Larsen and Ms Sharpston, appointed Judge and Advocate General respectively to the Court of Justice of the European Communities by Decision of the Representatives of the Governments of the Member States of the European Communities of 20 July 2005 and 14 October 2005 [1], took the oath before the Court on 10 January 2006.
[1] OJ L 204, 5.8.2005, p. 11, and L 279, 22.10.2005, p. 70.
--------------------------------------------------
