COMMISSION REGULATION (EC) No 1406/98 of 1 July 1998 repealing the measures set out in Article 1 of Council Regulation (EC) No 703/98 suspending certain concessions set out in Regulation (EC) No 3066/95 establishing certain concessions in the form of Community tariff quotas for certain agricultural products and providing for the adjustment, as an autonomous and transitional measure, of certain agricultural concessions provided for in the Europe Agreements to take account of the Agreement on agriculture concluded during the Uruguay Round multilateral trade negotiations
THE EUROPEAN COMMISSION,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 703/98 of 17 March 1998 suspending certain concessions set out in Regulation (EC) No 3066/95 establishing certain concessions in the form of Community tariff quotas for certain agricultural products and providing for the adjustment, as an autonomous and transitional measure, of certain agricultural concessions provided for in the Europe Agreements to take account of the Agreement on agriculture concluded during the Uruguay Round multilateral trade negotiations (1), and in particular Article 2 thereof,
Whereas Council Regulation (EC) No 3066/95 (2), as last amended by Regulation (EC) No 1595/97 (3) establishes certain concessions in the form of Community tariff quotas for certain agricultural products and provides for the adjustment as an autonomous and transitional measure of certain agricultural concessions provided for in the Europe Agreements to take account of the Agreement on agriculture concluded during the Uruguay Round multilateral trade negotiations;
Whereas Regulation (EC) No 703/98, in order to protect the Community's trade interests after the Czech Republic increased unilaterally the import duties for certain agricultural products originating in the Community, autonomously suspended in an equivalent way certain of the concessions set out in Regulation (EC) No 3066/95;
Whereas the Czech Republic has now repealed the import duties for certain agricultural products, re-establishing the reciprocity of treatment;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committees concerned,
HAS ADOPTED THIS REGULATION:
Article 1
The measures set out in Article 1 of Regulation (EC) No 703/98 are hereby repealed.
Article 2
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 1 July 1998.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ L 98, 31. 3. 1998, p. 1.
(2) OJ L 328, 30. 12. 1995, p. 31.
(3) OJ L 216, 8. 8. 1997, p. 1.
