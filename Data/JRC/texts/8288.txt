COUNCIL DECISION
of 29 July 1999
concerning the provisional application of the Agreement on Trade, Development and Cooperation between the European Community and its Member States, of the one part, and the Republic of South Africa, of the other part
(1999/753/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community and in particular Article 310 thereof in connection with Article 300(2), first paragraph, first and second sentence,
Having regard to the proposal from the Commission,
Whereas:
(1) the Community and South Africa have undertaken to adopt procedures for the early application of the Agreement on Trade, Development and Cooperation between the European Community and its Member States, of the one part, and the Republic of South Africa, of the other part;
(2) the provisional application of the said Agreement through an Agreement in the form of an Exchange of Letters is necessary to provide a comprehensive framework of cooperation between the Community and South Africa, pending the completion of the procedures required to bring the said Agreement into force;
(3) the Agreement in the form of an Exchange of Letters should therefore be approved,
HAS DECIDED AS FOLLOWS:
Article 1
The Agreement in the form of an Exchange of Letters between the Community and South Africa, which provides for the provisional application of a portion of the Agreement on Trade, Development and Cooperation between the European Community and its Member States, on the one part, and the Republic of South Africa, on the other part, is hereby approved on behalf of the Community.
The text of the Agreement in the form of an Exchange of Letters is attached to this Decision, as well as the text of the Agreement.
Article 2
For the purposes of the implementation of Article 97 of the Agreement, a representative of the Commission shall preside over the Cooperation Council and present the position of the Community during the period of provisional application of the Agreement.
The position to be taken by the Community within the Cooperation Council shall be laid down by the Council on a proposal from the Commission.
Article 3
This Decision shall be published in the Official Journal of the European Communities.
Done at Brussels, 29 July 1999.
For the Council
The President
S. HASSI
