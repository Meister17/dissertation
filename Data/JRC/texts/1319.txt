Commission Directive 2001/39/EC
of 23 May 2001
amending the Annexes to Council Directives 86/362/EEC, 86/363/EEC and 90/642/EEC on the fixing of maximum levels for pesticide residues in and on cereals, foodstuffs of animal origin and certain products of plant origin, including fruit and vegetables respectively
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 86/362/EEC of 24 July 1986 on the fixing of maximum levels for pesticide residues in and on cereals(1), as last amended by Commission Directive 2000/82/EC(2), and in particular Article 10 thereof,
Having regard to Council Directive 86/363/EEC of 24 July 1986 on the fixing of maximum levels for pesticide residues in and on foodstuffs of animal origin(3), as last amended by Directive 2000/82/EC, and in particular Article 10 thereof,
Having regard to Council Directive 90/642/EEC of 27 November 1990 on fixing of maximum levels for pesticide residues in and on certain products of plant origin including fruit and vegetables(4), as last amended by Directive 2000/82/EC, and in particular Article 7 thereof,
Having regard to Council Directive 91/414/EEC of 15 July 1991 concerning the placing of plant protection products on the market(5), as last amended by Commission Directive 2001/21/EC(6), and in particular Article 4(1)(f) thereof,
Whereas:
(1) The new active substances, azimsulfuron, and prohexadione calcium were included in Annex I to Directive 91/414/EEC by Commission Directives 1999/80/EC(7) and 2000/50/EC(8), respectively, for use as a pre-emergence herbicide on rice, and as a plant growth regulator respectively.
(2) The substances were included in Annex I to Directive 91/414/EEC based on assessment of the information submitted concerning the proposed uses.
(3) Prior to the inclusion of the substances in Annex I to Directive 91/414/EEC, their use had been provisionally authorised in certain Member States in accordance with Article 8(1) of the Directive. Following inclusion of the substances in Annex I, those Member States authorised a number of plant protection products containing them in accordance with Article 4 of the Directive, and established provisional maximum residue levels as required by Article 4(1)(f). As required by the Directive, those levels, and the information on which they were based, have been notified to the Commission. This information together with data available from other sources has been reviewed and is sufficient to fix certain maximum residue levels (MRLS). Where no Community maximum residue level or provisional MRL exists, Member States must establish a national provisional maximum residue level in accordance with Article 4(1)(f) of the Directive before the authorisation may be granted. In view of Article 5 of Directive 86/363/EEC, this also applies for provisional MRLs for animal products where commodities containing residues of an active substance may be expected to be used as animal feeds.
(4) At their inclusion in Annex I to Directive 91/414/EEC, the technical and scientific evaluations of azimsulfuron, and prohexadione calcium were finalised on 2 July 1999, and 16 June 2000, respectively in the Commission review reports for azimsulfuron and prohexadione calcium. In these review reports the acceptable daily intake (ADI) for azimsulfuron was set at 0,1 mg/kg bw/day, and for prohexadione calcium at 0,2 mg/kg bw/day. The lifetime exposure of consumers of food products treated with azimsulfuron, and prohexadione calcium has been assessed and evaluated in accordance with the procedures and practices used within the Community, taking account of guidelines published by the World Health Organisation(9) and the opinion of the Scientific Committee for Plants(10) on this methodology. It has been calculated that the maximum residue levels provided for in this Directive do not result in these ADIs being exceeded.
(5) Acute toxic effects requiring the setting of an acute reference dose were not noted during the evaluations and discussions that preceded the inclusion of azimsulfuron, and prohexadione calcium in Annex I to Directive 91/414/EEC.
(6) To ensure that the consumer is adequately protected from exposure to residues in or on products for which no authorisations have been granted, it is prudent to set provisional maximum residue levels at the lower limit of analytical determination for all those products covered by Directives 86/362/EEC, 86/363/EEC and 90/642/EEC. The setting at Community level of such provisional maximum residue levels does not prevent the Member States from establishing provisional maximum residue levels for azimsulfuron and prohexadione calcium in accordance with Article 4(1)(f) of Directive 91/414/EEC and Annex VI thereto. Four years is considered a sufficient period of time during which to establish most further uses of azimsulfuron and prohexadione calcium. After that period these provisional maximum residue levels should become definitive.
(7) The measures provided for in this Directive were communicated to the World Trade Organisation and the comments received were taken into consideration. The possibility of fixing import tolerance maximum residue levels for specific pesticide/crop combinations will be examined by the Commission on the basis of the submission of acceptable data.
(8) The opinions of the Scientific Committee for Plants, in particular advice and recommendations concerning the protection of consumers of food products treated with pesticides, have been taken into account.
(9) The measures provided for in this Directive are in accordance with the opinion of the Standing Committee on Plant Health,
HAS ADOPTED THIS DIRECTIVE:
Article 1
In part A of Annex II to Directive 86/362/EEC the following rows are added:
>TABLE>
Article 2
In part B of Annex II to Directive 86/363/EEC the following row is added:
>TABLE>
Article 3
In Annex II to Directive 90/642/EEC the columns headed "Azimsulfuron", and "Prohexadione (prohexadione and its salts expressed as prohexadione)" set out in the Annex to this Directive are added.
Article 4
Member States shall adopt and publish the provisions necessary to comply with this Directive by 31 December 2001 at the latest. They shall forthwith inform the Commission thereof.
They shall apply those provisions from 1 January 2002.
When Member States adopt those provisions, they shall contain a reference to this Directive or be accompanied by such a reference on the occasion of their official publication. Member States shall determine how such reference is to be made.
Article 5
This Directive shall enter into force on the 20th day following its publication in the Official Journal of the European Communities.
Article 6
This Directive is addressed to the Member States.
Done at Brussels, 23 May 2001.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 221, 7.8.1986, p. 37.
(2) OJ L 3, 6.1.2001, p. 18.
(3) OJ L 221, 7.8.1986, p. 43.
(4) OJ L 350, 14.12.1990, p. 71.
(5) OJ L 230, 19.8.1991, p. 1.
(6) OJ L 69, 10.3.2001, p. 17.
(7) OJ L 210, 10.8.1999, p. 13.
(8) OJ L 198, 4.8.2000, p. 39.
(9) Guidelines for predicting dietary intake of pesticide residues (revised), prepared by the GEMS/Food Programme in collaboration with the Codex Committee on Pesticide Residues, published by the World Health Organisation 1997 (WHO/FSF/FOS/97.7).
(10) Opinion of the Scientific Committee on Plants regarding questions relating to amending the annexes to Council Directives 86/362/EEC, 86/363/EEC and 90/642/EEC (Opinion expressed by the Scientific Committee on Plants, 14 July 1998) (http://europa.eu.int/comm/dg24/health/sc/scp/out21_en.html).
ANNEX
>TABLE>
