Brussels, 15.03.2005
COM(2005) 96 final
2003/0134 COD
OPINION OF THE COMMISSION pursuant to Article 251 (2), third subparagraph, point (c) of the EC Treaty, on the European Parliament's amendments to the Council's common position regarding the proposal for a
DIRECTIVE OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL CONCERNING UNFAIR BUSINESS-TO-CONSUMER COMMERCIAL PRACTICES IN THE INTERNAL MARKET AND AMENDING COUNCIL DIRECTIVE 84/450/EEC, DIRECTIVES 97/7/EC, 98/27/EC AND 2002/65/EC OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL AND REGULATION (EC) NO. 2006/2004 OF THE EUROPEAN PARLIAMENT AND THE COUNCIL (“UNFAIR COMMERCIAL PRACTICES DIRECTIVE”)
AMENDING THE PROPOSAL OF THE COMMISSION pursuant to Article 250 (2) of the EC Treaty
2003/0134 COD
OPINION OF THE COMMISSION pursuant to Article 251 (2), third subparagraph, point (c) of the EC Treaty, on the European Parliament's amendment[s] to the Council's common position regarding the proposal for a
DIRECTIVE OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL concerning unfair business-to-consumer commercial practices in the internal market and amending Council Directive 84/450/EEC, Directives 97/7/EC, 98/27/EC and 2002/65/EC of the European Parliament and of the Council and Regulation (EC) No. 2006/2004 of the European Parliament and of the Council (“Unfair Commercial Practices Directive”)
1. Introduction
Article 251(2), third subparagraph, point (c) of the EC Treaty provides that the Commission is to deliver an opinion on the amendments proposed by the European Parliament at second reading. The Commission sets out its opinion below on the amendments proposed by Parliament.
2. Background
Date of transmission of the proposal to the EP and the Council (document COM(2003) 356 final – 2003/0134 COD): | 20 June 2003. |
Date of the opinion of the European Economic and Social Committee: | 29 January 2004. |
Date of the opinion of the European Parliament, first reading: | 20 April 2004. |
Date of adoption of the common position Date of European Parliament opinion, second reading | 15 November 2004 qualified majority). 24 February 2005 |
3. PURPOSE OF THE PROPOSAL
This Proposal addresses barriers to the functioning of the internal market for retail goods and services arising from fragmented national regulation and the consequent costs and uncertainties for businesses, and from variable consumer protection standards which undermine consumer confidence.
It does so by establishing a common, EU-wide framework for the regulation of unfair business-to-consumer commercial practices (notably advertising and marketing) which harm consumers’ economic interests. The centrepiece of this framework is a general prohibition of unfair commercial practices, based on common criteria for determining when a commercial practice is unfair. For extra legal certainty, this is supplemented by further elaboration of two key types of unfairness, misleading and aggressive practices, and a blacklist of practices which will always be unfair and are therefore prohibited up-front.
4. Opinion of the Commission on the amendments by the European Parliament
4.1. Amendments accepted by the Commission
The Commission can accept all 19 amendments adopted by the European Parliament in full. They are the result of a compromise agreement reached between the European Parliament, Council and Commission during the second reading. The amendments are in line with the Commission’s objectives for the proposal and maintain the balance of interests achieved in the common position.
5. Conclusion
Pursuant to Article 250(2) of the EC Treaty, the Commission amends its proposal as set out above.
