Commission Decision
of 13 December 2002
on the list of establishments in the Falkland Islands approved for the purpose of importing fresh meat into the Community
(notified under document number C(2002) 4988)
(Text with EEA relevance)
(2002/987/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 72/462/EEC of 12 December 1972 on health and veterinary inspection problems upon importation of bovine, ovine, and caprine animals, and swine, fresh meat or meat products from third countries(1), as last amended by Council Regulation (EC) No 1452/2001(2), and in particular Articles 4(1) and 18(1) thereof,
Whereas:
(1) Establishments in third countries may only be authorised to export fresh meat to the Community if they satisfy the general and special conditions laid down in Directive 72/462/EEC.
(2) Following a Community mission, it appears that the animal health situation in the Falkland Islands compares favourably with that in the Member States, particularly as regards disease transmission through meat, and that the operation of controls over the production of fresh meat is satisfactory.
(3) For the purpose of Article 4(3) of Directive 72/462/EEC, the Falkland Islands has forwarded details of the establishment which should be authorised to export fresh meat to the Community.
(4) A Community inspection has shown that the hygiene standards of that establishment are satisfactory and that it may therefore be included on the first list of establishments, to be drawn up in accordance with Article 4(1) of Directive 72/462/EEC, from which imports of fresh meat may be authorised. Imports of fresh meat from the establishment set out on the list in the Annex continue to be subject to provisions already laid down, the general provisions of the Treaty and in particular the other Community veterinary regulations regarding health protection.
(5) The measures provided for in this Regulation are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
1. The establishment in the Falkand Islands as listed in the Annex is hereby approved for the purposes of exporting fresh meat to the Community.
2. Imports from this establishment shall remain subject to the Community veterinary provisions, and in particular those concerning health protection.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 13 December 2002.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 302, 31.12.1972, p. 28.
(2) OJ L 198, 21.7.2001, p. 11.
ANNEX
Country: The Falkland Islands
>TABLE>
