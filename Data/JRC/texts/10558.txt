Removal from the register of Case C-121/05 P [1]
(2006/C 60/69)
(Language of the case: German)
By order of 14 December 2005, the President of the Court of Justice of the European Communities has ordered the removal from the register of Case C-121/05 P: Office for Harmonisation in the Internal Market (Trade Marks and Designs) (OHIM)
v Deutsche Post EURO EXPRESS GmbH.
[1] OJ C 143, 11.06.2005.
--------------------------------------------------
