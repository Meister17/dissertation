COMMISSION REGULATION (EEC) No 3746/91 of 18 December 1991 amending for the fourth time Regulation (EEC) No 2159/89 laying down detailed rules for applying the specific measures for nuts and locust beans as provided for in Title IIa of Council Regulation (EEC) No 1035/72
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 1035/72 of 18 May 1972 on the common organization of the market in fruit and vegetables (1), as last amended by Regulation (EEC) No 1623/91 (2), and in particular Article 14g thereof,
Having regard to Council Regulation (EEC) No 2145/91 of 15 July 1991 amending Regulation (EEC) No 790/89 as regards the maximum amount of aid for quality and marketing improvement in the nut and locust bean sector (3), and in particular Article 2 thereof,
Whereas Article 2 of Council Regulation (EEC) No 790/89 of 20 March 1989 fixing the level of additional flat-rate aid for the formation of producers' organizations and the maximum amount applied to aid for quality and marketing improvement in the nut- and locust bean-growing sector (4) provides for the maximum amount of aid per hectare and per year to be higher for grubbing operations followed by replanting and/or varietal conversion than for other work and for the maximum amounts of the aid for grubbing operations to be increased; whereas Commission Regulation (EEC) No 2159/89 of 18 July 1989 laying down detailed rules for applying the specific measures for nuts and locust beans as provided for in Title IIa of Council Regulation (EEC) No 1035/72 (5), as last amended by Regulation (EEC) No 2286/91 (6), should be amended accordingly;
Whereas these new provisions are, subject to certain conditions, applicable to plans approved prior to the entry into force of Regulation (EEC) No 2145/91, that is, before 23 July 1991; whereas, in the interests of good administrative practice, requests for plans to be adjusted or revised for this purpose must be submitted before a certain deadline;
Whereas the new maximum amount of aid of ECU 475 per hectare per year for grubbing operations is to be granted for a period of five years; whereas, although that period will commence from 1 September 1993 for plans approved before 23 July 1991, it should be specified that the maximum area in respect of which the aid is granted may not be exceeded throughout the period of application of the plan;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Fruit and Vegetables,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EEC) No 2159/89 is hereby amended as follows:
1. The following paragraph is added to Article 8:
'1a. In the case of quality and marketing improvement plans approved before 23 July 1991, requests for modifications shall be lodged with the competent authority of the Member State not later than 31 December 1992.
The five-year period for granting the aid referred to in Article 2 (1) of Regulation (EEC) No 790/89 in respect of these plans shall commence on 1 September 1993. However, the areas covered by operations carried out since the date on which the plan was initially approved may not exceed 40 % of the total area of the orchard.'
2. Annex IV is replaced by the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Communities. This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 18 December 1991. For the Commission
Ray MAC SHARRY
Member of the Commission
(1) OJ No L 118, 18. 5. 1972, p. 1. (2) OJ No L 150, 15. 6. 1991, p. 8. (3) OJ No L 200, 23. 7. 1991, p. 1. (4) OJ No L 85, 30. 3. 1989, p. 6. (5) OJ No L 207, 18. 7. 1989, p. 19. (6) OJ No L 209, 3. 7. 1991, p. 5.
ANNEX
'ANNEX IV
AID APPLICATION PROVIDED FOR IN ARTICLE 19
Business name of the producers' organization:
Administrative address:
(Street, number, locality, telephone number, telex number):
Bank and account number to which the aid should be paid:
Specific recognition pursuant to Article 14b of Regulation (EEC) No 1035/72:
Date: Decision No:
Total area of the orchard covered by the plan(s):
Reference period from:
to:
Date of approval of plan:
List of measures carried out during the period from to
which correspond to the . . .th year of implementation:
(a) eligible for the amount of ECU 475 referred to in Article 2 (1) of Regulation (EEC) No 790/89, as amended by Regulation (EEC) No 2145/91
Type of measure Single cost per hectare Area (*) Amount (national currency) 1. Grubbing/replanting Invoice No of (or supporting documents) Invoice No of Invoice No of Invoice No of Invoice No of 2. Varietal conversion (double grafting) Invoice No of (or supporting documents) Invoice No of Invoice No of Invoice No of Total Areas which have benefited from measures under (a) during the previous year Overall total (A)
G/A % 40 %.
(*) Only areas on which the measures have been implemented exhaustively may be selected as eligible for aid.
(b) eligible for the maximum amount of ECU 200/ha referred to in Article 2 (1) of Regulation (EEC) No 790/89, as amended by Regulation (EEC) No 2145/91; measures provided for in Article 7 of Regulation (EEC) No 2159/89 except for grubbing and conversion
Type of measure List of measures Single cost per hectare Area covered Amount 1. Measures Invoice No of Invoice No of Invoice No of 2. Measures Invoice No of Invoice No of 3. Measures Invoice No of Invoice No of Total (A)
(c) eligible for the maximum amount of ECU 200/ha referred to in Article 2 (2) of Regulation (EEC) No 790/89, as amended by Regulation (EEC) No 2145/91; measures provided for in Article 7 of Regulation (EEC) No 2159/89 except for grubbing and conversion
Type of measure List of measures Single cost per hectare Area covered Amount 1. Measures Invoice No of Invoice No of Invoice No of 2. Measures Invoice No of Invoice No of 3. Measures Invoice No of Invoice No of Total (A)
Total expenditure for the reference period
To be completed by the Member State Application received on: A. Eligible expenditure 1. Total expenditure declared 2. Total sums not eligible in the plan 3. (1 2) Expenditure to be taken into consideration B. Maximum permitted amount 1. ECU: 2. Rate on 1 September 19 . . 3. Total area 4. (1 × 2 × 3) Total eligible amount C. Amount to be paid
Paid on: '
