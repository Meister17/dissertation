Commission Regulation (EC) No 939/2006
of 23 June 2006
setting the amount of the aid for pears for processing for the 2006/07 marketing year
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 2201/96 of 28 October 1996 on the common organisation of the markets in processed fruit and vegetable products [1], and in particular Article 6(1) thereof,
Whereas:
(1) Article 3(3)(c) of Commission Regulation (EC) No 1535/2003 of 29 August 2003 laying down detailed rules for applying Council Regulation (EC) No 2201/96 as regards the aid scheme for products processed from fruit and vegetables [2] provides that the Commission is to publish the amount of aid applicable to pears for processing no later than 15 June.
(2) The average quantity of pears processed under the aid scheme in the last three marketing years is 8574 tonnes higher than the Community threshold.
(3) For those Member States that have overrun their processing threshold, the amount of the aid for pears for processing for the 2006/07 marketing year must therefore be adjusted in relation to the level set in Article 4(2) of Regulation (EC) No 2201/96, in accordance with Article 5(2) of that Regulation.
(4) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Products Processed from Fruit and Vegetables,
HAS ADOPTED THIS REGULATION:
Article 1
For the 2006/07 marketing year, the amount of the aid for pears under Article 2 of Regulation (EC) No 2201/96 shall be:
- EUR 161,70 per tonne in the Czech Republic,
- EUR 101,58 per tonne in Greece,
- EUR 150,77 per tonne in Spain,
- EUR 161,70 per tonne in France,
- EUR 148,47 per tonne in Italy,
- EUR 161,70 per tonne in Hungary,
- EUR 41,99 per tonne in the Netherlands,
- EUR 161,70 per tonne in Austria,
- EUR 161,70 per tonne in Portugal.
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 23 June 2006.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 297, 21.11.1996, p. 29. Regulation last amended by Commission Regulation (EC) No 386/2004 (OJ L 64, 2.3.2004, p. 25).
[2] OJ L 218, 30.8.2003, p. 14. Regulation last amended by Regulation (EC) No 1663/2005 (OJ L 267, 12.10.2005, p. 22).
--------------------------------------------------
