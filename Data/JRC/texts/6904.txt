Brussels, 5.4.2005
COM(2005) 112 final
2005/0032 (COD)
Proposal for a
REGULATION OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL
establishing a common framework for business registers for statistical purposes and repealing Council Regulation (EEC) No 2186/93
(presented by the Commission)
EXPLANATORY MEMORANDUM
1. BACKGROUND
The existing Business Registers Regulation (BR) (2186/93), which harmonised the business registers used by the Member States for statistical purposes, dates back to 1993 and is now partly outdated. In recent years, the situation has changed significantly and statistical requirements have evolved.
Three kinds of additional requirements have progressively emerged:
- globalisation of the economy has created the need to collect information on enterprise groups;
- the integration of activities of the different sectors has called for a full coverage of the whole economy;
- the Single Market requires improved statistical comparability, which notably depends on the availability of harmonised sources for the population of businesses operating in the EU.
The objective of the proposal is to update the existing BR Regulation to take these new requirements into account.
To achieve that aim, several modifications to the current BR Regulation are proposed and summarised in the paragraphs below.
2. CONTENTS OF THE REGULATION
Given the new data requirements presented above, two major changes are introduced in the proposed draft Regulation.
- All the enterprises carrying out economic activities contributing to gross domestic product, their local units and the corresponding legal units should be registered on a mandatory basis (some sectors of activity are optional in the current version of the Regulation).
- Financial links and enterprise groups will have to be covered and data on multinational groups and their constituent units will have to be exchanged between countries and Eurostat.
2.1. Full coverage of the economy
The existence of Community statistical legislation covering the whole economy, and the increasing use of business registers for national accounts purposes, make it essential for the business registers to cover the whole economy. The additional sectors to be covered on a mandatory basis are “public administration” and “agriculture and fishing”.
Inclusion of Public Administration
The role of the public sector is changing. Certain activities previously managed by the public sector can now be managed by public or private enterprises. To get a clear picture of the situation, more comparable sources on those activities are needed. This can only be achieved by the compulsory coverage of the public sector in the national business registers used for statistical purposes, according to agreed standards.
Inclusion of Agriculture and Fishing
The strong policy interest in rural development requires information not only on agriculture but also on its increasing combination with other activities which are not covered by the largely product-based agricultural statistics. A harmonised treatment of agriculture in business registers will give the basic information needed for rural policy.
2.2. Data on enterprise groups
There is a growing demand for enterprise group information, at the national level for the purpose of assessing the concentration of the economy, and at the international level for the production of statistics related to globalisation. To meet that demand, many statistics are already produced, some of them under Community statistical law (statistics on foreign affiliates, balance of payments, and external trade), but more are needed. The current situation, where only the truncated parts of the enterprise groups in the national territory are registered, is not sufficient and the data on the multinational groups must be consolidated at the European level.
The draft regulation requires the National business registers to record the financial links between legal units and to transmit information on cross-national linkages to the Commission (Eurostat).
This should result in
- harmonised sampling frames for the existing surveys which use the concept of financial links;
- better comparability of many current statistics, where financial links between enterprises in different countries play an important role, e.g. productivity data;
- additional information on the population of enterprise groups as the registers could also be used as direct sources of statistics on globalisation. This would be invaluable for different Community competition and research policies and for trade negotiations.
This proposal should also bring the scope and the quality of multinational enterprise group data much closer to the USA level.
3. IMPACT OF THE PROPOSAL
Most Member States have already partly implemented the new requirements
In almost all Member States, the three optional NACE sections are already at least partially covered and certain information on enterprise groups is available or under preparation. In the majority of cases a framework thus already exists and it is up to the Member States to take further measures to improve their coverage or quality, for instance by possible use of additional sources. Starting from the different national situations, a harmonisation between all Member States can only be obtained through the adoption of the common methodology, as presented in the proposed Regulation.
Recording of small entities is not required
Because international interest focuses on multinational groups, there are no strict requirements on the coverage of all-resident groups. The coverage of the smallest enterprises without employed persons is also left to subsidiarity, because this depends on the administrative sources nationally available. Thus the national situation and varying availability of sources have been taken into account where possible.
Some variables are optional in order to keep implementation costs at a reasonable level
The recording of certain variables is conditional on the availability of the information from administrative sources in individual the Member States and certain enterprise group variables are optional until the transmission of the data to Eurostat - consolidation of the data in Eurostat, and feedback of the consistent corrected data back to Member States - has been established. An important example of this is the definition of the controlling country in Foreign Affiliates statistics, where the nationally available information may be inconsistent or missing and consistency can only be ensured at European level.
Turnover is optional for agriculture, fishing and the public sector.
4. CONSULTATION WITH THE MEMBER STATES
The draft Regulation is the product of extensive consultations with the Member States and has been discussed at length by the various interested parties, several times in the Business Registers–Statistical Units Working Group and in the meetings of Directors of Business Statistics and of the Statistical Programme Committee. The views of major users in the Member States, EFTA, Candidate Countries and in the Commission and of other stakeholders have been considered. The present proposal is a balanced position between the level of detail needed by the main users and the workload in the National Statistical Institutes.
2005/0032 (COD)
Proposal for a
REGULATION OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL
establishing a common framework for business registers for statistical purposes and repealing Council Regulation (EEC) No 2186/93 (Text with EEA relevance)
THE EUROPEAN PARLIAMENT AND THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 285(1) thereof,
Having regard to the proposal from the Commission[1],
Having regard to the opinion of the European Parliament[2],
Acting in accordance with the procedure laid down in Article 251 of the Treaty[3],
Whereas:
(1) Council Regulation (EEC) No 2186/93 of 22 July 1993[4] established a common framework for setting up statistical business registers with harmonised definitions, characteristics, scope and updating procedures. In order to maintain the development of business registers in a harmonised framework, a new Regulation should be adopted.
(2) Council Regulation (EEC) No 696/93 of 15 March 1993 on the statistical units for the observation and analysis of the production system in the Community[5] contains the definitions of the statistical unit to be used. The Single Market requires improved statistical comparability to meet Community requirements. In order to achieve that improvement, common definitions and descriptions have to be adopted for enterprises and other relevant statistical units to be covered.
(3) Council Regulations (EC, Euratom) No 58/97 of 20 December 1996 on Structural Business Statistics[6] and No 1165/98 of 19 May 1998 concerning short-terms statistics[7] have established a common framework for the collection, compilation, transmission and evaluation of Community statistics on the structure, activity, competitiveness and performance of businesses in the Community. Business registers for statistical purposes represent a basic element of such a common framework, making it possible to organise and co-ordinate statistical surveys by providing a harmonised sampling frame.
(4) Business registers are one element in reconciling the conflicting requirements for increased information on enterprises and lightening their administrative burden, in particular by using existing information in administrative and legal registers, especially in the case of micro, small and medium-sized enterprises, as defined in Commission Recommendation 2003/361/EC[8].
(5) Business registers for statistical purposes are also the main source for business demography, as they keep track of business creations and closures as well as the structural changes in the economy by concentration or de-concentration, brought about by such operations as mergers, take-overs, break-ups, split-offs and restructuring of the business population.
(6) The important role played by public enterprises in the national economies of the Member States has been acknowledged. Commission Directive 80/723/EEC of 25 June 1980 on the transparency of financial relations between Member States and public undertakings[9] covers certain kinds of public undertakings. Public enterprises and public corporations should therefore be identified in business registers and this can be done by the institutional sector classification.
(7) Links of control between legal units are needed in defining enterprise groups, in delineating the enterprises correctly, in profiling large and complex units and in studying the concentration of the economy. Enterprise group information improves the quality of the business registers and can be used to reduce the risk of disclosure of confidential data. Certain financial data are often more meaningful at the enterprise group or subgroup level than at the enterprise level and they may be available only at the group or subgroup level. Recording of enterprise groups enables, when necessary, surveys directly of the group, instead of their constituent units, and this can reduce the response burden significantly. In order to register enterprise groups, business registers have to be further harmonised.
(8) The increasing globalisation of the economy challenges the current production of several statistics. By recording multinational enterprise groups, business registers form a basic tool for the improvement of many statistics related to globalisation: international trade in goods and services, balance of payments, foreign direct investment, foreign affiliates, research, development and innovation, and labour market. The majority of these statistics cover the whole economy and demand the coverage of all sectors of the economy by the business registers.
(9) According to Article 3(2) of Council Regulation (Euratom, EEC) No 1588/90 of 11 June 1990 on the transmission of data subject to statistical confidentiality to the Statistical Office of the European Communities[10], national rules on statistical confidentiality may not be invoked to prevent the transmission of confidential statistical data to the Community authority (Eurostat) where an act of Community law provides for the transmission of such data.
(10) In order to guarantee the fulfilment of the obligations laid down in this Regulation, the national institutions responsible for the collection of the data within the Member States may need access to administrative data sources such as registers held by Tax and Social Security Authorities, Central Banks, other public institutions and other data bases containing information on cross-border transactions and positions, wherever such data are necessary for the production of Community statistics.
(11) The measures necessary for the implementation of this Regulation should be adopted in accordance with Council Decision 1999/468/EC of 28 June 1999 laying down the procedures for the exercise of implementing powers conferred on the Commission[11].
(12) Regulation (EEC) 2186/93 should therefore be repealed.
(13) The Statistical Programme Committee has been consulted.
HAVE ADOPTED THIS REGULATION:
Article 1
Purpose
This Regulation establishes a common framework for statistical business registers in the Community.
Member States shall set up one or more harmonised registers for statistical purposes, as a tool for the preparation and co-ordination of surveys, as a source of information for statistical analysis of the business population and its demography, for the mobilisation of administrative data and for the detection and construction of statistical units.
Article 2
Definitions
For the purposes of this Regulation, the following definitions apply:
'Legal unit' shall mean a legal unit as defined in Section II.A.3 of the Annex to Regulation (EEC) No 696/93[12].
'Enterprise' shall mean an enterprise as defined in Section III.A of the Annex to that Regulation.
'Local unit' shall mean a local unit as defined in Section III.F of the Annex to that Regulation.
'Enterprise group’ shall mean an enterprise group as defined in Section III.C of the Annex to Regulation (EEC) No 696/93.
'Multinational enterprise group' shall mean an enterprise group which has at least two enterprises or legal units located in different countries.
'Truncated enterprise group' shall mean the enterprises and the legal units of an enterprise group, which are resident in the same country. It may comprise only one unit, if the other units are non-resident. An enterprise may be the truncated enterprise group or part thereof.
Article 3
Scope
1. In accordance with the definitions given in Article 2 and subject to the limitations specified in this Article, registers shall be compiled of
all enterprises carrying on economic activities contributing to the gross domestic product (GDP), and their local units,
the legal units of which those enterprises consist,
truncated enterprise groups and the information on the multinational enterprise groups defined in the Annex,
all-resident enterprise groups.
2. This requirement shall not, however, apply to households in so far as the goods and services they produce are for their own consumption, or involve the letting of own property.
3. Local units without separate legal entity (branches), which are dependent on foreign enterprises and classified as quasi-corporations according to ESA95 and SNA93 principles, shall be treated as enterprises in the business registers.
4. The enterprise group can be observed through the links of control between legal units. The definition of control, as given in the European System of Accounts ESA95 (Regulation (EC) No 2223/96), paragraph 2.26, shall be used for the delineation of enterprise groups.
5. This Regulation shall apply only to units which exercise wholly or partially an economic activity. Any activity consisting in offering goods and services on a given market is an economic activity. Non-market services contributing to the gross domestic product, as well as direct and indirect holding of active legal units are regarded as economic activity for business registers purposes. Economically inactive legal units are part of an enterprise only in combination with economically active legal units.
6. The extent to which enterprises with less than half a person employed, and all-resident enterprise groups of no statistical importance to the Member States are to be included in the registers shall be decided under the procedure laid down in Article 14.
Article 4
Data sources
1. Member States may, whilst complying with conditions as to quality referred to in Article 6, collect the information required under this Regulation using any sources they consider relevant. National authorities shall be authorised, within their sphere of competence, to collect for statistical purposes information covered by this Regulation contained in administrative and legal files.
2. Where the required data cannot be collected at a reasonable cost, statistical estimation procedures may be used, while respecting the level of accuracy and quality.
Article 5
Registers characteristics
The units listed in the registers shall be characterised by an identity number and the descriptive details specified in the Annex.
The list of characteristics shall be updated and the characteristics and continuity rules defined in accordance with the procedure laid down in Article 14.
Article 6
Quality standards and reports
1. Member States shall take all measures necessary to ensure the quality of the business registers.
2. Member States shall provide the Commission (Eurostat), at its request with a report on the quality of the business registers (hereinafter referred to as “quality reports”).
3. The common quality standards, as well as the content and periodicity of the quality reports, shall be specified by the Commission (Eurostat) in accordance with the procedure referred to in Article 14 and taking into account the implications as regards the cost of compiling the data.
4. Member States shall inform the Commission (Eurostat) of major methodological or other changes that would influence the quality of the business registers as soon as they become known and not later than six months after any such change enters into force.
Article 7
Recommendations manual
The Commission shall publish a business registers recommendations manual. The manual will be updated in close co-operation with the Member States.
Article 8
Time reference and periodicity
1. Entries to and removals from the registers shall be updated at least once a year.
2. The frequency of updating shall depend on the kind of unit, the variable considered, the size of the unit and the source generally used for the update.
3. The rules for updating shall be adopted in accordance with the procedure laid down in article 14.
4. Member States shall make annually a copy that reflects the state of the registers at the end of the year and keep that copy 30 years for the purpose of analysis.
Article 9
Transmission of data
1. Member States shall carry out statistical analyses of the registers and transmit the information to the Commission (Eurostat), following a format and a procedure defined by the Commission in accordance with the procedure laid down in Article 14.
2. Member States shall transmit to the Commission (Eurostat), at its request, any relevant information with regard to the implementation of this Regulation in the Member States.
Article 10
Exchange of confidential data between Member States
The exchange of confidential data, as defined by Article 13 of Regulation (EC) No 322/97, shall be allowed between Member States where the exchange is necessary to ensure the quality of the multinational enterprise group information in the European Union. Member States receiving confidential data from other Member States shall treat that information confidentially.
Article 11
Transmission of multinational enterprise group data
1. Member States shall transmit individual data on multinational enterprise groups and their constituent units, as defined in the Annex, to the Commission (Eurostat) to provide for the statistical information on multinational groups in the European Union.
2. In order to ensure a consistent record of data, the Commission (Eurostat) shall transmit to each Member State data on a multinational enterprise group, including its constituent units, when at least one legal unit of this group is located in the territory of that Member State.
3. The scope, format and procedure for the transmission of the individual data to the Commission (Eurostat) and for the transmission of data on the multinational enterprise groups to the Member States shall be adopted in accordance with the procedure laid down in Article 14.
Article 12
Transition period and derogations
When the business registers require a major adaptation, the Commission may grant derogation at the request of a Member State for a transitional period that shall not exceed two years from the date this Regulation shall apply.
For agriculture, forestry and fishing, public administration and defence, and compulsory social security, the transitional period shall not exceed three years.
Article 13
Implementing measures
The measures for implementing this Regulation shall be laid down in accordance with the procedure specified in Article 14. Such measures concern:
the coverage of smallest enterprises and all-resident enterprise groups, as provided for in Article 3;
the transmission of registers information and quality reports, as provided for in Articles 6 and 9;
the rules for updating registers, as provided for in Article 8;
the transmission of individual data for multinational enterprise groups to the Commission (Eurostat) and the transmission of multinational enterprise group data to the Member States, as provided for in Article 11;
the updating of the list of registers characteristics in the Annex, their definitions and their continuity rules, as provided for in Article 5, in so far as such updating, after a quantitative assessment, does not imply a burden on the units or on the Member States, which is disproportionate to the anticipated results.
Article 14
Committee
1. The Commission shall be assisted by the Statistical Programme Committee set up by Decision 89/382/EEC, Euratom[13].
2. Where reference is made to this paragraph, Articles 5 and 7 of Decision 1999/468/EC shall apply, having regard to the provisions of Article 8 thereof.
The period referred to in Article 5(6) of Decision 1999/468/EC shall be three months.
Article 15
Repeal
Council Regulation (EEC) No 2186/93 is repealed.
References to the repealed regulation shall be construed as being made to this Regulation.
Article 16
Entry into force
This Regulation shall enter into force on the twentieth day following that of its publication in the Official Journal of the European Union .
It shall apply from 1 January 2007.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels,
For the European Parliament For the Council
The President The President
ANNEX
The business registers shall contain the following information by unit. The information does not have to be separately stored for each unit, if it can be derived from other unit(s).
1. LEGAL UNIT |
IDENTIFICATION CHARACTERISTICS | 1.1 | Identity number |
1.2a | Name |
1.2b | Address (including postcode) at the most detailed level |
1.2c | Optional: Telephone and fax numbers, electronic mail address, and information to permit electronic collection of data |
1.3 | Value Added Tax (VAT) registration number or, failing that, other administrative identity number |
DEMOGRAPHIC CHARACTERISTICS | 1.4 | Date of incorporation for legal persons or date of official recognition as an economic operator for natural persons |
1.5 | Date on which the legal unit ceased to be part of an enterprise (as defined in 3.3) |
ECONOMIC/ STRATIFICATION CHARACTERISTICS | 1.6 | Legal form |
LINKS WITH OTHER REGISTERS | Reference to associated registers, in which the legal unit is recorded and which contain information that can be used for statistical purposes |
1.7a | Reference to the register of intra-Community operators set up in accordance with Regulation (EC) No 638/2004[14], and Reference to customs files or to the register of extra-Community operators |
1.7b | Optional: Reference to balance sheet data (for units required to publish accounts), and Reference to the balance of payments register or foreign direct investment register, and Reference to the farm register |
Additional characteristics for legal units which are part of enterprises belonging to an enterprise group:
LINK WITH ENTERPRISE GROUP | 1.8 | Identity number of the resident/truncated enterprise group (4.1), to which the unit belongs |
1.9 | Date of association to the resident/truncated group |
1.10 | Date of separation from the resident/truncated group |
CONTROL OF UNITS | The resident control links can be recorded either top-down (1.11a) or bottom-up (1.11b). Only the first level of control, direct or indirect, is recorded for each unit (the whole chain of control can be obtained by combining these). |
1.11a | Identity number(s) of resident legal unit(s), which are controlled by the legal unit |
1.11b | Identity number of the resident legal unit, which controls the legal unit |
1.12 | (a) Country(ies) of registration, and (b) identity number(s) or, name(s), address(es) and VAT number(s), of non-resident legal unit(s), which are controlled by the legal unit |
1.13 | (a) Country of registration, and (b) identity number or, name, address and VAT number, of the non-resident legal unit, which controls the legal unit |
OWNERSHIP OF UNITS | The resident ownership can be recorded either top-down (1.14a) or bottom-up (1.14b). The recording of the information and the threshold used for the shareholding are subject to the availability of this information in the administrative sources. The recommended threshold is 10 % or more of direct ownership. |
1.14a | (a) Identity number(s), and (b) shares (%) of resident legal unit(s) owned by the legal unit |
1.14b | (a) Identity number(s), and (b) shares (%) of resident legal unit(s), which own(s) the legal unit |
1.15 | (a) Country(ies) of registration, and (b) identity number(s) or, name(s), address(es) and VAT number(s), and (c) shares (%) of non-resident legal unit(s) owned by the legal unit |
1.16 | (a) Country(ies) of registration, and (b) identity number(s) or, name(s), address(es), and VAT number(s), and (c) shares (%) of non-resident legal unit(s), which own(s) the legal unit |
2. LOCAL UNIT |
IDENTIFICATION CHARACTERISTICS | 2.1 | Identity number |
2.2a | Name |
2.2b | Address (including postcode) at the most detailed level |
2.2c | Optional: Telephone and fax numbers, electronic mail address, and information to permit electronic collection of data |
2.3 | Identity number of the enterprise (3.1), to which the local unit belongs |
DEMOGRAPHIC CHARACTERISTICS | 2.4 | Date of commencement of activities |
2.5 | Date of final cessation of activities |
ECONOMIC/ STRATIFICATION CHARACTERISTICS | 2.6 | Principal activity code at NACE 4-digit level |
2.7 | Secondary activities, if any, at NACE 4-digit level; this point concerns only local units which are the subject of surveys |
2.8 | Optional: Activity carried out in the local unit constituting an ancillary activity of the enterprise to which it belongs (Yes/No) |
2.9 | Number of persons employed |
2.10a | Number of employees |
2.10b | Optional: Number of employees in full-time equivalents |
2.11 | Geographical location code |
LINKS WITH OTHER REGISTERS | 2.12 | Reference to associated registers, in which the local unit appears and which contain information which can be used for statistical purposes (if such associated registers exist) |
3. ENTERPRISE |
IDENTIFICATION CHARACTERISTICS | 3.1 | Identity number |
3.2a | Name |
3.2b | Optional: Postal, electronic mail and web site addresses |
3.3 | Identity number(s) of the legal unit(s) of which the enterprise consist(s) |
DEMOGRAPHIC CHARACTERISTICS | 3.4 | Date of commencement of activities |
3.5 | Date of final cessation of activities |
ECONOMIC/ STRATIFICATION CHARACTERISTICS | 3.6 | Principal activity code at NACE 4-digit level |
3.7 | Secondary activities, if any, at NACE 4-digit level; this point concerns only enterprises which are the subject of surveys |
3.8 | Number of persons employed |
3.9a | Number of employees |
3.9b | Optional: Number of employees in full-time equivalents |
3.10 | Turnover; optional for agriculture, fishing and public sector |
3.11 | Institutional sector and sub-sector according to European System of Accounts |
Additional characteristics for enterprises belonging to an enterprise group:
LINK WITH ENTERPRISE GROUP | 3.12 | Identity number of the resident/truncated enterprise group (4.1), to which the enterprise belongs |
4. ENTERPRISE GROUP |
IDENTIFICATION CHARACTERISTICS | 4.1 | Identity number of the resident/truncated group |
4.2a | Name of the resident/truncated group |
4.2b | Optional: Postal, electronic mail and web site addresses of the resident/truncated head office |
4.3 | Identity number of the resident/truncated group head (equals the identity number of the legal unit, which is the resident group head). If the controlling unit is a natural person, who is not an economic operator, recording is subject to the availability of this information in the administrative sources |
4.4 | Type of enterprise group: 1. all-resident group; 2. domestically controlled truncated group; 3. foreign controlled truncated group; |
DEMOGRAPHIC CHARACTERISTICS | 4.5 | Date of commencement of the resident/truncated enterprise group |
4.6 | Date of cessation of the resident/truncated enterprise group |
ECONOMIC/ STRATIFICATION CHARACTERISTICS | 4.7 | Principal activity code of the resident/truncated group at NACE 2-digit level |
4.8 | Optional: Secondary activities of the resident/truncated group at NACE 2-digit level |
4.9 | Number of persons employed in the resident/truncated group |
4.10 | Optional: Consolidated turnover |
Additional characteristics for multinational enterprise groups (types 2 and 3 in 4.4):
The recording of variables 4.11 and 4.12a is optional until transmission of information on multinational groups, as stated in Article 11, has been established.
IDENTIFICATION CHARACTERISTICS | 4.11 | Identity number of the global group |
4.12a | Name of the global group |
4.12b | Optional: Country of registration, postal, electronic mail and web site addresses of the global head office |
4.13 | Identity number of the global group head, if the group head is resident (equals the identity number of the legal unit, which is the group head). If the global group head is non-resident, its country of registration, and optionally: its identity number or name and address. |
ECONOMIC/ STRATIFICATION CHARACTERISTICS | 4.14 | Optional: Number of persons employed globally |
4.15 | Optional: Consolidated global turnover |
4.16 | Optional: Country of global decision-centre |
4.17 | Optional: Countries where enterprises or local units are located |
[1] OJ C , , p. .
[2] OJ C , , p. .
[3] OJ C […], […], p.[…]
[4] OJ L 196, 5.8.1993, p.1
[5] OJ L 76, 30.3.1993, p. 1
[6] OJ L 14, 17.1.1997, p. 1
[7] OJ L 162, 5.6.1998, p. 1
[8] OJ L 124, 20.5.2003, p. 36
[9] OJ L 195, 29.7.1980, p. 35
[10] OJ L 151, 15.6.1990, p. 1
[11] OJ L 184, 17.7.1999, p. 23
[12] OJ L 76, 30.3.1993, p. 1
[13] OJ L 181, 28.6.1989, p. 47.
[14] OJ L 102, 7.4.2004, p. 1
