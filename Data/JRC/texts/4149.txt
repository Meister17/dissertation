Euro exchange rates [1]
6 December 2005
(2005/C 309/01)
| Currency | Exchange rate |
USD | US dollar | 1,1783 |
JPY | Japanese yen | 142,64 |
DKK | Danish krone | 7,4504 |
GBP | Pound sterling | 0,67905 |
SEK | Swedish krona | 9,3862 |
CHF | Swiss franc | 1,5410 |
ISK | Iceland króna | 76,26 |
NOK | Norwegian krone | 7,8750 |
BGN | Bulgarian lev | 1,9561 |
CYP | Cyprus pound | 0,5734 |
CZK | Czech koruna | 28,941 |
EEK | Estonian kroon | 15,6466 |
HUF | Hungarian forint | 252,79 |
LTL | Lithuanian litas | 3,4528 |
LVL | Latvian lats | 0,6971 |
MTL | Maltese lira | 0,4293 |
PLN | Polish zloty | 3,8313 |
RON | Romanian leu | 3,6560 |
SIT | Slovenian tolar | 239,54 |
SKK | Slovak koruna | 37,795 |
TRY | Turkish lira | 1,5961 |
AUD | Australian dollar | 1,5644 |
CAD | Canadian dollar | 1,3607 |
HKD | Hong Kong dollar | 9,1366 |
NZD | New Zealand dollar | 1,6417 |
SGD | Singapore dollar | 1,9876 |
KRW | South Korean won | 1218,72 |
ZAR | South African rand | 7,4248 |
CNY | Chinese yuan renminbi | 9,5183 |
HRK | Croatian kuna | 7,3925 |
IDR | Indonesian rupiah | 11694,63 |
MYR | Malaysian ringgit | 4,455 |
PHP | Philippine peso | 63,534 |
RUB | Russian rouble | 34,0670 |
THB | Thai baht | 48,744 |
[1] Source: reference exchange rate published by the ECB.
--------------------------------------------------
