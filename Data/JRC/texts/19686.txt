Commission Regulation (EC) No 813/2003
of 12 May 2003
on transitional measures Regulation (EC) No 1774/2002 of the European Parliament and of the Council as regards the collection, transport and disposal of former foodstuffs
(Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Regulation (EC) No 1774/2002 of the European Parliament and of the Council of 3 October 2002 laying down health rules concerning animal by-products not intended for human consumption(1), as amended by Commission Regulation (EC) No 808/2003(2), and in particular Article 32(1) thereof,
Whereas:
(1) Regulation (EC) No 1774/2002 provides for a complete revision of Community rules concerning animal by-products not intended for human consumption, including the introduction of a number of strict requirements. In addition, it provides that appropriate transitional measures may be adopted.
(2) In view of the strict nature of those requirements, it is necessary to provide for transitional measures for the Member States in order to allow industry sufficient time to adjust. In addition, alternative collection, transport, storage, handling, processing and uses for animal by-products need to be further developed as well as disposal methods for those by-products.
(3) Accordingly, as a temporary measure a derogation should be granted to the Member States to enable them to authorise operators to continue to apply national rules for the collection, transport and disposal of former foodstuffs of animal origin.
(4) In order to prevent a risk to animal and public health appropriate control systems should be maintained in the Member States for the period of the transitional measures.
(5) The measures provided for in this Regulation are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS REGULATION:
Article 1
Derogation regarding the collection, transport and disposal of former foodstuffs
1. Pursuant to Article 32(1) of Regulation (EC) No 1774/2002 and by way of derogation from point (f) of Article 6(2) and Article 7 of that Regulation, the Member States may grant individual authorisation to operators of premises and facilities to apply national rules until 31 December 2005 at the latest for the collection, transport and transformation of former foodstuffs referred to point (f) of Article 6(1) of that Regulation, provided that the national rules
(a) without prejudice to paragraph 2 below, ensure that former foodstuffs are not mixed with Category 1 and 2 materials; and
(b) comply with the rest of the requirements of Regulation (EC) No 1774/2002.
2. However, mixing of former foodstuffs with Category 1 or Category 2 materials may be allowed when the material is being sent for incineration or processing in a Category 1 or 2 plant prior to disposal as waste by incineration, co-incineration or landfill in accordance with Community legislation.
3. When former foodstuffs are sent for disposal as waste in an approved landfill site, all necessary measures shall be taken to ensure that the former foodstuffs are not mixed with unprocessed material of animal origin referred to in Articles 4 and 5 and points (a) to (e) and points (g) to (k) of Article 6(1).
Article 2
Control measures
The competent authority shall take the necessary measures to control compliance by authorised operators of premises and facilities with the conditions set out in Article 1.
Article 3
Withdrawal of approvals and disposal of material not complying with this Regulation
1. Individual authorisation by the competent authority for the collection, transport and disposal of former foodstuffs of animal origin shall be immediately and permanently withdrawn in respect of any operator, premises or facilities if the conditions set out in this Regulation are no longer fulfilled.
2. The competent authority shall withdraw any authorisation granted under Article 1 by 31 December 2005 at the latest.
The competent authority shall not grant a final approval under Regulation (EC) No 1774/2002 unless on the basis of its inspections it is satisfied that the premises and facilities referred to in Article 1 meet all the requirements of that Regulation.
3. Any material that does not comply with the requirements of this Regulation shall be disposed of in accordance with the instructions of the competent authority.
Article 4
Entry into force
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Union.
It shall apply from 1 May 2003 to 31 December 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 12 May 2003.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 273, 10.10.2002, p. 1.
(2) See page 1 of this Official Journal.
