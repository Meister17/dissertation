COUNCIL DECISION of 8 June 1993 amending Council Decision 88/591/ECSC, EEC, Euratom establishing a Court of First Instance of the European Communities
(93/350/Euratom, ECSC, EEC)THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Coal and Steel Community, and in particular Article 32d thereof,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 168a thereof,
Having regard to the Treaty establishing the European Atomic Energy Community, and in particular Article 140a thereof,
Having regard to the Protocol on the Statute of the Court of Justice of the European Coal and Steel Community, signed in Paris on 18 April 1951,
Having regard to the request from the Court of Justice,
Having regard to the opinion of the Commission,
Having regard to the opinion of the European Parliament (1),
Whereas the attachment to the Court of Justice of a Court of First Instance by Decision 88/591/ECSC, EEC, Euratom (2) is intended, by the establishment of a second court, in particular in respect of actions requiring close examination of complex facts, to improve the judicial protection of individual interests and to maintain the quality and effectiveness of judicial review in the Community legal order by enabling the Court of Justice to concentrate its activities on its fundamental task, of ensuring uniform interpretation of Community law;
Whereas, with the same end in view, it is appropriate, taking into account past experience to enlarge the jurisdiction transferred to the Court of First Instance to hear and determine at first instance certain classes of action or proceeding brought by natural or legal persons;
Whereas Decision 88/591/ECSC, EEC, Euratom should therefore be amended in consequence,
HAS DECIDED AS FOLLOWS:
Article 1
Decision 88/591/ECSC, EEC, Euratom is hereby amended as follows:
1. the following shall be substituted for Article 3 (1):
'The Court of First Instance shall exercise at first instance the jurisdiction conferred on the Court of Justice by the Treaties establishing the Communities and by the acts adopted in implementation thereof, save as otherwise provided in an act setting up a body governed by Community law:
(a) in disputes as referred to in Article 179 of the EEC Treaty and Article 152 on the EAEC Treaty;
(b) in actions brought by natural or legal persons pursuant to the second paragraph of Article 33, Article 35, the first and second paragraphs of Article 40 and Article 42 of the ECSC Treaty;
(c) in actions brought by natural or legal persons pursuant to the second paragraph of Article 173, the third paragraph of Article 175 and Articles 178 and 181 of the EEC Treaty;
(d) in actions brought by natural or legal persons pursuant to the second paragraph of Article 146, the third paragraph of Article 148 and Articles 151 and 153 of the EAEC Treaty.';
2. paragraphs 2 and 3 of Article 3 are hereby repealed;
3. the following shall be substituted for Article 4:
'Article 4
Save as hereinafter provided, Articles 34, 36, 39, 44 and 92 of the ECSC Treaty, Articles 172, 174, 176, 184 to 187 and 192 of the EEC Treaty and Articles 49, 83, 144b, 147, 149, 156 to 159 and 164 of the Euratom Treaty shall apply to the Court of First Instance.'
Article 2
In the Protocol on the Statute of the Court of Justice of the European Coal and Steel Community, as amended by Decision 88/591/ECSC, EEC, Euratom, the following shall be substituted for the second paragraph of Article 53:
'By way of derogation from Article 44 of the Treaty, decisions of the Court of First Instance declaring a general decision or general recommendation to be void shall take effect only as from the date of expiry of the period referred to in the first paragraph of Article 49 of this Statute, or if an appeal shall have been brought within that period, as from the date of dismissal of the appeal, without prejudice, however, to the right of a party to apply to the Court of Justice pursuant to the second and third paragraphs of Article 39 of the Treaty, for the suspension of the effects of the act which has been declared void or for the prescription of any other interim measure.'
Article 3
This Decision shall enter into force on the first day of the second month following that of its publication in the Official Journal of the European Communities; however, in respect of actions brought by natural or legal persons pursuant to the second paragraph of Article 33, Article 35 and the first and second paragraphs of Article 40 of the ECSC Treaty and which concern acts relating to the application of Article 74 of the said Treaty in respect of actions brought by natural or legal persons pursuant to the second paragraph of Article 173, the third paragraph of Article 175 and Article 178 of the EEC Treaty and relating to measures to protect trade within the meaning of Article 113 of that Treaty in the case of dumping and subsidies, its entry into force shall be deferred to a date that the Council shall fix by unanimous decision.
The provisions relating to actions brought under Article 42 of the ECSC Treaty, Article 181 of the EEC Treaty or Article 153 of the EAEC Treaty shall apply only to contracts concluded after the entry into force of this Decision.
Article 4
Cases falling within the scope of Article 3 of Decision 88/591/ECSC, EEC, Euratom, as amended by this Decision, of which the Court of Justice is seised on the date on which this Decision enters into force but in which the preliminary report provided for in Article 44 (1) of the Rules of Procedure of the Court of Justice has not yet been presented, shall be referred to the Court of First Instance.
Done at Luxembourg, 8 June 1993.
For the Council
The President
N. HELVEG PETERSEN
(1) OJ No C 241, 21. 9. 1992, p. 1.
(2) OJ No L 319, 25. 11. 1988, p. 1. Corrected version published in OJ No C 215, 21. 8. 1989, p. 1.
