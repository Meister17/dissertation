*****
COUNCIL DECISION
of 25 July 1985
laying down the procedure for appointing those members of the European Foundation to be chosen by the Community
(85/375/EEC)
THE COUNCIL OF THE EUROPEAN
COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 235 thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Whereas the representatives of the Governments of the Member States, meeting within the Council, established, by an Agreement of 29 March 1982, a European Foundation;
Whereas that Agreement provides that 10 members of the Foundation's Board may be appointed by the Community;
Whereas a procedure should be laid down to enable this right to be exercised;
Whereas provision should be made for appropriate participation in the Foundation's Board by Members of the European Parliament;
Whereas the Treaty has not provided the specific necessary powers for this purpose,
HAS DECIDED AS FOLLOWS:
Article 1
The members of the Board of the European Foundation for whose appointment the Community is responsible shall be appointed by the Council acting by a qualified majority, on a proposal from the Commission after consulting the European Parliament.
Article 2
In preparing its proposal, the Commission shall choose persons eminent by reason of their competence and experience. It shall ensure that the independence of the persons proposed is beyond doubt.
The list of names proposed shall include those of Members of the European Parliament.
Article 3
This Decision shall take effect on the date on which the Agreement establishing the European Foundation enters into force.
Done at Brussels, 25 July 1985.
For the Council
The President
J. POOS
(1) OJ No C 112, 7. 5. 1985, p. 4.
(2) Opinion delivered on 12 July 1985 (not yet published in the Official Journal).
