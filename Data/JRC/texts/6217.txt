Judgment of the Court of First Instance of 27 October 2005 — Les Éditions Albert René v OHIM
(Case T-336/03) [1]
Parties
Applicant(s): Les Éditions Albert René (Paris, France) (represented by: J. Pagenberg, lawyer)
Defendant(s): Office for Harmonisation in the Internal Market (Trade Marks and Designs) (represented by: S. Laitinen, Agent)
Other party or parties to the proceedings before the Board of Appeal of OHIM intervening before the Court of First Instance: Orange A/S (Copenhagen, Denmark) (represented by: J. Balling, lawyer)
Application for
annulment of the decision of the Fourth Board of Appeal of OHIM of 14 July 2003 (Case R 0559/2002-4) in opposition proceedings between Les Éditions Albert René and Orange A/S
Operative part of the judgment
The Court:
1. Dismisses the action;
2. Orders the applicant to pay the costs.
[1] OJ C 85, 3.4.2004.
--------------------------------------------------
