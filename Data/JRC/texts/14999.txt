Commission Regulation (EC) No 1473/2006
of 5 October 2006
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 6 October 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 5 October 2006.
For the Commission
Jean-Luc Demarty
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 386/2005 (OJ L 62, 9.3.2005, p. 3).
--------------------------------------------------
ANNEX
to Commission Regulation of 5 October 2006 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code [1] | Standard import value |
07020000 | 052 | 74,1 |
096 | 17,5 |
999 | 45,8 |
07070005 | 052 | 114,4 |
999 | 114,4 |
07099070 | 052 | 88,2 |
999 | 88,2 |
08055010 | 052 | 51,2 |
388 | 64,6 |
524 | 55,3 |
528 | 57,0 |
999 | 57,0 |
08061010 | 052 | 89,3 |
400 | 151,0 |
624 | 137,8 |
999 | 126,0 |
08081080 | 388 | 87,6 |
400 | 91,0 |
508 | 76,2 |
512 | 87,1 |
720 | 74,9 |
800 | 146,7 |
804 | 102,6 |
999 | 95,2 |
08082050 | 052 | 117,0 |
388 | 80,3 |
720 | 56,3 |
999 | 84,5 |
[1] Country nomenclature as fixed by Commission Regulation (EC) No 750/2005 (OJ L 126, 19.5.2005, p. 12). Code "999" stands for "of other origin".
--------------------------------------------------
