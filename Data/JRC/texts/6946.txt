Summary of Community decisions on marketing authorizations in respect of medicinal products from 20 November 2005 to 30 November 2005
(Decisions taken pursuant to Article 34 of Directive 2001/83/EC [1] or Article 38 of Directive 2001/82/EC [2])
(2005/C 334/08)
Issuing, maintenance or modification of a national marketing authorisation
Date of the decision | Name(s) of the medicinal product | Holder(s) of the marketing authorization | Member State concerned | Date of notification |
28.11.2005 | Celocoxib | See Annex I | See Annex I | 29.11.2005 |
28.11.2005 | Etoricoxib | See Annex II | See Annex II | 29.11.2005 |
28.11.2005 | Lumiracoxib | See Annex III | See Annex III | 29.11.2005 |
[1] OJ L 311, 28.11.2001, p. 67.
[2] OJ L 311, 28.11.2001, p. 1.
--------------------------------------------------
ANNEX I
LIST OF THE NAMES, PHARMACEUTICAL FORMS, STRENGTHS OF THE MEDICINAL PRODUCTS, ROUTE OF ADMINISTRATION, AND MARKETING AUTHORISATION HOLDERS IN THE MEMBER STATES AND NORWAY AND ICELAND
Member State | Marketing Authorisation Holder | Invented name | Strength | Pharmaceutical Form | Route of administration |
Österreich | Pfizer Corporation Austria Ges.m.b.H. Seidengasse 33-35 A-1071 Wien | Celebrex | 100 mg | Capsules | Oral |
Pfizer Corporation Austria Ges.m.b.H. Seidengasse 33-35 A-1071 Wien | Celebrex | 200 mg | Capsules | Oral |
Pfizer Corporation Austria Ges.m.b.H. Seidengasse 33-35 A-1071 Wien | Solexa | 100 mg | Capsules | Oral |
Pfizer Corporation Austria Ges.m.b.H. Seidengasse 33-35 A-1071 Wien | Solexa | 200 mg | Capsules | Oral |
Belgique/België | Pharmacia NV/SA Rijksweg 12 B-2870 Puurs | Celebrex | 100 mg | Capsules | Oral |
Pharmacia NV/SA Rijksweg 12 B-2870 Puurs | Celebrex | 200 mg | Capsules | Oral |
Pharmacia NV/SA Rijksweg 12 B-2870 Puurs | Solexa | 100 mg | Capsules | Oral |
Pharmacia NV/SA Rijksweg 12 B-2870 Puurs | Solexa | 200 mg | Capsules | Oral |
Κύπρος | Markides & Vouros Ltd. 5a-B Pargas Str P.O.Box 22002 1515 Lefkosia Cyprus | Celebrex | 100 mg | Capsules | Oral |
Markides & Vouros Ltd. 5a-B Pargas Str P.O.Box 22002 1515 Lefkosia Cyprus | Celebrex | 200 mg | Capsules | Oral |
Česká republika | Pharmacia Ltd. Davy Avenue Know Hill Milton Keynes MK5 8PH United Kingdom | Celebrex | 100 mg | Capsules | Oral |
Pharmacia Ltd. Davy Avenue Know Hill Milton Keynes MK5 8PH United Kingdom | Celebrex | 200 mg | Capsules | Oral |
Danmark | Pfizer ApS Lautrupvang 8 DK-2750 Ballerup | Celebra | 100 mg | Capsules | Oral |
Pfizer ApS Lautrupvang 8 DK-2750 Ballerup | Celebra | 200 mg | Capsules | Oral |
Pfizer ApS Lautrupvang 8 DK-2750 Ballerup | Solexa | 100 mg | Capsules | Oral |
Pfizer ApS Lautrupvang 8 DK-2750 Ballerup | Solexa | 200 mg | Capsules | Oral |
Eesti | Pfizer H.C.P. Corporation Eesti Pirita tee 20 EE-10127 Tallinn | Celebrex | 100 mg | Capsules | Oral |
Pfizer H.C.P. Corporation Eesti Pirita tee 20 EE-10127 Tallinn | Celebrex | 200 mg | Capsules | Oral |
Suomi | Pfizer Oy Tietokuja 4 FIN-00330 Helsinki | Celebra | 100 mg | Capsules | Oral |
Pfizer Oy Tietokuja 4 FIN-00330 Helsinki | Celebra | 200 mg | Capsules | Oral |
Pfizer Oy Tietokuja 4 FIN-00330 Helsinki | Solexa | 100 mg | Capsules | Oral |
Pfizer Oy Tietokuja 4 FIN-00330 Helsinki | Solexa | 200 mg | Capsules | Oral |
France | Pfizer Holding France 23-25, avenue du Docteur Lannelongue F-75668 Paris Cedex 14 | Celebrex | 100 mg | Capsules | Oral |
Pfizer Holding France 23-25, avenue du Docteur Lannelongue F-75668 Paris Cedex 14 | Celebrex | 200 mg | Capsules | Oral |
Cardel 23-25, avenue du Docteur Lannelongue F-75668 Paris Cedex 14 | Solexa | 100 mg | Capsules | Oral |
Cardel 23-25, avenue du Docteur Lannelongue F-75668 Paris Cedex 14 | Solexa | 200 mg | Capsules | Oral |
Deutschland | Pharmacia GmbH Pfizerstr. 1 D-76139 Karlsruhe | Celebra | 200 mg | Capsules | Oral |
Pharmacia GmbH Pfizerstr. 1 D-76139 Karlsruhe | Celebra | 100 mg | Capsules | Oral |
Pharmacia GmbH Pfizerstr. 1 D-76139 Karlsruhe | Celebrex | 200 mg | Capsules | Oral |
Pharmacia GmbH Pfizerstr. 1 D-76139 Karlsruhe | Celebrex | 100 mg | Capsules | Oral |
Ελλάδα | Pfizer Hellas A.E. 5, Alketou Street GR-116 33 Athens | Celebrex | 100 mg | Capsules | Oral |
Pfizer Hellas A.E. 5, Alketou Street GR-116 33 Athens | Celebrex | 200 mg | Capsules | Oral |
Pfizer Hellas A.E. 5, Alketou Street GR-116 33 Athens | Aclarex | 100 mg | Capsules | Oral |
Pfizer Hellas A.E. 5, Alketou Street GR-116 33 Athens | Aclarex | 200 mg | Capsules | Oral |
Magyarország | Pfizer Kft. Alkotás utca 53. MOM Park "F" Épület H-1123 Budapest | Celebrex | 200 mg | Capsules | Oral |
Island | Pfizer ApS Lautrupvang 8 DK-2750 Ballerup | Celebra | 100 mg | Capsules | Oral |
Pfizer ApS Lautrupvang 8 DK-2750 Ballerup | Celebra | 200 mg | Capsules | Oral |
Ireland | Pharmacia Ireland Limited Airways Industrial Estate Dublin 17 Ireland | Celebrex | 100 mg | Capsules | Oral |
Pharmacia Ireland Limited Airways Industrial Estate Dublin 17 Ireland | Celebrex | 200 mg | Capsules | Oral |
Pharmacia Ireland Limited Airways Industrial Estate Dublin 17 Ireland | Solexa | 100 mg | Capsules | Oral |
Pharmacia Ireland Limited Airways Industrial Estate Dublin 17 Ireland | Solexa | 200 mg | Capsules | Oral |
Italia | Pharmacia Italia S.p.A. Via Robert Koch, 1-2 I-20152 Milano | Artilog | 100 mg | Capsules | Oral |
Pharmacia Italia S.p.A. Via Robert Koch, 1-2 I-20152 Milano | Artilog | 200 mg | Capsules | Oral |
Sefarma S.r.l. Via Robert Koch, 1-2 I-20152 Milano | Artrid | 100 mg | Capsules | Oral |
Sefarma S.r.l. Via Robert Koch, 1-2 I-20152 Milano | Artrid | 200 mg | Capsules | Oral |
Pfizer Italia S.r.l. Strada Statale 156 KM 50 I-04010 Borgo San Michele, Latina | Solexa | 100 mg | Capsules | Oral |
Pfizer Italia S.r.l. Strada Statale 156 KM 50 I-04010 Borgo San Michele, Latina | Solexa | 200 mg | Capsules | Oral |
Pharmacia Italia S.p.A. Via Robert Koch, 1-2 I-20152 Milano | Celebrex | 100 mg | Capsules | Oral |
Pharmacia Italia S.p.A. Via Robert Koch, 1-2 I-20152 Milano | Celebrex | 200 mg | Capsules | Oral |
Latvija | Pfizer Ltd. Ramsgate Road Sandwich Kent CT13 9NJ United Kingdom | Celebrex | 100 mg | Capsules | Oral |
Pfizer Ltd. Ramsgate Road Sandwich Kent CT13 9NJ United Kingdom | Celebrex | 200 mg | Capsules | Oral |
Lietuva | Pfizer H.C.P. Corporation Representation Office in Lithuania A. Gostauto 40 a LT-01112 Vilnius | Celebrex | 100 mg | Capsules | Oral |
Pfizer H.C.P. Corporation Representation Office in Lithuania A. Gostauto 40 a LT-01112 Vilnius | Celebrex | 200 mg | Capsules | Oral |
Luxembourg | Pharmacia NV/SA Rijksweg 12 B-2870 Puurs | Celebrex | 100 mg | Capsules | Oral |
Pharmacia NV/SA Rijksweg 12 B-2870 Puurs | Celebrex | 200 mg | Capsules | Oral |
Pharmacia NV/SA Rijksweg 12 B-2870 Puurs | Solexa | 100 mg | Capsules | Oral |
Pharmacia NV/SA Rijksweg 12 B-2870 Puurs | Solexa | 200 mg | Capsules | Oral |
Malta | Pfizer Hellas A. E. 5, Alketou Street GR-116 33 Athens | Celebrex | 100 mg | Capsules | Oral |
Pfizer Hellas A. E. 5, Alketou Street GR-116 33 Athens | Celebrex | 200 mg | Capsules | Oral |
Norge | Pfizer AS Postboks 3 N-1324 Lysaker | Celebra | 100 mg | Capsules | Oral |
Pfizer AS Postboks 3 N-1324 Lysaker | Celebra | 200 mg | Capsules | Oral |
Polska | Pharmacia Ltd. Ramsgate Road Sandwich Kent CT13 9NJ United Kingdom | Celebrex | 100 mg | Capsules | Oral |
Pharmacia Ltd. Ramsgate Road Sandwich Kent CT13 9NJ United Kingdom | Celebrex | 200 mg | Capsules | Oral |
Portugal | Laboratórios Pfizer, Lda. Lagoas Park, Edifício n.o 10 P-2740-244 Porto Salvo | Celebrex | 100 mg | Capsules | Oral |
Laboratórios Pfizer, Lda. Lagoas Park, Edifício n.o 10 P-2740-244 Porto Salvo | Celebrex | 200 mg | Capsules | Oral |
Lab. Medinfar — Produtos Farmacêuticos S.A. R. Manuel Ribeiro de Pavia, n.o 1 — Venda Nova P-2700-547 Amadora | Solexa | 100 mg | Capsules | Oral |
Lab. Medinfar — Produtos Farmacêuticos S.A. R. Manuel Ribeiro de Pavia, n.o 1 — Venda Nova P-2700-547 Amadora | Solexa | 200 mg | Capsules | Oral |
Slovenská republika | Pfizer Europe MA EEIG Ramsgate Road Sandwich Kent CT13 9NJ United Kingdom | Celebrex | 100 mg | Capsules | Oral |
Pfizer Europe MA EEIG Ramsgate Road Sandwich Kent CT13 9NJ United Kingdom | Celebrex | 200 mg | Capsules | Oral |
Slovenija | Pfizer Luxembourg SARL 283, route d'Arlon L-8011 Strassen | Celebrex | 100 mg | Capsules | Oral |
Pfizer Luxembourg SARL 283, route d'Arlon L-8011 Strassen | Celebrex | 200 mg | Capsules | Oral |
España | Pharmacia Spain S.A. Avenida de Europa, 20-B Parque Empresarial La Moraleja E-28108 Alcobendas, Madrid | Celebrex | 100 mg | Capsules | Oral |
Pharmacia Spain S.A. Avenida de Europa, 20-B Parque Empresarial La Moraleja E-28108 Alcobendas, Madrid | Artilog | 100 mg | Capsules | Oral |
Pharmacia Spain S.A. Avenida de Europa, 20-B Parque Empresarial La Moraleja E-28108 Alcobendas, Madrid | Celebrex | 200 mg | Capsules | Oral |
Pharmacia Spain S.A. Avenida de Europa, 20-B Parque Empresarial La Moraleja E-28108 Alcobendas, Madrid | Artilog | 200 mg | Capsules | Oral |
Sverige | Pfizer AB Nytorpsvägen 36 P.O. Box 501 S-183 25 Täby | Celebra | 100 mg | Capsules | Oral |
Pfizer AB Nytorpsvägen 36 P.O. Box 501 S-183 25 Täby | Celebra | 200 mg | Capsules | Oral |
Pfizer AB Nytorpsvägen 36 P.O. Box 501 S-183 25 Täby | Solexa | 100 mg | Capsules | Oral |
Pfizer AB Nytorpsvägen 36 P.O. Box 501 S-183 25 Täby | Solexa | 200 mg | Capsules | Oral |
Pfizer AB Nytorpsvägen 36 P.O. Box 501 S-183 25 Täby | Celora | 100 mg | Capsules | Oral |
Pfizer AB Nytorpsvägen 36 P.O. Box 501 S-183 25 Täby | Celora | 200 mg | Capsules | Oral |
Pfizer AB Nytorpsvägen 36 P.O. Box 501 S-183 25 Täby | Aclarix | 100 mg | Capsules | Oral |
Pfizer AB Nytorpsvägen 36 P.O. Box 501 S-183 25 Täby | Aclarix | 200 mg | Capsules | Oral |
Nederland | Pfizer bv Rivium Westlaan 142 2909 LD Capelle a/d IJssel Nederland | Celebrex | 100 mg | Capsules | Oral |
Pfizer bv Rivium Westlaan 142 2909 LD Capelle a/d IJssel Nederland | Celebrex | 200 mg | Capsules | Oral |
Pfizer bv Rivium Westlaan 142 2909 LD Capelle a/d IJssel Nederland | Solexa | 100 mg | Capsules | Oral |
Pfizer bv Rivium Westlaan 142 2909 LD Capelle a/d IJssel Nederland | Solexa | 200 mg | Capsules | Oral |
United Kingdom | Pfizer Ltd. Ramsgate Road Sandwich Kent CT13 9NJ United Kingdom | Celebrex | 100 mg | Capsules | Oral |
Pfizer Ltd. Ramsgate Road Sandwich Kent CT13 9NJ United Kingdom | Celebrex | 200 mg | Capsules | Oral |
Pfizer Ltd. Ramsgate Road Sandwich Kent CT13 9NJ United Kingdom | Solexa | 100 mg | Capsules | Oral |
Pfizer Ltd. Ramsgate Road Sandwich Kent CT13 9NJ United Kingdom | Solexa | 200 mg | Capsules | Oral |
--------------------------------------------------
ANNEX II
LIST OF THE NAMES, PHARMACEUTICAL FORMS, STRENGTHS OF THE MEDICINAL PRODUCTS, ROUTE OF ADMINISTRATION, AND MARKETING AUTHORISATION HOLDERS IN THE MEMBER STATES AND NORWAY AND ICELAND
Member State | Marketing Authorisation Holder | Invented name | Strength | Pharmaceutical Form | Route of administration |
Belgique/België | Merck Sharp & Dohme BV Chaussée de Waterloo, 1135 B-1180 Bruxelles | Arcoxia | 60 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme BV Chaussée de Waterloo, 1135 B-1180 Bruxelles | Arcoxia | 90 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme BV Chaussée de Waterloo, 1135 B-1180 Bruxelles | Arcoxia | 120 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme BV Chaussée de Waterloo, 1135 B-1180 Bruxelles | Ranacox | 60 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme BV Chaussée de Waterloo, 1135 B-1180 Bruxelles | Ranacox | 90 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme BV Chaussée de Waterloo, 1135 B-1180 Bruxelles | Ranacox | 120 mg | Film-coated tablet | Oral use |
Κύπρος | Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Arcoxia | 60 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Arcoxia | 90 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Arcoxia | 120 mg | Film-coated tablet | Oral use |
Danmark | Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Arcoxia | 60 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Arcoxia | 90 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Arcoxia | 120 mg | Film-coated tablet | Oral use |
Deutschland | MSD Sharp & Dohme GmbH Lindenplatz 1 D-85540 Haar | Etoricoxib MSD | 120 mg | Film-coated tablet | Oral use |
MSD Sharp & Dohme GmbH Lindenplatz 1 D-85540 Haar | Etoricoxib MSD | 90 mg | Film-coated tablet | Oral use |
MSD Sharp & Dohme GmbH Lindenplatz 1 D-85540 Haar | Etoricoxib MSD | 60 mg | Film-coated tablet | Oral use |
MSD Sharp & Dohme GmbH Lindenplatz 1 D-85540 Haar | Auxib | 120 mg | Film-coated tablet | Oral use |
MSD Sharp & Dohme GmbH Lindenplatz 1 D-85540 Haar | Auxib | 90 mg | Film-coated tablet | Oral use |
MSD Sharp & Dohme GmbH Lindenplatz 1 D-85540 Haar | Auxib | 60 mg | Film-coated tablet | Oral use |
MSD Sharp & Dohme GmbH Lindenplatz 1 D-85540 Haar | Arcoxia | 120 mg | Film-coated tablet | Oral use |
MSD Sharp & Dohme GmbH Lindenplatz 1 D-85540 Haar | Arcoxia | 90 mg | Film-coated tablet | Oral use |
MSD Sharp & Dohme GmbH Lindenplatz 1 D-85540 Haar | Arcoxia | 60 mg | Film-coated tablet | Oral use |
Eesti | Merck Sharp & Dohme OÜ Peterburi tee 46 EE-11415 Tallinn | Arcoxia | 60 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme OÜ Peterburi tee 46 EE-11415 Tallinn | Arcoxia | 90 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme OÜ Peterburi tee 46 EE-11415 Tallinn | Arcoxia | 120 mg | Film-coated tablet | Oral use |
Suomi | Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Arcoxia | 60 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Arcoxia | 90 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Arcoxia | 120 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Turox | 60 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Turox | 90 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Turox | 120 mg | Film-coated tablet | Oral use |
Ελλάδα | Vianex SA Tatoiou Street 18 Km Athens-Lamia National Road GR-14671 Nea Erythrea — Athene | Arcoxia | 60 mg | Film-coated tablet | Oral use |
Vianex SA Tatoiou Street 18 Km Athens-Lamia National Road GR-14671 Nea Erythrea — Athene Griekenland | Arcoxia | 90 mg | Film-coated tablet | Oral use |
Vianex SA Tatoiou Street 18 Km Athens-Lamia National Road GR-14671 Nea Erythrea — Athene | Arcoxia | 120 mg | Film-coated tablet | Oral use |
Vianex SA Tatoiou Street 18 Km Athens-Lamia National Road GR-14671 Nea Erythrea — Athene | Turox | 60 mg | Film-coated tablet | Oral use |
Vianex SA Tatoiou Street 18 Km Athens-Lamia National Road GR-14671 Nea Erythrea — Athene | Turox | 90 mg | Film-coated tablet | Oral use |
Vianex SA Tatoiou Street 18 Km Athens-Lamia National Road GR-14671 Nea Erythrea — Athene | Turox | 120 mg | Film-coated tablet | Oral use |
Magyarország | MSD Hungary Ltd. Alkotás utca 50 H-1123 Budapest | Arcoxia | 60 mg | Film-coated tablet | Oral use |
MSD Hungary Ltd. Alkotás utca 50 H-1123 Budapest | Arcoxia | 90 mg | Film-coated tablet | Oral use |
MSD Hungary Ltd. Alkotás utca 50 H-1123 Budapest | Arcoxia | 120 mg | Film-coated tablet | Oral use |
Ireland | Merck Sharp & Dohme Ltd Hertford Road Hoddesdon Hertfordshire EN11 9BU United Kingdom | Arcoxia | 60 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme Ltd Hertford Road Hoddesdon Hertfordshire EN11 9BU United Kingdom | Arcoxia | 90 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme Ltd Hertford Road Hoddesdon Hertfordshire EN11 9BU United Kingdom | Arcoxia | 120 mg | Film-coated tablet | Oral use |
Island | Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Arcoxia | 60 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Arcoxia | 90 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Arcoxia | 120 mg | Film-coated tablet | Oral use |
Italia | Merck Sharp & Dohme Italia SpA Via G. Fabbroni, 6 I-00191 Roma | Arcoxia | 60 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme Italia SpA Via G. Fabbroni, 6 I-00191 Roma | Arcoxia | 90 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme Italia SpA Via G. Fabbroni, 6 I-00191 Roma | Arcoxia | 120 mg | Film-coated tablet | Oral use |
Istituto Gentili SpA Via Benedetto Croce, 37 I-56125 Pisa | Algix | 60 mg | Film-coated tablet | Oral use |
Istituto Gentili SpA Via Benedetto Croce, 37 I-56125 Pisa | Algix | 90 mg | Film-coated tablet | Oral use |
Istituto Gentili SpA Via Benedetto Croce, 37 I-56125 Pisa | Algix | 120 mg | Film-coated tablet | Oral use |
Neopharmed SpA Via G. Fabbroni, 6 I-00191 Roma | Recoxib | 60 mg | Film-coated tablet | Oral use |
Neopharmed SpA Via G. Fabbroni, 6 I-00191 Roma | Recoxib | 90 mg | Film-coated tablet | Oral use |
Neopharmed SpA Via G. Fabbroni, 6 I-00191 Roma | Recoxib | 120 mg | Film-coated tablet | Oral use |
Addenda Pharma Srl Via dei Santi Pietro e Paolo, 30 I-00144 Roma | Tauxib | 60 mg | Film-coated tablet | Oral use |
Addenda Pharma Srl Via dei Santi Pietro e Paolo, 30 I-00144 Roma | Tauxib | 90 mg | Film-coated tablet | Oral use |
Addenda Pharma Srl Via dei Santi Pietro e Paolo, 30 I-00144 Roma | Tauxib | 120 mg | Film-coated tablet | Oral use |
Latvija | SIA "Merck Sharp & Dohme Latvija" Skanstes iela 13 LV-1013 Riga | Arcoxia | 60 mg | Film-coated tablet | Oral use |
SIA "Merck Sharp & Dohme Latvija" Skanstes iela 13 LV-1013 Riga | Arcoxia | 90 mg | Film-coated tablet | Oral use |
SIA "Merck Sharp & Dohme Latvija" Skanstes iela 13 LV-1013 Riga | Arcoxia | 120 mg | Film-coated tablet | Oral use |
Lietuva | Merck Sharp & Dohme UAB, Lithuania Gelezinio Vilko 18A LT-01112 Vilnius | Arcoxia | 60 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme UAB, Lithuania Gelezinio Vilko 18A LT-01112 Vilnius | Arcoxia | 90 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme UAB, Lithuania Gelezinio Vilko 18A LT-01112 Vilnius | Arcoxia | 120 mg | Film-coated tablet | Oral use |
Luxembourg | Merck Sharp & Dohme SA Chaussée de Waterloo 1135 B-1180 Bruxelles | Arcoxia | 60 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme SA Chaussée de Waterloo 1135 B-1180 Bruxelles | Arcoxia | 90 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme SA Chaussée de Waterloo 1135 B-1180 Bruxelles | Arcoxia | 120 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme SA Chaussée de Waterloo 1135 B-1180 Bruxelles | Ranacox | 60 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme SA Chaussée de Waterloo 1135 B-1180 Bruxelles | Ranacox | 90 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme SA Chaussée de Waterloo 1135 B-1180 Bruxelles | Ranacox | 120 mg | Film-coated tablet | Oral use |
Malta | Merck Sharp & Dohme Ltd Hertford Road Hoddesdon Hertfordshire EN11 9BU United Kingdom | Arcoxia | 60 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme Ltd Hertford Road Hoddesdon Hertfordshire EN11 9BU United Kingdom | Arcoxia | 90 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme Ltd Hertford Road Hoddesdon Hertfordshire EN11 9BU United Kingdom | Arcoxia | 120 mg | Film-coated tablet | Oral use |
Nederland | Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Arcoxia | 60 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Arcoxia | 90 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Arcoxia | 120 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Auxib | 60 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Auxib | 90 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Auxib | 120 mg | Film-coated tablet | Oral use |
Norge | Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Arcoxia | 60 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Arcoxia | 90 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Arcoxia | 120 mg | Film-coated tablet | Oral use |
Österreich | Merck Sharp & Dohme GmbH Donau-City Straße 6 A-1220 Wien | Arcoxia | 60 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme GmbH Donau-City Straße 6 A-1220 Wien | Arcoxia | 90 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme GmbH Donau-City Straße 6 A-1220 Wien | Arcoxia | 120 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme GmbH Donau-City Straße 6 A-1220 Wien | Auxib | 60 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme GmbH Donau-City Straße 6 A-1220 Wien | Auxib | 90 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme GmbH Donau-City Straße 6 A-1220 Wien | Auxib | 120 mg | Film-coated tablet | Oral use |
Polska | MSD Polska Sp. z.o.o. ul. Puławska 303 PL-02-785 Warszawa | Arcoxia | 60 mg | Film-coated tablet | Oral use |
MSD Polska Sp. z.o.o. ul. Puławska 303 PL-02-785 Warszawa | Arcoxia | 90 mg | Film-coated tablet | Oral use |
MSD Polska Sp. z.o.o. ul. Puławska 303 PL-02-785 Warszawa | Arcoxia | 120 mg | Film-coated tablet | Oral use |
Portugal | Merck Sharp & Dohme, Lda Quinta Da Fonte Edifício Vasco da Gama, n.o 19, Porto Salvo P-2770-192 Paço d'Arcos | Arcoxia | 60 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme, Lda Quinta Da Fonte Edifício Vasco da Gama, n.o 19, Porto Salvo P-2770-192 Paço d'Arcos | Arcoxia | 90 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme, Lda Quinta Da Fonte Edifício Vasco da Gama, n.o 19, Porto Salvo P-2770-192 Paço d'Arcos | Arcoxia | 120 mg | Film-coated tablet | Oral use |
Laboratórios BIAL-Portela S.A. Av. Da Siderurgia Nacional P-4745-457 S. Mamede do Coronado | Exxiv | 60 mg | Film-coated tablet | Oral use |
Laboratórios BIAL-Portela S.A. Av. Da Siderurgia Nacional P-4745-457 S. Mamede do Coronado | Exxiv | 90 mg | Film-coated tablet | Oral use |
Laboratórios BIAL-Portela S.A. Av. Da Siderurgia Nacional P-4745-457 S. Mamede do Coronado | Exxiv | 120 mg | Film-coated tablet | Oral use |
Farmacox-Companhia Farmacêutica, Lda. Quinta Da Fonte Edifício Vasco da Gama, n.o 19, Porto Salvo P-2770-192 Paço d'Arcos | Turox | 60 mg | Film-coated tablet | Oral use |
Farmacox-Companhia Farmacêutica, Lda. Quinta Da Fonte Edifício Vasco da Gama, n.o 19, Porto Salvo P-2770-192 Paço d'Arcos | Turox | 90 mg | Film-coated tablet | Oral use |
Farmacox-Companhia Farmacêutica, Lda. Quinta Da Fonte Edifício Vasco da Gama, n.o 19, Porto Salvo P-2770-192 Paço d'Arcos | Turox | 120 mg | Film-coated tablet | Oral use |
Slovenija | Merck Sharp & Dohme Ltd. Šmartinska cesta 140 SI-1000 Ljubljana | Arcoxia | 60 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme Ltd. Šmartinska cesta 140 SI-1000 Ljubljana | Arcoxia | 90 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme Ltd. Šmartinska cesta 140 SI-1000 Ljubljana | Arcoxia | 120 mg | Film-coated tablet | Oral use |
Slovenská republika | Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Arcoxia | 60 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Arcoxia | 90 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Arcoxia | 120 mg | Film-coated tablet | Oral use |
España | Merck Sharp & Dohme de España S.A. Josefa Valcárcel 38 E-Madrid 28027 | Arcoxia | 60 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme de España S.A. Josefa Valcárcel 38 E-Madrid 28027 | Arcoxia | 90 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme de España S.A. Josefa Valcárcel 38 E-Madrid 28027 | Arcoxia | 120 mg | Film-coated tablet | Oral use |
Laboratorios Abelló S.A. Josefa Valcárcel 38 E-Madrid 28027 | Exxiv | 60 mg | Film-coated tablet | Oral use |
Laboratorios Abelló S.A. Josefa Valcárcel 38 E-Madrid 28027 | Exxiv | 90 mg | Film-coated tablet | Oral use |
Laboratorios Abelló S.A. Josefa Valcárcel 38 E-Madrid 28027 | Exxiv | 120 mg | Film-coated tablet | Oral use |
Česká republika | Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Arcoxia | 60 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Arcoxia | 90 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Arcoxia | 120 mg | Film-coated tablet | Oral use |
United Kingdom | Merck Sharp & Dohme Ltd Hertford Road Hoddesdon Hertfordshire EN11 9BU United Kingdom | Arcoxia | 60 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme Ltd Hertford Road Hoddesdon Hertfordshire EN11 9BU United Kingdom | Arcoxia | 90 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme Ltd Hertford Road Hoddesdon Hertfordshire EN11 9BU United Kingdom | Arcoxia | 120 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme Ltd Hertford Road Hoddesdon Hertfordshire EN11 9BU United Kingdom | Auxib | 60 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme Ltd Hertford Road Hoddesdon Hertfordshire EN11 9BU United Kingdom | Auxib | 90 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme Ltd Hertford Road Hoddesdon Hertfordshire EN11 9BU United Kingdom | Auxib | 120 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme Ltd Hertford Road Hoddesdon Hertfordshire EN11 9BU United Kingdom | Exxiv | 60 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme Ltd Hertford Road Hoddesdon Hertfordshire EN11 9BU United Kingdom | Exxiv | 90 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme Ltd Hertford Road Hoddesdon Hertfordshire EN11 9BU United Kingdom | Exxiv | 120 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme Ltd Hertford Road Hoddesdon Hertfordshire EN11 9BU United Kingdom | Turox | 60 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme Ltd Hertford Road Hoddesdon Hertfordshire EN11 9BU United Kingdom | Turox | 90 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme Ltd Hertford Road Hoddesdon Hertfordshire EN11 9BU United Kingdom | Turox | 120 mg | Film-coated tablet | Oral use |
Sverige | Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Arcoxia | 60 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Arcoxia | 90 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Arcoxia | 120 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Turox | 60 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Turox | 90 mg | Film-coated tablet | Oral use |
Merck Sharp & Dohme BV Waarderweg 39 Postbus 581 2003 PC Haarlem Nederland | Turox | 120 mg | Film-coated tablet | Oral use |
--------------------------------------------------
ANNEX III
LIST OF THE NAMES, PHARMACEUTICAL FORMS, STRENGTHS OF THE MEDICINAL PRODUCTS, ROUTE OF ADMINISTRATION, AND MARKETING AUTHORISATION HOLDERS IN THE MEMBER STATES AND NORWAY AND ICELAND
Member State | Marketing Authorisation Holder | Invented name | Strength | Pharmaceutical Form | Route of administration |
United Kingdom | Novartis Pharmaceuticals UK Ltd Frimley Business Park Frimley Camberley Surrey GU16 7SR United Kingdom | Prexige | 100 mg | Film-coated tablet | Oral |
Novartis Pharmaceuticals UK Ltd Frimley Business Park Frimley Camberley Surrey GU16 7SR United Kingdom | Prexige | 200 mg | Film-coated tablet | Oral |
Novartis Pharmaceuticals UK Ltd Frimley Business Park Frimley Camberley Surrey GU16 7SR United Kingdom | Prexige | 400 mg | Film-coated tablet | Oral |
Novartis Pharmaceuticals UK Ltd Frimley Business Park Frimley Camberley Surrey GU16 7SR United Kingdom | Lumiracoxib | 100 mg | Film-coated tablet | Oral |
Novartis Pharmaceuticals UK Ltd Frimley Business Park Frimley Camberley Surrey GU16 7SR United Kingdom | Lumiracoxib | 200 mg | Film-coated tablet | Oral |
Novartis Pharmaceuticals UK Ltd Frimley Business Park Frimley Camberley Surrey GU16 7SR United Kingdom | Lumiracoxib | 400 mg | Film-coated tablet | Oral |
Novartis Pharmaceuticals UK Ltd Frimley Business Park Frimley Camberley Surrey GU16 7SR United Kingdom | Frexocel | 100 mg | Film-coated tablet | Oral |
Novartis Pharmaceuticals UK Ltd Frimley Business Park Frimley Camberley Surrey GU16 7SR United Kingdom | Frexocel | 200 mg | Film-coated tablet | Oral |
Novartis Pharmaceuticals UK Ltd Frimley Business Park Frimley Camberley Surrey GU16 7SR United Kingdom | Frexocel | 400 mg | Film-coated tablet | Oral |
Novartis Pharmaceuticals UK Ltd Frimley Business Park Frimley Camberley Surrey GU16 7SR United Kingdom | Stellige | 100 mg | Film-coated tablet | Oral |
Novartis Pharmaceuticals UK Ltd Frimley Business Park Frimley Camberley Surrey GU16 7SR United Kingdom | Stellige | 200 mg | Film-coated tablet | Oral |
Novartis Pharmaceuticals UK Ltd Frimley Business Park Frimley Camberley Surrey GU16 7SR United Kingdom | Stellige | 400 mg | Film-coated tablet | Oral |
Novartis Pharmaceuticals UK Ltd Frimley Business Park Frimley Camberley Surrey GU16 7SR United Kingdom | Exforge | 100 mg | Film-coated tablet | Oral |
Novartis Pharmaceuticals UK Ltd Frimley Business Park Frimley Camberley Surrey GU16 7SR United Kingdom | Exforge | 200 mg | Film-coated tablet | Oral |
Novartis Pharmaceuticals UK Ltd Frimley Business Park Frimley Camberley Surrey GU16 7SR United Kingdom | Exforge | 400 mg | Film-coated tablet | Oral |
--------------------------------------------------
