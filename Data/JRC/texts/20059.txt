COMMISSION DECISION of 20 March 1995 concerning the implementation of the Annex to Council Regulation (EEC) No 2930/86 defining the characteristics of fishing vessels (95/84/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2930/86 of 22 September 1986 defining the characteristics of fishing vessels (1), as amended by Regulation (EC) No 3259/94 (2), and in particular Article 4 thereof and the Annex thereto,
Whereas it is appropriate to take into account the situations in the different Member States regarding the procedures and methods for the measurement of the tonnage of their fleets;
Whereas the new deadlines agreed by the Council to ensure the remeasurement of small fishing vessels with a length of less than 24 metres must be used to ensure a gradual and balanced implementation of the formulae and operations linked to the remeasurement; whereas it is therefore appropriate that annual intermediate objectives should be fixed to ensure that those operations are gradually accomplished;
Whereas in the meantime it is necessary to have estimates of the gross tonnage of the Community fleet made available before the deadline fixed for the mid-term review of the multiannual guidance programmes for the period 1993-1996 defined by Commission decisions (3),
Whereas rationalization at the level of each Member State and harmonization at Community level of the units of tonnage measurement are amongst the conditions for the effective implementation of the multiannual guidance programmes for the period 1993-1996, under the transparent conditions which are needed for supervising compliance with the objectives;
Whereas the measures provided for in this Decision are in accordance with the opinion of the Management Committee for Fisheries and Aquaculture,
HAS ADOPTED THIS DECISION:
Article 1
The values of the functions a1, a2 and a3 set out in Annex I to Regulation (EEC) No 2930/86 are fixed in accordance with the tables in Annex I to this Decision.
Article 2
Each Member State will forward to the Commission before 15 March 1995 the lists of vessels by segment of their multiannual guidance programme, together with their gross tonnages (GT), measured, in the case of vessels of more than 24 metres, between perpendiculars which do not undertake international voyages, and estimated, in the case of the vessels of less than 24 metres in length, between perpendiculars.
Article 3
The compilation of the parameters of the 1969 London Convention and their application to small vessels of less than 15 metres in overall length for which the parameters Bi and Ti are not available will be carried out progressively in accordance with the timetable set out in Annex 2.
The effective remeasurement of vessels from 15 metres in overall length to 24 metres in length between perpendiculars according to the provisions of the London Convention shall be carried out progressively in accordance with the timetable set out in Annex 3.
For this purpose each Member State may draw up lists of vessels selected on the basis of their age or other relevant parameters so that this timetable can be adhered to.
Article 4
The lists of vessels concerned by Articles 2 and 3 as well as the data on the gross tonnages attached to these lists are communicated to the Commission in accordance with Articles 3, 8 and 9 of Commission Regulation (EC) No 109/94 (4). The internal number of each vessel will be communicated with these lists.
Any modification of the physical characteristics of a vessel, when such modification is carried out during the remeasurement period and when such modification is likely to result in a change in the GT tonnage for the purposes of the London Convention, shall be the subject of a communication from the Member State concerned to the Commission, giving the tonnage values before and after modification.
For vessels having an overall length of less than 15 metres, those tonnage values shall be calculated using the formulae specified by Council Regulation (EC) No 3259/94 and Annex I to this Decision. For vessels having an overall length of 15 metres or more, those tonnage values shall be calculated in accordance with the provisions of the London Convention.
Article 5
This Decision is addressed to the Member States.
Done at Brussels, 20 March 1995.
For the Commission
Emma BONINO
Member of the Commission
(1) OJ No L 274, 25. 9. 1986, p. 1.
(2) OJ No L 339, 29. 12. 1994, p. 11.
(3) OJ No L 401, 31. 12. 1992, p. 3 ff.
(4) OJ No L 19, 22. 1. 1994, p. 5.
ANNEX I
VALUES OF THE FUNCTIONS a1, a2 AND a3
1. New vessels of overall length less than 15 metres and existing vessels of overall length less than 15 metres for which London Convention parameters are available
a1 = 0,5194 + 0,0145 Loa but not to be taken less than 0,60
where Loa is the overall length as defined in Article 2 of Regulation (EEC) No 2930/86.
2. Existing vessels of overall length less than 15 metres for which the London Convention parameters are not available
a2 = 0,4974 + 0,0255 Loa but not to be taken less than 0,60
For Spain, Greece and Italy, which do not have available the Oslo Convention parameters required to estimate gross tonnage using a2 in the formular specified by Council Regulation (EC) No 3259/94, the following provisions will apply:
- Spain:
Oslo Convention parameters will be estimated on the basis of the national parameters currently available.
- Greece, Italy:
The formula to estimate gross tonnage will be developed by Greece and Italy based on the national parameters currently available.
Spain, Italy and Greece will progressively substitute the estimates of tonnage so obtained by tonnage calculated using the function a1 according to the rate set down in Annex 2 for the application of the London Convention parameters to this category of vessels.
3. Existing vessels between 15 metres overall length and 24 metres length between perpendiculars
a3 = 0,3097 Bi/Ti + Loa (Y - 1900)/2767-C but not to be taken less than 0,65 or more than 1,45
where Y is the year of construction, not to be taken less than 1950, and where B1 and T1 are the hull breadth and depth respectively as defined by the Oslo Convention. The additive adjustment C takes the following values:
>TABLE>
For Spain and Italy, which do not have available the Oslo Convention parameters required to estimate gross tonnage using a3 in the formular specified by Council Regulation (EC) No 3259/94, the following provisions will apply:
- Spain:
Oslo Convention parameters will be estimated on the basis of the national parameters currently available.
- Italy:
The formula to estimate gross tonnage will be developed by Italy based on the national parameters currently available.
The estimates and formulae developed by Spain, Greece and Italy described in paragraphs 2 and 3 are subject to the agreement of the Commission and will be available for inspection by other Member States.
4. Exceptions
The gross tonnage of vessels whose form or constructional features are such as to render the application of these formulae unreasonable or impracticable (catamarans, trimarans and other vessels of novel design) shall be determined by the administration of the Member State concerned. Where the tonnage is so determined, the administration shall communicate to the Commission details of the method used for that purpose. These will be made available for examination by other Member States.
ANNEX II
PROGRAMME FOR THE APPLICATION OF THE LONDON CONVENTION PARAMETERS TO SMALL VESSELS BETWEEN 0 AND 15 METRES OVERALL LENGTH FOR WHICH THE PARAMETERS Bi AND Ti ARE NOT AVAILABLE
The number (n) of vessels to be remeasured is communicated to the Commission within the two months following the adoption of the present Decision.
The application of the London Convention parameters to the vessels concerned by this Annex is ensured according to the following rate:
- by 31 December 1995 at least 25 % of n,
- by 31 December 1996 at least 50 % of n,
- by 31 December 1997 at least 75 % of n,
- by 31 December 1998 100 % of n.
ANNEX III
PROGRAMME FOR THE REMEASUREMENT OF VESSELS BETWEEN 15 METRES OVERALL LENGTH AND 24 METRES LENGTH BETWEEN PERPENDICULARS
The number of vessels to be remeasured (N) is communicated to the Commission within the two months following the adoption of the present Decision.
The effective remeasurement of the vessels concerned by this Annex is ensured according to the following rate:
- by 31 December 1997 at least 33 % of N,
- by 31 December 1999 at least 55 % of N,
- by 31 December 2001 at least 77 % of N,
- by 31 December 2003 100 % of N.
