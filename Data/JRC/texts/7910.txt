*****
COUNCIL DIRECTIVE
of 7 June 1990
on the freedom of access to information on the environment
(90/313/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 130s thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Considering the principles and objectives defined by the action programmes of the European Communities on the environment of 1973 (4), 1977 (5) and 1983 (6), and more particularly the action programme of 1987 (7), which calls, in particular, for devising 'ways of improving public access to information held by environmental authorities';
Whereas the Council of the European Communities and the representatives of the Governments of the Member States, meeting within the Council, declared in their resolution of 19 October 1987 on the continuation and implementation of a European Community policy and action programme on the environment (1987 to 1992) (8) that it was important, in compliance with the respective responsibilities of the Community and the Member States, to concentrate Community action on certain priority areas, including better access to information on the environment;
Whereas the European Parliament stressed, in its opinion on the fourth action programme of the European Communities on the environment (9), that 'access to information for all must be made possible by a specific Community programme';
Whereas access to information on the environment held by public authorities will improve environmental protection;
Whereas the disparities between the laws in force in the Member States concerning access to information on the environment held by public authorities can create inequality within the Community as regards access to information and/or as regards conditions of competition;
Whereas it is necessary to guarantee to any natural or legal person throughout the Community free access to avaiblable information on the environment in written, visual, aural or data-base form held by public authorities, concerning the state of the environment, activities or measures adversely affecting, or likely so to affect the environment, and those designed to protect it;
Whereas, in certain specific and clearly defined cases, it may be justified to refuse a request for information relating to the environment;
Whereas a refusal by a public authority to forward the information requested must be justified;
Whereas it must be possible for the applicant to appeal against the public authority's decision;
Whereas access to information relating to the environment held by bodies with public responsibilities for the environment and under the control of public authorities should also be ensured;
Whereas, as part of an overall strategy to disseminate information on the environment, general information should actively be provided to the public on the state of the environment;
Whereas the operation of this Directive should be subject to a review in the light of the experience gained,
HAS ADOPTED THIS DIRECTIVE:
Article 1
The object of this Directive is to ensure freedom of access to, and dissemination of, information on the environment held by public authorities and to set out the basic terms and conditions on which such information should be made available.
Article 2
For the purposes of this Directive:
(a) 'information relating to the environment' shall mean any available information in written, visual, aural or data-base form on the state of water, air, soil, fauna, flora, land and natural sites, and on activities (including those which give rise to nuisances such as noise) or measures adversely affecting, or likely so to affect these, and on activities or measures designed to protect these, including administrative measures and environmental management programmes;
(b) 'public authorities' shall mean any public administration at national, regional or local level with responsibilities, and possessing information, relating to the environment with the exception of bodies acting in a judicial or legislative capacity.
Article 3
1. Save as provided in this Article, Member States shall ensure that public authorities are required to make available information relating to the environment to any natural or legal person at his request and without his having to prove an interest.
Member States shall define the practical arrangements under which such information is effectively made available.
2. Member States may provide for a request for such information to be refused where it affects:
- the confidentiality of the proceedings of public authorities, international relations and national defence,
- public security,
- matters which are, or have been, sub judice, or under enquiry (including disciplinary enquiries), or which are the subject of preliminary investigation proceedings,
- commercial and industrial confidentiality, including intellectual property,
- the confidentiality of personal data and/or files,
- material supplied by a third party without that party being under a legal obligation to do so,
- material, the disclosure of which would make it more likely that the environment to which such material related would be damaged.
Information held by public authorities shall be supplied in part where it is possible to separate out information on items concerning the interests referred to above.
3. A request for information may be refused where it would involve the supply of unfinished documents or data or internal communications, or where the request is manifestly unreasonable or formulated in too general a manner.
4. A public authority shall respond to a person requesting information as soon as possible and at the latest within two months. The reasons for a refusal to provide the information requested must be given.
Article 4
A person who considers that his request for information has been unreasonably refused or ignored, or has been inadequately answered by a public authority, may seek a judicial or administrative review of the decision in accordance with the relevant national legal system.
Article 5
Member States may make a charge for supplying the information, but such charge may not exceed a reasonable cost.
Article 6
Member States shall take the necessary steps to ensure that information relating to the environment held by bodies with public responsibilities for the environment and under the control of public authorities is made available on the same terms and conditions as those set out in Articles 3, 4 and 5 either via the competent public authority or directly by the body itself.
Article 7
Member States shall take the necessary steps to provide general information to the public on the state of environment by such means as the periodic publication of descriptive reports.
Article 8
Four years after the date referred to in Article 9 (1), the Member States shall report to the Commission on the experience gained in the light of which the Commission shall make a report to the European Parliament and the Council together with any proposal for revision which it may consider appropriate. Article 9
1. Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive by 31 December 1992 at the latest. They shall forthwith inform the Commission thereof.
2. Member States shall communicate to the Commission the main provisions of national law which they adopt in the field governed by this Directive.
Article 10
This Directive is addressed to the Member States.
Done at Luxembourg, 7 June 1990.
For the Council
The President
P. FLYNN
(1) OJ No C 335, 30. 12. 1988, p. 5.
(2) OJ No C 120, 16. 5. 1989, p. 231.
(3) OJ No C 139, 5. 6. 1989, p. 47.
(4) OJ No C 112, 20. 12. 1973, p. 1.
(5) OJ No C 139, 13. 6. 1977, p. 1.
(6) OJ No C 46, 17. 2. 1983, p. 1.
(7) OJ No C 70, 18. 3. 1987, p. 3.
(8) OJ No C 289, 29. 10. 1987, p. 3.
(9) OJ No C 156, 15. 6. 1987, p. 138.
