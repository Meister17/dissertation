COUNCIL DECISION
of 15 March 1993
concerning the conclusion of the Agreements in the form of exchanges of letters between the European Economic Community, of the one part, and the Republic of Austria, the Republic of Finland, the Republic of Iceland, the Kingdom of Norway and the Kingdom of Sweden, of the other part, on the provisional application of the Agreements on certain arrangements in the field of agriculture, signed by the said parties in Oporto on 2 May 1992
(93/239/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 113 thereof,
Having regard to the proposal from the Commission,
Whereas, during the discussion on the adjustment of the Agreement on the European Economic Area (EEA) following the decision of the Swiss Confederation not to ratify that Agreement, it proved possible to negotiate Agreements in the form of exchanges of letters between the European Economic Community, of the one part, and the Republic of Austria, the Republic of Finland, the Republic of Iceland, the Kingdom of Norway and the Kingdom of Sweden, of the other part, on the provisional application, as from 15 April 1993, of the bilateral agreements on certain arrangements in the field of agriculture, signed by the same parties in Oporto on 2 May 1992;
Whereas the Agreements in the form of exchanges of letters refer to Article 15 of the Free Trade Agreements between the European Economic Community and each of those countries;
Whereas the Agreements in question should be approved,
HAS DECIDED AS FOLLOWS:
Article 1
The Agreements in the form of exchanges of letters between the European Economic Community, of the one part, and the Republic of Austria, the Republic of Finland, the Republic of Iceland, the Kingdom of Norway and the Kingdom of Sweden, of the other part, on the provisional application as from 15 April 1993 of the bilateral agreements on certain arrangements in the field of agriculture, signed by the said parties in Oporto on 2 May 1992 (1) are hereby approved by the Community.
The texts of the Agreements are attached to this Decision.
Article 2
The President of the Council is hereby authorized to designate the person empowered to sign the Agreements in order to bind the Community.
Done at Brussels, 15 March 1993.
For the Council
The President
M. JELVED
(1) See pages 2, 18, 32, 43 and 59 of this Official Journal.
COUNCIL DECISION
of 15 March 1993
concerning the conclusion of the Agreements in the form of exchanges of letters between the European Economic Community, of the one part, and the Republic of Austria, the Republic of Finland, the Republic of Iceland, the Kingdom of Norway and the Kingdom of Sweden, of the other part, on the provisional application of the Agreements on certain arrangements in the field of agriculture, signed by the said parties in Oporto on 2 May 1992
(93/239/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 113 thereof,
Having regard to the proposal from the Commission,
Whereas, during the discussion on the adjustment of the Agreement on the European Economic Area (EEA) following the decision of the Swiss Confederation not to ratify that Agreement, it proved possible to negotiate Agreements in the form of exchanges of letters between the European Economic Community, of the one part, and the Republic of Austria, the Republic of Finland, the Republic of Iceland, the Kingdom of Norway and the Kingdom of Sweden, of the other part, on the provisional application, as from 15 April 1993, of the bilateral agreements on certain arrangements in the field of agriculture, signed by the same parties in Oporto on 2 May 1992;
Whereas the Agreements in the form of exchanges of letters refer to Article 15 of the Free Trade Agreements between the European Economic Community and each of those countries;
Whereas the Agreements in question should be approved,
HAS DECIDED AS FOLLOWS:
Article 1
The Agreements in the form of exchanges of letters between the European Economic Community, of the one part, and the Republic of Austria, the Republic of Finland, the Republic of Iceland, the Kingdom of Norway and the Kingdom of Sweden, of the other part, on the provisional application as from 15 April 1993 of the bilateral agreements on certain arrangements in the field of agriculture, signed by the said parties in Oporto on 2 May 1992 (1) are hereby approved by the Community.
The texts of the Agreements are attached to this Decision.
Article 2
The President of the Council is hereby authorized to designate the person empowered to sign the Agreements in order to bind the Community.
Done at Brussels, 15 March 1993.
For the Council
The President
M. JELVED
(1) See pages 2, 18, 32, 43 and 59 of this Official Journal.
