Commission Regulation (EC) No 896/2005
of 15 June 2005
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 16 June 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 15 June 2005.
For the Commission
J. M. Silva Rodríguez
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 1947/2002 (OJ L 299, 1.11.2002, p. 17).
--------------------------------------------------
ANNEX
to Commission Regulation of 15 June 2005 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code [1] | Standard import value |
07020000 | 052 | 56,6 |
204 | 75,2 |
999 | 65,9 |
07070005 | 052 | 90,9 |
999 | 90,9 |
07099070 | 052 | 84,4 |
999 | 84,4 |
08055010 | 324 | 59,0 |
382 | 70,4 |
388 | 53,0 |
528 | 61,3 |
624 | 69,2 |
999 | 62,6 |
08081080 | 388 | 84,5 |
400 | 80,5 |
404 | 90,5 |
508 | 67,6 |
512 | 70,5 |
524 | 70,5 |
528 | 72,3 |
720 | 59,3 |
804 | 92,4 |
999 | 76,5 |
08091000 | 052 | 176,5 |
624 | 183,0 |
999 | 179,8 |
08092095 | 052 | 291,5 |
400 | 398,9 |
999 | 345,2 |
08093010, 08093090 | 052 | 204,6 |
999 | 204,6 |
08094005 | 052 | 127,3 |
999 | 127,3 |
[1] Country nomenclature as fixed by Commission Regulation (EC) No 750/2005 (OJ L 126, 19.5.2005, p. 12). Code "999" stands for "of other origin".
--------------------------------------------------
