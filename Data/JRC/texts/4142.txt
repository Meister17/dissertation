Notice of the expiry of certain countervailing measures
(2005/C 110/10)
Further to the publication of a notice of impending expiry [1], following which no request for a review was received, the Commission gives notice that the countervailing measures mentioned below will shortly expire.
This notice is published in accordance with Article 18(4) of Regulation (EC) No 2026/97 [2] of 6 October 1997 on protection against subsidised imports from countries not members of the European Community.
Product | Country(ies) of origin or exportation | Measures | Reference | Date of expiry |
Synthetic fibres of polyester | AustraliaIndonesia | Countervailing duty | Council Regulation (EC) No 978/2000 (OJ L 113, 12.5.2000, p. 1) as last amended by Regulation (EC) No 902/2001 (OJ L 127, 9.5.2001, p. 20) | 13.5.2005 |
[1] OJ C 254, 14.10.2004, p. 2.
[2] OJ L 288, 21.10.1997, p. 1. Regulation as last amended by Council Regulation (EC) No 461/2004 (OJ L 77, 13.3.2004, p. 12).
--------------------------------------------------
