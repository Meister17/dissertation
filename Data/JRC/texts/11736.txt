Prior notification of a concentration
(Case COMP/M.4025 — ATEL/EOSH)
(2006/C 21/03)
(Text with EEA relevance)
1. On 20 January 2006, the Commission received a notification of a proposed concentration pursuant to Article 4 of Council Regulation (EC) No 139/2004 [1] by which the undertaking NewCo, resulting from the restructuring of the undertakings Motor-Columbus AG ("MC", Switzerland) and Aare-Tessin AG ("ATEL", Switzerland), acquires within the meaning of Article 3(1)(b) of the Council Regulation control of all the operational assets and activities of the undertaking EOS Holding ("EOSH assets", Switzerland) as well as specifically identified electricity assets currently belonging to the EDF group ("EDF Swiss assets", Switzerland) by way of purchase of share and assets.
2. The business activities of the undertakings concerned are:
- for MC: holding company;
- for ATEL: predominantly generation and wholesale supply of electricity;
- for the EOSH assets: generation and wholesale supply of electricity;
- for the EDF Swiss assets: generation and wholesale supply of electricity.
3. On preliminary examination, the Commission finds that the notified transaction could fall within the scope of Regulation (EC) No 139/2004. However, the final decision on this point is reserved.
4. The Commission invites interested third parties to submit their possible observations on the proposed operation to the Commission.
Observations must reach the Commission not later than 10 days following the date of this publication. Observations can be sent to the Commission by fax (No (32-2) 296 43 01 or 296 72 44) or by post, under reference number COMP/M.4025 — ATEL/EOSH, to the following address:
European Commission
Competition DG
Merger Registry
J-70
B-1049 Brussels
[1] OJ L 24, 29.1.2004, p. 1.
--------------------------------------------------
