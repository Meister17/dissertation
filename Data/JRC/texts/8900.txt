COUNCIL REGULATION (EEC) No 110/76 of 19 January 1976 laying down general rules for granting export refunds on fishery products and criteria for fixing the amount of such refunds
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 100/76 of 19 January 1976 on the common organization of the market in fishery products (1), and in particular Article 23 (3) thereof,
Having regard to the proposal from the Commission,
Whereas Article 23 (1) of Regulation (EEC) No 100/76 allows an export refund to be fixed, to the extent necessary to enable economically important exports of the products listed in Article 1 (2) of that Regulation to be effected;
Whereas export refunds on products subject to the common organization of the market in fishery products must be fixed in accordance with certain criteria which would make it possible to cover the difference between prices for those products within the Community and those ruling in international trade, while respecting the general aims of the common organization ; whereas to this end the supply situation and prices for those products within the Community and prices ruling in international trade must be taken into account ; whereas provision should be made for the possibility of any coefficients that may be fixed being taken into account when the amount of the refund on fishery products is being calculated;
Whereas if price trends are to be noted prices must be determined in accordance with general principles ; whereas to this end prices on third country markets and in countries of destination, producer prices recorded in third countries and free-at-Community-frontier prices should be taken into account when prices on the world market are being determined ; whereas prices ruling on the representative markets of the Community and prices ruling on exportation should be used as a basis in determining Community prices;
Whereas provision must be made for varying the amount of the refund according to the destination of the products, in view of the special conditions which apply to imports in certain countries of destination;
Whereas fish caught by Community producers is of Community origin even if it is landed in ports situated outside the customs territory of the Community ; whereas, however, to make verification possible, refunds should be granted only in respect of fish landed in ports situated within the customs territory of the Community as defined by Regulation (EEC) No 1496/68 (2), as amended by the Act of Accession (3);
Whereas to ensure Community exporters some stability in the amount of the refund and certainty with regard to the list of products eligible for a refund, it should be possible for the list and the amounts to remain valid for a relatively long period, which would be determined in accordance with normal trade practice;
Whereas to avoid distortions of competition between Community traders, the administrative conditions under which they operate must be identical throughout the Community ; whereas there is no justification for granting a refund where the products in question are imported from third countries and re-exported to third countries,
HAS ADOPTED THIS REGULATION:
Article 1
This Regulation lays down general rules for fixing and granting export refunds on the products listed in Article 1 (2) of Regulation (EEC) No 100/76. (1)See page 1 of this Official Journal. (2)OJ No L 238, 28.9.1968, p. 1. (3)OJ No L 73, 27.3.1972, p. 14.
Article 2
The following shall be taken into account when refunds are being fixed: (a) the existing situation and the future trend with regard to: - prices and availabilities of fishery products on the Community market,
- prices for fishery products on the world market;
(b) the aims of the common organization of the market in fishery products, which are to ensure equilibrium and the natural development of prices and trade on this market;
(c) the minimum marketing and transport costs from Community markets to ports or other points of export in the Community, as well as costs incurred in placing the goods on the markets of the countries of destination ; and
(d) the economic significance of the proposed exports.
Article 3
1. When prices on the Community market are being determined, account shall be taken of the ruling prices which are most favourable from the exportation point of view.
2. The following shall be taken into account when prices on the world market are being determined; (a) prices ruling on the markets of the main importing third countries;
(b) producer prices recorded in the main exporting third countries ; and
(c) free-at-Community-frontier prices.
Article 4
Where the world market situation or the specific requirements of certain markets make this necessary, the refund may be varied according to the destination of the products in question.
Article 5
The list of products on which an export refund is granted and the amount of such refund shall be fixed at least once every three months.
Article 6
No refund shall be granted on products of Community origin which are landed, direct from the fishing grounds, in ports situated outside the customs territory of the Commission.
Article 7
1. The refund shall be paid upon proof: - that the products have been exported from the Community, and
- that the products are of Community origin.
2. Where Article 4 applies, the refund shall be paid under the conditions laid down in paragraph 1 of this Article, provided it is proved that the product has reached the destination for which the refund was fixed.
Exceptions may be made to this rule in accordance with the procedure referred to in paragraph 3 of this Article, provided that conditions are laid down which offer equivalent guarantees.
3. Additional provisions may be adopted in accordance with the procedure laid down in Article 32 of Regulation (EEC) No 100/76.
Article 8
1. Council Regulation (EEC) No 165/71 of 26 January 1971 laying down general rules for granting export refunds on fishery products and criteria for fixing the amount of such refunds (1), is hereby repealed.
2. References to the Regulation repealed by virtue of paragraph 1 shall be construed as references to this Regulation.
Article 9
This Regulation shall enter into force on 1 February 1976.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 19 January 1976.
For the Council
The President
J. HAMILIUS (1)OJ No L 23, 29.1.1971, p. 1.
