COMMISSION REGULATION (EURATOM) No 3956/92 of 21 December 1992 on the conclusion by the European Atomic Energy Community of an Agreement establishing an International Science and Technology Centre between the United States of America, Japan, the Russian Federation and, acting as one Party, the European Atomic Energy Community and the European Economic Community
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Atomic Energy Community and in particular the second paragraph of Article 101 thereof,
Whereas the Agreement establishing an International Science and Technology Centre between the United States of America, Japan, the Russian Federation and, acting as one Party, the European Atomic Energy Community and the European Economic Community was signed on 27 November 1992; whereas by Decision of 14 December 1992 the Council approved the said Agreement for the purposes of conclusion by the Commission on behalf of the European Atomic Energy Community;
Whereas the Agreement should be concluded on behalf of the European Atomic Energy Community,
HAS ADOPTED THIS REGULATION:
Article 1
The Agreement establishing an International Science and Technology Centre between the United States of America, Japan, the Russian Federation and, acting as one Party, the European Atomic Energy Community and the European Economic Community together with the Community declaration relating to Article 1 are hereby approved on behalf of the European Atomic Energy Community.
The texts of the Agreement and the Declaration are annexed to this Regulation (1).
Article 2
The President of the Commission shall give on behalf of the European Atomic Energy Community the notification provided for in Article XVIII of the Agreement.
Article 3
One Representative of the Community on the Governing Board shall be appointed by each of the Council and the Commission pursuant to Article IV (C) of the Agreement.
Article 4
The International Science and Technology Centre shall have legal personality and enjoy the most extensive legal capacity accorded to legal persons under laws applicable in the Community and, in particular, may contract, acquire or dispose of movable and immovable property and be a party to legal proceedings.
Article 5
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 21 December 1992.
For the Commission
Jacques DELORS
President
(1) See page 3 of the Official Journal.
