Euro exchange rates [1]
5 October 2006
(2006/C 241/06)
| Currency | Exchange rate |
USD | US dollar | 1,2721 |
JPY | Japanese yen | 149,45 |
DKK | Danish krone | 7,4565 |
GBP | Pound sterling | 0,67610 |
SEK | Swedish krona | 9,2895 |
CHF | Swiss franc | 1,5887 |
ISK | Iceland króna | 86,30 |
NOK | Norwegian krone | 8,3780 |
BGN | Bulgarian lev | 1,9558 |
CYP | Cyprus pound | 0,5767 |
CZK | Czech koruna | 28,211 |
EEK | Estonian kroon | 15,6466 |
HUF | Hungarian forint | 274,40 |
LTL | Lithuanian litas | 3,4528 |
LVL | Latvian lats | 0,6961 |
MTL | Maltese lira | 0,4293 |
PLN | Polish zloty | 3,9361 |
RON | Romanian leu | 3,5304 |
SIT | Slovenian tolar | 239,60 |
SKK | Slovak koruna | 37,150 |
TRY | Turkish lira | 1,9090 |
AUD | Australian dollar | 1,7019 |
CAD | Canadian dollar | 1,4321 |
HKD | Hong Kong dollar | 9,9059 |
NZD | New Zealand dollar | 1,9154 |
SGD | Singapore dollar | 2,0135 |
KRW | South Korean won | 1207,48 |
ZAR | South African rand | 9,9818 |
CNY | Chinese yuan renminbi | 10,0548 |
HRK | Croatian kuna | 7,3878 |
IDR | Indonesian rupiah | 11716,04 |
MYR | Malaysian ringgit | 4,6864 |
PHP | Philippine peso | 63,605 |
RUB | Russian rouble | 34,0500 |
THB | Thai baht | 47,816 |
[1] Source: reference exchange rate published by the ECB.
--------------------------------------------------
