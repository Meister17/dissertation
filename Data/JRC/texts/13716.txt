Commission Regulation (EC) No 1557/2006
of 18 October 2006
laying down detailed rules for implementing Council Regulation (EC) No 1952/2005 as regards registration of contracts and the communicaiton of data concerning hops
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1952/2005 of 23 November 2005 concerning the common organisation of the market in hops and repealing Regulations (EEC) No 1696/71, (EEC) No 1037/72, (EEC) No 879/73 and (EEC) No 1981/82 [1], and in particular the fourth and fifth indents of Article 17 thereof,
Whereas:
(1) Following the adoption of Regulation (EC) No 1952/2005, in the interests of clarity and logic, Commission Regulation (EEC) No 776/73 of 20 March 1973 on registration of contracts and communication of data with respect to hops [2] should be repealed and replaced by a new Regulation.
(2) Article 14 of Regulation (EC) No 1952/2005 requires any contract to supply hops produced within the Community concluded between a producer or an association of producers and a buyer to be registered. The rules for such registration should consequently be laid down.
(3) Deliveries made under contracts concluded in advance, referred to in Article 14(2) of Regulation (EC) No 1952/2005, might not comply with the agreed terms, particularly as regards quantities. Deliveries under those contracts must therefore also be registered so as to have accurate information on the disposal of hops.
(4) In order to facilitate the registration of contracts concluded in advance, it should be laid down that they must be concluded in writing and notified to the bodies designated by each Member State.
(5) In the absence of other supporting documents, contracts other than those concluded in advance should be registered on the basis of duplicates of the receipted invoices for the deliveries made.
(6) Article 15 of Regulation (EC) No 1952/2005 lays down that the Member States and the Commission must send each other such information as is necessary for the implementation of that Regulation. Rules should be laid down for sending that information.
(7) Hops are no longer grown in Ireland and therefore, in the interests of clarity and logic, Commission Regulation (EEC) No 1375/75 of 29 May 1975 on the provisions of recognition of producer groups for hops in Ireland [3] should be repealed.
(8) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Hops,
HAS ADOPTED THIS REGULATION:
Article 1
Only contracts for hops harvested in the Member State concerned shall be registered under Article 14(1) of Regulation (EC) No 1952/2005.
Article 2
The body designated by the Member State in accordance with Article 14(1) of Regulation (EC) No 1952/2005 shall register all deliveries made, distinguishing between contracts concluded in advance, referred to in Article 14(2) of that Regulation, and other contracts.
Article 3
Contracts concluded in advance shall be concluded in writing. A copy of each contract concluded in advance shall be communicated by the producer or recognised producer group to the body referred to in Article 2 within one month of its conclusion.
Article 4
The registration of contracts other than those concluded in advance shall be on the basis of a duplicate of the receipted invoice to be sent by the seller to the body referred to in Article 2.
The seller may send such duplicates either as and when deliveries are made or all together, but must in any event send them by 15 March.
Article 5
For each harvest, the Member States shall send the Commission the information detailed in the Annex by electronic means by 15 April of the year following the harvest concerned.
Article 6
Regulations (EEC) Nos 776/73 and 1375/75 are hereby repealed.
Article 7
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 18 October 2006.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 317, 3.12.2005, p. 29.
[2] OJ L 74, 22.3.1973, p. 14. Regulation as last amended by Regulation (EC) No 1516/77 (OJ L 169, 7.7.1977, p. 12).
[3] OJ L 139, 30.5.1975, p. 27.
--------------------------------------------------
ANNEX
HOPS: Contracts concluded in advance and harvest statement
Information to be sent to the Commission by 15 April of the year following that of the harvest concerned
Harvest:
Member State:
| Bitter hops | Aromatic hops | Total |
(1) | (2) | (3) | (4) |
+++++ TIFF +++++
+++++ TIFF +++++
1.QUANTITY OF HOPS COVERED BY CONTRACTS CONCLUDED IN ADVANCE FOR THE HARVEST CONCERNED (tonnes) | | | |
2.DELIVERIES OF HOPS:
2.1.Under contracts concluded in advance
2.1.1.Quantity delivered (tonnes) | | | |
2.1.2.Average price [1] (EUR/kg [2]) | | | |
2.2.Under other contracts
2.2.1.Quantity delivered (tonnes) | | | |
2.2.2.Average price [1] (EUR/kg [2]) | | | |
2.3.Total quantity delivered (tonnes) | | | |
3.QUANTITY OF HOPS REMAINING UNSOLD (tonnes) | | | |
4.ALPHA ACID:
4.1.Alpha-acid production (tonnes) | | | |
4.2.Average alpha-acid content (%) | | | |
5.AREA DOWN TO HOPS (hectares):
5.1.Total area harvested | | | |
5.2.Total area newly sown (year of harvest) | | | |
+++++ TIFF +++++
+++++ TIFF +++++
6.NUMBER OF FARMERS GROWING HOPS | | | |
+++++ TIFF +++++
+++++ TIFF +++++
7.QUANTITY OF HOPS COVERED BY CONTRACTS CONCLUDED IN ADVANCE FOR THE NEXT HARVEST (tonnes) | | | |
[1] Farm-gate price.
[2] Member States using their national currency must use the conversion rate applicable on 1 January of the year following the harvest.
--------------------------------------------------
