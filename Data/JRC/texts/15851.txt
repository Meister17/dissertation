Decision of the EEA Joint Committee
No 28/2006
of 10 March 2006
amending Annex II (Technical regulations, standards, testing and certification) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex II to the Agreement was amended by Decision of the EEA Joint Committee No 144/2005 of 2 December 2005 [1].
(2) Certain review clauses in Chapter XV of Annex II to the Agreement need to be revised as some are extended and others deleted,
HAS DECIDED AS FOLLOWS:
Article 1
Chapter XV of Annex II to the Agreement shall be amended as follows:
1. In point 1 (Council Directive 67/548/EEC), the adaptation texts (c) and (d) shall be replaced by the following:
"(c) The following provisions shall not apply to Norway:
(i) Article 30, in conjunction with Articles 4 and 5, with respect to the requirements for the classification, labelling and/or specific concentration limits for the substances or groups of substances listed in Annex I to the Directive and shown in the following list. Norway may require the use of different classification, labelling and/or specific concentration limits for these substances;
Name | CAS No | Index No | Einecs |
n-hexane | 110-54-3 | 601-037-00-0 | 203-777-6 |
acrylamide | 79-06-1 | 616-003-00-0 | 201-173-7 |
(ii) Article 30, in conjunction with Articles 4 and 6, with respect to the requirements for the classification, labelling and/or specific concentration limits for the substances or group of substances not listed in Annex I to the Directive shown in the following list. Norway may require the use of different classification, labelling and/or specific concentration limits for these substances;
Name | CAS No | Index No | Einecs |
methylacrylamidoglykolat (content between 0,1 % and 0,01 % acrylamide) | 77402-05-2 | [NOR-UNN-02-91] | 403-230-3 |
methylacrylamidometoksyacetat (content between 0,1 % and 0,01 % acrylamide) | 77402-03-0 | [NOR-UNN-03-01] | 401-890-7 |
(iii) For substances covered by adaptation (c)(i) above, the provisions of Article 23(2) of the Directive, requiring the use of the words "EC-label";
(iv) The Contracting Parties agree on the objective that the provisions of the Community acts on dangerous substances and preparations should apply by 1 July 2007. Pursuant to cooperation in order to solve remaining problems, a review of the situation will take place during 2006, including matters not covered by the Community legislation. If an EFTA State concludes that it will need any derogation from the Community acts relating to classification and labelling, the latter shall not apply to it unless the EEA Joint Committee agrees on another solution."
2. In the review clause of point 4 (Council Directive 76/769/EEC) the words "mercury compounds", "pentachlorophenol" and "cadmium" shall be deleted.
3. In the review clause of point 4 (Council Directive 76/769/EEC) the word "2005" shall be replaced by the word "2009".
4. In the review clause of point 10 (Commission Directive 91/155/EEC), the words " 1 July 2005" shall be replaced by the words " 1 July 2007" and the word "2004" shall be replaced by the word "2006".
5. In point 12r (Directive 1999/45/EC of the European Parliament and the Council) the adaptation texts (d) and (e) shall be replaced by the following:
"(d) The following provisions shall not apply to Norway:
(i) Article 18, in conjunction with Articles 6 and 10, with respect to preparations containing substances as defined in point 1(c)(i) and (ii).
(ii) The Contracting Parties agree on the objective that the provisions of the Community acts on dangerous substances and preparations should apply by 1 July 2007. Pursuant to cooperation in order to solve remaining problems, a review of the situation will take place during 2006, including matters not covered by the Community legislation. If an EFTA State concludes that it will need any derogation from the Community acts relating to classification and labelling, the latter shall not apply to it unless the EEA Joint Committee agrees on another solution."
Article 2
This Decision shall enter into force on 11 March 2006, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [2].
Article 3
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 10 March 2006.
For the EEA Joint Committee
The President
R. Wright
[1] OJ L 53, 23.2.2006, p. 40.
[2] No constitutional requirements indicated.
--------------------------------------------------
