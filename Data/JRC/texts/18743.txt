COMMISSION DECISION of 14 April 1993 laying down the methods to be used for detecting residues of substances having a hormonal or a thyrostatic action
(93/256/EEC)THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Directive 85/358/EEC of 16 July 1985 supplementing Directive 81/602/EEC concerning the prohibition of certain substances having a hormonal or a thyrostatic action (1), as last amended by Directive 88/146/EEC (2), and in particular Article 5 (2) thereof,
Whereas Article 8 (1) of Council Directive 64/433/EEC of 26 June 1964 on health problems affecting intra-Community trade in fresh meat (3), as last amended by Directive 92/5/EEC (4), and the second subparagraph of Article 11 (4) of Council Directive 85/397/EEC of 5 August 1985 on health and animal-health problems affecting intra-Community trade in heat-treated milk (5), as last amended by Directive 89/662/EEC (6), provide that examinations for residues are to be carried out in accordance with proven methods which are scientifically recognized, in particular those laid down in Community directives or other international standards;
Whereas the determination of methods of sample analysis includes definition of the analytical procedures to be followed, the rules to be observed when drawing samples and the criteria to be applied when carrying out analyses;
Whereas the analytical procedures adopted must be sufficiently sensitive to detect the presence of residues of substances having a hormonal or a thyrostatic action;
Whereas sampling is an essential part of the method of analysis; whereas rules for the drawing of samples should therefore be laid down;
Whereas, for the purposes of this Decision, account should be taken of the criteria set out in point 1 of the Annex to Council Directive 85/591/EEC of 20 December 1985 concerning the introduction of Community methods of sampling and analysis for the monitoring of foodstuffs intended for human consumption (7);
Whereas, taking into account developments in scientific and technical knowledge, and for the sake of clarity, it is necessary to revoke Commission Decision 87/410/EEC (8);
Whereas the measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
The routine analytical procedures authorized for detecting residues of substances having a hormonal or a thyrostatic action shall be the following:
- immunoassay,
- thin-layer chromatography,
- liquid chromatography,
- gas chromatography,
- mass spectrometry,
- spectrometry,
or any other method which fulfils comparable criteria to those laid down for related methods in the Annex.
Article 2
Samples for analysis shall be taken in accordance with the following rules:
1. the sample must be representative and of sufficient size to allow adequate analysis and to allow a repeat analysis and any confirmatory analysis;
2. the samples must be marked in such a way that identification remains possible at all times;
3. the sampling procedure, packaging, preservation, transport and storage of the samples must be such as to maintain their integrity and not prejudice the result of the examination. Unauthorized access to the samples must be prevented.
Article 3
The criteria applicable to routine methods for the analysis of residues of substances having a hormonal or a thyrostatic action are set out in the Annex.
Article 4
This Decision shall re-examined before 1 January 1996 in order to take account of developments in scientific and technical knowledge.
Article 5
Commission Decision 87/410/EEC is hereby revoked.
Article 6
This Decision is addressed to the Member States.
Done at Brussels, 14 April 1993.
For the Commission
René STEICHEN
Member of the Commission
(1) OJ No L 191, 23. 7. 1985, p. 46.
(2) OJ No L 70, 16. 3. 1988, p. 16.
(3) OJ No 121, 29. 7. 1964, p. 2012/64.
(4) OJ No L 57, 2. 3. 1992, p. 1.
(5) OJ No L 226, 24. 8. 1985, p. 13.
(6) OJ No L 395, 30. 12. 1989, p. 13.
(7) OJ No L 372, 31. 12. 1985, p. 50.
(8) OJ No L 223, 11. 8. 1987, p. 18.
ANNEX
1. DEFINITIONS AND GENERAL REQUIREMENTS
1.1. Definitions
1.1.1. Routine methods of analysis
These are methods of analysis used by Member States to implement national plans for the control of residues in food-producing animals and their products in compliance with Council Directive 86/469/EEC (1). Routine methods must have been validated by operational laboratories, and must fulfil the relevant criteria set out in this Annex. They may be used for screening and/or conformity pruposes:
- methods used for screening purposes (screening methods) are methods which are used to detect the presence of an analyte or class of analytes at the level of interest. These methods have a high sample throughput capacity and care used to sift large numbers of samples for potential positives. They are aimed at avoiding false negative results,
- methods used for confirmatory purposes (confirmatory methods) are methods which provide full or complementary information enabling the analyte to be identified unequivocally at the level of interest. These methods are aimed at preventing false positive results as well as having an acceptable low probability of false negative results.
1.1.2. Analyte
This is a component of a test sample which has to be detected, identified and/or quantified. The term 'analyte' includes, where appropriate, derivatives formed from the analyte during the analysis.
A quantitative measures of an analyte has to be reported as:
- an amount, expressed as a mass quantity (e. g. mg, ng)
or
- a content, expressed as a mass fraction (e.g. mg kg 1, ng kg 1), a mass concentration (e. g. mg l 1) or a concentration (e. g. mol l 1).
1.1.3. Samples
1.1.3.1. Laboratory sample
This is a sample as prepared for sending to the laboratory and intended for inspection or testing.
1.1.3.2. Test sample
This is a sample prepared from the laboratory sample and from which test portions will be taken.
1.1.3.3. Test portion
This is the quantity of material drawn from the test sample (or, if both are the same, from the laboratory sample) and on which the test or observation is actually carried out.
1.1.4. Standard analyte
This is a well-defined substance of declared analyte content in its highest available purity to be used as a reference in the analysis.
1.1.5. Reference material
This is a material of which one, or several, properties have been confirmed by a validated method, so that it can be used to calibrate an apparatus or to verify a method of measurement.
1.1.6. Blank determinations
1.1.6.1. Sample blank determination
This is the complete analytical procedure applied to a test portion taken from a sample from which the analyte is absent.
1.1.6.2. Reagent blank determination
This is the complete analytical procedure applied with omission of the test portion or using an equivalent amount of suitable solvent in place of the test portion.
1.1.7. Specificity
Specificity is the ability of a method to distinguish between the analyte being measured and other substances. This characteristic is predominantly a function of the measuring principle used, but can vary according to class of compound or matrix.
Details concerning specificity must relate at least to any substances which might be expected to give rise to a signal when the measuring principle described is used, e. g. homologues, analogues, metabolic products of the residue of interest. From the details concerning specificity it must be possible to derive quantitatively the extent to which the method can distinguish between the analyte and the other substances under the experimental conditions.
1.1.8. Accuracy
In this Decision this refers to accuracy of the mean. The definition which shall be used is laid down in ISO 3534-1977 under 2.83 (Accuracy of the mean: the closeness of agreement between the true value and the mean result which would be obtained by applying the experimental procedure a very large number of times).
The principal limitations on accuracy are:
(a) random errors;
(b) systematic errors.
For a very large number of experiments, the accuracy of the mean approaches the systematic error. For desk review of a method, the number of experiments must be specified.
The measure of accuracy is the difference between the mean value measured for a reference material and its true value, expressed as a percentage of the true value. If no reference material is available, relevant parameters may be evaluated by analysing fortified sample material.
In cases where neither absolute defining methods nor certified reference materials are available, the analyte content of a sample may be defined by the results obtained with the aid of a method which exhibits a high degreee of specificity, accuracy and precision for the analyte.
1.1.9. Precision
This is the closeness of agreement between the results obtained by applying the experimental procedure several times under prescribed conditions (ISO 3534-1977 (2), 2.84), and covers repeatability and reproducibility.
Repeatability:
The closeness of agreement between mutually independent test results obtained under repeatability conditions, i.e. with the same method on identical test material in the same laboratory by the same operator using the same equipment within short intervals of time.
Reproducibility:
The closeness of agreement between mutually independent test results obtained under reproducibility conditions, i.e. with the same method on identical test material in different laboratories with different operators using different equipment.
According to the Annex to Council Directive 85/591/EEC (3) the precision values for methods of analysis which are to be considered for adoption under the provisions of that Directive shall be obtained from a collaborative trial which has preferably been conducted in accordance with ISO 5725-1986 (4). For this purpose, the terms repeatability and reproducibility are defined in ISO 5725-1986. For conducting such trials, sample materials of known analyte content ranging around the maximum residue limit to be enforced shall be used.
Until such time as the reproducibility of a method has been established by a collaborative trial, then for the purpose of pre-selection of candidate methods by desk review, it is sufficient that data on repeatability are available.
The measure of repeatability and reproducibility to be used is the coefficient of variation as defined in ISO 3534-1977, 2.35 (Coefficient of variation: the ratio of the standard deviation to the absolute value of the arithmetic mean).
1.1.10. Limit of detection
This is the smallest measured content from which it is possible to deduce the presence of the analyte with reasonable statistical certainty (at least 95 % for unauthorized substances - see 1.2.6.1).
The limit of detection can be calculated using different approaches:
(a) one way is to carry out sample blank determinations on at least 20 representative blank samples. The limit of detection is calculated as the apparent content corresponding to the value of the mean plus three times the standard deviation for the blank determinations.
Note 1: The amount of test portion typically used in the analysis should be specified.
Note 2: If it is to be expected that factors such as species, sex, age, feeding or other environmental factors may influence the characteristics of a method, then a set of at least 20 blank samples is required for each individual homogeneous population to which the method is to be applied;
(b) alternatively, in the case of spectrometric determinations in which representative blank determinations give white noise only, the limit of detection is calculated as the apparent content corresponding to three times the peak-to-peak noise.
1.1.11. Limit of determination
This is the smallest analyte content for which the method has been validated with specified accuracy and precision.
1.1.12. Sensitivity
This is a measure of the ability of a method to discriminate between differences in analyte content. In this Decision, sensitivity is calculated as the slope of the calibration curve at the level of interest.
1.1.13. Practicability
This is a characteristic of an analytical procedure which is dependent on the scope of the method and is determined by requirements such as sample throughput and costs.
1.1.14. Applicability
This is a list of the sample materials and/or analytes to which the method can be applied as presented or with specified minor modifications.
1.1.15. Interpretation of results
1.1.15.1. Positive result
The presence of the analyte in the sample is proved, according to the analytical procedure, when the general criteria, and the criteria specified for the individual detection method, are fulfilled.
(a) For substances with a zero tolerance, the result of the analysis is 'positive' if the identity of the analyte in the sample is proved unambiguously;
(b) For substances with an established maximum residue limit, the result of the analysis is 'positive' if the experimentally determined content of the analyte in the sample, (after applying any correction for recovery), is greater than that established maximum residue limit, which takes into account the acceptable probability of obtaining false positive or false negative results.
1.1.15.2. Negative result
The result of the analysis is regarded as 'negative' according to the analytical procedure, when the general criteria and the criteria specified for the individual detection method are fulfilled in the case of appropriate reference materials and blank determination and:
(a) in the case of substances for which there is a zero tolerance, the identity of the analyte has not been proved unambiguously; or,
(b) in the case of substances with an established maximum residue limit, the measured content of the analyte in the sample is below the level specified in 1.1.15.1 (b) above. Note: A negative result does not prove in case (a) that the analyte is absent from the sample, or in case (b) that the true content of the analyte is below the maximum residue limit.
1.1.16. Co-chromatography
This is a procedure in which the purified test solution prior to the chromatographic step(s) is divided into two parts and:
(a) one part is chromatographed as such;
(b) the standard analyte that is to be identified is added to the other part, and this mixed solution of test solution and standard analyte is chromatographed. The amount of added standard analyte has to be similar to the estimated amount of the analyte in the test solution.
1.1.17. Immunogram
In this Decision, an immunogram is defined as a graphical plot of immunochemical response versus retention time or elution volume as obtained from chromatographic separation with (in general off-line) immunochemical detection of the components of the sample extract.
1.2. General requirements
1.2.1. Criteria
In accordance with the Annex to Council Directive 85/591/EEC, the criteria set out below shall apply to the examination of methods of analysis.
1.2.2. Screening methods
Fixed requirements cannot be set for screening methods. The most important aspect of performance is that the incidence of false negative results at the level of interest must be minimal.
1.2.2.1. Specificity must be defined.
1.2.2.2. Accuracy and precision:
Quantification may not be necessary. Depending on whether the use of a substance is prohibited or authorised, a screening method may be qualitative or quantitative. False positive results are acceptable but false negative results at the level of interest should be minimal.
1.2.2.3. Limit of detection:
This should be appropriate for the purpose. For substances with an established maximum residue limit, it must be sufficiently low to detect residues at this level. For substances which are not authorized for use in food-producing animals, the limit of detection should be as low as possible.
1.2.2.4. Practicability:
A high sample throughput capability is desirable and costs should be low.
1.2.3. Confirmatory methods
1.2.3.1. Specificity:
As far as possible, confirmatory methods must provide unambiguous information on the chemical structure of the analyte. When more than one compound gives the same response, then the method cannot discriminate between these compounds.
Methods based only on chromatographic analysis without the use of molecular spectrometric detection are not suitable for use as confirmatory methods.
If a single technique lacks sufficient specificity, the desired specificity may be achieved by analytical procedures consisting of suitable combinations of clean-up, chromatographic separation(s) and spectrometric or immunochemical detection, e.g. GC-MS, LC-MS, IAC/GC-MS, GC-IR, LC-IR, LC/IMG.
1.2.3.2. Accuracy
In the case of repeated analysis of a reference material, the guideline ranges for the deviation of the mean experimentally determined content (after applying any correction for recovery) from the true value are as follows:
In the case of repeated analysis of a reference material under reproducibility conditions, typical values for the inter-laboratory coefficient of variation (CV) calculated according to the Horwitz equation [(CV(%) = 2(5) 0,5 logC), where C is the content expressed as a power of 10] are as follows:
typically be between one-half and two thirds of the above values. 1.2.3.4. Limit of detection:
Adequate for the purpose (see 1.2.6.1).
1.2.3.5. Limit of determination:
Adequate for the purpose (see 1.2.6.2).
1.2.3.6. Sensitivity:
Adequate for the purpose.
1.2.3.7. Practicability:
Speed and cost are of lesser importance compared with screening methods.
For confirmatory methods, most aspects of practicability are of minor significance compared with the other criteria defined in this Decision. it is usually sufficient that the required reagents and equipment are available.
1.2.4. Calibration curves
If the method depends on a calibration curve then the following information must be given:
- the mathematical formula which describes the calibration curve,
- acceptable ranges within which the parameters of the calibration curve may vary from day to day,
- the working range of the calibration curve.
Whenever possible, suitable internal standards and reference materials should be used for the quality control of calibration curves of confirmatory methods, and details of the variance of the variables which is valid at least for the working range of the calibration curve should be given.
1.2.5. Susceptibility to interference
1.2.5.1. For all experimental conditions which could in practice be subject to fluctuation (e.g. stability of reagents, composition of the sample, pH, temperature) any variations which could affect the analytical result should be indicated. The method description shall include means of overcoming any foreseeable interference. If necessary, alternative detection principles suited for confirmation shall be described.
1.2.5.2. If co-chromatography is carried out, then only one peak should be obtained, the enhanced peak height (or area) being equivalent to the amount of added analyte. With GC or LC, the peak width at half maximum height should be within the range 90-110 % of the original width, and the retention times should be identical within a margin of 5 %. For TLC methods, the spot presumed to be due to the analyte should be intensified only; a new spot should not appear, and the visual appearance should not change.
1.2.5.3. It is of prime importance that interference which might arise from matrix components should be investigated.
1.2.6. Relationship between permitted residue levels and analytical limits
1.2.6.1. For substances which are not authorized for use in food-producing animals, the limit of detection of the analytical method must be sufficiently low that residue levels which would be expected after illegal use will be detected with at least 95 % probability.
1.2.6.2. For substances with an established maximum residue limit, the limit of determination of the method plus three times the standard deviation which the method produces for a sample at the maximum residue limit shall not exceed the established maximum residue limit.
1.2.6.3. For substances with an established maximum residue limit, the method should be validated at that limit and at one-half and twice the limit.
2. CRITERIA FOR THE IDENTIFICATION AND QUANTIFICATION OF RESIDUES
2.1. General requirement
Laboratories carrying out analyses for the final confirmation of the presence of residues of low molecular weight organic substances shall ensure that the criteria for the interpretation of results are fulfilled in accordance with the requirements of this section. The criteria are designed for the identification of the analyte and aim to prevent false positive results. For a positive conclusion, the analytical results have to fulfil the criteria laid down for the particular analytical method.
2.2. General considerations for the whole analytical method
2.2.1. Preparation of the sample
The sample should be obtained, handled and processed in such away that there is a maximum chance of detecting the analyte, if present.
2.2.2. Susceptibility to interference
Information as detailed under 1.2.5 (Susceptibility to interference) should be submitted.
2.2.3. General criteria for the whole procedure
2.2.3.1. The specificity (1.1.7) and the limits of detection (1.1.10) and determination (1.1.11) of the method for the analyte in the sample material under investigation have to be known.
Note: This information can be obtained from experimental data and/or theoretical considerations.
2.2.3.2. For a positive result, the physical and chemical behaviour of the analyte during the analysis should be indistinguishable from those of the corresponding standard analyte in the appropriate sample material.
2.2.3.3. The positive or negative result of the analysis will hold only within the borders of specificity and limits of detection and determination of the procedure for the analyte and sample material under investigation.
2.2.3.4. Reference or fortified material containing known amounts of analyte should prefereably be carried through the entire procedure simultaneously with each batch of test samples analysed. Alternatively, an internal standard may be added to test samples.
2.2.4. Criteria for off-line physical and/or chemical preconcentration, purification, and separation
2.2.4.1. The analyte should be in the fraction which is typical for the corresponding standard analyte in the appropriate sample material under the same experimental conditions.
2.2.4.2. Retention data for standards, control samples and test portions should be submitted together with the final result: positive or negative.
2.2.5. Criteria for quantitative measurements
2.2.5.1. The recovery must be measured and specified for all quantitative measurements.
2.2.5.2. Intra-laboratory variability in recovery should be as low as possible.
2.2.5.3. It must be clearly stated whether or not final results have been corrected for recovery. If they have, then the method used for correction must be described.
2.3. Criteria for methods of analysis which may be used for confirmatory purposes only in combination with other methods.
2.3.1. Quality requirements for the determination of an analyte by IA
2.3.1.1. The working range of the calibration curve has to be specified and has in general to cover a concentration range of at least one decade.
2.3.1.2. A minimum of six calibration points is required, adequately distributed along the calibration curve.
2.3.1.3. Adequate quality control parameters have to be in line with those of preceding assays, e.g. NSB and parameters of the calibration curve.
2.3.1.4. Control samples have to be included in each assay. Concentration levels: zero and at lower, middle and upper parts of the working range. Results for these have to be in line with those of previous assays.
All raw data for the control samples and for the test portion should be submitted together with the final result: positive or negative.
2.3.2. Criteria for the determination of an analyte by GC or LC using non-specific detection
2.3.2.1. The analyte should elute at the retention time which is typical for the corresponding standard analyte under the same experimental conditions.
2.3.2.2. The nearest peak maximum in the chromatogram should be separated from the designated analyte peak by at least one full width at 10 % of the maximum height.
2.3.2.3. For additional information, co-chromatography may be used and chromatography using at least two columns of different polarity.
2.3.3. Criteria for the determination of an analyte by TLC
2.3.3.1. The Rf value(s) of the analyte should agree with the Rf value(s) typical for the standard analyte. This requirement is fulfilled when the Rf value(s) of the analyte is (are) within ± 3 % of the Rf value(s) of the standard analyte under the same experimental conditions.
2.3.3.2. The visual appearance of the analyte should be indistinguishable from that of the standard analyte.
2.3.3.3. The centre of the spot nearest to that due to the analyte should be separated from it by at least half the sum of the spot diameters.
2.3.3.4. For additional information, co-chromatography and/or two-dimensional TLC may be used.
2.4. Criteria for methods of analysis which may be used for confirmatory purposes
2.4.1. Criteria for the determination of an analyte by LC/IA or LC/IMG
2.4.1.1. For LC/IMG, the analyte peak in the IMG should be constructed from at least five LC fractions.
2.4.1.2. Criteria 2.2.4.1 and 2.2.4.2 must be fulfilled.
2.4.1.3. Reagents
The source and characteristics of the antibody and other reagents should be specified.
2.4.1.4. Calibration curve
As the method depends on calibration curves, the information itemised under 1.2.4 (Calibration curves) must be given.
The quality requirements specified for IA (2.3.1.1 to 2.3.1.4) must be fulfilled.
2.4.1.5. For confirmatory purposes, if the method is not used in combination with other methods, then two different LC separations or two immunograms using antibodies with different specificities must be carried out.
2.4.2. Criteria for the determination of an analyte LC-SP
2.4.2.1. Criteria 2.3.2.1 and 2.3.2.2 must be fulfilled.
2.4.2.2. The absorption maxima in the spectrum of the analyte should be at the same wavelengths as those of the standard analyte within a margin determined by the resolution of the detection system. For diode array detection this is typically within ± 2 nm.
2.4.2.3. The spectrum of the analyte above 220 nm should not be visually different from the spectrum of the standard analyte for those parts of the two spectra with a relative absorbance & ge; 10 %. This criterion is met when the same maxima are present and at no observed point is the difference between the two spectra more than 10 % of the absorbance of the standard analyte.
2.4.2.4. For confirmatory purposes, if the method is not used in combination with other methods, then co-chromatography in the LC step is mandatory. See 1.2.5.2 for the requirements to be fulfilled by co-chromatography.
2.4.3. Criteria for the determination of an analyte by TLC-SP
2.4.3.1. The method must fulfil the criteria specified for TLC (2.3.3.1 to 2.3.3.3).
2.4.3.2. The absorption maxima in the spectrum of the analyte should be at the same wavelengths as those of the standard analyte, within a margin determined by the resolution of the detection system.
2.4.3.3. The spectrum of the analyte should not be visually different from the spectrum of the standard analyte.
2.4.3.4. For confirmatory purposes, if the method is not used in combination with other methods, then co-chromatography in the TLC step is mandatory. See 1.2.5.2 for the requirements to be fulfilled by co-chromatography.
2.4.4. Criteria for the determination of an analyte by GC-MS
2.4.4.1. GC criteria
2.4.4.1.1. Criteria 2.3.2.1 and 2.3.2.2 must be fulfilled.
2.4.4.1.2. An internal standard should be used if a material suitable for this purpose is available. It should preferably be a stable isotope labelled form of the analyte or, if this is not available, a related standard with a retention time close to that of the analyte.
2.4.4.1.3. The ratio of the retention time of the analyte on GC to that of the internal standard, i.e. the relative retention time of the analyte, should be the same as that of the standard analyte in the appropriate matrix, within a margin of ± 0,5 %.
2.4.4.1.4. For use as a confirmatory method, if no internal standard is used, then identification of the analyte must be supported by using co-chromatography.
2.4.4.2. Criteria for LRMS
2.4.4.2.1. For use as a screening method, at least the intensity of the most abundant diagnostic ion must be measured.
2.4.4.2.2. For use as a confirmatory method, the intensities of preferably at least four diagnostic ions should be measured. If the compound does not yield four diagnostic ions with the method used, then identification of the analyte should be based on the results of at least two independent GC-LRMS methods with different derivatives and/or ionization techniques, each producing two or three diagnostic ions.
The molecular ion should preferably be one of the diagnostic ions selected.
2.4.4.2.3. The relative abundances of all diagnostic ions monitored from the analyte should match those of the standard analyte, preferably within a margin of ± 10 % (EI mode) or ± 20 % (CI mode).
Criteria for the identification of an analyte by IR
2.4.5.1. Definition of adequate peaks
Adequate peaks are absorption maxima in the infrared spectrum of a standard analyte, fulfilling the following requirements.
2.4.5.1.1. The absorption maximum is in the wave-number range 1 800-500 cm 1.
2.4.5.1.2. The intensity of the absorption is not less than:
(a) a specific molar absorbance of 40 with respect to zero absorbance and 20 with respect to peak base line;
or
(b) a relative absorbance of 12,5 % of the absorbance of the most intense peak in the region 1 800 - 500 cm 1 when both are measured with respect to zero absorbance, and 5 % of the absorbance of the most intense peak in the region 1 800 - 500 cm 1 when both are measured with respect to their peak base line.
Note: Although adequate peaks according to (a) may be preferred from a theoretical point of view, those according to (b) are easier to determine in practice.
2.4.5.2. The number of peaks in the infrared spectrum of the analyte whose frequencies correspond with an adequate peak in the spectrum of the standard analyte, within a margin of ± 1 cm 1 is determined.
2.4.5.3. IR criteria
2.4.5.3.1. Absorption must be present in all regions of the analyte spectrum which correspond with an adequate peak in the reference spectrum of the standard analyte.
2.4.5.3.2. A minimum of six adequate peaks is required in the infrared spectrum of the standard analyte. If there are less than six adequate peaks, then the spectrum at issue cannot be used as a reference spectrum.
2.4.5.3.3. The 'score', i.e. the percentage of the adequate peaks found in the infrared spectrum of the analyte, shall be at least 50.
2.4.5.3.4. Where there is no exact match for an adequate peak, the relevant region of the analyte spectrum must be consistent with the presence of a matching peak.
2.4.5.3.5. The procedure is only applicable to absorption peaks in the sample spectrum with an intensity of at least three times the peak to peak noise.
2.5. Other methods of analysis
Analytical methods or combinations of methods other than those considered in Sections 2.3 and 2.4 (e.g. LC-MS, MS-MS, GC-IR) may be used for screening or confirmatory purposes provided that they fulfil comparable criteria which permit unambiguous identification of the analyte at the level of interest.
APPENDIX
List of abbreviations and symbols CI = chemical ionization
EI = electron impact ionization
g = gram(s)
GC = gas chromatography
IA = immunoassay
IAC = immunoaffinity chromatography
IMG = immunogram
IR = infrared spectrometry
kg = kilogram(s) (103 g)
l = litre(s)
LC = liquid chromatography
LRMS = low resolution mass spectrometry
mg = milligram(s) (10 3 g)
MS = mass spectrometry
ng = nanogram(s) (10 9 g)
NSB = non-specific binding
Rf = distance moved relative to the solvent front
SP = spectrometry, e.g. with diode array detection
TLC = thin-layer chromatography
mg = microgram(s) (10 6 g)
/ = off-line hyphenated techniques
- = on-line hyphenated techniques
e.g. LC/GC - MS = LC off-line followed by GC with on-line MS
(1) OJ No L 275 , 26. 9. 1986, p. 36.
(2) International Organization for Standardization: Statistics - vocabulary and symbols.
(3) OJ No L 372, 31. 12. 1985, p. 50.
(4) Internatial Organization for Standardization: Precision of test methods - determination of repeatability and reproducibility for a standard test method by inter-laboratory tests.
