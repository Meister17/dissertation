Decision of the European Parliament, the Council, the Commission, the Court of Justice, the Court of Auditors, the Economic and Social Committee, the Committee of the Regions and the European Ombudsman
of 25 July 2002
establishing a European Communities Personnel Selection Office
(2002/620/EC)
THE EUROPEAN PARLIAMENT, THE COUNCIL OF THE EUROPEAN UNION, THE EUROPEAN COMMISSION, THE COURT OF JUSTICE, THE COURT OF AUDITORS, THE ECONOMIC AND SOCIAL COMMITTEE, THE COMMITTEE OF THE REGIONS AND THE EUROPEAN OMBUDSMAN,
Having regard to the Staff Regulations of officials of the European Communities and to the Conditions of employment of other servants of the European Communities, as laid down by Council Regulation (EEC, Euratom, ECSC) No 259/68(1), as last amended by Regulation (EC, ECSC, Euratom) No 490/2002(2), and in particular the third paragraph of Article 2 of the said Staff Regulations,
Having regard to the opinion of the Staff Regulations Committee,
Whereas:
(1) In the interests of making efficient and economic use of resources, a common interinstitutional body should be entrusted with the means of selecting officials and other servants to serve the European Communities.
(2) The interinstitutional body so established should have the task of drawing up reserve lists from among candidates in open competitions in line with the needs indicated by each institution and in compliance with the Staff Regulations, the decisions to appoint successful candidates being taken by each appointing authority.
(3) On the same terms, the interinstitutional body should also be able to assist the institutions, bodies, offices and agencies established by or in accordance with the Treaties with their internal competitions and the selection of other servants,
HAVE DECIDED AS FOLLOWS:
Article 1
Establishment
A European Communities Personnel Selection Office (hereinafter referred to as "the Office") is hereby established.
Article 2
Powers
1. The Office shall exercise the powers of selection conferred under the first paragraph of Article 30 of the Staff Regulations and under Annex III thereto on the appointing authorities of the institutions signing this Decision. In exceptional cases only and with the agreement of the Office, the institutions may hold their own open competitions to meet specific needs for highly specialised staff.
2. Where the powers referred to in paragraph 1 are conferred on the appointing authority of a body, office or agency established by or in accordance with the Treaties, the Office may exercise such powers at the request of the latter.
3. The decisions to appoint successful candidates shall be taken by the appointing authorities of the European Parliament, the Council, the Commission, the Court of Justice, the Court of Auditors, the Economic and Social Committee, the Committee of the Regions and the European Ombudsman and by any body, office or agency established by or in accordance with the Treaties which has delegated its powers to, or called on the services of, the Office.
Article 3
Duties
1. In response to requests made to it by the appointing authorities referred to in Article 2, the Office shall draw up reserve lists from among candidates in open competitions as referred to in the first paragraph of Article 30 of the Staff Regulations and in accordance with Annex III thereto.
2. The Office may assist the institutions, bodies, offices and agencies established by or in accordance with the Treaties with internal competitions and the selection of other servants.
Article 4
Requests, complaints and appeals
In accordance with Article 91a of the Staff Regulations, requests and complaints relating to the exercise of the powers conferred under Article 2(1) and (2) of this Decision shall be lodged with the Office. Any appeal in these areas shall be made against the Commission.
Article 5
Implementation
The Secretaries-General of the European Parliament, the Council and the Commission, the Registrar of the Court of Justice, the Secretaries-General of the Court of Auditors, the Economic and Social Committee and the Committee of the Regions and the representative of the European Ombudsman shall by mutual agreement take the measures necessary to implement this Decision.
Article 6
Effective date
This Decision shall take effect on the day of its publication in the Official Journal of the European Communities.
For the European Parliament
The President
Patrick Cox
For the Council
The President
Jaume Matas i Palou
For the Commission
The President
Romano Prodi
For the Court of Justice
The President
Gil Carlos Rodríguez Iglesias
For the Court of Auditors
The President
Juan Manuel Fabra Vallés
For the Economic and Social Committee
The President
G. Frerichs
For the Committee of the Regions
The President
Sir Albert Bore
The European Ombudsman
Jacob Söderman
(1) OJ L 56, 4.3.1968, p. 1.
(2) OJ L 77, 20.3.2002, p. 1.
Declaration by the Bureau of the European Parliament
The Bureau of the European Parliament
1. has authorised its President to sign the decision of the institutions establishing the European Communities Personnel Selection Office (the "Office"), and its Secretary-General to sign the two subsidiary decisions concerning its operations;
2. affirms that the institutional autonomy of the European Parliament is not affected by the creation of the Office, in so far as it will remain exclusively competent for the recruitment of permanent officials, in accordance with its institutional interests, from the reserve lists drawn up by the Office;
3. recalls that the selection and recruitment of other categories of staff, in particular the staff of the political groups, remains the exclusive competence of the European Parliament, except in so far as it may seek the technical assistance of the Office in this respect;
4. recalls likewise that the organisation of internal competitions, permitting the passage of officials from one category to another, remains the exclusive responsibility of the European Parliament; reiterates its intention periodically to organise internal competitions for the different staff categories;
5. confirms its commitment to a multilingual and multicultural administration which is balanced from a linguistic and geographical point of view; gives notice that the capacity of the Office to provide reserve lists such as to allow a recruitment policy ensuring such balance is one of the main criteria on which its performance will be judged;
6. further gives notice that, in the event of the Office not being able to provide reserve lists sufficient to guarantee linguistic and geographical balance, the European Parliament reserves its right autonomously to organise specific selection competitions to redress the situation in accordance with Article 2 of the Decision establishing the Office;
7. recalls its decision of 8 April to instruct Parliament's representatives on the Management Board of the Office not to approve the use of age limits in the organisation of open selection competitions.
