DIRECTIVE 96/70/EC OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL of 28 October 1996 amending Council Directive 80/777/EEC on the approximation of the laws of the Member States relating to the exploitation and marketing of natural mineral waters
THE EUROPEAN PARLIAMENT AND THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 100a thereof;
Having regard to the proposal from the Commission (1);
Having regard to the Opinion of the Economic and Social Committee (2);
Acting in accordance with the procedure laid down in Article 189 b of the Treaty (3);
(1) Whereas Directive 80/777/EEC (4) harmonized the laws of the Member States relating to the exploitation and marketing of natural mineral waters;
(2) Whereas the primary purposes of any rules on natural mineral waters should be to protect the health of consumers, to prevent consumers from being misled and to ensure fair trading;
(3) Whereas it is desirable to amend Directive 80/777/EEC to take account of technical and scientific progress since 1980; whereas it is also desirable to rationalize the provisions of that Directive in line with other provisions of Community food law;
(4) Whereas it is necessary to extend the period of recognition for natural mineral waters originating from third countries in order to simplify the administrative procedures;
(5) Whereas it is necessary to clarify the circumstances under which the use of ozone-enriched air may be permitted in order to separate unstable elements from natural mineral waters under conditions which will ensure that the composition of the water as regards its essential constituents is not affected;
(6) Whereas the inclusion of the statement of the analytical composition of a natural mineral water should be made compulsory in order to ensure that consumers are informed;
(7) Whereas it is appropriate to lay down certain provisions on spring waters;
(8) Whereas it is advisable, in order to ensure smooth running of the internal market for natural mineral waters, to introduce a procedure to allow for coordinated action between the Member States in urgent situations which may present a risk to public health;
(9) Whereas a procedure to lay down certain detailed provisions concerning natural mineral waters, notably in respect of the limits for the levels of certain constituents of natural mineral waters, should be established; whereas provisions for the indication, on labelling, of high levels of certain constituents should also be adopted; whereas methods of analysis, including limits of detection, to check the absence of pollution of natural mineral waters, and sampling procedures and methods of analysis for checking the microbiological characteristics of natural mineral waters, should be determined;
(10) Whereas any decision on natural mineral waters likely to have en effect on public health should be adopted following consultation of the Scientific Committee for Food,
HAVE ADOPTED THIS DIRECTIVE:
Article 1
Directive 80/777/EEC is hereby amended as follows:
1. The third subparagraph of Article 1 (2) shall be replaced by the following:
'The validity of the certification referred to in the second subparagraph may not exceed a period of five years. It shall not be necessary to repeat the recognition procedure referred to in the first subparagraph if the certification is renewed before the end of the said period.`
2. Article 4 shall be replaced by the following:
'Article 4
1. Natural mineral water, in its state at source, may not be the subject of any treatment other than:
(a) the separation of its unstable elements, such as iron and sulphur compounds, by filtration or decanting, possibly preceded by oxygenation, in so far as this treatment does not alter the composition of the water as regards the essential constituents which give it its properties;
(b) the separation of iron, manganese and sulphur compounds and arsenic from certain natural mineral waters by treatment with ozone-enriched air in so far as such treatment does not alter the composition of the water as regards the essential constituents which give it its properties, and provided that:
- the treatment complies with the conditions for use to be laid down in accordance with the procedure laid down in Article 12 and following consultation of the Scientific Committee for Food established by Commission Decision 95/273/EC(*),
- the treatment is notified to, and specifically controlled by, the competent authorities;
(c) the separation of undesirable constituents other than those specified in (a) or (b), in so far as this treatment does not alter the composition of the water as regards the essential constituents which give it its properties, and provided that:
- the treatment complies with the conditions for use to be laid down in accordance with the procedure laid down in Article 12 and following consultation of the Scientific Committee for Food,
- the treatment is notified to, and specifically controlled by, the competent authorities;
(d) the total or partial elimination of free carbon dioxide by exclusively physical methods.
2. Natural mineral water, in its state at source, may not be the subject of any addition other than the introduction or the reintroduction of carbon dioxide under the conditions laid down in Annex I, section III.
3. In particular, any disinfection treatment by whatever means and, subject to paragraph 2, the addition of bacteriostatic elements or any other treatment likely to change the viable colony count of the natural mineral water shall be prohibited.
4. Paragraph 1 shall not constitute a bar to the utilization of natural mineral waters and spring waters in the manufacture of soft drinks.
(*) OJ No L 167, 18. 7. 1995, p. 22.`
3. Article 7 (2) shall be replaced by the following:
'2. Labels on natural mineral waters shall also give the following mandatory information:
(a) a statement of the analytical composition, giving its characteristic constituents;
(b) the place where the spring is exploited and the name of the spring;
(c) information on any treatments referred to in Article 4 (1) (b) and (c).
2 a. In the absence of Community provisions on information on any treatments referred to in paragraph 2 (c), Member States may maintain the national provision.`
4. Article 7 (3) shall be deleted.
5. The following paragraphs shall be added to Article 9:
'4a. The term "spring water" shall be reserved for a water which is intended for human consumption in its natural state, and bottled at source, which:
- satisfies the conditions of exploitation laid down in Annex II, paragraphs 2 and 3, which shall be fully applicable to spring waters,
- satisfies the microbiological requirements laid down in Article 5,
- satisfies the labelling requirements of Article 7 (2) (b) and (c) and Article 8,
- has not undergone any treatment other than those referred to in Article 4. Other treatments may be authorized in accordance with the procedure laid down in Article 12.
In addition, spring waters shall comply with the provisions of Council Directive 80/778/EEC of 15 July 1980 relating to the quality of water intended for human consumption(*).
4 b. In the absence of Community provisions on the treatment for spring waters referred to in the fourth indent of Article 9 (4a) Member States may maintain the national provisions on the treatments.
(*) OJ No L 229, 30. 8. 1980, p. 11. Directive as last amended by the 1994 Act of Accession.`
6. Article 10 (2) shall be deleted.
7. The following Article shall be inserted:
'Article 10a
1. Where a Member State has detailed grounds for considering that a natural mineral water does not comply with the provisions laid down in this Directive, or endangers public health, albeit freely circulating in one or more Member States, that Member State may temporarily restrict or suspend trade in that product within its territory. It shall immediately inform the Commission and the other Member States thereof and give reasons for its decision.
2. At the request of any Member State or the Commission, the Member State which has recognized the water shall provide all relevant information concerning recognition of the water, together with the results of the regular checks.
3. The Commission shall examine as soon as possible the grounds adduced by the Member State referred to in paragraph 1 within the Standing Committee for Foodstuffs, and shall then deliver its opinion forthwith and take appropriate measures.
4. If the Commission considers that amendments to this Directive are necessary in order to ensure the protection of public health, it shall initiate the procedure laid down in Article 12, with a view to adopting those amendments. The Member State which has adopted safeguard measures may, in that event, retain them until the amendments have been adopted.`
8. Article 11 shall be replaced by the following:
'Article 11
1. The following shall be adopted in accordance with the procedure laid down in Article 12:
- limits for the levels of constituents of natural mineral waters,
- any necessary provisions for the indication on the labelling of high levels of certain constituents,
- the conditions of use of ozone-enriched air referred to in Article 4 (1) (b),
- the information on the treatments referred to in Article 7 (2) (c).
2. The following may be adopted in accordance with the procedure laid down in Article 12:
- methods of analysis, including limits of detection, to determine the absence of pollution of natural mineral waters,
- the sampling procedures and the methods of analysis necessary for checking the microbiological characteristics of natural mineral waters.`
9. The following Article shall be inserted:
'Article 11a
Any decision likely to have an effect on public health shall be adopted by the Commission following consultation of the Scientific Committee for Food.`
Article 2
Member States shall, where necessary, amend their laws, regulations or administrative provisions so as to:
- permit trade in products complying with this Directive by not later than 28 October 1997,
- prohibit trade in products not complying with this Directive with effect from 28 October 1998. However, trade in products placed on the market or labelled before that date and not conforming with this Directive may continue until stocks run out.
They shall immediately inform the Commission thereof.
When Member States adopt these measures, they shall contain a reference to this Directive or shall be accompanied by such reference on the occasion of their official publication. The methods of making such a reference shall be laid down by the Member States.
Article 3
This Directive shall enter into force on the twentieth day following that of its publication in the Official Journal of the European Communities.
Article 4
This Directive is addressed to the Member States.
Done at Luxembourg, 28 October 1996.
For the European Parliament
The President
K. HÄNSCH
For the Council
The President
I. YATES
(1) OJ No C 314, 11. 11. 1994, p. 4, and OJ No C 33, 6. 2. 1996, p. 15.
(2) OJ No C 110, 2. 5. 1995, p. 55.
(3) Opinion of the European Parliament of 11 October 1995 (OJ No C 287, 30. 10. 1995, p. 101), common position of the Council of 22 December 1995 (OJ No C 59, 28. 2. 1996, p. 44) and Decision of the European Parliament of 22 May 1996 (OJ No C 166, 10. 6. 1996, p. 61). Council Decision of 26 July 1996.
(4) OJ No L 229, 30. 8. 1980, p. 1. Directive as last amended by the 1994 Act of Accession.
