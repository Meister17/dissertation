Judgment of the Court
(First Chamber)
of 15 September 2005
in Case C-258/04: Reference for a preliminary ruling from the Cour de travail de Liège in Office national de l'emploi
v Ioannis Ioannidis [1]
In Case C-258/04: reference for a preliminary ruling under Article 234 EC from the Cour de travail de Liège (Belgium), made by decision of 7 June 2004, received at the Court on 17 June 2004, in the proceedings between Office national de l'emploi and Ioannis Ioannidis — the Court (First Chamber), composed of P. Jann, President of the Chamber, N. Colneric, J.N. Cunha Rodrigues (Rapporteur), M. Ilešič and E. Levits, Judges; D. Ruiz-Jarabo Colomer, Advocate General; R. Grass, Registrar, gave a judgment on 15 September 2005, in which it ruled:
[1] OJ C 201 of 07.08.2004.
--------------------------------------------------
