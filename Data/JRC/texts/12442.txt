Judgment of the Court (Second Chamber) of 29 June 2006 — SGL Carbon AG v Commission of the European Communities
(Case C-308/04 P) [1]
Parties
Appellant: SGL Carbon AG (represented by M. Klusmann and K. Beckmann, lawyers)
Other parties to the proceedings: Commission of the European Communities, (represented by A. Bouquet, H. Gading and M. Schneider, Agents), Tokai Carbon Co. Ltd, established in Tokyo (Japan), Nippon Carbon Co. Ltd, established in Tokyo, Showa Denko KK, established in Tokyo, GrafTech International Ltd, formerly UCAR International Inc., established in Wilmington (United States), SEC Corp., established in Amagasaki (Japan), The Carbide/Graphite Group Inc., established in Pittsburgh (United States)
Re:
Appeal brought against the judgment of the Court of First Instance (Second Chamber) of 29 April 2004 in Joined Cases T-236/01, T-239/01, T-244/01 to T-246/01, T-251/01 and T-252/01 Tokai Carbon and Others as regards Case T-239/01 — Annulment of Commission Decision 2002/271/EC of 18 July 2001 relating to a proceeding under Article 81 EC and Article 53 of the EEA Agreement — Case COMP/E-1/36.490 — Graphite electrodes (OJ 2002 L 100, p. 1)
Operative part of the judgment
The Court:
1. Dismisses the appeal;
2. Orders SGL Carbon AG to pay the costs.
[1] OJ C 262, 23.10.2004.
--------------------------------------------------
