Euro exchange rates [1]
18 July 2006
(2006/C 167/01)
| Currency | Exchange rate |
USD | US dollar | 1,2531 |
JPY | Japanese yen | 146,61 |
DKK | Danish krone | 7,4598 |
GBP | Pound sterling | 0,68490 |
SEK | Swedish krona | 9,2527 |
CHF | Swiss franc | 1,5641 |
ISK | Iceland króna | 94,90 |
NOK | Norwegian krone | 7,9295 |
BGN | Bulgarian lev | 1,9558 |
CYP | Cyprus pound | 0,5750 |
CZK | Czech koruna | 28,445 |
EEK | Estonian kroon | 15,6466 |
HUF | Hungarian forint | 278,45 |
LTL | Lithuanian litas | 3,4528 |
LVL | Latvian lats | 0,6960 |
MTL | Maltese lira | 0,4293 |
PLN | Polish zloty | 4,0263 |
RON | Romanian leu | 3,5857 |
SIT | Slovenian tolar | 239,63 |
SKK | Slovak koruna | 38,562 |
TRY | Turkish lira | 1,9833 |
AUD | Australian dollar | 1,6684 |
CAD | Canadian dollar | 1,4213 |
HKD | Hong Kong dollar | 9,7418 |
NZD | New Zealand dollar | 1,9956 |
SGD | Singapore dollar | 1,9947 |
KRW | South Korean won | 1201,28 |
ZAR | South African rand | 9,0163 |
CNY | Chinese yuan renminbi | 10,0268 |
HRK | Croatian kuna | 7,2452 |
IDR | Indonesian rupiah | 11578,64 |
MYR | Malaysian ringgit | 4,615 |
PHP | Philippine peso | 65,995 |
RUB | Russian rouble | 33,8590 |
THB | Thai baht | 47,777 |
[1] Source: reference exchange rate published by the ECB.
--------------------------------------------------
