COUNCIL DIRECTIVE of 23 July 1990 on the common system of taxation applicable in the case of parent companies and subsidiaries of different Member States (90/435/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 100 thereof,
Having regard to the proposal of the Commission (1),
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas the grouping together of companies of different Member States may be necessary in order to create within the Community conditions analogous to those of an internal market and in order thus to ensure the establishment and effective functioning of the common market; whereas such operations ought not to be hampered by restrictions, disadvantages or distortions arising in particular from the tax provisions of the Member States; whereas it is therefore necessary to introduce with respect to such grouping together of companies of different Member States, tax rules which are neutral from the point of view of competition, in order to allow enterprises to adapt to the requirements of the common market, to increase their productivity and to improve their competitive strength at the international level;
Whereas such grouping together may result in the formation of groups of parent companies and subsidiaries;
Whereas the existing tax provisions which govern the relations between parent companies and subsidiaries of different Member States vary appreciably from one Member State to another and are generally less advantageous than those applicable to parent companies and subsidiaries of the same Member State; whereas cooperation between companies of different Member States is thereby disadvantaged in comparison with cooperation between companies of the same Member State; whereas it is necessary to eliminate this disadvantage by the introduction of a common system in order to facilitate the grouping together of companies;
Whereas where a parent company by virtue of its association with its subsidiary receives distributed profits, the State of the parent company must:
- either refrain from taxing such profits,
- or tax such profits while authorizing the parent company to deduct from the amount of tax due that fraction of the corporation tax paid by the subsidiary which relates to those profits;
Whereas it is furthermore necessary, in order to ensure fiscal neutrality, that the profits which a subsidiary distributes to its parent company be exempt from withholding tax; whereas, however, the Federal Republic of Germany and the Hellenic Republic, by reason of the particular nature of their corporate tax systems, and the Portuguese Republic, for budgetary reasons, should be authorized to maintain temporarily a withholding tax,
HAS ADOPTED THIS DIRECTIVE:
Article 1
1. Each Member State shall apply this Directive:
- to distributions of profits received by companies of that State which come from their subsidiaries of other Member States,
- to distributions of profits by companies of that State to companies of other Member States of which they are subsidiaries.
2. This Directive shall not preclude the application of domestic or agreement-based provisions required for the prevention of fraud or abuse.
Article 2
For the purposes of this Directive 'company of a Member State' shall mean any company which:
(a) takes one of the forms listed in the Annex hereto;
(b)
according to the tax laws of a Member State is considered to be resident in that State for tax purposes and, under the terms of a double taxation agreement concluded with a third State, is not considered to be resident for tax purposes outside the Community;
(c)
moreover, is subject to one of the following taxes, without the possibility of an option or of being exempt:
- impôt des sociétés/vennootschapsbelasting in Belgium,
- selskabsskat in Denmark,
- Koerperschaftsteuer in the Federal Republic of Germany,
- foros eisodimatos nomikon prosopon kerdoskopikoy charaktira in Greece,
- impuesto sobre sociedades in Spain,
- impôt sur les sociétés in France,
- corporation tax in Ireland,
- imposta sul reddito delle persone giuridiche in Italy,
- impôt sur le revenu des collectivités in Luxembourg,
- vennootschapsbelasting in the Netherlands,
- imposto sobre o rendimento das pessoas colectivas in Portugal,
- corporation tax in the United Kingdom,
or to any other tax which may be substituted for any of the above taxes.
Article 3
1. For the purposes of applying this Directive,
(a) the status of parent company shall be attributed at least to any company of a Member State which fulfils the conditions set out in Article 2 and has a minimum holding of 25 % in the capital of a company of another Member State fulfilling the same conditions;
(b) 'subsidiary' shall mean that company the capital of which includes the holding referred to in (a).
2. By way of derogation from paragraph 1, Member States shall have the option of:
- replacing, by means of bilateral agreement, the criterion of a holding in the capital by that of a holding of voting rights,
- not applying this Directive to companies of that Member State which do not maintain for an uninterrupted period of at least two years holdings qualifying them as parent companies or to those of their companies in which a company of another Member State does not maintain such a holding for an uninterrupted period of at least two years.
Article 4
1. Where a parent company, by virtue of its association with its subsidiary, receives distributed profits, the State of the parent company shall, except when the latter is liquidated, either:
- refrain from taxing such profits, or
- tax such profits while authorizing the parent company to deduct from the amount of tax due that fraction of the corporation tax paid by the subsidiary which relates to those profits and, if appropriate, the amount of the withholding tax levied by the Member State in which the subsidiary is resident, pursuant to the derogations provided for in Article 5, up to the limit of the amount of the corresponding domestic tax.
2. However, each Member State shall retain the option of providing that any charges relating to the holding and any losses resulting from the distribution of the profits of the subsidiary may not be deducted from the taxable profits of the parent company. Where the management costs relating to the holding in such a case are fixed as a flat rate, the fixed amount may not exceed 5 % of the profits distributed by the subsidiary.
3. Paragraph 1 shall apply until the date of effective entry into force of a common system of company taxation.
The Council shall at the appropriate time adopt the rules to apply after the date referred to in the first subparagraph.
Article 5
1. Profits which a subsidiary distributed to its parent company shall, at least where the latter holds a minimum of 25 % of the capital of the subsidiary, be exempt from withholding tax.
2. Notwithstanding paragraph 1, the Hellenic Republic may, for so long as it does not charge corporation tax on distributed profits, levy a withholding tax on profits distributed to parent companies of other Member States. However, the rate of that withholding tax must not exceed the rate provided for in bilateral double-taxation agreements.
3. Notwithstanding paragraph 1, the Federal Republic of Germany may, for as long as it charges corporation tax on distributed profits at a rate at least 11 points lower than the rate applicable to retained profits, and at the latest until mid-1996, impose a compensatory withholding tax of 5 % on profits distributed by its subsidiary companies.
4. Notwithstanding paragraph 1, the Portuguese Republic may levy a withholding tax on profits distributed by its subsidiaries to parent companies of other Member States until a date not later than the end of the eighth year following the date of application of this Directive.
Subject to the existing bilateral agreements concluded between Portugal and a Member State, the rate of this withholding tax may not exceed 15 % during the first five years and 10 % during the last three years of that period.
Before the end of the eighth year the Council shall decide unanimously, on a proposal from the Commission, on a possible extension of the provisions of this paragraph.
Article 6
The Member State of a parent company may not charge withholding tax on the profits which such a company receives from a subsidiary.
Article 7
1. The term 'withholding tax' as used in this Directive shall not cover an advance payment or prepayment (précompte) of corporation tax to the Member State of the subsidiary which is made in connection with a distribution of profits to its parent company.
2. This Directive shall not affect the application of domestic or agreement-based provisions designed to eliminate or lessen economic double taxation of dividends, in particular provisions relating to the payment of tax credits to the recipients of dividends.
Article 8
1. Member States shall bring into force the laws, regulations and administrative provisions necessary for them
to comply with this Directive before 1 January 1992. They shall forthwith inform the Commission thereof.
2. Member States shall ensure that the texts of the main provisions of domestic law which they adopt in the field covered by this Directive are communicated to the Commission.
Article 9
This Directive is addressed to the Member States.
Done at Brussels, 23 July 1990.
For the Council
The President
G. CARLI
(1) OJ No C 39, 22. 3. 1969, p. 7 and Amendment transmitted on 5 July 1985.(2)
OJ No C 51, 29. 4. 1970, p. 6.(3)
OJ No C 100, 1. 8. 1969, p. 7.
ANNEX List of companies referred to in Article 2 (a)
(a) companies under Belgian law known as 'société anonyme' / 'naamloze vennootschap', 'société en commandite par actions' / 'commanditaire vennootschap op aandelen', 'société privée à responsabilité limitée' / 'besloten vennootschap met beperkte aansprakelijkheid' and those public law bodies that operate under private law;
(b)
companies under Danish law known as: 'aktieselskab', 'anpartsselskab';
(c)
companies under German law known as: 'Aktiengesellschaft', 'Kommanditgesellschaft auf Aktien', 'Gesellschaft mit beschraenkter Haftung', 'bergrechtliche Gewerkschaft';
(d)
companies under Greek law known as: 'anonymi etairia';
(e)
companies under Spanish law known as: 'sociedad anónima', 'sociedad comanditaria por acciones', 'sociedad de responsabilidad limitada' and those public law bodies which operate under private law;
(f)
companies under French law known as 'société anonyme', 'société en commandite par actions', 'société à responsabilité limitée' and industrial and commercial public establishments and undertakings;
(g)
the companies in Irish law known as public companies limited by shares or by guarantee, private companies limited by shares or by guarantee, bodies registered under the Industrial and Provident Societies Acts or building societies registered under the Building Societies Acts;
(h)
companies under Italian law known as 'società per azioni', 'società in accomandita per azioni', 'società a responsabilità limitata', and public and private entities carrying on industrial and commercial activities;
(i)
companies under Luxembourg law known as 'société anonyme', 'société en commandite par actions', 'société à responsabilité limitée';
(j)
companies under Dutch law known as: 'naamloze vennootschap', 'besloten vennootschap met beperkte aansprakelijkheid';
(k)
commercial companies or civil law companies having a commercial form cooperatives and public undertakings incorporated in accordance with Portuguese law;
(l)
companies incorporated under the law of the United Kingdom.
