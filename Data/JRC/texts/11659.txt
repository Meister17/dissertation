Commission Decision
of 23 November 2005
on the State Aid schemes implemented by Slovenia in the framework of its legislation on Carbon Dioxide Emission Tax
(notified under document number C(2005) 4435)
(Only the Slovene version is authentic)
(Text with EEA relevance)
(2006/640/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community, and in particular the first subparagraph of Article 88(2) thereof,
Having regard to the Agreement on the European Economic Area, and in particular Article 62(1)(a) thereof,
Having called on interested parties to submit their comments pursuant to the provision(s) cited above [1],
Whereas:
PROCEDURE
(1) On 18 October 2002, the Slovene authorities informed the Commission about the existence of a State aid scheme whereby certain categories of companies benefit from a tax reduction under the national CO2 emission tax. The scheme was registered at the Commission as case SI 1/2003. The scheme had previously been approved by the national State aid authority of Slovenia in conformity with Annex IV, Chapter 3, paragraph 2 of the Treaty of Accession of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia to the European Union [2] (Treaty of Accession), on the basis of the Community Guidelines on State aid for environmental protection (Environmental guidelines) [3].
(2) Due to the lack of complete information concerning the measure, the Commission asked Slovenia for further clarification and the scheme could not be included in the existing aid list under the Appendix to the Annex IV of the Treaty of Accession.
(3) Further information was submitted to the Commission by the Slovene authorities on 7 November 2002, 1 April 2003, 16 May 2003, 1 October 2003, 4 February 2004, 1 June 2004, 17 September 2004 and 28 September 2004. Two meetings took place between the Slovene authorities and the Commission on 24 November 2003 and 8 March 2004.
(4) Meanwhile, major changes took place in the EU legislation that had a significant impact on the Slovene CO2 tax system:
- the Council Directive 2003/96 of 27 October 2003 on the taxation of energy products and electricity [4]("Energy Taxation Directive"),
- the Directive 2003/87/EC of the European Parliament and Council of 13 October 2003 establishing a scheme for greenhouse gas emission allowances trading within the Community and amending Council Directive 96/61/EC [5] ("Directive on emission trading"), and
- the Directive 2004/8/EC of the European Parliament and Council of 11 February 2004 on the promotion of cogeneration [6]
have entered into force on their respective days of publication.
(5) Consequently, the Slovene authorities decided to modify their tax scheme, and notified the new — at that time draft — legislation to the Commission. The Commission registered the new scheme in June 2004, under the number N 402/2004.
(6) Based on the information at its disposal, the Commission had doubts as to the compatibility of certain parts of both measures SI 1/2003 and N 402/2004 with the common market. Thus, on 14 December 2004, it initiated a formal investigation procedure on the basis of articles 4.4 and 6 of Council Regulation (EC) No 659/1999 on the rules for the application of article 93 of the EC Treaty [7] and requested the Slovene authorities to submit their comments (the "Opening Decision"). A meaningful summary of that Opening Decision was published on 22 February 2005 in the Official Journal of the European Union [8]. All interested parties were invited to submit their comments within one month of the date of publication.
(7) After the opening of the formal investigation procedure, the Commission registered the case number C 47/2004 for case SI 1/2003 and the case number C 44/2004 for case N 402/2004.
(8) By letter dated 18 January 2005, registered on 20 January 2005, the Slovene authorities submitted their comments with regard to the doubts raised by the Commission in its Opening Decision. On 4 April and 7 July 2005, the Commission sent further questions to the Slovene authorities, which were answered respectively by letters dated 17 May and 8 August 2005.
(9) The Commission did not receive any comments from third parties.
(10) For an easier understanding of the amendments introduced by the new legislation, the Opening Decision covered both the old system of tax reductions (case SI 1/2003) and the new scheme (case N 402/2004). For reasons of clarity and coherence, the present decision also covers both cases C 44/2004 and C 47/2004.
A) SCHEME C 47/2004 (ex case SI 1/2003)
1. DESCRIPTION OF THE SCHEME
(11) The scheme is based on the "Regulation on tax for air pollution with CO2 emissions" of 17 October 2002, and entered into force in Slovenia in October 2002 (the "Regulation"). The new, modified legislation (scheme C 44/2004) entered into force on 1 May 2005, and replaced the Regulation.
(12) Therefore, by the present decision the Commission assesses the compatibility of the Regulation with the common market, covering the period of time between 1 May 2004 (date of accession of Slovenia to the EU) and 1 May 2005 (end of application of the Regulation).
(13) The Regulation foresaw a tax levied on the basis of the quantity of CO2 emitted by each installation. It contained three categories of tax reductions that were submitted to the Commission for approval as operating aid measures under the Environmental guidelines:
(i) Companies that produce electricity in combined heat and power (CHP) installations could be granted a tax reduction if they had an at least 5 % energy saving for existing installations, or 10 % for new installations.
In its Opening Decision, the Commission found this aid compatible with article 87(3)(c) of the EC Treaty.
(ii) The second category of tax reductions concerned all installations that were operating in Slovenia before 1998, had an average of at least 10t CO2 emissions per year during the period 1986 to 1998, and have asked for an emission permit from the Ministry of Environment before 2002. Special reduction rates were foreseen for the following categories of beneficiaries:
- installations producing heat isolation materials,
- power plants feeding electricity to a high voltage transmission network,
- installations of transport of natural gas in gas networks,
- district heating installations, for CO2 emissions due to the use of fossil fuels.
In its Opening Decision, the Commission found that the tax reduction for power plants feeding electricity to a high voltage transmission network (second indent above), did not constitute State aid in the meaning of article 87(1) of the EC Treaty.
It initiated a formal investigation procedure concerning all other tax reductions under this category, on the basis of articles 4.4 and 6 of Council Regulation (EC) No 659/1999 on the rules for the application of article 93 of the EC Treaty.
(iii) The third category of tax reductions concerned large combustion plants of power stations delivering electricity to a high voltage transmission network, using domestic coal as fuel.
In its Opening Decision, the Commission came to the conclusion that this measure did not constitute State aid in the meaning of article 87(1) of the EC Treaty.
(14) The investigation procedure of the Commission therefore concentrated on the State aid measures under point (ii) above.
2. DE MINIMIS AID
(15) At the date of the Commission's Opening Decision ( 14 December 2004), the Regulation was still applicable. However, in their letter dated 17 May 2005, the Slovene authorities confirmed that no administrative decision on CO2 tax reduction had been taken on the basis of the Regulation as of the date of reception of the Commission's decision by the Slovene authorities ( 22 December 2004). On 1 May 2005, the new legislation entered into force and replaced the Regulation.
(16) To the Commission's request (letter dated 4 April 2005), the Slovene authorities provided it with a list of all the beneficiaries that had received tax reduction under the Regulation after the date of accession of Slovenia to the EU, as well as the corresponding amounts of tax reduction, until the end of applicability of the Regulation (letter dated 17 May 2005).
(17) According to this information, the overall amount of the tax reduction between 1 May 2004 and 1 May 2005 was 998771 euros, granted to 153 companies in total. None of the companies have received more than EUR 100000. In fact, only two companies received more than 27000 euros, but none of them more than EUR 100000.
(18) The Slovene authorities therefore argue that, as a consequence of the very short period of application of the Regulation after the accession of Slovenia to the EU, the amount of aid granted under this scheme is lower than the threshold of EUR 100000 fixed by article 2 of the Commission Regulation on de minimis aid [9].
(19) In their letter dated 8 August 2005, the Slovene authorities describe in details the system put in place to monitor de minimis aid in Slovenia. According to this information, Slovenia has set up a system for monitoring and supervising the granting of aid under the de minimis rule by establishing a central register of de minimis aid, in the State Aid Monitoring Department of the Ministry of Finance. Before the granting of any de minimis aid by any authority, this department must check that the conditions of the Commission Regulation on de minimis aid are respected. The central register was established before the accession of Slovenia to the EU.
(20) The Slovene authorities confirmed in their letter dated 8 August 2005 that, due to the use of this centralized system, the beneficiaries of the measure could not receive any aid that would exceed EUR 100000 per beneficiary over a period of three years.
3. ASSESSMENT
(21) On the date of reception of the Commission's Opening Decision ( 22 December 2004), the Slovene authorities immediately put an end to the application of the tax reduction scheme at stake. A significantly modified new scheme entered into force a few months later, on 1 May 2005. Thus, the Regulation assessed by the present decision was applicable in Slovenia for a period of one year after accession, but it was de facto applied for a period of less then 8 months (from 1 May 2004 to 22 December 2004).
(22) As a result of this short application period, the aid granted under this scheme is lower than the threshold of EUR 100000 per beneficiary fixed by article 2 of the Commission Regulation on de minimis aid.
(23) By their letters of 17 May 2005 and 8 August 2005, the Slovene authorities also undertook to respect all other conditions of the Commission Regulation on de minimis aid, and described the monitoring system that ensures the correct application of those rules.
4. CONCLUSION
(24) The Commission therefore concludes that the measure fulfils the criteria of the Commission Regulation on de minimis aid and, in line with its article 2.1, is deemed not to constitute State aid in the meaning of article 87(1) of the EC Treaty.
B) SCHEME C 44/2004 (ex N 402/2004): Modification of the scheme C47/2004
1. DESCRIPTION OF THE SCHEME
(25) In their letter of information registered on 1 June 2004, the Slovene authorities informed the Commission about significant modifications in the Slovene legislation, leading to, inter alia, the amendment of the Regulation on CO2 taxation in force since 2002. The new set of national acts consists of the new Environmental protection act [10], the act amending the Law on Excise Duties [11] and a governmental decree on the taxation of CO2 emissions (the "Decree"), entered into force on 1 May 2005.
(26) The Decree keeps the logic of the previous system of CO2 taxation unchanged: the tax is based on the quantity of CO2 emitted by the installations.
(27) It contains three measures of tax reduction that were submitted to the Commission for approval under the Environmental guidelines. All the three measures have a duration of 5 years: from 1 January 2005 till 31 December 2009.
(i) Companies that produce electricity in combined heat and power (CHP) installations can be granted a tax reduction if they achieve certain energy savings.
In its Opening Decision, the Commission found this measure compatible with article 87(3)(c) of the EC Treaty. Although the measure was only a draft Decree at the time of that decision, the Slovene authorities confirmed by their letter dated 17 May 2005 that this measure had not been modified.
(ii) The second category of reductions concerns power plants feeding electricity to a high voltage transmission network, and certain large combustion installations listed under article 23 of the Decree.
As far as the power plants are concerned, the Commission concluded in its Opening Decision that this measure did not constitute State aid. Concerning the large combustion installations, the Commission found their tax reduction compatible with the EC Treaty.
(iii) According to the draft Decree as submitted to the Commission before its Opening Decision, all operators that feed electricity to a high voltage transmission network but are neither energy intensive businesses nor covered by a voluntary environmental agreement or a tradable permit scheme, could benefit from 43 % tax reduction in 2005 decreasing by 8 percentage points each year. District heating installations in the same situation could benefit from a 26 % reduction in 2005 decreasing by 8 percentage points each year.
In its Opening Decision, the Commission raised doubts as to the compatibility of this measure with the common market and, based on articles 4.4 and 6 of Council Regulation (EC) No 659/1999 on the rules for the application of article 93 of the EC Treaty, it initiated a formal investigation procedure. This was the only category of tax reduction in the new draft Decree that was subject to the Commission's State aid investigation procedure.
(28) Following the Commission's Opening Decision, the Slovene authorities modified the draft Decree. The final version of the Decree, as entered into force in May 2005, replaces this category of tax reduction by the following categories:
(29) Under article 18, 3rd indent of the Decree, companies that participate in the EU emission trading scheme, in line with the Directive on emission trading, and are not energy intensive, can benefit from a tax reduction from the national CO2 tax.
(30) Under article 18, 4th indent, companies that enter into voluntary environmental agreements, can also benefit from tax reduction.
(31) The tax reduction rate is decreasing by 8 percentage points each year:
- 2005: 43 %,
- 2006: 35 %,
- 2007: 27 %,
- 2008: 19 %, and
- 2009: 11 % of tax reduction.
The last year of tax reduction is 2009: no reduction applies as of 2010.
(32) District heating installations benefit from a 26 % reduction in 2005, decreasing by 8 percentage points each year.
2. ASSESSMENT
(33) The Slovene authorities notified the aid measure to the Commission before implementing it.
(34) The measure that is subject to the Commission's investigation procedure is mainly based on articles 18, 3rd and 4th indent; and articles 22 to 24 of the Decree. Although the Decree entered into force during the investigation procedure of the Commission, the Slovene authorities confirm in their letter dated 17 May 2005 that articles 18, 4th indent; 23 and 24 will become applicable only after the Commission's final approval. They therefore comply with their obligation on the basis of article 88(3) of the EC Treaty and article 3 of the Council Regulation (EC) no 659/1999 on the rules for the application of article 93 of the EC Treaty, as far as these articles are concerned.
(35) However, the tax reimbursement measures under the Commission's investigation procedure can also be based on articles 18, 3rd indent and article 22 of the Decree. The Slovene authorities consider [12] that these articles were brought in line with the EC Treaty after the Commission's Opening Decision, and they therefore did not suspend their entering into force until the Commission's final approval. These articles are thus in force since 1 May 2005, in breach of article 3 of the Council Regulation (EC) no 659/1999 on the rules for the application of article 93 of the EC Treaty.
2.1 Existence of aid within the meaning of article 87(1) of the EC Treaty
(36) The Commission is of the view that the amendments introduced by the Slovene authorities in the tax reduction measure since the Opening Decision, do not in any way change the assessment in the Opening Decision concerning the existence of aid within the meaning of article 87(1) of the EC Treaty. Consequently, the Commission considers that the measures under assessment constitute State aid within the meaning of article 87(1) of the EC Treaty.
2.2 Compatibility of the aid with the EC Treaty
(37) The Commission notes that the Slovene authorities have structured the scheme on the basis of the Environmental guidelines and the Energy Taxation Directive.
Compatibility with the Environmental guidelines
(38) The Commission assesses the compatibility of the measures in particular with articles 51.2 and 51.1 (b) 1st indent of the Environmental guidelines. The Slovene CO2 taxation system has been introduced in October 2002. Therefore, according to article 51.2, the provisions of article 51.1 can only apply if the following two conditions are satisfied at the same time:
(a) the tax has an appreciable positive impact in terms of environmental protection. The logic of the Slovene tax system is to tax companies with a higher rate of CO2 emissions more than companies that emit less CO2. Such a taxation system leads inherently to an incentive for the companies to act in a more environmentally friendly manner, by emitting less CO2. Therefore, the Commission considers that this first criterion of article 51.2 is fulfilled.
(b) the derogation for the beneficiaries must have been decided on when the tax was adopted. The categories of beneficiaries foreseen by the initial act of 2002 on CO2 taxation are much larger than the categories covered by the Decree under assessment. The modifications introduced are due to the accession of Slovenia to the EU and the subsequent changes in the applicable legislation. The Commission considers that these modifications left the nature and logic of the derogations unchanged. They only reduce the circle of the beneficiaries in line with the applicable EU legislation.
(39) The Commission therefore concludes that this second condition of article 51.2 of the Environmental guidelines is also fulfilled.
(40) As a consequence of the above, in accordance with article 51.2 of the Environmental guidelines, the provisions of point 51.1 may apply to the measures under assessment.
(41) According to article 51.1(b)1st indent, where the tax reduction concerns a Community tax, a maximum 10 year exemption period can be authorised by the Commission if the amount effectively paid by the beneficiaries after the reduction remains higher than the Community minimum.
(42) Since 1 January 2004, the Energy Taxation Directive foresees a harmonised energy taxation in the Member States. The Commission considers, in line with article 4 of that Directive, that the Slovene tax system based on the quantity of CO2 emitted by the companies, taxes energy products as defined under article 2 of the Energy Taxation Directive and therefore falls within the scope of that Directive. Hence, the Slovene tax system concerns a Community tax, in the meaning of article 51.1(b) 1st indent.
(43) The reduction only applies for a period of less than 5 years which is less than the maximum foreseen by article 51.1.
(44) With regard to the different levels of taxation applicable in Slovenia for different input fuels, the tax rate to be paid by an installation will depend on the nature of the input it will use for its operation. The Commission therefore cannot verify and make sure a priori that the minimum levels of taxation fixed by the Energy Taxation Directive will be respected for each installation. In their letter dated 17 May 2005, the Slovene authorities repeated their commitment to ensure for both categories of beneficiaries that the tax they pay after reduction will remain higher than the Community minimum, defined by the Energy Taxation Directive. The tax reductions are granted in the form of tax reimbursements, the competent national authority can therefore verify compliance with the minimum harmonised level for each installation, before executing the reimbursement.
(45) The Commission also takes into consideration the decreasing nature of the tax reductions, leading to significantly lower reductions each year.
(46) On the basis of the above undertaking by the Slovene authorities, the Commission considers that the condition of article 51.1(b)1st indent, whereby the amount effectively paid by the beneficiaries after the reduction has to remain higher than the Community minimum, is fulfilled.
(47) The conditions of article 51.1(b)1st indent of the Guidelines on environmental protection are therefore satisfied for both categories of beneficiaries.
Compatibility with the Energy Taxation Directive
(48) The Energy Taxation Directive requires in its article 17.1 that even if the minimum levels of taxation prescribed in that Directive are respected, Member States can only apply tax reductions if it is in favour of energy-intensive businesses or if the beneficiary has entered into special agreements with environmental protection objectives or is covered by a tradable permit scheme.
(49) The beneficiaries covered by article 18, 3rd indent of the Decree, must participate in the EU emission trading scheme, in line with the Directive on emission trading [13], in order to benefit from the reduction.
(50) The beneficiaries covered by article 18, 4th indent of the Decree, must enter into voluntary environmental agreements, in order to benefit from the tax reduction. The environmental target to be achieved by the beneficiaries under the environmental agreements is a reduction of CO2 emissions of 2,5 % by the end of 2008, compared to the emissions during the reference period (1999 to 2002).
(51) In light of the above, the Commission finds that both categories of tax reductions are in line with the requirements of the Energy Taxation Directive,
HAS ADOPTED THIS DECISION:
Article 1
The tax reduction measures, as foreseen by the Slovene governmental Decree on the taxation of CO2 emissions, entered into force on 1 May 2005, are compatible with article 87(3)(c) of the EC Treaty.
Article 2
The present Decision covers the tax reductions granted on the basis of the Decree, until 31 December 2009.
Article 3
The present Decision is addressed to the Republic of Slovenia.
Done at Brussels, 23 November 2005.
For the Commission
Neelie Kroes
Member of the Commission
[1] OJ C 46, 22.2.2005, p. 3.
[2] OJ L 236, 23.9.2003.
[3] OJ C 37, 3.2.2001, p. 3.
[4] OJ L 283, 31.10.2003, p. 51.
[5] OJ L 275, 25.10.2003, p. 32.
[6] OJ L 52, 21.2.2004, p. 50.
[7] OJ L 83, 27.3.1999, p. 1.
[8] OJ C 46, 22.2.2005.
[9] Commission Regulation No 69/2001 of 12 January 2001 on the application of articles 87 and 88 of the EC Treaty to de minimis aid
[10] Ur.I.RS 41/2004.
[11] Ur.I.RS 42/2004.
[12] See the Slovene authorities’ letter dated 17 May 2005.
[13] See footnote 3.
--------------------------------------------------
