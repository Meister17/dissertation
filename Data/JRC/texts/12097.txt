Action brought on 30 December 2005 — MPDV Mikrolab v OHIM
Parties
Applicant: MPDV Mikrolab GmbH, Mikroprozessordatenverarbeitung und Mikroprozessorlabor (Mosbach, Germany) (represented by W. Göpfert, lawyer)
Defendant: Office for Harmonisation in the Internal Market (Trade Marks and Designs)
Form of order sought
The applicant claims that the Court should:
- annul the decision of the Second Board of Appeal of 19 October 2005 as regards the appeal proceedings R 1059/2004-2, and;
- order the defendant to pay the costs.
Pleas in law and main arguments
Community trade mark concerned: The word mark "manufacturing score card" for goods and services in Classes 9, 35 and 42 — Application No 3334596
Decision of the Examiner: Rejection of the application
Decision of the Board of Appeal: Dismissal of the appeal
Pleas in law: Infringement of Article 7(1)(c) of Council Regulation No 40/94 as the mark for which registration is sought is capable of being protected because the fact that the elements constituting the mark sought could, each in themselves, or, as part of a different constellation, perhaps have a descriptive content is not sufficient to preclude protection.
Infringement of Article 7(1)(b) of the Regulation, inasmuch as the overall combination of elements, as regards the goods and services for which registration of the mark is sought, is imaginative and conveys an unclear and vague meaning which goes beyond a mere bringing together of the terms.
--------------------------------------------------
