COMMISSION REGULATION (EEC) No 840/92
of 1 April 1992
concerning the classification of certain goods in the combined nomenclature
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 2658/87 (1) on the tariff and statistical nomenclature and on the Common Customs Tariff, as last amended by Regulation (EEC) No 627/92 (2), and in particular Article 9,
Whereas in order to ensure uniform application of the combined nomenclature annexed to the said Regulation, it is necessary to adopt measures concerning the classification of the goods referred to in Annex to this Regulation;
Whereas Regulation (EEC) No 2658/87 has set down the general rules for the interpretation of the combined nomenclature and these rules also apply to any other nomenclature which is wholly or partly based on it or which adds any additional subdivisions to it and which is established by specific Community provisions, which a view to the application of tariff or other measures relating to trade in goods;
Whereas, pursuant to the said general rules, the goods described in column 1 of the table annexed to the present Regulation must be classified under the appropriate CN codes indicated in column 2, by virtue of the reasons set out in column 3;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Nomenclature Committee,
HAS ADOPTED THIS REGULATION:
Article 1
The goods described in column 1 of the annexed table are now classified within the combined nomenclature under the appropriate CN codes indicated in column 2 of the said table.
Article 2
This Regulation shall enter into force on the 21st day after its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 1 April 1992. For the Commission
Christiane SCRIVENER
Member of the Commission
ANNEX
>TABLE>
(1) Photo is of a purely illustrative nature.
