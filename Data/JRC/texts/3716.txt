Commission Decision
of 30 June 2004
on the State aid which the Netherlands is planning to implement in favour of four shipyards to support six shipbuilding contracts
(notified under document number C(2004) 2213)
(Only the Dutch text is authentic)
(Text with EEA relevance)
(2005/122/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community, and in particular the first subparagraph of Article 88(2) thereof,
Having regard to the Agreement on the European Economic Area, and in particular Article 62(1)(a) thereof,
Having regard to the Commission Framework on State aid to shipbuilding [1],
Having called on interested parties to submit their comments [2], pursuant to the provisions cited above and having regard to their comments,
Whereas:
I. PROCEDURE
(1) By letters dated 9 September 2002, The Netherlands notified the Commission of the aid. By letters dated 30 January 2003, 16 May 2003, 16 July 2003 and 16 September 2003, it provided the Commission with further information.
(2) By letter dated 11 November 2003, the Commission informed The Netherlands that it had decided to initiate the procedure laid down in Article 88(2) of the EC Treaty in respect of the aid. The Netherlands replied by letters dated 28 November 2003 and 12 December 2003.
(3) The Commission decision to initiate the procedure was published in the Official Journal of the European Union. The Commission invited interested parties to submit their comments on the aid.
(4) The Commission received comments from interested parties. It forwarded them to The Netherlands, which was given the opportunity to react; its comments were received by letter dated 23 March 2004.
II. DETAILED DESCRIPTION OF THE AID
(5) The notifications propose to give aid for a total of EUR 21,6 million, in the form of grants, in support of six shipbuilding contracts to four Dutch shipyards: Bodewes Scheepswerven BV; Bodewes Scheepswerven Volharding Foxhol; Scheepswerf Visser; and Scheepswerf de Merwede. The aid has been offered to the yards, subject to the Commission’s approval.
(6) The purpose of the notified aid is to match aid allegedly offered by Spain to certain private Spanish shipyards which competed for the contracts referred to above. According to The Netherlands, the alleged Spanish aid implied a price discount of between 9 and 13 %. The Dutch shipyards received all the concerned orders and the ships are under construction or have already been delivered. Details of the notified Dutch aid are presented in table 1 below.
(EUR million) |
Notification No | Beneficiary Shipyard | Ships | Aid amount |
N 601/02 | Bodewes Scheepswerven BV | 4 container ships | […] |
N 602/02 | Bodewes Scheepswerven BV | 3 ro-ro vessels | […] |
N 603/02 | Visser | Arctic Trawler | […] |
N 604/02 | Bodewes Scheepswerven BV | 4 multi-purpose freight vessels | […] |
N 605/02 | Bodewes Volharding Foxhol | 6 container ships | […] |
N 606/02 | De Merwede | 2 hopper dredgers | […] |
Legal basis — the aid scheme
(7) According to the Netherlands, the proposed aid may be granted under the so-called heavy matching provisions (Matchingfonds zwaar) of the Decision on export credit subsidies (Besluit subsidies exportfinancieringsarrangementen, hereinafter called BSE), as first approved by the Commission, letter of 24 June 1992 (SG(92) D/8272 — aid number N 134/92) [4]. The BSE was subject to various amendments, among which the most important was approved by the Commission in a letter dated 12 December 1997, reference SG(97) D/10395 (aid number N 337/97) [5]. In this decision the scheme was approved for the period of 1997 to the end of 2002.
Grounds for initiating the procedure
(8) The opening of the procedure addressed doubts in the following areas:
(a) the Commission considered that granting aid to match alleged illegal aid from another EC Member State is contrary to the EC Treaty and therefore had doubts that the notified aid could be compatible. In this context it had doubts that the approval of the Dutch scheme included the right to match aid provided from another EU Member State;
(b) even if the scheme would allow for such intra-EU matching, the Commission had doubts as to the fulfilment of the procedures in establishing the existence of illegal aid to match;
(c) the Commission further had doubts that the aid could be approved on the basis of Article 3(4) of the shipbuilding Regulation [6], since that Article concerns export credits to shipowners while the Netherlands notified grants to the shipyards.
III. COMMENTS FROM INTERESTED PARTIES
(9) Comments were received from the representative of the beneficiaries and from a party requesting his identity not to be disclosed. All these parties argue that the aid should be approved, stressing among other things that the Commission had approved the matching scheme, that the presented proof of the alleged Spanish aid was sufficient, and that the Spanish denial that the aid would be available was not sufficiently explicit.
(10) The representative of the beneficiaries further argued that there is nothing in the Commission’s decisions approving the scheme, or in the text of the scheme, that suggests that the matching would only be allowed if the competitor originates from a third (non-EU) country. It also argues that the planned aid is based on an approved scheme and therefore is existing aid, which therefore should not be assessed on the basis of the shipbuilding Regulation.
IV. COMMENTS FROM THE NETHERLANDS
(11) The Netherlands claims to have acted within the rules (the BSE scheme) governing heavy matching, which were authorised by the Commission and which are in accordance with the relevant OECD procedures. The Netherlands maintains that the BSE scheme does not exclude matching against alleged aid provided by another EU Member State and that the procedures have shown with sufficient certainty that there was indeed Spanish aid. The Netherlands therefore considers that it is entitled to proceed with the matching and that the beneficiaries have legitimate expectations to receive the aid.
(12) The Netherlands also considers that the aid is existing aid, since it is based on an approved scheme, and the fact that the Dutch authorities nevertheless decided to notify these cases was due to an Exchange of Letters between the Dutch Minister of Economic Affairs and the Commissioner in charge of Competition. The Dutch authorities consider that the Commission, in this case, should have presented to the Netherlands a recommendation containing appropriate measures before launching a formal investigation procedure. Finally, the Netherlands considers that the Commission’s doubts, based on Article 3(4) of Regulation (EC) No 1540/98 (the shipbuilding Regulation), concerning the right to provide grants to shipyards instead of credit facilities to shipowners, are unfounded since the legal basis should be the Council decisions based on the OECD export credit rules.
V. ASSESSMENT OF THE AID
Legal basis for the assessment
(13) According to Article 87(1) of the EC Treaty, any aid granted by a Member State or through State resources in any form whatsoever which distorts or threatens to distort competition by favouring certain undertakings or the production of certain goods shall, in so far as it affects trade between Member States, be incompatible with the common market. Pursuant to established case-law of the European courts, the criterion of trade being affected is met if the recipient firm carries out an economic activity involving trade between Member States.
(14) The Commission notes that the Netherlands intends to grant subsidies to four shipyards for the construction of vessels, as detailed above. The Commission notes, therefore, that the beneficiaries are involved in an economic activity involving trade between Member States. The alleged "raison d'être" of the measures is indeed (unfair) competition from shipyards of another Member State. The Commission considers that the notified aid falls within the scope of Article 87(1) of the EC Treaty.
(15) According to Article 87(3)(c) of the EC Treaty, aid to facilitate the development of certain economic activities or of certain economic areas, where such aid does not adversely affect trading conditions to an extent contrary to the common interest, may be considered to be compatible with the common market. Based on this Article, the BSE scheme was approved in 1992 and was modified in 1997. However, it was stated in the general provision that requests for subsidies will be turned down if they are in contravention of the EEC Treaty.
(16) Furthermore, the Commission has clarified its interpretation of the rules on state aid applicable to shipbuilding in its Framework on State aid to shipbuilding, which is applicable from 1 January 2004 to 31 December 2006. Previously, State aid to shipbuilding was regulated by the shipbuilding Regulation.
Assessment of whether the aid is contrary to general principles of EC law
(17) The principle that a Member State should not act on its own to counter the effects of unlawful aid from another Member State has been clearly established by the Court. Specifically, the Court has held that it is not possible to justify an aid on the ground that other Member States have granted illegal aid [7]. The Commission observes that the notified aid aims at matching alleged illegal aid from another EC Member State. This is therefore contrary to the general principles of the EC Treaty. The notified aid is incompatible with the EC Treaty and should therefore not be authorised.
Assessment whether the aid could be approved based on the BSE scheme
(18) Since the Netherlands argues that the aid would be compatible with the BSE scheme, and therefore compatible with the EC treaty, this issue has also been assessed by the Commission. The Commission can conclude that the notified aid cannot be authorised under this scheme for two reasons.
(19) Firstly, the Commission’s approval of the BSE scheme expired by the end of 2002. Since the Commission must base its decisions for notified State aid on the legislation in force at the time of its decision, it is not possible to authorise any aid based on an aid scheme for which the approval has expired.
(20) Secondly, even if the aid scheme had not expired, the Commission considers that the aid scheme in question did not authorise aid to match alleged aid from another Member State.
(21) Some elements in the Commission decisions indicate, indeed, that intra-EU matching was not authorised under these decisions:
(a) firstly, the statement expressed in the Commission’s decision approving the scheme that "requests for subsidies will be turned down if they are in contravention of the EEC Treaty" meant not only that the scheme had to be approved by the Commission, but also that the application of the scheme had to be in conformity with the general provisions of the EEC Treaty;
(b) secondly, in its decision on the modified scheme in 1997 (N 337/97), the Commission stated, as a general observation before assessing the scheme, that "Although trade effects between Member State are probably felt less intensively in cases of aid to trade transactions outside the EU [8], such effects cannot a priori be excluded.". This is a clear indication that the notified scheme was assumed to involve aid to trade transactions outside the EU.
(22) The Dutch authorities have claimed that the Commission should not have opened the formal investigation procedure against an aid granted on the basis of an approved scheme. Instead the Commission should have presented the Netherlands with a recommendation for appropriate measures.
(23) On this issue the Commission notes that it was the Netherlands that notified the aid. Therefore, even though an aid scheme existed at the time of the notification, to which the Netherlands refers, it was the notification by the Netherlands that led the Commission to procedurally handle this case as an ad hoc case and not as an individual application of a scheme. In addition, since the aid does not fall within a scheme as approved by the Commission, it had to be individually notified and the Commission had the duty to open the procedure under Article 88(2) of the Treaty in case of doubts on its compatibility with the common market.
Proof of illegal aid to match
(24) Furthermore, the Commission is of the opinion that all of the conditions of the BSE scheme have not been fulfilled in the present case and that, in particular, sufficient proof of the existence of illegal Spanish aid was not provided. In this respect the comments from the Netherlands and the potential beneficiaries do not alleviate the doubts expressed in the opening decision. The Commission considers that the Spanish authorities have clearly denied that the aid would ever be available. In State aid proceedings the Commission has to, in the last analysis, rely on the statements of the Member State supposed to (have) grant(ed) the aid.
Assessment as ad hoc aid
(25) In the opening of the procedure the Commission also explored the possibility of approving the aid directly based on Article 3(4) of the shipbuilding Regulation, now replaced by section 3.3.4. of the Framework on State aid to shipbuilding, which states that "aid in the form of State supported credit facilities granted to national and non-national shipowners or third parties for the building (…) of vessels may [9] be deemed compatible with the common market and shall not be counted within the ceiling (on operating aid) if it complies with the terms of (…) OECD Understanding on export credits for ships…", since the OECD Understanding for ships contains a matching clause.
(26) In this respect, the Commission first notes that it does not accept the matching of alleged aid from another Member State; therefore, this provision is not applicable in the present case. The Commission considers that the use of "may" gives it discretion not to apply this provision for matching alleged aid from another Member State. Secondly, the Commission recalls that no new information has been presented which would make it modify its doubts that there were sufficient proof of the alleged Spanish aid (see recital 24). Thirdly, the Commission confirms, as mentioned in its decision to open the procedure, that the provision concerns credits to shipowners (or third parties) while the aid at stake relates to grants to shipyards. Finally, the Commission disagrees with the Netherlands that the aid should only be assessed on the basis of Council decisions based on the OECD export credit rules. State aid within the meaning of Article 87(1) of the Treaty to the shipbuilding industry has necessarily to be assessed on the basis of the applicable rules that the Commission imposed itself (the framework on State aid to shipbuilding) in order to apply the derogations to the incompatibility of State aid as foreseen by the Treaty.
(27) The Commission also considers that no other legal basis exists under which the notified State aid could be approved. Besides, the Dutch authorities have not invoked any other derogation of the Treaty.
Legitimate expectations of the beneficiaries
(28) As already mentioned, the BSE scheme was not applicable and therefore no legitimate expectations from the beneficiaries can be accepted. In any case, the beneficiaries could not claim legitimate expectations since the aid was granted by the Netherlands subject to the outcome of the Commission assessment.
VI. CONCLUSION
HAS ADOPTED THIS DECISION:
Article 1
The State aid which the Netherlands is planning to implement for Bodewes Scheepswerven BV, amounting to EUR […], for Scheepswerf Visser, amounting to EUR […], for Bodewes Scheepswerf Volharding Foxhol, amounting to EUR […] and for Scheepswerf De Merwede, amounting to EUR […] is incompatible with the common market.
The aid may accordingly not be implemented.
Article 2
The Netherlands shall inform the Commission, within two months of notification of this Decision, of the measures taken to comply with it.
Article 3
This Decision is addressed to the Kingdom of the Netherlands.
Done at Brussels, 30 June 2004.
For the Commission
Mario Monti
Member of the Commission
[1] OJ C 317, 30.12.2003, p. 11.
[2] OJ C 11, 15.1.2004, p. 5.
[4] OJ C 203, 11.8.1992.
[5] OJ C 253, 12.8.1998, p. 13.
[6] Council Regulation (EC) No 1540/98 of 29 June 1998 establishing new rules on aid to shipbuilding (OJ L 202, 18.7.1998, p. 1).
[7] See Case 78/79, Steinike 1 Weinlig v Federal Republic of Germany [1977] ECR 595, paragraph 24.
[8] Not underlined in the 1997 decision.
[9] Not underlined in the Framework.
--------------------------------------------------
