Regulation No 26 of the Economic Commission for Europe of the United Nations (UN/ECE) — Uniform provisions concerning the approval of vehicles with regard to their external projections
1. SCOPE AND PURPOSE
1.1. This Regulation applies to external projections of category M1 vehicles [1]. It does not apply to exterior rear-view mirrors or to the ball of towing devices.
1.2. The purpose of this Regulation is to reduce the risk or seriousness of bodily injury to a person hit by the bodywork or brushing against it in the event of a collision. This is valid both when the vehicle is stationary and in motion.
2. DEFINITIONS
For the purposes of this Regulation:
2.1. "Approval of a vehicle" means the approval of a vehicle type with regard to its external projections.
2.2. "Vehicle type" means a category of motor vehicles which do not differ in such essential respects as, shape or materials of the external surface.
2.3. "External surface" means the outside of the vehicle including the bonnet, the lid of the luggage compartment, the doors, the wings, the roof, the lighting and light-signalling devices and the visible strengthening components.
2.4. "Floor line" means the line determined as follows:
Successively position round a laden vehicle a cone with a vertical axis the height of which is not defined, and with a half angle of 30° in such a way that it contacts, constantly and as low as possible, the external surface of the vehicle. The floor line is the geometric trace of these points of contact. In determining the floor line, the jacking points, exhaust pipes or wheels shall not be taken into consideration. The gaps of the wheel arches are assumed to be filled in by an imaginary surface forming a smooth continuation of the surrounding external surface. At both ends of the vehicle the bumper shall be taken into consideration when establishing the floor line. Dependent upon the particular vehicle the floor line trace may occur at the bumper section extremity or at the body panel below the bumper. Where two or more points of contact occur simultaneously, the lower point of contact shall be used to determine the floor line.
2.5. "Radius of curvature" means the radius of the arc of a circle which comes closest to the rounded form of the component under consideration.
2.6. "Laden vehicle" means the vehicle laden to the maximum permitted technical mass. Vehicles equipped with hydropneumatic, hydraulic or pneumatic suspension or a device for automatic levelling according to load shall be tested with the vehicle in the most adverse normal running condition specified by the manufacturer.
2.7. "Extreme outer edge" of the vehicle means, in relation to the sides of the vehicle, the plane parallel to the median longitudinal plane of the vehicle coinciding with its outer lateral edge, and, in relation to the front and rear ends, the perpendicular transverse plane of the vehicle coinciding with its outer front and rear edges, account not being taken of the projection:
2.7.1. of tyres near their point of contact with the ground, and connections for tyre pressure gauges;
2.7.2. of any anti-skid devices which may be mounted on the wheels;
2.7.3. of rear-view mirrors;
2.7.4. of side direction indicator lamps, end outline marker lamps, front and rear position (side) lamps and parking lamps;
2.7.5. in relation to the front and rear ends, of parts mounted on the bumpers, of towing devices and of exhaust pipes.
2.8. "The dimension of the projection" of a component mounted on a panel means the dimension determined by the method described in paragraph 2 of Annex 3 to this Regulation.
2.9. "The nominal line of a panel" means the line passing through the two points represented by the position of the centre of a sphere when its surface makes its first and last contact with a component during the measuring procedure described in paragraph 2.2 of Annex 3 to this Regulation.
3. APPLICATION FOR APPROVAL
3.1 Application for approval of a vehicle type with regard to its external projections.
3.1.1. The application for approval of a vehicle type with regard to its external projections shall be submitted by the vehicle manufacturer or by his duly accredited representative.
3.1.2. It shall be accompanied by the following documents in triplicate:
3.1.2.1. photographs of the front, rear, and side parts of the vehicle taken at an angle of 30° to 45° to the vertical longitudinal median plane of the vehicle;
3.1.2.2. drawings with dimensions of the bumpers and, where appropriate;
3.1.2.3. drawings of certain external projections and if applicable drawings of certain sections of the external surface referred to in 6.9.1.
3.1.3. A vehicle representative of the type of vehicle to be approved shall be submitted to the technical service responsible for conducting the approval tests. At the request of the aforesaid technical service, certain components and certain samples of the material used shall likewise be submitted.
3.2. Application for type approval with regard to luggage racks, ski racks or radio receiving or transmitting aerials considered to be separate technical units.
3.2.1. Application for type approval with regard to luggage racks, ski racks or radio receiving or transmitting aerials considered to be separate technical units shall be submitted by the vehicle manufacturer or the manufacturer of the aforementioned separate technical units, or by their duly accredited representative.
3.2.2. For every type of any one of the devices referred to in paragraph 3.2.1 above, the application shall be accompanied by the following:
3.2.2.1. triplicate copies of documents specifying the technical characteristics of the separate technical unit and the assembly instructions to be supplied with every separate technical unit sold;
3.2.2.2. a specimen of the type of separate technical unit. Should the responsible authority consider it necessary, it may request a further specimen.
4. APPROVAL
4.1. Approval of a vehicle type with regard to its external projections.
4.1.1. If the vehicle type submitted for approval pursuant to this Regulation meets the requirements of paragraphs 5 and 6 below, approval of that vehicle type shall be granted.
4.1.2. An approval number shall be assigned to each vehicle type approved. Its first two digits (at present 02 corresponding to the 02 series of amendments which entered into force on 13 December 1996) shall indicate the series of amendments incorporating the most recent major technical amendments made to the Regulation at the time of issue of the approval. The same Contracting Party may not assign the same number to another vehicle type.
4.1.3. Notice of approval or of extension or refusal or withdrawal of approval or production definitely discontinued of a vehicle type pursuant to this Regulation shall be communicated to the Parties to the 1958 Agreement applying this Regulation by means of a communication form conforming to the model in Annex 1 to this Regulation.
4.1.4. There shall be affixed, conspicuously and in a readily accessible place specified on the approval form, to every vehicle conforming to a vehicle type approved under this Regulation an international approval mark consisting of:
4.1.4.1. a circle surrounding the letter "E" followed by the distinguishing number of the country which has granted approval;
4.1.4.2. the number of this Regulation, followed by the letter "R", a dash and the approval number to the right of the circle prescribed in paragraph 4.1.4.1.
4.1.5. If the vehicle conforms to a vehicle type approved, under one or more other Regulations annexed to the Agreement, in the country which has granted approval under this Regulation the symbol prescribed in paragraph 4.1.4.1 need not be repeated; in such a case the additional numbers and symbols of all the Regulations under which approval has been granted in the country which has granted approval under this Regulation shall be placed in vertical columns to the right of the symbol prescribed in paragraph 4.1.4.1.
4.1.6. The approval mark shall be clearly legible and be indelible.
4.1.7. The approval mark shall be placed close to or on the vehicle data plate affixed by the manufacturer.
4.1.8. Annex 2 to this Regulation gives examples of arrangements of approval marks.
4.1.9. The competent authority shall verify the existence of satisfactory arrangements for ensuring effective control of the conformity of production before type approval is granted.
4.2. Approval with regard to luggage racks, ski racks or radio receiving or transmitting aerials considered to be separate technical units.
4.2.1. If the type of separate technical unit submitted for approval pursuant to this Regulation meets the requirements of paragraphs 6.16, 6.17 and 6.18 below, approval of that type of separate technical unit shall be granted.
4.2.2. An approval number shall be assigned to each type of separate technical unit approved. Its first two digits (at present 02 corresponding to the 02 series of amendments which entered into force on 13 December 1996) shall indicate the series of amendments incorporating the most recent major technical amendments made to the Regulation at the time of issue of the approval. The same Contracting Party may not assign the same number to another type of separate technical unit.
4.2.3. Notice of approval, or of extension or refusal or withdrawal of approval or production definitely discontinued, of a type of separate technical unit pursuant to this Regulation shall be communicated to the Parties to the 1958 Agreement applying this Regulation by means of a communication form conforming to the model in Annex 4 to this Regulation.
4.2.4. There shall be affixed, conspicuously and in a readily accessible place specified on the approval form, to every separate technical unit conforming to a type approved under this Regulation, an international approval mark consisting of:
4.2.4.1. a circle surrounding the letter "E" followed by the distinguishing number of the country which has granted approval [2];
4.2.4.2. the number of this Regulation, followed by the letter "R", a dash and the approval number to the right of the circle prescribed in paragraph 4.2.4.1.
4.2.5. The approval mark shall be clearly legible and be indelible.
4.2.6. The approval mark shall be placed close to or on the separate technical unit data plate affixed by the manufacturer.
4.2.7. Annex 2 to this Regulation gives examples of arrangements of approval marks.
4.2.8. The competent authority shall verify the existence of satisfactory arrangements for ensuring effective control of conformity of production before type approval is granted.
5. GENERAL SPECIFICATIONS
5.1. The provisions of this Regulation shall not apply to those parts of the external surface which, with the vehicle in the laden condition, with all doors, windows and access lids etc., in the closed position, are either:
5.1.1. at a height of more than 2 metres, or
5.1.2. below the floor line, or
5.1.3. so located that, in their static condition as well as when in operation, they cannot be contacted by a sphere 100 mm in diameter.
5.2. The external surface of vehicles shall not exhibit, directed outwards, any pointed or sharp parts or any projections of such shape, dimensions, direction or hardness as to be likely to increase the risk or seriousness of bodily injury to a person hit by the external surface or brushing against it in the event of a collision.
5.3. The external surface of vehicles shall not exhibit, directed outwards, any part likely to catch on pedestrians, cyclists or motor cyclists.
5.4. No protruding part of the external surface shall have a radius of curvature less than 2,5 mm. This requirement shall not apply to parts of the external surface which protrude less than 5 mm, but the outward facing angles of such parts shall be blunted, save where such parts protrude less than 1,5 mm.
5.5. Protruding parts of the external surface, made of a material of hardness not exceeding 60 shore A, may have a radius of curvature less than 2,5 mm.
The hardness measurement shall be taken with the component as installed on the vehicle. Where it is impossible to carry out a hardness measurement by the Shore A procedure, comparable measurements shall be used for evaluation.
5.6. The provisions of the above paragraphs 5.1 to 5.5 shall apply in addition to the particular specifications of the following paragraph 6, except where these particular specifications expressly provide otherwise.
6. PARTICULAR SPECIFICATIONS
6.1. Ornaments
6.1.1. Added ornaments which project more than 10 mm from their support shall retract, become detached or bend over under a force of 10 daN exerted at their most salient point in any direction in a plane approximately parallel to the surface on which they are mounted. These provisions shall not apply to ornaments on radiator grilles, to which only the general requirements of paragraph 5 shall apply. To apply the 10 daN force a flat-ended ram of not more than 50 mm diameter shall be used. Where this is not possible, an equivalent method shall be used. After the ornaments are retracted, detached or bent over, the remaining projections shall not project more than 10 mm. These projections shall in any case satisfy the provisions of paragraph 5.2. If the ornament is mounted on a base, this base is regarded as belonging to the ornament and not to the supporting surface.
6.1.2. Protective strips or shielding on the external surface shall not be subject to the requirements of paragraph 6.1.1 above; however, they shall be firmly secured to the vehicle.
6.2. Headlights
6.2.1. Projecting visors and rims shall be permitted on headlights, provided that their projection, as measured in relation to the external transparent surface of the headlight does not exceed 30 mm and their radius of curvature is at least 2,5 mm throughout. In the case of a headlamp mounted behind an additional transparent surface, the projection shall be measured from the outermost transparent surface. The projections shall be determined according to the method described in paragraph 3 of Annex 3 to this Regulation.
6.2.2. Retracting headlights shall meet the provisions of paragraph 6.2.1 above in both the operative and retracted positions.
6.2.3. The provisions of paragraph 6.2.1 above do not apply to headlamps which are sunk into the bodywork or which are "overhung" by the bodywork, if the latter complies with the requirements of paragraph 6.9.1.
6.3. Grilles and gaps
6.3.1. The requirements of paragraph 5.4 shall not apply to gaps between fixed or movable elements, including those forming part of air intake or outlet grilles and radiator grilles, provided that the distance between consecutive elements does not exceed 40 mm and provided that the grilles and gaps have a functional purpose. For gaps of between 40 mm and 25 mm the radii of curvature shall be 1 mm or more. However, if the distance between two consecutive elements is equal to or less than 25 mm, the radii of curvature of external faces of the elements shall not be less than 0,5 mm. The distance between two consecutive elements of grilles and gaps shall be determined according to the method described in paragraph 4 of Annex 3 to this Regulation.
6.3.2. The junction of the front with the side faces of each element forming a grille or gap shall be blunted.
6.4. Windscreen wipers
6.4.1. The windscreen wiper fittings shall be such that the wiper shaft is furnished with a protective casing which has a radius of curvature meeting the requirements of paragraph 5.4 above and an end surface area of not less than 150 mm2. In the case of rounded covers, these shall have a minimum projected area of 150 mm2 when measured not more than 6,5 mm from the point projecting furthest. These requirements shall also be met by rear window wipers and headlamp wipers.
6.4.2. Paragraph 5.4 shall not apply to the wiper blades or to any supporting members. However, these units shall be so made as to have no sharp angles or pointed or cutting parts.
6.5. Bumpers
6.5.1. The ends of the bumpers shall be turned in towards the external surface in order to minimise the risk of fouling. This requirement is considered to be satisfied if either the bumper is recessed or integrated within the bodywork or the end of the bumper is turned in so that it is not contactable by a 100 mm sphere and the gap between the bumper end and the surrounding bodywork does not exceed 20 mm.
6.5.2. If the line of the bumper which corresponds to the outline contour of the car vertical projection is on a rigid surface, that surface shall have a minimum radius of curvature of 5 mm at all its points lying from the contour line to 20 mm inward, and a minimum radius of curvature of 2,5 mm in all other cases. This provision applies to that part of the zone lying from the contour line to 20 mm inward which is situated between and in front (or rear in case of the rear bumper) of tangential points with the contour line of two vertical planes each forming with the longitudinal plane of symmetry of the vehicle an angle of 15° (see fig. 1).
+++++ TIFF +++++
6.5.3. The requirement of paragraph 6.5.2 shall not apply to parts on or of the bumper or to bumper insets which have a projection of less than 5 mm, with special reference to joint covers and jets for headlamp washers; but the outward facing angles of such parts shall be blunted, save where such parts protrude less than 1,5 mm.
6.6. Handles, hinges and push-buttons of doors, luggage compartments and bonnets; fuel tank filler caps and covers
6.6.1. The projection shall not exceed 40 mm in the case of door or luggage compartment handles and 30 mm in all other cases.
6.6.2. If lateral door handles rotate to operate, they shall meet one or other of the following requirements:
6.6.2.1. In the case of handles which rotate parallel to the plane of the door, the open end of handles must be directed towards the rear. The end of such handles shall be turned back towards the plane of the door and fitted into a protective surround or be recessed.
6.6.2.2. Handles which pivot outwards in any direction which is not parallel to the plane of the door shall, when in the closed position, be enclosed in a protective surround or be recessed. The open end shall face either rearwards or downwards.
Nevertheless, handles which do not comply with this last condition may be accepted if:
(a) they have an independent return mechanism,
(b) should the return mechanism fail, they cannot project more than 15 mm,
(c) they comply, in such opened position, with the provisions of paragraph 5.4,
and
(d) their end surface area, when measured not more than 6,5 mm from the point projecting furthest, is not less than 150 mm2.
6.7. Wheels, wheel nuts, hub caps and wheel discs
6.7.1. The requirements of paragraph 5.4 shall not apply.
6.7.2. The wheels, wheel nuts, hub caps and wheel discs shall not exhibit any pointed or sharp projections that extend beyond the external plane of the wheel rim. Wing nuts shall not be allowed.
6.7.3. When the vehicle is travelling in a straight line, no part of the wheels other than the tyres, situated above the horizontal plane passing through their axis of rotation shall project beyond the vertical projection, in a horizontal plane of the external surface or structure. However, if functional requirements so warrant, wheel discs which cover wheel and hub nuts may project beyond the vertical projection of the external surface or structure on condition that the radius of curvature of the surface of the projecting part is not less than 30 mm and that the projection beyond the vertical projection of the external surface or structure in no case exceeds 30 mm.
6.8. Sheet-metal edges
6.8.1. Sheet-metal edges, such as gutter edges and the rails of sliding doors, shall not be permitted unless they are folded back or are fitted with a shield meeting the requirements of this Regulation which are applicable to it. An unprotected edge shall be considered to be folded back either if it is folded back by approximately 180°, or if it is folded towards the bodywork in such a manner that it cannot be contacted by a sphere having a diameter of 100 mm.
6.9. Body-panels
6.9.1. Folds in body panels may have a radius of curvature of less than 2,5 mm provided that it is not less than one-tenth of the height "H" of the projection, measured in accordance with the method described in paragraph 1 of Annex 3.
6.10. Lateral air or rain deflectors
6.10.1. Lateral deflectors shall have a radius of curvature of at least 1 mm on edges capable of being directed outwards.
6.11. Jacking brackets and exhaust pipes
6.11.1. The jacking brackets and exhaust pipe(s) shall not project more than 10 mm beyond the vertical projection of the floor line lying vertically above them. As an exception to this requirement an exhaust pipe may project more than 10 mm beyond the vertical projection of the floor line, so long as it terminates in rounded edges, the minimum radius of curvature being 2,5 mm.
6.12. Air intake and outlet flaps
6.12.1. Air intake and outlet flaps shall meet the requirements of paragraphs 5.2, 5.3 and 5.4 in all positions of use.
6.13. Roof
6.13.1. Opening roofs shall be considered only in the closed position.
6.13.2. Convertible vehicles shall be examined with the hood in both the raised and lowered positions.
6.13.2.1. With the hood lowered, no examination shall be made of the vehicle inside an imaginary surface formed by the hood when in the raised position.
6.13.2.2. Where a cover for the linkage of the hood when folded is provided as standard equipment, the examination shall be made with the cover in position.
6.14. Windows
6.14.1. Windows which move outwards from the external surface of the vehicle shall comply with the following provisions in all positions of use:
6.14.1.1. no exposed edge shall face forwards;
6.14.1.2. no part of the window shall project beyond the extreme outer edge of the vehicle.
6.15. Registration plate brackets
6.15.1. Supporting brackets provided by the vehicle manufacturer for registration plates shall comply with the requirements of paragraph 5.4 of this Regulation if they are contactable by a 100 mm diameter sphere when a registration plate is fitted in accordance with the vehicle manufacturer's recommendation.
6.16. Luggage racks and ski racks
6.16.1. Luggage racks and ski racks shall be so attached to the vehicle that positive locking exists in at least one direction and that horizontal, longitudinal and transverse forces can be transmitted which are at least equal to the vertical load-bearing capacity of the rack as specified by its manufacturer. For the test of the luggage rack or ski rack fixed to the vehicle according to the manufacturer's instructions, the test loads shall not be applied at one point only.
6.16.2. Surfaces which, after installation of the rack, can be contacted by a sphere of 165 mm diameter shall not have parts with a radius of curvature less than 2,5 mm, unless the provisions of paragraph 6.3 can be applied.
6.16.3. Fastening elements such as bolts that are tightened or loosened without tools shall not project more than 40 mm beyond the surfaces referred to in 6.16.2, the projection being determined according to the method prescribed in paragraph 2 of Annex 3, but using a sphere of 165 mm diameter in those cases where the method prescribed in paragraph 2.2 of that annex is employed.
6.17. Radio receiving and transmitting aerials
6.17.1. Radio receiving and transmitting aerials shall be fitted to the vehicle in such a way that if their unattached end is less than 2 m from the road surface in any position of use specified by the manufacturer of the aerial, it shall be inside the zone bounded by the vertical planes which are 10 cm inside the extreme outer edge of the vehicle as defined in paragraph 2.7.
6.17.2. Furthermore, aerials shall be so fitted to the vehicle, and if necessary their unattached ends so restricted, that no part of the aerials protrude beyond the extreme outer edge of the vehicle as defined in paragraph 2.7.
6.17.3. Shafts of aerials may have radii of curvature of less than 2,5 mm. However, the unattached ends shall be fitted with fixed cappings, the radii of curvature of which are not less than 2,5 mm.
6.17.4. The bases of aerials shall not project more than 30 mm when determined according to the procedure of Annex 3, paragraph 2. However, in the case of aerials with amplifiers built into the base, these bases may project up to 40 mm.
6.18. Assembly instructions
6.18.1. Luggage racks, ski racks and radio receiving or transmitting aerials that have been approved as separate technical units may not be offered for sale, sold or purchased unless accompanied by assembly instructions. The assembly instructions shall contain sufficient information to enable the approved components to be mounted on the vehicle in a manner that complies with the relevant provisions of paragraphs 5 and 6. In particular, the positions of use must be indicated for telescopic aerials.
7. MODIFICATION OF A VEHICLE TYPE AND EXTENSION OF APPROVAL
7.1. The administrative department which has granted approval of the vehicle type shall be notified of any modification of the vehicle type. That department may then;
7.1.1. either consider that the modifications made are unlikely to have an appreciable adverse effect;
7.1.2. or require a further report from the technical service conducting tests.
7.2. Confirmation of approval, with a description of the modifications, or refusal of approval shall be communicated, by the procedure laid down in paragraph 4.3 above, to the Parties to the Agreement applying this Regulation.
7.3. The competent authority issuing the extension of approval shall assign a series number for such an extension and inform thereof the other Parties to the 1958 Agreement applying this Regulation by means of a communication form conforming to the model in Annex 1 to this Regulation.
8. CONFORMITY OF PRODUCTION
8.1. Vehicle (separate technical unit) approved under this Regulation shall be so manufactured as to conform to the type approved by meeting the requirements set forth in paragraphs 5 and 6 above.
8.2. In order to verify that the requirements of paragraph 8.1 are met, suitable controls of the production shall be carried out.
8.3. The holder of the approval shall in particular:
8.3.1. ensure the existence of procedures for the effective control of the quality of products;
8.3.2. have access to the control equipment necessary for checking the conformity to each approved type;
8.3.3. ensure that data of test results are recorded and that related documents shall remain available for a period to be determined in accordance with the administrative service;
8.3.4. analyse the results of each type of test in order to verify and ensure the stability of the product characteristics making allowance for variation of an industrial production;
8.3.5. ensure that for each type of product at least the tests prescribed in Annex 3 to this Regulation are carried out;
8.3.6. ensure that any sampling of samples or test pieces giving evidence of non-conformity with the type of test considered shall give rise to another sampling and another test. All the necessary steps shall be taken to re-establish the conformity of the corresponding production.
8.4. The competent authority which has granted type approval may at any time verify the conformity control methods applicable to each production unit.
8.4.1. In every inspection, the test books and production survey records shall be presented to the visiting inspector.
8.4.2. The inspector may take samples at random to be tested in the manufacturer's laboratory. The minimum number of samples may be determined according to the results of the manufacturer's own verification.
8.4.3. When the quality level appears unsatisfactory or when it seems necessary to verify the validity of the tests carried out in the application of paragraph 8.4.2 above, the inspector shall select samples to be sent to the technical service which has conducted the type approval tests.
8.4.4. The competent authority may carry out any test prescribed in this Regulation.
8.4.5. The normal frequency of inspections authorised by the competent authority shall be one per two years. In the case where negative results are recorded during one of these visits, the competent authority shall ensure that all necessary steps are taken to re-establish the conformity of production as rapidly as possible.
9. PENALTIES FOR NON-CONFORMITY OF PRODUCTION
9.1. The approval granted in respect of a vehicle type pursuant to this Regulation may be withdrawn if the requirement laid down in paragraph 8.1 above is not complied with.
9.2. If a Party to the Agreement applying this Regulation withdraws an approval it has previously granted, it shall forthwith notify the other Contracting Parties applying this Regulation by means of a communication form conforming to the model in Annex 1 to this Regulation.
10. PRODUCTION DEFINITELY DISCONTINUED
If the holder of the approval completely ceases to manufacture the type approved in accordance with this Regulation, he shall so inform the authority which granted the approval. Upon receiving the relevant communication that authority shall inform thereof the other Parties to the 1958 Agreement which apply this Regulation by means of a communication form conforming to the model in Annex 1 to this Regulation.
11. NAMES AND ADDRESSES OF TECHNICAL SERVICES CONDUCTING APPROVAL TESTS AND OF ADMINISTRATIVE DEPARTMENTS
The Parties to the Agreement applying this Regulation shall communicate to the United Nations Secretariat the names and addresses of the technical services conducting approval tests and of the administrative departments, which grant approval and to which forms certifying approval or refusal or withdrawal of approval, issued in other countries, are to be sent.
[1] As defined in Annex 7 of the Consolidated Resolution on the Construction of Vehicles (R.E.3) (document TRANS/SC1/WP29/78/Amend.3).
[2] 1 for Germany, 2 for France, 3 for Italy, 4 for the Netherlands, 5 for Sweden, 6 for Belgium, 7 for Hungary, 8 for the Czech Republic, 9 for Spain, 10 for Yugoslavia, 11 for the United Kingdom, 12 for Austria, 13 for Luxembourg and 14 for Switzerland. 15 (vacant), 16 for Norway, 17 for Finland, 18 for Denmark, 19 for Romania, 20 for Poland, 21 for Portugal, 22 for the Russian Federation, 23 for Greece, 24 for Ireland, 25 for Croatia, 26 for Slovenia, 27 for Slovakia, 28 for Belarus, 29 for Estonia, 30 (vacant), 31 for Bosnia and Herzegovina, 32 for Latvia, 33 (vacant), 34 for Bulgaria, 35-36 (vacant), 37 for Turkey, 38-39 (vacant), 40 for The former Yugosla
v Republic of Macedonia, 41 (vacant), 42 for the European Community (Approvals are granted by its Member States using their respective ECE symbol), 43 for Japan, 44 (vacant), 45 for Australia and 46 for Ukraine. Subsequent numbers shall be assigned to other countries in the chronological order in which they ratify or accede to the Agreement Concerning the Adoption of Uniform Technical Prescriptions for Wheeled Vehicles, Equipment and Parts which can be Fitted and/or be Used on Wheeled Vehicles and the Conditions for Reciprocal Recognition of Approvals Granted on the Basis of these Prescriptions, and the numbers thus assigned shall be communicated by the Secretary-General of the United Nations to the Contracting Parties to the Agreement.
--------------------------------------------------
ANNEX 1
COMMUNICATION
(Maximum format: A4 (210 × 297 mm))
+++++ TIFF +++++
--------------------------------------------------
ANNEX 2
ARRANGEMENTS OF APPROVAL MARKS
Model A
(See paragraphs 4.1.4 and 4.2.4 of this Regulation)
+++++ TIFF +++++
The above approval mark affixed to a vehicle shows that the vehicle type concerned has been approved in the Netherlands (E4) pursuant to Regulation No 26 and under approval number 022439. The first two digits of the approval number indicate that, at the time of approval, Regulation No 26 included the 02 series of amendments.
Model B
(See paragraph 4.1.5 of this Regulation)
+++++ TIFF +++++
The above approval mark affixed to a vehicle shows that the vehicle type concerned has been approved in the Netherlands (E4) pursuant to Regulation No 26 and Regulation No 24 [1]. The first two digits of the approval numbers indicate that, at the dates when the respective approvals were given, Regulation No 26 included the 02 series of amendments, while Regulation No 24 already included the 03 series of amendments.
[1] The second Regulation number is given merely as an example; the corrected absorption coefficient is 1,30 m–1.
--------------------------------------------------
ANNEX 3
METHODS OF DETERMINING THE DIMENSIONS OF PROJECTIONS AND GAPS
1. METHOD OF DETERMINING THE HEIGHT OF THE PROJECTION OF FOLDS IN BODY PANELS
1.1. The height H of a projection is determined graphically by reference to the circumference of a 165 mm diameter circle, internally tangential to the external outline of the external surface at the section to be checked.
1.2. H is the maximum value of the distance, measured along a straight line passing through the centre of the 165 mm diameter circle between the circumference of the aforesaid circle and the external contour of the projection (see Fig. 1).
1.3. In cases where it is not possible for a 100 mm diameter circle to contact externally part of the external outline of the external surface at the section under consideration, the surface outline in this area will be assumed to be that formed by the circumference of the 100 mm diameter circle between its tangent points with the external outline (see Fig. 2).
1.4. Drawings of the necessary sections through the external surface shall be provided by the manufacturer to allow the height of the projections referred to above to be measured.
+++++ TIFF +++++
+++++ TIFF +++++
2. METHOD OF DETERMINING THE DIMENSION OF THE PROJECTION OF A COMPONENT MOUNTED ON THE EXTERNAL SURFACE
2.1. The dimension of the projection of a component which is mounted on a convex surface may be determined either directly or by reference to a drawing of an appropriate section of this component in its installed condition.
2.2. If the dimension of the projection of a component which is mounted on a surface other than convex cannot be determined by simple measurement, it shall be determined by the maximum variation of the distance of the centre of a 100 mm diameter sphere from the nominal line of the panel when the sphere is moved over and is in constant contact with that component. Figure 3 shows an example of the use of this procedure.
3. METHOD OF DETERMINING THE PROJECTION OF HEADLAMP VISORS AND RIMS
3.1. The projection from the external surface of the headlamp shall be measured horizontally from the point of contact of a 100 mm diameter sphere as shown in Figure 4.
4. METHOD OF DETERMINING THE DIMENSION OF A GAP OR THE SPACE BETWEEN ELEMENTS OF A GRILLE
4.1. The dimension of a gap or space between elements of a grille shall be determined by the distance between two planes passing through the points of contact of the sphere and perpendicular to the line joining those points of contact. Figures 5 and 6 show examples of the use of this procedure.
+++++ TIFF +++++
+++++ TIFF +++++
+++++ TIFF +++++
+++++ TIFF +++++
--------------------------------------------------
ANNEX 4
COMMUNICATION
(Maximum format: A4 (210 × 297 mm))
+++++ TIFF +++++
--------------------------------------------------
