Agreement in the Form of an Exchange of Letters
Concerning amendments to the annexes to the Agreement between the European Community and the United States of America on sanitary measures to protect public and animal health in trade in live animals and animal products
A. Letter from the European Community
Brussels, 7 October 2004
Your Excellency,
With reference to the Agreement between the European Community and the United States of America on sanitary measures to protect public and animal health in trade in live animals and animal products, I have the honour to propose to you to amend the Annexes of the Agreement as follows:
As recommended by the Joint Management Committee established under Article 14(1) of the Agreement, replace the texts of point 6 of Annex V and of footnote (1) to the Agreement with the text in Appendix A to the present letter, and replace the text of Annex VI to the Agreement, with the text in Appendix B to the present letter.
I would be obliged if you would confirm the agreement of the United States of America to such amendments to point 6 of Annex V, to footnote (1), and to Annex VI to the Agreement.
Please accept, Sir, the assurance of my highest consideration.
Enclosure: Appendix A to replace Annex V, point 6 and footnote (1) to the Agreement and Appendix B to replace Annex VI to the Agreement
[1]
For the European Community
Jaana Husu-Kallio
B. Letter from the United states of America
Brussels, 6 April 2005
Dear Madam,
I refer to your letter of 7 October 2004 containing details of the proposed Appendix A to replace Annex V, point 6 and footnote (1) and Appendix B to replace Annex VI of the Agreement of July 20 1999 between the United States of America and the European Community on the sanitary measures to protect public and animal health in trade in live animals and animal products.
In this regard, I have the honor to confirm the acceptability to the United States of America of the proposed amendments as recommended by the Joint Management Committee established under Article 14(1) of the Agreement, a copy of which is attached hereto. It is my understanding that these amendments shall take effect on the date on which the EC notifies the US that it has completed the necessary procedures for implementing these amendments.
Please accept the assurances of my high consideration.
Enclosure: Appendix A to replace Annex V, point 6 and footnote (1) to the Agreement and Appendix B to replace Annex VI to the Agreement
[2]
For the Ambassador
Norval E. Francis
[1]
[2]
--------------------------------------------------
