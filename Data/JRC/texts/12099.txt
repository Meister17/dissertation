Commission Regulation (EC) No 22/2006
of 9 January 2006
opening a standing invitation to tender for the resale on the Community market of sugar held by the intervention agencies of Belgium, the Czech Republic, Spain, France, Ireland, Italy, Hungary, Poland, Slovakia and Sweden
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1260/2001 of 19 June 2001 on the common organisation of the market in the sugar sector [1], and in particular Article 9(3) thereof,
Whereas:
(1) Belgium, the Czech Republic, Spain, France, Ireland, Italy, Hungary, Poland, Slovakia and Sweden have intervention stocks of sugar. In order to respond to market needs, it is appropriate to make these stocks available on the internal market.
(2) Commission Regulation (EC) No 1262/2001 of 27 June 2001 laying down detailed rules for implementing Council Regulation (EC) No 1260/2001 as regards the buying in and sale of sugar by intervention agencies [2] should apply to such a sale.
(3) However, Article 22(2) and (3) of Regulation (EC) No 1262/2001 providing for the publication of the invitation of tender in the "C" series of the Official Journal of the European Union at least 10 days before the period for submission of tenders expires should not apply as Member States have difficulties for the translation into all Community languages and this will create unnecessary delays in the sales of their intervention sugar. Furthermore, Article 28(1)(a) of the said Regulation provides for a tendering security of EUR 0,73 per 100 kilograms. For the sale of intervention sugar on the internal market, the security lodged by the tenderer should be brought in line with the intervention price. As a consequence, Article 28(1)(a) should not apply.
(4) To take account of the situation on the Community market, provision should be made for the Commission to fix a minimum selling price for each partial invitation to tender.
(5) The intervention agencies of Belgium, the Czech Republic, Spain, France, Ireland, Italy, Hungary, Poland, Slovakia and Sweden should communicate the tenders to the Commission. The tenderers should remain anonymous.
(6) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Sugar,
HAS ADOPTED THIS REGULATION:
Article 1
The intervention agencies of Belgium, the Czech Republic, Spain, France, Ireland, Italy, Hungary, Poland, Slovakia and Sweden shall offer for sale by standing invitation to tender on the Community internal market a total quantity of 1009124 tonnes of sugar accepted into intervention and available for sale on the internal market. The Member States concerned and the quantities involved are laid down in Annex I.
Article 2
1. The tenders and the sales provided for in Article 1 shall take place in accordance with Regulation (EC) No 1262/2001, except as otherwise provided by this Regulation.
2. By way of derogation from Article 22(2) and (3) of Regulation (EC) No 1262/2001, each intervention agency concerned shall draw up a notice of invitation to tender and publish it at least eight days before the beginning of the period for the submission of tenders.
The notice shall indicate, in particular, the terms of the invitation to tender.
The notice, and all changes to it, shall be forwarded to the Commission before publication.
Article 3
The minimum bid for each partial invitation to tender shall be 250 tonnes.
Article 4
1. The period during which tenders may be submitted in response to the first partial invitation to tender shall begin on 26 January 2006 and shall end on 1 February 2006 at 15.00, Brussels time.
The periods during which tenders may be submitted in response to the second and subsequent partial invitations shall begin on the first working day following the end of the preceding period. They shall end at 15.00, Brussels time:
- on 15 February 2006,
- on 1, 15 and 29 March 2006,
- on 5 and 19 April 2006,
- on 3, 17 and 31 May 2006,
- on 7, 14, 21 and 28 June 2006.
2. Tenders shall be lodged with the intervention agency holding the sugar as laid down in Annex I.
Article 5
By way of derogation from Article 28(1)(a) of Regulation (EC) No 1262/2001, a tendering security of EUR 20 per 100 kg of sugar shall be lodged by each tenderer.
Article 6
The intervention agencies concerned shall communicate to the Commission tenders submitted within two hours from the expiry of the deadline for the submissions laid down in Article 4(1).
The tenderers shall not be identified.
Tenders submitted shall be communicated in electronic form according to be the model laid down in the Annex II.
When no tenders are submitted, the Member State shall communicate this to the Commission within the same time limit.
Article 7
1. The Commission shall fix per Member State concerned the minimum sale price or decide not to accept the tenders in accordance with the procedure referred to in Article 42(2) of Regulation (EC) No 1260/2001.
2. Where an award at a minimum price set pursuant to paragraph 1 would result in the available quantity for that Member State being exceeded, that award shall be limited to such quantity as is still available.
Where awards for a Member State to all tenderers offering the same price would result in the quantity for that Member State being exceeded, then the quantity available shall be awarded as follows:
(a) by division among the tenderers concerned in proportion of the total quantities in each of their tenders; or
(b) by apportionment among the tenderers concerned by reference to a maximum tonnage fixed for each of them; or
(c) by drawing of lots.
Article 8
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 9 January 2006.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 178, 30.6.2001, p. 1. Regulation as last amended by Commission Regulation (EC) No 39/2004 (OJ L 6, 10.1.2004, p. 16).
[2] OJ L 178, 30.6.2001, p. 48. Regulation amended by Regulation (EC) No 1498/2005 (OJ L 240, 16.9.2005, p. 39).
--------------------------------------------------
ANNEX I
Member States holding intervention sugar
Member State | Intervention Agency | Quantities held by the intervention agency and available for the sale on the internal market |
Belgium | Bureau d’intervention et de restitution belge Rue de Trèves, 82 B-1040 Bruxelles Tel. (32-2) 287 24 11 Fax (32-2) 287 25 24 | 100539 |
Czech Republic | Státní zemědělský intervenční fond Oddělení pro cukr a škrob Ve Smečkách 33 CZ-11000 Praha 1 Tel. 420 222 871 886 Fax 420 296 806 404 | 13000 |
Spain | Fondo Español de Garantía Agraria C/Beneficencia, 8 E-28004 Madrid Tel. (34-91) 347 64 66 Fax (34-91) 347 63 97 | 8300 |
France | Fonds d’intervention et de régularisation du marché du sucre Bureau de l’intervention 21, avenue Bosquet F-75007 Paris Tél. (33) 144 18 23 37 Fax (33) 144 18 20 08 | 20000 |
Ireland | Intervention Section On Farm Investment Subsidies & storage Division Department of Agriculture & Food Johnstown Castle Estate Wexford Tel. (353) 536 34 37 Fax (353) 534 28 41 | 12000 |
Italy | AGEA — Agenzia per le erogazioni in Agricoltura Ufficio ammassi pubblici e privati e alcool Via Torino, 45 00185 Roma Tel. (39) 06 49 49 95 58 Fax (39) 06 49 49 97 61 | 571111 |
Hungary | Mezőgazdasági és Vidékfejlesztési Hivatal (MVH) (Agricultural and Rural Development Agency) Soroksári út 22-24 H-1095 Budapest Tel. (36-1) 219 62 13 Fax (36-1) 219 89 05 or (36-1) 219 62 59 | 110500 |
Poland | Agencja Rynku Rolnego Biuro Cukru Dział Dopłat i Interwencji Nowy Świat 6/12 00-400 Warszawa Tel. (48) 226 61 71 30 Fax (48) 226 61 72 77 | 94636 |
Slovakia | Pôdohospodárska platobná agentúra Oddelenie cukru a ostatných komodít Dobrovičova, 12 SK-81526 Bratislava Tel. (421-2) 58 24 32 55 Fax (421-2) 58 24 33 62 | 20000 |
Sweden | Statens jordbruksverk Vallgatan 8 S-55182 Jönköping Tel. (46-36) 15 50 00 Fax (46-36) 19 05 46 | 59038 |
--------------------------------------------------
ANNEX II
Standing invitation to tender for the resale of sugar held by the intervention agencies
Form [1]
Model for the communication to the Commission as referred to in Article 6
(Regulation (EC) No 22/2006)
1 | 2 | 3 | 4 | 5 |
Member State selling intervention sugar | Numbering of tenderers | Lot No | Quantity (t) | Tender price EUR/100 kg |
| 1 | | | |
| 2 | | | |
| 3 | | | |
| etc. | | | |
[1] To be faxed to the following number: (32-2) 292 10 34.
--------------------------------------------------
