Commission Decision
of 23 October 2006
suspending the definitive anti-dumping duty imposed by Regulation (EC) No 215/2002 on imports of ferro molybdenum originating in the People’s Republic of China
(2006/714/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 384/96 of 22 December 1995 on protection against dumped imports from countries not members of the European Community [1] (the basic Regulation), and in particular Article 14(4) thereof,
After consulting the Advisory Committee,
Whereas:
A. PROCEDURE
(1) The Council, by Regulation (EC) No 215/2002 of 28 January 2002 [2], imposed a definitive anti-dumping duty on imports of ferro molybdenum originating in the People’s Republic of China (PRC), falling under CN code 72027000 (the product concerned). The rate of the anti-dumping duty is 22,5 %.
(2) Information on a change of market conditions which occurred after the original investigation period (i.e. from 1 October 1999 to 30 September 2000), and which might justify the suspension of the measures currently in force, in accordance with Article 14(4) of the basic Regulation, was provided to the Commission. Consequently, the Commission examined whether such suspension was warranted.
B. GROUNDS
(3) Article 14(4) of the basic Regulation provides that, in the Community interest, anti-dumping measures may be suspended on the grounds that market conditions have temporarily changed to an extent that injury would be unlikely to resume as a result of such suspension, provided that the Community industry has been given an opportunity to comment and these comments have been taken into account. Article 14(4) further specifies that the anti-dumping measures concerned may be reinstated at any time if the reason for suspension is no longer applicable.
(4) Eurofer, on behalf of a number of users of the product concerned, has alleged that in the time since the investigation period, the market situation has changed. The complainants in the original investigation and other Community producers of the product concerned, represented by Euroalliages, commented on these allegations, and an adversarial exchange of views took place.
(5) Since the definitive imposition of the measures in February 2002, Chinese imports have decreased substantially. Eurostat statistics point to a drop from ca. 12 KT in 2001 to virtually no imports in the period 1 April 2005- 31 March 2006. Euroalliages calculated a higher degree of penetration with imports reaching over 1 KT based on the allegation that certain imports declared as having a Dutch origin were in reality Chinese. In any event, even under this assumption, the very significant drop in import penetration is clear.
(6) Regarding imports from other third countries, these have increased from ca. 2,7 KT to 10,7 KT, thus compensating partially the drop in Chinese imports. Consumption has increased by 14 %.
(7) Market prices in the Community have increased from ca. EUR 8/kg in the original IP to ca. EUR 80/kg in 2005 and are in the range of ca. EUR 60/kg in 2006. These trends can also be found in other major markets across the world.
(8) Out of the factors claimed by the parties the main explanation for this price increase seems to be a shortage in roasting capacities, i.e., the capacities to transform molybdenum concentrate into molybdenum oxide (which is then converted into ferro molybdenum). This driving factor explains to a large extent the price increases and the demand-supply imbalance that this has generated in the Community market. On the basis of the information presented, it appears that the roasting capacity shortfall will disappear in all likelihood in the course of 2007 as a result of new roasting capacity coming on stream.
(9) With regard to the Community industry, it is to be noted that since the imposition of the measures, the situation of the Community industry has improved. The sales and production volumes have increased by 25 % and 5 % respectively, reaching a market share of ca. 26 %. The profit situation also improved. Although the Community industry has not consistently achieved the normal 5 % profit level established by the original investigation, the Community industry has, however, gained up to 5 percentage points and become profitable.
(10) Chinese export prices to third countries have followed the same upward trend described above indicating that should measures be suspended it is unlikely that they would decrease in the very short term to such an extent that injury would resume.
(11) No indications have been found as to why the suspension would not be in the Community interest.
C. CONCLUSION
(12) In conclusion, given the temporary change in market conditions, and in particular the high level of prices of the product concerned practised on the Community market, which is far above the injurious level found in the original investigation, together with the alleged demand-supply imbalance of the product concerned, it is considered that the injury linked to the imports of the product concerned originating in the PRC is unlikely to resume as a result of the suspension. It is therefore proposed to suspend for nine months the measures in force in accordance with Article 14(4) of the basic Regulation.
(13) Pursuant to Article 14(4) of the basic Regulation, the Commission has informed the Community industry of its intention to suspend the anti-dumping measures in force. The Community industry has been given an opportunity to comment. The Community industry did not oppose the suspension of the anti-dumping measures in force.
(14) The Commission therefore considers that all requirements for suspending the anti-dumping duty imposed on the product concerned are met, in accordance with Article 14(4) of the basic Regulation. Consequently, the anti-dumping duty imposed by Regulation (EC) No 215/2002 should be suspended for a period of nine months.
(15) The Commission will monitor the development of imports and the prices of the product concerned. Should a situation arise at any time in which increased volumes at dumped prices of the product concerned from the PRC resume and consequently cause injury to the Community industry, the Commission will reinstate the anti-dumping duty by repealing the present suspension,
HAS ADOPTED THIS DECISION:
Article 1
The definitive anti-dumping duty imposed by Council Regulation (EC) No 215/2002 on imports of ferro molybdenum, falling within CN code 72027000, and originating in the People's Republic of China is hereby suspended for a period of nine months.
Article 2
This Decision shall enter into force on the date following that of its publication in the Official Journal of the European Union.
Done at Brussels, 23 October 2006.
For the Commission
Peter Mandelson
Member of the Commission
[1] OJ L 56, 6.3.1996, p. 1. Regulation as last amended by Regulation (EC) No 2117/2005 (OJ L 340, 23.12.2005, p. 17).
[2] OJ L 35, 6.2.2002, p. 1.
--------------------------------------------------
