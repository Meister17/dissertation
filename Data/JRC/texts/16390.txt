COMMISSION REGULATION (EC) No 770/96 of 26 April 1996 amending Regulation (EEC) No 3002/92 laying down common detailed rules for verifying the use and/or destination of products from intervention
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation No 136/66/EEC of 22 September 1966 on the establishment of a common organization of the market in oils and fats (1), as last amended by Regulation (EC) No 3290/94 (2), and in particular Articles 12 (4) and 26 (3) thereof, and to the corresponding provisions of the other Regulations establishing a common organization of agricultural product markets,
Whereas Commission Regulation (EEC) No 3002/92 (3), as last amended by Regulation (EEC) No 1938/93 (4), lays down common detailed rules notably as to the release of the security which guarantees the proper use and/or destination of products from intervention;
Whereas Article 8 (1) of Council Regulation (EEC) No 729/70 (5) of 21 April 1970 on the financing of the common agricultural policy, as last amended by Regulation (EC) No 1287/95 (6), requires Member States to take the measures necessary to recover sums lost as a result of irregularities in accordance with national provisions laid down by law, regulation or administrative action;
Whereas in order to ensure equal treatment of sales from intervention stocks at a reduced price level and comparable schemes which provide for the granting of an aid, notably export refunds, to ensure equal treatment for operators in the Member States and to facilitate the recovery of economic advantages unduly granted it is appropriate to provide for the payment of an amount equal to the amount of the security unduly released;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the relevant management committees,
HAS ADOPTED THIS REGULATION:
Article 1
The following Article is inserted in Regulation (EEC) No 3002/92:
'Article 5a
1. Where, after the release in whole or in part of the security referred to in Article 5, it is established that the products in whole or in part did not reach the prescribed use and/or destination the competent authority of the Member State where the security has been released shall require, in accordance with Article 8 (1) of Regulation (EEC) No 729/70, the operator concerned to pay an amount equal to the amount of the security which would have been forfeited if the failure would have been taken into account before the release of the security. This amount shall be increased by interest calculated from the date of release to the day preceding the date of payment.
The receipt by the competent authority of the amount referred to in the preceding subparagraph shall constitute the recovery of the economic advantage unduly granted.
2. The payment shall be made within 30 days from the day of receipt of the demand for payment.
Whereas the time limit for payment is not met, Member States may decide that, instead of payment, the amount to be received shall be deducted from subsequent payments to the operator concerned.
3. The interest rate shall be calculated in accordance with the provisions of national law but may not be less than the rate applicable for the recovery of national amounts.
No interest shall be levied, or at the most an amount to be determined by the Member State corresponding to the undue profit, if the release of the security was an error of the competent authority.
4. Member States may refrain from demanding the payment referred to in paragraph 1 where the amount does not exceed ECU 60, provided that, under national law, such cases are covered by similar rules.
5. The amount received in accordance with paragraph 1 shall be paid to the paying agency which shall deduct the amounts concerned from the European Agricultural Guidance and Guarantee Fund (EAGGF) expenditure, without prejudice to Article 7 of Council Regulation (EEC) No 595/91 (*).
(*) OJ No L 67, 14. 3. 1991, p. 11.`
Article 2
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Communities.
It shall apply to securities given on or after that date.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 26 April 1996.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ No 172, 30. 9. 1966, p. 3025/66.
(2) OJ No L 349, 31. 12. 1994, p. 105.
(3) OJ No L 301, 17. 10. 1992, p. 17.
(4) OJ No L 176, 20. 7. 1993, p. 12.
(5) OJ No L 94, 28. 4. 1970, p. 13.
(6) OJ No L 125, 8. 6. 1995, p. 1.
