Commission Regulation (EC) No 2120/2005
of 22 December 2005
amending Regulation (EC) No 638/2003 laying down detailed rules for applying Council Regulation (EC) No 2286/2002 and Council Decision 2001/822/EC as regards the arrangements applicable to imports of rice originating in the African, Caribbean and Pacific States (ACP States) and the overseas countries and territories (OCT)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Union,
Having regard to Council Regulation (EC) No 1785/2003 of 29 September 2003 on the common organisation of the market in rice [1], and in particular Article 13(1) thereof,
Having regard to Council Regulation (EC) No 2286/2002 of 10 December 2002 on the arrangements applicable to agricultural products and goods resulting from the processing of agricultural products originating in the African, Caribbean and Pacific States (ACP States) [2], and in particular Article 5 thereof,
Having regard to Council Decision 2001/822/EC of 27 November 2001 on the association of the overseas countries and territories with the European Community (Overseas Association Decision) [3], and in particular the seventh subparagraph of Article 6(5) of Annex III thereto,
Whereas:
(1) Regulation (EC) No 2286/2002 implements the arrangements for imports from the ACP States made as a result of the ACP-EC Partnership Agreement signed in Cotonou on 23 June 2000.
(2) Decision 2001/822/EC lays down that the cumulation of ACP/OCT origin within the meaning of Article 6(1) and (5) of Annex III to that Decision is allowed within a total annual quantity of 160000 tonnes of rice, expressed as husked-rice equivalent, for the products falling within CN code 1006.
(3) Commission Regulation (EC) No 638/2003 [4] provides for the issue of import licences at intervals designed to ensure balanced market management. This aim has not been fully achieved under current market management conditions in view of the harvest periods in the ACP States and overseas countries and territories concerned. In order to solve this problem and to bring the issue of licences more closely into line with the harvest period in the ACP States and overseas countries and territories concerned, the tranche currently fixed for the month of January should be put back a month and Regulation (EC) No 638/2003 should be amended accordingly.
(4) To allow optimal management of the tariff quotas concerned, this Regulation should apply from 1 January 2006.
(5) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Cereals,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 638/2003 is hereby amended as follows:
(a) in Article 3(1) "January" is replaced by "February";
(b) in Article 5(1) "January" is replaced by "February";
(c) Article 10(1) is amended as follows:
(i) in point (a), "January" is replaced by "February";
(ii) in point (b) "January" is replaced by "February".
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Union.
It shall apply from 1 January 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 22 December 2005.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 270, 21.10.2003, p. 96.
[2] OJ L 348, 21.12.2002, p. 5.
[3] OJ L 314, 30.11.2001, p. 1.
[4] OJ L 93, 10.4.2003, p. 3. Regulation as amended by Regulation (EC) No 1950/2005 (OJ L 312, 29.11.2005, p. 18).
--------------------------------------------------
