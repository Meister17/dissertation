Commission Decision
of 4 August 2006
drawing up the list of regions eligible for funding from the Structural Funds on a transitional and specific basis under the Regional competitiveness and employment objective for the period 2007-2013
(notified under document number C(2006) 3480)
(2006/597/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1083/2006 of 11 July 2006 laying down general provisions for the European Regional Development Fund, the European Social Fund and the Cohesion Fund and repealing Regulation (EC) No 1260/1999 [1], and in particular Article 8(4) thereof,
Whereas:
(1) Pursuant to point (b) of Article 3(2) of Regulation (EC) No 1083/2006 the Regional competitiveness and employment objective aims at strengthening the competitiveness and attractiveness of regions.
(2) Pursuant to the first subparagraph of Article 8(2) of Regulation (EC) No 1083/2006 the NUTS level II regions totally covered by Objective 1 in 2006 under Article 3 of Council Regulation (EC) No 1260/1999 of 21 June 1999 laying down general provisions on the Structural Funds [2] whose nominal per capita gross domestic product (GDP) level, measured and calculated according to Article 5(1) of Regulation (EC) No 1083/2006, will exceed 75 % of the average GDP of the EU 15 shall be eligible, on a transitional and specific basis, for financing by the Structural Funds under the Regional competitiveness and employment objective.
(3) Pursuant to the second subparagraph of Article 8(2) of Regulation (EC) No 1083/2006, Cyprus shall also benefit in 2007-2013 from the transitional financing applicable to the regions referred to in the first subparagraph of the same Article.
(4) It is necessary to establish the lists of eligible regions accordingly,
HAS ADOPTED THIS DECISION:
Article 1
The regions eligible for funding from the Structural Funds under the Regional competitiveness and employment objective on a transitional and specific basis, as referred to in Article 8(2) of Regulation (EC) No 1083/2006, shall be those listed in the Annex to this Decision.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 4 August 2006.
For the Commission
Danuta Hübner
Member of the Commission
[1] OJ L 210, 31.7.2006, p. 25.
[2] OJ L 161, 26.6.1999, p. 1. Regulation as last amended by Regulation (EC) No 1198/2006 (OJ L 223, 15.8.2006, p. 1).
--------------------------------------------------
ANNEX
List of NUTS II regions eligible for funding from the Structural Funds on a transitional and specific basis under the Regional competitiveness and employment objective for the period from 1 January 2007 to 31 December 2013
GR24 Sterea Ellada
GR42 Notio Aigaio
ES41 Castilla y León
ES52 Comunidad Valenciana
ES70 Canarias
IE01 Border, Midland and Western
ITG2 Sardegna
CY00 Kypros/Kıbrıs
HU10 Közép-Magyarország
PT30 Região Autónoma da Madeira
FI13 Itä-Suomi
UKD5 Merseyside
UKE3 South Yorkshire
--------------------------------------------------
