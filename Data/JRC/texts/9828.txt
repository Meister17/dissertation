Notice of initiation of a partial interim review of the anti-dumping measures applicable to imports of bicycles originating in the People's Republic of China
(2006/C 5/02)
The Commission has decided on its own initiative to initiate a partial interim review pursuant to Article 11(3) of Council Regulation (EC) No 384/96 on protection against dumped imports from countries not members of the European Community ("the basic Regulation") [1]. The review is limited in scope to dumping aspects as far as one exporting producer, Giant China Co. Ltd., ("the company") is concerned.
1. Product
The product under review is bicycles and other cycles (including delivery tricycles, but excluding unicycles), not motorised originating in the People's Republic of China ('the product concerned'), currently classifiable within CN codes ex87120010, 87120030 and ex87120080. These CN codes are given only for information.
2. Existing measures
The measures currently in force are a definitive anti-dumping duty imposed by Council Regulation (EC) No 1524/2000 [2], as last amended by Regulation (EC) No 1095/2005 [3].
3. Grounds for the review
There is sufficient prima facie evidence available to the Commission, that the circumstances on the basis of which the existing measures were established have changed and that these changes are of a lasting nature.
The information at the Commission's disposal indicates that market economy conditions prevail for the company as demonstrated by the fact that it fulfils the criteria of Article 2(7)(c) of the basic Regulation. Furthermore, a comparison of normal value based on the company's own costs/domestic prices and its export prices would lead to a reduction of dumping significantly below the level of the current measure. Therefore, the continued imposition of measures at the existing levels, which were based on the level of dumping previously established, is no longer necessary to offset dumping.
4. Procedure for the determination of dumping
Having determined, after consulting the Advisory Committee, that sufficient evidence exists to justify the initiation of a partial interim review, the Commission hereby initiates a review in accordance with Article 11(3) of the basic Regulation, with a view to determine whether the company operates under market economy conditions as defined in Article 2(7)(c) of the basic Regulation or alternatively whether the company fulfils the requirements to have an individual duty established in accordance with Article 9(5) of the basic Regulation and, if so, to determine the company's individual margin of dumping and, should dumping be found, the level of the duty to which its imports of the product concerned into the Community should be subject.
The investigation will assess the need for the continuation, removal or amendment of the existing measures in respect of the company mentioned above.
(a) Questionnaires
In order to obtain the information it deems necessary for its investigation, the Commission will send questionnaires to the company and to the authorities of the exporting country concerned. This information and supporting evidence should reach the Commission within the time limit set in point 5(a)(i) of this notice.
(b) Collection of information and holding of hearings
All interested parties are hereby invited to make their views known, submit information other than questionnaire replies and to provide supporting evidence. This information and supporting evidence must reach the Commission within the time limit set in point 5(a)(i) of this notice.
Furthermore, the Commission may hear interested parties, provided that they make a request showing that there are particular reasons why they should be heard. This request must be made within the time limit set in point 5(a)(ii) of this notice.
(c) Market economy status
In the event that the company provides sufficient evidence showing that it operates under market economy conditions, i.e. that it meets the criteria laid down in Article 2(7)(c) of the basic Regulation, normal value will be determined in accordance with Article 2(7)(b) of the basic Regulation. For this purpose, a duly substantiated claim must be submitted within the specific time limit set in point 5(b) of this notice. The Commission will send a claim form to the company as well as to the authorities of the People's Republic of China.
(d) Selection of the market economy country
In the event that the company is not granted market economy status but fulfils the requirements to have an individual duty established in accordance with Article 9(5) of the basic Regulation, an appropriate market economy country will be used for the purpose of establishing normal value in respect of the People's Republic of China in accordance with Article 2(7)(a) of the basic Regulation. The Commission envisages using Mexico again for this purpose as was done in the investigation which led to the imposition of the current measures on imports of the product concerned from the People's Republic of China. Interested parties are hereby invited to comment on the appropriateness of this choice within the specific time limit set in point 5(c) of this notice.
Furthermore, in the event that the company is granted market economy status, the Commission may, if necessary, also use findings concerning the normal value established in an appropriate market economy country, e.g. for the purpose of replacing any unreliable cost or price elements in the People's Republic of China which are needed in establishing the normal value, if reliable required data are not available in the People's Republic of China. The Commission envisages using Mexico also for this purpose.
5 Time limits
(a) General time limits
(i) For parties to make themselves known, to submit questionnaire replies and any other information
All interested parties, if their representations are to be taken into account during the investigation, must make themselves known by contacting the Commission, present their views and submit questionnaire replies or any other information within 40 days of the date of publication of this notice in the Official Journal of the European Union, unless otherwise specified. Attention is drawn to the fact that the exercise of most procedural rights set out in the basic Regulation depends on the party's making itself known within the aforementioned period. Furthermore, the company has to submit its questionnaire reply within the aforementioned period of 40 days.
(ii) Hearings
All interested parties may also apply to be heard by the Commission within the same 40-day time limit.
(b) Specific time limit for submission of claims for market economy status
The company's duly substantiated claim for market economy status, as mentioned in point 4(c) of this notice, must reach the Commission within 21 days of the date of publication of this notice in the Official Journal of the European Union.
(c) Specific time limit for the selection of the market economy country
Parties to the investigation may wish to comment on the appropriateness of Mexico which, as mentioned in point 4(d) of this notice, is envisaged as a market economy country for the purpose of establishing normal value in respect of the People's Republic of China. These comments must reach the Commission within 10 days of the date of publication of this notice in the Official Journal of the European Union.
6. Written submissions, questionnaire replies and correspondence
All submissions and requests made by interested parties must be made in writing (not in electronic format, unless otherwise specified) and must indicate the name, address, e-mail address, telephone and fax numbers of the interested party. All written submissions, including the information requested in this notice, questionnaire replies and correspondence provided by interested parties on a confidential basis shall be labelled as "Limited" [4] and, in accordance with Article 19(2) of the basic Regulation, shall be accompanied by a non-confidential version, which will be labelled "For inspection by interested parties".
Commission address for correspondence:
European Commission
Directorate General for Trade
Directorate B
Office: J-79 5/16
B-1049 Brussels
Fax: (32-2) 295 65 05
7. Non-cooperation
In cases in which any interested party refuses access to or does not provide the necessary information within the time limits, or significantly impedes the investigation, provisional or final findings, affirmative or negative, may be made in accordance with Article 18 of the basic Regulation, on the basis of the facts available.
Where it is found that any interested party has supplied false or misleading information, the information shall be disregarded and use may be made of the facts available. If an interested party does not cooperate or cooperates only partially and findings are therefore based on facts available in accordance with Article 18 of the basic Regulation, the result may be less favourable to that party than if it had cooperated.
[1] OJ L 56, 6.3.1996, p. 1. Regulation as last amended by Council Regulation (EC) No 461/2004 (OJ L 77, 13.3.2004, p. 12).
[2] OJ L 175, 14.7.2000, p. 39.
[3] OJ L 183, 14.7.2005, p. 1.
[4] This means that the document is for internal use only. It is protected pursuant to Article 4 of Regulation (EC) No 1049/2001 of the European Parliament and of the Council (OJ L 145, 31.5.2001, p. 43). It is a confidential document pursuant to Article 19 of Council Regulation (EC) No 384/96 (OJ L 56, 6.3.1996, p. 1) and Article 6 of the WTO Agreement on Implementation of Article VI of the GATT 1994 (Anti-dumping Agreement).
--------------------------------------------------
