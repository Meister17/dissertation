Commission Decision
of 7 August 2001
amending Decision 94/467/EC laying down health conditions for the transit of equidae from one third country to another in accordance with Article 9(1) of Council Directive 91/496/EEC
(notified under document number C(2001) 2482)
(Text with EEA relevance)
(2001/662/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/496/EEC of 15 July 1991 laying down the principles governing the organisation of veterinary checks on animals entering the Community from third countries and amending Directives 89/662/EEC, 90/425/EEC and 90/675/EEC(1), as last amended by Directive 96/43/EC(2), and in particular Article 9(1)(c) thereof,
Whereas:
(1) Council Directive 90/426/EEC of 26 June 1990 on animal health conditions governing the movement and import from third countries of equidae(3) was last amended by Decision 2001/298/EC(4).
(2) Commission Decision 92/260/EEC(5), as last amended by Decision 2001/619/EC(6), laid down the animal health conditions and veterinary certification for temporary admission of registered horses. This Decision requires among others a certain residence in the country of dispatch. The residence in Member States or certain listed third countries may however count for the calculation of the period considered, provided that at least the same health requirements are fulfilled.
(3) Commission Decision 94/467/EC(7) laid down animal health conditions for the transit of equidae from one third country to another in accordance with Article 9(1) of Directive 91/496/EEC. The details of the animal health conditions refer to those laid down in Decision 92/260/EEC.
(4) The current situation complicates unnecessarily the transit through Member States of certain equidae otherwise eligible for temporary admission or permanent imports into the Community. It is the purpose of this Decision to ease this transit for registered horses.
(5) The measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
A third paragraph is added to Article 1 of Decision 94/467/EC as follows: "3. Derogating from the provisions in paragraph 2 and only in the case of registered horses, the list of countries in the third indent in paragraph (d) of Section III of the certificates A, B, C, D and E in Annex II to Decision 92/260/EEC shall be replaced by the list of third countries in Groups A to E in Annex I to Decision 92/260/EEC."
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 7 August 2001.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 268, 24.9.1991, p. 56.
(2) OJ L 162, 1.7.1996, p. 1.
(3) OJ L 224, 18.8.1990, p. 42.
(4) OJ L 102, 12.4.2001, p. 63.
(5) OJ L 130, 15.5.1992, p. 67.
(6) OJ L 215, 9.8.2001, p. 55.
(7) OJ L 190, 26.7.1994, p. 28.
