COUNCIL DECISION of 27 January 1997 concerning the conclusion on behalf of the European Community of an Exchange of Letters recording the common understanding on the principles of international cooperation on research and development activities in the domain of intelligent manufacturing systems between the European Community and the United States of America, Japan, Australia, Canada and the EFTA countries of Norway and Switzerland (97/378/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 130m, in conjunction with Article 228 (2), first sentence, and the first subparagraph of Article 228 (3) thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament (1),
Whereas international cooperation in the domain of intelligent manufacturing systems will strengthen the scientific and technological bases of the Community in industry and will contribute to the competitivity of Community industry;
Whereas a two-year feasibility study on international collaboration in the domain of intelligent manufacturing systems has proved that there is added-value in such cooperation;
Whereas the Council has authorized the Commission to negotiate a common understanding with the United States of America, Japan, Australia, Canada and the EFTA countries of Norway and Switzerland;
Whereas a common understanding in the form of an Exchange of Letters was reached with the third countries,
HAS DECIDED AS FOLLOWS:
Article 1
The Exchange of Letters recording the common understanding on the principles of international cooperation in research and development activities in the domain of intelligent manufacturing systems between the European Community and the United States of America, Japan, Australia, Canada and the EFTA countries of Norway and Switzerland is hereby approved.
The text of the Exchange of Letters is attached to this Decision.
Article 2
The President of the Council is hereby authorized to designate the person empowered to sign the Exchange of Letters referred to in Article 1 in order to bind the Community.
Done at Brussels, 27 January 1997.
For the Council
The President
G. ZALM
(1) OJ No C 20, 20. 1. 1997, p. 371.
