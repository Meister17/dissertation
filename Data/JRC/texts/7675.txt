DECISION OF THE REPRESENTATIVES OF THE GOVERNMENTS OF THE MEMBER STATES of 5 April 1977 on the provisional location of the Court of Auditors (77/323/EEC, Euratom, ECSC)
THE REPRESENTATIVES OF THE GOVERNMENTS OF THE MEMBER STATES,
Having regard to the Treaty establishing a Single Council and a Single Commission of the European Communities, and in particular Article 37 thereof,
Having regard to the Decision of the Representatives of the Governments of the Member States of 8 April 1965 on the provisional location of certain institutions and departments of the Communities (1), and in particular Article 10 thereof,
Having regard to the opinion of the Commission,
Whereas, without prejudice to the application of Article 77 of the Treaty establishing the European Coal and Steel Community, of Article 216 of the Treaty establishing the European Economic Community, and of Article 189 of the Treaty establishing the European Atomic Energy Community, it is necessary to make arrangements for the provisional place of work of the Court of Auditors, set up by the Treaty of 22 July 1975, amending certain financial provisions of the Treaties establishing the European Communities and of the Treaty establishing a Single Council and a Single Commission of the European Communities,
HAVE DECIDED:
Article 1
The Court of Auditors shall be located at Luxembourg, which shall be the provisional place of work within the meaning of the Decision of the Representatives of the Governments of the Member States of 8 April 1965 on the provisional location of certain institutions and departments of the Communities.
Article 2
This Decision shall enter into force on the same date as the Treaty of 22 July 1975 amending certain financial provisions of the Treaties establishing the European Communities and the Treaty establishing a Single Council and a Single Commission of the European Communities.
Done at Luxembourg, 5 April 1977.
The President
D. OWEN (1)OJ No L 152, 13.7.1967, p. 18.
