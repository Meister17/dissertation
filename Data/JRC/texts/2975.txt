Authorisation for State aid pursuant to Articles 87 and 88 of the EC Treaty
Cases where the Commission raises no objections
(2005/C 263/02)
Date of adoption of the decision: 29.7.2005
Member State: Italy (Apulia)
Aid No: N 469/2004
Title: Assistance in farming areas affected by natural disasters (tornado of 12 July 2004 in the province of Foggia)
Objective: To compensate farmers for damage to agricultural production and farming structures as a result of bad weather
Legal basis: Decreto legislativo n. 102/2004
Budget: See the approved scheme (NN 54/A/04)
Aid intensity or amount: Up to 100 %
Duration: Measure applying an aid scheme approved by the Commission
Other information: Measure applying the scheme approved by the Commission under State aid NN 54/A/2004 (Commission letter C(2005)1622 final, dated 7 June 2005)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 2.8.2005
Member State: Italy (Friuli-Venezia Giulia)
Aid No: N 470/2004
Title: Assistance in agricultural areas which have suffered damage (frosts of 23 to 25 May 2004, province of Pordenone)
Objective: To compensate for damage to agricultural production as a result of bad weather
Legal basis: Decreto legislativo n. 102/2004
Budget: See the approved scheme (NN 54/A/04)
Aid intensity or amount: Up to 100 % of the damage
Duration: Measure applying an aid scheme approved by the Commission
Other information: Measure applying the scheme approved by the Commission under State aid NN 54/A/2004 (Commission letter C(2005)1622 final, dated 7 June 2005)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 2.8.2005
Member State: Italy (Calabria)
Aid No: N 471/2004
Title: Assistance in farming areas affected by natural disasters (hail-storms of 17, 18 and 19 June 2004)
Objective: To compensate farmers for damage to agricultural production and farming structures as a result of bad weather
Legal basis: Decreto legislativo n. 102/2004
Budget: See the approved scheme (NN 54/A/04)
Aid intensity or amount: Up to 100 % of the cost of the damage
Duration: Measure applying an aid scheme approved by the Commission
Other information: Measure applying the scheme approved by the Commission under State aid NN 54/A/2004 (Commission letter C(2005)1622 final, dated 7 June 2005)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 29.7.2005
Member State: Italy (Lombardy)
Aid No: N 474/2004
Title: Assistance in agricultural areas which have suffered damage (hail-storm of 20 August 2004, province of Bergamo)
Objective: To compensate for damage to agricultural production as a result of bad weather
Legal basis: Decreto legislativo n. 102/2004
Budget: See the approved scheme (NN 54/A/04)
Aid intensity or amount: Up to 100 % of the damage
Duration: Measure applying an aid scheme approved by the Commission
Other information: Measure applying the scheme approved by the Commission under State aid NN 54/A/2004 (Commission letter C(2005)1622 final, dated 7 June 2005)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 2.8.2005
Member State: Italy (Campania)
Aid No: N 475/2004
Title: Assistance in farming areas affected by natural disasters (hail-storms of 3 September 2004)
Objective: To compensate for damage to agricultural production as a result of bad weather
Legal basis: Decreto legislativo n. 102/2004
Budget: See the approved scheme (NN 54/A/04)
Aid intensity or amount: Up to 100 % of the cost of the damage
Duration: Measure applying an aid scheme approved by the Commission
Other information: Measure applying the scheme approved by the Commission under State aid NN 54/A/2004 (Commission letter C(2005) 1622 final, dated 7 June 2005)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 30.8.2005
Member State: Italy (Marche)
Aid No: N 482/2004
Title: Assistance for quality certification and traceability scheme for agricultural and agri-food products
A) aid for quality certification and traceability schemes for agricultural products and agri-foodstuffs (articles 2, 3, 4 and 9);
B) aid for technical assistance for adapting specifications for the production of registered quality products in line with the Community, national and regional rules (article 6);
C) start-up aid for consortiums for the protection of designations of origin (article 7)
Legal basis: Legge regionale 10 dicembre 2003, n. 23, articoli 2, 3, 4, 6, 7, 8 e 9
Budget: Annual budget (estimate): around EUR 2 million
Aid intensity or amount: Variable depending on measure
Duration: Measures A and C: seven years. Measure B: indefinite
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 29.7.2005
Member State: Italy (Emilia-Romagna)
Aid No: N 484/2004
Title: Assistance in agricultural areas which have suffered damage (hail-storms in the summer of 2004, provinces of Forlì-Cesena, Piacenza, Ravenna, Ferrara and Modena)
Objective: To compensate for damage to agricultural production and farming structures as a result of bad weather [hail which affected the provinces of Forlì ( 27 July 2004), Piacenza ( 20 June 2004), Ravenna ( 27 July 2004), Ferrara ( 2 June 2004 and 3 August 2004), Modena ( 2 June 2004, 24 July 2004 and 3 August 2004)]
Legal basis: Decreto legislativo n. 102/2004
Budget: EUR 50884,37 to be financed from the budget approved under Aid No NN 54/A/04
Aid intensity or amount: Up to 80 % of the cost of the damage
Duration: Measure applying an aid scheme approved by the Commission
Other information: Measure applying the scheme approved by the Commission under State aid NN 54/A/2004 (Commission letter C(2005)1622 final, dated 7 June 2005)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 29.7.2005
Member State: Italy (Friuli-Venezia Giulia)
Aid No: N 488/2004
Title: Assistance in farming areas which have suffered damage (summers of 2003 and 2004, hail-storms and tornado in the provinces of Trieste, Pordenone and Udine)
Objective: To compensate for damage to agricultural production and farming structures as a result of bad weather (hail-storms in the provinces of Pordenone on 13 July 2003 and Trieste on 27 June 2004, and a tornado in the province of Udine on 26 August 2004)
Legal basis: Legge n. 185/1992 e Decreto legislativo n. 102/2004
Budget: See the approved schemes (C 12/B/95 and NN 54/A/04)
Aid intensity or amount: Up to 100 % of the cost of the damage
Duration: Measure applying aid schemes approved by the Commission
Other information: Measure applying the scheme approved by the Commission under State aid C 12/B/95 (Commission Decision 2004/307/EC of 16 December 2003) and NN 54/A/2004 (Commission letter C(2005) 1622 final, dated 7 June 2005)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 29.7.2005
Member State: Italy (Sicily)
Aid No: N 490/2004
Title: Assistance in agricultural areas which have suffered damage (wind-storms in the province of Palermo in April and May 2004)
Objective: To compensate farmers for damage to agricultural production and farming structures as a result of bad weather
Legal basis: Decreto legislativo n. 102/2004
Budget: See the approved scheme (NN 54/A/04)
Aid intensity or amount: Up to 100 %
Duration: Measure applying an aid scheme approved by the Commission
Other details: Measure applying the scheme approved by the Commission under State aid NN 54/A/2004 (Commission letter C(2005)1622 final, dated 7 June 2005)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 29.7.2005
Member State: Italy (Apulia)
Aid No: N 495/2004
Title: Assistance in farming areas affected by natural disasters (hail-storms of 26 July 2004 in the province of Brindisi)
Objective: To compensate for damage to agricultural production and farming structures as a result of bad weather
Legal basis: Decreto legislativo n. 102/2004
Budget: See the approved scheme (NN 54/A/04)
Aid intensity or amount: Up to 100 %
Duration: Measure applying an aid scheme approved by the Commission
Other information: Measure applying the scheme approved by the Commission under State aid NN 54/A/2004 (Commission letter C(2005) 1622 final, dated 7 June 2005)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 29.7.2005
Member State: Italy (Campania)
Aid No: N 512/2004
Title: Assistance in farming areas affected by natural disasters (hail-storms of 1, 2, 3, 6 and 25 September 2004 in the provinces of Naples and Benevento).
Objective: To compensate for damage to agricultural production as a result of bad weather
Legal basis: Decreto legislativo n. 102/2004
Budget: See the approved scheme (NN 54/A/04)
Aid intensity or amount: Up to 100 % of the cost of the damage
Duration: Measure applying an aid scheme approved by the Commission
Other information: Measure applying the scheme approved by the Commission under State aid NN 54/A/2004 (Commission letter C(2005)1622 final, dated 7 June 2005)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 29.7.2005
Member State: Italy (Sicily)
Aid No: N 513/2004
Title: Assistance in farming areas affected by natural disasters (hail, tornado and torrential rain of 17 June 2004 in the provinces of Catania and Trapani)
Objective: To compensate for damage to agricultural production and farming structures as a result of bad weather
Legal basis: Decreto legislativo n. 102/2004
Budget: EUR 22353000, to be financed from the budget approved under aid scheme NN 54/A/04
Aid intensity or amount: Up to 100 %
Duration: Measure applying an aid scheme approved by the Commission
Other details: Measure applying the scheme approved by the Commission under State aid NN 54/A/2004 (Commission letter C(2005)1622 final, dated 7 June 2005)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 29.7.2005
Member State: The Netherlands (Flevoland)
Aid No: N 516/2003
Title: School fruit project Flevoland
Objective: The aid measure has as its aim to promote the consumption of fruit (apples) at primary and secondary schools in the Dutch province of Flevoland
- Decentralisatie-overeenkomst van de Staat der Nederlanden en de provincie Flevoland met betrekking tot het Enkelvoudig Programmeringsdocument (EPD) 2000-2006. (PME 000320/08, 24.1.2001)
- EPD-overeenkomst tussen de provincie Flevoland en Stichting schoolfruit Flevoland
- Investment aid: EUR 155000
- Technical support: EUR 144000
- Support for Advertisement/promotion: EUR 12500
- For investment costs: 38 %
- For technical support: 48 %
- For advertisement: 25 %
- Investment aid: One-off
- Aid for technical support and advertising: 2005-2007
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 12.8.2005
Member State: Italy (Friuli-Venezia Giulia)
Aid No: N 525/2004
Title: Legislative Decree No 102/2004: Assistance in farming areas which have suffered damage (hail-storms of 19 and 20 June 2004 in the provinces of Udine and Pordenone)
Objective: To provide meteorological information about the bad weather that caused the damage for which compensation is to be paid as part of the scheme approved under Aid No NN 54/A/04
Legal basis: Decreto legislativo n. 102/2004 ("Nuova disciplina del Fondo di solidarietà nazionale")
Budget: The amount of the aid, which has still to be fixed, will be financed from the overall budget of EUR 200 million allocated to the approved scheme (NN 54/A/04)
Aid intensity or amount: Up to 100 %
Duration: One-off aid
Other information: Measure applying the scheme approved by the Commission under State aid NN 54/A/2004 (Commission letter C(2005) 1622 final, dated 7 June 2005)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
--------------------------------------------------
