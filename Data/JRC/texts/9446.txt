Commission Decision
of 16 January 2006
concerning the placing on the market, in accordance with Directive 2001/18/EC of the European Parliament and of the Council, of a maize product (Zea mays L., hybrid MON 863 × MON 810) genetically modified for resistance to corn rootworm and certain lepidopteran pests of maize
(notified under document number C(2005) 5980)
(Only the German text is authentic)
(2006/47/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Directive 2001/18/EC of the European Parliament and of the Council of 12 March 2001 on the deliberate release into the environment of genetically modified organisms and repealing Council Directive 90/220/EEC [1], and in particular the first subparagraph of Article 18(1) thereof,
After consulting the European Food Safety Authority,
Whereas:
(1) Pursuant to Directive 2001/18/EC, the placing on the market of a product containing or consisting of a genetically modified organism or a combination of genetically modified organisms is subject to written consent being granted by the competent authority of a Member State, in accordance with the procedure laid down in that Directive.
(2) A notification concerning the placing on the market of two genetically modified maize products (Zea mays L., line MON 863 and hybrid MON 863 × MON 810) was submitted by Monsanto Europe S.A. to the competent authority of Germany (Reference C/DE/02/9). A unique identifier (MON-ØØ863-5×MON-ØØ81Ø-6) has been assigned to the MON 863 × MON 810 maize for the purposes of Regulation (EC) No 1830/2003 of the European Parliament and of the Council of 22 September 2003 concerning the traceability and labelling of genetically modified organisms and the traceability of food and feed products produced from genetically modified organisms and amending Directive 2001/18/EC [2] and Commission Regulation (EC) No 65/2004 of 14 January 2004 establishing a system for the development and assignment of unique identifiers for genetically modified organisms [3].
(3) The notification originally covered importation and use as for any other maize grains including feed, with the exception of food use and cultivation in the Community of varieties derived from the MON 863 transformation event and of the MON 863 × MON 810 hybrid.
(4) In accordance with the procedure provided for in Article 14 of Directive 2001/18/EC, the competent authority of Germany prepared an assessment report, which was forwarded in April 2003 to the Commission. The Commission forwarded the full notification and assessment report to the other Member States in May 2003. That assessment report concludes that no reasons have emerged on the basis of which consent for the placing on the market of MON 863 as well as MON 863 × MON 810 should be withheld, if specific conditions are fulfilled.
(5) The competent authorities of other Member States raised objections to the placing on the market of MON 863 as well as MON 863 × MON 810.
(6) The placing on the market of MON 810 maize is authorised in accordance with Commission Decision 98/294/EC of 22 April 1998 concerning the placing on the market of genetically modified maize (Zea mays L. line MON 810), pursuant to Council Directive 90/220/EEC [4]. The placing on the market of MON 863 is authorised in accordance with Commission Decision 2005/608/EC of 8 August 2005 concerning the placing on the market, in accordance with Directive 2001/18/EC of the European Parliament and of the Council, of a maize product (Zea mays L., line MON 863) genetically modified for resistance to corn rootworm [5].
(7) On 2 April 2004, the European Food Safety Authority considered that it is scientifically valid to use the data from the single lines MON 863 and MON 810 to support the safety assessment of MON 863 × MON 810, but decided, with regard to the need for confirmatory data for the safety assessment of the hybrid itself, to request a 90-day sub-chronic rat study with the maize hybrid in order to complete its safety assessment.
(8) The opinion adopted on 8 June 2005 by the European Food Safety Authority, concluded, from all evidence provided, that MON 863 × MON 810 is unlikely to have an adverse effect on human and animal health or the environment in the context of its proposed use. The European Food Safety Authority also deemed that the scope of the monitoring plan provided by the applicant was in line with the intended uses of MON 863 × MON 810.
(9) On 8 July 2005, Monsanto Europe S.A. agreed to limit the scope of the present Decision to import and processing. An application for the placing on the market of food and feed containing, consisting of, or produced from MON 863 × MON 810 has been made by Monsanto Europe S.A. under Regulation (EC) No 1829/2003 of the European Parliament and of the Council of 22 September 2003 on genetically modified food and feed [6].
(10) An examination of the information submitted in the notification, the objections maintained by Member States in the framework of Directive 2001/18/EC, and the opinion of the European Food Safety Authority, discloses no reason to believe that the placing on the market of Zea mays L. hybrid MON 863 × MON 810 will adversely affect human or animal health or the environment.
(11) Adventitious or technically unavoidable traces of genetically modified organisms in products are exempted from labelling and traceability requirements in accordance with thresholds established under Directive 2001/18/EC and Regulation (EC) No 1829/2003.
(12) In the light of the opinion of the European Food Safety Authority, it is not necessary to establish specific conditions for the intended uses with regard to the handling or packaging of the product and the protection of particular ecosystems, environments or geographical areas.
(13) Prior to the placing on the market of the product, the necessary measures to ensure its labelling and traceability at all stages of its placing on the market, including verification by appropriate validated detection methodology, should be applicable.
(14) The measures provided for in this Decision are not in accordance with the opinion of the Committee established under Article 30 of Directive 2001/18/EC and the Commission therefore submitted to the Council a proposal relating to these measures. Since on the expiry of the period laid down in Article 30(2) of Directive 2001/18/EC, the Council had neither adopted the proposed measures nor indicated its opposition to them, in accordance with Article 5(6) of Council Decision 1999/468/EC of 28 June 1999 laying down the procedures for the exercise of implementing powers conferred on the Commission [7], the measures should be adopted by the Commission,
HAS ADOPTED THIS DECISION:
Article 1
Consent
Without prejudice to other Community legislation, in particular Regulation (EC) No 258/97 of the European Parliament and of the Council [8] and Regulation (EC) No 1829/2003, written consent shall be granted by the competent authority of Germany to the placing on the market, in accordance with this Decision, of the product identified in Article 2, as notified by Monsanto Europe S.A. (Reference C/DE/02/9).
The consent shall, in accordance with Article 19(3) of Directive 2001/18/EC, explicitly specify the conditions to which the consent is subject, which are set out in Articles 3 and 4.
Article 2
Product
The genetically modified organisms to be placed on the market as or in products, hereinafter "the product", are grains of maize (Zea mays L. MON 863 × MON 810), obtained by conventional breeding of MON 863 and MON 810. Descriptions of the MON 810 and MON 863 maizes are provided for in Decisions 98/294/EC and 2005/608/EC respectively.
Article 3
Conditions for placing on the market
The product may be put to the same uses as any other maize, with the exception of cultivation and uses as or in food and feed, and may be placed on the market subject to the following conditions:
(a) the period of validity of the consent shall be 10 years starting from the date on which the consent is issued;
(b) the unique identifier of the product shall be MON-ØØ863-5×MON-ØØ81Ø-6;
(c) without prejudice to Article 25 of Directive 2001/18/EC, the consent holder shall, whenever requested to do so, make positive and negative control samples of the product, or its genetic material, or reference materials available to the competent authorities and inspection services of Member States as well as to the Community control laboratories;
(d) without prejudice to specific labelling requirements provided by Regulation (EC) No 1829/2003 the words "This product contains genetically modified maize" or "This product contains genetically modified MON 863 × MON 810" shall appear either on a label or in a document accompanying the product, except where other Community legislation sets a threshold below which such information is not required;
(e) as long as the product has not been authorised for the placing on the market for the purpose of cultivation, the words "not for cultivation" shall appear either on a label or in a document accompanying the product.
Article 4
Monitoring
1. Throughout the period of validity of the consent, the consent holder shall ensure that the monitoring plan, contained in the notification and consisting of a general surveillance plan, the objective of which is to check for any adverse effects on human and animal health or the environment arising from handling or use of the product, is put in place and implemented.
2. The consent holder shall directly inform the operators and users concerning the safety and general characteristics of the product and of the conditions as to monitoring, including the appropriate management measures to be taken in case of accidental grain spillage.
3. The consent holder shall submit to the Commission and to the competent authorities of the Member States annual reports on the results of the monitoring activities.
4. Without prejudice to Article 20 of Directive 2001/18/EC the monitoring plan as notified shall, where appropriate and subject to the agreement of the Commission and the competent authority of the Member State which received the original notification, be revised by the consent holder and/or by the competent authority of the Member State which received the original notification, in the light of the results of the monitoring activities. Proposals for a revised monitoring plan shall be submitted to the competent authorities of the Member States.
5. The consent holder shall be in the position to give evidence to the Commission and the competent authorities of the Member States:
(a) that the monitoring networks as specified in the monitoring plan contained in the notification collect the information relevant for the monitoring of the product, and
(b) that the members of these networks have agreed to make available that information to the consent holder before the date of the submission of the monitoring reports to the Commission and competent authorities of the Member States in accordance with paragraph 3.
Article 5
Applicability
This Decision shall apply from the date on which a Community Decision authorising the placing on the market of the product referred to in Article 1 for uses as or in food and feed within the meaning of Regulation (EC) No 178/2002 of the European Parliament and of the Council [9] and including a method, validated by the Community reference laboratory, for detection of the product is applicable.
Article 6
Addressee
This Decision is addressed to the Federal Republic of Germany.
Done at Brussels, 16 January 2006.
For the Commission
Stavros Dimas
Member of the Commission
[1] OJ L 106, 17.4.2001, p. 1. Directive as last amended by Regulation (EC) No 1830/2003 (OJ L 268, 18.10.2003, p. 24).
[2] OJ L 268, 18.10.2003, p. 24.
[3] OJ L 10, 16.1.2004, p. 5.
[4] OJ L 131, 5.5.1998, p. 32.
[5] OJ L 207, 10.8.2005, p. 17.
[6] OJ L 268, 18.10.2003, p. 1.
[7] OJ L 184, 17.7.1999, p. 23.
[8] OJ L 43, 14.2.1997, p. 1. Regulation as amended by Regulation (EC) No 1882/2003 (OJ L 284, 31.10.2003, p. 1).
[9] OJ L 31, 1.2.2002, p. 1. Regulation as amended by Regulation (EC) No 1642/2003 (OJ L 245, 29.9.2003, p. 4).
--------------------------------------------------
