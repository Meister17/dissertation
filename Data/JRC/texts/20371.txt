*****
COMMISSION REGULATION (EEC) No 2539/84
of 5 September 1984
laying down detailed rules for certain sales of frozen beef held by the intervention agencies
THE COMMISSION OF THE EUROPEAN
COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 805/68 of 27 June 1968 on the common organization of the markets in beef and veal (1), as last amended by the Act of Accession of Greece, and in particular Article 7 (3) thereof,
Whereas Council Regulation (EEC) No 98/69 of 16 January 1969 laying down general rules for the disposal of frozen beef and veal by intervention agencies (2), as amended by Regulation (EEC) No 429/77 (3), provides that selling prices may be fixed on a flat-rate basis in advance or determined by means of an invitation to tender;
Whereas there are generally a very high number of applications in respect of certain sales of frozen beef and veal held by intervention agencies; whereas, in order to satisfy certain specific requirements and in order to ensure better financial management of intervention stocks, provision should be made for a two-stage sales procedure involving firstly sales by tender and secondly sales at prices fixed in advance; whereas detailed rules should be established for this type of sale;
Whereas Commission Regulation (EEC) No 2173/79 (4) lays down detailed rules for the disposal of beef bought in by intervention agencies by means of both sales at prices fixed in advance and invitations to tender; whereas for the sake of simplicity the greatest possible account should be taken thereof;
Whereas, in order to ensure the economic management of stocks, it should be laid down that the intervention agencies should first sell meat which has been in storage longest;
Whereas, in view of the administrative difficulties this will entail in certain Member States, a derogation should be provided from the second indent of Article 2 (2) of Regulation (EEC) No 2173/79;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Beef and Veal,
HAS ADOPTED THIS REGULATION:
Article 1
1. This Regulation lays down detailed rules for certain sales of frozen beef held by the intervention agencies of the Member States carried out in two successive stages, at the first stage by tender and at the second at prices fixed on a flat-rate basis in advance.
2. Subject to the provisions of this Regulation, sales by tender and subsequently at prices fixed in advance shall take place pursuant to the provisions of Regulation (EEC) No 2173/79.
3. The intervention agencies shall first sell those products which have been in storage longest.
Article 2
1. At the first stage the meat shall be put up for sale by tender.
2. Notwithstanding Articles 6 and 7 of Regulation (EEC) No 2173/79, the Annexes to the Regulation relating to the sale of the beef concerned shall replace the notice of invitation to tender.
Information on quantities and on locations where meat is stored may be obtained from the addresses given in the Annex to the aforesaid Regulation relating to the sale of the beef concerned. In addition, intervention agencies may post a notice of such invitations at their head offices and announce them by other means.
3. The provisions of Article 8 of Regulation (EEC) No 2173/79 governing participation in the tendering procedure and the submission of tenders shall apply.
Tenders offering a price less than the minimum price referred to in Article 3 (1) shall not be admitted.
However, tenders shall not indicate the cold store or stores where the products applied for are stored.
Article 3
1. Notwithstanding Articles 9 and 10 of Regulation (EEC) No 2173/79, the intervention agencies shall, for each quality of frozen beef, allocate the quantities put up for sale to those tenderers whose tenders offer a price higher than or equal to the minimum price fixed in the Annex to the Regulation relating to the sale of the beef concerned.
For the purposes of applying the first subparagraph, the intervention agencies shall first accept the tender or tenders made at the price which is highest above the aforesaid minimum price. Subsequent quantities shall be assigned to the tenderers referred to in the first subparagraph according to the prices which they have offered starting from that which is highest above the minimum price in question.
Where, as a result of several tenders at the same price being received, the quantity available is inadequate, the intervention agency shall apportion it, where appropriate in agreement with the tenderers or by holding a ballot.
2. Notwithstanding Article 11 of Regulation (EEC) No 2173/79, each tenderer shall be notified by the intervention agency concerned of the result of his participation in the invitation to tender within four working days at the latest following the time limit set for the submission of tenders in response to the invitation in question.
3. After completion of the sale by tender referred to above, the intervention agencies shall, at their head offices, post a notice of any quantities remaining available.
Article 4
1. The quantities remaining available after completion of the sale by tender referred to in Articles 2 and 3 shall be put up for sale at the minimum price fixed in the Annex to the Regulation relating to the sale of the beef concerned.
2. Purchase applications may be made from the fifth working day following the time limit set for the submission of tenders in response to the invitation to tender concerned.
3. Purchase applications shall not indicate the cold store or stores where the products applied for are stored.
Article 5
Notwithstanding the first subparagraph of Article 15 (1) of Regulation (EEC) No 2173/79, the amount of the security shall be fixed at the time of each sale.
Article 6
Notwithstanding Article 18 (1) of Regulation (EEC) No 2173/79, the time limit for taking over meat sold pursuant to this Regulation shall be two months from the time limit for the submission of tenders or, in the case of sales at fixed prices, from the date of acceptance of the application referred to in Article 3 (2) of the said Regulation.
Article 7
The intervention agencies shall inform the Commission, within the time limit referred to in Article 3 (2), in respect of each invitation to tender, of the quantities applied for and the corresponding prices offered, and of the quantities allocated and the sales prices actually applied pursuant to Article 3.
Article 8
The provisions of this Regulation shall apply in the case of private sales where the Regulations relating to such sales refer to this Regulation.
Article 9
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 5 September 1984.
For the Commission
Poul DALSAGER
Member of the Commission
(1) OJ No L 148, 28. 6. 1968, p. 26.
(2) OJ No L 14, 21. 1. 1969, p. 2.
(3) OJ No L 61, 5. 3. 1977, p. 18.
(4) OJ No L 251, 5. 10. 1979, p. 12.
