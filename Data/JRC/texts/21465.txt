Regulation (EC) No 2056/2002 of the European Parliament and of the Council
of 5 November 2002
amending Council Regulation (EC, Euratom) No 58/97 concerning structural business statistics
(Text with EEA relevance)
THE EUROPEAN PARLIAMENT AND THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 285 thereof,
Having regard to the proposal from the Commission(1),
Having regard to the opinion of the Economic and Social Committee(2),
Having regard to the opinion of the European Central Bank(3),
Acting in accordance with the procedure laid down in Article 251 of the Treaty(4),
Whereas:
(1) Regulation (EC, Euratom) No 58/97(5) established a common framework for the collection, compilation, transmission and evaluation of Community statistics on the structure, activity, competitiveness and performance of businesses in the Community.
(2) Developments in Community monetary, economic and social integration require the extension of the common framework to credit institutions, pension funds, other financial intermediation and activities auxiliary to financial intermediation.
(3) The operation and development of the internal market has increased the need for information measuring its effectiveness, in particular in the sectors of credit institutions, pension funds, other financial intermediation and activities auxiliary to financial intermediation.
(4) The liberalisation of international trade in financial services necessitates business statistics in the financial services area to support the trade negotiations.
(5) The compilation of national and regional accounts according to Council Regulation (EC) No 2223/96 of 25 June 1996 on the European system of national and regional accounts in the Community(6) requires comparable, complete and reliable business statistics in the financial services area.
(6) The introduction of the single currency will have a major impact on the structure of the financial services industry and the cross-border flows of capital, which stresses the importance of information on competitiveness, the internal market and internationalisation.
(7) The smooth conduct of policies pursued by the competent authorities relating to prudential supervision of credit institutions and the stability of the financial system implies the need for additional information on credit institutions and related services.
(8) A developing pension funds sector could help to boost the capital markets through increasingly taking advantage of the liberalisation of investment rules.
(9) Decision No 2179/98/EC of the European Parliament and of the Council of 24 September 1998 on the review of the European Community programme of policy action in relation to the environment and sustainable development "Towards sustainability"(7) reiterated the need for reliable and comparable data, statistics and indicators as a key tool for the evaluation of the cost of compliance with environmental regulations.
(10) The Statistical Programme Committee set up by Decision 89/382/EEC, Euratom(8), the Banking Advisory Committee set up by Directive 77/780/EEC(9), the Committee on Monetary, Financial and Balance of Payments Statistics set up by Decision 91/115/EEC(10) and the Insurance Committee set up by Directive 91/675/EEC(11) have been consulted,
HAVE ADOPTED THIS REGULATION:
Article 1
Regulation (EC, Euratom) No 58/97 is amended as follows:
1. in Article 5 the following indents shall be added:
"- a detailed module for structural statistics on credit institutions defined in Annex 6,
- a detailed module for structural statistics on pension funds defined in Annex 7.";
2. the text of Annexes 6 and 7, as set out in the Annex to this Regulation, shall be added.
Article 2
Annex 1 to Regulation (EC, Euratom) No 58/97 shall be amended as follows:
1. in section 5 the following sentence shall be added:
"However the first reference year for which statistics on the activity classes covered by the NACE REV. 1 group 65.2 and division 67 are to be compiled, shall be decided in accordance with the procedure laid down in Article 13 of this Regulation.";
2. Section 8 shall be replaced by the following:
"Section 8
Transmission of results
1. The results are to be transmitted within 18 months of the end of the calendar year of the reference period, except for the NACE REV. 1 activity class 65.11 and the activities of NACE REV. 1 covered by Annexes 5, 6 and 7. For the NACE REV. 1 activity class 65.11 the transmission delay is 10 months. For the activities covered by Annexes 5, 6 and 7 the transmission delay is laid down in these annexes. However the transmission delay of the results on the activity classes covered by the NACE REV. 1 group 65.2 and division 67 shall be decided in accordance with the procedure laid down in Article 13 of this Regulation.
2. Except for the divisions 65 and 66 of NACE REV. 1, preliminary national results or estimates are transmitted within 10 months of the end of the calendar year of the reference period for the enterprise statistics compiled for the characteristics listed below:
12110 (turnover),
16110 (number of persons employed).
These preliminary results or estimates are to be broken down to NACE REV. 1 three-digit level (group), except for Sections H, I and K of NACE REV. 1, for which they are to be broken down according to the groupings laid down in section 9. For the division 67 of NACE REV. 1, the transmission of preliminary results or estimates shall be decided in accordance with the procedure laid down in Article 13 of this Regulation.";
3. in section 9, Section J shall be replaced by the following:
"SECTION J
Financial intermediation
To enable Community statistics to be compiled, Member States will transmit component national results broken down to the classes of NACE REV. 1.";
4. in section 10, paragraph 1, the first sentence shall be replaced by the following:
"Member States will provide the Commission with a report relating to the definition, structure and availability of information on statistical units which are classified under Sections M to O of NACE REV. 1."
Article 3
Annex 2 to Regulation (EC, Euratom) No 58/97 shall be amended as follows:
1. in section 4, paragraph 3, the following characteristic shall be inserted after the variable 21 11 0 (investment in equipment and plant for pollution control, and special anti-pollution accessories - mainly end-of-pipe equipment):
"21 12 0 - Investment in equipment and plant linked to cleaner technology (integrated technology) (*)";
2. the footnote in section 4, paragraph 3 shall be replaced by the following:
"(*) If the total amount of the turnover or the number of persons employed in a division of NACE REV. 1 Sections C to E represent, in a Member State, less than 1 % of the Community total, the information necessary for the compilation of statistics relating to characteristics 21 11 0, 21 12 0, 22 11 0 and 22 12 0 need not be collected for the purposes of this Regulation. If necessary for Community policy requirements, the Commission may, in accordance with the procedure laid down in Article 13 of this Regulation, request ad hoc collection of these data.";
3. in section 4, paragraph 4, the following characteristic shall be inserted after the variable 20 31 0 (Purchases of electricity (in value)):
"21 14 0 - Total current expenditure on environmental protection (*)";
4. in section 4, paragraph 4, the following footnote shall be added:
"(*) If the total amount of the turnover or the number of persons employed in a division of NACE REV. 1 Sections C to E represent, in a Member State, less than 1 % of the Community total, the information necessary for the compilation of statistics relating to characteristic 21 14 0 need not be collected for the purposes of this Regulation. If necessary for Community policy requirements, the Commission may, in accordance with the procedure laid down in Article 13 of this Regulation, request ad hoc collection of these data.";
5. in section 5, the following paragraphs shall be added:
"3. The first reference year for which statistics on characteristics 21 12 0 and 21 14 0 are to be compiled is the calendar year 2001.
4. The statistics on characteristic 21 12 0 are to be compiled yearly. The statistics on characteristic 21 14 0 are to be compiled every three years.";
6. in section 7, paragraph 6 shall be replaced by the following:
"6. The results for characteristics 21 11 0, 21 12 0 and 21 14 0, are to be broken down to the NACE REV. 1 two-digit level (division).";
7. in section 7, the following paragraph shall be added:
"7. The results for characteristics 21 11 0, 21 12 0 and 21 14 0, are to be broken down to the following environmental domains: protection of ambient air and climate, wastewater management, waste management and other environmental protection activities. The results for the environmental domains shall be broken down to NACE REV. 1 two-digit level (division).";
8. in section 9, the following characteristic shall be inserted:
"21 11 0 - Investment in equipment and plant for pollution control, and special anti-pollution accessories (mainly end-of-pipe equipment)."
The following comment shall be added for characteristics 21 11 0, 21 12 0 and 21 14 0:
"Only specific breakdown on the environmental domains biodiversity and landscape, soil and groundwater.";
9. in section 10, the following sentence shall be added:
"For the compilation of statistics on characteristics 21 12 0 and 21 14 0 this transitional period may be extended by a further period of up to four years in accordance with the procedure laid down in Article 13 of this Regulation."
Article 4
This Regulation shall enter into force on the 20th day following that of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 5 November 2002.
For the European Parliament
The President
P. Cox
For the Council
The President
T. Pedersen
(1) OJ C 154 E, 29.5.2001, p. 129 and
OJ C 332 E, 27.11.2001, p. 340.
(2) OJ C 260, 17.9.2001, p. 54.
(3) OJ C 131, 3.5.2001, p. 5.
(4) Opinion of the European Parliament of 13 June 2001 (OJ C 53 E, 28.2.2002, p. 213), Council Common Position of 20 June 2002 (OJ C 228, 25.9.2002, p. 1) and Decision of the European Parliament of 24 September 2002 (not yet published in the Official Journal).
(5) OJ L 14, 17.1.1997, p. 1. Regulation as last amended by Regulation (EC, Euratom) No 410/98 (OJ L 52, 21.2.1998, p. 1).
(6) OJ L 310, 30.11.1996, p. 1. Regulation as last amended by Regulation (EC) No 359/2002 of the European Parliament and of the Council (OJ L 58, 28.2.2002, p. 1).
(7) OJ L 275, 10.10.1998, p. 1.
(8) OJ L 181, 28.6.1989, p. 47.
(9) OJ L 322, 17.12.1977, p. 30. Directive as last amended by Directive 98/33/EC of the European Parliament and the Council (OJ L 204, 21.7.1998, p. 29).
(10) OJ L 59, 6.3.1991, p. 19. Decision as last amended by Decision 96/174/EC (OJ L 51, 1.3.1996, p. 48).
(11) OJ L 374, 31.12.1991, p. 32.
ANNEX
"ANNEX 6
A DETAILED MODULE FOR STRUCTURAL STATISTICS ON CREDIT INSTITUTIONS
Section 1
Aim
The aim of this Annex is to establish a common framework for the collection, compilation, transmission and evaluation of Community statistics on the structure, activity, competitiveness and performance of the credit institutions sector. This module includes a detailed list of characteristics on which statistics are to be compiled in order to improve knowledge on the national, Community and international development of the sector of credit institutions.
Section 2
Scope
The statistics to be compiled shall relate to the domains referred to in points (i), (ii) and (iii) of Article 2 of this Regulation, and in particular to:
1. the detailed analysis of the structure, activity, competitiveness and performance of credit institutions;
2. the development and distribution of total business and business per product, international activities, employment, capital and reserves, and other assets and liabilities.
Section 3
Coverage
1. The statistics are to be compiled for the activities of credit institutions within the scope of NACE REV. 1 classes 65.12 and 65.22.
2. The statistics are to be compiled for the activities of all credit institutions referred to in Article 2(1)(a) and Article 2(2) of Council Directive 86/635/EEC of 8 December 1986 on the annual accounts and consolidated accounts of banks and other financial institutions(1), with the exemption of central banks.
3. Branches of credit institutions referred to in Article 24 of Directive 2000/12/EC of the European Parliament and of the Council of 20 March 2000 relating to the taking up and pursuit of the business of credit institutions(2) whose activity falls within the scope of NACE REV. 1 classes 65.12 and 65.22, are to be assimilated to the credit institutions referred to in paragraph 2.
Section 4
Characteristics
The characteristics are listed below. The characteristics in italics are also included in the common module lists of Annex 1. When characteristics are derived directly from the annual accounts, accounting years ending within a reference year shall be assimilated to the said reference year.
The list includes:
(i) characteristics listed in Article 4 of Directive 86/635/EEC: asset side of the balance sheet: item 4; liability side of the balance sheet: items 2(a) + 2(b) as an aggregate, items 7 + 8 + 9 + 10 + 11 + 12 + 13 + 14 as an aggregate;
(ii) characteristics listed in Article 27 of Directive 86/635/EEC: item 2, items 3(a) + 3(b) + 3(c) as an aggregate, item 3(a), item 4, item 5, item 6, item 7, items 8(a) + 8(b) as an aggregate, item 8(b), item 10, items 11 + 12 as an aggregate, items 9 + 13 + 14 as an aggregate, items 15 + 16 as an aggregate, item 19, items 15 + 20 + 22 as an aggregate, item 23;
(iii) the following additional characteristics:
>TABLE>
(iv) characteristics for which yearly regional statistics are to be compiled:
>TABLE>
Section 5
First reference year
The first reference year for which annual statistics are to be compiled for the characteristics listed in Section 4 is the calendar year 2001.
Section 6
Production of results
1. The results are to be broken down to the following NACE REV. 1 classes: 65.12 and 65.22, separately.
2. The results of the regional statistics are to be broken down to NACE REV. 1 four-digit level (classes) and level 1 of the nomenclature of territorial units (NUTS).
Section 7
Transmission of results
The transmission delay of the results shall be decided in accordance with the procedure laid down in Article 13 of this Regulation. It shall not be longer than 10 months from the end of the reference year.
Section 8
Committee for Monetary, Financial and Balance of Payments Statistics
The Commission shall inform the Committee for Monetary, Financial and Balance of Payments Statistics about the implementation of this module and about all measures for adjustment to economic and technical developments concerning the collection and statistical processing of data, the processing and transmission of results.
Section 9
Pilot studies
1. For the activities covered by this Annex, the Commission will institute the following pilot studies to be carried out by Member States:
(a) information on derivatives and off-balance sheet items;
(b) information on the distribution networks;
(c) information needed to break down the transactions of credit institutions according to prices and volumes.
2. The pilot studies are to be carried out in order to assess the relevance and feasibility of obtaining data, taking into account the benefits of the availability of the data in relation to the cost of collection and the burden on business.
Section 10
Transitional period
For the purposes of this detailed module, the transitional period will not exceed three years from the beginning of the first reference year for the compilation of the statistics indicated in Section 5.
(1) OJ L 372, 31.12.1986, p. 1. Directive as last amended by Directive 2001/65/EC of the European Parliament and of the Council (OJ L 283, 27.10.2001, p. 28).
(2) OJ L 126, 26.5.2000, p. 1. Directive as last amended by Directive 2000/28/EC (OJ L 275, 27.10.2000, p. 37).
ANNEX 7
A DETAILED MODULE FOR STRUCTURAL STATISTICS ON PENSION FUNDS
Section 1
Aim
The aim of this Annex is to establish a common framework for the collection, compilation, transmission and evaluation of Community statistics on the structure, activity, competitiveness and performance of the pension funds sector. This module includes a detailed list of characteristics on which statistics are to be compiled in order to improve knowledge of the national, Community and international development of the pension funds sector.
Section 2
Scope
The statistics to be compiled shall relate to the domains referred to in points (i), (ii) and (iii) of Article 2 of this Regulation, and in particular to:
1. the detailed analysis of the structure, activity, competitiveness and performance of pension funds;
2. the development and distribution of total business, patterns of pension funds members, international activities, employment, investments and liabilities.
Section 3
Coverage
1. The statistics are to be compiled for all activities within the scope of NACE REV. 1 class 66.02. This class covers the activities of autonomous pension funds.
2. Some statistics are to be compiled for enterprises with non-autonomous pension funds which are carried out as ancillary activities.
Section 4
Characteristics
1. The list of characteristics set out below indicates, where relevant, the type of statistical unit for which the statistics are to be compiled. The characteristics in italics are also included in the common module lists of Annex 1. When characteristics are derived directly from the annual accounts, accounting years ending within a reference year are to be assimilated to the said reference year.
2. Demographic and enterprise characteristics for which yearly statistics are to be compiled (for enterprises with autonomous pension funds only):
>TABLE>
3. Enterprise characteristics for which yearly statistics are to be compiled (for enterprises with non-autonomous pension funds only):
>TABLE>
Section 5
First reference year
The first reference year for which annual statistics are to be compiled for the characteristics listed in Section 4 is the calendar year 2002.
Section 6
Production of results
1. The results for the characteristics listed in Section 4, paragraph 2, are to be broken down according to the NACE REV. 1, four-digit level (class).
2. The results for the characteristics listed in Section 4, paragraph 3, are to be broken down according to the NACE REV. 1 section level.
Section 7
Transmission of results
The results are to be transmitted within 12 months from the end of the reference year.
Section 8
Insurance Committee
The Commission shall inform the Insurance Committee about the implementation of this module and about all measures for adjustment to economic and technical developments concerning the collection and statistical processing of data, the processing and transmission of results.
Section 9
Pilot studies
For the activities covered by this Annex, the Commission will institute the following pilot studies to be carried out by Member States:
1. More in-depth information on cross-border activities of pension funds:
>TABLE>
2. Additional information on non-autonomous pension funds:
>TABLE>
3. Information on derivatives and off-balance sheet items.
The pilot studies shall be carried out in order to assess the relevance and feasibility of obtaining data, taking into account the benefits of the availability of the data in relation to the cost of collection and the burden on business.
Section 10
Transitional period
For the purposes of this detailed module, the transitional period will not exceed three years from the beginning of the first reference year for the compilation of the statistics indicated in Section 5. This period may be extended by a further period of up to three years in accordance with the procedure laid down in Article 13 of this Regulation."
