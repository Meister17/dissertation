Euro exchange rates [1]
27 October 2006
(2006/C 260/02)
| Currency | Exchange rate |
USD | US dollar | 1,2683 |
JPY | Japanese yen | 150,26 |
DKK | Danish krone | 7,4547 |
GBP | Pound sterling | 0,67060 |
SEK | Swedish krona | 9,2215 |
CHF | Swiss franc | 1,5911 |
ISK | Iceland króna | 86,57 |
NOK | Norwegian krone | 8,3110 |
BGN | Bulgarian lev | 1,9558 |
CYP | Cyprus pound | 0,5768 |
CZK | Czech koruna | 28,410 |
EEK | Estonian kroon | 15,6466 |
HUF | Hungarian forint | 261,70 |
LTL | Lithuanian litas | 3,4528 |
LVL | Latvian lats | 0,6961 |
MTL | Maltese lira | 0,4293 |
PLN | Polish zloty | 3,8781 |
RON | Romanian leu | 3,5205 |
SIT | Slovenian tolar | 239,62 |
SKK | Slovak koruna | 36,380 |
TRY | Turkish lira | 1,8403 |
AUD | Australian dollar | 1,6561 |
CAD | Canadian dollar | 1,4262 |
HKD | Hong Kong dollar | 9,8699 |
NZD | New Zealand dollar | 1,9279 |
SGD | Singapore dollar | 1,9837 |
KRW | South Korean won | 1201,52 |
ZAR | South African rand | 9,5450 |
CNY | Chinese yuan renminbi | 10,0064 |
HRK | Croatian kuna | 7,3660 |
IDR | Indonesian rupiah | 11547,87 |
MYR | Malaysian ringgit | 4,6312 |
PHP | Philippine peso | 63,149 |
RUB | Russian rouble | 33,9773 |
THB | Thai baht | 46,764 |
[1] Source: reference exchange rate published by the ECB.
--------------------------------------------------
