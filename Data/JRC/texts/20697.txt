COMMISSION DECISION of 29 June 1994 amending or repealing some detailed rules concerning the animal health and public health conditions required for imports of certain live animals and animal products from Austria, Finland, Norway and Sweden in application of the European Economic Area Agreement (Text with EEA relevance) (94/453/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 72/462/EEC of 12 December 1972 on health and veterinary inspection problems upon importation of bovine, ovine and caprine animals and swine, fresh meat or meat products from third countries (1), as last amended by Regulation (EEC) No 1601/92 (2), and in particular Articles 3, 4, 8, 11, 14 and 16 thereof,
Having regard to Council Directive 88/407/EEC of 14 June 1988 laying down the animal health requirements applicable to intra-Community trade in and imports of deep frozen semen of domestic animals of the bovine species (3), as last amended by Directive 93/60/EEC (4), and in particular Articles 8 and 9 thereof,
Having regard to Council Directive 89/556/EEC of 25 September 1989 on animal health conditions governing intra-Community trade in and importation from third countries of embryos of domestic animals of the bovine species (5), as last amended by Directive 93/52/EEC (6), and in particular Articles 8, 9 and 10 thereof,
Having regard to Council Directive 90/426/EEC of 26 June 1990 on animal health conditions governing the movement and imports from third countries of equidae (7), as last amended by Directive 92/36/EEC (8), and in particular Articles 15a, 16, 18 and 19 (ii) thereof,
Having regard to Council Directive 90/675/EEC of 10 December 1990 laying down the principles governing the organization of veterinary checks on products entering the Community from third countries (9), as last amended by Directive 92/118/EEC (10), and in particular Article 19 thereof,
Having regard to Council Directive 90/429/EEC of 26 June 1990 laying down the animal health requirements applicable to intra-Community trade in and imports of semen of domestic animals of the porcine species (11), and in particular Articles 7, 9 (2) and 10 (2) thereof,
Having regard to Council Directive 91/496/EEC of 15 July 1991 laying down the principles governing the organization of veterinary checks on animals entering the Community from third countries and amending Directives 89/662/EEC, 90/425/EEC and 90/675/EEC (12), as last amended by Decision 92/438/EEC (13), and in particular Article 18 thereof,
Having regard to Council Directive 92/118/EEC of 17 December 1992 laying down animal health and public health requirements governing trade in and imports into the Community of products not subject to the said requirements laid down in specific Community rules referred to in Annex A (I) to Directive 89/662/EEC and as regards pathogens, to Directive 90/425/EEC (14), and in particular Article 10 (2) thereof,
Whereas an agreement on the European Economic Area has been concluded on the 13 December 1993 between the European Communities, their Member States and the Republic of Austria, the Republic of Finland, the Republic of Iceland, the Principality of Liechtenstein, the Kingdom of Norway and the Kingdom of Sweden (15);
Whereas in the veterinary field and in order to enable the EFTA Surveillance Authority to take the necessary measures, the provisions of this Agreement apply only from 1 July 1994;
Whereas in the framework of this Agreement, Austria, Finland, Norway and Sweden apply some Community veterinary provisions laying down animal health and public health conditions governing trade in certain live animals and animal products; whereas, therefore, some Community provisions laying down animal health and public health requirements for imports from third countries will not apply to these countries any more;
Whereas account must be taken so that the Community third country regime for ovine and caprine animals remains applicable for imports from Austria, Finland and Norway;
Whereas, in this context, it is necessary to repeal the specific rules applying to Austria, Finland, Norway and Sweden and to amend other detailed rules applying to third countries including these four countries;
Whereas the measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
In Part 1 of the Annex to Council Decision 79/542/EEC (16), the data relating to Austria, Finland and Norway are deleted except those concerning live ovine and caprine and residues, and the line relating to Sweden is deleted.
Article 2
Commission Decision 80/790/EEC (17) is repealed.
Article 3
Commission Decision 80/799/EEC (18) is repealed.
Article 4
Commission Decision 80/800/EEC (19) is repealed.
Article 5
Council Decision 82/730/EEC (20) is repealed.
Article 6
Council Decision 82/731/EEC (21) is repealed.
Article 7
Council Decision 82/736/EEC (22) is repealed.
Article 8
Council Decision 83/421/EEC (23) is repealed.
Article 9
In the Annex to Commission Decision 90/14/EEC (24), 'Austria', 'Finland', 'Norway' and 'Sweden' are deleted.
Article 10
In the Annex to Commission Decision 91/270/EEC (25), 'Austria', 'Finland', 'Norway' and 'Sweden' are deleted.
Article 11
In the second part of Annex A and Annex B to Commission Decision 91/449/EEC (26), 'Austria', 'Finland', 'Norway' and 'Sweden' are deleted.
Article 12
Commission Decision 92/260/EEC (27) is amended as follows:
1. in Annex I, group A is replaced by:
'Group A
Greenland, Iceland and Switzerland';
2. in Annex II (A) the title of the health certificate is replaced by:
'Health certificate for the temporary admission of registered horses into the Community territory from Greenland, Iceland or Switzerland for a period of less than 90 days coming'.
Article 13
Commission Decision 92/401/EEC (28) is repealed.
Article 14
Commission Decision 92/461/EEC (29) is repealed.
Article 15
Commission Decision 92/462/EEC (30) is repealed.
Article 16
In part II Annex A to Commission Decision 92/471/EEC (31), 'Austria', 'Finland', 'Norway' and 'Sweden' are deleted.
Article 17
In the Annex to Commission Decision 93/160/EEC (32), 'Austria', 'Finland', 'Norway' and 'Sweden' are deleted.
Article 18
Commission Decision 93/195/EEC (33) is amended as follows:
1. in Annex I, group A is replaced by:
'Group A
Greenland, Iceland and Switzerland';
2. in Annex II, group A is replaced by:
'Group A
Greenland, Iceland and Switzerland'.
Article 19
Commission Decision 93/196/EEC (34) is amended as follows:
1. in Annex I, 'Austria, Finland' and 'Norway, Sweden' are deleted from footnote 5;
2. in Annex II, footnote 3, group A is replaced by:
'Group A:
Greenland, Iceland and Switzerland'.
Article 20
Commission Decision 93/197/EEC (35) is amended as follows:
1. in Annex I, group A is replaced by:
'Group A
Greenland, Iceland and Switzerland';
2. in Annex II (A) the title of the health certificate is replaced by:
'Health certificate for imports into Community territory of registered equidae and equidae for breeding and production from Greenland, Iceland and Switzerland'.
Article 21
In Part 2A of the Annex to Commission Decision 93/198/EEC (36), 'Sweden' is deleted.
Article 22
In Part 2 of the Annex to Commission Decision 93/199/EEC (37), 'Austria: Vorarlberg, Tyrol, Upper Austria, Burgenland, Carinthia, Styria and Vienna', 'Finland', 'Norway' and 'Sweden' are deleted.
Article 23
Commission Decision 93/321/EEC (38) is amended as follows:
1. in the title, 'Sweden, Norway, Finland and' is deleted;
2. in Article 1 (1) 'Sweden, Norway, Finland and' is deleted.
Article 24
Commission Decision 93/432/EEC (39) is repealed.
Article 25
Commission Decision 93/451/EEC (40) is repealed.
Article 26
Commission Decision 93/688/EC (41) is repealed.
Article 27
In the Annex to Commission Decision 93/693/EC (42), Parts 4, 8 and 9 are deleted.
Article 28
In the Annex to Commission Decision 94/70/EC (43) the lines relating to Austria, Finland, Norway and Sweden are deleted.
Article 29
In the Annex to Commission Decision 94/85/EC (44), the lines relating to Austria, Finland, Norway and Sweden are deleted.
Article 30
In Part II (B) of the Annex to Commission Decision 94/278/EC (45), 'Finland', 'Norway' and 'Sweden' are deleted.
Article 31
Commission Decision 94/316/EC (46) is repealed.
Article 32
This Decision is addressed to the Member States.
Done at Brussels, 29 June 1994.
For the Commission
René STEICHEN
Member of the Commission
(1) OJ No L 302, 31. 12. 1972, p. 28.
(2) OJ No L 173, 27. 6. 1992, p. 13.
(3) OJ No L 194, 22. 7. 1988, p. 10.
(4) OJ No L 186, 28. 7. 1993, p. 28.
(5) OJ No L 302, 19. 10. 1989, p. 1.
(6) OJ No L 175, 19. 7. 1993, p. 21.
(7) OJ No L 224, 18. 8. 1990, p. 42.
(8) OJ No L 157, 10. 6. 1992, p. 28.
(9) OJ No L 373, 31. 12. 1990, p. 1.
(10) OJ No L 62, 15. 3. 1993, p. 49.
(11) OJ No L 224, 18. 8. 1990, p. 62.
(12) OJ No L 268, 24. 9. 1991, p. 56.
(13) OJ No L 243, 25. 8. 1992, p. 27.
(14) OJ No L 62, 15. 3. 1993, p. 49.
(15) OJ No L 1, 3. 1. 1994, p. 1.
(16) OJ No L 146, 14. 6. 1979, p. 15.
(17) OJ No L 233, 4. 9. 1980, p. 47.
(18) OJ No L 234, 5. 9. 1980, p. 35.
(19) OJ No L 234, 5. 9. 1980, p. 38.
(20) OJ No L 311, 8. 11. 1982, p. 1.
(21) OJ No L 311, 8. 11. 1982, p. 4.
(22) OJ No L 311, 8. 11. 1982, p. 18.
(23) OJ No L 238, 27. 8. 1983, p. 35.
(24) OJ No L 8, 11. 1. 1990, p. 71.
(25) OJ No L 134, 29. 5. 1991, p. 56.
(26) OJ No L 240, 29. 8. 1991, p. 28.
(27) OJ No L 130, 15. 5. 1992, p. 67.
(28) OJ No L 224, 8. 8. 1992, p. 1.
(29) OJ No L 261, 7. 9. 1992, p. 18.
(30) OJ No L 261, 7. 9. 1992, p. 34.
(31) OJ No L 270, 15. 9. 1992, p. 27.
(32) OJ No L 67, 19. 3. 1993, p. 27.
(33) OJ No L 86, 6. 4. 1993, p. 1.
(34) OJ No L 86, 6. 4. 1993, p. 7.
(35) OJ No L 86, 6. 4. 1993, p. 16.
(36) OJ No L 86, 6. 4. 1993, p. 34.
(37) OJ No L 86, 6. 4. 1993, p. 43.
(38) OJ No L 123, 19. 5. 1993, p. 36.
(39) OJ No L 200, 10. 8. 1993, p. 39.
(40) OJ No L 210, 21. 8. 1993, p. 21.
(41) OJ No L 319, 21. 12. 1993, p. 51.
(42) OJ No L 320, 22. 12. 1993, p. 35.
(43) OJ No L 36, 8. 2. 1994, p. 5.
(44) OJ No L 44, 17. 2. 1994, p. 33.
(45) OJ No L 120, 11. 5. 1994, p. 44.
(46) OJ No L 140, 3. 6. 1994, p. 30.
