Commission Directive 2004/104/EC
of 14 October 2004
adapting to technical progress Council Directive 72/245/EEC relating to the radio interference (electromagnetic compatibility) of vehicles and amending Directive 70/156/EEC on the approximation of the laws of the Member States relating to the type-approval of motor vehicles and their trailers
(Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 70/156/EEC of 6 February 1970 on the approximation of the laws of the Member States relating to the type-approval of motor vehicles and their trailers [1], and in particular Article 13(2) thereof,
Having regard to Council Directive 72/245/EEC of 20 June 1972 relating to the radio interference (electromagnetic compatibility) of vehicles [2], and in particular Article 4 thereof,
Whereas:
(1) Directive 72/245/EEC is one of the separate directives under the type-approval procedure established by Directive 70/156/EEC.
(2) Since 1995, when Directive 72/245/EEC was amended, there has been a considerable increase in the number of electrical and electronic components fitted to motor vehicles. Such components now control not only comfort, information and entertainment devices but even certain safety-relevant functions.
(3) In the light of the experience gained since the amendment to Directive 72/245/EEC, it is no longer necessary for after-market equipment unrelated to safety functions to be regulated by a Directive specifically concerning electromagnetic compatibility (EMC) in the automobile sector. For such equipment it is sufficient to obtain a Declaration of Conformity in accordance with the procedures laid down in Council Directive 89/336/EEC of 3 May 1989 on the approximation of the laws of the Member States relating to electromagnetic compatibility [3] and in Directive 1999/5/EC of the European Parliament and of the Council of 9 March 1999 on radio equipment and telecommunications terminal equipment and the mutual recognition of their conformity [4].
(4) The EMC requirements and test provisions for electrical and electronic equipment have constantly been updated through the standardisation work of the International Special Committee on Radio Interference (CISPR) and the International Organisation for Standardisation (ISO). It is therefore appropriate to refer in this Directive to the test procedures outlined in the recent editions of the relevant standards.
(5) Directive 72/245/EEC should therefore be amended accordingly.
(6) The amendments to Directive 72/245/EEC have an impact on Directive 70/156/EEC. It is therefore necessary to amend that directive accordingly.
(7) The measures provided for in this Directive are in accordance with the opinion of the Committee for Adaptation to Technical Progress established by Directive 70/156/EEC,
HAS ADOPTED THIS DIRECTIVE:
Article 1
The Annexes to Directive 72/245/EEC are replaced by the Annexes to this Directive.
Article 2
1. With effect from 1 January 2006, for vehicles, components or separate technical units which comply with the provisions laid down in Annexes I to X to Directive 72/245/EEC as amended by this Directive, no Member State may, on grounds relating to electromagnetic compatibility:
(a) refuse to grant EC type-approval, or national type-approval or
(b) prohibit registration, sale or entry into service.
2. With effect from 1 July 2006, for a type of vehicle, component or separate technical unit where the requirements laid down in Annexes I to X to Directive 72/245/EEC, as amended by this Directive, are not fulfilled, Member States, on grounds related to electromagnetic compatibility:
(a) shall no longer grant EC type-approval, and
(b) may refuse to grant national type-approval.
3. With effect from 1 January 2009, if the provisions laid down in Annexes I to X to Directive 72/245/EEC, as amended by this Directive, are not fulfilled, Member States, on grounds related to electromagnetic compatibility:
(a) shall consider certificates of conformity which accompany new vehicles in accordance with the provisions of Directive 70/156/EEC to be no longer valid for the purposes of Article 7(1) of that Directive;
(b) may refuse the registration, sale or entry into service of new vehicles.
4. As from 1 January 2009, the provisions laid down in Annexes I to X to Directive 72/245/EEC, as amended by this Directive, relating to electromagnetic compatibility, shall apply to components or separate technical units for the purposes of Article 7(2) of Directive 70/156/EEC.
Article 3
Directive 70/156/EEC is amended as follows:
1. Annex I is amended as follows:
(a) the following line is added to point 0.5:
"Name and address of authorised representative, if any:"
(b) the following point is inserted:
"12.7. Table of installation and use of RF transmitters in the vehicle(s), if applicable (see Annex I, 3.1.8.):
+++++ TABLE +++++
The applicant for type-approval must also supply, where appropriate:
Appendix 1
A list (with make(s) and type(s) of all electrical and/or electronic components concerned by this Directive (see points 2.1.9 and 2.1.10) and not previously listed.
Appendix 2
Schematics or drawing of the general arrangement of electrical and/or electronic components (concerned by this Directive) and the general wiring harness arrangement.
Appendix 3
Description of vehicle chosen to represent the type
Body style:
Left or right hand drive:
Wheelbase:
Appendix 4
Relevant test report(s) supplied by the manufacturer or approved/recognised laboratories for the purpose of drawing up the type-approval certificate."
2. In Annex III, Section A, the following line is added to point 0.5:
"Name and address of authorised representative, if any:".
Article 4
Transposition
1. Member States shall adopt and publish, by 31 December 2005 at the latest, the laws, regulations and administrative provisions necessary to comply with this Directive. They shall forthwith communicate to the Commission the text of those provisions and a correlation table between those provisions and this Directive.
They shall apply those provisions from 1 January 2006.
When Member States adopt those provisions, they shall contain a reference to this Directive or be accompanied by such reference on the occasion of their official publication. Member States shall determine how such reference is to be made.
2. Member States shall communicate to the Commission the texts of the main provisions of national law, which they adopt in the field governed by this Directive.
Article 5
This Directive shall enter into force on the 20th day following that of its publication in the Official Journal of the European Union.
Article 6
This Directive is addressed to the Member States.
Done at Brussels, 14 October 2004.
For the Commission
Olli Rehn
Member of the Commission
--------------------------------------------------
[1] OJ L 42, 23.2.1970, p. 1. Directive as last amended by Commission Directive 2004/78/EC (OJ L 153, 30.4.2004, p. 103).
[2] OJ L 152, 6.7.1972, p. 15. Directive as last amended by Directive 95/54/EC of the European Parliament and of the Council (OJ L 266, 8.11.1995, p. 1).
[3] OJ L 139, 23.5.1989, p. 19. Directive as last amended by Directive 93/68/EEC (OJ L 220, 30.8.1993, p. 1).
[4] OJ L 91, 7.4.1999, p. 10.
--------------------------------------------------
+++++ ANNEX 1 ++++++++++ ANNEX 2 ++++++++++ ANNEX 3 ++++++++++ ANNEX 4 ++++++++++ ANNEX 5 ++++++++++ ANNEX 6 ++++++++++ ANNEX 7 ++++++++++ ANNEX 8 ++++++++++ ANNEX 9 ++++++++++ ANNEX 10 ++++++++++ ANNEX 11 ++++++++++ ANNEX 12 ++++++++++ ANNEX 13 ++++++++++ ANNEX 14 +++++
