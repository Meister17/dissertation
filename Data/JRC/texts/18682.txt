COMMISSION REGULATION (EEC) No 2928/93 of 25 October 1993 amending Regulation (EEC) No 890/78 laying down detailed rules for the certification of hops
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 1696/71 of 26 July 1971 on the common organization of the market in hops (1), as last amended by Regulation (EEC) No 3124/92 (2), and in particular Article 2 (5) thereof,
Having regard to Council Regulation (EEC) No 1784/77 of 19 July 1977 concerning the certitication of hops (3), as las amended by Regulation (EEC) No 1987/93 (4), excludes certain products from the certification process on account or their specific characteristics or their intended use; whereas isomerized hop powders and the new isomerized hop products referred to in Article 1 of Regulation (EEC) No 1784/77 fall within that group of products, as do isomerized hop extracts; whereas those products in Commission Regulation (EEC) No 890/78 (5), as last amended by Regulation (EEC) No 2265/91 (6), should be defined more precisely;
Whereas the second subparagraph of Article 1 (1) of Regulation (EEC) No 1784/77 lays down that products exempt from certification must be subject to a control procedure; whereas the control procedure must ensure that such products do not disturb the normal marketing of certified products and are used only for the purposes stated and only be those for whom they are intended;
Whereas the control procedures should be the responsibility of the bodies carrying out certification;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Hops,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EEC) No 890/78 is hereby amended as follows:
1. Article 1 is replaced by the following:
'Article 1
(a) "unprepared hops" means hops which have undergone only preliminary drying and packaging;
(b) "prepared hops" means hops which have undergone final drying and final packaging;
(c) "seeded hops" means hops marketed with a seed content greater than 2 % of their weight;
(d) "seedless hops" means hops marketed with a seed content not exceeding 2 % of their weight;
(e) "isomerized hop extract" means an extract in which the alpha acids have been almost totally isomerized;
(f) "isomerized hop powder" means a powder in which the alpha acids have been almost totally isomerized;
(g) "new isomerized hop products" means products in which not only have the alpha acids been totally isomerized but other constituents have also undergone alteration to a greater or lesser extent (according to the stage at which and the condition under which the transformation of the alpha acids has been carried out) or have been deliberately eliminated from the final product;
(h) "sealing" means closure of the package under official supervision and in such a way that the means of clusure will be damaged when the package is opened;
(i) "closed operating circuit" means a process for preparing or processing hops carried out under official supervision and in such a way that no hops or processed products can be added or removed during the operation. The closed operating circuit starts with the opening of the sealed package containing the hops or hop products to be prepared or processed and ends with the sealing of the package containing the processed hops or hop product;
(j) "consignment" means a number of packages of hops or hop products with the same characteristics presented at the same time for certification by the same individual or associated producer or by the same processor.';
2. paragraphs 5 and 6 of Article 8 are replaced by the following:
'5. With the exception of the substances set out in Annex V, only the certified hops and hop products referred to in Article 7 of Regulation (EEC) No 1784/77 may enter the closed operating circuit. They may enter only in the state in which they have been certified.
6. If, during the production of extracts manufactured by the use of carbon dioxide, processing in the closed operating circuit has to be interrupted for technical reasons, the representatives of the official bodies or departments referred to in Article 1 (6) of Regulation (EEC) No 1784/77 shall seal the package containing the intermediate product at the point of interruption. The seals may be broken only by the abovementioned representatives when processing resumes.';
3. Article 10 is amended as follows:
(a) the opening phrase is replaced by the following:
'The products referred to in Article 1 (1) (a) to (f) of Regulation (EEC) No 1784/77 shall be subject to the following checks:'
(b) point (b) is replaced by the following:
'(b) In the case of isomerized hop extracts, isomerized hop powders and the new isomerized hop products listed in Annex VI, the processor shall each year, not later than 31 December, declare to the control body the quantities produced and the quantities marketed. The packaging must bear the words 'isomerized hop extract', 'isomerized hop powder' or 'new isomerized hop product' and must state the weight or volume, the original variety, the product used and and the percentage of the product used;'
(c) point (d) is replaced by the following:
'(d) In the case of hops and hop products put up in small packets for sale to private individuals for their own use, the weight of the package may not exceed:
- 1 kg in the case of cones or powder,
- 300 g in the case of extract, powder and the new isomerized products.
A description of the product and its weight must appear on the package.';
4. the Annex hereto is added is Annex VI.
Article 2
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 25 October 1993.
For the Commission
René STEICHEN
Member of the Commission
(1) OJ No L 175, 4. 8. 1971, p. 1.
(2) OJ No L 313, 30. 10. 1992, p. 1.
(3) OJ No L 200, 8. 8. 1977, p. 1.
(4) OJ No L 182, 24. 7. 1993, p. 1.
(5) OJ No L 117, 29. 4. 1978, p. 43.
(6) OJ No L 208, 30. 7. 1991, p. 22.
ANNEX
'ANNEX VI
ISOMERIZED HOP PRODUCTS ON, OR NEAR, MARKET - NOVEMBER 1992
"" ID="01">Isomerized pellets> ID="02">Conventional Type 90 powder mixed with metallic oxide (usually magnesium), pelleted and subjected to slow, low temperature, warming up> ID="03">In replacement of standard hop pellets in kettle, or as late addition in kettle"> ID="01">Extrudate (extruded hop powder)> ID="02">Conventional powder mixed with metallic carbonates, oxides or hydroxides (or mixtures thereof), forced through an extrusion cooker (high pressure plus temperature for short period)> ID="03">As for isomerized pellets"> ID="01">Stabilized pellets> ID="02">As for isomerized pellets, but with no warming up> ID="03">As for isomerized pellets"> ID="01">Isomerized kettle extracts (including PIKE, MIKE, IKE, IRE)> ID="02">Generally conventional (usually CO2) extracts mixed with metallic oxides, hydroxides or carbonates (or mixtures thereof) and subjected to heating or pressure (or both). In some products, the metal ions and metallic salts are removed from the final mixture> ID="03">In replacement of standard kettle extracts, or as late addition in kettle"> ID="01">Post-fermentation - isomerized extracts> ID="02">Hop extracts purified and treated as above to give relatively pure isomerized alpha acid (generally in the form of alkali metal salts of isomerized alpha acids (usually potassium))> ID="03">As final adjustment to beer bitterness levels, with no effect on other beer flavours"> ID="01">Post-fermentation - reduced isomerized extracts> ID="02">Hop extracts purified, chemically reduced and treated as above to give relatively pure reduced isomerized products> ID="03">For control of beer bitterness levels, protection against "sunstruck" taints and enhancement of foam stability with no effect on other beer flavours' ">
