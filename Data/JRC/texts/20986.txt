Commission Directive 2002/78/EC
of 1 October 2002
adapting to technical progress Council Directive 71/320/EEC on the approximation of the laws of the Member States relating to the braking devices of certain categories of motor vehicles and their trailers
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 70/156/EEC of 6 February 1970 on the approximation of the laws of the Member States relating to the type-approval of motor vehicles and their trailers(1), as last amended by Commission Directive 2001/116/EC(2), and in particular Article 13(2) thereof,
Having regard to Council Directive 71/320/EEC of 26 July 1971 on the approximation of the laws of the Member States relating to the braking devices of certain categories of motor vehicles and their trailers(3), as last amended by Commission Directive 98/12/EC(4), and in particular Article 5 thereof,
Whereas:
(1) Directive 71/320/EEC is one of the separate Directives of the EC type-approval procedure which has been established by Directive 70/156/EEC. Consequently, the provisions laid down in Directive 70/156/EEC relating to vehicle systems, components and separate technical units apply to Directive 71/320/EEC.
(2) It is not considered necessary to apply the requirements concerning the approval of after-market replacement brake lining assemblies to those assemblies used in the approval of the braking system, provided such assemblies can be identified in accordance with the requirements of this Directive.
(3) It is necessary to clarify the application of Directive 71/320/EEC to the after-market replacement brake lining assemblies with regard to their marking and packaging. Differentiation needs to be made between those replacement brake lining assemblies that are identical to the original equipment supplied for the specified vehicles and those that are not.
(4) Directive 71/320/EEC should be amended accordingly;
(5) The measures provided for in this Directive are in accordance with the opinion of the Committee for adaptation to technical progress established by Directive 70/156/EEC,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Annexes I, IX and XV to Directive 71/320/EEC are amended in accordance with the Annex to this Directive.
Article 2
With effect from 1 January 2003 Member States shall not, on grounds relating to vehicle braking systems prohibit the sale or entry into service of replacement brake linings if the replacement brake linings comply with the requirements of Directive 71/320/EEC, as amended by this Directive.
Article 3
1. With effect from 1 June 2003 Member States may, on grounds relating to vehicle braking systems prohibit the sale or entry into service of replacement brake linings if the replacement brake linings do not comply with the requirements of Directive 71/320/EEC, as amended by this Directive.
2. Notwithstanding the provision of paragraph 1, for the purpose of replacement parts, Member States shall permit the sale or entry into service of replacement brake linings intended for fitting to vehicle types for which type-approval was granted prior to the entry into force of Directive 71/320/EEC, as amended by Directive 98/12/EC, and on condition that such replacement brake linings do not contravene the provisions of the previous version of Directive 71/320/EEC, as amended by Directive 98/12/EC, which was applicable at the time of entry into service of these vehicles. In any case these brake linings shall not contain asbestos.
Article 4
Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive by 31 December 2002 at the latest. They shall forthwith inform the Commission thereof.
When Member States adopt those provisions, they shall contain a reference to this Directive or be accompanied by such a reference on the occasion of their official publication. Member States shall determine how such reference is to be made.
Article 5
This Directive shall enter into force on the 20th day following that of its publication in the Official Journal of the European Communities.
Article 6
This Directive is addressed to the Member States.
Done at Brussels, 1 October 2002.
For the Commission
Erkki Liikanen
Member of the Commission
(1) OJ L 42, 23.2.1970, p. 1.
(2) OJ L 18, 21.1.2002, p. 1.
(3) OJ L 202, 6.9.1971, p. 37.
(4) OJ L 81, 18.3.1998, p. 1.
ANNEX
The Annexes to Directive 71/320/EEC are amended as follows:
1. In Annex I, point 2, the following points 2.3 to 2.3.4 are added: "2.3. Brake linings and brake lining assemblies
2.3.1. Brake lining assemblies used to replace components at the end of their working life shall comply with the requirements in Annex XV for those categories of vehicles specified in point 1.1 of Annex XV.
2.3.2. However, where the brake lining assemblies are of a type covered by point 1.2 of the Addendum to Annex IX and are intended for fitment to a vehicle/axle/brake to which the relevant type approval document refers, they do not need to comply with Annex XV provided they fulfil the requirements in points 2.3.2.1 to 2.3.2.2.
2.3.2.1. Marking
The brake lining assemblies shall bear at least the following identifications:
2.3.2.1.1. Vehicle and/or component manufacturer's name or trade mark;
2.3.2.1.2. Make and identifying part number of the brake lining assembly as recorded in the information mentioned in point 2.3.4.
2.3.2.2. Packaging
The brake lining assemblies shall be packaged in axle sets in accordance with the following requirements:
2.3.2.2.1. Each package shall be sealed and constructed to show previous opening;
2.3.2.2.2. Each package shall display at least:
2.3.2.2.2.1. The quantity of brake lining assemblies contained;
2.3.2.2.2.2. Vehicle and/or component manufacturer's name or trade mark;
2.3.2.2.2.3. Make and identifying part number(s) of the brake lining assembly (assemblies), as recorded in the information mentioned in point 2.3.4.
2.3.2.2.2.4. Part number(s) of the axle set, as recorded in the information mentioned in point 2.3.4.
2.3.2.2.2.5. Sufficient information for the customer to identify the vehicles/axles/brakes for which the contents are approved.
2.3.2.2.3. Each package shall contain fitting instructions with particular reference to ancillary parts and stating that the brake lining assemblies must be replaced in axle sets.
2.3.2.2.3.1. The fitting instructions may alternatively be supplied in a separate transparent container together with the brake lining assembly package.
2.3.3. Brake lining assemblies supplied to vehicle manufacturers exclusively for use during the assembly of vehicles do not need to comply with the requirements in points 2.3.2.1 and 2.3.2.2 above.
2.3.4. The vehicle manufacturer shall provide to the technical service and/or approval authority the necessary information in electronic format which makes the link between the relevant part numbers and the type approval documentation.
This information shall contain:
- make(s) and type(s) of vehicle
- make(s) and type(s) of brake lining
- part number(s) and quantity of the brake lining assemblies
- part number(s) of the axle set
- type approval number of the braking system of the relevant vehicle type(s)."
2. Annex IX, Appendix 1, is amended as follows:(a) The first line of the EC type-approval certificate is replaced by the following: "Communication(1) concerning the"
(b) In the addendum to the EC type-approval certificate, points 1.2, 1.2.1 and 1.2.2 are replaced by the following: "1.2. Brake linings
1.2.1. Brake linings tested to all relevant prescriptions of Annex II
1.2.1.1. Make(s) and type(s) of brake linings:
1.2.2. Alternative brake linings tested in Annex XII
1.2.2.1. Make(s) and type of brake linings:"
3. Annex XV is amended as follows:(a) Point 6.1 is replaced by the following: "Replacement brake lining assemblies conforming to a type approved in accordance with this Directive shall be packaged in axle sets."
(b) Point 6.3.4 is replaced by the following: "sufficient for the customer to identify the vehicles/axles/brakes for which the contents are approved."
(1) At the request of (an) applicant(s) for an approval to Annex XV of Directive 71/320/EEC, the information referred to in Annex IX, Appendix 3, of Directive 71/320/EEC shall be provided by the Type Approval Authority. However, this information shall not be provided for purposes other than approvals to Annex XV of Directive 71/320/EEC.
