Council Decision
of 2 June 2004
on the signing and conclusion of the Agreement between the European Community and the Swiss Confederation providing for measures equivalent to those laid down in Council Directive 2003/48/EC on taxation of savings income in the form of interest payments and the accompanying Memorandum of Understanding
(2004/911/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 94 in conjunction with Article 300(2), first subparagraph (3), first subparagraph and (4) thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament [1],
Whereas:
(1) On 16 October 2001, the Council authorised the Commission to negotiate with the Swiss Confederation an appropriate agreement for securing the adoption by the Swiss Confederation of measures equivalent to those to be applied within the Community to ensure effective taxation of savings income in the form of interest payments.
(2) The application of the provisions of Directive 2003/48/EC [2] depends on the application by the Swiss Confederation of measures equivalent to those contained in that Directive, in accordance with an Agreement entered into by the Swiss Confederation with the Community.
(3) The Community has decided to grant the request of the Swiss Confederation for the inclusion in the Agreement of measures equivalent to the regimes provided for in Council Directive 90/435/EEC of 23 July 1990 on the common system of taxation applicable in the case of parent companies and subsidiaries of different Member States [3] and in Council Directive 2003/49/EC of 3 June 2003 on a common system of taxation applicable to interest and royalty payments made between associated companies of different Member States [4] in their original versions.
(4) The Agreement and the Memorandum of Understanding should be signed and approved on behalf of the Community.
(5) It is necessary to provide for a simple and rapid procedure for possible adaptations of Annexes I and II to the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
The Agreement between the European Community and the Swiss Confederation providing for measures equivalent to those laid down in Council Directive 2003/48/EC on taxation of savings income in the form of interest payments and the accompanying Memorandum of Understanding are hereby approved on behalf of the European Community.
The texts of the Agreement and the accompanying Memorandum of Understanding are attached to this Decision.
Article 2
The Commission is hereby authorised to approve, on behalf of the Community, amendments to the Annexes to the Agreement which ensure that they correspond to the data relating to competent authorities resulting from the notifications referred to in Article 5(a) of Directive 2003/48/EC and to the data in the Annex thereto.
Article 3
The President of the Council is hereby authorised to designate the persons empowered to sign the Agreement, the accompanying Memorandum of Understanding and the Exchange of Letters referred to in Article 22(2) of the Agreement and in the Memorandum of Understanding on behalf of the Community.
Article 4
The President of the Council shall effect the notification provided for in Article 17(1) of the Agreement on behalf of the Community [5].
Article 5
This Decision shall be published in the Official Journal of the European Union.
Done at Luxembourg, 2 June 2004.
For the Council
The President
C. McCreevy
--------------------------------------------------
[1] Opinion delivered on 30 March 2004 (not yet published in the Official Journal).
[2] OJ L 157, 26.6.2003, p. 38.
[3] OJ L 225, 20.8.1990, p. 6.
[4] OJ L 157, 26.6.2003, p. 49.
[5] The date of entry into force of the Agreement will be published in the Official Journal of the European Union by the General Secretariat of the Council.
