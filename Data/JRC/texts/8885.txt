COUNCIL DIRECTIVE of 4 May 1976 on pollution caused by certain dangerous substances discharged into the aquatic environment of the Community (76/464/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Articles 100 and 235 thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament (1),
Having regard to the opinion of the Economic and Social Committee (2),
Whereas there is an urgent need for general and simultaneous action by the Member States to protect the aquatic environment of the Community from pollution, particularly that caused by certain persistent, toxic and bioaccumulable substances;
Whereas several conventions or draft conventions, including the Convention for the prevention of marine pollution from land-based sources, the draft Convention for the protection of the Rhine against chemical pollution and the draft European Convention for the protection of international watercourses against pollution, are designed to protect international watercourses and the marine environment from pollution ; whereas it is important to ensure the coordinated implementation of these conventions;
Whereas any disparity between the provisions on the discharge of certain dangerous substances into the aquatic environment already applicable or in preparation in the various Member States may create unequal conditions of competition and thus directly affect the functioning of the common market ; whereas it is therefore necessary to approximate laws in this field, as provided for in Article 100 of the Treaty;
Whereas it seems necessary for this approximation of laws to be accompanied by Community action so that one of the aims of the Community in the sphere of protection of the environment and improvement of the quality of life can be achieved by more extensive rules ; whereas certain specific provisions to this effect should therefore be laid down ; whereas Article 235 of the Treaty should be invoked as the powers required for this purpose have not been provided for by the Treaty;
Whereas the programme of action of the European Communities on the environment (3), provides for number of measures to protect fresh water and se water from certain pollutants;
Whereas in order to ensure effective protection of the aquatic environment of the Community, it is necessary to establish a first list, called List I, of certain individual substances selected mainly on the basis of their toxicity, persistence, and bioaccumulation, with the exception of those which are biologically harmless or (1)OJ No C 5, 8.1.1975, p. 62. (2)OJ No C 108, 15.5.1975, p. 76. (3)OJ No C 112, 20.12.1973, p. 1.
which are rapidly converted into substances which are biologically harmless, and a second list, called List II, containing substances which have a deleterious effect on the aquatic environment, which can, however, be confined to a given area and which depend on the characteristics and location of the water into which they are discharged ; whereas any discharge of these substances should be subject to prior authorization which specifies emission standards;
Whereas pollution through the discharge of the various dangerous substances within List I must be eliminated ; whereas the Council should, within specific time limits and on a proposal from the Commission, adopt limit values which the emission standards should not exceed, methods of measurement, and the time limits with which existing dischargers should comply;
Whereas the Member States should apply these limit values, except where a Member State can prove to the Commission, in accordance with a monitoring procedure set up by the Council, that the quality objectives established by the Council, on a proposal from the Commission, are being met and continuously maintained throughout the area which might be affected by the discharges because of the action taken, among others, by that Member State;
Whereas it is necessary to reduce water pollution caused by the substances within List II ; whereas to this end the Member States should establish programmes which incorporate quality objectives for water drawn up in compliance with Council Directives where they exist ; whereas the emission standards applicable to such substances should be calculated in terms of these quality objectives;
Whereas, subject to certain exceptions and modifications, this Directive should be applied to discharges into ground water pending the adoption of specific Community rules in the matter;
Whereas one or more Member States may be able, individually or jointly, to take more stringent measures than those provided for under this Directive;
Whereas an inventory of discharges of certain particularly dangerous substances into the aquatic environment of the Community should be drawn up in order to know where they originated;
Whereas it may be necessary to revise and, where required, supplement Lists I and II on the basis of experience, if appropriate, by transferring certain substances from List II to List I,
HAS ADOPTED THIS DIRECTIVE:
Article 1
1. Subject to Article 8, this Directive shall apply to: - inland surface water,
- territorial waters,
- internal coastal waters,
- ground water.
2. For the purposes of this Directive: (a) "inland surface water" means all static or flowing fresh surface water situated in the territory of one or more Member States;
(b) "internal coastal waters" means waters on the land-ward side of the base line from which the breadth of territorial waters is measured, extending, in the case of watercourses, up to the fresh-water limit;
(c) "fresh-water limit" means the place in the watercourse where, at low tide and in a period of low fresh-water flow, there is an appreciable increase in salinity due to the presence of sea-water;
(d) "discharge" means the introduction into the waters referred to in paragraph 1 of any substances in List I or List II of the Annex, with the exception of: - discharges of dredgings,
- operational discharges from ships in territorial waters,
- dumping from ships in territorial waters;
(e) "pollution" means the discharge by man, directly or indirectly, of substances or energy into the aquatic environment, the results of which are such as to cause hazards to human health, harm to living resources and to aquatic ecosystems, damage to amenities or interference with other legitimate uses of water.
Article 2
Member States shall take the appropriate steps to eliminate pollution of the waters referred to in Article 1 by the dangerous substances in the families and groups of substances in List I of the Annex and to reduce pollution of the said waters by the dangerous substances in the families and groups of substances in List II of the Annex, in accordance with this Directive, the provisions of which represent only a first step towards this goal.
Article 3
With regard to the substances belonging to the families and groups of substances in List I, hereinafter called "substances within List I": 1. all discharges into the waters referred to in Article 1 which are liable to contain any such substance shall require prior authorization by the competent authority of the Member State concerned;
2. the authorization shall lay down emission standards with regard to discharges of any such substance into the waters referred to in Article 1 and, where this is necessary for the implementation of this Directive, to discharges of any such substance into sewers;
3. in the case of existing discharges of any such substance into the waters referred to in Article 1, the dischargers must comply with the conditions laid down in the authorization within the period stipulated therein. This period may not exceed the limits laid down in accordance with Article 6 (4);
4. authorizations may be granted for a limited period only. They may be renewed, taking into account any charges in the limit values referred to in Article 6.
Article 4
1. Member States shall apply a system of zero-emission to discharges into ground water of substances within List I.
2. Member States shall apply to ground water the provisions of this Directive relating to the substances belonging to the families and groups of substances in List II, hereinafter called "substances within List II".
3. Paragraphs 1 and 2 shall apply neither to domestic effluents nor to discharges injected into deep, saline and unusable strata.
4. The provisions of this Directive relating to ground water shall no longer apply upon the implementation of a separate Directive on ground water.
Article 5
1. The emission standards laid down in the authorizations granted pursuant to Article 3 shall determine: (a) the maximum concentration of a substance permissible in a discharge. In the case of dilution the limit value provided for in Article 6 (1) (a) shall be divided by the dilution factor;
(b) the maximum quantity of a substance permissible in a discharge during one or more specified periods of time. This quantity may, if necessary, also be expressed as a unit of weight of the pollutant per unit of the characteristic element of the polluting activity (e.g. unit of weight per unit of raw material or per product unit).
2. For each authorization, the competent authority of the Member State concerned may, if necessary, impose more stringent emission standards than those resulting from the application of the limit values laid down by the Council pursuant to Article 6, taking into account in particular the toxicity, persistence, and bioaccumulation of the substance concerned in the environment into which it is discharged.
3. If the discharger states that he is unable to comply with the required emission standards, or if this situation is evident to the competent authority in the Member State concerned, authorization shall be refused.
4. Should the emission standards not be complied with, the competent authority in the Member State concerned shall take all appropriate steps to ensure that the conditions of authorization are fulfilled and, if necessary, that the discharge is prohibited.
Article 6
1. The Council, acting on a proposal from the Commission, shall lay down the limit values which the emission standards must not exceed for the various dangerous substances included in the families and groups of substances within List I. These limit values shall be determined by: (a) the maximum concentration of a substance permissible in a discharge, and
(b) where appropriate, the maximum quantity of such a substance expressed as a unit of weight of the pollutant per unit of the characteristic element of the polluting activity (e.g. unit of weight per unit of raw material or per product unit).
Where appropriate, limit values applicable to industrial effluents shall be established according to sector and type of product.
The limit values applicable to the substances within List I shall be laid down mainly on the basis of: - toxicity,
- persistence,
- bioaccumulation,
taking into account the best technical means available.
2. The Council, acting on a proposal from the Commission, shall lay down quality objectives for the substances within List I.
These objectives shall be laid down principally on the basis of the toxicity, persistence and accumulation of the said substances in living organisms and in sediment, as indicated by the latest conclusive scientific data, taking into account the difference in characteristics between salt-water and fresh water.
3. The limit values established in accordance with paragraph 1 shall apply except in the cases where a Member State can prove to the Commission, in accordance with a monitoring procedure set up by the Council on a proposal from the Commission, that the quality objectives established in accordance with paragraph 2, or more severe Community quality objectives, are being met and continuously maintained throughout the area which might be affected by the discharges because of the action taken, among others, by that Member State.
The Commission shall report to the Council the instances where it has had recourse to the quality objectives method. Every five years the Council shall review, on the basis of a Commission proposal and in accordance with Article 148 of the Treaty, the instances where the said method has been applied.
4. For those substances included in the families and groups of substances referred to in paragraph 1, the deadlines referred to in point 3 of Article 3 shall be laid down by the Council in accordance with Article 12, taking into account the features of the industrial sectors concerned and, where appropriate, the types of products.
Article 7
1. In order to reduce pollution of the waters referred to in Article 1 by the substances within List II, Member States shall establish programmes in the implementation of which they shall apply in particular the methods referred to in paragraphs 2 and 3.
2. All discharges into the waters referred to in Article 1 which are liable to contain any of the substances within List II shall require prior authorization by the competent authority in the Member State concerned, in which emission standards shall be laid down. Such standards shall be based on the quality objectives, which shall be fixed as provided for in paragraph 3.
3. The programmes referred to in paragraph 1 shall include quality objectives for water ; these shall be laid down in accordance with Council Directives, where they exist.
4. The programmes may also include specific provisions governing the composition and use of substances or groups of substances and products and shall take into account the latest economically feasible technical developments.
5. The programmes shall set deadlines for their implementation.
6. Summaries of the programmes and the results of their implementation shall be communicated to the Commission.
7. The Commission, together with the Member States, shall arrange for regular comparisons of the programmes in order to ensure sufficient coordination in their implementation. If it sees fit, it shall submit relevant proposals to the Council to this end.
Article 8
Member States shall take all appropriate steps to implement measures adopted by them pursuant to this Directive in such a way as not to increase the pollution of waters to which Article 1 does not apply. They shall in addition prohibit all acts which intentionally or unintentionally circumvent the provisions of this Directive.
Article 9
The application of the measures taken pursuant to this Directive may on no account lead, either directly or indirectly, to increased pollution of the waters referred to in Article 1.
Article 10
Where appropriate, one or more Member States may individually or jointly take more stringent measures than those provided for under this Directive.
Article 11
The competent authority shall draw up an inventory of the discharges into the waters referred to in Article 1 which may contain substances within List I to which emission standards are applicable.
Article 12
1. The Council, acting unanimously, shall take a decision within nine months on any Commission proposal made pursuant to Article 6 and on the proposals concerning the methods of measurement applicable.
Proposals concerning an initial series of substances as well as the methods of measurement applicable and the deadlines referred to in Article 6 (4) shall be submitted by the Commission within a maximum period of two years following notification of this Directive.
2. The Commission shall, where possible within 27 months following notification of this Directive, forward the first proposals made pursuant to Article 7 (7). The Council, acting unanimously, shall take a decision within nine months.
Article 13
1. For the purposes of this Directive, Member States shall supply the Commission, at its request to be submitted in each case, with all the necessary information, and in particular: - details of authorizations granted pursuant to Article 3 and Article 7 (2),
- the results of the inventory provided for in Article 11,
- the results of monitoring by the national network,
- additional information on the programmes referred to in Article 7.
2. Information acquired as a result of the application of this Article shall be used only for the purpose for which it was requested.
3. The Commission and the competent authorities of the Member States, their officials and other servants shall not disclose information acquired by them pursuant to this Directive and of a kind covered by the obligation of professional secrecy.
4. The provisions of paragraphs 2 and 3 shall not prevent publication of general information or surveys which do not contain information relating to particular undertakings or associations of undertakings.
Article 14
The Council, acting on a proposal from the Commission, which shall act on its own initiative or at the request of a Member State, shall revise and, where necessary, supplement Lists I and II on the basis of experience, if appropriate, by transferring certain substances from List II to List I.
Article 15
This Directive is addressed to the Member States.
Done at Brussels, 4 May 1976.
For the Council
The President
G. THORN
ANNEX
List I of families and groups of substances
List I contains certain individual substances which belong to the following families and groups of substances, selected mainly on the basis of their toxicity, persistence and bioaccumulation, with the exception of those which are biologically harmless or which are rapidly converted into substances which are biologically harmless: 1. organohalogen compounds and substances which may form such compounds in the aquatic environment,
2. organophosphorus compounds,
3. organotin compounds,
4. substances in respect of which it has been proved that they possess carcinogenic properties in or via the aquatic environment (1),
5. mercury and its compounds,
6. cadmium and its compounds,
7. persistent mineral oils and hydrocarbons of petroleum origin,
and for the purposes of implementing Articles 2, 8, 9 and 14 of this Directive:
8. persistent synthetic substances which may float, remain in suspension or sink and which may interfere with any use of the waters.
List II of families and groups of substances
List II contains: - substances belonging to the families and groups of substances in List I for which the limit values referred to in Article 6 of the Directive have not been determined,
- certain individual substances and categories of substances belonging to the families and groups of substances listed below,
and which have a deleterious effect on the aquatic environment, which can, however, be confined to a given area and which depend on the characteristics and location of the water into which they are discharged.
Families and groups of substances referred to in the second indent
1. The following metalloids and metals and their compounds: >PIC FILE= "T0009307">
2. Biocides and their derivatives not appearing in List I.
3. Substances which have a deleterious effect on the taste and/or smell of the products for human consumption derived from the aquatic environment,
and compounds liable to give rise to such substances in water.
4. Toxic or persistent organic compounds of silicon, and substances which may give rise to such compounds in water, excluding those which are biologically harmless or are rapidly converted in water into harmless substances. (1)Where certain substances in list II are carcinogenic, they are included in category 4 of this list.
5. Inorganic compounds of phosphorus and elemental phosphorus.
6. Non persistent mineral oils and hydrocarbons of petroleum origin.
7. Cyanides, fluorides.
8. Substances which have an adverse effect on the oxygen balance, particularly : ammonia, nitrites.
Statement on Article 8
With regard to the discharge of waste water into the open sea by means of pipelines, Member States undertake to lay down requirements which shall be not less stringent than those imposed by this Directive.
ANNEX
List I of families and groups of substances
List I contains certain individual substances which belong to the following families and groups of substances, selected mainly on the basis of their toxicity, persistence and bioaccumulation, with the exception of those which are biologically harmless or which are rapidly converted into substances which are biologically harmless: 1. organohalogen compounds and substances which may form such compounds in the aquatic environment,
2. organophosphorus compounds,
3. organotin compounds,
4. substances in respect of which it has been proved that they possess carcinogenic properties in or via the aquatic environment (1),
5. mercury and its compounds,
6. cadmium and its compounds,
7. persistent mineral oils and hydrocarbons of petroleum origin,
and for the purposes of implementing Articles 2, 8, 9 and 14 of this Directive:
8. persistent synthetic substances which may float, remain in suspension or sink and which may interfere with any use of the waters.
List II of families and groups of substances
List II contains: - substances belonging to the families and groups of substances in List I for which the limit values referred to in Article 6 of the Directive have not been determined,
- certain individual substances and categories of substances belonging to the families and groups of substances listed below,
and which have a deleterious effect on the aquatic environment, which can, however, be confined to a given area and which depend on the characteristics and location of the water into which they are discharged.
Families and groups of substances referred to in the second indent
1. The following metalloids and metals and their compounds: >PIC FILE= "T0009307">
2. Biocides and their derivatives not appearing in List I.
3. Substances which have a deleterious effect on the taste and/or smell of the products for human consumption derived from the aquatic environment,
and compounds liable to give rise to such substances in water.
4. Toxic or persistent organic compounds of silicon, and substances which may give rise to such compounds in water, excluding those which are biologically harmless or are rapidly converted in water into harmless substances. (1)Where certain substances in list II are carcinogenic, they are included in category 4 of this list.
5. Inorganic compounds of phosphorus and elemental phosphorus.
6. Non persistent mineral oils and hydrocarbons of petroleum origin.
7. Cyanides, fluorides.
8. Substances which have an adverse effect on the oxygen balance, particularly : ammonia, nitrites.
Statement on Article 8
With regard to the discharge of waste water into the open sea by means of pipelines, Member States undertake to lay down requirements which shall be not less stringent than those imposed by this Directive.
