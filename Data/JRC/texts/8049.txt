*****
COMMISSION DECISION
of 16 January 1990
fixing the agro-economic trend coefficient to be used for defining the European size unit in connection with the Community typology for agricultural holdings
(90/36/EEC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation No 79/65/EEC of 15 June 1965 setting up a network for the collection of accountancy data on the incomes and business operation of agricultural holdings in the European Economic Community (1), as last amended by Regulation (EEC) No 3644/85 (2),
Having regard to Council Regulation (EEC) No 571/88 of 29 February 1988 on the organization of community surveys on the structure of agricultural holdings between 1988 and 1997 (3), as amended by Regulation (EEC) No 807/89 (4), and in particular Article 8 (3) thereof,
Whereas Annex III to Commission Decision 85/377/EEC of 7 June 1985 establishing a Community typology for agricultural holdings (5) defines the European size unit; whereas the definition refers to a coefficient to take account of global agro-economic trends since the reference period '1980'; whereas the coefficient is to be calculated by the Commission and fixed after consultation with the Member States and whereas such consultation has taken place at the meetings of the Group of Experts 'Typology for Agricultural Holdings',
HAS ADOPTED THIS DECISION:
Article 1
The agro-economic trend coefficient used for defining the European size unit as provided for in Annex III (A) (2) to Decision 85/377/EEC is hereby fixed, for the trend between the reference periods '1980' and '1986', at 1,2.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 16 January 1990.
For the Commission
Henning CHRISTOPHERSEN
Vice-President
(1) OJ No 109, 23. 6. 1965, p. 1859/65.
(2) OJ No L 348, 24. 12. 1985, p. 4.
(3) OJ No L 56, 2. 3. 1988, p. 1.
(4) OJ No L 86, 31. 3. 1989, p. 1.
(5) OJ No L 220, 17. 8. 1985, p. 1.
