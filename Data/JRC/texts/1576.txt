Prior notification of a concentration
(Case COMP/M.4050 — Goldman Sachs/Cinven/Ahlsell)
(2005/C 307/04)
(Text with EEA relevance)
1. On 24 November 2005, the Commission received a notification of a proposed concentration pursuant to Article 4 of Council Regulation (EC) No 139/2004 [1] by which the undertakings GS Capital Partners Funds belonging to the Goldman Sachs Group, Inc ("Goldman Sachs", US) and the Cinven Group (UK) acquire within the meaning of Article 3(1)(b) of the Council Regulation joint control of the undertaking Ahlsell Group AB ("Ahlsell", Sweden) by way of purchase of shares.
2. The business activities of the undertakings concerned are:
- for Goldman Sachs: investment banking and securities firm. Goldman Sachs controls inter alia Prysmian, undertaking actives in the manufacturing of energy and telecom cables;
- for Cinven Group: private equity investments;
- for Ahlsell: distribution of installation products, including heating and plumbing, electrical and tools and machinery.
3. On preliminary examination, the Commission finds that the notified transaction could fall within the scope of Regulation (EC) No 139/2004. However, the final decision on this point is reserved.
4. The Commission invites interested third parties to submit their possible observations on the proposed operation to the Commission.
Observations must reach the Commission not later than 10 days following the date of this publication. Observations can be sent to the Commission by fax (No (32-2) 296 43 01 or 296 72 44) or by post, under reference number COMP/M.4050 — Goldman Sachs/Cinven/Ahlsell, to the following address:
European Commission
Competition DG
Merger Registry
J-70
BE-1049 Brussels
[1] OJ L 24, 29.1.2004, p. 1.
--------------------------------------------------
