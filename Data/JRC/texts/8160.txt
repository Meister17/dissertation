COUNCIL REGULATION (EC) No 2793/1999
of 17 December 1999
on certain procedures for applying the Trade, Development and Cooperation Agreement between the European Community and the Republic of South Africa
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 133 thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) The Council has concluded a Trade, Development and Cooperation Agreement between the European Community and the Republic of South Africa, hereinafter referred to as "the Agreement", and decided by Decision 1999/753/EC(1) that the Agreement enters into force provisionally on 1 January 2000;
(2) The tariff preferences provided in the Agreement are applicable to products originating in the Republic of South Africa in accordance with Protocol 1 of the Agreement;
(3) It is necessary to lay down the procedures for applying certain provisions of the Agreement;
(4) The preferential rates of duty to be applied by the Community under the Agreement should normally be calculated on the basis of the conventional rate of duty in the Common Customs Tariff for the products concerned; they should, however, be calculated from the autonomous rate of duty where no conventional rate is given for the products concerned or where the autonomous rate is lower than the conventional rate; whereas it is unnecessary to include in the coverage of this Regulation products for which the Common Customs Tariff duty is free; the calculation must in no case be based on duties applied under conventional or autonomous tariff quotas;
(5) The Agreement stipulates that certain products originating in the Republic of South Africa may be imported into the Community, within the limits of tariff quotas, at a reduced or a zero rate of customs duty; the Agreement specifies the products eligible for those tariff measures, their volumes and duties; the most suitable method for managing the tariff quota for products of CN code ex 0406 is based on import licences and should be carried out by the Commission; the other tariff quotas should be managed, as a rule, on a first-come first-served basis in accordance with Articles 308a to 308c of Regulation (EEC) No 2454/93 of 2 July 1993 laying down provisions for the implementation of Council Regulation (EEC) No 2913/92 establishing the Community Customs Code(2);
(6) Amendments to the Combined Nomenclature and Taric codes and adaptations arising from the conclusion of agreements, protocols or exchanges of letters between the Community and the Republic of South Africa do not involve changes of substance; in the interest of simplicity, provision should therefore be made for the Commission, assisted by the Customs Code Committee, to take the measures necessary for the implementation of this Regulation, in accordance with Council Decision 1999/468/EC of 28 June 1999 laying down the procedures for the exercise of implementing powers of the Commission(3);
(7) In the interest of combating fraud, provisions should be made to submit preferential imports into the Community to surveillance,
HAS ADOPTED THIS REGULATION:
Article 1
1. For the application of preferential duty under the Agreement, the expression "duty effectively applied" shall be taken to mean either:
- the lowest rate of duty appearing in column 3 or 4, taking into account the periods of application mentioned or referred to in that column, of the second part of Annex I of Council Regulation (EEC) No 2658/87 of 23 July 1987 on the tariff and statistical nomenclature and on the Common Customs Tariff(4), or
- the GSP rate according to Article 2 of Council Regulation (EC) No 2820/98 of 21 December 1998 applying a multiannual scheme of generalised tariff preferences for the period 1 July 1999 to 31 December 2001(5),
whichever is the lower. However, the expression "duty effectively applied" shall not be taken to refer to a duty set up within the framework of a tariff quota under Article 26 of the Treaty or under Annex 7 of Regulation (EC) No 2658/87.
2. For the purposes of the Annex to this Regulation, the term "MFN" shall be taken to mean the lowest rate of duty appearing in column 3 or 4, taking into account the periods of application mentioned or referred to in that column, of the second part of Annex I of Regulation (EEC) No 2658/87.
3. Subject to paragraph 4, the final rate of preferential duty calculated in accordance with this Regulation shall be rounded down to the first decimal place.
4. Where the result of calculating the rate of preferential duty in application of paragraph 3 is one of the following, the preferential rate shall be considered a full exemption:
- 1 % or less in the case of ad valorem duties, or
- EUR 0,5 or less per individual euro amount in the case of specific duties.
Article 2
1. The customs duties on the products listed in the Annex, originating in the Republic of South Africa, shall be reduced to the levels provided in the Annex and within the limits of the tariff quotas specified in that Annex, without prejudice to Article 8.
2. These tariff quotas shall be managed in accordance with Articles 308a to 308c of Commission Regulation (EEC) No 2454/93.
3. The reductions of tariff quota duty referred to in the Annex are expressed as a percentage of the customs duties effectively applied to South African goods, as defined in Article 1(1) on the day of provisional entry into force of the Agreement.
Article 3
The Commission shall open an annual duty-free tariff quota for cheese and curd of CN codes 0406 10 20, 0406 10 80, 0406 20 90, 0406 30 10, 0406 30 31, 0406 30 39, 0406 30 90, 0406 40 90, 0406 90 01, 0406 90 21, 0406 90 50, 0406 90 69, 0406 90 78, 0406 90 86, 0406 90 87, 0406 90 88, 0406 90 93 and 0406 90 99 originating in the Republic of South Africa. The initial annual volume of this tariff quota shall be 5000 tonnes. An annual growth factor of 5 % shall apply to this volume. The resulting figure shall be rounded up to the next complete unit.
Article 4
After the first year, the tariff quotas referred to in Article 2 shall be increased each year by the percentage specified as the annual growth factor in the Annex. The resulting figure shall be rounded up to the next complete unit.
Article 5
Without prejudice to Articles 2 to 4, the amendments and technical adaptations of this Regulation made necessary by amendments to the Combined Nomenclature and Taric codes or arising from the conclusion of agreements, protocols or exchanges of letters between the Community and the Republic of South Africa, shall be adopted by the Commission in accordance with the management procedure set out in Article 6(2).
Article 6
1. The Commission shall be assisted by the Customs Code Committee, hereinafter referred to as "the Committee".
2. Where reference is made to this paragraph, Articles 4 and 7 of Decision 1999/468/EC shall apply.
The period laid down in Article 4(3) of Decision 1999/468/EC shall be set at three months.
3. The Committee shall adopt its rules of procedure.
Article 7
1. Products put into free circulation with the benefit of the preferential rates provided under the Agreement, other than those covered by Article 2, shall be subject to surveillance. The Commission in consultation with the Member States shall decide the products to which this surveillance applies.
2. Article 308d of Regulation (EEC) No 2454/93 shall apply.
3. The Member States and the Commission shall cooperate closely to ensure that this measure is complied with.
Article 8
The tariff quota referred to at order number 09.1825 in the Annex shall be opened for the first time from the entry into force of the EC/RSA agreement on wines and spirits.
Article 9
This Regulation shall enter into force on the day following that of its publication in the Official Journal of the European Communities.
It shall apply from the date of entry into force of the Agreement(6).
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 17 December 1999.
For the Council
The President
K. HEMILÄ
(1) OJ L 311, 4.12.1999, p. 1.
(2) OJ L 253, 11.10.1993, p. 1. Regulation last amended by Regulation (EC) No 502/1999 (OJ L 65, 12.3.1999, p. 1).
(3) OJ L 184, 17.7.1999, p. 23.
(4) OJ L 256, 7.9.1987, p. 1. Regulation last amended by Regulation (EC) No 2261/98 (OJ L 292, 30.10.1998, p. 1).
(5) OJ L 357, 30.12.1998, p. 1.
(6) The date of entry into force of the Agreement will be published in the Official Journal of the European Communities by the General Secretariat of the Council.
ANNEX
PRODUCTS REFERRED TO IN ARTICLE 2
Notwithstanding the rules for the interpretation of the combined nomenclature, the wording for the description of the products is to be considered as having no more than an indicative value, the preferential scheme being determined, within the context of this Annex, by the coverage of the CN codes as they exist at the time of adoption of the current regulation. Where ex CN codes are indicated, the preferential scheme is to be determined by application of the CN code and corresponding description taken together.
>TABLE>
