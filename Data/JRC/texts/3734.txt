Removal from the register of Case C-160/05 [1]
(2005/C 330/37)
Language of the case: French
By order of 4 October 2005, the President of the Court of Justice of the European Communities has ordered the removal from the register of Case C-160/05: Commission of the European Communities
v French Republic.
[1] OJ C 115, 14.5.2005.
--------------------------------------------------
