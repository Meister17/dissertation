COMMISSION REGULATION (EEC) N° 3994/87
of 23 December 1987
amending Regulation N° 136/66/EEC on the common organization of the market in oils
and fats
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) N° 2658/87 of 23 July 1987 on the tariff and statistical nomenclature and on the Common Customs Tariff (1), as amended by Regulation (EEC) N° 3985/87 (2), and in particular Article 15 thereof,
Whereas Council Regulation (EEC) N° 2658/87 establishes, with effect from 1 January 1988, a combined goods nomenclature based on the Harmonized System which will meet the requirements both of the Common Customs Tariff and the nomenclature of goods for the external trade statistics of the Community;
Whereas, as a consequence, it is necessary to express the descriptions of goods and tariff heading numbers which appear in Council Regulation N° 136/66/EEC (3), as last amended by Regulation (EEC) N° 1915/87 (4), according to the terms of the combined nomenclature; whereas these adaptations do not call for any amendment of substance;
HAS ADOPTED THIS REGULATION:
Article 1
Regulation N° 136/66/EEC is modified as follows:
1.
Article 1 (2) is replaced by the following:
'2. This Regulation shall cover the following products:
>TABLE>
31. 12. 87
Official Journal of the European Communities
2.
Article 2 (1) is replaced by the following:
'1. For the products referred to in Article 1 (2) (a), (b) and (d), with the exception of those falling within subheading 0709 90 39 and 0711 20 90, and for the products falling within subheading 2306 90 11, the rates of duty shown in the combined nomenclature shall be applied.
For the products referred to in Article 1 (2) (c) and (e), with the exception of those falling within subheading 2306 90 11, and for the products falling within subheadings 0709 90 39 and 0711 20 90, a system of levies on imports form third countries shall be applied.'
3.
Article 14 (1) is replaced by the following:
'1. Where untreated olive oil falling within subheadings 1509 10 and 1510 00 10 is imported from third countries and where the threshold price is higher than the cif price, a levy equal to the difference between these two prices shall be charged.'
4.
Article 15 (1) and (2) are replaced by the following, respectively:
'1. A levy shall be charged on imports from third countries of olive oil falling within subheadings 1509 90 00 and 1510 00 90; this levy shall be composed of a variable component corresponding to the levy applicable to the quantity of olive oil needed for production of the imported oil, which may be fixed at a standard rate, and a fixed component to protect the processing industry.
2. Where, for one or more origins, the offer prices on the world market for olive oil falling within subheadings 1509 90 00 and 1510 00 90 are not in line with the cif price referred to in Article 14, the latter shall be replaced for the calculation of the variable component of the levy by a price determined on the basis of the abovementioned offer prices.'
5.
Article 17 (1) and (2) are replaced by the following, respectively:
'1. On imports from third countries of olives falling within subheadings 0709 90 39 and 0711 20 90, a levy derived from the levy applicable to olive oil pursuant to Article 14 and based on the oil content of the imported products shall be charged.
However, the levy charged may not be less than an amount equal to 8 % of the value of the imported product, such amount to be fixed at a standard rate.
On imports from third countries of products falling within subheadings 2306 90 19, 1522 00 31 and 1522 00 39, a levy derived from the levy applicable to olive oil and based on the oil content of the imported product shall be charged.'
Article 2
This Regulation shall enter into force on 1 January 1988.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 23 December 1987.
For the Commission
Frans ANDRIESSEN
Vice-President
SPA:L377UMBE11.96
FF: 5UEN; SETUP: 01; Hoehe: 1001 mm; 334 Zeilen; 9616 Zeichen;
Bediener: JUTT Pr.: B;
Kunde: 40800 England 11
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 23 December 1987.
For the Commission
Frans ANDRIESSEN
Vice-President
SPA:L377UMBE11.96
FF: 5UEN; SETUP: 01; Hoehe: 1001 mm; 334 Zeilen; 9616 Zeichen;
Bediener: JUTT Pr.: B;
Kunde: 40800 England 11
