Council Decision
of 22 December 2004
appointing a Spanish alternate member of the Committee of the Regions
(2005/18/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 263 thereof,
Having regard to the proposal from the Spanish Government,
Whereas:
(1) On 22 January 2002 the Council adopted a Decision 2002/60/EC appointing the members and alternate members of the Committee of the Regions [1].
(2) A seat as an alternate member of the Committee of the Regions has fallen vacant following the ending of the mandate of Mr Alain CUENCA GARCÍA, of which the Council was notified on 29 November 2004,
HAS DECIDED AS FOLLOWS:
Sole Article
Mr Juan Carlos MARTÍN MALLÉN, Director General de Asuntos Europeos y Acción Exterior — Diputación General de Aragón, is hereby appointed an alternate member of the Committee of the Regions in place of Mr Alain CUENCA GARCÍA for the remainder of his term of office, ending on 25 January 2006.
Done at Brussels, 22 December 2004.
For the Council
The President
C. Veerman
--------------------------------------------------
[1] OJ L 24, 26.1.2002, p. 38.
