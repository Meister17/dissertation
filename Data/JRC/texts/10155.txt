Judgment of the Court
(Sixth Chamber)
of 17 November 2005
in Case C-476/04: Commission of the European Communities v Hellenic Republic [1]
In Case C-476/04 Commission of the European Communities (Agents: M. Kontoy and C. O'Reilly) v Hellenic Republic (Agent: N. Dafniou) — action under Article 226 EC for failure to fulfil obligations, brought on 12 November 2004 — the Court (Sixth Chamber), composed of A. Borg Barthet (Rapporteur), acting as President of the Sixth Chamber, U. Lõhmus and A. Ó Caoimh, Judges; D. Ruiz-Jarabo Colomer, Advocate General; R. Grass, Registrar, gave a judgment on 17 November 2005, in which it:
1. Declares that, by failing to adopt the laws, regulations and administrative provisions necessary to comply with Council Directive 2001/55/EC of 20 July 2001 on minimum standards for giving temporary protection in the event of a mass influx of displaced persons and on measures promoting a balance of efforts between Member States in receiving such persons and bearing the consequences thereof, the Hellenic Republic has failed to fulfil its obligations under that directive;
2. Orders the Hellenic Republic to pay the costs.
[1] OJ C 6 of 8.1.2005.
--------------------------------------------------
