COUNCIL REGULATION (EC) No 2840/98 of 21 December 1998 amending Regulation (EC) No 1734/94 on financial and technical cooperation with the Occupied Territories
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 130w thereof,
Having regard to the proposal from the Commission (1),
Acting in accordance with the procedure referred to in Article 189c of the Treaty (2),
Whereas Council Regulation (EC) No 1734/94 of 11 July 1994 on financial and technical cooperation with the Occupied Territories (3) lays down detailed arrangements and rules for administering the Community programme for aid and assistance to the Palestinian population in the West Bank and Gaza Strip;
Whereas, according to that Regulation, the Community is to implement financial and technical cooperation with the West Bank and Gaza Strip under a five-year programme; whereas this programme expires at the end of 1998;
Whereas the persistent deadlock in the peace process that prevails is the worst crisis since the Middle East peace process was launched in 1991; whereas international economic assistance has nevertheless succeeded in maintaining the peace process alive and providing support for the Palestinian Authority;
Whereas the objective is to prevent any further deterioration of the Palestinian economy by minimising and cancelling out the effects of closures and other obstacles to development as well as to contribute to a sound management and fiscal balance of the Palestinian Authority and to consolidate it by means of institutional reinforcement;
Whereas the ultimate aim is the achievement of sustainable economic and social development and to foster democracy, human rights and the development of civil society;
Whereas, given the present situation, the Community should pursue its aid effort; whereas to this end a five-year period of assistance (1999 to 2003) should be launched and Regulation (EC) No 1734/94 amended accordingly; whereas this Regulation should be reviewed by the Council within two years and not later than 31 December 2000 in order to take into account recent development and bring it into line with the reviewed Council Regulation (EC) No 1488/96 of 23 July 1996 on financial and technical measures to accompany (MEDA) the reform of economic and social structures in the framework of the Euro-Mediterranean partnership (4);
Whereas 'the Occupied Territories` should be replaced by 'the West Bank and the Gaza Strip` throughout the text of Regulation (EC) No 1734/94;
Whereas Article 4 of Regulation (EC) No 1734/94 provides that all financing decisions on projects and operations are to be adopted in accordance with the procedure laid down in Article 5 thereof; whereas, in order to allow for rapid and flexible reactions and to increase expediency, only financing decisions exceeding ECU 2 000 000 other than those relating to interest-rate subsidies on Bank loans should be adopted in accordance with that procedure;
Whereas Council Decision 97/256/EC of 14 April 1997 granting a Community guarantee to the European Investment Bank against losses under loans for projects outside the Community (Central and Eastern Europe countries, Mediterranean countries, Latin America and Asian countries and South Africa) (5) covers guarantees also in this region for the period to the year 2000,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 1734/94 is hereby amended as follows:
1. the title shall be replaced by the following: 'Council Regulation (EC) No 1734/94 of 11 July 1994 on financial and technical cooperation with the West Bank and the Gaza Strip`;
2. Article 1 shall be replaced by the following:
'Article 1
1. The Community shall implement financial and technical cooperation with the West Bank and the Gaza Strip under a five-year period (1999 to 2003) with the aim of aiding their achievement of sustainable economic, political and social development. Should new financial perspectives be adopted for the period after 2000, the amount of this cooperation shall be determined in accordance with the financial perspectives and conditional upon the budgetary authority deciding the amount in the annual budgetary procedure.
2. Within two years and not later than 31 December 2000, the Council shall review this Regulation on the basis of an independent evaluation of the programmes as envisaged in Article 6. This review shall also take into account recent developments in the area and might also consider bringing the Regulation into line with Council Regulation (EC) No 1488/96 of 23 July 1996 on financial and technical measures to accompany (MEDA) the reform of economic and social structures in the framework of the Euro-Mediterranean partnership (*).
(*) OJ L 189, 30.7.1996, p. 1. Regulation as amended by Regulation (EC) No 780/98 (OJ L 113, 15.4.1998, p. 3).`
3. Article 2 shall be amended as follows:
(a) the words 'and the development of civil society` shall be added after 'human rights` at the end of paragraph 1;
(b) the following subparagraph shall be added to paragraph 3:
'The said projects and operations shall aim inter alia at the promotion of employment and the creation of employment by improving the social services and combating poverty`.
(c) in paragraphs 5 and 6 'the Occupied Territories` shall be replaced by 'the West Bank and the Gaza Strip`.
4. in Article 3 'the Occupied Territories` shall be replaced by 'the West Bank and the Gaza Strip`.
5. Article 4(1) to (3) shall be replaced by the following:
'Article 4
1. Financing decisions on projects and operations under this Regulation exceeding ECU 2 000 000 other than those relating to interest-rate subsidies on bank loans shall be adopted in accordance with the procedure laid down in Article 5.
2. Financing decisions on overall allocations for technical cooperation, training and trade promotion shall be adopted in accordance with the procedure laid down in Article 5. Within an overall allocation, the Commission shall adopt financing decisions not exceeding ECU 2 000 000.
The Committee referred to in Article 5 shall be informed systematically and promptly, and in any event before the next meeting, of financing decisions for measures not involving more than ECU 2 000 000.
3. Decisions amending financing decisions adopted in accordance with the procedure laid down in Article 5 shall be taken by the Commission where they do not entail any substantial amendments or additional commitments in excess of 20 % of the original commitment. The Commission shall inform the Committee referred to in Article 5 immediately of any such decisions.`
6. Article 5(1) shall be replaced by the following:
'1. The Commission shall be assisted by the MED Committee set up pursuant to Article 11 of Regulation (EC) No 1488/96.`;
7. Article 6(1) shall be replaced by the following:
'1. The Commission shall examine the state of implementation of cooperation under this Regulation and shall report annually in writing to the European Parliament and the Council.`
Article 2
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Luxembourg, 21 December 1998.
For the Council
The President
M. BARTENSTEIN
(1) OJ C 253, 12. 8. 1998, p. 15.
(2) Opinion of the European Parliament of 16 September 1998 (OJ C 313, 12. 10. 1998), Council Common Position of 13 October 1998 (OJ C 388, 14. 12. 1998) and Decision of the European Parliament of 3 December 1998 (OJ C 398, 21. 12. 1998).
(3) OJ L 182, 16. 7. 1994, p. 4.
(4) OJ L 189, 30. 7. 1996, p. 1. Regulation as amended by Regulation (EC) No 780/98 (OJ L 113, 15. 4. 1998, p. 3).
(5) OJ L 102, 19. 4. 1997, p. 33. Decision as amended by Decision No 98/348/EC (OJ L 155, 29. 5. 1998, p. 53).
