*****
COUNCIL DIRECTIVE
of 27 July 1990
amending Annex II to Directive 86/280/EEC on limit values and quality objectives for discharges of certain dangerous substances included in list I of the Annex to Directive 76/464/EEC
(90/415/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 130s thereof,
Having regard to Council Directive 76/464/EEC of 4 May 1976 on pollution caused by certain dangerous substances discharged into the aquatic environment of the Community (1), and in particular Articles 6 and 12 thereof,
Having regard to the proposal from the Commission (2),
Having regard to the opinion of the European Parliament (3),
Having regard to the opinion of the Economic and Social Committee (4),
Whereas, in order to protect the aquatic environment of the Community against pollution by certain dangerous substances, Article 3 of Directive 76/464/EEC introduces a system of prior authorization laying down emission standards for discharges of the substances in List I in the Annex thereto; whereas Article 6 of the said Directive provides that limit values shall be laid down for such emission standards and quality objectives for the aquatic environment affected by discharges of the substances;
Whereas Member States are required to apply the limit values except in cases where they may employ quality objectives;
Whereas Directive 86/280/EEC (5), as amended by Directive 88/347/EEC (6), will have to be amended and supplemented, on proposals from the Commission, in line with developments in scientific knowledge relating principally to the toxicity, persistence and accumulation of the substances referred to in living oganisms and sediments, or in the event of an improvement in the best technical means available; whereas it is necessary, for that purpose, to provide for additions to the said Directive, relating to measures in respect of other dangerous substances, and for amendments to the content of Annex II;
Whereas Article 5 of Directive 86/280/EEC provides that, in the case of certain significant sources of pollution by these substances other than sources subject to Community limit values or national emission standards, specific programmes should be devised to eliminate the pollution;
Whereas it is appropriate that small discharges which are subject to the provisions of Article 5 of Directive 86/280/EEC may be exempted from the requirements of Article 3 of Directive 76/464/EEC;
Wheras, on the basis of the criteria laid down in Directive 76/464/EEC, 1-2-dichloroethane, trichloroethylene, perchloroethylene and trichlorobenzene should be made subject to the provisions of Directive 86/280/EEC,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Annex II to Directive 86/280/EEC is hereby amended as set out in the Annex hereto.
Article 2
1. Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive within 18 months of its notification (1). They shall forthwith inform the Commission thereof.
2. Member States shall communicate to the Commission the texts of the main provisions of national law which they adopt in the field governed by this Directive.
Article 3
This Directive is addressed to the Member States.
Done at Brussels, 27 July 1990.
For the Council
The President
E. RUBBI
(1) OJ No L 129, 18. 5. 1976, p. 23.
(2) OJ No C 253, 29. 9. 1988, p. 4.
(3) OJ No C 96, 17. 4. 1989, p. 188.
(4) OJ No C 23, 30. 1. 1989, p. 4.
(5) OJ No L 181, 4. 7. 1986, p. 16.
(6) OJ No L 158, 25. 6. 1988, p. 35.
(1) This Directive was notified to the Member States on 31 July 1990.
ANNEX
AMENDMENTS TO ANNEX II TO DIRECTIVE 86/280/EEC
1. The following is added below the title:
' 8. Relating to 1,2-dichloroethane (EDC)
9. Relating to trichloroethylene (TRI)
10. Relating to perchloroethylene (PER)
11. Relating to trichlorobenzene (TCB)'.
2. The following section is added:
'VIII.: Specific provisions relating to 1,2-dichloroethane (EDC) (No 59) (*)
CAS - No 107-06-2
(*) Article 5 of Directive 86/280/EEC applies in particular to EDC used as a solvent away from a production or processing site if annual discharges amount to less than 30 kg/year. Such small discharges may be exempted from the requirements of Article 3 of Directive 76/464/EEC. Notwithstanding Article 5 (3) of Directive 86/280/EEC, Member States must implement their specific programmes no later than 1 January 1993. They must communicate them to the Commission at the same time.
Heading A (59): Limit values for emission standards (1)
1.2.3,4.5 // // // // // Type of industrial plant (2) (3) // Type of average value // Limit values expressed as // To be complied with as from 1.2.3.4.5 // // // weight (g/tonne) (4) // concentration (mg/litre) (5) // // // // // // // (a) Production only of 1,2-dichloroethane (without processing or use on the same site) // Monthly Daily // 4 2,5 8 5 // 2 1,25 4 2,5 // 1. 1. 1993 1. 1. 1995 1. 1. 1993 1. 1. 1995 // // // // // // (b) Production of 1,2-dichloroethane, and processing or use at the same site, except for the use defined in (e) below (6) (7) // Monthly Daily // 12 5 24 10 // 6 2,5 12 5 // 1. 1. 1993 1. 1. 1995 1. 1. 1993 1. 1. 1995 // // // // // // (c) Processing of 1,2-dichloroethane into substances other than vinyl chloride (8) // Monthly Daily // 2,5 5 // 1 2 // 1. 1. 1993 1. 1. 1993 // // // // // // (d) Use of EDC for degreasing metals (away from an industrial site covered by (b)) (9) // Monthly Daily // - - // 0,1 0,2 // 1. 1. 1993 1. 1. 1993 // // // // // // (e) Use of EDC in the production of ion exchangers (10) // Monthly Daily // - - // - - // - - // // // // //
(1) In view of the volatility of EDC and in order to ensure compliance with Article 3 (6) of Directive 86/280/EEC, where the process used involves open-air agitation of the effluents containing EDC, Member States must require compliance with the limit values upstream of the plants concerned; they must ensure that all waters likely to be polluted are properly taken into account.
(2) The purified EDC production capacity includes that fraction of the EDC which is not cracked in the vinyl chloride (VC) production unit associated with the EDC production unit and which is recycled to the EDC purification section of the plant.
Production or processing capacity is the capacity authorized by the administration or, failing that, the highest annual quantity produced or processed over the four years prior to the granting or review of the authorization. The capacity authorized by the administration should not differ greatly from actual production.
(3) A simplified monitoring procedure may be introduced where annual discharges do not exceed 30 kg/year.
(4) These limit values relate:
- for sectors (a) and (b), to purified EDC production capacity expressed in tonnes,
- for sector (c), to EDC processing capacity expressed in tonnes.
However, in the case of sector (b), if the processing and utilization capacity is greater than the production capacity, the limit values shall be applied in relation to the global processing and utilization capacity. If there are several plants on the same site, the limit values shall apply to the plants taken together.
(5) Without prejudice to the provisions of heading A (4) in Annex I, these concentration limits relate to the following reference volumes:
(a) 2 m3/tonne of purified EDC production capacity;
(b) 2,5 m3/tonne of purified EDC production capacity;
(c) 2,5 m3/tonne of EDC processing capacity.
(6) The limit values take account of all diffuse internal sources and/or of EDC used as a solvent within the industrial production site; this will ensure a reduction in EDC discharges of more than 99 %.
Nevertheless, the combination of the best available technology and the absence of any diffuse internal source enables reduction amounts greater than 99,9 % to be achieved.
On the basis of the experience acquired in the application of the present measures, the Commission will present to the Council in good time proposals for more severe limit values to be applied from 1998.
(7) Where a Member State takes the view that, owing to the integration of EDC production with the manufacture of other chlorinated hydrocarbons, an EDC production process is unlikely to comply with these limit values by the 1 January 1993 deadline, it must advise the Commission thereof before 1 January 1991. A programme for the reduction of EDC discharges which will enable these limit values to be complied with by 1 January 1997 will be submitted to the Commission no later than 31 December 1993. The following limit value must, meanwhile, be complied with as at 1 January 1993:
- 40 g EDC/tonne of purified EDC production capacity (monthly and daily averages).
The limit value expressed as concentration is deduced on the basis of the volume of water discharged by the plant(s) concerned.
(8) The production of the following substances specifically is involved here: ethylene diamine, ethylene polyamine, 1.1.1.-trichloroethane, trichloroethylene and perchloroethylene.
(9) These limit values apply only to plants the annual discharges from which exceed 30 kg/year.
(10) It is not possible at present to adopt limit values for this sector. The Council shall adopt such limit values at a later stage, acting on a proposal from the Commission. In the meantime, Member States will apply national limit values in accordance with Annex I, heading A, point 3. Heading B (59): quality objectives
1.2.3 // // // // Environment // Quality objectives (m g/litre) // To be complied with as from // // // // Inland surface waters // // // Estuary waters // // // Inland coastal waters other than estuary waters // 10 // 1. 1. 1993 // Territorial waters // // // // //
The Commission will compare the results of the monitoring carried out, in accordance with the third indent of Article 13 (1) of Directive 76/464/EEC, with an indicative concentration of 2,5 mg/litre.
The Commission will, by 1998, re-examine the quality objectives on the basis of the experience acquired in the application of the present measures.
Heading C (59): reference method of measurement
1. The reference method of measurement to be used for determining the presence of 1,2-dichloroethane in effluents and the water environment is gas chromatography with electron capture detection after extraction by means of an appropriate solvent or gas chromatography following isolation by means of the "purge and trap" process and trapping by using a cryogenically cooled capillary trap. The limit of determination is 10 mg/litre for effluents and 1 mg/litre for the water environment.
2. The accuracy and precision of the method must be plus or minus 50 % at a concentration which represents twice the value of the limit of determination.
3. Member States may determine concentrations of EDC by reference to the quantity of AOX, EOX or VOX, provided that the Commission is first satisfied that these methods give equivalent results and until the general solvent Directive is adopted.
The Member States concerned will establish regularly the relationship in concentration between EDC and the parameter used.
IX. Specific provisions relating to trichloroethylene (TRI) (No 121) (*)
CAS 79.01.6
(*) Article 5 of Directive 86/280/EEC applies in particular to TRI used as a solvent for dry-cleaning, for the extraction of grease or odours and for degreasing metals where annual discharges amount to less than 30 kg/year. Such small discharges may be exempted from the requirements of Article 3 of Directive 76/464/EEC. Notwithstanding Article 5 (3) of Directive 86/280/EEC, Member States must implement their specific programmes no later than 1 January 1993. They must communicate them to the Commission at the same time.
Heading A (121): limit values for emission standards (1)
1.2.3,4.5 // // // // // Type of industrial plants (2) // Type of average value // Limit values expressed as // To be complied with as from 1.2.3.4.5 // // // weight (g/tonne) (3) // concentracion (mg/litre) (4) // // // // // // // (a) Trichloroethylene (TRI) and perchloroethylene (PER) production // Monthly Daily // 10 2,5 20 5 // 2 0,5 4 1 // 1. 1. 1993 1. 1. 1995 1. 1. 1993 1. 1. 1995 // // // // // // (b) Use of TRI for degreasing metals (5) // Monthly Daily // // 0,1 0,2 // 1. 1. 1993 1. 1. 1993 // // // // //
(1) In view of the volatility of trichloroethylene and in order to ensure compliance with Article 3 (6) of Directive 86/280/EEC, where the process used involves open-air agitation of the effluents containing tricholoroethylene, Member States must require compliance with the limit values upstream of the plants concerned; they must ensure that all waters likely to be polluted are properly taken into account. (2) A simplified monitoring procedure may be introduced where annual discharges do not exceed 30 kg/year.
(3) For sector (a), limit values for TRI discharges relate to overall TRI + PER production capacity.
For existing plant using dehydrochlorination of tetrachloroethane, the capacity of production is equivalent to the capacity of TRI-PER production, the ratio of TRI-PER production taken at one third.
Production or processing capacity is the capacity authorized by the administration or, failing that, the highest annual quantity produced or processed over the four years prior to the granting or review of the authorization. The capacity authorized by the administration should not differ greatly from actual production.
(4) Without prejudice to the provisions of heading A (4) in Annex I, TRI limit concentrations relate to the following reference values:
- sector (a), 5 m3/tonne of TRI + PER production.
(5) These limit values apply only to industrial plants the annual discharges from which exceed 30 kg/year.
Heading B (121): quality objectives
1.2.3 // // // // Environment // Quality objective (m g/litre) // To be complied with as from // // // // Inland surface waters // // // Estuary waters // // // Inland coastal waters other than estuary waters // 10 // 1. 1. 1993 // Territorial waters // // // // //
The Commission will compare the results of the monitoring carried out, in accordance with the third indent of Article 13 (1) of Directive 76/464/EEC, with an indicative concentration of 2,5 mg/litre.
The Commission will, by 1988, re-examine the quality objectives on the basis of the experience acquired in the application of the present measures.
Heading C (121): reference method of measurement
1. The reference method of measurement to be used for determining the presence of trichloroethylene (TRI) in effluents and the water environment is gas chromatography with electron capture detection after extraction by means of an appropriate solvent.
The limit of determination for TRI is 10 mg/litre for effluents and 0,1 mg/litre for the water environment.
2. The accuracy and precision of the method must be plus or minus 50 % at a concentration which represents twice the value of the limit of determination.
3. Member States may determine concentrations of TRI by reference to the quantity of AOX, EOX or VOX provided that the Commission is first satisfied that these methods give equivalent results and until the general solvent Directive is adopted.
The Member States concerned will establish regularly the relationship in concentration between TRI and the parameter used.
X. Specific provisions relating to perchloroethylene (PER) (No 111) (*)
CAS-127-18-4
(*) Article 5 of Directive 86/280/EEC applies in particular to PER used as a solvent for dry-cleaning, for the extraction of grease or odours and for degreasing metals where annual discharges amount to less than 30 kg/year. Such small discharges may be exempted from the requirements of Article 3 of Directive 76/464/EEC. Notwithstanding Article 5 (3) of Directive 86/280/EEC, Member States must implement their specific programmes no later than 1 January 1993. They must communicate them to the Commission at the same time.
Heading A (111): limit values for emission standards (1)
1.2.3,4.5 // // // // // Type of industrial plant (2) // Type of average value // Limit values expressed as // To be complied with as from 1.2.3.4.5 // // // weight (g/tonne) (3) // concentration (mg/litre) (4) // // // // // // // (a) Trichloroethylene (TRI) and perchloroethylene (PER) production (TRI-PER processes) // Monthly Daily // 10 2,5 20 5 // 2 0,5 4 1 // 1. 1. 1993 1. 1. 1995 1. 1. 1993 1. 1. 1995 // // // // // // (b) Carbon tetrachloride and perchloroethylene production (TETRA-PER processes) // Monthly Daily // 10 2,5 20 5 // 5 1,25 10 2,5 // 1. 1. 1993 1. 1. 1995 1. 1. 1993 1. 1. 1995 // // // // // // (c) Use of PER for degreasing metals (5) // Monthly Daily // - - // 0,1 0,2 // 1. 1. 1993 1. 1. 1993 // // // // // // (d) Chlorofluorocarbon production (6) // Monthly Daily // - - // - - // - - // // // // //
(1) In view of the volatility of perchloroethylene and in order to ensure compliance with Article 3 (6) of Directive 86/280/EEC, where the process used involves open-air agitation of the effluents containing perchloroethylene, the Member States must require compliance with the limit values upstream of the plants concerned; they must ensure that all waters likely to be polluted are properly taken into account.
(2) A simplified monitoring procedure may be introduced where annual discharges do not exceed 30 kg/year.
(3) For sectors (a) and (b) the limit values for PER discharges relate either to overall TRI + PER production capacity or to overall TETRA + PER production capacity.
Production or processing capacity is the capacity authorized by the administration or, failing that, the highest annual quantity produced or processed over the four years prior to the granting or review of the authorization. The capacity authorized by the administration should not differ greatly from actual production.
(4) Without prejudice to the provisions of heading A (4) in Annex I, PER limit concentrations relate to the following reference volumes:
- (a), 5 m3/tonne of TRI + PER production,
- (b), 2 m3/tonne of TETRA + PER production.
(5) These limit values apply only to industrial plants the annual discharges from which exceed 30 kg/year.
(6) It is not possible at present to adopt limit values for this sector. The Council shall adopt them at a later stage, acting on a proposal from the Commission. In the meantime, Member States will apply national emission standards in accordance with Annex I, heading A, point 3.
Heading B (111): quality objectives
1.2.3 // // // // Environment // Quality objective (m g/litre) // To be complied with as from // // // // Inland surface waters // // // Estuary waters // // // Inland coastal waters other than estuary waters // 10 // 1. 1. 1993 // Territorial waters // // // // //
The Commission will compare the results of the monitoring carried out, in accordance with the third indent of Article 13 (1) of Directive 76/464/EEC, with an indicative concentration of 2,5 mg/litre.
The Commission will, by 1998, re-examine the quality objectives on the basis of the experience acquired in the application of the present measures.
Heading C (111): reference method of measurement
1. The reference method of measurement to be used for determining the presence of perchloroethylene (PER) in effluents and the water environment is gas chromatography with electron capture detection after extraction by means of an appropriate solvent.
The limit of determination for PER is 10 mg/litre for effluents and 0,1 mg/litre for the water environment.
2. The accuracy and precision of the method must be plus or minus 50 % at a concentration which represents twice the value of the limit of determination.
3. Member States may determine concentrations of PER by reference to the quantity of AOX, EOX or VOX, provided that the Commission is first satisfied that these methods give equivalent results and until the general solvent Directive is adopted.
The Member States concerned will establish regularly the relationship in concentration between PER and the parameter used.
XI. Specific provisions relating to trichlorobenzene (*) (TCB) (117, 118) (**)
(*) Article 5 of Directive 86/280/EEC applies in particular to TCB used as a solvent or colouring support in the textile industry, or as a component of the oils used in transformers until such time as there is specific Community legislation on this subject. Notwithstanding Article 5 (3), Member States must implement their specific programmes no later than 1 January 1993. They must communicate them to the Commission at the same time.
(**) TCB may occur as one of the following three isomers:
- 1, 2, 3-TCB - CAS 87/61-6;
- 1, 2, 4-TCB - CAS 120-82-1 (No 118 of the EEC list);
- 1, 3, 5-TCB - CAS 180-70-3.
Technical TCB (No 117 of the EEC list) is a mixture of these three isomers, with a preponderance of 1,2,4-TCB, and may also contain small quantities of di-and tetrachlorobenzene.
In any case, these provisions apply to the total TCB (the sum of the three isomers).
Heading A (117, 118): limit values for emission standards
Standstill: There must be no significant direct or indirect increase over time in pollution arising from discharges of TCB and affecting concentrations in sediments and/or molluscs and/or shellfish and/or fish.
1.2.3,4.5 // // // // // Type of industrial plant // Type of average value // Limit values expressed as // To be complied with as from 1.2.3.4.5 // // // weight (g/tonne) (1) // concentration (mg/litre) (2) // // // // // // // (a) Poduction of TCB via dehydrochlorination of HCH and/or processing TCB // Monthly Daily // 25 10 50 20 // 2,5 1 5 2 // 1.1.1993 1.1.1995 1.1.1993 1.1.1995 // // // // // // (b) Production and/or processing of chlorobenzenes via chlorination of benzene (3) // Monthly Daily // 5 0,5 10 1 // 0,5 0,05 1 0,1 // 1.1.1993 1.1.1995 1.1.1993 1.1.1995 // // // // //
(1) The limit values for discharges of TCB (sum of the three isomers) are given:
- for sector (a): in relation to the total TCB production capacity,
- for sector (b): in relation to the total production or processing capacity for mono- and dichlorobenzenes. Production or processing capacity is the capacity authorized by the administration or, failing that, the highest annual quantity produced or processed over the four years prior to the granting or review of the authorization. The capacity authorized by the administration should not differ greatly from actual production.
(2) Without prejudice to the provisions of heading A (4) in Annex I, limit concentrations relate to the following reference volumes:
- sector (a): 10 m3/tonne of TCB produced or processed,
- sector (b): 10 m3/tonne of mono- and dichlorobenzene produced or processed.
(3) For the existing plants discharging less than 50 kg/year by 1 January 1995, the limit values which are to be complied with at this date are equal to half of the limit values which are to be complied with as from 1 January 1993.
Heading B (117, 118): quality objectives
Standstill: There must be no significant increase over time in the concentration of TCB in sediments and/or molluscs and/or shellfish and/or fish.
1.2.3 // // // // Environment // Quality objective (m g/litre) // To be complied with as from // // // // Inland surface waters // // // Estuary waters // // // Inland coastal waters other than estuary waters // 0,4 // 1. 1. 1993 // Territorial waters // // // // //
The Commission will compare the results of the monitoring carried out, in accordance with the third indent of Article 13 (1) of Directive 76/464/EEC, with an indicative concentration of 0,1 mg/litre.
The Commission will, by 1998, re-examine the quality objectives on the basis of the experience acquired in the application of the present measures.
Heading C (117, 118): reference method of measurement
1. The reference method of measurement to be used for determining the presence of trichlorobenzene (TCB) in effluents and the water environment is gas chromatography with electron capture detection after extraction by means of an appropriate solvent. The limit of determination for each esomer separately is 1 mg/litre for effluents and 10 ng/litre for the water environment.
2. The reference method to be used for determining TCB in sediments and organisms is gas chromatography with electron capture detection after appropriate preparation of the sample. The limit of determination for each isomer separately is 1 m g/kg of dry matter.
3. Member States may determine concentrations of TCB by reference to the quantity of AOX or EOX, provided that the Commission is first satisfied that these methods give equivalent results and until the general solvent Directive is adopted.
The Member States concerned will establish regularly the relationship in concentration between TCB and the parameter used.
4. The accuracy and precision of the method must be plus or minus 50 % at a concentration which represents twice the value of the limit of determination.sh.
Environment Quality objective
(mg/litre) To be complied
with as from Inland surface waters Estuary waters Inland coastal waters other than estuary waters 0,4 1. 1. 1993 Territorial waters
The Commission will compare the results of the monitoring carried out, in accordance with the third indent of Article 13 (1) of Directive 76/464/EEC, with an indicative concentration of 0,1 mg/litre.
The Commission will, by 1998, re-examine the quality objectives on the basis of the experience acquired in the application of the present measures.
Heading C (117, 118): reference method of measurement
1. The reference method of measurement to be used for determining the presence of trichlorobenzene (TCB) in effluents and the water environment is gas chromatography with electron capture detection after extraction by means of an appropriate solvent. The limit of determination for each esomer separately is 1 mg/litre for effluents and 10 ng/litre for the water environment.
2. The reference method to be used for determining TCB in sediments and organisms is gas chromatography with electron capture detection after appropriate preparation of the sample. The limit of determination for each isomer separately is 1 mg/kg of dry matter.
3. Member States may determine concentrations of TCB by reference to the quantity of AOX or EOX, provided that the Commission is first satisfied that these methods give equivalent results and until the general solvent Directive is adopted.
The Member States concerned will establish regularly the relationship in concentration between TCB and the parameter used.
4. The accuracy and precision of the method must be plus or minus 50 % at a concentration which represents twice the value of the limit of determination.'
