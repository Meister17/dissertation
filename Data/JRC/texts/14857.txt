Order of the Civil Service Tribunal (Second Chamber) of 13 July 2006 — E
v Commission
(Case F-5/06) [1]
Parties
Applicant: E (London, United Kingdom) (represented by: S. Rodrigues and Y. Minatchy, lawyers)
Defendant: Commission of the European Communities (represented by: J. Currall and V. Joris, Agents)
Re:
Annulment of the Appointing Authority's decision of 4 October 2005 dismissing the applicant's complaint concerning the checking of the lawfulness of disciplinary proceedings and of a procedure for recognition of the applicant's occupational disease, and a claim for damages.
Operative part of the order
1. The action is dismissed as clearly inadmissible.
2. The parties shall each bear their own costs.
[1] OJ C 74, 25.3.2006.
--------------------------------------------------
