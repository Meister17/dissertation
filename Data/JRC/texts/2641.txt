Commission Regulation (EC) No 1291/2005
of 5 August 2005
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 6 August 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 5 August 2005.
For the Commission
J. M. Silva Rodríguez
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 1947/2002 (OJ L 299, 1.11.2002, p. 17).
--------------------------------------------------
ANNEX
to Commission Regulation of 5 August 2005 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code | Standard import value |
07020000 | 052 | 44,5 |
096 | 41,1 |
999 | 42,8 |
07070005 | 052 | 75,8 |
096 | 39,7 |
999 | 57,8 |
07099070 | 052 | 77,2 |
999 | 77,2 |
08055010 | 382 | 67,4 |
388 | 69,4 |
524 | 60,9 |
528 | 62,0 |
999 | 64,9 |
08061010 | 052 | 103,9 |
204 | 57,3 |
220 | 128,8 |
624 | 155,1 |
999 | 111,3 |
08081080 | 388 | 79,5 |
400 | 66,7 |
508 | 68,0 |
512 | 64,7 |
528 | 77,2 |
720 | 67,2 |
804 | 72,4 |
999 | 70,8 |
08082050 | 052 | 110,0 |
388 | 56,9 |
512 | 18,8 |
528 | 53,2 |
800 | 50,6 |
999 | 57,9 |
08092095 | 052 | 303,5 |
400 | 327,9 |
404 | 318,7 |
999 | 316,7 |
08093010, 08093090 | 052 | 113,1 |
999 | 113,1 |
08094005 | 094 | 49,8 |
624 | 63,6 |
999 | 56,7 |
--------------------------------------------------
