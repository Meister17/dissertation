Action brought on 16 May 2006 — Commission of the European Communities v Kingdom of Spain
Parties
Applicant: Commission of the European Communities (represented by: G. Braun and J.R. Vidal Puig, acting as Agents)
Defendant: Kingdom of Spain
Form of order sought
- Declare that the Kingdom of Spain, by failing to adopt the laws, regulations and administrative provisions necessary to comply with Commission Directive 2004/72/EC [1] of 29 April 2004 implementing Directive 2003/6/EC [2] of the European Parliament and of the Council as regards accepted market practices, the definition of inside information in relation to derivatives on commodities, the drawing up of lists of insiders, the notification of managers' transactions and the notification of suspicious transactions and, in any event, by failing to inform the Commission thereof, has failed to fulfil its obligations under that directive;
- order the Kingdom of Spain to pay the costs.
Pleas in law and main arguments
The time-limit prescribed for the implementation in national law of Directive 2004/72/EC expired on 12 October 2004.
[1] OJ L 162, 30.4.2004, p. 70.
[2] OJ L 96, 12.4.2003, p. 16.
--------------------------------------------------
