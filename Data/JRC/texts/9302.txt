Council Directive 2006/18/EC
of 14 February 2006
amending Directive 77/388/EEC with regard to reduced rates of value added tax
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 93 thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament [1],
Having regard to the opinion of the European Economic and Social Committee [2],
Whereas:
(1) The possibility of applying a reduced rate of value added tax should be granted in respect of supplies of district heating as for supplies of natural gas and electricity, for which the possibility of applying a reduced rate is already allowed in Sixth Council Directive 77/388/EEC of 17 May 1977 on the harmonisation of the laws of the Member States relating to turnover taxes — Common system of value added tax: uniform basis of assessment [3].
(2) To achieve a better understanding of the impact of reduced rates, it is necessary for the Commission to prepare an assessment report on the impact of reduced rates applied to locally supplied services, notably in terms of job creation, economic growth and the proper functioning of the internal market.
(3) The experiment of reduced rates for labour-intensive services should therefore be extended until 31 December 2010 and it should also be made possible for all Member States to take part in it under the same conditions.
(4) Accordingly, Member States wishing to avail themselves, for the first time, of the option provided for in Article 28(6) of Directive 77/388/EEC and those wishing to amend the list of services to which they have applied the said provision in the past should submit a request to the Commission, together with the relevant particulars for the purpose of assessment. Such prior assessment by the Commission does not appear necessary where Member States have previously benefited from an authorisation and submitted a report on the matter to the Commission.
(5) To ensure legal continuity, this Directive should be applicable as from 1 January 2006.
(6) Implementation of this Directive in no way implies change in the legislative provisions of Member States,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Directive 77/388/EEC is hereby amended as follows:
1. Article 12 shall be amended as follows:
(a) paragraph 3(b) shall be replaced by the following:
"(b) Member States may apply a reduced rate to supplies of natural gas, electricity and district heating provided that no risk of distortion of competition exists. A Member State intending to apply such a rate must inform the Commission before doing so. The Commission shall give a decision on the existence of a risk of distortion of competition. If the Commission has not taken that decision within three months of the receipt of the information a risk of distortion of competition is deemed not to exist.";
(b) in paragraph 4, the following subparagraph shall be inserted:
"By 30 June 2007 at the latest the Commission shall present to the European Parliament and the Council an overall assessment report on the impact of reduced rates applying to locally supplied services, including restaurant services, notably in terms of job creation, economic growth and the proper functioning of the internal market, based on a study carried out by an independent economic think-tank.";
2. Article 28(6) shall be amended as follows:
(a) the first subparagraph shall be replaced by the following:
"The Council, acting unanimously on a proposal from the Commission, may authorise any Member State to apply until 31 December 2010 at the latest the reduced rates provided for in the third subparagraph of Article 12(3)(a) to services listed in a maximum of two of the categories set out in Annex K. In exceptional cases, a Member State may be authorised to apply the reduced rates to services belonging to three of the aforementioned categories.";
(b) the fourth subparagraph shall be replaced by the following:
"Any Member State wishing to apply for the first time after 31 December 2005 a reduced rate to one or more of the services mentioned in the first subparagraph pursuant to this provision shall inform the Commission before 31 March 2006. It shall communicate to it before that date all relevant particulars concerning the new measures it wishes to introduce, and in particular the following:
(a) scope of the measure and detailed description of the services concerned;
(b) particulars showing that the conditions laid down in the second and third subparagraphs have been met;
(c) particulars showing the budgetary cost of the measure envisaged."
Article 2
This Directive shall enter into force on the day of its publication in the Official Journal of the European Union.
It shall be applicable as from 1 January 2006.
Article 3
This Directive is addressed to the Member States.
Done at Brussels, 14 February 2006.
For the Council
The President
K.-H. Grasser
[1] OJ C 89 E, 14.4.2004, p. 138.
[2] OJ C 32, 5.2.2004, p. 113.
[3] OJ L 145, 13.6.1977, p. 1. Directive as last amended by Directive 2005/92/EC (OJ L 345, 28.12.2005, p. 19).
--------------------------------------------------
