Judgment of the Court (First Chamber) of 28 September 2006 (reference for a preliminary ruling from the Audiencia Provincial de Málaga — Spain) — Criminal proceedings against G. Francesco Gasparini, José Ma L.A. Gasparini, G. Costa Bozzo, Juan de Lucchi Calcagno, Francesco Mario Gasparini, José A. Hormiga Marrero, Sindicatura Quiebra
(Case C-467/04) [1]
Referring court
Audiencia Provincial de Málaga
Parties in the main proceedings
G. Francesco Gasparini, José Ma L.A. Gasparini, G. Costa Bozzo, Juan de Lucchi Calcagno, Francesco Mario Gasparini, José A. Hormiga Marrero, Sindicatura Quiebra
Re:
Reference for a preliminary ruling — Audiencia Provincial de Málaga — Interpretation of Article 54 of the Convention implementing the Schengen Agreement of 14 June 1985 between the Governments of the States of the Benelux Economic Union, the Federal Republic of Germany and the French Republic on the gradual abolition of checks at their common borders (OJ 2000 L 239, p. 19) — Ne bis in idem principle — Scope — Interpretation of Article 24 EC — Scope
Operative part of the judgment
1. The ne bis in idem principle, enshrined in Article 54 of the Convention implementing the Schengen Agreement of 14 June 1985 between the Governments of the States of the Benelux Economic Union, the Federal Republic of Germany and the French Republic on the gradual abolition of checks at their common borders, signed in Schengen on 19 June 1990, applies in respect of a decision of a court of a Contracting State, made after criminal proceedings have been brought, by which the accused is acquitted finally because prosecution of the offence is time-barred.
2. That principle does not apply to persons other than those whose trial has been finally disposed of in a Contracting State.
3. A criminal court of a Contracting State cannot hold goods to be in free circulation in national territory solely because a criminal court of another Contracting State has found, in relation to the same goods, that prosecution for the offence of smuggling is time-barred.
4. The marketing of goods in another Member State, after their importation into the Member State where the accused was acquitted, constitutes conduct which may form part of the "same acts" within the meaning of Article 54 of the Convention.
[1] OJ C 6, 08.01.2005.
--------------------------------------------------
