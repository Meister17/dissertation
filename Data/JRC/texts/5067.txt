Notice of the impending expiry of certain anti-dumping measures
(2005/C 209/02)
1. As provided for in Article 11(2) of Council Regulation (EC) No 384/96 of 22 December 1995 [1] on protection against dumped imports from countries not members of the European Community, the Commission gives notice that, unless a review is initiated in accordance with the following procedure, the anti-dumping measures mentioned below will expire on the date mentioned in the table below.
2. Procedure
Community producers may lodge a written request for a review. This request must contain sufficient evidence that the expiry of the measures would be likely to result in a continuation or recurrence of dumping and injury.
Should the Commission decide to review the measures concerned, importers, exporters, representatives of the exporting country and Community producers will then be provided with the opportunity to amplify, rebut or comment on the matters set out in the review request.
3. Time limit
Community producers may submit a written request for a review on the above basis, to reach the European Commission, Directorate-General for Trade (Division B-1), J-79 5/16, B-1049 Brussels [2] at any time from the date of the publication of the present notice but no later than three months before the date mentioned in the table below.
4. This notice is published in accordance with Article 11(2) of Council Regulation (EC) No 384/96 of 22 December 1995.
Product | Country(ies) of origin or exportation | Measures | Reference | Date of expiry |
Urea | Russia | Anti-dumping duty | Council Regulation (EC) No 901/2001 (OJ L 127, 9.5.2001, p. 11) | 10.5.2006 |
Aluminium foil | People's Republic of ChinaRussia | Anti-dumping duty | Council Regulation (EC) No 950/2001 (OJ L 134, 17.5.2001, p. 1) as last amended by Regulation (EC) No 998/2004 (OJ L 183, 20.5.2004, p. 4) | 18.5.2006 |
Russia | Undertaking | Commission Decision No 2001/381/EC (OJ L 134, 17.5.2001, p. 67) | 18.5.2006 |
[1] OJ L 56, 6.3.1996, p. 1. Regulation as last amended by Council Regulation (EC) No 461/2004 (OJ L 77, 13.3.2004, p. 12).
[2] Telefax : (32-2) 295 65 05.
--------------------------------------------------
