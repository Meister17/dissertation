Decision of the EEA Joint Committee
No 101/2004
of 9 July 2004
amending Annex VI (Social security) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex VI to the Agreement was amended by the Agreement on the participation of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Hungary, the Republic of Latvia, the Republic of Lithuania, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic in the European Economic Area signed on 14 October 2003 in Luxemburg [1].
(2) Regulation (EC) No 631/2004 of the European Parliament and of the Council of 31 March 2004 amending Council Regulation (EEC) No 1408/71 on the application of social security schemes to employed persons, to self-employed persons and to members of their families moving within the Community, and Council Regulation (EEC) No 574/72 laying down the procedure for implementing Regulation (EEC) No 1408/71, in respect of the alignment of rights and the simplification of procedures [2] is to be incorporated into the Agreement.
HAS DECIDED AS FOLLOWS:
Article 1
The following indent shall be added in points 1 (Council Regulation (EEC) No 1408/71) and 2 (Council Regulation (EEC) No 574/72) of Annex VI to the Agreement:
"— 32004 R 0631: Regulation (EC) No 631/2004 of the European Parliament and of the Council of 31 March 2004 (OJ L 100, 6.4.2004, p. 1)."
Article 2
The texts of Regulation (EC) No 631/2004 in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 10 July 2004, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [3].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 9 July 2004.
For the EEA Joint Committee
The President
Kjartan Jóhannsson
--------------------------------------------------
[1] OJ L 130, 29.4.2004, p. 3.
[2] OJ L 100, 6.4.2004, p. 1.
[3] No constitutional requirements indicated.
