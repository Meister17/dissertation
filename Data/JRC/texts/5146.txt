COM documents other than legislative proposals adopted by the Commission
(2005/C 55/04)
Document | Part | Date | Title |
COM(2004) 603 | | 1.10.2004 | Report from the Commission to the Council, the European Parliament and the European Economic and Social Committee on the application of Council Regulation (EC) No 1348/2000 on the service in the Member States of Judicial and Extrajudicial documents in civil or commercial matters |
COM(2004) 723 | | 26.10.2004 | Communication from the Commission: The EU Economy: 2004 Review — Summary and main conclusions |
COM(2004) 745 | | 9.11.2004 | Report from the Commission to the Council and the European Parliament on incentives for EMAS registered organisations |
COM(2004) 837 | | 28.12.2004 | Communication from the Commission to the European Parliament and the Council: Technical adjustment of the financial perspective for 2006 in line with movements in GNI and prices (Point 15 of the Interinstitutional Agreement of 6 May 1999 on budgetary discipline and improvement of the budgetary procedure) |
COM(2004) 855 | | 10.1.2005 | Report from the Commission to the Council and the European Parliament: Fifth report under Article 12 of Regulation (EEC, Euratom) No 1553/89 on VAT collection and control procedures |
COM(2005) 30 | | 8.2.2005 | Communication from the Commission to the Council and the European Parliament: Report on the implementation of the European Charter for Small Enterprises |
These texts are available on EUR-Lex: http://europa.eu.int/eur-lex/lex/
--------------------------------------------------
