Council Decision
of 4 April 2001
on the approval, on behalf of the European Community, of the Protocol to the 1979 Convention on Long-range Transboundary Air Pollution on Heavy Metals
(2001/379/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 175(1), in conjunction with Article 300(2), first sentence of the first subparagraph, and (3), first subparagraph, thereof,
Having regard to the proposal from the Commission(1),
Having regard to the opinion of the European Parliament(2),
Whereas:
(1) The Community signed in Aarhus on 24 June 1998 the Protocol to the 1979 Convention on Long-range Transboundary Air Pollution on Heavy Metals (hereinafter "the Protocol").
(2) The Protocol aims at controlling emissions of heavy metals caused by anthropogenic activities that are subject to long-range transboundary atmospheric transport and that are likely to have significant adverse effects on human health or the environment.
(3) The Protocol stipulates the reduction of total annual emissions into the atmosphere of cadmium, lead and mercury, and the application of product control measures.
(4) The measures envisaged in the Protocol contribute to achieving objectives of the Community policy on environment.
(5) The Community and the Member States cooperate, in the framework of their respective competences, with non-member countries and the competent international organisations.
(6) The Community should approve the Protocol,
HAS DECIDED AS FOLLOWS:
Article 1
The Protocol to the 1979 Convention on Long-range Transboundary Air Pollution on Heavy Metals, signed on 24 June 1998, is hereby approved on behalf of the European Community.
The text of the Protocol is attached to this Decision.
Article 2
The President of the Council is hereby authorised to designate the person empowered to deposit the instrument of approval with the Secretary-General of the United Nations, in accordance with Article 16 of the Protocol.
Article 3
This Decision will be published in the Official Journal of the European Communities.
Done at Luxembourg, 4 April 2001.
For the Council
The President
B. Rosengren
(1) OJ C 311 E, 31.10.2000, p. 136.
(2) Opinion given on 24 October 2000 (not yet published in the Official Journal).
