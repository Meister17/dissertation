Council Common Position 2006/242/CFSP
of 20 March 2006
relating to the 2006 Review Conference of the Biological and Toxin Weapons Convention (BTWC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty on European Union, and in particular Article 15 thereof,
Whereas:
(1) The European Union considers the Convention on the Prohibition of the Development, Production and Stockpiling of Bacteriological (Biological) and Toxin Weapons and on Their Destruction (BTWC) as a key component of the international non-proliferation and disarmament framework and the cornerstone of efforts to prevent biological agents and toxins from ever being developed and used as weapons. Furthermore, the European Union remains committed to the development of measures to verify compliance with the BTWC in the longer term.
(2) On 17 May 1999, the Council adopted Common Position 1999/346/CFSP [1] relating to progress towards a legally binding Protocol to strengthen compliance with the BTWC and on 25 June 1996, Common Position 96/408/CFSP [2] relating to preparation for the Fourth Review Conference of the BTWC.
(3) On 17 November 2003 the Council adopted Common Position 2003/805/CFSP on the universalisation and reinforcement of multilateral agreements in the field of non-proliferation of weapons of mass destruction and means of delivery [3]. Under that Common Position, the BTWC is included as one of these multilateral agreements.
(4) On 12 December 2003, the European Council adopted a Strategy against the Proliferation of Weapons of Mass Destruction which aims, inter alia, at reinforcing the BTWC, continuing reflection on verification of the BTWC, supporting national implementation of the BTWC, including through penal legislation, and strengthening compliance with it.
(5) On 28 April 2004, the United Nations Security Council unanimously adopted Resolution 1540 (2004) describing the proliferation of weapons of mass destruction and their means of delivery as a threat to international peace and security. Implementation of the provisions of this Resolution contributes to implementation of the BTWC.
(6) On 1 June 2004, the Council adopted a statement of support for the Proliferation Security Initiative on Weapons of Mass Destruction.
(7) On 14 November 2002, the States Parties to the BTWC decided, by consensus, to hold three annual meetings of States Parties of one week duration commencing in 2003 until the Sixth Review Conference, to be held not later than the end of 2006. Each meeting of the States Parties would be prepared by a two-week meeting of experts, and the Sixth Review Conference would consider the work of these meetings and decide on any further action. The States Parties decided that the Sixth Review Conference would be held in Geneva in 2006, and would be preceded by a Preparatory Committee.
(8) On 13 December 1982, the United Nations General Assembly adopted a Resolution (A/RES/37/98) on Chemical and Bacteriological (Biological) Weapons requesting the United Nations Secretary-General to investigate information that may be brought to his attention concerning activities that may constitute a violation of the 1925 Geneva Protocol. On 26 August 1988 the United Nations Security Council adopted Resolution 620 which, inter alia, encourages the Secretary-General to carry out promptly investigations in response to allegations concerning the possible use of chemical and bacteriological (biological) or toxin weapons that may constitute a violation of the 1925 Geneva Protocol.
(9) On 27 February 2006, the European Union agreed on a Joint Action in respect of the BTWC with the objectives of promoting universality of the BTWC and supporting its implementation by States Parties in order to ensure that States Parties transpose the international obligations of the BTWC into their national legislation and administrative measures.
(10) In parallel with the Joint Action, the European Union agreed on an Action Plan in respect of the BTWC in which Member States undertook to submit Confidence Building Measures returns to the United Nations in April 2006 and lists of relevant experts and laboratories to the United Nations Secretary-General to facilitate any investigation of alleged chemical and biological weapons use.
(11) In the light of the forthcoming BTWC Review Conference during the period 20 November to 8 December 2006 and its Preparatory Committee 26 to 28 April 2006, it is appropriate to update the European Union position,
HAS ADOPTED THIS COMMON POSITION:
Article 1
The objective of the European Union shall be to strengthen further the Convention on the Prohibition of the Development, Production and Stockpiling of Bacteriological (Biological) and Toxin Weapons Convention and on Their Destruction (BTWC). The European Union continues to work towards identifying effective mechanisms to strengthen and verify compliance with the BTWC. The European Union shall therefore promote a successful outcome of the Sixth Review Conference in 2006.
Article 2
For the purposes of the objective laid down in Article 1, the European Union shall:
(a) contribute to a full review of the operation of the BTWC at the Sixth Review Conference, including the implementation of undertakings of the States Parties under the BTWC;
(b) support a further intersessional work programme during the period between the Sixth and Seventh Review Conferences and identify specific areas and procedures for further progress under this work programme;
(c) support a Seventh Review Conference of the BTWC, to be held no later than 2011;
(d) help build a consensus for a successful outcome of the Sixth Review Conference, on the basis of the framework established by previous such Conferences, and shall promote, inter alia, the following essential issues:
(i) universal accession of all States to the BTWC, including calling on all States not party thereto to accede to the BTWC without further delay and to commit legally to the disarmament and non-proliferation of biological and toxin weapons; and, pending the accession of such States to the BTWC, encouraging such States to participate as observers in the meetings of the States Parties to the BTWC and to implement its provisions on a voluntary basis. Working towards the ban on biological and toxin weapons being declared universally binding rules of international law, including through universalisation of the BTWC;
(ii) full compliance with the obligations under the BTWC and effective implementation by all States Parties;
(iii) in relation to full compliance with all the provisions of the BTWC by all States Parties, strengthening, where necessary, national implementation measures, including penal legislation, and control over pathogenic micro-organisms and toxins in the framework of the BTWC. Working towards identifying effective mechanisms to strengthen and verify compliance within the BTWC;
(iv) efforts to enhance transparency through the increased exchange of information among States Parties, including through the annual information exchange among the States Parties to the Convention (Confidence Building Measures (CBM)), identifying measures to assess and enhance the country coverage and the usefulness of the CBM mechanism, and exploring the relevance of any possible enhancement of its scope;
(v) compliance with obligations under United Nations Security Council Resolution 1540 (2004), in particular to eliminate the risk of biological or toxin weapons being acquired or used for terrorist purposes, including possible terrorist access to materials, equipment, and knowledge that could be used in the development and production of biological and toxin weapons;
(vi) the G8 Global Partnership programmes targeted at support for disarmament, control and security of sensitive materials, facilities, and expertise;
(vii) consideration of, and decisions on further action on, the work undertaken to date under the intersessional programme during the period 2003 to 2005 and the efforts to discuss, and promote common understanding and effective action on: the adoption of necessary national measures to implement the prohibitions set forth in the BTWC, including the enactment of penal legislation; national mechanisms to establish and maintain the security and overseeing of pathogenic micro-organisms and toxins; enhancing international capabilities for responding to, investigating, and mitigating the effects of, cases of alleged use of biological or toxin weapons or suspicious outbreaks of disease; strengthening and broadening national and international institutional efforts and existing mechanisms for the surveillance, detection, diagnosis and combating of infectious diseases affecting humans, animals, and plants; the content, promulgation, and adoption of codes of conduct for scientists; noting that continued efforts on the abovementioned subjects will be required by all States Parties to enhance implementation of the BTWC.
Article 3
Action taken by the European Union for the purposes of Article 2 shall comprise:
(a) agreement by Member States on specific, practical and feasible proposals for the effective enhancement of the implementation of the BTWC for submission on behalf of the European Union for consideration by States Parties to the Convention at the Sixth Review Conference;
(b) where appropriate, approaches by the Presidency, pursuant to Article 18 of the Treaty on European Union
(i) with a view to promoting universal accession to the BTWC;
(ii) to promote national implementation of the BTWC by States Parties;
(iii) to urge States Parties to support and participate in an effective and complete review of the BTWC and thereby reiterate their commitment to this fundamental international norm against biological weapons;
(iv) to promote the abovementioned proposals submitted by the European Union for States Parties' consideration which are aimed at further strengthening the BTWC;
(c) statements by the European Union delivered by the Presidency in the run up to, and during, the Review Conference.
Article 4
This Common Position shall take effect on the day of its adoption.
Article 5
This Common Position shall be published in the Official Journal of the European Union.
Done at Brussels, 20 March 2006.
For the Council
The President
U. Plassnik
[1] OJ L 133, 28.5.1999, p. 3.
[2] OJ L 168, 6.7.1996, p. 3.
[3] OJ L 302, 20.11.2003, p. 34.
--------------------------------------------------
