Commission Regulation (EC) No 1172/2005
of 19 July 2005
fixing the A1 and B export refunds for fruit and vegetables (tomatoes, oranges, lemons, table grapes and apples)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 2200/96 of 28 October 1996 on the common organisation of the market in fruit and vegetables [1], and in particular the third subparagraph of Article 35(3),
Whereas:
(1) Commission Regulation (EC) No 1961/2001 [2] lays down the detailed rules of application for export refunds on fruit and vegetables.
(2) Article 35(1) of Regulation (EC) No 2200/96 provides that, to the extent necessary for economically significant exports, the products exported by the Community may be covered by export refunds, within the limits resulting from agreements concluded in accordance with Article 300 of the Treaty.
(3) Under Article 35(2) of Regulation (EC) No 2200/96, care must be taken to ensure that the trade flows previously brought about by the refund scheme are not disrupted. For this reason and because exports of fruit and vegetables are seasonal in nature, the quantities scheduled for each product should be fixed, based on the agricultural product nomenclature for export refunds established by Commission Regulation (EEC) No 3846/87 [3]. These quantities must be allocated taking account of the perishability of the products concerned.
(4) Article 35(4) of Regulation (EC) No 2200/96 provides that refunds must be fixed in the light of the existing situation or outlook for fruit and vegetable prices on the Community market and supplies available on the one hand, and prices on the international market on the other hand. Account must also be taken of the transport and marketing costs and of the economic aspect of the exports planned.
(5) In accordance with Article 35(5) of Regulation (EC) No 2200/96, prices on the Community market are to be established in the light of the most favourable prices from the export standpoint.
(6) The international trade situation or the special requirements of certain markets may call for the refund on a given product to vary according to its destination.
(7) Tomatoes, oranges, lemons, table grapes and apples of classes Extra, I and II of the common quality standards can currently be exported in economically significant quantities.
(8) In order to ensure the best use of available resources and in view of the structure of Community exports, it is appropriate to fix the A1 and B export refunds.
(9) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for fresh Fruit and Vegetables,
HAS ADOPTED THIS REGULATION:
Article 1
1. For system A1, the refund rates, the refund application period and the scheduled quantities for the products concerned are fixed in the Annex hereto. For system B, the indicative refund rates, the licence application period and the scheduled quantities for the products concerned are fixed in the Annex hereto.
2. The licences issued in respect of food aid as referred to in Article 16 of Commission Regulation (EC) No 1291/2000 [4] shall not count against the eligible quantities in the Annex hereto.
Article 2
This Regulation shall enter into force on 9 September 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 19 July 2005.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 297, 21.11.1996, p. 1. Regulation as last amended by Commission Regulation (EC) No 47/2003 (OJ L 7, 11.1.2003, p. 64).
[2] OJ L 268, 9.10.2001, p. 8. Regulation as last amended by Regulation (EC) No 386/2005 (OJ L 62, 9.3.2005, p. 3).
[3] OJ L 366, 24.12.1987, p. 1. Regulation as last amended by Regulation (EC) No 558/2005 (OJ L 94, 13.4.2005, p. 22).
[4] OJ L 152, 24.6.2000, p. 1. Regulation as last amended by Regulation (EC) No 1741/2004 (OJ L 311, 8.10.2004, p. 17).
--------------------------------------------------
ANNEX
to the Commission Regulation of 19 July 2005 fixing the export refunds on fruit and vegetables (tomatoes, oranges, lemons, table grapes and apples)
Product code | Destination | System A1Refund application period 9.9.2005 to 8.11.2005 | System BLicence application period 16.9.2005 to 15.11.2005 |
Refund amount(EUR/t net weight) | Scheduled quantiy(t) | Indicative refund amount(EUR/t net weight) | Scheduled quantity(t) |
0702 00 00 9100 | F08 | 35 | | 35 | 3407 |
0805 10 20 9100 | A00 | 38 | | 38 | 7121 |
0805 50 10 9100 | A00 | 60 | | 60 | 3995 |
0806 10 10 9100 | A00 | 23 | | 23 | 14012 |
0808 10 80 9100 | F04, F09 | 36 | | 36 | 20168 |
--------------------------------------------------
