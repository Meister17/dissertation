Commission Regulation (EC) No 797/2002
of 14 May 2002
amending Annexes III and VIII to Council Regulation (EEC) No 3030/93 on common rules for imports of certain textile products from third countries
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 3030/93 of 12 October 1993 on common rules for imports of certain textile products from third countries(1), as last amended by Commission Regulation (EC) No 27/2002(2), and in particular Article 19 thereof,
Whereas:
(1) The Council has adopted by Regulation (EC) No 2474/2000(3) the list of textiles and clothing products to be integrated into GATT 1994 on 1 January 2002.
(2) The People's Republic of China became a full member of the World Trade Organization on 11 December 2001. Taiwan became a full member on 1 January 2002.
(3) The certificate of origin and the export certificate used by Taiwan differ slightly from the standard model in Regulation (EEC) No 3030/93. Specimens have been provided by the Taiwanese authorities.
(4) The flexibility provisions for both countries, as laid out in Regulation (EEC) No 3030/93, need to be adapted in order to comply with the Community's international obligations.
(5) Regulation (EEC) No 3030/93 should be amended accordingly.
(6) In order to ensure that the Community complies with its international obligations the measures provided for in this Regulation should apply with effect from 1 January 2002.
(7) The measures provided for in this Regulation are in accordance with the opinion of the Textile Committee,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EEC) No 3030/93 is amended as follows:
1. Annex III is amended in accordance with part A of the Annex to this Regulation.
2. Annex VIII is replaced by the text set out in part B of the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Communities.
It shall apply with effect from 1 January 2002.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 14 May 2002.
For the Commission
Pascal Lamy
Member of the Commission
(1) OJ L 275, 8.11.1993, p. 3.
(2) OJ L 9, 11.1.2002, p. 1.
(3) OJ L 286, 11.11.2000, p. 1.
ANNEX
(A) In Annex III of Regulation (EEC) No 3030/93 the following specimen of a Taiwan export certificate and certificate of origin are added:
>PIC FILE= "L_2002128EN.003101.TIF">
>PIC FILE= "L_2002128EN.003301.TIF">
(B) Annex VIII of Regulation (EEC) No 3030/93 is replaced by the following:
""
ANNEX VIII
REFERRED TO IN ARTICLE 7
Flexibility provisions
The attached table indicates for each of the supplier countries listed in column 1 the maximum amounts which, after advance notification to the Commission, it may transfer between the corresponding quantitative limits indicated in Annex V in accordance with the following provisions:
- advance utilisation of the quantitative limit for the particular category established for the following quota year shall be authorised up to the percentage of the quantitative limit for the current year indicated in column 2; the amounts in question shall be deducted from the corresponding quantitative limits for the following year,
- carry-over of amounts not utilised in a given year to the corresponding quantitative limit for the following year shall be authorised up to the percentage of the quantitative limit for the year of actual utilisation indicated in column 3,
- transfers from categories 1 to categories 2 and 3 shall be authorised up to the percentages of the quantitative limit to which the transfer is made indicated in column 4,
- transfers between categories 2 and 3 shall be authorised up to the percentages of the quantitative limit to which the transfer is made indicated in column 5,
- transfers between categories 4, 5, 6, 7 and 8 shall be authorised up to the percentages of the quantitative limit to which the transfer is made indicated in column 6,
- transfers into any of the categories in Groups II or III (and where applicable Group IV) from any of the categories in Groups I, II or III shall be authorised up to the percentages of the quantitative limit to which the transfer is made indicated in column 7.
The cumulative application of the flexibility provisions referred to above shall not result in an increase in any Community quantitative limit for a given year above the percentage indicated in column 8.
The table of equivalence applicable to the abovementioned transfers is given in Annex I.
Additional conditions, possibilities for transfers and notes are given in column 9 of the table.
>TABLE>
n.a. = not applicable.
Flexibility provisions for quantitative restrictions referred to in Appendix C to Annex V
>TABLE>
Appendix to Annex VIII
Flexibility provisions Hong Kong
>TABLE>
>TABLE>
