Decision of the EEA Joint Committee
No 151/2005
of 2 December 2005
amending Annex XIII (Transport) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex XIII to the Agreement was amended by Decision of the EEA Joint Committee No 122/2005 [1].
(2) Regulation (EC) No 2320/2002 of the European Parliament and of the Council of 16 December 2002 establishing common rules in the field of civil aviation security [2] was incorporated into the Agreement by Decision of the EEA Joint Committee No 61/2004, with country specific adaptations.
(3) Commission Regulation (EC) No 781/2005 of 24 May 2005 amending Regulation (EC) No 622/2003 laying down measures for the implementation of the common basic standards on aviation security [3] is to be incorporated into the Agreement.
(4) Commission Regulation (EC) No 857/2005 of 6 June 2005 amending Regulation (EC) No 622/2003 laying down measures for the implementation of the common basic standards on aviation security [4] is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
The following indents shall be added in point 66i (Commission Regulation (EC) No 622/2003) of Annex XIII to the Agreement:
"— 32005 R 0781: Commission Regulation (EC) No 781/2005 (OJ L 131, 25.5.2005, p. 24),
— 32005 R 0857: Commission Regulation (EC) No 857/2005 (OJ L 143, 7.6.2005, p. 9)."
Article 2
The texts of Regulations (EC) No 781/2005 and (EC) No 857/2005 in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 3 December 2005, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [5].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 2 December 2005.
For the EEA Joint Committee
The President
H.S.H. Prinz Nikolaus von Liechtenstein
[1] OJ L 339, 22.12.2005, p. 30.
[2] OJ L 355, 30.12.2002, p. 1. Regulation as amended by Regulation (EC) No 849/2004 (OJ L 158, 30.4.2004, p. 1, as corrected by OJ L 229, 29.6.2004, p. 3).
[3] OJ L 131, 25.5.2005, p. 24.
[4] OJ L 143, 7.6.2005, p. 9.
[5] No constitutional requirements indicated.
--------------------------------------------------
