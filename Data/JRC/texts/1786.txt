Information communicated by Member States regarding State aid granted under Commission Regulation (EC) No 70/2001 of 12 January 2001 on the application of Articles 87 and 88 of the EC Treaty to State aid to small and medium-sized enterprises
(2005/C 327/12)
(Text with EEA relevance)
Aid No : XS 125/02
Member State : Italy
Region : Lazio
Title of aid scheme : Rules for promoting economic development, social cohesion and employment in Lazio. Identification and organisation of local productive clusters, industrial districts and investment areas in the region
Legal basis :
Legge Regionale 19 dicembre 2001, n. 36 "Norme per l'incremento dello sviluppo economico, della coesione sociale e dell'occupazione nel Lazio. Individuazione e organizzazione dei sistemi produttivi locali, dei distretti industriali e delle aree laziali di investimento", pubblicata sul supplemento ordinario n. 7 al BURL (Bollettino Ufficiale della Regione Lazio) n. 36 del 29/12/2001
Regolamemto regionale 28 ottobre 2002, n. 2 "Regolamento per il finanziamento dei sistemi produttivi locali, dei distretti industriali e delle aree laziali di investimento", pubblicato sul supplemento ordinario n. 4 al BURL n. 30 del 30 ottobre 2002
Annual expenditure planned under the scheme : Total EUR 4250000 (for all aid under the law)
Maximum aid intensity - areas eligible for the derogation under Article 87(3)(c): 8 % nge + 10 % gge for small firms; 8 % nge + 6 % gge for medium-sized firms.
- other areas:15 % gge for small firms; 7,5 % gge for medium-sized firms.
Consultancy: up to 50 % pf eligible costs
Date of implementation : The aid may be granted after publication of the public notice in the Lazio Region Official Gazette (Bollettino Ufficiale della Regione Lazio, BURL)
Duration of scheme : Open-ended; the aid scheme is in any case exempt from compulsory notification under Article 88(3) of the EC Treaty up to 31 December 2006, when Regulation (EC) No 70/2001 expires
Objective of aid : The aid is intended to promote investment and consultancy programmes in the areas identified by the Lazio Region as industrial districts, local productive clusters and investment areas
Economic sector(s) concerned :
The activity codes are those identified on a case-by-case basis by the Regional authorities in relation to industrial districts, local productive clusters and investment areas
Name and address of granting authority Regione Lazio
Via Rosa Raimondi Garibaldi, 7
I-00145, Rome
--------------------------------------------------
