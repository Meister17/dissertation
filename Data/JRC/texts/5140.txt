Authorisation for State aid pursuant to Articles 87 and 88 of the EC Treaty
Cases where the Commission raises no objections
(2005/C 62/04)
(Text with EEA relevance)
Date of adoption of the decision: 14 July 2004
Member State [Region]: Italy
Aid No: N 584/2003
Title: Extension of delivery deadline for 2 Ro-Ro TP ships (numbers C.209 and C.210)
Objective [sector]: Shipbuilding
Legal basis: Articolo 2 della Legge n. 522 del 28.12.1999
Aid intensity or amount: 9 %
Other information: The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:http://europa.eu.int/comm/secretariat_general/sgb/state_aids
--------------------------------------------------
