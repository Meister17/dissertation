PROTOCOL on veterinary matters supplementary to the agreement in the form of an exchange of letters between the European Economic Community and the Principality of Andorra
THE PRINCIPALITY OF ANDORRA
and
THE EUROPEAN COMMUNITY
HAVING REGARD to the Agreement, in the form of an Exchange of Letters between the European Economic Community and the Principality of Andorra, signed in Luxembourg on 28 June 1990,
DESIRING to maintain traditional flows of trade in live animals and animal products between Andorra and the European Community;
WHEREAS such trade should accordingly be conducted in compliance with Community veterinary rules;
WHEREAS, to this end, the Agreement between the European Economic Community and the Principality of Andorra should be supplemented,
HAVE AGREED UPON THE FOLLOWING PROVISIONS:
Article 1
The Principality of Andorra undertakes to apply Community veterinary rules in respect of the following:
I. Preventive measures/notification of diseases
II. Animal health: trade and placing on the market (except third countries)
III. Animal health protective measures for animal health products (except third countries)
IV. Public health protective measures: rules governing placing on the market (except third countries)
V. Hormones, residues, BST, zoonoses, animal residues, medicated feed
VI. Imports from third countries
VII. Inspection, animal identification, mutual assistance
VIII. Animal science (including provisions in respect of third countries)
IX. Animal protection
X. Institutional matters (primarily concerning procedures for the ongoing incorporation of the acquis communautaire in the veterinary field into Andorran law).
Article 2
A list of the Community veterinary provisions to be applied by Andorra shall be drawn up by the Joint Committee set up under Article 17 of the Agreement between the European Economic Community and the Principality of Andorra.
A veterinary sub-group shall be set up under the Joint Committee. It shall periodically examine the situation concerning Community law applicable to Andorra. If necessary, the sub-group shall make recommendations to the Joint Committee with a view to amending or updating the legislation in question.
Article 3
This Protocol shall form an integral part of the Agreement.
Article 4
This Protocol shall be approved by the Contracting Parties in accordance with their own procedures. It shall enter into force on the first day of the second month following notification of the completion of such procedures by the Contracting Parties.
Article 5
This Protocol is drawn up in two copies in the Danish, Dutch, English, French, Finnish, German, Greek, Italian, Portuguese, Spanish, Swedish and Catalan languages, each text being equally authentic.
Hecho en Bruselas, el quince de mayo de mil novecientos noventa y siete.
Udfærdiget i Bruxelles den femtende maj nitten hundrede og syvoghalvfems.
Geschehen zu Brüssel am fünfzehnten Mai neunzehnhundertsiebenundneunzig.
¸ãéíå óôéò ÂñõîÝëëåò, óôéò äåêáðÝíôå ÌáÀïõ ÷ßëéá åííéáêüóéá åíåíÞíôá åðôÜ.
Done at Brussels on the fifteenth day of May in the year one thousand nine hundred and ninety-seven.
Fait à Bruxelles, le quinze mai mil neuf cent quatre-vingt-dix-sept.
Fatto a Bruxelles, addì quindici maggio millenovecentonovantasette.
Gedaan te Brussel, de vijftiende mei negentienhonderd zevenennegentig.
Feito em Bruxelas, em quinze de Maio de mil novecentos e noventa e sete.
Tehty Brysselissä viidentenätoista päivänä toukokuuta vuonna tuhatyhdeksänsataayhdeksänkymmentäseitsemän.
Som skedde i Bryssel den femtonde maj nittonhundranittiosju.
Fet a Brussel.les, el quinze de maig de mil nou cents noranta set.
Por la Comunidad Europea
For Det Europæiske Fællesskab
Für die Europäische Gemeinschaft
Ãéá ôçí ÅõñùðáúêÞ Êïéíüôçôá
For the European Community
Pour la Communauté européenne
Per la Comunità europea
Voor de Europese Gemeenschap
Pela Comunidade Europeia
Euroopan yhteisön puolesta
På Europeiska gemenskapens vägnar
>REFERENCE TO A FILM>
Pel Govern del Principat d'Andorra
>REFERENCE TO A FILM>
