Commission Regulation (EC) No 2195/2003
of 16 December 2003
opening tariff quotas for the year 2004 for imports into the European Community of certain processed agricultural products originating in Norway
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 3448/93 of 6 December 1993 laying down the trade arrangements applicable to certain goods resulting from the processing of agricultural products(1), as last amended by Regulation (EC) No 2580/2000(2) and, in particular, Article 7(2) thereof,
Having regard to Council Decision 2002/981/EC of 11 November 2002 concerning the conclusion of an Agreement in the form of an Exchange of Letters between the European Community, of the one part, and the Kingdom of Norway, of the other part, on Protocol 2 to the Agreement between the European Economic Community and the Kingdom of Norway(3) and, in particular, Article 2 thereof,
Whereas:
(1) The annual quotas for certain processed agricultural products originating in Norway provided for in the Agreement in the form of an Exchange of Letters between the European Community, of the one part, and the Kingdom of Norway, of the other part, on Protocol 2 to the Agreement between the European Economic Community and the Kingdom of Norway should be opened for 2004.
(2) Commission Regulation (EEC) No 2454/93 of 2 July 1993 laying down provisions for the implementation of Council Regulation (EEC) No 2913/92 establishing the Common Customs Code(4), as last amended by Regulation (EC) No 1335/2003(5), lays down rules for the management of tariff quotas. It is appropriate to provide that the tariff quota opened by this Regulation is to be managed in accordance with those rules.
(3) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for horizontal questions concerning trade in processed agricultural products not listed in Annex I,
HAS ADOPTED THIS REGULATION:
Article 1
The Community tariff quotas for imports of processed agricultural products originating in Norway, as specified in the Annex, shall be opened duty-free from 1 January to 31 December 2004.
Article 2
The Community tariff quota referred to in Article 1 shall be managed by the Commission in accordance with Articles 308a, 308b and 308c of Regulation (EEC) No 2454/93.
Article 3
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Union.
It shall be applicable from 1 January 2004.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 16 December 2003.
For the Commission
Erkki Liikanen
Member of the Commission
(1) OJ L 318, 20.12.1993, p. 18.
(2) OJ L 298, 25.11.2000, p. 5.
(3) OJ L 341, 17.12.2002, p. 63.
(4) OJ L 253, 11.10.1993, p. 1.
(5) OJ L 187, 26.7.2003, p. 16.
ANNEX
>TABLE>
