Commission Regulation (EC) No 1972/2003
of 10 November 2003
on transitional measures to be adopted in respect of trade in agricultural products on account of the accession of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to the Treaty of Accession of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia, and in particular Article 2(3) thereof,
Having regard to the Act of Accession of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia, and in particular Article 41, first paragraph, thereof,
Whereas:
(1) Transitional measures should be adopted in order to avoid the risk of deflection of trade, affecting the common organisation of agricultural markets due to the accession of 10 new States to the European Union on 1 May 2004.
(2) In accordance with the rules set out in Commission Regulation (EC) No 800/1999 of 15 April 1999 laying down common detailed rules for the application of the system of export refunds on agricultural products(1), as last amended by Regulation (EC) No 500/2003(2), products are not entitled to refund unless they have left the customs territory of the Community within 60 days of acceptance of the export declaration. The obligation to leave the customs territory of the Community within 60 days of acceptance of the export declaration is also a primary requirement for releasing the security linked to the licence under Commission Regulation (EC) No 1291/2000 of 9 June 2000 laying down common detailed rules for the application of the system of import and export licences and advance fixing certificates for agricultural products(3), as last amended by Regulation (EC) No 325/2003(4). Since the internal borders will be removed upon accession, products exported from the Community of Fifteen must have left the customs territory of the Community by 30 April 2004 at the latest in all cases, including where the export declaration has been accepted less than 60 days before the date of accession.
(3) Trade deflections liable to disrupt the market organisations often involve products moved artificially with a view to enlargement and do not form part of the normal stocks of the State concerned. Surplus stocks may also result from national production. Provisions should, therefore, be made for deterrent charges to be levied on surplus stocks in the new Member States.
(4) It is necessary to prevent goods in respect of which export refunds were paid before 1 May 2004 from benefiting from a second refund when exported to third countries after 30 April 2004.
(5) The measures provided for in this Regulation are necessary and appropriate and should be applied in uniform fashion.
(6) The measures provided for in this Regulation are in accordance with the opinion of all the Management Committees concerned,
HAS ADOPTED THIS REGULATION:
Article 1
Definitions
For the purposes of this Regulation:
(a) "Community of Fifteen" means the Community as constituted on 30 April 2004;
(b) "new Member States" means the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia;
(c) "enlarged Community" means the Community as constituted on 1 May 2004;
(d) "products" means agricultural products and/or goods not included in Annex I of the EC Treaty;
(e) "production refund" means the refund granted pursuant to Article 7 of Council Regulation (EEC) No 1766/92(5) or Article 7 of Council Regulation (EC) No 3072/95(6) or Article 7 of Council Regulation (EC) No 1260/2001(7).
Article 2
Exports from the Community of Fifteen
Where, for the products intended for export from the Community of Fifteen to one of the new Member States for which an export declaration has been accepted by 30 April 2004 at the latest, the conditions set out in Article 3 of Regulation (EC) No 800/1999 have not been complied with by that date, the beneficiary shall reimburse any refund received in accordance with Article 52 of that Regulation.
Article 3
Suspensive regime
1. This Article shall apply by way of derogation from Annex IV, Chapter 5, to the Act of Accession and from Articles 20 and 214 of Council Regulation (EEC) No 2913/92 of 12 October 1992 establishing the Community Customs Code(8).
2. Products listed in Article 4(5), which before 1 May 2004 have been in free circulation in the Community of Fifteen or in a new Member State and on 1 May 2004 are in temporary storage or under one of the customs treatments or procedures referred to in Article 4(15)(b) and (16)(b) to (g) of Regulation (EEC) No 2913/92 in the enlarged Community, or which are in transport after having been the subject of export formalities within the enlarged Community shall be charged with the erga omnes import duty rate applicable on the date of release for free circulation.
The first subparagraph shall not apply to products exported from the Community of Fifteen if the importer gives evidence that no export refund has been sought for the products of the country of export. Upon the importer's request, the exporter shall arrange to obtain an endorsement by the competent authority on the export declaration that an export refund has not been sought for the products of the country of export.
3. Products listed in Article 4(5) coming from third countries which are under inward processing referred to in Article 4(16)(d) or temporary admission referred to in Article 4(16)(f) of Regulation (EEC) No 2913/92 in a new Member State on 1 May 2004 and which are released for free circulation on or after that date, shall be charged with the import duty applicable on the date of release for free circulation to products coming from third countries.
Article 4
Charges on goods in free circulation
1. Without prejudice to Annex IV, Chapter 4, to the Act of Accession, and where stricter legislation does not apply at national level, the new Member States shall levy charges on holders of surplus stocks at 1 May 2004 of products in free circulation.
2. In order to determine the surplus stock of each holder, the new Member States shall take into account, in particular:
(a) averages of stocks available in the years preceding accession;
(b) the pattern of trade in the years preceding accession;
(c) the circumstances in which stocks were built up.
The notion surplus stocks applies to products imported into the new Member States or originating from the new Member States. The notion surplus stocks applies also to products intended for the market of the new Member States.
The recording of the stocks shall be performed on the basis of the Combined Nomenclature applicable on 1 May 2004.
3. The amount of the charge referred to in paragraph 1 shall be determined by the erga omnes import duty rate applicable on 1 May 2004. The revenue of the charge collected by national authorities shall be assigned to the national budget of the new Member State.
4. In order to ensure that the charge referred to in paragraph 1 is correctly applied, the new Member States shall, without delay, carry out an inventory of stocks available as at 1 May 2004. For this purpose the new Member State shall notify the Commission of the quantity of products in surplus stocks, except of those quantities in public stocks as referred to in Article 5, by 31 July 2004 at the latest.
5. This Article shall apply to products covered by the following CN codes:
- in the case of Cyprus:
0204 43 10, 0206 29 91, 0408 11 80, 1602 32 11, 0402 10, 0402 21, 0405 10, 0405 20 10, 0405 20 30, 0405 90, 0406, 0703 20 00, 0711 51 00, 1001, 1002, 1003, 1004, 1005, 1006 10, 1006 20, 1006 30, 1006 40, 1007, 1008, 1101, 1102, 1103, 1104, 1107, 1108, 1509, 1510, 1517, 1702 30 (9), 1702 40 (10), 1702 90 (11), 1901 90 99, 2003 10 20, 2003 10 30, 2106 90 98 (12),
- in the case of the Czech Republic:
0201 30 00, 0202 30 90, 0206 29 91, 0203 11 10, 0203 21 10, 0207 14 10, 0207 14 60, 0207 14 70, 0207 27 10, 0408 11 80, 0408 91 80, 1602 32 11, 0402 10, 0402 21, 0405 10, 0405 20 10, 0405 20 30, 0405 90, 0406, 0703 20 00, 0711 51 00, 1001, 1002, 1003, 1004, 1005, 1006 10, 1006 20, 1006 30, 1006 40, 1007, 1008, 1101, 1102, 1103, 1104, 1107, 1108, 1509, 1510, 1517, 1702 30 (13), 1702 40 (14), 1702 90 (15), 1806 20, 1901 90 99, 2003 10 20, 2003 10 30, 2008 20, 2009 11, 2009 12, 2009 19, 2009 40, 2106 90 98 (16),
- in the case of Estonia:
0201 30 00, 0202 30 90, 0204 30 00, 0204 43 10, 0206 29 91, 0203 11 10, 0203 21 10, 0207 14 10, 0207 14 50, 0207 14 60, 0207 14 70, 0207 27 10, 0402 10, 0402 21, 0405 10, 0405 20 10, 0405 20 30, 0405 90, 0406, 0703 20 00, 0711 51 00, 1001, 1002, 1003, 1004, 1005, 1006 10, 1006 20, 1006 30, 1006 40, 1007, 1008, 1101, 1102, 1103, 1104, 1107, 1108, 1509, 1510, 1517, 1702 30 (17), 1702 40 (18), 1702 90 (19), 1806 20, 1901 90 99, 2003 10 20, 2003 10 30, 2009 11, 2009 12, 2009 19, 2009 40, 2009 71, 2009 79, 2106 90 98 (20),
- in the case of Hungary:
0201 30 00, 0202 30 90, 0204 30 00, 0204 43 10, 0206 29 91, 0203 11 10, 0203 21 10, 0207 14 10, 0207 14 60, 0207 14 70, 0402 10, 0402 21, 0405 10, 0405 90, 0406, 0703 20 00, 0711 51 00, 1001, 1002, 1003, 1004, 1005, 1006 10, 1006 20, 1006 30, 1006 40, 1007, 1008, 1101, 1102 10, 1102 20, 1103, 1104, 1107, 1108, 1509, 1510, 1517, 1702 30 (21), 1702 40 (22), 1702 90 (23), 1806 20, 2003 10 20, 2003 10 30, 2106 90 98 (24),
- in the case of Latvia:
0201 30 00, 0202 30 90, 0204 30 00, 0204 43 10, 0206 29 91, 0207 12 90, 0207 14 10, 0207 14 60, 0207 14 70, 0207 27 10, 0408 11 80, 0408 91 80, 0402 10, 0402 21, 0405 10, 0405 90, 0406, 0703 20 00, 0711 51 00, 1001, 1002, 1003, 1004, 1005, 1006 10, 1006 20, 1006 30, 1006 40, 1007, 1008, 1101, 1102, 1103, 1104, 1107, 1108, 1509, 1510, 1517, 1702 30 (25), 1702 40 (26), 1702 90 (27), 1806 20, 1901 90 99, 2003 10 20, 2003 10 30, 2009 11, 2009 19, 2106 90 98 (28),
- in the case of Lithuania:
0201 30 00, 0202 30 90, 0204 30 00, 0204 43 10, 0206 29 91, 0203 11 10, 0203 21 10, 0207 14 10, 0207 14 60, 0207 14 70, 0207 27 10, 0402 10, 0402 21, 0405 10, 0405 90, 0406, 0703 20 00, 0711 51 00, 1001, 1002, 1003, 1004, 1005, 1006 10, 1006 20, 1006 30, 1006 40, 1007, 1008, 1101, 1102, 1103, 1104, 1107, 1108, 1509, 1510, 1517, 1702 30 (29), 1702 40 (30), 1702 90 (31), 1901 90 99, 2002 90, 2003 10 20, 2003 10 30, 2008 20, 2009 11, 2009 12, 2009 19, 2009 40, 2106 90 98 (32),
- in the case of Malta:
0202 30 90, 0204 30 00, 0204 43 10, 0408 11 80, 0408 91 80, 0206 29 91, 0402 10, 0402 21, 0405 10, 0405 20 10, 0405 20 30, 0405 90, 0406, 0703 20 00, 0711 51 00, 1001, 1002, 1003, 1004, 1005, 1006 10, 1006 20, 1006 30, 1006 40, 1007, 1008, 1101, 1102, 1103, 1104, 1107, 1108, 1509, 1510, 1517, 1702 30 (33), 1702 40 (34), 1702 90 (35), 1806 20, 2003 10 20, 2003 10 30, 2106 90 98 (36),
- in the case of Poland:
0201 30 00, 0202 30 90, 0203 11 10, 0203 21 10, 0204 30 00, 0204 43 10, 0206 29 91, 0402 10, 0402 21, 0405 10, 0405 90, 0406, 0703 20 00, 0711 51 00, 1001, 1002, 1003, 1004, 1005, 1006 10, 1006 20, 1006 30, 1006 40, 1007, 1008, 1101, 1102, 1103, 1104, 1107, 1108, 1509, 1510, 1517, 1702 30 (37), 1702 40 (38), 1702 90 (39), 2003 10 20, 2003 10 30, 2008 20,
- in the case of Slovakia:
0201 30 00, 0202 30 90, 0206 29 91, 0203 11 10, 0203 21 10, 0207 14 10, 0207 14 60, 0207 14 70, 0207 27 10, 0408 11 80, 0408 91 80, 1602 32 11, 0402 10, 0402 21, 0405 10, 0405 20 10, 0405 20 30, 0405 90, 0406, 0703 20 00, 0711 51 00, 1001, 1002, 1003, 1004, 1005, 1006 10, 1006 20, 1006 30, 1006 40, 1007, 1008, 1101, 1102, 1103, 1104, 1107, 1108, 1509, 1510, 1517, 1702 30 (40), 1702 40 (41), 1702 90 (42), 1806 20, 1901 90 99, 2003 10 20, 2003 10 30, 2008 20, 2009 11, 2009 12, 2009 19, 2009 40, 2106 90 98 (43),
- in the case of Slovenia:
0201 30 00, 0202 30 90, 0204 30 00, 0204 43 10, 0206 29 91, 0203 11 10, 0203 21 10, 0207 12 10, 0207 12 90, 0207 14 10, 0207 14 60, 0207 14 70, 0408 11 80, 0408 91 80, 1602 32 11, 0402 10, 0402 21, 0405 10, 0405 20 10, 0405 20 30, 0405 90, 0406, 0703 20 00, 0711 51 00, 1001, 1002, 1003, 1004, 1005, 1006 10, 1006 20, 1006 30, 1006 40, 1007, 1008, 1101, 1102, 1103, 1104, 1107, 1108, 1509, 1510, 1517, 1702 30 (44), 1702 40 (45), 1702 90 (46), 2003 10 20, 2003 10 30, 2008 20, 2009 11, 2009 12, 2009 19, 2009 40, 2106 90 98 (47).
Where a CN code covers products for which the import duty in paragraph 3 is not the same, the inventory of stocks as referred to in paragraph 4 shall be made for each product or group of products subject to a different import duty.
6. The Commission may add products to the list or remove products from the list set out in paragraph 5.
Article 5
Census of public stocks
By 31 July 2004 at the latest, each new Member State shall communicate the list and the quantities of goods which are in public stocks in that Member State as referred to in Annex IV, Chapter 4 of the Act of Accession.
Article 6
National security stocks
The stocks as referred to in Article 4(4) and Article 5 shall not include national security stocks which may possibly have been constituted by the new Member States. The latter shall inform the Commission of all changes made to national security stocks together with the conditions governing the changes for the purposes of establishing the Community supply balance.
Article 7
Measures in the event of non-payment of charges
If any Member State suspects that the charges provided for in Articles 3 and 4 have not been paid in respect of a product, it shall inform the Member State concerned so as to enable it to take appropriate measures.
Article 8
Proof of non-payment of refunds/production refund
Products for which the declaration of export to third countries is accepted by the new Member States during the period from 1 May 2004 to 30 April 2005 may qualify for an export refund or for a refund under one of the procedures referred to in Articles 4 and 5 of Council Regulation (EEC) No 565/80(48) provided that it is established that no export refund has already been paid in respect of those products or their constituents.
Article 9
No double payment of refunds
No product shall be eligible for more than one export refund. Any product which attracted an export refund shall not be eligible for a production refund when used in the manufacturing of products referred to in Annex I to Commission Regulation (EC) No 1722/93(49) or in Annex I to Regulation (EC) No 1265/2001(50).
Article 10
Entry into force
This Regulation shall enter into force subject to and on the date of the entry into force of the Treaty of Accession of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia in the European Union.
It shall apply until 30 April 2007.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 10 November 2003.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 102, 17.4.1999, p. 11.
(2) OJ L 74, 20.3.2003, p. 19.
(3) OJ L 152, 24.6.2000, p. 1.
(4) OJ L 47, 21.2.2003, p. 21.
(5) OJ L 181, 1.7.1992, p. 21.
(6) OJ L 329, 30.12.1995, p. 18.
(7) OJ L 178, 30.6.2001, p. 1.
(8) OJ L 302, 19.10.1992, p. 1.
(9) Except of 1702 30 10.
(10) Except of 1702 40 10.
(11) Limited to 1702 90 10, 1702 90 50, 1702 90 75, 1702 90 79.
(12) Only for goods with a milk content of more than 40 %.
(13) Except of 1702 30 10.
(14) Except of 1702 40 10.
(15) Limited to 1702 90 10, 1702 90 50, 1702 90 75, 1702 90 79.
(16) Only for goods with a milk content of more than 40 %.
(17) Except of 1702 30 10.
(18) Except of 1702 40 10.
(19) Limited to 1702 90 10, 1702 90 50, 1702 90 75, 1702 90 79.
(20) Only for goods with a milk content of more than 40 %.
(21) Except of 1702 30 10.
(22) Except of 1702 40 10.
(23) Limited to 1702 90 10, 1702 90 50, 1702 90 75, 1702 90 79.
(24) Only for goods with a milk content of more than 40 %.
(25) Except of 1702 30 10.
(26) Except of 1702 40 10.
(27) Limited to 1702 90 10, 1702 90 50, 1702 90 75, 1702 90 79.
(28) Only for goods with a milk content of more than 40 %.
(29) Except of 1702 30 10.
(30) Except of 1702 40 10.
(31) Limited to 1702 90 10, 1702 90 50, 1702 90 75, 1702 90 79.
(32) Only for goods with a milk content of more than 40 %.
(33) Except of 1702 30 10.
(34) Except of 1702 40 10.
(35) Limited to 1702 90 10, 1702 90 50, 1702 90 75, 1702 90 79.
(36) Only for goods with a milk content of more than 40 %.
(37) Except of 1702 30 10.
(38) Except of 1702 40 10.
(39) Limited to 1702 90 10, 1702 90 50, 1702 90 75, 1702 90 79.
(40) Except of 1702 30 10.
(41) Except of 1702 40 10.
(42) Limited to 1702 90 10, 1702 90 50, 1702 90 75, 1702 90 79.
(43) Only for goods with a milk content of more than 40 %.
(44) Except of 1702 30 10.
(45) Except of 1702 40 10.
(46) Limited to 1702 90 10, 1702 90 50, 1702 90 75, 1702 90 79.
(47) Only for goods with a milk content of more than 40 %.
(48) OJ L 62, 7.3.1980, p. 5.
(49) OJ L 159, 1.7.1993, p. 112.
(50) OJ L 178, 30.6.2001, p. 63.
