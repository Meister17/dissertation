*****
COMMISSION REGULATION (EEC) No 27/85
of 4 January 1985
laying down detailed rules for the application of Regulation (EEC) No 2262/84 laying down special measures in respect of olive oil
THE COMMISSION OF THE EUROPEAN
COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 2262/84 of 17 July 1984 laying down special measures in respect of olive oil (1), and in particular Article 5 thereof,
Whereas, under Article 1 of Regulation (EEC) No 2262/84, each Member State producing more than a minimal quantity of olive oil is to set up an agency for the purpose of carrying out certain inspection work and other duties in connection with the olive oil production aid scheme; whereas the agency must be in a position to carry out the tasks specified in that Regulation; whereas each agency must therefore possess the minimum powers required to discharge these tasks;
Whereas, in order to guarantee correct and effective application of the production aid scheme, Article 1 (3) of Regulation (EEC) No 2262/84 specifies that the agency shall be given full powers by the Member State concerned to carry out its tasks; whereas to this end each Member State concerned must confer on the agency's inspectors powers to enter all premises and land used by persons subject to inspection for their professional activities, and to demand such information and make such verifications as are necessary for accomplishment of the agency's tasks;
Whereas the Member States concerned must take all measures necessary to safeguard the rights of persons subject to inspection whose interests may be affected thereby;
Whereas the agency will carry out its work in the framework of a work schedule and budget drawn up by the Member State concerned acting on a proposal from the agency and after consulting the Commission; whereas the minimum content of the schedule and size of the budget, and also the procedure to be followed for their establishment and possible adjustment, should therefore be laid down;
Whereas, under Article 1 (4) of Regulation (EEC) No 2262/84, the Commission shall regularly monitor the work of the agencies; whereas a procedure should consequently be laid down by which the Commission will be kept informed of the progress of this work;
Whereas the Community will for the first three marketing years contribute to the actual expenditure of the agencies; whereas procedures for this financing operation should therefore be laid down and also procedures for any verification work in connection with it;
Whereas in view of the time required to set up the inspection agencies in producer Member States special arrangements should be made for the 1984/85 marketing year;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Oils and Fats,
HAS ADOPTED THIS REGULATION:
Article 1
1. In accordance with Article 1 (1) of Regulation (EEC) No 2262/84 each producer Member State shall, not later than 31 March 1985, set up an agency for the purpose of carrying out the checks and duties specified in that Regulation.
2. Member States whose production, calculated on the basis of the average quantity eligible for production aid during the last four marketing years, does not exceed 3 000 tonnes, are not required to set up an agency.
Article 2
1. Each agency shall be granted the legal powers required in order to carry out its duties.
2. Each agency shall, within the framework of the work schedule and budget referred to in Article 1 (4) of Regulation (EEC) No 2262/84, be given autonomy as regards the recruitment of staff and the planning of its work, and all expenditure arising therefrom.
3. The number of staff on the agency's payroll, their level of training and experience, the resources made available to them and the manner in which the agency's departments are organized shall be such as to permit the duties referred to in Article 1 (2) of Regulation (EEC) No 2262/84 to be carried out. More specifically, personnel responsible for control shall have the
necessary technical knowledge and experience to enable them to carry out the checks specified in Council Regulation (EEC) No 2261/84 (1) and Commission Regulation (EEC) No 3061/84 (2), in particular as regards the assessment of agronomic data, technical checks on mills and the scrutiny of stock records and accounts.
4. The official shall, for the purpose of carrying out the duties assigned to them pursuant to Regulation (EEC) No 2262/84, be given appropriate powers by the Member State concerned to obtain any information or evidence and carry out any checks which may be necessary in respect of producers, producers' organizations and associations thereof and approved mills, and in particular:
(a) the authority to check books and other business records;
(b) the authority to make copies of or take extracts from business records;
(c) the right to request that information be given verbally, on the spot;
(d) the right of access to any business premises or land covered by the control arrangements.
Member States shall take whatever measures are necessary to safeguard the rights vested by their national legal orders in those natural and legal persons who are subject to inspection.
Member States shall recognize the officials' findings as having conclusive force under their national legal orders.
Article 3
1. From the 1985/86 marketing year onwards the agency shall propose a work schedule and a budget estimate in respect of each marketing year. The work schedule shall, without prejudice to the conditions laid down in Regulations (EEC) No 2261/84 and (EEC) No 3061/84, be drawn up on the basis of the representativeness of the operators covered by the control arrangements. representativeness Priority shall, however, be given to any area of activity or region in which there is a major risk of irregularities.
2. The work schedule shall include:
(a) a plan for the use of the data in the computerized files specified in Article 16 of Regulation (EEC) No 2261/84; such data shall include information arising from the register of olive cultivation;
(b) an outline of the inspection work that the agency plans to carry out in respect of:
- producers' organizations,
- associations of producers' organizations, and
- approved mills,
together with a description of the way in which those checks are to be carried out;
(c) a schedule of the work to be carried out by the agency with a view to determining yields of olives and olive oil;
(d) a description of the investigations to be conducted as regards the destination of oil obtained by pressing olives and the by-products thereof, and statistical surveys of olive oil production, processing and consumption;
(e) a list of any other work to be carried out at the request of the Member State or the Commission pursuant to the second and third subparagraph of Article 1 (2) of Regulation (EEC) No 2262/84;
(f) details of the training which the agency intends to provide for its staff;
(g) a list of the officials responsible for liaising with the Commission.
The agency shall, moreover, indicate the estimated number of man-days required in respect of each area of activity in the work schedule.
3. The agency's budget shall include the following headings, each of which must be dealt with in a sufficiently detailed manner:
1. List of posts;
2. Expenditure on staff;
3. Administrative expenditure;
4. Expenditure on individual projects;
5. Spending on investment;
6. Other expenditure;
7. Income from the Member State concerned;
8. Contribution by the Community, by virtue of the second subparagraph of Article 1 (5) of Regulation (EEC) No 2262/84;
9. Income pursuant to the first subparagraph of Article 1 (5) of Regulation (EEC) No 2262/84;
10. Other income.
4. The agency shall, for the purposes of drawing up the draft work schedule and budget estimate, take into account the frequency of checks required by Community legislation, the experience gained in previous marketing years and, without prejudice to the responsibilities of the Member State concerned, any observations or comments made by the Commission before it is drawn up.
Article 4
1. Not later than 15 August each year, the agency shall submit its draft work schedule and budget estimate to the Member State concerned. The latter shall, on the basis of the draft, draw up the work schedule and budget estimate and shall submit them to the Commission not later than 15 September each year.
The Commission may, within 30 days, and without prejudice to the responsibilities of the Member State concerned, request that the latter introduce any change in the budget and the work schedule that the Commission considers available for the purposes of satisfactory operation of the production aid scheme.
2. The agency's definitive work schedule and budget shall be adopted by the Member State concerned not later than 31 October each year and shall be forwarded to the Commission forthwith.
3. Subject to Commission approval and provided the overall amount entered in the budget does not increase as a result, the Member State may, with a view to making the checks more effective, amend the agency's work schedule and budget during the marketing year.
However, should an exceptional situation arise in which there is a risk of fraud seriously endangering proper operation of the production aid scheme, the agency shall inform the Member State in question and the Commission. In this case, the agency may modify its plan and the inspection work after having obtained the agreement of the Member State in question. That Member State shall inform the Commission without delay.
If, during a marketing year, the agency is asked by the Member State or the Commission to carry out specific investigations, the work schedule and the budget shall be amended accordingly. The procedure laid down in paragraphs 1 and 2 shall apply mutatis mutandis to the introduction of such amendments.
Article 5
1. In order that Commission officials may, as laid down in Article 1 (4) of Regulation (EEC) No 2262/84, monitor the work carried out by the agency, the agency shall, not later than the 15th day of each month, submit to the Member State concerned and the Commission the schedule of its management and control work for the following month. The agency shall, as soon as possible, also notify the Commission and the Member State concerned of any change in the implementation of the monthly work schedule.
2. The agency shall, not later than 30 days after the end of each quarter, submit to the Member State and Commission a summary report on the work carried out by the agency, together with a financial statement showing the cash-flow situation and the expenditure incurred by the agency in respect of each budget chapter.
3. Representatives of the Commission, of the Member State concerned and of the agency shall at least quarterly to consider the work carried out by the agency and that which it intends to carry out in the future.
Article 6
1. The Member State concerned shall, not later than 31 May each year, transmit to the Commission the revenue and expenditure account for the preceding marketing year accompanied by a report from the governmental authority responsible for supervision of the agency.
2. Not later than three months after that date the Commission shall take a decision on the amount representing the agency's actual expenditure that is to be granted to the producer Member State in respect of the year in question. The said amount, less the advance payments referred to in paragraph 4 and in Article 7 (3), shall be paid once it is established that the agency has performed the work assigned to it.
3. For the purpose of scrutiny of the revenue and expenditure account, Commission officials shall be entitled to have access to the agencies' financial records and supporting documents.
4. The amount representing the agency's operating expenditure during a marketing year shall be agreed by the Commission and the Member State concerned on the basis of the agency's budget estimate. The Commission may, however, alter the monthly instalments in the light of the agency's rate of spending as calculated on the basis of the figures in the quarterly reports referred to in Article 5 (2).
Article 7
1. The draft work schedule and the budget estimate for 1984/85 shall be drawn up by the Member States concerned in accordance with Article 3 (2) and (3) and shall be forwarded to the Commission not later than 28 February 1985.
The draft work schedule shall include the agency's staff recruitment plan for the marketing year concerned.
The proposed activities of the agency, including inspection work, must be consonant with the recruitment plan and with the training programme scheduled. Member States shall on the same occasion transmit to the Commission the proposed statute for the agency. This must include a staff recruitment procedure that provides sufficient guarantees that the aims set out in Article 2 (3) will be achieved.
Within 30 days the Commission may request the Member State, without prejudice to the responsibilities of the latter, to make any change in the budget or schedule which it considers appropriate and shall submit its comments, if any, on the statute.
2. The work schedule and budget for 1984/85 shall be adopted by the Member State concerned not later than 30 April 1985.
3. After receiving the 1984/85 draft work schedule and budget estimate the Commission may, on the basis of the latter and in order to facilitate the setting up of the agency, advance to the Member States the amount representing the cost of setting up the agency. The agency shall, after being formally set up, be entitled to receive the monthly advance payments in respect of operating costs which are referred to in Article 6 (4).
Article 8
Until such time as the agency is in a position to perform all the other duties and checks assigned to it, producer Member States shall at least carry out the checks laid down in Regulation (EEC) No 2261/84, Commission Regulation (EEC) No 2711/84 (1) and Regulation (EEC) No 3061/84 in accordance with existing procedures.
Article 9
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Communities.
The provisions of Articles 4, 5 and 6 shall be applicable only to the work schedule programmes and budgets relative to the 1984/85, 1985/86 and 1986/87 marketing years.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 4 January 1984.
For the Commission
Poul DALSAGER
Member of the Commission
(1) OJ No L 208, 3. 8. 1984, p. 11.
(1) OJ No L 208, 3. 8. 1984, p. 3.
(2) OJ No L 288, 1. 11. 1984, p. 52.
(1) OJ No L 258, 27. 9. 1984, p. 12.
