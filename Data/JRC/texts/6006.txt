Action brought on 26 September 2005 — Genette v Commission
Parties
Applicant(s): Genette (Gorze, France) (represented by: M.-A. Lucas, lawyer)
Defendant(s): Commission of the European Communities
Form of order sought
The applicant(s) claim(s) that the Court should:
- annul the decision of the Head of the "Pensions" Unit of 25 January 2005 rejecting the applicant's application of 31 October 2004 for the transfer of his pension rights acquired in Belgium (No D/1106/2004);
- annul the decision of the Director General of the ADMIN DG of 10 June 2005 dismissing the applicant's complaint of 22 April 2005 against the decision of the Head of the "Pensions" Unit of 2 February 2005 rejecting his application of 31 October 2004;
- order the defendant to pay the costs.
Pleas in law and main arguments
Following an application by the applicant, a Commission official, the pension rights he had acquired in Belgium were transferred to the Community scheme in 2002, in accordance with the provisions of a Belgian law on that subject adopted in 1991. In 2003, Belgium adopted a new law governing those transfers, whose provisions, according to the applicant, were more advantageous for him.
The 1991 law provided for the possibility to withdraw the transfer application, with the institution's agreement. The applicant therefore submitted an application for the Commission to record its agreement on the withdrawal of the application he had submitted under the regime governed by the 1991 law, so that he might, subsequently, submit a new application governed by the 2003 law. That application was rejected by the contested decision, on the ground that the Community provisions do not provide for the possibility to withdraw an application.
By his action, the applicant challenges the rejection of his application. He points out a number of obvious errors in the assessment of the subject of his application, the final nature of the decisions called into question by his application, the existence of new and substantial facts and the time-limit for submitting the application. He also alleges infringement of Article 11(2) of Annex VIII to the Staff Regulations, and of the general provisions for its implementation. In addition, the applicant submits that the contested decisions infringe his fundamental right to effective judicial protection and the duty of assistance laid down in Article 24 of the Staff Regulations.
Lastly, the applicant pleads that the Belgian law of 1991 conflicts with Community law, specifically Article 11(2) of Annex VIII and the principle of equal treatment.
--------------------------------------------------
