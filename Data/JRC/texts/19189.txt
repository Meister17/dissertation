Commission Decision
of 16 June 2003
amending Decision 2002/975/EC on introducing vaccination to supplement the measures to control infections with low pathogenic avian influenza in Italy and on specific movement control measures
(notified under document number C(2003) 1834)
(Text with EEA relevance)
(2003/436/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 90/425/EEC of 26 June 1990 concerning veterinary and zootechnical checks applicable in intra-Community trade in certain live animals and products with a view to the completion of the internal market(1), as last amended by Directive 2002/33/EC of the European Parliament and of the Council(2), and in particular Article 10(4) thereof,
Having regard to Council Directive 89/662/EEC of 11 December 1989 concerning veterinary checks in intra-Community trade with a view to the completion of the internal market(3), as last amended by Regulation (EC) No 806/2003(4), and in particular Article 9(4) thereof,
Having regard to Council Directive 92/40/EEC of 19 May 1992 introducing Community measures for the control of avian influenza(5), as last amended by Regulation (EC) No 806/2003, and in particular Article 16 thereof,
Whereas:
(1) During 1999 and 2000 Italy has experienced outbreaks of highly pathogenic avian influenza of subtype H7N1 with devastating economic losses for the poultry industry. Prior to the epidemic a virus of low pathogenicity had been circulating in the area.
(2) During monitoring for avian influenza the presence of low pathogenic avian influenza virus of subtype H7N3 was detected in the regions of Veneto and Lombardia in October 2002.
(3) In order to control the spread of infection with low pathogenic avian influenza virus the Commission has approved a vaccination programme by Commission Decision 2002/975/EC(6).
(4) The results of the vaccination programme reported at several meetings of the Standing Committees on the Food Chain and Animal Health are generally favourable in view of the control of the disease within the vaccination zone. However, the infection has spread to some areas adjacent to the established vaccination zone.
(5) The vaccination zone should therefore be extended to cover areas at risk for the propagation of the virus while the stringent monitoring measures and trade restrictions are applied accordingly.
(6) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
In Decision 2002/975/EC Annex I shall be replaced by the Annex to this Decision.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 16 June 2003.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 224, 18.8.1990, p. 29.
(2) OJ L 315, 19.11.2002, p. 14.
(3) OJ L 395, 30.12.1989, p. 13.
(4) OJ L 122, 16.5.2003, p. 1.
(5) OJ L 167, 22.6.1992, p. 1.
(6) OJ L 337, 13.12.2002, p. 87.
ANNEX
"ANNEX I
VACCINATION ZONE
Veneto Region
Verona Province
The vaccination zone comprises the territory of the following municipalities:
Albaredo d'Adige
Angiari
Arcole
Belfiore
Bevilacqua
Bonavigo
Boschi Sant'Anna
Bovolone
Bussolengo
Buttapietra
Calmiero area to the south of the A4 motorway
Casaleone
Castel d'Azzano
Castelnuovo del Garda area to the south of the A4 motorway
Cerea
Cologna Veneta
Colognola ai Colli area to the south of the A4 motorway
Concamarise
Erbè
Gazzo Veronese
Isola della Scala
Isola Rizza
Lavagno area to the south of the A4 motorway
Minerbe
Monteforte d'Alpone area to the south of the A4 motorway
Mozzecane
Nogara
Nogarole Rocca
Oppeano
Palù
Pescantina
Peschiera del Garda area to the south of the A4 motorway
Povegliano Veronese
Pressana
Ronco all'Adige
Roverchiara
Roveredo di Guà
S. Bonifacio area to the south of the A4 motorway
S. Giovanni Lupatoto area to the south of the A4 motorway
S. Martino Buon Albergo area to the south of the A4 motorway
S. Pietro di Morubio
Salizzole
Sanguinetto
Soave area to the south of the A4 motorway
Sommacampagna
Sona
Sorgà
Trevenzuolo
Valeggio sul Mincio
Verona area to the south of the A4 motorway
Veronella
Vigasio
Villafranca di Verona
Zevio
Zimella
Vicenza Province
The vaccination zone comprises the territory of the following municipalities:
Agugliaro
Albettone
Alonte
Asigliano Veneto
Barbarano Vicentino
Campiglia dei Berici
Castegnero
Lonigo
Montegalda
Montegaldella
Mossano
Nanto
Noventa Vicentina
Orgiano
Poiana Maggiore
S. Germano dei Berici
Sossano
Villaga
Padova Province
The vaccination zone comprises the territory of the following municipalities:
Carceri
Casale di Scodosia
Este
Lozzo Atestino
Megliadino S. Fidenzio
Megliadino S. Vitale
Montagnana
Ospedaletto Euganeo
Ponso
S. Margherita d'Adige
Saletto
Urbana
Lombardia Region
Mantova Province
The vaccination zone comprises the territory of the following municipalities:
Acquanegra Sul Chiese
Asola
Bigarello
Canneto Sull'oglio
Casalmoro
Casaloldo
Casalromano
Castel D'ario
Castel Goffredo
Castelbelforte
Castiglione Delle Stiviere
Cavriana
Ceresara
Gazoldo Degli Ippoliti
Goito
Guidizzolo
Mariana Mantovana
Marmirolo
Medole
Monzambano
Piubega
Ponti Sul Mincio
Porto Mantovano
Redondesco
Rodigo
Roncoferraro
Roverbella
San Giorgio Di Mantova
Solferino
Villimpenta
Volta Mantovana
Brescia Province
The vaccination zone comprises the territory of the following municipalities:
Acquafredda
Alfianello
Azzano Mella
Bagnolo Mella
Barbariga
Bassano Bresciano
Berlingo
Borgo San Giacomo
Borgosatollo
Brandico
Brescia area to the south of the A4 motorway
Calcinato area to the south of the A4 motorway
Calvisano
Capriano del Colle
Carpendolo
Castegnato area to the south of the A4 motorway
Castel Mella
Castelcovati
Castenedolo area to the south of the A4 motorway
Castrezzato
Cazzago San Martino
Chiari
Cigole
Boccaglio
Cologne
Comezzano-Cizzago
Corzano
Dello
Desenzano del Garda area to the south of the A4 motorway
Erbusco area to the south of the A4 motorway
Fiesse
Flero
Gambara
Ghedi
Gottolengo
Isorella
Leno
Lograto
Lonato area to the south of the A4 motorway
Longhena
Maclodio
Mairano
Manerbio
Milzano
Montichiari
Montirone
Offlaga
Orzinuovi
Orzivecchi
Ospitaletto area to the south of the A4 motorway
Palazzolo sull'Oglio area to the south of the A4 motorway
Pavone del Mella
Pompiano
Poncarale
Pontevico
Pontoglio
Pozzolengo area to the south of the A4 motorway
Pralboino
Quinzano d'Oglio
Remedello
Rezzato area to the south of the A4 motorway
Roccafranca
Roncadelle area to the south of the A4 motorway
Rovato area to the south of the A4 motorway
Rudiano
San Gervasio Bresciano
San Paolo
San Zeno Naviglio
Seniga
Torbole Casaglia
Travagliato
Trenzano
Urago d'Oglio
Verolanuova
Verolavecchia
Villachiara
Visano
Bergamo Province
The vaccination zone comprises the territory of the following municipalities:
Antegnate
Bagnatica area to the south of the A4 motorway
Barbata
Bariano
Bolgare area to the south of the A4 motorway
Calcinate
Calcio
Castelli Calepio area to the south of the A4 motorway
Cavernago
Cividale al Piano
Cologno al Serio
Cortenuova
Costa di Mezzate area to the south of the A4 motorway
Covo
Fara Olivana con Sola
Fontanella
Ghisalba
Grumello del Monte area to the south of the A4 motorway
Isso
Martinengo
Morengo
Mornico al Serio
Pagazzano
Palosco
Pumenengo
Romano di Lombardia
Seriate area to the south of the A4 motorway
Telgate area to the south of the A4 motorway
Torre Pallavicina
Cremona Province
The vaccination zone comprises the territory of the following municipalities:
Camisano
Casale Cremasco-Vidolasco
Casaletto di Sopra
Castel Gabbiano
Soncino".
