COMMISSION REGULATION (EC) No 3175/94 of 21 December 1994 laying down detailed rules of application for the specific arrangements for the supply of cereal products to the smaller Aegean islands and establishing the forecast supply balance
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2019/93 of 19 July 1993 introducing specific measures for the smaller Aegean islands concerning certain agricultural products (1), as amended by Commission Regulation (EC) No 822/94 (2), and in particular
Article 4
thereof,
Whereas Commission Regulation (EEC) No 2958/93 (3), lays down common detailed rules for the implementation of the specific arrangements for the supply of certain agricultural products to the smaller Aegean islands;
Whereas, in order to take account of commercial practices specific to the cereals sector, detailed rules, additional to or derogating from the provisions of Regulation (EEC) No 2958/93, should be laid down;
Whereas, pursuant to Article 2 of Regulation (EEC) No 2019/93, the forecast supply balance for cereal products for the smaller Aegean islands for 1995 should be established; whereas that balance may be revised during the year on the basis of trends in the smaller islands' requirements;
Whereas, to ensure effective management of the supply arrangements, a timetable for the submission of licence applications should be established and the period of validity of aid certificates fixed;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Cereals,
HAS ADOPTED THIS REGULATION:
Article 1
Pursuant to Article 2 of Regulation (EEC) No 2019/93, the forecast supply balance for cereal products originating in the Community for the smaller Aegean islands for 1995 shall be as laid down in the Annex hereto.
Article 2
The validity of aid certificates shall expire on the final day of the second month following their month of issue.
Article 3
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Communities.
It shall apply from 1 January 1995.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 21 December 1994.
For the Commission
René STEICHEN
Member of the Commission
(1) OJ No L 184, 27. 7. 1993, p. 1.
(2) OJ No L 95, 14. 4. 1994, p. 1.
(3) OJ No L 267, 28. 10. 1993, p. 4.
ANNEX
Supply balance for cereals for the smaller Aegean islands for 1995 "(tonnes)"" ID="1">Grain cereals> ID="2">1001, 1002, 1003, 1004 and 1005> ID="3">10 000> ID="4">30 750"> ID="1">Wheat flour> ID="2">1001 and 1002> ID="3">10 000> ID="4">30 750"> ID="1">Food industry residues and waste> ID="2">2302 to 2308> ID="3">1 000> ID="4">16 500"> ID="1">Preparations of a kind used in animal feeding> ID="2">2309 90> ID="3">1 000> ID="4">6 500"> ID="1">Total > ID="2">22 000> ID="3">84 500"> ID="1">Grand total > ID="2">106 500">
These groups are defined in Annexes I and II to Regulation (EEC) No 2958/93.
