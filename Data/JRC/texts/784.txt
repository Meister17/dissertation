COUNCIL DECISION
of 13 March 2000
amending Decision 1999/70/EC concerning the external auditors of the national central banks
(2000/223/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Statute of the European System of Central Banks and of the European Central Bank and in particular to Article 27(1) thereof,
Having regard to the recommendation of the European Central Bank (hereinafter referred to as the ECB) of 21 February 2000,
Whereas:
(1) The accounts of the ECB and of the national central banks are to be audited by independent external auditors recommended by the Governing Council of the ECB and approved by the Council of the European Union.
(2) The Governing Council of the ECB recommended the Council to approve the replacement, starting from the financial year 2000, of one of the external auditors for the Deutsche Bundesbank who were approved by the Council in its Decision 1999/70/EC(1).
(3) It is appropriate to follow the recommendation of the Governing Council,
HAS DECIDED AS FOLLOWS:
Article 1
Article 1(2) of Decision 1999/70/EC shall be replaced by the following:
"2. KPMG Deutsche Treuhand-Gesellschaft AG and Ernst & Young Deutsche Allgemeine Treuhand AG are hereby approved as the external auditors of the Deutsche Bundesbank for the annual accounts starting from the financial year 2000."
Article 2
This Decision shall be notified to the ECB.
Article 3
This Decision shall be published in the Official Journal of the European Communities.
Done at Brussels, 13 March 2000.
For the Council
The President
J. PINA MOURA
(1) OJ L 22, 29.1.1999, p. 69.
