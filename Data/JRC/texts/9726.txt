Commission Decision
of 21 December 2005
relating to a proceeding under Article 81 of the Treaty establishing the European Community and Article 53 of the EEA Agreement against Flexsys NV, Bayer AG, Crompton Manufacturing Company Inc. (former Uniroyal Chemical Company Inc.), Crompton Europe Ltd, Chemtura Corporation (former Crompton Corporation), General Química SA, Repsol Química SA and Repsol YPF SA.
(Case No COMP/F/C.38.443 — Rubber chemicals)
(notified under document number (2005) 5592)
(only the English, German and Spanish texts are authentic)
(Text with EEA relevance)
(2006/902/EC)
1. Summary of the infringement
1.1. Addressees
(1) This decision is addressed to the following undertakings:
- Flexsys N.V.;
- Bayer AG;
- Crompton Manufacturing Company, Inc. (former Uniroyal Chemical Company Inc.);
- Crompton Europe Ltd;
- Chemtura Corporation (former Crompton Corporation);
- General Química SA;
- Repsol Química SA;
- Repsol YPF SA.
(2) The addressees of the Decision participated in a single, complex and continuous infringement of Article 81 of the Treaty establishing the European Community and of Article 53 of the Agreement on the European Economic Area, involving the fixing of prices and the exchange of confidential information concerning certain rubber chemicals (antioxidants, antiozonants and primary accelerators) in the EEA and worldwide markets.
1.2. The rubber chemicals sector
(3) Rubber chemicals are synthetic or organic chemicals that act as productivity and quality enhancers in the manufacture of rubber, mainly used in vehicle tires. In 2001, the EEA market value was estimated at EUR 200 million, covering the categories antiozonants, antioxidants and primary accelerators that were affected by the cartel.
(4) The major global producers of rubber chemicals are Flexsys, Bayer and Chemtura (former Crompton), accounting together for approximately a half of the world-wide rubber chemical market. There are a number of significant smaller competitors, such as General Química (Spain), Duslo (Slovakia), Istrochem (Slovakia), Noveon (USA) and Great Lakes (USA), as well as many minor competitors particularly in Asia.
(5) The major customers for rubber chemicals are the globally operating big tire companies Michelin (France), Goodyear (USA), Bridgestone/Firestone (Japan), Continental (Germany) and Pirelli (Italy), accounting together for about 35-40 % of world-wide consumption.
(6) The geographic scope of rubber chemicals business changed gradually from regional to global in the mid-1990s. This affected the scope of the cartel as well, so that after 1995 the parties reached understandings mostly about world-wide price increases.
1.3. Functioning of the cartel
(7) Whilst there are a number of indications that collusive activities within the rubber chemicals industry were already taking place at least occasionally in the 1970s, the Commission only has sufficiently firm evidence of the existence of the cartel for the period covering the years 1996-2001 for Flexsys, Bayer and Crompton (now Chemtura) (including Crompton Europe and Uniroyal Chemical Company). These undertakings agreed to raise prices of certain rubber chemicals (antioxidants, antiozonants and primary accelerators) in the EEA and world-wide markets at least in 1996, 1998, 1999, 2000 and 2001. General Química, which must be considered a fringe player, participated to these agreements in 1999 and 2000.
(8) Coordination of price increases normally followed a general pattern, involving contacts among the competitors during a preparatory phase preceding the announcement to customers, thereafter during the negotiations with customers, and lastly after the contracts had been made to monitor compliance and success on the market. During the contacts preceding the coordinated action, the parties sought support for a suggested price increase and agreed upon its amount, the products and territory covered, as well as the leader and the timing of the announcements. During the implementation phase, the focus was on the customers’ reactions to the announced price increases and exchanges on the positions regarding price negotiations with the customers. The follow-up contacts included typically the exchange of detailed information on contracted volumes and prices with specific customers.
1.4. Procedure
(9) The investigation into the rubber chemicals sector was initiated as a result of an application for conditional immunity from fines by Flexsys in April 2002, which was granted in June 2002. Subsequently, the Commission carried out inspections at the premises of Bayer, Crompton Europe and General Química in September 2002.
(10) Crompton (now Chemtura), Bayer and General Química applied for leniency, on 8 October 2002, 24 October 2002 and 7 June 2004, respectively. The Commission informed, in due course, all the applicants of its intention to apply reductions of fines.
(11) On 12 April 2005, the Commission adopted a Statement of Objections against Bayer, Crompton, Crompton Europe, Uniroyal Chemical Company, Flexsys, Akzo Nobel, Pharmacia (former Monsanto), General Química, Repsol Química, Repsol YPF, Duslo, Prezam, Vagus and Istrochem. An Oral Hearing on the case was held on 18 July 2005. The proceedings were subsequently closed against Akzo Nobel NV, Pharmacia Corporation, Duslo a.s., Prezam a.s., Vagus a.s., and Istrochem a.s.
1.5. Liabilities
(12) Repsol YPF SA and Repsol Química SA, although they did not participate themselves in the arrangements in question, are nevertheless held responsible for the conduct of their wholly owned subsidiary General Química.
2. Fines
2.1. Basic Amount
(13) The basic amount of the fine is determined according to the gravity and duration of the infringement.
2.1.1. Gravity
(14) In assessing the gravity of the infringement, the Commission takes account of its nature, its actual impact on the market, where this can be measured, and the size of the relevant geographic market.
(15) Considering the nature of the infringement and its geographic scope (the infringement in this case consisted primarily of secret collusion between cartel members to fix prices in the EEA and elsewhere, supported by the exchange of confidential information), the infringement must be qualified as very serious.
2.1.2. Differential treatment
(16) Within the category of very serious infringements, the scale of likely fines makes it possible to apply differential treatment to undertakings in order to take account of the effective economic capacity of the offenders to cause significant damage to competition, as well as to set the fine at a level which ensures that it has sufficient deterrent effect.
(17) Based on the fact that both the geographic scope of the cartel and the rubber chemicals business in general is essentially world-wide, the global market shares in 2001, the last full year of the infringement, are used as reference values in the calculation of the fines.
(18) Flexsys was the largest market operator in the world, with a market share of approximately [20-30] %. It is therefore placed in a first category. Bayer, with a market share of approximately [10-20] %, is placed in a second category. Crompton, with a market share of approximately [10-20] %, is placed in a third category. Finally, General Química, with a market share of approximately [0-10] %, is placed in a fourth category. The starting amounts will be fixed proportionally, albeit not arithmetically, having regard to the market shares.
2.1.3. Sufficient deterrence
(19) Within the category of very serious infringements, the scale of likely fines also makes it possible to set the fines at a level which ensures that they have sufficient deterrent effect, taking into account the size of each undertaking. In 2004, the total turnovers of the undertakings were as follows: Bayer EUR 29,7 billion; Crompton approximately EUR 2 billion; Flexsys EUR approximately 425 million and Repsol YPF 41,7 billion. Accordingly, the Commission considers it appropriate to multiply the fine for Bayer by 2 and for Repsol by 2,5.
2.1.4. Increase for duration
(20) Flexsys, Bayer and Uniroyal (including Crompton Europe) committed an infringement of six years, whereas Crompton Corporation (now Chemtura) is liable for an infringement of five years and four months. All of these undertakings committed an infringement of long duration, and their starting amounts will consequently be increased by 10 % for each full year of infringement.
(21) General Química committed an infringement of eight months. Its infringement amounting to less than one year, no increase will be applied to its fine.
2.2. Attenuating circumstances
(22) In the case of General Química, it is appropriate to reduce its fine due to its passive and minor role in the infringement, as compared to the other participants in the cartel, by 50 %.
2.3. Application of the 2002 Leniency Notice
2.3.1. Immunity
(23) Flexsys was the first to submit evidence which enabled the Commission to adopt a decision to carry out an investigation in connection with the alleged cartel in the rubber chemicals industry. Flexsys has co-operated fully, on a continuous basis and expeditiously throughout the Commission's administrative procedure and provided the Commission with all evidence available to it relating to the suspected infringement. Flexsys ended its involvement in the suspected infringement no later than the time at which it submitted evidence under the Leniency Notice and did not take steps to coerce other undertakings to participate in the infringement. Hence, Flexsys qualifies for a full immunity from fines.
(24) Crompton has contested Flexsys’ immunity, claiming inter alia that Flexsys has failed to fulfil the conditions of its immunity by coercing other parties and by continuing the infringement after its application for immunity. After a close investigation of Crompton's allegations, the Commission considers that there is no decisive material evidence to support these allegations.
2.3.2. Point 23 (b), first indent (reduction of 30-50 %)
(25) Crompton was the first undertaking to meet the requirements of point 21 of the Leniency Notice, as it provided the Commission with evidence which represents significant added value with respect to the evidence already in the Commission's possession at the time of its submission. It qualifies, therefore, under point 23 (b), first indent, for a reduction of 30-50 % of the fine.
(26) In view of its early cooperation, the quality of its evidence and its extensive and continuous cooperation throughout the proceedings, the Commission considers that Crompton qualifies for the maximum of 50 % reduction
2.3.3. Point 23 (b), second indent (reduction of 20-30 %)
(27) Bayer was the second undertaking to meet the requirements of point 21 of the Leniency Notice, as it provided the Commission with evidence which represents significant added value with respect to the evidence already in the Commission's possession at the time of its submission. It qualifies, therefore, under point 23 (b), second indent, for a reduction of 20-30 % of the fine. The extent of the value added by Bayer to the case is limited and it has admitted the infringement only for its last four years. Thus, the Commission considers that Bayer qualifies for the very minimum reduction within the relevant band, i.e. a reduction of 20 %.
2.3.4. Point 23 (b), third indent (reduction of up to 20 %)
(28) General Química was the third undertaking to meet the requirements of point 21 of the Leniency Notice, as it provided the Commission with evidence which represents significant added value with respect to the evidence already in the Commission's possession at the time of its submission. General Química qualifies, therefore, under point 23 (b), third indent, for a reduction of up to 20 % of the fine. Considering that General Química fulfilled the condition of significant added value relatively late in the proceedings, over a year and a half after the Commission's inspections to its premises, and that the extent to which its submission added value to the evidence has remained limited, the Commission finds that General Química (and Repsol) is entitled to a 10 % reduction of the fine that would otherwise have been imposed.
2.3.5. Final remark on the application of the leniency notice
(29) In this case, the Commission also issued a strong warning against leniency applicants attempting to weaken its ability to prove the infringement, where, taken together, there is a consistent body of indicia and evidence showing the existence of the cartel. The Commission considered that such attitude puts the extent and continuity of cooperation of leniency applicants into serious doubt.
3. Decision
(30) The following undertakings have infringed Article 81(1) of the Treaty and Article 53 of the EEA Agreement by participating, for the periods indicated, in a complex of agreements and concerted practices consisting of price fixing and the exchange of confidential information in the rubber chemicals sector in the EEA:
a) Bayer AG, from 1 January 1996 until 31 December 2001;
b) Crompton Manufacturing Company Inc., from 1 January 1996 until 31 December 2001;
c) Crompton Europe Ltd., from 1 January 1996 until 31 December 2001;
d) Chemtura Corporation, from 21 August 1996 until 31 December 2001;
e) Flexsys N.V., from 1 January 1996 until 31 December 2001;
f) General Química SA, from 31 October 1999 until 30 June 2000;
g) Repsol Química SA, from 31 October 1999 until 30 June 2000;
h) Repsol YPF SA, from 31 October 1999 until 30 June 2000.
(31) The undertakings listed above shall immediately bring to an end the infringements also referred to above, insofar as they have not already done so. They shall refrain from repeating any act or conduct described above, and from any act or conduct having the same or similar object or effect.
(32) For the infringements referred to above,, the following fines are imposed on the following undertakings:
(a)Flexsys N.V | EUR 0, |
(b)Crompton Manufacturing Company, Inc., jointly and severally with Crompton Europe Ltd. | EUR 13,60 million, |
of which jointly and severally with Chemtura Corporation: | EUR 12,75 million, |
(c)Bayer AG: | EUR 58,88 million, |
(d)General Química SA, jointly and severally with Repsol Química SA and Repsol YPF SA | EUR 3,38 million. |
A non-confidential version of the full text of the Decision can be found in the authentic languages of the case and in the Commission's working languages at the DG Competition website at http://ec.europa.eu/comm/competition/.
--------------------------------------------------
