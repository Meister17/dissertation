Prior notification of a concentration
(Case COMP/M.4353 — Permira/All3Media Group)
(2006/C 205/03)
(Text with EEA relevance)
1. On 18 August 2006, the Commission received a notification of a proposed concentration pursuant to Article 4 of Council Regulation (EC) No 139/2004 [1] by which the Permira Europe III Fund belonging to the group Permira Holdings Ltd ("PHL", UK) acquires within the meaning of Article 3(1)(b) of the Council Regulation control of the whole of the undertaking All3Media Group Limited ("A3M", UK) by way of purchase of shares.
2. The business activities of the undertakings concerned are:
- for undertaking PHL: private equity fund,
- for undertaking A3M: independent production and distribution of television programmes.
3. On preliminary examination, the Commission finds that the notified transaction could fall within the scope of Regulation (EC) No 139/2004. However, the final decision on this point is reserved.
4. The Commission invites interested third parties to submit their possible observations on the proposed operation to the Commission.
Observations must reach the Commission not later than 10 days following the date of this publication. Observations can be sent to the Commission by fax (fax No (32-2) 296 43 01 or 296 72 44) or by post, under reference number COMP/M.4353 — Permira/All3Media Group, to the following address:
European Commission
Directorate-General for Competition
Merger Registry
J-70
B-1049 Bruxelles/Brussel
[1] OJ L 24, 29.1.2004, p. 1.
--------------------------------------------------
