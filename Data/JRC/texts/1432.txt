Commission Decision
of 6 August 2001
amending Decision 94/984/EC as regards the importation of fresh poultry meat from Brazil
(notified under document number C(2001) 2469)
(Text with EEA relevance)
(2001/659/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/494/EEC of 26 June 1991 on animal health conditions governing intra-Community trade in and imports from third countries of fresh poultry meat(1), as last amended by Directive 1999/89/EC(2), and in particular Articles 11 and 12 thereof,
Whereas:
(1) Commission Decision 94/984/EC of 20 December 1994 laying down animal health conditions and veterinary certificates for the importation of fresh poultry meat from certain third countries(3), as last amended by Decision 2001/598/EC(4), provides for veterinary certificates containing two different health attestations, model A and model B, their use depending on the Newcastle disease situation in the country concerned.
(2) Certain regions of Brazil are entitled to use the model A certificate for the exportation of poultry meat to the Community.
(3) Outbreaks of Newcastle disease have affected some non-commercial poultry flocks in the State of Goiás. Therefore this region of Brazil which is authorised to export fresh poultry meat to the Community is no longer free of Newcastle disease.
(4) An inspection carried out by the Commission's services in Brazil in October 2000 to assess the animal health situation and the additional information received from the Brazilian authorities have shown that their disease control measures for outbreaks of Newcastle disease are equivalent to those laid down in Council Directive 92/66/EEC of 14 July 1992 introducing Community measures for the control of Newcastle disease(5), as last amended by the Act of Accession of Austria, Finland and Sweden.
(5) It is appropriate on this basis to continue to allow the importation of fresh poultry meat from this region. Health attestation model A should therefore be amended.
(6) It is appropriate to restrict the scope of this Decision to poultry species covered by Council Directive 71/118/EEC of 15 February 1971 on health problems affecting trade in fresh poultry meat(6), as last amended by Directive 97/79/EC(7), and, if necessary, to lay down the animal health conditions and veterinary certification for other poultry species in a separate Decision.
(7) The measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
Annex II part 2 model A to Decision 94/984/EC is amended in accordance with the Annex to this Decision.
Article 2
This Decision shall apply to fresh poultry meat certified as from 1 September 2001.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 6 August 2001.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 268, 24.9.1991, p. 35.
(2) OJ L 300, 23.11.1999, p. 17.
(3) OJ L 378, 21.12.1994, p. 11.
(4) OJ L 210, 3.8.2001, p. 37.
(5) OJ L 260, 5.9.1992, p. 1.
(6) OJ L 55, 8.3.1971, p. 1.
(7) OJ L 24, 30.1.1998, p. 31.
ANNEX
"
>PIC FILE= "L_2001232EN.002102.TIF">
>PIC FILE= "L_2001232EN.002201.TIF">"
