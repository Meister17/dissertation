Commission Regulation (EC) No 2584/2000
of 24 November 2000
establishing a system for the communication of information on certain supplies of beef, veal and pigmeat by road to the territory of the Russian Federation
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1254/1999 of 17 May 1999 on the common organisation of the market in beef and veal(1) and in particular Articles 33(12) and 41 thereof, and to the corresponding provisions of the other regulations on the common organisation of the markets in agricultural products,
Whereas:
(1) Article 2 of the protocol on mutual administrative assistance for the correct application of customs legislation annexed to the Partnership and Cooperation Agreement between the European Communities and their Member States, of the one part, and the Russian Federation, of the other part(2), provides that the parties are to assist each other in ensuring that customs legislation is correctly applied, in particular by the prevention, detection and investigation of contraventions of that legislation. To implement that administrative assistance, the Commission, represented by the European Anti-Fraud Office (hereinafter referred to as "OLAF") and the Russian authorities have concluded an arrangement establishing a mechanism for the communication of information on movements of goods between the Community and the Russian Federation.
(2) As part of that administrative assistance, specifically in relation to the transport by road of beef, veal and pigmeat products bound for the Russian Federation, the information which operators must forward to the competent authorities of the Member States and the system for communicating that information between the competent authorities of the Member States, OLAF and the Russian authorities must be laid down.
(3) That information and the system of communication introduced will make it possible to trace exports of the products concerned to the Russian Federation and, where appropriate, detect cases in which a refund is not due and must be recovered.
(4) Application of the provisions of this Regulation will be evaluated after a significant period of operation. The review carried out on that basis may, where appropriate, lead to their extension to exports of other products and other means of transport and involve financial consequences where obligations are or are not met.
(5) The measures provided for in this Regulation are in accordance with the opinions of the Management Committees concerned,
HAS ADOPTED THIS REGULATION:
Article 1
This Regulation shall apply to consignments by truck of beef, veal and pigment products falling within CN codes 0201, 0202 and 0203 bound for the territory of the Russian Federation for which the export declarations are accompanied by an export refund application.
This Regulation shall not apply to consignments as referred to in the first subparagraph of a quantity less than 3000 kilograms.
Article 2
Exporters shall communicate to the central body designated by each Member State of export, for each export declaration, within four working days of the date of unloading of the products in Russia, the following information:
(a) a description of the goods, indicating the eight-figure product code of the combined nomenclature;
(b) the export declaration number;
(c) the net quantity in kilograms;
(d) the TIR carnet number or the reference number of the Russian customs transit document;
(e) the Member State of export, the customs office of departure and the date on which the export customs formalities were completed;
(f) the licence number of the warehouse under customs supervision to which the product was delivered in Russia;
(g) the date of delivery of the product to the warehouse under customs supervision in Russia.
Article 3
1. The body in the Member State concerned referred to in Article 2 shall forward the information it receives to OLAF by electronic mail within two working days of the date of receipt.
2. The information referred to in Article 2, and an identification number for each export operation, shall be sent by OLAF to the Russian customs authorities upon receipt.
3. OLAF shall inform the central body in the Member State concerned, as appropriate, of the Russian customs authorities' reply, within two working days of receipt of that reply; or of the failure by those authorities to reply, within two working days of the end of the three-week period laid down for a reply by the Russian authorities under the administrative arrangement concluded with them.
Article 4
1. The information referred to in Articles 1 and 2 shall not constitute additional requirements to those laid down for the grant of export refunds in the sectors concerned.
2. The information communication system established by this Regulation shall be evaluated after an effective period of application of six months.
Article 5
1. This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Communities.
2. It shall apply to consignments for which the export declarations referred to in Article 1 are accepted from 1 February 2001.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 24 November 2000.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 160, 26.6.1999, p. 21.
(2) OJ L 327, 28.11.1997, p. 48.
