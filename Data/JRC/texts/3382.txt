Euro exchange rates [1]
16 November 2005
(2005/C 285/02)
| Currency | Exchange rate |
USD | US dollar | 1,1677 |
JPY | Japanese yen | 139,40 |
DKK | Danish krone | 7,4571 |
GBP | Pound sterling | 0,67690 |
SEK | Swedish krona | 9,6126 |
CHF | Swiss franc | 1,5469 |
ISK | Iceland króna | 72,48 |
NOK | Norwegian krone | 7,8135 |
BGN | Bulgarian lev | 1,9559 |
CYP | Cyprus pound | 0,5734 |
CZK | Czech koruna | 29,283 |
EEK | Estonian kroon | 15,6466 |
HUF | Hungarian forint | 250,63 |
LTL | Lithuanian litas | 3,4528 |
LVL | Latvian lats | 0,6964 |
MTL | Maltese lira | 0,4293 |
PLN | Polish zloty | 3,9845 |
RON | Romanian leu | 3,6387 |
SIT | Slovenian tolar | 239,50 |
SKK | Slovak koruna | 38,730 |
TRY | Turkish lira | 1,5868 |
AUD | Australian dollar | 1,6026 |
CAD | Canadian dollar | 1,3961 |
HKD | Hong Kong dollar | 9,0565 |
NZD | New Zealand dollar | 1,7087 |
SGD | Singapore dollar | 1,9895 |
KRW | South Korean won | 1211,37 |
ZAR | South African rand | 7,9063 |
CNY | Chinese yuan renminbi | 9,4391 |
HRK | Croatian kuna | 7,3550 |
IDR | Indonesian rupiah | 11688,68 |
MYR | Malaysian ringgit | 4,414 |
PHP | Philippine peso | 63,721 |
RUB | Russian rouble | 33,7130 |
THB | Thai baht | 48,094 |
[1] Source: reference exchange rate published by the ECB.
--------------------------------------------------
