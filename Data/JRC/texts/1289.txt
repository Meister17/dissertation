Commission Decision
of 25 October 2001
amending Decision 93/197/EEC with regard to importation of equidae from the Falkland Islands
(notified under document number C(2001) 3198)
(Text with EEA relevance)
(2001/766/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 90/426/EEC of 26 June 1990 on animal health conditions governing the movement and imports from third countries of equidae(1), as last amended by Decision 2001/298/EC(2), and in particular Article 13(2), Article 15(a), Article 16(2) and Article 19(i) thereof,
Whereas:
(1) The Falkland Islands are included in Part 1 of the Annex to Council Decision 79/542/EEC of 21 December 1976 drawing up a list of third countries from which the Member States authorise imports of bovine animals, swine, equidae, sheep and goats, fresh meat and meat products, as last amended by Commission Decision 2001/731/EC(3), thereby establishing the authorisation in principle of such imports, subject however to the condition that animal and public health requirements be met.
(2) Following a Commission veterinary inspection mission to the Falkland Islands the animal health situation and in particular the equine health situation appears to be under the satisfactory control of the veterinary services.
(3) The veterinary authorities of the Falkland Islands have provided a written undertaking to notify within 24 hours by fax, telegram or telex to the Commission and the Member States the confirmation of any infectious or contagious disease in equidae mentioned in Annex A to Directive 90/426/EEC, which are compulsorily notifiable in the country, and within due time any change in the vaccination or import policy in respect of equidae.
(4) The animal health conditions and veterinary certification for imports into the Member States of equidae must be adopted according to the animal health situation of the third country concerned. Consequently, Commission Decision 93/197/EEC of 5 February 1993 on animal health conditions and veterinary certification for imports of registered equidae and equidae for breeding and production(4), as last amended by Decision 2001/754/EC(5), must be amended accordingly.
(5) For clarity the ISO country code should be used for amendments of lists of third countries.
(6) The measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS REGULATION:
Article 1
Decision 93/197/EEC is amended as follows:
1. The list of third countries in Group A of Annex I is replaced by the following: "Switzerland (CH), Falkland Islands (FK), Greenland (GL), Iceland (IS)".
2. The title of the health certificate set out in Annex II(A) is replaced by the following: "HEALTH CERTIFICATE
for imports into Community territory of registered equidae and equidae for breeding and production from Switzerland, Falkland Islands, Greenland, Iceland".
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 25 October 2001.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 224, 18.8.1990, p. 42.
(2) OJ L 102, 12.4.2001, p. 63.
(3) OJ L 146, 14.6.1979, p. 15.
(4) OJ L 274, 17.10.2001, p. 22.
(5) OJ L 86, 6.4.1993, p. 16.
