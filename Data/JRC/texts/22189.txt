COMMISSION DECISION of 10 June 1997 amending for the fourth time Decision 92/486/EEC establishing the form of cooperation between the Animo host centre and Member States (Text with EEA relevance) (97/395/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 90/425/EEC of 26 June 1990 on veterinary and zootechnical checks applicable in intra-Community trade in certain live animals and products with a view to completion of the internal market (1), as last amended by Decision 92/118/EEC (2), and in particular Article 20 (3) thereof,
Whereas in order to permit examination in depth of the various possible structural options for the Animo network a study is being carried out by a private firm;
Whereas in order to ensure continuity of the network pending completion of that study the period of validity of the arrangements determined by Commission Decision 92/486/EEC of 25 September 1992 establishing the form of cooperation between the Animo host centre and Member States (3), as last amended by Decision 96/296/EC (4), should be extended;
Whereas the content of this Decision is in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
In Article 2a (1) of Decision 92/486/EEC the first indent is replaced by the following:
'- are extended for two years`.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 10 June 1997.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ No L 224, 18. 8. 1990, p. 29.
(2) OJ No L 62, 15. 3. 1993, p. 49.
(3) OJ No L 291, 7. 10. 1992, p. 20.
(4) OJ No L 113, 7. 5. 1996, p. 25.
