Council Regulation (EC) No 1941/2004
of 2 November 2004
terminating the "new exporter" review of Regulation (EC) No 2605/2000 imposing definitive anti-dumping duties on imports of certain electronic weighing scales (REWS) originating, inter alia, in Taiwan
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 384/96 of 22 December 1995 on protection against dumped imports from countries not members of the European Community [1] (the basic Regulation), and in particular Article 11(4) thereof,
Having regard to the proposal submitted by the Commission after consulting the Advisory Committee,
Whereas:
A. MEASURES IN FORCE
(1) The measures currently in force on imports into the Community of certain electronic weighing scales (REWS) originating in Taiwan are definitive anti-dumping duties imposed by Regulation (EC) No 2605/2000 [2]. Pursuant to the same Regulation, anti-dumping duties were also imposed on imports of REWS originating in the People's Republic of China and the Republic of Korea. There are also anti-dumping measures in force on REWS originating in Japan and Singapore [3].
B. CURRENT INVESTIGATION
1. Request for a review
(2) After the imposition of definitive anti-dumping duties on imports of REWS originating in Taiwan, the Commission received a request to initiate a "new exporter" review of Regulation (EC) No 2605/2000, pursuant to Article 11(4) of the basic Regulation, from a Taiwanese company, Charder Electronic Co., Ltd (Charder). This company claimed that it was not related to any of the exporting producers in Taiwan subject to the anti-dumping measures in force with regard to REWS. Furthermore, it claimed that it had not exported REWS to the Community during the original investigation period (i.e. the period from 1 September 1998 to 31 August 1999), but had started to export REWS to the Community thereafter.
2. Initiation of a "new exporter" review
(3) The Commission examined the evidence submitted by Charder and considered it sufficient to justify the initiation of a review in accordance with Article 11(4) of the basic Regulation. After consultation of the Advisory Committee and after the Community industry concerned had been given the opportunity to comment, the Commission initiated, by Regulation (EC) No 2034/2003 [4], a review of Regulation (EC) No 2605/2000 with regard to Charder and commenced its investigation.
(4) Pursuant to Regulation (EC) No 2034/2003 initiating the review, the anti-dumping duty of 13,4 % imposed by Regulation (EC) No 2605/2000 on imports of REWS produced by Charder was repealed. Simultaneously, pursuant to Article 14(5) of the basic Regulation, customs authorities were directed to take appropriate steps to register such imports.
3. Product concerned
(5) The product covered by the current review is the same as in the original investigation, i.e. electronic weighing scales for use in the retail trade, having a maximum weighing capacity not exceeding 30 kg, which incorporate a digital display of the weight, unit price and price to be paid (whether or not including a means of printing this data), normally declared within CN code ex84238150 (TARIC code 8423815010) and originating in Taiwan.
4. Parties concerned
(6) The Commission officially advised Charder and the representatives of the exporting country of the initiation of the review. Interested parties were given the opportunity to make their views known in writing and to be heard.
(7) The Commission also sent out a questionnaire to Charder and received a reply within the deadline. The Commission sought and verified all the information it deemed necessary for the determination of dumping, and a verification visit was carried out at the premises of Charder and of a company in the Community importing products manufactured by Charder (the importer).
5. Investigation period
(8) The investigation of dumping covered the period from 1 October 2002 to 30 September 2003 (the investigation period or IP).
C. RESULTS OF THE INVESTIGATION
(9) The investigation confirmed that Charder had not exported the product concerned during the original investigation period.
(10) Furthermore, Charder was able to demonstrate that it was not related to any of the exporters or producers in Taiwan which are subject to the anti-dumping measures imposed on imports of REWS originating in Taiwan.
(11) However, Article 11(4) of the basic Regulation also requires that a new exporter has actually exported the product concerned to the Community following the original investigation period or that it can demonstrate that it has entered into an irrevocable contractual obligation to export a significant quantity of the product concerned to the Community. In this respect, it was found that the products produced and exported to the Community by Charder during the IP, reported as the product concerned, were not in a condition to be sold to end-users. Although these products were declared by Charder and the importer as the product concerned, they were found to be unfinished products and had different physical characteristics than the product concerned. These unfinished products were further processed by the importer and transformed into electronic weighing scales. Moreover, it should also be noted that none of the further processed scales were sold in the IP. For these reasons, the imported products cannot be classified as the product concerned. Furthermore, Charder did not demonstrate that it had entered into an irrevocable contractual obligation to export a significant quantity of the product concerned to the Community.
(12) For the above reasons, it is concluded that Charder was not able to demonstrate that it actually fulfilled the criteria to be considered as a new exporter within the meaning of Article 11(4) of the basic Regulation.
D. TERMINATION OF THE REVIEW
(13) In the light of the results of the investigation, the review should be terminated without amending the level of the duty applicable to Charder, which should be maintained at the level of the definitive countrywide anti-dumping duty rate established in the original investigation, i.e. 13,4 %.
E. RETROACTIVE LEVYING OF THE ANTI-DUMPING DUTY
(14) In the light of the above findings, the anti-dumping duty applicable to Charder shall be levied retroactively on imports of the product concerned, which have been made subject to registration pursuant to Article 3 of Regulation (EC) No 2034/2003.
F. DISCLOSURE
(15) All parties concerned were informed of the essential facts and considerations on the basis of which it was intended to terminate the present review and to impose the anti-dumping duty retroactively on imports made subject to registration. No objections were raised to the facts and considerations disclosed.
(16) This review should therefore be terminated without any amendment to Regulation (EC) No 2605/2000,
HAS ADOPTED THIS REGULATION:
Article 1
1. The "new exporter" review of Regulation (EC) No 2605/2000 imposing definitive anti-dumping duties on imports of certain electronic weighing scales (REWS) originating, inter alia, in Taiwan, initiated pursuant to Article 11(4) of Regulation (EC) No 384/96, is hereby terminated without amending the anti-dumping duties in force.
2. The duty of 13,4 % imposed by Regulation (EC) No 2605/2000 on imports of certain electronic weighing scales (REWS) originating, inter alia, in Taiwan shall be levied retroactively on imports of the product concerned, which have been registered pursuant to Article 3 of Regulation (EC) No 2034/2003.
3. Unless otherwise specified, the provisions in force concerning customs duties shall apply.
Article 2
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 2 November 2004.
For the Council
The President
B. R. Bot
--------------------------------------------------
[1] OJ L 56, 6.3.1996, p. 1. Regulation as last amended by Regulation (EC) No 461/2004 (OJ L 77, 13.3.2004, p. 12).
[2] OJ L 301, 30.11.2000, p. 42. Regulation as amended by Commission Regulation (EC) No 1408/2004 (OJ L 256, 3.8.2004, p. 8).
[3] Council Regulation (EC) No 468/2001 of 6 March 2001 imposing a definitive anti-dumping duty on imports of certain electronic weighing scales originating in Japan (OJ L 67, 9.3.2001, p. 24) and Council Regulation (EC) No 469/2001 of 6 March 2001 imposing a definitive anti-dumping duty on imports of certain electronic weighing scales originating in Singapore (OJ L 67, 9.3.2001, p. 37).
[4] OJ L 302, 20.11.2003, p. 3.
