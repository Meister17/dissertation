Action brought on 23 September 2005 — UPC France v Commission
Parties
Applicant(s): UPC France Holding BV (Schiphol-Rijk, Netherlands) (represented by: M.D. Powell and N. Flandin, lawyers)
Defendant(s): Commission of the European Communities
Form of order sought
The applicant(s) claim(s) that the Court should:
- annul Commission Decision State Aid No 382/2004 — France;
- order the Commission to pay the costs.
Pleas in law and main arguments
By the present action, the applicant requests the annulment of Commission Decision State Aid No 382/2004 — France of 3 May 2005 [1] declaring that the subsidy granted by the French authorities for the construction and operation of a high-throughput telecommunications network in Limousin (DORSAL) does not constitute aid.
In support of its action, the applicant relies on several pleas, alleging infringement of essential procedural requirements and infringement of Community law.
As regards infringement of essential procedural requirements, the applicant claims that the Commission infringed its procedural rights by wrongly refraining from initiating the formal investigation procedure provided for in Article 88(2) of the EC Treaty and Article 6 of Regulation No 659/99 [2], which would have enabled it, as an interested party, to submit comments before the Commission took the decision.
By its first plea, relating to infringement of Community law, the applicant claims that the Commission made a manifest error of assessment by considering that the measure in question did not contain aid because the subsidy had been granted in connection with compensation for public service and the criteria set out in the decision in Altmark [3] had been fulfilled in this case.
Moreover, the applicant challenges the contested decision inasmuch as it classifies the high output service as public service, when the definition of public service in relation to telecommunications, according to the "universal service" directive, covers only low-throughput services.
The applicant then submits that if the obligations at issue must be regarded as a public service task, which they are not, the Commission made a manifest error of assessment by taking the view that, in the city of Limoges, there is a shortage of private initiative and absence of competitive forces. It also claims that the procedure used to choose the operator responsible for carrying out a supposed service task in the general interest did not make it possible to select the candidate able to provide the service at the lowest cost for the community. In any event, according to the applicant, in this case and as far as the city of Limoges is concerned the public subsidy cannot be justified as compensation for the carrying out of a public service task and Altmark cannot therefore apply.
By its next plea, the applicant also relies on substantial procedural irregularities in the procedure for delegation of the public service task which meant that the potential candidates for public service delegation were not accorded equal treatment.
The final plea raised by the applicant alleges a failure to give sufficient reasons, since the Commission did not explain why it took into consideration the region of Limousin as a whole without taking account of the particular features of urban areas which, like the city of Limoges, present real pockets of competition within the region, nor why it did not at any time in the contested decision refer to the presence of the applicant's network in Limoges.
[1] C (2005) 1170 final 1.
[2] Council Regulation (EC) No 659/1999 of 22 March 1999 laying down detailed rules for the application of Article 93 of the EC Treaty, OJ 1999 L 83 of 27.03.1999, p. 1.
[3] Case C-280/00 [2003] ECR I-7747.
--------------------------------------------------
