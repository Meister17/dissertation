DECISION OF THE COUNCIL AND THE COMMISSION
of 13 December 1993
on the conclusion of the Agreement on the European Economic Area between the European Communities, their Member States and the Republic of Austria, the Republic of Finland, the Republic of Iceland, the Principality of Liechtenstein, the Kingdom of Norway, the Kingdom of Sweden and the Swiss Confederation
(94/1/ECSC, EC)
THE COUNCIL OF THE EUROPEAN UNION,
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Coal and Steel Community,
Having regard to the Treaty establishing the European Community, and in particular Article 238 in conjunction with Article 228 (3), second subparagraph thereof,
Having regard to the assent of the European Parliament (1),
Whereas the Agreement on the European Economic Area between the European Communities, their Member States and the Republic of Austria, the Republic of Finland, the Republic of Iceland, the Principality of Liechtenstein, the Kingdom of Norway, the Kingdom of Sweden and the Swiss Confederation, signed in Oporto on 2 May 1992 should be approved,
HAVE DECIDED AS FOLLOWS:
Article 1
The Agreement on the European Economic Area between the European Communities, their Member States and the Republic of Austria, the Republic of Finland, the Republic of Iceland, the Principality of Liechtenstein, the Kingdom of Norway, the Kingdom of Sweden and the Swiss Confederation, the Protocols, the Annexes annexed thereto and the Declarations, the Agreed Minutes and exchanges of letters attached to the Final Act are hereby approved on behalf of the European Community and the European Coal and Steel Community.
The texts of the acts referred to in the first paragraph are attached to this Decision.
Article 2
The act of approval provided for in Article 129 of the Agreement shall be deposited by the President of the Council on behalf of the European Community and by the President of the Commission on behalf of the European Coal and Steel Community (2).
Done at Brussels, 13 December 1993.
For the Council
The President
Ph. MAYSTADT
For the Commission
The President
J. DELORS
(1) OJ No C 305, 23. 11. 1992, p. 66.
(2) See page 606 of this Official Journal.
