COMMISSION DIRECTIVE 98/22/EC of 15 April 1998 laying down the minimum conditions for carrying out plant health checks in the Community, at inspection posts other than those at the place of destination, of plants, plant products or other objects coming from third countries
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 77/93/EEC of 21 December 1976 on protective measures against the introduction into the Community of organisms harmful to plants or plant products and against their spread within the Community (1), as last amended by Commission Directive 98/2/EC (2), and in particular the penultimate subparagraph of Article 12(6) thereof,
Whereas, if arrangements for the plant health checks of plants, plant products or other objects listed in part B of Annex V to Directive 77/93/EEC coming from third countries are to be operated efficiently, harmonised minimum conditions should be laid down for carrying out these checks at inspection posts other than those at the place of destination;
Whereas the minimum conditions laid down for carrying out such plant health checks must take account of technical requirements applicable to the responsible official bodies as referred to in Article 2(1)(g) of Directive 77/93/EEC in charge of the said inspection posts as well as of provisions applicable to facilities, tools and equipment enabling the said responsible official bodies to carry out the required plant health checks;
Whereas the measures provided for in this Directive are in accordance with the opinion of the Standing Committee on Plant Health,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Member States shall ensure that the plant health checks referred to in Article 12(6), fourth subparagraph of Directive 77/93/EEC, of plants, plant products or other objects listed in Annex V, part B of the said Directive and coming from third countries, and carried out at inspection posts other than those at the place of destination, satisfy at least the minimum conditions laid down in the Annex to this Directive.
Article 2
1. Member States shall bring into force the laws, regulations or administrative provisions necessary to comply with this Directive on 1 October 1998. They shall immediately inform the Commission thereof.
When Member States adopt these measures, they shall contain a reference to this Directive or shall be accompanied by such a reference at the time of their official publication. The procedure for such a reference shall be adopted by Member States.
2. Member States shall immediately communicate to the Commission all provisions of national law which they adopt in the field covered by this Directive. The Commission shall inform the other Member States thereof.
Article 3
This Directive shall enter into force on the day following its publication in the Official Journal of the European Communities.
Article 4
This Directive is addressed to the Member States.
Done at Brussels, 15 April 1998.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ L 26, 31. 1. 1977, p. 20.
(2) OJ L 15, 21. 1. 1998, p. 34.
ANNEX
Minimum conditions for carrying out plant health checks in the Community, at inspection posts other than those at the place of destination, of plants, plant products or other objects coming from third countries
1. The responsible official bodies, as referred to in Article 2(1)(g) of Directive 77/93/EEC in charge of the inspection posts referred to in Article 1 of this Directive, shall:
- have authority to carry out their duties,
- have technical competence, especially in the detection and diagnosis of harmful organisms,
- have expertise in identification of harmful organisms, or access to such expertise,
- have access to appropriate administrative, inspection and testing facilities, tools and equipment, as specified in paragraph 3,
- have access to facilities for proper storage and quarantine of consignments and, when necessary, for destruction (or other suitable treatment), of all or part of the intercepted consignment,
- have available:
(a) written, up-to-date national inspection guidelines based on the domestic legislation of the Member State, as established within the framework of Community legislation,
(b) an up-to-date set of Community guidelines for the experts and for national inspectors, referred to in Article 19a(6) of Directive 77/93/EEC,
(c) up-to-date Community plant health legislation,
(d) an up-to-date list including addresses and telephone numbers of specialised laboratories which have been officially approved for carrying out tests for determining the presence of harmful organisms or for the identification of harmful organisms. A suitable procedure should be put in place to ensure the integrity and security of the sample(s) when moved to the laboratory and during the testing process,
(e) up-to-date information on consignments of plants, plant products or other objects coming from third countries which have undergone:
- official interception,
- official tests in specialised laboratories together with their results,
provided at least that this information is relevant to the plant health checks for the place at which they are carried out,
- adapt the established programme of plant health checks as quickly as possible in such a way as to meet actual needs, in the light of new plant health risks or any changes in the quantity/volume of the plants, plant products or other objects offered for introduction at the inspection posts referred to in Article 1.
2. The public servants and qualified agents as referred to in Article 2(1)(i), second subparagraph of Directive 77/93/EEC actually in charge of carrying out the inspections at the inspection posts referred to in Article 1 of this Directive shall have:
- technical competence, especially in the detection of harmful organisms,
- expertise in identification of harmful organisms, or access to such expertise,
as part of their qualifications required under the said Article 2(1)(i) second subparagraph, and shall have directly available the information referred to in paragraph 1, sixth indent.
3. The facilities, tools and equipment as referred to in paragraph 1 shall have at least:
(a) in respect of administrative facilities:
- a rapid communication system with:
- the authority referred to in Article 1(6) of Directive 77/93/EEC,
- the specialised laboratories referred to in paragraph 1,
- the customs authorities,
- the Commission,
- other Member States,
- a document duplication system;
(b) in respect of inspection facilities:
- suitable areas for inspection, as appropriate,
- adequate lighting,
- inspection table(s),
- equipment suitable for:
- visual checks,
- disinfecting the premises and equipment used for plant health checks,
- the preparation of samples for possible further tests in the specialised laboratories referred to in paragraph 1;
(c) in respect of facilities for the sampling of consignments:
- appropriate material for the individual identification and packaging of each sample,
- adequate packaging material for sending samples to the specialised laboratories referred to in paragraph 1,
- seals,
- official stamps,
- adequate lighting.
