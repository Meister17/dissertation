Commission Decision
of 10 October 2003
amending Decision 93/52/EEC as regards the recognition of certain Italian provinces as officially free of brucellosis
(notified under document number C(2003) 3562)
(Text with EEA relevance)
(2003/732/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Directive 91/68/EEC of 28 January 1991 on animal health conditions governing intra-Community trade in ovine and caprine animals(1), as last amended by Council Directive 2003/50/EC(2), and in particular Annex A(1)(II) thereto,
Whereas:
(1) In Italy, in the region of Lombardia (provinces of Bergamo, Brescia, Como, Cremona, Lecco, Lodi, Mantua, Milan, Pavia, Sondrio and Varese) and in the province of Trento, brucellosis (Brucella melitensis) has been a notifiable disease for at least five years.
(2) In the provinces of Bergamo, Brescia, Como, Cremona, Lecco, Lodi, Mantua, Milan, Pavia, Sondrio, Varese and Trento, at least 99,8 % of the ovine or caprine holdings are officially brucellosis-free holdings. These provinces undertake, furthermore, to comply with Annex A(1)(II)(2) to Directive 91/68/EEC.
(3) The provinces of Bergamo, Brescia, Como, Cremona, Lecco, Lodi, Mantua, Milan, Pavia, Sondrio, Varese and Trento should consequently be recognised as officially free of brucellosis (Brucella melitensis).
(4) Commission Decision 93/52/EEC(3), as last amended by Decision 2003/237/EC(4), should therefore be amended accordingly.
(5) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
Annex II to Decision 93/52/EEC is replaced by the text in the Annex to this Decision.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 10 October 2003.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 46, 19.2.1991, p. 19.
(2) OJ L 169, 8.7.2003, p. 51.
(3) OJ L 13, 21.1.1993, p. 14.
(4) OJ L 87, 4.4.2003, p. 13.
ANNEX
"ANNEX II
In France:
Ain, Aisne, Allier, Ardèche, Ardennes, Aube, Aveyron, Cantal, Charente, Charente-Maritime, Cher, Corrèze, Côte-d'Or, Côtes-d'Armor, Creuse, Deux-Sèvres, Dordogne, Doubs, Essonne, Eure, Eure-et-Loir, Finistère, Gers, Gironde, Hauts-de-Seine, Haute-Loire, Haute-Vienne, Ille-et-Vilaine, Indre, Indre-et-Loire, Jura, Loir-et-Cher, Loire, Loire-Atlantique, Loiret, Lot-et-Garonne, Lot, Lozère, Maine-et-Loire, Manche, Marne, Mayenne, Morbihan, Nièvre, Nord, Oise, Orne, Pas-de-Calais, Puy-de-Dôme, Rhône, Haute-Saône, Saône-et-Loire, Sarthe, Seine-Maritime, Seine-Saint-Denis, Territoire de Belfort, Val-de-Marne, Val-d'Oise, Vendée, Vienne, Yonne, Yvelines, Ville de Paris, Vosges.
In Italy:
- Lombardy region: provinces of Bergamo, Brescia, Como, Cremona, Lecco, Lodi, Mantua, Milan, Pavia, Sondrio, Varese.
- Sardinia region: Cagliari, Nuoro, Oristano and Sassari.
- Trentino-Alto Aldige region: provinces of Bolzano and Trento.
- Tuscany region: province of Arezzo.
In Portugal:
Autonomous Region of the Azores.
In Spain:
Santa Cruz de Tenerife, Las Palmas."
