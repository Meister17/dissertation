Commission Regulation (EC) No 1009/2005
of 30 June 2005
amending Regulation (EC) No 2799/1999 laying down detailed rules for applying Council Regulation (EC) No 1255/1999 as regards the grant of aid for skimmed milk and skimmed-milk powder intended for animal feed and the sale of such skimmed-milk powder
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1255/1999 of 17 May 1999 on the common organisation of the market in milk and milk products [1], and in particular Articles 10 and 15 thereof,
Whereas:
(1) Article 7(1) of Commission Regulation (EC) No 2799/1999 [2] fixes the amount of aid for skimmed milk and skimmed-milk powder intended for animal feed taking into account the factors set out in Article 11(2) of Regulation (EC) No 1255/1999. In view of reduction in the intervention price of skimmed milk powder from 1 July 2005, the amount of aid should be reduced.
(2) Regulation (EC) No 2799/1999 should therefore be amended accordingly.
(3) The Management Committee for Milk and Milk Products has not delivered an opinion within the time-limit set by its chairman,
HAS ADOPTED THIS REGULATION:
Article 1
In Article 7 of Regulation (EC) No 2799/1999, paragraph 1 is replaced by the following:
"1. Aid is fixed at:
(a) EUR 2,42 per 100 kg of skimmed milk with a protein content of not less than 35,6 % of the non-fatty dry extract;
(b) EUR 2,14 per 100 kg of skimmed milk with a protein content of not less than 31,4 % but less than 35,6 % of the non-fatty dry extract;
(c) EUR 30,00 per 100 kg of skimmed-milk powder with a protein content of not less than 35,6 % of the non-fatty dry extract;
(d) EUR 26,46 per 100 kg of skimmed-milk powder with a protein content of not less than 31,4 % but less than 35,6 % of the non-fatty dry extract."
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Union.
It shall apply as of 1 July 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 30 June 2005.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 160, 26.6.1999, p. 48. Regulation as last amended by Commission Regulation (EC) No 186/2004 (OJ L 29, 3.2.2004, p. 6).
[2] OJ L 340, 31.12.1999, p. 3. Regulation as last amended by Regulation (EC) No 2250/2004 (OJ L 381, 28.12.2004, p. 25).
--------------------------------------------------
