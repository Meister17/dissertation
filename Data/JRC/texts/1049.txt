Commission Regulation (EC) No 2594/2001
of 28 December 2001
derogating from Regulation (EC) No 174/1999 laying down special detailed rules for the application of Council Regulation (EEC) No 804/68 as regards export licences and export refunds in the case of milk and milk products
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1255/1999 of 17 May on the common organisation of the market in milk and milk products(1), as last amended by Regulation (EC) No 1670/2000(2), and in particular Article 31(14) thereof,
Whereas:
(1) Article 6 of Commission Regulation (EC) No 174/1999(3), as last amended by Regulation (EC) No 2298/2001(4), fixes the period of validity of export licences. In the case of cheese, such licences are valid until the end of the fourth month following issue. Under the bilateral agreement between the European Community and the Swiss Confederation on trade in agricultural products, signed in Luxembourg on 21 June 1999, refunds for cheese exported to Switzerland are to be abolished with effect from the date of entry into force of that agreement, which is currently being ratified. Under Article 17 of that agreement, it is to enter into force on the first day of the second month following the final notification of the deposit of the instruments of ratification or approval of all the agreements mentioned in that Article. To ensure compliance with the provisions of the agreement in that respect, the period of validity of the licences concerned should be shortened so that, when the agreement enters into force, licences issued with advance fixing of the refund having Switzerland as their destination are no longer valid.
(2) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Milk and Milk Products,
HAS ADOPTED THIS REGULATION:
Article 1
Notwithstanding Article 6(c) of Regulation (EC) No 174/1999, export licences with advance fixing of the refund for products falling within CN code 0406, having Switzerland as their destination, shall be valid until the end of the month following that of issue.
Article 2
This Regulation shall enter into force on 1 January 2002.
It shall apply to licences issued from 1 January 2002 onwards.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 28 December 2001.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 160, 26.6.1999, p. 48.
(2) OJ L 193, 29.7.2000, p. 10.
(3) OJ L 20, 27.1.1999, p. 8.
(4) OJ L 308, 27.11.2001, p. 16.
