AGREEMENT ON THE ACCESSION OF THE REPUBLIC OF FINLAND
to the Convention implementing the Schengen Agreement of 14 June 1985 on the gradual abolition of checks at the common borders signed at Schengen on 19 June 1990
The KINGDOM OF BELGIUM, the FEDERAL REPUBLIC OF GERMANY, the FRENCH REPUBLIC, the GRAND DUCHY OF LUXEMBOURG and the KINGDOM OF THE NETHERLANDS, Parties to the Convention implementing the Schengen Agreement of 14 June 1985 between the Governments of the States of the Benelux Economic Union, the Federal Republic of Germany and the French Republic on the gradual abolition of checks at their common borders signed at Schengen on 19 June 1990, hereinafter referred to as "the 1990 Convention", as well as the Italian Republic, the Kingdom of Spain and the Portuguese Republic, the Hellenic Republic, and the Republic of Austria, which acceded to the 1990 Convention by the Agreements signed on 27 November 1990, on 25 June 1991, on 6 November 1992 and on 28 April 1995 respectively,
of the one part,
and the REPUBLIC OF FINLAND, of the other part,
Having regard to the signature done at Luxembourg on this nineteenth day of December in the year one thousand nine hundred and ninety-six of the Protocol on the Accession of the Government of the Republic of Finland to the Schengen Agreement of 14 June 1985 between the Governments of the States of the Benelux Economic Union, the Federal Republic of Germany and the French Republic on the gradual abolition of checks at their common borders, as amended by the Protocols on the Accession of the Governments of the Italian Republic, the Kingdom of Spain and the Portuguese Republic, the Hellenic Republic, and the Republic of Austria, signed on 27 November 1990, 25 June 1991, on 6 November 1992 and on 28 April 1995 respectively,
On the basis of Article 140 of the 1990 Convention,
HAVE AGREED AS FOLLOWS:
Article 1
The Republic of Finland hereby accedes to the 1990 Convention.
Article 2
1. At the date of signing this Agreement, the officers referred to in Article 40(4) of the 1990 Convention as regards the Republic of Finland shall be:
(a) Poliisin virkamiehistä poliisimiehet - av polisens tjänstemän polismän (officers of the police);
(b) Rajavartiolaitoksen virkamiehistä rajavartiomiehet - av gränsbevakningsväsendets tjänstemän gränsbevakningsmän (frontier guard officials of the Frontier Guard), as regards trafficking in human beings referred to in Article 40(7) of the 1990 Convention;
(c) Tullimiehet - tulltjänstemän (customs officers), under the conditions laid down in appropriate bilateral agreements referred to in Article 40(6) of the 1990 Convention, with respect to their powers regarding the illicit trafficking in narcotic drugs and psychotropic substances, trafficking in arms and explosives, and the illicit transportation of toxic and hazardous waste.
2. At the date of signing this Agreement, the authority referred to in Article 40(5) of the 1990 Convention as regards the Rebublic of Finland shall be the Keskusrikospoliisi - Centralkriminalpolisen (the National Bureau of Investigation).
Article 3
As the date of signing this Agreement, the officers referred to in Article 41(7) of the 1990 Convention as regards the Republic of Finland shall be:
(1) Poliisin virkamiehistä poliisimiehet - av polisens tjänstemän polismän (officers of the police);
(2) Rajavartiolaitoksen virkamiehistä rajavartiomiehet - av gränsbevakningsväsendets tjänstemän gränsbevakningsmän (frontier guard officials of the Frontier Guard), as regards trafficking in human beings referred to in Article 40(7) of the 1990 Convention;
(3) Tullimiehet - tulltjänstemän (customs officers), under the conditions laid down in appropriate bilateral agreements referred to in Article 41(10) of the 1990 Convention, with respect to their powers regarding the illicit trafficking in narcotic drugs and psychotropic substances, trafficking in arms and explosives, and the illicit transportation of toxic and hazardous waste.
Article 4
At the date of signing this Agreement, the competent Ministry referred to in Article 65(2) of the 1990 Convention as regards the Republic of Finland shall be the Oikeusministeriö - Justitieministeriet (Ministry of Justice).
Article 5
The provisions of this Agreement shall not prejudice cooperation within the framework of the Nordic Passport Union, insofar as such cooperation does not conflict with, or impede, the application of this Agreement.
Article 6
1. This Agreement shall be subject to ratification, acceptance or approval. The instruments of ratification, acceptance or approval shall be deposited with the Government of the Grand Duchy of Luxembourg, which shall notify all the Contracting Parties thereof.
2. This Agreement shall enter into force on the first day of the second month following the deposit of the instruments of ratification, acceptance or approval by the States for which the 1990 Convention has entered into force and by the Republic of Finland.
With regard to other States, this Agreement shall enter into force on the first day of the second month following the deposit of their instruments of ratification, acceptance or approval, provided that this Agreement has entered into force in accordance with the provisions of the preceding subparagraph.
3. The Government of the Grand Duchy of Luxembourg shall notify each of the Contracting Parties of the date of entry into force.
Article 7
1. The Government of the Grand Duchy of Luxembourg shall transmit to the Government of the Republic of Finland a certified copy of the 1990 Convention in the Dutch, French, German, Greek, Italian, Portuguese and Spanish languages.
2. The text of the 1990 Convention drawn up in the Finnish language is annexed to this Agreement and shall be authentic under the same conditions as the texts of the 1990 Convention drawn up in the Dutch, French, German, Greek, Italian, Portuguese and Spanish languages.
In witness whereof, the undersigned, duly authorised to this effect, have signed this Agreement.
Done at Luxembourg this nineteenth day of December in the year one thousand nine hundred and ninety-six in a single original in the Dutch, Finnish, French, German, Greek, Italian, Portuguese and Spanish languages, all eight texts being equally authentic, such original remaining deposited in the archives of the Government of the Grand Duchy of Luxembourg, which shall transmit a certified copy to each of the Contracting Parties.
For the Government of the Kingdom of Belgium
>PIC FILE= "L_2000239EN.010701.TIF">
For the Government of the Federal Republic of Germany
>PIC FILE= "L_2000239EN.010801.TIF">
For the Government of the Hellenic Republic
>PIC FILE= "L_2000239EN.010802.TIF">
For the Government of the Kingdom of Spain
>PIC FILE= "L_2000239EN.010803.TIF">
For the Government of the French Republic
>PIC FILE= "L_2000239EN.010804.TIF">
For the Government of the Italian Republic
>PIC FILE= "L_2000239EN.010805.TIF">
For the Government of the Grand Duchy of Luxembourg
>PIC FILE= "L_2000239EN.010806.TIF">
For the Government of the Kingdom of the Netherlands
>PIC FILE= "L_2000239EN.010901.TIF">
For the Government of the Republic of Austria
>PIC FILE= "L_2000239EN.010902.TIF">
For the Government of the Portuguese Republic
>PIC FILE= "L_2000239EN.010903.TIF">
For the Government of the Republic of Finland
>PIC FILE= "L_2000239EN.010904.TIF">
FINAL ACT
I. At the time of signing the Agreement on the Accession of the Republic of Finland to the Convention implementing the Schengen Agreement of 14 June 1985 between the Governments of the States of the Benelux Economic Union, the Federal Republic of Germany and the French Republic on the gradual abolition of checks at their common borders signed at Schengen on 19 June 1990, to which the Italian Republic, the Kingdom of Spain and the Portuguese Republic, the Hellenic Republic, and the Kingdom of Austria acceded by the Agreements signed on 27 November 1990, on 25 June 1991, on 6 November 1992 and on 28 April 1995 respectively, the Government of the Republic of Finland has subscribed to the Final Act, the Minutes and the Joint Declaration by the Ministers and State Secretaries which were signed at the time of signing the 1990 Convention.
The Government of the Republic of Finland has subscribed to the Joint Declarations and has taken note of the unilateral Declarations contained therein.
The Government of the Grand Duchy of Luxembourg shall transmit to the Government of the Republic of Finland a certified copy of the Final Act, the Minutes and the Joint Declaration by the Ministers and State Secretaries which were signed at the time of signing the 1990 Convention, in the Dutch, Finnish, French, German, Greek, Italian, Portuguese and Spanish languages.
II. At the time of signing the Agreement on the Accession of the Republic of Finland to the Convention implementing the Schengen Agreement of 14 June 1985 between the Governments of the States of the Benelux Economic Union, the Federal Republic of Germany and the French Republic on the gradual abolition of checks at their common borders signed at Schengen on 19 June 1990, to which the Italian Republic, the Kingdom of Spain and the Portuguese Republic, the Hellenic Republic, and the Kingdom of Austria acceded by the Agreements signed on 27 November 1990, on 25 June 1991, on 6 November 1992 and on 28 April 1995 respectively, the Contracting Parties have adopted the following Declarations:
1. Joint Declaration on Article 6 of the Accession Agreement
The Contracting Parties shall, prior to the entry into force of the Accession Agreement, inform each other of all circumstances that could have a significant bearing on the areas covered by the 1990 Convention and on the bringing into force of the Accession Agreement.
This Accession Agreement shall be brought into force between the States for which the 1990 Convention has been brought into force and the Republic of Finland when the preconditions for implementation of the 1990 Convention have been fulfilled in all these States and checks at the external borders are effective there.
With regard to each of the other States, this Accession Agreement shall be brought into force when the preconditions for the implementation of the 1990 Convention have been fulfilled in that State and when checks at the external borders are effective there.
2. Joint Declaration on Article 9(2) of the 1990 Convention
The Contracting Parties specify that at the time of signing the Agreement on the Accession of the Republic of Finland to the 1990 Convention, the common visa arrangements referred to in Article 9(2) of the 1990 Convention shall be taken to mean the common arrangements applied by the Signatory Parties to the said Convention since 19 June 1990.
3. Joint Declaration on the Convention drawn up on the basis of Article K.3 of the Treaty on European Union relating to extradition
The States party to the 1990 Convention hereby confirm that Article 5(4) of the Convention drawn up on the basis of Article K.3 of the Treaty on European Union relating to extradition between the Member States of the European Union, signed at Dublin on 27 September 1996, and their respective Declarations annexed to the said Convention, shall apply within the framework of the 1990 Convention.
III. The Contracting Parties have taken note of the Declarations by the Republic of Finland on the Agreements on the Accession of the Italian Republic, the Kingdom of Spain, the Portuguese Republic, the Hellenic Republic, and the Republic of Austria.
The Government of the Republic of Finland takes note of the contents of the Agreements on the Accession of the Italian Republic, the Kingdom of Spain and the Portuguese Republic, the Hellenic Republic, and the Republic of Austria to the 1990 Convention, signed on 27 November 1990, on 25 June 1991, on 6 November 1992 and on 28 April 1995 respectively, and of the contents of the Final Acts and Declarations annexed to the said Agreements.
The Government of the Grand Duchy of Luxembourg shall transmit a certified copy of the abovementioned instruments to the Government of the Republic of Finland.
Declaration by the Republic of Finland on the Agreements on the Accession of the Kingdom of Denmark and the Kingdom of Sweden to the 1990 Convention
At the time of signing this Agreement, the Republic of Finland takes note of the contents of the Agreements on the Accession of the Kingdom of Denmark and the Kingdom of Sweden to the 1990 Convention and of the contents of the related Final Acts and Declarations.
Declaration by the Government of the Republic of Finland on the Åland islands
The Republic of Finland hereby declares that the obligations arising from Article 2 of Protocol 2 to the Act concerning the conditions of accession of the Republic of Austria, the Republic of Finland and the Kingdom of Sweden and the adjustments to the Treaties on which the European Union is found relating to the Åland islands shall be complied with when implementing the 1990 Convention.
Done at Luxembourg this nineteenth day of December in the year one thousand nine hundred and ninety-six in a single original in the Dutch, Finnish, French, German, Greek, Italian, Portuguese and Spanish languages, all eight texts being equally authentic, such original remaining deposited in the archives of the Government of the Grand Duchy of Luxembourg, which shall transmit a certified copy to each of the Contracting Parties.
For the Government of the Kingdom of Belgium
>PIC FILE= "L_2000239EN.011101.TIF">
For the Government of the Federal Republic of Germany
>PIC FILE= "L_2000239EN.011201.TIF">
For the Government of the Hellenic Republic
>PIC FILE= "L_2000239EN.011202.TIF">
For the Government of the Kingdom of Spain
>PIC FILE= "L_2000239EN.011203.TIF">
For the Government of the French Republic
>PIC FILE= "L_2000239EN.011204.TIF">
For the Government of the Italian Republic
>PIC FILE= "L_2000239EN.011205.TIF">
For the Government of the Grand Duchy of Luxembourg
>PIC FILE= "L_2000239EN.011206.TIF">
For the Government of the Kingdom of the Netherlands
>PIC FILE= "L_2000239EN.011301.TIF">
For the Government of the Republic of Austria
>PIC FILE= "L_2000239EN.011302.TIF">
For the Government of the Portuguese Republic
>PIC FILE= "L_2000239EN.011303.TIF">
For the Government of the Republic of Finland
>PIC FILE= "L_2000239EN.011304.TIF">
DECLARATION BY THE MINISTERS AND STATE SECRETARIES
On the nineteenth day of December in the year one thousand nine hundred and ninety-six, the representatives of the Governments of the Kingdom of Belgium, the Federal Republic of Germany, the Hellenic Republic, the Kingdom of Spain, the Republic of Finland, the French Republic, the Italian Republic, the Grand Duchy of Luxembourg, the Kingdom of the Netherlands, the Republic of Austria and the Portuguese Republic signed at Luxembourg the Agreement on the Accession of the Republic of Finland to the Convention implementing the Schengen Agreement of 14 June 1985 between the Governments of the States of the Benelux Economic Union, the Federal Republic of Germany and the French Republic on the gradual abolition of checks at their common borders, signed at Schengen on 19 June 1990, to which the Italian Republic, the Kingdom of Spain and the Portuguese Republic, the Hellenic Republic, and the Republic of Austria acceded by the Agreements signed on 27 November 1990, on 25 June 1991, on 6 November 1992 and on 28 April 1995 respectively.
They noted that the representative of the Government of the Republic of Finland declared support for the Declaration made at Schengen on 19 June 1990 by the Ministers and State Secretaries representing the Governments of the Kingdom of Belgium, the Federal Republic of Germany, the French Republic, the Grand Duchy of Luxembourg and the Kingdom of the Netherlands and for the decision confirmed on the same date upon signature of the Convention implementing the Schengen Agreement, which Declaration and decision the Governments of the Italian Republic, the Kingdom of Spain, the Portuguese Republic, the Hellenic Republic and the Republic of Austria have also supported.
