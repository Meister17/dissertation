P6_TA(2005)0023
EC-Swiss Confederation Agreement on MEDIA Plus and MEDIA Training programmes *
European Parliament legislative resolution on the proposal for a Council decision concerning the conclusion of an Agreement between the Community and the Swiss Confederation in the audiovisual field, establishing the terms and conditions for the participation of the Swiss Confederation in the MEDIA Plus and MEDIA Training Community programmes, and a final Act (COM(2004)0649 — C6-0174/2004 — 2004/0230(CNS))
(Consultation procedure)
The European Parliament,
- having regard to the proposal for a Council decision (COM(2004)0649) [1],
- having regard to Article 150(4) and Article 157(3), in conjunction with the first sentence of the first subparagraph of Article 300(2), of the EC Treaty,
- having regard to Article 300(3), first subparagraph, of the EC Treaty, pursuant to which the Council consulted Parliament (C6-0174/2004),
- having regard to Rules 51, 83(7) and 43(1) of its Rules of Procedure,
- having regard to the report of the Committee on Culture and Education (A6-0018/2005),
1. Approves conclusion of the agreement;
2. Instructs its President to forward its position to the Council and Commission, and the governments and parliaments of the Member States and the Swiss Confederation.
[1] Not yet published in OJ.
--------------------------------------------------
