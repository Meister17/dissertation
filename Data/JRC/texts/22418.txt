COMMISSION DIRECTIVE 97/15/EC of 25 March 1997 adopting Eurocontrol standards and amending Council Directive 93/65/EEC on the definition and use of compatible technical specifications for the procurement of air-traffic-management equipment and systems (Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 93/65/EEC of 19 July 1993 on the definition and use of compatible technical specifications for the procurement of air-traffic-management equipment and systems (1), and in particular Articles 3 and 5 (2) thereof,
Whereas in accordance with Article 3 of Directive 93/65/EEC the Commission shall identify and adopt Eurocontrol standards;
Whereas Annex I to Directive 93/65/EEC contains an indicative list of Eurocontrol standards; whereas that list should be as complete as possible;
Whereas Annex II to Directive 93/65/EEC defines the awarding entities responsible for the purchasing of air-navigation equipment; whereas this list needs to be kept up to date;
Whereas, given that Eurocontrol has adopted two standards, it is appropriate that these standards be made mandatory; whereas it is also appropriate that the indicative list in Annex I be amended as a result of introduction by Eurocontrol of the Eatchip programme;
Whereas it is necessary to amend Annex II to take account of changes notified by Member States and further to include the awarding entities of new Member States;
Whereas the measures provided for in this Directive are in accordance with the opinion of the Committee established in accordance with Directive 93/65/EEC,
HAS ADOPTED THIS DIRECTIVE:
Article 1
The mandatory elements of Eurocontrol specifications included in the following Eurocontrol standards documents are adopted in as much as they are necessary for the implementation of an integrated European air-traffic-management system:
- the Eurocontrol standard for on-line data interchange (OLDI), first edition, (Eurocontrol document reference 001-92),
- the Eurocontrol standard for air traffic services data exchange presentation (ADEXP) (Eurocontrol document reference 002-93).
Article 2
Annex I to Directive 93/65/EEC is replaced by the Annex to this Directive.
Article 3
Annex II to the Directive is amended as follows:
1. The awarding entities of Eurocontrol, Belgium, Denmark, France, Greece, Ireland, Italy, Portugal and United Kingdom, are modified as follows:
EUROCONTROL
Rue de la Fusée, 96
B-1130 Bruxelles
Belgium
Régie des Voies Aériennes (RVA/RLW)
Centre Communication Nord (CCN)
rue du Progrès 80 Bte 2
B-1030 Bruxelles
Denmark
Statens Luftfartsvæsen
(Civil Aviation Administration)
Postbox 744
DK-Copenhagen SV
Billund Lufthavn
PO Box 10
DK-7190 Billund
Københavns Lufthavne A/S
PO Box 74
Flyvervej 11
DK-2770 Kastrup
France
Ministre chargé de l'aviation civile
Direction générale de l'aviation civile
48, rue Camille Desmoulins
F-92452 Issy les Moulineaux Cedex
and, where competent:
Aéroport de Paris
291, Boulevard Raspail
F-75675 Paris Cedex 14
Greece
Ministry of Transport and Communications
Civil Aviation Authority
General Directorate of Air Navigation
who delegates in particular to:
Electronics Division
Vasileos Georgiou 1
PO Box 73751-16604 Elliniko
GR-Athens
Ireland
The Irish Aviation Authority
Aviation House
Hawkins Street
IRL-Dublin 2
Aer Rianta Cpt.
Dublin Airport
IRL-County Dublin
Italy
ENAV
Ente Nazionale di Assistenza al Volo
Via Salaria, 715
I-00138 Roma
Portugal
ANA-E.P. (Empresa Pública de Aeroportos e Navegação Aérea).
Rua D, Edifício 120
Aeroporto de Lisboa
P-1700 Lisboa
United Kingdom
National Air Traffic Services Ltd
CAA House
45-59 Kingsway
UK-London WC2B 6TE
2. The following awarding entities are added to the list:
Austria
Austro Control GmbH
Schnirchgasse 11
A-1030 Wien
Finland
Ilmailulaitos/Luftfartsverket
(CAA Finland)
PO Box 50
FIN-01531 Vantaa
Sweden
Swedish Civil Aviation Administration
Luftfartsverket
Vikboplan 11
S-601 79 Norrköping
Article 4
1. The Member States shall bring into force the provisions necessary for them to comply with this Directive before 1 December 1997 at the latest. They shall forthwith inform the Commission thereof.
When Member States adopt those provisions they shall include references to this Directive or shall accompany them with such references on their official publication. The Member States shall lay down the manner in which such references shall be made.
2. The Member States shall communicate to the Commission the texts of the main provisions of national law which they adopt in the field governed by this Directive. The Commission shall inform the other Member States thereof.
Article 5
This Directive shall enter into force on the 20th day following its publication in the Official Journal of the European Communities.
Article 6
This Directive is addressed to the Member States.
Done at Brussels, 25 March 1997.
For the Commission
Neil KINNOCK
Member of the Commission
(1) OJ No L 187, 29. 7. 1993, p. 52.
ANNEX
'ANNEX I
DOMAINS FOR EUROCONTROL STANDARDS AS REFERRED TO IN ARTICLE 3
Indicative list
Communications
Navigation
Surveillance
Data-processing systems
Airspace management and air traffic management procedures
Air traffic management working rules and operational requirements
Human resources`
