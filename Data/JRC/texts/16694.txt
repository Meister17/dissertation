Council Regulation (EC, Euratom) No 2273/2004
of 22 December 2004
amending Regulation (EC, Euratom) No 2728/94 establishing a Guarantee Fund for external actions
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 308 thereof,
Having regard to the Treaty establishing the European Atomic Energy Community, and in particular Article 203 thereof,
Having regard to the proposal from the Commission,
Having regard to the Opinion of the European Parliament,
Having regard to the opinion of the Court of Auditors [1],
Whereas:
(1) The accession of 10 new Member States took place on 1 May 2004.
(2) In addition, the possibility of further accessions should be taken into account.
(3) The Communities have granted loans and guaranteed loans to accession countries or for projects executed in those countries. Those loans and guarantees are currently covered by the Guarantee Fund and will remain outstanding or in force after the date of accession. From that date, they will cease to be external actions of the Communities and should therefore be covered directly by the general budget of the European Union and no longer by the Guarantee Fund.
(4) The European Investment Bank should inform the Commission of the amount of its outstanding operations under the Community guarantee in new Members States on the day of accession.
(5) The report prepared by the Commission in accordance with Article 9 of Council Regulation (EC, Euratom) No 2728/94 of 31 October 1994 establishing a Guarantee Fund for external actions [2] concludes that it is not necessary to amend any parameters of the Guarantee Fund to take into account the enlargement of the European Union.
(6) In view of the amount of information needed for the annual report required by Article 7 of Regulation (EC, Euratom) No 2728/94 and the complexity of the procedures to be accomplished before submission of the report, the time provided for its preparation should be extended.
(7) Regulation (EC, Euratom) No 2728/94 should therefore be amended accordingly.
(8) The Treaties provide for no powers, other than those in Article 308 of the EC Treaty and Article 203 of the Euratom Treaty, for the adoption of this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC, Euratom) No 2728/94 is hereby amended as follows:
1. In Article 1, the following subparagraph shall be added:
"All operations carried out for the benefit of a third country or for the purpose of financing projects in a third country shall fall outside the scope of this Regulation with effect from the date on which that country accedes to the European Union.";
2. The following Article shall be inserted:
"Article 3a
Following the accession of a new Member State to the European Union, the target amount shall be reduced by an amount calculated on the basis of the operations referred to in the third subparagraph of Article 1.
In order to calculate the amount of the reduction, the percentage rate referred to in the second subparagraph of Article 3 applicable on the date of accession shall be applied to the amount of those operations outstanding on that date.
The surplus shall be paid back to a special heading in the statement of revenue in the general budget of the European Union.";
3. In Article 7, the date "31 March" shall be replaced by "31 May".
Article 2
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Union.
It shall apply from 1 May 2004.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 22 December 2004.
For the Council
The President
C. Veerman
--------------------------------------------------
[1] OJ C 19, 23.1.2004, p. 3.
[2] OJ L 293, 12.11.1994, p. 1. Regulation as amended by Regulation (EC, Euratom) No 1149/1999 (OJ L 139, 2.6.1999, p. 1).
