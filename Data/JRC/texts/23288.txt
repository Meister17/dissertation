COUNCIL REGULATION (EC) No 1138/98 of 28 May 1998 amending Annexes II and III to Regulation (EC) No 519/94 on common rules for imports from certain third countries
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 113 thereof,
Having regard to the proposal from the Commission,
Whereas Regulation (EC) No 519/94 (1) introduced, in respect of the People's Republic of China, the quotas listed in Annex II to that Regulation and the surveillance measures listed in Annex III thereto;
Whereas the Council's objective in establishing the quotas was to strike a balance between an appropriate level of protection for the Community industries concerned and maintenance of an acceptable level of trade with China, taking into account the various interest in play;
Whereas analysis of the main economic indicators, in particular the volume and market share of Chinese imports, leads to the conclusion that the quota on toys falling within HS/CN codes 9503 41, 9503 49 and 9503 90 should be abolished and that such abolition would be neither inconsistent with the above objective nor liable to disrupt the Community market;
Whereas, in the light of the experience acquired in implementing the quotas, the situation of the Community producers concerned indicates that a 5 % upward adjustment of the quotas would be appropriate and would be neither inconsistent with the above objective nor liable to disrupt the Community market; whereas, however, in the case of footwear the particularly sensitive nature of the industry indicates that for the time being no increase is appropriate;
Whereas the products in respect of which the quota is abolished by this Regulation should, however, be subject to prior Community surveillance, in order to ensure adequate monitoring of the volume and prices of the imports of the products concerned;
Whereas the quantitative quotas and the surveillance measures introduced pursuant to Regulation (EC) No 519/94 should therefore be amended,
HAS ADOPTED THIS REGULATION:
Article 1
Annexes II and III to Regulation (EC) No 519/94 shall be replaced by the Annexes which appear in Annexes I and II to this Regulation respectively.
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Communities.
It shall apply from 1 January 1998.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 28 May 1998.
For the Council
The President
M. FISHER
(1) OJ L 67, 10. 3. 1994, p. 89. Regulation as last amended by Regulation (EC) No 847/97 (OJ L 122, 14. 5. 1997, p. 1).
ANNEX I
'ANNEX II
>TABLE>
`
ANNEX II
'ANNEX III
>TABLE>
`
