Commission Regulation (EC) No 2429/2001
of 12 December 2001
amending Regulation (EC) No 1623/2000 laying down detailed rules for implementing Council Regulation (EC) No 1493/1999 on the common organisation of the market in wine with regard to market mechanisms and amending Regulation (EC) No 442/2001 opening crisis distillation as provided for in Article 30 of Council Regulation (EC) No 1493/1999 for table wines in Portugal
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1493/1999 of 17 May 1999 on the common organisation of the market in wine(1), as last amended by Regulation (EC) No 2826/2000(2), and in particular Article 33 thereof,
Whereas:
(1) As a result of the opening of crisis distillation in Portugal in the 2000/01 wine year, there is a need for addition public storage facilities for alcohol to be delivered to the intervention agency. This has called for major installation work which could not be completed in time. This situation makes it impossible for distillers to meet the delivery date for the alcohol, i.e. 30 November 2001. That date should therefore be extended by one month and this amendment made to apply from 1 December 2001.
(2) Under Article 62(1) of Commission Regulation (EC) No 1623/2000(3), as last amended by Regulation (EC) No 2047/2001(4), the date in question concerns by-products. Under Article 4(3) of Regulation (EC) No 442/2001(5), as amended by Regulation (EC) No 1233/2001(6), the date in question concerns alcohol produced under that distillation.
(3) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Wine,
HAS ADOPTED THIS REGULATION:
Article 1
1. The following subparagraph is added to Article 62(1) of Regulation (EC) No 1623/2000: "Notwithstanding the first subparagraph, as regards Portugal and for the 2000/01 wine year, distillers may deliver products of an alcoholic strength of at least 92 % vol to the intervention agency until 31 December following the wine year concerned at the latest."
2. Article 4(3) of Regulation (EC) No 442/2001 is replaced by the following: "3. The wine shall be delivered to the distilleries by 20 July 2001 at the latest. The alcohol obtained shall be delivered to the intervention agency by 31 December 2001 at the latest."
Article 2
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Communities.
It shall apply from 1 December 2001.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 12 December 2001.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 179, 14.7.1999, p. 1.
(2) OJ L 328, 23.12.2000, p. 2.
(3) OJ L 194, 31.7.2000, p. 45.
(4) OJ L 276, 19.10.2001, p. 15.
(5) OJ L 63, 3.3.2001, p. 52.
(6) OJ L 168, 23.6.2001, p. 11.
