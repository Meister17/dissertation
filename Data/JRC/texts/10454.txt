[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 17.11.2006
COM(2006) 698 final
2006/0230 (ACC)
Proposal for a
COUNCIL DECISION
on the signing and the provisional application of a bilateral agreement in the form of an exchange of letters between the European Community and the Republic of Belarus amending the Agreement between the European Community and the Republic of Belarus on trade in textile products
(presented by the Commission)
EXPLANATORY MEMORANDUM
CONTEXT OF THE PROPOSAL |
110 | Grounds for and objectives of the proposal The bilateral agreement on trade in textile products between the European Community and the Republic of Belarus will expire on 31 December 2006. The proposal provides for an extension of the present textiles agreement until 31 December 2007 and moderate adjustments in some quantitative limits. |
120 | General context The bilateral agreement regulating trade in textiles with the Republic of Belarus has been in place since 1993 and has been amended on several occasions. The current version of this agreement includes tariff commitments on the side of the Republic of Belarus (tariffs largely similar to EU levels) and a snapback clause in case of the non-respect of those tariff levels. Quantitative restrictions are in place for 34 textiles and clothing categories. In the absence of the proposed amendment, the Republic of Belarus would be free to increase its import duties and introduce other import restrictions, to the detriment of EU exports. |
130 | Existing provisions in the area of the proposal OJ L 123, 17.5.1994, p. 1, OJ L 94, 26.4.1995, p. 44, OJ L 81, 30.3.1996, OJ L 336, 29.12.1999, p. 26, OJ L 345, 31.12.2003, p. 150, OJ L 72, 18.3.2005, p. 18. |
140 | Consistency with the other policies and objectives of the Union In view of the political situation prevailing in Belarus, the proposed amendment is restrictive. In 2007 Belarus would be, together with North Korea, Montenegro and Kosovo, the only country subject to textile quotas with the EU. (Agreed levels on certain textiles and clothing categories exist with China following the signing of a Memorandum of Understanding on 10 June 2005). |
CONSULTATION OF INTERESTED PARTIES AND IMPACT ASSESSMENT |
Consultation of interested parties |
219 | This Decision provides for the signature and the provisional application of an international trade agreement amending an agreement that otherwise would have expired. No formal consultation is necessary. |
Collection and use of expertise |
229 | There was no need for external expertise. |
230 | Impact assessment Impacts have been assessed in the course of the negotiations of the international agreement. It was therefore not necessary to conduct an impact assessment of this measure. |
LEGAL ELEMENTS OF THE PROPOSAL |
305 | Summary of the proposed action The proposal provides for an extension of the present textiles agreement until 31 December 2007. There will be no particular negative effects or risks for developing countries. |
310 | Legal basis Article 133 of the Treaty establishing the EC. |
329 | Subsidiarity principle The proposal falls under the exclusive competence of the Community. The subsidiarity principle therefore does not apply. |
Choice of instruments |
341 | Proposed instruments: decision. |
342 | Other means would not be adequate for the following reason(s). There are no other means available for the signature and the provisional application of an international agreement. |
BUDGETARY IMPLICATION |
409 | The proposal has no implication for the Community budget. |
1. 2006/0230 (ACC)
Proposal for a
COUNCIL DECISION
on the signing and the provisional application of a bilateral agreement in the form of an exchange of letters between the European Community and the Republic of Belarus amending the Agreement between the European Community and the Republic of Belarus on trade in textile products
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 133 in conjunction with Article 300(2), first sentence of the first subparagraph thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) The Commission has negotiated on behalf of the Community a bilateral agreement to extend for one year the existing bilateral agreement and protocols on trade in textile products with the Republic of Belarus, with some adjustments of the quantitative limits.
(2) Subject to its possible conclusion at a later date, the proposed agreement should be signed on behalf of the Community.
(3) This bilateral agreement should be applied on a provisional basis as of 1 January 2007, pending the completion of procedures required for its conclusion, subject to the reciprocal provisional application by the Republic of Belarus,
HAS DECIDED AS FOLLOWS:
Article 1
Subject to a possible conclusion at a later date, the President of the Council is hereby authorised to designate the person(s) empowered to sign on behalf of the European Community the agreement in the form of an Exchange of Letters between the European Community and the Republic of Belarus amending the Agreement between the European Community and the Republic of Belarus on trade in textile products, initialled on 1 April 1993, as last amended by an Agreement in the form of an Exchange of Letters initialled on 11 November 2005.
Article 2
The annexed agreement in the form of an Exchange of Letters shall be applied on a provisional basis, pending its formal conclusion and subject to reciprocal provisional application of that agreement by the Republic of Belarus, from 1 January 2007.
Article 3
1. If the Republic of Belarus fails to fulfil its obligations under paragraph 2.4 of the annexed agreement, the quota for 2007 will be reduced to the levels applicable in 2006;
2. The decision to implement paragraph 1 shall be taken in accordance with the procedures referred to in Article 17 of Council Regulation (EEC) No 3030/93 of 12 October 1993 on common rules for imports of certain textile products from third countries[1].
This Decision shall be published in the Official Journal of the European Union.
It shall enter into force the day after its publication in the Official Journal.
Done at Brussels,
For the Council
The President
ANNEX
AGREEMENT in the form of an Exchange of Letters between the European Community and the Republic of Belarus amending the Agreement between the European Community and the Republic of Belarus on trade in textile products
Letter from the Council of the European Union
Sir,
1. I have the honour to refer to the Agreement between the European Community and the Republic of Belarus on trade in textile products initialled on 1 April 1993, as last amended and extended by the Agreement in the form of an Exchange of Letters initialled on 11 November 2005 (hereafter referred to as the 'Agreement').
2. In view of the expiry of the Agreement on 31 December 2006 and in accordance with Article 19(1) of the Agreement, the European Community and the Republic of Belarus agree to extend the duration of the Agreement, for a further period of one year, subject to the following amendments and conditions:
2.1. The text of Article 19(1) of the Agreement shall read as follows:
'This Agreement shall enter into force on the first day of the month following the date on which the Parties notify each other of the completion of the procedures necessary for that purpose. It shall be applicable until 31 December 2007.'
2.2. Annex II which sets out the quantitative restrictions for exports from the Republic of Belarus to the European Community is replaced by Appendix 1 to this letter.
2.3. The Annex to Protocol C which sets out the quantitative restrictions for exports from the Republic of Belarus to the European Community after OPT operations in the Republic of Belarus is replaced for the period of 1 January 2007 to 31 December 2007 by Appendix 2 to this letter.
2.4. Imports into Belarus of textile and clothing products of European Community origin shall be subject in 2007 to custom duties not exceeding those provided for 2003 in Appendix 4 of the Agreement in the form of an Exchange of Letters between the European Community and the Republic of Belarus initialled on 11 November 1999.In the case of non-application of these rates the Community will have the right to reintroduce for the period of the agreement remaining unexpired on a pro rata basis the levels for quantitative restrictions applicable for 2006 as specified in the Exchange of Letters initialled on 11 November 2005.
3. Should the Republic of Belarus become a Member of the World Trade Organisation (WTO) before the date of the expiry of the Agreement, the agreements and rules of the WTO shall be applied from the date of the Republic of Belarus’ accession to the WTO.
4. I should be obliged if you could kindly confirm the acceptance of your Government of the foregoing. Should this be the case, this agreement in the form of an Exchange of Letters shall enter into force on the first day of the month following the day on which the Parties have notified each other that the legal procedures necessary to this end have been completed. In the meantime, it shall be applied provisionally from 1 January 2007 on the condition of reciprocity.
Please accept, Sir, the assurance of my highest consideration.
For the Council of the European Union
Appendix 1
Annex II
Belarus | Category | unit | Quota as from 1 January 2007 |
Group IA | 1 | tonnes | 1 585 |
2 | tonnes | 6 600 |
3 | tonnes | 242 |
Group IB | 4 | T pieces | 1 839 |
5 | T pieces | 1 105 |
6 | T pieces | 1 705 |
7 | T pieces | 1 377 |
8 | T pieces | 1160 |
Group IIA | 9 | tonnes | 363 |
20 | tonnes | 329 |
22 | tonnes | 524 |
23 | tonnes | 255 |
39 | tonnes | 241 |
Group IIB | 12 | T pairs | 5 959 |
13 | T pieces | 2 651 |
15 | T pieces | 1 726 |
16 | T pieces | 186 |
21 | T pieces | 930 |
24 | T pieces | 844 |
26/27 | T pieces | 1 117 |
29 | T pieces | 468 |
73 | T pieces | 329 |
83 | tonnes | 184 |
Group IIIA | 33 | tonnes | 387 |
36 | tonnes | 1 309 |
37 | tonnes | 463 |
50 | tonnes | 207 |
Group IIIB | 67 | tonnes | 356 |
74 | T pieces | 377 |
90 | tonnes | 208 |
Group IV | 115 | tonnes | 114 |
117 | tonnes | 2 310 |
118 | tonnes | 471 |
T pieces: thousand of pieces
Appendix 2
Annex to Protocol C
Category | Unit | As from 1 January 2007 |
4 | 1 000 pieces | 5 399 |
5 | 1 000 pieces | 7 526 |
6 | 1 000 pieces | 10 037 |
7 | 1 000 pieces | 7 534 |
8 | 1 000 pieces | 2 565 |
12 | 1 000 pieces | 5 072 |
13 | 1 000 pieces | 795 |
15 | 1 000 pieces | 4 400 |
16 | 1 000 pieces | 896 |
21 | 1 000 pieces | 2 927 |
24 | 1 000 pieces | 754 |
26/27 | 1 000 pieces | 3 668 |
29 | 1 000 pieces | 1 487 |
73 | 1 000 pieces | 5 700 |
83 | Tonnes | 757 |
74 | 1 000 pieces | 994 |
Letter from the Government of the Republic of Belarus
Sir,
I have the honour to acknowledge receipt of your letter of ….. which reads as follows:
“ Sir,
1. I have the honour to refer to the Agreement between the European Community and the Republic of Belarus on trade in textile products initialled on 1 April 1993, as last amended and extended by the Agreement in the form of an Exchange of Letters initialled on 11 November 2005 (hereafter referred to as the 'Agreement').
2. In view of the expiry of the Agreement on 31 December 2006 and in accordance with Article 19(1) of the Agreement, the European Community and the Republic of Belarus agree to extend the duration of the Agreement, for a further period of one year, subject to the following amendments and conditions:
2.1. The text of Article 19(1) of the Agreement shall read as follows:
'This Agreement shall enter into force on the first day of the month following the date on which the Parties notify each other of the completion of the procedures necessary for that purpose. It shall be applicable until 31 December 2007.'
2.2. Annex II which sets out the quantitative restrictions for exports from the Republic of Belarus to the European Community is replaced by Appendix 1 to this letter.
2.3. The Annex to Protocol C which sets out the quantitative restrictions for exports from the Republic of Belarus to the European Community after OPT operations in the Republic of Belarus is replaced for the period of 1 January 2007 to 31 December 2007 by Appendix 2 to this letter.
2.4. Imports into Belarus of textile and clothing products of European Community origin shall be subject in 2007 to custom duties not exceeding those provided for 2003 in Appendix 4 of the Agreement in the form of an Exchange of Letters between the European Community and the Republic of Belarus initialled on 11 November 1999.In the case of non-application of these rates the Community will have the right to reintroduce for the period of the agreement remaining unexpired on a pro rata basis the levels for quantitative restrictions applicable for 2006 as specified in the Exchange of Letters initialled on 11 November 2005.
3. Should the Republic of Belarus become a Member of the World Trade Organisation (WTO) before the date of the expiry of the Agreement, the agreements and rules of the WTO shall be applied from the date of the Republic of Belarus’ accession to the WTO.
4. I should be obliged if you could kindly confirm the acceptance of your Government of the foregoing. Should this be the case, this agreement in the form of an Exchange of Letters shall enter into force on the first day of the month following the day on which the Parties have notified each other that the legal procedures necessary to this end have been completed. In the meantime, it shall be applied provisionally from 1 January 2007 on the condition of reciprocity.
Please accept, Sir, the assurance of my highest consideration.”
I have the honour to confirm that my Government is in agreement with the content of your letter.
Please accept, Sir, the assurance of my highest consideration.
For the Government of the Republic of Belarus
[1] OJ L 275, 8.11.1993, p. 1. Regulation last amended by Commission Regulation (EC) No 35/2006 of 11 January 2006 (OJ L 7, 12.1.2006, p. 8).
