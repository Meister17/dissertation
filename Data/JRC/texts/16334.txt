COMMISSION REGULATION (EC) No 1147/96 of 25 June 1996 amending Annexes II and III to Council Regulation (EEC) No 2377/90 laying down a Community procedure for the establishment of maximum residue limits of veterinary medicinal products in foodstuffs of animal origin (Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2377/90 of 26 June 1990 laying down a Community procedure for the establishment of maximum residue limits of veterinary medicinal products in foodstuffs of animal origin (1), as last amended by Commission Regulation (EC) No 0000/96 (2), and in particular Articles 6, 7 and 8 thereof;
Whereas, in accordance with Regulation (EEC) No 2377/90, maximum residue limits must be established progressively for all pharmacologically active substances which are used within the Community in veterinary medicinal products intended for administration to food-producing animals;
Whereas maximum residue limits should be established only after the examination within the Committee for Veterinary Medicinal Products of all the relevant information concerning the safety of residues of the substance concerned for the consumer of foodstuffs of animal origin and the impact of residues on the industrial processing of foodstuffs;
Whereas, in establishing maximum residue limits for residues of veterinary medicinal products in foodstuffs of animal origin, it is necessary to specify the animal species in which residues may be present, the levels which may be present in each of the relevant meat tissues obtained from the treated animal (target tissue) and the nature of the residue which is relevant for the monitoring of residues (marker residue);
Whereas, for the control of residues, as provided for in appropriate Community legislation, maximum residue limits should usually be established for the target tissues of liver or kidney; whereas, however, the liver and kidney are frequently removed from carcases moving in international trade, and maximum residue limits should therefore also always be established for muscle or fat tissues;
Whereas, in the case of veterinary medicinal products intended for use in laying birds, lactating animals or honey bees, maximum residue limits must also be established for eggs, milk or honey;
Whereas propane, n-butane, isobutane, papaverine, polyethylene glycols (molecular weight ranging from 200 to 10 000), policresulen, magnesium and its compounds, papain and phenol should be inserted into Annex II to Regulation (EEC) No 2377/90;
Whereas, in order to allow for the completion of scientific studies, clorsulon and vedaprofen should be inserted into Annex III to Regulation (EEC) No 2377/90;
Whereas a period of 60 days should be allowed before the entry into force of this Regulation in order to allow Member States to make any adjustment which may be necessary to the authorizations to place the veterinary medicinal products concerned on the market which have been granted in accordance with the Council Directive 81/851/EEC (3), as last amended by Directive 93/40/EEC (4), to take account of the provisions of this Regulation;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Standing Committee on Veterinary Medicinal Products,
HAS ADOPTED THIS REGULATION:
Article 1
Annexes II and III to Regulation (EEC) No 2377/90 are hereby amended as set out in the Annex hereto.
Article 2
This Regulation shall enter into force on the 60th day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 25 June 1996.
For the Commission
Martin BANGEMANN
Member of the Commission
(1) OJ No L 224, 18. 8. 1990, p. 1.
(2) See page 6 of this Official Journal.
(3) OJ No L 317, 6. 11. 1981, p. 1.
(4) OJ No L 214, 24. 8. 1993, p. 31.
ANNEX
A. Annex II is modified as follows:
1. Inorganic chemicals
>TABLE>
2. Organic compounds
>TABLE>
3. Substances generally recognized as safe
>TABLE>
B. Annex III is modified as follows:
1. Anti-infectious agents
1.1. Chemotherapeutics
1.1.5. Benzenesulphonamides
>TABLE>
5. Anti-inflammatory agents
5.1. Nonsteroidal anti-inflammatory agents
5.1.1. Arylpropionic acid derivative
>TABLE>
