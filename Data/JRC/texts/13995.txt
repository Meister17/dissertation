Action brought on 18 April 2006 — Commission of the European Communities v Kingdom of Spain
Parties
Applicant: Commission of the European Communities (represented by: D. Recchia and A. Alcover San Pedro, Agents, acting as Agents)
Defendant: Kingdom of Spain
Form of order sought
- a declaration that, in relation to the plan to irrigate the irrigable area of the Segarra-Garrigues Canal, the Kingdom of Spain has failed to fulfil its obligations under Articles 2, 3 and 4(1) and (4) of Council Directive 79/409/EEC [1] of 2 April 1979 on the conservation of wild birds;
- an order that the Kingdom of Spain should pay the costs.
Pleas in law and main arguments
The Member States may not take advantage of their failure to designate Zones in accordance with the Directive in order to perform actions in those areas that may endanger its objectives and when, as in this case, the Member State has not designated protected areas where it ought to have done, the levels of protection required in an area of importance for birds are those laid down in Article 4(4) of the Directive.
In consequence, the Commission takes the view that, by having provided for and implemented the procedures leading to the authorisation of the Segarra-Garrigues Canal irrigation plan, the Kingdom of Spain has failed to fulfil its obligations under Articles 2, 3 and 4(1) and (4) of the Directive.
[1] OJ 1979 L 103, p. 1
--------------------------------------------------
