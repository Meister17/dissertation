COUNCIL AND COMMISSION DECISION of 23 September 1997 on the conclusion, by the European Communities, of the Energy Charter Treaty and the Energy Charter Protocol on energy efficiency and related environmental aspects (1) (98/181/EC, ECSC, Euratom)
THE COUNCIL OF THE EUROPEAN UNION,
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Coal and Steel Community and, in particular, Article 95 thereof,
Having regard to the Treaty establishing the European Community and, in particular, Article 54(2), the last sentence of Article 57(2), Articles 66, 73c(2), 87, 99, 100a, 113, 130s(1) and 235, in conjunction with the second sentence of Article 228(2) and the second subparagraph of Article 228(3) thereof,
Having regard to the Treaty establishing the European Atomic Energy Community and, in particular, the second paragraph of Article 101 thereof,
Having regard to the opinion of the Consultative Committee set up by the Treaty establishing the European Coal and Steel Community,
Having regard to the proposal from the Commission,
Having regard to the unanimous assent of the Council pursuant to Article 95 of the Treaty establishing the European Coal and Steel Community,
Having regard to the assent of the European Parliament (2),
Having regard to the approval given by the Council pursuant to Article 101 of the Treaty establishing the European Atomic Energy Community,
Whereas the European Energy Charter was signed by the European Communities and by their Member States on 17 December 1991;
Whereas, on 17 December 1994, the European Communities and their Member States signed the Energy Charter Treaty and the Energy Charter Protocol on energy efficiency and related environmental aspects in order to provide a secure and binding international legal framework for the principles and objectives set out in that Charter;
Whereas the European Communities and their Member States have been applying the Energy Charter Treaty provisionally by virtue of Council Decisions 94/998/ EC (3) and 94/1067/Euratom (4) since the date of signature thereof;
Whereas the principles and objectives of the Energy Charter Treaty are of fundamental importance to Europe's future, allowing the members of the Commonwealth of Independent States and the countries of Central and Eastern Europe to develop their energy potential, while helping to improve security of supply;
Whereas the principles and objectives of the Energy Charter Protocol will help to provide greater protection for the environment, notably by promoting energy efficiency;
Whereas it is necessary to consolidate the initiative and the central role of the European Communities, by enabling the latter to participate fully in the implementation of the Energy Charter Treaty and the Energy Charter Protocol;
Whereas conclusion of the Energy Charter Treaty and the Energy Charter Protocol will help attain the objectives of the European Communities;
Whereas Article 73c(2) of the Treaty establishing the European Community must be used as a legal basis for this Decision, since the Energy Charter Treaty imposes certain obligations on the European Communities regarding the movement of capital and payments between the Communities and third country contracting parties to the Energy Charter Treaty;
Whereas the Energy Charter Treaty could affect legislative acts based on Article 235 of the Treaty establishing the European Community; whereas the latter Treaty has provided no powers other than those arising from Article 235 for implementation of the obligations imposed by that Treaty with regard to cooperation in the energy field;
Whereas the Energy Charter Treaty and the Energy Charter Protocol must be approved by the European Communities;
Whereas, in order to ensure uniform external representation of the European Communities, both in the conclusion procedure and in implementing the commitments entered into by the European Communities and the Member States, appropriate coordination procedures should be established; whereas, to this end, provision should be made for this Decision to be deposited with the Government of the Portuguese Republic at the same time as the instruments of ratification of the Member States; whereas, for the same reason, the position to be adopted by the European Communities and the Member States must be coordinated with regard to the decisions to be taken by the Energy Charter Conference set up by the Energy Charter Treaty in the fields for which they share responsibility;
Whereas the Energy Charter Conference has autonomous decision-making powers; whereas, consequently, appropriate procedures must be provided for establishing the European Communities' position within the Energy Charter Conference;
Whereas these procedures should be of a simplified nature, in order to allow for an effective participation of the European Communities in the said Conference;
Whereas, with regard to decisions of the aforementioned Conference requiring the adoption or modification of Community legislation, the Council and the Commission shall act in accordance with the rules laid down in Article 228(1) and (2) of the Treaty establishing the European Community;
Whereas, where the decisions to be taken by the Energy Charter Conference concern areas of mixed competence, the European Communities and the Member States are to cooperate with a view to achieving a common position, in accordance with the jurisprudence of the Court of Justice of the European Communities,
HAVE DECIDED AS FOLLOWS:
Article 1
The Energy Charter Treaty and the Energy Charter Protocol on energy efficiency and related environmental aspects (hereinafter referred to as the 'Energy Charter Protocol`) are hereby approved on behalf of the European Coal and Steel Community, the European Community and the European Atomic Energy Community.
The texts of the Energy Charter Treaty and of the Energy Charter Protocol are attached to this Decision.
Article 2
The President of the Council shall, on behalf of the European Community, deposit the instrument of approval of the Energy Charter Treaty and of the Energy Charter Protocol with the Government of the Portuguese Republic, in accordance with Articles 39 and 49 of the Energy Charter Treaty and with Articles 15 and 21 of the Energy Charter Protocol. Under the same conditions, the President of the Commission shall deposit the instruments of approval on behalf of the European Coal and Steel Community and of the European Atomic Energy Community.
The President of the Council and the President of the Commission, acting on behalf of the European Community, the European Coal and Steel Community and the European Atomic Energy Community respectively, shall consult with the Member States in order to ensure, as far as possible, the simultaneous deposit of their instruments of approval.
Article 3
1. The position which the European Community may be required to take within the Energy Charter Conference, set up by the Energy Charter Treaty, with regard to decisions of that Conference requiring the introduction or modification of Community legislation shall, subject to paragraph 3, be adopted by the Council, acting in accordance with the relevant rules of the Treaty establishing the European Community.
The Council shall act by qualified majority. However, the Council shall act by unanimity if the decision to be taken by the said Conference covers a field for which unanimity would be required for the adoption of internal Community rules.
2. In other cases, the position to be taken by the European Community shall be adopted by the Council.
3. For matters falling within paragraph 1, the Council and the Commission shall fully inform the European Parliament on a regular basis and shall give the European Parliament the opportunity to express its views on the position of the Community to be taken in the Conference referred to in paragraph 1.
In the case of decisions of the said Conference pursuant to Article 34(7) of the Energy Charter Treaty, the Council shall consult, or obtain the assent of, the European Parliament before taking its decision, under the conditions laid down in Article 228(3) of the Treaty establishing the European Community.
4. The position to be taken in the name of the European Coal and Steel Community shall be adopted by the Commission with the approval of the Council, acting by qualified majority or unanimity according to the subject matter.
5. The position to be taken in the name of the European Atomic Energy Community shall be adopted by the Commission with the approval of the Council acting by a qualified majority.
Article 4
This Decision shall be published in the Official Journal of the European Communities.
Done at Brussels, 23 September 1997.
For the Council
The President
H. WIJERS
For the Commission
The President
J. SANTER
(1) The Decision on the conclusion of the Energy Charter Treaty on behalf of the European Community was adopted by the Council on 27 May 1997.
(2) OJ C 85, 17.3.1997.
(3) OJ L 380, 31.12.1994, p. 1.
(4) OJ L 380, 31.12.1994, p. 113.
