COMMISSION DECISION
of 16 December 1999
establishing the detailed rules for the application of Article 9 of Council Directive 97/78/EC concerning the transhipment of products at a Border Inspection Post where the consignments are intended for eventual import into the European Community, and amending Commission Decision 93/14/EEC
(notified under document number C(1999) 4506)
(Text with EEA relevance)
(2000/25/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 97/78/EC of 18 December 1997 laying down the principles governing the organisation of veterinary checks on products entering the Community from third countries(1), and particularly Article 9(2), thereof,
(1) Whereas it is important that the official veterinarian responsible at a border inspection post receives appropriate information in the case of consignments of products that are transhipped before being introduced into the EU at another border inspection post;
(2) Whereas it is appropriate to define the time periods which specify the veterinary checks required to be carried out;
(3) Whereas it is important to specify that the consignments of products must be submitted to the full range of veterinary checks laid down after the end of the maximum period envisaged;
(4) Whereas the existing rules for transhipment of consignments at a border inspection post are laid down in Article 4 of Commission Decision 93/14/EEC(2) and must be repealed and replaced by this Decision;
(5) Whereas the measures provided for in this decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
When consignments of products are presented at a border inspection post for subsequent transhipment, the person responsible for the load must notify the official veterinarian responsible at the border inspection post, at the time of arrival and by a means fixed by the competent authority, of the estimated time of unloading of the consignment, the border inspection post of destination, and if necessary the exact location of the consignment.
Article 2
In order to apply Article 9(1)(b)(i) of Directive 97/78/EC:
- for an airport the minimum period shall be 12 hours and the maximum period shall be 48 hours;
- for a seaport the minimum period shall be seven days and the maximum period shall be 20 days.
Article 3
In cases where the maximum period defined in Article 2 has elapsed the consignment must be submitted to the checks laid down in Article 4 of Directive 97/78/EC, at the border inspection post of introduction.
Article 4
Article 4 of Decision 93/14/EEC is hereby repealed.
Article 5
The present decision shall apply from 1 January 2000.
Article 6
This Decision is addressed to Member States.
Done at Brussels, 16 December 1999.
For the Commission
David BYRNE
Member of the Commission
(1) OJ L 24, 30.1.1998, p. 3.
(2) OJ L 9, 15.1.1993, p. 42.
