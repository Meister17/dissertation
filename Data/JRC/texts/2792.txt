Commission Regulation (EC) No 1216/2005
of 28 July 2005
amending Regulation (EC) No 1227/2000 laying down detailed rules for the application of Council Regulation (EC) No 1493/1999 on the common organisation of the market in wine, as regards production potential
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1493/1999 of 17 May 1999 on the common organisation of the market in wine [1], and in particular Article 80(b) thereof,
Whereas:
(1) Several Member States are having serious difficulties applying the provisions of Article 2(3) of Regulation (EC) No 1493/1999 concerning the regularisation of illegal plantation. These difficulties concern, in particular, the date from which the procedure in question is to commence. Applying the various provisions regarding the grant of the derogation imposes a serious and complex administrative burden, particularly as regards checks and penalties.
(2) To overcome these difficulties, the provisions on the regularisation of illegal plantation need to be amended. Pending the submission of an appropriate proposal to the Council and in the interests of sound administration, the date for the closure of the procedure laid down in Commission Regulation (EC) No 1227/2000 [2] should therefore be postponed definitively to 31 December 2007.
(3) Regulation (EC) No 1227/2000 should therefore be amended accordingly.
(4) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Wine,
HAS ADOPTED THIS REGULATION:
Article 1
Article 2(1a) of Regulation (EC) No 1227/2000 is hereby replaced by the following:
"1a. The deadline laid down in Article 2(3) of Regulation (EC) No 1493/1999 shall be postponed to 31 December 2007."
Article 2
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Union.
It shall apply from 1 August 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 28 July 2005.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 179, 14.7.1999, p. 1. Regulation last amended by Commission Regulation (EC) No 1795/2003 (OJ L 262, 14.10.2003, p. 13).
[2] OJ L 143, 16.6.2000, p. 1. Regulation last amended by Regulation (EC) No 1074/2005 (OJ L 175, 8.7.2005, p. 12).
--------------------------------------------------
