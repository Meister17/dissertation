Order of the Court of First Instance of 20 March 2006 — Bioelettrica
v Commission
(Case T -287/01) [1]
(2006/C 121/32)
Language of the case: Italian
The President of the Third Chamber has ordered that the case be removed from the register.
[1] OJ C 31, 2.2.2002.
--------------------------------------------------
