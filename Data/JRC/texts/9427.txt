Commission Regulation (EC) No 1632/2006
of 3 November 2006
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 4 November 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 3 November 2006.
For the Commission
Jean-Luc Demarty
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 386/2005 (OJ L 62, 9.3.2005, p. 3).
--------------------------------------------------
ANNEX
to Commission Regulation of 3 November 2006 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code [1] | Standard import value |
07020000 | 052 | 62,9 |
096 | 40,4 |
204 | 53,3 |
999 | 52,2 |
07070005 | 052 | 103,8 |
096 | 81,8 |
204 | 46,9 |
220 | 155,5 |
628 | 196,3 |
999 | 116,9 |
07099070 | 052 | 85,0 |
204 | 54,1 |
999 | 69,6 |
08055010 | 052 | 68,1 |
388 | 49,1 |
524 | 46,0 |
528 | 42,9 |
999 | 51,5 |
08061010 | 052 | 102,1 |
508 | 250,4 |
999 | 176,3 |
08081080 | 388 | 81,9 |
400 | 99,0 |
800 | 160,2 |
804 | 103,2 |
999 | 111,1 |
08082050 | 052 | 100,1 |
400 | 174,0 |
720 | 78,0 |
999 | 117,4 |
[1] Country nomenclature as fixed by Commission Regulation (EC) No 750/2005 (OJ L 126, 19.5.2005, p. 12). Code "999" stands for "of other origin".
--------------------------------------------------
