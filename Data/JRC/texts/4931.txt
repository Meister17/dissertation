Judgment of the Court
(Third Chamber)
of 8 September 2005
in Case C-416/02: Commission of the European Communities v Kingdom of Spain [1]
In Case C-416/02: Action under Article 226 EC for failure to fulfil obligations, brought on 19 November 2002, Commission of the European Communities (Agent: G. Valero Jordana) supported by United Kingdom of Great Britain and Northern Ireland (Agents: K. Manji, and subsequently C. White, instructing D. Wyatt QC) v Kingdom of Spain (Agent: N. Díaz Abad) — the Court (Third Chamber), composed of A. Rosas, President of the Chamber, J.-P. Puissochet (Rapporteur), S. von Bahr, U. Lõhmus and A. Ó Caoimh, Judges; C. Stix-Hackl, Advocate General; M. Ferreira, Principal Administrator, for the Registrar, gave a judgment on 8 September 2005, in which it:
1. Declares that, by failing to ensure that urban waste water from the agglomeration of Vera is subjected to such treatment as is required by Article 5(2) of Council Directive 91/271/EEC of 21 May 1991 concerning urban waste-water treatment, that is to say treatment which is more stringent than that described in Article 4 of that directive, and by failing to designate the Rambla de Mojácar as a vulnerable zone contrary to Article 3(1), (2) and (4) of Council Directive 91/676/EEC of 12 December 1991 concerning the protection of waters against pollution caused by nitrates from agricultural sources, the Kingdom of Spain has failed to fulfil its obligations under those directives;
2. Dismisses the remainder of the action;
3. Orders the Kingdom of Spain to bear two thirds of all the costs and the Commission of the European Communities to bear the other third;
4. Orders the United Kingdom of Great Britain and Northern Ireland to bear its own costs.
[1] OJ C 31 of 08. 02. 2003.
--------------------------------------------------
