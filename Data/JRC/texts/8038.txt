*****
COMMISSION DECISION
of 30 July 1990
on the list of establishments in Namibia approved for the purpose of importing fresh meat into the Community
(90/432/EEC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Directive 72/462/EEC of 12 December 1972 on health and veterinary inspection problems upon importation of bovine animals and swine and fresh meat or meat products from third countries (1), as last amended by Directive 89/662/EEC (2), and in particular Articles 4 (1) and 18 (1) thereof,
Whereas establishments in third countries cannot be authorized to export fresh meat to the Community unless they satisfy the general and special conditions laid down in Directive 72/462/EEC;
Whereas Namibia has become independent, it is necessary to establish the list of establishments approved in that country for the purpose of importing fresh meat into the Community;
Whereas these establishments are regularly inspected in the framework of the Community missions carried out in the south of Africa pursuant to Article 5 of Directive 72/462/EEC and Article 2 (1) of Commission Decision 86/474/EEC on the implementation of the on-the-spot inspections to be carried out in respect of the importation of bovine animals and swine and fresh meat from non-member countries (3);
Whereas the standard of hygiene in the establishments in Namibia can be regarded as satisfactory and in these circumstances, they can be included in a list of establishments authorized to export to the Community;
Whereas import of fresh meat from the establishments appearing in the Annex remains subject to Community provisions laid down elsewhere, in particular concerning animal health, examination for the presence of residues in fresh meat, prohibition of the use in livestock farming of certain substances having a hormonal action;
Whereas the measures provided for in this Decision are in accordance with the opinion of the Stanbding Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
1. The establishments in Namibia appearing in the Annex are hereby approved for the import of fresh into the Community pursuant to the said Annex.
2. Imports from establishments listed in the Annex shall remain subject to the Community veterinary provisions laid down elsewhere.
Article 2
Member States shall prohibit imports of fresh meat coming from establishments not appearing in the Annex.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 30 July 1990.
For the Commission
Ray MAC SHARRY
Member of the Commission
(1) OJ No L 302, 31. 12. 1972, p. 28.
(2) OJ No L 395, 30. 12. 1989, p. 13.
(3) OJ No L 279, 30. 9. 1986, p. 55.
ANNEX
List of establishments
1.2.3,10 // // // // Approval No // Establishment/address // Category (*) // // // // // // // 1.2.3.4.5.6.7.8.9.10 // // // SL // CP // CS // B // S/G // P // SP // SR // // // // // // // // // // // 22 // Swavleis, Windhoek // × // × // // × // // // × // (1) // // // // // // // // // // // 23 // Swavleis, Okahandja // × // × // // × // // // // (1) // // // // // // // // // // // 27 // Windhoek Wild, Windhoek // // × // // × // // // // (2) // // // // // // // // // // 1.2.3.4.5.6.7 // (*) // SL: // Slaughterhouse // B: // Bovine Meat // SR: // Special Remarks // // CP: // Cutting Premises // S/G: // Sheep Meat/Goat Meat // // // // CS: // Cold Store // P: // Pigmeat // // // // // // SP: // Meat from Solipeds // //
1.2 // (1) // Offal excluded. // (2) // Only when the establishment contains no meat of wild clovenhoofed animals at the time bovine meat is cut.
