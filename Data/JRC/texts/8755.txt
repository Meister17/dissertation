COUNCIL DIRECTIVE of 21 December 1988 amending for the fourth time Directive 76/768/EEC on the approximation of the laws of the Member States relating to cosmetic products (88/667/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 100a thereof,
Having regard to the proposal from the Commission (1),
In cooperation with the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas the successive amendments made to the Annexes to Directive 76/768/EEC (4), as last amended by Directive 88/233/EEC (5), require the provisions of the Directive to be adapted;
Whereas experience gained since the adoption of Directive 76/768/EEC has shown that the provisions on labelling should be improved and that the period laid down in Article 12 (2) is inadequate,
HAS ADOPTED THIS DIRECTIVE:
Article 1 Directive 76/768/EEC is hereby amended as follows:
1. In Article 1, paragraph 3 is replaced by:
´3. Cosmetic products containing one of the substances listed in Annex V shall be excluded from the scope of this Directive. Member States may take such measures as they deem necessary with regard to those products.'
2. In Article 4, (c) and (d) are replaced by the following:
´(c) colouring agents other than those listed in Annex IV, Part 1, with the exception of cosmetic products containing colouring agents intended solely to colour hair;
(d) colouring agents listed in Annex IV, Part 1, used outside the conditions laid down, with the exception of cosmetic products containing colouring agents intended solely to colour hair.'
3. Article 5 is replaced by the following:
Article 5Member States shall allow the marketing of cosmetic products containing:
(a) the substances listed in Annex III, Part 2, within the limits and under the conditions laid down, up to the dates in column (g) of that Annex;
(b) the colouring agents listed in Annex IV, Part 2, within the limits and under the conditions laid down, until the admission dates given in that Annex;
(c) the preservatives listed in Annex VI, Part 2, within the limits and under the condition laid down, until the dates given in column (f) of that Annex. However, some of these substances may be used in other concentrations for specific purposes apparent from the presentation of the product;
(d) the UV filters listed in Part 2 of Annex VII, within the limits and under the conditions laid down, until the dates given in column (f) of that Annex.
At these dates, these substances, colouring agents, preservatives and UV filters shall be:
- definitively allowed, or
- definitively prohibited (Annex II), or
- maintained for a given period specified in Part 2 of Annexes III, IV, VI an- deleted from all the Annexes, on the basis of available scientific information or because they are no longer used.'
4. Article 6 is replaced by the following
´Article 6
1. Member States shall take all measures necessary to ensure that cosmetic products may be marketed only if the container and packaging bear the following informarion in indelible, easily legible and visible lettering:
(a) the name or style and the address or registered office of the manufacturer or the person responsible for marketing the cosmetic product who is established within the Community. Such information may be abbreviated in so far as the abbreviation makes it generally possible to identify the undertaking. Member States may require that the country of origin be specified for goods manufactured outside the Community;
b) the nominal content at the time of packaging, given by weight or by volume, except in the case of packaging containing less than five grams or five millilitres, free samples and single-application packs; for pre-packages normally sold as a number of items, for which details of weight or volume are not significant, the content need not be given provided the number of items appears on the packaging. This information need not be given if the number of items is easy to see from the outside or if the product is normally only sold individually;
c) the date of minimum durability. The date of minimum durability of a cosmetic product shall be the date until which this product, stored under appropriate conditions, continues to fulfil its initial function and, in particular, remains in conformity with Article 2.
The date of minimum durability shall be indicated by the words: "Best used before the end of...'' followed by either:
- the date itself, or
- details of where the date appears on the packaging.
If necessary, this information shall be supplemented by an indication of the conditions which must be satisfied to guarantee the stated durability.
The date shall be clearly expressed and shall consist of the month and the year in that order. Indication of the date of durability shall not be mandatory for cosmetic products the minimum durability of which exceeds 30 months;
(d) particular precautions to be observed in use, and especially those listed in the column "Conditions of use and warnings which must be printed on the label'' in Annexes III, IV, VI and VII, which must appear on the container and packaging as well as any special precautionary information on cosmetic products for professional use, in particular in hairdressing. Where this is impossible for practical reasons, this information must appear on an enclosed leaflet, with abbreviated information on the container and the packaging referring the consumer to the information specified;
(e) the batch number of manufacture or the reference for identifying the goods. Where this is impossible for practical reasons because the cosmetic products are too small, such information need appear only on the packaging.
2. For cosmetic products that are not pre-packaged, are packaged at the point of sale at the purchaser's request, or are pre-packaged for immediate sale, Member States shall adopt detailed rules for indication of the particulars referred to in paragraph 1.
3. Member States shall take all measures necessary to ensure that, in the labelling, putting up for sale and advertising of cosmetic products, text, names, trade marks, pictures and figurative or other signs are not used to imply that these products have characteristics which they do not have.'
5. Article 12 (2) is replaced by:
´2. The Commission shall as soon as possible consult the Member States concerned, following which it shall deliver its opinion without delay and take the appropriate steps.'
6. Annex III, Part 2, becomes Annex IV, Part 1.
7. Annex IV, Part 1, becomes Annex III, Part 2.
Article 2 1. Member States shall take all necessary measures to ensure that as from 1 January 1992 neither manufacturers nor importers established in the Community place on the market products whose labelling does not satisfy the requirements of this Directive.
2. Member States shall take all necessary measures to ensure that the products referred to in paragraph 1 can no
longer be sold or disposed of to the final consumer after 31 December 1993.
Article 3 1. Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with the provisions of this Directive not later than 31 December 1989. They shall forthwith inform the Commission thereof.
2. Member States shall communicate to the Commission the provisions of national law which they adopt in the field governed by this Directive.
Article 4 This Directive is addressed to the Member States.
Done at Brussels, 21 December 1988.
For the Council
The President
V. PAPANDREOU
(1) OJ No C 86, 1. 4. 1987, p. 3.
(2) OJ No C 122, 9. 5. 1988, p. 80 and decision of 14 December 1988 (not yet published in the Official Journal).
(3) OJ No C 319, 30. 11. 1987, p. 5.
(4) OJ No L 262, 27. 9. 1976, p. 169.
(5) OJ No L 105, 26. 4. 1988, p. 11.
