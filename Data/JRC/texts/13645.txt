Judgment of the Court (Fourth Chamber) of 4 May 2006 (reference for a preliminary ruling from the Verwaltungsgerichtshof Baden-Württemberg — Germany) — Reinhold Haug v Land Baden-Württemberg
(Case C-286/05) [1]
Referring court
Verwaltungsgerichtshof Baden-Württemberg
Parties to the main proceedings
Applicant: Reinhold Haug
Defendant: Land Baden-Württemberg
Re:
Reference for a preliminary ruling — Verwaltungsgerichtshof (Administrative Court) Baden-Württemberg — Interpretation of Arts. 2(2), second sentence, 4(1) and (4) and 5(1) of Council Regulation (EC, Euratom) No 2988/95 of 18 December 1995 on the protection of the European Communities' financial interests (OJ 1995 L 312, p. 1) and Article 31(3) of Commission Regulation (EC) No 2419/2001 of 11 December 2001 laying down detailed rules for applying the integrated administration and control system for certain Community aid schemes established by Council Regulation (EEC) No 3508/92 (OJ 2001 L 327, p. 11) — Retroactive application of a less strict provision — Meaning of "administrative measure" and "administrative penalty" — Repayment of area aid unduly received
Operative part of the judgment
The second sentence of Article 2(2) of Council Regulation (EC, Euratom) No 2988/95 of 18 December 1995 on the protection of the European Communities' financial interests does not apply if, a difference of more than 20 % of the determined area within the meaning of Article 9(2) of Commission Regulation (EEC) No 3887/92 of 23 December 1992 laying down detailed rules for applying the integrated administration and control system for certain Community aid schemes having been found, full repayment of the Community aid initially granted, together with interest, is sought whereas the economic operator concerned contends that the amount of the aid to be repaid might be lower under Article 31(3) of Commission Regulation (EC) No 2419/2001 of 11 December 2001 laying down detailed rules for applying the integrated administration and control system for certain Community aid schemes established by Council Regulation (EEC) No 3508/92.
[1] OJ C 229, 17.9.2005.
--------------------------------------------------
