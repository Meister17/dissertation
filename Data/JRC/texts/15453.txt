Judgment of the Court of First Instance of 23 February 2006 — Il Ponte Finanziaria v OHIM
(Case T-194/03) [1]
Parties:
Applicant: Il Ponte Finanziaria (Scandicci, Italy) (represented by: P.L. Roncaglia, A. Torrigiani Malaspina and M. Boletto, lawyers)
Defendant: Office for Harmonisation in the Internal Market (Trade Marks and Designs) (represented by: M. Buffolo and O. Montalto, agents)
Other party to the proceedings before the Board of Appeal of OHIM intervening before the Court of First Instance: Marine Enterprise Projects — Società Unipersonale di Alberto Fiorenzi Srl (Numana, Italy) (represented by: D. Marchi, lawyer)
Action
brought against the decision of the Fourth Board of Appeal of OHIM of 17 March 2003 (Case R 1015/2001-4) relating to opposition proceedings between Il Ponte Finanziaria SpA and Marine Enterprise Projects — Società Unipersonale di Alberto Fiorenzi Srl
Operative part of the judgment
The Court:
1. dismisses the action;
2. orders the applicant to pay the costs.
[1] OJ 2003 C 184.
--------------------------------------------------
