COUNCIL DIRECTIVE of 27 July 1976 on the approximation of the laws of the Member States relating to alcoholometers and alcohol hydrometers (76/765/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 100 thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament (1),
Having regard to the opinion of the Economic and Social Committee (2),
Whereas in the Member States the definition, design and procedures for the approval and testing of alcoholometers and alcohol hydrometers are the subject of mandatory provisions which differ from one Member State to another and consequently hinder the movement of and trade in these instruments within the Community ; whereas it is therefore necessary to approximate these provisions;
Whereas harmonization of the laws, regulations and administrative provisions relating to these instruments is essential also as a complement to existing provisions relating to the method of determining alcoholic strength from the results of measurements taken, in order to remove all risk of ambiguity or dispute over the results of such measurements;
Whereas Council Directive 71/316/EEC of 26 July 1971 on the approximation of the laws of the Member States relating to common provisions for both measuring instruments and methods of metrological control (3) laid down the EEC pattern approval and EEC initial verification procedures ; whereas in accordance with that Directive it is necessary to lay down the technical requirements which the design and functioning of alcoholometers and alcohol hydrometers must satisfy in order to be freely imported, marketed and used after having undergone the requisite inspections and having been provided with the required marks and signs;
Whereas, in its resolution of 17 December 1973 (4) on industrial policy, the Council invited the Commission to forward to it before 1 December 1974 a proposal for a Directive on alcoholometry and alcoholometers,
HAS ADOPTED THIS DIRECTIVE:
Article 1
This Directive defines the characteristics of alcoholometers and alcohol hydrometers used to determine the alcoholic strength of mixtures of water and ethanol.
Article 2
Those alcoholometers and alcohol hydrometers which may bear EEC marks and signs are described in the Annex.
Such instruments shall be subject to EEC pattern approval and shall be submitted for EEC initial verification.
Article 3
No Member State may refuse, prohibit or restrict the placing on the market or the use of any alcoholometer or alcohol hydrometer bearing the EEC pattern approval sign or EEC verification mark on the grounds of their metrological properties.
Article 4
1. Member States shall adopt and publish within a period of 24 months from the date of notification of (1)OJ No C 76, 7.4.1975, p. 39. (2)OJ No C 248, 29.10.1975, p. 22. (3)OJ No L 202, 6.9.1971, p. 1. (4)OJ No C 117, 31.12.1973, p. 1.
this Directive the measures necessary to conform with this Directive and shall forthwith inform the Commission thereof.
They shall apply these measures from 1 January 1980 at the latest.
2. Member States shall inform the Commission of the texts of the main provisions of national law which they adopt in the field covered by this Directive.
Article 5
This Directive is addressed to the Member States.
Done at Brussels, 27 July 1976.
For the Council
The President
M. van der STOEL
ANNEX ALCOHOLOMETERS AND ALCOHOL HYDROMETERS
1. DEFINITION OF THE INSTRUMENTS 1.1. Alcoholometers are glass instruments which indicate: - the alcoholic strength by mass, or
- the alcoholic strength by volume,
of a mixture of water and ethanol.
They are described as either mass alcoholometers or volume alcoholometers, depending upon what is measured.
Alcohol hydrometers are glass instruments designed to measure the density of a mixture of water and ethanol.
1.2. The instruments defined in this Directive are graduated at a reference temperature of 20 ºC, in accordance with the values appearing in the international alcohol tables published by the International Organization of Legal Metrology.
1.3. They are graduated for readings made at the free horizontal surface of the liquid.
2. DESCRIPTION OF THE INSTRUMENTS 2.1. Alcoholometers and alcohol hydrometers are glass instruments, consisting of: - a cylindrical body, the bottom of which is cone-shaped or hemispherical so that it does not entrap air bubbles,
- a hollow cylindrical stem fused to the upper part of the body ; its upper end is closed.
2.2. The entire external surface of each instrument must be symmetrical about its main axis.
The cross-section must not exhibit any abrupt alteration.
2.3. The lower part of the body must contain the loading material, the purpose of which is to adjust the mass of the instrument.
2.4. The stem must carry a scale marked on a cylindrical support rigidly fixed to the inside of the stem.
3. PRINCIPLES OF CONSTRUCTION 3.1. The glass used for making the instruments shall be transparent and free from any defect liable to interfere with the reading of scale measurements.
The glass shall have a coefficient of cubic expansion of (25 ± 2) 10-6 ºC-1.
3.2. The loading material shall be fixed in the bottom of the instrument. After the finished instrument has been kept in a horizontal position for one hour at 80 ºC and subsequently cooled in that position, it shall float with its axis vertical to within 1 degree 30 minutes.
4. SCALE 4.1. No instrument shall have more than one scale of the type referred to in 4.5 or 4.6.
4.2. The scale and the inscriptions shall be marked on a support having a smooth matt surface.
This support shall be held rigidly in place in the stem and reference marks shall be provided so that any displacement of the scale and its support relative to the stem is apparent.
The support, the scale and the inscriptions shall show no trace of distortion, discoloration or charring when maintained at 70 ºC for 24 hours.
4.3. The scale marks shall be: - situated in planes perpendicular to the axis of the instrument,
- black (1) and marked clearly and indelibly,
- fine, clear-cut and of a uniform thickness not greater than 0 72 mm.
4.4. The length of the short lines on the scale shall be at least one-fifth, that of the medium lines at least one-third and that of the long lines at least half of the circumference of the stem.
4.5. Alcoholometers shall have nominal scales graduated by % mass or by % volume of alcohol. They shall cover a range not greater than 10 % of alcohol by volume or by mass.
The scale interval shall be 0 71 %.
Each scale shall include from five to 10 additional scale intervals beyond its upper and lower nominal range limits.
4.6. The nominal scales of alcohol hydrometers shall be graduated in kilogrammes per cubic metre. They shall cover a range not greater than 20 kg/m3.
The scale interval shall be 0 72 kg/m3.
Each scale shall include from five to 10 additional scale intervals beyond its upper and lower nominal range limits. However, the scale must not extend beyond 1 000 kg/m3.
5. GRADUATION AND NUMBERING 5.1. On alcoholometers, every 10th scale mark, counting from one end of the nominal scale, shall be a long line. There shall be a medium line between each successive pair of long lines and four short lines between each long line and the nearest medium line.
Only the long lines shall be numbered.
5.2. On alcohol hydrometers, every fifth line, counting from one end of the nominal scale, shall be a long line. There shall be four short lines between two consecutive long lines.
Only the fifth or 10th lines shall be numbered.
5.3. The lines indicating the limits of the nominal scale shall show the figures in full. On alcohol hydrometers the other numbers may be abbreviated.
6. CLASSIFICATION AND PRINCIPAL DIMENSIONS OF INSTRUMENTS 6.1. The instruments shall be of one of the following classes of accuracy: - Class I : The minimum mean scale spacing shall be 1 75 mm. Instruments in this class shall not incorporate a thermometer.
- Class II : The minimum mean scale spacing shall be 1 705 mm. Instruments in this class may incorporate a thermometer.
- Class III : The minimum mean scale spacing shall be 0 785 mm. Instruments in this class may incorporate a thermometer.
6.2. The external diameter of the body of any instrument shall be between 19 and 40 mm.
The external diameter of the stem shall be at least 3 mm for Class I and Class II instruments and at least 2 75 mm for Class III instruments. The stem shall extend for at least 15 mm above the uppermost scale mark.
(1)Beyond the range of the nominal scale the scale lines may be of a different colour. The cross-section of the stem must be uniform for at least 5 mm below the lowest scale mark.
7. INSCRIPTIONS 7.1. The following inscriptions shall be legibly and indelibly marked inside the instrument: >PIC FILE= "T0009640">
7.2. The mass of the instrument, expressed to the nearest milligramme may, if desired, be marked on the body.
8. MAXIMUM PERMISSIBLE ERRORS AND VERIFICATION 8.1. The maximum permissible error for alcoholometers and alcohol hydrometers shall be: - for Class I, ± one half scale interval for each reading measured,
- for Classes II and III, ± one scale interval for each reading measured.
8.2. Verification shall be carried out at a minimum of three points in the nominal scale range.
9. THERMOMETERS USED IN DETERMINING THE ALCOHOLIC STRENGTH 9.1. If the instrument used to determine the alcoholic strength belongs to Class I, the thermometer used shall be: - of the metallic resistance or mercury-expansion type with glass casing,
- graduated in 0 71 or 0 705 ºC.
The maximum permissible error is ± 0 705 ºC for all scale readings. Mercury thermometers shall include a scale mark at 0 ºC.
9.2. If the instrument used to determine the alcoholic strength belongs to Class II or III, the thermometer shall be of the mercury-expansion type, with a glass casing and be gratuated to 0 71 or 0 72 or 0 75 ºC. It shall have a scale mark at 0 ºC.
The maximum permissible error, positive or negative, shall be:
0 71 ºC if the thermometer is graduated to 0 71 ºC,
0 715 ºC if the thermometer is graduated to 0 72 ºC,
0 72 ºC if the thermometer is graduated to 0 75 ºC.
The thermometer may be incorporated in the instrument used to determine the alcoholic strength.
In this case, it need not have any scale mark at 0 ºC.
9.3. The minimum scale spacing shall be: - 0 77 mm in the case of thermometers graduated to 0 705, 0 71, and 0 72 ºC and
- 1 70 mm in the case of thermometers graduated to 0 75 ºC.
9.4. The thickness of the lines shall not be more than one-fifth of the scale spacing.
10. MARKINGS
On the back of alcoholometers and alcohol hydrometers a space must be left on the upper third of the body for the EEC initial verification mark.
In accordance with 3.1.1 of Annex II to Directive 71/316/EEC and by way of derogation from the general rule laid down in section 3 of that Annex, the EEC initial verification mark must, due to the special marking requirements for glass instruments, consist of a series of signs having the following meaning: - a small letter "e",
- the last two digits of the year of EEC initial verification
- the identifying letter or letters of the State where the EEC initial verification was carried out,
- if necessary, the identifying number of the verification office.
When the marking is carried out by sandblasting, the letters and numbers shall be applied so as not to impair their legibility.
Example:
e 75 D 48 : EEC initial verification carried out in 1975 by Bureau 48 in the Federal Republic of Germany.
