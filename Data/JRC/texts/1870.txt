Removal from the register of Case C-389/04 [1]
(2005/C 330/23)
Language of the case: French
By order of 5 October 2005, the President of the Court of Justice of the European Communities has ordered the removal from the register of Case C-389/04: Commission of the European Communities
v French Republic.
[1] OJ C 273, 6.11.2004.
--------------------------------------------------
