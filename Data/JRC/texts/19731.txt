AGREEMENT IN THE FORM OF AN EXCHANGE OF LETTERS
on the amendments to the Annexes to the Agreement between the European Community and the United States of America on sanitary measures to protect public and animal health in trade in live animals and animal products
A. Letter from the European Community
Brussels, 27 October 2003
Your Excellency,
With reference to the Agreement between the European Community and the United States of America on sanitary measures to protect public and animal health in trade in live animals and animal products. I have the honour to propose to you to amend the Annexes of the Agreement as follows:
Replace the text of Annex V, point 43 of the Agreement, as recommended by the Joint Management Committee established under Article 14(1) of the Agreement with the respective text of Appendix A as attached hereto.
I would be obliged if you would confirm the Agreement of the United States of America to such amendment of Annex V to the Agreement.
Please accept, Sir, the assurance of my highest consideration.
For the European Community
Jaana Husu-Kallio
Enclosure:
Appendix A to replace Annex V, point 43 of the Agreement
B. Letter from the United States of America
Brussels, November 14, 2003
Madam,
I refer to your letter of 10/27/03 containing details of the proposed amendments to Annex V of the Agreement of July 20, 1999 between the United States of America and the European Community on the sanitary measures to protect public and animal health in trade in live animals and animal products, as set forth in Appendix A of your letter.
In this regard I have the honor to confirm the acceptability to the United States of America of the proposed amendments as recommended by the Joint Management Committee established under Article 14(1) of the Agreement, a copy of which is attached hereto. It is my understanding that these amendments shall take effect on the date on which the EC notifies the US that it has completed the necessary procedures for implementing these amendments.
Please accept, Madam, the assurances of my high consideration.
For the competent authority of the United States of America
Norval Francis
Enclosure:
Appendix A amending Annex V of the Agreement
APPENDIX A
"ANNEX V
>TABLE>"
