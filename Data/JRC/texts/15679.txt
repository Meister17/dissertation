Euro exchange rates [1]
13 September 2006
(2006/C 221/01)
| Currency | Exchange rate |
USD | US dollar | 1,2677 |
JPY | Japanese yen | 149,23 |
DKK | Danish krone | 7,4597 |
GBP | Pound sterling | 0,67655 |
SEK | Swedish krona | 9,2478 |
CHF | Swiss franc | 1,5892 |
ISK | Iceland króna | 89,71 |
NOK | Norwegian krone | 8,3825 |
BGN | Bulgarian lev | 1,9558 |
CYP | Cyprus pound | 0,5766 |
CZK | Czech koruna | 28,541 |
EEK | Estonian kroon | 15,6466 |
HUF | Hungarian forint | 273,76 |
LTL | Lithuanian litas | 3,4528 |
LVL | Latvian lats | 0,6959 |
MTL | Maltese lira | 0,4293 |
PLN | Polish zloty | 3,9637 |
RON | Romanian leu | 3,5024 |
SIT | Slovenian tolar | 239,59 |
SKK | Slovak koruna | 37,368 |
TRY | Turkish lira | 1,8630 |
AUD | Australian dollar | 1,6885 |
CAD | Canadian dollar | 1,4228 |
HKD | Hong Kong dollar | 9,8636 |
NZD | New Zealand dollar | 1,9653 |
SGD | Singapore dollar | 1,9989 |
KRW | South Korean won | 1215,85 |
ZAR | South African rand | 9,3236 |
CNY | Chinese yuan renminbi | 10,0764 |
HRK | Croatian kuna | 7,3820 |
IDR | Indonesian rupiah | 11551,92 |
MYR | Malaysian ringgit | 4,656 |
PHP | Philippine peso | 63,816 |
RUB | Russian rouble | 33,9850 |
THB | Thai baht | 47,389 |
[1] Source: reference exchange rate published by the ECB.
--------------------------------------------------
