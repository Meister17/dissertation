P6_TA(2005)0197
Anchorages for safety belts in motor vehicles ***II
European Parliament legislative resolution on the Council common position for adopting a directive of the European Parliament and of the Council amending Council Directive 76/115/EEC on the approximation of the laws of the Member States relating to anchorages for motor-vehicle safety belts (11933/3/2004 — C6-0030/2005 — 2003/0136(COD))
(Codecision procedure: second reading)
The European Parliament,
- having regard to the Council common position (11933/3/2004 — C6-0030/2005) [1],
- having regard to its position at first reading [2] on the Commission proposal to Parliament and the Council (COM(2003)0362),
- having regard to Article 251(2) of the EC Treaty,
- having regard to Rule 67 of its Rules of Procedure,
- having regard to the recommendation for second reading of the Committee on Transport and Tourism (A6-0117/2005),
1. Approves the common position;
2. Notes that the act is adopted in accordance with the common position;
3. Instructs its President to sign the act with the President of the Council pursuant to Article 254(1) of the EC Treaty;
4. Instructs its Secretary-General to sign the act, once it has been verified that all the procedures have been duly completed, and, in agreement with the Secretary-General of the Council, to have it published in the Official Journal of the European Union;
5. Instructs its President to forward its position to the Council and Commission.
[1] OJ C 111 E, 11.5.2005, p. 23.
[2] OJ C 91 E, 15.4.2004, p. 496.
--------------------------------------------------
