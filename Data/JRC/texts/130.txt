COMMISSION DECISION of 18 July 1989 laying down the certificate of hybrid breeding pigs, their semen, ova and embryos (89/506/EEC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Directive 88/661/EEC of 19 December 1988 on the zootechnical standards applicable to breeding animals of the porcine species (1), and in particular Articles 9 and 10 (1), fifth indent, thereof,
Whereas it is necessary to fix the data which must be mentioned on the certificate; whereas it is necessary for practical reasons to provide a model certificate and the conditions in which the data can be entered into the document accompanying hybrid breeding pigs, their semen, ova and embryos;
Whereas the measures provided for in this Decision are in accordance with the opinion of the Standing Committee on Zootechnics,
HAS ADOPTED THIS DECISION:
Article 1
The following particulars must be mentioned in the certificate of hybrid breeding pigs:
- issuing body,
- entry number in register,
- date of issue,
- system of identification,
- identification,
- date of birth,
- genetic type, line,
- sex,
- name and address of breeder,
- name and address of owner.
Article 2
1. The particulars provided for in Article 1 may be indicated:
(a) in the form of a certificate conforming to the specimen in Annex I;
(b) in documentation accompanying the hybrid breeding pig. In this event the competent authorities must certify that the particulars set out in Article 1 are indicated in those documents, by the following formula:
'The undersigned certifies that these documents contain the particulars mentioned in Article 1 of Commission Decision 89/506/EEC.'
2. The data on hybrid breeding pigs of the same lineage may be given in a single certificate or in the documentation accompanying a consignment of animals of the same origin and for the same destination. The model certificate shown in Annex I shall be adapted accordingly.
Article 3
The following particulars must be mentioned in the certificate of the semen of hybrid breeding pigs:
- all data as listed in Article 1 concerning the male that provides the semen,
- information allowing identification of the semen, the date of its collection and the names and addresses of the semen collection centre and of the consignee.
Article 4
The particulars provided for in Article 3 may be indicated:
1. in the form of a certificate conforming to the specimen in Annex II;
2. in documentation accompanying the semen of the hybrid breeding pig. In this event the competent authorities must certify that the particulars set out in Article 3 are indicated in those documents, by the following formula:
'The undersigned certifies that these documents contain the particulars mentioned in Article 3 of Commission Decision 89/506/EEC.'
Article 5
The following particulars must be mentioned in the certificate of the ova of hybrid breeding pigs:
- all data as listed in Article 1 concerning the sow that provides the ovum,
- information allowing identification of the semen, the date of its collection and the names and addresses of the semen collection centre and of the consignee.
If there is more than one ovum in a single straw this must be clearly stated and furthermore the ova must all have the same parentage.
Article 6
The particulars provided for in Article 5 may be indicated:
1. in the form of a certificate conforming to the specimen in Annex III;
2. in documentation accompanying the ova of the hybrid breeding pig. In this event the competent authorities must certify that the particulars set out in Article 5 are indicated in those documents, by the following formula:
'The undersigned certifies that these documents contain the particulars mentioned in Article 5 of Commission Decision 89/506/EEC.'
Article 7
The following particulars must be mentioned in the certificate of the embryos of hybrid breeding pigs:
- all data as listed in Article 1 concerning the donor sow and fertilizing boar,
- information allowing identification of the embryos, date of insemination or fecondation, date of collection and the names and addresses of the embryo collection centre and of the consignee.
If there is more than one embryo in a single straw this must be clearly stated and furthermore the embryos must all have the same parentage.
Article 8
The particulars provided for in Article 7 may be indicated:
1. in the form of a certificate conforming to the specimen in Annex IV;
2. in documentation accompanying the embryos of the hybrid breeding pig. In this event the competent authorities must certify that the particulars set out in Article 7 are indicated in those documents, by the following formula:
'The undersigned certifies that these documents contain the particulars mentioned in Article 7 of Commission Decision 89/506/EEC.'
Article 9
This Decision is addressed to the Member States.
Done at Brussels, 18 July 1989.
For the Commission
Ray MAC SHARRY
Member of the Commission
(1) OJ No L 382, 31. 12. 1988, p. 36.
ANNEX I
SPECIMEN CERTIFICATE for hybrid breeding pigs Issuing body: .
Entry No in register: .
System of identification (tag, tattoo, brand, earmark, sketch card): .
.
Identification: .
Name of animal (optional): .
Date of birth: .
Sex: .
Genetic type, line: .
Name and address of the consignee (¹): .
Name and address of breeder: .
Name and address of owner: .
Done at ................................................, on ................................................
.
(signature)
.
(name in capital letters and title of signatory)
(¹) Where the certificate accompanies a consignment of animals in accordance with Article 2 (2) of Decision 89/506/EEC.
ANNEX II
SPECIMEN CERTIFICATE for semen of hybrid breeding pigs A.
Particulars on donor boar:
Issuing body: .
Entry No in register: .
System of identification (tag, tattoo, brand, earmark, sketch card):.
.
Identification: .
Name of animal (optional): .
Date of birth: .
Genetic type, line: .
Name and address of owner: .
Name and address of breeder: .
Done at ................................................, on ................................................
.
(signature)
.
(name in capital letters and title of signatory)
B.
Particulars on semen
Semen identification system (colour, number): .
Identification: .
II.
Number of doses
Date(s) of collection
Identification of donor boar
Genetic type, line
II.
Origin of semen:
Name and address of semen collection centre(s):.
.
.
Destination of semen:
Name and address of consignee: .
.
.
Done at ................................................, on ................................................
.
(signature)
.
(name in capital letters and title of signatory)
ANNEX III
SPECIMEN CERTIFICATE for ova of hybrid breeding pigs A.
Particulars on donor sow:
Issuing body: .
Entry No in register: .
System of identification (tag, tattoo, brand, earmark, sketch card): .
.
Identification: .
Name of animal (optional): .
Date of birth: .
Genetic type, line: .
Name and address of owner: .
Name and address of breeder: .
Done at ................................................, on ................................................
.
(signature)
.
(name in capital letters and title of signatory)
B.
Particulars on ova:
Ova identification system (colour, number): .
.
Identification: .
Number of ova per straw: .
II.
Number of ova
Date(s) of collection
Identification of donor sow
Genetic type, line
II.
Origin of ovum/ova:
Address of ova collection centre(s): .
.
.
Destination of ova:
Name and address of consignee: .
.
.
Done at ................................................, on ................................................
.
(signature)
.
(name in capital letters and title of signatory)
ANNEX IV
SPECIMEN CERTIFICATE for embryos of hybrid breeding pigs A.
Particulars on donor boar:
Issuing body: .
Entry No in register: .
System of identification (tag, tattoo, brand, earmark, sketch card):.
.
Identification: .
Name of animal (optional):.
Date of birth: .
Genetic type, line: .
Name and address of owner: .
Name and address of breeder: .
B.
Particulars on donor sow:
Issuing body: .
Entry No in register: .
System of identification (tag, tattoo, brand, earmark, sketch card):.
.
Identification: .
Name of animal (optional):.
Date of birth: .
Genetic type, line: .
Name and address of owner: .
Name and address of breeder: .
Done at ................................................, on ................................................
.
(signature)
.
(name in capital letters and title of signatory)
C.
Particulars on embryos:
Embryo identification system (number, colour):.
Identification: .
Number of ova per straw: .
II.
Number
of embryos
Date(s) of insemination
or fecundation
Date of
collection
Identification of
donor sow and donor boar
Genetic type,
line
II.
Origin of embryos:
Address of embryo collection centre(s):.
.
.
Destinationof embryos:
Name and address of consignee: .
.
.
Done at ................................................, on ................................................
.
(signature)
.
(name in capital letters and title of signatory)
