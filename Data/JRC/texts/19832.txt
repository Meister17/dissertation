Council Regulation (EC) No 320/2003
of 18 February 2003
terminating the review of the anti-dumping measures applicable to imports of threaded malleable cast-iron tube or pipe fittings originating in Brazil, the Czech Republic, Japan, the People's Republic of China, the Republic of Korea and Thailand
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 384/96 of 22 December 1995 on protection against dumped imports from countries not members of the European Community(1), and in particular Articles 8, 9 and Article 11(3) thereof,
Having regard to Council Regulation (EC) No 1515/2001 of 23 July 2001 on the measures that may be taken by the Community following a report adopted by the WTO Dispute Settlement Body concerning anti-dumping and anti-subsidy matters(2) and in particular Article 2 thereof,
Having regard to the proposal from the Commission, after consulting the Advisory Committee,
Whereas:
A. PROCEDURE
(1) On 26 March 2001, the Commission received a request from an exporting producer of threaded malleable fittings in the Czech Republic, namely Moravske Zelezarny AS, to amend the definitive anti-dumping duty imposed on it by Council Regulation (EC) No 1784/2000 of 11 August 2000 imposing a definitive anti-dumping duty and collecting definitively the provisional duty imposed on imports of certain malleable cast-iron tube or pipe fittings originating in Brazil, the Czech Republic, Japan, the People's Republic of China, the Republic of Korea and Thailand(3).
(2) The exporting producer requested a review on the basis that its individual anti-dumping duty rate is based on methodologies which are not in line with the conclusions contained in the Appellate Body report and a panel report as modified by the Appellate Body report in the case "European Communities - Anti-dumping measures on imports of cotton-type bed-linen from India"(4) (Reports) and in particular the legal interpretation afforded in those reports to Articles 2(2)(2)(ii) and 2(4)(2) of the WTO Anti-dumping Agreement, as adopted by the Dispute Settlement Body of the World Trade Organisation (WTO).
(3) Consequently, the Commission offered, by notice of 5 December 2001 (hereinafter referred to as notice of initiation) published in the Official Journal of the European Communities(5), the possibility of a review of the anti-dumping measures applicable to imports of threaded malleable cast-iron tube or pipe fittings originating in Brazil, the Czech Republic, Japan, the People's Republic of China, the Republic of Korea and Thailand.
(4) The scope of the review was limited to the examination of dumping by those exporting producers in the countries concerned whose duty rates are based on a dumping methodology at issue in the reports and which submitted a full questionnaire reply within the time limits set out in the notice of initiation. This review was based on Article 2 of Regulation (EC) No 1515/2001.
(5) The Commission officially advised all known exporting producers and the relevant authorities in the exporting countries of the initiation of the proceeding. The parties concerned had the opportunity to make their views known in writing and to request a hearing within the time limit set out in the notice of initiation.
(6) The Commission sent questionnaires to all parties known to be concerned and to all other companies which made themselves known within the deadlines set out in the notice of initiation and received only one reply from an exporting producer in Thailand. The Czech producer which had originally requested a review did not respond to the questionnaire.
(7) Subsequently, this exporting producer in Thailand decided to withdraw its application for review. Therefore, and since no other exporter submitted a questionnaire response pursuant to the notice of initiation, the present investigation should be terminated.
B. CONCLUSIONS
(8) On the basis of the above, it is concluded that the review should be terminated and the anti-dumping measures imposed by Regulation (EC) No 1784/2000 on imports of the product concerned originating in Brazil, the Czech Republic, Japan, the People's Republic of China, the Republic of Korea and Thailand should remain in force, without changing the level of the measures for the exporting producers in the countries concerned. Likewise, the undertakings originally accepted should remain in place,
HAS ADOPTED THIS REGULATION:
Article 1
The review of anti-dumping measures concerning imports of threaded malleable cast-iron tube or pipe fittings currently classifiable within CN code ex 7307 19 10 (TARIC code 7307 19 10 10 ) and originating in Brazil, the Czech Republic, Japan, the People's Republic of China, the Republic of Korea and Thailand, is hereby terminated.
Article 2
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 18 February 2003.
For the Council
The President
N. Christodoulakis
(1) OJ L 56, 6.3.1996, p. 1. Regulation as last amended by Regulation (EC) No 1972/2002 (OJ L 305, 7.11.2002, p. 1).
(2) OJ L 201, 26.7.2001, p. 10.
(3) OJ L 208, 18.8.2000, p. 8.
(4) WT/DS 141/AB/R, 1.3.2001.
(5) OJ C 342, 5.12.2001, p. 5.
