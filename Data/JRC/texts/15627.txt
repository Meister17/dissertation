Information communicated by Member States regarding State aid granted under Commission Regulation (EC) No 68/2001 of 12 January 2001 on the application of Articles 87 and 88 of the EC Treaty to training aid
(2006/C 83/03)
(Text with EEA relevance)
Aid No | XT 56/01 |
Member State | Belgium |
Region | Flanders |
Title of aid scheme or name of company receiving individual aid | Decree of the Flemish Government establishing the detailed conditions and rules under which subsidies are granted for continuing education and training for employees and firms, Leverage Credit — Training section |
Legal basis | Decreet van 8 december 2000 houdende diverse bepalingen, inzonderheid artikel 16 |
Annual expenditure planned or overall amount of aid granted to the company | The amounts differ according to the budget year. For the period 2002-2006, an annual (indicative) amount of EUR 6 to 9 million is provided from the Flemish budget, or on average EUR 7,5 million a year. As regards aid from the ESF, an average annual (indicative) amount of EUR 4 million is provided for the period 2002-2006 |
Maximum aid intensity | The maximum aid intensity (the total official aid from the Member State and the European Union) for training projects is determined on the basis of Commission Regulation (EC) No 68/2001 and the provisions of Article 29(3)(b) of Council Regulation (EC) No 1260/1999. The ceilings are as follows: —Commission Regulation (EC) No 68/2001: maximum aid intensity (i.e. EU and Flemish aid combined) for training with respect to the subsidised basis: |
| SMEs | Large enterprises |
General training | 70 % | 50 % |
These intensities may be increased by 5 percentage points for assisted areas covered by Article 87(3)(c) of the Treaty. —Council Regulation (EC) No 1260/1999, Article 29(3)(b):The permitted ESF aid amounts to a maximum of 50 % of the total eligible cost and, as a general rule, at least 25 % of eligible public expenditure |
Date of implementation | The measure was approved by the Flemish Government on 27 September 2002 |
Duration of scheme or individual aid award | Until 31 December 2006 |
Objective of aid | Firms may request a subsidy only for general training of employed persons; specific training is not eligible. The definitions of "general" and "specific" training in Commission Regulation (EC) No 68/2001 of 12 January 2001 on the application of Articles 87 and 88 of the EC Treaty to training aid are applicable. Training measures should be aimed at adapting employees and self employed persons to rapid development and change in business and/or society. They must concern at least 10 participants and provide for training of at least 8 hours per participant. Projects are ranked according to an assesment in which cumulative bonus points are granted on the basis of various selection criteria, including targeting of vulnerable groups, sustainable business, ICT, "bottleneck vacancies". A maximum of 21 bonus points may be earned. Depending on the available budgets, the highest ranked are selected |
Economic sectors concerned | All sectors |
Name and address of the granting authority | Name: ESF-Agentschap Ministerie van de Vlaamse Gemeenschap |
Address: Markiesstraat 1 B-1000 Brussels |
Other information | The Leverage Credit — Training decree, approved by the Flemish Government on 28 September 2001 is abolished and replaced by the new decree approved by the Flemish Government on 27 September 2002. The amendments which were made in the new proposal are rather limited in scope. Basically, they concern a better concept of vulnerable groups, the restriction of the scheme to general training, an amendment of the consultation procedure for subregional employment committees and a review of the system of bonus points |
Aid number | XT 59/02 |
Member State | Italy |
Region | Veneto |
Title of aid scheme | Aid scheme operated by the individual Chambers of Commerce of the Veneto for the training of employees of SMEs in that region |
Legal basis | Delibere e/o provvedimenti delle CCIAA e/o delle loro aziende speciali, Unione regionale e del loro Centro estero, che contemplano precisa indicazione del Regolamento comunitario di esenzione in parola. |
Annual expenditure planned under the scheme | EUR 1841504 If this total expenditure were to increase by more than 20 % before the end of 2006, the Chambers undertake formally to notify the modification of the scheme to the Commission |
Maximum aid intensity | In accordance with the ceilings laid down in Article 4 of Regulation (EC) No 68/2001, the aid will not exceed: a)35 % for specific training given to employees of SMEsb)70 % for general training given to employees of SMEsc)35 % where it is not possible to distinguish between specific and general training.In any case, aid granted to any single initiative will never exceed EUR 200000, and there will be no individual aid grant approaching the figure of EUR 1 million, in accordance with Article 5 of Regulation (EC) No 68/2001 |
Date of implementation | From the date of transmission of this notice. |
Duration of scheme | Until 31 December 2006. |
Objective of aid | The scheme concerns both general and specific training under the terms, criteria and conditions laid down in Article 2 of Regulation (EC) No 68/2001. The general training will cover all sectors and the following subjects: 1.business administration and finance,2.business organisation,3.information technology including use of the Internet and e-business,4.quality, environment and safety,5.marketing,6.logistics,7.human resources management and communications,8.customer management.There will be some refresher courses for people in certain skilled trades (e.g. beauticians, goldsmiths, photographers) |
Economic sector(s) concerned | All SMEs in the Veneto, without distinction, are eligible for this scheme, including those listed in Annex I to the EC Treaty. |
Name and address of granting authority | Name: Chambers of Commerce, Industry, Agriculture and Crafts (CCIAA) in the Veneto [1]. Coordinating office: for this subject the Unione Regionale delle Camere di Commercio del Veneto |
Address: via Sansovino 9 I-30173 Mestre (VE) tel. (39-41) 258 16 66 fax (39-41) 258 16 00 e-mail europa@eicveneto.it. |
Aid number | XT 1/03 |
Member State | Belgium |
Region | Flanders |
Title of aid scheme | Promotion of training for workers: the scheme applies to specific groups of employees who are following a training course at the request of their employer;the employers are partly exempt from payment of certain training costs;this applies only to general training carried out by the VDAB on its own account |
Beneficiary | What categories of employees are eligible for this partial exemption? 1.Workers at risk, if they undergo training within six months of their recruitment. Workers at risk are:workers who were unemployed for at least 12 months before taking up their employment;workers who were in receipt of income support for at least six months before taking up their employment;workers who when they took up their employment were registered with the Rijksfonds voor sociale reclassering van mindervaliden (National Fund for the Social Rehabilitation of the Disabled);workers under 18 who are in compulsory part-time education;workers over 18 who do not have a higher secondary school leaving certificate;workers who for three years prior to taking up their employment had no income, did not engage in a professional activity, had interrupted their professional activity for three years or had never engaged in a professional activity.2.Workers threatened with unemployment:in the case of collective redundancies,in the case of individual dismissal,if they work for a firm in difficulty, [2] if they work for a firm undergoing restructuring [3]3.Workers belonging to companies with 25 workers or fewer.workers from companies with fewer than 10 workersworkers from companies with between 10 and 25 workers4.Workers who are no longer able to carry out their present duties for medical reasons |
Legal basis | Wijziging van het besluit van de Vlaamse regering van 21-12-1988 houdende de organisatie van de arbeidsbemiddeling en de beroepsopleiding. Deze besluitswijziging wordt voorgelegd aan de Vlaamse regering op 6-12-2002. |
Annual expenditure planned under the scheme | Between EUR 800000 and EUR 1,5 million a year |
Maximum aid intensity | For the training of their employees, the VDAB's employer customers are charged only the costs of the trainer, the material, and depreciation of the equipment. It is on the basis of these costs that the allowance, as mentioned in the amendment to the decision, is calculated. The other costs, that do qualify as training costs (cf. Regulation (EC) No 68/2001), are in any case met by the employer. The intensity of the aid varies according to the target group, but on the basis of various simulations it has never proved to be more than 50 %. The simulations always assumed the highest possible contribution from the VDAB and the minimum wage for an employee in the sector according to the collective labour agreement. most expensive VDAB training: aid intensity: 49,03 % (representing only 0,13 % of turnover)the most common training: aid intensity: 44,44 % (representing 18,45 % of turnover)In this decision the "highest possible VDAB contribution" usually applies in specific cases where the employee can often be regarded "de facto" as unemployed (collective redundancies, individual dismissal, unable to continue in his present duties for medical reasons); in the other cases the VDAB contribution is lower by a half. Moreover: under the terms of the Regulation, aid of up to 50 % is allowed for large undertakings, while the VDAB is primarily concerned with specific target groups and SMEs, for which the same Regulation allows a higher percentage. If the rate of support were still to be higher than that stipulated in the Regulation, the decision provides for the limiting of the aid percentage to the maximum allowed |
Date of implementation | 1 January 2003 |
Duration of scheme | 30 June 2007 The rules are laid down in a decision of the Flemish Government and are not subject to a time limit. In theory, the rules will continue to apply after the date given unless modified by the Flemish Government by decree. If requested we will adapt the measures to the Regulation which will take effect after 31 December 2006 |
Objective of aid | One of the tasks of the VDAB is to provide training for workers at the request of their employer in return for payment. In order to promote training for certain groups of workers their employer is offered a grant. This grant applies only to the general training provided by the VDAB. The VDAB offers a very wide range of training. At the present time we are offering 1622 training courses over the whole of Flanders. A complete list is available at www.vdab.be/opleidingen |
Economic sector(s) concerned | All sectors can make use of these aid measures. |
Name and address of the granting authority | Name: Vlaamse Dienst voor Arbeidsbemiddeling en Beroepsopleiding (VDAB) Keizerslaan 11 B-1000 Brussels |
Address: Contact person: Frank Roegiest Dienst betalende opleidingen Keizerslaan 11 B-1000 Brussels tel. (32-2) 506 15 78 fax (32-2) 506 15 15 e-mail: froegies@vdab.be |
Aid No | XT 89/04 |
Member State | Italy |
Region | Autonomous Province of Trento |
Title of aid scheme | Financing for training measures for workers in employment in implementation of Article 6(4) of Act No 53/2000 — year 2004 |
Legal basis | Deliberazione della Giunta Provinciale n. 2410 di data 22/10/2004 pubblicata sul Bollettino della Regione Trentino Alto Adige del 2/11/04 n 44 supp.1. |
Annual expenditure planned or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount year 2004 | EUR 522301,09 (EUR 0,52 million) |
Loans guaranteed | |
Individual aid | Overall aid amount | |
Loans guaranteed | |
Maximum aid intensity | In conformity with Article 4(2)-(6) of the Regulation | Yes | |
Date of implementation | From 2 November 2004 |
Duration of scheme or individual aid award | Until 31 December 2004 |
Objective of aid | General training | Yes |
Specific training | Yes |
Economic sectors concerned | All sectors eligible for training aid | Yes |
Name and address of the granting authority | Name: Provincia Autonoma di Trento — Dipartimento Politiche Sociali e del Lavoro — Ufficio Fondo Sociale Europeo |
Address: via Giusti, 40 I-38100 Trento |
Large individual aid grants | In conformity with Article 5 of the Regulation, the measure excludes awards of aid or requires prior notification to the Commission of awards of aid, if the amount of aid granted to one enterprise for a single training project exceeds EUR 1 million. | Yes | |
Aid No | XT 90/04 |
Member State | Italy |
Region | Autonomous Province of Trento |
Title of aid scheme | Financing of training projects for staff of firms (including business people and managers) Measure D1 of the European Social Fund — year 2004 |
Legal basis | Deliberazione della Giunta Provinciale n. 2409 di data 22/10/2004 (pubblicata sul Bollettino della Regione Trentino Alto Adige del 2/11/04 n.44 sup.1) |
Annual expenditure planned or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount 2004 | EUR 3,43 million |
Loans guaranteed | |
Individual aid | Overall aid amount | |
Loans guaranteed | |
Maximum aid intensity | In conformity with Article 4(2)-(6) of the Regulation | Yes | |
Date of implementation | From 2 November 2004 |
Duration of scheme or individual aid award | Until 31 December 2004 |
Objective of aid | General training | Yes |
Specific training | Yes |
Economic sectors concerned | All sectors eligible for training aid | Yes |
Name and address of the granting authority | Name: Provincia Autonoma di Trento — Dipartimento Politiche Sociali e del Lavoro — Ufficio Fondo Sociale Europeo |
Address: via Giusti, 40 I-38100 Trento |
Large individual aid grants | In conformity with Article 5 of the Regulation the measure excludes awards of aid or requires prior notification to the Commission of awards of aid, if the amount of aid granted to one enterprise for a single training project exceeds EUR 1 million. | Yes | |
[1] This concerns the Chambers of Commerce, Industry, Agriculture and Crafts (CCIAA) of Venice, Verona, Belluno, Vicenza and Treviso, directly or indirectly via their special agencies, the Regional Union and their External Centre. The Chambers of Padua and Rovigo do not grant aid under this Regulation at the moment.
[2] Firm in difficulty: an undertaking which in the annual accounts for the two financial years preceding the date of the application for recognition for tax purposes records a loss from ordinary activities, when for the last financial year this loss is greater than the amount of write-downs and depreciation for start-up costs on intangible and tangible assets.
[3] Firm undergoing restructuring: an undertaking which, in accordance with the procedure laid down in collective labour agreement No. 24 of 20 October 1975 concerning the procedure for informing and consulting workers' representatives in the matter of collective redundancies and in the Royal Decree of 24 May 1976 on collective redundancies, is carrying out collective redundancies; an undertaking which, in application of the provisions of Article 51 of the law of 3 July 1978 on contracts of employment, during the year preceding the application for recognition, has experienced a number of days of unemployment at least equal to 20 % of the total number of days declared for workers to the Rijksdienst voor Sociale Zekerheid (National Social Security Office); an undertaking in a situation which could lead to collective redundancies as specified in the first provision but which has not in fact implemented the redundancies, provided it can show that by the granting of a derogation the dismissal of the workers involved can be avoided.
--------------------------------------------------
