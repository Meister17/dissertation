P6_TA(2005)0199
Equality action programmes ***I
European Parliament legislative resolution on the proposal for a decision of the European Parliament and of the Council amending Council Decision 2001/51/EC establishing a Programme relating to the Community framework strategy on gender equality and Decision No 848/2004/EC of the European Parliament and of the Council establishing a Community action programme to promote organisations active at European level in the field of equality between men and women (COM(2004) 0551 — C6-0107/2004 — 2004/0194(COD))
(Codecision procedure: first reading)
The European Parliament,
- having regard to the Commission proposal to the European Parliament and the Council (COM(2004) 0551) [1],
- having regard to Article 251(2) and Article 13(2) of the EC Treaty, pursuant to which the Commission submitted the proposal to Parliament (C6-0107/2004),
- having regard to Rule 51 of its Rules of Procedure,
- having regard to the report of the Committee on Women's Rights and Gender Equality and the opinion of the Committee on Budgets (A6-0132/2005),
1. Approves the Commission proposal;
2. Calls on the Commission to refer the matter to Parliament again if it intends to amend its proposal substantially or replace it with another text;
3. Considers that the financial statement of the Commission proposal is compatible with the ceiling of heading 3 of the current Financial Perspectives, without prejudicing other policies;
4. Calls on the Commission explicitly to make adequate budgetary provision in the new Financial Perspectives for the period 2007-2013 for the Programme relating to the Community framework strategy on gender equality (2001-2005) [2] and for the Community action programme to promote organisations active at European level in the field of equality between men and women [3];
5. Instructs its President to forward its position to the Council and Commission.
[1] Not yet published in OJ.
[2] OJ L 17, 19.1.2001, p. 22.
[3] OJ L 157, 30.4.2004, p. 18.
--------------------------------------------------
