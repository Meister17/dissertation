*****
COUNCIL DIRECTIVE
of 24 July 1990
amending Directive 84/647/EEC on the use of vehicles hired without drivers for the carriage of goods by road
(90/398/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 75 thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas Article 8 of Directive 84/647/EEC (4) requires the Council to re-examine Articles 3 (2) and 4 (2) of the abovementioned Directive which contain restrictive clauses;
Whereas the restrictive clauses have resulted in unequal application of the Directive in the Community; whereas abolition, at least in part, of these clauses would allow better financial management and cut the costs of hauliers operating on their own account or for hire or reward;
Whereas the abovementioned Directive should therefore be amended,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Directive 84/647/EEC is hereby amended as follows:
1. Article 3 (2) is replaced by the following:
'2. Member States may exclude from the provisions of paragraph 1 own-account transport operations carried out by vehicles with a total permissible laden weight of more than 6 tonnes.'
2. Article 4 (2) is deleted.
Article 2
Member States shall take the measures necessary to comply with this Directive not later than 31 December 1990 and shall forthwith inform the Commission thereof.
Article 3
This Directive is addressed to the Member States.
Done at Brussels, 24 July 1990.
For the Council
The President
C. MANNINO
(1) OJ No C 296, 24. 11. 1989, p. 7 and OJ No C 150, 19. 6. 1990, p. 4.
(2) OJ No C 96, 17. 4. 1990, p. 352.
(3) OJ No C 124, 21. 5. 1990, p. 20.
(4) OJ No L 335, 22. 12. 1984, p. 72.
