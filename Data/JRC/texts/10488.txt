Commission Decision
of 24 October 2006
authorising the placing on the market of "rapeseed oil high in unsaponifiable matter" as a novel food ingredient under Regulation (EC) No 258/97 of the European Parliament and of the Council
(notified under document number C(2006) 4975)
(Only the French text is authentic)
(2006/722/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Regulation (EC) No 258/97 of the European Parliament and of the Council of 27 January 1997 concerning novel foods and novel food ingredients [1], and in particular Article 7 thereof,
Whereas:
(1) On 24 October 2001 the company Laboratoires Pharmascience (now Laboratoires Expanscience) made a request to the competent authorities of France to place "rapeseed oil high in unsaponifiable matter" on the market as a novel food ingredient.
(2) On 8 January 2002 the competent authorities of France forwarded their initial assessment report to the Commission. The report came to the conclusion that "rapeseed oil high in unsaponifiable matter" proposed as a food ingredient at a daily intake of 1,5 g per day would make it possible to supplement vitamin E intake and also concluded that the levels of phytosterol were not sufficient to reduce cholesterolaemia.
(3) The Commission forwarded the initial assessment report to all Member States on 18 February 2002.
(4) Within the 60-day period laid down in Article 6(4) of Regulation (EC) No 258/97, reasoned objections to the marketing of the product were raised in accordance with that provision.
(5) The European Food Safety Authority (EFSA) was therefore consulted on 30 January 2004.
(6) On 6 December 2005, EFSA adopted the "Opinion of the Scientific Panel on Dietetic Products, Nutrition and Allergies on a request from the Commission related to "rapeseed oil high in unsaponifiable matter" as a novel food ingredient".
(7) The opinion came to the conclusion that the proposed use level of 1,5 g per day of "rapeseed oil high in unsaponifiable matter" was safe.
(8) It is recognised that "rapeseed oil high in unsaponifiable matter" at the intended use level of 1,5 g is a safe source of Vitamin E. For labelling and presentation, Directive 2002/46/EC of the European Parliament and of the Council of 10 June 2002 on the approximation of laws of the Member States relating to food supplements [2] applies.
(9) On the basis of the scientific assessment, it is established that "rapeseed oil high in unsaponifiable matter" complies with the criteria laid down in Article 3(1) of Regulation (EC) No 258/97.
(10) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
"Rapeseed oil high in unsaponifiable matter", as specified in the Annex may be placed on the market in the Community as a novel food ingredient for use in food supplements.
Article 2
The maximum amount of "rapeseed oil high in unsaponifiable matter" present in a portion recommended for daily consumption by the manufacturer shall be 1,5 g.
Article 3
The designation of the novel food ingredient shall be "rapeseed oil extract".
Article 4
This Decision is addressed to Laboratoires Expanscience, Siège Social, 10, Avenue de l’Arche, F-92419 Courbevoie Cedex.
Done at Brussels, 24 October 2006.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 43, 14.2.1997, p. 1. Regulation as last amended by Regulation (EC) No 1882/2003 (OJ L 284, 31.10.2003, p. 1).
[2] OJ L 183, 12.7.2002, p. 51. Directive as amended by Commission Directive 2006/37/EC (OJ L 94, 1.4.2006, p. 32).
--------------------------------------------------
ANNEX
Specifications of "rapeseed oil high in unsaponifiable matter"
DESCRIPTION
"Rapeseed oil high in unsaponifiable matter" is produced by vacuum distillation and it is different from refined rapeseed oil in the concentration of the unsaponifiable fraction (1 g in refined rapeseed oil and 9 g in "rapeseed oil high in unsaponifiable matter"). There is a minor reduction of triglycerides containing monounsaturated and polyunsaturated fatty acids.
SPECIFICATIONS
Unsaponifiable matter | > 7 g/100 g |
Tocopherols | > 0,8 g/100 g |
α-tocopherol (%) | 30-50 % |
γ-tocopherol (%) | 50-70 % |
δ-tocopherol (%) | < 6 % |
Sterols, triterpenic alcohols, methylsterols | > 5 g/100 g |
Fatty acids in triglycerides
palmitic acid | 3-8 % |
stearic acid | 0,8-2,5 % |
oleic acid | 50-70 % |
linoleic acid | 15-28 % |
linolenic acid | 6-14 % |
erucic acid | < 2 % |
Acid value | ≤ 6 mg KOH/g |
Peroxide value | ≤ 10 mEq O2/kg |
Iron (Fe) | < 1000 μg/kg |
Copper (Cu) | < 100 μg/kg |
Polycyclic aromatic hydrocarbons (PAH) Benzo(a)pyrene | < 2 μg/kg |
Treatment with active carbon is required to ensure that polycyclic aromatic hydrocarbons (PAH) are not enriched in the production of "rapeseed oil high in unsaponifiable matter".
--------------------------------------------------
