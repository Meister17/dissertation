Action brought on 26 July 2006 — Commission of the European Communities
v Italian Republic
Parties
Applicant: Commission of the European Communities (represented by: J. Enegren and L. Pignataro, Agents)
Defendant: Italian Republic
Form of order sought
- declare that, by failing to adopt the laws, regulations and administrative provisions necessary to comply with Directive 2002/14/EC [1] establishing a general framework for informing and consulting employees in the European Community or, in any event, by failing to communicate them to the Commission, the Italian Republic has failed to fulfil its obligations under Article 11 of that directive;
- order the Italian Republic to pay the costs.
Pleas in law and main arguments
The period for transposition of the directive expired on 23 March 2005.
[1] OJ L 80, 23.3.2002, p. 29.
--------------------------------------------------
