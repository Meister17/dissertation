Election of a President of a Chamber of three Judges
(2006/C 190/23)
On 5 July 2006, the Court of First Instance elected, in accordance with Article 15(3) of the Rules of Procedure, Judge Cooke as President of a Chamber of three Judges for the period from 1 October 2006 to 31 August 2007.
--------------------------------------------------
