Statement of revenue and expenditure of the Community Plant Variety Office (CPVO) Supplementary Rectifying Budgets (SRB) financial year 2004
(2004/879/EC)
STATEMENT OF REVENUE
TITLE 3
RESERVE
CHAPTER 3 0 — RESERVE FOR DEFICIT
BUD 2004 | SRB 2004 | New amounts |
2983000 | 466000 | 3449000 |
STATEMENT OF EXPENDITURE
TITLE 2
BUILDINGS, EQUIPMENT AND MISCELLANEOUS OPERATING EXPENDITURE
CHAPTER 2 1 — DATA PROCESSING
2 1 0 Equipment, software and external services
BUD 2004 | SRB 2004 | New amounts |
543000 | 416000 | 959000 |
CHAPTER 2 6 — INTERNAL AUDIT AND EVALUATIONS
2 6 0 Internal audit and evaluations
BUD 2004 | SRB 2004 | New amounts |
70000 | 50000 | 120000 |
--------------------------------------------------
Establishment plan
Category and grade | 2004 | 2003 |
Permanent posts | Temporary posts | Permanent posts | Temporary posts |
A*16 (A1) | — | — | — | — |
A*15 (A2) | — | 1 | — | 1 |
A*14 (A3) | — | 1 | — | 1 |
A*13 | — | — | — | — |
A*12 (A4) | 1 | — | 1 | 1 |
A*11 (A5) | 1 | — | 1 | — |
A*10 (A6) | — | 2 | — | 1 |
A*9 | — | — | — | — |
A*8 (A7) | — | — | — | — |
A*7 (A8) | — | — | — | — |
A*6 | — | — | — | — |
A*5 | — | — | — | — |
Total A* | 2 | 4 | 2 | 4 |
B*11 | — | — | — | — |
B*10 (B1) | — | — | — | — |
B*9 | — | — | — | — |
B*8 (B2) | 3 | 2 | — | 1 |
B*7 (B3) | 3 | 6 | 5 | 4 |
B*6 | — | — | — | — |
B*5 (B4) | — | 2 | 2 | 2 |
B*4 (B5) | — | 1 | — | — |
B*3 | — | — | — | — |
Total B* | 6 | 11 | 7 | 7 |
C*7 | — | — | — | — |
C*6 (C1) | — | 1 | — | 1 |
C*5 (C2) | 2 | 1 | — | 1 |
C*4 (C3) | — | 2 | 2 | — |
C*3 (C4) | 1 | 4 | 1 | 6 |
C*2 (C5) | — | 1 | — | 1 |
C*1 | — | — | — | — |
Total C* | 3 | 9 | 3 | 9 |
D*5 | — | — | — | — |
D*4 (D1) | — | — | — | — |
D*3 (D2) | — | 1 | — | 1 |
D*2 (D3) | — | — | — | — |
D*1 (D4) | — | 2 | — | 2 |
Total D* | — | 3 | — | 3 |
Grand Total | 11 | 27 | 12 | 23 |
--------------------------------------------------
