Action brought on 21 April 2006 — Pimlott v Europol
Parties
Applicant: Mike Pimlott (Porchester, Hampshire, United Kingdom) (represented by: D.C. Coppens, lawyer)
Defendant: European Police Office (Europol)
Form of order sought
- Annul Europol's decision of 25 January 2006;
- Order Europol to renew the applicant's contract for a period of four years, from 1 January 2006 to 31 December 2010;
- Order Europol to pay the costs.
Pleas in law and main arguments
The applicant, after having been first engaged by Europol on 1 January 2000 for a period initially fixed at four years, was employed from 1 January 2002 in a different position within the same bureau, under a new contract, which was to end on 31 December 2005.
In his action, the applicant challenges Europol's decision to renew that contract for two years only. He claims that, from the time he took up his new post under the new contract, Europol should have treated him not as a staff member to whom the authorities of his State of origin had granted unpaid special leave, but as a staff member who was no longer linked to those authorities. The distinction is relevant because, in the first case, the second indent of Article 6 of the Staff Regulations applicable to Europol employees, permitting renewal for a period of two years, is to be applied, while in the second case the third indent of that provision, permitting renewal for a period of four years, is to be applied.
The applicant submits that in 2002 he severed all ties with his original employer in the United Kingdom and that he was persuaded that the latter would amend the staff roll as a result. Even were it to be established that, in fact, the United Kingdom authorities did not remove him from that roll, the applicant claims that he himself should not in any event have to bear the consequences of that negligence.
--------------------------------------------------
