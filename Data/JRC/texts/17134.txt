Decision of the EEA Joint Committee
No 140/2004
of 29 October 2004
amending Annex I (Veterinary and phytosanitary matters) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex I to the Agreement was amended by Decision of the EEA Joint Committee No 96/2004 of 9 July 2004 [1].
(2) Commission Regulation (EC) No 879/2004 of 29 April 2004 concerning the provisional authorisation of a new use of an additive already authorised in feedingstuffs (Saccharomyces cerevisiae) [2], as corrected by OJ L 180, 15.5.2004, p. 30, is to be incorporated into the Agreement.
(3) Commission Regulation (EC) No 880/2004 of 29 April 2004 authorising without time limit the use of beta-carotene and canthaxanthin as additives in feedingstuffs belonging to the group of colouring matters including pigments [3] is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
The following points shall be inserted after point 1zm (Commission Regulation (EC) No 490/2004) of Chapter II of Annex I to the Agreement:
"1zn. 32004 R 0879: Commission Regulation (EC) No 879/2004 of 29 April 2004 concerning the provisional authorisation of a new use of an additive already authorised in feedingstuffs (Saccharomyces cerevisiae) (OJ L 162, 30.4.2004, p. 65), as corrected by OJ L 180, 15.5.2004, p. 30.
1zo. 32004 R 0880: Commission Regulation (EC) No 880/2004 of 29 April 2004 authorising without time limit the use of beta-carotene and canthaxanthin as additives in feedingstuffs belonging to the group of colouring matters including pigments (OJ L 162, 30.4.2004, p. 68)."
Article 2
The texts of Regulations (EC) Nos 879/2004, as corrected by OJ L 180, 15.5.2004, p. 30, and 880/2004 in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 30 October 2004, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [4].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 29 October 2004.
For the EEA Joint Committee
The President
Kjartan Jóhannsson
[1] OJ L 376, 23.12.2004, p. 17.
[2] OJ L 162, 30.4.2004, p. 65.
[3] OJ L 162, 30.4.2004, p. 68.
[4] No constitutional requirements indicated.
--------------------------------------------------
