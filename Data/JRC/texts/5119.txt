Commission Regulation (EC) No 558/2005
of 12 April 2005
amending Regulations (EEC) No 3846/87 establishing an agricultural product nomenclature for export refunds and (EC) No 174/1999 laying down special detailed rules for the application of Council Regulation (EEC) No 804/68 as regards export licences and export refunds in the case of milk and milk products
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1255/1999 of 17 May 1999 on the common organisation of the markets in milk and milk products [1], and in particular Article 31(14) thereof,
Whereas:
(1) Commission Regulation (EEC) No 3846/87 [2], on the basis of the Combined Nomenclature, establishes an agricultural product nomenclature for export refunds.
(2) The refund nomenclature provides for cheeses to be eligible for an export refund if they meet minimum requirements as regards milk dry matter and milk fat. A type of cheese produced in some new Member States may meet those necessary requirements but may not benefit from a refund since it is not covered by the present classification system of the export refund nomenclature. Given the importance of that cheese for the dairy industry, trade and milk producers involved, it is appropriate to add a product code under an "other cheeses" position enabling that cheese to be classified under the export refund nomenclature.
(3) The quantities for which export licences are requested in the product category "cheeses" are continually exceeding the profile of the export limits imposed on the Community in the framework of the Agreement on Agriculture following the Uruguay Round of Multilateral Trade Negotiations. The additional export licences which will be applied for under the newly created position will further increase the pressure on the category.
(4) Article 3 of Commission Regulation (EC) No 174/1999 [3] provides that no refund shall be granted on exports of cheese where the free-at-frontier price prior to application of the refund in the Member State of export is less than EUR 230 per 100 kilograms. Cheese falling within code 0406 90 33 9919 of the nomenclature for refunds is exempt from this provision. It is appropriate in these circumstances and given the high level of demand for export licences for cheeses to apply this provision to all cheeses without exception.
(5) Footnote 10 of Sector 9 of Annex I of Regulation (EEC) No 3846/87, applicable to grated, powdered and processed cheeses, states that added non-lactic matter will not be taken into account for the purpose of calculating the refund. It is appropriate to extend this provision to all cheeses and to better describe the non-lactic matter concerned. It may not be possible for the exporter and even more difficult for the competent authorities to determine the weight of these matters. It is therefore appropriate to reduce the refund by a standard amount.
(6) The refund is granted on the net weight of the cheeses. Some confusion may exist in cases where cheeses are contained in an envelope of paraffin, ash or wax or wrapped in a film of plastic. It is appropriate to provide that such packing are not a part of the net weight of the product for the purpose of calculating the refund. It may not be possible for the exporter and for the competent authorities, as regards the plastic film, the paraffin and the ash, to determine the weight of such materials. It is therefore suitable to reduce the refund by a standard amount.
(7) Regulations (EEC) No 3846/87 and (EC) No 174/1999 should be amended accordingly.
(8) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Milk and Milk Products,
HAS ADOPTED THIS REGULATION:
Article 1
Annex I to Regulation (EEC) No 3846/87 is amended in accordance with the Annex to this Regulation.
Article 2
In Article 3 of Regulation (EC) No 174/1999 the fourth paragraph is deleted.
Article 3
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Union.
It shall apply on export licences applied for from 27 May 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 12 April 2005.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 160, 26.6.1999, p. 48. Regulation as last amended by Commission Regulation (EC) No 186/2004 (OJ L 29, 3.2.2004, p. 6).
[2] OJ L 366, 24.12.1987, p. 1. Regulation as last amended by Regulation (EC) No 2199/2004 (OJ L 380, 24.12.2004, p. 1).
[3] OJ L 20, 27.1.1999, p. 8. Regulation as last amended by Regulation (EC) No 2250/2004 (OJ L 381, 28.12.2004, p. 25).
--------------------------------------------------
ANNEX
Sector 9 of Annex I to Regulation (EEC) No 3846/87 is amended as follows:
1. The description of the CN code "ex0406" is replaced by the following: "Cheese and curd (7) (10):"
2. The description of the CN code "ex040620" is replaced by the following: "— Grated or powdered cheese, of all kinds:"
3. The description of the CN code "ex040630" is replaced by the following: "— Processed cheese, not grated or powdered:"
4. The information relating to CN code "ex04069088" is replaced by the following:
CN code | Description of goods | Additional requirements for using the product code | Product code |
Maximum water content in product weight(%) | Minimum fat content in the dry matter(%) |
"ex04069088 | – – – – – – – – Exceeding 62 % but not exceeding 72 %: | | | |
– – – – – – – – – cheeses produced from whey | | | 0406 90 88 9100 |
– – – – – – – – – Other: | | | |
– – – – – – – – – – Of a fat content, by weight, in the dry matter: | | | |
– – – – – – – – – – – Of 10 % or more but less than 19 % | 60 | 10 | 0406 90 88 9300 |
– – – – – – – – – – – Of 40 % or more: | | | |
– – – – – – – – – – – – Akawi | 55 | 40 | 0406 90 88 9500" |
5. Footnote 7 is replaced by the following text:
(a) In the case of cheeses presented in immediate packing which also contain preserving liquid, in particular brine, the refund is granted on the net weight, less the weight of the liquid.
(b) The film of plastic, the paraffin, the ash and the wax used as a packing are not considered as a part of the net weight of the product for the purpose of the refund.
(c) Where the cheese is presented in a film of plastic, and where the net weight declared includes the weight of the film in plastic, the refund amount shall be reduced by 0,5 %.
When completing customs formalities, the applicant shall state that the cheese is packed in a film of plastic and whether the declared net weight includes the weight of the film in plastic.
(d) Where the cheese is presented in paraffin or ash, and where the net weight declared includes the weight of the paraffin or the ash, the refund amount shall be reduced by 2 %.
When completing customs formalities, the applicant shall state that the cheese is packed in paraffin or in ash, and whether the declared net weight includes the weight of the ash or the paraffin.
(e) Where the cheese is presented in wax, when completing customs formalities, the applicant must state on the declaration the net weight of the cheese not incorporating the weight of the wax."
[0]
6. Footnote 10 is replaced by the following:
(a) Where the product contains non-lactic ingredients, other than spices or herbs, such as in particular ham, nuts, shrimps, salmon, olives, raisins, the refund amount shall be reduced by 10 %.
When completing customs formalities, the applicant shall state on the declaration provided for that purpose that there is addition of such non-lactic ingredients.
(b) Where the product contains herbs or spices, such as in particular mustard, basil, garlic, oregano, the refund amount shall be reduced by 1 %.
When completing customs formalities, the applicant shall state on the declaration provided for that purpose that there is addition of herbs or spices.
(c) Where the product contains casein and/or caseinates and/or whey and/or products derived from whey and/or lactose and/or permeate and/or products covered by CN code 3504, the added casein and/or caseinates and/or whey and/or products derived from whey (excluding whey butter covered by CN code 04051050) and/or lactose and/or permeate and/or products covered by CN code 3504 will not be taken into account for the purpose of calculating the refund.
When completing customs formalities, the applicant shall state on the declaration provided for that purpose whether or not casein and/or caseinates and/or whey and/or products derived from whey and/or lactose and/or permeate and/or products covered by CN code 3504 have been added and, where this is the case, the maximum content by weight of casein and/or caseinates and/or whey and/or products derived from whey (specifying where applicable the whey butter content) and/or lactose and/or permeate and/or products covered by CN code 3504 added per 100 kilograms of finished product.
(d) The products referred to may contain quantities of added non-lactic matter required for their manufacture or preservation, such as salt, rennet or mould."
[0]
[0]
[0]
--------------------------------------------------
