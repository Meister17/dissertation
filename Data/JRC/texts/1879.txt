Opinion of the European Economic and Social Committee on the Proposal for a Decision of the European Parliament and of the Council creating the Youth in Action programme for the period 2007-2013
(COM (2004) 471 final — 2004/0152 (COD))
(2005/C 234/11)
Procedure
On 9 September 2004, the Council decided to consult the European Economic and Social Committee, under Article 262 of the Treaty establishing the European Community, on the abovementiond proposaL.
The Section for Employment, Social Affairs and Citizenship, which was responsible for preparing the Committee's work on the subject, adopted its opinion on 18 February 2005. The rapporteur was Mr Rodríguez García-Caro.
At its 415th plenary session, held on 9 and 10 March 2005 (meeting of 10 March), the European Economic and Social Committee adopted the following opinion by 196 votes to none, with three abstentions:
1. Introduction
1.1 Since 1988, through the successive phases of the Youth for Europe programme, the European Voluntary Service for Young People programme and the current YOUTH programme (which includes the actions of its predecessors), the European Union has initiated a series of measures to implement Article 149(2) TEC, which states that "Community action shall be aimed … at encouraging the development of youth exchanges and of exchanges of socio-educational instructors".
1.2 The various specific youth-oriented programmes adopted have been — and continue to be — much appreciated, and their actions have seen a high level of participation from target populations. These programmes have enabled Member States to focus their efforts on those initiatives that enable their young citizens to strengthen links and acquire experience and knowledge through exchanges between the different participating countries. These exchanges are not connected to labour or educational issues.
1.3 The importance assigned to EU citizenship in Articles 17 to 22 TEC underpins the role that the programme must play in the near future. This focuses on promoting the active citizenship of young people and strengthening their sense of belonging to Europe.
1.4 The first two phases of the Youth for Europe programme dealt essentially with two main types of action: direct support for youth projects involving exchanges and mobility of young people between participating countries, and study and in-service training visits for youth workers.
The third phase, from 1995 to 1999, extended the exchange and mobility measures to third countries and continued the actions geared towards youth workers. It also introduced actions to promote youth-oriented activities, boost cooperation between Member States in terms of youth policies and information campaigns targeting young people, and encourage research into youth-related issues.
1.5 The European Voluntary Service for Young People programme (which ran from 1998 to 2002) opened up youth initiatives to include specific volunteer and solidarity activities, as part of the policy for cooperation in the youth field. The aim was to boost young people's involvement, encourage enterprise and initiative and promote the European ideal.
1.6 Lastly, the YOUTH programme, in force until 2006, encompasses the previous programmes in the youth field, revitalising and modernising them in the light of the new challenges. As well as maintaining the cooperation and mobility initiatives, it includes other concrete actions to support innovative, creative projects set up by young people.
1.7 In recent times, the Council of Ministers and European Councils have, on several occasions, clearly emphasised the need to ensure the continuity of the current programme. The European Parliament has also been actively involved, calling on the Commission to draw up a programme to replace the current one, in order to cater for the growing needs in the field of youth policy.
Likewise, the Treaty establishing a Constitution for Europe builds on the provisions of Article 149 TEC, stating that Community action should aim to encourage the participation of young people in democratic life in Europe.
1.8 In addition to the above reasons (which themselves justify the extension of measures aimed at young people), both the interim evaluation of the current programme and the public consultation launched by the Commission have revealed the need to maintain a specific programme that will sustain actions and promote young people's European identity and active citizenship.
1.9 Together, these aspects have led to the presentation of the Youth in Action programme for 2007-2013; under Article 149(4) TEC, the Committee's opinion is sought on the programme.
2. Content of the proposal
2.1 Broadly, the programme aims to:
- promote the active participation of young people in civil society;
- foster the values of tolerance, solidarity and intercultural dialogue among young people;
- encourage European citizenship.
2.2 The programme's general objectives, which correspond to the actions it comprises, are as follows:
- to promote young people's active citizenship in general and their European citizenship in particular;
- to develop solidarity among young people, in particular in order to reinforce social cohesion in the European Union;
- to foster mutual understanding between peoples through young people;
- to contribute to developing the quality of support systems for youth activities and the capabilities of civil society organisations in the youth field;
- to promote European cooperation in youth policies.
2.3 The actions of the programme (which refer clearly to each of the general objectives mentioned above) and the measures that these comprise are as follows:
- Youth for Europe
- youth exchanges;
- support for young people's initiatives;
- participative democracy projects.
European Voluntary Service.
- European Voluntary Service (individual);
- European Voluntary Service (groups);
- cooperation between civil or volunteer services.
Youth of the World
- cooperation with the neighbouring countries of the enlarged Europe;
- cooperation with other countries.
Youth workers and support systems
- support for bodies active at European level in the field of youth;
- support for the European Youth Forum;
- training and networking of youth workers;
- projects encouraging innovation and quality;
- information activities for young people and youth workers;
- associations;
- support for the structures of the programme;
- adding to the value of the programme.
Support for policy cooperation
- meetings of young people and those responsible for youth policy;
- support for activities to improve knowledge of youth;
- cooperation with international organisations.
2.4 The programme, scheduled for 2007-2013 and with a budget of EUR 915 million, is aimed at young people aged between 13 and 30.
3. Comments on the Proposal
3.1 The Committee has consistently supported previous youth-oriented programmes. It therefore welcomes the current proposal for a decision on the whole, and is pleased that Community action in this area is to be continued.
Since 1986, the Committee has supported this type of action, putting forward views and observations aimed at improving the content of the actions. The following opinions have been issued by the Committee:
- Opinion on the Proposal for a Council Decision creating the Youth for Europe programme. [1]
- Opinion on the Proposal for a Council Decision adopting the Youth for Europe programme (2nd phase). [2]
- Opinion on the Proposal for a European Parliament and Council Decision adopting the Youth for Europe programme (3rd phase). [3]
- Opinion on the Proposal for a European Parliament and Council Decision establishing the European Voluntary Service for Young People programme [4].
- Opinion on the Proposal for a European Parliament and Council Decision establishing the programme for Youth [5].
- Opinion on the Proposal for a European Parliament and Council Decision establishing a programme to promote bodies active at European level in the field of youth [6].
3.2 The Committee is encouraged to note that the text of the proposal includes recommendations expressed in its own-initiative Opinion on the White paper: youth policy [7]. As an integral part of European civil society, the Committee acts as a channel of communication between civil society and the European institutions, bringing its experience and knowledge to bear on all actions benefiting EU citizens.
3.3 The fact that the actions established by the previous youth programmes are being continued makes it clear that, independently of the objectives set out in these programmes, the actions are still useful and worth continuing.
However, while the need for the proposal and all its positive points are acknowledged, the Committee considers that, overall, it is more of an extension of the current programme's actions than an initiative comprising innovative measures to further the programme's main objective, i.e. to promote young people's active citizenship and their sense of belonging to Europe. The Committee considers that support should be given to new measures in some of the Programme's actions, such as promoting projects for participatory democracy (contained in Action 1) and specifically to measures for organising dialogue between decision-makers and young people, setting up youth parliaments or projects for raising awareness about active citizenship.
Although the main aim stated above still applies, it is important that the programme be consistent with the concept of life-long and life-wide learning, including learning acquired through experience. The non-formal education received by young people through the programme will be complementary to other forms of education and learning, as supported through other EU programmes.
3.4 The Committee would like the following to be added as a new measure under the Action entitled "Support for Policy Cooperation":
Thematic seminars, conferences, study visits, feasibility visits organised by youth organisations targeting young people on various subject matters of European interest.
3.5 The budget for the actions set out in the programme is EUR 915 m. over seven years. This sum comes close to the EUR 1 bn. that the Committee deemed necessary for the YOUTH programme for the period 2000-2006. In effect, the budget is being increased from the EUR 657 m. or so earmarked for the YOUTH programme to EUR 915 m. for the Youth in Action programme.
This positive fact warrants two comments. Firstly, the YOUTH budget covers a period of five years, while the Youth in Action programme will cover seven years. Secondly, the population group targeted by the current programme comprises 50 million young people, while the future programme will be aimed at 75 million.
This means that EUR 12 continue to be allocated per young person, which falls short of the EUR 20 recommended by this Committee in its opinion on the YOUTH programme [8]. Clearly, therefore, the budget allocation is still insufficient.
3.6 The Committee wishes to highlight one of the recommendations made in the proposal resulting from the interim evaluation of the YOUTH programme. Specifically, the Committee strongly agrees that the new programme, aimed at all youth sectors, should focus particularly on young people with fewer opportunities, in the widest sense of the word. In this context it is also very important to ensure that resources are distributed equally between young women and young men. The Committee reiterates the support that it has shown for these groups of young people ever since its first opinions were issued in the field of youth. It also wishes to have precise information on the actual level of participation in this programme for young people who have the fewest opportunities.
3.7 Also as a result of the recommendations made in the wake of the interim evaluation of the YOUTH Programme, the Committee believes that the profile of the new Youth in Action programme should be raised to ensure that effective information about the programme and its actions reaches as many young people and associations as possible. The Committee believes that this information should be readily available in all educational establishments, employment agencies, sports clubs and federations, and in any other institution or organisation where large numbers of young people are to be found.
3.8 Fostering the concept of European citizenship and encouraging a sense of belonging to a European Union that is steadily gaining in substance are objectives which we all support. Community action is geared towards this goal, and one of its specific areas of activity is covered in the proposal under consideration.
In the communication entitled Making citizenship Work: fostering European culture and diversity through programmes for Youth, Culture, Audiovisual and Civic Participation [9], the Commission highlights the need for European citizens to have the opportunity to feel that they belong to the EU, but it also states that, in fact, many citizens experience the Union as a distant and remote political and economic entity.
The Committee believes that actions of this kind are needed to encourage a sense of citizenship and belonging to the EU. However, it also believes that the institutions and Member States must assess their own responsibility for not meeting these objectives fully, and for the fact that the EU is seen by many as a collection of governmental economic interests that occasionally come up for discussion.
3.9 Fostering values based on tolerance, solidarity, mutual understanding and dialogue with other cultures and between generations is an objective that must be supported strongly and unreservedly. These values are explicitly included in the proposal and the Committee is in favour of seeking to achieve them.
Given that the programme is specifically aimed at young people and certain age groups in particular (adolescents) and young adults who have the fewest opportunities, the Committee considers that the proposal should place specific emphasis on certain equally important values that are not covered by the current text. These values, which are essential in order for young people to play an integral and active part in society, are to do with responsibility, the satisfaction of a job well done, compliance with social norms, and so on. In an advanced society where citizens are clearly informed of their rights, it is also important to seize every opportunity to communicate these values to citizens, particularly young people. The Youth in Action programme could be an instrument for fostering these values.
3.10 The significant linguistic diversity of the EU is further evidence of its cultural wealth. The Committee regularly stresses the importance of language learning in order to enhance knowledge and understanding among the citizens of the EU.
The Committee believes that the Youth in Action programme should also help to promote language learning, as a reference to this is included in its general and specific objectives.
The Committee proposes that linguistic diversity be mentioned in Article 2(3) of the proposal, together with Europe's cultural and multicultural diversity.
Objective 1 d) of Article 3 should therefore be worded as follows: "fostering intercultural and language learning among young people."
3.11 One of the more important aspects of the programme, as regards the transmission of values to young people, is the European Voluntary Service initiative. Since it was set up as a specific programme in 1998, it has been a valuable tool for promoting solidarity among young people and enriching personal development. The Committee therefore supports the wide variety of fields of action covered by this initiative, and is again pleased to note the Commission's sensitivity to the Committee's previous recommendations on the subject.
3.12 To the Committee, the funding earmarked for this action (the amount allocated in the financial statement appended to the proposal) seems extremely high, given that only a relatively small number of young people participate in it. On the other hand, feedback from some Member States suggests that the interest of young people in voluntary work exceeds the capacity of organisations. This may have to do with the application requirements for this action and its effectiveness. In this context, the Committee therefore believes that Action 1, Youth for Europe, is more likely to generate participation and thus have a greater effect on young people with fewer opportunities, and therefore feels it is necessary to assess the extent to which the budget distribution between the individual actions reflects demand. At the same time, options for boosting the effectiveness of each action should be considered.
3.13 "Youth policy should seek to involve young people at all stages in the decision-making process in order both to benefit from their first-hand experience and to motivate them as active and responsible citizens". This sentence, which forms part of the recommendations of the own-initiative Opinion on the White Paper: youth policy, [10] defines the aims that could serve as the frame of reference for the programme. Insofar as the programme follows this guideline, it will bring together the two basic principles set out in its objectives: citizenship and participation.
3.14 Outsourcing a high proportion of the programme's activities means that additional efforts will be required to ensure consistency and transparency. The Committee appreciates the need to keep management close to ordinary people, but it believes that the Commission should remain involved in the decision-making process when it comes to selecting the organisations eligible for funding from the programme.
3.15 Large-scale devolved implementation of the programme at national, regional and local level, with the extensive participation of organisations and individuals, will require maximum stringency, transparency and visibility to ensure that public funds are properly used.
3.16 In this process of selecting organisations and allocating funds to the projects they propose, vigilance is required of the Community institutions and competent institutions in participating countries to make sure that funds from the Youth in Action programme are not used to finance any association or organisation which tolerates or harbours — whether by design or omission — any intolerant, violent, racist or xenophobic attitudes. To this end, the organisations responsible for selecting and approving projects, both at Community level and within Member States, should identify organisations with such a history and exclude them from any selection process.
In selecting organisations eligible for Community funding, it must also be ensured that such organisations are genuinely representative and have a certain number of members.
3.17 The Committee agrees with the statement in the text of the proposal that its European added value is directly linked to the nature of the actions, i.e. promoting collaboration between States in order to allow young people to move between different countries. Countries could not achieve this by themselves. It is therefore paramount that all the participating countries take into consideration the provisions of Article 6(5) of the proposal, and take every possible step to eliminate any remaining obstacles to mobility.
The Committee has commented on this aspect on a number of occasions, in opinions on other phases of this programme and on problems related to mobility [11].
In these opinions, the Committee has strongly emphasised the need to accelerate initiatives to make it possible for citizens, particularly young people, to participate in the programme, without being hampered by bureaucratic problems that restrict its accessibility and which should have been ironed out by the Member States.
3.18 The extension of the age range of participants, by reducing the lower age limit to 13 and increasing the upper limit to 30, is a noteworthy achievement that is in line with previous recommendations of this Committee. This means opening up the programme to a large section of the population, with 75 million potential participants. The Committee considers that a study should be drawn up to give a clearer picture of this age group (unprecedented in history not only in its diversity, but also in its unity) and thereby underpin the youth programme in the European Union, especially if the latter wishes to follow the recommendations of the High-Level Group on "the future of social policy", which seek to change the generational pact.
However, the Committee reiterates its suggestion to extend the age for participating in exchange initiatives to 11: although this age group can be considered as pre-adolescent, there is no doubt that learning and the transmission of values are absorbed in specific ways when they take place at an earlier age. This participation should always take place as part of a properly organised scheme and never on an individual basis.
3.19 Article 15 of the proposal for a decision calls for the mandatory interim and ex post evaluation of the programme. As regards the interim evaluation, the Committee believes that one of the important aspects to be evaluated is the programme's impact in the various participating countries. Having assessed its impact, we could then concentrate on raising awareness of the programme in those countries where participation in the Youth in Action initiatives is low. Thus we could try to weight the distribution of funds, so that they are not concentrated in structurally strong areas with extensive experience of developing this type of action, at the same time helping to transfer this experience to regions with lower levels of participation in the programme's actions.
3.20 As already noted in this opinion, the Youth in Action programme builds on the actions of previous programmes in the youth field. Therefore, the social partners must participate in the initial preparation and subsequent assessment work, and youth organisations must be involved in implementing some of the programme's actions, particularly those which involve unpaid work. This will help avoid potential distortions of the labour market and prevent voluntary work from being used as a substitute for paid work, especially skilled work.
3.21 Moreover, the Committee stresses the need for increased collaboration between the social partners and youth or youth-oriented organisations, as regards the measures to support youth initiatives under the Youth for Europe programme. The experience of our organisations can serve as a strong stimulus for initiative, enterprise and creativity among young Europeans.
3.22 The Committee welcomes the inclusion of measures to support youth associations as a means of encouraging young people's involvement in civil society. It therefore feels that particular emphasis should be placed on promoting such associations in places and among groups of young people poorly served by existing organisations.
In relation to the financial provisions included in the statement accompanying the proposal, the Committee is dissatisfied with the reduction in the budget earmarked for organisations active at European level in the field of youth. The Committee believes that to remain consistent with the objectives of the new programme, the proportion allocated to this action must not drop below the levels in the YOUTH programme.
3.23 The Committee welcomes the new initiative launched by the Heads of State and Government of France, Germany, Spain and Sweden to sign a Pact for European Youth as part of the Lisbon Strategy, in order to develop new ways of involving young people in politics.
The Committee believes that the pact should be seen as a means of obtaining results in fields such as employment, social inclusion and education, in the European Union's youth policy and the Youth in Action programme, and as a new issue on the European Youth Policy agenda.
Brussels, 10 March 2005.
The President
of the European Economic and Social Committee
Anne-Marie Sigmund
[1] CES 769/1986
[2] OJ C 159 of 17.06.1991
[3] OJ C 148 of 30.05.1994
[4] OJ C 158 of 26.05.1997
[5] OJ C 410 of 30.12.1998
[6] OJ C 10 of 14.01.2004
[7] OJ C 116 of 20.04.2001
[8] OJ C 410 of 30.12.1998
[9] COM (2004) 154 final.
[10] OJ C 116 of 20.04.2001
[11] OJ C 133 of 28.4.1997 - Opinion on the Green Paper on education, training and research: obstacles to cross-frontier mobilityOJ C 149 of 21.6.2002 - Opinion on the proposal for a European Parliament and Council directive on the right of citizens of the Union and their family members to move and reside freely within the territory of the Member States.
--------------------------------------------------
APPENDIX
to the Opinion of the Economic and Social Committee
The following amendment, though rejected, received at least one-quarter of the votes cast:
New point 3.14
The Commission should make sure that the work of National Agencies administrating the Youth programme is coordinated in such a way that the decentralisation of its implementation in Member States does not create new barriers for the access to this programme.
The work and practices of National Agencies should be monitored and evaluated by a Committee composed of European Commission officials and relevant social partners in the course of implementation of the new Youth in Action programme.
Reason
The Youth in Action programme proposal is much more decentralised than the current Youth programme. In the course of implementation national youth agencies of the countries participating in the programme play the major role and render the important decisions, they set national priorities and decide upon many details of the programme implementation. Decentralisation is a big problem for many European youth organisations and networks, due to different national priorities of National Agencies and their different approach to partnership, European youth organisations and their branches from different countries cannot cooperate in a desired manner.
Result of the vote
Votes for: 51
Votes against: 72
Abstentions: 30
--------------------------------------------------
