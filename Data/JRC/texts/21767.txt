COUNCIL DECISION of 30 November 1992 concerning the conclusion of the Agreement between the European Economic Community and the Swiss Confederation on the carriage of goods by road and rail (92/578/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 75 thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament (1),
Having regard to the opinion of the Economic and Social Committee (2),
Whereas the agreement between the European Economic Community and the Swiss Confederation on the carriage of goods by road and rail can provide a solution to the various current problems of trans-Alpine goods traffic; whereas it is necessary to ensure the non-discriminatory development of transit so as to enable international trade to be conducted at the least possible cost to the public at large and to reduce to a minimum the administrative and technical obstacles which affect transit;
Whereas these objectives must, at the same time, take account of respect for users' freedom of choice and aspects relating to road safety, protection of public health and the environment in Alpine regions;
Whereas the objectives and the content of the Agreement fall within the scope of the common transport policy and the technical standards play their part in attaining these objectives;
Whereas it is appropriate to lay down a procedure with a view to approving the administrative arrangements provided for by the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
The Agreement between the European Economic Community and the Swiss Confederation on the carriage of goods by road and rail is hereby approved on behalf of the Community.
The text of the Agreement is attached to this Decision.
Article 2
The President of the Council shall give the notification provided for in Article 21 of the Agreement.
Article 3
The administrative arrangement provided for in point II.4 of Annex 6 to the Agreement shall be approved in accordance with the procedure laid down in Article 4 of this Decision.
Article 4
The Commission shall be assisted by a committee composed of the representatives of the Member States and chaired by the Commission representative.
The representative of the Commission shall submit to the Commission a draft of the measures to be taken. The Committee shall deliver its opinion on the draft within a time limit which the chairman may lay down according to the urgency of the matter. The opinion shall be delivered by the majority laid down in Article 148 (2) of the Treaty in the case of decisions which the Council is required to adopt on a proposal from the Commission. The votes of the representatives of the Member States within the Committee shall be weighted in the manner set out in that Article. The chairman shall not vote.
The Commission shall adopt the provisions envisaged if they are in accordance with the opinion of the Committee.
If the measures envisaged are not in accordance with the opinion of the Committee, or if no opinion is delivered, the Commission shall, without delay, submit to the Official Journal of the European Communities
No L 373/27
21. 12. 92
Council a proposal relating to the measures to be taken. The Council shall act by a qualified majority.
If, on the expiry of a period of four weeks from the date of referral to the Council, the Council has not acted, the proposed measures shall be adopted by the Commission.
Article 5
The Commission shall adopt the necessary measures for implementing the administrative arrangement referred to in Article 3 in accordance with the procedure laid down in Article 4.
Done at Brussels, 30 November 1992.
For the Council
The President
T. EGGAR
(1) OJ No C 305, 23. 11. 1992.(2) OJ No C 313, 30. 11. 1992, p. 16.
