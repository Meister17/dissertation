Removal from the register of Case C-510/03 [1]
(2005/C 296/37)
(Language of the case: German)
By order of 21 July 2005, the President of the Court of Justice of the European Communities has ordered the removal from the register of Case C-510/03 Commission of the European Communities
v Federal Republic of Germany.
[1] OJ C 21, 24.1.2004.
--------------------------------------------------
