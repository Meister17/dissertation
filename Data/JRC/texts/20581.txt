COMMISSION REGULATION (EC) No 3272/94 of 27 December 1994 concerning the classification of certain goods in the combined nomenclature
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2658/87 (1) on the tariff and statistical nomenclature and on the Common Customs Tariff, as last amended by Commission Regulation (EC) 3176/94 (2), and in particular Article 9 thereof,
Whereas in order to ensure uniform application of the combined nomenclature annexed to the said Regulation, it is necessary to adopt measures concerning the classification of the goods referred to in the Annex to this Regulation;
Whereas Regulation (EEC) No 2658/87 has set down the general rules for the interpretation of the combined nomenclature and those rules also apply to any other nomenclature which is wholly or partly based on it or which adds any additional subdivision to it and which is established by specific Community provisions, with a view to the application of tariff and other measures relating to trade in goods;
Whereas, pursuant to the said general rules, the goods described in column 1 of the table annexed to the present Regulation must be classified under the appropriate CN codes indicated in column 2, by virtue of the reasons set out in column 3;
Whereas it is accepted that binding tariff information issued by the customs authorities of Member States in respect of the classification of goods in the combined nomenclature and which do not conform to the rights established by this Regulation, can continue to be invoked, under the provisions in Article 12 (6) of Council Regulation (EEC) No 2913/92 establishing the Community Customs Code (3), for a period of three months by the holder;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Tariff and Statistical Nomenclature Section of the Customs Code Committee,
HAS ADOPTED THIS REGULATION:
Article 1
The goods described in column 1 of the annexed table are now classified within the combined nomenclature under the appropriate CN codes indicated in column 2 of the said table.
Article 2
Binding tariff information issued by the customs authorities of Member States which do not conform to the rights established by this Regulation can continue to be invoked under the provisions of Article 12 (6) of Regulation (EEC) No 2913/92 for a period of three months.
Article 3
This Regulation shall enter into force on the 21st day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 27 December 1994.
For the Commission
Christiane SCRIVENER
Member of the Commission
(1) OJ No L 256, 7. 9. 1987, p. 1.
(2) OJ No L 335, 23. 12. 1994, p. 56.
(3) OJ No L 302, 19. 10. 1992, p. 1.
ANNEX
"" ID="1">1. A steel ball coated with silicon rubber and meeting the following specifications:> ID="2">8473 30 90> ID="3">Classification is determined by the provisions of General Rules 1 and 6 for the interpretation of the combined nomenclature, Note 2 b) to Section XVI and by the wording of CN codes 8473, 8473 30 and 8473 30 90."> ID="1">- shore hardness (A type) of 70 ± 5,"> ID="1">- raised molding parting line not exceeding 0,05 mm,"> ID="1">- diameter not exceeding 22 mm, and"> ID="1">- total weight of 31 grams (± 1 gram)"> ID="1">designed for use in the manufacture of a pointing device (so called 'computer mouse')."> ID="1">2. A magnetic floppy disk on which an analogue (wave-form) signal has been recorded during manufacture, solely for the purpose of checking the quality of the disk's magnetic coating.> ID="2">8523 20 90> ID="3">Classification is determined by the provisions of General Rules 1 and 6 for the interpretation of the combined nomenclature and by the wording of CN codes 8523, 8523 20 and 8523 20 90."> ID="3">As the analogue signal in question cannot serve any purpose after manufacture, such a signal cannot be considered as a recording within the meaning of heading 8524."> ID="1">3. A junction box for use in cable television systems, comprising a metal box (approximate size 10 × 10 × 7 cm) fitted with cable inputs and terminals. The box contains an electric circuit with a number of electric components (such as coils, transformers, resistors and capacitors) and connections.> ID="2">8543 80 80> ID="3">Classification is determined by the provisions of General Rules 1 and 6 for the interpretation of the combined nomenclature and by the wording of CN codes 8543, 8543 80 and 8543 80 80."> ID="1">The function of the junction box is to reduce the signal voltage fed through the cable to the television receivers and, by means of a filter, to prevent the supply voltage for the amplifiers from reaching the television receivers.> ID="3">The junction box in design and function is not an apparatus for making connections to or in electrical circuits within the meaning of heading 8536. Although used as a part for television systems, it is an appliance having an individual function.">
