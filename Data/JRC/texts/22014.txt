FIRST COUNCIL DIRECTIVE on the establishment of certain common rules for international transport (carriage of goods by road for hire or reward)
THE COUNCIL OF THE EUROPEAN ECONOMIC COMMUNITY,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 75 (1) thereof;
Having regard to the proposal from the Commission;
Having regard to the Opinion of the Economic and Social Committee;
Having regard to the Opinion of the European Parliament;
Whereas the adoption of a common transport policy involves inter alia laying down common rules for the international carriage of goods by road to or from the territory of a Member State or passing across the territory of one or more Member States;
Whereas the progressive establishment of the common market must not be impeded by obstacles in the transport sector ; whereas it is necessary to ensure a progressive expansion of the international carriage of goods by road, bearing in mind developments in trade and movement of goods within the Community;
HAS ADOPTED THIS DIRECTIVE:
Article 1
1. Each Member State shall, by the end of 1962 at the latest and in the manner laid down in paragraphs 2 and 3 of this Article, liberalise the types of international carriage of goods by road for hire or reward involving other Member States listed in Annexes I and II to this Directive, where such carriage is performed to or from the territory of that Member State or passing in transit across the territory of that Member State.
2. The types of carriage listed in Annex I shall be exempted from any quota or authorisation system.
3. The types of carriage listed in Annex II shall no longer be subject to a quota system. They may, however, remain subject to authorisation provided no quantitative restriction is involved ; in such case Member States shall ensure that decisions on applications for authorisation are given within five days of receipt.
4. The two Annexes to this Directive shall form an integral part thereof.
Article 2
Member States shall inform the Commission of the measures taken to implement this Directive within three months of its entry into force and in any event before the end of 1962.
Article 3
This Directive shall not affect the conditions under which any Member State authorises its own nationals to engage in the activities mentioned in this Directive.
Article 4
This Directive is addressed to the Member States.
Done at Brussels, 23 July 1962.
For the Council
The President
E. COLOMBO
ANNEX 1 Types of carriage to be exempted from any transport quota or authorisation system
1. Frontier traffic in a zone extending on each side of the frontier to a depth of 25 kilometres as the crow flies, provided that the total distance involved does not exceed 50 kilometres as the crow flies.
2. Occasional carriage of goods to or from airports, in the event of air services being diverted.
3. Carriage of luggage in trailers coupled to passenger-carrying vehicles and the carriage of luggage in all types of vehicle to and from airports.
4. Carriage of mails.
5. Carriage of damaged vehicles.
6. Carriage of refuse and sewage.
7. Carriage of animal carcases for disposal.
8. Carriage of bees and fish fry.
9. Funeral transport.
ANNEX II Types of carriage to be exempted from any quota system but which may remain subject to authorisation in accordance with Article 1 (3) of this Directive
1. Carriage from a Member State into the frontier zone of an adjacent Member State, extending to a depth of 25 kilometres as the crow flies from their common frontier.
2. Carriage of goods in motor vehicles the laden weight of which, including that of trailers, does not exceed 6000 kilogrammes.
3. Carriage of objects and works of art for exhibition or for commercial purposes.
4. Occasional carriage of objects and material exclusively for publicity or information purposes.
5. Removals by undertakings having special staff and equipment for this purpose.
6. Carriage of material, properties and animals to or from theatrical, musical or film performances or sporting events, circuses, exhibitions or fairs, or to or from the making of radio or television broadcasts or films.
