ANNEX
FIFTH PROTOCOL TO THE GENERAL AGREEMENT ON TRADE IN SERVICES
MEMBERS OF THE WORLD TRADE ORGANISATION (hereinafter referred to as 'the WTO`) whose Schedules of specific commitments and lists of exemptions from Article II of the General Agreement on Trade in Services concerning financial services are annexed to this Protocol (hereinafter referred to as 'Members concerned`),
Having carried out negotiations under the terms of the second Decision on financial services adopted by the Council for Trade in Services on 21 July 1995 (S/L/9),
AGREE AS FOLLOWS:
1. A schedule of specific commitments and a list of exemptions from Article II concerning financial services annexed to this Protocol relating to a Member shall, upon the entry into force of this Protocol for that Member, replace the financial services sections of the schedule of specific commitments and the list of Article II exemptions of that Member.
2. This Protocol shall be open for acceptance, by signature or otherwise, by the Member concerned until 29 January 1999.
3. This Protocol shall enter into force on the 30th day following the date of its acceptance by all Members concerned. If by 30 January 1999 it has not been accepted by all Members concerned, those Members which have accepted it before that date may, within a period of 30 days thereafter, decide on its entry into force.
4. This Protocol shall be deposited with the Director-General of the WTO. The Director-General of the WTO shall promptly furnish to each Member of the WTO a certified copy of this Protocol and notifications of acceptances thereof pursuant to paragraph 3.
5. This Protocol shall be registered in accordance with the provisions of Article 102 of the Charter of the United Nations.
Done at Geneva this twenty-seventh day of February one thousand nine hundred and ninety-eight, in a single copy in English, French and Spanish languages, each text being authentic, except as otherwise provided for in respect of the schedules annexed hereto.
Annex
EUROPEAN COMMUNITIES AND THEIR MEMBER STATES
SCHEDULE OF SPECIFIC COMMITMENTS
Supplement 4
(This is authentic in English, French and Spanish)
This text replaces the financial services section contained in document GATS/SC/31/Suppl. 1/Rev. 1.
>TABLE>
ADDITIONAL COMMITMENTS BY THE EUROPEAN COMMUNITIES AND THEIR MEMBER STATES
INSURANCE
(a) The European Communities and their Member States note the close cooperation among the insurance regulatory and supervisory authorities of the Member States and encourage their efforts to promote improved supervisory standards.
(b) Member States will make their best endeavours to consider within six months complete applications for licences to conduct direct insurance underwriting business, through the establishment in a Member State of a subsidiary in accordance with the legislation of that Member State, by an undertaking governed by the laws of a third country. In cases where such applications are refused, the Member State authority will make its best endeavours to notify the undertaking in question and give the reasons for the refusal of the application.
(c) The supervisory authorities of the Member States will make their best endeavours to respond without undue delay to requests for information by applicants on the status of complete applications for licences to conduct direct insurance underwriting business, through the establishment in a Member State of a subsidiary in accordance with the legislation of that Member State by an undertaking governed by the laws of a third country.
(d) The European Communities and their Member States will make their best endeavours to examine any questions pertaining to the smooth operation of the internal market in insurance, and consider any issues that might have an impact on the internal market in insurance.
(e) The European Communities and their Member States note that, as regards motor insurance, under EC law as in force on 31 December 1997, and without prejudice to future legislation, premiums may be calculated taking several risk factors into account.
(f) The European Communities and their Member States note that under EC law, as in force on 31 December 1997, and without prejudice to future legislation, the prior approval by national supervisory authorities of policy conditions and scales of premiums that an insurance undertaking intends to use is generally not required.
(g) The European Communities and their Member States not that under EC law, as in force on 31 December 1997, and without prejudice to future legislation, the prior approval by national supervisory authorities of increases in premium rates is generally not required.
OTHER FINANCIAL SERVICES
(a) In application of the relevant EC directives, Member States will make their best endeavours to consider within 12 months complete applications for licences to conduct banking activities, through the establishment in a Member State of a subsidiary in accordance with the legislation of that member State, by an undertaking governed by the laws of a third country. In cases where such applications are refused, the Member State will make its best endeavours to notify the undertaking in question and give the reasons for the refusal of the application.
(b) Member States will make their best endeavours to respond without undue delay to requests for information by applicants on the status of complete applications for licences to conduct banking activities, through the establishment in a Member State of a subsidiary in accordance with the legislation of that Member State, by an undertaking governed by the laws of a third country.
(c) In application of the relevant EC directives, Member States will make their best endeavours to consider within six months complete applications for licences to conduct investment services in the securities field, as defined in the Investment Services Directive, through the establishment in a Member State of a subsidiary in accordance with the legislation of that Member State, by an undertaking governed by the laws of a third country. In cases where such applications are refused, the Member State will make its best endeavours to notify the undertaking in question and give the reasons for the refusal of the application.
(d) Member States will make their best endeavours to respond without undue delay to requests for information by applicants on the status of complete applications for licenses to conduct investment services in the securities area, through the establishment in a Member State of a subsidiary in accordance with the legislation of that Member State, by an undertaking governed by the laws of a third country.
EUROPEAN COMMUNITIES AND THEIR MEMBER STATES
FINAL LIST OF ARTICLE II (MFN) EXEMPTIONS
(This is authentic in English only)
>TABLE>
DECISION ADOPTING THE FIFTH PROTOCOL TO THE GENERAL AGREEMENT ON TRADE IN SERVICES
adopted by the Committee on Trade in Financial Services on 14 November 1997
THE COMMITTEE ON TRADE IN FINANCIAL SERVICES,
Having regard to the results of the negotiations conducted under the terms of the Second Decision on Financial Services adopted by the Council for Trade in Services on 21 July 1995 (S/L/9),
DECIDES AS FOLLOWS:
1. To adopt the text of the 'Fifth Protocol to the General Agreement on Trade in Services`.
2. Commencing immediately and continuing until the date of entry into force of the Fifth Protocol to the General Agreement on Trade in Services, Members concerned shall, to the fullest extent consistent with their existing legislation, not take measures which would be inconsistent with their undertakings resulting from these negotiations.
3. The Committee shall monitor the acceptance of the Protocol by Members concerned and shall, at the request of a Member, examine any concerns raised regarding the application of paragraph 2 above.
DECISION OF DECEMBER 1997 ON COMMITMENTS IN FINANCIAL SERVICES
adopted by the Council for Trade in Services on 12 December 1997
THE COUNCIL FOR TRADE IN SERVICES,
Having regard to the Second Decision on Financial Services adopted by the Council for Trade in Services on 21 July 1995 (S/L/9),
Noting the results of the negotiations carried out under the terms of that Decision,
Having regard to the Decision adopting the Fifth Protocol to the General Agreement on Trade in Services adopted by the Committee on Trade in Financial Services on 14 November 1997 (S/L/44),
DECIDES AS FOLLOWS:
1. If the Fifth Protocol to the General Agreement on Trade in Services (GATS) does not enter into force in accordance with paragraph 3 therein:
(a) notwithstanding Article XXI of the GATS, a Member may, during a period of 60 days beginning on 1 March 1999, modify or withdraw all or part of the commitments on financial services inscribed in its schedule;
(b) notwithstanding Article II of the GATS and paragraphs 1 and 2 of the Annex on Article II exemptions, a Member may, during the same period referred to in paragraph 1(a), list in that Annex measures relating to financial services which are inconsistent with paragraph 1 of Article II of the GATS.
2. The Committee on Trade in Financial Services shall establish any procedures necessary for the implementation of paragraph 1.
