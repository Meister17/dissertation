Information communicated by Member States regarding State aid granted under Commission Regulation (EC) No 68/2001 of 12 January 2001 on the application of Articles 87 and 88 of the EC Treaty to training aid
(2005/C 321/10)
(Text with EEA relevance)
Aid No : XT 50/03
Member State : Italy
Region : Basilicata
Title of aid scheme : Priority III, Measure III.1.D.1, Public Notice (A.P.) No 01-2003 — Continuing Training
Legal basis - P.O.R. Basilicata 2000/2006 approvato con decisione 2372 del 22.8.2000
- Complemento di programmazione
Annual expenditure planned under the scheme : The total sum of resources provided for 2003 for Measure III.1.D.1 is EUR 6240693
| For general training programmes | For specific training programmes |
Large enterprises | 60 % | 35 % |
SMEs | 80 % | 45 % |
Maximum aid intensity :
Regarding the final recipient, the intensities specified are increased by 10 percentage points if the aid is intended for training disadvantaged workers as referred to in Article 2(g) of Regulation (EC) No 68/2001
Date of implementation : 23 July 2003
Duration of scheme : Until 31 December 2005
Objective of aid : To put in place Continuing training schemes for companies located in the Basilicata region. Whether the training is general or specific will be established ex ante by the Evaluation Committee, based on the specific, defined criteria in the attached public announcement
Economic sector(s) concerned : All sectors except those in Article 32 of the EC Treaty
Name and address of granting authority Regione Basilicata
Dipartimento Formazione Lavoro Cultura e Sport
C.so Umberto I, 28
I-85100 Potenza
Other information 1. Exempted training aid, Former Regulation (EC) No 68/2001
2. De minimis training aid, Former Regulation (EC) No 69/2001
--------------------------------------------------
