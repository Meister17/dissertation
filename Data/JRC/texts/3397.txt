Action brought on 16 September 2005 — Republic of Finland v Commission of the European Communities
Parties
Applicant(s): Republic of Finland (represented by: Tuula Pynnä, Agent)
Defendant(s): Commission of the European Communities
Form of order sought
The applicant(s) claim(s) that the Court should:
- annul the Commission's decision, dated 8 July 2005 and notified on the same day, refusing, contrary to the principle of loyal cooperation under Article 10 EC and to the case-law of the Court of Justice concerning conditional payments, to open negotiations with Finland on the conditional payment of the retroactive customs duties and default interest on them up to the date of payment demanded by the Commission from Finland in infringement proceedings 2003/2180 under Article 226 EC, and
- order the Commission to pay the costs.
Pleas in law and main arguments
In the contested decision the Commission considered that in the present case it was not under an obligation to act within the meaning of Article 232 EC. Finland had, in its letter under Article 232 EC, called on the Commission, under the principle of loyal cooperation under Article 10 EC and the case-law of the Court of Justice on conditional payments, to take a decision to enter into negotiations with Finland on the conditional payment, pending the decision of the Court of Justice in the case, of the disputed customs debt and the associated default interest.
Finland submits that by taking the contested decision the Commission infringed the EC Treaty or the legislation relating to its application within the meaning of the second paragraph of Article 230 EC by refusing, contrary to the principle of loyal cooperation under Article 10 EC and the case-law of the Court of Justice on conditional payments, to start negotiations on the conditional payment of the retroactive customs duties and default interest on them up to the date of payment demanded by the Commission from Finland under Regulation (EC, Euratom) No 1150/2000 [1] in infringement proceedings 2003/2180, and by not giving reasons for the refusal contrary to Article 253 EC.
[1] Council Regulation (EC, Euratom) No 1150/2000 of 22 May 2000 implementing Decision 94/728/EC, Euratom on the system of the Communities' own resources, OJ L 130 of 31.5.2000, p. 1.
--------------------------------------------------
