Order of the President of the Court of First Instance of 26 June 2006 — Olympiakes Aerogrammes
v Commission
(Case T-416/05 R)
Parties
Applicant: Olympiakes Aerogrammes AE (Kallithea, Greece) (represented by: V. Christianos, lawyer)
Defendant: Commission of the European Communities (represented by: D. Triantafyllou and T. Scharf, acting as Agents)
Re:
Application for a suspension of operation of Article 2, combined with Article 1(1) of Commission Decision C 11/2004 on State aid (ex NN 4/2003) — Olympiaki Aeroporia — Restructuring and privatisation, of 14 September 2005
Operative part of the order
1. The application for interim measures is dismissed;
2. Costs are reserved.
--------------------------------------------------
