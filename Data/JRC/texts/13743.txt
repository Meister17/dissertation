Declaration by the Netherlands in accordance with Article 5 and Article 97 of Council Regulation (EEC) No 1408/71 of 14 June 1971 on the application of social security schemes to employed persons, to self-employed persons and to members of their families moving within the Community
(2006/C 56/01)
The Netherlands Government wishes to inform you of the following:
In accordance with Article 5 and Article 97 of Regulation (EEC) No 1408/71 on the application of social security schemes to employed persons, to self-employed persons and to members of their families moving within the Community, Member States are required to specify the social security legislation to which this Regulation applies and to notify it to you.
The Netherlands Government wishes to amend one point of the declaration
On 1 January 2005 the Childcare Act entered into force. The financing of childcare is borne by three parties: parents, government and employers. A precondition for a means-tested government contribution towards childcare costs is that both parents must be in employment or in receipt of benefit. The contribution paid by the government must be classified as a family allowance.
The above gives rise to the following amendment:
Legislation and schemes referred to in Article 4(1) and (2) of the Regulation
Family allowances:
- "Law of 9 July 2004 (Netherlands Official Gazette 2004, 455) regulating matters relating to contributions towards the costs of childcare and safeguards concerning the quality of childcare (Childcare Act)".
--------------------------------------------------
