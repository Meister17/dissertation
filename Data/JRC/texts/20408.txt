COMMISSION DECISION of 18 April 1994 concerning veterinary certification for placing on the market in the United Kingdom and Ireland of dogs and cats not originating in those countries (94/273/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 92/65/EEC of 13 July 1992 concerning laying down animal health requirements governing trade in and imports into the Community of animals, semen, ova and embryos not subject to animal health requirements laid down in specific Community rules referred to in Annex A (I) to Directive 90/425/EEC (1), and in particular Article 10 (3) (a) (vii) thereof,
Whereas it is necessary to establish a veterinary certificate for dogs and cats that are placed on the market in the United Kingdom and Ireland and not originating in those countries;
Whereas the animals concerned must fulfil the relevant necessary health conditions, as reflected in the certificate and duly attested to by an authorized veterinarian;
Whereas conforming to Article 10 (3) (a) (vii) dogs and cats must be accompanied by an individual vaccination record;
Whereas the measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
The model of the certificate provided for in Article 10 (3) (a) (vii) of Directive 92/65/EEC is established in the Annex.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 18 April 1994.
For the Commission
René STEICHEN
Member of the Commission
(1) OJ No L 268, 14. 9. 1992, p. 54.
ANNEX
(1) Health certificates may be drawn up only for animals which are to be transported in the same mode of transport and which come from the same holding and which are being sent to the same consignee.
(2) Delete where not applicable.
(3) The certificate is valid for only one species at a time.
(4) Give the registration number in the case of lorries, trucks, vans or cars, the flight number in the case of aircraft, the name in the case of boat/ship, and in the case of rail travel, the estimated date and time of arrival.
(5) Delete where not applicable.
