Final Report of the Hearing Officer
in case COMP/ M.3333 — SONY/BMG
(pursuant to Article 15 of Commission Decision 2001/462/EC, ECSC of 23 May 2001 on the terms of reference of hearing officers in certain competition proceedings — OJ L 162, 19.6.2001, p. 21)
(2005/C 52/04)
(Text with EEA relevance)
The draft decision gives rise to the following observations:
Written procedure
It is recalled that on 9 January 2004 the undertakings Bertelsmann AG ("Bertelsmann") and Sony Corporation of America belonging to the Sony group, Japan ("Sony"), [1] notified the merger of their global recorded music businesses to the Commission under Article 4 of Council Regulation (EEC) No 4064/89 of 21 December 1989 on the control of concentrations between undertakings [2] ("the Merger Regulation").
By decision of 12 February 2004 the Commission initiated proceedings pursuant to Article 6(1)(c) of the Merger Regulation.
The procedure was suspended from 7 April until 5 May 2004 pursuant to Article 11(5) of the Merger Regulation as the parties had not fully responded to a request for information.
A statement of objections ("SO") was sent to the notifying parties on 24 May 2004.
The notifying parties were asked to reply by 9 June 2004. This deadline was complied with.
Access to file
Access to the file was granted to the notifying parties on 19 May 2004.
Subsequent to a meeting between myself, the representatives of the notifying parties and the case team which took place on 1 June 2004, I granted access to additional information in the Commission's file.
In order to allow the economists of the notifying parties to access third parties' confidential data in the Commission's data room, the economists signed a confidentiality declaration, the contents of which had been approved by Universal Music international, Warner Music Group and EMI Group. By mutual consent with the notifying parties and the third parties, I controlled compliance with this clause.
Finally, the notifying parties were granted further access to the file on 10 June 2004, when they were provided with the non-confidential version of the documents submitted by the European Broadcasting Association and Apple Computer Inc.
Involvement of third parties
I admitted the following undertakings as third parties according to Article 11(c) of Commission Regulation (EC) No 447/98: Apple Computer Inc., Universal Music International, Syndicat des Détaillants Spécialisés du Disque and Union des Producteurs Phonographiques Français Indépendants, European Consumer's Organisation, European Broadcasting Union, Playlouder, IMPALA, International Music Managers Forum, the Swedish Society of Popular Music Composers, EMI group, Warner Music Group, Time Warner Inc. In order to inform them of the nature and subject matter of the procedure, pursuant to Article 16 of Commission Regulation (EC) No 447/98, DG Competition sent them a non-confidential version of the SO.
Oral hearing
An oral hearing took place on 14 and 15 June 2004.
Most of the third parties that had taken part in the procedure also participated in the oral hearing.
EMI Group and Warner Music Group requested to be admitted as observers to the oral hearing. As I had already informed both companies before in writing, I take the view that hearings are not occasions in which interested third parties are allowed to attend without participating actively. Therefore, their admission was contingent on their willingness to present their views in the hearing. Since they were not in a position to do this, they could not be admitted.
Notwithstanding their non-admission to the oral hearing, I consider that both companies, as well as the other third parties, have had sufficient possibilities to participate in the ongoing proceedings and to ensure that their views are known by the Commission services. All third parties have had the opportunity to obtain an in-depth knowledge of the issues raised by the case in the course of the procedure; some third parties made comprehensive contributions to the Commission analysis undertaken both before the issuing of the SO and after having received the non confidential version of the SO.
Taking into account the replies of the parties to the SO and their explanations given at the oral hearing, DG Competition has concluded that the objections which it had set out in the SO did not stand.
In the light of the above, I consider that the rights to be heard of all participants to the present proceeding have been respected.
Brussels, 13 July 2004.
(signed)
Serge Durande
[1] Both refered to hereinafter as "the notifying parties".
[2] OJ L 395, 30.12.1989, p. 1; corrigendum, OJ L 257, 21.9.1990, p. 13. Last amended by Regulation (EC) No 1319/97 (OJ L 180, 9.7.1997, p. 1); corrigendum, OJ L 40, 13.2.1998, p. 17.
--------------------------------------------------
