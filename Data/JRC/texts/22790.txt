COMMISSION REGULATION (EEC) No 3582/81 of 14 December 1981 amending Regulation (EEC) No 2973/79 laying down detailed rules for the application of granting of assistance for the export of beef and veal products which may benefit from a special import treatment in a third country
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 805/68 of 27 June 1968 on the common organization of the market in beef and veal (1), as last amended by the Act of Accession of Greece, and in particular Article 15 (2) thereof,
Having regard to Council Regulation (EEC) No 2931/79 of 20 December 1979 on the granting of assistance for export of agricultural products which may benefit from a special import treatment in a third country (2), and in particular Article 1 (2) thereof,
Whereas Commission Regulation (EEC) No 2973/79 (3), as last amended by Regulation (EEC) No 2377/80 (4), laid down detailed rules for the application of Regulation (EEC) No 2931/79;
Whereas the United States of America may apply special arrangements for an annual quantity of 5 000 tonnes of beef and veal originating in the Community and meeting certain requirements ; whereas experience shows that this quantity should be distributed over four quarterly instalments;
Whereas Commission Regulation (EEC) No 77/81 (5) provided that applications for export licences for the month of January 1982 could be lodged from 16 December 1981 ; whereas it is necessary to repeal this provision to take account of this new system;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Beef and Veal,
HAS ADOPTED THIS REGULATION:
Article 1
The following subparagraph is added to Article 1 (1) of Regulation (EEC) No 2973/79:
"This quantity shall be divided into four parts of which the first three, which may not exceed 1 250 tonnes, shall be exported during the first three quarters of each year. The Commission shall determine the remainder to be exported during the fourth quarter".
Article 2
The third subparagraph of Article 2 of Regulation (EEC) No 77/81 is deleted.
Article 3
This Regulation shall enter into force on 1 January 1982.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 14 December 1981.
For the Commission
Poul DALSAGER
Member of the Commission (1) OJ No L 148, 28.6.1968, p. 24. (2) OJ No L 334, 28.12.1979, p. 8. (3) OJ No L 336, 29.12.1979, p. 44. (4) OJ No L 241, 13.9.1980, p. 5. (5) OJ No L 12, 14.1.1981, p. 5.
