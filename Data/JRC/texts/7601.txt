COUNCIL DECISION of 22 December 1986 establishing a consultation and cooperation procedure in the field of tourism (86/664/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 235 thereof,
Having regard to the proposal from the Commission(1),
Having regard to the opinion of the European Parliament(2),
Having regard to the opinion of the Economic and Social Committee(3),
Whereas Article 2 of the Treaty provides that the Community shall have as its task in particular to promote throughout the Community a harmonious development of economic activities, a continuous and balanced expansion and closer relations between the States belonging to it; whereas tourism can help to achieve these objectives;
Whereas the Council resolution of 10 April 1984 on a Community policy on tourism(4) emphasizes the need for consultation between the Member States and the Commission on tourism;
Whereas consultation is a useful means of facilitating cooperation between the Member States and the Commission with a view to achieving the objectives of the Treaty;
Whereas each Member State should give the other Member States and the Commission the benefit of its experience in the field of tourism;
Whereas, with a view to consultation in the field of tourism, exchanges of information between the Member States and the Commission should be ensured;
Whereas such consultation should not duplicate work carried out in other Community bodies,
HAS DECIDED AS FOLLOWS:
Article 1
An Advisory Committee on Tourism, hereinafter referred to as the 'Committee', shall be set up under the auspices of the Commission. It shall be made up of members designated by each Member State.
Article 2
The task of the Committee shall be to facilitate exchanges of information, consultation and, where appropriate, cooperation on tourism, and, in particular, on the provision of services for tourists.
Article 3
For the purposes referred to in Article 2, each Member State shall send the Commission, once a year, a report on the most significant measures it has taken and, as far as possible, on measures it is considering taking in the provision of services for tourists which could have consequences for travellers from the other Member States.
The Commission shall inform the other Member States thereof.
Article 4
1. The Committee, which shall meet at least once a year, shall hold an exchange of views on the basis of the reports referred to in Article 3 in order to facilitate, where necessary, future cooperation amongst the Member States in pursuit of the objectives referred to in Article 2.
2. At the request of the Commission or a Member State, the Committee shall also discuss any matter which may be of interest to a number of Member States.
3. The Committee shall also advise the Commission on any question on which the latter has requested an opinion.
4. The information and consultations provided for in this Decision shall be covered by professional secrecy.
Article 5
The Committee shall be chaired by the Commission.
The Commission shall provide the Committee with secretarial services.
Done at Brussels, 22 December 1986.
For the CouncilThe PresidentG. SHAW
(1)OJ N° C 114, 15. 5. 1986, p. 8.
(2)Opinion delivered on 12 December 1986 (not yet published in the Official Journal).
(3)OJ N° C 328, 22. 12. 1986, p. 1.
(4)OJ N° C 115, 30. 4. 1984, p. 1.
