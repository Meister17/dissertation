Action brought on 28 June 2006 — BA.LA. di Lanciotti V. & C. S.A.S. and Others v Commission
Parties
Applicants: BA.LA. di Lanciotti V. & C. S.A.S. and Others (represented by: P.M. Tabellini, G. Celona, E. Bidoggia and E.M. Tabellini)
Defendant: Commission of the European Communities
Form of order sought by the applicants
- that the Court of First Instance should declare admissible the action with regard to all the applicants
- that, on the merits, the Court should annul Article 1(1) and (2)(b) of and recitals 28 to 31 and 250 to 252 in the preamble to Commission Regulation (EC) No 553/2006 of 23 March 2006 imposing a provisional anti-dumping duty on imports of certain footwear with uppers of leather originating in the People's Republic of China and Vietnam, and any preliminary or connected measure, in so far as they define children's footwear and exclude it from being subject to the anti-dumping duty
- that the Court should order the defendant to pay all the applicants' costs
Pleas in law and main arguments
The applicants in this case are all manufacturers, either exclusively or chiefly, of children's footwear. They are situated so close to one another as to form a "footwear district" on the boundary between the provinces of Fermo and Macerata.
In support of their claims, the applicants plead:
- misuse of powers, in that the defendant had recourse to a procedure introduced and strictly regulated by a Basic Regulation implementing an international agreement, in order to pursue vaguely social ends (recitals 250 to 252) that call for the exclusion from the notion of Community industry of a large proportion of products which are not merely similar but actually identical;
- manifest errors of fact, in relation to the identification of such footwear with insoles of less than 24 centimetres with children's footwear, to the physical and technical features of children's footwear, and also the description of the sector concerned, to the alleged financial cost to families and to the real damage suffered by the children's footwear industry;
- infringement of Council Decision 87/369 of 7 April 1987 concerning the conclusion of the International Convention on the Harmonized Commodity Description and Coding System in so far as the contested regulation uses the measurement of the insole as a ground for distinguishing between one kind of footwear and another.
Finally, the applicants allege breach of the principle of equal treatment and non-discrimination.
--------------------------------------------------
