Commission Regulation (EC) No 546/2003
of 27 March 2003
on certain notifications regarding the application of Council Regulations (EEC) No 2771/75, (EEC) No 2777/75 and (EEC) No 2783/75 in the eggs and poultrymeat sectors
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2771/75 of 29 October 1975 on the common organisation of the market in eggs(1), as last amended by Commission Regulation (EC) No 493/2002(2), and in particular Article 15 thereof,
Having regard to Council Regulation (EEC) No 2777/75 of 29 October 1975 on the common organisation of the market in poultrymeat(3), as last amended by Regulation (EC) No 493/2002, and in particular Article 15 thereof,
Having regard to Council Regulation (EEC) No 2783/75 of 29 October 1975 on the common system of trade for ovalbumin and lactalbumin(4), as last amended by Commission Regulation (EC) No 2916/95(5), and in particular Article 10 thereof,
Whereas:
(1) Commission Regulation (EC) No 572/1999 of 16 March 1999 concerning certain reciprocal communications between the Member States and the Commission relating to eggs and poultrymeat and repealing Regulation (EEC) No 1527/73(6) introduced a system whereby the Member States and the Commission notify each other of the prices on the eggs and poultrymeat markets, with a view to their sound management.
(2) Experience indicates a need to make some improvements to the system. Regulation (EC) No 572/1999 must therefore be replaced.
(3) The weekly prices should be notified to the Commission by an electronic data transmission system acceptable to it and the prices should be available for consultation by electronic means.
(4) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Eggs and Poultrymeat,
HAS ADOPTED THIS REGULATION:
Article 1
1. No later than 12.00 each Thursday, each Member State shall electronically notify the Commission of:
(a) the selling price in packing stations for eggs in class A from caged hens, being the average of categories L and M;
(b) the selling price in slaughter plants or the wholesale prices recorded on the representative markets for whole class A chickens known as "65 % chickens", or for another whole chicken presentation if it is more representative.
2. The prices referred to in paragraph 1 shall be the average prices in the week preceding the week of notification. They shall exclude VAT and be expressed in national currency per 100 kg.
3. Member States shall adopt no later than 1 May 2003 an electronic data transmission system acceptable to the Commission.
Article 2
At least once a month at Management Committee meetings, the Commission shall provide a summary of the prices notified under Article 1 and shall make them available to the Member States on its website.
Article 3
Regulation (EC) No 572/1999 is hereby repealed.
Article 4
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 27 March 2003.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 282, 1.11.1975, p. 49.
(2) OJ L 77, 20.3.2002, p. 7.
(3) OJ L 282, 1.11.1975, p. 77.
(4) OJ L 282, 1.11.1975, p. 104.
(5) OJ L 305, 19.12.1995, p. 49.
(6) OJ L 70, 17.3.1999, p. 16.
