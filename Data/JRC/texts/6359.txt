Decision of the EEA Joint Committee No 44/2005
of 29 April 2005
amending Annex I (Veterinary and phytosanitary matters) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex I to the Agreement was amended by Decision of the EEA Joint Committee No 43/2005 of 11 March 2005 [1].
(2) Commission Regulation (EC) No 136/2004 of 22 January 2004 laying down procedures for veterinary checks at Community border inspection posts on products imported from third countries [2] is to be incorporated into the Agreement.
(3) Commission Regulation (EC) No 282/2004 of 18 February 2004 introducing a document for the declaration of, and veterinary checks on, animals from third countries entering the Community [3] is to be incorporated into the Agreement.
(4) Commission Decision 2004/253/EC of 10 March 2004 laying down the transitional measures to be applied by Hungary with regard to veterinary checks on live animals entering Hungary from Romania [4] is to be incorporated into the Agreement.
(5) Commission Decision 2004/273/EC of 18 March 2004 adapting Decision 2001/881/EC as regards additions and deletions to the list of border inspection posts in view of the accession of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia [5] is to be incorporated into the Agreement.
(6) Commission Regulation (EC) No 585/2004 of 26 March 2004 amending Regulation (EC) No 282/2004 introducing a document for the declaration of, and veterinary checks on, animals from third countries entering the Community [6] is to be incorporated into the Agreement.
(7) Commission Regulation (EC) No 599/2004 of 30 March 2004 concerning the adoption of a harmonised model certificate and inspection report linked to intra-Community trade in animals and products of animal origin [7] is to be incorporated into the Agreement.
(8) Commission Decision 2004/292/EC of 30 March 2004 on the introduction of the Traces system and amending Decision 92/486/EEC [8] is to be incorporated into the Agreement.
(9) Commission Decision 2004/408/EC of 26 April 2004 amending Decisions 2001/881/EC and 2002/459/EC as regards changes and further additions to the list of border inspection posts [9], as corrected by OJ L 208, 10.6.2004, p. 17, is to be incorporated into the Agreement.
(10) Commission Decision 2004/469/EC of 29 April 2004 amending Decision 2001/881/EC as regards the list of border inspection posts in view of the accession of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia [10], as corrected by OJ L 212, 12.6.2004, p. 7, is to be incorporated into the Agreement.
(11) Commission Decision 2004/477/EC of 29 April 2004 adapting Decision 2002/459/EC as regards additions to the list of units in the Traces computer network as a result of the accession of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia [11], as corrected by OJ L 212, 12.6.2004, p. 53, is to be incorporated into the Agreement.
(12) This Decision is not to apply to Liechtenstein,
HAS DECIDED AS FOLLOWS:
Article 1
Part 1.2 of Chapter I of Annex I to the Agreement shall be amended as specified in the Annex to this Decision.
Article 2
The texts of Regulations (EC) No 136/2004, (EC) No 282/2004, (EC) No 585/2004, (EC) No 599/2004 and Decisions 2004/253/EC, 2004/273/EC, 2004/292/EC, 2004/408/EC, as corrected by OJ L 208, 10.6.2004, p. 17, 2004/469/EC, as corrected by OJ L 212, 12.6.2004, p. 7, and 2004/477/EC, as corrected by OJ L 212, 12.6.2004, p. 53, in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 30 April 2005, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [12].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 29 April 2005.
For the EEA Joint Committee
The President
Richard Wright
[1] OJ L 198, 28.7.2005, p. 45.
[2] OJ L 21, 28.1.2004, p. 11.
[3] OJ L 49, 19.2.2004, p. 11.
[4] OJ L 79, 17.3.2004, p. 47.
[5] OJ L 86, 24.3.2004, p. 21.
[6] OJ L 91, 30.3.2004, p. 17.
[7] OJ L 94, 31.3.2004, p. 44.
[8] OJ L 94, 31.3.2004, p. 63.
[9] OJ L 151, 30.4.2004, p. 21.
[10] OJ L 160, 30.4.2004, p. 7.
[11] OJ L 160, 30.4.2004, p. 86.
[12] No constitutional requirements indicated.
--------------------------------------------------
ANNEX
Part 1.2 of Chapter I of Annex I to the Agreement shall be amended as specified below:
1. the following indents shall be added in point 39 (Commission Decision 2001/881/EC):
"— 32004 D 0273: Commission Decision 2004/273/EC of 18 March 2004 (OJ L 86, 24.3.2004, p. 21),
— 32004 D 0408: Commission Decision 2004/408/EC of 26 April 2004 (OJ L 151, 30.4.2004, p. 21), as corrected by OJ L 208, 10.6.2004, p. 17,
— 32004 D 0469: Commission Decision 2004/469/EC of 29 April 2004 (OJ L 160, 30.4.2004, p. 7), as corrected by OJ L 212, 12.6.2004, p. 7.";
2. the following indents shall be added in point 46 (Commission Decision 2002/459/EC):
"— 32004 D 0408: Commission Decision 2004/408/EC of 26 April 2004 (OJ L 151, 30.4.2004, p. 21), as corrected by OJ L 208, 10.6.2004, p. 17,
— 32004 D 0477: Commission Decision 2004/477/EC of 29 April 2004 (OJ L 160, 30.4.2004, p. 86), as corrected by OJ L 212, 12.6.2004, p. 53.";
3. the following points shall be inserted after point 114 (Commission Decision 2003/630/EC):
"115. 32004 R 0136: Commission Regulation (EC) No 136/2004 of 22 January 2004 laying down procedures for veterinary checks at Community border inspection posts on products imported from third countries (OJ L 21, 28.1.2004, p. 11).
This act applies also to Iceland for the areas covered by the specific acts to which reference is made in paragraph 2 of the introductory Part.
The provisions of this Regulation shall, for the purposes of the Agreement, be read with the following adaptation:
"Iceland, Norway" shall be added after "Finland" in Article 8(4)".
116. 32004 D 0253: Commission Decision 2004/253/EC of 10 March 2004 laying down the transitional measures to be applied by Hungary with regard to veterinary checks on live animals entering Hungary from Romania (OJ L 79, 17.3.2004, p. 47).
This act applies also to Iceland for the areas covered by the specific acts to which reference is made in paragraph 2 of the introductory Part.
117. 32004 R 0282: Commission Regulation (EC) No 282/2004 of 18 February 2004 introducing a document for the declaration of, and veterinary checks on, animals from third countries entering the Community (OJ L 49, 19.2.2004, p. 11), as amended by:
- 32004 R 0585: Commission Regulation (EC) No 585/2004 of 26 March 2004 (OJ L 91, 30.3.2004, p. 17).
This act applies also to Iceland for the areas covered by the specific acts to which reference is made in paragraph 2 of the introductory Part.
118. 32004 D 0292: Commission Decision 2004/292/EC of 30 March 2004 on the introduction of the Traces system and amending Decision 92/486/EEC (OJ L 94, 31.3.2004, p. 63).
This act applies also to Iceland for the areas covered by the specific acts to which reference is made in paragraph 2 of the introductory Part.
"119. 32004 R 0599: Commission Regulation (EC) No 599/2004 of 30 March 2004 concerning the adoption of a harmonised model certificate and inspection report linked to intra-Community trade in animals and products of animal origin (OJ L 94, 31.3.2004, p. 44).
This act applies also to Iceland for the areas covered by the specific acts to which reference is made in paragraph 2 of the introductory Part.";
4. the text of point 14 (Commission Decision 92/527/EEC) shall be deleted;
5. the text of point 16 (Commission Decision 93/13/EEC) shall be deleted;
6. the following indent shall be added in point 12 (Commission Decision 92/486/EEC):
"— 32004 D 0292: Commission Decision 2004/292/EC of 30 March 2004 (OJ L 94, 31.3.2004, p. 63)."
--------------------------------------------------
