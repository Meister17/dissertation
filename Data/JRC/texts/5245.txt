Notification according to Article 95(4) of the EC Treaty
Request for an authorisation to extend the application of national legislation derogating from the provisions of a Community Harmonisation Measure
(2005/C 197/04)
(Text with EEA relevance)
1. On 29 June 2005 the Kingdom of Sweden notified a request to extend the application of national legislation concerning the placing on the market of fertilizers containing cadmium. This legislation was already in force at the date of accession of the Kingdom of Sweden to the European Union and deviates from the provisions of Regulation (EC) No 2003/2003 relating to fertilizers [1].
2. Upon accession to the European Union at the beginning of 1995, the Kingdom of Sweden had legally binding limit values for the concentration of cadmium in mineral fertilizers and Article 112 and point 4 of Annex XII of the Act of Accession of the Kingdom of Sweden provide that Article 7 of Directive 76/116/EEC [2], in so far as it concerns the cadmium content of fertilizers, shall not apply to the Kingdom of Sweden before 1 January 1999. Directive 76/116/EEC was subsequently amended by Directive 98/97/EC [3] regarding the marketing in Austria, Finland and in Sweden of fertilizers containing cadmium, allowing, inter alia, the Kingdom of Sweden to prohibit the marketing on its territory of fertilizers containing cadmium at concentrations in excess of that which was fixed nationally at the date of accession. This derogation applied for the period from 1 January 1999 until 31 December 2001.
3. On 7 December 2001 the Kingdom of Sweden notified existing national legislation, deviating from the provisions of Directive 76/116/EEC on the approximation of the laws of the Member States relating to fertilizers. After careful consideration, the Commission Decision 2002/399/EC of 24 May 2002 [4] on the national provisions notified by the Kingdom of Sweden under Article 95(4) of the EC Treaty concerning the maximum admissible content of cadmium in fertilizers granted a prolongation of the derogation from Directive 76/116/EEC until 31 December 2005. This period was granted on the assumption that harmonised legislation would be in place by end 2005. Although work is in progress, legislation would not be adopted at Community level before the end of the year.
4. The national legislation prohibits on the territory of the Kingdom of Sweden the marketing and the transfer of fertilizers covered by customs tariffs numbers 25.10, 28.09, 28.35, 31.03 and 31.05 containing cadmium at concentrations in excess of 100 grams per tonne of phosphorus.
5. Article 95(4) stipulates that if, after the adoption by the Council or by the Commission of a harmonisation measure, a Member State deems it necessary to maintain national provisions on grounds of major needs referred to in Article 30, or relating to the protection of the environment or the working environment, it shall notify the Commission of these provisions as well as the grounds for maintaining them.
6. The Commission shall, within six months of the notification approve or reject the national provisions involved after having verified whether or not they are a means of arbitrary discrimination or a disguised restriction to trade between Member States and whether or not they shall constitute an obstacle to the functioning of the internal market.
7. The Swedish authority justify its request by referring to a risk assessment carried out by Sweden and other Member States. The assessments follows an agreed methodology for risk assessments based on the guidelines concerning risk assessments on community level according to the Council Regulation (EC) No 793/93/EEC [5] of 23 March 1993 on the evaluation and control of the risks of existing substance. The Swedish risk assessment is published on the internet homepage www.forum.europa.eu.int/enterprise.
The Swedish risk assessment shows that if fertilizers with a higher cadmium content than today would be allowed, this would lead to a substantial increase of the cadmium concentrations in soils. These high concentrations of cadmium in soils would lead to toxic effects on soil organisms. Unacceptable concentrations could also appear in watercourses in agricultural regions.
Furthermore, the risk assessment shows that if fertilizers with higher cadmium content than today would be allowed this would lead to a substantial increase in the dietary intake of cadmium. The safety margin between today's exposure and the WHO [6] Provisional Tolerable Weekly Intake level is extremely small. For some high-risk groups, such as women with low body iron stores, there are no safety margins at all. A high dietary intake of cadmium could therefore lead to a larger number of people who could be affected by reduced kidney functions and increased osteoporosis.
8. The Kingdom of Sweden therefore deems it necessary according to Article 95(4) of the Treaty establishing the European Community to maintain the national regulations concerning cadmium in fertilizers from the 1 January 2006 and until the ad-hoc EU legislation concerning cadmium in fertilizers enters into force.
9. Possible observations on the notification thus made by the Kingdom of Sweden submitted to the Commission later than 30 days from the date of publication of this notice may not be taken into consideration.
10. Further information regarding the request from the Kingdom of Sweden can be obtained from:
Mrs Ingrid Svedinger
Jordbruksdepartementet
Ministry of Agriculure, Food and Consumer Affairs
Livsmedels- och djurenheten
Food and Animal Division
S-103 33 Stockholm
Tel. (46-8) 405 10 00
Fax (46-8) 20 64 96
E-mail: ingrid.svedinger@agriculture.ministry.se
Contact point in the European Commission:
European Commission
Directorate-General Enterprise and Industry
Mr Philippe Brunerie
Unit G2 Chemicals
Avenue des Nerviens 105
B-1040 Brussels
Tel. (32-2) 295 21 99
Fax (32-2) 295 02 81
e-mail: Entr-Chemicals@cec.eu.int
[1] OJ L 304, 21.11.2003, p. 1.
[2] This directive is repealed and its content is integrated into Regulation (EC) No 2003/2003. Article 7 of this Directive corresponds to Article 5 of Regulation (EC) No 2003/2003.
[3] OJ L 18, 23.1.1999, p. 60.
[4] OJ L 138, 28.5.2002, p. 24.
[5] OJ L 84, 5.4.1993, p. 1.
[6] World Health Organization
--------------------------------------------------
