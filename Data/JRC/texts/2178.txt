Council Decision
of 12 July 2005
concerning the conclusion of an agreement between the European Community and the Government of Ukraine on trade in certain steel products
(2005/638/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 133 in conjunction with Article 300(2) thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) The Partnership and Cooperation Agreement between the European Communities and their Member States and Ukraine [1] entered into force on 1 March 1998.
(2) Article 22(1) of the Partnership and Cooperation Agreement provides that trade in certain steel products shall be governed by Title III, save for Article 14 thereof, and by the provisions of an Agreement.
(3) For the years 1995 to 2001, trade in certain steel products was the subject of Agreements between the Parties, and in 2002, 2003 and up to 19 November 2004 of specific arrangements. A further Agreement was concluded on 19 November 2004 covering the period to 31 December 2004. A new Agreement covering the period to 31 December 2006 has been negotiated between the Parties.
(4) The Agreement should be approved,
HAS DECIDED AS FOLLOWS:
Article 1
The Agreement between the European Community and the Government of Ukraine concerning trade in certain steel products is hereby approved on behalf of the Community.
The text of the Agreement is annexed to this Decision.
Article 2
The President of the Council is hereby authorised to designate the person(s) empowered to sign the Agreement in order to bind the Community.
Done at Brussels, 12 July 2005.
For the Council
The President
G. Brown
[1] OJ L 49, 19.2.1998, p. 3.
--------------------------------------------------
