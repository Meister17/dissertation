Judgment of the Court of First Instance of 29 September 2005 — Thommes
v Commission
(Case T-195/03) [1]
Parties
Applicant(s): Gustav Thommes (Wezembeek-Oppem, Belgium) (represented by: M. Thewes and V. Wiot, lawyers)
Defendant(s): Commission of the European Communities (represented by: J. Currall, Agent, assisted by B. Wägenbaur, lawyer)
Application for
Application for annulment of the Commission's decisions relating to the recovery of a part of the installation allowance paid to the applicant in the context of a change in his place of employment and the refusal to grant him an installation allowance in connection with a further re-assignment.
Operative part of the judgment
The Court:
1) Dismisses the action.
2) Orders each party to bear its own costs.
[1] OJ C 200, 23.8.2003.
--------------------------------------------------
