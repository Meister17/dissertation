*****
COMMISSION REGULATION (EEC) No 3305/82
of 9 December 1982
laying down detailed rules for the provision of administrative assistance in connection with the export of cheeses eligible for special treatment on import into Norway
THE COMMISSION OF THE EUROPEAN
COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 2931/79 of 20 December 1979 on the granting of assistance on export of agricultural products which may benefit from a special import treatment in a third country (1), and in particular Article 1 (2) thereof,
Whereas Norway has agreed to allow imports of certain cheeses, as well as to enlarge the import consignments for the Community for which a specific import levy is applied; whereas this measure will apply from 1 January 1983;
Whereas the Community has undertaken to grant administrative assistance to the authorities of Norway to ensure correct application of the agreement; whereas to this end the cheeses concerned should be accompanied by a certificate issued by the competent authorities in the Community;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Milk and Milk Products,
HAS ADOPTED THIS REGULATION:
Article 1
On the export to Norway of cheeses falling within heading No 04.04 of the Common Customs Tariff, produced in the Community, a certificate corresponding to the specimen set out in the Annex hereto shall be issued at the request of the person concerned.
Article 2
1. Certificates shall be printed on white paper and in English. Their size shall be 210 × 297 mm. Each certificate shall be given a serial number by the issuing agency.
The exporting Member State may require that the certificate to be used on its territory shall in addition to English be printed in one of its official languages.
2. Certificates shall be made out in one original and at least two copies. These copies shall bear the same serial number as the original. The original and copies shall be completed either in typescript or in manuscript; in the latter case, they shall be completed in block letters and in ink.
Article 3
1. The certificate and copies thereof shall be issued by the agency designated by each Member State.
2. The issuing agency shall keep one copy of the certificate. The original and the other copy shall be produced to the customs office where customs export formalities for export to Norway are completed.
3. The customs office referred to in paragraph 2 shall complete the box reserved for its use on the original and return it to the person concerned. The copy shall be kept by that customs office.
Article 4
The certificate shall be valid only after endorsement by the competent customs office. It shall cover the quantity indicated thereon. However, a quantity exceeding by not more than 5 % the quantity indicated on the certificate shall be regarded as covered by that quantity.
Article 5
Member States shall take whatever measures may be necessary for the purpose of checking the origin, type and quality of the cheeses in respect of which certificates are issued.
Article 6
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
It shall apply from 1 January 1983.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 9 December 1982.
For the Commission
Poul DALSAGER
Member of the Commission
(1) OJ No L 334, 28. 12. 1979, p. 8.
