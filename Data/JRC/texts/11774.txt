Joint declaration by the European Community, on the one hand, and the Home Rule Government of Greenland and the Government of Denmark, on the other, on partnership between the European Community and Greenland
1. PREAMBLE
The European Community, on the one hand, and the Home Rule Government of Greenland and the Government of Denmark, on the other,
- guided by the close historical, political, economic and cultural connections between Europe and Greenland,
- recalling the close and lasting links between the European Community and Greenland that were established by the Greenland Treaty in 1985, agreeing that these links should be maintained and confirming that mutual interests, notably the development needs of Greenland, will be taken into account by the European Community in its policies generally and in its relations with Greenland particularly,
- noting that the Council of the European Union agreed on 24 February 2003 that there is a need to broaden and strengthen future relations between the EU and Greenland, while taking into account the importance of fisheries and structural development problems in Greenland,
- considering that the European Community has a continuing geostrategic interest in treating Greenland, being part of a Member State, as a privileged neighbour and in contributing to Greenland's wellbeing and economic development,
- considering that Greenland is one of the overseas countries and territories associated with the European Community,
- considering that the European Community will take account of Greenland's interests in the context of the Arctic window of its northern dimension policy, mindful of the specific relevance of Greenland's fragile natural environment and of the challenges confronting its population,
- noting that, for the European Community, the maintenance of fishing activities in Greenland waters, by vessels flying the flag of a Member State, plays an essential part in the proper functioning of the common fisheries policy in the North Atlantic,
have decided to further strengthen the relationship and cooperation between the EU and Greenland, based on broadly shared interests, to the mutual benefit of their peoples, and to endow their mutual relations with a long term perspective.
2. COMMON OBJECTIVES
The European Community and Greenland state their intention to strengthen their partnership and cooperation in the following areas:
- cooperation in sustainably managing fish stocks and the marine environment as well as providing fishing opportunities for the vessels of EU Member States. This is, and will remain, an essential pillar of the partnership between the EC and Greenland and will continue to be based on the EC Greenland Fisheries Partnership Agreement and its successor Protocols,
- cooperation in the field of education and training,
- cooperation in the context of Greenland's efforts to develop its economy in a sustainable way, in particular in the fields of mineral resources, tourism and culture,
- cooperation in the context of the EC's efforts to deal with climate change and to promote sustainable development in the Arctic area,
- cooperation and joint scientific research on and practical development of alternative energy sources,
- joint research and development concerning northern maritime routes and Greenland's exclusive economic zone.
The European Community, for its part, states that:
- considering its objectives under the Greenland Treaty, which entered into force on 1 February 1985, and the close and mutually beneficial relations between Greenland and the European Union over the last 20 years,
- noting the geostrategic importance of Greenland, in particular in the context of the Arctic window of the northern dimension policy,
- considering the importance of Greenland as a responsible partner for the management and conservation of the environment and natural resources, including fish stocks,
it wishes to develop its cooperation with Greenland over the next seven years on the basis of a twofold partnership consisting of a Fisheries Partnership Agreement, taking into account the actual fishing possibilities available to the European Community in Greenland's waters, and of comprehensive cooperation in sectors other than fisheries, taking into account Greenland's status as an OCT;
it further wishes to ensure the continuation of the commitment of funds from the EU budget for cooperation with Greenland, oriented to the same level as in previous years, on the basis of this twofold partnership.
3. IMPLEMENTATION OF THE PARTNERSHIP
To achieve their common goals and develop their partnership, the European Community and Greenland will inform and consult each other in the areas covered by this partnership and any other mutually decided areas that arise in the spirit of this declaration.
They consider that:
- concerning the implementation of the part of the comprehensive partnership other than the Fisheries Partnership Agreement, Greenland's association with the European Community as an overseas country or territory represents an appropriate framework to respond to the needs of Greenland and to care for its specific needs, taking into account its very remote location, its extreme climate and its historical legacy,
- their cooperation should be developed in areas including education, food safety and the development of resources that have the potential to contribute towards the sustainable development of Greenland's economy and to remedy its structural problems,
- scientific cooperation covering new energy resources as well as climate change should be developed. This cooperation will also involve scientific research concerning Greenland's exclusive economic zone, bearing in mind future developments in maritime transport.
The European Community intends to pursue future cooperation with Greenland, in sectors other than fisheries, by means of a Council Decision, taken on the basis of Article 187 of the EC Treaty, to include funding through financial support from the budget of up to EUR 25 million per year within the financial perspective for 2007-2013.
Both sides share the conviction that their partnership will greatly benefit from the mutual knowledge and understanding acquired through the full use of all existing consultative forums and in particular through regular dialogue and consultations, as appropriate, between officials on both sides.
In this context Greenland intends to report at the end of each year on progress towards fulfilling the abovementioned objectives. Before the end of June 2010 the European Community and Greenland will undertake a comprehensive mid-term review of their partnership.
Hecho en Luxemburgo, el veintisiete de junio de dos mil seis.
V Lucemburku dne dvacátého sedmého června dva tisíce šest.
Udfærdiget i Luxembourg den syvogtyvende juni to tusind og seks.
Geschehen zu Luxemburg am siebenundzwanzigsten Juni zweitausendsechs.
Kahe tuhande kuuenda aasta juunikuu kahekümne seitsmendal päeval Luxembourgis.
Λουξεμβούργο, είκοσι επτά Ιουνίου δύο χιλιάδες έξι.
Done at Luxembourg, on the twenty-seventh day of June, in the year two thousand and six.
Fait à Luxembourg, le vingt-sept juin deux mille six.
Fatto a Lussemburgo, addì ventisette giugno duemilasei.
Luksemburgā, divtūkstoš sestā gada divdesmit septītajā jūnijā.
Priimta du tūkstančiai šeštų metų birželio dvidešimt septintą dieną Liuksemburge.
Kelt Luxembourgban, a kettőezer-hatodik év június havának huszonhetedik napján.
Magħmul fil-Lussemburgu, fis-sebgħa u għoxrin jum ta' Gunju tas-sena elfejn u sitia.
Gedaan te Luxemburg, de zevenentwintigste juni tweeduizend zes.
Sporządzono w Luksemburgu dnia dwudziestego siódmego czerwca roku dwutysięcznego szóstego.
Feito no Luxemburgo, em vinte e sete de Junho de dois mil e seis.
V Luxemburgu dňa dvadsiateho siedmeho júna dvetisícšesť.
V Luxembourgu, sedemindvajsetega junija leta dva tisoč šest.
Tehty Luxemburgissa kahdentenakymmenentenäseitsemäntenä päivänä kesäkuuta vuonna kaksituhattakuusi.
Som skedde i Luxemburg den tjugosjunde juni tjugohundrasex.
Por la Comunidad Europea
Za Evropské společenství
For Det Europæiske Fællesskab
Für die Europäische Gemeinschaft
Euroopa Ühenduse nimel
Για την Ευρωπαϊκή Κοινότητα
For the European Community
Pour la Communauté européenne
Per la Comunità europea
Eiropas Kopienas vārdā
Europos bendrijos vardu
Az Europai Közösség részéről
Għall-Komunità Ewropea
Voor de Europese Gemeenschap
W imieniu Wspólnoty Europejskiej
Pela Comunidade Europeia
Za Európske spoločenstvo
Za Evropsko skupnost
Euroopan yhteisön puolesta
På Europeiska gemenskapens vägnar
+++++ TIFF +++++
+++++ TIFF +++++
Por el Gobierno de Dinamarca
Za vládu Dánska
For den danske regering
Für die Regierung Dänemarks
Taani valitsuse ja nimel
Για την Κυβέρνηση της Δανίας
For the Government of Denmark
Pour le gouvernement du Danemark
Per il governo della Danimarca
Dānijas valdības vārdā
Danijos Vyriausybės vardu
Dánia kormánya részéről
Għall-Gvern tad-Danimarka
Voor de Regering van Denemarken
W imieniu Rządu Danii
Pelo Governo da Dinamarca
Za vládu Dánska
Za vlado Danske
Tanskan hallituksen puolesta
På Danmarks regerings vägnar
+++++ TIFF +++++
Por el Gobierno local de Groenlandia
Za místní vládu Grónska
For det grønlandske landsstyre
Für die örtliche Regierung Grönlands
Gröönimaa kohaliku valitsuse nimel
Για την Τοπική Κυβέρνηση της Γροιλανδίας
For the Home Rule Government of Greenland
Pour le gouvernement local du Groenland
Per il governo locale della Groenlandia
Grenlandes pašvaldības vārdā
Grenlandijos vietinės Vyriausybės vardu
Grönland Önkormányzata részéről
Għall-Gvern Lokali tal-Groenlandja
Voor de Plaatselijke Regering van Groenland
W imieniu Rządu Lokalnego Grenlandii
Pelo Governo local da Gronelândia
Za miestnu vládu Grónska
Za lokalno vlado Grenlandije
Grönlannin maakuntahallituksen puolesta
På Grönlands lokala regerings vagnar
+++++ TIFF +++++
--------------------------------------------------
