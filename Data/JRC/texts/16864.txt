Commission Regulation (EC) No 432/2004
of 5 March 2004
adapting for the eighth time to technical progress Council Regulation (EEC) No 3821/85 of 20 December 1985 on recording equipment in road transport
(Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 3821/85 of 20 December 1985 on recording equipment in road transport(1), as last amended by Regulation (EC) No 1882/2003 of the European Parliament and of the Council(2), and in particular Article 17 thereof,
Whereas:
(1) Annex I B to Council Regulation (EC) No 3821/85 sets out the technical specifications for the construction, testing, installation and inspection of recording equipment in road transport.
(2) Paying particular attention to the overall security of the system and to the interoperability between the recording equipment and the tachograph cards, certain technical specifications, set out in Annex 1 B to Regulation (EC) No 3821/85 should be amended.
(3) The measures provided for in this Regulation are in accordance with the opinion of the Committee established under Article 18 of Regulation (EC) No 3821/85,
HAS ADOPTED THIS REGULATION:
Article 1
Annex I B to Regulation (EEC) No 3821/85 is amended as follows:
1. in Chapter IV, paragraph 1, requirement 172, the words "ΚΑΡΤΑ ΟΔΗΟΥ" shall be replaced by "ΚΑΡΤΑ ΟΔΗΓΟΥ";
2. in Chapter IV, paragraph 5.3.9, requirement 227, the words "purpose of calibration (first installation, installation, periodic inspection)" shall be replaced by "purpose of calibration (activation, first installation, installation, periodic inspection)";
3. in Appendix 1, paragraph 2.29, the last two lines shall be replaced by the following:
">TABLE>"
4. in Appendix 1, at the end of paragraph 2.67, the following footnote shall be added:
"Footnote: An updated list of codes identifying the manufacturers will be available on the website of the European Certification Authority."
5. in Appendix 2, paragraph 3.6.3, requirement TCS_333, fifth indent, the formula "(Offset + Le &gt; EF size)" shall be replaced by "(Offset + Lc &gt; EF size)";
6. in Appendix 2, paragraph 3.6.7, requirement TCS_348, third column, the value "Ceh" shall be replaced by "C2h";
7. in Appendix 7, paragraph 2.2.2, in the fourth line of the eighth column the data "'8F' 'EA'" shall be replaced by "'EA' '8F'";
8. in Appendix 7, paragraph 2.2.2.2, requirement DDP_006, the reference to "'8F' 'EA'" shall be replaced by "'EA' '8F'";
9. in Appendix 7, paragraph 2.2.6.5, requirement DDP_033, the Length (Bytes) "(164)" shall be replaced by "(167)";
10. in Appendix 8, paragraph 8.2, requirement CPR_075:
(a) in the heading of table 40 the reference "recordDataIdentifier value >REFERENCE TO A GRAPHIC> F00B" shall be replaced by "recordDataIdentifier value >REFERENCE TO A GRAPHIC> F90B";
(b) in the third column of table 40 (Operating range) the reference to "-59 to 59 min" shall be replaced by "-59 to +59 min";
11. in Appendix 8, paragraph 8.2, requirement CPR_076, in the heading of table 41 the reference "recordDataIdentifier value >REFERENCE TO A GRAPHIC> F022" shall be replaced by "recordDataIdentifier value >REFERENCE TO A GRAPHIC> F922";
12. in Appendix 8, paragraph 8.2, requirement CPR_078, in the heading of table 42 the reference "recordDataIdentifier value >REFERENCE TO A GRAPHIC> F07E" shall be replaced by "recordDataIdentifier value >REFERENCE TO A GRAPHIC> F97E";
13. in Appendix 10, paragraph 4.2, the words "and company card" shall be inserted after the words "control card";
14. in Appendix 10, paragraph 4.2.3, the words "The following assignments" shall be replaced by "Additionally the following assignments";
15. in Appendix 10, paragraph 4.3.2, the words "GENERAL_READ: User data may be read from the TOE by any user, except cardholder identification data which may be read from control cards by VEHICLE_UNIT only." shall be replaced by "GENERAL_READ: User data may be read from the TOE by any user, except cardholder identification data which may be read from control cards and company cards by VEHICLE_UNIT only.";
16. in Appendix 11, paragraph 2.2.1, requirement CSM_003, the words "Public exponent, e, for RSA calculations will be different from 2 inn all generated RSA keys." shall be replaced by "Public exponent, e, for RSA calculations is an integer between 3 and n-1 satisfying gcd(e, lcm(p-1, q-1))=1.";
17. in Appendix 11, paragraph 3.3.1, requirement CSM_017, note 5, subparagraph 5.1, second table, in the second column the words "BCD coding" shall be replaced by "Integer";
18. in Appendix 11, 3.3.2, requirement CSM_018, the words "except for its Annex A.4," shall be inserted after the words "in accordance with ISO/IEC 9796-2,";
19. in Appendix 11, paragraph 4, requirement CSM_020, on the left side of the second diagram, in the tenth box, the word "signature" shall be replaced by "signature*".
Article 2
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 5 March 2004.
For the Commission
Loyola De Palacio
Vice-President
(1) OJ L 370, 31.12.1985, p. 8.
(2) OJ L 284, 31.10.2003, p. 1.
