Council Regulation (EC) No 640/2006
of 10 April 2006
repealing Regulations (EEC) No 3181/78 and (EEC) No 1736/79 concerning the European Monetary System
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 308 thereof,
Having regard to the proposal from the Commission,
Having regard to the Opinion of the European Parliament [1],
Having regard to the Opinion of the European Central Bank [2],
Whereas:
(1) Council Regulation (EEC) No 3181/78 of 18 December 1978 relating to the European monetary system [3] gives the European Monetary Co-operation Fund (EMCF) the power to receive reserves from Member States and to issue ECUs. The tasks of EMCF were taken over by European Monetary Institute and the EMCF was dissolved. Subsequently those tasks were taken over by the European Central Bank. Therefore, that Regulation is not relevant anymore and should be repealed.
(2) Council Regulation (EEC) No 1736/79 of 3 August 1979 on interest subsidies for certain loans granted under the European monetary system [4] provides that during a period of five years from the date of its application the Community may grant interest subsidies on certain types of loans (European Investment Bank (EIB) loans to finance investments in the less prosperous Member States, inter alia in infrastructure). This period of five years, which was not extended, has expired in 1984. Furthermore, according to Article 1 of that Regulation, a Member State had to participate in the mechanisms of the European monetary system in order to benefit from the subsidies. This condition suggests also that that Regulation is no longer applicable. Those loans granted by the EIB which benefited from the subsidies have in the meantime been repaid. Therefore, that Regulation is not relevant anymore and should be repealed,
HAS ADOPTED THIS REGULATION:
Article 1
Regulations (EEC) No 3181/78 and (EEC) No 1736/79 are hereby repealed.
Article 2
This Regulation shall enter into force on the twentieth day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Luxembourg, 10 April 2006.
For the Council
The President
U. Plassnik
[1] Opinion delivered on 14 February 2006 (not yet published in the Official Journal).
[2] OJ C 49, 28.2.2006, p. 35.
[3] OJ L 379, 30.12.1978, p. 2. Regulation as amended by Regulation (EEC) No 3066/85 (OJ L 290, 1.11.1985, p. 95).
[4] OJ L 200, 8.8.1979, p. 1. Regulation as amended by Regulation (EEC) No 2790/82 (OJ L 295, 21.10.1982, p. 2).
--------------------------------------------------
