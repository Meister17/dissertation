Council Decision
of 6 May 2003
on the designation of the European Capital of Culture 2006
(2003/399/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community,
Having regard to Decision No 1419/1999/EC of the European Parliament and of the Council of 25 May 1999 establishing a Community action for the European Capital of Culture event for the years 2005 to 2019(1), and in particular Article 2(3) thereof,
Having regard to the Selection Panel report received by the European Parliament, the Council and the Commission on 1 October 2002,
HAS DECIDED AS FOLLOWS:
Article 1
The City of Patras shall be designated as the European Capital of Culture 2006.
Article 2
The City of Patras shall take all necessary measures to ensure the effective implementation of Articles 1 and 5 of Decision No 1419/1999/EC.
Done at Brussels, 6 May 2003.
For the Council
The President
P. Efthymiou
(1) OJ L 166, 1.7.1999, p. 1.
