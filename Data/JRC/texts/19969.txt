COMMISSION DIRECTIVE 95/13/EC
of 23 May 1995
implementing Council Directive 92/75/EEC with regard to energy labelling of household electric tumble driers
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 92/75/EEC of 22 September 1992 on the indication by labelling and standard product information of the consumption of energy and other resources of household appliances (1), and in particular Articles 9 and 12 thereof,
Whereas under Directive 92/75/EEC the Commission is to adopt an implementing directive in respect of household appliances including electric tumble driers;
Whereas electricity use by tumble driers accounts for a significant part of total Community energy demand; whereas the scope for reduced energy use by these appliances is substantial;
Whereas the Community, confirming the interest of an international standardization system capable of producing standards that are actually used by all partners in international trade and of meeting the requirements of Community policy, invites the European standards organization to continue their cooperation with international standards organizations;
Whereas the European Committee for Standardization (CEN) and the European Committee for Electrotechnical Standardization (Cenelec) are the bodies recognized as competent to adopt harmonized standards in accordance with the general guidelines for cooperation between the Commission and these two bodies signed on 13 November 1984; whereas, within the meaning of this Directive, a harmonized standard is a technical specification (European standard or harmonization document) adopted by Cenelec, on the basis of a remit (mandate) from the Commission in accordance with the provisions of Council Directive 83/189/EEC of 28 March 1983 laying down a procedure for the provision of information in the field of technical standards and regulations (2), as last amended by Directive 94/10/EC of the European Parliament and the Council (3), and on the basis of those general guidelines;
Whereas the measures set out in this Directive are in accordance with the opinion of the committee set up under Article 10 of Directive 92/75/EEC,
HAS ADOPTED THIS DIRECTIVE:
Article 1
1. This Directive shall apply to electric mains operated household tumble driers. Appliances that can also use other energy sources are excluded, as are combined washer-driers.
2. The information required by this Directive shall be measured in accordance with harmonized standards, the reference numbers of which have been published in the Official Journal of the European Communities and for which Member States have published the reference numbers of the national standards transposing those harmonized standards. Throughout this Directive any provisions requiring the giving of information relating to noise shall apply only where that information is required under Article 3 of Council Directive 86/594/EEC (4). This information, where required, shall be measured in accordance with that Directive.
3. The harmonized standards referred to in paragraph 2 shall be drawn up under mandate from the Commission in accordance with Directive 83/189/EEC.
4. 'Dealer', 'supplier', 'information sheet', 'other essential resources' and 'supplementary information' shall have the meanings set out in Article 1 (4) of Directive 92/75/EEC.
Article 2
1. The technical documentation referred to in Article 2 (3) of Directive 92/75/EEC shall include:
- the name and address of the supplier,
- a general description of the appliance, sufficient for it to be uniquely identified,
- information, including drawings as relevant, on the main design features of the model and in particular items which appreciably affect its energy consumption,
- reports of relevant measurement tests carried out under test procedures of the harmonized standards referred to in Article 1 (2),
- operating instructions, if any.
2. The label referred to in Article 2 (1) of Directive 92/75/EEC shall be as specified in Annex I to this Directive. The label shall be placed on the outside of the front or top of the appliance, in such a way as to be clearly visible, and not obscured.
3. The content and format of the fiche referred to in Article 2 (1) of Directive 92/75/EEC shall be as specified in Annex II to this Directive.
4. In the circumstances covered by Article 5 of Directive 92/75/EEC, and where the offer for sale, hire, or hire purchase, is provided by means of a printed communication, such as a mail order catalogue, then that printed communication shall include all the information specified in Annex III to this Directive.
5. The energy efficiency class of an appliance, as specified in the label and the fiche, shall be as specified in Annex IV.
Article 3
Member States shall take all necessary measures to ensure that all suppliers and dealers established in their territory fulfil their obligations under this Directive.
Article 4
1. Member States shall adopt and publish the laws, regulations and administrative provisions necessary to comply with this Directive by 1 March 1996. They shall immediately inform the Commission thereof. They shall apply those provisions from 1 April 1996.
However, Member States shall allow, until 30 September 1996:
- the placing on the market, the commercialization and/or the display of products,
- the distribution of the printed communications referred to in Article 2 (4), which do not conform to this Directive.
When Member States adopt these provisions, these shall contain a reference to this Directive or shall be accompanied by such reference at the time of their official publication. The procedure for such reference shall be adopted by Member States.
2. Member States shall communicate to the Commission the text of the provisions of national law which they adopt in the field covered by this Directive.
Article 5
This Directive shall enter into force on the 20th day following its publication in the Official Journal of the European Communities.
Article 6
This Directive is addressed to the Member States.
Done at Brussels, 23 May 1995.
For the Commission
Christos PAPOUTSIS
Member of the Commission
(1) OJ No L 297, 13. 10. 1992, p. 16.
(2) OJ No L 109, 26. 4. 1983, p. 8.
(3) OJ No L 100, 19. 4. 1994, p. 30.
(4) OJ No L 334, 6. 12. 1986, p. 24.
ANNEX I
THE LABEL
Label design
1. The label shall be the appropriate language version chosen from the following illustrations:
>REFERENCE TO A FILM>
>REFERENCE TO A FILM>
>REFERENCE TO A FILM>
>REFERENCE TO A FILM>
>REFERENCE TO A FILM>
>REFERENCE TO A FILM>
>REFERENCE TO A FILM>
>REFERENCE TO A FILM>
>REFERENCE TO A FILM>
> REFERENCE TO A FILM>
>REFERENCE TO A FILM>
>REFERENCE TO A FILM>
Notes concerning the label
2. The following notes define the information to be included:
Note:
I. Supplier's name or trade mark.
II. Supplier's model identifier.
III. The energy efficiency class of an appliance shall be determined in accordance with Annex IV. This shall be placed at the same level as the relevant arrow.
IV. Without prejudice to any requirements under the EU eco-label scheme, where an appliance has been granted a 'EU eco-label' pursuant to Council Regulation (EEC) No 880/92 (1), a copy of the eco-label may be added here. The 'electric tumble drier label design guide' referred to below, explains how the eco-label mark may be included in the label.
V. Energy consumption in kWh per cycle, for 'dry cotton cycle' in accordance with test procedures of the harmonized standards referred to in Article 1 (2).
VI. Rated capacity of cotton, in kg, in accordance with the harmonized standards referred to in Article 1 (2).
VII. The type of appliance, air vented or condensing, in accordance with test procedures of the harmonized standards referred to in Article 1 (2). The arrow shall be placed at the same level as the relevant type.
VIII. Where applicable, noise measured in accordance with Council Directive 86/594/EEC (2).
Note:
The equivalent terms in other languages in those described above are given in Annex V.
Printing
3. The following defines certain aspects of the label:
>REFERENCE TO A FILM>
Colours used:
CMYB - Cyan, magenta, yellow, black.
For example: 07X0: 0 % cyan, 70 % magenta, 100 % yellow, 0 % black.
Arrows:
- A: X0X0
- B: 70X0
- C: 30X0
- D: 00X0
- E: 03X0
- F: 07X0
- G: 0XX0
Outline colour: X070
All text is in black. The background is white.
Complete printing information is contained in the 'Electric tumble drier label design guide', which is for information only, obtainable from:
The Secretary of the Committee on energy labelling and standard product information for household appliances,
Directorate-General for Energy XVII,
European Commission,
Rue de la Loi/Wetstraat 200,
B-1049 Brussels.
(1) OJ No L 99, 11. 4. 1992, p. 1.
(2) OJ No L 344, 6. 12. 1986, p. 24. The relevant noise measurement standards are EN 60704-2-4 and EN 60704-3.
ANNEX II
THE FICHE
The fiche shall contain the following information. The information may be given in the form of a table covering a number of appliances supplied by the same supplier, in which case it shall be given in the order specified, or given close to the description of the appliance:
1. Supplier's trade mark.
2. Supplier's model identifier.
3. The energy efficiency class of the model as defined in Annex IV. Expressed as 'Energy efficiency class . . . on a scale of A (more efficient) to G (less efficient). Where this information is provided in a table this may be expressed by other means provided it is clear that the scale is from A (more efficient) to G (less efficient)'.
4. Where the information is provided in a table, and where some of the appliances listed in the table have been granted a 'EU eco-label' pursuant to Regulation (EEC) No 880/92, this information may be included here. In this case the row heading shall state 'EU eco-label', and the entry shall consist of a copy of the eco-label mark. This provision is without prejudice to any requirements under the Community eco-label mark scheme.
5. Energy consumption (Annex I note V).
6. Rated capacity of cotton (Annex I note VI).
7. Water consumption in accordance with test procedues of the harmonized standards referred to in Article 1 (2), for 'dry cotton' programme cycle, if applicable.
8. Drying time in accordance with test procedures of the harmonized standards referred to in Article 1 (2) for 'dry cotton' cycle.
9. The same information as given above, under notes 5, 6, 7 and 8, but in respect of the 'iron dry cotton' and 'easy care textiles' programmes. These lines may be omitted if there is no such cycle on the machines in question.
10. Suppliers may include the information in points 5 to 8 in respect of other drying cycles.
11. The average annual consumption of energy (and water if applicable) based on the drying of 150 kg using 'dry cotton', plus 280 kg using 'iron-dry' cotton, plus 150 kg using 'easy care' textile programmes. This shall be expressed as 'estimated annual consumption for a four person household normally using a drier'.
12. The type of appliance, air vented or condensing, in accordance with test procedures of the harmonized standards referred to in Article 1 (2), (Annex I note VII).
13. Where applicable, 'noise' in accordance with Directive 86/594/EEC.
If a copy of the label, either in colour or black and white, is included in the fiche, only the further information included in the fiche needs to be included.
Note:
The equivalent terms in other languages to those described above are given in Annex V.
ANNEX III
MAIL ORDER AND OTHER DISTANCE SELLING
Mail order catalogues and other printed communications referred to in Article 2 (4) shall contain the following information, given in the order specified:
1. Energy efficiency class (Annex II point 3)
2. Energy consumption (Annex I note V)
3. Capacity (Annex I note VI)
4. Water consumption per cycle (if applicable) (Annex II point 7)
5. Estimated annual consumption per household (Annex II point 11)
6. Noise (Annex I note VIII).
Where other information contained in the fiche is provided it shall be in the form defined in Annex II and shall be included in the above table in the order defined for the fiche.
Note:
The equivalent terms in other languages to those described above are given in Annex V.
ANNEX IV
ENERGY EFFICIENCY CLASS
The energy efficiency class of an appliance shall be defined in accordance with the following tables:
>TABLE>
>TABLE>
ANNEX V
>TABLE>
