Commission Decision
of 8 September 2003
establishing additional guarantees regarding salmonella for consignments to Finland and Sweden of breeding poultry and day-old chicks for introduction into flocks of breeding poultry or flocks of productive poultry
(notified under document number C(2003) 3190)
(Text with EEA relevance)
(2003/644/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 90/539/EEC of 15 October 1990 on animal health conditions governing intra-Community trade in, and imports from third countries of, poultry and hatching eggs(1), as last amended by Regulation (EC) No 806/2003(2), and in particular Article 9a(2) thereof,
Whereas:
(1) Commission Decision 95/160/EC of 21 April 1995 establishing additional guarantees regarding salmonella for consignments to Finland and Sweden of breeding poultry and day-old chicks for introduction into flocks of breeding poultry or flocks of productive poultry(3), has been substantially amended(4). In the interests of clarity and rationality the said Decision should be codified.
(2) The Commission has approved the operational programmes submitted by Finland and Sweden regarding salmonella controls. Those programmes include specific measures for breeding poultry and for day-old chicks for introduction into flocks of breeding poultry or flocks of productive poultry.
(3) Guarantees should be established equivalent to those implemented by Finland and Sweden under their operational programmes.
(4) The detailed guarantees are to be based in particular on a microbiological examination of the poultry to be sent to Finland and Sweden.
(5) In this context different rules should be laid down for breeding poultry and day-old chicks.
(6) Rules should be established for this microbiological examination of samples by laying down the sampling method, the number of samples to be taken and the microbiological methods for examining the samples.
(7) Those guarantees should not be applicable to any flock that is subject to a programme recognised as equivalent to that implemented by Finland and Sweden.
(8) With regard to consignments originating from third countries, Finland and Sweden should apply import requirements at least as stringent as those laid down in this Decision.
(9) The methods described in this Decision take into account the opinion of the European Food Safety Authority.
(10) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
Breeding poultry to be sent to Finland and Sweden shall be subject to a microbiological test, effected by sampling in the flock of origin.
Article 2
The microbiological test referred to in Article 1 shall be carried out as laid down in Annex I.
Article 3
1. Breeding poultry to be sent to Finland and Sweden shall be accompanied by the certificate shown in Annex II.
2. The certificate provided for in paragraph 1 may:
- either be accompanied by the model 3 certificate of Annex IV to Directive 90/539/EEC,
- or be incorporated in the certificate referred to in the first indent.
Article 4
Day-old chicks to be sent to Finland or Sweden for introduction into flocks of breeding poultry or flocks of productive poultry must come from hatching eggs from breeding poultry that have been subjected to the test provided for in Article 2.
Article 5
1. Day-old chicks to be sent to Finland or Sweden for introduction into flocks of breeding poultry or flocks of productive poultry must be accompanied by a certificate as shown in Annex III.
2. The certificate provided for in paragraph 1 may:
- either be accompanied by the model 2 certificate shown in Annex IV to Directive 90/539/EEC,
- or be incorporated in the certificate referred to in the first subparagraph.
Article 6
The additional guarantees provided for in this Decision shall not be applicable to flocks subject to a programme recognised, according to the procedure laid down in Article 32 of Directive 90/539/EEC, as equivalent to that implemented by Finland and Sweden.
Article 7
Decision 95/160/EC is repealed.
References to the repealed Decision shall be construed as references to this Decision and shall be read in accordance with the correlation table in Annex V.
Article 8
This Decision is addressed to the Member States.
Done at Brussels, 8 September 2003.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 303, 31.10.1990, p. 6.
(2) OJ L 122, 16.5.2003, p. 1.
(3) OJ L 105, 9.5.1995, p. 40.
(4) See Annex IV.
ANNEX I
1. General rules
The flock of origin must be isolated for 15 days.
The microbiological test must cover all the salmonella serotypes.
2. Sampling method and number of samples to be taken
The sampling method and the number of samples to be taken must be as laid down in Annex III, Section I, point II A(2)(b) and (c) and point II B of Council Directive 92/117/EEC(1).
3. Microbiological methods for examination of the samples
- Microbiological testing of the samples for salmonella should be carried out to the standard of the International Organisation for Standardisation ISO 6579:1993 or revised editions, or by the method described by the Nordic Committee on Food Analysis (NMKL method No 71, fourth edition, 1991) or revised editions.
- Where the results of analysis are contested between Member States the standard of the International Organisation for Standardisation ISO 6579:1993 or revised editions should be regarded as the reference method.
(1) OJ L 62, 15.3.1993, p. 38.
ANNEX II
>PIC FILE= "L_2003228EN.003202.TIF">
ANNEX III
>PIC FILE= "L_2003228EN.003302.TIF">
ANNEX IV
Repealed Decision with its amendment
>TABLE>
ANNEX V
CORRELATION TABLE
>TABLE>
