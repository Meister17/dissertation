Council Decision
of 28 November 2006
concerning the conclusion of the Agreement in the form of an Exchange of Letters between the European Community and the Argentine Republic relating to the modification of concessions in the schedules of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic in the course of accession to the European Community
(2006/930/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 133, in conjunction with the first sentence of the first subparagraph of Article 300(2) thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) On 22 March 2004 the Council authorised the Commission to open negotiations with certain other Members of the WTO under Article XXIV.6 of the GATT 1994, in the course of the accessions to the European Union of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic.
(2) Negotiations have been conducted by the Commission in consultation with the Committee established by Article 133 of the Treaty and within the framework of the negotiating directives issued by the Council.
(3) The Commission has finalised negotiations for an Agreement in the form of an Exchange of Letters between the European Community and the Argentine Republic. The Agreement should be approved,
HAS DECIDED AS FOLLOWS:
Article 1
The Agreement in the form of an Exchange of Letters between the European Community and the Argentine Republic relating to the modification of concessions in the schedules of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic in the course of accession to the European Community, is hereby approved on behalf of the Community.
The text of the Agreement in the form of an Exchange of Letters is attached to this Decision.
Article 2
The President of the Council is hereby authorised to designate the person(s) empowered to sign the Agreement in the form of an Exchange of Letters referred to in Article 1 in order to bind the Community.
Done at Brussels, 28 November 2006.
For the Council
The President
E. Heinäluoma
--------------------------------------------------
