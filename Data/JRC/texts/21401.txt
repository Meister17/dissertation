Commission Regulation (EC) No 1614/2002
of 6 September 2002
adjusting Council Regulation (EC, Euratom) No 58/97 to economic and technical developments and amending Commission Regulations (EC) No 2700/98, (EC) No 2701/98 and (EC) No 2702/98
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC, Euratom) No 58/97 of 20 December 1996 concerning structural business statistics(1), as amended by Regulation (EC, Euratom) No 410/98(2), and in particular Article 12(i), (ii), (iii), (vii) and (viii) thereof,
Whereas:
(1) It is necessary to regularly update the lists of characteristics set out in Regulation (EC, Euratom) No 58/97 as well as the level of breakdown requested in order to meet changing needs due to economic developments.
(2) The provisions in Regulation (EC, Euratom) No 58/97 concerning the collection and statistical processing of data, the processing and the transmission of the results should be adjusted to economic and technical developments.
(3) The addition of some important characteristics and the deletion of other characteristics, which are difficult to collect and costly, should improve the balance between sectoral statistics and especially between the industry and services sectors.
(4) Furthermore, it is appropriate to add some new definitions to Commission Regulation (EC) No 2700/98 of 17 December 1998 concerning the definitions of characteristics for structural business statistics(3), and to delete or amend some of the existing definitions in that Regulation in order to make them more relevant to the activities concerned.
(5) The reduction of the level of breakdown of series by size-class provided for by Commission Regulation (EC) No 2701/98 of 17 December 1998 concerning the series of data to be produced for structural business statistics(4) should alleviate the statistical burden and improve the quality of the statistics provided.
(6) The technical format for the data concerning the coming years provided for by Commission Regulation (EC) No 2702/98 of 17 December 1998 concerning the technical format for transmission of structural business statistics(5) should be adjusted in order to facilitate such transmission.
(7) Regulations (EC) No 2700/98, (EC) No 2701/98 and (EC) No 2702/98 should, therefore, be amended accordingly.
(8) The measures provided for in this Regulation are in accordance with the opinion of the Statistical Programme Committee,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC, Euratom) No 58/97 is adjusted to economic and technical developments in accordance with Annex I to this Regulation.
Article 2
The Annex to Regulation (EC) No 2700/98 is amended in accordance with Annex II to this Regulation.
Article 3
The Annex to Regulation (EC) No 2701/98 is amended in accordance with Annex III to this Regulation.
Article 4
The Annex to Regulation (EC) No 2702/98 is amended in accordance with Annex IV to this Regulation.
Article 5
This Regulation shall enter into force on the 20th day following that of its publication in the Official Journal of the European Communities.
This Regulation shall apply for the data concerning the 2002 reference year.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 6 September 2002.
For the Commission
Pedro Solbes Mira
Member of the Commission
(1) OJ L 14, 17.1.1997, p. 1.
(2) OJ L 52, 21.2.1998, p. 1.
(3) OJ L 344, 18.12.1998, p. 49.
(4) OJ L 344, 18.12.1998, p. 81.
(5) OJ L 344, 18.12.1998, p. 102.
ANNEX I
Council Regulation (EC, Euratom) No 58/97 is amended as follows:
1. The table in Section 4, paragraph 4, of Annex 1 (Common module for annual structural statistics) is replaced by the following table:
">TABLE>"
2. Annex 2 (A detailed module for structural statistics in industry) is amended as follows:
(a) in Section 4, paragraph 3: (i) the following characteristics are deleted:
>TABLE>
(ii) the following characteristic is renamed:
>TABLE>
(b) the table in Section 4, paragraph 4, is replaced by the following table:
">TABLE>"
(c) in Section 7 (i) paragraph 1, is replaced by the following:
1. The results for the statistics, except for characteristics 18 11 0, 20 11 1, 20 11 2, 20 11 3, 20 11 4, 20 11 5, 20 11 6, 22 11 0 and 22 12 0 are to be broken down to the NACE REV.1 4-digit level (class).
The characteristics 18 11 0, 20 11 1, 20 11 2, 20 11 3, 20 11 4, 20 11 5, 20 11 6, 22 11 0 and 22 12 0 are to be broken down to the NACE REV.1 3-digit level (group).
(ii) paragraph 3 is deleted.
3. The table in Section 4, paragraph 3, of Annex 3 (A detailed module for structural statistics on distributive trades) is replaced by the following table:
">TABLE>"
4. Annex 4 (A detailed module for structural statistics in construction) is amended as follows:
(a) in Section 4, paragraph 3: (i) the following characteristics are deleted:
>TABLE>
(ii) the following characteristic is renamed:
>TABLE>
(b) the table in Section 4, paragraph 4, is replaced by the following table:
">TABLE>"
(c) in Section 7, (i) paragraph 1 is replaced by the following:
1. The results for the statistics, except for characteristics 18 11 0, 20 11 1, 20 11 2, 20 11 3, 20 11 4, 20 11 5, 20 11 6, 22 11 0, 22 12 0, 15 42 0, 15 44 1 and 15 44 2, are to be broken down to NACE REV. 1 4-digit level (class).
The results for the characteristics 18 11 0, 20 11 1, 20 11 2, 20 11 3, 20 11 4, 20 11 5, 20 11 6, 15 42 0, 15 44 1 and 15 44 2 are to be broken down to NACE REV.1 3-digit level (group).
The results for characteristics 22 11 0 and 22 12 0 are to be broken down to NACE REV.1 2-digit level (division).
(ii) paragraph 3 is deleted.
ANNEX II
Commission Regulation (EC) No 2700/98 is amended as follows:
1. The following definitions are added:
Code: 20 11 1
Title: Purchases of solid fuels (in value)
Purchases of solid fuels during the reference period should be included in this variable only if they are purchased to be used as fuel. Solid fuels purchased as raw material or for resale without transformation should be excluded.
Solid fuels consist of coking coal, steam coal (other bituminous coal and anthracite), sub-bituminous coal, coke oven coke, gas-works coke, brown coal coke, tar, coal patent fuels and other solid fuels.
Link to company accounts
Purchases of individual energy products cannot be isolated in company accounts. They are part of raw materials and consumables.
Link to other variables
Part of purchases of energy products (20 11 0)
Code: 20 11 2
Title: Purchases of petroleum products (in value)
Purchases of petroleum products during the reference period should be included in this variable only if they are purchased to be used as fuel. Petroleum products purchased as raw material or for resale without transformation should be excluded.
Petroleum products include the following products:
Motor gasoline (leaded and unleaded),
Transport diesel,
Heating and other gasoil,
Fuel oil (with high or low sulphur content),
Liquified petroleum gas (LPG),
Other petroleum products such as aviation gasoline, gasoline type jet fuel, kerosene type jet fuel, others.
Link to company accounts
Purchases of individual energy products cannot be isolated in company accounts. They are part of raw materials and consumables.
Link to other variables
Part of purchases of energy products (20 11 0)
Code: 20 113
Title: Purchases of natural and derived gas (in value)
Purchases of natural and derived gas during the reference period should be included in this variable only if they are purchased to be used as fuel. Natural and derived gas purchased as raw material or for resale without transformation should be excluded.
Natural gas is a methane-rich combustible gas coming from natural fields. Derived gases consist of coke-oven gas (= gas recovered as a by-product of coke ovens), blast furnace gas (gas recovered as a by-product of blast furnaces) and gasworks gas (= gas obtained by carbonisation, cracking, reforming, gasification or simple mixing of gas and/or air in gasworks) and oxygen steel furnace gas (gas recovered as a by-product of the production of steel in an oxygen furnace).
Link to company accounts
Purchases of individual energy products cannot be isolated in company accounts. They are part of raw materials and consumables.
Link to other variables
Part of purchases of energy products (20 11 0)
Code: 20 11 4
Title: Purchases of renewable energy sources (in value)
Purchases of renewable energy sources during the reference period should be included in this variable only if these are purchased to be used as fuel. Renewable energy sources purchased as raw material or for resale without transformation should be excluded.
Renewable energy sources include biomass, biomass waste or other renewable energy sources.
Link to company accounts
Purchases of individual energy products cannot be isolated in company accounts. They are part of raw materials and consumables.
Link to other variables
Part of purchases of energy products (20 11 0)
Code: 20 11 5
Title: Purchases of heat (in value)
Heat is produced by heating plants using fossil fuels, biomass, wastes or by Combined Heat and Power plants (CHP) or from geothermal fields.
Link to company accounts
Purchases of individual energy products cannot be isolated in company accounts. They are part of raw materials and consumables.
Link to other variables
Part of purchases of energy products (20 11 0)
Code: 20 11 6
Title: Purchases of electricity (in value)
Electricity is a secondary energy source obtained from fossil fuels, nuclear power, biomass, wastes and other renewable energy sources (such as hydropower, wind, solar or geothermal energy sources).
Link to company accounts
Purchases of individual energy products cannot be isolated in company accounts. They are part of raw materials and consumables.
Link to other variables
Part of purchases of energy products (20 11 0)
2. The following definitions are deleted:
>TABLE>
3. The following definition is amended:
Code: 18 11 0
Title: Turnover from the principal activity at the NACE Rev. 1 three-digit level.
Definition
The part of turnover derived from the principal activity of the unit. The principal activity of a unit is determined according to the rules laid down in Council Regulation (EEC) No 696/93 of 15 March 1993 on the statistical units for the observation and analysis of the production system in the Community(1).
Turnover derived from the sale of goods and services which have been subject to a subcontracting relationship is included. Turnover derived from the resale of goods and services purchased for resale in the same condition is excluded.
Link to company accounts
Turnover from the principal activity at the NACE Rev. 1 three-digit level cannot be isolated in company accounts. It is part of net turnover.
Link to other variables
Part of turnover (12 11 0).
(1) OJ L 76, 30.3.1993, p. 1.
ANNEX III
Commission Regulation (EC) No 2701/98 is amended as follows:
1. The series of data for industry, construction, services and trade are amended as follows:
The table for the series 1A is replaced by the following table:
"Series 1A
>TABLE>"
2. The table for the series 1B is replaced by the following table:
"Series 1B
>TABLE>"
3. The series of data for industry are amended as follows:
(a) The series 2C Annual enterprise statistics by type of ownershipis deleted from the summary table for the series of data on industry.
(b) The table for the series 2A is replaced by the following table:
"Series 2A
>TABLE>"
(c) The seventh row of the table for the series 2B is replaced by the following row:
>TABLE>
(d) The table for the series 2C is deleted.
(e) The seventh row of the table for the series 2D is replaced by the following row:
>TABLE>
(f) The seventh row of the table for the series 2K is replaced by the following row:
>TABLE>
(g) The table for the series 2L is replaced by the following table:
"Series 2L
>TABLE>"
4. The series of data for Distributive trades are amended as follows:
(a) The table for the series 3B is replaced by the following table:
"Series 3B
>TABLE>"
(b) The seventh row of the table for the series 3C is replaced by the following row:
>TABLE>
5. The series of data for Construction are amended as follows
(a) The series 4C Annual enterprise statistics by type of ownership is deleted from the summary table for the series of data on Construction.
(b) The table for the series 4A is replaced by the following table:
"Series 4A
>TABLE>"
(c) The table for the series 4C is deleted.
(d) The seventh row of the table for the series 4D is replaced by the following row:
>TABLE>
(e) The following characteristic is deleted from the fifth row of the table for the series 4H:
>TABLE>
(f) The seventh row of the table for the series 4K is replaced by the following row:
>TABLE>
(g) The table for the series 4L is replaced by the following table:
"Series 4L
>TABLE>"
ANNEX IV
Regulation (EC) No 2702/98 is amended as follows:
1. The following series are deleted from the table 3.1 The series type:
>TABLE>
2. The following size class is added to the table 3.3. Size classes:
>TABLE>
3. The table in paragraph 3.5 Form of ownership or FATS identification is replaced by the following table:
">TABLE>"
4. The table in paragraph 3.7 Variable is replaced with the following table:
"3.7. Variable
>TABLE>"
