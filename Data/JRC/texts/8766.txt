*****
COUNCIL REGULATION (EEC) No 1811/88
of 23 June 1988
concerning the application of Decision 1/88 of the EEC-EFTA Joint Committee 'Common transit' amending Appendices I, II and III to the Convention of 20 May 1987 on a common transit procedure
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Heaving regard to the Treaty establishing the European Economic Community, and in particular Article 113 thereof,
Having regard to the proposal from the Commission,
Whereas Article 15 (3) of the Convention between the European Community, the Republic of Austria, the Republic of Finland, the Republic of Iceland, the Kingdom of Norway, the Kingdom of Sweden and the Swiss Confederation on a common transit procedure (1) confers on the Joint Committee set up by that Convention the power to adopt, by decision, amendments to the Appendices to the Convention;
Whereas the Joint Committee has decided to amend and adapt certain provisions of Appendices I, II and III to the Convention;
Whereas those amendments and adaptations are the subject of Decision 1/88 of the Joint Committee; whereas it is necessary to take measures for the implementation of that Decision,
HAS ADOPTED THIS REGULATION:
Article 1
Decision 1/88 of the EEC-EFTA Joint Committee 'Common transit' amending Appendices I, II and III to the Convention on a common transit procedure shall apply in the Community.
The text of the said Decision is attached to this Regulation.
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Luxembourg, 23 June 1988.
For the Council
The President
W. von GELDERN
(1) OJ No L 226, 13. 8. 1987, p. 1.
DECISION 1/88 OF THE EEC-EFTA JOINT COMMITTEE 'COMMON TRANSIT'
of 22 April 1988
amending Appendices I, II and III to the Convention of 20 May 1987 on a common transit procedure
THE JOINT COMMITTEE,
Having regard to the Convention of 20 May 1987 on a common transit procedure, and in particular Article 15 (3) (a) thereof,
Whereas the new Article 40 (a) of the Regulation on Community transit does not apply to common transit; whereas Appendix I to the Convention should consequently be adapted;
Whereas Appendix II to the Convention contains, inter alia, detailed rules for the application of the common transit procedure;
Whereas it should be specified that the time limit prescribed by the office of departure by which the goods must be produced at the office of destination is binding on the customs authorities of countries whose territory is entered during the T 1 ot T 2 operation and shall not therefore be altered by those authorities;
Whereas is order to overcome any difficulties which might arise from the renumbering of boxes on railway documents used as customs transit documents in the context of the simplified transit procedure for the carriage of goods by rail, it has proved necessary to refer to such boxes by their heading rather than by their number;
Whereas the indication of the flat-rate guarantee voucher number as required under Appendix III to the Convention in the list of codes to be used in box 52: 'Guarantee' to indicate the type of guarantee has in certain respects proved superfluous; whereas there is therefore no need for it to be retained,
HAS DECIDED AS FOLLOWS:
Article 1
Appendix I to the Convention is amended as follows:
The text appearing after Article 39 is replaced by the following:
'Articles 40, 40a and 41
(This Appendix does not contain Articles 40, 40a and 41.)'
Article 2
Appendix II to the Convention is amended as follows:
1. In Article 1, the following is inserted after paragraph 5:
'5a. (This Article does not contain paragraph 5a.)'
2. In Article 5 (3), 'and shall be signed by the person signing that form' is deleted.
3. Article 9 (1) and (2) is replaced by the following:
'1. Where Articles 29 to 61 operate, Article 5 (2), and Articles 6 to 8 shall apply to loading lists which accompany the International Consignment Note or TR Transfer Note. The number of such lists shall be shown in the box reserved for particulars of accompanying documents of either the International Consignment Note or the TR Transfer Note, whichever is produced.
In addition, the loading list shall include the wagon number to which the International Consignment Note refers or, where appropriate, the number of the container containing the goods.
2. For transports beginning within the territory of the Contracting Parties comprising at the same time goods moving under the T 1 procedure and goods moving under the T 2 procedure, separate loading lists shall be used; in the case of goods carried in large containers under cover of TR Transfer Notes, such separate lists shall be completed for each large container which contains both categories of goods.
For transports beginning in the Community a reference to the serial numbers of the loading lists relating to the goods moving under the T 1 procedure shall be inserted in the box reserved for the description of the goods of either the International Consignment Note or TR Transfer Note, whichever is produced.
For transports beginning in an EFTA Country, a reference to the serial numbers of the loading lists relating to the goods moving under the T 2 procedure shall be inserted in the box reserved for the description of the goods of either the International Consignment Note or TR Transfer Note, whichever is produced. 4. The following is inserted after Article 9:
'Time limit for the production of goods
Article 9a
The time limit prescribed by the office of departure by which the goods must be produced at the office of destination shall be binding on the customs authorities of the countries whose territory is entered during the T 1 or T 2 operation ans shall not be altered by those authorities.'
5. The following is inserted after Article 19:
'Articles 19 a to 19c
(This Appendix does not contain Articles 19a to 19c.)'
6. Article 35 (2) and (3) is replaced by the following:
'2. Goods, the carriage of which begins in the Community, shall be deemed to be moving under the T 2 procedure. If, however, the goods are to move under the T 1 procedure, the office of departure shall indicate on sheets 1, 2 and 3 of the International Consignment Note that the goods to which that document refers are moving under the T 1 procedure; the symbol ''T 1" shall accordingly be clearly shown in the box reserved for customs. In the case of goods moving under the T 2 procedure the symbol ''T 2" need not be entered on the document.
3. Goods, the carriage of which begins in an EFTA country shall be deemed to be moving under the T 1 procedure. If, however, the goods are to move under the T 2 procedure in accordance with the provisions of Article 2 (3) (b) of the Convention, the office of departure shall indicate on sheet 3 of the International Consignment Note that the goods to which the document refers are moving under the T 2 procedure; the symbol ''T 2" shall accordingly be clearly shown in the box reserved for customs, together with the stamp of the customs office of departure and the signature of the responsible official. In the case of goods moving under the T 1 procedure the symbol ''T 1" need not be entered on the document.'
7. In Article 45, the first subparagraph of Definition No 4 'List of large containers' is supplemented by the following:
'The list shall be produced in the same number of copies as the TR Transfer Note to which it relates.'
8. In Article 61 (2), the first subparagraph is replaced by the following:
'2. In the case referred to in paragraph 1, a reference to the transit document or documents used shall be clearly entered in the box reserved for particulars of accompanying documents at the time when the International Consignment Note or the International Express Parcels Consignment Note is filled in. That reference shall specify the type, office of issue, date and registration number of each document used.'
9. Article 61 (4) is replaced by the following:
'4. Where a transit operation is effected under cover of a TR Transfer Note in accordance with Articles 44 to 58, the International Consignment Note used for the operation shall be excluded from the scope of Articles 29 to 43, 59, 60 and 61 (1) and (2). The International Consignment Note shall bear a clear reference to the TR Transfer Note in the box reserved for particulars of accompanying documents. That reference shall comprise the words ''Transfer Note" followed by the serial number.'
10. In Article 67 (1) and (3), 'copies 1, 4 and 5' is replaced by 'copies 1 and 4'.
11. In Article 85 (3), 'it shall be signed by the person who signs the T 2 L document' is deleted.
12. In Annex IX, the text alongside figure 1 is replaced by the following:
'1. The coat for arms or any other signs or letters characterizing the country'.
Article 3
Appendix III to the Convention is amended as follows:
1. In Annex IX, under the heading 'Box 52: Guarantee' in the list of codes applicable, 'guarantee voucher number' in the third column alongside the words 'For flat-rate guarantee' is deleted.
Article 4
This Decision shall enter into force on 1 July 1988.
Done at Brussels, 22 April 1988.
For the Joint Committee
The Chairman
E. R. VILAR
