COUNCIL DECISION
of 11 December 1991
designating a Community Coordinating Institute for foot-and-mouth disease vaccines and laying down its functions
(91/665/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Directive 85/511/EEC of 18 November 1985 introducing Community measures for the control of foot-and-mouth disease (1), as amended by Directive 90/423/EEC (2), and in particular Article 14 thereof,
Having regard to the proposal from the Commission,
Whereas Article 14 (1) of Directive 85/511/EEC authorizes Member States to retain reserves of antigens and to designate establishments for the packaging and storage of ready-to-use vaccines for emergency vaccination;
Whereas, pursuant to Article 14 (3) of that Directive, the Commission is required in particular to make proposals for the setting-up of at least two Community reserves of foot-and-mouth disease vaccines;
Whereas Article 14 (2) of that same Directive provides for the designation of a specialized institute responsible for carrying out checks on foot-and-mouth disease vaccines and cross-immunity checks, as well as providing for the institute's powers to be determined,
HAS ADOPTED THIS DECISION:
Article 1
Coordination of the control of foot-and-mouth disease vaccines used within the Community shall be carried out by the 'Centraal Diergeneeskundig Instituut, Lelystad, Nederland`, hereinafter referred to as the 'Community Coordinating Institute`.
Article 2
The functions of the Community Coordinating Institute shall be:
1. to coordinate the methods of control carried out by the national laboratories on foot-and-mouth disease antigens and vaccines intended for use in accordance with Articles 13 (3) and 14 (1) of Directive 85/511/EEC;
2.
to coordinate the control of foot-and-mouth disease vaccines by national laboratories in each Member State specifically by:
(a) at regular intervals or at the request of the Institute or the Commission, receiving representative samples of batches of foot-and-mouth disease vaccines intended for use in the Community, including those produced in third countries for use either in the Community or in Community-supported vaccination campaigns, and testing such vaccines for safety and potency;
(b)
carrying out comparative studies to ensure that safety and potency testing in each Member State is carried out by comparable methodology;
(c)
testing, by means of cross-immunity assays in live cattle, the efficacy of existing vaccines against important new field strains of foot-and-mouth disease virus and communicating the results of such assays without delay to the Commission and the Member States;
(d)
gathering data and information on the control procedures and the vaccine tests and the periodic transmission of such information to the Commission and the Member States;
3.
(a)
to implement the necessary arrangements for further training of experts in vaccine verification and testing with a view to harmonizing such techniques;
(b)
to organize an annual meeting where representatives of the national laboratories may review vaccine control and testing techniques and the progress of coordination;
4.
(a)
to check antigen submitted for the Community foot-and-mouth disease vaccine reserves, as set up in accordance with Decision 91/666/EEC (3), for potency, safety and compliance with veterinary technical supply conditions;
(b)
to carry out routine potency testing of antigen stored in the Community foot-and-mouth disease vaccine reserves;
(c)
to check the sterility of vaccine produced from the Community antigen reserves when needed, in accordance with the European Pharmacopoeia;
5.
(a)
in cooperation with appropriate Community experts, to assist and advise the Commission in ensuring that institutes which produce foot-and-mouth disease vaccines meet the necessary minimum safety requirements and in particular those laid down in accordance with Article 3 (2) of Council Decision 89/531/EEC of 25 September 1989 designating a reference laboratory for the identification of foot-and-mouth disease virus and determining the functions of that laboratory (1);
(b)
in cooperation with appropriate Community experts, to assist the Commission in reviewing the list of establishments and laboratories authorized to manipulate the virus in connection with vaccine production, in accordance with Article 13 (2) of Directive 85/511/EEC;
6. in cooperation with the suppliers of antigen and the reference laboratory for the identification of the foot-and-mouth disease virus, to review strains stored in the Community reserves and to assist in selecting suitable new strains to be adapted to tissue culture for future vaccine production;
7.
to store important seed virus strains other than those held in Community foot-and-mouth disease vaccine reserves;
8.
at the request of the Commission, and taking account of scientific and technological developments, to carry out research into the efficacy and innocuity of vaccines, which is necessary or useful for their monitoring, in conformity with detailed conditions to be determined under the procedure provided for in Article 16 of Directive 85/511/EEC;
9.
to collaborate with the reference laboratory for the identification of the foot-and-mouth disease virus in all relevant aspects of foot-and-mouth disease, including the publishing of an annual bulletin on the respective activities of the Community Coordinating Institute and the said laboratory and the training of veterinarians concerned in the clinical diagnosis and epidemiology of foot-and-mouth disease.
Article 3
1. The Community Coordinating Institute shall operate according to recognized conditions of strict disease security as indicated in 'Minimum standards for institutes working with foot-and-mouth disease virus in vitro and in vivo` - European Commission for the control of foot-and-mouth disease - 26th session, Rome, April 1985.
2. The Community Coordinating Institute shall formulate and recommend the disease security measures to be taken by national laboratories in accordance with the standards referred to in paragraph 1, particularly as regards monitoring of foot-and-mouth disease vaccines, except for those tasks which Article 3 (2) of Decision 89/531/EEC assigns to the reference laboratory for the identification of foot-and-mouth disease virus.
Article 4
The rules to be followed with regard to funding shall be adopted in accordance with Article 28 of Council Decision 90/424/EEC of 26 June 1990 on expenditure in the veterinary field (2), as amended by Decision 91/133/EEC (3).
Article 5
The Community Coordinating Institute shall be appointed for a period of 5 years as from 1 January 1992.
Before that period expires, the Council, acting by a qualified majority on a proposal from the Commission, shall decide whether the said period should be continued or whether this Decision should be amended.
Article 6
This Decision is addressed to the Member States.
Done at Brussels, 11 December 1991.
For the Council
The President
P. BUKMAN
(1) OJ N° L 315, 26. 11. 1985, p. 11.
(2) OJ N° L 224, 18. 8. 1990, p. 13.
(3) See page 21 of this Official Journal.
(1) OJ N° L 279, 28. 9. 1989, p. 32.
(2) OJ N° L 224, 18. 8. 1990, p. 19.
(3) OJ N° L 66, 13. 3. 1991, p. 18.
