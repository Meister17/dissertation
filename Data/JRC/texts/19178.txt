Commission Regulation (EC) No 2307/2003
of 29 December 2003
amending Regulation (EC) No 2550/2001 as regards the areas eligible for the goat premium
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 2529/2001 of 19 December 2001 on the common organisation of the market in sheepmeat and goatmeat(1), and in particular Article 4(2) thereof,
Whereas:
(1) The areas eligible for the premium for goatmeat producers are listed in Annex I to Commission Regulation (EC) No 2550/2001 of 21 December 2001 laying down detailed rules for the application of Council Regulation (EC) No 2529/2001 on the common organisation of the market in sheepmeat and goatmeat as regards premium schemes and amending Regulation (EC) No 2419/2001(2), as amended by Regulation (EC) No 623/2002(3).
(2) A further examination has shown that the list of geographical areas should be updated. Following an analysis of the goat production system in the overseas departments, the French authorities have established that those departments meet the criteria laid down in Article 4(2) of Regulation (EC) No 2529/2001.
(3) This update does not prejudice ex post checks concerning the conditions for the grant of aid laid down in Article 4(2) of Regulation (EC) No 2529/2001.
(4) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Sheep and Goats,
HAS ADOPTED THIS REGULATION:
Article 1
Annex I to Regulation (EC) No 2550/2001 is replaced by the Annex hereto.
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Union.
It shall apply from 1 January 2004.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 29 December 2003.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 341, 22.12.2001, p. 3.
(2) OJ L 341, 22.12.2001, p. 105.
(3) OJ L 95, 12.4.2002, p. 12.
ANNEX
"ANNEX I
Areas eligible for the goat premium
1. France: Corsica, the overseas departments and all mountain areas within the meaning of Article 18 of Council Regulation (EC) No 1257/1999(1), situated outside those regions.
2. Greece: the whole country.
3. Italy: Lazio, Abruzzi, Molise, Campania, Apulia, Basilicata, Calabria, Sicily and Sardinia and all mountain areas within the meaning of Article 18 of Regulation (EC) No 1257/1999 situated outside those regions.
4. Spain: the Autonomous Communities of Andalusia, Aragon, the Balearic Islands, Castile-La Mancha, Castile-Leon, Catalonia, Extremadura, Galicia (with the exception of the Provinces of La Coruña and Lugo), Madrid, Murcia, Rioja, Valencia and the Canary Islands and all mountain areas within the meaning of Article 18 of Regulation (EC) No 1257/1999 situated outside those regions.
5. Portugal: the whole country, with the exception of the Azores.
6. Austria: all mountain areas within the meaning of Article 18 of Regulation (EC) No 1257/1999.
7. Germany: all mountain areas within the meaning of Article 18 of Regulation (EC) No 1257/1999.
(1) OJ L 160, 26.6.1999, p. 80."
