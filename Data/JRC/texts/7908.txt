COUNCIL DIRECTIVE of 26 June 1990 laying down the animal health requirements applicable to intra-Community trade in and imports of semen of domestic animals of the porcine species (90/429/EEC
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 43 thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas provisions relating to animal health problems in intra-Community trade in bovine animals and swine appear in Directive 64/432/EEC (4), as last amended by Directive 89/360/EEC (5); whereas in addition, Directive 72/462/EEC (6), as last amended by Directive 89/227/EEC (7) contains provisions relating to veterinary inspection problems encountered upon importation of bovine animals and swine from third countries;
Whereas the abovementioned provisions have ensured, with regard to intra-Community trade and imports into the Community of bovine animals and swine from third countries, that the country of provenance guarantees that animal health criteria have been fulfilled so that the risk of animal disease spreading has been virtually eliminated; whereas there is nevertheless a certain risk of the spread of such disease in the case of trade in semen;
Whereas in the context of the Community policy of harmonizing national animal health provisions governing intra-Community trade in animals and animal products, it is now necessary to create a harmonized system for intra-Community trade and imports into the Community of semen of porcine animals;
Whereas, in the context of intra-Community trade in semen, the Member State where the semen is collected should be under an obligation to ensure that such semen has been collected and processed at approved and supervised collection centres, has been obtained from animals whose
health status is such as to ensure that the risk of spread of animal disease is eliminated, has been collected, processed, stored and transported in accordance with rules which preserve its health status and is accompanied during transport to the country of destination by an animal health certificate in order to ensure that this obligation has been fulfilled;
Whereas the difference in the policies pursued within the Community with regard to vaccination against certain diseases justifies the maintenance of derogations, limited in time, authorizing the requirement by the Member States, in respect of certain diseases, of additional protection against those diseases;
Whereas for imports of semen into the Community from third countries a list of third countries should be drawn up taking into account animal health criteria; whereas independently of such a list the Member States should authorize importation of semen only from semen collection centres which reach certain standards and which are officially supervised; whereas, in addition, in respect of countries on that list, specific animal health conditions should be laid down according to circumstances; whereas, in order to verify compliance with these standards, it must be possible to carry out on-the-spot checks;
Whereas the rules and procedures for checks laid down in Council Directive 89/662/EEC of 11 December 1989 concerning veterinary checks in intra-Community trade with a view to the completion of the internal market (8) should be extended to cover this Directive;
Whereas in order to prevent the transmission of certain contagious diseases, import controls should be carried out when a consignment of semen arrives on the territory of the Community, except in the case of external transit;
Whereas a Member State should be permitted to take emergency measures in the event of an outbreak of a contagious disease in another Member State or in a third country; whereas the dangers associated with such diseases and the protective measures they necessitate should be assessed in the same way throughout the Community; whereas to that end, an emergency Community procedure under which the necessary measures must be taken should be instituted within the Standing Veterinary Committee;
Whereas the Commission should be entrusted with taking certain measures for implementing this Directive; whereas to that end, a procedure should be established for close and effective cooperation between the Commission and the Member States within the Standing Veterinary Committee;
Whereas this Directive does not affect trade in semen produced before the date on which the Member States must comply with it,
HAS ADOPTED THIS DIRECTIVE:
CHAPTER I
General provisions
Article 1
This Directive lays down the animal health conditions applicable to intra-Community trade in and imports from third countries of semen of domestic animals of the porcine species.
Article 2
For the purposes of this Directive, the definitions contained in Article 2 of Directives 64/432/EEC, 72/462/EEC, 80/407/EEC (9) and 90/425/EEC (10) shall apply as necessary.
Moreover, 'semen' means the ejaculate of a domestic animal of the porcine species, in the unaltered state or prepared or diluted.
CHAPTER II
Intra-Community trade
Article 3
Each Member State shall ensure that only semen, meeting the following general conditions, is intended for trade:
(a) it must have been collected and processed, for the purpose of artifical insemination, in a collection centre approved from the point of view of animal health for the purposes of intra-Community trade in accordance with Article 5 (1);
(b)
it must have been collected from domestic animals of the porcine species whose health status complies with Annex B;
(c)
it must have been collected, processed, stored and transported in accordance with Annexes A and C.
Article 4
1. Until 31 December 1992, Member States in which all collection centres contain only animals which have not been vaccinated against Aujeszky's disease giving a negative reaction to the serum neutralization test, or to the ELISA test for Aujeszky's disease, in accordance with the provisions of this Directive:
- may refuse admission to their territory of semen from collection centres which do not have that status,
- may not prohibit the admission of semen from boars which have been vaccinated in the collection centre with the GI deleted vaccine, provided that:
- such vaccination has only been carried out on boars that were serum-negative with regard to the virus of Anjeszky's disease,
- serological examinations carried out at the earliest three weeks after vaccination of such boars do not reveal the presence of antibodies induced by the disease virus.
In this event a sample of semen from each daily collection intended for trade may be subjected to a virus isolation test in an approved laboratory in the Member State of destination.
The provisions of this paragraph shall not come into effect until such time as the Commission, acting in accordance with Article 18, not later than 1 July 1991, has laid down the protocols for the tests to be used for these examinations following the opinion of the Scientific Veterinary Committee, in particular in connection with the frequency of the tests to be carried out in the centre, the virus isolation tests and the effectiveness and safety of the GI deleted vaccine.
2. In accordance with the procedure referred to in Article 18, it may be decided to extend the provisions of paragraph 1 to part of the territory of a Member State if all the collection centres in that part of the territory contain only animals giving a negative reaction to the serum neutralization test or the ELISA test for Aujeszky's disease.
3. The Council shall, before 31 December 1992, review this Article on the basis of a report from the Commission, accompanied by any proposals.
Article 5
1. The Member State on whose territory the semen collection centre is situated shall ensure that the approval provided for in Article 3 (a) is granted only if it meets the conditions of Annex A and satisfies the other provisions of this Directive.
The Member State shall also ensure that the official veterinarian supervises the observance of those provisions. The official veterinarian shall propose that approval be withdrawn when one or more of the provisions is no longer observed.
2. All approved semen collection centres shall be registered, each centre being given a veterinary registration number. Each Member State shall send a list of semen collection centres and their veterinary registration numbers to the other Member States and to the Commission and shall notify them of any withdrawal of approval.
3. The general rules for applying this Article shall be adopted in accordance with the procedure laid down in Article 18.
Article 6
1. Member States shall ensure that each consignment of semen is accompanied by an animal health certificate drawn up in accordance with the specimen in Annex D by an official veterinarian of the Member State of collection.
This certificate must:
(a) be drawn up in at least one of the official languages of the Member State of collection and one of those of the Member State of destination;
(b)
accompany the consignment to its destination in its original form;
(c)
be drawn up on a single sheet of paper;
(d)
be made out to a single consignee.
2. The Member State of destination may, in addition to measures provided for in Article 8 of Directive 90/425/EEC, take the necessary measures, including storage in quarantine, provided this does not affect the viability of the semen, in order to obtain definite proof in cases where semen is suspected of being infected or contamined by pathogenic organisms.
CHAPTER III
Imports from third countries
Article 7
1. A Member State may authorize importation of semen only from those third countries which appear on a list drawn up in accordance with the procedure laid down in Article 19. That list may be supplemented or amended in accordance with the procedure laid down in Article 18.
2. In deciding whether a third country may appear on the list referred to in paragraph 1, particular account shall be taken of:
(a) the state of health of the livestock, other domestic animals and wildlife in that country, with particular reference to exotic animal diseases, and of the environmental health situation in that country, which might endanger animal health in the Member States;
(b)
the regularity and rapidity of the information supplied by that country concerning the existence of contagious animal diseases in its territory transmissible by semen, in particular those diseases mentioned in lists A and B of the International Office of Epizootic Diseases;
(c)
that country's rules on animal disease prevention and control;
(d)
the structure of the veterinary services in that country and their powers;
(e)
the organization and implementation of measures to prevent and control contagious animal diseases; and
(f)
the guarantees which that country can give with regard to compliance with this Directive.
3. The list referred to in paragraph 1 and all amendments thereto shall be published in the Official Journal of the European Communities.
Article 8
1. Under the procedure laid down in Article 19, a list shall be drawn up of semen collection centres from which Member States may authorize the importation of semen originating in third countries. The list may be amended or supplemented according to the same procedure.
2. In deciding whether a semen collection centre in a third country may appear on the list referred to in paragraph 1, particular account shall be taken of the veterinary services and the supervision to which semen collection centres are subject.
3. A semen collection centre may appear on the list provided for in paragraph 1 only if:
(a) it is situated in one of the countries on the list referred to in Article 7 (1);
(b)
it fulfils the requirements of Chapters I and II of
Annex A;
(c)
it has been officially approved for exports to the Community by the veterinary services of the third country concerned;
(d)
it is placed under the supervision of a centre veterinarian of the third country concerned; and
(e)
it is subject to inspection by an official veterinarian of the third country concerned at least twice a year.
Article 9
1. Semen must come from animals which, immediately prior to collection of their semen, have remained for at least three months in the territory of a third country on the list referred to in Article 7 (1).
2. Without prejudice to Article 7 (1) and paragraph 1 of this Article, Member States shall not authorize the importation of semen from a third country on the list unless the semen complies with the animal health requirements adopted under the procedure laid down in Article 18, for imports of semen from that country.
In adopting the requirements referred to in the first subparagraph, consideration shall be given to:
(a) the health situation in the area surrounding the semen collection centre, with particular reference to the diseases appearing on list A of the International Office of Epizootic Diseases;
(b)
the state of health of the herd in the semen collection centre and testing requirements;
(c)
the state of health of the donor animal and testing requirements;
(d)
testing requirements in relation to semen.
3. The reference basis for fixing animal health conditions shall be the standards laid down in Chapter II and the corresponding Annexes. It may be decided, in accordance with the procedure laid down in Article 18, on a case-by-case basis, to waive these conditions where the third country concerned provides similar animal health guarantees, that are at least equivalent.
4. Article 4 shall apply.
Article 10
1. Member States shall authorize the importation of semen only on submission of an animal health certificate drawn up and signed by an official veterinarian of the third country of collection.
This certificate must:
(a) be drawn up in at least one of the official languages of the Member State of destination and one of those of the Member State where the import control provided for in Article 11 is carried out;
(b)
accompany the semen to its destination in its original form;
(c)
be drawn up on a single sheet of paper;
(d)
be made out to a single consignee.
2. The animal health certificate must correspond to a specimen drawn up under the procedure laid down in Article 19.
Article 11
1. Member States shall ensure that each consignment of semen entering the customs territory of the Community is subjected to control before being released for free circulation or placed under a customs procedure and shall prohibit the introduction of the semen into the Community if the import control made on arrival reveals that:
- the semen does not come from the territory of a third country on the list drawn up in accordance with
Article 7
(1),
- the semen does not come from a semen collection centre on the list provided for in Article 8 (1),
- the semen comes from the territory of a third country from which imports are prohibited in accordance with Article 15 (2),
- the animal health certificate which accompanies the semen is not in conformity with the conditions laid down in Article 10 and fixed pursuant thereto.
This paragraph shall not apply to consignments of semen which arrive in the customs territory of the Community and are placed under a customs transit procedure for consignment to a destination situated outside the said territory.
However, it shall be applicable where customs transit is waived during transport through the territory of the Community.
2. The Member State of destination may take the necessary measures, including storage in quarantine provided that this does not affect the viability of the semen, in order to obtain definite proof in cases where semen is suspected of being infected or contamined by pathogenic organisms.
3. If the admission of semen has been prohibited on any of the grounds set out in paragraphs 1 and 2 and the exporting third country does not authorize the return of the semen within 30 days in the case of deep-frozen semen, or immediately in the case of fresh semen, the competent veterinary authority of the Member State of destination may order it to be destroyed.
Article 12
Each consignment of semen authorized for admission into the Community by a Member State on the basis of the control referred to in Article 11 (1) must, when sent to the territory of another Member State, be accompanied by the original certificate or an authenticated copy thereof, suitably endorsed, in either case, by the competent authority which was responsible for the control carried out in accordance with Article 11.
Article 13
If it is decided to take destruction measures pursuant to Article 11 (3), any costs incurred shall be chargeable to the consignor, the consignee or their agent, without compensation by the State.
CHAPTER IV
Precautionary and control measures
Article 14
The rules set out in Directive 90/425/EEC shall apply in particular with regard to checks at origin, the organization and the monitoring of the checks to be carried out by the Member State of destination.
Article 15
1. The precautionary measures provided for in Article 10 of Directive 90/425/EEC shall apply to intra-Community trade.
2. Without prejudice to Articles 8, 9 and 10, if in a third country a contagious animal disease which can be carried by semen and is liable to endanger the health of the livestock in a Member State breaks out or spreads or if any other reason connected with animal health so justifies, the Member State of destination concerned shall prohibit the import of that semen, whether imported directly or indirectly through another Member State, either from the whole of the third country or from part of its territory.
Measures taken by the Member States on the basis of the first subparagraph and the repeal of such measures must be communicated immediately to the other Member States and the Commission together with the reasons for such measures.
Under the procedure laid down in Article 18, it may be decided that those measures must be amended, in particular in order to coordinate them with measures adopted by the other Member States, or that they must be repealed.
If the situation envisaged in the first subparagraph arises and if it is necessary that other Member States also apply the measures taken under that subparagraph, amended where necessary in accordance with the third subparagraph, appropriate steps shall be taken under the procedure laid down in Article 18.
Resumption of imports from the third country concerned shall be authorized under the procedure laid down in Article 18.
Article 16
1. Veterinary experts from the Commission may, in cooperation with the competent authorities of the Member States and third countries, make on-the-spot checks in so far as that is indispensable for ensuring uniform application of this Directive.
The country of collection within whose territory a check is being carried out shall give all necessary assistance to the experts in carrying out their duties. The Commission shall
inform the country of collection concerned of the results of the checks.
The country of collection concerned shall take any measures which may prove necessary to take account of the results of this check. If the country of collection does not take those measures, the Commission may, after the situation has been examined by the Standing Veterinary Committee, have recourse to the provisions of the third subparagraph of Article 6 (2) and of Article 5.
2. The general provisions for implementing this Article, especially as regards the frequency and method of carrying out the checks referred to in the first subparagraph of paragraph 1, shall be laid down under the procedure set out in Article 19.
CHAPTER V
Final provisions
Article 17
The Annexes to this Directive shall be ameneded in accordance with the procedure set out in Article 18 to adapt them to advances in technology.
Article 18
1. Where the procedure laid down in this Article is to be followed, matters shall without delay be referred by the chairman, either on his own initiative or at the request of a Member State, to the Standing Veterinary Committee (hereinafter referred to as 'the Committee') set up by Decision 68/361/EEC (11).
2. Within the Committee, the votes of the Member States shall be weighted as provided for in Article 148 (2) of the Treaty. The chairman shall not vote.
3. The representative of the Commission shall submit a draft of the measures to be adopted. The Committee shall deliever its opinion on the draft within a time limit which the chairman may lay down according to the urgency of the matter. Opinions shall be delivered by a majority of 54 votes.
4. The Commission shall adopt the measures and apply them immediately where they are in accordance with the Committee's opinion. Where they are not in accordance with the Committee's opinion or in the absence of any opinion, the Commission shall forthwith submit to the Council a proposal relating to the measures to be taken. The Council shall adopt the measures by a qualified majority.
If, on the expiry of three months from the date on which the matter was referred to it, the Council has not adopted any measures, the Commission shall adopt the proposed
measures and apply them immediately, save where the Council has decided against the said measures by a simple majority.
Article 19
1. Where the procedure laid down in this Article is to be followed, matters shall without delay be referred to the Committee by the chairman, either on his own initiative or at the request of a Member State.
2. Within the Committee, the votes of the Member States shall be weighted as provided for in Article 148 (2) of the Treaty. The chairman shall not vote.
3. The representative of the Commission shall submit to the Committee a draft of the measures to be adopted. The Committee shall deliver its opinion on such measures within two days. The opinion shall be delivered by a majority of 54 votes.
4. The Commission shall adopt the measures and apply them immediately where they are in accordance with the Committee's opinion. Where they are not in accordance with the Committee's opinion, or in the absence of any opinion, the Commission shall forthwith submit to the Council a proposal relating to the measures to be taken. The Council shall adopt the measures by a qualified majority.
If, on the expiry of 15 days from the date on which the matter was referred to it, the Council has not adopted any measures, the Commission shall adopt the proposed measures and apply them immediately, save where the Council has decided against the said measures by a simple majority.
Article 20
1. This Directive shall not be applicable to semen collected and processed in a Member State before 31 December 1991.
2. Until the date of entry into force of the decisions adopted pursuant to Article 8, 9 and 10, Member States shall not apply to imports of semen from third countries more favourable conditions than those resulting from application of Chapter II.
Article 21
Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive by 31 December 1991 at the latest. They shall forthwith inform the Commission thereof.
Article 22
This Directive is addressed to the Member States.
Done at Luxembourg, 26 June 1990.
For the Council
The President
M. O'KENNEDY
(1) OJ No C 267, 6. 10. 1983, p. 5.
(2) OJ No C 342, 19. 12. 1983, p. 11.
(3) OJ No C 140, 28. 5. 1984, p. 6.
(4) OJ No 121, 29. 7. 1964, p. 1977/64.
(5) OJ No L 153, 6. 6. 1989, p. 29.
(6) OJ No L 302, 31. 12. 1972, p. 28.
(7) OJ No L 93, 6. 4. 1989, p. 25.(8) OJ No L 395, 31. 12. 1989, p. 13.(9) OJ No L 194, 22. 7. 1988, p. 10.
(10) See page 29 of this Official Journal.(11) OJ No L 255, 18. 10. 1968, p. 23.
ANNEX A CHAPTER I Conditions for the approval of semen collection centres
Semen collection centres must:
(a) be placed under the permanent supervision of a centre veterinarian;
(b)
have at least
ii(i) animal housing including isolation facilities,
i(ii) semen collection facilities including a separate room for the cleaning and disinfection or sterilization of equipment;
(iii) a semen processing room which need not necessarily be on the same site,
(iv) a semen storage room which need not necessarily be on the same site;
(c)
be so constructed or isolated that contact with livestock outside is prevented;
(d)
be so constructed that the animal housing and the semen collecting, processing and storage facilities can be readily cleaned and disinfected;
(e)
have isolation accommodation which shall have no direct communication with the normal animal accommodation;
(f)
be so designed that the animal accommodation is physically separated from the semen processing room and both are separated from the semen storage room.
CHAPTER II Conditions relating to the supervision of semen collection centres
The collection centres must:
(a) be so supervised that they contain only male animals of the species whose semen is to be collected;
(b)
be so supervised that a record, file or computer record is kept of all porcine animals at the centre, giving details of the breed, date of birth and identification of each of the animals, and also a record, file or computer record of all checks for diseases and all vaccinations carried out, giving also information from the disease/health file of each animal;
(c)
be regularly inspected by an official veterinarian, at least twice a year, at which time checks on the conditions of approval and supervision shall be carried out;
(d)
be so supervised that the entry of unauthorized persons is prevented. Furthermore, authorized visitors must be required to comply with the conditions laid down by the centre veterinarian;
(e)
employ technically competent staff suitably trained in disinfection procedures and hygiene techniques relevant to the control of the spread of disease;
(f)
be so supervised that:
iii(i) only semen collected at an approved centre is processed and stored in approved centres, without coming into contact with any other consignment of semen,
ii(ii)
collection, processing and storage of semen takes place only on the premises set aside for the purpose and under conditions of the strictest hygiene,
i(iii)
all implements which come into contact with the semen or the donor animal during collection and processing are properly disinfected or sterilized prior to use,
i(iv)
products of animal origin used in the processing of semen - including additives or a diluent - are obtained from sources which present no animal health risk or are so treated prior to use that such risk is prevented,
ii(v)
storage flasks and transport flasks are properly disinfected or sterlized before the beginning of each filling operation,
i(vi)
the cryogenic agent used has not been previously used for other products of animal origin,
(vii)
each collection of semen, whether or not it is separated into individual doses, is clearly marked in such a way that the date of collection of the semen and the breed and identification of the donor animal, as well as the name and the registration number of the centre, preceded by the name of the country of origin, where appropriate, in the form of a code, can be readily established; the characteristics and form of this marking will be established under the procedure laid down in Article 19.
ANNEX B CHAPTER I Conditions applying to the admission of animals to approved semen collection centres
1. All boars admitted to a semen collection centre must:
(a) have been subjected to a period of isolation of at least 30 days in accommodation specifically approved for the purpose by the competent authority of the Member State, and where only boars having at least the same health status are present;
(b)
prior to their entering the isolation accommodation described in (a) have been chosen from herds or holdings:
ii(i) which are officially free of classical swine fever,
i(ii)
which are free of brucellosis,
(iii)
in which no animal vaccinated against foot-and-mouth disease has been present in the preceding 12 months,
(iv)
in which no clinical, serological or virological sign of Aujeszky's disease has been detected in the preceding 12 months,
i(v)
which are not covered by any prohibition in accordance with the requirements of Directive 64/432/EEC with regard to African swine fever, swine vesicular disease, Teschen's disease and foot-and-mouth disease.
The animals may not previously have been kept in other herds of a lower status;
(c)
before the period of isolation specified in (a), and within the previous 30 days, have been subjected to the following tests with negative results:
ii(i) a complement fixation test carried out in accordance with the provisions of Annex C of Directive 64/432/EEC in respect of brucellosis,
i(ii)
- a serum neutralization or an ELISA test using all the viral antigens in the case of non-vaccinated pigs,
- an ELISA test for GI antigens in the case of pigs vaccinated with a GI deleted vaccine,
(iii)
pending the introduction of a Community policy to combat foot-and-mouth disease, an ELISA test for the presence of foot-and-mouth disease,
(iv)
an ELISA test or a serum neutralization test for the presence of classical swine fever.
The competent authority may give authorization for the tests referred to in (c) to be carried out in the isolation accommodation, provided that the results are known before the beginning of the 30-day isolation period laid down in (d);
(d)
during the period of isolation of at least 30 days specified in (a), have been subjected to the following tests with negative results:
ii(i) a serum agglutination test complying with the procedure laid down in Annex C to Directive 64/432/EEC and showing a brucella count lower than 30 IU of agglutination per millilitre and a complement fixation test showing a brucella count lower than 20 EEC units per millilitre (20 ICFT units) in the case of animals coming from brucellosis-free herds,
i(ii)
- a serum neutralization or an ELISA test using all the viral antigens in the case of non-vaccinated pigs,
- an ELISA test for GI antigens in the case of pigs vaccinated with a GI deleted vaccine,
(iii)
until the introduction of a Community policy on combating foot-and-mouth disease, an ELISA test for the presence of foot-and-mouth disease,
(iv)
a microscopic agglutination test for the presence of leptospirosis (sero-vars pomona, grippotyphosa, tarassovi, hardjo, bratislava and ballum serum viruses), or have been treated for leptospirosis with two injections of streptomycin at an interval of 14 days (25 mg per kg of live body weight).
Without prejudice to the provisions applicable in cases where foot-and-mouth disease and swine fever is found, if any of the above tests should prove positive, the animal must be removed forthwith from the isolation accommodation. In the case of group isolation, the competent authority must take all necessary measures to allow the remaining animals to be admitted to the collection centre in accordance with this Annex.
2. All tests must be carried out in a laboratory approved by the Member State.
3. Animals may only be admitted to the semen collection centre with the express permission of the centre veterinarian. All animal movements, both in and out, must be recorded.
4. No animal admitted to the semen collection centre may show any clinical sign of disease on the day of admission; all animals must, without prejudice to paragraph 5, have come from isolation accommodation as referred to in paragraph 1 (a) which, on the day of consignment, officially fulfils the following conditions:
(a) is situated in the centre of an area of 10 kilometres radius in which there has been no case of foot-and-mouth disease or swine fever for at least 30 days;
(b)
has for at least three months been free from foot-and-mouth disease and brucellosis;
(c)
has for at least 30 days been free from Aujeszky's disease and from those porcine diseases which are compulsorily notifiable in accordance with Annex E to Directive 64/432/EEC.
5. Provided that the conditions laid down in paragraph 4 are satisfied and the routine tests referred to in Chapter II have been carried out during the previous 12 months, animals may be transferred from one approved semen collection centre to another of equal health status without isolation or testing if transfer is direct. The animal in question must not come into direct or indirect contact with cloven-hoofed animals of a lower health status and the means of transport used must have been disinfected before use. If the movement from one semen collection centre to another takes place between Member States, it must take place in accordance with Directive 64/432/EEC.
CHAPTER II Compulsory routine tests for boars kept at an approved semen collection centre
1. All boars kept at an approved semen collection centre must be subjected when leaving the centre to the following tests with negative results:
ii(i) - a serum neutralization or an ELISA test using all the viral antigens in the case of non-vaccinated pigs,
- an ELISA test for GI antigens in the case of pigs vaccinated with a GI deleted vaccine,
i(ii) until the introduction of a Community policy on combating foot-and-mouth disease, an ELISA test for the presence of foot-and-mouth disease,
(iii) a complement fixation test conducted in accordance with Annex C to Directive 64/432/EEC regarding brucellosis,
(iv) an ELISA test or serum neutralization test for the presence of classical swine fever.
In addition, boars kept more than 12 months at the collection centre must be subjected to the tests referred to in points (i) and (iii) not later than 18 months after their admission.
2. All tests must be carried out in a laboratory approved by the Member State.
3. Without prejudice to the provisions applicable in cases where foot-and-mouth disease or swine fever is found, if any of the above tests should prove positive, the animal must be isolated and the semen collected from it since the last negative test may not be the subject of intra-Community trade.
Semen collected from all other animals at the centre since the date when the positive test was carried out shall be held in separate storage and may not be the subject of intra-Community trade until the health status of the centre has been re-established.
ANNEX C Conditions which semen collected at approved centres must satisfy for the purposes of intra-Community trade 1. Semen must be obtained from animals which:
(a) show no clinical signs of disease on the day the semen is collected;
(b)
have not been vaccinated against foot-and-mouth disease;
(c)
satisfy the requirements of Annex B, Chapter I;
(d)
are not allowed to serve naturally;
(e)
are kept in semen collection centres which have been free from foot-and-mouth disease for at least three months prior to dispatching of the semen, and are situated in the centre of an area of 10 kilometres radius in which for at least 30 days there has been no case of foot-and-mouth disease; furthermore the centres must not be situated in a restricted area designated under the provisions of the Directives relating to contagious diseases of the porcine species;
(f)
have been kept in semen collection centres which, during the 30-day period immediately prior to collection, have been free from those porcine diseases which are compulsorily notifiable in accordance with Annex E to Directive 64/432/EEC and from Aujeszky's disease.
2. An effective combination of antibiotics, in particular against leptospires and mycoplasmas, must be added to the semen after final dilution. This combination must produce an effect at least equivalent to the following dilutions:
not less than: 500 IU per ml streptomycin,
500 IU per ml penicillin,
150 mg per ml lincomycin,
300 mg per ml spectinomycin.
Immediately after the addition of the antibiotics the diluted semen must be kept at a temperature of at least 15 gC for a period of not less than 45 minutes.
3. Semen for intra-Community trade must:
i(i) be stored as laid down in Chapters I and II of Annex A prior to dispatch;
(ii) be transported to the Member State of destination in flasks which have been cleaned and disinfected or sterilized before use and which have been sealed prior to dispatch from the approved storage facilities.
ANNEX D
1. Consignor (name and full address)
3. Consignee (name and full address)
Notes
(a) A separate certificate must be issued for each consignment of semen
(b)
The original of this certificate must accompany the consignment to the place of destination
6. Place of loading
8. Means of transport
9. Place and Member State of destination
11. Number and code-mark of semen containers
ANIMAL HEALTH CERTIFICATE
NoORIGINAL
2. Member State of collection
4. Competent authority
5. Competent local authority
7. Name and address of semen collection centre
10. Registration number of semen collection centre
12. Identification of semen
(a) Number of doses
(d) Identification of donor animal
(b) Date(s) of collection
(c) Breed
13. I, the undersigned official veterinarian, certify that:
(a) the semen described above was collected, processed and stored under conditions which comply with the standards laid down in Directive 90/429/EEC;
(b)
the semen described above was collected from boars:
i(i) on a collection centre which only contains animals that have not been vaccinated against Aujeszky's disease and which have reacted negatively to the serum neutralization test or to the ELISA test for Aujeszky's disease, in accordance with the provisions of Directive 90/429/EEC (¹),
or
(ii)
on a centre in which some or all the boars have been vaccinated against Aujeszky's disease using a GI deleted vaccine; such boars having been seronegative with regard to Aujeszky's disease before vaccination and subjected three weeks later to a further serological examination which did not reveal the presence of antibodies induced by the disease virus; and, in that case, the semen of each batch has been subjected to a virus isolation test for Aujeszky's disease in the laboratory .................. (²), and gave a negative reaction (¹);
(c)
the semen described above was sent to the place of loading in sealed containers under conditions which comply with the provisions of Directive No 90/429/EEC.
Done at ..........................................................................., .
Signature .
Name and qualification (in block letters) .
.
.
(¹) Delete whichever of points (i) or (ii) does not apply.
(²) Name of the laboratory specified in accordance with Article 4 (1) of Directive 90/429/EEC.
Stamp
