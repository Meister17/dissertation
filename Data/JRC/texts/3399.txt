Notification by he Republic of Hungary concerning visa reciprocity [1]
(2005/C 251/22)
With reference to Article 2 of Council Regulation (EC) No 851/2005 amending Regulation (EC) No 539/2001 listing the third countries whose nationals must be in possession of visas when crossing the external borders and those whose nationals are exempt from that requirement, the Ministry of Foreign Affairs of the Republic of Hungary hereby informs the Council of the European Union and the European Commission that as of 24 June 2005 the following countries listed in Annex II to Regulation (EC) No 539/2001 impose a visa requirement on nationals of the Republic of Hungary:
- Australia,
- Brunei Darussalam,
- Canada,
- United States of America.
The Ministry of Foreign Affairs of the Republic of Hungary asks the Commission to take the necessary steps without delay to ensure that those countries lift the visa obligation as soon as possible and, if necessary, make a proposal concerning temporary measures in accordance with the new Article 1(4)(c) of the Regulation and the statement made when the amendment was adopted.
At the Commission's request, the Ministry of Foreign Affairs of the Republic of Hungary will give detailed information concerning steps taken to date to restore visa-free travel to the above countries.
[1] This notification is published in conformity with Article 2 of Council Regulation (EC) No 851/2005 of 2 June 2005 (OJ L 141, 4.6.2005, p. 3) amending Regulation (EC) No 539/2001 of 15 March 2001 (OJ L 81, 21.3.2001, p. 1).
--------------------------------------------------
