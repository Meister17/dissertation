COMMISSION REGULATION (EC) No 896/97 of 20 May 1997 amending and rectifying Regulation (EC) No 1663/95 laying down detailed rules for the application of Regulation (EEC) No 729/70 regarding the procedure for the clearance of the accounts of the EAGGF Guarantee Section
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 729/70 of 21 April 1970 on the financing of the common agricultural policy (1), as last amended by Regulation (EC) No 1287/95 (2), and in particular Article 5 (3) thereof,
Whereas, in accordance with Article 5 (2) (b) of Regulation (EEC) No 729/70, the Commission is to clear the accounts of the paying agencies referred to in Article 4 of that Regulation before 30 April of the year following the financial year concerned;
Whereas decisions on the clearance of accounts as referred to in Article 5 (2) (b) of Regulation (EEC) No 729/70 are to relate solely to the integrality, exactitude and veracity of the accounts transmitted; whereas the definitive charging and rejection of expenditure, advances against which have been reduced or suspended, in particular pursuant to Article 13 of Council Decision 94/729/EC (3) on budgetary discipline and/or Article 4 (2) of Commission Regulation (EC) No 296/96 of 16 February 1996 on data to be forwarded by the Member States and the monthly booking of expenditure financed under the Guarantee Section of the Agricultural Guidance and Guarantee Fund (EAGGF) and repealing Regulation (EEC) No 2776/88 (4) and/or reduced pursuant to Article 4 (3) of that Regulation, shall only be decided subsequently pursuant to Article 5 (2) (c) of Regulation (EEC) No 729/70; whereas, in order to prevent expenditure which has been reduced or suspended from being financed prematurely or provisionally as a consequence of applying Article 7 of Commission Regulation (EC) No 1663/95 (5), those provisions should be adapted;
Whereas it is necessary to rectify a substantial error in the French language version of Regulation (EC) No 1663/95;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the EAGGF Committee,
HAS ADOPTED THIS REGULATION:
Article 1
Article 7 (1) of Regulation (EC) No 1663/95 is hereby replaced by the following:
'1. Accounts clearance decisions as provided for in Article 5 (2) (b) of Regulation (EEC) No 729/70 shall, without prejudice to decisions taken subsequently in accordance with paragraph 2 (c) of that Article, determine the amount of expenditure effected in each Member State during the financial year in question which shall be recognized as being chargeable to the EAGGF on the basis of the accounts referred to in Article 5 (1) (b) of that Regulation and the reductions in and suspensions of advances in respect of the financial year concerned, including reductions as referred to in the second subparagraph of Article 4 (3) of Commission Regulation (EC) No 296/96 (*).
Amounts that, in accordance with accounts clearance decisions as referred to in the first subparagraph, are recoverable from, or payable to, each Member State shall be determined by deducting advances paid during the financial year in question from expenditure recognized for that year in accordance with the first subparagraph. Such amounts shall be deducted from, or added to, advances against expenditure from the second month following that in which the accounts clearance decision is taken.
(*) OJ No L 39, 17. 2. 1996, p. 5.`
Article 2
(concerns only the French language version)
Article 3
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 20 May 1997.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ No L 94, 28. 4. 1970, p. 13.
(2) OJ No L 125, 8. 6. 1995, p. 1.
(3) OJ No L 293, 12. 11. 1994, p. 14.
(4) OJ No L 39, 17. 2. 1996, p. 5.
(5) OJ No L 158, 8. 7. 1995, p. 6.
