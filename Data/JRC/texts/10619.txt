Judgment of the Court (Third Chamber) of 11 May 2006 — Commission of the European Communities v Italian Republic
(Case C-197/03) [1]
Parties
Applicant: Commission of the European Communities (represented by: E. Traversa, Agent)
Defendan): Italian Republic (represented by: I.M. Braguglia, Agent, and by M.P. Gentili, lawyer)
Re:
Failure of a Member State to fulfil obligations — Breach of Article 10(c) of Council Directive 69/335/EEC of 17 July 1969 concerning indirect taxes on the raising of capital (OJ, English Special Edition 1969(II), p. 412) — National law introducing retrospectively flat-rate annual charges on the registration of documents other than companies instruments of incorporation and laying down a discriminatory and restrictive system for reimbursement of the annual charge on registration of companies instruments of incorporation
Operative part of the judgment
The Court:
1. Declares that, by introducing retroactive charges which do not constitute duties paid by way of permitted fees or dues where the registrations in the register of companies for which they are charged have already given rise to charges for which the retroactive charges are intended to be a substitute but which are not reimbursed to those who have paid them, or where those retroactive charges relate to years in which no registration in the register was made justifying their being levied, and by adopting provisions making repayment of a tax held to be contrary to Community law by a judgment of the Court, or whose incompatibility with Community law is apparent from such a judgment, subject to conditions relating specifically to that tax which are less favourable than those which would otherwise be applied to repayment of the tax in question, the Italian Republic has failed to fulfil its obligations under Articles 10 and 12(1)(b) of Council Directive 69/335/EEC of 17 July 1969 concerning indirect taxes on the raising of capital and under the principles identified by the Court in relation to recovery of undue payment;
2. Dismisses the action as to the remainder;
3. Orders the Italian Republic to bear three quarters of the total costs and the Commission of the European Communities to bear the other quarter.
[1] OJ C 171, 19.07.2003.
--------------------------------------------------
