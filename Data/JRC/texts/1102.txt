Council Decision
of 12 March 2001
concerning the participation of the Community in the International Lead and Zinc Study Group
(2001/221/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 133 thereof,
Having regard to the proposal of the Commission,
Whereas:
(1) The Terms of Reference of the International Lead and Zinc Study Group (ILZSG) were adopted at the Inaugural Meeting for Lead and Zinc, under the auspices of the United Nations Economic and Social Council, in New York in May 1959.
(2) The ILZSG operates independently as an autonomous United Nations-affiliated intergovernmental organisation that provides its members with:
(a) accurate and timely information on world markets of lead and zinc; and
(b) regular intergovernmental consultations on international trade in lead and zinc and any other related issue important to member countries.
(3) The work of ILZSG is carried out largely by its six committees, Standing, Statistical and Forecasting, Mine and Smelter Projects, Recycling, International Economic and Environment. In addition, an Industry Advisory Panel, consisting of senior experts from the lead and zinc industry of member countries is chaired by the Chairman of the Study Group. The Panel provides advice to Study Group members and is open for consultation.
(4) ILZSG is recognised as an international commodity body by the United Nations Common Fund for Commodities, which entitles ILZSG to apply for funding for development projects from the Common Fund.
(5) Governments and Contracting Parties of the WTO/GATT have been asked to notify the United Nations Secretary-General of their acceptance of the Terms of Reference and the Rules of Procedure in accordance with ILZSG Rule of Procedure 1.
(6) The financing of ILZSG comes from Member Governments. Contributions are calculated by dividing one half of the budget equally among member countries and the other half is allocated in proportion to the amount of total trade in lead and zinc of each country.
(7) Several Member States of the Community already participate in the work of ILZSG,
HAS ADOPTED THIS DECISION:
Article 1
The Terms of Reference and the Rules of Procedure of the International Lead and Zinc Study Group are hereby accepted by the Community.
The Community shall lodge its instrument of acceptance with the Secretary-General of the United Nations.
The texts of the Terms of Reference and the Rules of Procedure are annexed to this Decision.
Article 2
The President of the Council is hereby authorised to designate the persons empowered to lodge the instruments of acceptance on behalf of the Community.
Done at Brussels, 12 March 2001.
For the Council
The President
B. Ringholm
ANNEX I
TERMS OF REFERENCE OF THE INTERNATIONAL LEAD AND ZINC STUDY GROUP
Membership
1. Membership of the International Lead and Zinc Study Group shall be open to the Governments of States Members of the United Nations or of appropriate specialised agencies or to Contracting Parties to the General Agreement on Tariffs and Trade, which consider themselves substantially interested in the production or consumption of or trade in lead and zinc.
Functions
2. The group shall provide opportunities for appropriate intergovernmental consultations on international trade in lead and/or zinc and shall make such studies of the world situation in lead and zinc, as it sees fit, having regard especially to the desirability of providing continuous accurate information regarding the supply and demand position and of its probable development. For this purpose the group shall arrange for the collection and dissemination of statistics, making use of existing sources so far as practicable.
3. The group shall, as appropriate, consider possible solutions to any special problems or difficulties which exist or may be expected to arise in lead or zinc and are unlikely to be resolved in the ordinary development of world trade.
4. The group may report to Member Governments. Such reports may include suggestions and/or recommendations.
5. For the purpose of these terms of reference, lead and zinc shall include scraps, wastes and/or residues and such lead and zinc products as the group may determine.
Operation of study group
6. The group shall meet at times and places mutually convenient to its members.
7. The group shall adopt such rules of procedure as are considered necessary to carry out its functions.
8. The group shall make such secretariat arrangements as it may deem necessary for the proper conduct of its work.
9. The participating Governments shall contribute to the expenses of the group on a basis to be determined by it.
10. The group shall remain in existence as long as it continues, in the opinion of the participating Governments, to serve a useful purpose.
11. The group shall make such arrangements as it considers appropriate by which information may be exchanged with the interested non-participating Governments of the States referred to in paragraph 1 and with appropriate non-governmental and intergovernmental organisations. The group shall cooperate in particular with the Interim Coordinating Committee for International Commodity Arrangements which, under Resolution 557 F (XVIII) of the Economic and Social Council, has the function of coordinating the activities of study groups and councils.
(Reproduced as Document LZ/13 of 13 September 1960, from Document E/conf. 31/1 of 6 May 1959, the Report of the Inaugural Session of the International Lead and Zinc Study Group).
ANNEX II
RULES OF PROCEDURE OF THE INTERNATIONAL LEAD AND ZINC STUDY GROUP
Membership
Rule 1
Any Government referred to in paragraph 1 of the Terms of Reference which desires to become a member of the Study Group shall notify the Secretary-General in writing accordingly. The notification shall include a declaration by the Government concerned that it considers itself substantially interested in the production or consumption of or trade in lead and/or zinc and that it accepts the Terms of Reference and the Rules of Procedure.
Rule 2
A member may at any time withdraw from the Group by written notification in advance to the Secretary-General, the withdrawal taking effect on the date specified in the notification. Withdrawal shall be without prejudice to any financial obligations already incurred and shall not entitle the withdrawing Government to any rebate of its contribution for the year in which the withdrawal occurs.
Rule 3
The Secretary-General shall notify each member of the Group of any notification received in accordance with Rules 1 or 2.
Representation
Rule 4
Each member of the Group shall, if possible, designate a person resident at the seat of the Group to whom all notices and other communications regarding the work of the Group shall be addressed, but other arrangements may be made with the Secretary-General.
Rule 5
Each member of the Group shall notify the Secretary-General as soon as possible of the names of the representative, alternates and advisers designated to represent it at a session. Members may, however, designate permanent delegations to represent them at all sessions of the Group until they decide otherwise.
Rule 6
If a member and the territories for the international relations of which it is responsible form a group of which one or more units are mainly interested in the production of lead and zinc and one or more in the consumption of lead and zinc, there may, at the request of the member, be either joint representation for all such territories within the Group or separate representation for the territories interested in production and separate representation for the territories interested in consumption of lead and zinc. Where a territory or group of territories is separately represented in accordance with this rule, it shall for the purposes of these Rules be regarded as a member of the Group.
Liaison
Rule 7
The Group shall make such arrangements as it considers appropriate by which information may be exchanged with interested non-participating Governments of the State referred to in paragraph 1 of its Terms of Reference and with appropriate non-governmental and intergovernmental organisations.
The Group may invite any appropriate intergovernmental or non-governmental organisation substantially interested in lead and zinc problems to be represented at its meetings by an observer, on the understanding that such organisation will extend similar rights to the Group. Such observer may attend all meetings of the Group unless the Group decides otherwise, in respect of the whole or any part of a particular meeting or series of meetings but, unless the Group decides otherwise, may not attend meetings of the Standing Committee nor of any committee or subcommittee on which all members of the Group are not represented.
The Chairman may invite any such observer to participate in the Group's discussion of any item in which the organisation represented by the observer is substantially interested, but the observer shall not have the right to vote or to submit proposals.
The following Rules of Procedure of the Group shall apply to any such organisation, mutatis mutandis: Rules 4, 5, 13, 16, 26, 27 and 28.
Financial obligations
Rule 8
The financial year for the Group shall be from 1 January to 31 December.
Rule 9
Each member of the Group shall contribute annually to the expenses of the Group in accordance with the scale of contributions established subject to a minimum contribution, on the basis of the interest of each member in both lead and zinc. At the last scheduled session of each year the budget for the ensuing year shall be approved by the Group and the contributions for each member Government established. The Secretary-General will immediately notify each member Government of the amount of its contribution. Contributions shall be due on 1 January. Should any member country still not have paid its contribution for the previous calendar year by the time of the regular Spring meeting of the Standing Committee, it shall explain any such delay at that meeting. Any member in arrears by more than its contribution for the preceding financial year shall be deprived of its voting rights or may be suspended from membership for so long as it remains in arrears.
Rule 10
Any member joining the group in the course of a financial year shall pay such proportion of its normal annual contributions as the Group may establish. Contributions received from new members shall not affect the contributions applicable to existing members in the financial year in question.
Rule 11
Contributions of members shall be payable in the currency of the State in which the headquarters of the Group is located. Financial arrangements for the Group shall be made by the Secretary-General with the authority of the Standing Committee and such arrangements shall apply until the Group decides otherwise.
Rule 12
The adoption of a budget shall be the authority to incur the expenditures set out therein. Within the limits of the overall budget and with the approval of the Standing Committee or of an appropriately designated body or officer of the Standing Committee, any appropriation under any one heading of the budget may be applied to any other heading. Payment for the account of the Group may be made under such authority(ies) as the Standing Committee may from time to time decide.
Rule 13
Travelling and subsistence expenses of delegations of members, including those of delegations to Committees or other bodies of the Group, shall not be a charge on the funds of the Group.
Headquarters of the Group
Rule 14
The headquarters of the Group shall be in London until the Group decides otherwise. The Group shall hold its sessions at such places as it decides.
Sessions of the Group
Rule 15
Sessions of the Group, other than those decided upon at a previous session, shall be held at the request of the Standing Committee or of the Chairman of the Group or of not less than four members. Where the request is made on the grounds of urgency, the request shall include a statement of the reasons.
Rule 16
The Secretary-General shall send to the designated representative of each member of the Group a written notification of the date of each session together with a provisional agenda for that session. Such notification and provisional agenda shall be dispatched at least 35 days before the commencement of the session. If a session is convened on the grounds of urgency, the notification and provisional agenda shall be dispatched at least 15 days prior to the session and the notification shall include a statement of the reasons for convening the session.
Provisional agenda
Rule 17
The provisional agenda for each session shall be prepared by the Secretary-General in consultation with the Chairman of the Group. If a member of the Group wishes a particular matter to be discussed at a session of the Group it shall if possible notify the Secretary-General 60 days before the commencement of the session, including in that notification a written explanation. The agenda shall be finally decided at the Session of the Group.
Chairman and Vice-Chairmen
Rule 18
The Group shall have a Chairman and two Vice-Chairmen who shall be elected for a calendar year and may be re-elected. The elections for a calendar year shall be held at an appropriate meeting in the preceding calendar year but, in default of such elections, the Chairman and Vice-Chairman shall continue in that capacity until their successors have been elected and have taken office.
Rule 19
The duties of the Chairman or a Vice-Chairman acting as a Chairman, shall include the following:
(a) preside and conduct the proceedings of each session;
(b) declare the opening and closing of each session of the Group;
(c) direct discussions at meetings, ensure observance of these rules, accord the right to speak and, subject to Rule 20, decide all questions of order;
(d) put questions, announce decisions, and, if a vote is requested, call for votes and announce the result of voting.
Conduct of business
Rule 20
During the discussion of any matter, any representative may raise a point of order and may move the closure or adjournment of the debate. In each such case, the Chairman shall immediately state his ruling, which shall stand unless overruled by the meeting.
Rule 21
The required quorum for each meeting of the Group shall be a majority of its members.
Rule 22
Each meeting of the Group shall be private unless it decides otherwise.
Rule 23
In the ordinary course of business, decisions shall be taken according to the sense of the meeting and without a vote. If a vote is requested on decisions concerning the budget, any amendment of the budget and any amendment either of the Terms of Reference or of this rule, a two-thirds majority of members present and voting shall be required. The vote shall be by a show of hands, roll-call or secret ballot, in accordance with the request. Should a vote be requested on other decisions, a simple majority shall be sufficient.
Rule 24
The Chairman, or a Vice-Chairman acting in his place, shall have no vote, but may appoint another member of his delegation to vote in his place.
Rule 25
The Chairman of the Standing Committee may arrange for the Group to reach decisions on any matter by correspondence. For this purpose a communication shall be sent to members inviting them to record their votes within a specified time-limit, which shall not be less than 21 days. The communication shall state clearly the matter at issue and the proposals in respect of which Members are invited to vote for or against. At the end of the specified time-limit, the Secretary-General shall notify all members of the decision reached. If the replies of four members contain objections to the correspondence procedure, no vote shall be taken and the matter shall stand over for decision at the next session of the Group.
Official and working languages
Rule 26
English, French, Russian and Spanish shall be the official and working languages of the Group. Any representative wishing to speak in any other languages shall be responsible for interpretation into one of the working languages.
All documents for the Group shall be translated into the four working languages.
Rule 27
Minutes of meetings shall consist of a summary record of the proceedings which shall be provisional in the first case. If any delegation wishes to amend any of its statements reported in the provisional record, such amendment shall be made by notification to the Secretary-General within 21 days of the issue of that record and no other changes shall be made unless approved by the Group at its next session.
Rule 28
Information which is the property of the Group, reports of proceedings and all other documents of the Study Group and its various committees and other bodies shall be confidential until and unless the Group or the Standing Committee as appropriate, decides otherwise.
Standing Committee
Rule 29
A Standing Committee shall be established by the Group and shall consist of those members of the Group which have indicated to the Secretary-General their desire to participate in its work. Documents connected with the work of the Committee shall be circulated to a person designated by each member of the Committee.
The Standing Committee shall elect its own Chairman and Vice-Chairmen.
The Secretary-General, or an officer designated by him, shall serve as Secretary of the Committee.
The Committee, which shall meet at least twice a year, shall adopt its own Rules of Procedure.
Rule 30
The Standing Committee shall keep the lead and zinc situation currently under review and make such recommendations to the Group as it may deem advisable. It shall carry out such other tasks as may be delegated to it by the Group. In addition, it shall exercise appropriate responsibility for the work of the Secretariat, preparation of a draft budget and other financial action in accordance with Rule 12. All financial transactions on behalf of the Group shall be notified regularly to the Committee.
Other Committees
Rule 31
The Group may establish such other committees or bodies as may be appropriate on such terms and conditions as it may determine.
Secretariat
Rule 32
The Group shall have a Secretariat consisting of a Secretary-General and such staff as may be required. The Secretariat shall be appointed or provided in such manner as the Group may decide.
Rule 33
The Secretary-General, subject to such decisions regarding provision of the Secretariat as may be made by the Group, shall be responsible for the execution of all duties falling on the Secretariat, including servicing the Group and its Committees.
Amendment
Rule 34
These Rules may be amended by decision of the Group taken in accordance with Rule 23.
(Document LZ/161 of 26 September 1977, revised from Documents LZ/58 of 13 November 1964, LZ/15 of 10 October 1960, and LZ/9 of 3 August 1960).
