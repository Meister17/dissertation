Commission Decision
of 13 May 2005
on the clearance of the accounts of Member States' expenditure financed by the European Agricultural Guidance and Guarantee Fund (EAGGF), Guarantee Section, for the 2004 financial year
(notified under document number C(2005) 1443)
(2005/385/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1258/1999 of 17 May 1999 on the financing of the common agricultural policy [1], and in particular Article 7(3) thereof,
After consulting the Fund Committee,
Whereas:
(1) Under Article 7(3) of Regulation (EC) No 1258/1999, the Commission, on the basis of the annual accounts submitted by the Member States, accompanied by the information required for clearance and a certificate regarding the integrality, accuracy and veracity of the accounts transmitted and the reports established by the certification bodies, clears the accounts of the paying agencies referred to in Article 4(1) of that Regulation.
(2) Pursuant to Article 7(1) of Commission Regulation (EC) No 296/96 of 16 February 1996 on data to be transmitted by the Member States and the monthly booking of expenditure financed under the Guarantee Section of the European Agricultural Guidance and Guarantee Fund (EAGGF) [2], account is taken for the 2004 financial year of expenditure incurred by the Member States between 16 October 2003 and 15 October 2004.
(3) The time limits granted to the Member States for the submission to the Commission of the documents referred to in Article 6(1)(b) of Regulation (EC) 1258/1999 and in Article 4(1) of Commission Regulation (EC) No 1663/95 of 7 July 1995 laying down detailed rules for the application of Council Regulation (EEC) No 729/70 regarding the procedure for the clearance of accounts of the EAGGF Guarantee Section [3], have expired.
(4) The Commission has checked the information submitted and communicated to the Member States before 31 March 2005 the results of its verifications, along with the necessary amendments.
(5) Under the first subparagraph of Article 7(1) of Regulation (EC) No 1663/95, the accounts clearance decision referred to in Article 7(3) of Regulation (EC) No 1258/1999 must determine, without prejudice to decisions taken subsequently in accordance with Article 7(4) of the Regulation, the amount of expenditure effected in each Member State during the financial year in question recognised as being chargeable to the EAGGF Guarantee Section, on the basis of the accounts referred to in Article 6(1)(b) of Regulation (EC) No 1258/1999 and the reductions and suspensions of advances for the financial year concerned, including the reductions referred to in the second subparagraph of Article 4(3) of Regulation (EC) No 296/96. Under Article 154 of Council Regulation (EC, Euratom) No 1605/2002 of 25 June 2002 on the Financial Regulation applicable to the general budget of the European Communities [4], the outcome of the clearance decision, that is to say any discrepancy which may occur between the total expenditure booked to the accounts for a financial year pursuant to Article 151(1) and Article 152 and the total expenditure taken into consideration by the Commission in this Decision, is to be booked, under a single article, as additional expenditure or a reduction in expenditure.
(6) For certain paying agencies, in the light of the verifications made, the annual accounts and the accompanying documents permit the Commission to take a decision on the integrality, accuracy and veracity of the accounts submitted. Annex I lists the amounts cleared by Member State. The details of these amounts were described in the Summary Report that was presented to the Fund Committee at the same time as this Decision.
(7) In the light of the verifications made, the information submitted by certain paying agencies requires additional inquiries and their accounts cannot be cleared in this Decision. Annex II lists the paying agencies concerned.
(8) Article 4(2) of Regulation (EC) No 296/96, in liaison with Article 14 of Council Regulation (EC) No 2040/2000 of 26 September 2000 on budgetary discipline [5] lays down that advances against booking are to be reduced for expenditure effected by the Member States after the limits or deadlines laid down. However, under Article 4(3) of Regulation (EC) No 296/96, any overrun of deadlines during August, September and October is to be taken into account in the accounts clearance decision except where noted before the last decision of the financial year relating to advances. Some of the expenditure declared by certain Member States during the abovementioned period and for the measures for which the Commission did not accept any extenuating circumstances was effected after the statutory limits or deadlines laid down. This Decision should therefore lay down the relevant reductions. A decision will be taken at a later date, in accordance with Article 7(4) of Regulation (EC) No 1258/1999, definitively fixing the expenditure for which Community financing will not be granted regarding those reductions and any other expenditure which may be found to have been effected after the limits or deadlines laid down.
(9) The Commission, in accordance with Article 14 of Regulation (EC) No 2040/2000 and Article 4(2) of Regulation (EC) No 296/96, has already reduced or suspended a number of monthly advances on entry into the accounts of expenditure for the 2004 financial year and makes in this Decision the reductions laid down in Article 4(3) of Regulation (EC) No 296/96. In the light of the above, to avoid any premature or merely a temporary reimbursement of the amounts in question, they should not be recognised in this Decision, without prejudice to further examination under Article 7(4) of Regulation (EC) No 1258/1999.
(10) The second subparagraph of Article 7(1) of Regulation (EC) No 1663/95 lays down that the amounts that are recoverable from, or payable to, each Member State, in accordance with the accounts clearance decision referred to in the first subparagraph, shall be determined by deducting advances paid during the financial year in question, i.e. 2004, from expenditure recognised for that year in accordance with the first subparagraph. Such amounts are to be deducted from, or added to, advances against expenditure from the second month following that in which the accounts clearance decision is taken.
(11) In accordance with the final subparagraph of Article 7(3) of Regulation (EC) No 1258/1999 and Article 7(1) of Regulation (EC) No 1663/95, this Decision, adopted on the basis of accounting information, does not prejudice decisions taken subsequently by the Commission excluding from Community financing expenditure not effected in accordance with Community rules,
HAS ADOPTED THIS DECISION:
Article 1
With the exception of the paying agencies referred to in Article 2, the accounts of the paying agencies of the Member States concerning expenditure financed by the EAGGF Guarantee Section in respect of the 2004 financial year are hereby cleared. The amounts which are recoverable from, or payable to, each Member State under this Decision are set out in Annex I.
Article 2
For the 2004 financial year, the accounts of the Member States' paying agencies in respect of expenditure financed by the EAGGF Guarantee Section, shown in Annex II, are disjoined from this Decision and shall be the subject of a future clearance Decision.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 13 May 2005.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 160, 26.6.1999, p. 103.
[2] OJ L 39, 17.2.1996, p. 5. Regulation as last amended by Regulation (EC) No 1655/2004 (OJ L 298, 23.9.2004, p. 3).
[3] OJ L 158, 8.7.1995, p. 6. Regulation as last amended by Regulation (EC) No 465/2005 (OJ L 77, 23.3.2005, p. 6 ).
[4] OJ L 248, 16.9.2002, p. 1.
[5] OJ L 244, 29.9.2000, p. 27.
--------------------------------------------------
ANNEX I
MS | | 2004 — Expenditure for the Paying Agencies for which the accounts are | Total a + b | Reductions and suspensions for the whole financial year | Total including reductions and suspensions | Advances paid to the Member State for the financial year | Amount to be recovered from (–) or paid to (+) the Member State |
cleared | disjoined |
= expenditure declared in the annual declaration | = total of the expenditure in the monthly declarations |
a | b | c = a + b | d | e = c + d | f | g = e – f |
AT | EUR | 1141832188,85 | 0,00 | 1141832188,85 | 0,00 | 1141832188,85 | 1141832509,04 | – 320,19 |
BE | EUR | 1072926545,09 | 0,00 | 1072926545,09 | 0,00 | 1072926545,09 | 1072805591,37 | 120953,72 |
CZ | CZK | 148270977,89 | 0,00 | 148270977,89 | 0,00 | 148270977,89 | 148270977,89 | 0,00 |
DE | EUR | 6010175861,68 | 23818955,08 | 6033994816,76 | – 150191,69 | 6033844625,07 | 6033635575,97 | 209049,10 |
DK | DKK | 9058346238,16 | 0,00 | 9058346238,16 | – 68177,57 | 9058278060,59 | 9058602584,17 | – 324523,58 |
EE | EEK | 8595434,55 | 0,00 | 8595434,55 | 0,00 | 8595434,55 | 8595434,55 | 0,00 |
EL | EUR | 2781442489,74 | 0,00 | 2781442489,74 | – 5228942,57 | 2776213547,17 | 2777610434,43 | – 1396887,26 |
ES | EUR | 6269452812,02 | 57020505,80 | 6326473317,82 | – 7926338,98 | 6318546978,84 | 6319215724,26 | – 668745,42 |
FI | EUR | 869358525,94 | 0,00 | 869358525,94 | – 4383,80 | 869354142,14 | 868904449,67 | 449692,47 |
FR | EUR | 9395956559,98 | 1868053,41 | 9397824613,39 | – 9219078,83 | 9388605534,56 | 9389117043,59 | – 511509,03 |
HU | HUF | 125098884,00 | 0,00 | 125098884,00 | 0,00 | 125098884,00 | 125098884,00 | 0,00 |
IE | EUR | 1829924935,77 | 0,00 | 1829924935,77 | – 1354653,66 | 1828570282,11 | 1829730495,20 | – 1160213,09 |
IT | EUR | 1194172909,54 | 3835460014,48 | 5029632924,02 | – 48452006,98 | 4981180917,04 | 5022642872,80 | – 41461955,76 |
LT | LTL | 1826753,89 | 0,00 | 1826753,89 | 0,00 | 1826753,89 | 1826753,89 | 0,00 |
LU | EUR | 0,00 | 37803193,51 | 37803193,51 | – 42350,66 | 37760842,85 | 37760842,85 | 0,00 |
LV | LVL | 23671,15 | 0,00 | 23671,15 | 0,00 | 23671,15 | 23671,15 | 0,00 |
NL | EUR | 1262187678,33 | 0,00 | 1262187678,33 | – 313300,35 | 1261874377,98 | 1261891680,76 | – 17302,78 |
PL | PLN | 46695429,61 | 0,00 | 46695429,61 | 0,00 | 46695429,61 | 46695429,61 | 0,00 |
PT | EUR | 824235249,10 | 0,00 | 824235249,10 | – 884668,91 | 823350580,19 | 823155282,67 | 195297,52 |
SE | SEK | 7740689327,48 | 0,00 | 7740689327,48 | 0,00 | 7740689327,48 | 7740689327,48 | 0,00 |
SI | SIT | 16964300,84 | 0,00 | 16964300,84 | 0,00 | 16964300,84 | 16964300,84 | 0,00 |
SK | SKK | 57252395,16 | 0,00 | 57252395,16 | 0,00 | 57252395,16 | 57252395,16 | 0,00 |
UK | GBP | 2782254804,67 | 0,00 | 2782254804,67 | – 36835148,07 | 2745419656,60 | 2747004082,12 | – 1584425,52 |
--------------------------------------------------
ANNEX II
Member State | Paying Agency |
Germany | Bayern Umwelt |
Spain | Madrid |
France | SDE |
Italy | AGEA |
Luxembourg | Ministère de l'agriculture |
--------------------------------------------------
