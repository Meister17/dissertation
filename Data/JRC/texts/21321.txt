Commission Decision
of 15 July 2002
approving vaccines against bovine brucellosis within the framework of Council Directive 64/432/EEC
(notified under document number C(2002) 2592)
(Text with EEA relevance)
(2002/598/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 64/432/EEC of 26 June 1964 on animal health problems affecting intra-Community trade in bovine animals and swine(1), as last amended by Commission Regulation (EC) No 535/2002(2), and in particular the third indent of paragraph 4(i) in Annex A(II) thereto;
Whereas:
(1) Community rules on the use of vaccines against brucellosis in bovine animals are laid down in Directive 64/432/EEC.
(2) Bovine brucellosis continues to occur in certain areas of the Community. Vaccination is considered an effective instrument to be employed under certain conditions in conjunction with a test and slaughter policy, in particular in areas with extensive livestock rearing.
(3) A newly developed vaccine offers additional advantages to those already approved and in particular does not interfere with the diagnostic procedures applied in the framework of eradication programmes in force in some Member States in accordance with Community legislation.
(4) In certain cases brucellosis in bovine animals is linked to sheep and goat brucellosis and eradication measures must be carried out within the framework of programmes for the eradication of brucellosis caused by Brucella melitensis, including vaccination with the appropriate vaccine.
(5) The requirements for the production and recommendations for the use of live strain RB 51 and live strain Rev.1 vaccines against bovine brucellosis are included in the Manual of Standards for Diagnostic Tests and Vaccines of the Office International des Epizooties, 4th Edition of 2000, published in August 2001.
(6) It is therefore appropriate to approve, subject to certain conditions, the use of live strain RB 51 and live strain Rev.1 vaccines within brucellosis eradication programmes approved pursuant to Council Decision 90/424/EEC of 26 June 1990 on expenditure in the veterinary field(3), as last amended by Decision 2001/572/EC(4), in order to take into account scientific developments and international standards.
(7) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
For the purposes of this Decision, "competent authority" means the central authority of a Member State competent to carry out veterinary or zootechnical checks or any authority to which it has delegated that competence for the particular purpose of the application of this Decision.
Article 2
The following vaccines against bovine brucellosis are hereby approved for immunisation of female bovine animals under the conditions set out in Article 3:
(a) live strain RB 51 vaccine for animals at risk of infection with Brucella abortus;
(b) live strain Rev.1 vaccine for animals at risk of infection with Brucella melitensis.
Article 3
1. Member States availing of the use of the vaccines approved under Article 2 shall ensure that the conditions set out in paragraphs 2 to 6 are met.
2. The storage, supply, distribution and sale of the vaccines shall be under the control of the competent authority.
3. The vaccines shall only be used by an official veterinary or a veterinary specifically authorised by the competent authority within a brucellosis eradication programme submitted by a Member State and approved by the Commission pursuant to Article 24(7) of Decision 90/424/EEC.
4. The competent authority shall submit to the Commission and to the other Member States detailed information on the vaccination programme, in particular on the vaccination area, the age of the animals to be vaccinated and the test system in place to identify vaccinated animals.
5. The competent authority shall ensure that vaccinated animals are not subject to intra-Community trade, in particular by applying additional methods of marking and registration of vaccinated animals.
6. The competent authority shall inform public health services of the use of these vaccines and the available diagnostic and therapeutic systems in place.
Article 4
This Decision is addressed to the Member States.
Done at Brussels, 15 July 2002.
For the Commission
David Byrne
Member of the Commission
(1) OJ 121, 29.7.1964, p. 1977/64.
(2) OJ L 80, 23.3.2002, p. 22.
(3) OJ L 224, 18.8.1990, p. 19.
(4) OJ L 203, 28.7.2001, p. 16.
