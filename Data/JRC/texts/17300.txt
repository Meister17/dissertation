Commission Regulation (EC) No 1978/2004
of 16 November 2004
establishing unit values for the determination of the customs value of certain perishable goods
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2913/92 of 12 October 1992 establishing the Community Customs Code [1],
Having regard to Commission Regulation (EEC) No 2454/93 of 2 July 1993 [2] laying down provisions for the implementation of Regulation (EEC) No 2913/92, and in particular Article 173(1) thereof,
Whereas:
(1) Articles 173 to 177 of Regulation (EEC) No 2454/93 provide that the Commission shall periodically establish unit values for the products referred to in the classification in Annex 26 to that Regulation.
(2) The result of applying the rules and criteria laid down in the abovementioned Articles to the elements communicated to the Commission in accordance with Article 173(2) of Regulation (EEC) No 2454/93 is that unit values set out in the Annex to this Regulation should be established in regard to the products in question,
HAS ADOPTED THIS REGULATION:
Article 1
The unit values provided for in Article 173(1) of Regulation (EEC) No 2454/93 are hereby established as set out in the table in the Annex hereto.
Article 2
This Regulation shall enter into force on 19 November 2004.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 16 November 2004.
For the Commission
Olli Rehn
Member of the Commission
[1] OJ L 302, 19.10.1992, p. 1. Regulation as last amended by Regulation (EC) No 2700/2000 (OJ L 311, 12.12.2000, p. 17).
[2] OJ L 253, 11.10.1993, p. 1. Regulation as last amended by Commission Regulation (EC) No 2286/2003 (OJ L 343, 31.12.2003, p. 1).
--------------------------------------------------
ANNEX
Code | Description | Amount of unit values per 100 kg |
Species, varieties, CN code | EURLTLSEK | CYPLVLGBP | CZKMTL | DKKPLN | EEKSIT | HUFSKK |
1.10 | New potatoes07019050 | — | — | — | — | — | — |
— | — | — | — | — | — |
— | — | | | | |
1.30 | Onions (other than seed)07031019 | 38,49 | 22,23 | 1212,82 | 286,04 | 602,24 | 9408,88 |
132,90 | 26,14 | 16,63 | 165,06 | 9229,13 | 1524,59 |
345,11 | 26,97 | | | | |
1.40 | Garlic07032000 | 106,86 | 61,71 | 3367,22 | 794,16 | 1672,03 | 26122,44 |
368,97 | 72,57 | 46,16 | 458,26 | 25623,39 | 4232,81 |
958,16 | 74,87 | | | | |
1.50 | Leeksex07039000 | 80,25 | 46,34 | 2528,68 | 596,39 | 1255,64 | 19617,11 |
277,09 | 54,50 | 34,67 | 344,14 | 19242,35 | 3178,70 |
719,55 | 56,22 | | | | |
1.60 | Cauliflowers07041000 | — | — | — | — | — | — |
1.80 | White cabbages and red cabbages07049010 | 16,57 | 9,57 | 522,12 | 123,14 | 259,26 | 4050,54 |
57,21 | 11,25 | 7,16 | 71,06 | 3973,15 | 656,34 |
148,57 | 11,61 | | | | |
1.90 | Sprouting broccoli or calabrese (Brassica oleracea L. convar. botrytis (L.) Alef var. italica Plenck)ex07049090 | 61,43 | 35,48 | 1935,66 | 456,52 | 961,17 | 15016,56 |
212,11 | 41,72 | 26,54 | 263,43 | 14729,69 | 2433,24 |
550,80 | 43,04 | | | | |
1.100 | Chinese cabbageex07049090 | 75,36 | 43,52 | 2374,59 | 560,05 | 1179,13 | 18421,75 |
260,20 | 51,18 | 32,56 | 323,17 | 18069,82 | 2985,01 |
657,70 | 52,80 | | | | |
1.110 | Cabbage lettuce (head lettuce)07051100 | — | — | — | — | — | — |
1.130 | Carrotsex07061000 | 26,74 | 15,44 | 842,58 | 198,72 | 418,39 | 6536,59 |
92,33 | 18,16 | 11,55 | 114,67 | 6411,72 | 1059,17 |
239,76 | 18,73 | | | | |
1.140 | Radishesex07069090 | 39,73 | 22,94 | 1251,89 | 295,26 | 621,64 | 9712,00 |
137,18 | 26,98 | 17,16 | 170,37 | 9526,46 | 1573,71 |
356,23 | 27,83 | | | | |
1.160 | Peas (Pisum sativum)07081000 | 401,77 | 232,02 | 12659,73 | 2985,78 | 6286,31 | 98212,33 |
1387,23 | 272,84 | 173,56 | 1722,90 | 96336,07 | 15914,05 |
3602,38 | 281,48 | | | | |
1.170 | Beans: | | | | | | |
1.170.1 | —Beans (Vigna spp., Phaseolus spp.)ex07082000 | 120,71 | 69,71 | 3803,60 | 897,08 | 1888,72 | 29507,78 |
416,79 | 81,97 | 52,15 | 517,64 | 28944,06 | 4781,36 |
1082,33 | 84,57 | | | | |
1.170.2 | —Beans (Phaseolus spp., vulgaris var. Compressus Savi)ex07082000 | 194,17 | 112,13 | 6118,30 | 1442,99 | 3038,10 | 47464,86 |
670,43 | 131,86 | 83,88 | 832,66 | 46558,08 | 7691,07 |
1740,99 | 136,04 | | | | |
1.180 | Broad beansex07089000 | — | — | — | — | — | — |
1.190 | Globe artichokes07091000 | — | — | — | — | — | — |
1.200 | Asparagus: | | | | | | |
1.200.1 | —greenex07092000 | 244,55 | 141,23 | 7705,86 | 1817,42 | 3826,42 | 59780,96 |
844,39 | 166,08 | 105,65 | 1048,72 | 58638,89 | 9686,74 |
2192,73 | 171,33 | | | | |
1.200.2 | —otherex07092000 | 454,18 | 262,29 | 14311,10 | 3375,26 | 7106,31 | 111023,40 |
1568,18 | 308,43 | 196,20 | 1947,64 | 108902,39 | 17989,92 |
4072,28 | 318,20 | | | | |
1.210 | Aubergines (eggplants)07093000 | 95,69 | 55,26 | 3015,27 | 711,15 | 1497,26 | 23392,01 |
330,41 | 64,98 | 41,34 | 410,36 | 22945,12 | 3790,38 |
858,01 | 67,04 | | | | |
1.220 | Ribbed celery (Apium graveolens L., var. dulce (Mill.) Pers.)ex07094000 | 83,53 | 48,24 | 2632,03 | 620,76 | 1306,96 | 20418,91 |
288,41 | 56,73 | 36,08 | 358,20 | 20028,82 | 3308,62 |
748,96 | 58,52 | | | | |
1.230 | Chantarelles07095910 | 553,21 | 319,48 | 17431,65 | 4111,24 | 8655,86 | 135232,18 |
1910,12 | 375,68 | 238,99 | 2372,33 | 132648,69 | 21912,65 |
4960,25 | 387,58 | | | | |
1.240 | Sweet peppers07096010 | 107,89 | 62,30 | 3399,52 | 801,77 | 1688,07 | 26373,00 |
372,51 | 73,27 | 46,61 | 462,65 | 25869,17 | 4273,41 |
967,35 | 75,59 | | | | |
1.250 | Fennel07099050 | — | — | — | — | — | — |
1.270 | Sweet potatoes, whole, fresh (intended for human consumption)07142010 | 78,73 | 45,47 | 2480,88 | 585,11 | 1231,90 | 19246,28 |
271,85 | 53,47 | 34,01 | 337,63 | 18878,60 | 3118,61 |
705,94 | 55,16 | | | | |
2.10 | Chestnuts (Castanea spp.) freshex08024000 | — | — | — | — | — | — |
2.30 | Pineapples, freshex08043000 | 71,81 | 41,47 | 2262,84 | 533,69 | 1123,64 | 17554,81 |
247,96 | 48,77 | 31,02 | 307,96 | 17219,44 | 2844,53 |
643,90 | 50,31 | | | | |
2.40 | Avocados, freshex08044000 | 160,63 | 92,76 | 5061,47 | 1193,74 | 2513,32 | 39266,17 |
554,63 | 109,08 | 69,39 | 688,83 | 38516,03 | 6362,58 |
1440,26 | 112,54 | | | | |
2.50 | Guavas and mangoes, freshex080450 | — | — | — | — | — | — |
2.60 | Sweet oranges, fresh: | | | | | | |
2.60.1 | —Sanguines and semi-sanguines08051010 | 52,21 | 30,15 | 1645,14 | 388,00 | 816,91 | 12762,73 |
180,27 | 35,46 | 22,55 | 223,89 | 12518,91 | 2068,04 |
468,13 | 36,58 | | | | |
2.60.2 | —Navels, navelines, navelates, salustianas, vernas, Valencia lates, Maltese, shamoutis, ovalis, trovita and hamlins08051030 | 50,65 | 29,25 | 1595,88 | 376,39 | 792,45 | 12380,63 |
174,87 | 34,39 | 21,88 | 217,19 | 12144,11 | 2006,12 |
454,12 | 35,48 | | | | |
2.60.3 | —Others08051050 | 54,47 | 31,45 | 1716,20 | 404,76 | 852,19 | 13313,99 |
188,06 | 36,99 | 23,53 | 233,56 | 13059,64 | 2157,36 |
488,35 | 38,16 | | | | |
2.70 | Mandarins (including tangerines and satsumas), fresh; clementines, wilkings and similar citrus hybrids, fresh: | | | | | | |
2.70.1 | —Clementinesex08052010 | — | — | — | — | — | — |
— | — | — | — | — | — |
— | — | | | | |
2.70.2 | —Monreales and satsumasex08052030 | — | — | — | — | — | — |
— | — | — | — | — | — |
— | — | | | | |
2.70.3 | —Mandarines and wilkingsex08052050 | — | — | — | — | — | — |
— | — | — | — | — | — |
— | — | | | | |
2.70.4 | —Tangerines and othersex08052070ex08052090 | — | — | — | — | — | — |
— | — | — | — | — | — |
— | — | | | | |
2.85 | Limes (Citrus aurantifolia, Citrus latifolia), fresh08055090 | 103,48 | 59,76 | 3260,81 | 769,06 | 1619,19 | 25296,86 |
357,31 | 70,28 | 44,71 | 443,77 | 24813,59 | 4099,03 |
927,88 | 72,50 | | | | |
2.90 | Grapefruit, fresh: | | | | | | |
2.90.1 | —whiteex08054000 | 76,70 | 44,29 | 2416,68 | 569,97 | 1200,03 | 18748,29 |
264,82 | 52,08 | 33,13 | 328,89 | 18390,12 | 3037,92 |
687,68 | 53,73 | | | | |
2.90.2 | —pinkex08054000 | 82,97 | 47,91 | 2614,27 | 616,57 | 1298,14 | 20281,16 |
286,47 | 56,34 | 35,84 | 355,79 | 19893,71 | 3286,30 |
743,90 | 58,13 | | | | |
2.100 | Table grapes08061010 | 178,10 | 102,85 | 5611,93 | 1323,57 | 2786,66 | 43536,54 |
614,94 | 120,95 | 76,94 | 763,75 | 42704,82 | 7054,54 |
1596,90 | 124,78 | | | | |
2.110 | Water melons08071100 | 57,43 | 33,17 | 1809,62 | 426,80 | 898,58 | 14038,76 |
198,29 | 39,00 | 24,81 | 246,28 | 13770,57 | 2274,80 |
514,93 | 40,24 | | | | |
2.120 | Melons (other than water melons): | | | | | | |
2.120.1 | —Amarillo, cuper, honey dew (including cantalene), onteniente, piel de sapo (including verde liso), rochet, tendral, futuroex08071900 | 44,25 | 25,55 | 1394,30 | 328,85 | 692,36 | 10816,81 |
152,79 | 30,05 | 19,12 | 189,76 | 10610,17 | 1752,73 |
396,76 | 31,00 | | | | |
2.120.2 | —Otherex08071900 | 89,32 | 51,58 | 2814,40 | 663,77 | 1397,52 | 21833,74 |
308,40 | 60,66 | 38,59 | 383,02 | 21416,62 | 3537,88 |
800,85 | 62,58 | | | | |
2.140 | Pears | | | | | | |
2.140.1 | —Pears — nashi (Pyrus pyrifolia),Pears — Ya (Pyrus bretscheideri)ex08082050 | — | — | — | — | — | — |
— | — | — | — | — | — |
— | — | | | | |
2.140.2 | —Otherex08082050 | — | — | — | — | — | — |
— | — | — | — | — | — |
— | — | | | | |
2.150 | Apricots08091000 | 214,58 | 123,92 | 6761,42 | 1594,67 | 3357,45 | 52454,08 |
740,90 | 145,72 | 92,70 | 920,18 | 51451,99 | 8499,51 |
1923,99 | 150,33 | | | | |
2.160 | Cherries0809209508092005 | 851,77 | 491,90 | 26839,27 | 6330,01 | 13327,30 | 208215,15 |
2940,99 | 578,44 | 367,96 | 3652,64 | 204237,39 | 33738,61 |
7637,22 | 596,75 | | | | |
2.170 | Peaches08093090 | 352,71 | 203,69 | 11113,96 | 2621,21 | 5518,74 | 86220,45 |
1217,84 | 239,53 | 152,37 | 1512,53 | 84573,28 | 13970,92 |
3162,52 | 247,11 | | | | |
2.180 | Nectarinesex08093010 | 272,32 | 157,27 | 8580,92 | 2023,80 | 4260,94 | 66569,53 |
940,28 | 184,94 | 117,64 | 1167,81 | 65297,78 | 10786,74 |
2441,74 | 190,79 | | | | |
2.190 | Plums08094005 | 117,37 | 67,78 | 3698,35 | 872,25 | 1836,45 | 28691,29 |
405,26 | 79,71 | 50,70 | 503,32 | 28143,17 | 4649,06 |
1052,38 | 82,23 | | | | |
2.200 | Strawberries08101000 | 330,34 | 190,77 | 10409,01 | 2454,95 | 5168,70 | 80751,61 |
1140,60 | 224,33 | 142,71 | 1416,60 | 79208,93 | 13084,77 |
2961,93 | 231,44 | | | | |
2.205 | Raspberries08102010 | 304,95 | 176,11 | 9608,97 | 2266,27 | 4771,43 | 74545,03 |
1052,93 | 207,09 | 131,74 | 1307,72 | 73120,91 | 12079,07 |
2734,27 | 213,65 | | | | |
2.210 | Fruit of the species Vaccinium myrtillus08104030 | 1690,61 | 976,33 | 53271,12 | 12563,94 | 26452,30 | 413269,61 |
5837,34 | 1148,09 | 730,34 | 7249,84 | 405374,47 | 66965,06 |
15158,52 | 1184,44 | | | | |
2.220 | Kiwi fruit (Actinidia chinensis Planch.)08105000 | 152,33 | 87,97 | 4799,84 | 1132,04 | 2383,41 | 37236,48 |
525,96 | 103,45 | 65,81 | 653,23 | 36525,11 | 6033,70 |
1365,81 | 106,72 | | | | |
2.230 | Pomegranatesex08109095 | 115,12 | 66,48 | 3627,54 | 855,55 | 1801,29 | 28141,94 |
397,50 | 78,18 | 49,73 | 493,68 | 27604,31 | 4560,04 |
1032,23 | 80,66 | | | | |
2.240 | Khakis (including sharon fruit)ex08109095 | 112,29 | 64,85 | 3538,13 | 834,46 | 1756,89 | 27448,26 |
387,70 | 76,25 | 48,51 | 481,52 | 26923,89 | 4447,64 |
1006,79 | 78,67 | | | | |
2.250 | Lycheesex081090 | — | — | — | — | — | — |
--------------------------------------------------
