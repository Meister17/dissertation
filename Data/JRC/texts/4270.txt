Decision of the EEA Joint Committee No 31/2005
of 11 March 2005
amending Annex II (Technical regulations, standards, testing and certification) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex II to the Agreement was amended by the Agreement on the participation of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Hungary, the Republic of Latvia, the Republic of Lithuania, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic in the European Economic Area signed on 14 October 2003 in Luxembourg [1].
(2) Directive 2004/22/EC of the European Parliament and of the Council of 31 March 2004 on measuring instruments [2] is to be incorporated into the Agreement.
(3) Directive 2004/22/EC repeals, with effect from 30 October 2006, Council Directives 71/318/EEC [3], 71/319/EEC [4], 71/348/EEC [5], 73/362/EEC [6], 75/410/EEC [7], 76/891/EEC [8], 77/95/EEC [9], 77/313/EEC [10], 78/1031/EEC [11] and 79/830/EEC [12], which are incorporated into the Agreement and which are consequently to be deleted from the Agreement with effect from 30 October 2006,
HAS DECIDED AS FOLLOWS:
Article 1
Chapter IX of Annex II to the Agreement shall be amended as follows:
1. The following point shall be inserted after point 27a (Council Directive 93/42/EEC):
"27b. 32004 L 0022: Directive 2004/22/EC of the European Parliament and of the Council of 31 March 2004 on measuring instruments (OJ L 135, 30.4.2004, p. 1)."
2. The text of points 3 (Council Directive 71/318/EEC), 4 (Council Directive 71/319/EEC), 6 (Council Directive 71/348/EEC), 9 (Council Directive 73/362/EEC), 14 (Council Directive 75/410/EEC), 19 (Council Directive 76/891/EEC), 20 (Council Directive 77/95/EEC), 21 (Council Directive 77/313/EEC), 22 (Council Directive 78/1031/EEC) and 23 (Council Directive 79/830/EEC) shall be deleted with effect from 30 October 2006.
Article 2
The texts of Directive 2004/22/EC in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 12 March 2005, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [13].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 11 March 2005.
For the EEA Joint Committee
The President
Richard Wright
[1] OJ L 130, 29.4.2004, p. 3.
[2] OJ L 135, 30.4.2004, p. 1.
[3] OJ L 202, 6.9.1971, p. 21.
[4] OJ L 202, 6.9.1971, p. 32.
[5] OJ L 239, 25.10.1971, p. 9.
[6] OJ L 335, 5.12.1973, p. 56.
[7] OJ L 183, 14.7.1975, p. 25.
[8] OJ L 336, 4.12.1976, p. 30.
[9] OJ L 26, 31.1.1977, p. 59.
[10] OJ L 105, 28.4.1977, p. 18.
[11] OJ L 364, 27.12.1978, p. 1.
[12] OJ L 259, 15.10.1979, p. 1.
[13] Constitutional requirements indicated.
--------------------------------------------------
