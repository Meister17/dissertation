Commission Regulation (EC) No 1686/2003
of 25 September 2003
amending the Annexes to Council Regulation (EC) No 2501/2001 applying a scheme of generalised tariff preferences for the period from 1 January 2002 to 31 December 2004
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 2501/2001 of 10 December 2001 applying a scheme of generalised tariff preferences for the period from 1 January 2002 to 31 December 2004(1), as last amended by Regulation (EC) No 814/2003(2), and in particular Article 35 thereof,
Whereas:
(1) Article 35 of Regulation (EC) No 2501/2001 lays down the procedure for updating the Annexes to the Regulation to take account of amendments to the Combined Nomenclature or changes in the international status or classification of countries or territories.
(2) Commission Regulation (EC) No 1779/2002 of 4 October 2002 on the nomenclature of countries and territories for the external trade statistics of the Community and statistics of trade between Member States(3) includes data affecting the list in Annex I to Regulation (EC) No 2501/2001. Commission Regulation (EC) No 1832/2002 of 1 August 2002 amending Annex I to Council Regulation (EEC) No 2658/87 on the tariff and statistical nomenclature and on the Common Customs Tariff(4) includes data affecting the list in Annex IV to Regulation (EC) No 2501/2001. These Regulations entered into force on 1 January 2003.
(3) The lists in Annexes I and IV to Regulation (EC) No 2501/2001 should therefore be amended accordingly, with effect from 1 January 2003.
(4) The measures provided for in this Regulation are in accordance with the opinion of the Generalised Preferences Committee,
HAS ADOPTED THIS REGULATION:
Article 1
Annexes I and IV to Regulation (EC) No 2501/2001 are amended in accordance with the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Union.
It shall apply from 1 January 2003.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 25 September 2003.
For the Commission
Pascal Lamy
Member of the Commission
(1) OJ L 346, 31.12.2001, p. 1.
(2) OJ L 116, 13.5.2003, p. 1.
(3) OJ L 269, 5.10.2002, p. 6.
(4) OJ L 290, 28.10.2002, p. 1.
ANNEX
Annex I, page 17, first column:
the code: ">TABLE>"
for East Timor is replaced by: ">TABLE>".
Annex IV, page 23, first column and the corresponding description:
for: ">TABLE>",
read: ">TABLE>";
Annex IV, page 24, first column and the corresponding description:
for: ">TABLE>",
read: ">TABLE>";
Annex IV, page 24, first column:
for: ">TABLE>",
read: ">TABLE>";
Annex IV, page 25, first column:
for: ">TABLE>",
read: ">TABLE>";
Annex IV, page 33, first column and the corresponding description:
for: ">TABLE>",
read: ">TABLE>";
Annex IV, page 33, first column and corresponding description, columns G and D:
After " ex 1209 91 " insert " 1209 91 30 Salad beet seed or beetroot seed (Beta vulgaris var. conditiva)", an "S" in column G and an "X" in column D;
Annex IV, page 41, first column:
delete " 2309 90 93 " and the corresponding description;
Annex IV, page 41, first column:
for: ">TABLE>",
read: ">TABLE>".
