COMMISSION REGULATION (EC) No 593/2000
of 13 March 2000
suspending application of Council Regulation (EC) No 2006/97 laying down certain rules for the application of the special arrangements for imports of olive oil originating in Morocco
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 2006/97 of 9 October 1997 laying down certain rules for the application of the special arrangements for imports of olive oil originating in Morocco(1),
Whereas:
(1) Articles 17 and 18 of, and Annex B to the Cooperation Agreement between the European Economic Community and the Kingdom of Morocco(2), provide for special arrangements for imports of olive oil falling within CN codes 1509 and 1510, wholly obtained in Morocco and transported direct from that country to the Community. Those arrangements were extended beyond 1 January 1994 by an agreement in the form of an Exchange of Letters with the Kingdom of Morocco for the duration of the Cooperation Agreement.
(2) Regulation (EC) No 2006/97 establishes certain rules for the application of the arrangements with the aim of adapting them to the requirements of the Agreement on agriculture concluded as part of the Uruguay Round of multilateral trade negotiations.
(3) Article 5 of Regulation (EC) No 2006/97 states that where a new agreement is concluded the Commission shall adopt the resultant adjustments in accordance with the procedure laid down in Article 38 of Regulation No 136/66/EEC of 22 September 1966 on the establishment of a common organisation of the market in oils and fats(3), as last amended by Regulation (EC) No 2702/1999(4). The special arrangements will no longer be applicable once the Euro-Mediterranean Agreement establishing an association between the European Communities and their Member States, of the one part, and the Kingdom of Morocco, of the other part(5), enters into force on 1 March 2000. Pending the formal repeal of Regulation (EC) No 2006/97 by an act of Council, therefore, it is necessary to adapt it to the new situation by suspending its application with effect from the entry into force of the new agreement.
(4) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for oils and fats,
HAS ADOPTED THIS REGULATION:
Article 1
Application of Council Regulation (EC) No 2006/97 is suspended.
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Communities.
It shall apply from 1 March 2000.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 13 March 2000.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ L 284, 16.10.1997, p. 13.
(2) OJ L 264, 27.9.1978, p. 2.
(3) OJ 172, 30.9.1966, p. 3025/66.
(4) OJ L 327, 21.12.1999, p. 7.
(5) OJ L 70, 18.3.2000.
