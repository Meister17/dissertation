Decision of the EEA Joint Committee No 53/2005
of 29 April 2005
amending Annex II (Technical regulations, standards, testing and certification) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex II to the Agreement was amended by Decision of the EEA Joint Committee No 43/2005 of 11 March 2005 [1].
(2) Commission Directive 2003/120/EC of 5 December 2003 amending Directive 90/496/EEC on nutrition labelling for foodstuffs [2] is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
The following shall be added in point 53 (Council Directive 90/496/EEC) of Chapter XII of Annex II to the Agreement:
", as amended by:
- 32003 L 0120: Commission Directive 2003/120/EC of 5 December 2003 (OJ L 333, 20.12.2003, p. 51)."
Article 2
The text of Directive 2003/120/EC in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 30 April 2005, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [3].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 29 April 2005.
For the EEA Joint Committee
The President
Richard Wright
[1] OJ L 198, 28.7.2005, p. 45.
[2] OJ L 333, 20.12.2003, p. 51.
[3] No constitutional requirements indicated.
--------------------------------------------------
