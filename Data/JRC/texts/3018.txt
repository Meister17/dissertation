Commission Regulation (EC) No 872/2005
of 9 June 2005
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 10 June 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 9 June 2005.
For the Commission
J. M. Silva Rodríguez
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 1947/2002 (OJ L 299, 1.11.2002, p. 17).
--------------------------------------------------
ANNEX
to Commission Regulation of 9 June 2005 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code | Standard import value |
07020000 | 052 | 84,2 |
204 | 50,1 |
999 | 67,2 |
07070005 | 052 | 92,9 |
999 | 92,9 |
07099070 | 052 | 92,6 |
999 | 92,6 |
08055010 | 388 | 64,9 |
528 | 47,8 |
624 | 63,4 |
999 | 58,7 |
08081080 | 204 | 70,2 |
388 | 91,2 |
400 | 121,4 |
404 | 78,8 |
508 | 73,7 |
512 | 72,8 |
524 | 61,3 |
528 | 64,1 |
720 | 82,3 |
804 | 95,4 |
999 | 81,1 |
08091000 | 052 | 198,2 |
624 | 183,0 |
999 | 190,6 |
08092095 | 052 | 344,6 |
068 | 238,7 |
400 | 426,9 |
999 | 336,7 |
--------------------------------------------------
