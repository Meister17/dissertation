Action brought on 28 July 2006 — Bellantone v Court of Auditors
Parties
Applicant: Gerardo Bellantone (Luxembourg, Luxembourg) (represented by: T. Bontinck and J. Feld, lawyers)
Defendant: European Court of Auditors
Form of order sought
The applicant claims that the Tribunal should:
- Annul the decision of the Secretary-General of the European Court of Auditors of 30 March 2006 rejecting the applicant's complaint seeking payment of the balance of the allowance in lieu of additional notice, of the severance grant and of the daily subsistence allowance;
- Order the defendant to pay: (i) EUR 20751,45 in respect of additional notice, (ii) EUR 39247,74 corresponding to the severance grant to which the applicant could have been entitled, (iii) EUR 8467,02 in respect of daily subsistence allowance;
- Order the defendant to pay interest for late payment until payment is made;
- Order the rectification of the remarks made in the contested decision concerning the absence of any remark by the applicant regarding the threat of possible dismissal by the defendant;
- Order the defendant to pay the costs.
Pleas in law and main arguments
The applicant, a former member of the temporary staff of the European Court of Auditors in Grade A*8, was appointed as a probationary official in Grade A*5, without his agreement being sought beforehand.
In his application, the applicant maintains that the defendant's conduct amounts to breaches of contractual and non-contractual duties. He alleges infringement of the Code of good administrative conduct for the staff of the European Court of Auditors, infringement of Article 25 of the Staff Regulations, as well as infringement of the general principles of the law of the public service relating to sound administration, legitimate expectations and the retention of acquired rights.
--------------------------------------------------
