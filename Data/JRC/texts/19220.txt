Commission Decision
of 24 July 2003
on the minimum set of leased lines with harmonised characteristics and associated standards referred to in Article 18 of the Universal Service Directive
(2003/548/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Directive 2002/22/EC of the European Parliament and of the Council of 7 March 2002 on universal service and users' rights relating to electronic communications networks and services (Universal Service Directive)(1) and in particular Article 18(3) thereof,
Whereas:
(1) Article 18(3) of the Universal Service Directive provides for the publication of the minimum set of leased lines with harmonised characteristics and associated standards to be published in the Official Journal of the European Union as part of the list of standards referred to in Article 17 of Directive 2002/21/EC of the European Parliament and of the Council of 7 March 2002 on a common regulatory framework for electronic communications networks and services (Framework Directive)(2).
(2) The minimum set of leased lines was defined in Annex II to Council Directive 92/44/EEC of 5 June 1992 on the application of open network provision (ONP) to leased lines(3), as last amended by Commission Decision 98/80/EC(4). That Directive was repealed with effect from 25 July 2003 by the Framework Directive.
(3) This Decision provides continuity of the legal basis for the minimum set of leased lines, for the purpose of implementation of the relevant provisions in the Framework Directive and the Universal Service Directive. The minimum set of leased lines in this Decision is the same as that in Directive 92/44/EEC, except that the references to European Telecommunications Standards (ETSs) have been replaced by references to European Standards (EN), as agreed by the European Telecommunications Standards Institute in 2001. However, leased lines that comply with the previous ETS standards should continue to be deemed in accordance with the requirements for the minimum set of leased lines.
(4) This Decision identifies the minimum set of leased lines with harmonised characteristics and associated standards and forms an integral part of the list of standards published in accordance with Article 17 of the Framework Directive 2002/21/EC. The current version of the list of standards, only containing voluntary provisions, was published in the Official Journal of the European Union in December 2002(5). For reasons of differences in procedure and in legal effect, it is appropriate to distinguish the chapters of the list of standards that include mandatory provisions in this Decision from those chapters that only include voluntary provisions.
(5) The measures provided for in this Decision are in accordance with the opinion of the Communications Committee,
HAS DECIDED AS FOLLOWS:
Sole Article
The minimum set of leased lines with harmonised characteristics and associated standards are set out in the Annex.
Done at Brussels, 24 July 2003.
For the Commission
Erkki Liikanen
Member of the Commission
(1) OJ L 108, 24.4.2002, p. 51.
(2) OJ L 108, 24.4.2002, p. 33.
(3) OJ L 165, 19.6.1992, p. 27.
(4) OJ L 14, 20.1.1998, p. 27.
(5) OJ C 331, 31.12.2002, p. 32.
ANNEX
LIST OF STANDARDS AND/OR SPECIFICATIONS FOR ELECTRONIC COMMUNICATIONS NETWORKS, SERVICES AND ASSOCIATED FACILITIES AND SERVICES
Mandatory part
Identification of the minimum set of leased lines
1. Purpose
This publication identifies the minimum set of leased lines with harmonised characteristics and associated standards referred to in Article 18 of Directive 2002/22/EC (Universal Service Directive).
This list forms part of the list of standards referred to in Article 17 of Directive 2002/21/EC (the Framework Directive).
This publication is in addition to the list of standards and/or specifications for electronic communications networks, services and associated facilities and services published in the Official Journal of the European Communities in December 2002(1).
2. Technical Standards
The standards mentioned in this publication are ETSI deliverables under the current ETSI nomenclature. According to the "ETSI Directives"(2), these deliverables are defined as follows:
European Standard (telecommunications series), EN: An ETSI deliverable containing normative provisions, approved for publication in a process involving the National Standards Organisations and/or ETSI National Delegations with implications concerning standstill and national transposition.
Harmonised Standard: An EN (telecommunications series) the drafting of which has been entrusted to ETSI by a mandate from the European Commission pursuant to Directive 98/48/EC (latest amendment to Council Directive 83/189/EEC) and has been drafted taking into account the applicable essential requirements of the "New Approach" Directive and whose reference has subsequently been announced in the Official Journal of the European Communities.
The version of the standards referred to in this list is the version valid at the time that the list is published.
3. Addresses where documents referenced can be obtained
ETSI Publications Office(3)
>TABLE>
4. References to EU legislation
The list refers to the following legislative documents which may be found at http://europa.eu.int/ information_society/topics/ telecoms/regulatory/index_en.htm
- Directive 2002/21/EC of the European Parliament and of the Council of 7 March 2002 on a common regulatory framework for electronic communications networks and services (Framework Directive) (OJ L 108, 24.4.2002, p. 33).
- Directive 2002/22/EC of the European Parliament and of the Council of 7 March 2002 on Universal service and users' rights relating to electronic communications networks and services (Universal Service Directive) (OJ L 108, 24.4.2002, p. 51).
Identification of the minimum set of leased lines with harmonised characteristics and associated standards
ANALOGUE LEASED LINES
>TABLE>
DIGITAL LEASED LINES
>TABLE>
(1) OJ C 331, 31.12.2002, p. 32.
(2) Available at http://portal.etsi.org/directives/.
(3) ETSI documents can be downloaded from the ETSI Publications Download Area (http://pda.etsi.org/pda/ queryform.asp).
