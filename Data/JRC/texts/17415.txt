P6_TA(2004)0018
Pre-accession measures for agriculture and rural development in the applicant countries (Bulgaria, Romania) *
European Parliament legislative resolution on the proposal for a Council regulation amending Regulation (EC) No 1268/1999 on Community support for pre-accession measures for agriculture and rural development in the applicant countries of Central and Eastern Europe in the pre-accession period (COM(2004)0163 — C5-0178/2004 — 2004/0054(CNS))
(Consultation procedure)
The European Parliament,
- having regard to the Commission proposal to the Council (COM(2004)0163) [1],
- having regard to Article 308 of the EC Treaty, pursuant to which the Council consulted Parliament (C5-0178/2004),
- having regard to Rule 51 of its Rules of Procedure,
- having regard to the report of the Committee on Agriculture (A6-0009/2004),
1. Approves the Commission proposal;
2. Calls on the Council to notify Parliament if it intends to depart from the text approved by Parliament;
3. Asks the Council to consult Parliament again if it intends to amend the Commission proposal substantially;
4. Instructs its President to forward its position to the Council and Commission.
[1] Not yet published in OJ.
--------------------------------------------------
