COMMISSION DECISION of 12 May 1995 amending Commission Decision 94/325/EC laying down specific conditions for importing fishery and aquaculture products from Thailand (Text with EEA relevance) (95/178/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/493/EEC of 22 July 1991 laying down the health conditions for the production and the placing on the market of fishery products (1), as last amended by the Treaty of Accession of Austria, Finland and Sweden, and in particular Article 11 (5) thereof,
Whereas the list of establishments approved by Thailand for importing fishery and aquaculture products into the Community has been drawn up in Commission Decision 94/325/EC (2), as amended by Decision 94/704/EC (3); whereas this list may be amended following the communication of a new list by the competent authority in Thailand;
Whereas the competent authority in Thailand has communicated a new list amending the data of two establishments and the date of expiry of approval of 22 establishments;
Whereas it is necessary to amend the list of approved establishments;
Whereas the measures provided for in this Decision have been drawn up in accordance with the procedure laid down by Commission Decision 90/13/EEC (4),
HAS ADOPTED THIS DECISION:
Article 1
Annex B of Decision 94/325/EC is replaced by the Annex to this Decision.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 12 May 1995.
For the Commission Franz FISCHLER Member of the Commission
ANNEX
'ANNEX B List of approval establishments >TABLE>
