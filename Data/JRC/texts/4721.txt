Brussels, 25.2.2005
COM(2005) 62 final
2005/0012 (CNS)
Proposal for a
COUNCIL DECISION
on the signature and provisional application of the Agreement between the European Community and the Republic of Lebanon on certain aspects of air services
Proposal for a
COUNCIL DECISION
on the conclusion of the Agreement between the European Community and the Republic of Lebanon on certain aspects of air services
(presented by the Commission)
EXPLANATORY MEMORANDUM
International aviation relations between Member States and third countries have been traditionally governed by bilateral air services agreements between Member States and third countries, their Annexes and other related bilateral or multilateral arrangements.
Following the judgements of the Court of Justice of the European Communities in the cases C-466/98, C-467/98, C-468/98, C-471/98, C-472/98, C-475/98 and C-476/98, the Community has exclusive competence with respect to various aspects of external aviation. The Court of Justice also clarified the right of Community air carriers to benefit from the right of establishment within the Community, including the right to non-discriminatory market access.
Traditional designation clauses in Member States’ bilateral air services agreements infringe Community law. They allow a third country to reject, withdraw or suspend the permissions or authorisations of an air carrier that has been designated by a Member State but that is not substantially owned and effectively controlled by that Member State or its nationals. This has been found to constitute discrimination against Community carriers established in the territory of a Member State but owned and controlled by nationals of other Member States. This is contrary to Article 43 of the Treaty which guarantees nationals of Member States who have exercised their freedom of establishment the same treatment in the host Member State as that accorded to nationals of that Member State.
Following the Court of Justice judgements, the Council authorised the Commission in June 2003 to open negotiations with third countries on the replacement of certain provisions in existing bilateral agreements with a Community agreement.[1]
In accordance with the mechanisms and directives in the Annex to the Council’s decision authorising the Commission to open negotiations with third countries on the replacement of certain provisions in existing bilateral agreements with a Community agreement, the Commission has negotiated an agreement with the Republic of Lebanon that replaces certain provisions in the existing bilateral air services agreements between Member States and the Republic of Lebanon. Article 2 of the Agreement replaces the traditional designation clauses with a Community designation clause, permitting all Community carriers to benefit from the right of establishment. Articles 4 and 5 of the Agreement address two types of clauses concerning matters of Community competence. Article 4 deals with the taxation of aviation fuel, a matter which has been harmonised by Council Directive 2003/96/EC restructuring the Community framework for the taxation of energy products and electricity, particularly Article 14 paragraph 2 thereof. Article 5 (Pricing) resolves conflicts between the existing bilateral air services agreements and Council Regulation No. 2409/92 on fares and rates for air services which prohibits third country carriers from being price leaders on air services for carriage wholly within the Community.
The Council is asked to approve the decisions on the signature and provisional application and on the conclusion of the agreement between the European Community and the Republic of Lebanon on certain aspects of air services and to designate the persons authorized to sign the agreement on behalf of the Community.
Proposal for a
COUNCIL DECISION
on the signature and provisional application of the Agreement between the European Community and the Republic of Lebanon on certain aspects of air services
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 80, paragraph 2, in conjunction with Article 300, paragraph 2, first sentence of the first subparagraph thereof,
Having regard to the proposal from the Commission[2],
Whereas:
The Council has authorised the Commission on 5 June 2003 to open negotiations with third countries on the replacement of certain provisions in existing bilateral agreements with a Community agreement;
The Commission has negotiated on behalf of the Community an agreement with the Republic of Lebanon on certain aspects of air services in accordance with the mechanisms and directives in the Annex to the Council’s decision authorising the Commission to open negotiations with third countries on the replacement of certain provisions in existing bilateral agreements with a Community agreement;
Subject to its possible conclusion at a later date, the agreement negotiated by the Commission should be signed and provisionally applied.
HAS DECIDED AS FOLLOWS:
Sole Article
1. Subject to its subsequent conclusion at a later date, the President of the Council is hereby authorised to designate the person or persons empowered to sign on behalf of the Community the agreement between the European Community and the Republic of Lebanon on certain aspects of air services.
2. Pending its entry into force, the agreement shall be applied provisionally from the first day of the first month following the date on which the parties have notified each other of the completion of the necessary procedures for this purpose. The President of the Council is hereby authorised to make the notification provided in Article 8.2 of the agreement.
3. The text of the agreement is annexed to this decision.
Done at Brussels,
For the Council
The President
2005/0012 (CNS)
Proposal for a
COUNCIL DECISION
on the conclusion of the Agreement between the European Community and the Republic of Lebanon on certain aspects of air services
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 80, paragraph 2, in conjunction with Article 300, paragraph 2, first sentence of the first subparagraph thereof and Article 300, paragraph 3, first subparagraph,
Having regard to the proposal from the Commission[3],
Having regard to the opinion of the European Parliament[4],
Whereas:
(1) The Council has authorised the Commission on 5 June 2003 to open negotiations with third countries on the replacement of certain provisions in existing bilateral agreements with a Community agreement;
(2) The Commission has negotiated on behalf of the Community an agreement with the Republic of Lebanon on certain aspects of air services in accordance with the mechanisms and directives in the Annex to the Council’s decision authorising the Commission to open negotiations with third countries on the replacement of certain provisions in existing bilateral agreements with a Community agreement;
(3) This agreement has been signed on behalf of the Community on subject to its possible conclusion at a later date, in conformity with the decision …/…/EC of the Council on [5]
(4) This agreement should be approved.
HAS DECIDED AS FOLLOWS:
Article 1
1. The agreement between the European Community and the Republic of Lebanon on certain aspects of air services is approved on behalf of the Community.
2. The text of the agreement is annexed to this decision.
Article 2
The president of the Council is authorised to designate the person empowered to make the notification provided in Article 8.1 of the Agreement.
Done at Brussels,
For the Council
The President
Annex
AGREEMENT between the European Community and the Republic of Lebanon on certain aspects of air services
THE EUROPEAN COMMUNITY
of the one part, and
THE REPUBLIC OF LEBANON
of the other part
(hereinafter referred to as ‘the Parties’)
NOTING that bilateral air service agreements have been concluded between several Member States of the European Community and the Republic of Lebanon containing provisions contrary to Community law.
NOTING that the European Community has exclusive competence with respect to several aspects that may be included in bilateral air service agreements between Member States of the European Community and third countries,
NOTING that under European Community law Community air carriers established in a Member State have the right to non-discriminatory access to air routes between the Member States of the European Community and third countries,
HAVING REGARD to the agreements between the European Community and certain third countries providing for the possibility for the nationals of such third countries to acquire ownership in air carriers licensed in accordance with European Community law,
RECOGNISING that provisions of the bilateral air service agreements between Member States of the European Community and the Republic of Lebanon, which are contrary to European Community law, must be brought into full conformity with it in order to establish a sound legal basis for air services between the European Community and the Republic of Lebanon and to preserve the continuity of such air services,
NOTING that it is not a purpose of the European Community, as part of these negotiations, to affect the total volume of air traffic between the European Community and the Republic of Lebanon, the balance between Community air carriers and air carriers of the Republic of Lebanon, or to negotiate amendments to the provisions of existing bilateral air service agreements concerning traffic rights.
HAVE AGREED AS FOLLOWS:
Article 1 General Provisions
1. For the purposes of this Agreement, ‘Member States’ shall mean Member States of the European Community.
2. References in each of the agreements listed in Annex 1 to nationals of the Member State that is a party to that agreement shall be understood as referring to nationals of the Member States of the European Community.
3. References in each of the agreements listed in Annex 1 to air carriers or airlines of the Member State that is a party to that agreement shall be understood as referring to air carriers or airlines designated by that Member State.
Article 2 Designation by a Member State
1. The provisions in paragraphs 2 and 3 of this Article shall supersede the corresponding provisions in the articles listed in Annex 2 (a) and (b) respectively, in relation to the designation of an air carrier by the Member State concerned, its authorisations and permissions granted by the Republic of Lebanon, and the refusal, revocation, suspension or limitation of the authorisations or permissions of the air carrier, respectively.
2. On receipt of a designation by a Member State, the Republic of Lebanon shall grant the appropriate authorisations and permissions with minimum procedural delay, provided that:
i. the air carrier is established in the territory of the designating Member State under the Treaty establishing the European Community and has a valid Operating Licence in accordance with European Community law;
ii. effective regulatory control of the air carrier is exercised and maintained by the Member State responsible for issuing its Air Operators Certificate and the relevant aeronautical authority is clearly identified in the designation; and
iii. the air carrier is owned and shall continue to be owned directly or through majority ownership by Member States and/or nationals of Member States, or by other states listed in Annex 3 and/or nationals of such other states, and shall at all times be effectively controlled by such states and/or such nationals.
3. The Republic of Lebanon may refuse, revoke, suspend or limit the authorisations or permissions of an air carrier designated by a Member State where:
i. the air carrier is not established in the territory of the designating Member State under the Treaty establishing the European Community or does not have a valid Operating Licence in accordance with European Community law;
ii. effective regulatory control of the air carrier is not exercised or not maintained by the Member State responsible for issuing its Air Operators Certificate, or the relevant aeronautical authority is not clearly identified in the designation; or
iii. the air carrier is not owned and effectively controlled directly or through majority ownership by Member States and/or nationals of Member States, or by other states listed in Annex 3 and/or nationals of such other states.
In exercising its right under this paragraph, the Republic of Lebanon shall not discriminate between Community air carriers on the grounds of nationality.
Article 3 Rights with regard to regulatory control
1. The provisions in paragraph 2 of this Article shall complement the articles listed in Annex 2 (c).
2. Where a Member State has designated an air carrier whose regulatory control is exercised and maintained by another Member State, the rights of the Republic of Lebanon under the safety provisions of the agreement between the Member State that has designated the air carrier and the Republic of Lebanon shall apply equally in respect of the adoption, exercise or maintenance of safety standards by that other Member State and in respect of the operating authorisation of that air carrier.
A rticle 4 Taxation of aviation fuel
1. The provisions in paragraph 2 of this Article shall complement the corresponding provisions in the articles listed in Annex 2 (d).
2. Notwithstanding any other provision to the contrary, nothing in each of the agreements listed in Annex 2 (d) shall prevent a Member State from imposing taxes, levies, duties, fees or charges on fuel supplied in its territory for use in an aircraft of a designated air carrier of the Republic of Lebanon that operates between a point in the territory of that Member State and another point in the territory of that Member State or in the territory of another Member State.
Article 5 Tariffs for carriage within the European Community
1. The provisions in paragraph 2 of this Article shall complement the articles listed in Annex 2 (e).
2. The tariffs to be charged by the air carrier(s) designated by the Republic of Lebanon under an agreement listed in Annex 1 containing a provision listed in Annex 2 (e) for carriage wholly within the European Community shall be subject to European Community law.
Article 6 Annexes to the Agreement
The Annexes to this Agreement shall form an integral part thereof.
Article 7 Revision or amendment
The Parties may, at any time, revise or amend this Agreement by mutual consent.
Article 8 Entry into force and provisional application
1. This Agreement shall enter in force when the Parties have notified each other in writing that their respective internal procedures necessary for its entry into force have been completed.
2. Notwithstanding paragraph 1, the Parties agree to provisionally apply this Agreement from the first day of the month following the date on which the Parties have notified each other of the completion of the procedures necessary for this purpose.
3. Agreements and other arrangements between Member States and the Republic of Lebanon which, at the date of signature of this Agreement, have not yet entered into force and are not being applied provisionally are listed in Annex 1 (b). This Agreement shall apply to all such Agreements and arrangements upon their entry into force or provisional application.
Article 9 Termination
1. In the event that an agreement listed in Annex 1 is terminated, all provisions of this Agreement that relate to the agreement listed in Annex 1 concerned shall terminate at the same time.
2. In the event that all agreements listed in Annex 1 are terminated, this Agreement shall terminate at the same time.
IN WITNESS WHEREOF, the undersigned, being duly authorised, have signed this Agreement.
Done at [….] in duplicate, on this […] day of […, …] in the Czech, Danish, Dutch, English, Estonian, Finnish, French, German, Greek, Hungarian, Italian, Latvian, Lithuanian, Maltese, Polish, Portuguese, Slovak, Slovene, Spanish, Swedish and Arabic languages. In case of divergence the English text shall prevail over the other language texts.
FOR THE EUROPEAN COMMUNITY: FOR THE REPUBLIC OF LEBANON:
Annex 1 List of agreements referred to in Article 1 of this Agreement
(a) Air service agreements between the Republic of Lebanon and Member States of the European Community which, at the date of signature of this Agreement, have been concluded, signed and/or are being applied provisionally
- Agreement between the Austrian Federal Government and the Government of the Republic of Lebanon for air services between and beyond their respective territories, done at Beirut on 2 April 1969, as amended (hereafter referred to as Lebanon-Austria Agreement);
- Agreement between the Belgian Government and the Lebanese Government concerning air services between and beyond their respective territories , done at Beirut on 24 December 1953, as amended (hereafter referred to as Lebanon-Belgium Agreement);
- Air Services Agreement between the Government of the Republic of Cyprus and the Government of the Republic of Lebanon , initialled on 23 May 1996 (hereafter referred to as Draft Lebanon-Cyprus Agreement);
- Air Transport Agreement between the Government of the Czech Republic and the Government of the Republic of Lebanon , done at Beirut on 22 September 2003 (hereafter referred to as Lebanon – Czech Republic Agreement);
- Air Transport Agreement between Denmark and Lebanon , done at Beirut on 21 October 1955 (hereafter referred to as Lebanon-Denmark Agreement);
- Draft Air Transport Agreement between the Government of the French Republic and the Government of the Republic of Lebanon , initialled and annexed to the Agreed Record of the consultations between delegations representing the Governments of the French Republic and of the Republic of Lebanon, signed in Paris on 24 June 1998 (hereafter referred to as Draft Lebanon – France Agreement);
- Air Transport Agreement between the Federal Republic of Germany and the Lebanese Republic , done at Beirut on 15 March 1961 as amended (hereafter referred to as Lebanon-Germany Agreement);
- Draft Air Services Agreement between the Government of the Federal Republic of Germany and the Government of the Republic of Lebanon , initialled and annexed to the Agreed Minutes signed in Bonn on 16 January 2002 (hereafter referred to as Draft Revised Lebanon-Germany Agreement);
- Agreement between the Government of the Kingdom of Greece and the Government of the Republic of Lebanon concerning the establishment of air services between their respective territories , done at Beirut on 6 September 1948 (hereafter referred to as Lebanon - Greece Agreement);
- Agreement between the Government of the Hungarian People’s Republic and the Republic of Lebanon relating to Civil Air Transport , done at Beirut on 15 January 1966 (hereafter referred to as Lebanon – Hungary Agreement);
- Air Transport Agreement between the Italian Government and the Government of the Republic of Lebanon , done at Beirut on 24 January 1949 as amended (hereafter referred to as Lebanon – Italy Agreement);
- Air Services Agreement between the Government of the Republic of Lebanon and the Government of the Grand Duchy of Luxembourg , initialled and attached as Appendix B to the Confidential Memorandum of Understanding signed in Beirut on 23 October 1998 (hereafter referred to as Draft Lebanon – Luxembourg Agreement);
- Draft Air Services Agreement between the Government of Malta and the Government of the Republic of Lebanon , initialled and attached as Appendix B to the Agreed Minutes signed in Beirut on 30 April 1999 (hereafter referred to as Draft Lebanon – Malta Agreement);
- Air Transport Agreement between the Kingdom of the Netherlands and the Republic of Lebanon , done at Beirut on 20 September 1949 (hereafter referred to as Lebanon-Netherlands Agreement);
- Air Services Agreement between the Government of the Polish People’s Republic and the Government of the Republic of Lebanon , done at Beirut on 25 April 1966 (hereafter referred to as Lebanon – Poland Agreement);
- Air Transport Agreement between Sweden and Lebanon , done at Beirut on 23 March 1953 (hereafter referred to as Lebanon-Sweden Agreement);
- Agreement between the Government of the United Kingdom of Great Britain and Northern Ireland and the Government of the Lebanese Republic for air services between and beyond their respective territories , done at Beirut on 15 August 1951, as amended (hereafter referred to as Lebanon-UK Agreement)
(b) Air service agreements and other arrangements initialled or signed between the Republic of Lebanon and Member States of the European Community which, at the date of signature of this Agreement, have not yet entered into force and are not being applied provisionally
- Draft Air Transport Agreement between the Kingdom of Spain and the Republic of Lebanon , initialled in Madrid on 21 August 1997 (hereafter referred to as Draft Lebanon – Spain Agreement)
Annex 2 List of articles in the agreements listed in Annex 1 and referred to in Articles 2 to 5 of this Agreement
(a) Designation by a Member State:
- Article 3 of the Lebanon – Austria Agreement;
- Article 3 of the Lebanon – Belgium Agreement;
- Article 4 of the Draft Lebanon – Cyprus Agreement;
- Article 3 of the Lebanon – Czech Republic Agreement;
- Article 3 of the Draft Lebanon – France Agreement;
- Article 3 of the Lebanon – Germany Agreement;
- Article 3 of the Lebanon – Hungary Agreement;
- Article 3 of the Draft Lebanon – Luxembourg Agreement;
- Article 6 of the Draft Lebanon – Malta Agreement;
- Article 3 of the Lebanon – Poland Agreement;
- Article 3 of the Draft Lebanon – Spain Agreement;
- Article 4 of the Lebanon – United Kingdom Agreement;
(b) Refusal, Revocation, Suspension or Limitation of Authorisations or Permissions:
- Article 4 of the Lebanon – Austria Agreement;
- Article 3 of the Lebanon – Belgium Agreement;
- Article 5 of the Draft Lebanon – Cyprus Agreement;
- Article 4 of the Lebanon – Czech Republic Agreement;
- Article 5 of the Lebanon – Denmark Agreement;
- Article 4 of the Draft Lebanon – France Agreement;
- Article 4 of the Lebanon – Germany Agreement;
- Article 6 of the Lebanon – Greece Agreement;
- Article 4 of the Lebanon – Hungary Agreement;
- Article 6 of the Lebanon – Italy Agreement;
- Article 4 of the Draft Lebanon – Luxembourg Agreement;
- Article 7 of the Draft Lebanon – Malta Agreement;
- Article 6 of the Lebanon – Netherlands Agreement;
- Article 3 of the Lebanon – Poland Agreement;
- Article 4 of the Draft Lebanon – Spain Agreement;
- Article 5 of the Lebanon – Sweden Agreement;
- Article 4 of the Lebanon – United Kingdom Agreement;
(c) Regulatory control:
- Article 7 bis of the Lebanon – Austria Agreement;
- Article 7 of the Lebanon – Czech Republic Agreement;
- Article 8 of the Draft Lebanon – France Agreement;
- Article 7 of the Draft Lebanon – Luxembourg Agreement;
- Article 6 of the Draft Revised Lebanon – Germany Agreement;
(d) Taxation of Aviation Fuel:
- Article 5 of the Lebanon – Austria Agreement;
- Article 4 of the Lebanon – Belgium Agreement;
- Article 7 of the Draft Lebanon – Cyprus Agreement;
- Article 8 of the Lebanon – Czech Republic Agreement;
- Article 9 of the Lebanon – Denmark Agreement;
- Article 10 of the Draft Lebanon – France Agreement;
- Article 6 of the Lebanon – Germany Agreement;
- Article 10 of the Draft Revised Lebanon – Germany Agreement;
- Article 3 of the Lebanon – Greece Agreement;
- Article 14 of the Lebanon – Hungary Agreement;
- Article 3 of the Lebanon – Italy Agreement;
- Article 8 of the Draft Lebanon – Luxembourg Agreement;
- Article 9 of the Draft Lebanon – Malta Agreement;
- Article 6 of the Lebanon – Poland Agreement;
- Article 5 of the Draft Lebanon – Spain Agreement;
- Article 9 of the Lebanon – Sweden Agreement;
- Article 5 of the Lebanon – United Kingdom Agreement;
(e) Tariffs for Carriage within the European Community:
- Article 9 of the Lebanon – Austria Agreement;
- Article 7 of the Lebanon – Belgium Agreement;
- Article 16 of the Draft Lebanon – Cyprus Agreement;
- Article 12 of the Lebanon – Czech Republic Agreement;
- Article 7 of the Lebanon – Denmark Agreement;
- Article 14 of the Draft Lebanon – France Agreement;
- Article 9 of the Lebanon – Germany Agreement;
- Article 14 of the Draft Revised Lebanon – Germany Agreement;
- Article 7 of the Lebanon – Hungary Agreement;
- Article 13 of the Draft Lebanon – Luxembourg Agreement;
- Article 14 of the Draft Lebanon – Malta Agreement;
- Article 10 of the Lebanon – Poland Agreement;
- Article 7 of the Draft Lebanon – Spain Agreement;
- Article 7 of the Lebanon – Sweden Agreement;
- Article 7 of the Lebanon – United Kingdom Agreement;
Annex 3 List of other states referred to in Article 2 of this Agreement
(a) The Republic of Iceland (under the Agreement on the European Economic Area) ;
(b) The Principality of Liechtenstein (under the Agreement on the European Economic Area) ;
(c ) The Kingdom of Norway (under the Agreement on the European Economic Area) ;
(d ) The Swiss Confederation (under the Agreement between the European Community and the Swiss Confederation on Air Transport)
[1] Council decision 11323/03 of 5 June 2003 (restricted document)
[2] OJ C , , p. .
[3] OJ C , , p. .
[4] OJ C , , p. .
[5] OJ C , , p. .
