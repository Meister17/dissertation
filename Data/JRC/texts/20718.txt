COMMISSION REGULATION (EC) No 180/94 of 28 January 1994 amending Regulation (EEC) No 1756/93 fixing the operative events for the agricultural conversion rates applicable to milk and milk products
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 3813/92 of 28 December 1992 on the unit of account and the conversion rates to be applied for the purposes of the common agricultural policy (1), and in particular Article 6 (2) thereof,
Whereas Commission Regulation (EEC) No 1756/93 (2), as last amended by Regulation (EC) No 114/94 (3), seeks to fix precisely the agricultural conversion rate to be applied in respect of all amounts fixed in ecus in the milk and milk products sector; whereas an addition must accordingly be made to the said Regulation specifying the operative event for the amounts referred to in Article 1 (3) of Commission Regulation (EC) No 3582/93 of 21 December 1993 on detailed rules for the application of Council Regulation (EEC) No 2073/92 on promoting consumption in the Community and expanding the markets for milk and milk products (4);
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Milk and Milk Products,
HAS ADOPTED THIS REGULATION:
Article 1
The following point is added to the Annex to Regulation (EEC) No 1756/93:
"" ID="1">'14. (EC) No 3582/93> ID="2">Amount of the ceiling and total amount of the overhead costs referred to in Article 1 (3); price referred to in Article 4 (3) (d)> ID="3">Agricultural conversion rate applicable on the final date for submission of the proposals referred to in Article 3'">
Article 2
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Communities.This Regulation shall be binding in its entirety and directly applicable in all Member States.Done at Brussels, 28 January 1994. For the Commission René STEICHEN Member of the Commission
(1) OJ No L 387, 31. 12. 1992, p. 1.
(2) OJ No L 161, 2. 7. 1993, p. 48.
(3) OJ No L 20, 25. 1. 1994, p. 2.
(4) OJ No L 326, 28. 12. 1993, p. 23.
