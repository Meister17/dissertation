Commission Regulation (EC) No 2149/2005
of 23 December 2005
fixing the reduction coefficients to be applied to applications for import licences for bananas originating in the ACP countries for the months of January and February 2006
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1964/2005 of 29 November 2005 on the tariff rates for bananas [1],
Having regard to Commission Regulation (EC) No 2015/2005 of 9 December 2005 on imports during January and February 2006 of bananas originating in ACP countries under the tariff quota opened by Council Regulation (EC) No 1964/2005 on the tariff rates for bananas [2], and in particular Article 6(2) thereof,
Whereas:
(1) The applications for import licences submitted in the Member States under Article 5 of Regulation (EC) No 2015/2005 and sent to the Commission in accordance with Article 6 of that Regulation exceed the available quantities fixed in Article 2 thereof, i.e. 135000 tonnes and 25000 tonnes for the operators referred to in Titles II and III respectively.
(2) The reduction coefficients to be applied to each application should therefore be fixed,
HAS ADOPTED THIS REGULATION:
Article 1
1. A reduction coefficient of 22,039 % shall be applied to each import licence application submitted by the operators referred to in Title II of Regulation (EC) No 2015/2005 under the tariff subquota of 135000 tonnes.
2. A reduction coefficient of 1,294 % shall be applied to each import licence application submitted by the operators referred to in Title III of Regulation (EC) No 2015/2005 under the tariff subquota of 25000 tonnes.
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 23 December 2005.
For the Commission
J. M. Silva Rodríguez
Director-General for Agriculture and Rural Development
[1] OJ L 316, 2.12.2005, p. 1.
[2] OJ L 324, 10.12.2005, p. 5.
--------------------------------------------------
