Council Decision
of 21 October 2002
on the signing, on behalf of the Community, and provisional application of an Agreement in the form of an Exchange of Letters between the European Community and the Former Yugoslav Republic of Macedonia concerning the system of ecopoints to be applied to transit traffic of the Former Yugoslav Republic of Macedonia through Austria
(2003/197/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 71(1), in conjunction with the first sentence of the first subparagraph of Article 300(2) thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) The Commission has negotiated an Agreement in the form of an Exchange of Letters between the European Community and the Former Yugoslav Republic of Macedonia concerning the system of ecopoints to be applied to transit traffic of the Former Yugoslav Republic of Macedonia through Austria.
(2) Subject to its possible conclusion at a later date, the Agreement initialled on 25 January 2001 should be signed.
(3) Provision should be made for the provisional application of the Agreement from 1 January 2002,
HAS DECIDED AS FOLLOWS:
Article 1
The signing of the Agreement in the form of an Exchange of Letters between the European Community and the Former Yugosla
v Republic of Macedonia concerning the system of ecopoints to be applied to transit traffic of the Former Yugoslav Republic of Macedonia through Austria, is hereby approved on behalf of the Community, subject to the Council Decision concerning the conclusion of the said Agreement.
The text of the Agreement is attached to this Decision.
Article 2
The President of the Council is hereby authorised to designate the person(s) empowered to sign the Agreement on behalf of the Community subject to its conclusion.
Article 3
Subject to reciprocity, the Agreement referred to in Article 1 shall be applied on a provisional basis from 1 January 2002, pending the completion of the procedures for its formal conclusion.
Article 4
This Decision shall be published in the Official Journal of the European Union.
Done at Luxembourg, 21 October 2002.
For the Council
The President
P. S. Møller
