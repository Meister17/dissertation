Commission Decision
of 13 April 2004
laying down special conditions for imports of fishery products from Zimbabwe
(notified under document number C(2004) 1328)
(Text with EEA relevance)
(2004/360/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/493/EEC of 22 July 1991 laying down the health conditions for the production and the placing on the market of fishery products(1), and in particular Article 11(1) thereof,
Whereas:
(1) An inspection has been carried out on behalf of the Commission in Zimbabwe to verify the conditions under which fishery products are produced, stored and dispatched to the Community.
(2) The requirements in the legislation of Zimbabwe on health inspection and monitoring of fishery products may be considered equivalent to those laid down in Directive 91/493/EEC.
(3) In particular, the "Department of Livestock and Veterinary Services (DLVS)", is capable of effectively verifying the implementation of the rules in force.
(4) The DLVS has provided official assurances on compliance with the standards for health controls and monitoring of fishery products as set out in Chapter V of the Annex to Directive 91/493/EEC and on the fulfilment of hygienic requirements equivalent to those laid down by that Directive.
(5) It is appropriate to lay down detailed provisions concerning fishery products imported into the Community from Zimbabwe, in accordance with Directive 91/493/EEC.
(6) It is also necessary to draw up a list of approved establishments, factory vessels, or cold stores, and a list of freezer vessels equipped in accordance with the requirements of Council Directive 92/48/EEC of 16 June 1992 laying down the minimum hygiene rules applicable to fishery products caught on board certain vessels in accordance with Article 3(1)(a)(i) of Directive 91/493/EEC(2). Those lists should be drawn up on the basis of a communication from the DLVS to the Commission.
(7) It is appropriate for this Decision to be applied 45 days after its publication providing for the necessary transitional period.
(8) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
The "Department of Livestock and Veterinary Services (DLVS)", shall be the competent authority in Zimbabwe identified for the purposes of verifying and certifying compliance of fishery products with the requirements of Directive 91/493/EEC.
Article 2
Fishery products imported into the Community from Zimbabwe shall meet the requirements set out in Articles 3, 4 and 5.
Article 3
1. Each consignment shall be accompanied by a numbered original health certificate in accordance with the model set out in Annex I and comprising a single sheet, duly completed, signed and dated.
2. The health certificate shall be drawn up in at least one official language of the Member State where the checks are carried out.
3. The health certificate shall bear the name, capacity and signature of the representative of the DLVS, and the latter's official stamp in a colour different from that of the endorsements.
Article 4
The fishery products shall come from approved establishments, factory vessels, or cold stores, or from registered freezer vessels listed in Annex II.
Article 5
All packages shall bear the word "ZIMBABWE" and the approval/registration number of the establishment, factory vessel, cold store or freezer vessel of origin in indelible letters, except in the case of frozen fishery products in bulk and intended for the manufacture of preserved foods.
Article 6
This Decision shall apply from 4 June 2004.
Article 7
This Decision is addressed to the Member States.
Done at Brussels, 13 April 2004.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 268, 24.9.1991, p. 15. Directive as last amended by Regulation (EC) No 806/2003 (OJ L 122, 16.5.2003, p. 1).
(2) OJ L 187, 7.7.1992, p. 41.
ANNEX I
>PIC FILE= "L_2004113EN.005102.TIF">
>PIC FILE= "L_2004113EN.005201.TIF">
ANNEX II
LIST OF ESTABLISHMENTS AND VESSELS
>TABLE>
Categorie Legend:
PP: Processing plant/Etablissement
