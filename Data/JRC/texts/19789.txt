Commission Regulation (EC) No 1461/2003
of 18 August 2003
laying down conditions for pilot projects for the electronic transmission of information on fishing activities and for remote sensing
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 2371/2002 of 20 December 2002 on the conservation and sustainable exploitation of fisheries resources under the Common Fisheries Policy(1), and in particular Article 22(3) and Article 23(5) thereof,
Whereas:
(1) Regulation (EC) No 2371/2002 requires the Council to decide in 2004 on the obligations to transmit electronically the information on fishing activities in order to improve the efficiency on management of fishing opportunities and to set up a means of remote sensing in order to detect fishing vessels at sea.
(2) Pilot projects relating respectively to the electronic transmission of information on fishing activities and to the remote sensing, can be carried out, in cooperation with the Commission, in order to assess the technology to be used, before 1 June 2004.
(3) It is therefore necessary, in order to ensure that Member States carry out these pilot projects, to lay down the conditions of application. In particular those conditions should cover, as regards the electronic transmission of information, the information to be recorded and reported by electronic means, and the functions of the systems on board the fishing vessels which record and transmit electronically the information concerned. As regards remote sensing, the conditions should also cover the functions of this system and the areas to be monitored during the pilot projects.
(4) The Commission needs to be informed by the Member States of the progress and the results of the pilot projects, in particular in order to assess the cost-effectiveness of the technologies to be used for improving the effectiveness of fisheries control.
(5) A financial contribution by the Community may be granted in respect of the pilot projects carried out by the Member States, under the conditions set out in Council Decision 2001/431/EC of 28 May 2001 on a financial contribution by the Community to certain expenditure incurred by the Member States in implementing the control, inspection and surveillance systems applicable to the common fisheries policy(2).
(6) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee of Fisheries and Aquaculture,
HAS ADOPTED THIS REGULATION:
CHAPTER I GENERAL PROVISIONS
Article 1
Subject matter
This Regulation lays down certain conditions for the carrying-out by Member States of the pilot projects provided for in Article 22(1)(c) and Article 23(3) of Regulation (EC) No 2371/2002 relating to the electronic transmission of information on fishing activities and to remote sensing.
Article 2
Time limits
1. The pilot projects shall be operational from 1 December 2003 at the latest and shall remain operational until at least 31 May 2004.
2. Projects relating to the electronic transmission of information on fishing activities and to remote sensing carried out by Member States prior to the date of entry into force of this Regulation are considered as pilot projects for the purposes of the present Regulation.
CHAPTER II ELECTRONIC TRANSMISSION OF INFORMATION ON FISHING ACTIVITIES
Article 3
Selection of fishing vessels
Member States carrying out pilot projects on the electronic transmission of information on fishing activities, shall select an appropriate number of vessels, preferably of different lengths.
Article 4
Reports to competent authority
Without prejudice of the obligations laid down in Community law, the master of Community fishing vessels participating in the pilot project shall record daily and report by electronic means to the competent authority of the flag Member State information on the trips and the fishing activities, indicating in particular:
(a) the quantities of each species caught and kept on board, where they exceed 50 kg of live-weight equivalent;
(b) the date and location of such catches;
(c) the type of gear used; and
(d) for the fishing vessels exceeding 18 metres in length overall and carrying out fishing activities in any areas where specific rules on access to waters and resources apply, the date and time of setting or re-setting of a static gear as well as the date and time of the completion of fishing operations using the static gear.
Article 5
Vessel equipment
Member States shall take the necessary steps to ensure that the vessels flying their flag and participating in the pilot project are equipped with systems capable of recording and transmitting electronically the information on their fishing activities during the pilot projects.
Article 6
Transmission of information
1. No later than one month before the projects become operational, Member States shall forward to the Commission:
(a) the complete reference of the competent authority designated to follow up the pilot projects;
(b) the list of participating vessels, containing for each vessel at least its internal fleet register number and its name;
(c) an outline of the technical characteristics of the onboard equipment; and
(d) a description on how data are collected and processed by the competent authority.
2. Once the projects are operational, Member States shall inform the Commission of any changes in the list of participating vessels.
3. The Commission shall communicate to Member States the data it has received under paragraphs (1) and (2).
Article 7
Processing of data on fishing activities
1. Member States shall take the necessary steps to ensure that the information transmitted electronically by the vessels flying their flag and participating in the project is processed by the competent authorities.
2. Whatever the system used, the authorities shall record the information in computer-readable form. Member States shall make sure that the data are kept on record until 31 December 2004.
CHAPTER III REMOTE SENSING
Article 8
Vessel detection systems
1. Member States establishing and testing vessel detection systems based on remote sensing technology (VDS) shall use remotely sensed images to detect fishing vessels at sea in the waters under their sovereignty or jurisdiction, as well as in other areas in which fishing vessels flying their flag may operate.
2. The pilot projects shall assess the capability of VDS for:
(a) using space-borne and airborne remotely sensed images to determine the number of fishing vessels and their position in a given area;
(b) cross-checking the positions of the fishing vessels detected by VDS with position reports from vessel monitoring systems (VMS), and
(c) signalling the possible presence of fishing vessels from which no position reports have been received through VMS.
Article 9
Maritime areas to be monitored
1. Member States shall decide upon the maritime area or areas to be monitored during the pilot projects.
2. No later than one month before the projects become operational, Member States shall communicate to the Commission details of the project, in particular the competent authority designated to follow up the pilot projects, the number of images and the areas of which these images will be taken. Member States shall justify their choice of the areas.
CHAPTER IV FINAL PROVISIONS
Article 10
Cooperation between Member States.
1. Member States may operate joint pilot projects.
2. The Commission shall monitor the progress of the pilot projects and facilitate the cooperation between Member States.
Article 11
Reports to the Commission
1. By 30 April 2004, each Member State shall submit to the Commission an assessment report on the pilot projects it has carried out, containing in particular a technical description of the systems implemented for the purpose of the pilot projects and how they inter-connect with existing systems for monitoring, control and surveillance.
2. By 31 July 2004, each Member State shall submit its final report, containing in particular details as to the cost-effectiveness and functioning of the systems, as well as their remarks on the use of the technologies involved.
Article 12
This Regulation shall enter into force on the seventh day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 18 August 2003.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 358, 31.12.2002, p. 59.
(2) OJ L 154, 9.6.2001, p. 22.
