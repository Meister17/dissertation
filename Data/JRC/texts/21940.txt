COMMISSION DECISION of 26 July 1978 establishing a list of epizootic diseases in accordance with Directive 72/462/EEC (78/685/EEC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Directive 72/462/EEC of 12 December 1972 on health and veterinary inspection problems upon importation of bovine animals and swine and fresh meat from third countries (1), and in particular Articles 12 (3) (c) and 29 thereof,
Whereas it may be necessary to proceed with the slaughter and destruction of all animals of a consignment which on arrival in the territory of the Community shows evidence or suspicion of an epizootic disease, in particular those which are exotic for the Community, in order to safeguard the health of man and animals and to avoid the very serious consequences which may result from the appearance or development of such diseases within the Community;
Whereas the measure provided for in this Decision is in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
All animals in a consignment presented for importation may be slaughtered and destroyed, when the health inspection carried out on the arrival of the animals in the territory of the Community in accordance with Article 12 (1) of Directive 72/462/EEC, shows evidence or suspicion of one of the epizootic diseases on the list figuring in the Annex hereto.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 26 July 1978.
For the Commission
Finn GUNDELACH
Vice-President (1)OJ No L 302, 31.12.1972, p. 28.
ANNEX
Swine : Foot-and-mouth disease : all types of virus
Classical and African swine fever
Swine vesicular disease
Teschen disease
Bovine animals : Foot-and-mouth disease : all types of virus
Rinderpest
