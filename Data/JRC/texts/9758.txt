Commission Decision
of 23 June 2006
establishing the Community’s financial contribution to the expenditure incurred in the context of the emergency measures taken to combat classical swine fever in Germany in 2002
(notified under document number C(2006) 2408)
(Only the German text is authentic)
(2006/433/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Decision 90/424/EEC of 26 June 1990 on expenditure in the veterinary field [1], and in particular Article 3(3) thereof,
Whereas:
(1) Outbreaks of classical swine fever occurred in Germany in 2002. The emergence of this disease presented a serious risk for the Community's livestock population.
(2) In order to prevent the spread of the disease and to help eradicate it as quickly as possible, the Community should contribute financially towards the eligible expenditure incurred by the Member State under the emergency measures taken to combat the disease, as provided for in Decision 90/424/EEC.
(3) Commission Decision 2003/745/EC of 13 October 2003 concerning a Community financial contribution towards the eradication of classical swine fever in Germany in 2002 [2] granted a financial contribution from the Community to Germany towards the expenditure incurred under the emergency measures to combat classical swine fever implemented in 2002.
(4) In accordance with that Decision, a first instalment of EUR 460000 was granted.
(5) Pursuant to that Decision, the balance of the Community contribution is to be based on the application submitted by Germany on 19 November 2003, documents setting out the figures quoted in the application, and the results of the in situ checks carried out by the Commission. The amount set out in the application submitted by Germany for the 2002 expenses was EUR 1933695,76, for which the Community financial contribution may not be higher than 50 % of the eligible expenditure.
(6) In view of the above considerations, the total amount of the Community’s financial contribution to the expenditure incurred associated with the eradication of classical swine fever in Germany in 2002 should now be fixed.
(7) The results of the checks carried out by the Commission in compliance with the Community veterinary rules and the conditions for granting Community financial support mean the entire amount of the expenditure submitted cannot be recognised as eligible.
(8) The Commission’s observations and method of calculating the eligible expenditure were communicated by letter to Germany.
(9) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
The total Community financial contribution towards the expenditure associated with eradicating classical swine fever in Germany in 2002 pursuant to Decision 2003/745/EC is fixed at EUR 925808,47.
Since a first instalment of EUR 460000 has already been granted in accordance with Decision 2003/745/EC, the balance of the Community financial contribution is fixed at EUR 465808,47.
Article 2
This Decision is addressed to the Federal Republic of Germany.
Done at Brussels, 23 June 2006.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 224, 18.8.1990, p. 19. Decision as last amended by Decision 2006/53/EC (OJ L 29, 2.2.2006, p. 37).
[2] OJ L 269, 21.10.2003, p. 18.
--------------------------------------------------
