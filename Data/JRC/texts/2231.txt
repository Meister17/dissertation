Authorisation for State aid pursuant to Articles 87 and 88 of the EC Treaty
Cases where the Commission raises no objections
(2005/C 131/04)
Date of adoption of the decision: 15 March 2005
Member State: Germany
Aid No: N 12/05
Title: Framework plan for joint task "Improving agricultural structures and coastal protection" for the period 2005-08; amendment 2005: promotion of the stability of the forests
Objective: The scheme Framework plan of the joint task "improvement of the agrarian structure and the coastal protection" for the period 2005 — 2008 will be modified with a measures for the promotion of the stability of the forests.
Legal basis: Gesetz über die Gemeinschaftsaufgabe "Verbesserung der Agrarstruktur und des Küstenschutzes", Rahmenplan "Verbesserung der Agrarstruktur und des Küstenschutzes"
Budget: EUR 1,1 billion per year (co-financed and nationally financed)
Aid intensity or amount: Variable
Duration: 2005-2008
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 12 April 2005
Member State: Germany
Aid No: N 13/05
Title: Framework plan for joint task "Improving agricultural structures and coastal protection" for the period 2005-08; extension until the year 2008
Objective: The approved scheme Framework plan of the joint task "improvement of the agrarian structure and the coastal protection" for the period 2004 — 2007; amendments 2004 (N 113/04) will be (partly) prolonged for one year.
Legal basis: Gesetz über die Gemeinschaftsaufgabe "Verbesserung der Agrarstruktur und des Küstenschutzes", Rahmenplan "Verbesserung der Agrarstruktur und des Küstenschutzes"
Budget: EUR 1,195 billion per year (co-financed and nationally financed)
Aid intensity or amount: Variable
Duration: 2005-2008
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 15 March 2005
Member State: United Kingdom (Northern Ireland)
Aid No: N 35/2005
Title: Extension to the Northern Ireland Beef Quality Initiative
Objective: The approved Northern Ireland Beef Quality Initiative (N 327/2002) will be prolonged by two years.
Legal basis: Administrative scheme, in accordance with the Budget (No 2) Act (Northern Ireland) 2002.
Budget: GBP 8 million (EUR 11,6 million)
Aid intensity or amount: Variable
Duration: Prolongation from 2006/07 to 2008/09 (new duration: 2002/03 to 2008/09)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 22 March 2005
Member State: Italy (Tuscany)
Aid No: N 50/2005
Title: Contributions to farmers taking part in the plan to monitor catarrhal fever in sheep (Blue tongue). Amendment to Regional Law No 25/2003.
Objective: Preventive measure within the framework of the plan to monitor and eradicate the epizootic disease known as Blue tongue.
Legal basis: Ordinanza ministeriale 11 maggio 2001 "Programma di vaccinazione per la febbre catarrale degli ovini (Blue tongue)"
Budget: EUR 600000
Aid intensity or amount: EUR 90 per animal per year.
Duration: 31 December 2005
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 6 April 2005
Member State: France
Aid No: N 86/2005
Title: Aid for traceability and quality improvement in the cereals sector. Amending and extending Aid N 358/2004.
Objective: To guarantee the quality of French cereal production.
Legal basis: Articles L 621-1 et suivants du Code rural
Budget: EUR 1300000
Aid intensity or amount: According to eligible costs: 15 %, 30 %
Duration: 1 year.
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
--------------------------------------------------
