Commission Regulation (EC) No 2037/2005
of 14 December 2005
amending the conditions for authorisation of a feed additive belonging to the group of coccidiostats
(Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Regulation (EC) No 1831/2003 of the European Parliament and of the Council of 22 September 2003 on additives for use in animal nutrition [1] and in particular Article 13(3) thereof,
Whereas:
(1) The additive lasalocid A sodium (Avatec 15 %) was authorised under certain conditions in accordance with Council Directive 70/524/EEC [2]. This additive is currently authorised in the group "coccidiostats" for turkeys by Commission Regulation (EC) No 2430/1999 [3] and chickens reared for laying and chickens for fattening by Commission Regulation (EC) No 1455/2004 [4]. This additive has been notified as existing product on the basis of Article 10 of Regulation (EC) No 1831/2003 and is subject to the verifications and the procedures in application of that provision.
(2) The company concerned submitted a new supplementary dossier requesting a modification of existing carrier.
(3) Regulation (EC) No 1831/2003 provides for the possibility to modify the authorisation of an additive further to an opinion of the European Food Safety Authority (the Authority) on whether the authorisation still meets the conditions set out in that Regulation.
(4) The Commission asked the Authority to evaluate the relevant data supporting the application for the change of authorisation referred to in Regulations (EC) Nos 2430/1999 and 1455/2004 and to advise on the possible harmful effects on safety and efficacy when lasalocid A sodium is used by new carrier. Following this request, the Authority has published on 26 August 2005 an opinion on the use of lasalocid A sodium in feedingstuffs.
(5) The opinion of the Authority concludes that the use of the new formulation would not be expected to introduce any additional risks or concerns for human, animal and environmental safety and that the new formulation does not adversely influence the stability of lasalocid A sodium.
(6) A Maximum Residue Level (MRL) for substance concerned has been established by Council Regulation (EEC) No 2377/90 of 26 June 1990 [5] laying down a Community procedure for the establishment of maximum residue levels of veterinary medicinal products in foodstuffs of animal origin.
(7) Regulations (EC) Nos 2430/1999 and 1455/2004 should therefore be amended accordingly.
(8) The measures provided for in this Regulation are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health.
HAS ADOPTED THIS REGULATION:
Article 1
In Annex I to Regulation (EC) No 2430/1999, the entry relating to E 763, lasalocid A sodium, is replaced by the text in Annex I to this Regulation.
Article 2
The Annex to Regulation (EC) No 1455/2004 is replaced by Annex II to this Regulation.
Article 3
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 14 December 2005.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 268, 18.10.2003, p. 29. Regulation as amended by Commission Regulation (EC) No 378/2005 (OJ L 59, 5.3.2005, p. 8).
[2] OJ L 270, 14.12.1970, p. 1. Directive repealed by Regulation (EC) No 1831/2003.
[3] OJ L 296, 17.11.1999, p. 3.
[4] OJ L 269, 17.8.2004, p. 14.
[5] OJ L 224, 18.8.1990, p. 1. Regulation as amended by Commission Regulation (EC) No 712/2005 (OJ L 120, 12.5.2005, p. 3).
--------------------------------------------------
ANNEX I
Registration number of additive | Name and registration number of person responsible for putting additive into circulation | Additive (Trade name) | Composition, chemical formula, description | Species or category of animal | Maximum age | Minimum content | Maximum content | Other provisions | End of period of authorisation | Maximum Residue Limits (MRLs) |
mg of active substance/kg of complete feedingstuff |
Coccidiostats and histomonostats
E 763 | Alpharma (Belgium) BVBA | Lasalocid A sodium 15 g/100 g (Avatec 15 % cc) | Additive composition:Lasalocid A sodium: 15 g/100 gCorn cob meal: 80,95 g/100 gLecithin: 2 g/100 gSoya oil: 2 g/100 gFerric oxide: 0,05 g/100 gActive substance:Lasalocid A sodium, C34H53O8Na,CAS number: 25999-20-6, sodium salt of 6-[(3R, 4S, 5S, 7R)-7-[(2S, 3S, 5S)-5-ethyl-5-[(2R, 5R, 6S)-5-ethyl-5-hydroxy-6-methyltetrahydro-2H-pyran2-yl]-tetrahydro-3-methyl-2-furyl]-4-hydroxy-3,5-dimethyl-6-oxononyl]-2,3-cresotic acid, produced by Streptomyces lasaliensis subsp. lasaliensis (ATCC 31180)Related impurities:Lasalocid sodium B-E: ≤ 10 % | Turkeys | 12 weeks | 90 | 125 | Use prohibited at least five days before slaughter. Indicate in the instructions for use: "Dangerous for equine species""This feedingstuff contains an ionophore: simultaneous use with certain medicinal substances can be contraindicated". | 30.9.2009 | Regulation (EEC) No 2377/90 |
Lasalocid A sodium 15 g/100 g (Avatec 150 G) | Additive composition:Lasalocid A sodium: 15 g/100 gCalcium sulphate dihydrate: 80,9 g/100 gCalcium lignosulphonate 4 g/100 gFerric oxide: 0,1 g/100 gActive substance:Lasalocid A sodium, C34H53O8Na,CAS number: 25999-20-6, sodium salt of 6-[(3R, 4S, 5S, 7R)-7-[(2S, 3S, 5S)-5-ethyl-5-[(2R, 5R, 6S)-5-ethyl-5-hydroxy-6-methyltetrahydro-2H-pyran2-yl]-tetrahydro-3-methyl-2-furyl]-4-hydroxy-3,5-dimethyl-6-oxononyl]-2,3-cresotic acid, produced by Streptomyces lasaliensis subsp. lasaliensis (ATCC 31180)Related impurities:Lasalocid sodium B-E: ≤ 10 % | Turkeys | 12 weeks | 90 | 125 | Use prohibited at least five days before slaughter. Indicate in the instructions for use: "Dangerous for equine species""This feedingstuff contains an ionophore: simultaneous use with certain medicinal substances can be contraindicated". | 30.9.2009 | Regulation (EEC) No 2377/90 |
--------------------------------------------------
ANNEX II
Registration number of additive | Name and registration number of person responsible for putting additive into circulation | Additive (Trade name) | Composition, chemical formula, description | Species or category of animal | Maximum age | Minimum content | Maximum content | Other provisions | End of period of authorisation | Maximum Residue Limits (MRLs) |
mg of active substance/kg of complete feedingstuff |
Coccidiostats and histomonostats
E 763 | Alpharma (Belgium) BVBA | Lasalocid A sodium 15 g/100 g (Avatec 15 % cc) | Additive composition:Lasalocid A sodium: 15 g/100 gCorn cob meal: 80,95 g/100 gLecithin: 2 g/100 gSoya oil: 2 g/100 gFerric oxide: 0,05 g/100 gActive substance:Lasalocid A sodium, C34H53O8Na,CAS number: 25999-20-6, sodium salt of 6-[(3R, 4S, 5S, 7R)-7-[(2S, 3S, 5S)-5-ethyl-5-[(2R, 5R, 6S)-5-ethyl-5-hydroxy-6-methyltetrahydro-2H-pyran2-yl]-tetrahydro-3-methyl-2-furyl]-4-hydroxy-3,5-dimethyl-6-oxononyl]-2,3-cresotic acid, produced by Streptomyces lasaliensis subsp. lasaliensis (ATCC 31180)Related impurities:Lasalocid sodium B-E: ≤ 10 % | Chickens for fattening | — | 75 | 125 | Use prohibited at least five days before slaughter. Indicate in the instructions for use: "Dangerous for equine species""This feedingstuff contains an ionophore: simultaneous use with certain medicinal substances can be contra-indicated". | 20.8.2014 | Regulation (EEC) No 2377/90 |
Chickens reared for laying | 16 weeks | 75 | 125 | 20.8.2014 |
Lasalocid A sodium 15 g/100 g (Avatec 150 G) | Additive composition:Lasalocid A sodium: 15 g/100 gCalcium sulphate dihydrate: 80,9 g/100 gCalcium lignosulphonate 4 g/100 gFerric oxide: 0,1 g/100 gActive substance:Lasalocid A sodium, C34H53O8Na,CAS number: 25999-20-6, sodium salt of 6-[(3R, 4S, 5S, 7R)-7-[(2S, 3S, 5S)-5-ethyl-5-[(2R, 5R, 6S)-5-ethyl-5-hydroxy-6-methyltetrahydro-2H-pyran2-yl]-tetrahydro-3-methyl-2-furyl]-4-hydroxy-3,5-dimethyl-6-oxononyl]-2,3-cresotic acid, produced by Streptomyces lasaliensis subsp. lasaliensis (ATCC 31180)Related impurities:Lasalocid sodium B-E: ≤ 10 % | Chickens for fattening | — | 75 | 125 | Use prohibited at least five days before slaughter. Indicate in the instructions for use: "Dangerous for equine species""This feedingstuff contains an ionophore: simultaneous use with certain medicinal substances can be contra-indicated". | 20.8.2014 | Regulation (EEC) No 2377/90 |
Chickens reared for laying | 16 weeks | 75 | 125 | 20.8.2014 |
--------------------------------------------------
