Authorisation for State aid pursuant to Articles 87 and 88 of the EC Treaty
Cases where the Commission raises no objections
(2006/C 276/08)
(Text with EEA relevance)
Date of adoption of the decision | 25.1.2006 |
Reference number of the aid | N 174/05 |
Member State | Spain |
Title | Environmental and R&D aid scheme for coalmining companies in the Autonomous Community of the Principality of Asturias |
Legal basis | Solicitud de ayuda, publicada anualmente por la Consejería de Industria, Comercio y Turismo del Gobierno del Principado de Asturias |
Objective | To support projects concerning research and technological development and environmental protection, the main objective being to improve safety |
Budget | EUR 1100000 annually |
Intensity | No individual aid may exceed EUR 300000 |
Duration | 2005-2007 |
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption of the decision | 22.9.2006 |
Reference number of the aid | N 234/06 |
Member State | Hungary |
Title | Az E85 bioüzemanyag jövedékiadó-kedvezménye |
Legal basis | A jövedéki adóról és a jövedéki termékek forgalmazásának különös szabályairól szóló 2003. évi CXXVII. törvény módosítása |
Type of measure | aid scheme |
Objective | environmental protection |
Form of aid | excise tax reduction |
Budget | Annually: HUF 440 million, Overall: HUF 2640 million |
Duration (period) | 1.1.2007- 31.12.2012 |
Economic sectors | biofuel production |
Name and address of the granting authority | Pénzügyminisztérium József Nádor tér 2-4. H-1051 Budapest |
Other information | commitment by the Member State to provide the Commission with annual reports |
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption of the decision | 22.6.2006 |
Reference number of the aid | N 296/06 |
Member State | France |
Region | Guadeloupe |
Title | Amendments to the social aid scheme established in 2004 to benefit certain categories of passengers on air services linking Guadeloupe to mainland France (N 385/2004) |
Legal basis | Article 60 de la loi no 2003-660 du 21 juillet 2003 de programme pour l'outre-mer |
Objective | Social aid scheme — air transport |
Budget | No impact on an estimated annual budget of around EUR 6 million |
Duration | 10 years |
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption of the decision | 4.7.2006 |
Reference number of the aid | N 390/05 |
Member State | Belgium |
Title | Construction of transhipment facilities on the railway line Lanaken — Maastricht |
Legal basis | Besluit van 15 december 2004 Euregio Benelux Middengebied, Besluit 11 juni 2004 Vlaamse regering. Décision du 15 décembre 2004 Euregio Benelux Middengebied, Décision du 11 juin 2004 gouvernement flamand |
Objective | Promotion of cross border freight transport |
Budget | EUR 5,9 million |
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption of the decision | 16.5.2006 |
Reference number of the aid | N 408/05 |
Member State | Finland |
Title | Amendments to existing aid regimes in Finland in favour of the maritime transport of cargo |
Legal basis | Laki ulkomaanliikenteen kauppa-alusluettelosta annetun lain muuttamisesta — Annettu Naantalissa 15 päivänä heinäkuuta 2005 |
Objective | Preserve competitiveness of the fleet and promote the maritime shipping "cluster" (employment of EC seafarers, preservation of maritime know-how in the EC and development of maritime skills and the improvement of safety) |
Budget | EUR 900000 per year (estimated impact of the notified measures) |
Duration | 10 years |
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption of the decision | 15.6.2006 |
Reference number of the aid | N 506/05 |
Member State | Italy |
Title | CIBA Specialità chimiche (Sasso Marconi) |
Legal basis | Decisione della Giunta Regionale n. 1002 del 22 aprile 2004 concernente la legge regionale n. 18/2003 |
Type of measure | Individual aid |
Objective | Environmental protection |
Form of aid | Direct grant |
Budget | Annual budget: —; Overall budget: EUR 880000 |
Intensity | 25 % |
Duration | 1 June 2006 — 30 June 2008 |
Economic sectors | Manufacturing industry |
Name and address of the granting authority | Ministero dell'Ambiente e della Tutela del Territorio |
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
--------------------------------------------------
