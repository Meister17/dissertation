Information communicated by Member States regarding State aid granted under Commission Regulation (EC) No 2204/2002 of 12 December 2002 on the application of Articles 87 and 88 of the EC Treaty to State aid for employment
(2005/C 278/10)
(Text with EEA relevance)
Aid No | XE 11/04 |
Member State | Poland |
Region | Entire country |
Title of aid scheme or name of company receiving individual aid | Compensation for the higher costs of employing disabled persons. |
Legal basis | Ustawa z dnia 27 sierpnia 1997 r. o rehabilitacji zawodowej i społecznej oraz zatrudnieniu osób niepełnosprawnych (Dz.U. nr 123, poz. 776 ze zm.) — art. 25 ust. 2, 3 i 3a, art. 26a ust. 1-5 oraz art. 26d ust. 1 w związku z art. 15, 17, 19, i 20 ust. 1; Rozporządzenie Rady Ministrów z dnia 18 maja 2004 r. w sprawie szczegółowych warunków udzielania pomocy przedsiębiorcom zatrudniającym osoby niepełnosprawne (Dz.U. nr 114, poz. 1194) |
Annual expenditure planned or overall amount of individual aid granted to the company | Annual overall amount | PLN 1900000 |
Loans guaranteed | |
Maximum aid intensity | In conformity with Articles 4(2)-(5), 5 and 6 of the Regulation | Yes | |
Date of implementation | 19.5.2004 |
Duration of scheme or individual aid award | Until 30.6.2007 |
Objective of aid | Art. 4: Creation of employment | No |
Art. 5: Recruitment of disadvantaged and disabled workers | No |
Art. 6: Employment of disabled workers | Yes |
Economic sectors concerned | —All Community sectors eligible for employment aid | Yes |
—All manufacturing | No |
—All services | No |
—Other | No |
Name and address of the granting authority | Name: Państwowy Fundusz Rehabilitacji Osób Niepełnosprawnych Address: Al. Jana Pawła II 13, Warsaw, tel. (48-22) 620 03 51 wew. 387 |
Name: Zakład Ubezpieczeń Społecznych Address: Ul. Czerniakowska 16, Warsaw |
Other information | The aid programme is co-financed by (reference) the European Social Fund within the framework of the Human Resource Development Operating Programme (2004-2006); Resources 1.4: Professional and social integration of disabled persons; Project: subsidised remuneration for companies which employ disabled persons with a view to compensating for the higher costs of employing the disabled/lower productivity. |
Aid subject to prior notification to the Commission | In conformity with Article 9 of the Regulation | Yes | |
Aid No | XE 14/04 |
Member State | Slovak Republic |
Region | 1.The regions eligible to be granted state aid under Objective 1 (Promoting the development and structural adjustment of regions whose development is lagging behind) are regions in which the per capita gross domestic product at purchasing power parity averaged over the preceding 3 years is less than 75 % of the average for the European Union. Since 1 March 2002 the regions with EC Treaty Article 87(3)(a) status are Western Slovakia (the territory of the Trnava, Nitra and Trenčín districts), Central Slovakia (the territory of the Žilina and Banská Bystrica districts) and Eastern Slovakia (the territory of the Košice and Prešov districts) .2.The regions eligible to be granted state aid under Objective 3 (Supporting the adaptation and modernisation of policies and systems of education, training and employment) are regions not covered by Objective 1. In accordance with Article 87(3)(c) of the EC Treaty, these are regions in which the per capita gross domestic product at purchasing power parity averaged over the preceding 3 years is over 75 % of the average for the European Union. Since 1 March 2002 Article 87(3)(c) status is enjoyed by the region demarcated by the territory of the Bratislava district. |
Title of aid scheme or name of company receiving individual aid | State aid scheme to support employment |
Legal basis | Právnym rámcom poskytovania štátnej pomoci na zamestnanosť je nariadenie Komisie (ES) č. 2204/2002 z 12. decembra 2002 o uplatňovaní článkov 87 a 88 Zmluvy o založení ES o štátnej pomoci na zamestnanosť a zákon NR SR č. 231/1999 Z. z. o štátnej pomoci v znení neskorších predpisov (ďalej len "zákon o štátnej pomoci"). Poskytovanie štátnej pomoci sa riadi aj ustanoveniami nariadenia Komisie (EK) č. 70/2001 z 12. januára 2001 o aplikácii článkov 87 a 88 Zmluvy o založení ES na štátnu pomoc malým a stredným podnikom a Multisektorálneho rámca regionálnej pomoci pre veľké investičné projekty, ako aj nariadenia (ES) Európskeho parlamentu a Rady č. 1784/1999 z 12. júla 1999 o Európskom sociálnom fonde a nariadenia Rady 1260/1999/ES ustanovujúceho všeobecné ustanovenia o štrukturálnych fondoch, zákonom č. 453/2003 Z. z. o orgánoch štátnej správy v oblasti sociálnych vecí, rodiny a služieb zamestnanosti a o zmene a doplnení niektorých zákonov, zákonom č. 599/2003 Z. z. o pomoci v hmotnej núdzi a o zmene a doplnení niektorých zákonov schváleným NR SR dňa 11. novembra 2003 s účinnosťou od 1. januára 2004, zákonom č. 5/2004 Z. z. o službách zamestnanosti a o zmene a doplnení niektorých zákonov v znení neskorších predpisov (ďalej len "zákon o službách zamestnanosti") |
Annual expenditure planned or overall amount of individual aid granted to the company | Annual expenditure planned (comprising ESF and state budget resources) | EUR 27,2 million |
Maximum aid intensity | In conformity with Articles 4(2)-(5), 5 and 6 of the Regulation | Yes | |
Date of implementation | 7.7.2004 — date of publication in the Obchodný vestník (Business Gazette) |
Duration of scheme or individual aid award | Until 31.12.2006 |
Objective of aid | Art. 4: Creation of employment | Yes |
Art. 5: Recruitment of disadvantaged and disabled workers | Yes |
Art. 6: Employment of disabled workers | Yes |
Economic sectors concerned | —All Community sectors eligible for employment aid | Yes |
—All manufacturing | Yes |
—All services | Yes |
—Other | Yes |
Name and address of the granting authority | Name: Ministry of Labour, Social Affairs and the Family Address: Špitálska 4-6 SK-816 43 Bratislava |
Name: Central Office for Labour, Social Affairs and the Family Address: Župné námestie 5-6 SK-812 67 Bratislava |
Other information | The State aid scheme is co-financed by the European Social Fund. |
Aid subject to prior notification to the Commission | In conformity with Article 9 of the Regulation | Yes | |
Aid No | XE 15/04 |
Member State | Poland |
Region | Entire country |
Title of aid scheme or name of company receiving individual aid | Intervention centres |
Legal basis | Rozporządzenie Ministra Gospodarki i Pracy z dnia 13 lipca 2004 r. w sprawie szczegółowego sposobu i trybu organizowania prac interwencyjnych i robót publicznych, jednorazowej refundacji kosztów z tytułu opłaconych składek na ubezpieczenia społeczne Rozporządzenie weszło w życie 16.7.2004 r. (Dz.U. nr 161 poz. 1683) Ustawa o promocji zatrudnienia i instytucjach rynku pracy Art. 60 |
Annual expenditure planned or overall amount of individual aid granted to the company | Annual overall amount | EUR 56,6 million |
Loans guaranteed | |
Maximum aid intensity | In conformity with Articles 4(2)-(5), 5 and 6 of the Regulation | Yes | |
Date of implementation | 16.7.2004 |
Duration of scheme or individual aid award | Until 31.12.2006 |
Objective of aid | Art. 4: Creation of employment | Yes |
Art. 5: Recruitment of disadvantaged and disabled workers | Yes |
Art. 6: Employment of disabled workers | |
Economic sectors concerned | —All Community sectors eligible for employment aid | Yes |
—All manufacturing | Yes |
—All services | Yes |
—Other | |
Name and address of the granting authority | Name: Starostowie Address: Approx. 400 agencies throughout Poland |
Other information | The programme enables intervention work to be organised, inter alia as part of the sectoral operation programme Human Resources Development (ESF) |
Aid subject to prior notification to the Commission | In conformity with Article 9 of the Regulation | Yes | |
Aid No | XE 16/04 |
Member State | Germany |
Region | Lower Saxony |
Title of aid scheme or name of company receiving individual aid | Directive on grants for "Youthplus businesses" |
Legal basis | § 44 Landeshaushaltsordnung |
Annual expenditure planned or overall amount of individual aid granted to the company | Annual overall amount | EUR 4,4 million planned |
Loans guaranteed | |
Maximum aid intensity | In conformity with Articles 4(2)-(5), 5 and 6 of the Regulation | Yes | |
Date of implementation | 1.9.2004 |
Duration of scheme or individual aid award | Until 30.6.2007 |
Objective of aid | Art. 4: creation of employment | Yes |
Art. 5: recruitment of disadvantaged and disabled workers | Yes |
Art. 6: employment of disabled workers | Yes |
Economic sectors concerned | —All Community sectors eligible for employment aid | Yes |
—All manufacturing | Yes |
—All services | Yes |
—Others | Yes |
Name and address of the granting authority | Name: Investitions- und Förderbank Niedersachsen GmbH Address: Günther-Wagner-Allee 14 DE-30177 Hannover Tel. (0511) 30 03 13 81 Email: edmund.rohde@nbank.de |
Other information | Is the scheme part-funded by Community resources: No |
Aid subject to prior notification to the Commission | In conformity with Article 9 of the Regulation | Yes | |
Aid No | XE 21/04 |
Member State | Poland |
Region | Entire country |
Title of aid scheme or name of company receiving individual aid | Job creation for the unemployed and allocation of funds to enable unemployed persons to start a business |
Legal basis | Rozporządzenie Ministra Gospodarki i Pracy z dnia 31 sierpnia 2004 r. w sprawie warunków i trybu refundacji ze środków Funduszu Pracy kosztów wyposażenia stanowiska pracy dla skierowanego bezrobotnego, przyznawania bezrobotnemu środków na podjęcie działalności gospodarczej i refundowanie kosztów pomocy prawnej, konsultacji i doradztwa oraz wymiaru dopuszczalnej pomocy (Dz.U. nr 196, poz. 2018) — wydane na podstawie art. 46 ust. 6 ustawy o promocji zatrudnienia i instytucjach rynku pracy. Rozporządzenie weszło w życie z dniem 8 września 2004 r. |
Annual expenditure planned or overall amount of individual aid granted to the company | Annual overall amount: 2004 — EUR 20 million 2005 — EUR 40 million Loans guaranteed |
Maximum aid intensity | In conformity with Articles 4(2)-(5), 5 and 6 of the Regulation | Yes | |
Date of implementation | 8.9.2004 |
Duration of scheme or individual aid award | Until 31.12.2006 |
Objective of aid | Art. 4: Creation of employment | Yes |
Art. 5: Recruitment of disadvantaged and disabled workers | Yes |
Art. 6: Employment of disabled workers | |
Economic sectors concerned | —All Community sectors eligible for employment aid | Yes |
—All manufacturing | Yes |
—All services | Yes |
—Other | Yes |
Name and address of the granting authority | Name: Starostowie Address: Approx. 340 agencies throughout Poland |
Other information | If the aid programme is co-financed by Community funds, please insert the following sentence: The aid programme is co-financed by [please indicate] — SPO RZL (EFS) |
Aid subject to prior notification to the Commission | In conformity with Article 9 of the Regulation | Yes | |
[1] With the exception of the shipbuilding sector and of other sectors subject to special rules in regulations and directives governing all State aid within the sector.
[2] With the exception of the shipbuilding sector and of other sectors subject to special rules in regulations and directives governing all State aid within the sector.
[3] With the exception of the shipbuilding sector and of other sectors subject to special rules in regulations and directives governing all State aid within the sector
[4] With the exception of the shipbuilding sector and of other sectors subject to special rules in regulations and directives governing all State aid within the sector.
[5] With the exception of: the shipbuilding sector, the mining industry and transport; aid to export-related activities, if the aid is directly linked to the quantities exported, to the establishment and operation of a distribution network or to other current expenditure linked to the export activity; aid contingent upon the use of domestic over imported goods.
--------------------------------------------------
