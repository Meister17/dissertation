COMMISSION DECISION of 12 May 1993 laying down the criteria for classifying third countries with regard to avian influenza and Newcastle disease
(93/342/EEC)THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Directive 90/539/EEC of 15 October 1990 on animal health conditions governing intra-Community trade in and imports from third countries of poultry and hatching eggs (1), as last amended by Directive 92/65/EEC (2), and in particular Article 22 (2) thereof,
Having regard to Council Directive 91/494/EEC of 26 June 1991 on animal health conditions governing intra-Community trade in and imports from third countries of fresh poultrymeat (3), as last amended by Directive 92/116/EEC (4), and in particular
Article 10
(2) thereof,
Whereas poultry, hatching eggs and poultry meat coming from third countries must be free from avian influenza and Newcastle disease; whereas it is necessary to establish the criteria for classifying third countries accordingly;
Whereas the criteria for third countries have to be established taking into account the provisions regarding Member States as laid down in Council Directives 92/40/EEC (5) and 92/66/EEC (6);
Whereas the measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
For the purpose of this Decision:
(a) 'Avian influenza' means infection as defined in Chapter I of Annex A;
(b) 'Newcastle disease' means infection as defined in Chapter II of Annex A;
(c) 'Recognized vaccine' means any vaccine against Newcastle disease complying with the criteria laid down in Annex B;
(d) 'Emergency vaccination' means vaccination used as a means of controlling the disease following one or more outbreaks and carried out:
(i) against avian influenza using any vaccine;
(ii) against Newcastle disease using non-recognized vaccines;
(e) 'Sanitary slaughter policy' means the application, in the event of outbreaks of avian influenza or Newcastle disease, of the measures provided for in Annex C;
(f) 'Commercial flock' means either any flock consisting of at least 200 birds or any other flocks from which the poultry, hatching eggs or meat is liable to be exported to the Community.
Article 2
A third country is classified as free from avian influenza and Newcastle disease if it fulfils the following general criteria:
(a) it must have a general animal-health structure allowing adequate monitoring of poultry flocks;
(b) it must have legislation which makes avian influenza and Newcastle disease notifiable diseases for all species of poultry and for all birds kept in captivity;
(c) it must undertake to examine closely any suspicion of those diseases;
(d) if there is suspicion, it must submit samples of each avian influenza virus or paramyxovirus found to specific laboratory testing in accordance with the procedure set out in Annex A;
(e) it must have at its disposal laboratory capacity in its own official laboratories or have arrangements with other national laboratories for rapid testing;
(f) it must allow those laboratories to be checked by experts from the Community;
(g) for each primary outbreak it must send virusisolates to the EEC reference laboratory in Weybridge (UK);
(h) it must notify to the Commission, within 24 hours after confirmation, of primary outbreaks in each region;
(i) it must send to the Commission, at least monthly in the case of secondary outbreaks, a report on the disease situation;
(j) in cases where vaccination against avian influenza and/or Newcastle disease is not forbidden, it must officially control the production, testing and distribution for the vaccines;
(k) it must communicate to the Commission the characteristics of each strain used for the production of vaccines against avian influenza or Newcastle disease.
Article 3
1. Without prejudice to the general criteria laid down in Article 2, a third country is classified as free from avian influenza if:
(a) no outbreaks of that disease have occurred in poultry on its territory for at least 36 months; and
(b) no vaccinations against avian influenza viruses of the same sub-types as those for which highly pathogenic viruses are known to exist (currently H5 and H7 sub-types) have been carried out for at least 12 months.
2. Where a sanitary slaughter policy is practised to control the disease and without prejudice to the provision of paragraph 1 (b) the period of 36 months referred to in paragraph 1 (a) is reduced to:
(a) 6 months if no emergency vaccination has been carried out;
(b) 12 months if emergency vaccination has been carried out, provided that a further 12-month period has elapsed after the official termination of such emergency vaccination.
Article 4
1. Without prejudice to the general criteria laid down in Article 2, a third country is classified as free from Newcastle disease for the first time if:
(a) no outbreaks of that disease have occurred in poultry on its territory for at least 36 months; and
(b) no vaccinations against Newcastle disease using non-recognized vaccines have been carried out for at least 12 months.
2. Where a sanitary slaughter policy is practised to control the disease and without prejudice to the provision of paragraph 1 (b) the period of 36 months referred to in paragraph 1 (a) is reduced to:
(a) 6 months if no emergency vaccination has been carried out;
(b) 12 months if emergency vaccination has been carried out, provided that a further 12-month period has elapsed after the official termination for such emergency vaccination.
3. By way of derogation from paragraphs 1 (a) and 2 (a), a third country is classified as free from Newcastle disease if the criteria set out in paragraphs 1 or 2 are met only for commercial flocks.
In such cases the third country concerned will be authorized to send fresh poultry meat to the Community if the additional guarantees laid down in Annex D are included in the accompanying animal health certificate. The export of live poultry and hatching eggs to the Community is not authorized in such cases.
4. By way of derogation from paragraphs 1 (b) and 2 (b) a third country is classified as free from Newcastle disease if it allows the use of vaccines against the disease, which, although complying with the general criteria set out in Annex B for the vaccines, do not fulfil the specific criteria thereof.
In such cases the third country concerned will be authorized to send live poultry and hatching eggs or fresh poultry meat to the Community if the additional guarantees laid down in Annex E or Annex F to this Decision are included in the accompanying animal health certificate.
Article 5
This Decision shall apply from 1 October 1993.
Article 6
This Decision is addressed to the Member States.
Done at Brussels, 12 May 1993.
For the Commission
René STEICHEN
Member of the Commission
(1) OJ No L 303, 31. 10. 1990, p. 6.
(2) OJ No L 268, 14. 9. 1992, p. 54.
(3) OJ No L 268, 24. 9. 1991, p. 35.
(4) OJ No L 62, 15. 3. 1993, p. 1.
(5) OJ No L 167, 22. 6. 1992, p. 1.
(6) OJ No L 260, 5. 9. 1992, p. 1.
ANNEX A
DEFINITIONS OF 'AVIAN INFLUENZA' AND 'NEWCASTLE DISEASE' CHAPTER I Avian influenza 'Avian influenza' means an infection of poultry caused by any influenza A virus which has an intravenous pathogenicity index (IVPI) in six-week-old chickens greater than 1,2 or any infection with influenza A virus of H5 or H7 sub-type for which nucleotide sequencing has demonstrated the presence of multiple basic amino acids at the cleavage site of the haemagglutinin.
The IVPI has to be determined according to the following method:
Intravenous pathogenicity index (IVPI)
1. Infective allantoic fluid from the lowest passage level available, preferably from the initial isolation without any selection, is diluted 101 in sterile isotonic saline.
2. 0,1 ml diluted virus is injected intravenously into each of 10 six-week-old chickens (specific pathogen free birds should be used).
3. Birds are examined at 24 hour intervals for 10 days.
4. At each observation each bird is recorded normal (O), sick (1), severely sick (2) or dead (3).
5. Record results and calculate index as shown in this example:
/* Tables: see OJ */
CHAPTER II Newcastle disease 'Newcastle disease' means an infection of poultry caused by any avian strain of the paramyxovirus 1 with an intracerebral pathogenicity index (ICPI) in one-day-old chicks greater than 0,7.
The ICPI has to be determined according to the following method:
Intracerebral pathogenicity index text (ICPI)
1. Infective freshly harvested allantoic fluid (HA titre must be greater than 24) is diluted 1: 10 in sterile isotonic saline (antibiotics must not be used).
2. 0,05 ml of the diluted virus is injected intracerebrally into each 10 one-day-old chicks (i.e. 24 - 40 hours after hatching). The chicks should be hatched from eggs obtained from a specific pathogen-free flock.
3. The birds are examined at intervals of 24 hours for eight days.
4. At each observation each bird is scored: 0 = normal; 1 = sick; 2 = dead.
5. The index is calculated as shown in the following example:
/* Tables: see OJ */
ANNEX B
CRITERIA FOR RECOGNISED VACCINES 1. General criteria
A. Vaccines have to be registered by the competent authorities of the third country concerned before being allowed to be distributed and used. For this registration the competent authorities have to rely on a complete file containing data about efficacy and innocuity; for imported vaccines the competent authorities can rely on data checked by the competent authorities of the country where the vaccine is produced, as far as these checks have been carried out in conformity with internationally accepted standards.
B. Furthermore importation or production and distribution of the vaccines have to be controlled by the competent authorities of the third country concerned.
C. Before distribution is allowed, each batch of vaccines has to be tested on innocuity, in particular regarding attenuation or inactivation and absence of undesired contaminating agents, and on efficacy on behalf of the competent authorities.
2. Specific criteria
A. Live attenuated Newcastle disease vaccines shall be prepared from a Newcastle disease virus strain for which the master seed has been tested and shown to have an intracerebral pathogenicity index (ICPI) of:
either
(i) less than 0,4 if not less than 107 EID50 are administered to each bird in the ICPI test;
or
(ii) less than 0,5 if not less than 108 EID50 are administered to each bird in the ICPI test.
B. Inactivated Newcastle disease vaccines shall be prepared form a Newcastle disease virus strain with an intracerebral pathogenicity index (ICPI) in one-day-old chicks of less than 0,7 if not less than 108 EID50 are administered to each bird in the ICPI test.
ANNEX C
MINIMAL MEASURES TO BE TAKEN WHEN SANITARY SLAUGHTER IS PRACTISED TO ELIMINATE OUTBREAKS OF AVIAN INFLUENZA OR NEWCASTLE DISEASE 1. In case of suspicion the holding concerned shall be placed under official supervision. This shall in particular mean that:
(a) all necessary samples shall be taken without delay and sent to a laboratory approved by the competent authorities for diagnosis;
(b) a record shall be made of all categories of poultry kept on the holding showing number of dead and diseased animals; this record shall be kept up-to-date and should be checked at each official visit;
(c) all poultry shall be kept isolated and, whenever possible, in their living quarters;
(d) no poultry is allowed to enter or to leave the holding;
(e) all movements of persons, vehicles, materials etc. onto and from the holding shall be subject to official authorization;
(f) table eggs can leave the holding after adequate disinfection or can be sent directly to an installation where they receive an adequate heat treatment;
(g) appropriate means of disinfection have to be used at the entrances of buildings housing poultry and of the holding itself;
(h) an epizootiological inquiry has to be carried out to discover the source of infection and its possible spread;
(i) possibly infected contact premises, i. e. these known from the inquiry meant in point (g) have to be put also under official surveillance.
2. As soon as the presence of the disease is officially confirmed on a holding, the following measures shall be taken in addition of those mentioned under point 1:
(a) all poultry on the holding shall be killed on the spot without any delay; their carcasses and eggs shall be destroyed; these operations have to be carried out in a way which minimizes the risk of spreading disease;
(b) any substance or waste liable to be contaminated shall be destroyed or treated in a way to ensure destruction of any virus present;
(c) the meat of poultry slaughtered during the presumed incubation period should be traced and destroyed;
(d) hatching eggs laid during the presumed incubation period should be traced and destroyed; poultry which hatched from such eggs shall be placed under official surveillance;
(e) after carrying out killing and destruction operations the premises shall be thoroughly cleansed and desinfected;
(f) no poultry shall be reintroduced onto the holding until at least 21 days after the end of the disinfection operations.
3. The operations mentioned in point 2 may be restricted to parts of holdings which form an epizootiological unit as far as the necessary guarantees exist to avoid spread of the disease to the non-infected units of the holding.
4. Around confirmed outbreaks of disease a protection zone with a minimum radius of 3 km and a surveillance zone with a minimum radius of 10 km shall be implemented. In these zones stand-still measures and controlled movements of poultry shall be in force until at least 21 days after the end of the disinfection operations on the infected holding. Before lifting the measures in these zones the authorities shall carry out the necessary inquiries and sampling of the poultry holdings to confirm that disease is no longer present in the region concerned.
5. The operations mentioned in this Annex have to be carried out by or under the supervision of the official veterinary authorities.
ANNEX D
ADDITIONAL GUARANTEES TO BE INCLUDED IN THE ANIMAL HEALTH CERTIFICATE FOR IMPORT OF FRESH POULTRY MEAT INTO THE COMMUNITY FROM THIRD COUNTRIES WHERE ARTICLE 4 (3) OF DECISION 93/342/EEC APPLIES The slaughter poultry flock from which the meat is issued,
(a) (i) either has not been vaccinated with vaccines against Newcastle disease which do not fulfil the specific requirements of Annex B point 2 of Decision 93/342/EEC (1)();
(ii) or has been vaccinated with vaccines which do not fulfil the specific requirements of Annex B point 2 of Decision 93/342/EEC, not less than 30 days before slaughter (*); and
(b) has undergone at slaughter, on the basis of an at random sample of cloacal swabs of at least 60 birds of each flock concerned, a virus isolation test for Newcastle disease in which no avian paramyxoviruses with an intracerebral pathogenicity index (ICPI) of more than 0,4 have been found; and
(c) has not been in contact during the period of 30 days preceding slaughter with poultry which do not fulfil the guarantees mentioned under (a) and (b).
(1)() Delete the unnecessary reference.
ANNEX E
ADDITIONAL GUARANTEES TO BE INCLUDED IN THE ANIMAL HEALTH CERTIFICATE FOR IMPORT OF LIVE POULTRY OR HATCHING EGGS INTO THE COMMUNITY FROM THIRD COUNTRIES WHERE ARTICLE 4 (4) OF DECISION 93/342/EEC APPLIES Although the use of vaccines against Newcastle disease which do not fulfil the specific requirements of point 2 of Annex B of Decision 93/342/EEC, is not prohibited in .................... (*), the
- live poultry (1)(),
- breeding poultry from which the hatching eggs (**)/one-day-old chicks (**) are issued (**)
(a) (i) either have not been vaccinated for at least 12 months with such vaccines (**),
(ii) or have been vaccinated with such vaccines within 12 months but not less than 60 days before consignment (**) or collection of hatching eggs (**), in which case the poultry of the flock of origin has undergone during the 14 days preceding consignment or collecting of eggs, on the basis of an at random sample of cloacal swabs of at least 60 birds of each flock concerned, a virus isolation test for Newcastle disease in which no avian paramyxoviruses with an Intracerebral Pathogenicity Index (ICPI) of more than 0,4 have been found (**); and
(b) have not been in contact during the periods of respectively 12 months or 60 days mentioned in (a) (i) or (a) (ii) with poultry which do not fulfil the guarantees mentioned respectively under (a) (i) or (a) (ii); and
(c) have been isolated under official surveillance on the holding of origin during the 14 days period mentioned in (a) (ii); and
(d) when it concerns export of one-day-old chicks, the hatching eggs from which they hatched have not been in contact in the hatchery or during transport with eggs or poultry which do not fulfil the abovementioned guarantees.
(1)() Name of the country of origin.
(2)() Delete the unnecessary reference.
ANNEX F
ADDITIONAL GUARANTEES TO BE INCLUDED IN THE ANIMAL HEALTH CERTIFICATE FOR IMPORT OF FRESH POULTRY MEAT INTO THE COMMUNITY FROM THIRD COUNTRIES WHERE ARTICLE 4 (4) OF DECISION 93/342/EEC APPLIES Although the use of vaccines against Newcastle disease which do not fulfil the specific requirements of Point 2 of Annex B of Decision 93/342/EEC, is not prohibited in .................... (*), the slaughter poultry from which the meat is issued
(a) (i) either have not been vaccinated for at least 12 months with such vaccines (**);
(ii) or have been vaccinated with such vaccines within 12 months but not less than 30 days before slaughter in which case the poultry of the flock of origin has undergone at slaughter, on the basis of an at random sample of cloacal swabs of at least 60 birds of each flock concerned, a virus isolation test for Newcastle disease in which no avian paramyxoviruses with an ICPI of more that 0,4 have been found (**); and
(b) have not been in contact during the periods of respectively 12 months or 30 days mentioned in (a) (i) or (a) (ii) with poultry which do not fulfil the guaranees mentioned respectively under (a) (i) or (a) (ii).
