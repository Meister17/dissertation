Information relating to the entry into force of the Agreement in the form of an Exchange of Letters between the European Community and the Republic of Korea pursuant to Article XXIV:6 and Article XXVIII of the General Agreement on Tariffs and Trade (GATT) 1994
The Agreement in the form of an Exchange of Letters between the European Community and the Republic of Korea pursuant to Article XXIV:6 and Article XXVIII of the General Agreement on Tariffs and Trade (GATT) 1994 (OJ L 340, 23.12.2005) has entered into force on 13 December 2005.
--------------------------------------------------
