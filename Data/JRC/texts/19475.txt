Commission Directive 2003/79/EC
of 13 August 2003
amending Council Directive 91/414/EEC to include Coniothyrium minitans as active substance
(Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/414/EEC of 15 July 1991 concerning the placing of plant protection products on the market(1), as last amended by Commission Directive 2003/70/EC(2), and in particular Article 6(1) thereof,
Whereas:
(1) In accordance with Article 6(2) of Directive 91/414/EEC the German authorities received on 10 September 1997 an application from Prophyta GmbH, hereafter referred to as the applicant, for the inclusion of the active substance Coniothyrium minitans in Annex I to the Directive. Commission Decision 98/676/EC(3) confirmed that the dossier was "complete" in the sense that it could be considered as satisfying, in principle, the data and information requirements of Annexes II and III to Directive 91/414/EEC.
(2) For this active substance, the effects on human health and the environment have been assessed, in accordance with the provisions of Article 6(2) and (4) of Directive 91/414/EEC, for the uses proposed by the applicant. The nominated rapporteur Member State submitted a draft assessment report concerning the substance to the Commission on 13 March 2000.
(3) The draft assessment report was reviewed by the Member States and the Commission within the Standing Committee on the Food Chain and Animal Health. The review was finalised on 4 July 2003 in the format of the Commission review report for Coniothyrium minitans.
(4) The review did not reveal any open questions or concerns, which would have required a consultation of the European Food Safety Authority.
(5) It has appeared from the various examinations made that plant protection products containing Coniothyrium minitans may be expected to satisfy, in general, the requirements laid down in Article 5(1)(a) and (b) of Directive 91/414/EEC in the light of Article 5(3) thereof, in particular with regard to the uses which were examined and detailed in the Commission review report. It is therefore appropriate to include this active substances in Annex I, in order to ensure that in all Member States the authorisations of plant protection products containing this active substance can be granted in accordance with the provisions of that Directive.
(6) Although the Uniform Principles have still to be adopted for micro-organisms, it is appropriate that the Member States should apply the general provisions of Article 4 of the Directive when granting authorisations. It is appropriate to provide that the finalised review report, except for confidential information within the meaning of Article 14 of Directive 91/414/EEC, is kept available or made available by the Member States for consultation by any interested parties.
(7) After inclusion, Member States should be allowed a reasonable period to implement the provisions of Directive 91/414/EEC as regards plant protection products containing Coniothyrium minitans and in particular to review existing provisional authorisations and, by the end of this period at the latest, to transform those authorisations into full authorisations, to amend them or to withdraw them in accordance with the provisions of Directive 91/414/EEC.
(8) It is therefore appropriate to amend Directive 91/414/EEC accordingly.
(9) The measures provided for in this Directive are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Annex I to Directive 91/414/EEC is amended as set out in the Annex to this Directive.
Article 2
Member States shall adopt and publish by 30 June 2004 at the latest the laws, regulations and administrative provisions necessary to comply with this Directive. They shall forthwith inform the Commission thereof.
They shall apply those provisions from 1 July 2004.
When Member States adopt those provisions, they shall contain a reference to this Directive or shall be accompanied by such a reference on the occasion of their official publication. Member States shall determine how such reference is to be made.
Article 3
1. Member States shall review the authorisation for each plant protection product containing Coniothyrium minitans to ensure that the conditions relating to this active substance set out in Annex I to Directive 91/414/EEC are complied with. Where necessary, they shall amend or withdraw authorisations in accordance with Directive 91/414/EEC by 30 June 2004 at the latest.
2. For each authorised plant protection product containing Coniothyrium minitans as either the only active substance or as one of several active substances all of which were listed in Annex I to Directive 91/414/EEC by 31 December 2004 at the latest, Member States shall re-evaluate the product on the basis of a dossier satisfying the requirements of Annex III thereto. On the basis of that evaluation, they shall determine whether the product satisfies the conditions set out in Article 4(1)(b), (c), (d) and (e) of Directive 91/414/EEC. Where necessary and by 30 June 2005 at the latest, they shall amend or withdraw the authorisation for each such plant protection product.
Article 4
This Directive shall enter into force on 1 January 2004.
Article 5
This Directive is addressed to the Member States.
Done at Brussels, 13 August 2003.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 230, 19.8.1991, p. 1.
(2) OJ L 184, 23.7.2003, p. 9.
(3) OJ L 317, 26.11.1998, p. 47.
ANNEX
In Annex I the following row is added at the end of the table.
">TABLE>"
