COUNCIL DECISION of 20 December 1979 setting up a consultation procedure on relations between Member States and third countries in the field of air transport and on action relating to such matters within international organizations (80/50/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 84 (2) thereof,
Having regard to the draft decision submitted by the Commission (1),
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas, to make allowance for developments affecting air transport and for their consequences for the Member States, it is desirable that problems of common interest with regard to relations between Member States and third countries in air transport and to action relating to such matters in international organizations should be identified in good time;
Whereas it is desirable to facilitate exchanges of information and consultations in this field and to promote coordination, where appropriate, of the action taken by the Member States within international organizations;
Whereas each Member State should give the benefit of its experience in relations with third countries in air transport to the other Member States and to the Commission;
Whereas information in this field is regularly exchanged in certain international organizations ; whereas these procedures should be supplemented at Community level by exchanges of information between the Member States and the Commission,
HAS ADOPTED THIS DECISION:
Article 1
The Member States and the Commission shall consult each other, at the request of a Member State or the Commission, in accordance with the procedures laid down in this Decision: (a) on air transport questions dealt with in international organization ; and
(b) on the various aspects of developments which have taken place in relations between Member States and third countries in air transport, and on the functioning of the significant elements of bilateral or multilateral agreements concluded in this field.
The consultations shall be held within one month of the request or as soon as possible in urgent cases.
Article 2
1. The main aims of the consultations provided for in Article 1 under (a) shall be: (a) to determine jointly whether the questions raise problems of common interest ; and
(b) depending upon the nature of such problems: - to consider jointly whether Member States' action within the international organizations concerned should be coordinated, or
- to consider jointly any other approach which might be appropriate.
2. The Member States and the Commission shall as soon as possible exchange any information of relevance to the aims described in paragraph 1.
Article 3
1. The main aims of the consultations provided for in Article 1 (b) shall be to examine the relevant issues and to consider any approach which might be appropriate.
2. For the purposes of the consultations referred to in paragraph 1, each Member State shall inform the other Member States and the Commission of developments which have taken place in the field of air transport and of the operation of bilateral or multilateral agreements concluded in that field, if it considers this likely to contribute to the identification of problems of common interest. (1)OJ No C 193, 31.7.1979, p. 9. (2)OJ No C 309, 10.12.1979, p. 58. (3)Opinion delivered on 21 November 1979 (not yet published in the Official Journal).
3. The Commission shall provide the Member States with any information it possesses regarding the matters referred to in paragraph 2.
Article 4
1. The exchange of information provided for in this Decision shall take place through the General Secretariat of the Council.
2. The consultations provided for in this Decision shall take place within the framework of the Council.
3. The information and consultations provided for in this Decision shall be covered by professional secrecy.
Article 5
At the end of a period of three years immediately following the date of notification of this Decision, the Council shall re-examine the consultation procedure with a view to amending or supplementing it, if experience shows this to be desirable.
Article 6
This Decision is addressed to the Member States.
Done at Brussels, 20 December 1979.
For the Council
The President
J. TUNNEY
