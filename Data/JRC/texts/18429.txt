*****
COMMISSION REGULATION (EEC) No 2288/83
of 29 July 1983
establishing the list of biological or chemical substances provided for in Article 60 (1) (b) of Council Regulation (EEC) No 918/83 setting up a Community system of reliefs from customs duty
THE COMMISSION OF THE EUROPEAN
COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 918/83 of setting up a Community system of reliefs from customs duty (1), and in particular Article 143 thereof,
Whereas Article 60 (1) (b) and (2) of Regulation (EEC) No 918/83 provides for admission with relief from import duties for biological or chemical substances imported exclusively for non-commercial purposes by public establishments, or those departments of public establishments, or by officially authorized private establishments, whose principal activity is education or scientific research; whereas such admission with relief from import duties is limited, however, to biological or chemical substances for which there is no equivalent production in the customs territory of the Community and which are included in a list drawn up in accordance with the procedure laid down in Article 143 (2) and (3) of the above Regulation;
Whereas according to information obtained from the Member States there is no equivalent production in the customs territory of the Community of the biological or chemical substances listed in the Annex to this Regulation;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Committee on Duty-Free Arrangements,
HAS ADOPTED THIS REGULATION:
Article 1
The list of biological or chemical substances eligible for admission with relief from import duty provided for in Article 60 (1) (b) of Regulation (EEC) No 918/83 is set out in the Annex to this Regulation.
Article 2
This Regulation shall enter into force on 1 July 1984.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 29 July 1983.
For the Commission
Karl-Heinz NARJES
Member of the Commission
(1) OJ No L 105, 23. 4. 1983, p. 1.
ANNEX
1.2.3 // // // // Reference No // CCT heading No // Description // // // // 20273 // 29.01 A // 3-Methylpent-1-ene // 20274 // 29.01 A // 4-Methylpent-1-ene // 20275 // 29.01 A // 2-Methylpent-2-ene // 20276 // 29.01 A // 3-Methylpent-2-ene // 20277 // 29.01 A // 4-Methylpent-2-ene // 21154 // 29.01 A // Oct-2-ene // 25634 // 29.01 C II // P-Mentha-1(7),2-diene (Beta-Phellandrene) // 14364 // 29.24 B // Decamethonium bromide (INN) // 20641 // 29.27 // 1-Naphtonitrile // 20642 // 29.27 // 2-Naphtonitrile // 14769 // 29.02 C // 4,4'-Dibromobiphenyl // 17305 // 29.03 A // Ethyl methanesulphonate // 22830 // 29.38 B I // Retinyl acetate // 18892 // 29.05 A III // Myo-inositol (Meso-inositol) // 21887 // 35.07 // Phosphoglucomutase // 19057 // 35.07 // Lactate dehydrogenase // 20193 // 29.10 B // Methyl-alpha-D-mannoside // // //
