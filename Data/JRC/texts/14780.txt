Action brought on 17 January 2006 by the Commission of the European Communities against the Grand Duchy of Luxembourg
An action against the Grand Duchy of Luxembourg was brought before the Court of Justice of the European Communities on 17 January 2006 by the Commission of the European Communities, represented by Michel Nolin, acting as Agent, with an address for service in Luxembourg.
The Commission claims that the Court should:
1. declare that, by failing to adopt the laws, regulations and administrative provisions necessary to comply with Commission Directive 2004/105/EC of 15 October 2004 determining the models of official phytosanitary certificates or phytosanitary certificates for re-export accompanying plants, plant products or other objects from third countries and listed in Council Directive 2000/29/EC [1] or, in any event, by failing to communicate those provisions to the Commission, the Grand Duchy of Luxembourg has failed to fulfil its obligations under that directive;
2. order the Grand Duchy of Luxembourg to pay the costs.
Pleas in law and main arguments
The period for transposition of Directive 2004/105 expired on 31 December 2004.
[1] OJ 2004 L 319, p. 9
--------------------------------------------------
