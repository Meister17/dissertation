*****
COUNCIL DIRECTIVE
of 24 July 1986
on the implementation of the principle of equal treatment for men and women in occupational social security schemes
(86/378/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Articles 100 and 235 thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas the Treaty provides that each Member State shall ensure the application of the principle that men and women should receive equal pay for equal work; whereas 'pay' should be taken to mean the ordinary basic or minimum wage or salary and any other consideration, whether in cash or in kind, which the worker receives, directly of indirectly, from his employer in respect of his employment;
Whereas, although the principle of equal pay does indeed apply directly in cases where discrimination can be determined solely on the basis of the criteria of equal treatment and equal pay, there are also situations in which implementation of this principle implies the adoption of additional measures which more clearly define its scope;
Whereas Article 1 (2) of Council Directive 76/207/EEC of 9 February 1976 on the implementation of the principle of equal treatment for men and women as regards access to employment, vocational training and promotion, and working conditions (4) provides that, with a view to ensuring the progressive implementation of the principle of equal treatment in matters of social security, the Council, acting on a proposal from the Commission, will adopt provisions defining its substance, its scope and the arrangements for its application; whereas the Council adopted to this end Directive 79/7/EEC of 19 December 1978 on the progressive implementation of the principle of equal treatment for men and women in matters of social security (5);
Whereas Article 3 (3) of Directive 79/7/EEC provides that, with a view to ensuring implementation of the principle of equal treatment in occupational schemes, the Council, acting on a proposal from the Commission, will adopt provisions defining its substance, its scope and the arrangements for its application;
Whereas the principle of equal treatment should be implemented in occupational social security schemes which provide protection against the risks specified in Article 3 (1) of Directive 79/7/EEC as well as those which provide employees with any other consideration in cash or in kind within the meaning of the Treaty;
Whereas implementation of the principle of equal treatment does not prejudice the provisions relating to the protection of women by reason of maternity,
HAS ADOPTED THIS DIRECTIVE:
Article 1
The object of this Directive is to implement, in occupational social security schemes, the principle of equal treatment for men and women, hereinafter referred to as 'the principle of equal treatment'.
Article 2
1. 'Occupational social security schemes' means schemes not governed by Directive 79/7/EEC whose purpose is to provide workers, whether employees or self-employed, in an undertaking or group of undertakings, area of economic activity or occupational sector or group of such sectors with benefits intended to supplement the benefits provided by statutory social security schemes or to replace them, whether membership of such schemes is compulsory or optional.
2. This Directive does not apply to:
(a) individual contracts,
(b) schemes having only one member,
(c) in the case of salaried workers, insurance schemes offered to participants individually to guarantee them:
- either additional benefits, or
- a choice of date on which the normal benefits will start, or a choice between several benefits.
Article 3
This Directive shall apply to members of the working population including self-employed persons, persons whose activity is interrupted by illness, matrnity, accident or involuntary unemployment and persons seeking employment, and to retired and disabled workers.
Article 4
This Directive shall apply to:
(a) occupational schemes which provide protection against the following risks:
- sickness,
- invalidity,
- old age, including early retirement,
- industrial accidents and occupational diseases,
- unemployment;
(b) occupational schemes which provide for other social benefits, in cash or in kind, and in particular survivors' benefits and family allowances, if such benefits are accorded to employed persons and thus constitute a consideration paid by the employer to the worker by reason of the latter's employment.
Article 5
1. Unter the conditions laid down in the following provisions, the principle of equal treatment implies that there shall be no discrimination on the basis of sex, either directly or indirectly, by reference in particular to marital or family status, especially as regards:
- the scope of the schemes and the conditions of access to them;
- the obligation to contribute and the calculation of contributions;
- the calculation of benefits, including supplementary benefits due in respect of a spouse or dependants, and the conditions governing the duration and retention of entitlement to benefits.
2. The principle of equal treatment shall not prejudice the provisions relating to the protection of women by reason of maternity.
Article 6
1. Provisions contrary to the principle of equal treatment shall include those based on sex, either directly or indirectly, in particular by reference to marital or family for:
(a) determining the persons who may participate in an occupational scheme;
(b) fixing the compulsory or optional nature of participation in an occupational scheme;
(c) laying down different rules as regards the age of entry into the scheme or the minimum period of employment or membership of the scheme required to obtain the benefits thereof;
(d) laying down different rules, except as provided for in subparagraphs (h) and (i), for the reimbursement of contributions where a worker leaves a scheme without having fulfilled the conditions guaranteeing him a deferred right to long-term benefits;
(e) setting different conditions for the granting of benefits of restricting such benefits to workers of one or other of the sexes;
(f) fixing different retirement ages;
(g) suspending the retention or acquisition of rights during periods of maternity leave or leave for family reasons which are granted by law or agreement and are paid by the employer;
(h) setting different levels of benefit, except insofar as may be necessary to take account of actuarial calculation factors which differ according to sex in the case of benefits designated as contribution-defined; (i) setting different levels of worker contribution;
setting different levels of employer contribution in the case of benefits designated as contribution-defined, except with a view to making the amount of those benefits more nearly equal;
(j) laying down different standards or standards applicable only to workers of a specified sex, except as provided for in subparagraphs (h) and (i), as regards the guarantee or retention of entitlement to deferred benefits when a worker leaves a scheme.
2. Where the granting of benefits within the scope of this Directive is left to the discretion of the scheme's management bodies, the latter must take account of the principle of equal treatment.
Article 7
Member States shall take all necessary steps to ensure that:
(a) provisions contrary to the principle of equal treatment in legally compulsory collective agreements, staff rules of undertakings or any other arrangements relating to occupational schemes are null and void, or may be declared null and void or amended;
(b) schemes containing such provisions may not be approved or extended by administrative measures.
Article 8
1. Member States shall take all necessary steps to ensure that the provisions of occupational schemes contrary to the principle of equal treatment are revised by 1 January 1993.
2. This Directive shall not preclude rights and obligations relating to a period of membership of an occupational scheme prior to revision of that scheme from remaining subject to the provisions of the scheme in force during that period.
Article 9
Member States may defer compulsory application of the principle of equal treatment with regard to:
(a) determination of pensionable age for the purposes of granting old-age or retirement pensions, and the possible implications for other benefits:
- either until the date on which such equality is achieved in statutory schemes,
- or, at the latest, until such equality is required by a directive.
(b) survivors' pensions until a directive requires the principle of equal treatment in statutory social security schemes in that regard;
(c) the application of the first subparagraph of Article 6 (1) (i) to take account of the different actuarial calculation factors, at the latest until the expiry of a thirteen-year period as from the notification of this Directive.
Article 10
Member States shall introduce into their national legal systems such measures as are necessary to enable all persons who consider themselves injured by failure to apply the principle of equal treatment to pursue their claims before the courts, possibly after bringing the matters before other competent authorities.
Article 11
Member States shall take all the necessary steps to protect worker against dismissal where this constitutes a response on the part of the employer to a complaint made at undertaking level or to the institution of legal proceedings aimed at enforcing compliance with the principle of equal treatment.
Article 12
1. Member States shall bring into force such laws, regulations and administrative provisions as are necessary in order to comply with this Directive at the latest three years after notification thereof (1). They shall immediately inform the Commission thereof.
2. Member States shall communicate to the Commission at the latest five years after notification of this Directive all information necessary to enable the Commission to draw up a report on the application of this Directive for submission to the Council.
Article 13
This Directive is addressed to the Member States.
Done at Brussels, 24 July 1986.
For the Council
The President
A. CLARK
(1) OJ No C 134, 21. 5. 1983, p. 7.
(2) OJ No C 117, 30. 4. 1984, p. 169.
(3) OJ No C 35, 9. 2. 1984, p. 7.
(4) OJ No L 39, 14. 2. 1976, p. 40.
(5) OJ No L 6, 10. 1. 1979, p. 24.
(1) This Directive was notified to the Member States on 30 July 1986
