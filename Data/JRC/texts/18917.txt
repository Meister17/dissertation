Commission Regulation (EC) No 1948/2003
of 4 November 2003
amending Regulation (EC) No 174/1999 as regards export licences and export refunds for cheese intended for Croatia and Russia, and derogating from that Regulation
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1255/1999 of 17 May 1999 on the common organisation of the market in milk and milk products(1), as last amended by Regulation (EC) No 1787/2003(2), and in particular Article 26(3) and Article 31(14) thereof,
Whereas:
(1) In order to permit special measures differentiated according to destination to be adopted, Article 15 of Commission Regulation (EC) No 174/1999 of 26 January 1999 laying down special detailed rules for the application of Council Regulation (EEC) No 804/68 as regards export licences and export refunds in the case of milk and milk products(3), as last amended by Regulation (EC) No 1392/2003(4), stipulates that licences for cheese exports are to be issued for one destination zone only and are valid only for the countries in that zone.
(2) Under the provisions of Article 15(3) of Regulation (EC) No 174/1999 Croatia is one of the destination countries in zone I. In order to stabilise trade flows in cheese to that country Commission Regulation (EC) No 951/2003(5) lays down special measures including a restriction on the period of validity of export licences. For the purpose of following market developments more closely, Croatia should be made a separate zone so that special measures can be taken for that destination. In addition, so as not to jeopardise the effectiveness of the measures the period of validity of the export licences issued for the destination Croatia should be limited.
(3) Under the same provisions, Russia is one of the destination countries in zone VI. In view of the development of exports and export licence applications for cheese to Russia, that country should be made a separate zone so that special measures can be taken for that destination.
(4) Regulation (EC) No 174/1999 should be amended and derogated from accordingly, and Regulation (EC) No 951/2003 should be repealed.
(5) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Milk and Milk Products,
HAS ADOPTED THIS REGULATION:
Article 1
In Article 15 of Regulation (EC) No 174/1999, paragraph 3 is replaced by the following:
"3. The following zones shall be defined for the purposes of paragraph 1:
(a) zone I: destination codes 070, 091 and 093 to 096 inclusive;
(b) zone II: destination code 092;
(c) zone III: destination code 400;
(d) zone IV: destination code 075;
(e) zone VI: all other destination codes."
Article 2
By derogation from point (c) in Article 6 of Regulation (EC) No 174/1999, the period of validity of export licences with advance fixing of the refund which are issued from 1 December 2003 to 31 December 2003 to cover products falling within CN code 0406 with destination Croatia, shall expire on 31 December 2003.
Article 3
Regulation (EC) No 951/2003 is hereby repealed.
Article 4
This Regulation shall enter into force on the day following that of its publication in the Official Journal of the European Union.
Articles 2 and 3 shall apply from 1 December 2003.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 4 November 2003.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 160, 26.6.1999, p. 48.
(2) OJ L 270, 21.10.2003, p. 121.
(3) OJ L 20, 27.1.1999, p. 8.
(4) OJ L 197, 5.8.2003, p. 3.
(5) OJ L 133, 29.5.2003, p. 82.
