Commission Decision
of 3 April 2002
amending Decision 2001/781/EC adopting a manual of receiving agencies and a glossary of documents that may be served under Council Regulation (EC) No 1348/2000 on the service in the Member States of judicial and extrajudicial documents in civil or commercial matters
(Notified under document number C(2002) 1132)
(2002/350/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1348/2000 of 29 May 2000 on the service in the Member States of judicial and extrajudicial documents in civil or commercial matters(1), and in particular points (a) and (b) of Article 17 thereof,
Whereas:
(1) In order to implement Regulation (EC) No 1348/2000 it was necessary to draw up and publish a manual containing information about the receiving agencies provided for in Article 2 of that Regulation.
(2) Point (b) of Article 17 of Regulation (EC) No 1348/2000 also provides for a glossary to be drawn up in the official languages of the European Union of documents that may be served on the basis of the Regulation.
(3) In accordance with Regulation (EC) No 1348/2000, Commission Decision 2001/781/EC of 25 September 2001 adopting a manual of receiving agencies and a glossary of documents that may be served under Council Regulation (EC) No 1348/2000 on the service in the Member States of judicial and extrajudicial documents in civil or commercial matters(2) has been published in the Official Journal of the European Communities.
(4) The manual and the glossary need to be amplified by the information sent to the Commission by Germany.
(5) The measures provided for in this Decision are in accordance with the opinion of the committee established by Article 18 of Regulation (EC) No 1348/2000,
HAS ADOPTED THIS DECISION:
Article 1
Annex I to Decision 2001/781/EC (manual referred to in point (a) of Article 17 of Regulation (EC) No 1348/2000) is amended in accordance with Annex I to this Decision.
Article 2
Annex II to Decision 2001/781/EC (glossary referred to in point (b) of Article 17 of Regulation (EC) No 1348/2000) is amended in accordance with Annex II to this Decision.
This Decision is addressed to the Member States.
Done at Brussels, 3 April 2002.
For the Commission
Antonio Vitorino
Member of the Commission
(1) OJ L 160, 30.6.2000, p. 37.
(2) OJ L 298, 15.11.2001, p. 1.
ANNEX I
MANUAL CONTAINING THE INFORMATION RELATING TO THE RECEIVING AGENCIES
1. CONTENTS: The words: "Germany: see OJ C 151, 22.5.2001, p. 4" shall be replaced by: >TABLE>
2. The following is inserted after the details for Belgium:
ALEMANIA - /TYSKLAND - /DEUTSCHLAND - /ΓΕΡΜΑΝΙΑ - /GERMANY - /ALLEMAGNE - /GERMANIA - /DUITSLAND - /ALEMANHA - /SAKSA - /TYSKLAND
>TABLE>
>TABLE>
>TABLE>
>TABLE>
>TABLE>
>TABLE>
>TABLE>
>TABLE>
ANNEX II
GLOSSARY OF DOCUMENTS WHICH MAY BE SERVED
1. CONTENTS: The words: "Germany: see OJ C 151, 22.5.2001, p. 4" shall be replaced by:
>TABLE>
2. The following shall be inserted after the details for Belgium:
ALEMANIA - /TYSKLAND - /DEUTSCHLAND - /ΓΕΡΜΑΝΙΑ - /GERMANY - /ALLEMAGNE - /GERMANIA - /DUITSLAND - /ALEMANHA - /SAKSA - /TYSKLAND
>TABLE>
