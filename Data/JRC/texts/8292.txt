COMMISSION REGULATION (EC) No 331/1999 of 12 February 1999 amending Regulation (EC) No 2629/97 as regards passports in the framework of the system for the identification and registration of bovine animals
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 820/97 of 21 April 1997 establishing a system for the identification and registration of bovine animals and regarding the labelling of beef and beef products (1), and in particular Article 10(b) thereof,
Whereas Commission Regulation (EC) No 2629/97 (2), as last amended by Regulation (EC) No 2194/98 (3), lays down detailed rules regarding ear-tags, holding registers and passports in the framework of the identification and registration system of bovine animals;
Whereas it is appropriate to take account of the difficulties pointed out by Member States regarding the information provided for the passports accompanying bovine animals born before 1 January 1998;
Whereas it is appropriate to make optional the mention of certain pieces of information on passports accompanying bovine animals born before 1 January 1998; whereas this derogation should not put in question the obligation to mention these pieces of information on passports of bovine animals born in the territory of a Member State where such a requirement is foreseen by its national rules;
Whereas Regulation (EC) No 2629/97 should be amended accordingly;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the European Agricultural Guidance and Guarantee Fund Committee,
HAS ADOPTED THIS REGULATION:
Article 1
The following paragraph 4 is hereby added to Article 6 of Regulation (EC) No 2629/97:
'4. In derogation to point 1(a), the information provided for by the second and fifth indents of Article 14, paragraph 3, point C.1 of Directive 64/432/EEC is not compulsory for passports of bovine animals born before 1 January 1998. The derogation foreseen in this paragraph takes effect without prejudice to the obligation to mention the abovementioned pieces of information on passports of bovine animals born in the territory of a Member State where such a requirement is foreseen by its national rules. Member States shall communicate to each other and to the Commission the rules applied regarding the information mentioned in this paragraph.`
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 12 February 1999.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ L 117, 7. 5. 1997, p. 1.
(2) OJ L 354, 30. 12. 1997, p. 19.
(3) OJ L 276, 13. 10. 1998, p. 4.
