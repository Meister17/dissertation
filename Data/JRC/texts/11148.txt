Euro exchange rates [1]
15 February 2006
(2006/C 39/01)
| Currency | Exchange rate |
USD | US dollar | 1,1904 |
JPY | Japanese yen | 139,96 |
DKK | Danish krone | 7,4654 |
GBP | Pound sterling | 0,68350 |
SEK | Swedish krona | 9,3325 |
CHF | Swiss franc | 1,5577 |
ISK | Iceland króna | 75,92 |
NOK | Norwegian krone | 8,1375 |
BGN | Bulgarian lev | 1,9558 |
CYP | Cyprus pound | 0,5745 |
CZK | Czech koruna | 28,395 |
EEK | Estonian kroon | 15,6466 |
HUF | Hungarian forint | 250,93 |
LTL | Lithuanian litas | 3,4528 |
LVL | Latvian lats | 0,6960 |
MTL | Maltese lira | 0,4293 |
PLN | Polish zloty | 3,7740 |
RON | Romanian leu | 3,5291 |
SIT | Slovenian tolar | 239,49 |
SKK | Slovak koruna | 37,395 |
TRY | Turkish lira | 1,5938 |
AUD | Australian dollar | 1,6042 |
CAD | Canadian dollar | 1,3734 |
HKD | Hong Kong dollar | 9,2393 |
NZD | New Zealand dollar | 1,7573 |
SGD | Singapore dollar | 1,9376 |
KRW | South Korean won | 1159,57 |
ZAR | South African rand | 7,2717 |
CNY | Chinese yuan renminbi | 9,5805 |
HRK | Croatian kuna | 7,3018 |
IDR | Indonesian rupiah | 10971,92 |
MYR | Malaysian ringgit | 4,430 |
PHP | Philippine peso | 61,365 |
RUB | Russian rouble | 33,5880 |
THB | Thai baht | 46,865 |
[1] Source: reference exchange rate published by the ECB.
--------------------------------------------------
