Council Regulation (EC) No 2559/2001
of 17 December 2001
amending Regulation (EC) No 2505/96 opening and providing for the administration of autonomous Community tariff quotas for certain agricultural and industrial products
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 26 thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) By virtue of Regulation (EC) No 2505/96(1), the Council opened Community tariff quotas for certain agricultural and industrial products Community demand for the products in question should be met under the most favourable conditions. Community tariff quotas should therefore be opened at reduced or zero rates of duty for appropriate volumes, and extended in the case of certain existing tariff quotas, while avoiding any disturbance to the markets for these products.
(2) It is no longer in the Community's interest to maintain a Community tariff quota on some of the products covered by the above Regulation, and those products should therefore be removed from the table in Annex I.
(3) In view of the large number of amendments coming into effect on 1 January 2002 and in order to clarify matters for the user, the table in Annex I to the said Regulation should be replaced by the table in the Annex to this Regulation.
(4) Having regard to the economic importance of this Regulation the grounds of urgency should be invoked provided for in point 1.3 of the Protocol annexed to the Treaty on European Union and to the Treaties establishing the European Communities on the role of national parliaments in the European Union.
(5) Regulation (EC) No 2505/96 should therefore be amended,
HAS ADOPTED THIS REGULATION:
Article 1
The table in Annex I to Regulation (EC) No 2505/96 is hereby replaced by the table set out in the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the day following that of its publication in the Official Journal of the European Communities.
It shall apply from 1 January 2002.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 17 December 2001.
For the Council
The President
A. Neyts-Uyttebroeck
(1) OJ L 345, 31.12.1996, p. 1. Regulation as last amended by Regulation (EC) No 1142/2001 (OJ L 155, 12.6.2001, p. 1).
ANNEX
"ANNEX I
>TABLE>"
