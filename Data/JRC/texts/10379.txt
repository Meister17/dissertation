Prior notification of a concentration
(Case COMP/M.4432 — Oerlikon/Saurer)
(2006/C 256/07)
(Text with EEA relevance)
1. On 16 October 2006 the Commission received a notification of a proposed concentration pursuant to Article 4 of Council Regulation (EC) No 139/2004 [1] by which OC Oerlikon Corporation AG ("Oerlikon", Switzerland), acquires within the meaning of Article 3(1)(b) of the Council Regulation control of the whole of Saurer AG ("Saurer", Switzeland), by way of purchase of shares.
2. The business activities of the undertakings concerned are:
- Oerlikon: coatings, optical components, semiconductor equipment, other equipment;
- Saurer: textile machinery, automotive transmission systems.
3. On preliminary examination, the Commission finds that the notified transaction could fall within the scope of Regulation (EC) No 139/2004. However, the final decision on this point is reserved.
4. The Commission invites interested third parties to submit their possible observations on the proposed operation to the Commission.
Observations must reach the Commission not later than 10 days following the date of this publication. Observations can be sent to the Commission by fax (fax No (32-2) 296 43 01 or 296 72 44) or by post, under reference number COMP/M.4432 — Oerlikon/Saurer, to the following address:
European Commission
Directorate-General for Competition
Merger Registry
J-70
B-1049 Bruxelles/Brussel
[1] OJ L 24, 29.1.2004, p. 1.
--------------------------------------------------
