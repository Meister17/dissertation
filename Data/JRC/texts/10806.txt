Judgment of the Court of First Instance of 13 June 2006 — Boyle and Others v Commission
(Joined Cases T-218/03 to T-240/03) [1]
Parties
Applicants: Cathal Boyle (Killybegs, Ireland) and the other 22 applicants whose names are set out in the annex to the judgment (represented by: P. Gallagher SC, A. Collins SC and D. Barry, Solicitor)
Defendant: Commission of the European Communities (represented by: G. Braun and B. Doherty, Agents)
Intervener in support of the applicant: Ireland [represented by: D. O'Hagan and C. O'Toole, Agents, and by D. Conlan Smyth, Barrister]
Re:
Annulment of Commission Decision 2003/245/EC of 4 April 2003 on the requests received by the Commission to increase MAGP IV objectives to take into account improvements on safety, navigation at sea, hygiene, product quality and working conditions for vessels of more than 12 m in length overall (OJ 2003 L 90, p. 48), in so far as it rejects the applicants' request to increase the capacity of their vessels
Operative part of the judgment
The Court:
1. Dismisses the applications lodged by Thomas Faherty (T-224/03), Ocean Trawlers Ltd (T-226/03), Larry Murphy (T-236/03) and O'Neill Fishing Co. Ltd (T-239/03);
2. Annuls Commission Decision 2003/245/EC of 4 April 2003 on the requests received by the Commission to increase MAGP IV objectives to take into account improvements on safety, navigation at sea, hygiene, product quality and working conditions for vessels of more than 12 m in length overall in so far as it applies to the vessels of the other applicants;
3. Orders the Commission to bear its own costs and to pay those incurred by the applicants referred to in paragraph 2;
4. Order the applicants referred to in paragraph 1 to bear their own costs;
5. Orders Ireland to bear its own costs.
[1] OJ C 239, 4.10.2003.
--------------------------------------------------
