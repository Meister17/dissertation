Commission Decision
of 11 July 2005
on the harmonised use of radio spectrum in the 5 GHz frequency band for the implementation of wireless access systems including radio local area networks (WAS/RLANs)
(notified under document number C(2005) 2467)
(Text with EEA relevance)
(2005/513/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Decision No 676/2002/EC of the European Parliament and of the Council of 7 March 2002 on a regulatory framework for radio spectrum policy in the European Community (radio spectrum Decision) [1], and in particular Article 4(3) thereof,
Whereas:
(1) Commission recommendation 2003/203/EC of 20 March 2003 on the harmonisation of the provision of public R-LAN access to public electronic communications networks and services in the Community [2] recommended Member States to allow the provision of public R-LAN access to public electronic communications networks and services in the available 5 GHz band.
(2) It also considered that further harmonisation in particular of the 5 GHz band would be necessary in the framework of Decision No 676/2002/EC to ensure that the band be available for R-LAN in all Member States and to alleviate the growing overloading of the 2,4 GHz band designated for R-LAN by Decision (01)07 of the European Radiocommunications Committee [3].
(3) The relevant parts of the 5 GHz band have been allocated to the mobile service, except aeronautical mobile service, on a primary basis, in all three Regions of the International Telecommunication Union (ITU) by the World Radiocommunication Conference 2003 (WRC-03), taking into account the need to protect other primary services in these frequency bands.
(4) WRC-03 adopted ITU-R Resolution 229 on the "Use of the bands 5150-5250, 5250-5350 MHz and 5470-5725 MHz by the mobile service for the implementation of wireless access systems including radio local area networks" which was an incentive for further European harmonisation to allow R-LAN systems to rapidly access the European Union.
(5) With a view to such harmonisation, a mandate [4] was issued on 23 December 2003 by the Commission to the European Conference of Postal and Telecommunications Administrations (CEPT), pursuant to Article 4(2) of Decision No 676/2002/EC, to harmonise radio spectrum in the 5 GHz band for use by RLANs.
(6) As a result of that mandate, the CEPT, through its Electronic Communications Committee, has defined in its report [5] of 12 November 2004 and in its Decision ECC/DEC(04)08 of 12 November 2004 specific technical and operational conditions for the use of specific frequencies in the 5 GHz band, which are acceptable to the Commission and the Radio Spectrum Committee and should be made applicable in the Community in order to ensure the development of WAS/RLANs on a harmonised basis in the Community.
(7) WAS/RLAN equipment must fulfil the requirements of Directive 1999/5/EC of the European Parliament and of the Council of 9 March 1999 on radio equipment and telecommunications terminal equipment and the mutual recognition of their conformity [6]. Article 3.2 of this Directive obliges manufacturers to ensure that equipment does not cause harmful interference to other users of the spectrum.
(8) In several Member States, there is an essential need for the operation of military and meteorological radars in the bands between 5250 and 5850 MHz which requires specific protection against harmful interference by WAS/RLAN.
(9) There also is a need to specify appropriate equivalent isotropic radiated power limits and operational restrictions, such as indoor use restrictions, for WAS/RLANs in particular in the frequency band 5150-5350 MHz in order to protect systems in the Earth exploration-satellite service (active), space research service (active) and mobile-satellite service feeder links.
(10) As specified in the CEPT report sharing between the radars in the radiodetermination service and WAS/RLANs in the frequency bands 5250-5350 MHz and 5470-5725 MHz is only feasible with the application of power limits and mitigation techniques that ensure that WAS/RLANs do not interfere with radar applications/systems. Transmitter Power Control (TPC) and Dynamic Frequency Selection (DFS) have therefore been included in the harmonised standard EN 301 893 [7] developed by the European Telecommunications Standards Institute (ETSI) to provide presumption of conformity for WAS/RLAN equipment with Directive 1999/5/EC. Transmitter power control (TPC) in WAS/RLANs in the bands 5250-5350 MHz and 5470-5725 MHz would facilitate sharing with satellite services by significantly reducing the aggregate interference. DFS, which complies with the detection, operational and response requirements set out in Annex I to Recommendation ITU-RM. 1652 [8] avoids that WAS/RLANs use frequencies that are in use by radars. The effectiveness of the mitigation techniques in EN 301 893 to protect fixed frequency radars will be monitored. It is subject to review so as to take account of new developments, based on the study by Member States of suitable test methods and procedures for mitigation techniques.
(11) It is recognised at Community and ITU level that there is a need for further studies and the possibility of development of alternative technical/operational conditions for WAS/RLANs, whilst still providing appropriate protection of other primary services in particular radiolocation. Furthermore, it is appropriate for national administrations to perform measurement campaigns and testing to facilitate coexistence between various services. Such studies and development will be taken into account in the future review of this Decision.
(12) The measures provided for in this Decision are in accordance with the opinion of the Radio Spectrum Committee,
HAS ADOPTED THIS DECISION:
Article 1
The purpose of this Decision is to harmonise the conditions for the availability and efficient use of the frequency bands 5150-5350 MHz and 5470-5725 MHz for wireless access systems including radio local area networks (WAS/RLANs).
Article 2
For the purposes of this Decision the following definitions shall apply:
(a) "wireless access systems including radio local area networks (WAS/RLANs)" shall mean broadband radio systems that allow wireless access for public and private applications disregarding the underlying network topology.
(b) "indoor use" shall mean use inside a building, including places assimilated thereto such as an aircraft, in which the shielding will typically provide the necessary attenuation to facilitate sharing with other services.
(c) "mean equivalent isotropically radiated power (e.i.r.p.)" shall mean e.i.r.p. during the transmission burst which corresponds to the highest power, if power control is implemented.
Article 3
Member States shall designate by 31 October 2005 at the latest the frequency bands 5150-5350 MHz and 5470-5725 MHz and take all appropriate means relating thereto for the implementation of WAS/RLANs, subject to the specific conditions laid down in Article 4.
Article 4
1. In the frequency band 5150-5350 MHz, WAS/RLANs shall be restricted to indoor use with a maximum mean e.i.r.p. of 200 mW.
Furthermore, the maximum mean e.i.r.p. density shall be limited:
(a) to 0,25 mW/25 kHz in any 25 kHz band, in the band 5150-5250 MHz and
(b) to 10 mW/MHz in any 1 MHz band, in the band 5250-5350 MHz.
2. In the frequency band 5470-5725 MHz, the indoor and outdoor use of WAS/RLANs shall be restricted to a maximum mean e.i.r.p. of 1 W and a maximum mean e.i.r.p. density of 50 mW/MHz in any 1 MHz band.
3. WAS/RLANs operating in the bands 5250-5350 MHz and 5470-5725 MHz shall employ transmitter power control, which provides, on average, a mitigation factor of at least 3 dB on the maximum permitted output power of the systems.
If transmitter power control is not in use, the maximum permitted mean e.i.r.p. and the corresponding mean e.i.r.p. density limits for the 5250-5350 MHz and 5470-5725 MHz bands shall be reduced by 3 dB.
4. WAS/RLANs operating in the bands 5250-5350 MHz and 5470-5725 MHz shall use mitigation techniques that give at least the same protection as the detection, operational and response requirements described in EN 301 893 to ensure compatible operation with radiodetermination systems. Such mitigation techniques shall equalise the probability of selecting a specific channel for all available channels so as to ensure, on average, a near-uniform spread of spectrum loading.
5. Member States shall keep mitigation techniques under regular review and report to the Commission thereupon.
Article 5
This Decision is addressed to the Member States.
Done at Brussels, 11 July 2005.
For the Commission
Viviane Reding
Member of the Commission
[1] OJ L 108, 24.4.2002, p. 1.
[2] OJ L 78, 25.3.2003, p. 12.
[3] ERC Decision (01)07 of 12 March 2001 on harmonised frequencies, technical characteristics and exemption from individual licensing of short range devices used for radio local area networks (RLANs) operating in the frequency band 2400-2483,5 MHz.
[4] Mandate to CEPT to harmonise technical and, in particular, operational conditions aiming at efficient spectrum use by RLANs in the bands 5150-5350 MHz and 5470-5725 MHz.
[5] CEPT response to the EC mandate to harmonise technical and, in particular, operational conditions aiming at efficient spectrum use by RLANs in the bands 5150-5350 MHz and 5470-5725 MHz.
[6] OJ L 91, 7.4.1999, p. 10.
[7] EN 301 893 is a harmonised standard developed by ETSI (European Telecommunications Standards Institute), ETSI Secretariat, entitled Broadband Radio Access Networks (BRAN); 5 GHz high performance RLAN; Harmonised EN covering essential requirements of Article 3.2 of the R&TTE Directive. The ETSI is recognised according to European Parliament and Council Directive 98/34/EC. This harmonised standard has been produced according to a mandate issued in accordance with relevant procedures of European Parliament and Council Directive 98/34/EC. The full text of EN 301 893 can be obtained from ETSI 650 Route des Lucioles F-06921 Sophia Antipolis Cedex.
[8] Recommendation ITU-R M.1652 Dynamic frequency selection (DFS) in wireless access systems including radio local area networks for the purpose of protecting the radiodetermination service in the 5 GHz band (Questions ITU-R 212/8 and ITU-R 142/9).
--------------------------------------------------
