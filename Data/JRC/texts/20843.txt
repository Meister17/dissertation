Commission Regulation (EC) No 434/2002
of 8 March 2002
amending Regulation (EC) No 94/2002 laying down detailed rules for applying Council Regulation (EC) No 2826/2000 on information and promotion actions for agricultural products on the internal market
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 2826/2000 of 19 December 2000 on information and promotion actions for agricultural products on the internal market(1), and in particular Articles 12 and 16 thereof,
Whereas:
(1) Commission Regulation (EC) No 94/2002(2), as amended by Regulation (EC) No 305/2002(3), lays down detailed rules for applying Regulation (EC) No 2826/2000.
(2) Article 5(1) of Regulation (EC) No 94/2002 sets 15 June and in the first instance 15 March 2002 as the deadline for presentation to the Member State concerned of programmes submitted by trade federations and interbranch organisations.
(3) On account of the late publication of the guidelines for the various sectors and in particular for cut flowers and live plants, the first year's deadline for the presentation of programmes to the Member State concerned should be put back to 31 March.
(4) The measures provided for in this Regulation are in accordance with the opinion delivered at the joint meeting of the management committees on agricultural product promotion,
HAS ADOPTED THIS REGULATION:
Article 1
The first sentence of Article 5(1) of Regulation (EC) No 94/2002 is hereby replaced by the following: "With a view to implementing measures contained in programmes as referred to in Article 6 of Regulation (EC) No 2826/2000, the Community trade federations or interbranch organisations that are representative of the sector(s) concerned shall submit programmes in response to calls for proposals issued by the Member States concerned before 31 March the first time and before 15 June thereafter."
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 8 March 2002.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 328, 21.12.2000, p. 2.
(2) OJ L 17, 19.1.2002, p. 20.
(3) OJ L 47, 19.2.2002, p. 12.
