COUNCIL DECISION of 19 December 1997 amending Decision 83/653/EEC on the allocation of the possibilities for catching herring in the North Sea as from 1 January 1984 (98/21/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 3760/92 of 20 December 1992 establishing a Community system for fisheries and aquaculture (1), and in particular Article 8(4)(ii) thereof,
Having regard to the proposal from the Commission,
Whereas Decision 83/653/EEC (2) sets out the method for the allocation of the possibilities for catching herring in the North Sea with a reference to the share of the TAC (Total Allowable Catch) available to the Community as constituted before the accession of Austria, Sweden and Finland;
Whereas the 1994 Act of Accession defines the share of North Sea herring allocated to Sweden as a percentage of the share of the TAC for this stock available to the enlarged Community;
Whereas it is appropriate to amend Decision 83/653/EEC in order to refer specifically to the enlarged Community and to include Sweden in the allocation,
HAS ADOPTED THIS DECISION:
Article 1
The Annex to Decision No 83/653/EEC shall be replaced by the Annex to this Decision.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 19 December 1997.
For the Council
The President
F. BODEN
(1) OJ L 389, 31.12.1992, p. 1. Regulation as amended by the 1994 Act of Accession.
(2) OJ L 371, 31. 12. 1983, p. 39.
ANNEX
>TABLE>
