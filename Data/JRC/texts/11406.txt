Authorisation for State aid pursuant to Articles 87 and 88 of the EC Treaty
Cases where the Commission raises no objections
(2006/C 222/02)
(Text with EEA relevance)
Date of adoption : 18.5.2006
Aid No : N 7/2006
Member States : United Kingdom
Title : Low Carbon Buildings Programme
Objective : Environmental protection (energy)
Legal basis : Science and Technology Act 1965 (Section 5)
Budget : GBP 25 million (EUR 37 million)
Duration : 3 years
Further information : Annual reports
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption : 19.7.2006
Number of the aid : N 102/2006
Member State : Italia (Friuli-Venezia Giulia)
Title : Fissazione dell'aliquota dell'imposta regionale sulle attività produttive
Legal basis : Legge regionale n. 2/2006, articolo 2 comma 2
Objective : Employment (All sectors)
Overall aid amount planned : EUR 54 million (2007-2008)
Duration : Unlimited
Other information : Tax advantage
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption : 5.5.2006
Aid No : N 221/2006
Member States : United Kingdom
Title : Support for Land Remediation; Prolongation
Legal basis : Regional Development Agencies Act 1998, the Greater London Authority Act 1999, the Enterprise and New Towns (Scotland) Act 1990, the Leasehold Reform, Housing and Urban Development Act 1993, the Local Government Act 2000, the Local Government in Scotland Act 2003, the Government of Wales Act (1998); and; the Welsh Development agency Act (1975)
Objective : Productive reuse of contaminated land, brownfield land and derelict land (all sectors)
Budget : ~ EUR 150 million p.a.
Aid intensity or amount : Depending on measure 100 % or 30 % + bonuses
Duration : 6 years
Further information : Annual reports
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption : 16.5.2006
Aid No : N 505/2005
Member State : Poland
Title : Polish audiovisual fund
Legal basis : Ustawa o kinematografii z dnia 30 czerwca 2005 r.; Rozporządzenie Ministra Kultury w sprawie udzielania przez Polski Instytut Sztuki Filmowej dofinansowania przedsięwzięć z zakresu kinematografii z dnia 27 października 2005 r. z późniejszymi zmianami
Objective : To support culture and protect national heritage by developing national cinematography and strengthening the film industry
Budget : PLN 100 million (EUR 25,4 million) per annum
Aid intensity or amount : The maximum aid intensity for all types of projects has been in principle set at 50 % of the project budget
Duration : Indefinite from 1 January 2006
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption : 19.7.2006
Aid No : N 593/2005
Member States : Sweden
Title : Prolongation of the Regionally Differentiated Energy Tax for Service Sector (Sweden)
Legal basis : Lagen (1994:1776) om skatt på energi (11 kap. 3 och 4 §§)
Objective : To reduce the tax burden on heating by Service Sector Companies in the North of Sweden where heating costs are relatively high
Budget : ~ EUR 16 million p.a.; total ~ EUR 95 million
Duration : 6 years
Further information : Annual reports
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption : 16.5.2006
Aid No : NN 27/2006
Member State : Germany
Region : Berlin
Title : Grundstücksverkauf — Landgericht Berlin
Type of measure : Measure that does not constitute aid
Intensity : Measure that does not constitute aid
Name and address of the granting authority Land Berlin
Senatsverwaltung für Finanzen
Klosterstraße 59
D-10179 Berlin
Other information : If, in future, second-tier beneficiaries of the measure were to be regarded as enterprises or if amendments made to the 2001 contract were to give rise to state aid, the aid would be compatible with the Treaty
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
--------------------------------------------------
