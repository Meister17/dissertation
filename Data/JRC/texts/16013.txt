Decision of the EEA Joint Committee
No 37/2006
of 10 March 2006
amending Annex XXII (Company law) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex XXII to the Agreement was amended by Decision of the EEA Joint Committee No 158/2005 of 2 December 2005 [1].
(2) Commission Regulation (EC) No 1751/2005 of 25 October 2005 amending Regulation (EC) No 1725/2003 adopting certain international accounting standards in accordance with Regulation (EC) No 1606/2002 of the European Parliament and of the Council, as regards IFRS 1, IAS 39 and SIC 12 [2] is to be incorporated into the Agreement.
(3) Commission Regulation (EC) No 1864/2005 of 15 November 2005 amending Regulation (EC) No 1725/2003 adopting certain international accounting standards in accordance with Regulation (EC) No 1606/2002 of the European Parliament and of the Council, as regards International Financial Reporting Standard No 1 and International Accounting Standards Nos. 32 and 39 [3] is to be incorporated into the Agreement.
(4) Commission Regulation (EC) No 1910/2005 of 8 November 2005 amending Regulation (EC) No 1725/2003 adopting certain international accounting standards in accordance with Regulation (EC) No 1606/2002 of the European Parliament and of the Council, as regards International Financial Reporting Standard 1 and 6, IASs 1, 16, 19, 24, 38 and 39, International Financial Reporting Interpretations Committee's Interpretations 4 and 5 [4] is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
The following indents shall be added in point 10ba (Commission Regulation (EC) No 1725/2003) of Annex XXII to the Agreement:
- "— 32005 R 1751: Commission Regulation (EC) No 1751/2005 of 25 October 2005 (OJ L 282, 26.10.2005, p. 3),
- — 32005 R 1864: Commission Regulation (EC) No 1864/2005 of 15 November 2005 (OJ L 299, 16.11.2005, p. 45),
- — 32005 R 1910: Commission Regulation (EC) No 1910/2005 of 8 November 2005 (OJ L 305, 24.11.2005, p. 4)."
Article 2
The texts of Regulations (EC) No 1751/2005, (EC) No 1864/2005 and (EC) No 1910/2005 in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force 20 days after its adoption, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [5].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 10 March 2006.
For the EEA Joint Committee
The President
R. Wright
[1] OJ L 53, 23.2.2006, p. 64.
[2] OJ L 282, 26.10.2005, p. 3.
[3] OJ L 299, 16.11.2005, p. 45.
[4] OJ L 305, 24.11.2005, p. 4.
[5] No constitutional requirements indicated.
--------------------------------------------------
