COMMISSION DECISION of 4 July 1996 concerning protective measures with regard to imports of animals and animal products from the Former Yugoslav Republic of Macedonia due to outbreaks of foot-and-mouth disease (Text with EEA relevance) (96/414/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 90/675/EEC of 10 December 1990 laying down the principles governing the organization of veterinary checks on products entering the Community from third countries (1), as last amended by Directive 95/52/EC (2), and in particular Article 19 (6) thereof,
Having regard to Council Directive 91/496/EEC of 15 July 1991 laying down the principles governing the organization of veterinary checks on animals entering the Community from third countries and amending Directives 89/662/EEC, 90/425/EEC and 90/675/EEC (3), as last amended by the Act of Accession of Austria, Finland and Sweden, and in particular Article 18 (1) thereof,
Whereas outbreaks of foot-and-mouth disease have occurred in the Former Yugoslav Republic of Macedonia (Fyrom); whereas, on the basis of a Commission inspection performed on the spot, it appeared that the authorities in Fyrom are not able to enforce the necessary controls to prevent the spread of the disease;
Whereas the situation in Fyrom presents a serious threat to the herds of Member States in view of the trade in certain animal products;
Whereas it is appropriate to take the necessary measures to protect the Community from the risk of introduction of this disease;
Whereas Commission Decision 93/242/EEC of 30 April 1993 concerning the importation into the Community of certain live animals and their products originating from certain European countries in relation to foot-and-mouth disease (4), as last amended by Decision 95/295/EC (5), provides for the prohibition of the importation of live animals of susceptible species from and through certain countries including Fyrom; whereas this Decision allows, under certain conditions, the importation of fresh meat and certain meat products from and through these countries;
Whereas Commission Decision 95/340/EC (6), as last amended by Decision 96/325/EC (7), draws up a list of third countries from which Member States authorize imports of raw milk, heat-treated milk and milk-based products; whereas Fyrom is included in this list; whereas it is necessary to ensure that any imported milk products have undergone a treatment sufficient to destroy the virus;
Whereas Council Directive 92/118/EEC of 17 December 1992 laying down animal health and public health requirements governing trade in and imports into the Community of products not subject to the said requirements laid down in specific Community rules referred to in Annex A (I) to Directive 89/662/EEC and, as regards pathogens, to Directive 90/425/EEC (8), as last amended by Commission Decision 96/405/EC (9), lays down the conditions for the importation of animal casings, hides and skins, bones and bone products, horn and horn products, hooves and hoove products, game trophies and unprocessed wool and hair; whereas these products may be imported only if treated in such a way as to destroy the virus; whereas, however, certain other products may still be imported; whereas this material constitutes a risk;
Whereas it is necessary therefore to prohibit the importation of certain animal products from Fyrom; whereas however certain products can be imported if they have undergone specific treatments;
Whereas the measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
Commission Decision 93/242/EEC is amended as follows:
1. In Annex A, footnote (1) relating to the Former Yugoslav Republic of Macedonia is deleted.
2. In Annex B, the words 'Former Yugoslav Republic of Macedonia` are deleted.
Article 2
1. Member States shall not authorize the importation of milk and milk-based products originating in Fyrom unless they have undergone a treatment which meets the requirements of Article 3 of Decision 95/340/EC.
2. In addition to the provisions of Decision 93/242/EEC, Member States shall not authorize the importation of the following products of the bovine, ovine, caprine and other bi-ungulate species originating in the territory of Fyrom:
- blood and blood products as described in Annex I, Chapter 7 to Directive 92/118/EEC,
- raw materials for the manufacture of animal feedingstuffs and pharmaceutical or technical products as described in Annex I, Chapter 10 to Directive 92/118/EEC,
- animal manure as described in Annex I, Chapter 14 to Directive 92/118/EEC.
3. The prohibition referred to in the first indent of paragraph 2 shall not apply to blood products which have undergone the treatment provided for in Annex I, Chapter 7 (3) (b) to Directive 92/118/EEC.
4. Member States shall ensure that the certificates accompanying animal products treated in accordance with paragraphs 1 or 3 and authorized to be sent from Fyrom shall bear the following words:
'Animal products conforming to Commission Decision 96/414/EC concerning protective measures with regard to imports of animals and animal products from the Former Yugosla
v Republic of Macedonia, due to outbreaks of foot-and mouth disease`.
Article 3
Member States shall amend the measures they apply to trade so as to bring them into compliance with this Decision. They shall immediately inform the Commission thereof.
Article 4
This Decision is addressed to the Member States.
Done at Brussels, 4 July 1996.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ No L 373, 31. 12. 1990, p. 1.
(2) OJ No L 265, 8. 11. 1995, p. 16.
(3) OJ No L 268, 24. 9. 1991, p. 56.
(4) OJ No L 110, 4. 5. 1993, p. 36.
(5) OJ No L 182, 2. 8. 1995, p. 30.
(6) OJ No L 200, 24. 8. 1995, p. 38.
(7) OJ No L 123, 23. 5. 1996, p. 24.
(8) OJ No L 62, 15. 3. 1993, p. 49.
(9) OJ No L 165, 4. 7. 1996, p. 40.
