Council Regulation (EC) No 2699/2000
of 4 December 2000
amending Regulation (EC) No 2200/96 on the common organisation of the market in fruit and vegetables, Regulation (EC) No 2201/96 on the common organisation of the market in processed fruit and vegetables and Regulation (EC) No 2202/96 introducing a Community aid scheme for producers of certain citrus fruits
THE COUNCIL OF THE EUROPEAN UNON,
Having regard to the Treaty establishing the European Community, and in particular Articles 36 and 37 thereof,
Having regard to the proposal from the Commission(1),
Having regard to the opinion of the European Parliament(2),
Having regard to the opinion of the Economic and Social Committee(3),
Whereas:
(1) The third subparagraph of Article 15(5) of Regulation (EC) No 2200/96(4) caps the amount of Community financial assistance granted to any individual producer organisation, and also sets a limit on the total granted to all producer organisations. The application of that limit introduces a variable factor into the scheme, thus complicating the task of producer organisations in developing and implementing operational programmes, and creating some uncertainty as to their financing. Experience has shown that this limit may be dispensed with while continuing to ensure proper financial management. In view of the results of past programmes, the limit for individual organisations can be set at 4,1 % of the value of the marketed production of each producer organisation.
(2) For citrus fruit, the divergence, due in particular to overrunning of the processing threshold, between Community withdrawal compensation and Community processing aid may in the future lead to unjustified diversion into withdrawal of products that should properly have been processed. To avoid this risk, the ceiling fixed in Articles 23 and 24 of Regulation (EC) No 2200/96 as a percentage of marketed quantity on eligibility for the Community withdrawal compensation should be reduced to 10 % for the 2001/02 marketing year and to 5 % from the 2002/03 marketing year onwards. This amendment makes it possible to simplify the wording of Articles 23 and 26 of that Regulation.
(3) Experience has shown that the administration of export refunds on fresh fruit and vegetables could be improved and simplified, at least in some cases, by using a tender procedure. Provision should therefore be made for the possibility of holding such invitations to tender.
(4) Experience of applying the Community aid scheme for tomato processing governed by Regulation (EC) No 2201/96(5) has shown that the quota mechanism introduces rigidity in the sector, preventing the processing industries concerned from adapting rapidly to market demand. To correct this rigidity, the quota system should be replaced by a system of processing thresholds, with aid for the following marketing year being reduced if the thresholds are overrun. To ensure that these arrangements are sufficiently flexible, a single Community threshold should be fixed, expressed in tonnes of fresh tomatoes intended for processing. To allow for expanding demand for these products, the threshold should be fixed above the level corresponding to the present quota scheme.
(5) The quantities of tomatoes, peaches and pears going for processing under the aid scheme governed by Regulation (EC) No 2201/96 have developed differently from one Member State to another. As a result, and to ensure that operators in each Member State assume greater responsibility, Community processing thresholds should be split between the Member States on an equitable basis, and any reduction in Community aid resulting from an overrun of the Community threshold should apply only in those Member States in which the threshold is exceeded. In this case, account must be taken of quantities that have not been processed in the Member States where the threshold was not overrun. To take into account the characteristics of the peeled tomato sector, the possibility must be given to the Member States to subdivide their national threshold into two sub-thresholds. In this case, reductions in aid subsequent to an overrun of this national threshold should be applied separately for each of the two sub-thresholds.
(6) At present, processing aid for tomatoes, peaches and pears is granted under Regulation (EC) No 2201/96 to processors who have paid producers for their raw materials a price not less than a certain minimum price. Aid is fixed by unit of weight of the processed product. The management of this scheme should be simplified, introducing more flexibility in trade relations between producer organisations and processors and contributing to the adjustment of supply to consumer demand at reasonable prices. To this end, aid should be granted to producer organisations that deliver fresh products to processors, this aid should be fixed in terms of the weight of raw materials irrespective of the weight of the processed product, and the minimum price should be discontinued.
(7) The amount of aid for processing tomatoes, peaches and pears should be fixed in particular on the basis of the aid granted in the most recent marketing years preceding this amendment to the scheme.
(8) As a result of this amendment to Title I of Regulation (EC) No 2201/96, the provisions governing processing aid for prunes derived from dried "d'Ente" plums and for dried figs must be adapted, although without any change of substance. Furthermore, the revision procedure for the list of processed products appearing in Annex I to Regulation (EC) No 2201/96 should be simplified.
(9) Article 5 of Regulation (EC) No 2202/96(6) lays down Community processing thresholds for lemons, oranges and grapefruit separately, and for mandarins, clementines and satsumas, hereinafter referred to as "small citrus fruit", taken together. Since the introduction of this regime, those thresholds have been largely exceeded each marketing year in the case of lemons and oranges, and to a lesser extent in the 1998/99 and 1999/2000 marketing years in the case of small citrus fruit. The thresholds for grapefruit have been complied with. Under the rules, those overruns have resulted in major reductions in processing aid. The continued existence of such a situation could lead in future to products usually intended for processing being diverted into withdrawals. As a result, the thresholds for lemons, oranges and small citrus fruit should be raised. To take into account the characteristics of the small citrus fruit sector, the possibility must be given to the Member States to subdivide their national threshold into two sub-thresholds. In this case, reductions in aid subsequent to an overrun of this national threshold should be applied separately for each of the two sub-thresholds.
(10) The quantities going for processing have evolved differently from one Member State to another. As a result, and to ensure that operators in each Member State assume greater responsibility, the Community processing thresholds should be split between the Member States on an equitable basis, and any reduction in Community aid resulting from an overrun of the Community threshold should apply only in those Member States in which the threshold is exceeded. In this case, account must be taken of quantities that have not been processed in the Member States where the threshold was not overrun.
(11) Changes in the numbering of the Annexes to Regulation (EC) No 2202/96 require an amendment to the wording of Article 3 of that Regulation.
(12) The measures necessary for the implementation of Regulations (EC) No 2200/96 and (EC) No 2201/96 should be adopted in accordance with Council Decision 1999/468/EC of 28 June 1999 laying down the procedures for the exercise of implementing powers conferred on the Commission(7).
(13) These amendments to Regulations (EC) No 2200/96, (EC) No 2201/96 and (EC) No 2202/96 should be applied from the 2001/02 marketing year. However, as the operational funds are managed on a calendar-year basis, the amendment to the third subparagraph of Article 15(5) of Regulation (EC) No 2200/96 should apply from 2001,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 2200/96 is amended as follows:
1. the third subparagraph of Article 15(5) shall be replaced by the following:"However, the financial assistance shall be capped at 4,1 % of the value of the marketed production of each producer organisation.";
2. Article 23(3), (4), (5) and (6) shall be replaced by the following:
"3. Where paragraph 1 is applied to any one of the products listed in Annex II which meet the relevant standards, producer organisations and their associations shall pay their producer members the Community withdrawal compensation indicated in Annex V, up to a limit:
- for citrus fruit, of 5 %,
- for apples and pears, of 8,5 %, and
- for other products, of 10 %,
of the marketed quantity.
The limits set in the first subparagraph shall apply to the marketed quantity of each product, as defined in accordance with the procedure laid down in Article 46, of only the members of the producer organisation concerned, or of another organisation where Article 11(1)(c) applies.
4. The ceilings fixed in paragraph 3 shall apply from the 2002/03 marketing year. For the 2001/02 marketing year, the ceilings shall be 10 % for citrus fruit, melons and water melons, and 20 % for other products.
The second subparagraph of paragraph 3 shall apply to the ceilings set in this paragraph.
5. The percentages in paragraphs 3 and 4 shall be annual averages over a three-year period, with a 3 % annual margin of overrun.";
3. Article 24 shall be replaced by the following:
"Article 24
In connection with the products listed in Annex II, producer organisations shall allow the benefits of Article 23 to growers who are not members of any of the collective structures provided for in this Regulation, if they so request. However, the Community withdrawal compensation shall be reduced by 10 %. In addition, the amount paid shall take account, on scrutiny of the evidence, of the overall withdrawal costs borne by the members. The compensation may not be granted on a volume greater than that share of the grower's marketed production corresponding to the percentages given in Article 23(3).";
4. Article 26 shall be replaced by the following:
"Article 26
The Community withdrawal compensation shall be a single amount valid throughout the Community.";
5. the third subparagraph of Article 35(3) shall be replaced by the following:"Refunds shall be fixed in accordance with the procedure laid down in Article 46. Refunds shall be fixed periodically or by tender.";
6. Article 45 shall be repealed;
7. Article 46 shall be replaced by the following:
"Article 46
1. The Commission shall be assisted by a committee, the Management Committee for Fresh Fruit and Vegetables, hereinafter referred to as the 'committee', composed of representatives of the Member States and chaired by the representative of the Commission.
2. Where reference is made to this paragraph, Articles 4 and 7 of Decision 1999/468/EC shall apply.
3. The period laid down in Article 4(3) of Decision 1999/468/EC shall be set at one month.
4. The committee shall adopt its rules of procedure."
Article 2
Regulation (EC) No 2201/96 is amended as follows:
1. Articles 2 to 6 shall be replaced by the following:
"Article 2
A Community aid scheme is hereby introduced to assist producer organisations supplying tomatoes, peaches and pears harvested in the Community for the production of the processed products listed in Annex I.
The list of processed products appearing in Annex I may be revised on the basis of changes in the market, in accordance with the procedure laid down in Article 29.
Article 3
1. The scheme referred to in Article 2 shall be based on contracts between, on the one hand, producer organisations recognised or provisionally authorised under Regulation (EC) No 2200/96 and, on the other, processors approved by the competent authorities in the Member States.
However, in the course of the 2001/02 marketing year, contracts between processors and individual producers shall also be eligible, for a quantity not exceeding 25 % of the total quantity contracted for by any processor.
2. Contracts shall be concluded by a specific date, to be defined in accordance with the procedure laid down in Article 29. In particular, they shall specify the quantities they cover, the schedule of supply to the processor and the price to be paid to the producer organisations, and shall require the processor to process the products covered.
As soon as they have been signed, the contracts shall be forwarded to the competent authorities in the Member States.
3. The producer organisations shall extend the benefit of the provisions of this Article to operators not affiliated to any of the collective structures provided for in Regulation (EC) No 2200/96, who undertake to market through such structures all their output of tomatoes, peaches and pears for processing and who pay a contribution towards the overall management costs of this system borne by the organisation.
Article 4
1. Aid shall be granted to producer organisations for the quantities of raw materials supplied for processing under the contracts referred to in Article 3.
2. The amount of aid shall be:
EUR 34,50/tonne for tomatoes,
EUR 47,70/tonne for peaches,
EUR 161,70/tonne for pears.
3. Without prejudice to the application of Article 5, aid shall be paid by the Member States to the producer organisations on request, as soon as the control authorities in the Member State in which processing is carried out have established that the products covered by the contracts have been supplied to the processing industry. The amount of aid received by the producer organisation shall be paid to its members, and where Article 3(3) is applied, to the operators concerned.
Article 5
1. For each of the products referred to in Article 2, Community and national processing thresholds shall be established as indicated in Annex II.
2. Whenever a Community processing threshold is overrun, the aid fixed for the product in question in accordance with Article 4(2) shall be reduced in all the Member States in which the corresponding threshold has been overrun.
For the purposes of applying the first subparagraph, threshold overruns shall be calculated by comparing the threshold with the average quantity processed with aid under this Regulation over the three marketing years preceding that for which aid must be set.
However, to calculate the overrun of the thresholds fixed for each Member State, any quantities still available below the threshold of a Member State but not processed shall be allocated to the other Member States, in proportion to their respective thresholds.
The reduction in aid shall be proportional to the volume of overrun relative to the relevant threshold.
3. For tomatoes, and by way of derogation from the second subparagraph of paragraph 2, the following arrangements shall apply to the first three marketing years immediately following implementation of this Regulation:
(a) for the first marketing year:
- overrun of the processing threshold shall be calculated on the basis of the quantity supplied for processing with aid during the year, and
- the aid fixed in Article 4(2) shall be reduced to EUR 31,36/tonne. However, in the Member States where the threshold has not been overrun, or where it has been overrun by less than 10 %, and in all Member States concerned if the Community threshold has not been overrun, a supplement shall be paid after the end of the marketing year. The supplement shall be fixed on the basis of the actual overrun of the threshold concerned;
(b) for the second marketing year, overrun of the processing threshold shall be calculated on the basis of the quantity supplied for processing with aid during the first year;
(c) for the third marketing year, overrun of the processing threshold shall be calculated on the basis of the average quantity supplied for processing with aid during the first and second years.
4. Member States may divide the national threshold for tomatoes into two sub-thresholds, namely tomatoes for processing into whole peeled tomatoes and those for processing into other tomato products.
Member States which take up this option shall inform the Commission thereof.
If the national threshold is overrun, the reduction in aid provided for in paragraph 2 shall be applied to the aid for both sub-thresholds in proportion to the recorded overrun of the sub-threshold concerned.
Article 6
1. Detailed rules for the application of Articles 2 to 5, and in particular rules governing approval of processors, conclusion of processing contracts, payment of aid, control measures and sanctions, marketing years, minimum characteristics of the raw material supplied for processing, minimum quality requirements for finished products and the financial consequences of overrunning thresholds, shall be adopted in accordance with the procedure laid down in Article 29.
2. Quality and quantity checks shall also be adopted in accordance with that procedure to verify:
- that the products supplied to processors by producer organisations meet requirements, and
- that the products supplied have actually been processed into products listed in Annex I.";
2. the following Articles shall be inserted after Article 6:
"Article 6a
1. A production aid scheme shall be applied:
(a) to dried figs falling within CN code 0804 20 90,
(b) to prunes derived from dried 'd'Ente' plums falling within CN code ex 0813 20 00,
obtained from fruit harvested in the Community.
2. Production aid shall be granted to processors who have paid producers for their raw materials a price not less than the minimum price under contracts between, on the one hand, producer organisations recognised or provisionally authorised under Regulation (EC) No 2200/96 and, on the other, processors.
However, in the course of the 2001/02 marketing year, contracts between processors and individual producers shall also be eligible, for a quantity not exceeding 25 % of the quantity giving entitlement to production aid.
The producer organisations shall extend the benefit of the provisions of this Article to operators not affiliated to any of the collective structures provided for in Regulation (EC) No 2200/96, who undertake to market through such structures all their output intended for the manufacture of the products referred to in paragraph 1 and who pay a contribution towards the overall management costs of this system borne by the organisation.
Contracts must be signed before the start of the marketing year.
Article 6b
1. The minimum price to be paid to producers shall be calculated on the basis of:
(a) the minimum price applying during the previous marketing year;
(b) the movement of market prices in the fruit and vegetables sector;
(c) the need to ensure normal market disposal of basic fresh products for the various uses, including supply to the processing industry.
2. Minimum prices shall be fixed before the start of each marketing year.
3. Minimum prices and detailed rules for the application of this Article shall be adopted in accordance with the procedure laid down in Article 29.
Article 6c
1. The production aid may not exceed the difference between the minimum price paid to the producer in the Community and the price of the raw material in the main producing and exporting third countries.
2. The amount of production aid shall be so fixed as to enable the Community product to be disposed of, within the limit set in paragraph 1.
To establish this amount, account shall be taken in particular:
(a) of the difference between the cost of the raw material in the Community and that in the major competing third countries;
(b) of the amount of aid fixed for the previous marketing year,
and
(c) where Community output of a product accounts for a substantial share of the market, movements in external trade volumes and in the prices obtaining in such trade, where the latter criterion results in a reduction in the amount of the aid.
3. The production aid shall be fixed in terms of the net weight of the processed product. The coefficients expressing the relationship between the weight of raw material used and the net weight of the processed product shall be defined on a standardised basis. They shall be regularly updated on the basis of experience.
4. Production aid shall be granted to processors only for processed products which:
(a) have been produced from raw materials harvested in the Community, for which the applicant has paid at least the minimum price referred to in Article 6a(2);
(b) meet minimum quality requirements.
5. The price of the raw material in the main competing third countries shall be determined mainly on the basis of the prices actually applying at the farm-gate stage for fresh products of a comparable quality used for processing, weighted on the basis of the quantities of finished products exported by those third countries.
6. Where Community production accounts for at least 50 % of the quantities of a product making up the Community consumption market, movements in volumes and prices of imports and exports shall be assessed by comparing the data for the calendar year preceding the start of the marketing year with the data for the previous calendar year.
7. The Commission shall fix the amount of the production aid before the start of each marketing year, in accordance with the procedure laid down in Article 29. The coefficients referred to in paragraph 3, the minimum quality requirements and the other detailed rules for the application of this Article shall be adopted in accordance with the same procedure.";
3. Article 28 shall be repealed;
4. Article 29 shall be replaced by the following:
"Article 29
1. The Commission shall be assisted by a committee, the Management Committee for Processed Fruit and Vegetables, hereafter referred to as the 'committee', composed of representatives of the Member States and chaired by the representative of the Commission.
2. Where reference is made to this paragraph, Articles 4 and 7 of Decision 1999/468/EC shall apply.
3. The period laid down in Article 4(3) of Decision 1999/468/EC shall be set at one month.
4. The committee shall adopt its rules of procedure.";
5. Annex I shall be replaced by the text appearing in Annex I to this Regulation;
6. Annex III shall be replaced by the text appearing in Annex II to this Regulation.
Article 3
Regulation (EC) No 2202/96 is amended as follows:
1. Article 3 shall be replaced by the following:
"Article 3
1. Aid shall be granted to producer organisations for the quantities delivered for processing under the contracts referred to in Article 2.
2. The amount of the aid is set out in Table 1 of Annex I.
However:
(a) where the contract referred to in Article 2(1) covers more than one marketing year and a minimum quantity of citrus fruit, to be determined in accordance with the procedure laid down in Article 46 of Regulation (EC) No 2200/96, the amount of the aid shall be that indicated in Table 2 of Annex I;
(b) for quantities delivered under the provisions of Article 4, the amount of the aid shall be that indicated in Table 3 of Annex I.
3. Without prejudice to the application of Article 5, the aid shall be paid by the Member States to producer organisations which apply therefor as soon as the inspecting authorities of the Member State in which processing is undertaken have established that the products covered by the contracts have been delivered to the processing industry.
The aid received by the producer organisations shall be paid to their members.
4. Measures shall be adopted in accordance with the procedure laid down in Article 46 of Regulation (EC) No 2200/96 to ensure that the processing industry meets its obligation to process the products delivered by the producer organisations.";
2. Article 5 shall be replaced by the following:
"Article 5
1. Processing thresholds shall be established for the Community and for each producer Member State, on the one hand for lemons, oranges and grapefruit separately and, on the other hand, for mandarins, clementines and satsumas taken together. The thresholds shall be as set out in Annex II.
2. Whenever a Community processing threshold is overrun, the aid fixed for the product in question in accordance with Article 3(2) shall be reduced in all the Member States in which the corresponding threshold has been overrun.
For the purposes of applying the first subparagraph, threshold overruns shall be calculated by comparing the threshold with the average quantity processed with aid under this Regulation over the three marketing years or equivalent periods preceding the marketing year for which the aid must be set.
However, to calculate the overrun of the thresholds fixed for each Member State, any quantities still available below the threshold of a Member State but not processed shall be allocated to the other Member States, in proportion to their respective thresholds.
The reduction in aid shall be proportional to the volume of overrun relative to the relevant threshold.
3. Member States may divide the national threshold laid down for small citrus fruit into two sub-thresholds, namely small citrus fruit for processing into segments and those for processing into juice.
Member States which take up this option shall inform the Commission thereof.
If the national threshold is overrun, the reduction in aid provided for in paragraph 2 shall be applied to the aid for both sub-thresholds in proportion to the recorded overrun of the sub-threshold concerned.";
3. the Annex becomes Annex I;
4. the text of Annex II appearing in Annex III to this Regulation shall be inserted after Annex I.
Article 4
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
It shall apply to each product or group of products from the 2001/02 marketing year. However, Article 1(1) shall apply to operational funds from 2001.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 4 December 2000.
For the Council
The President
J. Glavany
(1) OJ C 337 E, 28.11.2000, p. 207.
(2) Opinion delivered on 26 October 2000 (not yet published in the Official Journal).
(3) Opinion delivered on 19 October 2000 (not yet published in the Official Journal).
(4) OJ L 297, 21.11.1996, p. 1. Regulation as last amended by Regulation (EC) No 1257/1999 (OJ L 160, 26.6.1999, p. 80).
(5) OJ L 297, 21.11.1996, p. 29. Regulation as last amended by Regulation (EC) No 2701/1999 (OJ L 327, 21.12.1999, p. 5).
(6) OJ L 297, 21.11.1996, p. 49. Regulation as last amended by Regulation (EC) No 858/1999 (OJ L 108, 27.4.1999, p. 8).
(7) OJ L 184, 17.7.1999, p. 23.
ANNEX I
"ANNEX I
Processed products referred to in Article 2
>TABLE>"
ANNEX II
"ANNEX III
Processing thresholds referred to in Article 5
Net weight fresh product
>TABLE>"
ANNEX III
"ANNEX II
Processing thresholds referred to in Article 5
Net weight fresh product
>TABLE>"
