Commission Decision of 2 July 2004 laying down a derogation to the transitional regime established by Article 6 of Regulation (EC) No 998/2003 for the transit of pet animals through the territory of Sweden between the Island of Bornholm and the other parts of the territory of Denmark (notified under document number C(2004) 2435) (Only the Danish and Swedish texts are authentic) (Text with EEA relevance) (2004/557/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to the Regulation (EC) No 998/2003 of the European Parliament and of the Council of 26 May 2003 on the animal health requirements applicable to the non-commercial movement of pet animals and amending Council Directive 92/65/EEC(1), and in particular Article 21 thereof,
Whereas:
(1) Article 6 of Regulation (EC) No 998/2003 establishes for a transitional period of five years the veterinary conditions applying, inter alia , to non-commercial movements of pet dogs and cats, to the territory of Sweden.
(2) Those conditions are largely the same than the national conditions applying for entry into Sweden before the implementation of Regulation (EC) No 998/2003.
(3) A bilateral agreement existed between Sweden and Denmark establishing less restrictive requirements than those applicable for entry into Sweden for the transit of pet animals through the territory of Sweden between the Island of Bornholm (DK) in the Baltic Sea and the other parts of the territory of Denmark.
(4) It is appropriate to maintain this limited derogation to the transitional regime established under Article 6 of Regulation (EC) No 998/2003.
(5) The measure provided for in this Decision is in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
By derogation to Article 6 of Regulation (EC) No 998/2003 and until the end of the transitional period laid down in that Article, transit of pet animals of the species mentioned in Part A of Annex I to Regulation (EC) No 998/2003 between the Island of Bornholm and other parts of the territory of Denmark through the territory of Sweden is permitted according to the conditions agreed between the two Member States.
Article 2
This Decision shall apply from 3 July 2004 .
Article 3
This Decision is addressed to the Kingdom of Denmark and the Kingdom of Sweden.
Done at Brussels, 2 July 2004 .
For the Commission
David Byrne
Member of the Commission
(1) OJ L 146, 13.6.2003, p. 1. Regulation as amended by Commission Regulation (EC) No 592/2004 (OJ L 94, 31.3.2004, p. 7).
