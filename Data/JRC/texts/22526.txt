*****
COUNCIL DECISION
of 16 July 1985
on the comparability of vocational training qualifications between the Member States of the European Community
(85/368/EEC)
THE COUNCIL OF THE EUROPEAN
COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 128 thereof,
Having regard to Council Decision 63/266/EEC of 2 April 1963 laying down general principles for implementing a common vocational training policy (1), and in particular the eighth principle thereof,
Having regard to the proposal from the Commission, as amended on 17 July 1984,
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas the eighth principle of Decision 63/266/EEC is to make it possible to achieve the mutual recognition of certificates and other documents confirming completion of vocational training;
Whereas the Council resolution of 6 June 1974 (4) on the mutual recognition of diplomas, certificates and other evidence of formal qualifications requires lists of such qualifications recognized as being equivalent to be drawn up;
Whereas the absence of the said mutual recognition is a factor inhibiting freedom of movement for workers within the Community, insofar as it restricts the possibility for workers seeking employment in one Member State to rely on vocational qualifications which they have obtained in another Member State;
Whereas there is a very substantial degree of diversity in the vocational training systems in the Community; whereas these systems are constantly requiring adaptation to the new situations brought about by the impact of technological change on employment and job content;
Whereas the Council resolution of 11 July 1983 concerning vocational training policies in the European Community in the 1980s (5) affirmed the need for a convergence of policies in the vocational training field, whilst recognizing the diversity of training systems in the Member States, and the need for Community action to be flexible;
Whereas it has been possible for the Commission to establish as a reference point, with the help of the Advisory Committee for Vocational Training, a structure of levels of training which represents a first step towards the achievement of the aims laid down in the eighth principle of Decision 63/266/EEC, but whereas this structure does not reflect all the training systems being developed in the Member States;
Whereas for the skilled-worker level within this structure, and for selected priority groups of occupations, it has been possible to arrive at practical job descriptions and to identify the corresponding vocational training qualifications in the various Member States;
Whereas consultation with the vocational sectors concerned has provided evidence that these results can provide firms, workers and public authorities with valuable information concerning the comparability of vocational training qualifications;
Whereas the same basic methodology could be applied to other occupations or groups of occupations on advice from the Advisory Committee for Vocational Training and with the collaboration of employers, workers and the public authorities in the vocational sectors concerned;
Whereas it is therefore essential to make rapid progress towards the comparability of vocational training qualifications for all skilled workers, and to extend the work to other levels of training as quickly as possible;
Whereas it is advisable to have all the necessary opinions, in particular that of the Advisory Committee for Vocational Training, and the technical assistance of the European Centre for the Development of Vocational Training, and to enable the Member States and the Commission to act in accordance with existing procedures;
Whereas the Advisory Committee for Vocational Training delivered an opinion at its meeting on 18 and 19 January 1983;
Whereas paragraph 21 of the report of the Committee on a People's Europe of 29 and 30 March 1985 should be taken into account,
HAS ADOPTED THIS DECISION:
Article 1
The aim of enabling workers to make better use of their qualifications, in particular for the purposes of obtaining suitable employment in another Member State, shall require, for features of job descriptions mutually agreed by the Member States on behalf of workers, within the meaning of Article 128 of the Treaty, expedited common action by the Member States and the Commission to establish the comparability of vocational training qualifications in the Community and improved information on the subject.
Article 2
1. The Commission, in close cooperation with the Member States, shall undertake work to fulfil the aims set out in Article 1 on the comparability of vocational training qualifications between the various Member States, in respect of specific occupations or groups of occupations.
2. The work may use as a reference the structure of training levels drawn up by the Commission with the help of the Advisory Committee for Vocational Training.
The text of the said structure is attached to this Decision for information purposes.
3. The work referred to in paragraph 2 shall first and foremost concentrate on the occupational qualifications of skilled workers in mutually agreed occupations or groups of occupations.
4. The scope of this Decision may subsequently be extended to permit work to be undertaken, on a proposal from the Commission, at other levels of training.
5. The SEDOC register, used in connection with the European system for the international clearing of vacancies and applications for employment, shall, whenever possible, be used as the common frame of reference for vocational classifications.
Article 3
The following working procedure shall be employed by the Commission in establishing the comparability of vocational training qualifications in close cooperation with the Member States and the organizations of workers and employers at Community level:
- selection of the relevant occupations or groups of occupations on a proposal from the Member States or the competent employer or worker organizations at Community level;
- drawing up mutually agreed Community job descriptions for the occupations or groups of occupations referred to in the first indent;
- matching the vocational training qualifications recognized in the various Member States with the job descriptions referred to in the second indent;
- establishing tables incorporating information on:
(a) the SEDOC and national classification codes;
(b) the level of vocational training;
(c) for each Member State, the vocational title and corresponding vocational training qualifications;
(d) the organizations and institutions responsible for dispensing vocational training;
(e) the authorities and organizations competent to issue or to validate diplomas, certificates, or other documents certifying that vocational training has been acquired;
- publication of the mutually agreed Community job descriptions and the comparative tables in the Official Journal of the European Communities;
- establishment, within the meaning of Article 4 (3), of a standard information sheet for each occupation or group of occupations, to be published in the Official Journal of the European Communities;
- dissemination of information on the established comparabilities to all appropriate bodies at national, regional and local levels, as well as throughout the occupational sectors concerned.
This action could be supported by the creation of a Community-wide data base, if experience shows the need for such a base.
Article 4
1. Each Member State shall designate a coordination body, based wherever possible on existing structures, which shall be responsible for ensuring - in close collaboration with the social partners and the occupational sectors concerned - the proper dissemination of information to all interested bodies. The Member States shall also designate the body responsible for contacts with the coordination bodies in other Member States and with the Commission. 2. The coordination bodies of the Member States shall be competent to establish appropriate arrangements with regard to vocational training information for their competent national, regional or local bodies as well as for their own nationals wishing to work in other Member States and for workers who are nationals of other Member States, on established cases of comparable vocational qualifications.
3. The bodies referred to in paragraph 2 may supply on request in all Member States an information sheet drawn up in accordance with the model provided for in the sixth indent of Article 3, which the worker may present to the employer together with his national certificate.
4. The Commission is to continue studying the introduction of the European vocational training pass advocated by the Committee for a People's Europe in paragraph 21 of its report of 29 and 30 March 1985.
5. The Commission shall give the bodies referred to in paragraph 2, on request, all necessary assistance and advice concerning the preparation and setting up of the arrangements provided for in paragraph 2, including the adaptation and checking of the relevant technical documents.
Article 5
The Commission shall, in close liaison with the national coordination bodies designated by the Member States,
- review and update at appropriate, regular intervals, in close cooperation with the Member States and the organizations of workers and employers at Community level, the mutually agreed Community job descriptions and the comparative tables relating to the comparability of vocational training qualifications,
- where necessary, formulate proposals for a more efficient operation of the system including other measures likely to improve the situation as regards the comparability of vocational qualification certificates,
- where necessary, assist in the case of technical difficulties encountered by the national authorities or specialized bodies concerned.
Article 6
Each Member State shall submit to the Commission, for the first time two years after adoption of this Decision, and therefore every four years, a national report on the implementation of this Decision and the results obtained.
The Commission shall, at appropriate intervals, submit a report on its own work and on the application of this Decision in the Member States.
Article 7
This Decision is addressed to the Member States and the Commission.
Done at Brussels, 16 July 1985.
For the Council
The President
M. FISCHBACH
(1) OJ No 63, 20. 4. 1963, p. 1338/63.
(2) OJ No C 77, 19. 3. 1984, p. 11.
(3) OJ No C 35, 9. 2. 1984, p. 12.
(4) OJ No C 98, 20. 8. 1974, p. 1.
(5) OJ No C 193, 20. 7. 1983, p. 2.
ANNEX
Training-level structure referred to in Article 2 (2)
LEVEL 1
Training providing access to this level: compulsory education and professional initiation
This professional initiation is acquired at an educational establishment, in an out-of-school training programme, or at the undertaking. The volume of theoretical knowledge and practical capabilities involved is very limited.
This form of training must primarily enable the holder to perform relatively simple work and may be fairly quickly acquired.
LEVEL 2
Training providing access to this level: compulsory education and vocational training (including, in particular, apprenticeships)
This level corresponds to a level where the holder is fully qualified to engage in a specific activity, with the capacity to use the instruments and techniques relating thereto.
This activity involves chiefly the performance of work which may be independent within the limits of the relevant techniques.
LEVEL 3
Training providing access to this level: compulsory education and/or vocational training and additional technical training or technical educational training or other secondary-level training
This form of training involves a greater fund of theoretical knowledge than level 2. Activity involves chiefly technical work which can be performed independently and/or entail executive and coordination duties.
LEVEL 4
Training providing access to this level: secondary training (general or vocational) and post-secondary technical training
This form of training involves high-level technical training acquired at or outside educational establishments. The resultant qualification covers a higher level of knowledge and of capabilities. It does not generally require mastery of the scientific bases of the various areas concerned. Such capabilities and knowledge make it possible in a generally autonomous or in an independent way to assume design and/or management and/or administrative responsibilities.
LEVEL 5
Training providing access to this level: secondary training (general or vocational) and complete higher training
This form of training generally leads to an autonomously pursued vocational activity - as an employee or as self-employed person - entailing a mastery of the scientific bases of the occupation. The qualifications required for engaging in a vocational activity may be integrated at these various levels.
