COMMISSION DECISION of 20 May 1996 concerning the placing on the market of genetically modified male sterile chicory (Cichorium intybus L.) with partial tolerance to the herbicide glufosinate ammonium pursuant to Council Directive 90/220/EEC (Text with EEA relevance) (96/424/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 90/220/EEC of 23 April 1990 on the deliberate release into the environment of genetically modified organisms (1), as last amended by Commission Directive 94/15/EC (2), and in particular Article 13 thereof,
Whereas, in accordance with Article 10 to 18 of Directive 90/220/EEC, there is a Community procedure enabling the competent authority of a Member State to give consent to the placing on the market of products consisting of genetically modified organisms;
Whereas, a notification concerning the placing on the market of such a product has been submitted to the competent authorities of a Member State (The Netherlands);
Whereas, the competent authority of the Netherlands has subsequently forwarded the dossier thereon to the Commission with a favourable opinion; whereas the competent authorities of other Member States have raised objections to the said dossier;
Whereas, therefore, in accordance with Article 13 (3), the Commission is required to take a decision in accordance with the procedure provided for in Article 21 of Directive 90/220/EEC;
Whereas, the Commission having examined each of the objections raised in light of the scope of Directive 90/220/EEC and the information submitted in the dossier has reached the following conclusions:
- there is no reason to believe that there will be any adverse effects from the transfer of the bar gene to wild chicory populations given the fact that such transfer could only confer a competitive or selective advantage to wild populations if the herbicide glufosinate-ammonium were the only means of controlling these populations, which is not the case,
- the consent to the placing on the market of the product should not cover its use as human food or animal feed since the submitted notification does not cover these aspects,
- there are no safety reasons for mentioning on the label that the product has been obtained by genetic modification techniques,
- since 50 % of the hybrid seeds are tolerant to the herbicide, the label should mention that the product may be tolerant to the herbicide glufosinate-ammonium, so that breeders become aware that it may not be possible to control volunteers by glufosinate ammonium;
Whereas the authorization of chemical herbicides applied to plants and the assessment of the impact of their use on human health and the environment falls within the scope of Council Directive 91/414/EEC of July 1991 concerning the placing of plant protection products on the market (3), as last amended by Commission Directive 96/12/EC (4), and not within the scope of Directive 90/220/EEC;
Whereas Article 11 (6) and Article 16 (1) of the Directive provide additional safeguards if new information on risks of the product becomes available;
Whereas the measures provided for in this decision are in accordance with the opinion of the Committee of Member States representatives established under Article 21 of Directive 90/220/EEC,
HAS ADOPTED THIS DECISION:
Article 1
1. Without prejudice to other Community legislation and subject to the conditions outlined in paragraphs 2, 3 and 4, consent shall be given by the authorities of the Netherlands for the placing on the market of the following product, notified by Bejo-Zaden BV (Ref. C/NL/94/25), under Article 13 of Directive 90/220/EEC.
The product consists of seeds and plants derived from chicory (Cichorium intybus L. subspecies radicchio rosso) lines (RM3-3, RM3-4 and RM3-6) which have been transformed using Agrobacterium tumefaciens disarmed Ti-plasmid containing between the T-DNA borders:
(i) the barnase gene from Bacillus amyloliquefaciens (a ribonuclease) with the promoter PTA29 from Nicotiana tabacum and the terminator of the nopaline synthase gene from Agrobacterium tumefaciens;
(ii) the bar gene from Streptomyces hygroscopicus (a phosphinothricin acetyltransferase) with the promoter PSsuAra-tp from Arabidopsis thaliana and the TL-DNA gene 7 terminator from Agrobacterium tumefaciens;
(iii) the neo gene from Escherichia coli (a neomycin phosphotransferase II) with the promoter of the nopaline synthase gene from Agrobacterium tumefaciens and the octopine synthase gene terminator from Agrobacterium tumefaciens.
2. This consent covers any progeny derived from crosses of this product with any traditionally bred chicory.
3. The present consent covers the use of the product for breeding activities.
4. Without prejudice to labelling required by other Community legislation, it shall be indicated on the label of each package of seeds that the product:
- is to be used for breeding activities,
- and may be tolerant to the herbicide glufosinate ammonium.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 20 May 1996.
For the Commission
Ritt BJERREGAARD
Member of the Commission
(1) OJ No L 117, 8. 5. 1990, p. 15.
(2) OJ No L 103, 22. 4. 1994, p. 20.
(3) OJ No L 230, 19. 8. 1991, p. 1.
(4) OJ No L 65, 15. 3. 1996, p. 20.
