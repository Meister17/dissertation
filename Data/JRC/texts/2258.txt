Authorisation for State aid pursuant to Articles 87 and 88 of the EC Treaty
Cases where the Commission raises no objections
(2005/C 262/06)
Date of adoption of the decision: 20.7.2005
Member State: United Kingdom
Aid Nos: NN 12/04 (ex N 44/01); NN 28/04 (ex N 9/01); NN 27/04 (ex N 10/01)
Title: Climate change levy rebate (horticulture sector; pigs and poultry sector, food and drink processing sector)
Objective: For the horticultural sector a 50 % reduction in the rate of the levy is available up to five years in order to protect the sector's international competitiveness while it takes steps to reduce its energy consumption. For the energy intensive industries 80 % relief from the full rate of levy will be offered to those industries which are covered by the terms of a climate change levy agreement to improve energy efficiency and cut carbon dioxide emissions
Legal basis: Schedule 6 to the Finance Act 2000
Budget: GBP 456,69 million (EUR 687,27 million)
Aid intensity or amount:
Horticultural sector: 50 % rebate on the applicable levy rates
Energy intensive sectors: 80 % rebate on the applicable levy rates
Duration:
From 1 April 2001 to 31 March 2006 (horticulture)
From 1 April 2001 to 31 March 2011 (energy intensive sectors)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 6.9.2005
Member State: France
Aid No: NN 40/2004 (ex N 503/2003)
Title: Aid to groups of banana producers (Guadeloupe and Martinique)
Objective: To provide groups of banana producers in the Antilles with operating aid, which is tied to measures aimed at restructuring the banana sector in the French overseas departments
Budget: EUR 13 million gross, including a grant-equivalent of EUR 1,41 million
Duration: 5 years
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 24.8.2005
Member State: Italy (Umbria)
Aid No: NN 50/2003
Title: Aid for beekeeping
Objective: To safeguard and promote the development of beekeeping
Legal basis: Legge regionale n. 24 del 26 novembre 2002
Budget: EUR 37184 per year
Aid intensity or amount: Variable according to measure (investment aid, technical assistance)
Duration: 5 years
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 15.6.2004
Member State: Italy (Sicily)
Aid No: NN 114/C/98 (ex NN 114/98 and ex N 497/98)
Title: Article 4 of Regional Law No 16 of 31 August 1998 (promotion of agricultural products)
Objective: To subsidise the promotion of agricultural products through trade fairs, communication activities and other promotional events
Legal basis: Articolo 4 (promozione prodotti agricoli) della Legge della Regione Siciliana 31 agosto 1998 n. 16
Budget: EUR ca 2081794
Aid intensity or amount: 100 %
Duration: One off
Other information: The measures were implemented in 1999 in accordance with the State aid rules which were applicable before the entry into force of the Community guidelines for State aid for advertising of products listed in Annex I to the EC Treaty and of certain non-Annex I products (OJ C 252, 12.9.2001)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 24.8.2005
Member State: Italy (Calabria)
Aid No: N 390/2003
Title: Aid for quality agricultural production
Objective: To promote the production of quality agricultural products
Legal basis: Legge regionale n. 24 dell' 8 luglio 2002, articolo 11; Deliberazione della Giunta regionale della Calabria n. 516 dell' 8 luglio 2003
Budget: Around EUR 500000 per year
Aid intensity or amount: Variable depending on the measure (promotion of and support for newly formed associations and consortiums representing farmers producing quality agricultural products included in the list of quality products under Article 28 of the Treaty; aid for training and consultation of experts; aid for investment for the certification of quality production)
Duration: Indefinite
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 29.7.2005
Member State: Italy (Sicily)
Aid No: N 465/2004
Title: Assistance in farming areas which have suffered damage (tornado of 17 June 2004 in the province of Enna)
Objective: To compensate farmers for damage to agricultural production and farming structures as a result of bad weather
Legal basis: Decreto legislativo n. 102/2004
Budget: See the approved scheme (NN 54/A/04)
Aid intensity or amount: Up to 100 %
Duration: Measure applying an aid scheme approved by the Commission
Other details: Measure applying the scheme approved by the Commission under State aid NN 54/A/2004 (Commission letter C(2005)1622 final, dated 7 June 2005)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 29.7.2005
Member State: Italy (Apulia)
Aid No: N 466/2004
Title: Assistance in farming areas affected by natural disasters (rain in the province of Brindisi during the period from March to June 2004)
Objective: To compensate farmers for damage to agricultural production and farming structures as a result of bad weather
Legal basis: Decreto legislativo n. 102/2004
Budget: See the approved scheme (NN 54/A/04)
Aid intensity or amount: Up to 100 %
Duration: Measure applying an aid scheme approved by the Commission
Other details: Measure applying the scheme approved by the Commission under State aid NN 54/A/2004 (Commission letter C(2005)1622 final, dated 7 June 2005)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 29.7.2005
Member State: Italy (Calabria)
Aid No: N 467/2004
Title: Assistance in farming areas affected by natural disasters (hail-storms of 30 May 2004)
Objective: To compensate for damage to agricultural production as a result of bad weather
Legal basis: Decreto legislativo n. 102/2004
Budget: See the approved scheme (NN 54/A/04)
Aid intensity or amount: Up to 100 % of the cost of the damage
Duration: Measure applying an aid scheme approved by the Commission
Other information: Measure applying the scheme approved by the Commission under State aid NN 54/A/2004 (Commission letter C(2005)1622 final, dated 7 June 2005)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 11.5.2005
Member State: Sweden
Aid No: N 593/2004
Title: Aid for organic farming
Objective: To increase the sales and consumption of products from organic farming that have been produced pursuant to Council Regulation (EEC) No 2092/91 Eligible measures are market analysis and statistics, measures to improve marketing, pilot projects to assist the development of organic farming as well as the distribution of organic products (aid rate EUR 100000 per beneficiary over a three-year period or to 50 % of the eligible costs, if the beneficiary is an SME, whichever amount is the highest) and advertising actions aimed at canteens and restaurants in private and public sectors (aid rate 50 % of the eligible costs)
Legal basis:
Riksdagens beslut avseende regeringens proposition 2004/2005: 1. Budgetpropositionen för år 2005
Jordbruksverkets regleringsbrev för 2005 och kommande år
Budget: SEK 15 million (approximately EUR 1648800) per year
Aid intensity or amount: See above under "Objective"
Duration: Until 2010
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
--------------------------------------------------
