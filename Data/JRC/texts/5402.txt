Decision of the EEA Joint Committee No 26/2005
of 11 March 2005
amending Annex I (Veterinary and phytosanitary matters) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex I to the Agreement was amended by Decision of the EEA Joint Committee No 1/2005 of 8 February 2005 [1].
(2) Commission Decision 2004/63/EC of 23 December 2003 amending Decision 2003/467/EC as regards the declaration that certain provinces of Italy are free of bovine brucellosis and enzootic bovine leukosis [2] is to be incorporated into the Agreement.
(3) Commission Decision 2004/101/EC of 6 January 2004 amending Annex D to Directive 88/407/EEC as regards health certificates applying to intra-Community trade in semen of domestic animals of bovine species [3] is to be incorporated into the Agreement.
(4) Commission Decision 2004/199/EC of 27 February 2004 amending Decision 93/52/EEC as regards the recognition of certain provinces in Italy as officially free of brucellosis [4] is to be incorporated into the Agreement.
(5) Commission Decision 2004/226/EC of 4 March 2004 approving tests for the detection of antibodies against bovine brucellosis within the framework of Council Directive 64/432/EEC [5] is to be incorporated into the Agreement.
(6) Commission Decision 2004/230/EC of 5 March 2004 amending Decision 2003/467/EC as regards the declaration that certain provinces in Italy are free of bovine tuberculosis and bovine brucellosis [6] is to be incorporated into the Agreement.
(7) Commission Decision 2004/233/EC of 4 March 2004 authorising laboratories to check the effectiveness of vaccination against rabies in certain domestic carnivores [7] is to be incorporated into the Agreement.
(8) Commission Decision 2004/235/EC of 1 March 2004 establishing additional guarantees regarding salmonella for consignments to Finland and Sweden of laying hens [8] is to be incorporated into the Agreement.
(9) Commission Decision 2004/252/EC of 10 March 2004 amending Decision 2001/106/EC as regards bovine semen storage centres [9] is to be incorporated into the Agreement.
(10) Commission Decision 2004/315/EC of 26 March 2004 recognising the system of surveillance networks for bovine holdings implemented in Member States or regions of Member States under Directive 64/432/EEC [10] is to be incorporated into the Agreement.
(11) Commission Decision 2004/320/EC of 31 March 2004 amending Decisions 93/52/EEC, 2001/618/EC and 2003/467/EC as regards the status of acceding countries with regard to brucellosis (B. melitensis), Aujeszky’s disease, enzootic bovine leukosis, bovine brucellosis and tuberculosis and of France with regard to Aujeszky’s disease [11] is to be incorporated into the Agreement.
(12) Commission Decision 2004/448/EC of 29 April 2004 amending Decision 2004/233/EC as regards the list of approved laboratories for checking the effectiveness of vaccination against rabies in certain domestic carnivores [12], as corrected by OJ L 193, 1.6.2004, p. 64, is to be incorporated into the Agreement.
(13) This Decision is not to apply to Iceland and Liechtenstein,
HAS DECIDED AS FOLLOWS:
Article 1
Chapter I of Annex I to the Agreement shall be amended as specified in the Annex to this Decision.
Article 2
The texts of Decisions 2004/63/EC, 2004/101/EC, 2004/199/EC, 2004/226/EC, 2004/230/EC, 2004/233/EC, 2004/235/EC, 2004/252/EC, 2004/315/EC, 2004/320/EC and 2004/448/EC, as corrected by OJ L 193, 1.6.2004, p. 64, in the Norwegian language, to be published in the EEA Supplement to the Official Journal of the European Union , shall be authentic.
Article 3
This Decision shall enter into force on 12 March 2005, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [13].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 11 March 2005.
For the EEA Joint Committee
The President
Richard Wright
[1] OJ L 161, 23.6.2005, p. 1.
[2] OJ L 13, 20.1.2004, p. 32.
[3] OJ L 30, 4.2.2004, p. 15.
[4] OJ L 64, 2.3.2004, p. 41.
[5] OJ L 68, 6.3.2004, p. 36.
[6] OJ L 70, 9.3.2004, p. 41.
[7] OJ L 71, 10.3.2004, p. 30.
[8] OJ L 72, 11.3.2004, p. 86.
[9] OJ L 79, 17.3.2004, p. 45.
[10] OJ L 100, 6.4.2004, p. 43.
[11] OJ L 102, 7.4.2004, p. 75.
[12] OJ L 155, 30.4.2004, p. 80.
[13] No constitutional requirements indicated.
--------------------------------------------------
ANNEX
Chapter I of Annex I to the Agreement shall be amended as specified below.
1. The following indent shall be added in points 7 (Council Directive 88/407/EEC) in Part 4.1 and 6 (Directive 88/407/EEC) in Part 8.1:
"— 32004 D 0101: Commission Decision 2004/101/EC of 6 January 2004 (OJ L 30, 4.2.2004, p. 15)."
2. The following indent shall be added in point 14 (Commission Decision 93/52/EEC) in Part 4.2:
"— 32004 D 0199: Commission Decision 2004/199/EC of 27 February 2004 (OJ L 64, 2.3.2004, p. 41)."
3. The following indent shall be added in points 14 (Commission Decision 93/52/EEC) and 64 (Commission Decision 2001/618/EEC) in Part 4.2:
"— 32004 D 0320: Commission Decision 2004/320/EC of 31 March 2004 (OJ L 102, 7.4.2004, p. 75)."
4. The following indent shall be added in point 61 (Commission Decision 2001/106/EC) in Part 4.2:
"— 32004 D 0252: Commission Decision 2004/252/EC of 10 March 2004 (OJ L 79, 17.3.2004, p. 45)."
5. The following shall be added in point 70 (Commission Decision 2003/467/EC) in Part 4.2:
", as amended by:
- 32004 D 0063: Commission Decision 2004/63/EC of 23 December 2003 (OJ L 13, 20.1.2004, p. 32),
- 32004 D 0230: Commission Decision 2004/230/EC of 5 March 2004 (OJ L 70, 9.3.2004, p. 41),
- 32004 D 0320: Commission Decision 2004/320/EC of 31 March 2004 (OJ L 102, 7.4.2004, p. 75)."
6. The following points shall be inserted after point 74 (Commission Decision 2003/886/EC) in Part 4.2:
"75. 32004 D 0226: Commission Decision 2004/226/EC of 4 March 2004 approving tests for the detection of antibodies against bovine brucellosis within the framework of Council Directive 64/432/EEC (OJ L 68, 6.3.2004, p. 36).
76. 32004 D 0233: Commission Decision 2004/233/EC of 4 March 2004 authorising laboratories to check the effectiveness of vaccination against rabies in certain domestic carnivores (OJ L 71, 10.3.2004, p. 30), as amended by:
- 32004 D 0448: Commission Decision 2004/448/EC of 29 April 2004 (OJ L 155, 30.4.2004, p. 80), as corrected by OJ L 193, 1.6.2004, p. 64.
77. 32004 D 0235: Commission Decision 2004/235/EC of 1 March 2004 establishing additional guarantees regarding salmonella for consignments to Finland and Sweden of laying hens (OJ L 72, 11.3.2004, p. 86).
The provisions of this Decision shall, for the purpose of the Agreement, be read with the following adaptation:
The provisions of this Decision shall apply to consignments to Norway.
78. 32004 D 0315: Commission Decision 2004/315/EC of 26 March 2004 recognising the system of surveillance networks for bovine holdings implemented in Member States or regions of Member States under Directive 64/432/EEC (OJ L 100, 6.4.2004, p. 43)."
7. The text of points 32 (Commission Decision 95/161/EC), 55 (Commission Decision 2000/330/EC) and 62 (Commission Decision 2001/296/EC) in Part 4.2 shall be deleted.
8. Under the heading "ACTS OF WHICH THE EFTA STATES AND THE EFTA SURVEILLANCE AUTHORITY SHALL TAKE DUE ACCOUNT" in Part 4.2, the text of points 53 (Commission Decision 2002/544/EC) and 54 (Commission Decision 2002/907/EC) shall be deleted.
--------------------------------------------------
