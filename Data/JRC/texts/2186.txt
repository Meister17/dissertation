Commission Regulation (EC) No 284/2005
of 18 February 2005
laying down derogations from Regulation (EC) No 800/1999 as regards products in the form of goods not covered by Annex I to the Treaty exported to third countries other than Switzerland and Liechtenstein
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 3448/93 of 6 December 1993 laying down the trade arrangements applicable to certain goods resulting from the processing of agricultural products [1] and in particular Article 8(3) thereof,
Whereas:
(1) Article 16(1) of Commission Regulation (EC) No 1520/2000 of 13 July 2000 laying down common detailed rules for the application of the system of granting export refunds on certain agricultural products exported in the form of goods not covered by Annex I to the Treaty, and the criteria for fixing the amount of such refunds [2] provides that Commission Regulation (EC) No 800/1999 of 15 April 1999 laying down common detailed rules for the application of the system of export refunds on agricultural products [3] shall apply as regards exports of products in the form of goods not covered by Annex I to the Treaty.
(2) Article 3 of Regulation (EC) No 800/1999, provides that entitlement to the export refund is acquired on importation into a specific third country when a differentiated refund applies for that third country. Articles 14, 15 and 16 of that Regulation lay down the conditions for the payment of the differentiated refund, in particular the documents to be supplied as proof of the goods’ arrival at destination.
(3) In the case of a differentiated refund, Article 18(1) and (2) of Regulation (EC) No 800/1999 provides that part of the refund, calculated using the lowest refund rate, is paid on application by the exporter once proof is furnished that the product has left the customs territory of the Community.
(4) The Agreement between the European Community and the Swiss Confederation amending the Agreement between the European Economic Community and the Swiss Confederation of 22 July 1972 [4], which was signed in October 2004, is provisionally applicable from 1 February 2005 by virtue of Council Decision 2005/45/EC of 22 December 2004 concerning the conclusion and the provisional application of the Agreement between the European Community and the Swiss Confederation amending the Agreement between the European Economic Community and the Swiss Confederation of 22 July 1972 as regards the provisions applicable to processed agricultural products [5].
(5) Pursuant to Decision 2005/45/EC exports of certain processed agricultural goods to Switzerland and Liechtenstein are, from 1 February 2005, no longer eligible for export refunds, unless the Community decides to introduce such refunds when the Swiss domestic reference price is lower than the Community’s domestic reference price.
(6) Decision 2005/45/EC introduces special provisions on administrative cooperation aimed at combating irregularities and fraud in customs and export refund related matters.
(7) In the light of those provisions and in order to avoid the imposition of unnecessary costs on operators in their commercial trade with other third countries, it is appropriate to derogate from Regulation (EC) No 800/1999 in so far as it requires proof of import in the case of differentiated refunds. It is also appropriate, where no export refunds have been fixed for the particular countries of destination in question, not to take account of that fact when the lowest rate of refund is determined.
(8) Since the measures laid down in Decision 2005/45/EC apply from 1 February 2005, this Regulation should apply from the same date and enter into force immediately.
(9) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee on horizontal questions concerning trade in processed agricultural products not listed in Annex I to the Treaty,
HAS ADOPTED THIS REGULATION:
Article 1
By way of derogation from Article 16 of Regulation (EC) No 800/1999, where the differentiation of the refund is the result solely of a refund not having been fixed for Switzerland or Liechtenstein proof that the customs import formalities have been completed shall not be a condition for payment of the refund in respect of the goods covered by Regulation (EC) No 1520/2000, which are listed in Tables I and II to Protocol 2 to the Agreement between the European Community and the Swiss Confederation of 22 July 1972 [6].
Article 2
The fact that no export refund has been fixed in respect of the export to Switzerland or Liechtenstein of the goods covered by Regulation (EC) No 1520/2000, which are listed in Tables I and II to Protocol 2 to the Agreement between the European Community and the Swiss Confederation of 22 July 1972 shall not be taken into account in determining the lowest rate of refund within the meaning of Article 18(2) of Regulation (EC) No 800/1999.
Article 3
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Union.
It shall apply from 1 February 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 18 February 2005.
For the Commission
Günter Verheugen
Vice-President
[1] OJ L 318, 20.12.1993, p. 18. Regulation as last amended by Regulation (EC) No 2580/2000 (OJ L 298, 25.11.2000, p. 5).
[2] OJ L 177, 15.7.2000, p. 1. Regulation as last amended by Regulation (EC) No 886/2004 (OJ L 168, 1.5.2004, p. 14).
[3] OJ L 102, 17.4.1999, p. 11. Regulation as last amended by Regulation (EC) No 671/2004 (OJ L 105, 14.4.2004, p. 5).
[4] OJ L 23, 26.1.2005, p. 19.
[5] OJ L 23, 26.1.2005, p. 17.
[6] OJ L 300, 31.12.1972, p. 189.
--------------------------------------------------
