DECISION No 508/2000/EC OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL
of 14 February 2000
establishing the Culture 2000 programme
THE EUROPEAN PARLIAMENT AND THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular the first indent of Article 151(5) thereof,
Having regard to the proposal from the Commission(1),
Having regard to the opinion of the Committee of the Regions(2),
Acting in accordance with the procedure laid down in Article 251 of the Treaty, in the light of the joint text approved by the Conciliation Committee on 9 December 1999(3),
Whereas:
(1) Culture has an important intrinsic value to all people in Europe, is an essential element of European integration and contributes to the affirmation and vitality of the European model of society and to the Community's influence on the international scene.
(2) Culture is both an economic factor and a factor in social integration and citizenship; for that reason, it has an important role to play in meeting the new challenges facing the Community, such as globalisation, the information society, social cohesion and the creation of employment.
(3) With a view to meeting the needs of the cultural dimension in the European Union, the Community has to take cultural aspects into account in its action under provisions of the Treaty other than Article 151, in particular in order to respect and to promote the diversity of its cultures; in this context, the Commission should encourage the dissemination of information about the opportunities for the cultural industries in the Structural Funds, in accordance with Council Regulation (EC) No 1260/1999 of 21 June 1999 laying down general provisions on the Structural Funds(4), and undertake studies to that end.
(4) In view of the growing importance of culture for European society and the challenges facing the Community at the dawn of the 21st century, it is important to increase the effectiveness and consistency of Community measures in the cultural field by proposing a single guidance and programming framework for the period 2000 to 2004, bearing in mind the need for the Community policies concerned to take greater account of culture; in this respect, the Council Decision of 22 September 1997 regarding the future of European cultural action(5) calls on the Commission to make proposals with a view to establishing a single instrument for programming and financing aimed at the implementation of Article 151 of the Treaty.
(5) If citizens give their full support to, and participate fully in, European integration, greater emphasis should be placed on their common cultural values and roots as a key element of their identity and their membership of a society founded on freedom, democracy, tolerance and solidarity; a better balance should be achieved between the economic and cultural aspects of the Community, so that these aspects can complement and sustain each other.
(6) The Treaty confers responsibility on the European Union for creating and ever-closer union among the peoples of Europe and for contributing to the flowering of the cultures of the Member States, while respecting their national and regional diversity and at the same time bringing the common cultural heritage to the fore; special attention should be devoted to safeguarding the position of Europe's small cultures and less widely-spoken languages.
(7) The Community is consequently committed to working towards the development of a cultural area common to the European people, which is open, varied and founded on the principle of subsidiarity, cooperation between all those involved in the cultural sector, the promotion of a legislative framework conducive to cultural activities and ensuring respect for cultural diversity, and the integration of the cultural dimension into Community policies as provided for in Article 151(4) of the Treaty.
(8) To bring to life the cultural area common to the European people, it is essential to encourage creative activities, promote cultural heritage with a European dimension, encourage mutual awareness of the culture and history of the peoples of Europe and support cultural exchanges with a view to improving the dissemination of knowledge and stimulating cooperation and creative activities.
(9) There is a need, in this context, to promote greater cooperation with those engaged in cultural activities by encouraging them to enter into cooperation agreements for the implementation of joint projects, to support more closely targeted measures having a high European profile, to provide support for specific and innovative measures and to encourage exchanges and dialogue on selected topics of European interest.
(10) The Kaleidoscope, Ariane and Raphael cultural programmes set out, respectively, in Decision No 719/96/EC(6), in Decision No 2085/97/EC(7) and in Decision No 2228/97/EC(8) of the European Parliament and of the Council marked the first positive stage in the implementation of Community action on culture; however, the Community's cultural endeavours should be simplified and reinforced on the basis of the results of the evaluation and achievements of the abovementioned programmes.
(11) In accordance with the Commission's communication "Agenda 2000", the effectiveness of measures at Community level should be increased, notably by concentrating the resources available for internal policies, including cultural action.
(12) Considerable experience has been acquired, particularly through the evaluation of the first cultural programmes, the wide-ranging consultation of all interested parties and the results of the Cultural Forum of the European Union held on 29 and 30 January 1998.
(13) The Community's cultural activities should take account of the specific nature, and hence the specific needs, of each cultural area.
(14) The conclusions of the European Council at Copenhagen on 21 and 23 June 1993 called for the opening of Community programmes to the countries of central and eastern Europe which have signed Association Agreements; the Community has signed, with some third countries, Cooperation Agreements which contain a cultural clause.
(15) This Decision therefore establishes a single financing and programming instrument for cultural cooperation, entitled the "Culture 2000 programme", for the period from 1 January 2000 to 31 December 2004.
(16) This Decision lays down for the entire duration of the programme a financial framework constituting the principal point of reference, within the meaning of point 33 of the Interinstitutional Agreement between the European Parliament, the Council and the Commission of 6 May 1999 on bugetary discipline and improvement of the budgetary procedure(9).
(17) The measures necessary for the implementation of this Decision should be adopted in accordance with Council Decision 1999/468/EC of 28 June 1999 laying down the procedures for the exercise of implementing powers conferred on the Commission(10).
(18) In accordance with the subsidiarity and proportionality principles established by Article 5 of the Treaty, as the objectives of this action cannot be sufficiently achieved by the Member States, they can therefore, by reason of the scale or effects of the proposed action, be better attained by the Community; this Decision is limited to the minimum required for the attainment of those objectives and does not go beyond what is necessary to that end.
(19) The Culture 2000 programme should be the only programme operating from the year 2000 in the field of culture; therefore Decision No 2228/97/EC should be repealed,
HAVE DECIDED AS FOLLOWS:
Article 1
Duration and objectives
A single financing and programming instrument for cultural cooperation, hereinafter referred to as the "Culture 2000 programme", is hereby established for the period from 1 January 2000 to 31 December 2004.
The Culture 2000 programme shall contribute to the promotion of a cultural area common to the European peoples. In this context, it shall support cooperation between creative artists, cultural operators, private and public promoters, the activities of the cultural networks, and other partners as well as the cultural institutions of the Member States and of the other participant States in order to attain the following objectives:
(a) promotion of cultural dialogue and of mutual knowledge of the culture and history of the European peoples;
(b) promotion of creativity and the transnational dissemination of culture and the movement of artists, creators and other cultural operators and professionals and their works, with a strong emphasis on young and socially disadvantaged people and on cultural diversity;
(c) the highlighting of cultural diversity and the development of new forms of cultural expression;
(d) sharing and highlighting, at the European level, the common cultural heritage of European significance; disseminating know-how and promoting good practices concerning its conservation and safeguarding;
(e) taking into account the role of culture in socioeconomic development;
(f) the fostering of intercultural dialogue and mutual exchange between European and non-European cultures;
(g) explicit recognition of culture as an economic factor and as a factore in social integration and citizenship;
(h) improved access to and participation in culture in the European Union for as many citizens as possible.
The Culture 2000 programme shall further an effective linkage with measures implemented under other Community policies which have cultural implications.
Article 2
Types of cultural actions and events
The objectives listed in Article 1 shall be achieved by the following means:
(a) specific innovative and/or experimental actions;
(b) integrated actions covered by structured, multiannual cultural Cooperation Agreements;
(c) special culturals events with a European and/or international dimension.
The actions and their implementing measures are described in Annex I. They are either vertical (concerning one cultural field) or horizontal (associating several cultural fields).
Article 3
Budget
The financial framework for the implementation of the Culture 2000 programme for the period referred to in Article 1 is hereby set at EUR 167 million.
The annual appropriations shall be authorised by the budgetary authority within the limits of the financial perspective.
Article 4
Implementation
1. The measures necessary for the implementation of this Decision relating to the matters listed below shall be adopted in accordance with the management procedure referred to in Article 5(2):
(a) the priorities and general guidelines for all the measures described in Annex I and the annual programme resulting therefrom,
(b) the general balance between all the actions,
(c) the selection rules and criteria for the various types of project described in Annex I Actions I.1, I.2 and I.3,
(d) the financial support to be provided by the Community amounts, duration, distribution and beneficiaries,
(e) the detailed procedures for monitoring and evaluating this programme, together with the conclusions of the assessment report provided for in Article 8 and any other measure readjusting the Culture 2000 programme arising from the assessment report.
2. The measures necessary for the implementation of this Decision relating to all other matters shall be adopted in accordance with the advisory procedure referred to in Article 5(3).
Article 5
Committee
1. The Commission shall be assisted by a Committee.
2. Where reference is made to this paragraph, Articles 4 and 7 of Decision 1999/468/EC shall apply, having regard to the provisions of Article 8 thereof.
The period referred to in Article 4(3) of Decision 1999/468/EC shall be set at two months.
3. Where reference is made to this paragraph, Articles 3 and 7 of Decision 1999/468/EC shall apply, having regard to the provisions of Article 8 thereof.
4. The Committee shall adopt its rules of procedure.
Article 6
Consistency and complementarity
In the implementation of the Culture 2000 programme, the Commission shall, in cooperation with the Member States, ensure the overall consistency and complementarity with relevant Community policies and actions having an impact on the field of culture. This could involve the possibility of including complementary projects financed through other Community programmes.
Article 7
Third countries and international organisations
The Culture 2000 programme shall be open to participation by the countries of the European Economic Area and also to participation by Cyprus and the associated countries of central and eastern Europe in accordance with the conditions laid down in the Association Agreements or in the additional Protocols to the Association Agreements relating to participation in Community programmes concluded or to be concluded with those countries.
The Culture 2000 programme shall also permit cooperation with other third countries which have concluded Association or Cooperation Agreements containing cultural clauses, on the basis of additional funds made available in accordance with procedures to be agreed with the countries in question.
The Culture 2000 programme shall permit joint action with international organisations competent in the field of culture, such as Unesco or the Council of Europe, on the basis of joint contributions and in accordance with the various rules prevailing in each institution or organisation for the realisation of the cultural actions and events listed in Article 2.
Article 8
Evaluation and monitoring
Not later than 31 December 2002 the Commission shall present to the European Parliament, the Council, the Economic and Social Committee and the Committee of the Regions a detailed assessment report on the results of the Culture 2000 programme, having regard to its objectives, and accompanied if necessary by a proposal for the amendment of this Decision.
On completion of the Culture 2000 programme, the Commission shall present a report on its implementation to the European Parliament, the Council, the Economic and Social Committee and the Committee of the Regions. Moreover, the Commission shall present annually a short report monitoring the situation of the implementation of the Culture 2000 programme to the European Parliament, the Council and the Committee of the Regions.
These assessment reports shall emphasise in particular the creation of added value, particularly of a cultural nature, and the socioeconomic consequences of the Community's financial support.
Article 9
Repeal
Decision No 2228/97/EC shall be repealed with effect from 1 January 2000.
Article 10
Entry into force
This Decision shall enter into force on the date of its publication in the Official Journal of the European Communities.
It shall apply from 1 January 2000.
Done at Brussels, 14 February 2000.
For the European Parliament
The President
N. FONTAINE
For the Council
The President
J. GANA
(1) OJ C 211, 7.7.1998, p. 18.
(2) OJ C 51, 22.2.1999, p. 68.
(3) Opinion of the European Parliament of 5 November 1998 (OJ C 359, 23.11.1998, p. 28), Council Common Position of 28 June 1999 (OJ C 232, 13.8.1999, p. 25) and Decision of the European Parliament of 28 October 1999 (not yet published in the Official Journal). Council Decision of 24 January 2000 and Decision of the European Parliament of 3 February 2000.
(4) OJ L 161, 26.6.1999, p. 1.
(5) OJ C 305, 7.10.1997, p. 1.
(6) OJ L 99, 20.4.1996, p. 20.
(7) OJ L 291, 24.10.1997, p. 26.
(8) OJ L 305, 8.11.1997, p. 31.
(9) OJ C 172, 18.6.1999, p. 1.
(10) JO L 184, 17.7.1999, p. 23.
ANNEX I
ACTIVITIES AND IMPLEMENTING MEASURES FOR THE CULTURE 2000 PROGRAMME
I. Description of actions and events
I.1. Specific innovative and/or experimental actions
Each year the Community will support events and projects carried out in partnership or in the forms of networks. These projects must involve operators from at least three States participating in the Culture 2000 programme, on the basis of priorities defined after consultation of the Committee referred to in Article 5, without prejudice to offering the associated countries participation in the programme in accordance with the procedures envisaged in Article 7. These actions will cover in principle a period of one year which may be extended to two supplementary years. These vertical actions (concerning one cultural field) or horizontal actions (associating several cultural fields) should be innovative and/or experimental and aim primarily to do the following:
(i) place the main emphasis on facilitating access to culture and wider cultural participation by the people in Europe, in all their social, regional and cultural diversity, in particular young people and the most underprivileged;
(ii) encourage the emergence and spread of new forms of expression, within and alongside traditional cultural fields (such as music, the performing arts, the plastic and visual arts, photography, architecture, literature, books, reading, the cultural heritage including the cultural landscape and children's culture);
(iii) support projects aimed at improving access to books and reading, as well as training professionals working in the field;
(iv) support projects of cooperation aimed at conserving, sharing, highlighting and safeguarding, at the European level, the common cultural heritage of European significance;
(v) support the creation of multimedia products, tailored to meet the needs of different publics, and thus make European artistic creation and heritage more visible and more accessible to all;
(vi) encourage initiatives, discussions and cooperation between cultural and sociocultural operators working in the field of social integration, especially integration of young people;
(vii) foster an intercultural dialogue and mutual exchange between European and other cultures, in particular by encouraging cooperation on subjects of common interest between cultural institutions and/or other operators in the Member States and those in third countries;
(viii) promote the dissemination of live cultural events using the new technologies of the information society.
Community support may not exceed 60 % of the budget for a specific action. In most cases this support may not be less than EUR 50000 or more than EUR 150000 a year.
I.2. Integrated actions covered by structured, multiannual transnational cultural cooperation agreements
The Culture 2000 programme shall encourage closer relations and joint working by supporting cultural networks and, in particular, networks of operators, cultural bodies and cultural institutions, involving in particular professionals in different participating States with a view to realising structured cultural projects both within and outside the Community. This measure relates to significant quality projects with a European dimension, involving at least five States participating in the Culture 2000 programme.
The cultural Cooperation Agreements will be aimed at carrying out structured, multiannual cultural actions, between operators of several Member States and those of other States participating in the Culture 2000 programme. These Agreements will concern transnational actions concerning one cultural field, (vertical actions) such as music, the performing arts, the plastic and visual arts, literature, books and reading including translation and cultural heritage. They will moreover promote, also by using new media, the achievement of trans-sectoral integrated actions (horizontal actions based on synergy), i.e. associating several cultural fields.
The Cooperation Agreements proposed in this way for up to three years shall comprise some or all of the following activities:
(i) co-productions and circulation of works and other cultural events in the European Union (e.g. exhibitions, festivals, etc.), making them accessible to as many citizens as possible;
(ii) mobility of artists, creators and other cultural operators;
(iii) further training for professionals in the cultural field and exchange of experience both in academic and practical terms;
(iv) enhancement of cultural sites and monuments within the Community with a view to raising awareness of European culture;
(v) research projects, public awareness campaigns, activities for teaching and the dissemination of knowledge, seminars, congresses, meetings on cultural topics of European importance;
(vi) use of new technologies;
(vii) projects aimed at the highlighting of cultural diversity and of multilingualism, promoting mutual awareness of the history, roots, common cultural values of the European peoples and their common cultural heritage.
Following consultation of the Committee referred to in Article 5(1) of the Decision, the Community will grant support for the implementation of cultural Cooperation Agreements. It will be intended to cover not only part of the funding of the project, but also expenses relating to the initial establishment of lasting cooperation, which may be multiannual, in a legal form recognised in one of the Member States of the Union.
In order for the agreement to be eligible, the activities it covers must involve operators from at least five States participating in the Culture 2000 programme.
Those responsible for multiannual cultural Cooperation Agreements receiving Community support for more than one year must submit to the Commission at the end of each year a summary of activities undertaken and of the expenditure on each activity, in order for the Community support to be carried over for the period of the project.
Community support may not exceed 60 % of the cultural Cooperation Agreement's budget. It may not be more than EUR 300000 a year.
This support may be raised by a maximum of 20 % in order to cover the relevant costs incurred in the management of the cultural Cooperation Agreements.
I.3. Special cultural events with a European or international dimension
These events, substantial in scale and scope, should strike a significant chord with the people of Europe and help to increase their sense of belonging to the same community as well as making them aware of the cultural diversity of the Member States, as well as intercultural and international dialogue.
These events include in particular:
(i) the European Capital of Culture and the European Cultural Month;
(ii) organising symposia to study questions of common cultural interest in order to foster cultural dialogue both inside and outside the Community;
(iii) organising innovative cultural events which have a strong appeal and are accessible to citizens in general, particularly in the field of cultural heritage, artistic activities and European history, and which in particular provide a link between education, the arts and culture;
(iv) recognising and highlighting European artistic talent, particularly among young people by means of, inter alia, European prizes in the various cultural spheres: literature, translation, architecture, etc.;
(v) support for projects admitted by the appropriate authorities of the participating States and involving the conservation and safeguarding of the cultural heritage of outstanding importance which contributes to development and dissemination of innovative concepts, methods and techniques at a European level and which can be described as "European heritage laboratories".
The priorities relating to these events will be established after consultation of the Committee referred to in Article 5 of the Decision.
Community support may not exceed 60 % of the budget for a special cultural event. It may not be less than EUR 200000 or more than EUR 1 million a year for the events referred to in item (i). For the events referred to in items (ii) to (v), the corresponding limits will in most cases not be less than EUR 150000 a year and in all cases not be more than EUR 300000 a year. The indicative allocation for these activities shall be 10 % of the financial framework of the programme.
The three types of actions and events described in I.1, I.2 and I.3 follow either a vertical (concerning one cultural field) or horizontal (associating several cultural fields) approach.
An indicative description of these approaches is provided in Annex II.
II. Coordination with the other Community instruments in the field of culture
The Commission will ensure coordination with other Community instruments active in the cultural sphere through specific actions, cultural Cooperation Agreements and special cultural events, mainly with a view to promoting and arranging for collaboration between sectors with common and converging interests, such as for example:
- culture and tourism (through cultural tourism),
- culture, education and youth (in particular, presentations to schools and colleges of audiovisual and multimedia products on European culture, with commentaries by creative or performing artists),
- culture and employment (encouraging the creation of jobs in the cultural sector, especially in the new cultural areas),
- culture and external relations,
- cultural statistics resulting from an exchange of comparative statistical information at Community level,
- culture and the internal market,
- culture and research,
- culture and the export of cultural goods.
III. Communication
Recipients of Community support must mention this support explicitly, and as prominently as possible, in all information or communications relating to the project.
IV. Technical assistance and accompanying actions
When executing the Culture 2000 programme, the Commission may have recourse to technical assistance organisations for which the financing is planned within the total funding of the programme not exceeding 3 % of the latter. It may also, under the same conditions, make use of experts or networks of experts.
In addition, the Commission may arrange evaluative studies as well as organise seminars, colloquia or other experts' meetings which might assist with the implementation of the Culture 2000 programme. The Commission may also organise actions related to information, publication and dissemination.
V. Contact points
The Commission and the Member States will organise on a voluntary basis, and step up, the mutual exchange of information for use in the implementation of the Culture 2000 programme, by means of cultural contact points which will be responsible for:
- promoting the programme,
- facilitating access to the programme for, and encouraging participation in its activities by as many professionals and operators in the cultural field as possible, by means of an effective dissemination of information,
- providing an efficient link with the various institutions providing aid to the cultural sector in the Member States, thus contributing to the complementarity between the measures taken under the Culture 2000 programme and national support measures,
- providing information and contact at the appropriate level between operators participating in the Culture 2000 programme and those participating in other Community programmes open for cultural projects.
VI. Overall budget breakdown
VI.1. At the beginning of the operation, and no later than 1 March every year, the Commission will submit to the Committee an ex ante breakdown of budget resources by type of action, taking into account, to this end, the objectives set out in Article 1 of the Decision.
VI.2. The funds available will be broken down internally subject to the following indicative guidelines:
(a) the funds allocated to specific innovative and/or experimental actions should be not more than 45 % of the annual budget for the Culture 2000 programme;
(b) the funds allocated to integrated actions covered by structured, multiannual cultural Cooperation Agreements should be not less than 35 % of the annual budget for the Culture 2000 programme;
(c) the funds allocated to special cultural events with a European and/or international dimension should be around 10 % of the annual budget for the Culture 2000 programme;
(d) the remaining expenditure, including the costs related to the contact points, should be around 10 % of the annual budget for the Culture 2000 programme.
VI.3. All the percentages given above are indicative and may be adapted by the Committee according to the procedure laid down in Article 4 of the Decision.
ANNEX II
INDICATIVE DESCRIPTION OF THE VERTICAL AND HORIZONTAL APPROACHES
The three actions of the Culture 2000 programme represent either a vertical approach (concerning one cultural field) or a horizontal approach (associating several cultural fields).
As an indication, these may be considered in the following manner.
I. A vertical approach
This implies a sectoral approach which seeks to take into account the specific needs of each cultural field, in particular.
(a) In the following fields: music, the performing arts, the plastic and visual arts, architecture, as well as regards other forms of artistic expression, for example multimedia, photography, children's culture and street art. This approach, according to the individual aspects of each cultural field, should:
(i) promote exchanges and cooperation between cultural operators;
(ii) aid the movement of artists and their works around Europe;
(iii) improve the possibilities of training and further training, in particular when combined with the improved mobility of those working in the cultural field (including teachers and students);
(iv) encourage creativity, while supporting the implementation of activities promoting European artists and their works in the abovementioned fields within Europe and favouring a policy of dialogue and exchanges with other world cultures;
(v) support initiatives which would use creativity as a means of social integration.
(b) As regards books, reading and translation, this approach aims:
(i) to encourage exchanges and cooperation between institutions and/or individuals from the different Member States and other countries participating in the programme as well as third countries;
(ii) to improve awareness and the distribution of literary creation and the history of the European people through supporting the translation of literary, dramatic and reference works, especially those in the lesser-used European languages and the languages of central and east European countries;
(iii) to encourage the mobility and further training of those working in the books and reading field;
(iv) to promote books and reading, in particular in young people and less favoured sectors of society.
The condition set out in the first paragraph of Annex I.1 concerning the minimum number of operators that are required from participating States in order to present projects under the "Culture 2000" programme, may be adapted to take account of the specific needs of literary translation.
(c) As regards cultural heritage of European importance, in particular intellectual and non-intellectual, movable and non-movable heritage (museums and collections, libraries, archives, including photographic archives, audiovisual archives covering cultural works), archaeological and sub-aquatic heritage, architectural heritage, all of the cultural sites and landscapes (cultural and natural goods), this approach seeks:
(i) to encourage projects of cooperation aimed at the conservation and restoration of the European cultural heritage;
(ii) to encourage the development of international cooperation between institutions and/or operators, in order to contribute to exchanges of know-how and the development of best practice as regards conservation and safeguarding the cultural heritage;
(iii) to improve access to the cultural heritage, where there is a European dimension, and encourage the active participation of the general public, in particular children, young people, the culturally deprived and inhabitants from rural or peripheral regions of the Community;
(iv) to encourage mobility and training on cultural heritage for those working in the cultural sector;
(v) to encourage international cooperation for the development of new technologies and innovation in the different heritage sectors and as regards the conservation of traditional crafts and methods;
(vi) to take heritage into consideration in other Community policies and programmes;
(vii) to encourage cooperation with third countries and the relevant international organisations.
The specific needs of different sectors of cultural life (performing and visual arts, books and reading, cultural heritage, etc.) will be taken into account in a balanced way in the allocation of funds.
II. A horizontal approach
This approach seeks to promote synergy and develop cultural creation, as much through the promotion of trans-sectoral activities involving a number of cultural sectors, as through supporting joint activities involving different Community programmes and policies (in particular those concerning education, youth, professional training, employment, etc.).
The indicative allocation for these activities shall be 10 % of the financial framework of the programme.
