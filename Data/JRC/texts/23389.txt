COUNCIL DECISION of 14 December 1998 concerning a Community system of fees in the animal feed sector (98/728/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 70/524/EEC of 23 November 1970 concerning additives in feedingstuffs, and in particular Article 6(2) thereof (1),
Having regard to Council Directive 95/69/EC of 22 December 1995 laying down the conditions and arrangements for approving and registering certain establishments and intermediaries operating in the animal feed sector and amending Directives 70/524/EEC, 74/63/EEC, 79/373/EEC and 82/471/EEC, and in particular Article 14 thereof (2),
Having regard to the proposal from the Commission (3),
Whereas provision should be made at Community level for fees to be levied for certain services in all Member States;
Whereas fees shall be charged only for examining dossiers of specified additives; whereas the relevant groups of additives should be listed;
Whereas the fees to be levied should cover solely the actual wage, social welfare and administrative costs of the body carrying out the services; whereas it is appropriate to lay down an exhaustive list of costs to be taken into account for the calculation of the said fees;
Whereas Member States should be given the opportunity to fix flat-rate sums for fees so as not to have to supply evidence of the costs actually incurred in each individual case;
Whereas Member States should enable the Commission, by providing the necessary information, to amend the Annexes when it sees fit; whereas such amendments should be made using the procedure laid down by this Decision in order to establish close cooperation between Member States and the Commission within the Standing Committee on Feedingstuffs,
HAS ADOPTED THIS DECISION:
Article 1
1. Member States shall ensure that a fee is levied for the costs incurred by the Member State acting as rapporteur in accordance with Articles 4 and 6(1) of Directive 70/524/EEC for the examination of the dossiers for additives listed in Annex A to this Decision.
2. Member States shall ensure that a fee is levied for the costs incurred in approving certain establishments and intermediaries in accordance with Article 5 of Directive 95/69/EC.
3. In the calculation of the fees mentioned in paragraphs 1 and 2, only the costs specified in Annex B shall be taken into account.
Article 2
The Annexes may be amended according to the procedure set out in Article 5.
Article 3
The direct or indirect refund by Member States of the fees within the meaning of this Decision shall be prohibited.
However, the application of flat-rate amounts by a Member State in the evaluation of individual cases shall not be regarded as an indirect refund.
Article 4
1. Member States shall draw up reports setting out the implementation of the rules of this Decision, specifying:
- the level of fees or flat-rate amounts charged in each case pursuant to Article 1(1) or (2);
- the method for calculating the fees in relating to the factors listed in Annex B.
Member States shall transmit their reports to the Commission by 14 December 2000 at the latest.
2. On the basis of the reports required under paragraph 1, the Commission shall submit to the Council by 14 December 2002 at the latest an overall summary report on the implementation of this Decision and, if applicable, proposals for further harmonisation of the systems of fees in the animal feed sector.
Article 5
1. Where the procedure laid down in this Article is to be followed, the Commission shall be assisted by the Standing Committee for Feedingstuffs, hereinafter referred to as 'the Committee`.
2. The representative of the Commission shall submit to the Committee a draft of the measures to be taken. The Committee shall deliver its opinion on the draft within a time limit which the Chairman may lay down according to the urgency of the matter. The opinion shall be delivered by the majority laid down in Article 148(2) of the Treaty in the case of decisions which the Council is required to adopt on a proposal from the Commission. The votes of the representatives of the Member States within the Committee shall be weighted in the manner set out in that Article. The Chairman shall not vote.
3. (a) The Commission shall adopt the intended measures when they are in accordance with the Committee's opinion.
(b) When the intended measures are not in accordance with the opinion of the Committee, or in the absence of any opinion, the Commission shall forthwith submit to the Council a proposal relating to the measures to be taken. The Council shall act by a qualified majority.
If, on the expiry of three months from the date on which the matter was referred to it, the Council has not adopted any measures, the Commission shall adopt the proposed measures and apply them immediately, save where the Council decides against the said measures by a simple majority.
Article 6
This Decision shall apply from 30 June 2000.
Article 7
This Decision is addressed to the Member States.
Done at Brussels, 14 December 1998.
For the Council
The President
W. MOLTERER
(1) OJ L 270, 14. 12. 1970, p. 1. Directive as last amended by Directive 98/92/EC (see page 49 of this Official Journal).
(2) OJ L 332, 30. 12. 1995, p. 15. Directive as last amended by Directive 98/92/EC (see page 49 of this Official Journal).
(3) OJ C 155, 20. 5. 1998, p. 29.
ANNEX A
Dossiers of additives subject to authorisation linked to the person responsible for putting them into circulation according to Directive 70/524/EEC.
ANNEX B
Exclusive list of the costs to be taken into account when calculating the fees according to Article 1(1) and (2):
Staff costs
- salaries including allowances where applicable, superannuation (pension) costs, staff insurance contributions.
Administrative costs
- accommodation including rent, heat, light and water, furniture, maintenance, insurance, interest, amortisation;
- general overheads including office equipment, stationary, postage, printing, telecommunications, training, subscription to periodicals;
- travel and associated costs.
Technical costs
- associated technical costs (e.g. laboratory costs, sampling);
- consultancy fees.
