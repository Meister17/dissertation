Authorisation for State aid pursuant to Articles 87 and 88 of the EC Treaty
Cases where the Commission raises no objections
(2006/C 37/04)
Date of adoption of the decision : 15.12.2005
Member State : The Republic of Latvia
No of the aid : N 97/2005
Title : Aid for reconstruction and renovation of water reclamation and amelioration systems
Objective : Public contribution to financing the investments in agricultural holdings for construction, reconstruction and improvement of water reclamation and amelioration systems
Legal basis - Ministru kabineta 2005. gada 25. janvāra noteikumi Nr. 70 "Noteikumi par valsts atbalstu lauksaimniecībai 2005. gadā un tā piešķiršanas kārtību" ( 2005. gada 17. februāra Latvijas Vēstnesis Nr. 27),
- 2003. gada 25. decembra Meliorācijas likuma 1. līdz 3. pants ( 2003. gada 11. decembra Latvijas Vēstnesis Nr. 175),
- 2004. gada 24. aprīļa Lauksaimniecības un lauku attīstības likuma 4. un 6. pants ( 2004. gada 23. aprīļa Latvijas Vēstnesis Nr. 64),
- 1995. gada 13. septembra Būvniecības likuma 12. panta 1. punkts ( 1995. gada 30. augusta Latvijas Vēstnesis Nr. 131),
- Ministru kabineta 2004. gada 8. aprīļa noteikumu Nr. 272 "Meliorācijas sistēmu ekspluatācijas un uzturēšanas noteikumi" 1. līdz 3. punkts ( 2004. gada 22. aprīļa Latvijas Vēstnesis Nr. 63),
- Ministru kabineta 2003. gada 17. jūlija noteikumi Nr. 382 "Meliorācijas sistēmu un hidrotehnisko būvju būvniecības kārtība" ( 2003. gada 16. jūlija Latvijas Vēstnesis Nr. 105)
Budget - 2006 — LVL 600000
- 2007 — LVL 600000
- 2008 — LVL 700000
- 2009 — LVL 700000
- 2010 — LVL 800000
Aid intensity or amount - 50 % in less-favoured areas, 40 % in other areas
- 55 % in less-favoured areas and 45 % in other areas for young farmers
Duration : 2006 — 2010
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 20.12.2005
Member State : Italy (Trento)
Aid No : N 200/2005
Title : Provincial law No 4 of 28 March 2003 Article 43 bis. Aid for the disposal of specified risk material
Objective : The measure aims to compensate the additional costs of disposal and destruction of fallen stock
Legal basis : Legge provinciale 28 marzo 2003 n. 4 "Sostegno all'economia agricola, disciplina dell'agricoltura biologica e della contrassegnazione di prodotti geneticamente non modificati" (articolo 43 bis), modificata dalla Legge provinciale 11 marzo 2005, n. 3, articolo 1
Budget - Overall budget: EUR 4200000 (2004 to 2009)
- Annual budget: EUR 700000
Aid intensity or amount : 100 % of removal costs, 75 % of destruction costs
Duration : From 2004 to 2009
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 9.12.2005
Member State : Denmark
Aid No : N 215/05
Title : Compensation for delay in lifting zone restrictions due to Newcastle disease
Objective : Aid would be granted to compensate for the losses due to delay in lifting zone restrictions due to Newcastle disease in 2002 on two poultry farms. The authorities have indicated that in 2002 due to a translation error the provisions of Article 9 of Directive 92/66/EEC (transposed on the national level with Order No 921 of 10 November 1994) were interpreted in such a way that the minimum quarantine was 21 days after the final cleaning and disinfection. The other language versions of the Directive prescribe a minimum period of 21 days after the initial cleaning and disinfection. The translation error was corrected on 7 May 2003
Legal basis : Afgørelse af 6. april 2005, truffet af Ministeriet for Familie- og Forbrugeranliggender
Budget : DKK 240626 (ca EUR 32300)
Aid intensity or amount : 100 % of the eligible costs
Duration : One-off
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 15.12.2005
Member State : Spain
Aid No : N 276/2005
Title : Aid for certification bodies for agricultural products
Objective : To promote the creation of certification bodies for agricultural and food products in accordance with standard EN 45.011
Legal basis : Proyecto de Orden APA/ /2005, de de, por la que se establecen las bases reguladoras para la concesión de subvenciones destinadas a la constitución y a la consolidación de entidades certificadoras de productos agrarios y alimenticios
Budget : EUR 720000
Aid intensity or amount : Up to 65 % of expenditure
Duration : One year (2005)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 7.12.2005
Member State : Sweden
No of the aid : N 310/2005
Title : Measures in connection with the storm in Southern Sweden on 8 — 9 January 2005
Objective : On 8 and 9 January 2005 the storm "Gudrun" hit Southern Sweden. The average gale force was 33 m/s, exceeding hurricane level and reaching at times 43 m/s. Electricity and telephone lines were cut, streets blocked and millions of trees fell. It has been estimated that the lives of about 20 persons were lost due to the storm. According to the Swedish authorities the amount of timber felled by the storm corresponds to the production of three years in the affected region. The prices of timber have dropped to a level 35 % below normal market prices and the felling and handling costs (excluding transport) have increased by SEK 60/m3fub (fub = timber measured under bark). It appears from the loss calculations presented by the Swedish authorities that the loss was at least SEK 114 per m3fub. The notified aid is limited to a tax reduction of SEK 50 per each m3 of storm-felled harvested and handled timber, which according to the Swedish authorities corresponds to an operational income of ca SEK 71 per m3fub. The compensation is paid only on the part of the losses exceeding one year's tree growth
Legal basis : Förordning (1997:75) om anläggningsstöd till plantering av fleråriga grödor för produktion av biomassa, ändrad genom ett regeringsbeslut om det anmälda stödet.
Budget : Ca. SEK 2 billion (ca EUR 210,5 million)
Aid intensity or amount : SEK 71 per each m3fub of storm-felled harvested and handled timber
Duration : One-off
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 15.12.2005
Member State : Spain.
Aid No : N 400/2005
Title : Aid for agricultural associations of national importance
Objective : To enhance the economic and social aspects of agricultural associations of national importance
Legal basis : Orden APA/ /2005, por la que se establecen las bases reguladoras para la concesión de subvenciones destinadas al fomento de la integración cooperativa de ámbito estatal
Budget : For 2005, EUR 2452130
Aid intensity or amount : Varies according to the aid
Duration : 2005
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 12.1.2006
Member State : Italy (Sicily)
Aid No : N 494/2005
Title : Assistance in agricultural areas affected by natural disasters (frosts of 25 January to 9 March 2005 in 15 municipalities of the region of Sicily, Agrigento province)
Objective : To compensate damage to agricultural production caused by bad weather
Legal basis : Decreto legislativo n. 102/2004
Budget : See the approved scheme (NN 54/A/04)
Aid intensity or amount : Up to 100 % of the cost of the damage
Duration : Measure applying an aid scheme approved by the Commission
Other information : Measure applying an aid scheme approved by the Commission under State Aid NN 54/A/2004 (Commission Letter C(2005)1622 final of 7 June 2005)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 9.12.2005
Member State : Italy (Sicily)
Aid No : N 495/2005
Title : Assistance in agricultural areas affected by natural disasters (heavy snowfalls of 25 — 28 January 2005 in six municipalities in the region of Sicily, Agrigento province)
Objective : To compensate losses to farming structures caused by bad weather
Legal basis : Decreto legislativo n. 102/2004
Budget : See the approved scheme (NN 54/A/04)
Aid intensity or amount : Up to 100 % of the cost of the damage
Duration : Measure applying an aid scheme approved by the Commission
Other information : Measure applying an aid scheme approved by the Commission under State Aid NN 54/A/2004 (Commission Letter C(2005)1622 final of 7 June 2005)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 13.12.2005
Member State : Italy (Basilicata)
Aid No : N 518/05
Title : Legislative Decree No 102/2004: Assistance in agricultural areas having suffered damage (torrential rain of 12 and 13 November 2004, whirlwind of 14 November 2004, Matera province)
Objective : To provide meteorological information on the bad weather that caused the damage for which compensation is to be granted on the basis of the approved aid scheme NN 54/A/ 04
Legal basis : Decreto legislativo 102/2004: "Nuova disciplina del Fondo di solidarietà nazionale"
Budget : The amount of aid, which is yet to be established, will be funded from the overall budget of EUR 200 million allocated for the approved aid scheme (NN 54/A/04)
Aid intensity or amount : Up to 100 %
Duration : One-off aid
Other information : Measure applying an aid scheme approved by the Commission under State Aid NN 54/A/2004 (Commission Letter C(2005)1622 final of 7 June 2005)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 15.12.2005
Member State : Italy (Calabria)
Aid No : N 561/05
Title : Assistance in agricultural areas affected by natural disasters (excessive snow in the period 1 January 2005 to 30 March 2005 in the province of Cosenza; violent winds of 10 and 11 April 2005 in the provinces of Cosenza and Reggio Calabria)
Objective : To compensate damage to agricultural production and farming structures caused by bad weather (excessive snow in the period 1 January 2005 to 30 March 2005 in the province of Cosenza; violent winds of 10 and 11 April 2005 in the provinces of Cosenza and Reggio Calabria)
Legal basis : Decreto legislativo 102/2004: "Nuova disciplina del Fondo di solidarietà nazionale"
Budget : To be financed from the budget approved under State Aid NN 54/A/04
Aid intensity or amount : Up to100 %
Duration : Measure applying an aid scheme approved by the Commission
Other information : Measure applying an aid scheme approved by the Commission under State Aid NN 54/A/2004 (Commission Letter C(2005)1622 final of 7 June 2005)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
--------------------------------------------------
