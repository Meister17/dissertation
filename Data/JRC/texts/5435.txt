Commission Regulation (EC) No 1042/2005
of 29 June 2005
amending Regulation (EC) No 2869/95 on the fees payable to the Office for Harmonization in the Internal Market (Trade Marks and Designs)
(Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 40/94 of 20 December 1993 on the Community trade mark [1], and in particular Article 139 thereof,
Whereas:
(1) According to the Regulation (EC) No 40/94, as implemented by Commission Regulation (EC) No 2868/95 of 13 December 1995 implementing Council Regulation (EC) No 40/94 on the Community trade mark [2], additional fees concerning search reports, division of a trade mark application or registration and continuation of proceedings should be established. The amounts of those new fees should be fixed.
(2) The search regime becomes optional from 10 March 2008 as provided for in Article 2 paragraph 2 of Council Regulation (EC) No 422/2004. From that date, the additional fee for national search reports should apply.
(3) Commission Regulation (EC) No 2869/95 [3] should therefore be amended accordingly.
(4) The measures provided for in this Regulation are in accordance with the opinion of the Committee on Fees, Implementation Rules and the Procedure of the Boards of Appeal of the Office for Harmonization in the Internal Market (trade marks and designs),
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 2869/95 is amended as follows:
1. the table in Article 2 is amended as follows:
(a) the following point 1a is added:
"1a.Search fee(a)for a Community trade mark application (Article 39(2), Rule 4(c)(b)for an international registration designating the European Community (Articles 39(2) and 150(2), Rule 10(2) | The amount of EUR 12 multiplied by the number of central industrial property offices referred to in paragraph 2 of Article 39 of the Regulation; that amount, and the subsequent changes, shall be published by the Office in the Official Journal of the Office" |
(b) point 6 is deleted;
(c) in point 13 "Fee for each class of goods and services exceeding three for an individual mark" is replaced by "Fee for the renewal of each class of goods and services exceeding three for an individual mark";
(d) in point 15 "Fee for each class of goods and services exceeding three for a collective mark" is replaced by "Fee for the renewal of each class of goods and services exceeding three for a collective mark";
(e) in point 19 "Fee for restitutio in integrum" is replaced by "Fee for the application for restitutio in integrum";
(f) in point 20 "Fee for the conversion" is replaced by "Fee for the application for the conversion";
(g) points 21 and 22 are replaced by the following:
"21.Fee for continuation of proceedings (Article 78a (1)) | 400 |
22.Fee for the declaration of division of a registered Community trade mark (Article 48a(4)) or an application for a Community trade mark (Article 44a(4)): | 250" |
(h) in point 23 the introductory phrase is replaced by: "Fee for the application for the registration of a license or another right in respect of a registered Community trade mark (point 5 of Article 157(2), Rule 33(1)) or an application for a Community trade mark (point 6 of Article 157(2), Rule 33(4):";
(i) in point 29, the following line is deleted:
"plus per page, exceeding 10 | 1" |
2. in article 13, paragraph 3 is replaced by the following:
"3. The refund shall be made once the communication to the International Bureau pursuant to Rule 113(2)(b) and (c) or Rule 115(5)(b), (c) and (6) of Regulation (EC) No 2868/95 has been issued".
Article 2
1. This Regulation shall enter into force on the 20th day following that of its publication in the Official Journal of the European Union.
2. Point (a) of Article 1(1) shall apply from 10 March 2008.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 29 June 2005.
For the Commission
Charlie McCreevy
Member of the Commission
[1] OJ L 11, 14.1.1994, p. 1. Regulation as last amended by Regulation (EC) No 422/2004 (OJ L 70, 9.3.2004, p. 1).
[2] OJ L 303, 15.12.1995, p. 1. Regulation as amended by Regulation (EC) No 782/2004 (OJ L 123, 27.4.2004, p. 88).
[3] OJ L 303, 15.12.1995, p. 33. Regulation as amended by Regulation (EC) No 781/2004 (OJ L 123, 27.4.2004, p. 85).
--------------------------------------------------
