Authorisation for State aid pursuant to Articles 87 and 88 of the EC Treaty
Cases where the Commission raises no objections
(2005/C 305/04)
(Text with EEA relevance)
Date of adoption : 20.7.2005
Member State : Denmark
Aid No : N 38/2005
Title : RD aid for environmental projects
Objective : To promote the framework conditions for the beneficiaries' relation to the environment, to ensure a smooth and efficient implementation of EU regulation and to simplify the environmental work of the beneficiaries
Legal basis : Tekstanmærkning 106 til § 23 på Finanslov 2004, vedtaget 18. december 2003 og Bekendtgørelse om virksomhedsordningen
Budget : In total, ca DKK 94,2 million (approximately EUR 12,6 million)
Intensity or amount : 25 % (precompetitive development activi-ties), 50 % (industrial research) and 100 % dissemination costs
Duration : Until 31.12.2007
Other information : Annual report
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 10.12.2003 — CORR.
Member State : Germany, Saarland
Aid No : N 56/2003
Title : Aid in favour of Villeroy Boch
Objective : Environmental aid investment (Production of ceramics sanitaryware, tile and tableware)
Legal basis : Verordnung über Qualitätsziele für bestimmte gefährliche Stoffe und zur Verringerung der Gewässerverschmutzung durch Programme vom 6. April 2001
Amount : grant of EUR 0,81 million
Aid intensity : 30 % gross
Duration : 2003-2005
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption : 14.7.2005
Member State : Spain (Madrid)
Aid No : N 172/2005
Title : Aid to innovation and technological development in the information technologies sector
Objective : To boost the development of scientific research projects aiming to the technological development of products, processes and services linked to the information technologies which contribute to the modernisation of firms' management, thereby increasing their competitiveness and innovative character. (Information technologies sector.)
Legal basis : Proyecto de Orden de la Consejería de Economía e Innovación Tecnológica por la que se regula la concesión de ayudas cofinanciadas por el Fondo Europeo de Desarrollo Regional para el fomento de la innovación y el desarrollo tecnológico en el sector de las tecnologías de la información de la Comunidad de Madrid
Budget : EUR 8 million (EUR 4 million per year)
Intensity or amount :
Industrial research: up to 50 %.
Pre-competitive development activities: up to 25 %.
Viability studies prior to industrial research: up to 75 %.
Viability studies prior to pre-competitive development activities: up to 50 %.
All the bonuses foreseen in point 5.10 of the RD Framework are potentially applicable
Duration : 1.1.2006 — 31.10.2007
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
--------------------------------------------------
