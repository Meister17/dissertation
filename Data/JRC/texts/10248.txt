Action brought on 16 December 2005 — El Corte Inglés S.A. v OHIM
Parties
Applicant: El Corte Inglés S.A. (Madrid) (represented by: Juan Luis Rivas Zurdo, lawyer)
Defendant: Office for Harmonisation in the Internal Market (Trade Marks and Designs)
Other party to the proceedings before the Board of Appeal of OHIM: Juan Bolaños Sabri
Form of order sought
The applicant claims that the Court should:
- annul the decision of the First Board of Appeal, of 21 September 2005, in Case R 1191/2004-1, in so far as, by upholding the application by the applicant for the Community mark, that decision provides the basis for the future grant of Community trade mark No 2.456.242 "PRAÑAM DISEÑO ORIGINAL JUAN BOLAÑOS", in Class 25;
- refuse registration of the Community trade mark No 2.456.242 "PRAÑAM DISEÑO ORIGINAL JUAN BOLAÑOS", Class 25;
- order the party or parties opposing this application to pay the costs.
Pleas in law and main arguments
Applicant for a Community trade mark: Juan Bolaños Sabri.
Community trade mark concerned: figurative mark "PIRAÑAM diseño original Juan Bolaños" (Application No 2.456.242) for goods in Classes 16, 21 and 25.
Proprietor of the mark or sign cited in the opposition proceedings: the applicant.
Mark or sign cited in opposition: Spanish word marks "PIRANHA" for goods in Classes 25 (No 790.520) and 18 (No 2.116.007).
Decision of the Opposition Division: the opposition upheld in part, the application for registration for goods in Class 25 being rejected.
Decision of the Board of Appeal: the decision appealed was annulled and the opposition set aside in its entirety.
Pleas in law: incorrect application of Article 8(1)(b) of Regulation No 40/94 on the Community trade mark.
--------------------------------------------------
