Commission Decision
of 21 December 2005
amending Decision 93/195/EEC on animal health conditions and veterinary certification for the re-entry of registered horses for racing, competition and cultural events after temporary export
(notified under document number C(2005) 5496)
(2005/943/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 90/426/EEC of 26 June 1990 on animal health conditions governing the movement and import from third countries of equidae [1], and in particular Article 19(ii) thereof,
Whereas:
(1) In accordance with Commission Decision 93/195/EEC [2], the re-entry of registered horses for racing, competition and cultural events after temporary export is restricted to horses kept for less than 30 days in a third country.
(2) Under that Decision, however, horses that have taken part in the United Arab Emirates Endurance World Cup and meet the requirements laid down in that Decision are authorised to re-enter Community territory after temporary export for less than 60 days.
(3) In order to make it easier for horses originating in the Community to take part in those competitions, this special rule should apply to all Endurance World Cup competitions carried out under the rules, including the veterinary supervision, of the Federation Equestre International (FEI), irrespective of in which of the countries approved in accordance with Directive 90/426/EEC the competition takes place.
(4) Decision 93/195/EEC should therefore be amended accordingly.
(5) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
Decision 93/195/EEC is amended as follows:
1. In Article 1, the seventh indent is replaced by the following:
- "— have taken part in the Endurance World Cup, irrespective of in which of the countries approved in accordance with Directive 90/426/EEC the competition takes place, and meet the requirements laid down in a health certificate in accordance with the model set out in Annex VII to this Decision."
2. Annex VII is replaced by the Annex to this Decision.
Article 2
This Decision shall apply from 27 December 2005.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 21 December 2005.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 224, 18.8.1990, p. 42. Directive as last amended by Directive 2004/68/EC (OJ L 139, 30.4.2004, p. 320; corrected version in OJ L 226, 25.6.2004, p. 128).
[2] OJ L 86, 6.4.1993, p. 1. Decision as last amended by Decision 2005/771/EC (OJ L 291, 5.11.2005, p. 38).
--------------------------------------------------
ANNEX
"ANNEX VII
+++++ TIFF +++++
--------------------------------------------------
