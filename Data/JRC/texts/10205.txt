Council Regulation (EC) No 151/2006
of 24 January 2006
amending Regulation (EC) No 2505/96 opening and providing for the administration of autonomous Community tariff quotas for certain agricultural and industrial products
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 26 thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) On 20 December 1996 the Council adopted Regulation (EC) No 2505/96 opening and providing for the administration of autonomous Community tariff quotas for certain agricultural and industrial products [1]. Community demand for the products in question is to be met under the most favourable conditions. New Community tariff quotas should therefore be opened at reduced or zero rates of duty for appropriate volumes, and certain existing tariff quotas should be extended, without disturbing the markets for these products.
(2) Since the quota amount for certain Community tariff quotas is insufficient to meet the needs of Community industry for the current quota period, these tariff quotas should be increased with effect from 1 January 2005 and adjusted with effect from 1 January 2006.
(3) It is no longer in the Community’s interest to continue to grant Community tariff quotas in 2006 for certain products on which duties were suspended in 2005. These products should therefore be removed from the table in Annex I to Regulation (EC) No 2505/96.
(4) In view of the many changes to be made, the entire text of Annex I to Regulation (EC) No 2505/96 should be replaced in the interests of clarity.
(5) Regulation (EC) No 2505/96 should therefore be amended accordingly.
(6) In view of the economic importance of this Regulation, it is necessary to rely upon the grounds of urgency provided for in point I.3 of the Protocol annexed to the Treaty on European Union and to the Treaties establishing the European Communities on the role of national parliaments in the European Union.
(7) Since the Annex to this Regulation is to apply from 1 January 2006, it should enter into force immediately,
HAS ADOPTED THIS REGULATION:
Article 1
Annex I to Regulation (EC) No 2505/96 shall be replaced by the Annex to this Regulation with effect from 1 January 2006.
Article 2
For the quota period from 1 January to 31 December 2005, in Annex I to Regulation (EC) No 2505/96:
- the quota amount of tariff quota 09.2603 is fixed at 3900 tonnes at a 0 % rate of duty,
- the quota amount of tariff quota 09.2975 is fixed at 540 tonnes at a 0 % rate of duty.
Article 3
For the quota period from 1 January to 31 December 2006, in Annex I to Regulation (EC) No 2505/96:
- the quota amount of tariff quota 09.2002 is fixed at 600 tonnes,
- the quota amount of tariff quota 09.2003 is fixed at 1400000 units,
- the quota amount of tariff quota 09.2030 is fixed at 300 tonnes,
- the quota amount of tariff quota 09.2603 is fixed at 4500 tonnes,
- the quota amount of tariff quota 09.2612 is fixed at 1500 tonnes,
- the quota amount of tariff quota 09.2624 is fixed at 425 tonnes,
- the quota amount of tariff quota 09.2837 is fixed at 600 tonnes,
- the quota amount of tariff quota 09.2975 is fixed at 600 tonnes,
- the quota amount of tariff quota 09.2979 is fixed at 800000 units.
Article 4
Tariff quotas 09.2004, 09.2009, 09.2018, 09.2021, 09.2022, 09.2023, 09.2028, 09.2613, 09.2621, 09.2622, 09.2623, 09.2626, 09.2630, 09.2881, 09.2964, 09.2985 and 09.2998 shall be closed with effect from 1 January 2006.
Article 5
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 24 January 2006.
For the Council
The President
K.-H. Grasser
[1] OJ L 345, 31.12.1996, p. 1. Regulation as last amended by Regulation (EC) No 1151/2005 (OJ L 185, 16.7.2005, p. 27).
--------------------------------------------------
ANNEX
Order No | CN code | TARIC subdivision | Description | Quota amount | Quota duty (%) | Quota period |
09.2002 | 29280090 | 30 | Phenylhydrazine | 600 tonnes | 0 | 1.1. to 31.12. |
09.2003 | 85438997 | 63 | Voltage controlled frequency generator, consisting of active and passive elements mounted on a printed circuit, contained in a housing whose exterior dimensions do not exceed 30 × 30 mm | 1400000 units | 0 | 1.1. to 31.12. |
09.2026 | 29033080 | 70 | 1,1,1,2 Tetrafluoroethane, certified odourless containing a maximum: 600 ppm by weight of 1,1,2,2-Tetrafluorethane2 ppm by weight of pentafluoroethane2 ppm by weight of chlorodifluorométhane2 ppm by weight of chloropentafluoroethane2 ppm by weight of dichlorodifluoromethanefor use in the manufacture of pharmaceutical grade propellant for medical metred dose inhalers | 4000 tonnes | 0 | 1.1. to 31.12. |
09.2030 | 29269095 | 74 | Chlorothalonil | 300 tonnes | 0 | 1.1. to 31.12. |
09.2140 | 38249099 | 98 | Mixture of tertiary amines containing by weight: 2,0-4,0 % of N,N-dimethyl-1-octanamineminimum 94 % of N,N-dimethyl-1-decanamine2 % maximum of N,N-dimethyl-1-dodecanamine and higher | 4500 tonnes | 0 | 1.1. to 31.12. |
09.2602 | ex29215119 | 10 | o-phenylenediamine | 1800 tonnes | 0 | 1.1. to 31.12. |
09.2603 | ex29310095 | 15 | Bis(3-triethoxysilylpropyl) tetrasulfide | 4500 tonnes | 0 | 1.1. to 31.12. |
09.2604 | ex39053000 | 10 | Poly(vinyl alcohol) partially acetalized with 5-(4-azido-2-sulfobenzylidene)-3-(formylpropyl)-rhodanine, sodium salt | 100 tonnes | 0 | 1.1. to 31.12. |
09.2610 | ex29252000 | 20 | (Chloromethylene)dimethylammonium chloride | 100 tonnes | 0 | 1.1. to 31.12. |
09.2611 | ex28261900 | 10 | Calcium fluoride having a total content of aluminium, magnesium and sodium of 0,25 mg/kg or less, in the form of powder | 55 tonnes | 0 | 1.1. to 31.12. |
09.2612 | ex29215990 | 30 | 3,3′-dichlorobenzidine dihydrochloride | 1500 tonnes | 0 | 1.1. to 31.12. |
09.2615 | ex29349990 | 70 | Ribonucleic acid | 110 tonnes | 0 | 1.1. to 31.12. |
09.2616 | ex39100000 | 30 | Polydimethylsiloxane with a degree of polymerisation of 2800 monomer units (± 100) | 1300 tonnes | 0 | 1.1. to 31.12. |
09.2618 | ex29181980 | 40 | (R)-2-Chloromandelic acid | 100 tonnes | 0 | 1.1. to 31.12. |
09.2619 | ex29349990 | 71 | 2-Thienylacetonitrile | 80 tonnes | 0 | 1.1. to 31.12. |
09.2620 | ex85269180 | 20 | Assembly for GPS system having a position determination function | 500000 units | 0 | 1.1. to 31.12. |
09.2624 | 29124200 | | Ethylvanillin (3-ethoxy-4-hydroxybenzaldehyde) | 425 tonnes | 0 | 1.1. to 31.12. |
09.2625 | ex39202021 | 20 | Film of polymers of polypropylene, biaxially oriented, of a thickness of 3,5 μm or more but less than 15 μm, of a width of 490 mm or more but not exceeding 620 mm, for the production of film capacitors | 170 tonnes | 0 | 1.1. to 31.12. |
09.2627 | ex70112000 | 55 | Glass face-plate with a diagonal measurement of 814,8 mm (± 1,5 mm) from the outer edge to the outer edge and having a light transmission of 51,1 % (± 2,2 %) by a glass thickness of 12,5 mm | 500000 units | 0 | 1.1. to 31.12. |
09.2628 | ex70195200 | 10 | Glass web woven from glass fibre coated in plastic, of a weight of 120 g/m2 (± 10 g/m2) of a type used in the manufacture of anti-insect rollers and framed nets | 350000 m2 | 0 | 1.1. to 31.12. |
09.2629 | ex76169990 | 85 | Aluminium telescopic handle for use in the manufacture of luggage | 240000 units | 0 | 1.1. to 31.12. |
09.2703 | ex28253000 | 10 | Vanadium oxides and hydroxides only for the production of alloys | 13000 tonnes | 0 | 1.1. to 31.12. |
09.2713 | ex20086019 | 10 | Sweet cherries, marinated in alcohol, of a diameter not exceeding 19,9 mm, stoned, intended for the manufacture of chocolate products: with a sugar content exceeding 9 % by weightwith a sugar content not exceeding 9 % by weight | 2000 tonnes | 10 | 1.1. to 31.12. |
ex20086039 | 10 | 10 |
09.2719 | ex20086019 | 20 | Sour cherries (Prunus cerasus), marinated in alcohol, of a diameter not exceeding 19,9 mm, intended for the manufacture of chocolate products: with a sugar content exceeding 9 % by weightwith a sugar content not exceeding 9 % by weight | 2000 tonnes | 10 | 1.1. to 31.12. |
ex20086039 | 20 | 10 |
09.2727 | ex39029090 | 93 | Synthetic poly-alpha-olefin having a viscosity of at least 38 × 10-6m2 s-1 (38 centistokes) at 100 °C measured using the ASTM D 445 method | 10000 tonnes | 0 | 1.1. to 31.12. |
09.2799 | ex72024990 | 10 | Ferro-chromium containing 1,5 % or more but not more than 4 % by weight of carbon and not more than 70 % of chromium | 50000 tonnes | 0 | 1.1. to 31.12. |
09.2809 | ex38029000 | 10 | Acid activated montmorillonite, for the manufacture of self-copy paper | 10000 tonnes | 0 | 1.1. to 31.12. |
09.2829 | ex38249099 | 19 | Solid extract of the residual, insoluble in aliphatic solvents, obtained during the extraction of resin from wood, having the following characteristics: a resin acid content not exceeding 30 % by weight,an acid number not exceeding 110 anda melting point of not less than 100 °C | 1600 tonnes | 0 | 1.1. to 31.12. |
09.2837 | ex29034980 | 10 | Bromochloromethane | 600 tonnes | 0 | 1.1. to 31.12. |
09.2841 | ex27129099 | 10 | Blend of 1-alkenes containing 80 % by weight or more of 1-alkenes of a chain-length of 20 and 22 carbon atoms | 10000 tonnes | 0 | 1.1. to 31.12. |
09.2849 | ex07108069 | 10 | Mushrooms of the species Auricularia polytricha (uncooked or cooked by steaming or boiling), frozen, for the manufacture of prepared meals | 700 tonnes | 0 | 1.1. to 31.12. |
09.2851 | ex29071200 | 10 | O-Cresol having a purity of not less than 98,5 % by weight | 20000 tonnes | 0 | 1.1. to 31.12. |
09.2853 | ex29309070 | 35 | Glutathione | 15 tonnes | 0 | 1.1. to 31.12. |
09.2882 | ex29089000 | 20 | 2,4-Dichloro-3-ethyl-6-nitrophenol, powdered | 90 tonnes | 0 | 1.1. to 31.12. |
09.2889 | 38051090 | — | Sulphate turpentine | 20000 tonnes | 0 | 1.1. to 31.12. |
09.2904 | ex85401119 | 95 | Flat-screen colour cathode-ray tube with a screen width/height ratio 4/3, a diagonal measurement of the screen of 79 cm or more but not exceeding 81 cm and a curvature radius of 50 m or more | 8500 units | 0 | 1.1. to 31.12. |
09.2913 | ex24011041 | 10 | Natural unmanufactured tobacco, whether or not cut in regular size, having a customs value of not less than EUR 450 per 100 kg net weight, for use as binder or wrapper for the manufacture of goods falling within subheading 24021000 | 6000 tonnes | 0 | 1.1. to 31.12. |
ex24011049 | 10 |
ex24011050 | 10 |
ex24011070 | 10 |
ex24011090 | 10 |
ex24012041 | 10 |
ex24012049 | 10 |
ex24012050 | 10 |
ex24012070 | 10 |
ex24012090 | 10 |
09.2914 | ex38249099 | 26 | Aqueous solution containing by weight not less than 40 % of dry betaine extract and 5 % or more but not more than 30 % by weight of organic or inorganic salts | 38000 tonnes | 0 | 1.1. to 31.12. |
09.2917 | ex29309013 | 90 | Cystine | 600 tonnes | 0 | 1.1. to 31.12. |
09.2919 | ex87082990 | 10 | Folding bellows for the manufacture of articulated buses | 2600 units | 0 | 1.1. to 31.12. |
09.2933 | ex29036990 | 30 | 1,3-Dichlorobenzene | 2600 tonnes | 0 | 1.1. to 31.12. |
09.2935 | 38061010 | — | Rosin and resin acids obtained from fresh oleoresins | 200000 tonnes | 0 | 1.1. to 31.12. |
09.2945 | ex29400000 | 20 | D-Xylose | 400 tonnes | 0 | 1.1. to 31.12. |
09.2947 | ex39046990 | 95 | Poly(vinylidene fluoride), in powder form, for the preparation of paint or varnish for coating metal | 1300 tonnes | 0 | 1.1. to 31.12. |
09.2950 | ex29055910 | 10 | 2-Chloroethanol, for the manufacture of liquid thioplasts of subheading 40029990 | 8400 tonnes | 0 | 1.1. to 31.12. |
09.2955 | ex29321900 | 60 | Flurtamone (ISO) | 300 tonnes | 0 | 1.1. to 31.12. |
09.2975 | ex29183000 | 10 | Benzophenone-3,3′:4,4′-tetracarboxylic dianhydride | 600 tonnes | 0 | 1.1. to 31.12. |
09.2976 | ex84079010 | 10 | Four-stroke petrol engines of a cylinder capacity not exceeding 250 cm3 for use in the manufacture of lawnmowers of subheading 843311 or mowers with motor of subheading 84332010 | 750000 units | 0 | 1.7.2005 to 30.6.2006 |
09.2979 | ex70112000 | 15 | Glass face-plate with a diagonal measurement from the outer edge to the outer edge of 81,5 cm (± 0,2 cm) and having a light transmission of 80 % (± 3 %) by a reference thickness of the glass of 11,43 mm | 800000 units | 0 | 1.1. to 31.12. |
09.2981 | ex84073390 | 10 | Spark-ignition reciprocating or rotary internal combustion piston engines, having a cylinder capacity of not less than 300 cc and a power of not less than 6 but not exceeding 15,5 kW, for the manufacture of: self-propelled lawn mowers with a seat of subheading 84331151tractors of subheading 87019011, having the main function of a lawn mower orfour-stroke mowers with a motor capacity of not less than 300 cc of subheading 84332010 | 210000 units | 0 | 1.1. to 31.12. |
ex84079080 | 10 |
ex84079090 | 10 |
09.2986 | ex38249099 | 76 | Mixture of tertiary amines containing: 60 % by weight of dodecyldimethylamine, or more20 % by weight of dimethyl(tetradecyl)amine, or more0,5 % by weight of hexadecyldimethylamine, or more,for use in the production of amine oxides | 14000 tonnes | 0 | 1.1. to 31.12. |
09.2992 | ex39023000 | 93 | Propylene-butylene copolymer, containing by weight not less than 60 % but not more than 68 % of propylene and not less than 32 % but not more than 40 % of butylene, of a melt viscosity not exceeding 3000 mPa at 190 °C, as determined by the ASTM D 3236 method, for use as an adhesive in the manufacture of products falling within subheading 481840 | 1000 tonnes | 0 | 1.1. to 31.12. |
09.2995 | ex85369085 | 95 | Keypads, comprising a layer of silicone and polycarbonate keytops orwholly of silicone or wholly of polycarbonate, including printed keys, for the manufacture or repair of mobile radio-telephones of subheading 85252020 | 20000000 units | 0 | 1.1. to 31.12. |
ex85389099 | 93 |
--------------------------------------------------
