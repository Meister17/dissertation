Commission communication in the framework of the implementation of Directive 2000/9/EC of the European Parliament and of the Council of 20 March 2000 relating to cableway installations designed to carry persons
(2005/C 230/02)
(Text with EEA relevance)
(Publication of titles and references of harmonised standards under the directive)
ESO | Reference and title of the harmonised standard(and reference document) | Reference of superseded standard | Date of cessation of presumption of conformity of superseded standardNote 1 |
CEN | EN 1709:2004Safety requirements for cableway installations designed to carry persons — Precommissioning inspection, maintenance, operational inspection and checks | — | |
CEN | EN 1908:2004Safety requirements of cableway installations designed to carry persons — Tensioning devices | — | |
CEN | EN 1909:2004Safety requirements for cableway installations designed to carry persons — Recovery and evacuation | — | |
CEN | EN 12385-8:2002Steel wire ropes — Safety — Part 8: Stranded hauling and carrying-hauling ropes for cableway installations designed to carry persons | — | |
CEN | EN 12385-9:2002Steel wire ropes — Safety — Part 9: Locked coil carrying ropes for cableway installations designed to carry persons | — | |
CEN | EN 12397:2004Safety requirements for cableway installations designed to carry persons — Operation | — | |
CEN | EN 12927-1:2004Safety requirements for cableway installations designed to carry persons — Ropes — Part 1: Selection criteria for ropes and their end fixings | — | |
CEN | EN 12927-2:2004Safety requirements for cableway installations designed to carry persons — Ropes — Part 2: Safety factors | — | |
CEN | EN 12927-3:2004Safety requirements for cableway installations designed to carry persons — Ropes — Part 3: Long splicing of 6 strand hauling, carrying hauling and towing ropes | — | |
CEN | EN 12927-4:2004Safety requirements for cableway installations designed to carry persons — Ropes — Part 4: End fixings | — | |
CEN | EN 12927-5:2004Safety requirements for cableway installations designed to carry persons — Ropes — Part 5: Storage, transportation, installation and tensioning | — | |
CEN | EN 12927-6:2004Safety requirements for cableway installations designed to carry persons — Ropes — Part 6: Discard criteria | — | |
CEN | EN 12927-7:2004Safety requirements for cableway installations designed to carry persons — Ropes — Part 7: Inspection, repair and maintenance | — | |
CEN | EN 12927-8:2004Safety requirements for cableway installations designed to carry persons — Ropes — Part 8: Magnetic rope testing (MRT) | — | |
CEN | EN 12929-1:2004Safety requirements for cableway installations designed to carry persons — General requirements — Part 1: Requirements for all installations | — | |
CEN | EN 12929-2:2004Safety requirements for cableway installations designed to carry persons — General requirements — Part 2: Additional requirements for reversible bicable aerial ropeways without carrier truck brakes | — | |
CEN | EN 12930:2004Safety requirements for cableway installations designed to carry persons — Calculations | — | |
CEN | EN 13107:2004Safety requirements for cableway installations designed to carry persons — Civil engineering works | — | |
CEN | EN 13223:2004Safety requirements for cableway installations designed to carry persons — Drive systems and other mechanical equipment | — | |
CEN | EN 13243:2004Safety requirements for cableway installations designed to carry persons — Electrical equipment other than for drive systems | — | |
EN 13243:2004/AC:2005 |
CEN | EN 13796-1:2005Safety requirements for cableway installations designed to carry persons — Carriers — Part 1: Grips, carrier trucks, on-board brakes, cabins, chairs, carriages, maintenance carriers, tow-hangers | — | |
CEN | EN 13796-2:2005Safety requirements for cableway installations designed to carry persons — Carriers — Part 2: Slipping resistance test for grips | — | |
CEN | EN 13796-3:2005Safety requirements for cableway installations designed to carry persons — Carriers — Part 3: Fatigue tests | — | |
Note 1 Generally the date of cessation of presumption of conformity will be the date of withdrawal ("dow"), set by the European Standardisation Organisation, but attention of users of these standards is drawn to the fact that in certain exceptional cases this can be otherwise.
Note 3 In case of amendments, the referenced standard is EN CCCCC:YYYY, its previous amendments, if any, and the new, quoted amendment. The superseded standard (column 3) therefore consists of EN CCCCC:YYYY and its previous amendments, if any, but without the new quoted amendment. On the date stated, the superseded standard ceases to give presumption of conformity with the essential requirements of the directive.
NOTE:
- Any information concerning the availability of the standards can be obtained either from the European Standardisation Organisations or from the national standardisation bodies of which the list is annexed to Directive 98/34/EC [2] of the European Parliament and of the Council amended by Directive 98/48/EC [3].
- Publication of the references in the Official Journal of the European Union does not imply that the standards are available in all the Community languages.
- This list replaces all the previous lists published in the Official Journal of the European Union. The Commission ensures the updating of this list.
More information about harmonised standards on the Internet at
http://europa.eu.int/comm/enterprise/newapproach/standardization/harmstds/
[1] ESO: European Standardisation Organisation:
- CEN: rue de Stassart 36, B-1050 Brussels, tel. (32-2) 550 08 11; fax (32-2) 550 08 19 (http://www.cenorm.be)
- CENELEC: rue de Stassart 35, B-1050 Brussels, tel. (32-2) 519 68 71; fax (32-2) 519 69 19 (http://www.cenelec.org)
- ETSI: 650, route des Lucioles, F-06921 Sophia Antipolis, tel. (33) 492 94 42 00; fax (33) 493 65 47 16 (http://www.etsi.org)
[2] OJ L 204, 21.7.1998, p.37.
[3] OJ L 217, 5.8.1998, p. 18.
--------------------------------------------------
