Action brought on 25 September 2006 — Commission of the European Communities v Kingdom of the Netherlands
Parties
Applicant: Commission of the European Communities (represented by: M. Condou-Durande and R. Troosters, Agents)
Defendant: Kingdom of the Netherlands
Form of order sought
- Declare that, by maintaining in force national provisions under which economically non-active and pensioned EU/EEA nationals must prove that they have lasting means of support in order to obtain a residence permit, the Kingdom of the Netherlands has failed to fulfil its obligations under Council Directive 90/364/EEC [1] of 28 June 1990 on the right of residence, Council Directive 90/365/EEC [2] of 28 June 1990 on the right of residence for employees and self-employed persons who have ceased their occupational activity and Council Directive 68/360/EEC [3] of 15 October 1968 on the abolition of restrictions on movement and residence within the Community for workers of Member States and their families;
- order the Kingdom of the Netherlands to pay the costs.
Pleas in law and main arguments
The requirement in the Netherlands rules that in order to obtain a residence permit a person must have sufficient means for a minimum period of one year is not in conformity with Community law.
[1] OJ 1990 L 180, p. 26.
[2] OJ 1990 L 180, p. 28.
[3] OJ, English Special Edition 1968(II), p. 485.
--------------------------------------------------
