Euro exchange rates [1]
29 August 2006
(2006/C 207/01)
| Currency | Exchange rate |
USD | US dollar | 1,2808 |
JPY | Japanese yen | 149,54 |
DKK | Danish krone | 7,4609 |
GBP | Pound sterling | 0,67450 |
SEK | Swedish krona | 9,2493 |
CHF | Swiss franc | 1,5774 |
ISK | Iceland króna | 89,47 |
NOK | Norwegian krone | 8,0425 |
BGN | Bulgarian lev | 1,9558 |
CYP | Cyprus pound | 0,5763 |
CZK | Czech koruna | 28,209 |
EEK | Estonian kroon | 15,6466 |
HUF | Hungarian forint | 275,42 |
LTL | Lithuanian litas | 3,4528 |
LVL | Latvian lats | 0,6960 |
MTL | Maltese lira | 0,4293 |
PLN | Polish zloty | 3,9510 |
RON | Romanian leu | 3,5292 |
SIT | Slovenian tolar | 239,59 |
SKK | Slovak koruna | 37,738 |
TRY | Turkish lira | 1,8887 |
AUD | Australian dollar | 1,6773 |
CAD | Canadian dollar | 1,4206 |
HKD | Hong Kong dollar | 9,9612 |
NZD | New Zealand dollar | 1,9874 |
SGD | Singapore dollar | 2,0146 |
KRW | South Korean won | 1230,98 |
ZAR | South African rand | 9,1389 |
CNY | Chinese yuan renminbi | 10,1980 |
HRK | Croatian kuna | 7,3220 |
IDR | Indonesian rupiah | 11645,67 |
MYR | Malaysian ringgit | 4,7108 |
PHP | Philippine peso | 65,366 |
RUB | Russian rouble | 34,2750 |
THB | Thai baht | 48,068 |
[1] Source: reference exchange rate published by the ECB.
--------------------------------------------------
