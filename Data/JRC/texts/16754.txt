Political and Security Committee Decision EUPOL Kinshasa/1/2004
of 9 December 2004
concerning the appointment of the Head of Mission of the EU Police Mission in Kinshasa (DRC), EUPOL "Kinshasa"
(2004/931/CFSP)
THE POLITICAL AND SECURITY COMMITTEE,
Having regard to the Treaty on European Union and in particular Article 25(3) thereof,
Having regard to Council Joint Action 2004/847/CFSP of 9 December 2004 on the launching of the EU Police Mission in Kinshasa (DRC) [1], and in particular Article 5 and 8 thereof,
Whereas:
(1) Articles 5 and 8 of Joint Action 2004/847/CFSP provides that the Council authorises the Political and Security Committee to take the relevant decisions in accordance with Article 25 of the Treaty on European Union, including the powers to appoint, upon a proposal by the Secretary-General/High Representative, a Head of Mission.
(2) The Secretary-General/High Representative has proposed the appointment of Mr Adílio CUSTÓDIO,
HAS DECIDED AS FOLLOWS:
Article 1
Mr Adílio CUSTÓDIO is hereby appointed Head of Mission of the EU Police Mission in Kinshasa (DRC) regarding the Integrated Police Unit (IPU) (EUPOL Kinshasa) from the day the mission will be launched. Until that date, he shall act as the Head of the Planning Team.
Article 2
This Decision shall take effect on the day of its adoption.
It shall apply until 31 December 2005.
Done at Brussels, 9 December 2004.
For the Political and Security Committee
The President
A. Hamer
--------------------------------------------------
[1] OJ L 367, 14.12.2004, p. 30.
