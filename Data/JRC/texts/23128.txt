COMMISSION REGULATION (EC) No 420/98 of 20 February 1998 amending Regulation (EEC) No 1756/93 fixing the operative events for the agricultural conversion rate applicable to milk and milk products
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 3813/92 of 28 December 1992 on the unit of account and the conversion rates to be applied for the purposes of the common agricultural policy (1), as last amended by Regulation (EC) No 150/95 (2), and in particular Article 6(2) thereof,
Whereas Part B of the Annex to Commission Regulation (EEC) No 1756/93 (3), as last amended by Regulation (EC) No 569/96 (4), fixes the operative events applicable pursuant to Commission Regulation (EEC) No 570/88 of 16 February 1988 on the sale of butter at reduced prices and the granting of aid for butter and concentrated butter for use in the manufacture of pastry products, ice-cream and other foodstuffs (5), as last amended by Regulation (EC) No 531/96 (6); whereas that Regulation was repealed and replaced by Commission Regulation (EC) No 2571/97 (7) which introduces, inter alia, the possibility of using cream to which no tracers have been added under the aid arrangements; whereas Regulation (EEC) No 1756/93 should be amended to take account of these amendments;
Whereas Part D of the Annex to Regulation (EEC) No 1756/93 fixes the operative events for storage costs to be paid by the seller where butter or skimmed-milk powder offered for public intervention do not fulfil the requirements laid down in Article 2 of Commission Regulation (EC) No 454/95 of 28 February 1995 laying down detailed rules for intervention on the market in butter and cream (8), as last amended by Regulation (EC) No 419/98 (9), and Article 1 of Commission Regulation (EC) No 322/96 of 22 February 1996 laying down detailed rules of application for the public storage of skimmed-milk powder (10), as last amended by Regulation (EC) No 419/98; whereas the said storage costs are fixed with reference to Article 7(2) of Commission Regulation (EEC) No 3597/90 of 12 December 1990 on the accounting rules for intervention measures involving the buying-in, storage and sale of agricultural products by intervention agencies (11), as last amended by Regulation (EC) No 1392/97 (12); whereas, in order to ensure consistency, the abovementioned operative events should be replaced by a reference to the operative event provided for in Article 7(2) of Regulation (EEC) No 3597/90;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Milk and Milk Products,
HAS ADOPTED THIS REGULATION:
Article 1
The Annex to Regulation (EEC) No 1756/93 is hereby amended as follows:
1. in Part B.II, point 6 is replaced by the following:
>TABLE>
2. in Part B.III, point 4 is replaced by the following:
>TABLE>
3. in point 2.A, Part D, the words 'Agricultural conversion rate applicable on the date of taking over within the meaning of Article 5(1)` are replaced by 'Agricultural conversion rate referred to in Article 7(2)(a) and (b) of Regulation (EEC) No 3597/90`;
4. in point 3.A, Part D, the words 'Agricultural conversion rate applicable on the date of taking over of the goods concerned` are replaced by 'Agricultural conversion rate referred to in Article 7(2)(a) and (b) of Regulation (EEC) No 3597/90`.
Article 2
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Communities.
However, Article 1(1) and (2) shall apply to invitations to tender with a deadline for the submission of tenders after 1 January 1998.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 20 February 1998.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ L 387, 31. 12. 1992, p. 1.
(2) OJ L 22, 31. 1. 1995, p. 1.
(3) OJ L 161, 2. 7. 1993, p. 48.
(4) OJ L 80, 30. 3. 1996, p. 48.
(5) OJ L 55, 1. 3. 1988, p. 31.
(6) OJ L 78, 28. 3. 1996, p. 13.
(7) OJ L 350, 20. 12. 1997, p. 3.
(8) OJ L 46, 1. 3. 1995, p. 1.
(9) See page 20 of this Official Journal.
(10) OJ L 45, 23. 2. 1996, p. 5.
(11) OJ L 350, 14. 12. 1990, p. 43.
(12) OJ L 190, 19. 7. 1997, p. 22.
