Commission Regulation (EC) No 2592/2001
of 28 December 2001
imposing further information and testing requirements on the manufacturers or importers of certain priority substances in accordance with Council Regulation (EEC) No 793/93 on the evaluation and control of the risk of existing substances
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 793/93 of 23 March 1993 on the evaluation and control of risks of existing substances(1), and in particular Article 10(2) thereof,
Whereas:
(1) The Member States designated as rapporteur pursuant to Regulation (EEC) No 793/93 for certain priority substances undergoing risk evaluation activities have evaluated the information submitted by the manufacturers or importers in respect of those substances. After consultation of the manufacturers or importers concerned, they have determined whether, for the purposes of the risk evaluation, it is necessary to require those manufacturers or importers to submit further information and/or to carry out further testing.
(2) The manufacturers and importers have checked whether the information needed to evaluate the substances in question is not available from former manufacturers or importers of those substances, in accordance with Article 10(5) of Regulation (EEC) No 793/93. The manufacturers and importers have also checked, in consultation with the Member States designated as rapporteur, whether tests on animals cannot be replaced or limited by using other methods.
(3) The Commission has been informed by the Member States designated as rapporteur of the need to request further information and testing from the manufacturers or importers of those substances.
(4) The Member States designated as rapporteur have submitted to the Commission the protocols on the requested further testing.
(5) Article 12(3) of Regulation (EEC) No 793/93 provides that in the case of a substance produced or imported as such or in a preparation by several manufacturers or importers, the further testing may be performed by one manufacturer/importer acting on behalf of the other manufacturers or importers concerned. In that case, the other manufacturers or importers are to make reference to the tests carried out and make a fair and equitable contribution to the cost.
(6) The provisions of this Regulation are in accordance with the opinion of the Committee established by Article 15 of Regulation (EEC) No 793/93,
HAS ADOPTED THIS REGULATION:
Article 1
The manufacturer(s) and importer(s) of the substances listed in the Annex to this Regulation, who have submitted information in accordance with the requirements of Articles 3, 4, 7 and 9 of Regulation (EEC) No 793/93, shall provide the information and perform the tests indicated in the Annex to this Regulation and shall deliver the relevant results to the Member States designated as rapporteur.
The tests shall be performed according to the protocols specified by the Member States designated as rapporteur.
The results shall be delivered within the time limits laid down in the Annex.
Article 2
This Regulation shall enter into force on the 20th day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 28 December 2001.
For the Commission
Margot Wallström
Member of the Commission
(1) OJ L 84, 5.4.1993, p. 1.
ANNEX
>TABLE>
