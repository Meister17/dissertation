Commission Decision
of 4 September 2003
on the procedure for attesting the conformity of construction products pursuant to Article 20(2) of Council Directive 89/106/EEC as regards kits for exterior wall claddings
(notified under document number C(2003) 3160)
(Text with EEA relevance)
(2003/640/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 89/106/EEC of 21 December 1988 on the approximation of laws, regulations and administrative provisions of the Member States relating to construction products(1), as amended by Directive 93/68/EEC(2), and in particular Article 13(4) thereof,
Whereas:
(1) The Commission is required to select, of the two procedures under Article 13(3) of Directive 89/106/EEC for attesting the conformity of a product, the least onerous possible procedure consistent with safety. This means that it is necessary to decide whether, for a given product or family of products, the existence of a factory production control system under the responsibility of the manufacturer is a necessary and sufficient condition for an attestation of conformity, or whether, for reasons related to compliance with the criteria mentioned in Article 13(4), the intervention of an approved certification body is required.
(2) Article 13(4) requires that the procedure thus determined shall be indicated in the mandates and in the technical specifications. It is therefore desirable to identify the products or family of products referred to in the technical specifications.
(3) The two procedures provided for in Article 13(3) are described in detail in Annex III to Directive 89/106/EEC. It is necessary therefore to specify clearly the methods by which the two procedures shall be implemented, by reference to Annex III, for each product or family of products, since Annex III gives preference to certain systems.
(4) The procedure referred to in Article 13(3)(a) corresponds to the systems set out in the first possibility, without continuous surveillance, and the second and third possibilities of section 2(ii) of Annex III. The procedure referred to in Article 13(3)(b) corresponds to the systems set out in section 2(i) of Annex III, and in the first possibility, with continuous surveillance, of section 2(ii) of Annex III.
(5) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on Construction,
HAS ADOPTED THIS DECISION:
Article 1
The products and families of products set out in Annex I shall have their conformity attested by a procedure whereby, in addition to a factory production control system operated by the manufacturer, an approved certification body is involved in assessment and surveillance of the production control or of the product itself.
Article 2
The procedure for attesting conformity as set out in Annex II shall be indicated in the mandates for guidelines for European technical approvals.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 4 September 2003.
For the Commission
Erkki Liikanen
Member of the Commission
(1) OJ L 40, 11.2.1989, p. 12.
(2) OJ L 220, 30.8.1993, p. 1.
ANNEX I
Kits for exterior wall claddings:
For external walls, or external finishes of walls.
ANNEX II
Note:
for products having more than one of the intended uses specified in the following families, the tasks for the approved body, derived from the relevant systems of attestation of conformity, are cumulative.
Product family: kits for exterior wall claddings (1/2)
Systems of attestation of conformity
For the product(s) and intended use(s) listed below, EOTA is requested to specify the following system(s) of attestation of conformity in the relevant guidelines for European technical approvals:
>TABLE>
System 2+: see Annex III(2)(ii) to Directive 89/106/EEC, first possibility, including certification of factory production control by an approved body on the basis of initial inspection of factory and of factory production control as well as of continuous surveillance, assessment and approval of the factory production control.
The specification for the system should be such that it can be implemented even where performance does not need to be determined for a certain characteristic, because at least one Member State has no legal requirement at all for such characteristic (see Article 2(1) of Directive 89/106/EEC and, where applicable, clause 1.2.3 of the Interpretative Documents). In those cases the verification of such a characteristic must not be imposed on the manufacturer if he does not wish to declare the performance of the product in that respect.
Product family: kits for exterior wall claddings (2/2)
Systems of attestation of conformity
For the product(s) and intended use(s) listed below, EOTA is requested to specify the following system(s) of attestation of conformity in the relevant guidelines for European technical approvals:
>TABLE>
System 1: see Annex III(2)(i) to Directive 89/106/EEC, without audit-testing of samples.
System 3: see Annex III(2)(ii) to Directive 89/106/EEC, second possibility.
System 4: see Annex III(2)(ii) to Directive 89/106/EEC, third possibility.
The specification for the system should be such that it can be implemented even where performance does not need to be determined for a certain characteristic, because at least one Member State has no legal requirement at all for such characteristic (see Article 2(1) of Directive 89/106/EEC and, where applicable, clause 1.2.3 of the interpretative documents). In those cases the verification of such a characteristic must not be imposed on the manufacturer if he does not wish to declare the performance of the product in that respect.
