Corrigendum to the Rules of the Administrative Commission on social security for migrant workers attached to the Commission of the European Communities
(Official Journal of the European Union C 119 of 20 May 2005)
(2005/C 129/08)
The publication is completed by the following text:
"Working methods for an enlarged Administrative Commission
Code of Conduct
The enlargement of the European Union on 1 May 2004 increases considerably the number of participants in the Administrative Commission meetings.
In view of the new situation, this Code of Conduct aims to improve the efficiency of the preparation and conduct of meetings of the Administrative Commission so that the necessarily limited time available can be better used.
I. Preparations of meetings
(a) Better documents
Time would be gained in meetings by the use of more efficient documents, e.g. CASSTM notes could be limited to 1-2 pages.
The timely availability of documents is essential for the good preparation of a meeting.
Delegations should keep in mind that at least 10 days are needed for the translation of any document of less than four pages. For any additional five pages an additional week has to be added to the time needed for the translation.
Delegations should strictly observe the deadline for submitting notes to the Secretariat as indicated in Article 6(2) of the rules.
(b) Organisation of the Agenda
Agendas should be organised with a view to enabling delegations to plan their composition, attendance and travel in a time-efficient manner.
The Agenda should to the greatest possible extent be structured so that items concerning the same social security branch are grouped together and dealt with at the meeting as consecutive items.
Changes to the adopted order of the Agenda shall be avoided if possible.
Whenever possible it should be indicated for each Agenda item whether the matter is for discussion or for the adoption of a decision.
(c) Better use of time between meetings
The time between meetings should be used constructively.
Following a meeting of the Administrative Commission, or any other group under its authority, the Secretariat shall immediately send out a memorandum which specifies the follow-up actions to be taken, as well as the time limits within which delegations are asked to act.
The Chairman, with the assistance of the Secretariat, shall take the necessary steps to advance work between meetings, e.g. contacts can be made to sort out problems on specific issues so that a proposal for a possible solution can be presented at a forthcoming meeting.
II. Conduct of meetings
(a) Role of the Presidency
The role of the Presidency and its Chairman is to direct the work of the Administrative Commission not only by taking an active part in the organisation of the Agendas but also to be the initiator of discussions and decision making in the Administrative Commission.
Each Presidency shall present its programme for the work of the Administrative Commission and how it plans to carry it out.
The Chairman shall aim to organise each meeting in a manner that will ensure the most efficient use of time.
At the start of a meeting, the Chairman shall give a short introduction and, in particular shall provide any necessary information regarding the handing of the meeting including an indication of the length of time he expects to be devoted to each item.
At the start of a discussion on a point, the Chairman shall, if he feels it to be necessary, depending on the type of discussion which is needed, indicate to delegations the maximum length of their interventions on that point. In most cases interventions should not exceed two minutes.
Full table rounds shall be an exception. They may only be used on specific questions, with a time limit on interventions set by the Chairman.
The Chairman shall give as much focus as possible to discussions, in particular by requesting delegations to respond to compromise texts of specific proposals.
At the end of each item on the Agenda the Chairman shall briefly sum up the outcome and the results achieved.
(b) Behaviour of delegations
Delegations should equally contribute to the efficient conduct of a meeting.
Delegations should, in particular keep in mind the following:
- to keep interventions brief and avoid repeating points made by previous speakers;
- like minded delegations could hold consultations with a view to the presentation by a single spokesperson of a common view on a specific matter;
- when discussing texts, delegations could propose concrete drafting proposals, submitted, if possible, in writing, rather than limit themselves to expressing their disagreement with a particular proposal;
- unless indicated otherwise by the Chairman, delegations could refrain from intervening when in agreement with a particular proposal; in such case silence would be taken as agreement.
(c) Harnessing technology
The Chairman as well as the Secretariat could, when deemed appropriate, make use of technical devices to enhance the efficiency of the conduct of a meeting."
--------------------------------------------------
