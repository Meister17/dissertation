Commission Regulation (EC) No 2208/2003
of 17 December 2003
amending Regulation (EEC) No 2921/90 on aid for the production of casein and caseinates from skimmed milk
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1255/1999 of 17 May 1999 on the common organisation of the market in milk and milk products(1), and in particular Article 15 thereof,
Whereas:
(1) Article 2(1) of Commission Regulation (EEC) No 2921/90(2) sets the aid for skimmed milk processed into casein or caseinates. Given the change in the market price for casein and caseinates on the Community and world markets, the aid should be decreased.
(2) The Management Committee for Milk and Milk Products has not delivered an opinion within the time limit set by its chairman,
HAS ADOPTED THIS REGULATION:
Article 1
In Article 2(1) of Regulation (EEC) No 2921/90, "EUR 6,70" is hereby replaced by "EUR 6,30".
Article 2
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 17 December 2003.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 160, 26.6.1999, p. 48; Regulation as last amended by Regulation (EC) No 1787/2003 (OJ L 270, 21.10.2003, p. 121).
(2) OJ L 279, 11.10.1990, p. 22; Regulation as last amended by Regulation (EC) No 785/2003 (OJ L 115, 9.5.2003, p. 15).
