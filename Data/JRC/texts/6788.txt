Council Common Position 2005/792/CFSP
of 14 November 2005
concerning restrictive measures against Uzbekistan
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty on European Union, and in particular Article 15 thereof,
Whereas:
(1) On 23 May 2005 the Council strongly condemned the excessive, disproportionate and indiscriminate use of force by the Uzbek security forces during events in Andijan in May and expressed its deep regret regarding the failure of the Uzbek authorities to respond adequately to the UN’s call for an independent international inquiry into these events.
(2) On 13 June 2005 the Council condemned the Uzbek authorities’ refusal to allow an independent international inquiry into the recent events in Andijan, reiterated its conviction that a credible independent international inquiry should be held and urged the Uzbek authorities to reconsider their position by the end of June 2005.
(3) On 18 July the Council recalled its conclusions of May 23 and June 13 and expressed its regret that the Uzbek authorities had not reconsidered their position by the given deadline of the end of June. On that occasion the Council indicated that it would be examining measures against Uzbekistan, such as the introduction of an embargo on exports to Uzbekistan of arms, military equipment and equipment which might be used for internal repression as well as other targeted measures.
(4) On 3 October 2005, the Council again expressed its profound concern at the situation in Uzbekistan and strongly condemned the Uzbek authorities’ refusal to allow an independent international inquiry into the recent events in Andijan in May. It stated that it continued to place primary importance on a credible and transparent independent international inquiry.
(5) In the light of the excessive, disproportionate and indiscriminate use of force by the Uzbek security forces during the Andijan events, the Council has decided to impose an embargo on exports to Uzbekistan of arms, military equipment and other equipment that might be used for internal repression.
(6) The Council has also decided to implement restrictions on admission to the European Union aimed at those individuals who are directly responsible for the indiscriminate and disproportionate use of force in Andijan and for the obstruction of an independent inquiry.
(7) The Council has decided to implement these measures for an initial period of one year. In the meantime, the Council will review the measures in the light of any significant changes to the current situation, in particular with regard to:
(i) the conduct and outcome of the ongoing trials of those accused of precipitating and participating in the disturbances in Andijan;
(ii) the situation regarding the detention and harassment of those who have questioned the Uzbek authorities’ version of events in Andijan;
(iii) Uzbek cooperation with any independent, international rapporteur appointed to investigate the disturbances in Andijan;
(iv) the outcome of any independent, international inquiry,
and any action that demonstrates the willingness of the Uzbek authorities to adhere to the principles of respect for human rights, rule of law and fundamental freedoms.
(8) Action by the Community is needed in order to implement certain measures,
HAS ADOPTED THIS COMMON POSITION:
Article 1
1. The sale, supply, transfer or export of arms and related materiel of all types, including weapons and ammunition, military vehicles and equipment, paramilitary equipment and spare parts for the aforementioned, to Uzbekistan by nationals of Member States, or from the territories of Member States, or using their flag vessels or aircraft shall be prohibited whether originating or not in their territories.
2. The sale, supply, transfer or export of equipment, listed in Annex I, which might be used for internal repression to Uzbekistan shall be prohibited.
3. It shall be prohibited:
(i) to provide technical assistance, brokering services and other services related to military activities and to the provision, manufacture, maintenance and use of arms and related materiel of all types, including weapons and ammunition, military vehicles and equipment, paramilitary equipment, and spare parts for the aforementioned, or related to equipment which might be used for internal repression, directly or indirectly to any natural or legal person, entity or body in, or for use in Uzbekistan.
(ii) to provide financing or financial assistance related to military activities, including in particular grants, loans and export credit insurance, for any sale, supply, transfer or export of arms and related materiel, or for the provision of related technical assistance, brokering services and other services, or related to equipment which might be used for internal repression, directly or indirectly to any natural or legal person, entity or body in, or for use in Uzbekistan.
Article 2
1. Article 1 shall not apply to:
(i) the sale, supply, transfer or export of non-lethal military equipment intended solely for humanitarian or protective use, or for institution-building programmes of the UN, the EU and the Community, or for EU and UN crisis management operations;
(ii) the supply, transfer, or export of arms and equipment referred to in Article 1 for the forces in Uzbekistan of contributors to the International Security Assistance Force (ISAF) and "Operation Enduring Freedom" (OEF);
(iii) the sale, supply, transfer or export of equipment which might be used for internal repression, intended solely for humanitarian or protective use;
(iv) the provision of financing, financial assistance or technical assistance related to equipment referred to at (i), (ii) and (iii),
on condition that such exports and assistance have been approved in advance by the relevant competent authority.
2. Article 1 shall not apply to protective clothing, including flak jackets and military helmets, temporarily exported to Uzbekistan by United Nations personnel, personnel of the EU, the Community or its Member States, representatives of the media and humanitarian and development workers and associated personnel for their personal use only.
Article 3
1. Member States shall take the necessary measures to prevent the entry into, or transit through, their territories of those individuals, listed in Annex II, directly responsible for the indiscriminate and disproportionate use of force in Andijan and the obstruction of an independent inquiry.
2. Paragraph 1 does not oblige a Member State to refuse its own nationals entry into its territory.
3. Paragraph 1 shall be without prejudice to cases where a Member State is bound by an obligation of international law, namely:
(i) as host country of an international intergovernmental organisation;
(ii) as host country to an international conference convened by, or under the auspices of, the United Nations; or
(iii) under a multilateral agreement conferring privileges and immunities; or
(iv) under the 1929 Treaty of Conciliation (Lateran pact) concluded by the Holy See (State of the Vatican City) and Italy.
4. Paragraph 3 shall apply also in cases where a Member State is host country of the Organisation for Security and Cooperation in Europe (OSCE.).
5. The Council shall be duly informed in all cases where a Member State grants an exemption pursuant to paragraphs 3 or 4.
6. Member States may grant exemptions from the measures imposed in paragraph 1 where travel is justified on the grounds of urgent humanitarian need, or on grounds of attending intergovernmental meetings, including those promoted by the European Union, where a political dialogue is conducted that directly promotes democracy, human rights and the rule of law in Uzbekistan.
7. A Member State wishing to grant exemptions referred to in paragraph 6 shall notify the Council in writing. The exemption will be deemed to be granted unless one or more of the Council Members raises an objection in writing within two working days of receiving notification of the proposed exemption. In the event that one or more of the Council members raises an objection, the Council, acting by a qualified majority, may decide to grant the proposed exemption.
8. In cases where pursuant to paragraphs 3, 4, 6 and 7, a Member State authorises the entry into, or transit through, its territory of persons listed in Annex II, the authorisation shall be limited to the purpose for which it is given and to the persons concerned thereby.
Article 4
No technical meetings scheduled in accordance with the Partnership and Cooperation Agreement establishing a partnership between the European Communities and their Member States, of the one part, and the Republic of Uzbekistan, of the other part [1], shall take place.
Article 5
This Common Position shall apply for a period of 12 months. It shall be kept under constant review. It shall be renewed, or amended as appropriate, if the Council deems that its objectives have not been met.
Article 6
This Common Position shall take effect on the date of its adoption.
Article 7
This Common Position shall be published in the Official Journal of the European Union.
Done at Brussels, 14 November 2005.
For the Council
The President
T. Jowell
[1] OJ L 229, 31.8.1999, p. 3.
--------------------------------------------------
ANNEX I
List of equipment which might be used for internal repression
Equipment for internal repression referred to in Article 1(2)
The list below does not comprise articles that have been specially designed or modified for military use.
1. Helmets providing ballistic protection, anti-riot helmets, anti-riot shields and ballistic shields and specially designed components therefor.
2. Specially designed fingerprint equipment.
3. Power controlled searchlights.
4. Construction equipment provided with ballistic protection.
5. Hunting knives.
6. Specially designed production equipment to make shotguns.
7. Ammunition hand-loading equipment.
8. Communications intercept devices.
9. Solid-state optical detectors.
10. Image-intensifier tubes.
11. Telescopic weapon sights.
12. Smooth-bore weapons and related ammunition, other than those specially designed for military use, and specially designed components therefor; except:
- signal pistols;
- air- and cartridge-powered guns designed as industrial tools or humane animal stunners.
13. Simulators for training in the use of firearms and specially designed or modified components and accessories therefor.
14. Bombs and grenades, other than those specially designed for military use, and specially designed components therefor.
15. Body armour, other than those manufactured to military standards or specifications, and specially designed components therefor.
16. All-wheel-drive utility vehicles capable of off-road use that have been manufactured or fitted with ballistic protection, and profiled armour for such vehicles.
17. Water cannon and specially designed or modified components therefor.
18. Vehicles equipped with a water cannon.
19. Vehicles specially designed or modified to be electrified to repel borders and components therefor specially designed or modified for that purpose.
20. Acoustic devices represented by the manufacturer or supplier as suitable for riot-control purposes, and specially designed components therefor.
21. Leg-irons, gang-chains, shackles and electric-shock belts, specially designed for restraining human beings; except:
- handcuffs for which the maximum overall dimension including chain does not exceed 240 mm when locked.
22. Portable devices designed or modified for the purpose of riot control or self-protection by the administration of an incapacitating substance (such as tear gas or pepper sprays), and specially designed components therefor.
23. Portable devices designed or modified for the purpose of riot control or self-protection by the administration of an electric shock (including electric-shocks batons, electric shock shields, stun guns and electric shock dart guns (tasers)) and components therefor specially designed or modified for that purpose.
24. Electronic equipment capable of detecting concealed explosives and specially designed components therefor; except:
- TV or X-ray inspection equipment.
25. Electronic jamming equipment specially designed to prevent the detonation by radio remote control of improvised devices and specially designed components therefor.
26. Equipment and devices specially designed to initiate explosions by electrical or non-electrical means, including firing sets, detonators, igniters, boosters and detonating cord, and specially designed components therefor; except:
- those specially designed for a specific commercial use consisting of the actuation or operation by explosive means of other equipment or devices the function of which is not the creation of explosions (e.g., car air-bag inflaters, electric-surge arresters of fire sprinkler actuators).
27. Equipment and devices designed for explosive ordnance disposal; except:
- bomb blankets;
- containers designed for folding objects known to be, or suspected of being improvised explosive devices.
28. Night vision and thermal imaging equipment and image intensifier tubes or solid state sensors therefor.
29. Linear cutting explosive charges.
30. Explosives and related substances as follows:
- amatol,
- nitrocellulose (containing more than 12,5 % nitrogen),
- nitroglycol,
- pentaerythritol tetranitrate (PETN),
- picryl chloride,
- tinitorphenylmethylnitramine (tetryl),
- 2,4,6-trinitrotoluene (TNT)
31. Software specially designed and technology required for all listed items.
--------------------------------------------------
ANNEX II
List of persons referred to in Article 3 of this Common Position
Surname, First Name: Almatov, Zakirjan
Sex: Male
Title, Function: Minister of Interior
Address (No, street, postal code, town, country): Tashkent, Uzbekistan
Date of birth: 10 October 1949
Place of birth (town, country): Tashkent, Uzbekistan
Passport or ID Number (including country that issued and date and place of issue): Passport No DA 0002600 (Diplomatic ppt)
Nationality: Uzbek
Other information (e.g. name of father and mother, fiscal number, telephone or fax number): None
Surname, First Name: Mullajonov, Tokhir Okhunovich
Alias: Alternative spelling for surname: Mullajanov
Sex: Male
Title, Function: First Deputy Interior Minister
Address (No, street, postal code, town, country): Tashkent, Uzbekistan
Date of birth: 10 October 1950
Place of birth (town, country): Ferghana, Uzbekistan
Passport or ID Number (including country that issued and date and place of issue): Passport No DA 0003586 (Diplomatic ppt) expires 05/11/2009
Nationality: Uzbek
Other information (e.g. name of father and mother, fiscal number, telephone or fax number): None
Surname, First Name: Gulamov, Kadir Gafurovich
Sex: Male
Title, Function: Defence Minister
Address (No, street, postal code, town, country): Tashkent, Uzbekistan
Date of birth: 17 February 1945
Place of birth (town, country): Tashkent, Uzbekistan
Passport or ID Number (including country that issued and date and place of issue): Passport No DA 0002284 (Diplomatic ppt) expires 24/10/2005
Nationality: Uzbek
Other information (e.g. name of father and mother, fiscal number, telephone or fax number): None
Surname, First Name: Ruslan Mirzaev
Sex: Male
Title, Function: National Security Council State Adviser
Surname, First Name: Saidullo Begaliyevich Begaliyev
Sex: Male
Title, Function: Andizhan Regional Governor
Surname, First Name: Kossimali Akhmedov
Sex: Male
Title, Function: Major General
Surname, First Name: Ergashev, Ismail Ergashevitch
Sex: Male
Title, Function: Major General (Retired)
Address (No, street, postal code, town, country): Not known
Date of birth: 5 August 1945
Place of birth (town, country): Vali Aitachaga, Uzbekistan
Passport or ID Number (including country that issued and date and place of issue): No details
Nationality: Uzbek
Other information (e.g. name of father and mother, fiscal number, telephone or fax number): None
Surname, First Name: Pavel Islamovich Ergashev
Sex: Male
Title, Function: Colonel
Surname, First Name: Vladimir Adolfovich Mamo
Sex: Male
Title, Function: Major General
Surname, First Name: Gregori Pak
Sex: Male
Title, Function: Colonel
Surname, First Name: Valeri Tadzhiev
Sex: Male
Title, Function: Colonel
Surname, First Name: Inoyatov, Rustam Raulovich
Sex: Male
Title, Function: Chief of SNB (National Security Service),
Address (No, street, postal code, town, country): Tashkent, Uzbekistan
Date of birth: 22 June 1944
Place of birth (town, country): Sherabad, Uzbekistan
Passport or ID Number (including country that issued and date and place of issue): Passport No DA 0003171 (Diplomatic ppt); also diplomatic passport No 0001892 (expired 15/09/2004)
Nationality: Uzbek
Other information (e.g. name of father and mother, fiscal number, telephone or fax number): None
--------------------------------------------------
