Resolution of the Council and of the representatives of the Governments of the Member States, meeting within the Council
of 26 February 2001
on strengthening the capabilities of the European Union in the field of civil protection
(2001/C 82/01)
THE COUNCIL AND THE REPRESENTATIVES OF THE GOVERNMENTS OF THE MEMBER STATES, MEETING WITHIN THE COUNCIL,
1. CONVINCED that the unending series of man-made, technological and natural disasters affecting the European Union and non-member countries calls for strengthening and making more efficient the capabilities of the European Union and the Member States in the field of civil protection.
2. BELIEVING that, without prejudice to Member States' responsibilities, the Community should promote an adequate mix of preparedness and prevention, an effective collection and circulation of information and experience, a coordination of the means which exist at Member State and Community level and a cooperation between Member States, especially when disasters are on a scale beyond the intervention capacity of individual Member States.
3. UNDERLINING the fact that the Community action programmes in the field of civil protection established by Council Decision 98/22/EC(1) and by Council Decision 1999/847/EC(2) contribute to a better use and integration of Member States resources and complement measures adopted in response to previous resolutions of the Council and the Member States(3).
4. CONSIDERING that it is appropriate to complement the previous resolutions by this resolution, taking into account recent developments, including those in the field of non-military crisis management in the framework of the common foreign and security policy (CFSP).
5. STRESS that the flexibility that has been built into the Community action programmes should facilitate the adjustment of actions envisaged at Community and Member State level to evolving priorities in the field of civil protection, inter alia, through an improved integration of civil protection objectives in other policies and actions, such as the protection of the environment, and should enhance the role of civil protection assets for civilian aspects of crisis management.
6. INFORMATION/EDUCATION/COMMUNICATION
HIGHLIGHT in this respect:
(a) the vade-mecum of civil protection in the European Union, which, inter alia, includes a description of national organisation of civil protection, relevant legal texts and methodology of emergency planning, while NOTING that it needs to be updated and complemented in several respects;
(b) the added value of the Community framework for contacts between national correspondents;
(c) that increased use of advanced information and telecommunications systems is necessary to make available up-to-date information in relation to disasters;
(d) the importance of developing initiatives such as civil-protection information campaigns and information, education and awareness-raising initiatives aimed at the public and in particular at young people to increase the level of self-protection of citizens;
(e) the benefit of exchanging persons responsible for civil protection as part of training programmes, in particular in the course of simulation exercises, and facilitating the pooling of experience of those who work for civil protection;
(f) that cooperation between schools and national training centres that are active in the field of civil protection should progress at a faster pace;
(g) the value of assessing the experience in the voluntary sector of non-governmental and other private organisations involved in civil protection in order to make better use of their resources and to enhance their contribution to civil protection-related activities.
7. OPERATIONAL ACTIVITIES AND INSTRUMENTS
EMPHASISE:
(a) the progress made regarding assistance arrangements between the Member States and civil protection organisations;
(b) the value of the Community's operational manual listing national and Community points of contact, points of access to expert opinion on certain areas of intervention, as well as registers of the resources that are available in each Member State and the procedures and arrangements for making such resources available, which contribute to better coordination between Member States;
(c) the value of the 24-hour operational service on a stand-by basis provided by the Commission and of arrangements with the Commission relating to the secondment of experts;
(d) the usefulness of the common emergency phone number 112 established by Council Decision 91/396/EEC of 29 July 1991 on the introduction of a single European emergency call number(4);
(e) the role of the permanent network of national correspondents in ensuring the consistency of Community cooperation and in collecting information on the assistance available in each Member State in the event of a disaster;
(f) the important role that general directors of civil protection or their counterparts can play in setting key priorities and common objectives and in ensuring a high degree of coordination, in particular at their regular meetings.
8. INTERNATIONAL COOPERATION
(a) STRESSING, that coordination of measures taken at international level, for example in the framework of the UN, OSCE and NATO, should be enhanced in order to rationalise the use of the resources involved in such measures; noting the intention of the Member States and the Commission to undertake steps with a view to avoiding duplication and to enhance the most efficient possible use of expertise;
(b) NOTE that EFTA countries are participating in various activities under the Community action programme;
(c) WELCOME the fact that the UN/ECE Convention on transboundary effects of industrial accidents entered into force on 19 April 2000;
(d) INSIST on a prompt implementation of the Resolution of the Council and of the representatives of the Governments of the Member States, meeting within the Council, of 9 December 1999 on cooperation with candidate central and east European Countries and Cyprus on civil protection(5), as regards in particular the exchange of information between them, including as regards operational manuals. CONSIDER that the same treatment should be given to the other countries that are candidates for accession to the European Union;
(e) INVITE the Commission and Member States to continue the pilot project on civil protection undertaken in the framework of the Euro-Mediterranean Strategy and to explore ways and means towards a strengthened cooperation under that strategy;
(f) ALSO INVITE the Commission and Member States to explore ways and means to establish a similar action through a Eurobaltic programme for civil protection cooperation in the Baltic and Barents region.
9. CIVIL PROTECTION RESOURCES IN THE FRAMEWORK OF CRISIS MANAGEMENT
(a) NOTING that, when implementing the action plan for non-military crisis management outlined by the Helsinki European Council of December 1999, civil protection assets may have to be used and that, as a consequence, it should be ensured that inter-pillar coherence is aimed at;
(b) WELCOME the priority given to civil protection in the study on concrete targets on civilian aspects of crisis management which was submitted to the Feira European Council on 19 and 20 June 2000;
(c) NOTE the setting up by a Council Decision, adopted on 22 May 2000, of a Committee for Civilian Aspects of Crisis Management(6);
(d) RECOMMEND that civil protection resources or assets of Member States may be used in the framework of crisis management in support of the CFSP, following their examination by the competent bodies.
(1) OJ L 8, 14.1.1998, p. 20.
(2) OJ L 327, 21.12.1999, p. 53.
(3) Resolutions of:
- 25 June 1987 on the introduction of Community cooperation on civil protection (OJ C 176, 4.7.1987, p. 1),
- 13 February 1989 on the new developments in Community cooperation on civil protection (OJ C 44, 23.2.1989, p. 3),
- 23 November 1990 on Community cooperation on civil protection (OJ C 315, 14.12.1990, p. 1),
- 23 November 1990 on improving mutual aid between Member States in the event of a natural or man-made disaster (OJ C 315, 14.12.1990, p. 3),
- 8 July 1991 on improving mutual aid in the event of a natural or technological disaster (OJ C 198, 27.7.1991, p. 1),
- 31 October 1994 on strengthening Community cooperation on civil protection (OJ C 313, 10.11.1994, p. 1).
(4) OJ L 217, 6.8.1991, p. 31.
(5) OJ C 373, 23.12.1999, p. 2.
(6) OJ L 127, 27.5.2000, p. 1.
