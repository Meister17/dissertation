COUNCIL DIRECTIVE of 17 September 1984 on the approximation of the laws of the Member States relating to welded unalloyed steel gas cylinders (84/527/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 100 thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas in the Member States the construction and the inspection of gas cylinders are subject to mandatory provisions varying from one Member State to another and consequently hinder trade in such cylinders ; whereas it is therefore necessary to approximate these provisions;
Whereas Council Directive 76/767/EEC of 27 July 1976 on the approximation of the laws of the Member States relating to common provisions for pressure vessels and methods of inspecting them (4), lays down in particular the procedures of EEC pattern approval and verification for these vessels ; whereas according to this Directive it is advisable to lay down the technical requirements to be complied with by EEC-type welded unalloyed steel gas cylinders with a capacity of 0,5 to 150 litres in order to be put into circulation, commercialized and used without restraint after having undergone inspection and bearing accordingly a mark and a symbol,
HAS ADOPTED THIS DIRECTIVE:
Article 1
This Directive shall apply to welded unalloyed steel gas cylinders formed from several pieces, of an actual thickness of 5 mm or less, capable of being refilled several times, with a capacity ranging from 0,5 to 150 litres inclusive and designed to contain and transport compressed, liquefied or dissolved gases except liquefied gases at very low temperatures and acetylene. The design pressure (Ph) of these cylinders shall not exceed 60 bars. These gas cylinders are hereinafter referred to as "cylinders".
Article 2
For the purposes of this Directive, "EEC-type cylinder" shall mean any cylinder designed and manufactured in such a way that it satisfies the requirements of this Directive and of Directive 76/767/EEC.
Article 3
No Member State may, on grounds relating to the construction or inspection of a cylinder within the meaning of Directive 76/767/EEC and this Directive refuse, prohibit or restrict the placing on the market and putting into service of an EEC-type cylinder.
Article 4
All EEC-type cylinders shall be subject to EEC pattern approval.
All EEC-type cylinders shall be subject to EEC verification with the exception of cylinders with a capacity of not more than 1 litre.
Article 5
Any amendments necessary to adapt sections 1, 2.1.1, 2.3 except for 2.3.3, 2.4 except for 2.4.1 and 2.4.2.1, 3.1.1, 3.1.2, 3.3, 3.4, 3.5, 5 except for 5.2.2 and 5.3, and 6 of Annex I and Annexes II and III to this Directive to technical progress shall be adopted in accordance with the procedure laid down in Article 20 of Directive 76/767/EEC. (1) OJ No C 104, 13.9.1974, p. 59. (2) OJ No C 5, 8.1.1975, p. 52. (3) OJ No C 62, 15.3.1975, p. 31. (4) OJ No L 262, 27.9.1976, p. 153.
Article 6
The procedure laid down in Article 17 of Directive 76/767/EEC may apply to sections 2.2, 2.3.2 and 3.4.1.1 of Annex I to this Directive.
Article 7
1. Member States shall bring into force the laws, regulations and administrative provisions needed in order to comply with this Directive within 18 months of its notification (1) and shall forthwith inform the Commission thereof.
2. Member States shall ensure that the texts of the provisions of national law which they adopt in the field covered by this Directive are communicated to the Commission.
Article 8
This Directive is addressed to the Member States.
Done at Brussels, 17 September 1984.
For the Council
The President
P. BARRY (1) This Directive was notified to the Member States on 26 September 1984.
ANNEX I
1. SYMBOLS AND TERMS USED IN THIS ANNEX
1.1. The symbols used in this Annex have the following meanings: >PIC FILE= "T0036325">
1.2. For the purposes of this Directive, "bursting pressure" means the pressure at plastic instability, i.e. the maximum pressure obtained during a burst test.
1.3. NORMALIZATION
The term "normalization" is used in this Directive as defined in paragraph 68 of EURONORM 52-83.
1.4. STRESS RELIEVING
The term "stress relieving" relates to the heat treatment to which a completed cylinder is subjected, during which the cylinder is heated to a temperature below the steel's lowest critical point (Acl) in order to reduce the residual stresses.
2. TECHNICAL REQUIREMENTS
2.1. MATERIALS 2.1.1. The material used for the manufacture of the stress-resistant cylinder shells must be steel as specified in EURONORM 120-83.
2.1.2. All components of the cylinder body and all the parts welded thereto must be made of mutually compatible materials.
2.1.3. The filler materials must be compatible with the steel so as to form welds with properties equivalent to those specified for the parent metal.
2.1.4. The cylinder manufacturer must obtain and provide chemical cast analysis certificates in respect of the steels supplied for the manufacture of the pressure parts.
2.1.5. It must be possible for independent analyses to be made. These analyses must be carried out either on specimens taken from the materials as supplied to the cylinder manufacturer or on the finished cylinders.
2.1.6. The manufacturer must make available to the inspection body the results of metallurgical and mechanical tests and analyses carried out on welds and must also provide it with a description of the welding methods and processes adopted which can be regarded as representative of the welds made during production.
2.2. HEAT TREATMENT
Cylinders shall be delivered in either the normalized or the stress-relieved condition. The cylinder manufacturer shall certify that the cylinders have been heat treated after completion of all welding and shall certify the process of heat treatment applied. Localized heat treatment is not permitted.
2.3. CALCULATIONS FOR THE PRESSURE PARTS 2.3.1. The thickness of the cylindrical shell wall at any point on the stress-resistant shell of the gas cylinders must not be less than that calculated by the formula: 2.3.1.1. for cylinders without longitudinal welds: >PIC FILE= "T0036326">
2.3.1.2. for cylinders with longitudinal welds: >PIC FILE= "T0036327">
Z being equal to: - 0,85 where the manufacturer radiographs the intersection of the welds for a distance of 100 mm beyond the intersection in the case of the longitudinal weld and of 50 mm (25 mm on each side) in the case of the circumferential welds. One cylinder selected at the beginning and one at the end of each shift period and per machine is to be radiographed,
- 1 where the manufacturer spot radiographs the intersection of the welds for a distance of 100 mm beyond the intersection in the case of the longitudinal weld and of 50 mm (25 mm on each side) in the case of the circumferential welds.
This examination shall be carried out on 10 % of cylinders manufactured, chosen at random.
Should these radiograph tests reveal unacceptable defects, as defined in 3.4.1.4, all the necessary steps must be taken to examine the production run in question and eliminate the defects.
2.3.2. Dimensions and calculation of ends (see figures in Appendix 1) 2.3.2.1. The cylinder ends must fulfil the following conditions: - torispherical ends >PIC FILE= "T0036328">
- elliptical ends >PIC FILE= "T0036329">
- hemispherical ends >PIC FILE= "T0036330">
2.3.2.2. The thickness of these dished ends must not at any point be less than the figure calculated by means of the following formula: >PIC FILE= "T0036331">
The coefficient of form C to be used for full ends is given in the table in Appendix 1.
However, the nominal thickness of the cylindrical edge of the ends must not be less than the nominal thickness of the cylindrical part.
2.3.3. The nominal wall thickness of the cylindrical part and of the dished end may not, under any circumstances, be less than: >PIC FILE= "T0036332">
with a minimum of 1,5 mm in both cases.
2.3.4. The cylinder body, excluding the valve seat, may be made up of two or three parts. The ends must be in one piece and convex.
2.4. CONSTRUCTION AND WORKMANSHIP 2.4.1. General requirements 2.4.1.1. The manufacturer guarantees on his own responsibility that he has the manufacturing facilities and processes to ensure that cylinders produced satisfy the requirements of this Directive.
2.4.1.2. The manufacturer must ensure through adequate supervision that the parent plates and pressed parts used to manufacture the cylinders are free from any defects likely to adversely affect the operating safety of the cylinder.
2.4.2. Pressure parts 2.4.2.1. The manufacturer must describe the welding methods and processes used and indicate the inspections carried out during production.
2.4.2.2. Technical welding requirements
The butt welds must be executed by an automatic welding process.
The butt welds ons the stress-resistant shell may not be located in any area where there are changes of profile.
Fillet welds may not be superimposed on butt welds and must be at least 10 mm away from them.
Welds joining parts making up the shell of the cylinder must satisfy the following conditions (see figures given as examples in Appendix 2): - longitudinal weld : this weld is executed in the form of a butt weld on the full section of the metal of the wall,
- circumferential weld other than those fixing the valve flange collar to the upper end : this weld is executed in the form of a butt weld on the full section of the metal to the wall. A joggle joint weld is considered to be a special type of butt weld,
- circumferential weld fixing the valve flange collar to the upper end : this weld may be either a butt or a fillet weld. If it is a butt weld, it must be applied on the full section of the metal of the wall. A joggle joint weld is regarded as a special type of butt weld.
The requirements of this indent are not applicable where the upper end has a seat within the cylinder and where this seat is fixed to the end by a weld which is unrelated to the leak-tightness of the cylinder (see Appendix 2 hereto, figure 4).
In the case of butt welds, the misalignment of the joint faces may not exceed one-fifth of the thickness of the walls (1/5 a).
2.4.2.3. Inspection of welds
The manufacturer must ensure that the welds show continuous penetration without any deviation of the weld seam, and that they are free of defects likely to jeopardize the safe use of the cylinder.
For two-piece cylinders, and with the exception of welds complying with figure 2A in Appendix 2, the circular butt welds should be radiographed over a length of 100 mm, on one cylinder selected at the beginning and one at the end of each shift period during continuous production and, in the event of production being interrupted for a period of more than 12 hours, on the first cylinder welded.
2.4.2.4. Out-of-roundness
The out-of-roundness of the cylindrical shell of the cylinder must be limited so that the difference between the maximum and minimum outside diameter of the same cross section is not more than 1 % of the average of those diameters.
2.4.3. Fittings 2.4.3.1. The carrying handles and protective collars must be manufactured and welded to the cylinders body in such a way as not to cause dangerous concentrations of stresses or be conducive to the collection of water.
2.4.3.2. The base of the cylinder must be sufficiently strong and made of metal compatible with the type of steel used for the cylinder ; the form of the base must give the cylinder sufficient stability. The top edge of the base must be welded to the cylinder in such a way as not to be conducive to the collection of water nor to allow water to penetrate between the base and the cylinder.
2.4.3.3. Where fitted, identification plates must be fixed on to the stress-resistant shell and not be removable ; all the necessary corrosion prevention measures must be taken.
2.4.3.4. Any other material, however, may be used for the manufacture of the bases, carrying handles or protective collars, provided that their strength is assured and that all risk of the cylinder end corroding is eliminated.
2.4.3.5. Protection of the cock or valve
The cylinder cock or valve must be effectively protected either by its design or by the design on the cylinder itself (e.g. protective collar), or by means of a protective cap or a securely attached cover.
3. TESTS
3.1. MECHANICAL TESTS 3.1.1. General requirements 3.1.1.1. Where not covered by the requirements contained in this Annex, the mechanical tests are to be carried out in accordance with EURONORMS: (a) 2-80 or 11-80 respectively in the case of the tensile test, according to whether the thickness of the test-piece is 3 mm or above, or less than 3 mm;
(b) 6-55 or 12-55 respectively in the case of the bend test, according to whether the thickness of the test-piece is 3 mm or above, or less than 3 mm.
3.1.1.2. All the mechanical tests for checking the properties of the parent metal and welds of the stress-resistant shells of the gas cylinders shall be carried out on test-pieces taken from finished cylinders.
3.1.2. Types of test and evaluation of test results 3.1.2.1. Each sample cylinder shall be subjected to the following tests: (A) For cylinders containing only circumferential welds (two-piece cylinders), on test-pieces taken from the places shown in figure 1 of Appendix 3: >PIC FILE= "T0036333">
(B) For cylinders with longitudinal and circumferential welds (three-piece cylinders), on test-pieces taken from the places shown in figure 2 of Appendix 3: >PIC FILE= "T0036334">
3.1.2.1.1. Test-pieces which are not sufficiently flat must be flattened by cold pressing.
3.1.2.1.2. In all test-pieces containing a weld, the weld shall be machined flush, to trim the surplus.
3.1.2.2. Tensile test 3.1.2.2.1. Tensile test on parent metal 3.1.2.2.1.1. The procedure for carrying out the tensile test is that given in the appropriate EURONORM in accordance with 3.1.1.1.
The two faces of the test-piece representing the inside and outside walls of the cylinder respectively must not be machined.
3.1.2.2.1.2. The values determined for yield stress must be at least equal to the minimum values guaranteed by the cylinder manufacturer.
The values determined for tensile strength and elongation after the test-piece of the parent metal fractures must comply with EURONORM 120-83 (table III).
3.1.2.2.2. Tensile test on welds 3.1.2.2.2.1. The tensile test perpendicular to the weld must be carried out on a test-piece having a reduced cross section 25 mm in width for a length extending up to 15 mm beyond the edges of the weld, as shown in the figure in Appendix 4. Beyond this central part the width of the test-piece must increase progressively.
3.1.2.2.2.2. The tensile strength value obtained must be at least equal to that guaranteed for the parent metal irrespective of where the fracture occurs in the cross section of the central part of the test-piece.
3.1.2.3. Bend test 3.1.2.3.1. The procedure for carrying out the bend test is that given in the appropriate EURONORM in accordance with 3.1.1.1. The bend test must, however, be carried out transversely to the weld on a test-piece 25 mm in width. The mandrel must be placed in the centre of the weld while the test is being performed.
3.1.2.3.2. Cracks must not appear in the test-piece when it is bent round a mandrel such that the inside edges are separated by a distance not greater than the diameter of the mandrel (see figure 2 in Appendix 5).
3.1.2.3.3. The ratio (n) between the diameter of the mandrel and the thickness of the test-piece must not exceed the values given in the following table: >PIC FILE= "T0036335">
3.2. BURST TEST UNDER HYDRAULIC PRESSURE 3.2.1. Test conditions
Cylinders subjected to this test must bear the inscriptions which it is proposed to make on the section of the cylinder subjected to pressure. 3.2.1.1. The burst test under hydraulic pressure must be carried out with equipment which enables the pressure to be increased at an even rate until the cylinder bursts and the change in pressure over time to be recorded.
3.2.2. Interpretation of test 3.2.2.1. The criteria adopted for the interpretation of the burst test are as follows: 3.2.2.1.1. Volumetric expansion of the cylinder ; this equals: - the volume of water used between the time when the pressure starts to rise and the time of bursting for cylinders of a capacity >= 6,5 litres,
- the difference between the volume of the cylinder at the beginning and the end of the test for cylinders of a capacity
3.2.2.1.2. Examination of the tear and the shape of its edges.
3.2.3. Minimum test requirements 3.2.3.1. The measured bursting pressure (Pr) must not under any circumstances be less than 9/4 of the test pressure (Ph).
3.2.3.2. Ratio of the volumetric expansion of the cylinder to its initial volume: - 20 % if the length of the cylinder is greater than the diameter,
- 17 % if the length of the cylinder is equal to or less than the diameter.
3.2.3.3. The burst test must not cause any fragmentation of the cylinder. 3.2.3.3.1. The main fracture must not show any brittleness, i.e. the edges of the fracture must not be radial but must be at an angle to a diametral plane and display a reduction of area throughout their thickness.
3.2.3.3.2. The fracture must not reveal a clear defect in the metal.
3.3. HYDRAULIC TEST 3.3.1. The water pressure in the cylinder must increase at an even rate until the test pressure is reached.
3.3.2. The cylinder must remain under the test pressure long enough to make it possible to establish that the pressure is not falling off and that the cylinder can be guaranteed leak-proof.
3.3.3. After the test the cylinder must show no signs of permanent deformation.
3.3.4. Any cylinder tested which does not pass the test must be rejected.
3.4. NON-DESTRUCTIVE EXAMINATION 3.4.1. Radiographic examination 3.4.1.1. Welds must be radiographed in accordance with ISO specification R 1106-1969, class B.
3.4.1.2. When a wire-type indicator is used, the smallest diameter of visible wire may not exceed 0,10 mm.
When a stepped and holed type indicator is used, the diameter of the smallest visible hole may not exceed 0,25 mm.
3.4.1.3. Assessment of the weld radiographs must be based on the original films in accordance with the practice recommended in ISO standard 2504 - 1973, paragraph 6.
3.4.1.4. The following defects are not acceptable: - cracks, inadequate welds or inadequate penetration of the weld.
The inclusions listed below are regarded as unacceptable: - any elongated inclusion or any group of rounded inclusions in a row where the length represented (over a weld length of 12 a) is greater than 6 mm,
- any gas inclusion measuring more than a/3 mm which is more than 25 mm from any other gas inclusion,
- any other gas inclusion measuring more than a/4 mm,
- gas inclusions over any 100 mm weld length, where the total area of all the figures is greater than 2 a mm2.
3.4.2. Macroscopic examination
The macroscopic examination of a full transverse section of the weld must show a complete fusion on the surface treated with any acid from the macro-preparation and must not show any assembly fault or a significant inclusion or other defects.
In case of doubt, a microscopic examination should be made of the suspect area.
3.5. EXAMINATION OF THE OUTSIDE OF THE WELD 3.5.1. This examination is carried out when the weld has been completed. The welded surface examined must be well illuminated, and must be free from grease, dust, scale residue or protective coating of any kind.
3.5.2. The fusion of the welded metal with the parent metal must be smooth and free from etching. There must be no cracks, notching or porous patches in the welded surface and the surface adjacent to the wall. The welded surface must be regular and even. Where a butt weld has been used, the excess thickness must not exceed 1/4 of the width of the weld.
4. EEC PATTERN APPROVAL
4.1. The EEC pattern approval referred to in Article 4 may be issued for types or families of cylinders.
"Type of cylinder" means cylinders of the same design and thickness, equipped with the same accessories, manufactured in the same workshops from sheet metal of identical technical specifications, welded by the same process and heat-treated under the same conditions.
"Family of cylinders" means three-piece cylinders from the same factory which differ in length alone, within the following limits: - the minimum length must not be less than 3 times the diameter of the cylinder,
- the maximum length must not be more than 1,5 times the length of the tested cylinder.
4.2. The applicant for pattern approval must, for each type of cylinder or family of cylinders, submit the documentation necessary for the verification prescribed below and make available to the Member State a batch of 50 cylinders from which the number of cylinders required for the following tests will be taken, together with any additional information required by the Member State. The applicant must indicate the type and duration of heat treatment, the temperature used and the welding process. He must obtain and provide cast analysis certificates for steel supplied for cylinder manufacture.
4.3. In the course of the EEC pattern approval process, it shall be verified that: - the calculations prescribed in 2.3 are correct,
- the conditions prescribed in 2.1,2.2 and 2.4 and 3.5 are fulfilled.
The following must be carried out on the cylinders submitted as prototypes: - the test prescribed in 3.1, on one cylinder,
- the test prescribed in 3.2, on one cylinder,
- the test prescribed in 3.4, on one cylinder.
If the results of the checks are satisfactory, the Member State shall issue the EEC pattern approval certificate in accordance with the specimen contained in Annex II.
5. EEC VERIFICATION
5.1. For the purpose of EEC verification the cylinder manufacturer must make available to the inspection body: 5.1.1. the EEC pattern approval certificate;
5.1.2. the cast analysis certificates for the steel supplied for the manufacture of the cylinders;
5.1.3. means of identifying the cast of steel from which each cylinder has come;
5.1.4. the documentation - in particular the documents relating to heat treatment - on the cylinders supplied by him, stating the process used in accordance with 2.2;
5.1.5. a list of the cylinders, giving the numbers and inscriptions prescribed in section 6;
5.1.6. the results of the non-destructive tests carried out during production and the welding methods used to ensure good reproductibility of cylinders during manufacture. The manufacturer must also provide a statement to the effect that for series production he undertakes to use a welding method identical to that which he used for the cylinders submitted for EEC pattern approval.
5.2. DURING EEC VERIFICATION 5.2.1. The inspection body must: - ascertain that the EEC pattern approval certificate has been obtained and that the cylinders conform to it,
- verify the documents giving information about the materials and the manufacturing processes, in particular those specified in 2.1.6,
- check whether the technical requirements set out in section 2 have been met and carry out a visual external and internal examination of each cylinder of a random sample,
- be present at the tests prescribed in 3.1 and 3.2 and check their progress,
- check whether the information supplied by the manufacturer mentioned in 5.1.6 is correct, and whether the verifications he has carried out are satisfactory,
- issue the EEC verification certificate on the lines of the specimen set out in Annex III.
5.2.2. For the purpose of carrying out the tests, a random sample of cylinders as indicated below shall be taken from each batch.
A batch shall consist of a maximum of 3 000 cylinders of the same type as defined in the second paragraph of 4.1, manufactured on the same day or on consecutive days.
TABLE 1 >PIC FILE= "T0036337">
Depending on the size of the batch, the sample cylinders shall be subjected to the mechanical tests provided for in 3.1 and to the hydraulic pressure burst test provided for in 3.2, according to the distribution indicated in table 1.
If two or more cylinders fail the tests, the batch must be rejected.
Should one of the cylinders fail either the mechanical tests or the burst test, a number of cylinders as indicated in table 2 shall be selected at random, and the tests carried out according to the distribution in table 1.
TABLE 2 >PIC FILE= "T0036338">
If one or more of these cylinders are not satisfactory, the batch must be rejected.
5.2.3. The selection of samples and all the tests are to be performed in the presence of a representative of the inspection body.
5.2.4. All the cylinders in the batch must undergo the hydraulic test specified in 3.3 in the presence and under the supervision of a representative of the inspection body.
5.3. EXEMPTION FROM EEC VERIFICATION
In the case of cylinders of less than 1 litre capacity, all the tests and checks specified in section 5 must be carried out by the manufacturer on his own responsibility. The manufacturer must make available to the inspection body all the documents and reports relating to the tests and checks.
6. MARKS AND INSCRIPTIONS
6.1. When the inspection body has carried out all the prescribed checks and if the results are satisfactory, it must issue a certificate stating the checks which have been performed.
6.2. For cylinders of less than 6,5 litres capacity, the marks and inscriptions relating to the cylinder construction may be made on the base ; for other cylinders, they shall be made on the head or a reinforced part of the cylinder or on an identification plate. However, some of these inscriptions may be made on the dished end during forming provided the integrity of the cylinder is not weakened.
6.3. EEC PATTERN APPROVAL MARK
By way of derogation from the requirements of section 3 of Annex I to Directive 76/767/EEC, the manufacturer must make the EEC pattern approval mark in the following order: - >PIC FILE= "T0036339">
- the number 3 identifying this Directive,
- the capital letter(s) identifying the Member State which has granted EEC pattern approval and the final two figures of the year in which pattern approval was granted,
- >PIC FILE= "T0036340">
6.4. EEC VERIFICATION MARK
By way of derogation from the requirements of section 3 of Annex II to Directive 76/767/EEC, the inspection body must make the EEC verification mark in the following order: - the small letter "e",
- the capital letter(s) identifying the Member State in which the verification is carried out, together, where necessary, with one or two figures identifying a territorial subdivision,
- the mark of the inspection body made by the verifying agent, together with the mark of the verifying agent if appropriate,
- a hexagon,
- >PIC FILE= "T0036341">
6.5. CONSTRUCTION INSCRIPTIONS 6.5.1. As regards the steel - A number indicating the value of Re in N/mm2 on which the calculation was based,
- the symbol N (cylinder in the normalized condition) or the symbol S (cylinder in the stress-relieved condition).
6.5.2. As regards the hydraulic test
The value of the test pressure in bars followed by the symbol "bars".
6.5.3. As regards the type of cylinder
The minimum capacity in litres, guaranteed by the manufacturer of the cylinder.
This capacity must be given to one decimal place and rounded down.
6.5.4. As regards origin
The capital letter(s) indicating the country of origin followed by the manufacturer's mark and the serial number.
6.6. OTHER INSCRIPTIONS
Where other inscriptions relating neither to the construction nor to the inspection thereof are required under national rules, they must be made on the cylinders in accordance with 6.2.
Appendix 1
>PIC FILE= "T0036342">
COEFFICIENT OF FORM C FOR DISHED ENDS
>PIC FILE= "T0036343">
>PIC FILE= "T0036344">
Appendix 2
>PIC FILE= "T0036345">
Appendix 3
>PIC FILE= "T0036346">
Appendix 4
>PIC FILE= "T0036347">
Appendix 5
>PIC FILE= "T0036348">
ANNEX II
>PIC FILE= "T0036349">
TECHNICAL ANNEX TO EEC PATTERN APPROVAL CERTIFICATE
1. Results of EEC examination of the pattern with a view to EEC pattern approval.
2. Main features of the pattern, in particular: - longitudinal cross-section of the type of cylinder which has received pattern approval, showing: - the nominal external diameter, D,
- the minimum thickness of the cylinder wall, a,
- the minimum thickness of the base and the head,
- the minimum and maximum length(s), Lmin, Lmax,
- the external height of the curved part of the base of cylinder H, in mm,
- the capacity or capacities, Vmin, Vmax;
- the pressure, Ph;
- the name of the manufacturer/No of the drawing and date;
- name of the type of cylinder;
- the steel in accordance with section 2.1.
ANNEX III MODEL
>PIC FILE= "T0036350">
>PIC FILE= "T0036351">
