Action brought on 10 March 2006 — Ider and Others v Commission
Parties
Applicants: Béatrice Ider and Others (represented by: L. Vogel, lawyer)
Defendant: Commission of the European Communities
Form of order sought
- annulment of the decision of the authority authorised to conclude contracts of employment (AACC) of 21 November 2005 rejecting the applicants' complaints of 26 July 2005 criticising the administrative decisions which fixed the grading and remuneration of each of the applicants and also criticising Article 8 of the decision adopted by the College of Commissioners on 27 April 2005 containing the 'General implementing provisions for the transitional measures applicable to staff employed by the Office for Infrastructure in Brussels in the day nurseries and kindergartens in Brussels' and Annexes I and II to that decision;
- also, in so far as necessary, annul the decisions against which the abovementioned complaints were directed;
- an order that the Commission of the European Communities is to pay the costs.
Pleas in law and main arguments
The applicants, who are currently contract staff working in the day nurseries and kindergartens in Brussels, were already performing the same work under employment contracts subject to Belgian law before they were appointed as contract staff. They dispute their grading and their remuneration fixed by the defendant on their appointment as contract staff.
In the first plea in law raised in their application, the applicants submit that pursuant to the Memorandum of Agreement concluded on 22 January 2002 between the Commission and the delegation of the staff of the day nurseries and kindergartens on contracts governed by Belgian law, they should have been given a more advantageous grading. Their grading in function group I, at grade I, constitutes a manifest error of assessment and a breach of the principle of non-discrimination, since they were regarded as inexperienced novices when they had a significant length of service.
In the second plea in law, the applicants claim infringement of Article 2(2) of the Conditions of Employment of Other Servants (CEOS), of the abovementioned Memorandum of Agreement, of the principle of non-discrimination and of the general principles applicable in social security matters. In particular, calculation of the remuneration to be guaranteed to the applicants should not have taken child allowances into account.
--------------------------------------------------
