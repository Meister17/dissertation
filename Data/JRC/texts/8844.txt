COUNCIL REGULATION (EEC) No 1526/76 of 24 June 1976 concerning imports of bran, sharps and other residues derived from the sifting, milling or other working of certain cereals originating in Morocco
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Articles 43 and 113 thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament (1),
Whereas the Cooperation Agreement between the European Economic Community and the Kingdom of Morocco and the Interim Agreement (2) on the advance implementation of certain provisions of the Cooperation Agreement were signed on 27 April 1976;
Whereas, under Article 23 of the Cooperation Agreement and Article 16 of the Interim Agreement, provided that Morocco levies a special charge on exports of bran, sharps and other residues derived from the sifting, milling or other working of cereals other than of maize or rice, falling within subheading 23.02 A II of the Common Customs Tariff, the variable component of the import levy shall be reduced by an amount equivalent to 60 % of the average of the variable components of the levies on the product in question for the three months preceding the month in which such an amount is fixed and the fixed component shall not be imposed;
Whereas this special charge on exports must be reflected in the import price of these products in the Community;
Whereas, in order to ensure that these Agreements are correctly applied, measures should be adopted requiring the importer, at the time when the bran, sharps and other residues are imported, to furnish proof that the special charge on exports has been collected by Morocco;
Whereas, pursuant inter alia to the Agreement in the form of an exchange of letters relating to Article 23 of the Cooperation Agreement and Article 16 of the Interim Agreement between the European Economic Community and the Kingdom of Morocco concerning the import into the Community of bran and sharps originating in Morocco (3), these Agreements require detailed rules for their application,
HAS ADOPTED THIS REGULATION:
Article 1
The variable component of the levy on imports into the Community of bran, sharps and other residues derived from the sifting, milling or other working of cereals, other than of maize or rice, falling within subheading 23.02 A II of the Common Customs Tariff originating in Morocco shall be that calculated in accordance with Article 2 of Council Regulation (EEC) No 2744/75 of 29 October 1975 on the import and export system for products processed from cereals and from rice (4), less an amount equivalent to 60 % of the average of the variable components of the levies on the product in question for the three months preceding the month in which such an amount is fixed.
Article 2
Article 1 shall apply to all imports in respect of which the importer can furnish proof that the special charge on exports has been levied by Morocco in accordance with Article 23 of the Cooperation Agreement or with Article 16 of the Interim Agreement. (1)Opinion delivered on 18 June 1976 and not yet published in the Official Journal. (2)OJ No L 141, 28.5.1976, p. 98. (3)See page 54 of this Official Journal. (4)OJ No L 281, 1.11.1975, p. 65.
Article 3
Detailed rules for the application of this Regulation, in particular as regards the fixing of the amount by which the levy is to be reduced, shall be adopted in accordance with the procedure laid down in Article 26 of Regulation No 359/67/EEC.
Article 4
The fixed component of the levy on imports into the Community of bran, sharps and other residues derived from the sifting, milling or other working of cereals, other than of maize or rice, falling within subheading 23.02 A II of the Common Customs Tariff originating in Morocco shall not be imposed.
Article 5
This Regulation shall enter into force on the day of the entry into force of the Agreement in the form of an exchange of letters relating to Article 23 of the Cooperation Agreement and Article 16 of the Interim Agreement between the European Economic Community and the Kingdom of Morocco concerning the import into the Community of bran and sharps originating in Morocco.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 24 June 1976.
For the Council
The President
G. THORN
