COMMISSION REGULATION (EC) No 1084/95 of 15 May 1995 abolishing the protective measure applicable to imports of garlic originating in Taiwan and replacing it with a certificate of origin
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 1035/72 of 18 May 1972 on the common organization of the market in fruit and vegetables (1), as last amended by the Act of Accession of Austria, Finland and Sweden and by Regulation (EC) No 3290/94 (2), and in particular Articles 22 b and 29 (2) thereof,
Whereas Commission Regulation (EEC) No 1859/93 of 12 July 1993 on the application of the system of import licences for garlic imported from third countries (3), as amended by Regulation (EC) No 1662/94 (4), makes the release of garlic for free circulation in the Community subject to the presentation of an import licence;
Whereas, by means of Regulation (EC) No 2091/94 (5), the Commission took a protective measure applicable to imports of garlic originating in Taiwan and Vietnam whereby the issue of import licences is suspended until 31 May 1995 for these two countries;
Whereas, in the case of Taiwan, the protective measure should not be pursued; whereas, however, because of well-founded doubts as to the true origin of garlic imports from Taiwan and so as to avoid any deflections of trade based on inexact documents, the safeguard measure should be replaced by the introduction of certificates of origin issued by the competent national authorities in accordance with Articles 55 to 65 of Commission Regulation (EEC) No 2454/93 of 2 July 1993 laying down provisions for the implementation of Council Regulation (EEC) No 2913/92 establishing the Community Customs Code (6), as last amended by Regulation (EC) No 3254/94 (7); whereas, for the same reason, direct transport to the Community of garlic originating in Taiwan must be imposed;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Fruit and Vegetables,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 2091/94 shall no longer apply to Taiwan from the date of entry into force of this Regulation.
Article 2
1. The release for free circulation in the Community of garlic originating in Taiwan shall be subject to the following conditions:
(a) the presentation of a certificate of origin issued by the competent national authorities of the country of origin, in accordance with Articles 55 to 65 of Regulation (EEC) No 2454/93;
and (b) the product must be transported direct from Taiwan into the Community.
2. The competent authorities for issuing certificates of origin are listed in the Annex.
3. The following shall be considered as transported direct from Taiwan to the Community:
(a) products transported without passing through the territory of any other country;
(b) products transported through the territory of countries other than Taiwan with or without transhipment or temporary warehousing within those countries, provided that transport through those countries is justified for geographical reasons or exclusively on account of transport requirements and that the products:
- have remained under the supervision of the customs authorities of the country of transit or warehousing,
- have not entered into commerce or been released for consumption,
and - have not undergone operations other than loading, reloading or any other operation intended to keep them in good condition.
4. Evidence that the conditions specified in point (b) of paragraph 3 have been fulfilled shall be supplied to the customs authorities in the Community by the production of:
(a) single transport document issued in Taiwan covering passage through the country of transit;
(b) a certificate issued by the customs authorities of the country of transit:
- giving an exact description of the goods,
- stating the dates of unloading and reloading or of their embarcation or disembarcation, identifying the vessels used, and - certifying the conditions under which the goods remained in the transit country;
or (c) failing these, any substantiating documents.
Article 3
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 15 May 1995.
For the Commission Franz FISCHLER Member of the Commission
ANNEX
Authorities referred to Article 2 paragraph 2
Bureau of Commodity Inspection & Quarantine Ministry of Economic Affairs for Export & Import Certificate issuing on behalf of Ministry of Economic Affairs Republic of China
