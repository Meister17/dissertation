Council Directive 2002/68/EC
of 19 July 2002
amending Directive 2002/57/EC on the marketing of seed of oil and fibre plants
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 37 thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament(1),
Having regard to the opinion of the Economic and Social Committee(2),
Whereas:
(1) Varietal associations of oil and fibre plants species should be included in the scope of Directive 2002/57/EC(3). The conditions to be satisfied by the varietal associations, including the colour of the official label required for packages of certified seed of varietal associations, should also be defined.
(2) Owing to their increased importance in the Community, seed of hybrid varieties of oil and fibre plant species additional to sunflower should also be included in the scope of certain definitions of Directive 2002/57/EC.
(3) Directive 2002/57/EC should therefore be amended accordingly.
(4) Owing to the increased importance in the Community of such seed, the Commission adopted Decision 95/232/EC(4) with the aim of establishing the conditions to be satisfied by the seed of hybrids and varietal associations of swede rape and turnip rape. That Decision expired on 30 June 2002. It is therefore appropriate to keep the Community conditions concerning the marketing of those seeds pending the application of the new provisions,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Directive 2002/57/EC is hereby amended as follows:
1. in Article 2, the following paragraph shall be inserted: "3a. Amendments to be made to paragraph 1(c) and (d) for the purpose of including hybrids of oil and fibre plants other than sunflower within the scope of this Directive shall be adopted in accordance with the procedure laid down in Article 25(2);"
2. in Article 12(1)(a), the following sentence shall be inserted after the second sentence: "In the case of certified seed of a varietal association, the label shall be blue with a diagonal green line;"
3. the following Article shall be inserted: "Article 19a
1. Member States shall permit seed of species of oil and fibre plants to be marketed in the form of a varietal association.
2. For purposes of paragraph 1:
(a) 'varietal association' means an association of certified seed of a specified pollinator-dependant hybrid officially admitted under Directive 2002/53/EC with certified seed of one or more specified pollinator(s), similarly admitted, and mechanically combined in proportions jointly determined by the persons responsible for the maintenance of these components, such combination having been notified to the certification authority;
(b) 'pollinator-dependant hybrid' means the male-sterile component within the 'varietal association' (female component);
(c) 'pollinator(s)' means the component shedding pollen within the 'varietal association' (male component).
3. The seed of the female and male components shall be dressed using seed dressings of different colours."
Article 2
In Article 5(5) of Decision 95/232/CE the date "30 June 2002" shall be replaced by "30 June 2003".
Article 3
1. Member States shall bring into force the laws, regulations or administrative provisions necessary to comply with this Directive no later than 30 June 2003. They shall forthwith inform the Commission thereof.
When Member States adopt these measures, they shall contain a reference to this Directive or shall be accompanied by such reference on the occasion of their official publication. The methods of making such reference shall be laid down by the Member States.
2. Member States shall communicate to the Commission the texts of the main provisions of national law which they adopt in the field governed by this Directive.
Article 4
This Directive shall enter into force on the seventh day following its publication in the Official Journal of the European Communities.
Article 2 shall apply from 1 July 2002.
Article 5
This Directive is addressed to the Member States.
Done at Brussels, 19 July 2002.
For the Council
The President
T. Pedersen
(1) Opinion delivered on 2 July 2002 (not yet published in the Official Journal).
(2) Opinion delivered on 17 July 2002 (not yet published in the Official Journal).
(3) OJ L 193, 20.7.2002, p. 74.
(4) OJ L 154, 5.7.1995, p. 22. Decision as last amended by Decision 2001/18/EC (OJ L 4, 9.1.2001, p. 36).
