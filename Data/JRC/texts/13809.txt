Commission Regulation (EC) No 1547/2006
of 13 October 2006
laying down detailed rules for the application of Council Regulation (EEC) No 2204/90
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2204/90 of 24 July 1990 laying down additional general rules on the common organisation of the market in milk and milk products as regards cheese [1], and in particular the second paragraph of Article 1, the second subparagraph of Article 3(3) and Article 5 thereof,
Whereas:
(1) Commission Regulation (EEC) No 2742/90 of 26 September 1990 laying down detailed rules for the application of Council Regulation (EEC) No 2204/90 [2] has been substantially amended several times [3]. In the interests of clarity and rationality the said Regulation should be codified.
(2) The first paragraph of Article 1 of Regulation (EEC) No 2204/90 stipulates that the use of casein and caseinates in the manufacture of cheese is subject to prior authorisation. It is necessary to lay down detailed rules for the grant of such authorisations, having regard to the requirements as regards checks on undertakings. Authorisations should be granted for a limited period, so that the Member States can impose penalties for failure to comply with Community provisions.
(3) The second paragraph of Article 1 of Regulation (EEC) No 2204/90 stipulates that the maximum percentages of casein and caseinates to be incorporated in cheese must be determined on the basis of objective criteria laid down having regard to what is technologically necessary. It is necessary to determine the said percentages on the basis of data supplied by the Member States. In order to facilitate checks to ensure compliance with this provision, it is advisable to set overall percentages rather than percentages for individual products.
(4) Article 3(1) of Regulation (EEC) No 2204/90 requires Member States to introduce administrative and physical controls. It is necessary to specify the conditions which these controls should fulfil, particularly with regard to their frequency.
(5) Article 3(3) of Regulation (EEC) No 2204/90 states that a sum equal to 110 % of the difference between the value of the skimmed milk necessary for producing 100 kilograms of caseins and caseinates resulting from the market price of skimmed milk powder on the one hand and the market price for casein and caseinates on the other hand is due per 100 kilograms for quantities of caseins and caseinates used without authorisation. This sum has to be determined having regard to prices recorded on the markets during a reference period.
(6) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Milk and Milk Products,
HAS ADOPTED THIS REGULATION:
Article 1
1. Authorisations referred to in Article 1 of Regulation (EEC) No 2204/90 shall be granted for a period of 12 months, at the request of the undertakings concerned, subject to their giving a prior undertaking in writing to accept and comply with the provisions of Article 3(1)(a) and (b) on the one hand, and Article 3(1)(c) on the other, of that Regulation.
2. Authorisations shall be issued with a serial number for each undertaking or, where necessary, for each production unit.
3. Authorisations may cover one or more types of cheese, in accordance with the application made by the undertaking concerned.
Article 2
1. The maximum percentages to be incorporated, as referred to in the second paragraph of Article 1 of Regulation (EEC) No 2204/90, are given in Annex I to this Regulation. They shall apply to the weight of the types of cheese set out in that Annex produced by the undertaking or production unit concerned during a six-month period.
2. The list of products given in Annex I and the maximum percentages relating thereto shall be adjusted to take account of reasoned applications providing evidence that there is a technological need for the addition of casein or caseinates.
Article 3
1. The stock accounts referred to in Article 3(1)(b) of Regulation (EEC) No 2204/90 shall include information as to the origin, composition and quantity of the raw materials used in the manufacture of the cheeses. Member States may require samples to be taken to provide a check on this information. Member States shall ensure that the information collected from undertakings is treated as confidential.
2. The checks referred to in Article 3(1)(c) of Regulation (EEC) No 2204/90 must fulfil the following requirements:
(a) at least 30 % of the undertakings in receipt of authorisations shall be checked each quarter;
(b) each undertaking in receipt of an authorisation shall be checked at least once per year, and undertakings producing more than 300 tonnes of cheese per year shall be checked at least twice per year.
3. Within a period of one month from when the infringement was recorded, Member States shall notify the Commission of cases in which casein and/or caseinates have been used either without authorisation or without abiding by the percentages laid down.
Article 4
1. The sum due in accordance with Article 3(3) of Regulation (EEC) No 2204/90 shall be EUR 22,00 per 100 kg of caseins and/or caseinates.
2. The sums recovered in this way shall be paid to the paying departments and agencies and offset by them against expenditure financed by the Guarantee Section of the European Agricultural Guidance and Guarantee Fund.
Article 5
In addition to the notifications required under Article 3(2) of Regulation (EEC) No 2204/90, Member States shall submit to the Commission before the end of each quarter the following information on the previous quarter:
(a) the number of authorisations granted and/or revoked;
(b) the quantities of casein and caseinates declared in respect of these authorisations, broken down by type of cheese.
Article 6
Regulation (EEC) No 2742/90 is repealed.
References to the repealed Regulation shall be construed as references to this Regulation and shall be read in accordance with the correlation table in Annex III.
Article 7
This Regulation shall enter into force on the 20th day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 13 October 2006.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 201, 31.7.1990, p. 7. Regulation as amended by Regulation (EC) No 2583/2001 (OJ L 345, 29.12.2001, p. 6).
[2] OJ L 264, 27.9.1990, p. 20. Regulation as last amended by Regulation (EC) No 1815/2005 (OJ L 292, 8.11.2005, p. 4).
[3] See Annex II.
--------------------------------------------------
ANNEX I
Maximum percentages to be incorporated, as referred to in Article 2(1):
(a) processed cheese falling within CN code 040630: 5 %;
(b) grated processed cheese covered by CN code ex040620: 5 % [1];
(c) processed cheese in powdered form covered by CN code ex040620: 5 % [1].
[1] Manufactured by a continuous process, without addition of already manufactured processed cheese.
--------------------------------------------------
ANNEX II
Repealed Regulation with its successive amendments
Commission Regulation (EEC) No 2742/90 (OJ L 264, 27.9.1990, p. 20) | |
Commission Regulation (EEC) No 837/91 (OJ L 85, 5.4.1991, p. 15) | |
Commission Regulation (EEC) No 2146/92 (OJ L 214, 30.7.1992, p. 23) | |
Commission Regulation (EC) No 1802/95 (OJ L 174, 26.7.1995, p. 27) | Only concerning references made in the Annex to Regulation (EEC) No 2742/90 |
Commission Regulation (EC) No 78/96 (OJ L 15, 20.1.1996, p. 15) | |
Commission Regulation (EC) No 265/2002 (OJ L 43, 14.2.2002, p. 13) | |
Commission Regulation (EC) No 1815/2005 (OJ L 292, 8.11.2005, p. 4) | |
--------------------------------------------------
ANNEX III
Correlation table
Regulation (EEC) No 2742/90 | This Regulation |
Articles 1 to 4 | Articles 1 to 4 |
Article 5, points (1) and (2) | Article 5, points (a) and (b) |
— | Article 6 |
Article 6 | Article 7 |
Annex, indents 1, 2 and 3 | Annex I, points (a) (b) and (c) |
— | Annex II |
— | Annex III |
--------------------------------------------------
