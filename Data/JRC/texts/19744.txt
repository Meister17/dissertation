Commission Regulation (EC) No 639/2003
of 9 April 2003
laying down detailed rules pursuant to Council Regulation (EC) No 1254/1999 as regards requirements for the granting of export refunds related to the welfare of live bovine animals during transport
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1254/1999 of 17 May 1999 on the common organisation of the market in beef and veal(1), as last amended by Commission Regulation (EC) No 2345/2001(2), and in particular Article 33(12) thereof,
Whereas:
(1) Article 33(9) of Regulation (EC) No 1254/1999 provides that the payment of the refund for exports of live bovine animals is subject to compliance with Community legislation concerning animal welfare and, in particular, Council Directive 91/628/EEC of 19 November 1991 on the protection of animals during transport(3), as last amended by Directive 95/29/EC(4).
(2) Detailed rules as regards the welfare of live bovine animals during transport are laid down in Commission Regulation (EC) No 615/98(5). Experience has shown the need to improve the enforcement of animal welfare requirements for the granting of refunds for the export of those animals. Therefore the checks and penalties provided for in that Regulation should be reinforced. In addition, in the interests of clarity, Regulation (EC) No 615/98 should be replaced.
(3) In order to guarantee that the animal welfare standards are maintained, a monitoring system should be introduced comprising compulsory checks at the exit point from the Community and after leaving the customs territory of the Community where there is a change of means of transport and also at the place of the first unloading in the third country of final destination.
(4) In order to facilitate proper checks on exit from the Community, it is necessary to designate exit points.
(5) The assessment of the physical condition and state of health of animals requires specific expertise and experience. Checks should therefore be carried out by a veterinarian. Moreover, the extent of those checks should be clarified and a model report set out in order to make those checks accurate and harmonised.
(6) Checks in third countries for the purposes of this Regulation should be compulsory and should be carried out by agencies of Member States or by international control and supervisory agencies (hereinafter referred to as SAs) approved and controlled by Member States in accordance with Commission Regulation (EC) No 800/1999 of 15 April 1999 laying down common detailed rules for the application of the system of export refunds on agricultural products(6), as last amended by Regulation (EC) No 444/2003(7). In order to carry out checks for purposes of this Regulation, the SAs should in particular meet the requirements for approval and control set out in Annex VI to Regulation (EC) No 800/1999 as from 1 January 2004.
(7) In addition to the non-payment of the export refund, when there is evidence that Directive 91/628/EEC is not complied with for a high number of animals, appropriate penalties should be applied. Furthermore, where such non-compliance is due to a complete disregard of animal welfare requirements, the total loss of the refund should be established.
(8) Member States should provide the Commission with the necessary information for purposes of monitoring and reporting on the application of this Regulation.
(9) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Beef and Veal,
HAS ADOPTED THIS REGULATION:
Article 1
Scope
Payment of export refunds for live bovine animals falling within CN code 0102 (hereinafter referred to as the animals), pursuant to Article 33(9), second subparagraph, of Regulation (EC) No 1254/1999, shall be subject to compliance, during the transport of the animals to the first place of unloading in the third country of final destination, with the provisions of Directive 91/628/EEC and of this Regulation.
Article 2
Checks within the Community
1. The exit of the animals from the customs territory of the Community may take place only through the following exit points:
(a) a border inspection post agreed by a Commission decision for veterinary checks on live ungulates from third countries; or
(b) an exit point designated by the Member State.
2. The official veterinarian at the exit point from the Community shall verify in accordance with Council Directive 96/93/EC(8) for those animals for which an export declaration is accepted whether:
(a) the requirements laid down in Directive 91/628/EEC have been complied with from the place of departure, as defined in Article 2(2)(e) of that Directive, until the exit point; and
(b) the transport conditions for the rest of the journey comply with the provisions of Directive 91/628/EEC and that the necessary arrangements have been taken to ensure their compliance until the first unloading in the third country of final destination.
The official veterinarian who has conducted the checks shall complete a report in accordance with the model set out in Annex I certifying whether the results of the checks performed in accordance with the first subparagraph are satisfactory or not satisfactory.
The veterinarian authority responsible for the exit point shall keep this report for at least three years.
3. If the official veterinarian at the exit point is satisfied that the requirements of paragraph 2 are met, he shall certify this by the entry:
- Resultados de los controles de conformidad con el artículo 2 del Reglamento (CE) n° 639/2003 satisfactorios
- Resultater af kontrollen efter artikel 2 i forordning (EF) nr. 639/2003 er tilfredsstillende
- Ergebnisse der Kontrollen nach Artikel 2 der Verordnung (EG) Nr. 639/2003 zufriedenstellend
- Αποτελέσματα των ελέγχων βάσει του άρθρου 2 του κανονισμού (EK) αριθ. 639/2003 ικανοποιητικά
- Results of the checks pursuant to Article 2 of Regulation (EC) No 639/2003 satisfactory
- Résultats des contrôles visés à l'article 2 du règlement (CE) n° 639/2003 satisfaisants
- Risultati dei controlli conformi alle disposizioni dell'articolo 2 del regolamento (CE) n. 639/2003
- Bevindingen bij controle overeenkomstig artikel 2 van Verordening (EG) nr. 639/2003 bevredigend
- Resultados dos controlos satisfatórios nos termos do artigo 2.o do Regulamento (CE) n.o 639/2003
- Asetuksen (EY) N:o 639/2003 2 artiklan mukaisten tarkastuksen tulokset ovat hyväksyttävät
- Resultaten av kontrollen enligt artikel 2 i förordning (EG) nr 639/2003 är tillfredsställande
and by stamping and signing the document constituting evidence of exit from the customs territory of the Community, either in Section J of the control copy T5 or in the most appropriate place on the national document.
4. The official veterinarian at the exit point shall endorse on the document referred to in paragraph 3 the total number of animals for which an export declaration had been accepted minus the number of animals which gave birth or aborted during transport, died or for which the requirements of Directive 91/628/EEC were not complied with.
5. Member States may require the exporter to give advance notice of the arrival of the consignment at the exit point to the official veterinarian at the exit point.
6. By way of derogation from paragraph 1, where the simplified Community transit procedure for carriage by rail or large containers referred to in Article 10 of Regulation (EC) No 800/1999 applies, the official veterinarian shall carry out checks at the office where the animals are placed under such procedure.
The certification and endorsements referred to in paragraphs 3 and 4 of this Article shall be made on the document used for the purpose of payment of the refund or on the T5 control copy in the case described in Article 10(4) of Regulation (EC) No 800/1999.
Article 3
Checks in third countries
1. After leaving the customs territory of the Community, the exporter shall ensure that the animals shall be subject to a check at:
(a) any place where there is a change of means of transport except where such change was not planned and is due to exceptional and unforeseen circumstances;
(b) the place of the first unloading in the third country of final destination.
2. An international control and supervisory agency, approved and controlled for this purpose by a Member State in accordance with Articles 16(a) to 16(f) of Regulation (EC) No 800/1999, or an official agency of a Member State shall be responsible for carrying out the checks provided for in paragraph 1.
The checks provided for in paragraph 1 shall be carried out by a veterinarian.
A report of each check carried out pursuant to paragraph 1 shall be completed in accordance with the models set out in Annexes II and III by the veterinarian who carried out the check.
Article 4
Procedure for payment of export refunds
1. The exporter shall inform the competent authority of the Member State where the export declaration is accepted about all necessary details of the journey, at the latest when the export declaration is lodged.
At the same time, or at the latest when he becomes aware thereof, the exporter shall inform the competent authority about any possible change of the means of transport.
2. Applications for the payment of export refunds drawn up in accordance with Article 49 of Regulation (EC) No 800/1999 must be supplemented within the time limit laid down in that Article by:
(a) the document referred to in Article 2(3) of this Regulation duly completed; and
(b) the reports provided for in Article 3(2) of this Regulation.
3. Where the checks referred to in Article 3(1) could not be carried out due to circumstances beyond the control of the exporter, the competent authority, on a reasoned request from the exporter, may accept other documents which prove to its satisfaction that Directive 91/628/EEC has been complied with.
Article 5
Non-payment of export refunds
1. The export refund shall not be paid for:
(a) animals which have died during transport, except as provided in paragraph 2;
(b) animals which have given birth or aborted during transport before their first unloading in the third country of final destination;
(c) animals for which the competent authority considers that Directive 91/628/EEC has not been complied with, in the light of the documents referred to in Article 4(2) and/or all other elements at its disposal concerning compliance with this Regulation.
The weight of an animal in respect of which the refund is not paid shall be determined on a flat-rate basis by dividing the total weight in kilograms given in the export declaration by the total number of animals given in that same declaration.
2. Where the animals have died during transport as a result of force majeure after leaving the customs territory of the Community:
(a) in the case of a non-differentiated refund, the total refund shall be paid;
(b) in the case of a differentiated refund, the part of the refund defined in accordance with Article 18(2) of Regulation (EC) No 800/1999 shall be paid.
Article 6
Penalties
1. The refund shall be further reduced by an amount equal to the amount of refund which is not paid pursuant to Article 5(1), if the number of animals for which no export refund is paid amounts to:
(a) more than 1 % of the number endorsed in the accepted export declaration, but at least two animals; or
(b) more than five animals.
2. The refund for all animals indicated in the export declaration shall be refused if the number of animals for which no refund is paid pursuant to Article 5(1) amounts to:
(a) more than 5 % of the number endorsed in the accepted export declaration, but at least three animals; or
(b) 10 animals, but at least 2 % of the number endorsed in the accepted export declaration.
3. For the purpose of paragraphs 1 and 2, the animals which died during transport and the animals that gave birth or aborted before their first unloading in the third country of final destination for which the exporter proves to the satisfaction of the competent authority that their death or the birth or abortion was not the result of non-compliance with Directive 91/628/EEC shall not be taken into account.
4. The penalty referred to in Article 51 of Regulation (EC) No 800/1999 shall not apply to the amount not paid and the amount of the reduction pursuant to Article 5 and to paragraphs 1 and 2 of this Article.
Article 7
Recovery of amounts over-paid
Where it is established after payment of the refund that Directive 91/628/EEC has not been complied with, the relevant part of the refund, including where appropriate the penalty pursuant to Article 6 of this Regulation, shall be considered to have been paid unduly and shall be recovered in accordance with the provisions of Article 52 of Regulation (EC) No 800/1999.
Article 8
Communication of information
Member States shall notify to the Commission no later than 31 March of each year the following information relating to the application of this Regulation during the previous calendar year:
(a) the number of export declarations for live animals for which the refund was paid and the number of live animals for which the refund was paid;
(b) the number of export declarations for which the refund was totally or partially not paid and the number of animals for which the refund was not paid;
(c) the number of export declarations for which the refund was totally or partially recovered and the number of animals for which the refund was recovered, including those for which the recovery of the refunds relates to export operations carried out before the period concerned;
(d) the reasons for the non-payment and the recovery of the refund for the animals referred to in points (b) and (c);
(e) the amounts of the refunds in euro that were not paid and the amounts that were recovered, including the recovered amounts corresponding to export operations carried out before the period concerned;
(f) the number of export declarations and the amounts for which the recovery procedure is still in process;
(g) any other information Member States consider relevant regarding the functioning of this Regulation.
Article 9
Repeal
Regulation (EC) No 615/98 is repealed. However, that Regulation shall continue to apply to export declarations accepted prior to the application of this Regulation.
References to the repealed Regulation shall be construed as references to this Regulation and shall be read in accordance with the correlation table set out in Annex IV.
Article 10
Entry into force
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Union.
It shall apply to export declarations accepted from 1 October 2003.
However, the requirement that the approval and control of the international control and supervisory agency referred to in Article 3(2) be in accordance with Articles 16(a) to 16(f) of Regulation (EC) No 800/1999 shall apply to export declarations accepted from 1 January 2004.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 9 April 2003.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 160, 26.6.1999, p. 21.
(2) OJ L 315, 1.12.2001, p. 29.
(3) OJ L 340, 11.12.1991, p. 17.
(4) OJ L 148, 30.6.1995, p. 52.
(5) OJ L 82, 19.3.1998, p. 19.
(6) OJ L 102, 17.4.1999, p. 11.
(7) OJ L 67, 12.3.2003, p. 3.
(8) OJ L 13, 16.1.1997, p. 28.
ANNEX I
>PIC FILE= "L_2003093EN.001402.TIF">
ANNEX II
>PIC FILE= "L_2003093EN.001502.TIF">
ANNEX III
>PIC FILE= "L_2003093EN.001602.TIF">
ANNEX IV
Correlation table
>TABLE>
