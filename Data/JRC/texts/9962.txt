[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 26.9.2006
COM(2006) 552 final
COMMUNICATION FROM THE COMMISSION TO THE EUROPEAN PARLIAMENT AND THE COUNCIL
on the International Health Regulations
COMMUNICATION FROM THE COMMISSION TO THE EUROPEAN PARLIAMENT AND THE COUNCIL
on the International Health Regulations
Blank Line.
1. Introduction 3
2. Background to the IHR 4
2.1. Brief description 4
2.2. Legal competence 4
3. Reservations 5
3.1. EC & MS reservations 5
3.2. EU reactions to third country reservations 5
4. Voluntary early application of flu-related aspects 5
4.1. Specific aspects identified for early application – EU context 6
5. Full Implementation – EU role 7
5.1. Memorandum of understanding between the Community & WHO 7
5.2. Role of existing EU networks, EWRS and the Health Security Committee 7
5.3. Role of the ECDC 9
5.4. Roster of Experts, Emergency & Review Committees 10
6. Restrictions on policy responses affecting international traffic 11
6.1. Border measures 11
6.2. Contact tracing 12
6.3. Particular issues in relation to pandemic flu 12
7. Operational Conclusions 12
ANNEX - Summary and List of Acronyms…… ….14
1. I NTRODUCTION
This Communication sets out the Commission’s views on implementation of the revised International Health Regulations (2005) - the IHR[1]. It is intended to promote a structured discussion with the Council and Parliament.
The IHR is an international legal instrument which aims to prevent, protect against and control the spread of disease, and to provide a public health response proportionate to the risks, but avoiding unnecessary interference with traffic and trade.
The IHR enters into force on 15 June 2007, and requires gradual implementation by 2016 at the latest. The World Health Assembly (WHA) of May 2006 adopted a resolution[2] calling for voluntary early application of certain IHR aspects relating to pandemic influenza.
Making the IHR work in practice will require close co-ordination between the Commission and Member States (MS). By working together, the European Union (EU) and MS can optimise implementation, and better protect EU citizens from public health emergencies of international concern (PHEICs).
In particular the European Centre for Disease Prevention and Control (ECDC[3]), and the EU Early Warning and Response System for public health threats (EWRS[4]), can help us to implement the IHR in a stronger, more coherent way.
In brief, this Communication :
- sets out the Commission’s interpretation of the EU legal position with regard to IHR reservations and early implementation of flu-related aspects;
- clarifies the EU role in IHR implementation, particularly through the ECDC and the EWRS;
- reminds MS of the restrictions the IHR places on national measures which can be taken on public health grounds, particularly on the types of international border measures which could be taken in response to pandemic flu;
- stimulates MS to develop and share their own plans for IHR implementation.
A summary and list of acronyms are annexed to this Communication.
2. BACKGROUND TO THE IHR
2.1. Brief description[5]
In 1951 the member states of the World Health Organization (WHO) adopted the first International Sanitary Regulations. These were renamed the International Health Regulations in 1969; since then they have been amended and modified four times, most recently and most comprehensively in 2005.
The IHR is an international legal instrument which is legally binding on all States Parties. It does not require ratification by individual states, but any state may reject the IHR or make reservations on specific aspects. 192 states are WHO members, including the 25 EU MS, Bulgaria and Romania.
The EU is not a party to the IHR, but the IHR recognizes the role of “regional economic integration organizations” such as the EU. Specifically, IHR Article 57(3) states that “Without prejudice to their obligations under these Regulations, States Parties that are members of a regional economic integration organization shall apply in their mutual relations the common rules in force in that regional economic integration organization” .
This means, for example, that if the WHO were to recommend states to refuse entry or departure to certain goods under the IHR (Article 18(2)), the EU would have to act collectively, at the initiative of the Commission, as EU single market legislation prevents MS from taking unilateral action.
2.2. Legal competence
The IHR is an international instrument which involves matters of mixed competence between national governments and the European Community (EC)[6].
Many IHR articles relate to issues covered by EC law. Depending on the law concerned, these are viewed either as a matter of exclusive Community competence, or shared competence between national governments and the Community. For example IHR Article 45 covers processing of personal data, which in the EU is addressed by legislation with a single market legal base[7], so is a matter of exclusive Community competence.
Other IHR articles are entirely a matter for national governments, because there is no related EC law. For example IHR Article 41 covers charges for the application of health measures to conveyances (ships and aircraft), which are not specifically dealt with in EC law and therefore not a Community competence.
It is not the purpose of this Communication to list which IHR articles are subject to national, Community, or shared competence, but to consider how the IHR should be implemented in a coordinated manner across the Community.
3. RESERVATIONS
Under IHR Article 62, it is open to States Parties to notify reservations to the WHO Director-General until 15 December 2006 under certain conditions. Reservations would generally be used to indicate that states cannot or will not implement particular aspects of the IHR.
MS and the Commission worked in close and effective cooperation throughout the IHR negotiations to ensure that the final IHR would be consistent with EC and national law, so that there would be no need for reservations.
3.1. EC & MS reservations
A final screening of EC law against the IHR did not find that any reservations were needed in respect of IHR aspects under Community competence.
To date no MS has identified a need for reservations on IHR aspects under national competence. It is possible that difficulties with individual provisions might arise during preparations for implementation. If so, a common EU approach would be necessary to formally enter reservations with the WHO, in order to respect the principle of unity of international representation, as recognised by EC case law and flowing from Article 10 of the Treaty establishing the European Community.
The Commission is satisfied there is no need for any IHR reservations on issues of Community competence. In the event that a Member State wishes to make an IHR reservation on an issue of national competence, EU coordination will be necessary. In order to achieve this before the deadline of December 2006, MS wishing to enter a reservation should notify the Commission and other MS thereof at the earliest possible stage so that a coordinated approach can be agreed.
3.2. EU reactions to third country reservations
No third country has yet submitted any reservations to the IHR, but some may wish to do so. If or when WHO receives a reservation, Article 62 of the IHR requires the WHO Director-General to notify every other WHO State Party which has not rejected the IHR, and allow 6 months for them to react to that reservation. If at least one third of those states object to the reservation, WHO will invite the reserving state to consider withdrawing its reservation within 3 months. If the state does not agree, WHO will seek the view of the IHR Review Committee.
EU coordination will also be necessary in order to establish a common approach to any third country reservations.
4. VOLUNTARY EARLY APPLICATION OF FLU-RELATED ASPECTS
WHA Resolution 59.2, adopted on 26 May 2006, calls upon States Parties to comply immediately, on a voluntary basis, with the IHR provisions considered relevant to the risk posed by avian and potential human pandemic influenza.
It is important to clarify that from 15 June 2007, the IHR enters into force and is binding on all parties. After that date, different provisions have different timetables for implementation, but compliance with the whole instrument is mandatory.
4.1. Specific aspects identified for early application – EU context
T he WHA resolution urges states to
1. designate IHR national focal points (NFPs) within 90 days of adoption of the WHA resolution (ie before end August 2006). This should help establish IHR lines of communication in time for the next flu season in the EU. We believe the ECDC should work closely with NFPs, especially in the case of disease outbreaks occurring in more than one EU MS. See section 5 on the role of the ECDC.
2. follow mechanisms and procedures in the IHR relating to diseases which may constitute a PHEIC . At EU level, this is already being taken into account. For example in advice to MS on the implementation of Directive 2004/38 on the free movement of EU citizens, “disease with epidemic potential as defined by the relevant instruments of the WHO” is defined in relation to IHR Annex 2.
3. notify WHO and subsequently communicate with them about any probable or confirmed human case of avian influenza. No suspected human cases have so far been identified in the EU.
4. disseminate to WHO collaborating centres information and biological materials related to highly pathogenic avian influenza and other novel influenza strains. Samples from suspect human cases in Turkey were sent to WHO laboratories in the UK for testing earlier this year. Delays in sending samples have been experienced with some EU airlines; this is being followed up with EU Directors General for Civil Aviation.
5. develop domestic influenza vaccine production capacity or work with neighbouring states to establish regional capacity. EU MS are working on this as a high priority. Vaccine capacity has been discussed in the Council, along with a Commission options paper on the establishment of an EU stockpile of antivirals.
6. strengthen collaboration on human and zoonotic influenzas among national organizations. The ECDC and Commission 6th Framework Programme for research are both facilitating EU scientific collaboration in this respect.
7. respect IHR time frames for activities and communications, particularly for reporting human cases of avian influenza. Experience so far suggests the IHR time frames should not be problematic for EU MS, but further assessment of MS capacities may be required.
8. collaborate, including by mobilizing financial support, to strengthen influenza surveillance and response in countries affected by avian and pandemic influenza. The EU has provided financial contributions to strengthen influenza surveillance and response in the EU, through the ECDC and the public health programme (Decision 1786/2002/EC), and in developing countries, through the Beijing Pledging Conference co-hosted by the Commission in January 2006.
4.2. Need for common EU approach to voluntary early application
In order to respect the principle of unity of international representation in the EC Treaty, a common approach is needed.
The same IHR aspects should be implemented to the same timetable across EU Member States. This will require coordination at EU level. The Commission will take any appropriate initiative which could be needed in order to facilitate this implementation.
5. FULL IMPLEMENTATION – EU ROLE
The IHR has clear EU policy implications, particularly on trade, transport and border policies in the single market, where in the event of a PHEIC, EC law provides the mechanisms to respond to emergencies .
As already noted during the negotiations, most provisions of the IHR concern both the EU and the MS, so it is necessary to coordinate closely in order to achieve an optimal implementation.
The EU, its institutions and networks can therefore play a positive role in IHR implementation, to add value and avoid duplication of effort at national level.
5.1. Memorandum of understanding between the Community & WHO
On the basis of the proposals on working practices and the role of EU institutions and networks set out below, it would be desirable to adopt an administrative memorandum of understanding between the Community and WHO to ensure that arrangements are clearly defined in respect of the IHR. The Commission would be responsible for drafting, negotiating and signing this memorandum.
5.2. Role of existing EU networks, EWRS and the Health Security Committee
Decision 2119/98/EC of the European Parliament and the Council set up a network for the epidemiological surveillance and control of communicable diseases in the EU. Commission Decision 2000/57 established criteria for the use of EWRS for the prevention and control of communicable diseases.
The main role of EWRS is to report communicable diseases affecting, or with potential risk of propagation to, more than one MS. Decision 2119/98/EC also requires MS to report measures taken in response, and to consult each other in liaison with the Commission with a view to coordinating efforts for the prevention and control of communicable diseases.
The scope of EWRS is limited to communicable diseases, including those of unknown origin. It is therefore not as broad as the IHR, which includes events of unknown cause or source , and the spread of toxic, infectious or otherwise hazardous materials , as potential PHEICs. This is set out in IHR Annex 2.
But bearing in mind that most potential PHEICs are likely to result from communicable diseases, it is clear that there are many similarities between the information and communication needs MS face with respect to EWRS and the IHR.
For potential PHEICs caused by sources other than communicable diseases, there may be a complementary role for the EU Health Security Committee (HSC), which was set up in 2001 to promote cooperation in tackling bioterrorism. HSC representatives are co-ordinating multi-sectoral responses to health threats in Member States, which are of particular importance in dealing with threats other than communicable disease outbreaks. HSC members communicate with one another and the Commission via a secure information system (RAS-BICHAT), which allows rapid alerting of such events, 24 hours, 7 days a week.
The ECDC Founding Regulation states that the ECDC shall assist the Commission by operating EWRS. They are currently surveying user requirements with a view to improving the system, learning from recent EU crisis simulation exercises on smallpox and flu, organized by the Commission to test EU and MS procedures.
In order to maximize efficiency the Commission proposes the following working practices:
9. Nominating the same national focal point for EWRS as for IHR . MS may need to make some adjustments in order to fulfil both systems’ requirements, but the overlap of functions would justify this approach, especially as any communicable disease notifiable under IHR would also be notifiable under EWRS. Complementarities with the EU Rapid Alert System for Biological and Chemical Attacks and Threats could also be explored to define the best way to handle IHR notifications on unknown sources or hazardous materials.
10. Simultaneously informing EWRS and WHO about events within their territory which are notifiable under IHR but which are not potential PHEICs. This option is already available, and used regularly, simply by activating an option on the EWRS interface to copy WHO. This could become standard practice, so that all interested parties are informed simultaneously.
11. Informing the EU Communicable Disease Network in advance of making a formal IHR notification of a potential PHEIC. This would allow MS to coordinate notifications, for example in the event of an outbreak of communicable disease which occurs simultaneously in more than one MS (a multi-state outbreak).
12. Particularly for multi-state outbreaks, using EWRS and / or the Health Security Committee to help coordinate health risk management and response prior to communicating with WHO. Decision 2119/98/EC already requires MS to consult each other with a view to coordinating their response in the event of an outbreak. A coordinated response to PHEICs under the IHR could be prepared through the EWRS and / or the Health Security Committee, depending on the nature of the threat. This response could then be communicated and coordinated with WHO at EU level, by the Presidency or by the Commission. This would give the EU a single voice, reduce unnecessary efforts and ensure more efficient policy coordination. This process could be defined on the basis of existing mechanisms.
5.3. Role of the ECDC
The ECDC is an independent agency charged with identifying, assessing, and communicating current and emerging threats to human health from communicable diseases. The ECDC collects and processes relevant scientific and technical data, provides scientific opinions and technical assistance, and coordinates European networking. The ECDC is also working with the Commission and MS to become the EU point of reference for the surveillance of communicable diseases; it is assessing and assisting MS in order to strengthen national surveillance capacities.
The surveillance activities undertaken by the ECDC will be very relevant in the case of a public health threat which requires IHR notification. The ECDC can also assist MS in national IHR implementation.
The ECDC is currently preparing guidelines for threat detection and assessment in the EU which would be useful in the context of IHR Annex I on Core Capacity Requirements.
The ECDC could provide guidance on how to use IHR Annex 2, which sets out the decision instrument for determining which events may constitute a PHEIC.
The ECDC could facilitate exchange of information and best practice between EU MS, possibly by hosting IHR meetings or workshops.
The ECDC can mobilise expertise, by sending epidemiological experts to affected areas within the EU and neighbouring countries, to help in risk assessment and advise on the most effective public health response. This complements existing WHO mechanisms for international assistance. Collaboration between ECDC and WHO is already working well and will be developed further in order to ensure maximum efficiency within the EU and wider Europe.
Finally, given WHO’s proposed terms of reference for IHR NFPs[8], the Commission believes the ECDC could and should perform a complementary role at EU level. Formally, the IHR text says only that “Each State Party shall designate or establish a National IHR Focal Point”. In practice, however, since the ECDC will be operating the EWRS and collating risk assessment data, it will be performing many of the same functions at the EU level which are attributed to NFPs at national level.
The Commission proposes that the ECDC’s role in the IHR should be formalised, particularly regarding the collection of data on issues within its mandate. Its role should include the following elements referred to in WHO’s guide on IHR NFPs:
13. Remaining accessible at all times for communications with WHO IHR Contact Points (via e-mail, telephone, fax).
14. Information-sharing during unexpected or unusual public health events (IHR Article 7): providing WHO IHR Contact Points with information on events within the EU which may constitute a PHEIC (subject to MS’ agreement).
15. Consultation (Article 8): advising WHO of events within the EU which do not require IHR notification. This would generally be done by copying or forwarding EWRS messages to WHO, subject to MS’ agreement.
16. Other reports (Article 9): responding to WHO requests for consultations, or attempts to obtain verification of reports of events occurring within the EU; and informing WHO of any evidence of a public health risk identified outside the EU that may cause international disease spread.
17. Verification (Article 10): responding to WHO requests for verification of reports from sources other than notifications or consultations of events which may constitute a PHEIC allegedly occurring in the EU.
18. Provision of information by WHO (Article 11): receiving information, in confidence, concerning notifications, consultations and reports on potential PHEICs submitted to WHO under Articles 5 - 10 inclusive. WHO could send this information via the EWRS.
19. Disseminating information to, and consolidating input from, relevant sectors of the EU administration, including those responsible for surveillance and reporting, points of entry, public health, and other departments. The ECDC role should be limited to risk assessment. On risk management, the Commission would lead, particularly through the Directorates General for Health & Consumer Protection; Justice, Liberty & Security; and Energy & Transport.
5.4. Roster of Experts, Emergency & Review Committees
IHR Article 47 provides for a Roster of Experts, to be appointed by the WHO Director-General, and to include “one member at the request of each State Party and, where appropriate, experts proposed by relevant intergovernmental and regional economic integration organizations”. The Commission recently wrote to WHO to propose Commission and ECDC experts for the IHR roster.
IHR Articles 48 to 53 relate to the Emergency and Review Committees. The Emergency Committee will advise WHO on whether a particular event constitutes a PHEIC, and will also be asked for its views on the appropriate policy response, by means of WHO temporary recommendations. The Review Committee will advise WHO on reservations, standing recommendations, technical recommendations on possible amendments to the IHR, and the general functioning of the IHR.
Both committees will be convened on an ad hoc basis at the request of WHO, and composed of experts selected from the IHR Roster, on the basis of expertise, experience, and equitable geographical representation. At least one member of the Emergency Committee should be an expert from the state in which the event arises.
In the event of a potential PHEIC in EU territory, it would be appropriate for WHO to invite the Commission and / or ECDC expert on the IHR roster to form part of the Emergency Committee .
6. RESTRICTIONS ON POLICY RESPONSES AFFECTING INTERNATIONAL TRAFFIC
IHR Article 43 makes clear that while states may take their own measures which achieve the same or greater level of health protection as WHO recommendations, “such measures shall not be more restrictive of international traffic and not more invasive or intrusive to persons than reasonably available alternatives that would achieve the appropriate level of health protection”.
The IHR places further restrictions on national measures which “significantly interfere” with international traffic. This is defined as refusing, or delaying by more than 24 hours, entry or departure of international travellers, baggage or cargo - eg by closing a border or imposing quarantine. In such cases states are required to demonstrate the scientific information and public health rationale behind the measure.
Any state impacted by such a measure may also request the implementing party to consult with it. If disputes were to arise over measures restricting trade, the World Trade Organization might be called upon to decide the dispute.
6.1. Border measures
A key objective of the revised IHR, particularly Part V, is to balance the need for restrictions on arbitrary border measures with the right of states to carry out necessary checks on travellers.
European law further restricts unilateral measures: at intra-Community borders, through the Schengen Borders Code[9]; and at all borders, through Directive 2004/38 on free movement[10], and the European Convention on Human Rights[11].
IHR Article 31 permits States Parties to require medical examination, vaccination or other prophylaxis of travellers as a condition of entry. This is subject to certain conditions, including that any examination is the least invasive and intrusive necessary to achieve the public health objective.
Under Directive 2004/38, MS may deny entry of EU citizens and their family members if they are considered a threat to public health, but only if this is proportionate and meets strict material and procedural safeguards. Under the Schengen Borders Code, third-country nationals may also be refused entry if considered a threat to public health. In order to define the notion of "threat to public health", both documents refer to the relevant instruments of the WHO.
While IHR Article 43 allows States Parties to take their own measures in response to international health emergencies, subject to certain conditions, in the EU border sures are a matter of Community competence which require EU coordination. Furthermore, as set out in the Network Decision 2119/1998, where MS intend to adopt measures for the control of communicable diseases, they must inform, and where possible consult, other MS and the Commission in advance. This is an important issue which requires discussion in Council.
6.2. Contact tracing
Under IHR Article 23, states may require travellers to provide information about their destination so that they can be contacted for public health purposes. There is currently no standard international approach to contact tracing, but provided the rules on the protection of personal data are fully complied with, it may be a useful policy tool for public health.
The Commission is working with the aviation industry and Directors General for Civil Aviation to discuss a possible EU approach to contact tracing.
This could be based on a standard passenger locator card to be filled in by passengers on board; WHO are already working on a template for use during PHEICs.
6.3. Particular issues in relation to pandemic flu
The measures to control international traffic described above are traditional health policy tools, addressed under IHR Part V, but their effectiveness is questionable in relation to flu, which can be transmitted before symptoms appear, and which spreads very quickly in the pandemic phase. National and local measures for social distancing, such as school closures and internal travel restrictions, may be more effective, as well as being more practical and less costly.
According to ECDC advice, screening international passengers on departure or arrival would be of very limited use in a flu pandemic, except in the early phase, when WHO has indicated exit screening might be worth considering. Still in order to be practical and cost-effective, this kind of border measure would benefit from policy coordination between countries of arrival and departure.
7. OPERATIONAL CONCLUSIONS
This Communication proposes a number of working practices for EU implementation of the IHR. Taking into account the views of the European Parliament and Council, the Commission will develop these proposals further, working with Member States and ECDC.
Specifically, the Commission will:
- develop coordinated EU positions on any IHR reservations from third countries, for discussion in Council;
- draft, negotiate and sign a Memorandum of Understanding on the IHR between the Community and the World Health Organization, clarifying the role of EWRS and ECDC in IHR implementation;
- at the request of MS, offer any clarifications necessary on issues of IHR legal competence.
The ECDC will:
- prepare guidelines for threat detection and assessment in the EU, which can be applied in the context of IHR Annex I on Core Capacity Requirements;
- if desired, develop guidance on how to use IHR Annex 2, which sets out the decision instrument for determining which events may constitute a PHEIC;
- facilitate exchange of information and best practice between EU MS on IHR implementation, including by hosting specific IHR workshops if desired.
The IHR sets implementation deadlines for States Parties to assess and develop core capacities to meet its requirements in relation to surveillance and response. Within five years of entry into force of the IHR in June 2007, States Parties are required to have developed and implemented plans to ensure that these core capacities are present and functioning within their territories. The Commission believes therefore that it would be useful to review progress with IHR implementation by 2012.
ANNEX – Summary and List of Acronyms
BLANK LINE.
Reservations: The Commission is satisfied there is no need for any IHR reservations on issues of EC competence. No MS has yet identified a need for reservations on issues of national competence. In the event that any reservations are required, EU coordination will be necessary. EU coordination will also be needed in reaction to any third country reservations.
Voluntary early application. It is important that the same IHR aspects are implemented to the same timetable across EU Member States. This will require coordination at EU level.
Existing EU networks, including EWRS. The Commission proposes four IHR working practices: nominating the same national focal point for EWRS as for IHR; simultaneously informing EWRS and WHO about public health events on their territory which do not constitute a PHEIC; informing the EU communicable disease network in advance of making a formal IHR notification of an event which may constitute a PHEIC; and using EWRS and/or the Health Security Committee to help coordinate risk management and response prior to communicating with WHO.
Role of the ECDC. The ECDC are already preparing guidelines on mapping national surveillance capacity, which should be useful in the context of IHR Annex 1. If desired, they could also develop guidance on how to use the decision instrument in IHR Annex 2, and facilitate exchange of information on IHR implementation, possibly by hosting meetings or workshops. The ECDC’s role in relation to the IHR should be formalised, to mirror certain aspects of the role of NFPs, including the receipt of information from WHO on notifications and consultations (via EWRS).
IHR Roster of Experts, Emergency & Review Committees. The Commission recently proposed Commission and ECDC experts to the WHO, to be appointed to the IHR roster. In the event of a potential PHEIC being notified by an EU MS, it would be appropriate for the Commission and/or ECDC expert to be selected to join the Emergency Committee.
Memorandum of Understanding between the Community & WHO. On the basis of the proposals on working practices set out above, it is desirable to prepare a memorandum of understanding between the Community and WHO to ensure that arrangements in relation to the IHR are properly defined. The Commission would be responsible for drafting, negotiating and signing such a memorandum.
IHR limits on unilateral policy responses, including border measures. The IHR clarifies that “such measures shall not be more restrictive of international traffic and not more invasive or intrusive to persons than reasonably available alternatives that would achieve the appropriate level of health protection”. Border measures are a matter of Community competence which require EU coordination. Furthermore, as set out in the Network Decision 2119/1998, where MS intend to adopt measures for the control of communicable diseases, they must inform, and where possible consult, other MS and the Commission in advance.
Particular issues in relation to pandemic flu. Measures at international borders are traditional health policy tools, but their effectiveness is questionable in relation to flu, which can be transmitted before symptoms appear, and spreads very quickly in the pandemic phase.
EC: European Community; ECDC : European Centre for Disease Prevention and Control; EP : European Parliament; EU : European Union; EWRS : Early Warning & Response System; IHR : International Health Regulations; MS : Member State; NFP: National Focal Point; PHEIC : Public Health Emergency of International Concern; WHO : World Health Organization; WHA : World Health Assembly.
[1] Report of WHA 58, with IHR text.
[2] Resolution WHA59.2.
[3] Established by Regulation 851/2004 of the EP and Council.
[4] Established by Council and Parliament Decision 2119/98 and Commission Decision 2000/57/EC.
[5] For more information see these Frequently Asked Questions.
[6] The EC is the first pillar of the EU, and is the part which has legal identity. Pillars 2 and 3 relate to political cooperation in foreign policy and justice.
[7] Directive 95/46/EC of the EP and Council, on data protection.
[8] WHO National IHR Focal Point Guide of July 2006, on the designation and role of NFPs.
[9] Regulation 562/2006 of the EP and Council, on the Schengen Borders Code.
[10] Directive 2004/38 of the EP and Council, on the right of EU citizens and family members to move and reside freely in the EU.
[11] Protocol 4 to the Convention for the Protection of Human Rights and Fundamental Freedoms.
