Resolution of the Council and of the Representatives of the Governments of the Member States, meeting within the Council, on addressing the concerns of young people in Europe — implementing the European Pact for Youth and promoting active citizenship
(2005/C 292/03)
THE COUNCIL OF THE EUROPEAN UNION AND THE REPRESENTATIVES OF THE GOVERNMENTS OF THE MEMBER STATES, MEETING WITHIN THE COUNCIL,
RECALL:
The European Commission's White Paper of November 2001 entitled "A new impetus for European youth" [1], which was endorsed by the Council in its conclusions of 14 February 2002 [2] and its Resolution of 27 June 2002 [3] establishing a framework for European cooperation in the youth field.
The European Pact for Youth, adopted by the Spring European Council on 22 and 23 March 2005, as one of the instruments contributing to the achievement of the Lisbon objectives of growth and jobs. The Pact focuses on three areas: Employment, integration and social advancement; Education, training and mobility; Reconciliation of working life and family life [4].
WELCOME the Commission's communication on European policies concerning youth, "Addressing the concerns of young people in Europe — implementing the European Youth Pact and promoting active citizenship" [5].
EMPHASISE THAT:
1. The active citizenship of young people is key to building democratic and inclusive societies at all levels.
2. Young people and their organisations have a role to play in the development of the European Union, including contributing to the Lisbon goals of boosting jobs and growth.
3. To help achieve the Lisbon employment goals, young people need to be fully integrated into society and therefore:
- supported in accessing the labour market and encouraged to develop creativity and entrepreneurial skills;
- equipped with knowledge, skills and competences through high-quality, relevant education, training and mobility experiences in the formal as well as the non-formal sector;
- able to reconcile working life and family life.
4. The Community instruments that support the Lisbon partnership for growth and jobs, such as the "Education and Training 2010" Work Programme and the Employment and Social Inclusion strategies, have a key role to play in achieving the aims of the European Pact for Youth.
5. The specific needs of young people should be taken into account by policymakers at local, regional, national and European level, so that the youth dimension in all relevant policies is strengthened.
6. Young people and their organisations should be consulted on and closely involved in the development, implementation and follow-up of policy actions affecting them, thereby also contributing to a greater sense of active citizenship among young people.
7. Those active in youth work and youth organisations need to be properly trained and qualified so as to provide effective support for young people.
NOTE THAT the European framework for cooperation in the youth field now comprises three strands:
- supporting the active citizenship of young people, through the Youth Open Method of Coordination,
- the European Pact for Youth, which highlights youth issues in core areas of the Lisbon partnership for growth and jobs,
- including a youth dimension in other relevant European policies.
The Youth Programme and the forthcoming Youth in Action Programme are complementary to this framework.
AGREE THAT:
1. Work should continue on supporting active citizenship through the priorities agreed under the Youth Open Method of Coordination: participation, information, voluntary activities and a better knowledge of youth, including improving knowledge of the issues raised in the European Pact for Youth.
2. The aims of the European Pact for Youth will be pursued within the Lisbon partnership for jobs and growth.
3. When developing a youth dimension in other relevant European policies, priority will be given to anti-discrimination, healthy lifestyles, including sport, and research on youth issues.
4. When implementing the framework for cooperation:
- complementarity and coherence between the three strands should be ensured;
- young people and their organisations should be consulted through the European Youth Forum, national youth councils or similar bodies and dialogue with non-organised young people should also be developed;
- the mechanisms and existing timetables for each strand should be respected;
- all discrimination based on sex, race or ethnic origin, religion or belief, disability or sexual orientation should be combated.
INVITE THE MEMBER STATES TO:
1. continue to implement the common objectives agreed under the Youth Open Method of Coordination;
2. ensure effective follow-up of the European Pact for Youth when implementing the Lisbon Strategy, for example by setting measurable objectives;
3. develop structured dialogue with young people and their organisations at national, regional and local level on policy actions affecting them, with the involvement of researchers in the youth field.
INVITE THE COMMISSION TO:
1. develop structured dialogue with young people at European level on policy actions affecting them, for example by making innovative use of information technology and holding regular conferences between young people, their organisations, researchers in the youth field and policymakers;
2. develop, promote and facilitate access to the European Youth Portal, EURES, PLOTEUS and EURODESK in order to help young people make the most of opportunities to work, volunteer and study abroad;
3. ensure effective follow-up of the European Pact for Youth when implementing the Lisbon Strategy.
CALL ON THE MEMBER STATES AND THE COMMISSION TO:
1. encourage, for young people and those active in youth work and youth organisations, the recognition of non-formal and informal learning, for example through developing a "Youthpass" and considering its inclusion in Europass, and consider the validation of such learning, whilst taking account of national situations and respecting Member States' competences;
2. identify obstacles to and exchange, develop and apply good practice concerning young people's mobility in order to make it easier for them to work, volunteer, train and study throughout the European Union and further afield;
3. make the best possible use of the opportunities offered by Community and Member State policies, programmes and other instruments to enhance young people's active citizenship, social inclusion, employability and educational attainment;
4. evaluate the framework for European cooperation in the youth field in 2009.
[1] 14441/01 – COM(2001) 681 final.
[2] OJ C 119, 22.5.2002, p. 6.
[3] OJ C 168, 13.7.2002, pp. 2-5.
[4] 7619/1/05. Conclusion 37.
[5] 9679/05 – COM(2005) 206 final.
--------------------------------------------------
