Uniform application of the Combined Nomenclature (CN)
(Classification of goods)
(2006/C 106/02)
Explanatory notes adopted in accordance with the procedure defined in Article 10(1) of Council Regulation (EEC) No 2658/87 of 23 July 1987 on the tariff and statistical nomenclature and on the Common Customs Tariff [1]
The explanatory notes to the Combined Nomenclature of the European Communities [2] shall be amended as follows:
On page 338:
8528 "According to General rule 3(c) for the interpretation of the Combined Nomenclature, video surveillance systems, consisting of a limited number of television cameras and one video monitor, fall within this heading when put up in a set for retail sale.
The components of such video surveillance systems are to be classified separately when the system is not put up in a set for retail sale (See HS Explantory Notes to Section XVI (VII))."
On page 339:
8529 "This heading does not include tripods for use with cameras of heading 8525 or Chapter 90 (classification according to constituent material)."
Replace CN subheadings 85299010 to 85299088 and the corresponding text by the following:
"85299040 to 85299095 1. dials;
2. tuning blocks;
3. diaphragms for television cameras;
4. so-called PAL-Secam adapters. These are decoder boards (printed circuit boards mounted with electrical components) for subsequent incorporation in television receivers designed for the reception of PAL signals, so as to make these sets suitable for the dual reception of PAL/Secam signals.
These subheadings do not include waveguides (classification of the tubes according to constituent material)."
On page 363:
CHAPTER 90
OPTICAL, PHOTOGRAPHIC, CINEMATOGRAPHIC, MEASURING, CHECKING, PRECISION, MEDICAL OR SURGICAL INSTRUMENTS AND APPARATUS; PARTS AND ACCESSORIES THEREOF
Insert the following text:
"General
This Chapter only includes tripods which are solely or principally for use with apparatus of this Chapter (See the HS Explanatory Notes to this Chapter (General, III)).
Tripods for use with cameras of this Chapter and heading 8525 are to be classified according to the constituent material."
[1] OJ L 256, 7.9.1987, p. 1. Regulation as last amended by Council Regulation (EC) No 486/2006 (OJ L 88, 25.3.2006, p. 1).
[2] OJ C 50, 28.2.2006, p. 1.
--------------------------------------------------
