Commission Decision
of 12 October 2006
amending, for the purposes of adapting to technical progress, the Annex to Directive 2002/95/EC of the European Parliament and of the Council as regards exemptions for applications of lead in crystal glass
(notified under document number C(2006) 4789)
(Text with EEA relevance)
(2006/690/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Directive 2002/95/EC of the European Parliament and of the Council of 27 January 2003 on the restriction of the use of certain hazardous substances in electrical and electronic equipment [1], and in particular Article 5(1)(b) thereof,
Whereas:
(1) Directive 2002/95/EC requires the Commission to evaluate certain hazardous substances prohibited pursuant to Article 4(1) of that Directive.
(2) Crystal glass has been progressively used for decorative purposes on electrical and electronic equipment. Since Council Directive 69/493/EEC of 15 December 1969 on the approximation of the laws of the Member States relating to crystal glass [2] prescribes the amount of lead to be present in crystal glass and the substitution of lead in crystal glass is therefore technically impracticable, the use of this hazardous substance in specific materials and components covered by that Directive is unavoidable. Those materials and components should be therefore exempted from the prohibition.
(3) Some exemptions from the prohibition for certain specific materials or components should be limited in their scope, in order to achieve a gradual phase-out of hazardous substances in electrical and electronic equipment, given that the use of those substances in such applications will become avoidable.
(4) Pursuant to Article 5(1)(c) of Directive 2002/95/EC each exemption listed in the Annex must be subject to a review at least every four years or four years after an item is added to the list.
(5) Directive 2002/95/EC should therefore be amended accordingly.
(6) Pursuant to Article 5(2) of Directive 2002/95/EC, the Commission has consulted the relevant parties.
(7) The measures provided for in this Decision are in accordance with the opinion of the Committee established by Article 18 of Directive 2006/12/EC of the European Parliament and of the Council [3],
HAS ADOPTED THIS DECISION:
Article 1
In the Annex to Directive 2002/95/EC the following point 29 is added:
"29. Lead bound in crystal glass as defined in Annex I (Categories 1, 2, 3 and 4) of Council Directive 69/493/EEC [4].
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 12 October 2006.
For the Commission
Stavros Dimas
Member of the Commission
[1] OJ L 37, 13.2.2003, p. 19. Directive as last amended by Commission Decision 2006/310/EC (OJ L 115, 28.4.2006, p. 38).
[2] OJ L 326, 29.12.1969, p. 36. Directive as last amended by 2003 Act of Accession.
[3] OJ L 114, 27.4.2006, p. 9.
[4] OJ L 326, 29.12.1969, p. 36. Directive as last amended by 2003 Act of Accession."
--------------------------------------------------
