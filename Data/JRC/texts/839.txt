ANNEX
Agreement
between the European Community and the Republic of Turkey concerning the participation of the Republic of Turkey in the European Environment Agency and the European environment information and observation network
The EUROPEAN COMMUNITY,
of the one part,
and the REPUBLIC OF TURKEY, hereinafter referred to as "Turkey",
of the other part,
TAKING INTO ACCOUNT Turkeys's application for participation in the European Environment Agency already before accession,
RECALLING that the Luxembourg European Council (December 1997) made participation in the Community programmes and agencies a way of stepping up the pre-accession strategy,
TAKING INTO ACCOUNT Council Regulation (EEC) No 1210/90, as amended by Council Regulation (EC) No 933/1999, on the establishment of the European Environment Agency and the European environment information and observation network,
RECOGNISING the fact that Turkey's ultimate objective is to become a member of the European Union, and that the participation in the European Environment Agency will help the Turkey to achieve this objective,
HAVE AGREED AS FOLLOWS:
Article 1
Turkey shall participate fully in the European Environment Agency, hereinafter referred to as the "Agency", and the European environment information and observation network (Eionet), as set up by Council Regulation (EEC) No 1210/90, as amended by Regulation (EC) No 933/1999.
Article 2
Turkey shall contribute financially to the activities referred to under Article 1 (Agency and Eionet) in accordance with the following:
- the contribution will progressively increase in a three-year period during which the activities will be phased in Turkey. The financial contributions required are:
- year 1: EUR 2033000
- year 2: EUR 2596000
- year 3: EUR 3127000.
From the fourth year of the entry into force of this Agreement Turkey has to bear the full cost of its financial contribution, i.e. EUR 3127000,
- the possible financial support from Community assistance programmes will be agreed separately according to the relevant Community programme.
The further terms and conditions on the financial contribution of Turkey are set out in Annex I to this Agreement, which shall form an integral part to it.
Article 3
Turkey shall participate fully, without the right to vote, in the Agency Management Board and shall be associated with the work of the Scientific Committee of the Agency.
Article 4
Turkey shall, within three months of the entry into force of this Agreement, inform the Agency of the main component elements of its national information networks as set out in Article 4(2) of Regulation (EEC) No 1210/90 as amended by Regulation (EC) No 933/1999.
Article 5
Turkey shall in particular designate from among the institutions referred to in Article 4 or from among other organisations established in its territory a "national focal point" for coordinating and/or transmitting the information to be supplied at national level to the Agency and to the institutions or bodies forming part of the Eionet, including the topic centres referred to under Article 6.
Article 6
Turkey may also, within the period laid down in Article 4, identify the institutions or other organisations established in its territory, which could be specifically entrusted with the task of cooperating with the Agency as regards certain topics of particular interest. An institution thus identified should be in a position to conclude an agreement with the Agency to act as a topic centre of the network for specific tasks. These centres shall cooperate with other institutions, which form part of the network.
Article 7
Within three months of receiving the information referred to in Articles 4, 5 and 6, the Management Board of the Agency shall review the main elements of the network to take account of the participation of Turkey.
Article 8
Turkey should provide data according to the obligations and practice established in the Agency work.
Article 9
The Agency may agree with the institutions or bodies designated by Turkey and which form part of the network, as referred to in Articles 4, 5 and 6, on the necessary arrangements, in particular contracts, for successfully carrying out the tasks which it may entrust to them.
Article 10
Environmental data supplied to or emanating from the Agency may be published and shall be made accessible to the public, provided that confidential information is afforded the same degree of protection in Turkey as it is afforded within the Community.
Article 11
The Agency shall have legal personality in Turkey. It shall enjoy in Turkey the most extensive legal capacity accorded to legal persons under its laws.
Article 12
Turkey shall apply to the Agency the Protocol of Privileges and Immunities of the European Communities, which, as Annex II to this Agreement, shall form an integral part thereof.
Article 13
By way of derogation from Article 12(2)(a) of Council Regulation (EEC, Euratom, ECSC) No 259/68 on Staff Regulations of Officials and the Conditions of Employment of Other Servants of the European Communities, nationals of Turkey enjoying their full rights as citizens may be engaged under contract by the Executive Director of the Agency.
Article 14
The Parties shall take any general or specific measures required to fulfil their obligations under this Agreement. They shall see to it that the objectives set out in this Agreement are attained.
Article 15
This Agreement is concluded for an unlimited period until Turkey shall become a member of European Union. Either Party may denounce this Agreement by notifying the other Party. This Agreement shall cease to apply six months after the delivery date of such notification to the other Party.
Article 16
This Agreement shall apply, on the one hand, to the territories in which the Treaties establishing the European Community, the European Atomic Energy Community and the European Coal and Steel Community are applied and under the conditions laid down in those Treaties and, on the other hand, to the territory of Turkey.
Article 17
This Agreement shall be approved by the Parties in accordance with their own procedures. This Agreement shall enter into force on the first day of the second month following the date on which the last Party has delivered the notification to the first Party that its procedures have been completed.
Article 18
This Agreement shall be drawn up in two original copies in the Danish, Dutch, English, Finnish, French, German, Italian, Spanish, Swedish, Greek, Portuguese and Turkish languages, each of these texts being equally authentic.
ANNEX I
FINANCIAL CONTRIBUTION OF TURKEY TO THE EUROPEAN ENVIRONMENT AGENCY
1. The financial contribution to be paid by Turkey to the budget of the European Union to participate in the European Environment Agency will be in:
- year 1 of participation: EUR 2033000
- year 2 of participation: EUR 2596000
- year 3 of participation: EUR 3127000.
From the fourth year Turkey has to bear the full cost of its financial contribution, i.e. EUR 3127000.
2. The possible financial support from Community assistance programmes will be agreed separately according to the relevant Community programme.
3. The contribution of Turkey will be managed in accordance with the Financial Regulation applicable to the general budget of the European Union.
Travel costs and subsistence costs incurred by representatives and experts of Turkey for the purposes of taking part in the European Environment Agency activities or meetings related to the implementation of the Agency's work programme shall be reimbursed by the European Environment Agency on the same basis as and in accordance with the procedures currently in force for the Member States of the European Union.
4. After the entry into force of this Agreement and at the beginning of each following year, the Commission will send to Turkey a call for funds corresponding to its contribution to the European Environment Agency under this Agreement. For the first calendar year of its participation Turkey will pay a contribution calculated from the date of participation to the end of the year on a pro rata basis. For the following years the contribution will be in accordance with this Agreement.
5. This contribution shall be expressed in euro and paid into a euro bank account of the Commission of the European Communities.
6. Turkey will pay its contribution according to the call for funds:
- for its own part by 1 May provided that the call for funds is sent by the Commission before 1 April, or at the latest in a period of 30 days after the call for funds is sent.
7. Any delay in the payment of the contribution shall give rise to the payment of interest by Turkey on the outstanding amount from the due date. The interest rate corresponds to the rate applied by the European Central Bank, on the due date, for its operations in euro, increased by 1,5 percentage points.
ANNEX II
PROTOCOL ON THE PRIVILEGES AND IMMUNITIES OF THE EUROPEAN COMMUNITIES
THE HIGH CONTRACTING PARTIES,
CONSIDERING that, in accordance with Article 28 of the Treaty establishing a Single Council and a Single Commission of the European Communities, these Communities and the European Investment Bank shall enjoy in the territories of the Member States such privileges and immunities as are necessary for the performance of their tasks,
HAVE AGREED on the following provisions, which shall be annexed to this Treaty.
CHAPTER I
PROPERTY, FUNDS, ASSETS AND OPERATIONS OF THE EUROPEAN COMMUNITIES
Article 1
The premises and buildings of the Communities shall be inviolable. They shall be exempt from search, requisition, confiscation or expropriation. The property and assets of the Communities shall not be the subject of any administrative or legal measure of constraint without the authorisation of the Court of Justice.
Article 2
The archives of the Communities shall be inviolable.
Article 3
The Communities, their assets, revenues and other property shall be exempt from all direct taxes.
The Governments of the Member States shall, wherever possible, take the appropriate measures to remit or refund the amount of indirect taxes or sales taxes included in the price of movable or immovable property, where the Communities make, for their official use, substantial purchases the price of which includes taxes of this kind. These provisions shall not be applied, however, so as to have the effect of distorting competition within the Communities.
No exemption shall be granted in respect of taxes and dues which amount merely to charges for public utility services.
Article 4
The Communities shall be exempt from all customs duties, prohibitions and restrictions on imports and exports in respect of articles intended for their official use: articles so imported shall not be disposed of, whether or not in return for payment, in the territory of the country into which they have been imported, except under conditions approved by the government of that country.
The Communities shall also be exempt from any customs duties and any prohibitions and restrictions on import and exports in respect of their publications.
Article 5
The European Coal and Steel Community may hold currency of any kind and operate accounts in any currency.
CHAPTER II
COMMUNICATIONS AND LAISSEZ-PASSER
Article 6
For their official communications and the transmission of all their documents, the institutions of the Communities shall enjoy in the territory of each Member State the treatment accorded by that State to diplomatic missions.
Official correspondence and other official communications of the institutions of the Communities shall not be subject to censorship.
Article 7
1. Laissez-passer in a form to be prescribed by the Council, which shall be recognised as valid travel documents by the authorities of the Member States, may be issued to members and servants of the institutions of the Communities by the Presidents of these institutions. These laissez-passer shall be issued to officials and other servants under conditions laid down in the Staff Regulations of Officials and the Conditions of Employment of Other Servants of the Communities.
The Commission may conclude agreements for these laissez-passer to be recognised as valid travel documents within the territory of non-member countries.
2. The provisions of Article 6 of the Protocol on the privileges and immunities of the European Coal and Steel Community shall, however, remain applicable to members and servants of the institutions who are at the date of entry into force of this Treaty in possession of the laissez-passer provided for in that Article, until the provisions of paragraph 1 of this Article are applied.
CHAPTER III
MEMBERS OF THE EUROPEAN PARLIAMENT
Article 8
No administrative or other restriction shall be imposed on the free movement of Members of the European Parliament travelling to or from the place of meeting of the European Parliament.
Members of the European Parliament shall, in respect of customs and exchange control, be accorded:
(a) by their own government, the same facilities as those accorded to senior officials travelling abroad on temporary official missions;
(b) by the Government of other Member States, the same facilities as those accorded to representatives of foreign governments on temporary official missions.
Article 9
Members of the European Parliament shall not be subject to any form of inquiry, detention or legal proceedings in respect of opinions expressed or votes cast by them in the performance of their duties.
Article 10
During the sessions of the European Parliament, its Members shall enjoy:
(a) in the territory of their own State, the immunities accorded to members of their parliament;
(b) in the territory of any other Member State, immunity from any measure of detention and from legal proceedings.
Immunity shall likewise apply to Members while they are travelling to and from the place of meeting of the European Parliament.
Immunity cannot be claimed when a Member is found in the act of committing an offence and shall not prevent the European Parliament from exercising its right to waive the immunity of one of its Members.
CHAPTER IV
REPRESENTATIVES OF MEMBER STATES TAKING PART IN THE WORK OF THE INSTITUTIONS OF THE EUROPEAN COMMUNITIES
Article 11
Representatives of Member States taking part in the work of the institutions of the Communities, their advisers and technical experts shall, in the performance of their duties and during their travel to and from the place of meeting, enjoy the customary privileges, immunities and facilities.
This Article shall also apply to members of the advisory bodies of the Communities.
CHAPTER V
OFFICIALS AND OTHER SERVANTS OF THE EUROPEAN COMMUNITIES
Article 12
In the territory of each Member State and whatever their nationality, officials and other servants of the Communities shall:
(a) subject to the provisions of the Treaties relating, on the one hand, to the rules on the liability of officials and other servants towards the Communities and, on the other hand, to the jurisdiction of the Court in disputes between the Communities and their officials and other servants, be immune from legal proceedings in respect of acts performed by them in their official capacity, including their words spoken or written. They shall continue to enjoy this immunity after they have ceased to hold office;
(b) together with their spouses and dependent members of their families, not be subject to immigration restrictions or to formalities for the registration of aliens;
(c) in respect of currency or exchange regulations, be accorded the same facilities as are customarily accorded to officials of international organisations;
(d) enjoy the right to import free of duty their furniture and effects at the time of first taking up their post in the country concerned, and the right to re-export free of duty their furniture and effects, on termination of their duties in that country, subject in either case to the conditions considered to be necessary by the government of the country in which this right is exercised;
(e) have the right to import free of duty a motor car for their personal use, acquired either in the country of their last residence or in the country of which they are nationals on the terms ruling in the home market in that country, and to re-export it free of duty, subject in either case to the conditions considered to be necessary by the government of the country concerned.
Article 13
Officials and other servants of the Communities shall be liable to a tax for the benefit of the Communities on salaries, wages and emoluments paid to them by the Communities, in accordance with the conditions and procedure laid down by the Council, acting on a proposal from the Commission.
They shall be exempt from national taxes on salaries, wages and emoluments paid by the Communities.
Article 14
In the application of income tax, wealth tax and death duties and in the application of conventions on the avoidance of double taxation concluded between Member States of the Communities, officials and other servants of the Communities who, solely by reason of the performance of their duties in the service of the Communities, establish their residence in the territory of a Member State other than their country of domicile for tax purposes at the time of entering the service of the Communities, shall be considered, both in the country of their actual residence and in the country of domicile for tax purposes, as having maintained their domicile in the latter country provided that it is a member of the Communities. This provision shall also apply to a spouse, to the extent that the latter is not separately engaged in a gainful occupation, and to children dependent on and in the care of the persons referred to in this Article.
Movable property belonging to persons referred to in the preceding paragraph and situated in the territory of the country where they are staying shall be exempt from death duties in that country; such property shall, for the assessment of such duty, be considered as being in the country of domicile for tax purposes, subject to the rights of non-member countries and to the possible application of provisions of international conventions on double taxation.
Any domicile acquired solely by reason of the performance of duties in the service of other international organisations shall not be taken into consideration in applying the provisions of this Article.
Article 15
The Council shall, acting unanimously on a proposal from the Commission, lay down the scheme of social security benefits for officials and other servants of the Communities.
Article 16
The Council shall, acting on a proposal from the Commission and after consulting the other institutions concerned, determine the categories of officials and other servants of the Communities to whom the provisions of Article 12, the second paragraph of Article 13, and Article 14 shall apply, in whole or in part.
The names, grades and addresses of officials and other servants included in such categories shall be communicated periodically to the Governments of the Member States.
CHAPTER VI
PRIVILEGES AND IMMUNITIES OF MISSIONS OF NON-MEMBER COUNTRIES ACCREDITED TO THE EUROPEAN COMMUNITIES
Article 17
The Member State in whose territory the Communities have their seat shall accord the customary diplomatic immunities and privileges to missions of non-member countries accredited to the Communities.
CHAPTER VII
GENERAL PROVISIONS
Article 18
Privileges, immunities and facilities shall be accorded to officials and other servants of the Communities solely in the interests of the Communities.
Each institution of the Communities shall be required to waive the immunity accorded to an official or other servant wherever that institution considers that the waiver of such immunity is not contrary to the interests of the Communities.
Article 19
The institutions of the Communities shall, for the purpose of applying this Protocol, cooperate with the responsible authorities of the Member States concerned.
Article 20
Articles 12 to 15 and Article 18 shall apply to Members of the Commission.
Article 21
Articles 12 to 15 and Article 18 shall apply to the Judges, the Advocates General, the Registrar and the Assistant Rapporteurs of the Court of Justice, without prejudice to the provisions of Article 3 of the Protocols on the Statute of the Court of Justice concerning immunity from legal proceedings of Judges and Advocates General.
Article 22
This Protocol shall also apply to the European Investment Bank, to the members of its organs, to its staff and to the representatives of the Member States taking part in its activities, without prejudice to the provisions of the Protocol on the Statute of the Bank.
The European Investment Bank shall in addition be exempt from any form of taxation or imposition of a like nature on the occasion of any increase in its capital and from the various formalities which may be connected therewith in the State where the Bank has its seat. Similarly, its dissolution or liquidation shall not give rise to any imposition. Finally, the activities of the Bank and of its organs carried on in accordance with its Statute shall not be subject to any turnover tax.
Article 23
This Protocol shall also apply to the European Central Bank, to the members of its organs and to its staff, without prejudice to the provisions of the Protocol on the Statute of the European System of Central Banks and the European Central Bank.
The European Central Bank shall, in addition, be exempt from any form of taxation or imposition of a like nature on the occasion of any increase in its capital and from the various formalities which may be connected therewith in the State where the Bank has its seat. The activities of the Bank and of its organs carried on in accordance with the Statute of the European System of Central Banks and of the European Central Bank shall not be subject to any turnover tax.
The above provisions shall also apply to the European Monetary Institute. Its dissolution or liquidation shall not give rise to any imposition.
IN WITNESS WHEREOF, the undersigned Plenipotentiaries have signed this Protocol.
Done at Brussels this eighth day of April in the year one thousand nine hundred and sixty-five.
