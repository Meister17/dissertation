Authorisation for State aid pursuant to Articles 87 and 88 of the EC Treaty
Cases where the Commission raises no objections
(2006/C 209/03)
(Text with EEA relevance)
Date of adoption : 24.5.2006
Member State : Spain (Catalonia)
Aid No : N 4/2006
Title : Programa Beatriz Pinós
Objective : Research and development
Legal basis : Resolución UNI/2429/2005, de 3 de agosto, por la que se aprueban las bases y se abre la convocatoria de ayudas posdoctorales dentro del programa Beatriu de Pinós (BP) 2005, DOGC número 4458 de 30 de agosto de 2005
Type of measure : Aid scheme
Form of aid : Direct grant
Budget : EUR 3453750
Maximum aid intensity : 75 %, 50 %, 25 %
Duration : 30.6.2008
Economic sectors : All sectors
Name and address of the granting authority Agencia de gestión de ayudas universitarios y de investigación (AGAUR)
Via Laietana, núm. 28
E-08003 Barcelona
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption : 13.3.2006
Member State : Germany — Saxony
Aid No : N 52/A/2006
Title : Pollution control and climate protection — prolongation and modification of an aid scheme
Objective : Protection of environment
Legal basis : Richtlinie des Sächsischen Staatsministeriums für Umwelt und Landwirtschaft über die Gewährung von Fördermitteln für Vorhaben des Immissions- und Klimaschutzes einschließlich der Nutzung erneuerbarer Energien im Freistaat Sachsen
Budget : EUR 15910000
Intensity or amount : Between 40 % and 50 % maximum of additional costs
Duration : Until 31 December 2008
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption : 13.3.2006
Member State : Sweden
Aid No : N 112/2004
Title : Energy and CO2 tax exemption for biofuels
Objective : Environment (Biofuel producers and fuel traders)
Legal basis : Lagen 1994:1776 om skatt på energi
Budget : EUR 129 million per year
Duration : until 31.12.2008
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption of the decision : 16.5.2006
Member State : Ireland
Aid No : N 149/2006
Title : Financing of M3 Clonee to North of Kells and Limerick tunnel
Objective : Transport
Legal basis : Irish Roads Act
Budget : No accurate estimate available; total construction cost: EUR 600 million/EUR 400 million
Duration : 30 to 45 years
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption : 24.1.2006
Member States : Lithuanian Republic
Aid No : N 294/2005
Title : Aid for development of biofuel production
Objective : Environmental protection, sectoral development
Legal basis - Lietuvos Respublikos biokuro ir bioalyvų įstatymas (Žin., 2004 Nr. 28-870),
- Biokuro gamybos ir naudojimo skatinimo 2004-2010 metais programa (Žin., 2004 Nr. 133-4786)
Budget : EUR 17,4 million (LTL 60 million) in the period 2006-2011
Duration : 31.12.2011
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption : 22.3.2006
Member State : Germany
Aid No : N 339/05
Title : Funding of the construction of the waste-fuelled power station MHKW Rothensee
Objective : Energy
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption : 22.2.2006
Member State : United Kingdom
Aid No : N 412/2005
Title : Extension and prolongation of Waste & Resources Action Programme
Objective : Environmental protection
Legal basis : Section 153 of the Environmental Projection Act 1990 and the Financial Assistance for Environmental Purposes (No 2) Order 2000 (SI 2000:2211)
Budget : EUR 28 million per year
Intensity or amount : Up to 55 % for SMEs in an Art 87(3)(a) region
Duration : Until 31.12.2010
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption of the decision : 7.6.2006
Member State : Ireland
Aid No : N 478/2004
Title : State guarantee for capital borrowings by Coràs Iompair Eirann (CIÉ) for infrastructure investment
Objective : Transport
Legal basis : Transport Act 1964
Budget : EUR 800 million
Aid intensity or amount : Measure not constituting aid
Duration : 10 years
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption : 20.3.2006
Member State : Czech Republic
Aid No : N 478/2005
Title : Program COST
Objective : Research and development
Legal basis : Zákon č. 130/2002 Sb., o podpoře výzkumu a vývoje z veřejných prostředků, Nařízení vlády č. 461/2002 Sb., o účelové podpoře výzkumu a vývoje z veřejných prostředků
Type of measure : Aid scheme
Form of aid : Direct grant
Budget : CZK 719000000
Maximum aid intensity : 100 %
Duration : End date: 31.12.2012
Economic sectors : All sectors
Name and address of the granting authority : Ministerstvo školství, mládeže a tělovýchovy
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption : 20.3.2006
Member State : Czech Republic
Aid No : N 479/2005
Title : Program EUREKA
Objective : Research and development
Legal basis : Zákon č. 130/2002 Sb., o podpoře výzkumu a vývoje z veřejných prostředků, Nařízení vlády č. 461/2002 Sb., o účelové podpoře výzkumu a vývoje z veřejných prostředků
Type of measure : Aid scheme
Form of aid : Direct grant
Budget : CZK 749000000
Maximum aid intensity : 50 %
Duration : End date: 31.12.2012
Economic sectors : All sectors
Name and address of the granting authority : Ministerstvo školství, mládeže a tělovýchovy
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption : 20.3.2006
Member State : Czech Republic
Aid No : N 480/2005
Title : Program EUPRO
Objective : Research and development
Legal basis : Zákon č. 130/2002 Sb., o podpoře výzkumu a vývoje z veřejných prostředků, Nařízení vlády č. 461/2002 Sb., o účelové podpoře výzkumu a vývoje z veřejných prostředků
Type of measure : Aid scheme
Form of aid : Direct grant
Budget : CZK 445000000
Maximum aid intensity : 100 %
Duration : End date: 31.12.2012
Economic sectors : All sectors
Name and address of the granting authority : Ministerstvo školství, mládeže a tělovýchovy
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption : 20.3.2006
Member State : Czech Republic
Aid No : N 481/2005
Title : Program KONTAKT
Objective : Research and development
Legal basis : Zákon č. 130/2002 Sb., o podpoře výzkumu a vývoje z veřejných prostředků, Nařízení vlády č. 461/2002 Sb., o účelové podpoře výzkumu a vývoje z veřejných prostředků
Type of measure : Aid scheme
Form of aid : Direct grant
Budget : CZK 697000000
Maximum aid intensity : 100 %, 50 %
Duration : End date: 31.12.2012
Economic sectors : All sectors
Name and address of the granting authority : Ministerstvo školství, mládeže a tělovýchovy
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption : 20.3.2006
Member State : Czech Republic
Aid No : N 482/2005
Title : Program INGO
Objective : Research and development
Legal basis : Zákon č. 130/2002 Sb., o podpoře výzkumu a vývoje z veřejných prostředků, Nařízení vlády č. 461/2002 Sb., o účelové podpoře výzkumu a vývoje z veřejných prostředků
Type of measure : Aid scheme
Form of aid : Direct grant
Budget : CZK 823000000
Maximum aid intensity : 100 %
Duration : End date: 31.12.2012
Economic sectors : All sectors
Name and address of the granting authority : Ministerstvo školství, mládeže a tělovýchovy
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption of the decision : 16.5.2006
Member State : Federal Republic of Germany
Aid No : N 604/2005
Title : Public funding for bus operators in the rural district of Wittenberg
Objective : Public bus transport service
Legal basis : Personenbeförderungsgesetz; Regionalisierungsgesetz; Gesetz über den öffentlichen Personennahverkehr des Landes Sachsen-Anhalt; Landkreisordnung des Landes Sachsen-Anhalt; Nahverkehrsplan des Landkreises Wittenberg; Satzung zur Förderung eigenwirtschaftlicher Verkehrsleistungen im Landkreis Sachsen-Anhalt
Budget : EUR 1 million a year
Aid intensity or amount : This measure does not constitute aid
Duration : 2007-2015
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption : 4.4.2006
Member State : Ireland (Border Midlands and West region)
Aid No : N 639/2005
Title : Abbott Vascular Devices Ireland. R&D Capability Grant
Objective : Regional development. Research and development
Legal basis : Research Programme NN 34/87
Type of measure : Individual aid
Form of aid : Direct grant
Budget : EUR 14000000
Maximum aid intensity : 35 %
Duration : 2005-2007
Economic sectors : Limited to the manufacturing industry
Name and address of the granting authority Industrial Development Agency Ireland
Wilton Park House, Wilton Place
Dublin 2
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption : 4.7.2006
Member State : Belgium
Aid No : N 649/2005
Title : Mesures de dispense partielle de précompte professionnel en faveur de la R&D
Objective : Research and development
Legal basis : Loi relative au pacte de solidarité entre les générations (Moniteur belge du 30.12.2005 p. 57266-57300)
Type of measure : Aid scheme
Form of aid : Tax advantage
Budget : EUR 116000000
Maximum aid intensity : 18 %
Duration :
Unlimited
Duration (end date): "Young Innovative Company": 1.7.2016
Economic sectors : All sectors
Name and address of the granting authority Ministre de l'Économie, de l'Énergie, du Commerce extérieur et de la Politique scientifique
Rue Bréderode, 9
B-1000 Bruxelles
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption of the decision : 21.4.2005
Member State : Italy
Aid No : NN 81/2002
Title : Compensation for traders and other operators in the fisheries sector following the events in Kosovo (1999)
Objective : Compensation for processing and marketing undertakings affected by those events
Legal basis : Articolo 2 bis della legge n. 405 del 9 novembre 1999
Budget : EUR 16000000
Duration : Aid paid in 2001 and 2002
Form of aid and intensity : Up to the loss incurred
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
--------------------------------------------------
