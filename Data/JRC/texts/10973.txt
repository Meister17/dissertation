Council and Commission Decision
of 21 February 2005
on the conclusion of the Protocol to the Partnership and Cooperation Agreement establishing a partnership between the European Communities and their Member States, of the one part, and the Republic of Moldova, of the other part, to take account of the accession of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic to the European Union
(2006/536/EC, Euratom)
THE COUNCIL OF THE EUROPEAN UNION AND THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community, and in particular Article 44(2), the last sentence of Article 47(2), and Articles 55, 57(2), 71, 80(2), 93, 94, 133 and 181a, in conjunction with the second sentence of Article 300(2) and the first subparagraph of Article 300(3), thereof,
Having regard to the Treaty establishing the European Atomic Energy Community, and in particular the second paragraph of Article 101 thereof,
Having regard to the 2003 Treaty of Accession, and in particular Article 2(3) thereof,
Having regard to the 2003 Act of Accession, and in particular Article 6(2) thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament [1],
Having regard to the Council's approval pursuant to Article 101 of the Treaty establishing the European Atomic Energy Community,
Whereas:
(1) The Protocol to the Partnership and Cooperation Agreement establishing a partnership between the European Communities and their Member States, of the one part, and the Republic of Moldova, of the other part, to take account of the accession of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic to the European Union was signed on behalf of the European Communities and their Member States on 30 April 2004.
(2) Pending its entry into force, the Protocol has been applied on a provisional basis as from 1 May 2004.
(3) The Protocol should be approved,
HAVE DECIDED AS FOLLOWS:
Article 1
The Protocol to the Partnership and Cooperation Agreement establishing a partnership between the European Communities and their Member States, of the one part, and the Republic of Moldova, of the other part, to take account of the accession of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic to the European Union is hereby approved on behalf of the European Community, the European Atomic Energy Community and the Member States.
The text of the Protocol is attached to this Decision [2].
Article 2
The President of the Council shall, on behalf of the Community and its Member States, give the notification provided for in Article 4 of the Protocol. The President of the Commission shall simultaneously give such notification on behalf of the European Atomic Energy Community.
Done at Brussels, 21 February 2005.
For the Council
The President
J. Asselborn
For the Commission
The President
J. M. Barroso
[1] OJ C 174 E, 14.7.2005, p. 43.
[2] See page 10 of this Official Journal.
--------------------------------------------------
