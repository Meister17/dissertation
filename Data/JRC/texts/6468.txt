Commission Regulation (EC) No 604/2005
of 19 April 2005
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 20 April 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 19 April 2005.
For the Commission
J. M. Silva Rodríguez
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 1947/2002 (OJ L 299, 1.11.2002, p. 17).
--------------------------------------------------
ANNEX
to Commission Regulation of 19 April 2005 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code | Standard import value |
07020000 | 052 | 111,4 |
204 | 83,8 |
212 | 129,8 |
624 | 101,8 |
999 | 106,7 |
07070005 | 052 | 134,3 |
204 | 52,5 |
999 | 93,4 |
07099070 | 052 | 100,6 |
204 | 33,6 |
999 | 67,1 |
08051020 | 052 | 46,8 |
204 | 46,7 |
212 | 50,3 |
220 | 47,8 |
400 | 53,7 |
624 | 60,6 |
999 | 51,0 |
08055010 | 052 | 65,8 |
220 | 69,6 |
388 | 70,6 |
400 | 67,0 |
528 | 44,6 |
624 | 68,8 |
999 | 64,4 |
08081080 | 388 | 90,2 |
400 | 134,5 |
404 | 123,2 |
508 | 66,4 |
512 | 73,3 |
524 | 63,2 |
528 | 77,5 |
720 | 72,3 |
804 | 109,7 |
999 | 90,0 |
08082050 | 388 | 86,3 |
512 | 67,4 |
528 | 65,7 |
720 | 59,5 |
999 | 69,7 |
--------------------------------------------------
