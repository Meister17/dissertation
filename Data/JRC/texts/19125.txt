Decision of the EEA Joint Committee
No 141/2003
of 7 November 2003
amending Annex II (Technical regulations, standards, testing and certification) and Annex IV (Energy) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex II to the Agreement was amended by Decision of the EEA Joint Committee No 29/1999 of 26 March 1999(1).
(2) Annex IV to the Agreement was amended by Decision of the EEA Joint Committee No 83/2002 of 25 June 2002(2).
(3) Commission Directive 2002/40/EC of 8 May 2002 implementing Council Directive 92/75/EEC with regard to energy labelling of household electric ovens(3), as corrected by OJ L 33, 8.2.2003, p. 43, is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
Annex II to the Agreement is amended as follows:
1. the following point is inserted after point 4f (Commission Directive 97/17/EC) of Chapter IV:
"4g. 32002 L 0040: Commission Directive 2002/40/EC of 8 May 2002 implementing Council Directive 92/75/EEC with regard to energy labelling of household electric ovens (OJ L 128, 15.5.2002, p. 45), as corrected by OJ L 33, 8.2.2003, p. 43.
The provisions of the Directive shall, for the purposes of this Agreement, be read with the following adaptations:
(a) Annex I to Commission Directive 2002/40/EC is supplemented with the texts as set out in section 6 of Appendix 1 to Annex II to this Agreement.
(b) Annex V to Commission Directive 2002/40/EC is supplemented with the texts as set out in section 6 of Appendix 2 to Annex II to this Agreement.";
2. Appendices 1 and 2 are supplemented as specified in the Annexes I and II to this Decision.
Article 2
Annex IV to the Agreement is amended as follows:
1. the following point is inserted after point 11f (Commission Directive 97/17/EC):
"11g. 32002 L 0040: Commission Directive 2002/40/EC of 8 May 2002 implementing Council Directive 92/75/EEC with regard to energy labelling of household electric ovens (OJ L 128, 15.5.2002, p. 45), as corrected by OJ L 33, 8.2.2003, p. 43(4).
The provisions of the Directive, for the purposes of this Agreement, are read with the following adaptations:
(a) Annex I to Commission Directive 2002/40/EC is supplemented with the texts as set out in section 6 of Appendix 5 to Annex IV to this Agreement;
(b) Annex V to Commission Directive 2002/40/EC is supplemented with the texts as set out in section 6 of Appendix 6 to Annex IV to this Agreement."
2. Appendices 5 and 6 are supplemented as specified in Annexes III and IV to this Decision.
Article 3
The text of Directive 2002/40/EC, as corrected by OJ L 33, 8.2.2003, p. 43, in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 4
This Decision shall enter into force on 8 November 2003, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee(5).
Article 5
This Decision shall be published in the EEA section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 7 November 2003.
For the EEA Joint Committee
The President
HSH Prinz Nikolaus von Liechtenstein
(1) OJ L 266, 19.10.2000, p. 5.
(2) OJ L 266, 3.10.2002, p. 34.
(3) OJ L 128, 15.5.2002, p. 45.
(4) Listed here for information purposes only; for application see Annex II 'Technical regulations, specifications, testing and certification'.
(5) No constitutional requirements indicated.
ANNEX I
TO DECISION OF THE EEA JOINT COMMITTEE No 141/2003
The following section is inserted after section 5 in Appendix 1 to Annex II to the Agreement:
"SECTION 6
COMMISSION DIRECTIVE 2002/40/EC
(household electric ovens)
>PIC FILE= "L_2004041EN.001401.TIF">
>PIC FILE= "L_2004041EN.001501.TIF">"
ANNEX II
TO DECISION OF THE EEA JOINT COMMITTEE No 141/2003
The following section is inserted after section 5 in Appendix 2 to Annex II to the Agreement:
"SECTION 6
COMMISSION DIRECTIVE 2002/40/EC
(household electric ovens)
>TABLE>"
ANNEX III
TO DECISION OF THE EEA JOINT COMMITTEE No 141/2003
The following section is inserted after section 5 in Appendix 5 to Annex IV to the Agreement:
"SECTION 6
COMMISSION DIRECTIVE 2002/40/EC
(household electric ovens)
>PIC FILE= "L_2004041EN.001901.TIF">
>PIC FILE= "L_2004041EN.002001.TIF">"
ANNEX IV
TO DECISION OF THE EEA JOINT COMMITTEE No 141/2003
The following section is inserted after section 5 in Appendix 6 to Annex IV to the Agreement:
"SECTION 6
COMMISSION DIRECTIVE 2002/40/EC
(household electric ovens)
>TABLE>."
