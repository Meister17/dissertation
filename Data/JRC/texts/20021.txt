COMMISSION DECISION of 13 July 1995 amending Decision 94/984/EC laying down animal health conditions and veterinary certificates for the importation of fresh poultrymeat from certain third countries (Text with EEA relevance) (95/302/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/494/EEC of 26 June 1991 on animal health conditions governing intra-Community trade in and imports from third countries of fresh poultry meat (1), as last amended by Directive 93/121/EC (2), and in particular Articles 11 and 12 thereof,
Whereas Commission Decision 94/984/EC (3) established the animal health conditions and the veterinary certificates for imports of fresh poultry meat from certain third countries;
Whereas it is now possible, in accordance with information received from Brazil and with the results of an inspection carried out by the Commission services in that country to review the regionalisation for Brazil; whereas a certain delay should be provided for this change to enable the Brazilian authorities to take into account the conclusions of the said inspection;
Whereas it appears, on the basis of information recently received, that Israel cannot comply with the requirements of certificate model B; whereas however Israel can comply with the requirements of Model A in respect of goose liver;
Whereas Commission Decisions 94/963/EC (4) and 95/98/EC (5) have laid down the status with regard to Newcastle disease for Finland and Sweden respectively; whereas, therefore, the footnotes referring to the Member States or parts thereof benefiting from additional guarantees, in accordance with Article 3 (A) (1), have to be extended to include also these Member States;
Whereas the measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
Commission Decision 94/984/EC is amended as follows:
1. in Annex,
(a) the line:
>TABLE>
is replaced by:
>TABLE>
(b) the line:
>TABLE>
is replaced by:
>TABLE>
(c) following footnotes are inserted:
'(1) applicable from 1 September 1995.
(2) goose liver only.`;
2. in Annex II, Part 2, Models A and B are replaced by Models A and B in the Annex to this Decision respectively.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 13 July 1995.
For the Commission Franz FISCHLER Member of the Commission
ANNEX
'PART 2 Model A 16. Health attestation:
I, the undersigned official veterinarian, hereby certify, in accordance with the provisions of Directive 91/494/EEC:
1. that .................... (1), region .................... (2), is free from avian influenza and Newcastle disease, as defined in the International Animal Health Code of OIE;
2. that the meat described above is obtained from poultry which:
(a) have been held in the territory of .................... (1), region .................... (2), since hatching or have been imported as day-old chicks;
(b) came from holdings:
- which have not been placed under animal health restrictions in connection with a poultry disease,
- around which, within a radius of 10 km, there have been no outbreaks of avian influenza or Newcastle disease for at least 30 days;
(c) have not been slaughtered in the context of any animal health scheme for the control or eradication of poultry diseases;
(d) have/have not (3) been vaccinated against Newcastle disease using a live vaccine during the 30 days preceding slaughter;
(e) during transport to the slaughterhouse did not come into contact with poultry suffering from avian influenza or Newcastle disease;
3. that the meat described above:
(a) comes from slaughterhouses which, at the time of slaughter, are not under restrictions due to a suspect or actual outbreak of avian influenza or Newcastle disease and around which, within a radius of 10 km, there have been no outbreaks of avian influenza or Newcastle disease for at least 30 days;
(b) has not been in contact, at any time of slaughter, cutting, storage or transport with meat which does not fulfil the requirements of Directive 91/494/EEC; Done at , on Seal (4) (signature of official veterinarian) (4) (name in capital letters, qualifications and title) Model B 16. Health attestation:
I, the undersigned official veterinarian, hereby certify, in accordance with the provisions of Directive 91/494/EEC:
1. that .................... (1), region ................... (2) is free from avian influenza and Newcastle disease, as defined in the International Animal Health Code of OIE;
2. that the meat described above is obtained from poultry which:
(a) have been held in the territory of ................... (1), region ................... (2), since hatching or have been imported as day-old chicks;
(b) come from holdings:
- which have not been placed under animal health restrictions in connection with a poultry disease,
- around which, within a radius of 10 km, there have been no outbreaks of avian influenza or Newcastle disease for at least 30 days;
(c) have not been slaughtered in the context of any animal disease using a live vaccine during the 30 days preceeding slaughter;
(d) have/have not (3) been vaccinated against Newcastle disease using a live vaccine during the 30 days preceeding slaughter;
(e) during transport to the slaughterhouse did not come into contact with poultry suffering from avian influenza or Newcastle disease;
3. that the commercial slaughter poultry flock from which the meat is issued,
(a) has not been vaccinated with vaccines prepared from a Newcastle disease virus Master Seed which shows a higher pathogenicity than lentogenic strains of the virus; and (b) has undergone at slaughter, on the basis of an at random sample of coactal swabs of at least 60 birds of each flock concerned, a virus isolation test for Newcastle disease, carried out in an official laboratory, in which no avian paramyxoviruses with an Intracerebral Pathogenicity Index (ICPI) of more than 0,4 have been found; and (c) has not been in contact during the period of 30 days preceeding slaughter with poultry which do not fulfil the guarantees mentioned under (a) and (b).
4. that the meat described above:
(a) comes from slaughterhouses which, at the time of slaughter, are not under restrictions due to a suspect or actual outbreak of avian influenza or Newcastle disease and around which, within a radius of 10 km, there have been no outbreaks of avian influenza of Newcastle disease for at least 30 days;
(b) has not been in contact, at any time of slaughter, cutting, storage or transport with meat which does not fulfil the requirements of Directive 91/494/EEC; Done at on Seal (4) (signature of official veterinarian) (4) (name in capital letters, qualification and title) (1) Name of the country of origin.
(2) Only to be completed if the authorization to export to the Community is restricted to certain regions of the third country concerned.
(3) Delete the unnecessary reference. If the poultry have been vaccinated within 30 days before slaughter, the consigment cannot be sent to Member States or regions thereof which have been recognized in accordance with Article 12 of Directive 90/539/EEC (Currently Denmark, Ireland, Finland, Sweden and, in the United Kingdom, Northern Ireland).
(4) Stamp and signature in a colour different to that of the printing.`
