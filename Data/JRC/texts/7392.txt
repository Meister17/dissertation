Commission Regulation (EC) No 421/2005
of 14 March 2005
on the issue of licences for the import of certain prepared or preserved citrus fruits (namely mandarins, etc.) in the period from 11 April 2005 to 10 April 2006
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 3285/94 of 22 December 1994 on common rules for imports and repealing Regulation (EC) No 518/94 [1],
Having regard to Council Regulation (EC) No 519/94 of 7 March 1994 on common rules for imports from certain third countries and repealing Regulations (EEC) No 1765/82, (EEC) No 1766/82 and (EEC) 3420/83 [2],
Having regard to Commission Regulation (EC) No 658/2004 of 7 April 2004 imposing definitive safeguard measures against imports of certain prepared or preserved citrus fruits (namely mandarins, etc.) [3] and in particular Article 8(1) thereof,
Whereas:
(1) The quantities for which licence applications have been lodged by traditional importers and by new importers under Article 5 of Regulation (EC) No 658/2004 exceed the quantities available for products originating in the People’s Republic of China.
(2) It is now necessary to fix, for each category of importer, the proportion of the quantity for which application is made which may be imported under licence,
HAS ADOPTED THIS REGULATION:
Article 1
Import licences applied for under Article 5(1) of Regulation (EC) No 658/2004, shall be issued at the percentage rates of the quantities applied for as set out in the Annex hereto.
Article 2
This Regulation shall enter into force on 11 April 2005 and apply until 10 April 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 14 March 2005.
For the Commission
Peter Mandelson
Member of the Commission
[1] OJ L 349, 31.12.1994, p. 53. Regulation as last amended by Regulation (EC) No 2200/2004 (OJ L 374, 22.12.2004, p. 1).
[2] OJ L 67, 10.3.1994, p. 89. Regulation as last amended by Regulation (EC) No 427/2003 (OJ L 65, 8.3.2003, p. 1).
[3] OJ L 104, 8.4.2004, p. 67.
--------------------------------------------------
ANNEX
Origin of the products | Percentage allocations |
People’s Republic of China | Other third countries |
—traditional importers(Article 2(d) of Regulation (EC) No 658/2004) | 38,204 % | N/A |
—other importers(Article 2(f) of Regulation (EC) No 658/2004) | 4,725 % | N/A |
--------------------------------------------------
