Action brought on 9 November 2005 by the Commission of the European Communities against the Italian Republic
An action against the Italian Republic was brought before the Court of Justice of the European Communities on 9 November 2005 by the Commission of the European Communities, represented by D. Recchia and M. Konstantinidis, acting as Agents, with an address for service in Luxembourg.
The Commission claims that the Court should:
1. declare that the Italian Republic, by adopting Legislative Decree No 209 of 24 June 2003 transposing the provisions of Directive 2000/53/EC [1] into national law in a manner inconsistent with the directive itself, has failed to fulfil its obligations under Article 2(2) and (5), Article 3(5), Article 4(2)(a) in conjunction with Annex II, Article 5(1) to (4), Article 6(3)(a) and (4), Article 7(1) and (2), Article 8(3) and (4), Article 10(3) and Article 12(2) of Directive 2000/53/EC of the European Parliament and of the Council of 18 September 2000 on end-of life vehicles;
2. order the Italian Republic to pay the costs.
Pleas in law and main arguments
The Commission of the European Communities brought an action on 7 November 2005 before the Court of Justice of the European Communities for a declaration that the Italian Republic, having failed to take all the measures necessary to:
- define end-of-life vehicles which are waste and their treatment, corresponding to those laid down in the directive;
- include three-wheel motor vehicles in end-of-life vehicles in the Legislative Decree transposing the directive;
- provide clearly that all dangerous materials and components as laid down in Annex II of the directive are removed from vehicles before any further treatment;
- provide, as far as technically feasible, for the collection in suitable collection facilities of waste used parts removed when passenger cars are repaired;
- regarding the issue of certificates of destruction, ensure that those are issued by authorised treatment centres as provided for in Article 6 of the directive. The issue of those certificates also does not appear to be a condition for the de-registration of end-of life vehicles as required by the directive;
- ensure that producers meet all, or a significant part of, the costs of the implementation of the system of free delivery of end-of-life vehicles;
- ensure the safety of dangerous components of end-of-life vehicles before they are treated;
- give preference to recycling over other recovery methods of the vehicles in question;
- preclude transposition of certain specific provisions of the directive, stated therein, through agreements within the meaning of Article 10 of the directive;
- implement a system of controls and assessments necessary to evaluate the attainment of the objectives stated in the directive so that those objectives have been attained before 1 January 2006;
- determine that the information to be provided by the producers of vehicles and components must reflect what is required by the treatment facilities;
- implement the obligation to regularly monitor the results achieved and to report such results to the competent authorities and the Commission and, likewise, implement the obligation on the competent authorities to make provisions for examining the progress reached under an agreement;
has failed to fulfil its obligations under Article 2(2) and (5), Article 3(5), Article 4(2)(a) in conjunction with Annex II, Article 5(1) to (4), Article 6(3)(a) and (4), Article 7(1) and (2), Article 8(3) and (4), Article 10(3) and Article 12(2) of Directive 2000/53/EC of the European Parliament and of the Council of 18 September 2000 on end-of life vehicles.
The Commission also requests that the Italian Republic be ordered to pay the costs of the action.
[1] OJ L 269 of 21.10.2000, p. 34.
--------------------------------------------------
