Action brought on 8 December 2005 — EMC Development v Commission
Parties
Applicant(s): EMC Development AB (Lulea, Sweden) [represented by: M. Elvinger, lawyer]
Defendant(s): Commission of the European Communities
Form of order sought
- Annul the Commission's decision dated 28th September 2005 rejecting the applicant's complaint pursuant to Article 3(2) of Council regulation 17/62;
- order the Commission to pay the costs.
Pleas in law and main arguments
The applicant is a corporate body inter alia responsible for the continued testing and further research and development regarding an alternative cement product known as energetically modified cement. The applicant introduced a complaint before the Commission under Regulation 17/62, accusing the European producers of Portland cement (the type of cement which is predominant in the European market) of a series of behaviours constituting serious violations of Article 81 EC. More specifically, the complaint concerned the EN 197-1 standard, adopted in the context of directive 89/106 [1]. According to the applicant, this standard was purposefully designed to favour existing major players in the market to the exclusion of other cement producers or market challenging products and technologies. This was allegedly achieved through close cooperation between the technical sub-committee of the European Committee for standardisation and CEMBUREAU, the duly appointed trade association of European cement producers, the vast majority of whose members are well established Portland cement producers.
The applicant now challenges the decision rejecting its complaint. It alleges that the offending standard amounts to a horizontal cooperation agreement in violation of Article 81 EC. In the alternative, the applicant contends that the standard breaches the aims of Articles 28 and 29 EC and cannot, in any event, be justified at a Member State level under Article 30 EC.
[1] Council Directive 89/106 of 21 December 1998 on the approximation of laws, regulations and administrative provisions of the Member States relating to Construction products, OJ L 40 11.12.1989, p.12
--------------------------------------------------
