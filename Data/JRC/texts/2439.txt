Commission Regulation (EC) No 1269/2005
of 1 August 2005
on the issue of import licences for frozen thin skirt of bovine animals
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1254/1999 of 17 May 1999 on the common organisation of the market in beef and veal [1],
Having regard to Commission Regulation (EC) No 996/97 of 3 June 1997 on the opening and administration of an import tariff quota for frozen thin skirt of bovine animals falling within CN code 02062991 [2], and in particular Article 8(3) thereof,
Whereas:
(1) Article 1(3)(b) of Regulation (EC) No 996/97 fixes the amount of frozen thin skirt which may be imported on special terms in 2005/2006 at 800 tonnes.
(2) Article 8(3) of Regulation (EC) No 996/97 lays down that the quantities applied for may be reduced. The applications lodged relate to total quantities which exceed the quantities available. Under these circumstances and taking care to ensure an equitable distribution of the available quantities, it is appropriate to reduce proportionally the quantities applied for,
HAS ADOPTED THIS REGULATION:
Article 1
All applications for import licences made pursuant to Article 8 of Regulation (EC) No 996/97 are hereby met to the extent of 0,53871 % of the quantity requested.
Article 2
This Regulation shall enter into force on 2 August 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 1 August 2005.
For the Commission
J. M. Silva Rodríguez
Director-General for Agriculture and Rural Development
[1] OJ L 160, 26.6.1999, p. 21. Regulation as last amended by Regulation (EC) No 1782/2003 (OJ L 270, 21.10.2003, p. 1).
[2] OJ L 144, 4.6.1997, p. 6. Regulation as last amended by Regulation (EC) No 1118/2004 (OJ L 217, 17.6.2004, p. 10).
--------------------------------------------------
