*****
COMMISSION DIRECTIVE
of 20 January 1989
amending Annex II to Council Directive 66/401/EEC on the marketing of fodder plant seed
(89/100/EEC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Directive 66/401/EEC of 14 June 1966 on the marketing of fodder plant seed (1), as last amended by Directive 88/380/EEC (2), and in particular Article 21a thereof,
Whereas, by reason of the conditions of its cultivation and its morphological characteristics, seed of meadow foxtail (Alopecurus pratensis) contains a relatively high proportion of seed of poa species;
Whereas, for seed of meadow foxtail, it is accordingly difficult to obtain, in relation to seed of poa species, the 1 % maximum content by weight of seeds or a single other plant species laid down in Annex II to Directive 66/401/EEC;
Whereas for seed of tall oatgrass (Arrhenatherum elatius) and golden oatgrass (Trisetum flavescens), which display similar morphological characteristics, this 1 % maximum does not apply to seed of poa species;
Whereas, in the light of the development of technical knowledge, it is therefore appropriate to amend Annex II to Directive 66/401/EEC in order to make the same provision for seed of meadow foxtail;
Whereas the measures provided for in this Directive are in accordance with the opinion of the Standing Committee on Seeds and Propagating Material for Agriculture, Horticulture and Forestry,
HAS ADOPTED THIS DIRECTIVE:
Article 1
In Annex II to Directive 66/401/EEC, part I, section 2 (A) ('Table'), column 6 ('Analytical purity - Maximum content of seeds of other plant species (% by weight) - A single species'), after '1,0' given for Alopecurus pratensis, '(f)' is added.
Article 2
Member States shall take the measures necessary to comply with this Directive not later than 1 January 1990. They shall forthwith inform the Commission thereof.
Article 3
This Directive is addressed to the Member States.
Done at Brussels, 20 January 1989.
For the Commission
Ray MAC SHARRY
Member of the Commission
(1) OJ No 125, 11. 7. 1966, p. 2298/66.
(2) OJ No L 187, 16. 7. 1988, p. 31.
