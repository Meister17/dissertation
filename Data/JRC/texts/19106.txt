Commission Regulation (EC) No 940/2003
of 28 May 2003
on import licence applications for rice originating in and coming from Egypt under the tariff quota provided for in Commission Regulation (EC) No 196/97
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 3072/95 of 22 December 1995 on the common organisation of the market in rice(1), as last amended by Commission Regulation (EC) No 411/2002(2),
Having regard to Council Regulation (EC) No 2184/96 of 28 October 1996 concerning imports into the Community of rice originating in and coming from Egypt(3),
Having regard to Commission Regulation (EC) No 196/97 of 31 January 1997 laying down detailed rules for the application of Council Regulation (EC) No 2184/96 concerning imports into the Community of rice originating in and coming from Egypt(4), and in particular the second subparagraph of Article 4(3) thereof,
Whereas:
(1) Article 4(3) of Commission Regulation (EC) No 196/97 stipulates that the Commission must set a single reduction percentage for quantities applied for if import licence applications exceed quantities available. That Article also provides that the Commission must notify the Member States of its decision within 10 working days of the day on which the licence applications are lodged.
(2) Import licence applications for rice falling within CN code 1006 lodged from 1 September 2001 to 19 May 2003 cover a quantity of 32065 tonnes while the maximum quantity to be made available is 32000 tonnes of rice falling within the above code.
(3) A single reduction percentage, as provided for in Article 4(3) of Regulation (EC) No 196/97, should therefore be set for the import licence applications lodged on 19 May 2003 and benefiting from the reduced customs duties provided for in Regulation (EC) No 2184/96.
(4) No more import licences allowing a reduced customs duties should be issued for the current marketing year.
(5) In view of its purpose, this Regulation should take effect on the day of its publication in the Official Journal of the European Union,
HAS ADOPTED THIS REGULATION:
Article 1
Import licence applications for rice falling within CN code 1006 and benefiting from the reduced customs duties provided for in Regulation (EC) No 2148/96, lodged on 19 May 2003 and notified to the Commission, shall give rise to the issue of licences for the quantities applied for multiplied by a reduction percentage of 24,475.
Article 2
Import licences under Regulation (EC) No 2148/96 shall no longer be issued in respect of licence applications for rice falling within CN code 1006 submitted on or after 20 May 2003.
Article 3
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 28 May 2003.
For the Commission
J. M. Silva Rodríguez
Agriculture Director-General
(1) OJ L 329, 30.12.1995, p. 18.
(2) OJ L 62, 5.3.2002, p. 27.
(3) OJ L 292, 15.11.1996, p. 1.
(4) OJ L 31, 1.2.1997, p. 53.
