Council Joint Action 2005/807/CFSP
of 21 November 2005
extending and amending the mandate of the European Union Monitoring Mission (EUMM)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty on European Union and, in particular, Article 14 thereof,
Whereas:
(1) On 25 November 2002, the Council adopted Joint Action 2002/921/CFSP extending the mandate of the European Union Monitoring Mission [1] (EUMM).
(2) On 22 November 2004, the Council adopted Joint Action 2004/794/CFSP [2] prolonging Joint Action 2002/921/CFSP until 31 December 2005.
(3) The EUMM should continue its activities in the Western Balkans in support of the European Union's policy towards that region, with a particular focus on Kosovo, Serbia and Montenegro, and neighbouring regions that might be affected by any adverse developments in Kosovo or Serbia and Montenegro.
(4) The mandate of the EUMM should therefore be extended and amended accordingly,
HAS ADOPTED THIS JOINT ACTION:
Article 1
The mandate of the EUMM is hereby extended until 31 December 2006.
Article 2
Joint Action 2002/921/CFSP is hereby amended as follows:
1. Article 2(2)(a) shall be replaced by the following:
"(a) monitor political and security developments in the area of its responsibility, with a particular focus on Kosovo, Serbia and Montenegro, and neighbouring regions that might be affected by any adverse developments in Kosovo or Serbia and Montenegro;";
2. Article 3(3), shall read:
"3. The Secretary- General/High Representative shall ensure that the EUMM functions flexibly and in a streamlined manner. In that context, he shall regularly re-examine the functions and the geographical territory covered by the EUMM so as to continue to adapt the internal organisation of the EUMM to the priorities of the Union in the Western Balkans. He shall report to the Council at the beginning of 2006 as to whether the conditions for ending monitoring activities in Albania are in place; he shall review early in 2006 the EUMM's presence in Bosnia and Herzegovina and shall put forward recommendations. The Commission shall be fully associated.";
3. Article 6(1) shall read:
"1. The financial reference amount intended to cover the expenditure related to the mission shall be:
(a) EUR 2 million for 2005 and
(b) EUR 1723982,80 for 2006.";
4. in the second paragraph of Article 8, the date " 31 December 2005" shall be replaced by " 31 December 2006".
Article 3
This Joint Action shall enter into force on the date of its adoption.
Article 4
This Joint Action shall be published in the Official Journal of the European Union.
Done at Brussels, 21 November 2005.
For the Council
The President
J. Straw
[1] OJ L 321, 26.11.2002, p. 51 and corrigendum in OJ L 324, 29.11.2002, p. 76.
[2] OJ L 349, 25.11.2004, p. 55.
--------------------------------------------------
