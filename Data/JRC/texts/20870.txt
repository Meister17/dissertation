Commission Regulation (EC) No 327/2002
of 21 February 2002
amending Regulation (EC) No 2316/1999 laying down detailed rules for the application of Council Regulation (EC) No 1251/1999 establishing a support system for producers of certain arable crops
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1251/1999 of 17 May 1999 establishing a support system for producers of certain arable crops(1), as last amended by Regulation (EC) No 1038/2001(2), and in particular Article 9 thereof,
Whereas:
(1) Commission Regulation (EC) No 2316/1999(3), as last amended by Regulation (EC) No 1157/2001(4), lays down detailed rules for applying Regulation (EC) No 1251/1999 with respect to the conditions for granting area payments for certain arable crops.
(2) Article 5a(2) of Regulation (EC) No 1251/1999 lays down that Member States are to check at least 30 % of the areas under hemp grown for fibre for which area payment applications have been made and at least 20 % of areas if a Member State introduces a system of prior approval for such crops. Experience gained in the 2001/02 marketing year shows that the requirements for these checks as laid down in Regulation (EC) No 2316/1999 should be specified in greater detail.
(3) In the interests of consistency and simplification, the specific provisions relating to the use of colza seed obtained from seed produced on the same farm should be deleted.
(4) Commission Regulation (EEC) No 3887/92 of 23 December 1992 laying down detailed rules for applying the integrated administration and control system for certain Community aid schemes(5), as last amended by Regulation (EC) No 2721/2000(6), has been consolidated and replaced by Regulation (EC) No 2419/2001(7), as amended by Regulation (EC) No 2550/2001(8). During that exercise, it was decided to transfer the specific provisions defining determined areas to the implementing rules for the schemes concerned.
(5) Commission Regulation (EC) No 1/2002 of 28 December 2001 laying down detailed rules for the application of Council Regulation (EC) No 1259/1999 as regards the simplified scheme for payments to farmers under certain support schemes(9) introduces a simplified scheme for certain producers. That Regulation provides for the specific applications under the simplified scheme to be taken into account for calculating any overrun on the base area and for calculating the overrun on the reference yield as referred to in Article 3(7) of Regulation (EC) No 1251/1999.
(6) In order to qualify for area payments producers are required to set aside part of the area of their holding. In order to ensure that the scheme is effective, producers should not be allowed to put set-aside land to any agricultural use.
(7) Austria has notified new names for certain regions eligible for the additional area payment for durum wheat.
(8) In response to applications by Italy, the Netherlands and the United Kingdom, new base areas should be fixed in accordance with the regionalisation plans of those Member States without altering the total base area.
(9) Under Council Regulation (EC) No 1017/94 of 26 April 1994 concerning the conversion of land currently under arable crops to extensive livestock farming in Portugal(10), as last amended by Regulation (EC) No 2582/2001(11), applications have been submitted for conversion of an area equivalent to 16185 hectares. The base area should therefore be adjusted accordingly.
(10) The Member States have notified the results of the tests to determine the tetrahydrocannabinol levels in the hemp varieties sown in 2001. Those results should be taken into account when drawing up the list of hemp varieties qualifying for area payments in the coming marketing years and the list of varieties temporarily accepted for 2002/03 which will require further testing during that marketing year.
(11) Regulation (EC) No 2316/1999 should therefore be amended accordingly.
(12) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Cereals,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 2316/1999 is hereby amended as follows:
1. Article 3(1)(c) is replaced by the following: "(c) on which the crop is maintained until at least the beginning of flowering under normal growing conditions in accordance with local practice.
Crops of oilseeds, protein plants, linseed, fibre flax and durum wheat must continue to be cultivated under normal growing conditions in accordance with local practice at least until 30 June prior to the marketing year in question, unless they are harvested at full maturity before that date. Protein plants may not be harvested until after the stage of lactic ripeness.
In the case of hemp grown for fibre, so that the checks provided for in Article 5a(2) of Regulation (EC) No 1251/1999 can be made, crops must continue to be cultivated under normal growing conditions in accordance with local practice at least until 10 days after the end of flowering. However, the Member State may authorise hemp grown for fibre to be harvested after flowering has begun but before the end of the 10-day period after flowering, provided the inspectors indicate which representative parts of each plot concerned must continue to be cultivated until ten days after flowering for inspection purposes, in accordance with the procedure laid down in Annex XIII."
2. Article 4(2)(b) is replaced by the following: "(b) seed harvested from certified seed of double zero varieties grown on the same holding;".
3. Article 4(3) is deleted.
4. The following is added at the end of Article 6(5): "Where a discrepancy is found between the minimum quantity of certified seed fixed by the Member State and the quantity actually used, the area shall be calculated by dividing the total quantity of certified seed for which the producer has furnished proof of use by the minimum quantity per hectare laid down by the Member State for the region of the producer in question. The area thus determined shall be used, after the abovementioned reductions have been made, for calculating the entitlement to the supplement or the special aid."
5. The last subparagraph of Article 7b(1) is replaced by the following: "The varieties of hemp grown for fibre listed in point 2(b) of Annex XII to this Regulation shall be subject to procedure B during the 2002/03 marketing year in all Member States growing those varieties."
6. Article 19(3) is replaced by the following: "3. Set-aside areas may not be used for any agricultural production, other than that provided for in Article 6(3) of Regulation (EC) No 1251/1999, nor put to any agricultural or lucrative use which would be incompatible with growing an arable crop."
7. Annex IV is replaced by Annex I to this Regulation.
8. In Annex VI, the details given under the headings "Italy", "Netherlands", "Portugal" and "United Kingdom" shall be replaced by the details given in Annex II to this Regulation.
9. Annex VII is replaced by Annex III to this Regulation.
10. Annex XII is replaced by Annex IV to this Regulation.
11. Annex XIII is replaced by Annex V to this Regulation.
Article 2
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Communities.
It shall apply for the 2002/03 and following marketing years.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 21 February 2002.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 160, 26.6.1999, p. 1.
(2) OJ L 145, 31.5.2001, p. 16.
(3) OJ L 280, 30.10.1999, p. 43.
(4) OJ L 157, 14.6.2001, p. 8.
(5) OJ L 391, 31.12.1992, p. 36.
(6) OJ L 314, 14.12.2000, p. 8.
(7) OJ L 327, 12.12.2001, p. 11.
(8) OJ L 341, 22.12.2001, p. 105.
(9) OJ L 1, 3.1.2002, p. 1.
(10) OJ L 112, 3.5.1994, p. 2.
(11) OJ L 345, 29.12.2001, p. 5.
ANNEX I
"ANNEX IV
(second subparagraph of Article 6(1))
ZONES ELIGIBLE FOR THE SUPPLEMENT FOR DURUM WHEAT IN AUSTRIA
PANNONIA:
1. Gebiete der Bezirksbauernkammern
2046 Tullnerfeld-Klosterneuburg
2054 Baden
2062 Bruck/Leitha-Schwechat
2089 Baden
2101 Gänserndorf
2241 Hollabrunn
2275 Tullnerfeld-Klosterneuburg
2305 Korneuburg
2321 Mistelbach
2330 Krems/Donau
2364 Gänserndorf
2399 Mistelbach
2402 Mödling
2470 Mistelbach
2500 Hollabrunn
2518 Hollabrunn
2551 Bruck/Leitha-Schwechat
2577 Korneuburg
2585 Tullnerfeld-Klosterneuburg
2623 Wr. Neustadt
2631 Mistelbach
2658 Gänserndorf
2. Gebiete der Bezirksreferate
3018 Neusiedl/See
3026 Eisenstadt
3034 Mattersburg
3042 Oberpullendorf
3. Gebiete der Landwirtschaftskammer
1007 Wien"
ANNEX II
">TABLE>"
ANNEX III
"ANNEX VII
(Article 10(4))
>PIC FILE= "L_2002051EN.001803.TIF">
>PIC FILE= "L_2002051EN.001901.TIF">"
ANNEX IV
"ANNEX XII
(Article 7a(1))
Varieties of flax and hemp grown for fibre eligible for the support system
1. Flax grown for fibre
Adelie
Agatha
Angelin
Argos
Ariane
Aurore
Belinka
Ceasar Augustus
Diane
Diva
Electra
Elise
Escalina
Evelin
Exel
Hermes
Ilona
Laura
Liflax
Liviola
Marina
Marylin
Nike
Opaline
Rosalin
Venus
Veralin
Viking
Viola
2a. Hemp grown for fibre
Carmagnola
Beniko
Cs
Delta-Ilosa
Delta 405
Dioica 88
Epsilon 68
Fedora 17
Fédrina 74
Felina 32
Felina 34 - Félina 34
Ferimon-Férimon
Fibranova
Fibrimon 24
Fibrimon 56
Futura
Futura 75
Juso 14
Santhica 23
Uso 31
2b. Hemp grown for fibre authorised in the 2002/03 marketing year
Bialobrzeskie
Fasamo
Fedora 19
Santhica 27"
ANNEX V
"ANNEX XIII
(Article 7b(1))
COMMUNITY METHOD FOR THE QUANTITATIVE DETERMINATION OF Δ9-THC (TETRAHYDROCANNABINOL) CONTENT IN HEMP VARIETIES
1. Subject matter and scope
This method seeks to determine the Δ9-tetrahydrocannabinol (THC) content of varieties of hemp (Cannabis sativa L.). As appropriate, the method involves applying procedure A or B herein described.
The method is based on the quantitative determination of Δ9-THC by gas chromatography (GC) after extraction with a suitable solvent.
1.1. Procedure A
Procedure A is used for checks on production as provided for in Article 5a(2) of Regulation (EC) No 1251/1999.
1.2. Procedure B
Procedure B is used in cases as referred to in the third subparagraph of Article 7b(1) of this Regulation and for checking that the conditions laid down in the second subparagraph of Article 5a(1) of Regulation (EC) No 1251/1999 are fulfilled with a view to inclusion on the list of varieties of hemp eligible for aid from the 2001/02 marketing year.
2. Sampling
Where a Member State uses the possibility provided for in the third paragraph of Article 3(1)(c), at least three distinct parts of the plot, comprising at least 4000 plants, must be left in the field on the inspector's instructions until at least ten days after flowering has finished so that a sample can be taken.
2.1. Samples
- Procedure A: in a standing crop of a given variety of hemp, take a 30 cm part containing at least one female inflorescence of each plant selected. Sampling is to be carried out during the period running from 20 days after the start of flowering to 10 days after the end of flowering, during the day, following a systematic pattern to ensure that the sample is representative of the field but excluding the edges of the crop.
Member States may authorise sampling to be carried out during the period from the start of flowering to 20 days after the start of flowering provided that, for each variety grown, other representative samples are taken in accordance with the above rules during the period from 20 days after the start of flowering to 10 days after the end of flowering,
- Procedure B: in a standing crop of a given variety of hemp, take the upper third of each plant selected. Sampling is to be carried out during the 10 days following the end of flowering, during the day, following a systematic pattern to ensure that the sample is representative of the field but excluding the edges of the crop. In the case of dioecious varieties, only female plants must be taken.
2.2. Sample size
- Procedure A: the sample is to comprise parts of 50 plants per field,
- Procedure B: the sample is to comprise parts of 200 plants per field.
Each sample is to be placed in a fabric or paper bag, without crushing it, and sent to the laboratory for analysis.
The Member State may provide for a second sample to be collected for counteranalysis, if required, to be kept either by the producer or by the body responsible for the analysis.
2.3. Drying and storage of the sample
Drying of the samples must begin as soon as possible and, in any case, within 48 hours using any method below 70 °C. Samples should be dried to a constant weight and to a moisture content of between 8 % and 13 %.
After drying, store the samples without crushing them at below 25 °C in a dark place.
3. Determination of THC content
3.1. Preparation of the test sample
Remove stems and seeds over 2 mm in size from the dried samples.
Grind the dried samples to obtain a semi-fine powder (passing through a 1 mm mesh sieve).
The powder may be stored for 10 weeks at below 25 °C in a dark, dry place.
3.2. Reagents and extraction solution
Reagents
- Δ9-tetrahydrocannabinol, pure for chromatographic purposes,
- squalane, pure for chromatographic purposes, as an internal standard.
Extraction solution
- 35 mg of squalane per 100 ml hexane.
3.3. Extraction of Δ9-THC
Weigh 100 mg of the powdered test sample, place in a centrifuge tube and add 5 ml of extraction solution containing the internal standard.
Place in an ultrasound bath and leave for 20 minutes. Centrifuge for five minutes at 3000 r.p.m. and then remove the supernatant THC solution. Inject the solution into the chromatograph and carry out a quantitative analysis.
3.4. Gas chromatography
(a) Apparatus
- gas chromatograph with a flame ionisation detector and a split/splitless injector,
- column allowing good separation of cannabinoids, for example a glass capillary column 25 m long and 0,22 mm in diameter impregnated with a 5 % non-polar phenyl-methyl-siloxane phase.
(b) Calibration ranges
At least three points for procedure A and five points for procedure B, including points 0,04 and 0,50 mg/ml Δ9-THC in extraction solution.
(c) Experimental conditions
>TABLE>
(d) Injection volume: 1 μl
4. Results
The findings are to be expressed to two decimal places in grams of Δ9-THC per 100 grams of analytical sample dried to constant weight. A tolerance of 0,03 g per 100 g applies.
- Procedure A: one determination per test sample.
However, where the result obtained is above the limit laid down in the second subparagraph of Article 5a(1) of Regulation (EC) No 1251/1999, a second determination must be carried out per analysis sample and the mean value of the two determinations will be taken as the result,
- Procedure B: the result corresponds to the mean value of two determinations per test sample."
