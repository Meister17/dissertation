Commission Decision
of 25 April 2005
amending Decision 2003/526/EC as regards classical swine fever control measures in France
(notified under document number C(2005) 1249)
(Text with EEA relevance)
(2005/339/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 90/425/EEC of 26 June 1990 concerning veterinary and zootechnical checks applicable in intra-Community trade in certain live animals and products with a view to the completion of the internal market [1], and in particular Article 10(4) thereof,
Whereas:
(1) In response to outbreaks of classical swine fever in certain Member States, Commission Decision 2003/526/EC of 18 July 2003 concerning protection measures relating to classical swine fever in certain Member States [2] was adopted. That Decision establishes certain additional disease control measures concerning classical swine fever.
(2) France has informed the Commission about the recent evolution of that disease in feral pigs in the Northern Vosges area of France. In light of the epidemiological information, it is appropriate to amend the area where disease control measures apply.
(3) Decision 2003/526/EC should therefore be amended accordingly.
(4) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
Part I(2) of the Annex to Decision 2003/526/EC is replaced by the Annex hereto.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 25 April 2005.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 224, 18.8.1990, p. 29. Directive as last amended by Directive 2002/33/EC of the European Parliament and of the Council (OJ L 315, 19.11.2002, p. 14).
[2] OJ L 183, 22.7.2003, p. 46. Decision as last amended by Decision 2005/225/EC (OJ L 71, 17.3.2005, p. 70).
--------------------------------------------------
ANNEX
"2. France:
The territory of the Department of Bas-Rhin and Moselle located west of the Rhine and the channel Rhine Marne, north of the motorway A4, east of the river Sarre and south of the border with Germany and the municipalities Holtzheim, Lingolsheim and Eckbolsheim."
--------------------------------------------------
