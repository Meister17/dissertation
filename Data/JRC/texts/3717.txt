Judgment of the Court
(First Chamber)
of 6 October 2005
in Case C-9/04: Reference for a preliminary ruling from the Hoge Raad der Nederlanden in criminal proceedings against Geharo BV [1]
In Case C-9/04: reference for a preliminary ruling under Article 234 EC from the Hoge Raad der Nederlanden (Netherlands), made by decision of 23 December 2003, received at the Court on 12 January 2004, in criminal proceedings against Geharo BV — the Court (First Chamber) composed of P. Jann, President of the Chamber, K. Lenaerts (Rapporteur), N. Colneric, K. Schiemann and E. Levits, Judges; P. Léger, Advocate General; R. Grass, Registrar, gave a judgment on 6 October 2005, in which it:
[1] OJ C 59, 06.03.2004.
--------------------------------------------------
