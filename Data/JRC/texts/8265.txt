COUNCIL DECISION
of 3 May 1999
on the establishment of a Financial Regulation governing the budgetary aspects of the management by the Secretary-General of the Council, of contracts concluded in his name, on behalf of certain Member States, relating to the installation and the functioning of the "Help Desk Server" of the Management Unit and of the Sirene Network Phase II
(1999/323/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the first sentence of the second subparagraph of Article 2(1) of the Protocol annexed to the Treaty on European Union and to the Treaty establishing the European Community, integrating the Schengen acquis into the framework of the European Union,
(1) Whereas the Secretary General of the Council was authorised by the Council Decision 1999/322/EC of 3 May 1999 to act, in the context of the integration of the Schengen acquis within the European Union, as representative of certain Member States for the purposes of concluding contracts relating to the installation and the functioning of the "Help Desk Server" of the Management Unit and of the Sirene Network Phase II and to manage such contracts(1);
(2) Whereas the financial obligations arising under those contracts are not borne by the budget of the European Communities; whereas, therefore, the provisions of the Financial Regulation of 21 December 1977 applicable to the general budget of the European Communities(2) do not apply;
(3) Whereas it is therefore necessary to establish specific rules to define the detailed procedures for establishing and implementing the budget required to meet the obligations arising under those contracts, for recovering the contributions to be paid by the States concerned and for the presentation and auditing of the accounts,
HAS DECIDED AS FOLLOWS:
CHAPTER I
General principles
Article 1
For the purposes of this Financial Regulation, the "budget" is the instrument which sets out forecasts of, and authorises in advance, for each financial year, the revenue and expenditure required to meet the obligations arising under the contracts referred to in the Council Decision of 3 May 1999.
Article 2
This Financial Regulation lays down the detailed rules for the establishment and implementation of the budget.
Article 3
1. The budget shall be subdivided into titles according to the contracts to be managed. The title concerning the contract referred to in Article 1(1) of the Council Decision of 3 May 1999 shall be subdivided into chapters covering the installation budget and the operating budget for the Sirene Network Phase II respectively. If necessary, each chapter shall be subdivided into articles.
2. Appropriations opened under each title may not be assigned to other expenditure titles.
Article 4
1. All revenue and expenditure shall be entered in full in the budget and in the accounts without any adjustment against each other. Total revenue shall cover total expenditure.
2. The financial year shall run from 1 January to 31 December.
3. Any budget contributions by the States referred to in Article 21 paid before the beginning of the financial year which they concern shall be credited to the budget for that financial year.
4. The expenditure of a financial year shall be entered in the accounts for that year on the basis of expenditure for which the financial controller received authorisation no later than 31 December and for which the corresponding payments were made by the accounting officer before the following 15 January.
CHAPTER II
Establishment of the budget
Article 5
1. The budget shall be drawn up in euro.
2. The Secretary-General of the Council shall forward the preliminary draft budget to the Schengen Information System Working Group, hereafter referred to as the "SIS Working Group", before 30 September and attach an explanatory memorandum.
3. The SIS Working Group shall deliver its opinion on that preliminary draft.
4. The Secretary-General shall establish the draft budget and forward it by 31 October to the States referred to in Article 21.
5. The Member States referred to in Article 21, meeting within the Council, shall adopt the budget before the end of the year.
6. The decision to adopt the budget, duly notified by the Secretary-General to the States referred to in Article 21, shall mark the point at which those States' contributions fall due.
Article 6
1. If the budget has not been finally adopted at the beginning of the financial year:
- payments may be made for up to one twelfth each month of the total appropriations authorised for each budget title for the preceding financial year;
- the contributions of the States referred to in Article 21 may be called up for up to one twelfth each month of the contributions paid under the last duly adopted budget.
2. Each decision to use an expenditure and revenue twelfth, up to a total of three twelfths of the amounts entered in the last duly adopted budget, shall be taken by the Secretary-General, who shall forward it by letter to the States referred to in Article 21.
3. Above the limit of three twelfths of the amounts entered in the last duly adopted budget, any decision to authorise payments and call up contributions shall be taken by the Member States referred to in Article 21, meeting within the Council.
4. Any measures taken pursuant to the preceding paragraphs shall be terminated immediately on final adoption of the budget.
Article 7
1. Any draft supplementary or amending budgets shall be submitted, examined and adopted in the same form and using the same procedure as the budget whose estimates they are amending.
2. An amending budget shall be submitted on an annual basis, in the month following the closure of the accounts as laid down in Article 25(1), with the aim of entering the balance of the budget outturn from the previous financial year as revenue in the case of a positive balance or expenditure if the balance is negative.
Article 8
The budget may be released to the public.
CHAPTER III
Implementation of the budget and accountancy
Article 9
The budget shall be implemented in accordance with the principle that the authorising officer and the accounting officer are different individuals. The duties of authorising officer, accounting officer and financial controller shall be mutually incompatible.
Article 10
1. The task of authorising officer for revenue and expenditure shall be carried out by a Director-General at the General Secretariat of the Council. The authorising officer shall implement the budget on behalf of the Secretary-General and within the limits of the appropriations allotted he may delegate his powers to a Director.
2. The authorising officer may decide on transfers between articles within each chapter. With the agreement of the SIS Working Group, he may decide on transfers between chapters within the same title. The SIS Working Group shall give its agreement under the same conditions as for adopting its opinion on the budget.
Article 11
The task of financial controller shall be carried out by the Council's Financial Controller in accordance with the rules applicable to the latter's duties.
Article 12
The receipt of revenue and the disbursement of expenditure shall be carried out by an accounting officer from Directorate-General A at the General Secretariat of the Council.
Article 13
1. For the collection of any amount owing pursuant to Article 21, the issue of a recovery order by the authorising officer shall be required. Recovery orders shall be forwarded to the accounting officer, who shall submit them to the financial controller for approval.
2. The purpose of this approval shall be to establish that:
- the revenue has been booked to the correct budget item,
- the recovery order is in order and conforms to the relevant provisions,
- the supporting documents are in order,
- the competent authority of the debtor State is correctly described,
- the due date is indicated,
- the amount and currency are correct.
3. The accounting officer shall assume responsibility for the recovery orders duly drawn up.
Article 14
1. Any measure likely to involve expenditure against the budget must first be the subject of a proposal for a commitment of expenditure from the authorising officer to the accounting officer, showing the purpose, the amount involved, the budget item to which the expenditure is to be charged and the creditor. The proposal shall be submitted by the accounting officer to the financial controller for approval.
2. The purpose of this approval shall be to establish that:
- the proposal for commitment has been presented in accordance with paragraph 1,
- the expenditure has been changed to the correct budget item,
- the appropriations are available in the budget,
- the expenditure is in order and conforms to the relevant provisions.
Article 15
1. The purpose of validation of expenditure by the authorising officer shall be to:
- verify the creditor's claim,
- determine or verify the existence and the amount of the sum due,
- verify the conditions under which the payment falls due,
- verify that purchases or services rendered are as ordered.
2. The authorising officer may have the verifications carried out under his responsibility.
Article 16
1. The authorising officer shall authorise the accounting officer, by the issue of a payment order (authorisation), to pay an item of expenditure which has been validated.
2. The authorisation shall state:
- the financial year against which the payment shall be charged,
- the budget title, chapter and article,
- the amount to be paid, in figures and in words, and the currency of payment,
- the name and address of the creditor,
- the purpose of the expenditure,
- the method of payment,
- the numbers and dates of the relevant approvals of commitment.
3. The payment order shall be dated and signed by the authorising officer.
4. The accounting officer shall submit the payment order, together with the original supporting documents, to the financial controller for approval.
5. The purpose of this approval shall be to establish that:
- the payment order was properly issued,
- the payment order agrees with the commitment of expenditure and the amount thereof is correct,
- the expenditure is charged to the correct item in the budget,
- the appropriations are available in the budget title or article concerned,
- the supporting documents are in order, and
- the creditor is correctly named and described.
6. Any expenditure must be covered beforehand by contributions from the States referred to in Article 21 or, failing those, by a bank loan. The costs of a pre-financing loan in the event of non-payment of contributions shall be divided among the States in default, pro rata to their unpaid contributions and taking into account the length of arrears.
Article 17
Payments shall be effected through a bank account specifically opened for that purpose in the name of the General Secretariat of the Council. Bank transfer orders shall require the joint signature of two officials nominated by the Secretary-General, of whom one shall be the accounting officer.
Article 18
Should the financial controller refuse to give the approval laid down in Articles 13, 14 or 16 and the authorising officer maintain his proposal, the matter shall be referred to the Secretary-General. Except in cases where the availability of appropriations is in doubt, the Secretary-General may, in a duly reasoned decision, overrule the refusal and confirm the recovery order, expenditure commitment or payment order. The Secretary-General shall within one month inform the Court of Auditors of any such decision. The decision shall be enforceable as from the date approval was refused.
Article 19
The liability to disciplinary action of the authorising officer, financial controller, and accounting officer in the event of failure to comply with the provisions of this Financial Regulation shall be as laid down in the Staff Regulations of Officials of the European Communities.
Article 20
The accounts shall be kept by the double-entry method on the basis of the calendar year. They shall show all revenue and expenditure for the financial year.
CHAPTER IV
States' contributions
Article 21
1. Budget revenue shall consist of financial contributions from the following Member States: Belgium, Denmark, Germany, Greece, Spain, France, Italy, Luxembourg, the Netherlands, Austria, Portugal, Finland, and Sweden, as well as Iceland and Norway.
2. These States' financial contributions shall be laid down in the budget and expressed in euro.
Article 22
The States referred to in Article 21 shall supply their financial contributions to the Secretary-General in accordance with the following formula:
The scale of contributions to be paid by the Member States referred to in Article 21, on the one hand, and by Iceland and Norway ("the other States"), on the other hand, shall be calculated annually on the basis of the share of each Member State concerned and of the other States in the total gross domestic product (GDP) for the preceding year of all the States referred to in Article 21. The scale of contributions by the Member States concerned shall be calculated annually, taking into account the other States' contributions, on the basis of the ratio of the VAT resources paid by each of those Member States to the total VAT resources of the European Communities, as established in the last amendment of the budget of the Communities during the preceding financial year.
Article 23
1. The Secretary-General shall forward requests for contributions to each State referred to in Article 21 by letter through the national administrations whose details have been notified to him.
2. The letter concerned shall contain the following information:
- the decision to adopt the budget or, in the event of Article 6 being invoked, the decision to call up contributions by provisional twelfths,
- the amount to be paid by each State, calculated in euro in accordance with the scale referred to in Article 22,
- the details necessary for paying the contribution.
3. Contributions shall be paid into the bank account referred to in Article 17.
4. They shall be payable in euro.
Article 24
1. The States referred to in Article 21 shall be required to pay 25 % of their contribution by 15 February, 1 April, 1 July and 1 October at the latest.
2. Should a State have defaulted on its financial obligations, the existing Community rules on interest chargeable in the event of late payment of contributions to the Community budget shall apply by analogy.
CHAPTER V
Presenting and auditing the accounts
Article 25
1. The Secretary-General shall, within two months from the end of the budget implementation period, draw up a revenue and expenditure account and a balance sheet and transmit them to the SIS Working Group.
2. The revenue and expenditure account shall cover all revenue and expenditure transactions relating to the preceding financial year. It shall be submitted in the same form and following the same subdivisions as the budget.
3. The following shall be attached to the account:
- a statement showing the situation of each of the States referred to in Article 21 as regards payment of its financial contribution, and
- an appropriation transfer statement.
4. The balance sheet shall show the budget assets and liabilities as at 31 December of the preceding financial year.
Article 26
1. The Court of Auditors shall be asked to audit the accounts.
2. The Secretary-General shall forward the revenue and expenditure account and the balance sheet to the Court of Auditors within 15 days following the period laid down in Article 25(1).
3. The purpose of the audit by the Court of Auditors shall be to establish that all revenue has been received and all expenditure incurred in a lawful and regular manner in accordance with the contracts to be managed, the budget and this Financial Regulation.
4. The Secretary-General shall provide the Court of Auditors with every facility it may consider necessary in order to carry out its task.
Article 27
The revenue and expenditure account, balance sheet and report by the Court of Auditors, accompanied where appropriate by the Secretary-General's comments, shall be submitted by 1 July to the States referred to in Article 21. The Member States referred to in Article 21, meeting within the Council, shall give a discharge to the Secretary-General in respect of the budget's implementation.
CHAPTER VI
Final provisions
Article 28
The introduction of the provisions of the Schengen acquis concerning the Schengen Information System for a State other than those referred to in Article 21, hereafter referred to as "other State" shall entail:
- readjustment of the share of contributions of the States referred to in Article 21 as laid down in Article 22,
- adjustment of the contributions of the States referred to in Article 21 in order to establish the contribution payable by the other State to the operation of Sirene Stage II for the whole of the current financial year,
- adjustment of the contributions of the States referred to in Article 21 in order to establish the proportion of earlier Sirene Stage II installation costs to be borne by the other State. This percentage shall be calculated on the basis of the ratio of the VAT resources paid by the other State to the total VAT resources of the European Communities for the earlier financial years in which the necessary Sirene Stage II installation costs have been incurred. The percentage contribution shall be the subject of a "credit note" to the States referred to in Article 21 for an amount pro rata to their share as calculated in accordance with Article 22. The other States may choose to allocate the amount concerned towards their share of the operating budget or request reimbursement.
Article 29
This Financial Regulation shall apply mutatis mutandis to the implementation of the revenue and expenditure required to meet the obligations arising under the contracts referred to in Article 1 of the Council Decision of 3 May 1999 for the remaining period of the financial year in which this Regulation enters into force.
Article 30
1. This Financial Regulation shall take effect immediately.
2. It shall be published in the Official Journal of the European Communities.
Done at Brussels, 3 May 1999.
For the Council
The President
J. FISCHER
(1) See page 49 of this Official Journal.
(2) OJ L 356, 31.12.1977, p. 1.
