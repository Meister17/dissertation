Council Regulation (EC) No 1300/2005
of 3 August 2005
amending Regulation (EC) No 27/2005, as concerns herring, mackerel, horse mackerel, sole and vessels engaged in illegal fisheries
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 2371/2002 of 20 December 2002 on the conservation and sustainable exploitation of fisheries resources under the Common Fisheries Policy [1], and in particular Article 20 thereof,
Whereas:
(1) Regulation (EC) No 27/2005 [2] fixes for 2005 the fishing opportunities and associated conditions for certain fish stocks and groups of fish stocks, applicable in Community waters and, for Community vessels, in waters where catch limitations are required.
(2) The International Baltic Sea Fisheries Commission (IBSFC) adopted a recommendation in September 2004 to increase the fishing opportunities for herring by 10000 tonnes for 2004 in Management Unit 3, which would give Finland an additional 8199 tonnes of fishing opportunities for herring. This recommendation was not made part of the Community legislation. As a consequence Finland has overfished its quota by 7856 tonnes for 2004 as the additional tonnes were not allocated. In Commission Regulation (EC) No 776/2005 of 19 May 2005 adapting certain fish quotas for 2005 pursuant to Council Regulation (EC) No 847/96 introducing additional conditions for year-to-year management of TACs and quotas [3] the Finnish quota of herring for 2005 was reduced by 7856 tonnes due to the overfishing. The Finnish quota for herring in subdivision 30-31 should therefore be increased by 7856 tonnes as the reduction was due to the fact that the IBSFC recommendation was not implemented in Community legislation. This amendment will not increase the amount of herring caught by Finland in 2005.
(3) The total available catch (TAC) adopted for mackerel in management area IIa (non-EC waters), Vb (EC waters), VI, VII, VIIIa, b, d, e, XII, XIV should cover EC waters and international waters of Vb in order to avoid misreporting. The management area should therefore be amended accordingly.
(4) The TAC adopted for horse mackerel in management area Vb (EC waters), VI, VII, VIIIa, b, d, e, XII, XIV should cover EC waters and international waters of Vb in order to avoid misreporting. The management area should therefore be amended accordingly.
(5) In the light of new scientific advice the TAC for common sole can be increased to 900 tonnes in management area IIIa, IIIb, c, d (EC waters). The TAC should therefore be amended accordingly.
(6) In order to allowing weighing of herring, mackerel and horse mackerel after transport from the port of landing, complementary measures should be implemented in 2005.
(7) In accordance with the Agreed Record of Conclusions of Fisheries Consultations between the European Community and Norway for 2005 the Parties have access to fish 50000 tonnes of their respective North Sea herring quotas in the other Parties' waters of Divisions IVa and IVb. These quantities can be increased by 10000 tonnes if called for. By letter of 29 June 2005 Norway has called for such an increase. The Community has submitted a similar request on 20 July 2005. It is therefore appropriate to implement these changes in the Community legislation.
(8) In May 2005 the North-East Atlantic Fisheries Commission (NEAFC) made a recommendation to place a number of vessels on the list of vessels that have been confirmed as having engaged in illegal, unreported and unregulated fisheries. A recommendation on measures to be applied on such vessels was adopted in February 2004. Implementation of the recommendations in the Community legal order should be ensured.
(9) Given the urgency of the matter, it is imperative to grant an exception to the six-week period referred to in paragraph I(3) of the Protocol on the role of national parliaments in the European Union, annexed to the Treaty on European Union and to the Treaties establishing the European Communities.
(10) Regulation (EC) No 27/2005 should therefore be amended accordingly,
HAS ADOPTED THIS REGULATION:
Article 1
Annexes IA, IB and III to Regulation (EC) No 27/2005 are hereby amended in accordance with the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 3 August 2005.
For the Council
The President
J. Straw
[1] OJ L 358, 31.12.2002, p. 59.
[2] OJ L 12, 14.1.2005, p. 1.
[3] OJ L 130, 24.5.2005, p. 7.
--------------------------------------------------
ANNEX
The Annexes to Regulation (EC) No 27/2005 are amended as follows:
1. in Annex IA:
the entry concerning the species Herring in zone Sub-division 30-31 is replaced by the following:
"Species__NEWLINE__Herring__NEWLINE__Clupea harengus | Zone__NEWLINE__Sub-divisions 30-31__NEWLINE__HER/3D30.; HER/3D31 |
Finland | 60327 | |
Sweden | 11529 | |
EC | 71856 | |
TAC | 71856 | Analytical TAC where Articles 3 and 4 of Regulation (EC) No 847/96 do not apply."; |
2. in Annex IB:
(a) the entry concerning the species Herring in zone IV north of 53° 30′ N is replaced by the following:
Within the limits of the abovementioned quotas, no more than the quantities given below may be taken in the zones specified:
| Norwegian waters south of 62° N(HER/*04N-) |
EC | 60000"; |
"Species:
Herring [1]
Clupea harengus
Zone:
IV north of 53° 30′ N
HER/4AB
|
Denmark | 95211 | |
Germany | 57215 | |
France | 20548 | |
The Netherlands | 56745 | |
Sweden | 5443 | |
United Kingdom | 70395 | |
EC | 305557 | |
Norway | 60000 [2] | |
TAC | 535000 | Analytical TAC where Articles 3 and 4 of Regulation (EC) No 847/96 do not apply. |
(b) the entry concerning the species Mackerel in zone IIa (non-EC waters), Vb (EC waters), VI, VII, VIIIa, b, d, e, XII, XIV is replaced by the following:
Within the limits of the abovementioned quotas, no more than the quantities given below may be taken in the zones specified, and only during the periods 1 January to 15 February and 1 October to 31 December.
| IVa (EC waters) MAC/*04A-C |
Germany | 4175 |
Spain | 0 |
France | 2784 |
Ireland | 13918 |
The Netherlands | 6089 |
United Kingdom | 38274 |
EC | 65240 |
Norway | 8500 |
Faroe Islands | 1002 |
"Species:
Mackerel
Scomber scombrus
Zone:
IIa (non-EC waters), Vb (EC waters and international waters), VI, VII, VIIIa, b, d, e, XII, XIV
MAC/2CX14-
|
Germany | 13845 | |
Spain | 20 | |
Estonia | 115 | |
France | 9231 | |
Ireland | 46149 | |
Latvia | 85 | |
Lithuania | 85 | |
The Netherlands | 20190 | |
Poland | 844 | |
United Kingdom | 126913 | |
EC | 217477 | |
Norway | 8500 [3] | |
Faroe Islands | 3322 [4] | |
TAC | 420000 [5] | Analytical TAC where Articles 3 and 4 of Regulation (EC) No 847/96 do not apply. |
(c) the entry concerning the species Common sole in zone IIIa, IIIb, c, d (EC waters) is replaced by the following:
"Species__NEWLINE__Common sole__NEWLINE__Solea solea | Zone__NEWLINE__IIIa, IIIb, c, d (EC waters)__NEWLINE__SOL/3A/BCD |
Denmark | 755 | |
Germany | 44 | |
The Netherlands | 73 | |
Sweden | 28 | |
EC | 900 | |
TAC | 900 | Analytical TAC where Articles 3 and 4 of Regulation (EC) No 847/96 apply."; |
(d) the entry concerning the species Horse mackerel in zone Vb (EC waters), VI, VII, VIIIa, b, d, e, XII, XIV is replaced by the following:
"Species__NEWLINE__Horse mackerel__NEWLINE__Trachurus spp. | Zone__NEWLINE__Vb (EC waters and international waters), VI, VII, VIIIa, b, d, e, XII, XIV__NEWLINE__JAX/578/14 |
Denmark | 12088 | |
Germany | 9662 | |
Spain | 13195 | |
France | 6384 | |
Ireland | 31454 | |
The Netherlands | 46096 | |
Portugal | 1277 | |
United Kingdom | 13067 | |
EC | 133223 | |
Faroe Islands | 4955 | |
TAC | 137000 | Analytical TAC where Articles 3 and 4 of Regulation (EC) No 847/96 do not apply. |
3. in Annex III:
(a) point 9 is replaced by the following:
"9. Landing and weighing procedures for herring, mackerel and horse mackerel
9.1. Scope
9.1.1. The following procedures shall apply to landings in the European Community by Community and third country vessels of quantities per landing exceeding 10 tonnes of herring, mackerel, and horse mackerel, or a combination thereof, taken in:
(a) for herring, ICES Sub-areas I, II, IV, VI and VII and Divisions IIIa and Vb;
(b) for mackerel and horse mackerel, in ICES Sub-areas III, IV, VI and VII and Division IIa.
9.2. Designated ports
9.2.1. Landings referred to in point 9.1 are only permitted in designated ports.
9.2.2. Each Member State concerned shall transmit to the Commission changes in the list, transmitted in 2004, of designated ports in which landings of herring, mackerel and horse mackerel may take place and, changes in inspection and surveillance procedures for those ports including the terms and conditions for recording and reporting the quantities of any of the species and stocks referred to in point 9.1.1 within each landing. Those changes shall be transmitted at least 15 days before they enter into force. The Commission shall transmit this information as well as ports designated by third countries to all Member States concerned.
9.3. Entry to port
9.3.1. The master of a fishing vessel referred to in point 9.1.1 or his agent shall inform the competent authorities of the Member State in which the landing is to be made, at least four hours in advance of entry to port of landing of the Member State concerned of the following:
(a) the port he intends to enter, the name of the vessel and its registration number;
(b) the estimated time of arrival at that port;
(c) the quantities in kilograms live weight by species retained on board;
(d) the management area in accordance with Annex I to this Regulation where the catch was taken.
9.4. Discharge
9.4.1. The competent authorities of the Member State concerned shall require that the discharge does not commence until authorised to do so.
9.5. Logbook
9.5.1. By way of derogation from the provisions of point 4.2 of Annex IV to Regulation (EEC) No 2807/83, the master of a fishing vessel shall submit, immediately upon arrival to port, the relevant page or pages of the logbook as demanded by the competent authority at the port of landing.
The quantities retained on board, notified prior to landing as referred to in point 9.3.1(c), shall be equal to the quantities recorded in the logbook after its completion.
By way of derogation from the provisions of Article 5(2) of Regulation (EEC) No 2807/83 the permitted margin of tolerance in estimates recorded into the logbook of the quantities in kilograms of fish retained on board of vessels shall be 8 %.
9.6. Weighing of fresh fish
9.6.1. All buyers purchasing fresh fish shall ensure that all quantities received are weighed on systems approved by the competent authorities. The weighing shall be carried out prior to the fish being sorted, processed, held in storage and transported from the port of landing or resold. The figure resulting from the weighing shall be used for the completion of landing declarations and sales notes.
9.6.2. When determining the weight any deduction for water shall not exceed 2 %.
9.7. Weighing of fresh fish after transport
9.7.1. By way of derogation from point 9.6.1, Member States may permit fresh fish to be weighed after transport from the port of landing provided that the fish is transported to a destination on the territory of the Member State no more than 60 kilometres from the port of landing and that:
(a) the tanker in which the fish is transported is accompanied by an inspector from the place of landing to the place where the fish is weighed; or
(b) approval is given by the competent authorities at the place of landing to transport the fish subject to the following provisions:
(i) immediately prior to the tanker leaving the port of landing, the buyer or his agent shall provide to the competent authorities a written declaration giving the species of the fish and name of the vessel from which it is to be discharged, the unique identity number of the tanker and details of the destination where the fish will be weighed as well as the estimated time of arrival of the tanker at the destination,
(ii) a copy of the declaration provided for in (i) shall be kept by the driver during the transport of the fish and handed over to the receiver of the fish at the destination.
9.8. Invoice
9.8.1. In addition to the obligations set out in Article 9(1) and (2) of Regulation (EC) No 2847/93 the processor or buyer of the quantities of fresh fish landed shall submit to the competent authorities of the Member State concerned a copy of the invoice or a document replacing it, as referred to in Article 22(3) of the Sixth Council Directive 77/388/EEC of 17 May 1977 on the harmonisation of the laws of the Member States relating to turnover taxes — Common system of value added tax: uniform basis of assessment [9].
9.8.2. Any such invoice or document shall include the information required by Article 9(3) of Regulation (EC) No 2847/93 as well as the name and registration number of the vessel from which the fish has been landed. This invoice or document shall be submitted on demand or within 12 hours of the completion on the weighing.
9.9. Weighing of frozen fish
9.9.1. All buyers or holders of frozen fish shall ensure that the quantities landed are weighed prior to the fish being processed, held in storage, transported from the port of landing or resold. Any tare weight equal to the weight of boxes, plastic or other containers in which the fish to be weighed is packed may be deducted from the weight of any quantities landed.
9.9.2. Alternatively, the weight of frozen fish packed in boxes may be determined by multiplying the average weight of a representative sample based on weighing the contents removed from the box and without plastic packaging whether or not after the thawing of any ice on the surface of the fish. Member States shall notify to the Commission for approval any changes in their sampling methodology approved by the Commission during 2004. Changes shall be approved by the Commission. The figure resulting from the weighing shall be used for the completion of landing declarations and sales notes.
9.10. Weighing facilities
9.10.1. In cases where publicly operated weighing facilities are used the party weighing the fish shall issue to the buyer a weighing slip indicating the date and time of the weighing and the identity number of the tanker. A copy of the weighing slip shall be attached to the invoice submitted to the competent authorities as provided for in point 9.8.
9.10.2. In cases where privately operated weighing facilities are used the system shall be approved, calibrated and sealed by the competent authorities and be subject to the following provisions:
(a) the party weighing the fish shall keep a paginated weighing logbook indicating:
(i) the name and registration number of the vessel from which the fish has been landed,
(ii) the identity number of the tankers in cases where fish has been transported from the port of landing before weighing,
(iii) the species of fish,
(iv) the weight of each landing,
(v) the date and time of the beginning and end of the weighing;
(b) where the weighing is carried out on a conveyor belt system a visible counter shall be fitted that records the cumulative total of the weight. Such cumulative total shall be recorded in the paginated logbook referred to in point (a);
(c) the weighing logbook and the copies of written declarations provided for in point 9.7.1(b)(ii) shall be kept for three years.
9.11. Access by competent authorities
The competent authorities shall have full access at all times to the weighing system, the weighing logbooks, written declarations and all premises where the fish is processed and kept.
9.12. Cross-checks
9.12.1. The competent authorities shall carry out administrative cross-checks on all landings between the following:
(a) quantities by species indicated in the prior notice of landing, referred to in point 9.3.1 and the quantities recorded in the vessel's logbook;
(b) quantities by species recorded in the vessel's logbook and the landing declaration or invoice or equivalent document referred to in point 9.8;
(c) quantities by species recorded on the landing declaration and invoice or equivalent document referred to in point 9.8.
9.13. Full inspection
9.13.1. The competent authorities of a Member State shall ensure that at least 15 % of the quantities of fish landed and at least 10 % of the landings of fish are subject to full inspections which shall include at least the following:
(a) monitoring of the weighing of the catch from the vessel, by species. In the case of vessels pumping catch ashore the weighing of the entire discharge from the vessels selected for inspection shall be monitored. In the case of freezer trawlers, all boxes shall be counted. A representative sample of boxes/pallets shall be weighed in order to arrive at an average weight for the boxes/pallets. Sampling of boxes shall also be undertaken according to an approved methodology in order to arrive at an average net weight for the fish (without packing, ice);
(b) in addition to the cross-checks referred to in point 9.12 cross-verification between the following:
(i) quantities by species recorded in the weighing logbook and the quantities by species recorded in the invoice or equivalent document referred to in point 9.8,
(ii) the written declarations received by the competent authorities pursuant to point 9.7.1(b)(i) and the written declarations held by the receiver of the fish pursuant to point 9.7.1(b)(ii),
(iii) identity numbers of tankers that appear in the written declarations provided for in point 9.7.1(b)(i) and the weighing logbooks;
(c) if the discharge is interrupted, permission shall be required before the discharge can recommence;
(d) verification that the vessel is empty of all fish, once the discharge has been completed.
9.13.2. All inspection activities covered by point 9 shall be documented. Such documentation shall be kept for 3 years.
(b) the following Part I is added:
"PART I
NORTH-EAST ATLANTIC
Vessels engaged in illegal, unreported and unregulated fisheries
Vessels that have been placed by the North-East Atlantic Fisheries Commission (NEAFC) on the list of vessels that have been confirmed as having engaged in illegal, unreported and unregulated fisheries (IUU vessels) are listed in Appendix 5. The following measures shall apply to these vessels:
(a) IUU vessels that enter ports are not authorised to land or tranship therein and shall be inspected by the competent authorities. Such inspections shall include the vessel's documents, log books, fishing gear, catch onboard and any other matter relating to the vessel's activities in the Regulatory Area of NEAFC. Information on the result of the inspections shall immediately be transmitted to the Commission;
(b) fishing vessels, support vessels, refuel vessels, mother-ships and cargo vessels flying the flag of a Member State shall not in any way assist IUU vessels or participate in any transhipment or joint fishing operations with vessels on that list;
(c) IUU vessels shall not be supplied in ports with provisions, fuel or other services;
(d) IUU vessels shall not be authorised to fish in Community waters and be prohibited to be chartered;
(e) imports of fish coming from IUU vessels shall be prohibited;
(f) Member States shall refuse the granting of their flag to IUU vessels and encourage importers, transporters and other sectors concerned to refrain from negotiating and from transhipping of fish caught by such vessels.
The Commission shall amend the list to be in accordance with the NEAFC list as soon as NEAFC adopts a new list.";
(c) the following Appendix 5 is added:
[9] OJ L 145, 13.6.1977, p. 1. Directive as last amended by Directive 2004/66/EC (OJ L 168, 1.5.2004, p. 35)."
--------------------------------------------------
"Appendix 5 to Annex III
List of vessels that have been confirmed by NEAFC as having engaged in illegal, unreported and unregulated fisheries
Vessel’s name | Flag State |
FONTENOVA | Panama |
IANNIS | Panama |
LANNIS I | Panama |
LISA | Commonwealth of Dominica |
KERGUELEN | Togo |
OKHOTINO | Commonwealth of Dominica |
OLCHAN | Commonwealth of Dominica |
OSTROE | Commonwealth of Dominica |
OSTROVETS | Commonwealth of Dominica |
OYRA | Commonwealth of Dominica |
OZHERELYE | Commonwealth of Dominica" |
--------------------------------------------------
