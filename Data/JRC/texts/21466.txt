Commission Regulation (EC) No 1181/2002
of 1 July 2002
amending Annex I of Council Regulation (EEC) No 2377/90 laying down a Community procedure for the establishment of maximum residue limits of veterinary medicinal products in foodstuffs of animal origin
(Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2377/90 of 26 June 1990 laying down a Community procedure for the establishment of maximum residue limits of veterinary medicinal products in foodstuffs of animal origin(1), as last amended by Commission Regulation (EC) No 869/2002(2) and in particular Articles 6, 7 and 8 thereof,
Whereas:
(1) In accordance with Regulation (EEC) No 2377/90, maximum residue limits must be established progressively for all pharmacologically active substances which are used within the Community in veterinary medicinal products intended for administration to food-producing animals.
(2) Maximum residue limits should be established only after the examination within the Committee for Veterinary Medicinal Products of all the relevant information concerning the safety of residues of the substance concerned for the consumer of foodstuffs of animal origin and the impact of residues on the industrial processing of foodstuffs.
(3) In establishing maximum residue limits for residues of veterinary medicinal products in foodstuffs of animal origin, it is necessary to specify the animal species in which residues may be present, the levels which may be present in each of the relevant meat tissues obtained from the treated animal (target tissue) and the nature of the residue which is relevant for the monitoring of residues (marker residue).
(4) In view of the reduced availability of veterinary medicinal products for certain food-producing species(3), maximum residue limits may be established by methods of extrapolation from maximum residue limits set for other species on a strictly scientific basis.
(5) For the control of residues, as provided for in appropriate Community legislation, maximum residue limits should usually be established for the target tissues of liver or kidney. However, the liver and kidney are frequently removed from carcasses moving in international trade, and maximum residue limits should therefore also always be established for muscle or fat tissues.
(6) In the case of veterinary medicinal products intended for use in laying birds, lactating animals or honey bees, maximum residue limits must also be established for eggs, milk or honey.
(7) Trimethoprim, Neomycin (including framycetin), Paromomycin, Spectinomycin, Colistin, Danofloxacin, Difloxacin, Enrofloxacin, Flumequine, Erythromycin, Tilmicosin, Tylosin, Florfenicol, Lincomycin and Oxyclozanide should be inserted into Annex I to Regulation (EEC) No 2377/90.
(8) An adequate period should be allowed before the entry into force of this Regulation in order to allow Member States to make any adjustment which may be necessary to the authorisations to place the veterinary medicinal products concerned on the market which have been granted in accordance with Directive 2001/82/EC(4) of the European Parliament and of the Council to take account of the provisions of this Regulation.
(9) The measures provided for in this Regulation are in accordance with the opinion of the Standing Committee on Veterinary Medicinal Products,
HAS ADOPTED THE FOLLOWING REGULATION:
Article 1
Annex I of Regulation (EEC) No 2377/90 is hereby amended as set out in the Annex hereto.
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
It shall apply from the sixtieth day following its publication.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 1 July 2002.
For the Commission
Erkki Liikanen
Member of the Commission
(1) OJ L 224, 18.8.1990, p. 1.
(2) OJ L 137, 25.5.2002, p. 10.
(3) Availability of veterinary medical products Communication from the Commission to the Council and the European Parliament COM(2000) 806 final.
(4) OJ L 311, 28.11.2001, p. 1.
ANNEX
Annex I to Regulation (EEC No 2377/90 is amended as follows:
1. Anti-infectious agents
1.1. Chemotherapeutics
1.1.2. Diamino pyrimidine derivatives
""
1.2. Antibiotics
1.2.3. Quinolones
" "
1.2.4. Macrolides
" "
1.2.5. Florfenicol and related compounds
">TABLE>"
1.2.9. Lincosamides
""
1.2.10. Aminoglycosides
" "
1.2.14. Polymyxins
""
2. Antiparasitic agents
2.1. Agents acting against endoparasites
2.1.4. Phenol derivatives including salicylanides
">TABLE>"
(1) For porcine and poultry species this MRL relates to 'skin and fat in natural proportions'.
(2) For fin fish this MRL relates to 'muscle and skin in natural proportions'.
(3) For fin fish this MRL relates to 'muscle and skin in natural proportions'.
(4) For porcine species this MRL relates to 'skin and fat in natural proportions'.
(5) For fin fish this MRL relates to a 'muscle and skin in natural proportions'.
(6) For procine species this MRL relates to 'skin and fat in natural proportions'.
(7) For porcine and poultry species this MRL relates to 'skin and fat in natural proportions'.
(8) For porcine and poultry species this MRL relates to 'skin and fat in natural proportions'.
(9) For fin fish this MRL relates to 'muscle and skin in natural proportions'.
(10) For porcine and poultry species this MRL relates to 'skin and fat in natural proportions'.
(11) For fin fish this MRL relates to 'muscle and skin in natural proportions'.
(12) For porcine and poultry species this MRL relates to 'skin and fat in natural proportions'.
(13) For fin fish this MRL relates to 'muscle and skin in natural proportions'.
