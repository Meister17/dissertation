COUNCIL DIRECTIVE 94/56/EC of 21 November 1994 establishing the fundamental principles governing the investigation of civil aviation accidents and incidents
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 84 (2) thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the Economic and Social Committee (2),
Acting in accordance with the procedure referred to in Article 189c of the Treaty (3),
Whereas a high general level of safety should be maintained in civil aviation in Europe and all efforts should be made to reduce the number of accidents and incidents;
Whereas the expeditious holding of technical investigations of civil aviation accidents and incidents improves air safety in helping to prevent the occurrence of such accidents and incidents;
Whereas account should be taken of the Convention on International Civil Aviation, signed in Chicago on 7 December 1994, which provides for implementation of the measures necessary to ensure the safe operation of aircraft; whereas particular account should be taken of Annex 13 to this Convention which lays down recommended international standards and practices for aircraft accident investigation;
Whereas, according to the international standards in Annex 13, the investigation of accidents is to be carried out under the responsibility of the State where the accident occurs;
Whereas if, in the case of serious incidents, investigation is not carried out by the State where the incident occurs, such investigation should be conducted by the State of registry;
Whereas investigation of serious incidents should be carried out in a similar way to investigation of accidents;
Whereas the scope of investigations must depend on the lessons which can be drawn from them for the improvement of safety;
Whereas air safety requires investigations to be carried out in the shortest possible time,
Whereas investigators should be able to accomplish their tasks unhindered;
Whereas the Member States must, in compliance with the legislation in force as regards the powers of the authorities responsible for the judicial inquiry and, where appropriate, in close collaboration with those authorities, ensure that those responsible for the technical inquiry are allowed to carry out their tasks in the best possible conditions;
Whereas investigation of accidents and incidents which have occurred in civil aviation should be carried out by or under the control of an independent body or entity in order to avoid any conflict of interest and any possible involvement in the causes of the occurrences being investigated;
Whereas the body or entity should be suitably equipped and its tasks could include prevention activities;
Whereas Member States should take measures to ensure mutual assistance, if required, in carrying out investigations;
Whereas a Member State must be able to delegate the task of carrying out an investigation to another Member State;
Whereas it is important for accident prevention to make public the findings of accident investigations in the shortest time possible;
Whereas the particular nature of incidents should be taken into account when circulating the findings of investigations into them;
Whereas the safety recommendations resulting from an accident or incident investigation should be duly taken into account by the Member States;
Whereas the sole aim of the technical investigation is to draw lessons which could prevent future accidents and incidents and whereas therefore the analysis of the occurrence, the conclusions and the safety recommendations are not designed to apportion blame or liability,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Objective
The purpose of this Directive is to improve air safety by facilitating the expeditious holding of investigations, the sole objective of which is the prevention of future accidents and incidents.
Article 2
Scope
1. This Directive shall apply to investigations into civil aviation accidents and incidents which have occurred in the territory of the Community taking into account the international obligations of the Member States.
2. This Directive shall also apply outside the territory of the Community to:
(i) investigations into accidents involving aircraft registered in a Member State, when such investigations are not carried out by another State;
(ii) investigations into serious incidents involving aircraft registered in a Member State or operated by an undertaking established in a Member State, when such investigations are not carried out by another State.
Article 3
Definitions
For the purposes of this Directive:
(a) 'accident' means an occurrence associated with the operation of an aircraft which takes place between the time any person boards the aircraft with the intention of flight until such time as all such persons have disembarked, in which:
1. a person is fatally or seriously injured as a result of:
- being in the aircraft, or
- direct contact with any part of the aircraft, including parts which have become detached from the aircraft, or
- direct exposure to jet blast,
except when the injuries are from natural causes, self-inflicted or inflicted by other persons, or when the injuries are to stowaways hiding outside the areas normally available to the passengers and crew; or
2. the aircraft sustains damage or structural failure which:
- adversely affects the structural strength, performance or flight characteristics of the aircraft, and
- would normally require major repair or replacement of the affected component,
except for engine failure or damage, when the damage is limited to the engine, its cowlings or accessories; or for damage limited to propellers, wing tips, antennas, tyres, brakes, fairings, small dents or puncture holes in the aircraft skin;
3. the aircraft is missing or is completely inaccessible;
(b) 'serious injury' mens an injury which is sustained by a person in an accident and which:
1. requires hospitalization for more than 48 hours, commencing within seven days from the date the injury was received; or
2. results in a fracture of any bone (except simple fractures of fingers, toes, or nose); or
3. involves lacerations which cause severe haemorrhage, nerve, muscle or tendon damage; or
4. involves injury to any internal organ; or
5. involves second or third degree burns, or any burns affecting more than 5% of the body surface; or
6. involves verified exposure to infectious substances or harmful radiation;
(c) 'fatal injury' means an injury which is sustained by a person in an accident and which results in his/her death within 30 days of the date of the accident;
(d) 'causes' means actions, omissions, events or conditions, or a combination thereof, which led to the accident or incident;
(e) 'investigation' means a process conducted for the purpose of accident and incident prevention which includes the gathering and analysis of information, the drawing of conclusions, including the determination of cause(s) and, when appropriate, the making of safety recommendations;
(f) 'investigator-in-charge' means a person charged, on the basis of his qualifications, with responsibility for the organization, conduct and control of an investigation;
(g) 'flight recorder' means any type of recorder installed in the aircraft for the purpose of facilitating accident/incident investigations;
(h) 'undertaking' means any natural person, any legal person, whether profit-making or not, or any official body whether having its own legal personality or not;
(i) 'operator' means any person, body or undertaking operating or proposing to operate one or more aircraft;
(j) 'incident' means an occurrence, other than an accident, associated with the operation of an aircraft which affects or would affect the safety of operation;
(k) 'serious incident' means an incident involving circumstances indicating that an accident nearly occurred (a list of examples of serious incidents can be found in the Annex);
(l) 'safety recommendation' means any proposal by the investigating body of the State conducting the technical investigation, based on information derived from that investigation, made with the intention of preventing accidents and incidents.
Article 4
Obligation to investigate
1. Every accident or serious incident shall be the subject of an investigation.
However, Member States may take measures to enable incidents not covered by the first subparagraph to be investigated when the investigating body may expect to draw air safety lessons from it.
2. The extent of investigations and the procedure to be followed in carrying out such investigations shall be determined by the investigating body, taking into account the principles and the objective of this Directive and depending on the lessons it expects to draw from the accident or serious incident for the improvement of safety.
3. The investigations referred to in paragraph 1 shall in no case be concerned with apportioning blame or liability.
Article 5
Status of investigation
1. Member States shall define, in the framework of their respective internal legal systems, a legal status of the investigation that will enable the investigators-in-charge to carry out their task in the most efficient way and within the shortest time.
2. In accordance with the legislation in force in the Member States and, where appropriate, in cooperation with the authorities responsible for the judicial inquiry, the investigators shall be authorized inter alia to:
(a) have free access to the site of the accident or incident as well as to the aircraft, its contents or its wreckage;
(b) ensure an immediate listing of evidence and controlled removal of debris, or components for examination or analysis purposes;
(c) have immediate access to and use of the contents of the flight recorders and any other recordings;
(d) have access to the results of examination of the bodies of victims or of tests made on samples taken from the bodies of victims;
(e) have immediate access to the results of examinations of the people involved in the operation of the aircraft or of tests made on samples taken from such people;
(f) examine witnesses;
(g) have free access to any relevant information or records held by the owner, the operator or the manufacturer of the aircraft and by the authorities responsible for civil aviation or airport operation.
Article 6
Investigating body or entity
1. Each Member State shall ensure that technical investigations are conducted or supervised by a permanent civil aviation body or entity. The body or entity concerned shall be functionally independent in particular of the national aviation authorities responsible for airworthiness, certification, flight operation, maintenance, licensing, air traffic control or airport operation and, in general, of any other party whose interests could conflict with the task entrusted to the investigating body or entity.
2. Notwithstanding paragraph 1, the activities entrusted to this body or entity may be extended to the gathering and analysis of air safety related data, in particular for prevention purposes, in so far as these activities do not affect its independence and entail no responsibility in regulatory, administrative or standards matters.
3. The body or entity referred to in paragraph 1 shall be given the means required to carry out its responsibilities independently of the authorities referred to in paragraph 1 and should be able to obtain sufficient resources to do so. Its investigators shall be afforded status giving them the necessary guarantees of independence. It shall comprise at least one investigator able to perform the function of investigator-in-charge in the event of an aircraft accident or serious incident.
4. If necessary, the body or entity may request the assistance of bodies or entities from other Member States to supply:
(a) installations, facilities and equipment for:
- the technial investigation of wreckage and aircraft equipment and other objects relevant to the investigation,
- the evaluation of information from flight recorders, and
- the computer storage and evaluation of air accident data.
(b) accident investigation experts to undertake specific tasks but only when an investigation is opened following a major accident.
When available, such assistance should, as far as possible, be free of charge.
5. A Member State may delegate the task of carrying out an investigation into an accident or incident to another Member State.
Article 7
Accident report
1. Any investigation into an accident shall be the subject of a report in a form appropriate to the type and seriousness of the accident. The report shall state the sole objective of the investigation as referred to in Article 1 and contain, where appropriate, safety recommendations.
2. The investigating body or entity shall make public the final accident report in the shortest possible time, and if possible within 12 months of the date of the accident.
Article 8
Incident report
1. Any investigation into an incident shall be the subject of a report in a form appropriate to the type and seriousness of the incident. The report shall, where appropriate, contain relevant safety recommendations. The report shall protect the anonymity of the persons involved in the incident.
2. The incident report shall be circulated to the parties likely to benefit from its findings with regard to safety.
Article 9
Safety recommendations
The reports and the safety recommendations referred to in Articles 7 and 8 shall be communicated to the undertakings or national aviation authorities concerned and copies forwarded to the Commission.
Member States shall take the necessary measures to ensure that the safety recommendations made by the investigating bodies or entities are duly taken into consideration, and, where appropriate, acted upon without prejudice to Community law.
Article 10
A safety recommendation shall in no case create a presumption of blame or liability for an accident or incident.
Article 11
Council Directive 80/1266/EEC of 16 December 1980 on future cooperation and mutual assistance between the Member States in the field of air accident investigation (4) is hereby repealed.
Article 12
1. Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive not later than 21 November 1994. They shall forthwith inform the Commission thereof.
2. When Member States adopt these provisions, they shall contain a reference to this Directive or shall be accompanied by such reference on the occasion of their official publication. The methods of making such reference shall be laid down by Member States.
Article 13
This Directive is addressed to the Member States.
Done at Brussels, 21 November 1994.
For the Council
The President
M. WISSMANN
(1) OJ No C 257, 22. 9. 1993, p. 8, OJ No C 109, 19. 4. 1994, p. 14.(2) OJ No C 34, 2. 2. 1994, p. 18.(3) Opinion of the European Parliament of 9 March 1994 (OJ No C 91, 28. 3. 1994, p. 123), Council common position of 16 May 1994 (OJ No C 172, 24. 6. 1994, p. 46) and Decision of the European Parliament of 26 October 1994 (OJ No C 323, 21. 11. 1994).(4) OJ No L 375, 31. 12. 1980, p. 32.
ANNEX
LIST OF EXAMPLES OF SERIOUS INCIDENTS The incidents listed below are typical examples of serious incidents. The list is not exhaustive and only serves as a guide to the definition of 'serious incident'.
- A near collision requiring an avoidance manoeuvre or when an avoiding manoeuvre would have been appropriate to avoid a collision or an unsafe situation.
- Controlled flight into terrain (CFIT) only marginally avoided.
- An aborted take-off on a closed or engaged runway, or a take-off from such runway with marginal separation from obstacle(s).
- A landing or attempted landing on a closed or engaged runway.
- Gross failure to achieve predicted performance during take-off or initial climb.
- All fires and smoke in the passenger compartment or in cargo compartments, or engine fires, even though such fires are extinguished with extinguishing agents.
- Any events which required the emergency use of oxygen by the flight crew.
- Aircraft structural failure or engine disintegration which is not classified as an accident.
- Multiple malfunctions of one or more aircraft systems that seriously affect the operation of the aircraft.
- Any case of flight crew incapacitation in flight.
- Any fuel state which would require the declaration of an emergency by the pilot.
- Take-off or landing incidents, such as undershooting, overrunning or running off the side of runways.
- System failures, weather phenomena, operation outside the approved flight envelope or other occurrences which could have caused difficulties controlling the aircraft.
- Failure of more than one system in a redundancy system which is mandatory for flight guidance and navigation.
