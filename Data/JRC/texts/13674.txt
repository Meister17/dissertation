Reference for a preliminary ruling from the Finanzgericht Hamburg by order of that court of 30 August 2005 in Jan de Nul N.V. v Hauptzollamt Oldenburg
Reference has been made to the Court of Justice of the European Communities by order of the Finanzgericht Hamburg of 30 August 2005, received at the Court Registry on 21 October 2005, for a preliminary ruling in the proceedings between Jan de Nul N.V. and Hauptzollamt Oldenburg on the following questions:
1. What interpretation should be given to the term "Community waters" in the first paragraph of Article 8(1)(c) of Directive 92/81 in contrast to the term "inland waterways" for the purposes of the first paragraph of Article 8(2)(b) of Directive 92/81 [1]?
2. Should the operation of a suction and holding vessel (so-called "hopper dredger") in Community waters always be regarded as navigation within the meaning of the first paragraph of Article 8(1)(c) of Directive 92/81 or is it necessary to draw a distinction between the various forms of activity during the course of its use?
[1] OJ 1992 L 316, p. 12.
--------------------------------------------------
