[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 14.8.2006
COM(2006) 457 final
Proposal for a
COUNCIL REGULATION
amending Regulation (EEC) No 1907/90, as regards the derogation on egg washing
(presented by the Commission)
EXPLANATORY MEMORANDUM
Council Regulation (EEC) No 1907/90 on certain marketing standards for eggs lays down the criteria for the derogation enabling packing centres to continue to wash eggs until 31 December 2006.
In the framework of simplification of the agricultural legislation, a new Council Regulation replacing Council Regulation (EEC) No 1907/90 was adopted by the Council on 19 June 2006 and enters into force on 1 July 2007. Pending the application of the new Council Regulation on marketing standards for eggs, the temporary derogation for egg washing foreseen until 31 December 2006 in Regulation (EEC) No 1907/90 should be extended until 1 July 2007.
Proposal for a
COUNCIL REGULATION
amending Regulation (EEC) No 1907/90, as regards the derogation on egg washing
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2771/75 of 29 October 1975 on the common organisation of the market in eggs[1], and in particular Article 2(2) thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) Article 6(4) of Council Regulation No 1907/90 of 26 June 1990 on certain marketing standards for eggs[2] lays down the criteria for the derogation enabling packing centres to continue to wash eggs until 31 December 2006.
(2) Such derogation has so far been granted to nine packing centres in Sweden and one in the Netherlands. Pending the application of Council Regulation (EC) No 1028/2006 of 19 June 2006 on marketing standards for eggs[3] on 1 July 2007, the transitional period for egg washing should be extended until that date.
(3) Regulation (EEC) No 1907/90 should therefore be amended accordingly,
HAS ADOPTED THIS REGULATION:
Article 1
In the first subparagraph of Article 6(4) of Regulation (EEC) No 1907/90, the date “31 December 2006” is replaced by the date “30 June 2007”.
Article 2
This Regulation shall enter into force on the seventh day following that of its publication in the Official Journal of the European Union .
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels,
For the Council
The President[pic][pic][pic][pic][pic][pic][pic][pic][pic]
[1] OJ L 282, 1.11.1975, p. 49. Regulation as last amended by Regulation (EC) No 679/2006. (OJ L 119, 4.5.2006, p. 1).
[2] OJ L 173, 6.7.1990, p. 5. Regulation as last amended by Regulation (EC) No 1039/2005 (OJ L 172, 5.7.2005, p. 1).
[3] OJ L 186, 7.7.2006, p. 1.
