Lists for the determination of the composition of the Fourth, Fifth and Sixth Chambers as from 11 October 2005
(2005/C 281/02)
At its meeting on 11 October 2005, the Court drew up the lists referred to in the second subparagraph of Article 11c(2) of the Rules of Procedure for the determination of the composition of the Chambers of three Judges as follows:
Fourth Chamber
Ms Colneric
Mr Cunha Rodrigues
Mr Lenaerts
Mr Juhász
Mr Ilešič
Mr Levits
Fifth Chamber
Mr Gulmann
Mr Schintgen
Ms Silva de Lapuerta
Mr Kūris
Mr Arestis
Mr Klučka
Sixth Chamber
Mr La Pergola
Mr Puissochet
Mr von Bahr
Mr Borg Barthet
Mr Lõhmus
Mr Ó Caoimh
--------------------------------------------------
