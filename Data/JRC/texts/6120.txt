Brussels, 5.4.2005
COM(2005) 130 final
2005/0025 (CNS)
Proposal for a
COUNCIL REGULATION
amending Regulation (EC, Euratom) No 2728/94 establishing a Guarantee Fund for external actions
(presented by the Commission)
EXPLANATORY MEMORANDUM
1. INTRODUCTION / MOTIVATION
The purpose of this amendment to Council Regulation (EC, Euratom) No 2728/94 of 31 October 1994 establishing a Guarantee Fund for external actions[1] (the “Regulation”) is to improve the rules of the provisioning mechanism of the Guarantee Fund (the “Fund”), i.e. the rules that determine how the Fund’s assets are brought in line with the target amount of the Fund.
Experience has shown that the present rules have often led to an over-provisioning of the Fund and therefore a sub-optimal use of budgetary funds. This problem has mainly been due to the use of uncertain forecast figures in the provisioning process. Furthermore, the present rules result in several transfers in and out of the Fund during one year, a situation that creates unnecessary administrative procedures involving not only the Commission services but also the two arms of the budgetary authority.
The proposed amendment is in line with the needs identified in the Comprehensive Report on the functioning of the Guarantee Fund (COM (2003) 604).
This amendment is limited to changes of the Fund’s provisioning mechanism.
2. THE FUND
The Regulation established a Fund for external actions so that the Community’s creditors could be reimbursed in the event of any default by the beneficiaries of loans granted or guaranteed by the Community. The main function of the Fund is to shield the Union budget from shocks due to defaults on loans or guaranteed loans covered by the Fund. The Fund is provisioned from the Union budget and has to be maintained at a certain percentage, the target rate (currently 9%), of the outstanding amount of the loans and loans guaranteed covered by the Fund.
The present provisioning mechanism is based on an ex-ante provisioning of individual Council decisions (Macro-financial assistance), individual forecasts of Commission decisions (Euratom loans) or a global annual forecast of planned loan signatures (EIB guaranteed lending).
3. THE NEW PROVISIONING MECHANISM
3.1. Basic principles of the new provisioning mechanism
The proposed new mechanism is based on an ex-post provisioning linked to the outstanding amount of loans and guaranteed loans, i.e. on actual net disbursements (i.e. disbursements minus amortizations minus cancellations).
In this context, one of the main advantages of the new provisioning system is its simplicity and its certainty about the budgetary requirements. Under the new mechanism, all figures necessary to determine the transfer from the budget to the Fund (or from the Fund to the budget) are known with certainty when the Preliminary Draft Budget (PDB) process starts and there is no need for forecasts (and the unavoidable forecast errors) and ex post budgetary adjustments.
In order to avoid the present practice of several transfers into the Fund during a year and a single payment from the Fund to the budget, only one net transfer between the Union budget and the Fund per year will occur under the new provisioning mechanism.
This simplification compares favourably with the present system which leads to numerous comparably high flows in and out of the Fund. For example, during the period 2000 – 2004, about EUR 894 million have been paid into the Fund in 13 transfers (the highest single amount being EUR 169 million) and about EUR 1.132 million have been paid back to budget in 5 annual payments resulting in a net transfer from the Fund to the Union budget of about EUR 238 million over this period. Compared to this total of 18 payments, the new system would have resulted in only 5 annual payments. According to simulations of the new mechanism assuming it had started in 1999, the highest annual single payment from the Union budget to the Fund would have been about EUR 88 million.
The single annual transfer is calculated as follows:
application of the target rate, currently 9%, to the amount of loans and guaranteed loans outstanding,
determination of the actual value of the Fund’s assets, including interest earned and losses caused by calls on the Fund,
transfer of the difference a) minus b) from the budget into the Fund (or to the budget in case of a resulting surplus in the Fund).
In a chronological view, the provisioning would take place in the following way:
Based on year-end outstanding amounts of loans and loans guaranteed in year “n-1” the required provisioning amount is calculated at the beginning of the year “n”. This amount is asked for in the PDB and a respective credit on the corresponding budget line is obtained at the beginning of the year “n+1”. Respectively, in case of a surplus of the Fund’s assets over the Fund’s target amount, an income to the budget will be entered in the PDB for the year “n+1”.
This implies a time lag between the moment when the year-end outstanding amount is known and the actual provisioning of about one year (January of year “n” to January/February of year “n+1”). This time lag is justified by the certainty of data and the improved transparency and predictability of the budgetary impact of the provisioning mechanism. This is not to be seen as an under-funding of the Fund. In fact, once inscribed in the draft budget for the following year, the amounts due can be considered as a sort of deferred payment by the Union budget similar to “capital not yet paid in” in the case of companies.
3.2. Mitigating the impact of major losses caused by loan defaults on the need to increase payments into the Fund - a smoothing mechanism
In case of major defaults a smoothing mechanism is foreseen, in order to maintain the Fund’s main purpose, i.e. to act as a shock absorber for the Union budget.
Losses caused by defaults on loans and guaranteed loans in year “n-1” would, without a specific rule, translate directly into an increase in the transfer to the Fund in the year “n+1”. The role of the Fund to act as a cushion to protect the budget from unexpected expenditure shocks could be compromised.
This problem is dealt within the proposal by the introduction of a smoothing mechanism to “spread” the impact of important losses due to defaults over several years. As smaller losses can be absorbed by the budget without problems, the smoothing mechanism would only be triggered if the losses would exceed a certain amount.
A threshold amount of EUR 100 million is proposed, based on the following reasoning:
According to estimates, based on various scenarios including a possible increase of the growth rate of loans and guaranteed loans during the next Financial Perspectives, the annual amount required for the “normal” provisioning would rarely exceed €100 million per year. If an amount of EUR 100 million to cover default–induced losses to the Fund (the maximum amount of losses that are absorbed by the “normal” provisioning mechanism without triggering the smoothing mechanism) or peaks in lending activity is added to this provisioning, the maximum annual budget transfer to the Fund would, under reasonable assumptions, be limited to an order of magnitude similar to the 2005 amount of the budget Reserve for the Fund (EUR 223 million).
An additional argument in favour of a smoothing mechanism setting in if losses exceed EUR 100 million is that the highest annual risk[2] for a given country is at present less than EUR 250 million, i.e. an amount which, in addition to the normal provisioning, would put too much strain on the budget for a given year. Obviously, it cannot be excluded either that more than one default could materialise during one year, or that the amounts would accumulate over time when a default situation persists for several years, which further supports the creation of a smoothing mechanism.
It should be noted that the smoothing mechanism works independently from the “normal” provisioning which would continue in its regular way. Over the smoothing period, each year the budget resources used for the smoothing transferred to the Fund portfolio (i.e. a maximum of EUR 100 million p.a.) would be deducted from the total amount to be recovered from the budget when the amount is effectively transferred to the Fund (together with the “normal” provision transfer) until the full amount is paid into the Fund. This solution has the advantage of being fully transparent by indicating which amount of the total annual provisioning of the Fund is due to the normal provisioning and which amount is due to the amortisation of the amounts to be recovered from the budget under the smoothing mechanism.
The length of the smoothing period would depend on the amount of the overall default induced loss or series of losses to be recovered from the budget. For example the repayment of a loss of EUR 350 million that occurred in 2010 would be spread over a period ending in the year 2015 (EUR 100 million in 2012-2014 and EUR 50 million in 2015). The amounts to be recovered from the budget under the smoothing mechanism would be treated like an asset of the Fund, i.e. be included in the Fund’s assets when the “normal” provisioning is calculated.
In order to avoid that the Fund, in the extremely unlikely case of a series of severe losses, would fall below a critical level of 50 % of the target rate, where there would be insufficient funds to ensure the role of the Fund as shock absorber for the budget, it is foreseen to maintain the provision (current Art. 5(2) would become Art. 5(3)) according to which the Commission would submit a report to the budgetary authority on exceptional measures to replenish the Fund.
3.3. Financial implications of the new provisioning mechanism and the financial statement
In the financial statement, an amount equal to the “normal” provisioning (= provisioning to cover the amount of outstanding loans and guaranteed loans plus up to EUR 100 million for losses) of up to about EUR 200 million has been indicated. This amount is the result of simulations that are based on the past observed growth rates of the amounts outstanding and the occurrence of default-induced losses of up to EUR 100 million p.a.
As explained in the previous section, in the very unlikely case that the smoothing mechanism would be triggered in case of major default-induced losses to the Fund, these “smoothing” amounts would be added to the amount to be paid by the budget under the “normal” provisioning. However, the design of the provisioning mechanism ensures that in any given year the default-induced amounts to be paid by the budget will never exceed EUR 100 million p.a. in addition to the “normal” provisioning and, therefore, the amount of EUR 200 million will not be exceeded under normal circumstances.
It should, however, be noted that important changes in the growth rate of the amounts of loans and loans guaranteed would have an impact on the budgetary resources needed for the Guarantee Fund. An example for such a situation could be a strong increase in the guaranteed lending activities of the European Investment Bank in the context of the new mandate for the next Financial Perspectives 2007-2013.
If a surplus of the Fund’s assets over the target amount materialises at the end of a given year “n-1”, e.g. due to high amortisations, high returns on the Fund’s portfolio or the recovery of outstanding defaults, this amount would be paid to a special budget line at the beginning of the year “n+1” as a revenue in the same way as a provisioning. The Member States would in this case not be penalised compared to the present provisioning mechanism as the surplus amount would be entered in year “n” in the PDB for the year “n+1” as a revenue.
4. TRANSITION PERIOD
During the first year of its application, i.e. as of 1 January 2006, the new provisioning mechanism implies that no budgetary transfers will occur (change from an ex-ante to an ex-post approach). However, as it is possible that new loan decisions occur towards the very end of 2005, payments between the budget and the Fund that could, due to administrative delays, under such conditions only be made in the first year of the application of the new provisioning mechanism should still be carried out as foreseen under the current Regulation.
It should also be noted that the amendment can for obvious budgetary reasons only take place at the beginning of a year. Should the 1 January 2006 starting date not be feasible, the amendment should take effect at the first January of the year following its adoption by the Council.
5. CONCLUSION
The main improvements for the Fund’s provisioning mechanism are:
- The improvement of the efficiency of the use of budgetary means and reducing to one the number of annual transfers between the Union Budget and the Fund;
- A provisioning based on the variations of the observed net-disbursements, thus creating more transparency and improving the precision of the budgetary programming.
2005/0025 (CNS)
Proposal for a
COUNCIL REGULATION
amending Regulation (EC, Euratom) No 2728/94 establishing a Guarantee Fund for external actions
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 308 thereof,
Having regard to the Treaty establishing the European Atomic Energy Community, and in particular Article 203 thereof,
Having regard to the proposal from the Commission[3],
Having regard to the opinion of the European Parliament[4],
Having regard to the opinion of the Court of Auditors[5],
Whereas:
(1) The efficiency of the use made of budgetary means reserved for the Guarantee Fund established by Council Regulation (EC, Euratom) No 2728/94[6] should be improved and the administrative work related to the budgetary management of the Guarantee Fund should be reduced.
(2) The transparency and programming of budgetary transactions in relation to the provisioning of the Guarantee Fund should be enhanced.
(3) The 2006 reserve amount under the Financial Perspectives 2000 - 2006 is fixed at EUR 229 million[7] (current prices) according to point 15 of the Interinstitutional Agreement of 6 May 1999 between the European Parliament, the Council and the Commission on budgetary discipline and improvement of the budgetary procedure[8].
(4) Should the reserve created to endow the Guarantee Fund be abolished for the Financial Perspectives 2007–2013, the funding of the Guarantee Fund should be provided for as an obligatory expenditure from the general budget of the European Union.
(5) The main function of the Guarantee Fund, namely to shield the general budget of the European Union against shocks due to defaults on loans or guaranteed loans covered by the Fund, should be maintained.
(6) Regulation (EC, Euratom) No 2728/94 should therefore be amended accordingly.
(7) The Treaties do not provide, for the adoption of this Regulation, powers, other than those in Article 308 of the EC Treaty and Article 203 of the Euratom Treaty,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC, Euratom) No 2728/94 is hereby amended as follows:
1. In Article 2, the first indent is replaced by the following:
‘– one annual payment from the general budget of the European Union pursuant to Articles 4 and 5,’;
2. In Article 3, the third paragraph is replaced by the following:
‘On the basis of the year-end “ n-1 ” difference between the target amount and the value of the Guarantee Fund’s net assets, calculated at the beginning of the year “n” , any surplus shall be paid in one transaction to a special heading in the statement of revenue in the general budget of the European Union of the year “n+1” .’;
3. Article 4 is replaced by the following:
“Article 4
Based on the year-end “n-1” difference between the target amount and the value of the Guarantee Fund’s net assets, calculated at the beginning of the year “ n” , the required provisioning amount shall be paid into the Fund in one transaction in the year “n+1” from the general budget of the European Union.”;
4. Article 5 is replaced by the following:
“Article 5
1. If, as a result of one or more defaults, the activation of guarantees during year “n-1” exceeds EUR 100 million, the amount exceeding EUR 100 million shall be paid back into the Guarantee Fund in annual tranches starting in year “n+1” and continuing over the following years until full repayment (smoothing mechanism). The size of the annual tranche is the lesser of the following:
- EUR 100 million or
- the remaining amount due in accordance with the smoothing mechanism.
2. The calculations based on this smoothing mechanism shall be made separately from the calculations referred to in Article 3, third paragraph, and in Article 4. Nevertheless, they will together result in one annual transfer. The amounts to be paid from the general budget of the European Union under this smoothing mechanism shall be treated as net assets of the Fund for the calculation pursuant to Articles 3 and 4.
3. If, as a result of the activation of guarantees on one or more major defaults, resources in the Fund fall below 50 % of the target amount, the Commission shall submit a report on exceptional measures that might be required to replenish the Fund.”
5. The Annex is deleted.
Article 2
This Regulation shall enter into force on 1 January 2006.
This Regulation shall not apply to ongoing Community borrowing or lending operations or guarantees to financial bodies of the type envisaged in point (a) of paragraph 1 of the Annex to Regulation (EC, Euratom) No 2728/94, if those operations or guarantees have been initiated or granted during the calendar year 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels,
For the Council
The President
FINANCIAL STATEMENT |
DATE: |
1. | BUDGET HEADING: Heading 6 (for 2006) and Heading 4: The EU as a global partner (as from 2007) | APPROPRIATIONS: |
2. | TITLE: Loan guarantees for third countries |
3. | LEGAL BASIS: Proposal for a Council Regulation amending Regulation (EC, Euratom) No 2728/94 establishing a Guarantee Fund for external actions |
4. | AIMS: To endow the Guarantee Fund for external actions |
5. | FINANCIAL IMPLICATIONS | YEAR n (EUR million) | FOLLOWING FINANCIAL YEARS (EUR million) |
5.0 | EXPENDITURE Item 01 04 01 14 Payments to the Guarantee Fund in respect of new operations | 0 | 200 |
5.1 | REVENUE Item 302 Surplus own resources resulting from repayment of the surplus from the Guarantee Fund for external actions | p.m. | p.m. |
[n] |
5.0.1 | ESTIMATED EXPENDITURE | 0 | 200 |
5.1.1 | ESTIMATED REVENUE | p.m. | p.m. |
5.2 | METHOD OF CALCULATION: Normal provisioning for operations is estimated to an amount of EUR 100 million. Another amount of EUR 100 million is added in order to cover major defaults resulting of the activation of guarantees. |
6.0 | CAN THE PROJECT BE FINANCED FROM APPROPRIATIONS ENTERED IN THE RELEVANT CHAPTER OF THE CURRENT BUDGET? | NO |
6.1 | CAN THE PROJECT BE FINANCED BY TRANSFER BETWEEN CHAPTERS OF THE CURRENT BUDGET? | NO |
6.2 | WILL A SUPPLEMENTARY BUDGET BE NECESSARY? | NO |
6.3 | WILL APPROPRIATIONS NEED TO BE ENTERED IN FUTURE BUDGETS? | YES |
OBSERVATIONS: Year 2006 If the proposal is adopted before 31.12.05, the amount is zero. Otherwise, the actual regulation applies and therefore the Guarantee Reserve is set at EUR 229 million (current prices). |
[1] OJ L 293, 12.11.1994, p. 1. Regulation as amended by Regulation (EC, Euratom) No 1149/1999 (OJ L 139, 2.6.1999, p. 1) and by Regulation (EC, Euratom) No 2273/2004 (OJ L 396, 31.12.2004, p. 28).
[2] As defaults do usually not lead to an acceleration of the whole credit amount, the annual risk can be defined as the amounts due in one year for amortisation and interest.
[3] OJ C , , p. .
[4] OJ C , , p. .
[5] OJ C , , p. .
[6] OJ L 293, 12.11.1994, p. 1. Regulation as last amended by Regulation (EC, Euratom) No 2273/2004 (OJ L 396, 31.12.2004, p.28).
[7] COM(2004) 837 final
[8] OJ C 172, 18.6.1999, p. 1.
