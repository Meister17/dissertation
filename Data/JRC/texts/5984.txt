Common Position (EC) No 19/2005
adopted by the Council on 17 February 2005
with a view to the adoption of the Regulation (EC) No …/2005 of the European Parliament and of the Council of … on controls of cash entering or leaving the Community
(2005/C 144 E/01)
THE EUROPEAN PARLIAMENT AND THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Articles 95 and 135 thereof,
Having regard to the proposal from the Commission [1],
Having regard to the opinion of the European Economic and Social Committee,
Acting in accordance with the procedure referred to in Article 251 of the Treaty [2],
Whereas:
(1) One of the Community's tasks is to promote harmonious, balanced and sustainable development of economic activities throughout the Community by establishing a common market and an economic and monetary union. To that end the internal market comprises an area without internal frontiers in which the free movement of goods, persons, services and capital is ensured.
(2) The introduction of the proceeds of illegal activities into the financial system and their investment after laundering are detrimental to sound and sustainable economic development. Accordingly, Council Directive 91/308/EEC of 10 June 1991 on prevention of the use of the financial system for the purpose of money laundering [3] introduced a Community mechanism to prevent money laundering by monitoring transactions through credit and financial institutions and certain types of professions. As there is a risk that the application of that mechanism will lead to an increase in cash movements for illicit purposes, Directive 91/308/EEC should be supplemented by a control system on cash entering or leaving the Community.
(3) At present such control systems are applied by only a few Member States, acting under national legislation. The disparities in legislation are detrimental to the proper functioning of the internal market. The basic elements should therefore be harmonised at Community level to ensure an equivalent level of control on movements of cash crossing the borders of the Community. Such harmonisation should not, however, affect the possibility for Member States to apply, in accordance with the existing provisions of the Treaty, national controls on movements of cash within the Community.
(4) Account should also be taken of complementary activities carried out in other international fora, in particular of the Financial Action Task Force on Money Laundering (FATF) which was established by the G7 Summit held in Paris in 1989. Special Recommendation IX of 22 October 2004 of the FATF calls on governments to take measures to detect physical cash movements, including a declaration system or other disclosure obligation.
(5) Accordingly, cash carried by any natural person entering or leaving the Community should be subject to the principle of obligatory declaration. This principle would enable the customs authorities to gather information on such cash movements and, where appropriate, transmit that information to other authorities. Customs authorities are present at the borders of the Community, where controls are most effective, and some have already built up practical experience in the matter. Use should be made of Council Regulation (EC) No 515/97 of 13 March 1997 on mutual assistance between the administrative authorities of the Member States and cooperation between the latter and the Commission to ensure the correct application of the law on customs and agricultural matters [4]. This mutual assistance should ensure both the correct application of cash controls and the transmission of information that might help to achieve the objectives of Directive 91/308/EEC.
(6) In view of its preventive purpose and deterrent character, the obligation to declare should be fulfilled upon entering or leaving the Community. However, in order to focus the authorities' action on significant movements of cash, only those movements of EUR 10000 or more should be subject to such an obligation. Also, it should be specified that the obligation to declare applies to the natural person carrying the cash, regardless of whether that person is the owner.
(7) Use should be made of a common standard for the information to be provided. This will enable competent authorities to exchange information more easily.
(8) It is desirable to establish the definitions needed for a uniform interpretation of this Regulation.
(9) Information gathered under this Regulation by the competent authorities should be passed on to the authorities referred to in Article 6(1) of Directive 91/308/EEC.
(10) Where there are indications that the sums of cash are related to any illegal activity, associated with the movement of cash, as referred to in Directive 91/308/EEC, information gathered under this Regulation by the competent authorities may be passed on to competent authorities in other Member States and/or to the Commission. Similarly, provision should be made for certain information to be transmitted whenever there are indications of cash movements involving sums lower than the threshold laid down in this Regulation.
(11) Competent authorities should be vested with the powers needed to exercise effective control on movements of cash.
(12) The powers of the competent authorities should be supplemented by the Member States' obligation to lay down penalties. However, penalties should only be imposed for failure to make a declaration in accordance with this Regulation.
(13) Since the objective of this Regulation cannot be sufficiently achieved by the Member States and can therefore, by reason of the transnational scale of money-laundering in the internal market, be better achieved at Community level, the Community may adopt measures, in accordance with the principle of subsidiarity as set out in Article 5 of the Treaty. In accordance with the principle of proportionality, as set out in that Article, this Regulation does not go beyond what is necessary in order to achieve that objective.
(14) This Regulation respects the fundamental rights and observes the principles recognised, in particular, by the Charter of Fundamental Rights of the European Union,
HAVE ADOPTED THIS REGULATION:
Article 1
Objective
1. This Regulation complements the provisions of Directive 91/308/EEC concerning transactions through financial and credit institutions and certain professions by laying down harmonised rules for the control, by the competent authorities, of cash entering or leaving the Community.
2. This Regulation is without prejudice to national measures to control cash movements within the Community, where such measures are taken in accordance with Article 58 of the Treaty.
Article 2
Definitions
For the purposes of this Regulation:
1. "competent authorities" means the customs authorities of the Member States or any other authorities empowered by Member States to apply this Regulation;
2. "cash" means:
(a) bearer negotiable instruments including monetary instruments in bearer form such as travellers cheques, negotiable instruments (including cheques, promissory notes and money orders) that are either in bearer form, endorsed without restriction, made out to a fictitious payee, or otherwise in such form that title thereto passes upon delivery and incomplete instruments (including cheques, promissory notes and money orders) signed, but with the payee's name omitted;
(b) currency (banknotes and coins that are in circulation as a medium of exchange).
Article 3
Obligation to declare
1. Any natural person entering or leaving the Community and carrying cash of a value of EUR 10000 or more shall declare that sum to the competent authorities of the Member State through which he is entering or leaving the Community in accordance with this Regulation. The obligation to declare shall not have been fulfilled if the information provided is incorrect or incomplete.
2. The declaration referred to in paragraph 1 shall contain details of:
(a) the declarant, including full name, date and place of birth and nationality;
(b) the owner of the cash;
(c) the intended recipient of the cash;
(d) the amount and nature of the cash;
(e) the provenance and intended use of the cash;
(f) the transport route;
(g) the means of transport.
3. Information shall be provided in writing, orally or electronically, to be determined by the Member State referred to in paragraph 1. However, where the declarant so requests, he shall be entitled to provide the information in writing. Where a written declaration has been lodged, an endorsed copy shall be delivered to the declarant upon request.
Article 4
Powers of the competent authorities
1. In order to check compliance with the obligation to declare laid down in Article 3, officials of the competent authorities shall be empowered, in accordance with the conditions laid down under national legislation, to control natural persons, their baggage and their means of transport.
2. In the event of failure to comply with the obligation to declare laid down in Article 3, cash may be detained by administrative decision in accordance with the conditions laid down under national legislation.
Article 5
Recording and processing of information
1. The information obtained under Article 3 and/or Article 4 shall be recorded and processed by the competent authorities of the Member State referred to in Article 3(1) and shall be made available to the authorities referred to in Article 6(1) of Directive 91/308/EEC of that Member State.
2. Where it appears from the controls provided for in Article 4 that a natural person is entering or leaving the Community with sums of cash lower than the threshold fixed in Article 3 and where there are indications of illegal activities associated with the movement of cash, as referred to in Directive 91/308/EEC, that information, the full name, date and place of birth and nationality of that person and details of the means of transport used may also be recorded and processed by the competent authorities of the Member State referred to in Article 3(1) and be made available to the authorities referred to in Article 6(1) of Directive 91/308/EEC of that Member State.
Article 6
Exchange of information
1. Where there are indications that the sums of cash are related to any illegal activity associated with the movement of cash, as referred to in Directive 91/308/EEC, the information obtained through the declaration provided for in Article 3 or the controls provided for in Article 4 may be transmitted to competent authorities in other Member States.
Regulation (EC) No 515/97 shall apply mutatis mutandis.
2. Where there are indications that the sums of cash involve the proceeds of fraud or any other illegal activity adversely affecting the financial interests of the Community, the information shall also be transmitted to the Commission.
Article 7
Exchange of information with third countries
In the framework of mutual administrative assistance, the information obtained under this Regulation may be communicated by Member States or by the Commission to a third country, subject to the consent of the competent authorities which obtained the information pursuant to Article 3 and/or Article 4 and to compliance with the relevant national and Community provisions on the transfer of personal data to third countries. Member States shall notify the Commission of such exchanges of information where particularly relevant for the implementation of this Regulation.
Article 8
Penalties
1. Each Member State shall introduce penalties to apply in the event of failure to comply with the obligation to declare laid down in Article 3. Such penalties must be effective, proportionate and dissuasive.
2. Member States shall notify the Commission, at the latest by … [5], of the penalties applicable in the event of failure to comply with the obligation to declare laid down in Article 3.
Article 9
Evaluation
Four years after its entry into force, the Commission shall submit to the European Parliament and the Council a report on the application of this Regulation.
Article 10
Entry into force
This Regulation shall enter into force on the twentieth day following that of its publication in the Official Journal of the European Union.
It shall apply from … [6].
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels,
For the European Parliament
The President
…
For the Council
The President
…
[1] OJ C 227 E, 24.9.2002, p. 574.
[2] Opinion of the European Parliament of 15 May 2003 (OJ C 67 E, 17.3.2004, p. 259), Council Common Position of 17 February 2005 and Position of the European Parliament of … (not yet published in the Official Journal).
[3] OJ L 166, 28.6.1991, p. 77. Directive as amended by Directive 2001/97/EC of the European Parliament and of the Council (OJ L 344, 28.12.2001, p. 76).
[4] OJ L 82, 22.3.1997, p. 1. Regulation as amended by Regulation (EC) No 807/2003 (OJ L 122, 16.5.2003, p. 36).
[5] 18 months after the entry into force of this Regulation.
[6] 18 months after the date of entry into force of this Regulation.
--------------------------------------------------
STATEMENT OF THE COUNCIL'S REASONS
I. INTRODUCTION
On 25 June 2002, the Commission submitted to the Council its Report on controls on cross-border cash movements together with a proposal for a Regulation of the European Parliament and the Council on the prevention of money laundering by means of customs cooperation, based on Article 135 of the Treaty [1] [2].
The European Parliament delivered its opinion at first reading on 15 May 2003 [3], adopting 23 amendments to the proposal.
The Commission presented to the Council on 3 July 2003 an amended proposal on the above subject [4].
On 17 February 2005, the Council adopted its common position in accordance with Article 251(2) of the Treaty.
II. OBJECTIVE
The proposal aims to introduce an obligation for natural persons to declare cash movements above a specific threshold at the EU's external borders. In addition, the proposal aims at enhancing border controls of cash movements and at improving exchanges of information between the relevant authorities.
III. ANALYSIS OF THE COMMON POSITION
1. General
The Council's common position, in accordance with the Commission proposal, aims to introduce harmonized border controls of cash movements, thereby supplementing Directive 91/308/EEC [5] and ensuring the proper functioning of the internal market through the elimination of the current disparities in national legislation.
2. Position of the Council in detail
The Council took the following positions on the amendments adopted by the European Parliament:
(a) Legal basis
In line with the opinion of the European Parliament (amendment 2) the Council added Article 95 as a legal basis to the proposal. The Council considered that the main feature of the draft Regulation should not be the prevention of money laundering (for which a different legal basis would be required) but the introduction of harmonised border controls for cash movements, for which Article 95 of the Treaty, governing the approximation of laws, regulations and administrative provisions of the Member States in connection with the functioning of the internal market, constitutes the appropriate legal basis.
(b) Transformation into a Directive
The Council could not accept the EP amendments linked to the transformation of the proposal into a Directive (amendments 1, 8, 9, 18, 20, 21, 22 and 23), considering that a sufficient degree of harmonisation in a relatively short period of time can only be achieved by means of a Regulation, which is generally applicable, binding in its entirety and directly applicable in all Member States.
(c) Obligatory declaration
In its common position, the Council supported the Commission's proposal for a system of obligatory declaration. The Council did not favour the idea of allowing Member States to choose between a declaration or a disclosure system as suggested by the European Parliament (amendments 2, 3, 4, 5, 6, 7, 8, 12, 14, 18 and 19). Such a choice between two systems would be contrary to the uniform Community-wide application of the measures proposed. However, the Council introduced a degree of flexibility by leaving the choice to Member States to introduce an obligation for written, oral or electronic declarations. Consequently, the Council agreed to delete the declaration form contained in the Annex to the proposal and to insert a provision on the information to be provided by the written, oral or electronic declarations (see also below point 3.b)).
(d) Threshold for declaration
The Council opted for a threshold of EUR 10000 which is lower than the thresholds foreseen in the Commission proposal and in the European Parliament's Opinion (amendments 3 and 7). The lower threshold reflects the fact that many Member States' legislation currently foresees considerably lower thresholds and that the level initially proposed (EUR 15000) would result in a severe decrease in the intensity of controls in those Member States.
The lower threshold also sends a clear signal at international level of the Council's willingness to adopt measures to monitor cash movements by introducing a strict and uniform limit throughout the Community, which is easy to implement and clearly recognizable by travellers entering or leaving the Community.
(e) Powers of the competent authorities
Concerning the powers of the competent authorities, the Council agreed with the European Parliament (amendments 10 and 17) that this provision should be placed in a different context and therefore moved it to the Article immediately following the provision on the obligation to declare. As to the content of this provision, the Council considered that the empowerment of national authorities should be done in accordance with the conditions laid down under national legislation. In addition, national authorities should also be empowered to control the means of transport in order to check compliance with the obligation to declare. However, the Council did not maintain the maximum time period of three days for the detention of cash as foreseen in the proposal and supported in amendment 11, considering that such a limitation in time would not allow the necessary flexibility for authorities to carry out controls and subsequent investigations in order to determine whether a penal procedure needed to be opened in a specific case.
(f) Definition of "cash"
The Financial Action Task Force on Money Laundering (FATF) adopted, on 22 October 2004, its Special Recommendation IX on cash couriers. This Recommendation, agreed at international level, contains a definition of "cash", which the Council inserted in the present draft Regulation in order to guarantee the widest possible coherence of rules at Community and international levels. As requested in amendment 13, the text extends the definition of cash to cover a wider range of cheques than initially proposed.
(g) Exchange of information
The Council clarified and restructured the provisions on exchange of information between authorities (amendment 15):
- firstly, it is clarified that the information obtained by declarations or controls shall be recorded and processed by the competent authorities of a Member State and, within that same Member State, be made available to the Financial Intelligence Unit (FIU), the latter point being also explicitly included in the FATF Special Recommendation IX. In the case of persons entering or leaving the Community with less than EUR 10000 but where there are indications of illegal activities, certain information on this person may also be recorded and processed by the competent authorities within one Member State and be made available to that Member State's FIU,
- secondly, information obtained by declarations or controls may be exchanged between Member States. For this exchange of information between Member States, Council Regulation (EC) No 515/97 of 13 March 1997 on mutual assistance between the administrative authorities of the Member States and cooperation between the latter and the Commission to ensure the correct application of the law on customs and agricultural matters applies mutatis mutandis,
- finally, information may be exchanged with third countries, in the framework of a mutual administrative assistance agreement. However, the communication of information is subject to the consent of the authority that collected the information initially and to personal data protection provisions. The Commission should be notified of such exchanges of information where particularly relevant to the implementation of the Regulation.
(h) Joint database
The Council did not accept the idea of forwarding the information obtained to a database jointly administered by the Member States and held by the European Police Office (Europol) (amendment 16). The Council considered that such a provision would not be covered by the legal basis of the draft Regulation.
(i) Report by the Commission
In its common position, the Council accepted the idea reflected in amendment 22 and introduced a provision requiring the Commission to submit a report to the European Parliament and the Council four years after the entry into force of the Regulation.
3. New elements introduced by the Council
In addition to the items on which the European Parliament has delivered its opinion and on which the Council common position is reflected above, the Council inserted the following new elements into the proposal:
(a) Scope of the Regulation
Intra-Community controls of cash may be maintained where these measures are in conformity with the Treaty.
In addition, the Council adapted the geographical coverage of the Regulation considering that, for the sake of transparency for travellers and in order to facilitate the application of the Regulation by the competent authorities, the control of cash movements should be carried out when a natural person is entering or leaving the Community territory. This solution would also ensure a parallel geographical application of Directive 91/308/EEC and the present draft Regulation.
(b) Declaration form
The uniform declaration form proposed by the Commission was not accepted by the Council. Instead, the Council preferred to specify the data to be provided in the declaration. This intends to limit the administrative burden for the competent authorities to the strict minimum when collecting the information from travellers whilst ensuring that a minimum of information on cash movements is collected and, subsequently, is available for exchange with other authorities.
(c) Copy of a written declaration
In the case of a written declaration by the declarant, the Council inserted a provision ensuring that the declarant shall be entitled to receive an endorsed copy of the declaration upon request.
(d) Penalties
The Council simplified the provision on penalties aligning it with similar provisions in comparable legal acts [6]. Member States should therefore introduce penalties of an effective, dissuasive and proportionate nature for infringements of the obligation to declare cash movements when crossing the external Community borders. The Council accepted amendment 19 concerning the extension of the time limit for notification of the applicable penalties to the Commission. It set a period of 18 months after the entry into force of the Regulation.
IV. CONCLUSION
The objective of the proposed Regulation to introduce harmonized border controls of cash movements and to strengthen cooperation between competent authorities is supported by the Council's common position. As there is a risk that the application of tightened controls in the financial sector following the adoption of Directive 91/308/EEC will lead to an increase in cash movements for illicit purposes, the Community underlines its commitment to combating the problem of illegal cash movements through the introduction of an obligation for natural persons to declare cash movements above a threshold of EUR 10000 at the Community borders, whilst avoiding possible negative effects on the functioning of the internal market by applying these measures, by way of a Regulation, in a harmonised way throughout the Community.
[1] OJ C 227 E, 24.9.2002, p. 574.
[2] COM(2002) 328 final.
[3] OJ C 67 E, 17.3.2004, p. 259.
[4] Doc. 11151/03 UD 67 EF 35 ECOFIN 203 CRIMORG 51 CODEC 948.
[5] Council Directive 91/308/EEC of 10 June 1991 on prevention of the use of the financial system for the purpose of money laundering (OJ L 166, 28.6.1991, p. 77).
[6] See, for example, Article 18 of Council Regulation (EC) No 1383/2003 of 22 July 2003 concerning customs action against goods suspected of infringing certain intellectual property rights and the measures to be taken against goods found to have infringed such rights (OJ L 196, 2.8.2003, p. 7).
--------------------------------------------------
