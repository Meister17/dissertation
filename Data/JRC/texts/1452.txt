Guideline of the European Central Bank
of 21 June 2001
amending Guideline ECB/2000/1 of 3 February 2000 on the management of the foreign reserve assets of the European Central Bank by the national central banks and the legal documentation for operations involving the foreign reserve assets of the European Central Bank
(ECB/2001/5)
(2001/526/EC)
THE GOVERNING COUNCIL OF THE EUROPEAN CENTRAL BANK,
Having regard to the Treaty establishing the European Community (hereinafter referred to as the "Treaty") and in particular to the third indent of Article 105(2) thereof and to the third indent of Article 3.1 and Articles 12.1, 14.3 and 30.6 of the Statute of the European System of Central Banks and of the European Central Bank (hereinafter referred to as the "Statute"),
Whereas:
(1) Pursuant to Guideline ECB/2000/1 of 3 February 2000 on the management of the foreign reserve assets of the European Central Bank by the national central banks and the legal documentation for operations involving the foreign reserve assets of the European Central Bank(1), each national central bank (NCB) of a participating Member State shall carry out operations involving the foreign reserve assets of the European Central Bank (ECB) as an agent for the ECB.
(2) The ECB considers it necessary for each NCB acting as agent for the ECB to apply minimum standards for the conduct of NCBs in the management of the ECB's foreign reserve assets. Article 38.1 of the Statute provides that members of the governing bodies and the staff of the ECB and the NCBs shall be required, even after their duties have ceased, not to disclose information of the kind covered by the obligation of professional secrecy.
(3) The ECB considers it appropriate that legal documentation for operations involving the ECB's foreign reserve assets be executed with counterparties only on a multibranch basis.
(4) In accordance with Articles 12.1 and 14.3 of the Statute, ECB guidelines form an integral part of Community law,
HAS ADOPTED THIS GUIDELINE:
Article 1
A new Article 3a shall be inserted in Guideline ECB/2000/1 as follows: "Article 3a
Minimum standards for the conduct of NCBs in the management of the ECB's foreign reserve assets
When acting as an agent for the ECB in the management of the ECB's foreign reserve assets, each NCB shall ensure that its internal rules related to such management, whether they are codes of conduct, personnel statutes or any other type of rules (internal rules) comply with the minimum standards for the conduct of the NCBs in the management of the ECB's foreign reserve assets, attached as Annex 4 to this Guideline."
Article 2
A new Annex 4 shall be added to Guideline ECB/2000/1 as follows:
"ANNEX 4
Minimum standards for the conduct of NCBs in the management of the ECB's foreign reserve assets
1. SCOPE
The internal rules of the NCBs should contain binding provisions ensuring compliance for all NCBs' activities or operations involving the ECB's foreign reserve assets with these minimum standards.
The rules contained herein should be applicable to the members of NCBs' decision-making bodies and to all NCBs' employees involved in the management of the ECB's foreign reserve assets (such employees and the members of decision-making bodies are hereinafter collectively referred to as "NCBs' employees").
These minimum standards are not intended to exclude or prejudice the operation of any other more stringent provisions laid down in NCBs' internal rules and that apply to NCBs' employees, and these minimum standards shall also be without prejudice to the operation of Article 38 of the Statute of the European System of Central Banks and of the European Central Bank, which provides that the members of the governing bodies and the staff of the ECB and the NCBs shall be required, even after their duties have ceased, not to disclose information of the kind covered by the obligation of professional secrecy.
2. MANAGEMENT SUPERVISION OF OPERATIONS WITH MARKET COUNTERPARTIES
Control of the activities of all NCBs' employees engaged in operations with market counterparties is the responsibility of NCB management. The authorisations and responsibilities according to which market operators and support staff should discharge their duties should be clearly set out in writing.
3. AVOIDANCE OF POTENTIAL CONFLICTS OF INTEREST
NCBs' employees are required to abstain from being a party to any economic or financial transactions that may hinder their independence and impartiality.
NCBs' employees should avoid any situation liable to give rise to a conflict of interest.
4. PROHIBITION OF INSIDER TRADING
NCBs should not allow NCBs' employees to conduct insider trading, nor to pass non-public confidential information obtained at the workplace on to third parties. Moreover, NCBs' employees may not use non-public ESCB related knowledge obtained at the workplace whenever they conduct private financial transactions.
Insider trading is defined as the activity of any person who, by virtue of his or her employment, profession or duties, has access to certain information of a precise nature that could be relevant to the management of the ECB's foreign reserve assets before it becomes public and takes advantage of that information with full knowledge of the facts by acquiring or disposing of, either for his or her own account or for the account of a third party, either directly or indirectly, assets (including transferable securities) or rights (including rights under derivatives contracts) to which that information is closely related.
The NCBs should have appropriate arrangements in place to enable their management and/or compliance officers to check that financial transactions entered into by NCBs' employees are in conformity with this rule, subject to applicable national laws and labour-market practices. Moreover, these arrangements should be strictly limited to compliance checks regarding those kinds of transactions that could be relevant to the management of the ECB's foreign reserve assets. Such compliance checks should only be carried out where there are compelling reasons for doing so.
5. ENTERTAINMENT AND GIFTS
NCBs' employees may not solicit gifts and entertainment from third parties in the course of the management of the ECB's foreign reserve assets, nor may NCBs' employees accept gifts and entertainment in excess of a customary or negligible amount, whether financial or non-financial, that may hinder their independence and impartiality.
NCBs' employees should be required to inform their management about any attempt by a counterparty to offer them such gifts or entertainment."
Article 3
Annex 3 to Guideline ECB/2000/1 shall be replaced as follows:
"ANNEX 3
Standard agreements for collateralised and over-the-counter derivatives operations
1. All collateralised operations involving the foreign reserve assets of the ECB comprising repurchase agreements, reverse repurchase agreements, buy/sell-back agreements and sell/buy-back agreements are to be documented under the following standard agreements, in such form as may be approved or amended by the ECB from time to time: for counterparties organised or incorporated under French law, the "Convention-cadre relative aux opérations de pension livrée"; for counterparties organised or incorporated under German law, the "Rahmenvertrag für echte Pensionsgeschäfte"; for counterparties organised or incorporated under the laws of a jurisdiction outside France, Germany and the United States, the "PSA/ISMA Global Master Repurchase Agreement"; and for counterparties organised or incorporated under US federal or State laws, "The Bond Market Association Master Repurchase Agreement".
2. All over-the-counter derivative operations involving the foreign reserve assets of the ECB are to be documented under the following standard agreements, in such form as may be approved or amended by the ECB from time to time: for counterparties organised or incorporated under French law, the "Convention-cadre relative aux opérations de marché à terme"; for counterparties organised or incorporated under German law, the "Rahmenvertrag für echte Finanztermingeschäfte"; for counterparties organised or incorporated under the laws of a jurisdiction outside France, Germany and the United States, the "1992 International Swaps and Derivatives Association Master Agreement" (Multicurrency - cross-border, English law version); and for counterparties organised or incorporated under US federal or State laws, the "1992 International Swaps and Derivatives Association Master Agreement" (Multi-currency - cross-border, New York law version)."
Article 4
Final provisions
This Guideline is addressed to the NCBs.
By 16 August 2001 at the latest, NCBs shall forward details of the texts and means by which they intend to comply with the minimum standards for their conduct in the management of the ECB's foreign reserve assets, as required by Article 3a of Guideline ECB/2000/1.
This Guideline shall enter into force on 21 June 2001.
This Guideline shall be published in the Official Journal of the European Communities.
Done at Frankfurt am Main, 21 June 2001.
On behalf of the Governing Council of the ECB
Willem F. Duisenberg
(1) OJ L 207, 17.8.2000, p. 24.
