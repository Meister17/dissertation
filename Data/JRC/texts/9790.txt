[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 28.4.2006
COM(2006) 186 final
REPORT FROM THE COMMISSION
Quality of petrol and diesel fuel used for road transport in the European Union: Third annual report (Reporting year 2004)
1. EXECUTIVE SUMMARY
Directive 98/70/EC[1] sets technical specifications on health and environmental grounds for fuels to be used for vehicles equipped with positive-ignition and compression-ignition engines. Directive 2003/17/EC[2], amending Directive 98/70/EC, requires a further reduction of the sulphur content of petrol and diesel fuels.
Significant violations of the fuel specification can lead to increased emissions and might even damage the engine and exhaust after-treatment systems. In order to ensure compliance with the fuel quality standards required under this Directive, Member States are required to introduce fuel quality monitoring systems.
Article 8 of Directive 98/70/EC, as amended by Article 1(5) of Directive 2003/17/EC, requires the Commission to publish annually, a report on the fuel quality in the different Member States. This third Commission Report summarises briefly Member States’ submissions, on the quality of petrol and diesel, as well as the volumes sold, for the year 2004[3]. All Member States except France submitted national reports for 2004.
The monitoring of fuel quality in 2004 shows that the specifications for petrol and diesel laid down in Directive 98/70/EC are in general met and few violations were identified. For petrol the main parameters of concern were again research octane number (RON, 34+ samples), summer vapour pressure (DVPE, 43+ samples) and distillation - evaporation at 100°C (17+ samples). For diesel the parameters of concern were sulphur content (22 samples), distillation 95% point (24 samples), cetane number (7+ samples) and density (5+ samples). Although several Member States reported non-compliant samples, far fewer samples exceeded the limit values (and the limits of tolerance for the test methods) compared to previous years. However, both the Czech Republic and Poland reported significant numbers of samples exceeding limit values. The Commission has not identified any negative repercussions on vehicle emissions or engine functioning related to these exceedances but continues to urge Member States to take action in order to ensure full compliance. Most are doing so already, and details of the action taken by Member States with regard to non-compliance are included, where provided, in the individual country chapters of the detailed report for 2004[4]. The Commission will continue insisting that Member States ensure full compliance with the fuel quality requirements laid down in the Directive.
For the abatement of air pollution and the introduction of new engine technology it is important to note that the share of <10 ppm and <50 ppm sulphur fuels increased significantly from 2001 to 2004 for EU-15. EU-10 sulphur content is significantly higher and this leads to a slight increase in average sulphur between 2003 and 2004 for the whole EU. Overall a general trend towards lower sulphur content in petrol and diesel fuel can be identified, as shown in Table 1.
Table 1: Annual trend in average sulphur content in petrol and diesel fuels for the EU
EU 15 average sulphur content, ppm | EU25 | EU10 | EU15* |
[pic] | [pic] |
Fuel Type | % | Fuel Type | % |
1 | Unleaded petrol min. RON=91 | 0.7% |
2 | Unleaded petrol min. RON=91 (<50 ppm S) | 0.1% | 13 | Diesel | 45.9% |
3 | Unleaded petrol min. RON=91 (<10 ppm S) | 7.8% |
4 | Unleaded petrol min. RON=95 | 40.4% |
5 | Unleaded petrol min. RON=95 (<50 ppm S) | 22.7% |
6 | Unleaded petrol min. RON=95 (<10 ppm S) | 19.3% | 14 | Diesel (<50 ppm sulphur) | 29.2% |
7 | Unleaded petrol 95=<RON<98 | 3.7% |
8 | Unleaded petrol 95=<RON<98 (<50 ppm S) | 0.8% |
9 | Unleaded petrol 95=<RON<98 (<10 ppm S) | 0.0% |
10 | Unleaded petrol RON>=98 | 2.4% | 15 | Diesel (<10 ppm sulphur) | 24.9% |
11 | Unleaded petrol RON>=98 (<50 ppm S) | 1.2% |
12 | Unleaded petrol RON>=98 (<10 ppm S) | 0.9% |
Similarly to 2001 - 2003, the largest total sales of fuels (of submissions received) in 2004 were made in Germany, Italy, Spain and the United Kingdom (Figure 3). Whilst diesel sales are dominant in many Member States, the relative sales of petrol and diesel vary.
As in 2001 - 2003, there is also still a degree of variation in the number of grades of fuel reported to be available across the EU (Figure 4) in 2004, with more petrol grades available, despite the larger quantities of diesel sold. The distinction between petrol grades is mainly a result of different octane levels (RON category) rather than different sulphur levels. Seven EU15 Member States (two more than in 2001) defined national fuel grades for low (<50 ppm) or sulphur free (<10 ppm) fuels in 2004. Reporting of fuel sales is again improved in 2004. No EU10 Member States had separate national low sulphur fuel grades in 2004.
Figure 2: Fuel Quality Monitoring sampling rate across the EU in 2004 (average number of samples per fuel grade)
Average Number Samples / Fuel Grade | [pic] |
Petrol | Diesel |
Figure 3: National fuel sales by fuel type across the EU (million litres)*
National Fuel Sales, million litres | [pic] |
Total Petrol | Total Diesel |
Figure 4: Number of fuel grades available nationally by fuel type across the EU
Number of Fuel Grades Available Nationally | [pic] |
Number of Petrol Grades | Number of Diesel Grades |
Figure 5: National sales of low sulphur petrol grades across the EU (%)
% National Petrol Sales | [pic] |
Petrol (regular) | Petrol (<50 ppm sulphur) | Petrol (<10 ppm sulphur) |
Figure 6: National sales of low sulphur diesel grades across the EU (%)
% National Diesel Sales | [pic] |
Diesel (regular) | Diesel (<50 ppm sulphur) | Diesel (<10 ppm sulphur) |
Figure 7: Average sulphur content of petrol and diesel grades across the EU (%)
Average Sulphur Content in Fuel (ppm) | [pic] |
Petrol | Diesel |
In 2004, low sulphur fuels were available in many countries across the EU, although mandatory introduction is not required until 2005 (see Figure 5 and Figure 6).
Sulphur free petrol was available in Austria, Germany, Ireland and Slovakia. Sulphur free diesel was available in Austria, Germany, Lithuania, Slovakia and Sweden.
Separate low or sulphur-free fuel grades, or separate sales figures were not available in 2004 in some Member States. However, fuels complying with these criteria were available in many cases, e.g. Belgium, Denmark, Finland, Hungary, Italy, Latvia, Lithuania, Luxembourg, the Netherlands, Portugal and Slovakia. This can be seen in Figure 7, which presents the average sulphur content of petrol and diesel grades by Member State across the EU. (Average sulphur content is calculated from the mean sulphur content from reporting on the sampled fuels, weighted to the quantities of different petrol or diesel fuel grades sold). Much of the fuel sold already complies with the 2005 sulphur limit (<50 ppm sulphur in petrol and diesel fuels).
4.2 Compliance with Directive 98/70/EC in 2004
Reports show that 11 Member States are in complete compliance with Directive 98/70/EC limit values for both petrol and diesel for all samples (compared to 5 in 2001 for the EU15). With the exception of oxygenates (for 7 Member States, see notes 3 and 4 of the table), 21 Member States also provided complete reporting across the range of parameters specified for monitoring in the Directive.
In 2004, 13 Member States (6 EU15) reported at least one petrol sample non-compliant with Directive 98/70/EC. This compares with 10 in 2001 and 9 in 2002 and 2003 from EU15. Of these, the main parameters of concern were again research octane number (RON, 34+ samples), summer vapour pressure (DVPE, 43+ samples) and distillation - evaporation at 100°C (17+ samples).
For diesel, 8 Member States (2 EU15) reported at least one sample non-compliant with Directive 98/70/EC. This compares with 4 in 2001, 6 in 2002 and 5 in 2003 from EU15.Of these, the parameters of concern were sulphur content (22 samples), distillation 95% point (24 samples), cetane number (7+ samples) and density (5+ samples).
Although many Member States reported non-compliant samples, far fewer samples exceeded the limit values (and the limits of tolerance for the test methods) compared to previous years. However, both the Czech Republic and Poland reported significant numbers of samples exceeding limit values. Table 2 summarises the compliance of Member States with Directive 98/70/EC for the year 2004 reporting in terms of the results of the analysis of samples against limit values and the reporting format and content. As for 2001 to 2003 the quality of the compliance assessment suffers in a few cases from incomplete information provided by Member States. Details of action taken with regard to limit value non-compliance by Member States are included where provided in the individual country chapters of the detailed report for the year 2004[11].
Table 2: Summary of Member State compliance with 98/70/EC for 2004 reporting.
Member State | Limit value non-compliance (1) (95% confidence limits) [Non-compliant samples / Total samples] | Incomplete reporting [Number of parameters not measured / Total] | Late report (Due by 30/6/2005) (2) | Notes |
Petrol | Diesel | Petrol | Diesel |
Austria | 1 / 200 | <2 months |
Belgium | >14 / 4810 | >2 / 5045 | 1 / 18 | <5 months | (3) |
Cyprus | 4 / 72 | 6 / 18 | <1 month | (4) |
Czech Republic | >86 / 780 | >40 / 700 |
Denmark |
Estonia | 4 / 123 | 35 / 652 | 11 / 18 | (5) |
Finland | 3 / 226 |
Greece | 7 / 18 | <1 month | (4) |
Hungary | 6 / 18 | (4) |
Ireland | 11 / 97 | <2 months |
Italy | 6 / 256 | 6 / 269 | <1 month | (6) |
Latvia | >1 / 127 | >2 / 239 | <1 month |
Lithuania | 11 / 18 | 2 / 5 | <1 month | (7) (8) |
Luxembourg | <5 months |
Malta | 4 / 18 | 1 / 5 | <1 month | (9) (10) |
Netherlands | 6 / 18 | <1 month | (4) |
Poland | 28 / 343 | 11 / 157 |
Portugal | 6 / 18 | (4) |
Slovakia | 5 / 238 | 2 / 107 | <1 month |
Slovenia | 8 / 109 | 1 / 113 | <1 month |
Spain | <3 months |
Sweden | 6 / 18 | (4) |
UK | <3 months | (11) |
No. Countries | 13 | 8 | 10 | 2 | 17 |
Notes:
1. It is not possible to confirm whether limit values have been respected in all samples, where reporting data is incomplete. Where it has not been possible to establish from submissions the number of samples exceeding the limit value a ‘>’ symbol indicates that the number of samples exceeding limits is a minimum and might be greater.
2. Directive 98/70/EC states that Member States should submit monitoring reports by no later than 30th June each year.
3. Oxygen content has not been reported
4. Oxygenates (other than ethers with more than 5 carbon atoms per molecule) have not been reported. However, in principle, all substances on the list are measured at once using the oxygenate test methods. However, the system has to be calibrated using a calibration sample, containing the same oxygenates in similar proportions as present in the sample under test. It is not clear in most cases, whether this has been carried out, however Italy and Portugal have stated no other oxygenates are added to the fuel. The total organically bound oxygen is calculated from the percentages by mass of the individual components after identification.
5. The only petrol parameters measured were RON, DVPE, Distillation, Benzene, Sulphur and Lead.
6. Test method EN 1601 employed by Italy for the determination of oxygenate content in petrol samples requires the examination of each sample chromatogram to identify possible oxygen containing components, before the actual determination is carried out. The examination of all sample chromatograms showed that only one oxygenate compound was present in each sample (MTBE, ETBE, TAME); no other oxygenates were detected beside one of these ethers.
7. The only petrol parameters measured were RON, Distillation, Aromatics, Benzene and Sulphur. Lithuania has stated the equipment needed for the additional tests were not available in 2004, however arrangements have been made so that 2005 reporting will be complete.
8. Cetane number and PAH have not been measured for diesel, however the necessary equipment is available for 2005 monitoring analysis.
9. Oxygen content and 3 of the oxygenates were not reported, see comment (4) for clarification on oxygenates test method.
10. Cetane index has been measured instead of Cetane number.
11. Report delayed due to late delivery of information from one fuel supplier.
The quality of the compliance assessment suffers in some cases due to the incomplete information provided by Member Sates. Details on specific exceedances are provided in the individual country chapters of the full report. The cases of non-compliance identified seem not to have major negative repercussions on fuel quality in general.
5. CONCLUSIONS
The monitoring of fuel quality in 2004 shows that the specifications for petrol and diesel laid down in Directive 98/70/EC are in general met. Very few violations were identified. The Commission has no indication of any negative repercussions on vehicle emissions or engine functioning due to these violations. The Commission remains concerned about the violations and will continue insisting that Member States ensure full compliance with the fuel quality requirements laid down in the Directive. The amendments to Directive 98/70/EC by Directive 2003/17/EC included the insertion of a new Article 9a which states "Member States shall determine the penalties applicable to breaches of the national provisions adopted pursuant to this Directive. The penalties determined must be effective, proportionate and dissuasive." It is expected that the implementation of this requirement will have positive repercussions on compliance.
The share of <10 and <50 ppm fuels increased significantly from 2001 to 2003. However, there was little change between 2003 and 2004 and the expansion of the EU has resulted in a slight reduction in the percentage of these in the overall fuel supply. The lack of identified zero or low sulphur grades limits customers’ ability to choose these. This will hamper the introduction of vehicles benefiting from them. The report does not address the extent to which sulphur free fuels were available on a geographical basis – this information is to be provided by Member States in reporting on 2005 monitoring onwards.
The fuel quality monitoring systems established at national level differ considerably and require further uniformity in order to provide transparent and comparable results. The implementation of Directive 2003/17/EC has led to improved quality of reporting as it requires Member States to report on monitoring in accordance to the new European Standard, EN 14274, or with systems of equivalent confidence. There still remain some issues to be addressed and in particular, where Member States do not report according to EN 14274 format, justification for this must be provided.
ANNEX: 2004 EU fuel sales by fuel type (million litres) (Sales for CY, LV and MT are not for the full calendar year, only for May-Dec 2004.)
ID No. |Million litres |Austria |Belgium | Denmark |Finland |France |Germany |Greece |Ireland |Italy |Luxembourg | Netherlands |Portugal |Spain |Sweden |UK | EU15 | EU15 | | |Fuel grade |AU |BE | DK |FI |FR |DE |EL |IE |IT |LU | NL |PT |ES |SE |UK |EU15 | % Total | |1 |Unleaded petrol min. RON=91 |- |- |520 |- |- |- |- |- |- |8 |- |- |- |- |- |528 |0.4% | |2 |Unleaded petrol min. RON=91 (<50 ppm S) |92 |- |- |- |- |- |- |- |- |- |- |- |- |- |- |92 |0.1% | |3 |Unleaded petrol min. RON=91 (<10 ppm S) |672 |- |- |- |- |10,013 |- |- |- |- |- |- |- |- |- |10,685 |8.6% | |4 |Unleaded petrol min. RON=95 |- |- |2,029 |2,208 |- |- |3,849 |2,201 |19,704 |573 |7,501 |- |7,976 |- |- |45,447 |36.4% | |5 |Unleaded petrol min. RON=95 (<50 ppm S) |121 |1,830 |- |- |- |- |- |- |- |- |- |- |- |4,905 |24,094 |30,950 |24.8% | |6 |Unleaded petrol min. RON=95 (<10 ppm S) |1,899 |- |- |- |- |23,887 |- |- |- |- |- |- |- |- |- |26,381 |21.2% | |7 |Unleaded petrol 95=<RON<98 |- |- |- |- |- |- |916 |- |- |- |- |1,890 |1,259 |- |- |4,064 |3.3% | |8 |Unleaded petrol 95=<RON<98 (<50 ppm S) |- |- |- |- |- |- |- |- |- |- |- |- |- |- |1,097 |1,097 |0.9% | |9 |Unleaded petrol 95=<RON<98 (<10 ppm S) |- |- |- |- |- |- |- |4 |- |- |- |- |- |- |- |4 |0.0% | |10 |Unleaded petrol RON>=98 |- |- |20 |303 |- |- |399 |- |- |- |- |574 |1,209 |640 |- |2,506 |2.0% | |11 |Unleaded petrol RON>=98 (<50 ppm S) |- |786 |- |- |- |- |61 |- |- |167 |- |- |- |- |- |1,654 |1.3% | |12 |Unleaded petrol RON>=98 (<10 ppm S) |104 |- |- |- |- |1,188 |- |- |- |- |- |- |- |- |- |1,292 |1.0% | | |Petrol (regular) | 0 |0 | 2,569 |2,512 |0 |0 |5,164 |2,201 |19,704 |582 | 7,501 |2,464 |10,445 |640 |0 |52,546 | 42.1% | | |Petrol (<50 ppm sulphur) | 213 |2,616 | 0 |0 |0 |0 |61 |0 |0 |167 | 0 |0 |0 |4,905 |25,191 |33,793 | 27.1% | | |Petrol (<10 ppm sulphur) | 2,676 |0 | 0 |0 |0 |35,088 |0 |4 |0 |0 | 0 |0 |0 |0 |0 |38,362 | 30.8% | | |Total Petrol | 2,889 |2,616 | 2,569 |2,512 |0 |35,088 |5,225 |2,204 |19,704 |749 | 7,501 |2,464 |10,445 |5,545 |25,191 |124,701 | 100.0% | |13 | Diesel | 215 |- |- |- |- |- |3,055 |- |28,777 |- |- |5,940 |26,447 |- |- |64,434 |40.4% | |14 | Diesel (<50 ppm sulphur) | 1,645 |7,579 |2,658 |2,363 |- |- |319 |2,474 |- |1,947 |9,724 |- |- |42 |22,252 |51,004 |32.0% | |15 | Diesel (<10 ppm sulphur) | 5,291 |- |- |- |- |34,642 |13 |- |- |- |- |- |- |4,031 |- |43,977 |27.6% | | | Total Diesel | 7,150 |7,579 | 2,658 |2,363 |0 |34,642 |3,387 |2,474 |28,777 |1,947 | 9,724 |5,940 |26,447 |4,073 |22,252 |159,415 | 100.0% | |
ID No. |Million litres |Cyprus |Czech Republic |Estonia |Hungary |Latvia |Lithuania |Malta |Poland |Slovakia |Slovenia | EU10 |EU10 | |European Union | European Union | | |Fuel grade |CY |CZ |EE |HU |LV |LT |MT |PL |SK |SI |EU10 | % Total | |EU | % Total | |1 |Unleaded petrol min. RON=91 |- |322 |20 |12 |- |60 |- |- |28 |- |441 |3.2% | |969 |0.7% | |2 |Unleaded petrol min. RON=91 (<50 ppm S) |- |- |- |- |- |37 |- |- |- |- |37 |0.3% | |128 |0.1% | |3 |Unleaded petrol min. RON=91 (<10 ppm S) |- |- |- |- |- |- |- |- |90 |- |90 |0.6% | |10,775 |7.8% | |4 |Unleaded petrol min. RON=95 |216 |2,714 |322 |1,812 |- |- |43 |5,331 |61 |- |10,498 |75.4% | |55,945 |40.4% | |5 |Unleaded petrol min. RON=95 (<50 ppm S) |- |- |- |- |- |348 |- |- |179 |- |527 |3.8% | |31,477 |22.7% | |6 |Unleaded petrol min. RON=95 (<10 ppm S) |- |- |- |- |- |- |- |- |407 |- |407 |2.9% | |26,787 |19.3% | |7 |Unleaded petrol 95=<RON<98 |- |- |- |- |254 |- |19 |- |- |845 |1,118 |8.0% | |5,183 |3.7% | |8 |Unleaded petrol 95=<RON<98 (<50 ppm S) |- |- |- |- |- |- |- |- |- |- |0 |0.0% | |1,097 |0.8% | |9 |Unleaded petrol 95=<RON<98 (<10 ppm S) |- |- |- |- |- |- |- |- |- |- |0 |0.0% | |4 |0.0% | |10 |Unleaded petrol RON>=98 |45 |35 |51 |168 |32 |- |- |393 |3 |52 |779 |5.6% | |3,285 |2.4% | |11 |Unleaded petrol RON>=98 (<50 ppm S) |- |- |- |- |- |8 |- |- |- |- |8 |0.1% | |1,662 |1.2% | |12 |Unleaded petrol RON>=98 (<10 ppm S) |- |- |- |- |- |1 |- |- |24 |- |25 |0.2% | |1,317 |0.9% | | |Petrol (regular) | 261 |3,070 |393 |1,992 |286 |60 |61 |5,724 |92 |897 |12,836 |92.2% | |65,382 | 47.2% | | |Petrol (<50 ppm sulphur) | 0 |0 |0 |0 |0 |392 |0 |0 |179 |0 |571 |4.1% | |34,364 | 24.8% | | |Petrol (<10 ppm sulphur) | 0 |0 |0 |0 |0 |1 |0 |0 |520 |0 |522 |3.7% | |38,883 | 28.0% | | |Total Petrol | 261 |3,070 |393 |1,992 |286 |453 |61 |5,724 |791 |897 |13,928 |100.0% | |138,628 | 100.0% | |13 | Diesel | 272 |4,171 |492 |2,710 |458 |35 |63 |7,677 |315 |894 |17,086 |93.6% | |81,520 |45.9% | |14 | Diesel (<50 ppm sulphur) | - |- |- |- |- |687 |- |- |197 |- |884 |4.8% | |51,888 |29.2% | |15 | Diesel (<10 ppm sulphur) | - |- |- |- |- |159 |- |- |119 |- |277 |1.5% | |44,254 |24.9% | | | Total Diesel | 272 |4,171 |492 |2,710 |458 |881 |63 |7,677 |630 |894 |18,248 |100.0% | |177,662 | 100.0% | |
[1] O.J. L 350, 28.12.1998, p. 58
[2] O.J. L 76, 22.3.2003, p. 10
[3] Reporting years 2001 and 2002 are covered by COM(2004) 310 final and 2003 by COM(2005) 69 final
[4] See http://europa.eu.int/comm/environment/air/pdf/fqm_summary_2004.pdf
[5] COM(2004) 310 final
[6] COM(2005) 69 final
[7] http://europa.eu.int/comm/environment/air/fuel_quality_monitoring.htm
[8] EN 14274:2003 - Automotive fuels - Assessment of petrol and diesel quality - Fuel Quality Monitoring System (FQMS).
[9] EN 14275:2003 - Automotive fuels - Assessment of petrol and diesel fuel quality -Sampling from retail site station pumps and commercial site fuel dispensers.
[10] The term “regular” is used for fuels with a sulphur content which is in accordance with Directive 98/70/EC (150 ppm for petrol and 350 ppm for diesel); the term “low sulphur” corresponds to a sulphur content of 50 ppm; the term “sulphur free” to a sulphur content of 10 ppm
[11] See http://europa.eu.int/comm/environment/air/fuel_quality_monitoring.htm
