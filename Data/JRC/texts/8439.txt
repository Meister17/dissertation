COUNCIL DECISION of 14 December 1998 on the conclusion of the Agreement between the European Community and the Government of Canada on sanitary measures to protect public and animal health in respect of trade in live animals and animal products (1999/201/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 113 in conjunction with Article 228(2), first sentence thereof,
Having regard to the proposal from the Commission,
Whereas the Agreement between the European Community and the Government of Canada on sanitary measures to protect public and animal health in respect of trade in live animals and animal products provides an adequate means for putting into practice the provisions of the World Trade Organisation Agreement on the Application of Sanitary and Phytosanitary Measures as regards public and animal health measures;
Whereas the Agreement will contribute towards facilitating bilateral trade between the European Community and Canada in live animals and animal products through the progressive recognition of the equivalence of sanitary measures, the recognition of animal health status, the application of regionalisation and the improvement of communication and cooperation;
Whereas it is appropriate to make provisions for a procedure establishing close and effective cooperation between the Commission and the Member States within the Standing Veterinary Committee;
Whereas the Agreement should be approved on behalf of the Community,
HAS DECIDED AS FOLLOWS:
Article 1
The Agreement between the European Community and the Government of Canada on sanitary measures to protect public and animal health in respect of trade in live animals and animal products is hereby approved on behalf of the Community.
The text of the Agreement and the Annexes thereto are attached to this Decision.
Article 2
The President of the Council is hereby authorised to designate the person or persons empowered to sign the Agreement in order to bind the Community.
Article 3
The measures necessary for the implementation of this Agreement, including guarantees with regard to fresh meat and meat-based products equivalent to those laid down by Council Directive 72/462/EEC of 12 December 1972 on health and veterinary inspection problems upon importation of bovine, ovine and caprine animals and swine, fresh meat or meat products from third countries (1), shall be established pursuant to the procedure laid down in Article 30 of that Directive.
Article 4
The Commission, assisted by Member States' representatives, shall represent the Community in the Joint Management Committee referred to in Article 16(1) of the Agreement.
The Community position with regard to the matters to be dealt with by that Joint Management Committee shall be established within the appropriate Council bodies, in accordance with the provisions of the Treaty.
Amendments to the Annexes to the Agreement which are the result of recommendations by the Joint Management Committee shall be adopted in accordance with the procedure laid down in Article 29 of Directive 72/462/EEC.
Article 5
This Decision shall be published in the Official Journal of the European Communities.
It shall take effect on the date of its publication.
Done at Brussels, 14 December 1998.
For the Council
The President
W. MOLTERER
(1) OJ L 302, 31.12.1972, p. 28. Directive as last amended by Directive 97/79/EC (OJ L 24, 30.1.1998, p. 31).
