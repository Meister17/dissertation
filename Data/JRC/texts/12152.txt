Commission Decision
of 13 December 2006
amending Decision 92/452/EEC as regards certain embryo collection and production teams in Canada, New Zealand and the United States of America
(notified under document number C(2006) 6441)
(Text with EEA relevance)
(2006/925/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 89/556/EEC of 25 September 1989 on animal health conditions governing intra-Community trade in and importation from third countries of embryos of domestic animals of the bovine species [1], and in particular Article 8(1) thereof,
Whereas:
(1) Commission Decision 92/452/EEC of 30 July 1992 establishing lists of embryo collection teams and embryo production teams approved in third countries for export of bovine embryos to the Community [2] provides that Member States are only to import embryos from third countries where they have been collected, processed and stored by embryo collection teams listed in that Decision.
(2) Canada has requested to add a new embryo production team to the list as regards entries for that country.
(3) New Zealand has requested that amendment be made to the name of a centre as regards the entries for that country.
(4) The United States of America have requested to amend some details concerning certain embryo collection and production teams as regards entries for that country.
(5) Canada, New Zealand and the United States of America have provided guarantees regarding compliance with the appropriate rules set out in Directive 89/556/EEC and the embryo collection teams concerned have been officially approved for exports to the Community by the veterinary services of those countries.
(6) Decision 92/452/EEC should therefore be amended accordingly.
(7) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health Committee,
HAS ADOPTED THIS DECISION:
Article 1
The Annex to Decision 92/452/EEC is amended in accordance with the Annex to this Decision.
Article 2
This Decision shall apply from the third day following that of its publication in the Official Journal of the European Union.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 13 December 2006.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 302, 19.10.1989, p. 1. Directive as last amended by Commission Decision 2006/60/EC (OJ L 31, 3.2.2006, p. 24).
[2] OJ L 250, 29.8.1992, p. 40. Decision as last amended by Decision 2006/706/EC (OJ L 291, 21.10.2006, p. 40).
--------------------------------------------------
ANNEX
The Annex to Decision 92/452/EEC is amended as follows:
(a) the following row for Canada is inserted:
"CA | | E1567 (IVF) | | IND Lifetech Inc 1629 Fosters Way Delta, British Columbia V3M 6S7 | Dr Richard Rémillard" |
(b) the row for New Zealand embryo collection team No NZEB11 is replaced by the following:
"NZ | | NZEB11 | | IVP International (NZ) Ltd PO Box 23026 Hamilton | Dr Rob Courtney Dr William Hancock" |
(c) the row for the United States of America embryo collection team No 02TX107 E1428 is replaced by the following:
"US | | 02TX107 E1428 | | OvaGenix 4700 Elmo Weedon RD #103 College Station, TX 77845 | Dr Stacy Smitherman" |
(d) the row for the United States of America embryo collection team No 99TX104 E874 is replaced by the following:
"US | | 99TX104 E874 | | Ultimate Genetics/Camp Cooley, Rt 3, Box 745 Franklin, TX 77856 | Dr Joe Oden Dr Dan Miller" |
(e) the row for the United States of America embryo collection team No 96TX088 E928 is replaced by the following:
"US | | 96TX088 E928 | | Ultimate Genetics/Normangee, 41402 OSR Normangee, TX 77871 | Dr Joe Oden Dr Dan Miller" |
(f) the row for the United States of America embryo collection team No 91TX012 E948 is replaced by the following:
"US | | 91TX012 E948 | | Veterinary Reproductive Services 8225 FM 471 South Castroville, TX 78009 | Dr Sam Castleberry" |
--------------------------------------------------
