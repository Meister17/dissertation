Publication of an application for registration pursuant to Article 6(2) of Regulation (EEC) No 2081/92 on the protection of geographical indications and designations of origin
(2005/C 288/04)
This publication confers the right to object to the application pursuant to Articles 7 and 12d of the abovementioned Regulation. Any objection to this application must be submitted via the competent authority in a Member State, in a WTO member country or in a third country recognized in accordance with Article 12(3) within a time limit of six months from the date of this publication. The arguments for publication are set out below, in particular under 4.6, and are considered to justify the application within the meaning of Regulation (EEC) No 2081/92.
SUMMARY
COUNCIL REGULATION (EEC) No 2081/92
"AZEITONAS DE CONSERVA DE ELVAS E CAMPO MAIOR"
EC No: PT/00216/ 10.12.2001
PDO (X) PGI ( )
This summary has been drawn up for information purposes only. For full details interested parties, in particular the producers of products covered by the PDO or PGI in question, are invited to consult the complete version of the specification, obtainable at national level or from the European Commission [1].
1. Responsible department in the Member State:
Name: | Instituto de Desenvolvimento Rural e Hidráulica |
Address: | 3 Av. Afonso Costa PT-1949-002 Lisbon |
Tel.: | (351-21) 844 22 00 |
Fax: | (351-21) 844 22 02 |
E-mail: | idrha@idrha.min-agricultura.pt |
2. Group:
Name: Agrodelta Indústrias Alimentares Lda (application for registration presented not by a producers' group but by a legal person under Article 1 of Regulation (EEC) No 2037/93).
Address Zona Industrial, PT-7370 Campo Maior
Tel.: (351) 268 680 140
Fax: (351) 268 680 141
E-mail: agrodelta@delta-cafes.pt
Composition: producer/processor (X) other ( )
3. Type of product:
Group 1.6 — Fruit, vegetables and cereals, fresh or processed
4. Specification:
(Summary of requirements under Article 4(2)):
Name: "Azeitonas de Conserva de Elvas e Campo Maior"
- soaking of sweetened green olives in brine;
- soaking of broken natural green olives in brine;
- soaking of mixed cut natural olives brine.
- 280-320 fruit/kg for whole green olives in brine;
- 340-360 fruit/kg for green olives, stuffed (containing red pepper flesh) or stoned, in brine;
- 240-260 fruit/kg for broken green olives in brine;
- 300-320 fruit/kg for mixed olives in brine, cut and flavoured with oregano and/or thyme.
When picked, the olives have a flesh to stone ratio of more than 5 and are easy to stone. Thanks to the soil and climate, regional know-how and the special characteristics of the varieties used, Azeitonas de Conserva de Elvas e Campo Maior feature the following chemical characteristics: a pH of 4 or more; free acidity, expressed as lactic acid, or more than 0,6g/100 ml; 6,5 to 7,5g of sodium chloride per 100 ml of brine; a combined acidity of less than 0,140 N; and a free acidity to volatile acidity ratio greater than 1. Organoleptically, the olives are classified as being of typical, superior or good quality in accordance with standard evaluation criteria (external and internal appearance, smell and taste).
Geographical area: In view of the soil and weather conditions required for growing and preparing the olives, the special know-how of the local population, and the authentic and unvarying local methods of production used, the geographical area in which production, treatment and packing may take place is limited to the municipalities of Elvas and Campo Maior. Packing must take place in the geographical area in order to ensure the traceability and control of the product and its designation and preserve the qualitative characteristics of a product which otherwise could have olives grown elsewhere mixed in with it.
Proof of origin: Olive-growing for preserves is part of a long-standing tradition in the municipalities concerned, with reference varieties already being collected in 1900. The product is of major economic importance at regional level. Azeitonas de Conserva de Elvas e Campo Maior are shipped to all parts of the country and even abroad and enjoy an excellent reputation. They are used in numerous traditional Portuguese recipes and are asked for by name, in particular as a first course. The olive groves are registered and approved by the Group after consulting the inspection body. The entire production process is subject to stringent checks that allow the product to be fully traceable. The tending of the olive groves, the picking and transport of the olives, and the various processes used are monitored, checked and registered. The certification mark on the packaging is numbered, allowing the origin of the product to be ascertained at any stage in the production chain.
Method of production:
The olives come from groves situated in the geographical area. The spacing of the trees is traditional (10 × 9 or 10 × 8 metres) or more intensive (7 × 7/7 × 6 metres). The irrigation techniques used tend to be simpler in the older olive groves (spray irrigation), more sophisticated in the modern ones (drop irrigation). Cultivation operations are, throughout, conducted in a manner that is in keeping with environmental conditions and reflect the know-how that has built up over time. The olives are picked by hand or by stripping branches in a single action, taking care to avoid damaging the fruit. For the production of green olives the fruit is picked when it has reached its maximum size and, at the beginning of the ripening process, has a firm texture. When cut across and twisted lightly between the fingers it readily sheds its stone.
Suitably large and markedly green olives of the Carrasquenha variety are used for the production of broken green olives. For the production of mixed olives, however, the fruit is picked before it is fully ripe, and may feature pink, chestnut-brown, greenish or thin black tinges.
The olives are graded by size and are rejected if they show signs of damage or are not suitably ripe.
Whether produced by following a traditional or a more industrial approach, the processes to which the olives are subjected are invariably gentle, e.g. using brine or dilute solutions of sodium hydroxide and lactic acid. Fermentation may be induced by certain yeasts and lactic flora.
The olives may where necessary be seasoned using herbs from the region (lemon tree or orange tree leaves, garlic, laurel, oregano and thyme) and are in some cases stuffed with red pepper flesh.
The breaking of the olives is done by hand or, in small quantities, by machine, in order not to pulverise the flesh or crack the olive stones.
When the production process is over the olives for preserving are packed in inert, waterproof packaging — specially selected for the product concerned — that does not adversely affect the contents. They are then covered in a suitable liquid to prevent oxidation or the formation of a film.
Link:
The area of production features a mild climate with, however, wintertime temperatures which, while sufficiently low to allow vegetative rest and encourage subsequent fruit-bearing, are not such as to adversely affect the crop.
Together with average rainfall and mild temperatures, the depth and fertility of the soil in the region help the fruit to mature and give it the ideal characteristics for the type of product concerned.
The varieties used are perfectly adapted to the region, yielding olives with a low oil content and a suitable consistency.
Ambient temperature is a key aspect of the preparation of preserved olives.
The average temperature at the beginning of the season is around 22° C. Exploiting the natural conditions helps to speed up the process and, early on, achieve ideal chemical characteristics in the brine.
The fact that the herbs, etc. used for seasoning the olives are available naturally is another major factor.
The organoleptic characteristics of the product are thus closely linked to the geographical area of production, the composition of the olive groves, and the soil and climate.
This type of olive-growing is part of a long-standing tradition, resulting in countless bibliographical references — in some cases dating back more than one century — both to it and to the extreme care shown in tending the olive groves and preserving the olives.
A number of farming and industrial traditions have built up which centre on the importance of olive-growing and the techniques used, in particular typical dress, songs and regional cooking.
Name: | AGRICERT Certificação de Produtos Alimentares Lda |
Address: | R. Dr. António Pires Antunes, 4 r/c Esq. Sala C PT-7350 Elvas |
Tel.: | 268 769 564/5 |
Fax: | 266 769 566 |
E-mail: | agricert@clix.pt |
Labelling: Azeitonas de Conserva de Elvas e Campo Maior PDO and the Community logo must appear on the labelling, together with the certification mark, which must contain the name of the product and the form of wording concerned, the name of the inspection body and the serial number (numerical or alphanumeric code enabling the product to be traced).
National requirements: —
[1] European Commission, Directorate-General for Agriculture, Agricultural product quality policy Unit, B-1049 Brussels.
--------------------------------------------------
