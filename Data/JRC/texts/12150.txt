Notice of the expiry of certain anti-dumping measures
(2006/C 112/02)
Further to the publication of a notice of impending expiry [1], following which no request for a review was received, the Commission gives notice that the anti-dumping measures mentioned below will shortly expire.
This notice is published in accordance with Article 11(2) of Council Regulation (EC) No 384/96 of 22 December 1995 [2] on protection against dumped imports from countries not members of the European Community.
Product | Country(ies) of origin or exportation | Measures | Reference | Date of expiry |
Aluminium foil | People's Republic of China Russia | Anti-dumping duty | Council Regulation (EC) No 950/2001 (OJ L 134, 17.5.2001, p. 1) as last amended by Regulation (EC) No 161/2006 (OJ L 26, 31.1.2006, p. 1) | 18.5.2006 |
[1] OJ C 209, 26.8.2005, p. 2.
[2] OJ L 56, 6.3.1996, p. 1. Regulation as last amended by Council Regulation (EC) No 2117/2005 (OJ L 340, 23.12.2005, p. 17).
--------------------------------------------------
