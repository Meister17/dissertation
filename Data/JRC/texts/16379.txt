COMMISSION DECISION of 3 May 1996 establishing health certification of live bivalve molluscs, echinoderms, tunicates and marine gastropods from third countries which are not covered by a specific decision (Text with EEA relevance) (96/333/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/492/EEC of 15 July 1991, laying down the health conditions for the production and the placing on the market of live bivalve molluscs (1), as last amended by the Act of Accession of Austria, Sweden and Finland, and in particular Article 9 thereof,
Whereas the Commission has laid down the special conditions for the importation of live bivalve molluscs, echinoderms, tunicates and marine gastropods for certain third countries;
Whereas for the importation of live bivalve molluscs, echinoderms, tunicates and marine gastropods from third countries not yet covered by this type of decision, it is advisable initially to establish a standardized model of a health certificate to avoid disruptions to trade;
Whereas the adoption of a standard health certificate would have positive effects on operators and for the control services and would facilitate the free circulation of live bivalve molluscs, echinoderms, tunicates and marine gastropods within the Community;
Whereas the model of health certificate established by this Decision is provisional and applicable for a period of two years during which individual decisions may be adopted; whereas the provisional certificate will therefore no longer apply to any third country for which an individual decision has been adopted;
Whereas the veterinary checks on imported bivalve molluscs, echinoderms, tunicates and marine gastropods have to be carried out in accordance with Council Directive 90/675/EEC of 10 December 1990, laying down the principles governing the organization of veterinary checks on products entering the Community from third countries (2), as last amended by Directive 95/52/EC (3); whereas those checks entail the presentation of a health certificate accompanying the imported products;
Whereas the adoption of a standard model of health certificate should be without prejudice to any specific import conditions that may be adopted for a third country after on-the-spot assessment of the health situation by Commission experts;
Whereas, in accordance with Article 8 of Directive 91/492/EEC, the health certificate should attest that the conditions of production, purification, storage, packaging and transport of live bivalve molluscs, echinoderms, tunicates and marine gastropods intended for the Community are at least equivalent to those laid down for the Community products;
Whereas the measures provided for in this Decision should be without prejudice to measures adopted hereafter for the protection of animal health;
Whereas the measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
Consignments of live bivalve molluscs, echinoderms, tunicates and marine gastropods brought into the territories defined in Annex I of Directive 90/675/EEC for direct human consumption shall have been harvested from approved production areas approved and inspected by the competent authority of the third country, shall come from an establishment inspected and approved by the competent authority of the third country, and shall be accompanied by ............the original of a numbered health certificate attesting that the health conditions of production, handling, where necessary purification, packaging and identification of the products are at least equivalent to those set out in Directive 91/492/EEC.
The model of that health certificate is laid down in Annex I to this Decision.
Article 2
Consignments of live bivalve molluscs, echinoderms, tunicates and marine gastropods introduced into the territories defined in Annex I of Directive 90/675/EEC for purification in an approved purification centre, or for re-laying in an approved re-laying zone, or processing in an approved processing establishment, shall have been harvested from approved production areas checked and approved by the competent authority of the third country, shall be accompanied by the original of a numbered health certificate attesting that the health conditions of production, harvesting and transportation of the batches are at least equivalent to those established by Directive 91/492/EEC.
The model of that health certificate is laid down in Annex II to this Decision.
Article 3
1. The health certificate referred to in Articles 1 and 2 shall consist of a single sheet of paper and shall be drawn up in at least one of the official languages of the country of entry into the Community, and, if necessary, in one of the languages of the country of destination.
2. The health certificates shall bear the name, designation and signature of the official inspector as well as the official seal of the competent authority, all of which shall be in a colour different from that of the other printing on the certificate.
Article 4
The health certificates provided for by this Decision shall not apply to live bivalve molluscs, echinoderms, tunicates and marine gastropods from a third country for which the individual conditions of importation are laid down elsewhere.
Article 5
This Decision shall apply from 1 July 1996 for two years.
Article 6
This Decision is addressed to the Member States.
Done at Brussels, 3 May 1996.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ No L 268, 24. 9. 1991, p. 1.
(2) OJ No L 373, 31. 12. 1990, p. 1.
(3) OJ No L 265, 8. 11. 1995, p. 16.
ANNEX I
>START OF GRAPHIC>
HEALTH CERTIFICATE (MODEL)
No ................
pertaining to live:
- bivalve molluscs (1),
- echinoderms (1),
- tunicates (1),
- marine gastropods (1)
intended for direct human consumption in the European Community.
Dispatching country: .................
Competent authority (2): .............
Inspection service (2): ..............
I. Identification of the products
Product wild/farmed (1):
- Species (scientific name): ..............
- Nature of packing: ......................
- Number of packages: .....................
- Net weight: .............................
- Necessary storage and transport temperature:
- Reference number of the analysis report (if necessary):
II. Source of the products
- Approved production area: ..............
- Name and official number of the approved establishment: ..............
III. Destination of the products
Products are to be sent
from: ........................
(place of dispatch)
to: ..........................
(countries and place of destination)
by the following means of transport (3): ..............
Name and address of the consignor: ....................
Name of consignee and address of the place of destination: .............
(1) Delete where not applicable.
(2) Name and address.
(3) Registration number of lorries, railway wagons or container, flight number or name of the ship.
IV. Health certificate
I, the undersigned official inspector, certify that the live products described above:
1. were collected, if necessary re-laid over a period at least two months, and transported under conditions at least equivalent to those laid down in Chapters I, II and III of the Annex to Directive 91/492/EEC;
2. were handled, and if necessary purified in accordance with the hygiene rules laid down in Chapter IV of the Annex to Directive 91/492/EEC;
3. were inspected in accordance with the requirements laid down in Chapter VI of the Annex to Directive 91/492/EEC;
4. were packaged, stored and transported in accordance with the requirements laid down in Chapters VII, VIII and IX of the Annex to Directive 91/492/EEC;
5. bear a health mark in accordance with provisions laid down in Chapter X of the Annex to Directive 91/492/EEC;
6. were analyzed and are in conformity with the requirements laid down in Chapter V of the Annex to Directive 91/492/EEC and therefore are fit for direct human consumption.
I declare that I am aware of the provisions of Directive 91/492/EEC laying down the health conditions for the production and the placing on the market of live bivalve molluscs.
Done at:.............,(place) on ................(date)
.........................................
(signature of the official Inspector) (1)
Official seal (1)
..........................................................
(name, title and designation of the signatory in capitals)
(1) The colour of the seal and of the signature must be different from that of the other printing on the certificate.
>END OF GRAPHIC>
ANNEX II
>START OF GRAPHIC>
HEALTH CERTIFICATE (MODEL)
No ..........................
pertaining tos:- bivalve molluscs (1),
- echinoderms (1),
- tunicates (1),
- marine gastropods (1),
live intended for:- purification (1),
- re-laying (1),
- processing (1)
in the European Community.
Dispatching country:.................
Competent authority (2):.............
Inspection service (2):..............
I. Identification of the products
Product wild/farmed (1):
- Species (scientific name):......
- Nature of packing:..............
- Number of packages:.............
- Net weight:.....................
- Date of harvesting:.............
II. Source of the products
- Approved production area:.......
- Class of the area: A - B - C (1) according to Chapter I of the Annex to Directive 91/492/EEC
III. Destination of the products
Products are to be sent
from: .............
(place of dispatch)
to: ................................
(countries and place of destination)
by the following means of transport (3):.............
Name and address of the consignor:...................
Name, address and approval number of the consignee:
- purification centre (1).............
- relaying area (1)...................
- processing establishment (1)........
(1) Delete where not applicable.
(2) Name and address.
(3) Registration number of lorries, railway wagons or container, flight number or name of the ship.
IV. Health certificate
I, the undersigned official inspector, certify that the live products described above:
1. come from a production area approved in accordance with the requirements laid down in Chapter I of the Annex to Directive 91/492/EEC;
2. were collected, handled, and transported under conditions at least equivalent to those laid down in Chapter II of the Annex to Directive 91/492/EEC;
3. were inspected in accordance with the requirements laid down in Chapter VI of the Annex to Directive 91/492/EEC and fulfil the requirements of Chapter V of this Annex except the microbiological standards;
4. are not fit for direct human consumption.
I declare that I am aware of the provisions of Council Directive 91/492/EEC laying down the health conditions for the production and the placing on the market of live bivalve molluscs.
Done at: ............., (place) on: .............(date)
.......................................
(signature of the official Inspector) (1)
Official seal (1)
..........................................................
(name, title and designation of the signatory in capitals)
(1) The colour of the seal and of the signature must be different from that of the other printing on the certificate.
>END OF GRAPHIC>
