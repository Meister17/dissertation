COMMISSION REGULATION (EEC) No 3378/91 of 20 November 1991 laying down detailed rules for the sale of butter from intervention stocks for export and amending Regulation (EEC) No 569/88
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 804/68 of 27 June 1968 on the common organization of the market in milk and milk products (1), as last amended by Regulation (EEC) No 1630/91 (2), and in particular Articles 6 (7) and 28 thereof,
Having regard to Council Regulation (EEC) No 1678/85 of 11 June 1985 on the exchange rates to be applied in agriculture (3), as last amended by Regulation (EEC) No 1640/91 (4), and in particular Article 4 thereof,
Whereas Article 6 of Council Regulation (EEC) No 985/68 of 15 July 1968 laying down general rules for intervention on the market in butter and cream (5), as last amended by Regulation (EEC) No 2045/91 (6), provides that special conditions may be laid down when butter is put on sale for export in order to take account of the special requirements for such sales and to ensure that the product is not diverted from its destination;
Whereas the quantities of butter now in public storage are such that a major effort should be made to make use of opportunities for disposing of it on the market of certain third countries without in any way interfering with the world market;
Whereas, therefore, quantities of butter in public storage should be made available to operators and invitations to tender organized in order, inter alia, to determine the minimum selling prices so that the Community's international commitments may be met; whereas measures should also be adopted to ensure that butter sold under this Regulation is not released in the Community;
Whereas, in order to increase the scope for sales opportunities on certain international markets, provision should be made for the butter to be exported in the unaltered state or after processing;
Whereas operators may buy the butter in question throughout the community; whereas steps should be taken, therefore, to adjust the monetary compensatory amounts in relation to the level of the selling prices of the butter from intervention;
Whereas, in order to ensure that the butter is not diverted from its intended destination, a control system should be applied from the time the butter is removed from stock until it arrives at its destination in the third country concerned; whereas for reasons of clarity it should be made clear that the control provisions laid down in Commission Regulation (EEC) No 569/88 of 16 February 1988 laying down common detailed rules for verifying the use and/or destination of products from intervention (7), as last amended by Regulation (EEC) No 3278/91 (8), apply; whereas, moreover, in view of the specific nature of the transaction, additional conditions must be laid down;
Whereas the Management Committee for Milk and Milk Products has not delivered an opinion within the time limit set by its Chairman,
HAS ADOPTED THIS REGULATION:
Article 1
1. The sale shall take place, under the conditions laid down in this Regulation, of butter bought in pursuant to Article 6 (1) of Regulation (EEC) No 804/68, which entered into storage before 1 September 1990.
2. Butter sold pursuant to this Regulation shall be exported in the unaltered state or after processing.
Article 2
1. The butter shall be sold ex-cold storage depot in accordance with the standing invitation to tender organized by each intervention agency for the relevant quantities of butter it holds.
2. The intervention agencies shall draw up a notice of invitation to tender stating:
(a) the quantities of butter put on sale;
(b) the closing date and place for the submission of tenders.
3. The notices of invitation to tender shall be published in the Official Journal of the European Communities not less than eight days prior to the expiry of the first deadline fixed for the submission of tenders. The intervention agencies may, in addition, insert the notices in other publications.
Article 3
1. During the term of validity of the standing invitation to tender, the intervention agencies shall organize special invitations to tender. Each of the latter shall cover any of the butter referred to in Article 1 remaining available.
2. The closing date for the submission of tenders for each special invitation to tender shall be 12 noon on the second and fourth Tuesdays of each month. Where, except for the fourth Tuesday in December, the Tuesday is a public holiday, the deadline shall be extended until 12 noon on the first subsequent working day.
Article 4
1. Intervention agencies shall keep up to date and make available to interested parties, at their request, a list of the cold storage depots in which the butter put up for sale by tender is stored and of the corresponding quantities. The intervention agencies shall in addition publish this updated list regularly, in an appropriate form which they shall specify in the notices of invitation to tender referred to in Article 2.
2. Intervention agencies shall take the necessary measures to enable interested parties to examine at their expense, before submitting their tenders, samples of the butter put up for sale.
Article 5
1. Interested parties shall participate in special invitations to tender either by lodging their written tenders with the intervention agency against an acknowledgment of receipt or by forwarding a registered letter to the intervention agency or by any other means of written telecommunication.
2. Tender shall state:
(a) the name and address of the tenderer;
(b) the total quantity applied for;
(c) the intended destination of the butter, with details of the quantities which are to be exported unprocessed and those which are to be exported after processing;
(d) the price tendered per tonne of butter, not including domestic taxes, ex-cold storage depot, expressed in ecus;
(e) the cold storage depots where the butter is held;
(f) the quantities applied for in other Member States.
3. Tenders shall be valid only if:
(a) they cover a minimum of 500 tonnes, taking account of the quantities applied for in the other Member States;
(b) they are accompanied by a written undertaking by the tenderer to export the butter allocated to him in the unaltered state within the time limit laid down in Article 9 (3) or after processing within the time limit laid down in Article 10 (5);
(c) proof if furnished that tenderers have lodged, before the expiry of the deadline for the submission of tenders, the tendering security referred to in Article 6 for the special invitation to tender concerned.
4. Tenders may not be withdrawn after the expiry of the deadline referred to in Article 3 (2) for the submission of tenders relating to the special invitation to tender concerned.
Article 6
1. Under this Regulation the maintenance of tenders after the expiry of the deadline for the submission of tenders and the payment of the price within the time limit referred to in the first subparagraph of Article 8 (1) shall constitute primary requirements whose fulfilment shall be ensured by the lodging of a security of ECU 10 per tonne.
2. Tendering securities shall be lodged in the Member States in which tenders are submitted.
However if the tender states that the butter is to be processed into concentrated butter in a Member State other than that of submission of the tender, the security may be lodged with the competent authority designated by the former Member State, which shall provide the tenderer with the proof referred to in Article 5 (3) (c). In that case, the intervention agency concerned shall inform the competent authority of the latter Member State of the facts resulting in release or seizure of the security.
Article 7
1. A minimum selling price for butter to be exported in the unaltered state and a minimum selling price for butter to be exported after processing shall be fixed in the light of the tenders received and in accordance with the procedure laid down in Article 30 of Regulation (EEC) No 804/68. Tenders shall be rejected if the proposed price is lower than the minimum price. Where two or more tenders under consideration relating to the same store and offering the same prices or the same difference from the minimum price relate to a total quantity exceeding the quantity available, the quantity available shall be allocated proportionately to the quantities tendered for.
A decision may be taken to make no award in respect of the invitation to tender.
2. At the same time as the minimum selling prices are fixed and in accordance with the same procedure, the following shall be fixed:
(a) the amount of the security to guarantee the fulfilment of the primary requirements concerning export of the butter in the unaltered state within the time limit laid down in Article 9 (3) or after processing within the time limit laid down in Article 10 (5);
(b) the coefficient applying to the monetary compensatory amounts applicable, where appropriate, to the butter sold.
3. The minimum selling prices referred to in paragraph 1, the prices to be paid by successful tenderers and the securities referred to in Article 6 and in paragraph 2 shall be converted into national currency using the agricultural conversion rate applicable on the closing date for the submission of tenders in response to the special invitation to tender concerned.
4. Obligations arising under the invitation to tender shall not be transferable.
5. Tenderers shall be informed individually forthwith by the intervention agency of the outcome of their tenders.
6. The intervention agencies shall issue removal orders showing:
(a) the quantity in respect of which the security has been lodged;
(b) the cold storage depot in which it is stored;
(c) the closing date for removal.
Article 8
1. The successful tenderers shall remove the butter which has been sold to them within 45 days following the closing date for the submission of tenders under the special invitation to tender concerned.
The butter may be removed in batches, each of which may not be less than 15 tonnes.
Except in case of force majeure, where the butter is not removed within the time limit referred to in the first subparagraph, storage shall be at the expense of the successful tenderer with effect from the first day following the expiry of that time limit.
2. The successful tenderer shall pay the intervention agency within the time limit referred to in paragraph 1, before removal of the butter, and in respect of each quantity he removes, the price shown in their tenders and shall lodge the security referred to in Article 7 (2). Securities shall be lodged in the Member State where the tender is submitted.
However, if the tender states that the butter is to be processed into concentrated butter in a Member State other than that of submission of the tender, the security referred to in Article 7 (2) may be lodged with the competent authority designated by the former Member State.
In that case, the intervention agency concerned shall inform the competent authority of the latter Member State of the facts resulting in release or seizure of the security.
Except in cases of force majeure, where the successful tenderer has failed to make the payment referred to in the first subparagraph within the time limit laid down, in addition to the loss of the security referred to in Article 6 (1), the sale shall be cancelled for the remaining quantities.
Article 9
1. Butter for export in the unaltered state shall be supplied by the intervention agency in packaging bearing at least one of the following descriptions in clearly visible and legible letters:
- Mantequilla destinada a la exportación con arreglo al Reglamento (CEE) no 3378/91;
- Smoer bestemt til udfoersel i henhold til forordning (EOEF) nr. 3378/91;
- Butter zur Ausfuhr - Verordnung (EWG) Nr. 3378/91;
- Voytyro poy proorizetai na exachthei vasei toy kanonismoy (EOK) arith. 3378/91·
- Butter for export under Regulation (EEC) No 3378/91;
- Beurre destiné à être exporté au titre du règlement (CEE) no 3378/91;
- Burro destinato ad essere esportato nel quadro del regolamento (CEE) n. 3378/91;
- Boter voor uitvoer in het kader van Verordening (EEG) nr. 3378/91;
- Manteiga destinada à exportaçao em conformidade com o Regulamento (CEE) no 3378/91.
2. The butter referred to in paragraph 1 may be exported in its original packaging or after having been repackaged.
If the butter is repackaged, the packaging shall bear at least one of the descriptions in paragraph 1 in clearly visible, legible letters.
3. The acceptance by the customs service of the export declaration for the butter referred to in this Article must take place in the Member State where the butter has been removed from store, within 90 days following the closing date for the submission of tenders under the special invitation to tender concerned.
Article 10
1. Butter sold pursuant to this Regulation may be exported after processing.
2. In that case, the tender must:
- specify the quantity of butter sold that will be processed,
- include an undertaking to notify the competent authorities, before the removal of the butter, of the undertakings in which processing will be carried out, approved for that purpose by the Member State on whose territory the processing will take place in accordance with Article 10 of Commission Regulation (EEC) No 570/88 (9).
3. The butter shall be supplied by the intervention agency in packaging bearing at least one of the following descriptions in clearly visible and legible letters:
- Mantequilla destinada a la transformación [Reglamento (CEE) no 3378/91];
- Smoer til forarbejdning [forordning (EOEF) nr. 3378/91];
- Zur Verarbeitung bestimmte Butter [Verordnung (EWG) Nr. 3378/91];
- Voytyro poy proorizetai gia metapoiisi [Kanonismos (EOK) arith. 3378/91];
- Butter for processing [Regulation (EEC) No 3378/91];
- Beurre destiné à la transformation [règlement (CEE) no 3378/91];
- Burro destinato alla trasformazione [regolamento (CEE) n. 3378/91];
- Boter voor verwerking [Verordening (EEG) nr. 3378/91];
- Manteiga destinada à transformaçao [Regulamento (CEE) no 3378/91].
4. The total quantity of butter referred to in the first indent of paragraph 2 above shall be processed at the undertakings referred to in the second indent of the same paragraph into a product with a milk fat content by weight of more than 99,5 % and shall produce at least 100 kilograms of concentrated butter per 122,1 kilograms of butter used.
Processed butter as referred to in the first subparagraph shall be exported after repackaging in packaging bearing at least one of the following descriptions in clearly visible, legible letters:
- Mantequilla concentrada destinada a la exportación con arreglo al Reglamento (CEE) no 3378/91;
- Koncentreret smoer bestemt til udfoersel i henhold til forordning (EOEF) nr. 3378/91;
- Zur Ausfuhr bestimmtes Butterfett - Verordnung (EWG) Nr. 3378/91;
- Sympyknomeno voytyro poy proorizetai na exachthei vasei toy kanonismoy (EOK) arith. 3378/91;
- Concentrated butter for export under Regulation (EEC) No 3378/91;
- Beurre concentré destiné à être exporté au titre du règlement (CEE) no 3378/91;
- Burro concentrato destinato all'esportazione a norma del regolamento (CEE) n. 3378/91;
- Boterconcentraat voor uitvoer op grond van Verordening (EEG) nr. 3378/91;
- Manteiga concentrada destinada à exportaçao em conformidade com o Regulamento (CEE) no 3378/91;
5. The acceptance by the customs service of the export declaration for the butter processed in accordance with this Article must take place in the Member State where the butter has been processed, within 120 days following the closing date for the submission of tenders under the special invitation to tender concerned.
6. From the time of removal of the butter and until the final product is exported, the butter referred to in Article 9 and products processed in accordance with paragraphs 4 and 5 shall be placed under customs control or shall be subject to an administrative control offering equivalent guarantees.
Article 11
The competent authorities of the Member States on whose territory the processing and repackaging operations referred to in Article 10 are carried out shall supervise those operations.
The costs of such supervision shall be borne by the operator concerned.
Article 12
1. Except in cases of force majeure, the security referred to in Article 7 (2) shall be forfeit in proportion to the quantities for which the proof referred to in Article 18 of Regulation (EEC) No 569/88 is not furnished within 12 months of the date of acceptance of the export declaration.
However, if the proof is furnished within the 18 months following the abovementioned period, 85 % of the security shall be reimbursed.
2. The provisions of Commission Regulations (EEC) No 569/88 and (EEC) No 2220/85 (10) shall apply except where this Regulation provides otherwise.
The special entries to be made in Sections 104 and 106 of the control copy shall be as set out in Part I, point 113 and Part II, point 40 of the Annex to Regulation (EEC) No 569/88.
Article 13
1. The following point and the relevant footnote shall be added to Part I 'products to be exported in the same state as that in which they were when removed from intervention stock' of the Annex to Regulation (EEC) No 569/88:
'113. Commission Regulation (EEC) No 3378/91 of 20 November laying down detailed rules for the sale of butter from intervention stocks for export (113);
(113) OJ No L 319, 21. 11. 1991, p. 40'.
2. The following point 40 and the relevant footnote shall be added to Part II 'Products subject to a use and/or destination other than that mentioned under 1' of the Annex to Regulation (EEC) No 569/88:
'40. Commission Regulation (EEC) No 3378/91 of 20 November 1991 laying down detailed rules for the sale of butter from intervention stocks for export (40):
(a) on the dispatch of the butter for processing:
- Section 104:
- destinada a la transformación y exportación posterior [Reglamento (CEE) no 3378/91];
- til forarbejdning og senere eksport [forordning (EOEF) nr. 3378/91];
- zur Verarbeitung und spaeteren Ausfuhr bestimmt [Verordnung (EWG) Nr. 3378/91];
- proorizomeno gia metapoiisi kai gia metepeita exagogi [Kanonismos (EOK) arith. 3378/91];
- intended for processing and, subsequently, export [Regulation (EEC) No 3378/91];
- destiné à la transformation et à l'exportation [règlement (CEE) no 3378/91];
- destinato alla trasformazione e alla successiva esportazione [regolamento (CEE) n. 3378/91];
- bestemd om te worden verwerkt en vervolgens te worden uitgevoerd [Verordening (EEG) nr. 3378/91];
- destinada à transformaçao e à exportaçao posterior [Regulamento (CEE) no 3378/91];
- Section 106:
the closing date for removing the butter
(b) On the export of the finished product:
- Section 104:
- Mantequilla concentrada destinada a la exportación [Reglamento (CEE) no 3378/91];
- Koncentreret smoer bestemt til eksport [forordning (EOEF) nr. 3378/91];
- zur Ausfuhr bestimmtes Butterfett [Verordnung (EWG) Nr. 3378/91];
- Sympyknomeno voytyro proorizomeno gia exagogi [Kanonismos (EOK) arith. 3378/91];
- Concentrated butter for export [Regulation (EEC) No 3378/91];
- Beurre concentré destiné à l'exportation [règlement (CEE) no 3378/91];
- Burro concentrato destinato all'esportazione [regolamento (CEE) n. 3378/91];
- Boterconcentraat bestemd voor uitvoer [Verordening (EEG) nr. 3378/91];
- Manteiga concentrada destinada à exportaçao [Regulamento (CEE) no 3378/91];
- Section 106:
- the closing date for removing the butter,
- the weight of butter used to manufacture the quantity of the finished product shown in Section 103.
(40) OJ No L 319, 21. 11. 1991, p. 40.'
Article 14
1. No export refund shall be granted in respect of the butter sold under this Regulation. Accession compensatory amounts shall not apply. The monetary compensatory amounts applicable to butter sold under this Regulation shall be multiplied by the coefficient fixed in accordance with Article 7 (2).
2. The removal orders referred to in Article 3 of Regulation (EEC) No 569/88, export declarations and, where applicable, T5 control copies shall bear the following:
'Sin restitución [Reglamento (CEE) no 3378/91];
Uden restitution [Forordning (EOEF) nr. 3378/91];
Keine Erstattung [Verordnung (EWG) Nr. 3378/91];
Choris epistrofi [Kanonismos (EOK) arith. 3378/91];
Without refund [Regulation (EEC) No 3378/91];
Sans restitution [Règlement (CEE) no 3378/91];
Senza restituzione [Regolamento (CEE) n. 3378/91];
Zonder restitutie [Verordening (EEG) nr. 3378/91];
Sem restituiçao [Regulamento (CEE) no 3378/91].'.
Article 15
The Member States shall notify the Commission forthwith of the quantities of butter which are the subject of:
- a sales contract,
- removal,
- processing,
under this Regulation.
Article 16
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Communities. This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 20 November 1991. For the Commission
Ray MAC SHARRY
Member of the Commission
(1) OJ No L 148, 28. 6. 1968, p. 13. (2) OJ No L 150, 15. 6. 1991, p. 19. (3) OJ No L 164, 24. 6. 1985, p. 11. (4) OJ No L 150, 15. 6. 1991, p. 38. (5) OJ No L 169, 18. 7. 1968, p. 1. (6) OJ No L 187, 13. 7. 1991, p. 1. (7) OJ No L 55, 1. 3. 1988, p. 1. (8) OJ No L 308, 9. 11. 1991, p. 49. (9) OJ No L 55, 1. 3. 1988, p. 31. (10) OJ No L 205, 3. 8. 1985, p. 5.
