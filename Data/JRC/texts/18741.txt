COMMISSION REGULATION (EEC) No 2225/93 of 27 July 1993 amending Regulation (EEC) No 2719/92 on the accompanying administrative document for the movement under duty-suspension arrangements of products subject to excise duty
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Directive 92/12/EEC of 25 February 1992 on the general arrangements for products subject to excise duty and on the holding, movement and monitoring of such products (1), as amended by Directive 92/108/EEC (2), and in particular
Articles 15 (5) and 18 (1) thereof,
Having regard to the opinion of the Committee on Excise Duties,
Whereas Article 15 (5) of Directive 92/12/EEC provides that an authorized warehousekeeper of dispatch or his agent may, during the carriage of products, choose an alternative place of delivery without special authorization from the relevant competent authority; whereas this has to be taken into account as regards the form of the accompanying document and its explanatory notes;
Whereas the fact that all Member States now issue excise numbers to their authorized warehousekeepers and their registered traders requires the obligatory indication of the excise number in the accompanying document; whereas, as a consequence, with the exception of non-registered traders, there is no further need to indicate the VAT identification number either for the consignor or for the consignee in the accompanying document;
Whereas there is a need to simplify and to facilitate the procedure as regards those accompanying documents which have been drawn up by an automatic or electronic data-processing system; whereas Member States should be authorized to enable the consignor, under certain conditions, to dispense with a signature on documents;
Whereas Commission Regulation (EEC) No 2719/92 (3) should therefore be amended,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EEC) No 2719/92 is amended as follows:
1. Article 1 is replaced by the following:
'Article 1
The document shown in Annex I shall be used as the administrative document accompanying the movement under duty-suspension arrangements of products subject to excise duty within the meaning of Article 3 (1) of Directive 92/12/EEC. The instructions concerning completion of the document and the procedures for its use are shown on the reverses of copy 1 of this document.'
2. The following sentence is added to Article 2 (2):
'The document shall be marked conspicuously with the following indication:
"Commercial accompanying document for the movement of products subject to excise duty under duty suspension".'
3. The following Article 2a is inserted:
'Article 2a
1. In cases where the accompanying document is drawn up by an electronic or automatic data-processing system, the competent authorities may authorize the consignor not to sign the document but to replace the signature by the special stamp shown in Annex II. Such authorization shall be subject to the condition that the consignor has previously given a written undertaking to those authorities that he will be liable for all risks inherent in intra-Community movements of products subject to excise duty under duty-suspension arrangements involving consignments which travel under cover of an accompanying document bearing such special stamp.
2. Accompanying documents drawn up in accordance with paragraph 1 shall contain in that part of Box 24 which is reserved for the consignor's signature, one of the following indications:
- Dispensa de firma
- Fritaget for underskrift
- Freistellung von der Unterschriftsleistung
- Den apaiteitai ypografi
- Signature waived
- Dispense de signature
- Dispensa dalla firma
- Van ondertekening vrijgesteld
- Dispensa de assinatura.
3. The special stamp referred to in paragraph 1 shall be placed in the upper right corner of Box A of the administrative accompanying document or, plainly visible, in the corresponding Box of a commercial document. The consignor may also be authorized to pre-print the special stamp.'
4. The Annex is replaced by Annexes I and II hereto.
Article 2
Existing stocks of the form which is to be replaced by the new form of Annex I may be used until exhausted.
Article 3
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 27 July 1993.
For the Commission
Christiane SCRIVENER
Member of the Commission
(1) OJ No L 76, 23. 3. 1992, p. 1.
(2) OJ No L 390, 31. 12. 1992, p. 124.
(3) OJ No L 276, 19. 9. 1992, p. 1.
ANNEX I
'ANNEX
'
ANNEX II
