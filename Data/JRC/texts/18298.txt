Decision of the EEA Joint Committee
No 131/2004
of 24 September 2004
amending Annex XV (State aid) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex XV to the Agreement was amended by Decision of the EEA Joint Committee No 80/2004 of 8 June 2004 [1].
(2) Commission Regulation (EC) No 363/2004 of 25 February 2004 amending Regulation (EC) No 68/2001 on the application of Articles 87 and 88 of the EC Treaty to training aid [2] is to be incorporated into the Agreement.
(3) Commission Regulation (EC) No 364/2004 of 25 February 2004 amending Regulation (EC) No 70/2001 as regards the extension of its scope to include aid for research and development [3] is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
1. Point 1d (Commission Regulation (EC) No 68/2001) of Annex XV to the Agreement shall be amended as follows:
1.1. the following shall be added:
", as amended by:
- 32004 R 0363: Commission Regulation (EC) No 363/2004 of 25 February 2004 (OJ L 63, 28.2.2004, p. 20).";
1.2. adaptation (c) shall be replaced by the following:
"Article 1 shall read: "This Regulation applies to training aid in all sectors covered by Articles 61 to 64 of the EEA Agreement, with the exception of aid falling within the scope of Council Regulation (EC) No 1407/2002."";
"(k) in Article 7(3), first paragraph, the words "Article 27 of Council Regulation (EC) No 659/1999" shall read "Article 27 of Protocol 3 to the Surveillance and Court Agreement";
(l) in Article 7a, the term "Article 88(3) of the Treaty" shall read "Article 1(3) of Protocol 3 to the Surveillance and Court Agreement". The term "compatible with the common market" shall read "compatible with the functioning of the EEA Agreement". The term "Article 87(3) of the Treaty" shall read "Article 61(3) of the EEA Agreement".";
2.1. the following shall be added:
", as amended by:
- 32004 R 0364: Commission Regulation (EC) No 364/2004 of 25 February 2004 (OJ L 63, 28.2.2004, p. 22).";
2.2. adaptations (e), (f), (g), (h) and (i) shall become adaptations (g), (h), (i), (j) and (k), respectively;
"(e) The terms "Article 87(3)(a)" and "Article 87(3)(a) of the Treaty" shall read "Article 61(3)(a) of the EEA Agreement";
(f) The terms "Article 87(3)(c)" and "Article 87(3)(c) of the Treaty" shall read "Article 61(3)(c) of the EEA Agreement";"
2.4. in new adaptation (i), the words "with regard to Articles 4 and 5," shall be inserted at the beginning of the adaptation text;
2.5. in new adaptation (j), the words "In Articles 3 and 5" shall be replaced by "In Articles 3, 5, 5a, 5b, 5c and 9a";
2.6. the new adaptation (k) shall be replaced by the following:
"In Article 4(2), the term "Article 87(3)(a) and (c) of the Treaty" shall read "Article 61(3)(a) and (c) of the EEA Agreement".";
"(l) in Article 6a(2), the words "Community Guidelines on State aid for rescuing and restructuring firms in difficulty" shall read "the Community guidelines on State aid for rescuing and restructuring firms in difficulty and the EFTA Surveillance Authority's Procedural and Substantive rules in the field of State aid, Chapter 16 on aid for rescuing and restructuring firms in difficulty";
(m) in Article 9, the words "Article 27 of Council Regulation (EC) No 659/1999" shall read "Article 27 of Protocol 3 to the Surveillance and Court Agreement"."
Article 2
The texts of Regulations (EC) Nos 363/2004 and 364/2004 in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 25 September 2004, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [4].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 24 September 2004.
For the EEA Joint Committee
The President
Kjartan Jóhannsson
[1] OJ L 349, 25.11.2004, p. 37.
[2] OJ L 63, 28.2.2004, p. 20.
[3] OJ L 63, 28.2.2004, p. 22.
[4] No constitutional requirements indicated.
--------------------------------------------------
