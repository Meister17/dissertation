Action brought on 24 August 2005 by the Commission of the European Communities against the United Kingdom
An action against the United Kingdom was brought before the Court of Justice of the European Communities on 24 August 2005 by the Commission of the European Communities, represented by Antonio Aresu and Nicola Yerrell, acting as Agents, with an address for service in Luxembourg.
The Commission claims that the Court should:
1. find that the United Kingdom has failed its obligations under the EC Treaty by failing to adopt all the laws, regulations and administrative provisions necessary to comply with Directive 2001/95/EC of 3 December 2001 on general product safety [1] and/or by failing to inform the Commission thereof;
2. condemn the United Kingdom to bear the costs of the procedure.
Pleas in law and main arguments
The period within which the directive had to be transposed expired on 15 January 2004.
[1] OJ L 11, 15.01.2002, p. 4
--------------------------------------------------
