Agreement
between the European Union and Bosnia and Herzegovina (BiH) on the activities of the European Union Police Mission (EUPM) IN BiH
THE EUROPEAN UNION,
on the one hand,
and BOSNIA AND HERZEGOVINA,
hereinafter referred to as the "Host Party",
on the other hand,
Together hereinafter referred to as the "Participating Parties",
TAKING INTO ACCOUNT
- the presence of the United Nations International Police Task Force (IPTF) in Bosnia and Herzegovina since 1996 and the offer of the European Union to ensure, by 1 January 2003, the follow-on to the IPTF in Bosnia and Herzegovina,
- the acceptance by Bosnia and Herzegovina of that offer,
- the adoption by the Council of the European Union on 11 March 2002 of Joint Action 2002/210/CFSP on the European Union Police Mission (EUPM), stating that the EUPM should establish sustainable policing arrangements under BiH ownership in accordance with best European and international practice, and thereby raising current BiH police standards,
HAVE AGREED AS FOLLOWS:
Article 1
Mandate
1. The European Union Police Mission, hereinafter referred to as "EUPM", shall establish its main headquarters in Sarajevo.
2. The EUPM shall also establish such other offices in Bosnia and Herzegovina as may be decided by the Head of Mission/Police Commissioner, in consultation with the Host Party. To this end, the EUPM shall deploy an initial total of 24 monitoring units co-located within the various Bosnia and Herzegovina Police structures at medium-high level, including with Entities, Public Security Centres, Cantons, State Intelligence Protection Agency, State Border Services and within the Brcko district.
3. The EUPM, entrusted with the necessary authority to monitor, mentor and inspect, should achieve its goal by the end of 2005.
4. The EUPM shall operate in accordance with its mandate as laid down in Article 1(2) of Joint Action 2002/210/CFSP.
5. The EUPM shall be autonomous with regard to the execution of its functions under this Agreement.
6. The Host Party shall provide the EUPM with all information and shall extend full cooperation as necessary for the accomplishment of the EUPM's objectives. The Host Party may appoint a liaison police officer to the EUPM.
Article 2
Composition
1. The EUPM shall be composed of the Head of Mission/Police Commissioner and other members of the EUPM.
2. The Head of Mission/Police Commissioner of the EUPM shall be appointed by the Council of the European Union. Other members of the EUPM shall be assigned to specific appointments by the Head of Mission.
3. Other members of the EUPM shall be composed of:
(a) police officers, seconded by the Member States of the European Union. Non-EU Member States may also appoint police officers to the EUPM, and thereby be, together with the European Union and its Member States, Sending Parties;
(b) international civilian staff, seconded by the Sending Parties, or recruited on a contractual basis by the EUPM as required;
(c) locally recruited personnel, who may be hired by the EUPM as required. Upon request of the Head of Mission/Police Commissioner, the Host Party shall facilitate the recruitment of such qualified local staff by the EUPM.
4. The number of members of the EUPM shall be determined by the Head of Mission/Police Commissioner.
Article 3
Chain of responsibilities
1. The EUPM in Bosnia and Herzegovina shall operate under the responsibility of the Head of Mission/Police Commissioner, who shall lead the EUPM and assume its day-to-day management.
2. The Head of Mission/Police Commissioner shall report to the Secretary-General/High Representative for the Common Foreign and Security Policy (SG/HR) through the European Union Special Representative (EUSR) in Bosnia and Herzegovina.
3. The Head of Mission/Police Commissioner shall inform the Host Party regularly on the activities of the EUPM.
Article 4
Status
1. The EUPM shall be granted the status equivalent to that of a diplomatic mission.
2. The main headquarters in Sarajevo, other offices, and all means of transport of the EUPM shall be inviolable.
3. EUPM personnel shall be granted all privileges and immunities equivalent to that of diplomatic agents granted under the Vienna Convention on Diplomatic Relations of 18 April 1961, subject to which the EU Member States and other Sending Parties shall have priority of jurisdiction. These privileges and immunities shall be granted to EUPM personnel during their mission, and thereafter, with respect to official acts previously performed in the exercise of their mission.
4. EUPM's administrative and technical staff shall enjoy a status equivalent to that enjoyed, in accordance with the Vienna Convention on Diplomatic Relations, by administrative and technical staff from Sending Parties employed in embassies. These privileges and immunities shall be granted to EUPM's administrative and technical staff during their mission, and thereafter, with respect to official acts previously performed in the exercise of their mission.
5. EUPM's locally hired auxiliary personnel shall enjoy a status equivalent to that enjoyed, in accordance with the Vienna Convention on Diplomatic Relations, by locally employed staff in embassies.
6. The Host Party shall facilitate all entries and departures of the Head of Mission/Police Commissioner and members of the EUPM, into and from the territory of Bosnia and Herzegovina. The EUPM shall provide the Host Party with a list of members of the EUPM and inform the Host Party in advance of the first arrival and final departure of personnel belonging to the EUPM.
7. The Host Party recognises the right of the Sending Parties and of the EUPM to import, free of duty or other restrictions, equipment, provisions, supplies and other goods required for the exclusive and official use of the EUPM. The Host Party also recognises their right to purchase such items on the territory of the Host Party as well as to export or otherwise dispose of such equipment, provisions, supplies and other goods so purchased or imported.
8. The Host Party also recognises the right of the EUPM personnel, as well as of the EUPM's administrative and technical staff, to purchase and/or import free of duty or other restrictions items required for their own personal use, and to export such items.
Article 5
Arms and dress
1. Members of the EUPM shall not carry arms.
2. Members of the EUPM may wear their national uniform or civilian dress, with distinctive EUPM identification. Members of the EUPM shall carry their national passport, as well as an EUPM identity card.
Article 6
Activities
1. The Host Party shall take all necessary measures for the protection, safety and security of the EUPM and its members. Any specific provisions, proposed by the Host Party, shall be agreed with the Head of Mission/Police Commissioner before implementation.
2. Members of the EUPM shall not undertake any action or activity incompatible with the impartial nature of their duties.
3. The EUPM and its members shall enjoy, together with its means of transport and equipment, the freedom of movement, necessary for carrying out the mandate of the Mission.
4. When conducting their activities, members of the EUPM may be accompanied by an interpreter and, at the request of the EUPM, by an escort officer appointed by the Host Party.
5. The EUPM may display the flag of the European Union on its main headquarters in Sarajevo, and otherwise as decided by the Head of Mission/Police Commissioner.
6. Vehicles, and other means of transport of the EUPM shall carry a distinctive Mission identification, which shall be notified to the relevant authorities.
Article 7
Travel and transport
1. Vehicles and other means of transport of the EUPM shall not be subject to compulsory registration or licensing, and all vehicles shall carry third party insurance.
2. The EUPM may use roads, bridges, canals and other waters, port facilities and airfields without the payment of dues, tolls or other charges.
3. The Host Party shall facilitate the EUPM in operating its own vehicles and other means of transport.
Article 8
Communications
1. The EUPM and its members shall have access, at the lowest cost, to appropriate telecommunications equipment of the Host Party or under the control of the Host Party for the purpose of their activities, including for communicating with diplomatic and consular representatives of the Sending Parties.
2. The EUPM and its members shall enjoy the right to unrestricted communication by their own radios (including satellite, mobile and hand-held radios), telephones, telegraphs, facsimiles or any other means. The Host Party shall provide, after signature of this Agreement, the frequencies on which radios can operate.
Article 9
Accommodation and practical arrangements
1. The Government of Bosnia and Herzegovina agrees, if requested, to assist the EUPM in finding suitable offices and accommodation.
2. As appropriate, the Participating Parties shall agree on other provisions concerning privileges and immunities as well as on practical arrangements, including urgent medical assistance, emergency evacuation, designation of official representatives as points of contacts, as well as travel documentation requirements.
Article 10
Entry into force
This Agreement shall enter into force upon signature. It shall remain in force for the duration of the mandate of the EUPM.
Done at Sarajevo, on
>PIC FILE= "L_2002293EN.000401.TIF">
2002, in the English language in four copies.
For the European Union
>PIC FILE= "L_2002293EN.000402.TIF">
For Bosnia and Herzegovina
>PIC FILE= "L_2002293EN.000403.TIF">
