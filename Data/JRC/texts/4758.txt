TRANSLATION
Protocol
on the implementation of the Alpine Convention of 1991 in the field of soil conservation
Soil Conservation Protocol
Preamble
THE FEDERAL REPUBLIC OF GERMANY,
THE REPUBLIC OF AUSTRIA,
THE FRENCH REPUBLIC,
THE ITALIAN REPUBLIC,
THE PRINCIPALITY OF LIECHTENSTEIN,
THE PRINCIPALITY OF MONACO,
THE REPUBLIC OF SLOVENIA,
THE SWISS CONFEDERATION,
and
THE EUROPEAN COMMUNITY,
IN ACCORDANCE with their task, arising from the Convention on the Protection of the Alps (Alpine Convention) of 7 November 1991, of pursuing a comprehensive policy for the protection and the sustainable development of the Alpine region;
IN COMPLIANCE with their obligations under Article 2(2) and (3) of the Alpine Convention;
AIMING to reduce quantitative and qualitative soil impairments, in particular by applying agricultural and silvicultural production processes which have a minimal detrimental impact on the soil, by using land economically, controlling erosion and restricting soil sealing;
AWARE of the fact that the protection of the Alpine soils, their sustainable management and the restoration of their natural functions in impaired locations are matters of general interest;
RECOGNISING that the Alps, constituting one of the largest continuous natural areas in Europe, are characterised by an ecological diversity and by highly sensitive ecosystems whose functionality must be preserved;
CONVINCED that the local population must be able to determine its own social, cultural and economic development plan and take part in its implementation in the existing institutional framework;
AWARE that, on the one hand, the Alps are an important living and economic environment for the resident populations and a recreational environment for the populations of other regions and that, on the other hand, the preservation of soil functions is jeopardised by diverging claims on soil utilisation which clash within the narrow confines of the Alpine region; and that for this reason, economic interests must be reconciled with ecological requirements;
RECOGNISING that the soil occupies a special position within ecosystems, that its formation as well as the regeneration of impaired soils happen very slowly, that increased soil erosion is to be expected owing to topographical conditions in the Alpine region, and that the soil constitutes a sink for harmful substances while contaminated soils can be a source of inputs of those substances into neighbouring ecosystems, thus putting at risk humans, animals and plants;
AWARE that soil utilisation, especially for the purposes of human settlement, the development of trade and industry, infrastructures, the extraction of mineral resources, tourism, agriculture, forestry and transport can lead to quantitative or qualitative soil impairments and that accordingly, adequate integrated measures should be proposed to prevent, control and repair damage to the soil;
CONSIDERING that soil conservation has manifold implications for other policies in the Alpine region and should therefore be coordinated in a cross-disciplinary and cross-sectoral fashion;
CONVINCED that certain problems can only be resolved in a cross-border framework and require joint measures on the part of the Alpine States, to be implemented by the Signatories in accordance with the available means,
HAVE AGREED AS FOLLOWS:
CHAPTER I
GENERAL OBLIGATIONS
Article 1
Objectives
1. This Protocol serves to implement the obligations in the field of soil conservation entered into by the Contracting Parties to the Alpine Convention.
2. The Alpine soil shall be preserved in a sustainable manner to allow it to perform:
1. its natural functions as
(a) a livelihood resource and a living environment for humans, animals, plants and micro-organisms,
(b) a characteristic element of nature and the landscape,
(c) an integral part of the ecological balance, especially with regard to its water and nutrient cycles,
(d) a conversion and compensating medium to offset inputs of substances, especially due to its filtering, buffering and storage qualities, in particular for the protection of groundwater,
(e) a genetic reservoir,
2. its function as an archive of natural history and the history of civilisation, as well as
3. its functions as
(a) a location for agricultural use including pasture farming and forestry,
(b) a space for human settlement and tourism activities,
(c) a location for other commercial usages, for transport, supply and distribution, and water and waste disposal,
(d) a source of raw materials.
In particular, the ecological functions of soil, which are essential elements of the ecological balance, shall be safeguarded and preserved both qualitatively and quantitatively on a long-term basis. The restoration of impaired soils shall be promoted.
3. The measures to be taken are aimed specifically at soil utilisation which suits its location, at the economical use of land resources, at the avoidance of erosion and detrimental changes to the soil structure, and at minimising the input of substances harmful to the soil.
4. The diversity of soils, which is typical of the Alpine region, and its characteristic locations shall be preserved and promoted in particular.
5. In this endeavour the principle of prevention, which comprises the safeguarding of the functionality of soils and the possibility to use them for various purposes as well as their availability to future generations with a view to sustainable development, is of particular significance.
Article 2
Fundamental obligations
1. The Contracting Parties undertake to initiate the requisite legal and administrative measures for ensuring the conservation of soils in the Alpine region. The respective national authorities shall be responsible for monitoring those measures.
2. If there is a risk of serious and sustained damage to the functionality of soils, protection shall, as a matter of principle, be given priority over utilisation.
3. The Contracting Parties shall explore the possibilities of supporting, through fiscal and/or financial measures, the actions for soil conservation in the Alpine region targeted by this Protocol. Measures compatible with soil conservation and with the objectives of a prudent and environmentally sound utilisation of soils shall be specially supported.
Article 3
Taking account of the objectives in other policies
The Contracting Parties undertake to take account of the objectives of this Protocol in their other policies as well. In the Alpine region, this applies specifically to regional planning, settlement and transport, energy management, agriculture and forestry, raw material extraction, trade and industry, tourism, nature conservation and landscape upkeep, water and waste management, and clean air.
Article 4
Participation of regional and local authorities
1. Each Contracting Party shall define, within its existing institutional framework, the best level of coordination and cooperation between the institutions and regional and local authorities directly concerned so as to encourage shared responsibility, in particular to exploit and develop synergies when implementing soil conservation policies and the resulting measures in the Alpine region.
2. The regional and local authorities directly concerned shall be involved in the various stages of preparing and implementing these policies and measures, within their sphere of competence and within the existing institutional framework.
Article 5
International cooperation
1. The Contracting Parties shall encourage stronger international cooperation among the competent institutions, especially with regard to the drawing up of soil registers, soil monitoring, the designation and monitoring of protected and impaired areas and danger zones, the provision and harmonisation of databases, the coordination of Alpine-specific soil conservation research, and mutual reporting.
2. The Contracting Parties undertake to remove obstacles to international cooperation between territorial authorities in the Alpine region, and to encourage solutions to shared problems at the most suitable level.
3. If the definition of measures relating to soil conservation falls within the sphere of national or international competence, the territorial authorities shall be given possibilities to efficiently represent the interests of the population.
CHAPTER II
SPECIFIC MEASURES
Article 6
Designation of protected areas
The Contracting Parties shall see to it that soils worthy of protection are included in the designation of protected areas. Specifically, soil and rock formations which have particularly characteristic features or a particular significance for the documentation of earth’s history, shall be preserved.
Article 7
Economical and prudent use of soils
1. In drawing up and implementing plans and/or programmes according to Article 9(3) of the Protocol on Spatial Planning and Sustainable Development, matters regarding soil conservation, especially the economical use of soil and land, shall be taken into consideration.
2. In order to limit soil sealing and soil consumption, the Contracting Parties shall provide for space-saving construction and an economical use of soil resources. They shall preferably seek to keep the development of human settlements within existing boundaries and to limit settlement growth outside these boundaries.
3. When assessing the spatial and environmental compatibility of large-scale projects in the fields of trade and industry, construction and infrastructure, especially in the transport, energy and tourism sectors, soil conservation and the scarcity of space in the Alpine region shall be taken into account within the framework of the national procedures.
4. Where natural conditions allow it, disused or impaired soils, especially landfills, slag heaps, infrastructures or ski runs, shall be restored to their original state or shall be recultivated.
Article 8
Economical use and prudent extraction of mineral resources
1. The Contracting Parties shall see to it that mineral resources are used economically. They shall work towards ensuring that preference is given to the utilisation of substitute materials and that recycling options are fully used or their development is encouraged.
2. When extracting, processing and utilising mineral resources, impairments of other soil functions shall be reduced to a minimum. In those areas which are particularly important for the protection of soil functions and in areas specifically designated as drinking water resources, the extraction of mineral resources shall be foregone.
Article 9
Conservation of soils in wetlands and moors
1. The Contracting Parties undertake to preserve high moors and lowland moors. To achieve this objective, the use of peat shall be discontinued completely in the medium term.
2. Drainage schemes in wetlands and moors shall be limited to the upkeep of existing networks unless there are sound reasons for exceptions. Remedial measures shall be promoted to minimise the environmental impact of existing drainage systems.
3. On principle, moor soils shall not be utilised or, when used for agricultural purposes, shall be managed so that their characteristic features remain intact.
Article 10
Designation and management of endangered areas
1. The Contracting Parties agree to draw up maps of Alpine areas which are endangered by geological, hydrogeological and hydrological risks, in particular by land movement (mass slides, mudslides, landslides), avalanches and floods, to register those areas and to designate danger zones when necessary. If applicable, seismic risks shall also be considered.
2. The Contracting Parties shall make sure that engineering techniques are used in endangered areas which are as compatible with nature as possible, and that local and traditional building materials which suit the local countryside are used. These measures shall be supported by appropriate silvicultural measures.
Article 11
Designation and management of Alpine areas threatened by erosion
1. The Contracting Parties undertake to map Alpine areas threatened by extensive erosion on the basis of comparable criteria for quantifying soil erosion, and to register those areas in as far as this is necessary for the protection of material goods.
2. Soil erosion shall be limited to the inevitable minimum. Areas damaged by erosion and land movement shall be rehabilitated in as far as this is necessary for the protection of human beings and material goods.
3. To protect human beings and material goods, measures to control water erosion as well as measures to reduce surface run-off shall preferably comprise hydraulic, engineering and silvicultural techniques with minimal environmental impact.
Article 12
Agriculture, pasture farming and forestry
1. To ensure protection against erosion and harmful soil compaction, the Contracting Parties undertake to use sound practices in agriculture, pasture farming and forestry which are adapted to suit local conditions.
2. As regards the input of substances through the use of fertilisers, herbicides and pesticides, the Contracting Parties shall strive to elaborate and implement shared standards for sound expert practices. The type, quantity and time of fertilisation shall be suited to the needs of the plants, taking into account the nutrients available in the soil, the organic substance as well as the location of the plants and the conditions in which they are cultivated. This is achieved by using ecological/biological and integrated methods of cultivation, as well as by matching livestock to natural local growth conditions.
3. In Alpine pasture areas, the usage of mineral fertilisers and synthetic herbicides and pesticides in particular shall be minimised. The use of sewage sludges shall be foregone.
Article 13
Silvicultural and other measures
1. With regard to mountain forests which offer a high degree of protection to their own location, or above all to human settlements, transport infrastructures, croplands and similar areas, the Contracting Parties undertake to give priority to the protective function of these forests and to gear their silvicultural management towards preserving this function. Such mountain forests shall be preserved in their original locations.
2. Specifically, forests shall be used and maintained in such a way that soil erosion and harmful soil compaction are avoided. To achieve this, silvicultural measures adapted to local conditions as well as natural forest rejuvenation shall be promoted.
Article 14
Effects of tourism infrastructures
1. The Contracting Parties shall use their influence in the most appropriate manner to ensure that:
- detrimental effects of tourism activities on Alpine soils are avoided,
- soils impaired by intensive tourism are stabilised, especially and whenever possible by restoring the vegetation cover and applying environmentally sound engineering techniques. Further utilisation of the soils shall seek to prevent such damage from recurring,
- permits for the construction and levelling of ski runs in forests with a protective function are granted only in exceptional cases and with the proviso that compensatory action is taken, and that such permits are not granted for fragile areas.
2. Chemical and biological additives for the grooming of ski runs are permissible only if proof of their ecological harmlessness has been furnished.
3. Where significant damage to soils and vegetation is found to exist, the Contracting Parties shall take the necessary remedial action at the earliest possible point in time.
Article 15
Limiting inputs of harmful substances
1. The Contracting Parties shall do everything in their power to minimise, through preventive action, inputs of harmful substances into the soils through water, air, waste and other substances harmful to the environment. Preference shall be given to measures limiting emissions at their sources.
2. To avoid soil contamination when using dangerous substances, the Contracting parties shall issue technical regulations, provide for checks, carry out research programmes and engage in educational work.
Article 16
Environmentally compatible utilisation of gritting materials
The Contracting Parties undertake to minimise the use of gritting salt and, wherever possible, to use slippage-preventing and less contaminating materials such as gravel and sand.
Article 17
Contaminated soils, environmental liabilities, waste management concepts
1. The Contracting Parties undertake to survey and document their environmental liabilities and suspicious landfills (environmental liabilities register), to analyse the condition of those areas and to assess their hazard potential using comparable methods.
2. To avoid soil contamination and to ensure the environmentally compatible pretreatment, treatment and disposal of waste and residual materials, waste management concepts shall be drawn up and implemented.
Article 18
Further measures
The Contracting Parties may take measures regarding soil conservation which go beyond the measures provided for in this Protocol.
CHAPTER III
RESEARCH, EDUCATION AND INFORMATION
Article 19
Research and monitoring
1. The Contracting Parties shall cooperate closely to promote and harmonise research projects and systematic monitoring programmes which are conducive to achieving the objectives of this Protocol.
2. The Contracting Parties shall ensure that the national results of the research and systematic observation are integrated in a joint permanent observation and information system and that they are made accessible to the public under the existing institutional framework.
3. The Contracting Parties agree to coordinate their Alpine-specific research projects on soil conservation while taking into account other national and international research developments, and to envisage joint research activities.
4. Special attention shall be given to evaluations of soil sensitivity regarding diverse human activities, to assessments of the regenerative capacity of soils, and to the examination of the most suitable pertinent technologies.
Article 20
Establishment of harmonised databases
1. The Contracting Parties agree to create comparable databases (soil parameters, sampling, analysis, evaluation) within the framework of the Alpine monitoring and information system, and to establish possibilities for data exchange.
2. The Contracting Parties shall reach agreement about soil-endangering substances which require priority treatment, and they shall strive for comparable evaluation parameters.
3. The Contracting Parties shall strive to establish representative records of the condition of Alpine soils taking into account the geological and hydrogeological situation, on the basis of identical evaluation systems and harmonised methods.
Article 21
Establishment of permanent monitoring areas and coordination of environmental monitoring
1. The Contracting Parties undertake to establish permanent monitoring areas in the Alpine region and to integrate them in an Alpine-wide soil monitoring network.
2. The Contracting Parties agree to coordinate their national soil monitoring programmes with the environmental monitoring programmes for air, water, flora and fauna.
3. Within the framework of their monitoring programmes, the Contracting Parties shall establish soil sample databases according to comparable parameters.
Article 22
Education and information
The Contracting Parties shall promote the education and further training as well as the information of the public regarding the objectives, measures and implementation of this Protocol.
CHAPTER IV
IMPLEMENTATION, MONITORING AND EVALUATION
Article 23
Implementation
The Contracting Parties undertake to ensure the implementation of this Protocol by taking any appropriate measures within the existing institutional framework.
Article 24
Monitoring of compliance with obligations
1. The Contracting Parties shall regularly report to the Standing Committee on measures taken under this Protocol. The reports shall also cover the effectiveness of the measures taken. The Alpine Conference shall determine the intervals at which the reports must be submitted.
2. The Standing Committee shall examine these reports in order to ensure that the Contracting Parties have fulfilled their obligations under this Protocol. It may also ask for additional information from the Contracting Parties concerned or have recourse to other information sources.
3. The Standing Committee shall draw up a report on the compliance of the Contracting Parties with the obligations arising from the Protocol, for the attention of the Alpine Conference.
4. The Alpine Conference shall take note of this report. If it finds that obligations have not been met, it may adopt recommendations.
Article 25
Evaluation of the effectiveness of the provisions
1. The Contracting Parties shall regularly examine and evaluate the effectiveness of the provisions of this Protocol. They shall consider the adoption of appropriate amendments to this Protocol where necessary in order to achieve objectives.
2. The regional and local authorities shall be associated with this evaluation within the existing institutional framework. Non-governmental organisations active in this field may be consulted.
CHAPTER V
FINAL PROVISIONS
Article 26
Links between the Alpine Convention and the Protocol
1. This Protocol constitutes a Protocol to the Alpine Convention within the meaning of Article 2 thereof and any other relevant articles of the Convention.
2. Only Contracting Parties to the Alpine Convention may become a party to this Protocol. Any denunciation of the Alpine Convention also implies denunciation of this Protocol.
3. Where the Alpine Conference discusses matters relating to this Protocol, only the Contracting Parties to this Protocol may take part in the vote.
Article 27
Signature and ratification
1. This Protocol shall be open for signature by the Signatory States of the Alpine Convention and the European Community on 16 October 1998 and from 16 November 1998 in the Republic of Austria as the depositary.
2. This Protocol shall enter into force for the Contracting Parties which have expressed their agreement to be bound by the said Protocol three months after the date on which three States have deposited their instrument of ratification, acceptance or approval.
3. For Parties which express their agreement to be bound by the Protocol at a later date, the Protocol shall enter into force three months after the date of deposit of the instrument of ratification, acceptance or approval. After the entry into force of an amendment to the Protocol, any new Contracting Party to the said Protocol shall become a Contracting Party to the Protocol, as amended.
Article 28
Notifications
The depositary shall, in respect of this Protocol, notify each State referred to in the Preamble and the European Community of
(a) any signature,
(b) the deposit of any instrument of ratification, acceptance or approval,
(c) any date of entry into force,
(d) any declaration made by a Contracting Party or signatory,
(e) any denunciation notified by a Contracting Party, including the date on which it becomes effective.
In witness whereof the undersigned, being duly authorised thereto, have signed this Protocol.
Done at Bled on 16 October 1998 in the French, German, Italian and Slovene languages, the four texts being equally authentic, the original text being deposited in the Austrian State archives. The depositary shall send a certified copy to each of the signatory States.
--------------------------------------------------
