COUNCIL DIRECTIVE 1999/89/EC
of 15 November 1999
amending Directive 91/494/EEC on animal health conditions governing intra-Community trade in and imports from third countries of fresh poultrymeat
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 37 thereof,
Having regard to the proposal from the Commission(1),
Having regard to the opinion of the European Parliament(2),
Having regard to the opinion of the Economic and Social Committee(3),
Whereas:
(1) Article 3A(1) of Council Directive 91/494/EEC of 26 June 1991 on animal health conditions governing intra-Community trade in, and imports from third countries of fresh poultry meat(4) lays down the rules for Newcastle disease vaccination for flocks of origin of poultry meat destined for Member States or regions of Member States, the status of which has been recognised in accordance with Article 12(2) of Council Directive 90/539/EEC of 15 October 1990 on animal health conditions governing intra-Community trade in and imports from third countries of poultry and hatching eggs(5);
(2) Commission Decision 93/152/EEC of 8 February 1993 laying down the criteria for vaccines to be used against Newcastle disease in the context of routine vaccinations programmes(6) applies from 1 January 1995;
(3) It is appropriate, as a result, to amend Directive 91/494/EEC and in particular Article 3A thereof;
(4) It is appropriate to amend the trading rules as applied to third countries by introducing the possibility to draw up supplementary rules for imports of poultry meat, which offer animal health guarantees at least equivalent to those laid down in Chapter II of Directive 91/494/EEC;
(5) It is appropriate, moreover, to amend Directive 91/494/EEC in order to take account of the provisions of Council Decision 1999/468/EC of 28 June 1999 laying down the procedures for the exercise of implementing powers conferred on the Commission(7),
HAS ADOPTED THIS DIRECTIVE:
Article 1
Directive 91/494/EEC is hereby amended as follows:
1. Article 3A(1) shall be replaced by the following: "1. has been held in the Community territory since hatching or has been imported from third countries in accordance with the requirements of Chapter III of Directive 90/539/EEC."
2. Article 3A(6) shall be deleted;
3. the following Article shall be inserted: "Article 14a
Notwithstanding Articles 8, 10, 11, 12, 13 and 14, the Commission may, in accordance with the procedure laid down in Article 18, decide to permit on a case-by case basis the importation of fresh poultry meat from third countries where such imports do not conform with Articles 8, 10, 11, 12, 13 and 14. Detailed rules for such importation shall be drawn up at the same time by the same procedure. Such shall offer animal health guarantees at least equivalent to the animal health guarantees offered by Chapter II of this Directive."
4. Article 17 shall be replaced by the follwing: "Article 17
1. The Commission shall be assisted by the Standing Veterinary Committee established by Decision 68/361/EEC(8), composed of representatives of the Member States and chaired by the representative of the Commission.
2. The representative of the Commission shall submit to the Committee a draft of the measures to be taken. The Committee shall deliver its opinion on the draft within a time limit which the chairman may lay down according to the urgency of the matter. The opinion shall be delivered by the majority laid down in Article 205(2) of the Treaty in the case of decisions which the Council is required to adopt on a proposal from the Commission. The votes of the representatives of the Member States within the Committee shall be weighted in the manner set out in that Article. The chairman shall not vote.
3. The Commission shall adopt the measures envisaged if they are in accordance with the opinion of the Committee.
4. If the measures envisaged are not in accordance with the opinion of the Committee, or if no opinion is delivered, the Commission shall, without delay, submit to the Council a proposal relating to the measures to be taken.
5. The Council, acting by qualified majority shall act on the proposal within 15 days of the referral.
If, within that period, the Council indicates by qualified majority that it opposes the proposal, the Commission shall reconsider it. It may present an amended proposal to the Council, re-submit the same proposal or present a legislative proposal on the basis of the Treaty.
If the Council has not adopted the proposal implementing measures or has not indicated its opposition to the proposed implementing measures by the expiry of the time limit, they shall be adopted by the Commission.".
5. Article 18 shall be replaced by the following: "Article 18
1. The Commission shall be assisted by the Standing Veterinary Committee established by Decision 68/361/EEC, composed of representatives of the Member States and chaired by the representative of the Commission.
2. The representative of the Commission shall submit to the Committee a draft of the measures to be taken. The Committee shall deliver its opinion on the draft within a time limit which the chairman may lay down according to the urgency of the matter. The opinion shall be delivered by the majority laid down in Article 205(2) of the Treaty in the case of decisions for which the Council is required to adopt on a proposal from the Commission. The votes of the representatives of the Member States within the Committee shall be weighted in the manner set out in that Article. The chairman shall not vote.
3. The Commission shall adopt the measures envisaged if they are in accordance with the opinion of the Committee.
4. If the measures envisaged are not in accordance with the opinion of the Committee, or if no opinion is delivered, the Commission shall, without delay, submit to the Council a proposal relating to the meaures to be taken.
5. The Council, acting by qualified majority shall act on the proposal within three months of the referrral.
If, within that period, the Council indicates by qualified majority that it opposes the proposal, the Commission shall reconsider it. It may present an amended proposal to the Council, re-submit the same proposal or present a legislative proposal on the basis of the Treaty.
If the Council has not adopted the proposed implementing measures or has not indicated its opposition to the proposed implementing measures by the expiry of the time limit, they shall be adopted by the Commission."
6. the Annex shall be deleted.
Article 2
1. Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive before 1 July 2000. They shall forthwith inform the Commission thereof.
When Member States adopt these measures they shall contain a reference to this Directive or shall be accompanied by such reference on the occasion of their official publication. The methods of making such a reference shall be laid down by the Member States.
2. Member States shall communicate to the Commission the texts of the provisions of national law which they adopt in the field governed by this Directive.
Article 3
This Directive shall enter into force on the day of its publication in the Official Journal of the European Communities.
Article 4
This Directive is addressed to the Member States.
Done at Brussels, 15 November 1999.
For the Council
The President
K. HEMILÄ
(1) OJ C 15, 20.1.1996, p. 15.
(2) OJ C 261, 9.9.1996, p. 188.
(3) OJ C 153, 28.5.1996, p. 46.
(4) OJ L 268, 24.9.1991, p. 35. Directive as last amended by Council Directive 93/121/EEC (OJ L 340, 31.12.1993, p. 39).
(5) OJ L 303, 31.10.1990, p. 6. Directive as last amended by the 1994 Act of Accession.
(6) OJ L 59, 12.3.1993, p. 35.
(7) OJ L 184, 17.7.1999, p. 23.
(8) OJ L 255, 18.10.1968, p. 23;
