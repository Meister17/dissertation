Order of the Court of First Instance of 31 May 2006 — Carrs Paper Ltd
v Commission of the European Communities
(Case T-123/02) [1]
Parties
Applicant: Carrs Paper Ltd (Shirley, United Kingdom) (represented by: J. Grayston and A. Bywater, Solicitors)
Defendant: Commission of the European Communities (represented by: W. Mölls and A. Whelan, acting as Agents, and by M. van der Woude and V. Landes, lawyers)
Re:
Applicant for annulment, or in the alternative, for a reduction of the fine imposed on the applicant by Commission Decision 2004/337/EC of 20 December 2001 relating to a proceeding under Article 81 of the EC Treaty and Article 53 of the EEA Agreement (Case COMP/E-1/36.212 — carbonless paper) (OJ 2004 L 115, p.1)
Operative part of the order
1. There is no need to adjudicate on the present action.
2. The applicant is to pay the costs.
[1] OJ C 169, 13.7.2002.
--------------------------------------------------
