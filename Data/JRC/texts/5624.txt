Commission Regulation (EC) No 1413/2005
of 26 August 2005
on the issue of import licences for certain preserved mushrooms imported under the autonomous tariff quota opened by Regulation (EC) No 1319/2005
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 1319/2005 of 11 August 2005 opening and providing for the administration of an autonomous tariff quota for preserved mushrooms [1], and in particular Article 6(3) thereof,
Whereas:
HAS ADOPTED THIS REGULATION:
Article 1
1. Import licences applied for by traditional importers pursuant to Article 4(1) of Regulation (EC) No 1319/2005 and submitted to the Commission on 24 August 2005 shall be issued for 10,728 % of the quantity applied for.
2. Import licences applied for by new importers pursuant to Article 4(1) of Regulation (EC) No 1319/2005 and submitted to the Commission on 24 August 2005 shall be issued for 11,695 % of the quantity applied for.
Article 2
This Regulation shall enter into force on 27 August 2005.
It shall apply until 31 December 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 26 August 2005.
For the Commission
J. M. Silva Rodríguez
Director-General for Agriculture and Rural Development
[1] OJ L 210, 12.8.2005, p. 13.
--------------------------------------------------
