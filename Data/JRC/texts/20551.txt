COMMISSION REGULATION (EC) No 804/94 of 11 April 1994 laying down certain detailed rules for the application of Council Regulation (EEC) No 2158/92 as regards forest-fire information systems
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2158/92 of 23 July 1992 on protection of the Community's forests against fire (1), and in particular Article 5 (3) thereof,
Whereas, in accordance with Article 5 (2) of Regulation (EEC) No 2158/92, the aims of setting up a system of information on forest fires are the promotion of exchanges of information on forest fires, the ongoing evaluation of the impact of measures taken by Member States and the Commission to protect forests against fire, the evaluation of the periods, degree and causes of risk and the development of strategies for the protection of forests against fire, with particular emphasis on the elimination or reduction of causes;
Whereas information on the assessment of the effectiveness of measures, as provided for in Article 5 (2) of Regulation (EEC) No 2158/92 must serve in the drafting of the progress report provided for in Article 10 (3);
Whereas, to comply with the abovementioned standard objectives, Member States must at least collect a set of data consisting of information, comparable at Community level and accessible at specified regular intervals, hereinafter called 'the minimum core of information on forest fires';
Whereas such data must be progressively harmonized at Community level and the common core must evolve, on the basis, in particular, of the close cooperation in the field between the Member States and the Commission within the Standing Forestry Committee, in such a way as to avoid disruption of existing national systems for collecting data on forest fires; whereas, to that end, the chronological sequence in the collecting of certain data for the common core should be specified;
Whereas, to qualify for a Community contribution to the establishment of information systems, Member States must at least comply with the minimum core of information on forest fires;
Whereas the conditions under which applications for a contribution are admissible for examination in the light of the objectives laid down in Article 5 (2) of Regulation (EEC) No 2158/92 should be laid down;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Standing Forestry Committee,
HAS ADOPTED THIS REGULATION:
Article 1
1. Member States shall collect a set of information on forest fires enabling them to meet the objectives laid down in Article 5 (2) of Regulation (EEC) No 2158/92.
2. The set of information shall contain at least a number of standard items, comparable at Community level, hereinafter called the 'minimum common core of information on forest fires', as set out in Annex I.
3. The collection of such a set of information may be confined to high and medium-risk areas in the Member States.
4. Each year from the date of entry into force of this Regulation Member States shall make the common-core data available to the Commission.
5. On application by the Member States in justified cases, time limits for making the common core data available may be extended.
6. The detailed technical rules for the application of this Article shall be as set out in Annex I.
Article 2
1. Applications for financial aid for the implementation of the collection of the set of information referred to in Article 1 (1), improvement of such collection or extension thereof to areas not yet covered must contain the details and documents listed in Annex II.
2. Applications which fail to satisfy the conditions laid down in paragraph 1 shall not be considered.
Article 3
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 11 April 1994.
For the Commission
René STEICHEN
Member of the Commission
(1) OJ No L 217, 31. 7. 1992, p. 3.
ANNEX I
DETAILED TECHNICAL RULES FOR THE APPLICATION OF ARTICLE 1 The minimum common core of information on forest fires referred to in Article 1 (2) of this Regulation must include, for each officially recorded forest fire, the details listed in point 1, plus, from 1 January 1994, the details listed in point 2.
The national definitions of the terms 'forest', 'forest fire', 'wooded area' and 'unwooded area' referred to below apply.
1. Details to be collected from the date of entry into force of this Regulation
(a) Date of first alert
The local date (day, month, year) on which the official forest-fire protection services were informed of the outbreak of the fire should be indicated.
Example: 21 June 1990 21. 6. 1990.
(b) Time of first alert
The local time (hour, minute) at which the official forest-fire protection services were informed of the outbreak of the fire should be indicated.
Example: 13 hours 10 minutes 13.10.
(c) Date of first intervention
The local date (day, month, year) on which the first fire-fighting units arrived on the scene of the forest fire should be indicated.
Example: 21 June 1990 21. 6. 1990.
(d) Time of first intervention
The local time (hour, minute) at which the first fire-fighting units arrived on the scene of the forest fire should be indicated.
Example: 13 hours 30 minutes 13.30.
(e) Date on which the fire was extinguished
The local date (day, month, year) on which the fire was completely extinguished, i.e. when the last fire-fighting units left the scene of the forest fire, should be indicated.
Example: 21 June 1990 21. 6. 1990.
(f) Time at which the fire was extinguished
The local time (hour, minute) at which the last fire-fighting units left the scene of the forest fire should be indicated.
Example: 17 hours 50 minutes 17.50.
(g) Location of outbreak
The name of the commune and the successive territorial units to which it belongs (province or department, region, State) in which the outbreak of the fire was reported.
Example: commune Grasse,
department or province Alpes maritimes,
region Provence, Alpes, Côte d'Azure,
State: France.
(h) Total area burnt
The total area covered by the fire and the unit of area used should be indicated. The unit of area and the precision of the measurement should be those customarily used in the Member State.
Example: 121,28 hectares 121,28 ha.
(i) Breakdown of burnt area into wooded and unwooded land
The wooded area and the unwooded area covered by the fire and the unit of area used or the respective percentages of the total area covered by the fire on wooded and unwooded land should be indicated. The unit of area and the precision of the measurement should be those customarily used in the Member State.
Example: wooded area: 91,28 ha,
unwooded area: 30,00 ha,
or
wooded area: 75,26 %,
unwooded area: 24,74 %.
(j) Presumed cause of the forest fire
The presumed cause of the fire should be indicated according to the following four categories:
1. cause of fires unknown;
2. natural cause, e.g. lightening;
3. accidental cause or negligence, i.e. the origin is connected directly or indirectly with a human activity but the person concerned did not act with the intention of destroying an area of forest (e.g. accidents caused by power lines, railways, works, barbecues, a bonfire that got out of control, etc.);
4. fires started deliberately, i.e. by someone intending to destroy an area of forest for whatever motive.
Example: presumed cause 4.
2. Additional details to be collected from 1 January 1995 at the latest
(k) Commune code
The European code for the commune in which the fire broke out should be indicated. This code consists of nine digits, representing the code of the Member State, the region, the province and the commune. By means of the code the location of the fire for administrative purposes can be established at once. The Member States will be able to obtain the list of European commune codes on a data storage medium from the Commission.
Example: 01 Member States 05 region 02 province 789
commune.
ANNEX II
DATA AND DOCUMENTS TO BE SUPPLIED IN SUPPORT OF APPLICATIONS FOR COMMUNITY AID UNDER ARTICLE 2 (1) OF THIS REGULATION Aid applications submitted must contain details as outlined below:
1. Applicant
2. General details of the application
2.1. Title of the project
2.2. Description of the context and objectives of the application
2.3. Detailed description of the application (all relevant documents, maps, etc. aiding the understanding of the application should be attached)
2.4. Geographical scope of the application and risk rating of the regions covered by the project
2.5. Expected starting and completion dates
2.6. Contribution of the project to the objectives of Article 5 (2) of Regulation (EEC) No 2158/92
3. Funding requested
3.1. Total costs of the project (in national currency)
3.2. Costs in respect of which aid is requested (in national currency)
3.3. Aid requested (in national currency)
3.4. Agency to which payments are to be made and bank account
