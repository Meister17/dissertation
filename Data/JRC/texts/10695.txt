Summary information communicated by Member States on State aid granted under Commission Regulation (EC) No 1/2004 of 23 December 2003 on the application of Articles 87 and 88 of the EC Treaty to State aid to small and medium-sized enterprises active in the production, processing and marketing of agricultural products
(Text with EEA relevance)
(2006/C 300/04)
XA Number : XA 96/06
Member State : Belgium
Region : Flemish Region
Name of company receiving individual aid : pcfruit (Proefcentrum Fruitteelt vzw) [Test Centre for Fruit Growing]
Legal basis : Decreet van 19 mei 2006 betreffende de oprichting en de werking van het Fonds voor Landbouw en Visserij, meer bepaald art. 4. § 2
Annual expenditure planned under the scheme or overall amount of individual aid granted to the company : EUR 5000 for 1 year (renewable)
Maximum aid intensity : 100 % EUR 5000
Date of implementation : 1 November 2006
Duration of scheme or individual aid award : 1 year, until 31 October 2007
Objective of aid :
To support small and medium-sized enterprises and sectoral development
More specifically, "aid to encourage the production and marketing of quality agricultural products" on the basis of Article 13 of Commission Regulation (EC) No 1/2004 of 23 December 2003 on the application of Articles 87 and 88 of the EC Treaty to State aid to small and medium-sized enterprises active in the production, processing and marketing of agricultural products
Economic sector(s) concerned : Production and marketing of propagating material and plants of high quality in the tree nursery sector (fruit-bearing and ligneous ornamental plants)
Name and address of the granting authority :
Vlaamse overheid
Beleidsdomein Landbouw en Visserij
Fonds voor Landbouw en Visserij
WTC III — 12de verd.
Simon Bolivarlaan 30
B-1000 Brussel
Website :
http://www.ejustice.just.fgov.be/cgi/welcome.pl
http://www.ejustice.just.fgov.be/doc/rech_n.htm
XA Number : XA 26/06
Member State : Netherlands
Title of aid scheme or name of company receiving individual aid : Aid to the Hoofdbedrijfschap Agrarische Groothandel [Agricultural Wholesale Main Trade Association] for wholesale in the flower and plant sector in the Netherlands
Legal basis : Algemene heffingsverordening bloemen en planten 2004, alsmede de Verordening heffing bloemen en planten 2005 alsmede hun jaarlijkse rechtsopvolgers, welke heffingsverordeningen hun wettelijke basis vinden in artikel 126 van de Wet op de bedrijfsorganisatie
Annual expenditure planned under the scheme or overall amount of individual aid granted to the company : The Hoofdbedrijfschap Agrarische Groothandel has estimated a sum of EUR 1500000 for technical support to wholesalers in the flower and plant sector in the Netherlands
Maximum aid intensity : EUR 1500000 a year
Date of implementation : After national adoption of the levy Ordinance (see legal basis), which will take place after expiry of the 10 working days stipulated in Regulation (EC) No 1/2004
Duration of scheme or individual aid award : The technical support will be for an indefinite period, since there is a continuous need for up-to-date knowledge and market information.
Objective of aid : To make wholesalers in the flower and plant sector more competitive by providing generally applicable knowledge and information, the enterprises concerned being too small to obtain such knowledge and information themselves. This comprises technical support under Article 14 of Regulation (EC) No 1/2004
Economic sector(s) concerned : The scheme applies to flower and plant wholesalers and therefore to the trading and marketing of agricultural products, without distinction according to the products' origin
Name and address of the granting authority :
Hoofdbedrijfschap Agrarische Groothandel
Postbus 1012
1430 BA Aalsmeer
Nederland
Website : www.hbag.nl and www.HBAGbloemen.nl
Aid No : XA 69/06
Member State : Netherlands
Title of aid scheme or name of company receiving individual aid : Aid for TSE tests on slaughter goats
Legal basis :
Begroting van het Ministerie van Landbouw, Natuur en Voedselkwaliteit.
Annex III to Regulation (EC) No 999/2001, as amended by Regulation (EC) No 214/2005, requires Member States to test goats for TSEs. In accordance with the budget, costs connected with those tests are not passed on to goat owners
Annual expenditure planned under the scheme or overall amount of individual aid granted to the company : The total budgetary allocation comprises an annual maximum of EUR 315000, based on a maximum of 26250 goats a year
Maximum aid intensity : The total cost of a test is EUR 42. The Netherlands contributes EUR 12 per slaughter goat (over 18 months old) in addition to the EUR 30 part-financed by the European Commission.
Date of implementation :
The requirement to test goats was introduced by Regulation (EC) No 214/2005 amending Regulation (EC) No 999/2001. Test costs are not passed on to the original goat owner. The aid scheme has been applied in the Netherlands since the beginning of 2005 and at that time was already notified as a temporary measure. Initially the Commission stated that goat testing should be continued until mid-2006. It recently stated that goat testing would continue until mid-2007. The planned amendment of the rules on TSE tests is expected to have been completed by then.
By means of the present notification the Netherlands therefore wishes to extend the above-mentioned temporary measure's period of validity.
Duration of scheme or individual aid award : The aid will be granted in this form until the Commission has taken a final decision on amending the Community requirements on testing slaughter goats for TSEs. At an earlier stage the Commission stated that goat testing would be continued until mid-2006. Slaughter goat testing is currently expected to be continued until mid-2007.
Objective of aid : To obtain epidemiological data on TSEs in goats. The background to the aid is as follows. Regulation (EC) No 999/2001 requires slaughter goats to be tested for TSEs, the aim being to ascertain within a short period the prevalence of BSE in goats. However, the cost of a TSE test far exceeds the small income obtained from a goat. Passing on the cost of testing would deter goat owners from offering their animals for slaughter. This would mean that no tests could be carried out and that therefore no epidemiological data could be collected, which would be highly undesirable given the importance for public health. Refunding test costs in full means that goats are offered for slaughter, whereby TSE tests can be carried out and test results obtained.
Economic sector(s) concerned : Agricultural undertakings, particularly goat owners.
Name and address of the granting authority : Ministerie van Landbouw, Natuur en Voedselkwaliteit.
Website : www.minlnv.nl/loket
Other information :
As indicated, the measure is temporary. The aid was initially notified in the expectation that the measure would be in force for a year at the most. Goat testing is currently expected to last until mid-2007.
Aid for BSE and TSE testing is permitted under the Community guidelines for State aid concerning TSE tests, fallen stock and slaughterhouse waste (OJ C 324, 24.12.2002)
XA number : XA 95/06
Member State : Belgium
Region : Flemish Region
Name of company receiving individual aid : PBB (Pépinière Belge — Belgische Boomkwekerij vzw) [Belgian Tree Nursery]
Legal basis : Decreet van 19 mei 2006 betreffende de oprichting en de werking van het Fonds voor Landbouw en Visserij, meer bepaald art. 4. § 2
Annual expenditure planned under the scheme or overall amount of individual aid granted to the company : EUR 15000 for 1 year (renewable)
Maximum aid intensity : 100 % EUR 15000
Date of implementation : 1 November 2006
Duration of scheme or individual aid award : 1 year, until 31 October 2007
Objective of aid :
To support small and medium-sized enterprises and sectoral development.
More specifically, "aid to encourage the production and marketing of quality agricultural products" on the basis of Article 13 of Commission Regulation (EC) No 1/2004 of 23 December 2003 on the application of Articles 87 and 88 of the EC Treaty to State aid to small and medium-sized enterprises active in the production, processing and marketing of agricultural products.
Economic sector(s) concerned : Production and marketing of propagating material and plants of high quality in the tree nursery sector (fruit-bearing and ligneous ornamental plants)
Name and address of the granting authority :
Vlaamse overheid
Beleidsdomein Landbouw en Visserij
Fonds voor Landbouw en Visserij
WTC III — 12de verd.
Simon Bolivarlaan 30
B-1000 Brussel
Website :
http://www.ejustice.just.fgov.be/cgi/welcome.pl
http://www.ejustice.just.fgov.be/doc/rech_n.htm
--------------------------------------------------
