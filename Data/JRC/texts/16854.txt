Commission Regulation (EC) No 2125/2004
of 14 December 2004
amending Regulation (EEC) No 890/78 laying down detailed rules for the certification of hops
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1696/71 of 26 July 1971 on the common organisation of the market in hops [1], and in particular Article 2(5) thereof,
Whereas:
(1) Having regard to the accession to the European Union of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia (hereinafter referred to as "the new Member States"), Commission Regulation (EEC) No 890/78 [2] should be updated in certain respects.
(2) Regulation (EEC) No 890/78 specifies the texts to be entered on the certificates in the official languages of the Community. Such texts should be specified in the languages of the new Member States.
(3) Regulation (EEC) No 890/78 lays down the deadlines by which Member States must notify the Commission of hop production zones and regions, and certification centres. Deadlines should therefore be laid down for the new Member States.
(4) Regulation (EEC) No 890/78 should be amended accordingly.
(5) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Hops,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EEC) No 890/78 is hereby amended as follows:
1. Article 5a is replaced by the following:
"Article 5a
The certificate referred to in Article 5 of Regulation (EEC) No 1784/77 shall bear at least one of the texts indicated in Annex IIa, applied by the authority empowered to carry out certifications."
2. In Article 6(3) the following subparagraph is inserted after the second subparagraph:
"In the case of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia, this information shall be communicated by 1 January 2005."
3. In Article 11, the following paragraph is added:
"In the case of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia, this information shall be communicated by 1 January 2005."
4. Annex I to this Regulation is added as Annex IIa.
5. Annex III is amended in accordance with Annex II to this Regulation.
Article 2
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 14 December 2004.
For the Commission
Mariann Fischer Boel
Member of the Commission
--------------------------------------------------
[1] OJ L 175, 4.8.1971, p. 1. Regulation as last amended by Regulation (EC) No 2320/2003 (OJ L 345, 31.12.2003, p. 18).
[2] OJ L 117, 29.4.1978, p. 43. Regulation as last amended by Regulation (EC) No 1021/95 (OJ L 103, 6.5.1995, p. 20).
--------------------------------------------------
+++++ ANNEX 1 +++++</br>
+++++ ANNEX 2 +++++</br>
