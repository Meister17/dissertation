Euro exchange rates [1]
17 May 2006
(2006/C 117/01)
| Currency | Exchange rate |
USD | US dollar | 1,2882 |
JPY | Japanese yen | 140,88 |
DKK | Danish krone | 7,4554 |
GBP | Pound sterling | 0,67930 |
SEK | Swedish krona | 9,3777 |
CHF | Swiss franc | 1,5518 |
ISK | Iceland króna | 89,89 |
NOK | Norwegian krone | 7,8210 |
BGN | Bulgarian lev | 1,9558 |
CYP | Cyprus pound | 0,5751 |
CZK | Czech koruna | 28,194 |
EEK | Estonian kroon | 15,6466 |
HUF | Hungarian forint | 262,69 |
LTL | Lithuanian litas | 3,4528 |
LVL | Latvian lats | 0,6960 |
MTL | Maltese lira | 0,4293 |
PLN | Polish zloty | 3,8869 |
RON | Romanian leu | 3,5078 |
SIT | Slovenian tolar | 239,65 |
SKK | Slovak koruna | 37,513 |
TRY | Turkish lira | 1,8480 |
AUD | Australian dollar | 1,6710 |
CAD | Canadian dollar | 1,4220 |
HKD | Hong Kong dollar | 9,9883 |
NZD | New Zealand dollar | 2,0533 |
SGD | Singapore dollar | 2,0214 |
KRW | South Korean won | 1206,98 |
ZAR | South African rand | 8,0831 |
CNY | Chinese yuan renminbi | 10,3133 |
HRK | Croatian kuna | 7,2698 |
IDR | Indonesian rupiah | 11503,63 |
MYR | Malaysian ringgit | 4,607 |
PHP | Philippine peso | 67,070 |
RUB | Russian rouble | 34,6930 |
THB | Thai baht | 48,835 |
[1] Source: reference exchange rate published by the ECB.
--------------------------------------------------
