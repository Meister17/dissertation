Decision of the EEA Joint Committee
No 82/2004
of 8 June 2004
amending Annex XX (Environment) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex XX to the Agreement was amended by the Agreement on the participation of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Hungary, the Republic of Latvia, the Republic of Lithuania, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic in the European Economic Area signed on 14 October 2003 in Luxembourg [1].
(2) Directive 2002/96/EC of the European Parliament and of the Council of 27 January 2003 on waste electrical and electronic equipment (WEEE) [2], is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
The following point shall be inserted after point 32f (Directive 2000/76/EC of the European Parliament and of the Council) of Annex XX to the Agreement:
"32fa. 32002 L 0096: Directive 2002/96/EC of the European Parliament and of the Council of 27 January 2003 on waste electrical and electronic equipment (WEEE) (OJ L 37, 13.2.2003, p. 24).
The provisions of the Directive shall, for the purposes of the present Agreement, be read with the following adaptation:
In Article 17(4)(a), the word "Iceland" shall be inserted after the word "Greece"."
Article 2
The texts of Directive 2002/96/EC in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 9 June 2004, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [3].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 8 June 2004.
For the EEA Joint Committee
The President
S. Gillespie
--------------------------------------------------
[1] OJ L 130, 29.4.2004, p. 3.
[2] OJ L 37, 13.2.2003, p. 24.
[3] Constitutional requirements indicated.
