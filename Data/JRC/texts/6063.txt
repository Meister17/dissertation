Rules of Procedure of the Court of Auditors of the European Communities
CONTENTS
Page
TITLE I: THE ORGANISATION OF THE COURT
CHAPTER I THE COURT
Article 1 Collegial nature
SECTION 1 THE MEMBERS
Article 2 Commencement of term of office
Article 3 Members' obligations and performance of their duties
Article 4 Compulsory retirement and deprival of the right to a pension or other benefits in its stead
Article 5 Order of precedence
Article 6 Temporary replacement of Members
SECTION 2 THE PRESIDENT
Article 7 Election of the President
Article 8 Temporary replacement of the President
Article 9 Duties of the President
SECTION 3 AUDIT GROUPS AND COMMITTEES
Article 10 Audit groups
Article 11 Committees
SECTION 4 THE SECRETARY-GENERAL
Article 12 The Secretary-General of the Court
CHAPTER II THE PERFORMANCE OF THE COURT'S DUTIES
Article 13 Delegations
Article 14 Authorising officer powers
Article 15 Organisation of Court departments
TITLE II: THE COURT'S OPERATIONAL PROCEDURES
CHAPTER I COURT MEETINGS
Article 16 Scheduling of meetings
Article 17 Agenda
Article 18 Decision-taking procedure
Article 19 Chairing of meetings
Article 20 Quorum
Article 21 Non-public nature of meetings
Article 22 Minutes of meetings
CHAPTER II DECISIONS
Article 23 Adoption of decisions
Article 24 Languages and authentication
Article 25 Forwarding and publication
CHAPTER III AUDITS AND THE PREPARATION OF REPORTS, OPINIONS, OBSERVATIONS AND STATEMENTS OF ASSURANCE
Article 26 The conduct of audits
Article 27 Rapporteurs
TITLE III: GENERAL AND FINAL PROVISIONS
Article 28 Numbers expressed as fractions
Article 29 Implementing rules
Article 30 Access to documents
Article 31 Entry into force
Article 32 Publication
THE COURT OF AUDITORS OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community, and in particular the fifth subparagraph of Article 248(4) thereof,
Having regard to the Treaty establishing the European Atomic Energy Community, and in particular the fifth subparagraph of Article 160c(4) thereof,
with the approval of the Council given on 15 November 2004,
HAS ADOPTED THESE RULES:
TITLE I
THE ORGANISATION OF THE COURT
CHAPTER I
THE COURT
Article 1
Collegial nature
Pursuant to the provisions of the Treaties and of the Financial Regulation, the Court shall be organised and shall act as a collegial body, in accordance with these Rules of Procedure.
SECTION 1
THE MEMBERS
Article 2
Commencement of term of office
The term of office of Members of the Court shall commence as from the date fixed for that purpose in the instrument of appointment, or, where there is no date fixed, from the date of adoption of that instrument.
Article 3
Members' obligations and performance of their duties
The Members shall perform their duties in accordance with Article 247(4) and (5) of the EC Treaty and Article 160b(4) and (5) of the EAEC Treaty.
Article 4
Compulsory retirement and deprival of the right to a pension or other benefits in its stead
1. Where the Court, acting by a majority decision of its Members, considers that the information that has been submitted to it is such as to establish that a Member has ceased to meet the required conditions or to satisfy the obligations with which he is entrusted (Article 247(7) of the EC Treaty and Article 160b(7) of the EAEC Treaty), it shall instruct the President or, if the President is the Member concerned, the Member next in order of precedence to the President under Article 5 of these Rules of Procedure to draw up a preliminary report.
2. The preliminary report shall be sent, accompanied by supporting documents, to all the Members, including the Member concerned, who shall in response forward his written observations within a reasonable time limit set by the President or, if the President is the Member concerned, by the Member next in order of precedence to him.
3. The Member concerned shall also be invited to present his explanations orally to the Court.
4. The decision to refer the matter to the Court of Justice in order to deprive the Member concerned of his office and/or of his right to a pension or other benefits in its stead shall be taken by secret ballot by a majority of four fifths of the Members of the Court. The Member concerned shall not take part in the ballot.
Article 5
Order of precedence
1. Members shall take precedence after the President by date of appointment; where a Member has been reappointed, his earlier term of office shall be taken into consideration, whether or not it immediately preceded the present term.
2. Members appointed on the same day shall take precedence according to age.
Article 6
Temporary replacement of Members
1. If a Member's office falls vacant, the Court shall designate the Member(s) who shall carry out his duties pending the appointment of a new Member.
2. If a Member is absent or unavailable, he shall be temporarily replaced by one (or more) Member(s) in accordance with the provisions laid down in the implementing rules.
SECTION 2
THE PRESIDENT
Article 7
Election of the President
1. The Court shall elect a new President before the expiry of the term of office of the incumbent President. However, if the expiry of the President's term of office coincides with the renewal of a part of the body of Members in accordance with Article 247(3) of the EC Treaty and Article 160b(3) of the EAEC Treaty, the election shall take place immediately, or at the latest 15 working days, after the Court in its new composition has taken up its duties.
2. The President shall be elected by secret ballot. The candidate who, in the first round of voting, obtains a two-thirds majority of the votes of the Members of the Court shall be elected President. If none of the candidates obtains this majority, a second round of voting shall take place at once and the candidate who obtains the majority of the votes of the Members of the Court shall be elected. If no candidate has obtained the majority of the votes of the Members of the Court in the second round of voting, further rounds of voting shall take place in accordance with the procedure laid down in the implementing rules.
Article 8
Temporary replacement of the President
1. If the office of President falls vacant, the duties of the President shall temporarily be carried out by the outgoing President, providing that he is still a Member of the Court, save in the event of incapacity. In any other case, the Member who has precedence in accordance with Article 5 shall become acting President.
2. Whilst carrying out the day-to-day management of the Court during the interim period, the acting President shall organise the election of the new President in accordance with Article 7. However, if the office of President falls vacant less than six months before the expiry of the incumbent's term, the President shall be replaced by the Member who has precedence in accordance with Article 5.
3. If the President is absent or unavailable, he shall be temporarily replaced by the Member who has precedence in accordance with Article 5.
Article 9
Duties of the President
1. The President of the Court shall:
(a) call and chair meetings of the Court and be responsible for the proper conduct of the sessions;
(b) ensure that the Court's decisions are implemented;
(c) ensure that the departments of the Court operate properly and that its various activities are soundly managed;
(d) appoint an agent to represent the Court in all litigation in which the Court is involved;
(e) represent the Court in its external relations, in particular in its relations with the discharge authority, the other Community institutions and the Member States' audit institutions.
2. The President may delegate part of his duties to one or more Members.
SECTION 3
AUDIT GROUPS AND COMMITTEES
Article 10
Audit groups
1. Audit groups shall be created and composed in accordance with the provisions laid down in the implementing rules.
2. They shall have the responsibility of preparing the adoption by the Court of documents in the field of audit.
3. On a proposal by the President, each of the other Members shall be assigned by the Court to an audit group.
4. The Members shall be accountable to the group and to the Court for the performance of the tasks entrusted to them.
Article 11
Committees
1. Committees shall be created and composed in accordance with the provisions laid down in the implementing rules.
2. They shall have the responsibility of dealing with matters not covered by the audit groups as provided for in Article 10(2).
SECTION 4
THE SECRETARY-GENERAL
Article 12
The Secretary-General of the Court
1. The Secretary-General of the Court shall be appointed by the Court following an election by secret ballot, in accordance with the procedure laid down in the implementing rules.
2. The Secretary-General shall be accountable to the Court, to which he shall make periodic reports on the discharge of his duties.
3. Under the authority of the Court, the Secretary-General shall be responsible for the Court's Secretariat.
4. The Secretary-General shall exercise the powers vested in the Appointing Authority within the meaning of Article 2 of the Staff Regulations of officials of the European Communities and the powers vested in the authority authorised to conclude contracts of employment within the meaning of Article 6 of the Conditions of Employment of other servants of the European Communities insofar as the decision of the Court on the exercise of the powers vested in the Appointing Authority and in the authority authorised to conclude contracts of employment does not provide otherwise.
5. The Secretary-General shall be responsible for the management of the Court's staff and for the administration of the Court, as well as for any other task assigned to him by the Court.
6. If the Secretary-General is absent or unavailable, he is temporarily replaced in accordance with the provisions laid down in the implementing rules.
CHAPTER II
THE PERFORMANCE OF THE COURT'S DUTIES
Article 13
Delegations
1. The Court may, provided the principle of collective responsibility is respected, empower one or more Members to take clearly defined management or administrative measures on its behalf and under its supervision, and in particular to take action in preparation for a decision to be adopted at a later date by the Members of the Court. The Members concerned shall report back to the Court on the measures they have taken.
2. A Member may empower one or more officials or members of staff to sign documents concerning matters within that Member's responsibility in accordance with the provisions laid down in the implementing rules.
Article 14
Authorising officer powers
1. The powers of authorising officer shall be exercised by the Members of the Court and, as authorising officer by delegation, by the Secretary-General in accordance with the internal rules for the implementation of the budget.
2. The Court shall lay down the rules and practices concerning the control procedures for the exercise of the powers of authorising officers and authorising officers by delegation in a decision on the internal rules for implementing the budget.
Article 15
Organisation of Court departments
1. The Court shall determine the structure of its departments.
2. On the Secretary-General's proposal, the Court shall distribute the posts set out in the establishment plan among the administrative departments and the audit groups.
TITLE II
THE COURT'S OPERATIONAL PROCEDURES
CHAPTER I
COURT MEETINGS
Article 16
Scheduling of meetings
1. The Court shall set provisional dates for its meetings once a year, before the end of the preceding year.
2. Additional meetings may be organised on the initiative of the President or at the request of at least a quarter of the Members of the Court.
Article 17
Agenda
1. The draft agenda for each meeting shall be drawn up by the President.
2. The draft agenda and any requests for amendment thereto shall be placed before the Court, which shall adopt the agenda at the beginning of each meeting.
3. Items for which adoption by the Court is possible without discussion shall be included in Part A of the agenda; items for adoption after discussion shall be included in Part B of the agenda.
4. Detailed provisions to determine the conditions under which an item may be adopted without discussion or must be adopted after discussion are laid down in the implementing rules.
5. The time-limits for distributing the agenda and related documents are laid down in the implementing rules.
Article 18
Decision-taking procedure
The Court shall make its decisions in formal session, save where it applies the written procedure laid down in Article 23(5).
Article 19
Chairing of meetings
The meetings of the Court shall be chaired by the President. If the President is absent or indisposed, they shall be chaired by the Member who is temporarily acting as President, within the meaning of Article 8.
Article 20
Quorum
At least two thirds of the Members of the Court must be present in order to deliberate.
Article 21
Non-public nature of meetings
The Court's meetings shall not be public, unless the Court decides otherwise.
Article 22
Minutes of meetings
Minutes shall be drawn up for each meeting of the Court.
CHAPTER II
DECISIONS
Article 23
Adoption of decisions
1. The Court, acting as a collegial body, shall adopt its decisions after prior examination in an audit group or committee with the exception of decisions to be taken in its capacity as Appointing Authority or as the authority authorised to conclude contracts of employment.
2. The Court shall adopt the documents mentioned in the third subparagraph of Article 248(4) of the EC Treaty and the third subparagraph of Article 160c(4) of the EAEC Treaty by a majority of its Members.
3. Without prejudice to Article 4(4) and Article 7(2), other decisions shall be taken by a majority of the Members present at the meeting of the Court. However, the Court may, on a proposal from a Member, declare, by a majority of the Members present at the meeting, that a specific question referred to the Court shall be decided by a majority of the Members of the Court.
4. Where a majority vote by the Members present is required to take a decision, in the event that the number of votes for and against should be equal the chairman shall have the casting vote.
5. The Court shall determine, on a case-by-case basis, the decisions to be adopted by the written procedure. Detailed rules for this procedure are laid down in the implementing rules.
Article 24
Languages and authentication
1. The reports, opinions, observations, statements of assurance and other documents, if for publication, shall be drawn up in all the official languages.
2. The documents shall be authenticated by the apposition of the President's signature on all the language versions.
Article 25
Forwarding and publication
Within the framework of the Treaties and in particular of the provisions of Article 248(4) of the EC Treaty and Article 160c(4) of the EAEC Treaty and without prejudice to the applicable provisions of the Financial Regulation, the implementing rules shall lay down the rules on the forwarding and publication of the Court's reports, opinions, observations, statements of assurance and other decisions.
CHAPTER III
AUDITS AND THE PREPARATION OF REPORTS, OPINIONS, OBSERVATIONS AND STATEMENTS OF ASSURANCE
Article 26
The conduct of audits
1. The Court shall lay down detailed rules for the conduct of the audits with which it is charged by the Treaties.
2. It shall perform its audits in accordance with the aims laid down in its work programme.
Article 27
Rapporteurs
1. For every task to be carried out, the audit group shall appoint a Member/Members to act as rapporteur. For each of the tasks going beyond the specific framework of a group, the rapporteur(s) shall be appointed on a case-by-case basis by the Court.
2. As soon as it has been asked for an opinion within the meaning either of Articles 279 and 280 of the EC Treaty and 183 of the EAEC Treaty, or of Articles 248 of the EC Treaty and 160c of the EAEC Treaty, or when it wishes to present observations pursuant to Articles 248 of the EC Treaty and 160c of the EAEC Treaty, the Court shall appoint, from amongst its Members, the rapporteur(s) with responsibility for appraising the matter and preparing the draft.
TITLE III
GENERAL AND FINAL PROVISIONS
Article 28
Numbers expressed as fractions
For the purposes of the present Rules of Procedure, where a number is expressed as a fraction, it shall be rounded up to the next whole number.
Article 29
Implementing Rules
1. The Court, acting by a majority of its Members, determines the rules for implementing these Rules of Procedure.
2. The implementing rules shall be made public on the Court's website.
Article 30
Access to documents
In accordance with the principles of transparency and good administration and without prejudice to Articles 143(2) and 144(1) of the Financial Regulation, any citizen of the Union, and any natural or legal person residing or having its registered office in a Member State, shall have a right of access to Court documents under the conditions laid down in the Decision laying down internal rules for the treatment of applications for access to documents held by the Court.
Article 31
Entry into force
These Rules of Procedure shall cancel and replace those adopted by the Court on 31 January 2002.
They shall enter into force on 1 January 2005.
Article 32
Publication
These Rules of Procedure shall be published in the Official Journal of the European Union.
Done at Luxembourg, 8 December 2004.
For the Court of Auditors
President
Juan Manuel Fabra vallés
--------------------------------------------------
