Notice of initiation of a partial interim review of the countervailing measures applicable to imports of polyethylene terephthalate (PET) film originating in India
(2005/C 172/04)
The Commission has received a request for a partial interim review pursuant to Article 19 of Council Regulation (EC) No 2026/97 on protection against subsidised imports from countries not members of the European Community [1] ("the basic Regulation"), as last amended by Council Regulation (EC) No 461/2004 [2].
1. Request for review
The request was lodged by the following Community producers: DuPont Teijin Films, Mitsubishi Polyester Film GmbH, Nuroll SpA and Toray Plastics Europe ("the applicants").
The request is limited in scope to the examination of subsidisation as far as Garware Polyester Ltd is concerned.
2. Product
The product under review is polyethylene terephthalate (PET) film originating in India ("the product concerned"), normally declared within CN codes ex39206219 and ex39206290. These CN codes are given only for information.
3. Existing measures
The measure currently in force for Garware Polyester Ltd is a definitive countervailing duty imposed by Council Regulation (EC) No 2597/1999 [3] imposing a definitive countervailing duty on imports of polyethylene terephthalate (PET) film originating in India and collecting definitively the provisional duty imposed. These measures are currently subject to an expiry review investigation [4] in accordance with Article 18 of the basic Regulation.
4. Grounds for the review
The request pursuant to Article 19 of the basic Regulation is based on the prima facie evidence, provided by the applicants, that, as far as Garware Polyester Ltd is concerned, the circumstances with regard to subsidisation have changed significantly.
The applicants allege that the existing measure on imports of the product under review from Garware Polyester Ltd at its present level is no longer sufficient to counteract the subsidisation which is causing injury. The applicants have provided sufficient evidence in accordance with Article 19(2) of the basic Regulation that the subsidy amount has increased well above the 3,8 % of the definitive countervailing duty which is currently applicable for Garware Polyester Ltd.
It is alleged that Garware Polyester Ltd benefits from a number of subsidies granted by the Government of India and the Government of Maharashtra. These alleged subsidies consist of, among others, the duty entitlement passbook scheme, the export promotion capital goods scheme, the package scheme of incentives of the Government of Maharashtra, special import licences and export credits.
It is alleged that the above schemes are subsidies since they involve a financial contribution from the Government of India or other regional governments and confer a benefit to Garware Polyester Ltd. They are alleged to be contingent upon export performance and therefore specific and countervailable or to be otherwise specific and countervailable.
5. Procedure
Having determined, after consulting the Advisory Committee, that sufficient evidence exists to justify the initiation of a partial interim review, the Commission hereby initiates a review in accordance with Article 19 of the basic Regulation.
(a) Questionnaires
In order to obtain the information it deems necessary for its investigation, the Commission will send questionnaires to Garware Polyester Ltd and to the Indian authorities. This information and supporting evidence should reach the Commission within the time limit set in point 6(a).
(b) Collection of information and holding of hearings
All interested parties are hereby invited to make their views known, submit information other than questionnaire replies and to provide supporting evidence. This information and supporting evidence must reach the Commission within the time limit set in point 6(a).
Furthermore, the Commission may hear interested parties, provided that they make a request showing that there are particular reasons why they should be heard. This request must be made within the time limit set in point 6(b).
6. Time limits
(a) For parties to make themselves known, to submit questionnaire replies and any other information
All interested parties, if their representations are to be taken into account during the investigation, must make themselves known by contacting the Commission, present their views and submit questionnaire replies or any other information within 40 days of the date of publication of this notice in the Official Journal of the European Union, unless otherwise specified. Attention is drawn to the fact that the exercise of most procedural rights set out in the basic Regulation depends on the party's making itself known within the aforementioned period.
(b) Hearings
All interested parties may also apply to be heard by the Commission within the same 40-day time limit.
7. Written submissions, questionnaire replies and correspondence
All submissions and requests made by interested parties must be made in writing (not in electronic format, unless otherwise specified) and must indicate the name, address, e-mail address, telephone and fax, and/or telex numbers of the interested party. All written submissions, including the information requested in this notice, questionnaire replies and correspondence provided by interested parties on a confidential basis shall be labelled as "Limited" [5] and, in accordance with Article 29(2) of the basic Regulation, shall be accompanied by a non-confidential version, which will be labelled "For inspection by interested parties".
Commission address for correspondence:
European Commission
Directorate General for Trade
Directorate B
Office: J-79 5/16
B-1049 Brussels
Fax (32-2) 295 65 05
8. Non-cooperation
In cases in which any interested party refuses access to or does not provide the necessary information within the time limits, or significantly impedes the investigation, provisional or final findings, affirmative or negative, may be made in accordance with Article 28 of the basic Regulation, on the basis of the facts available.
Where it is found that any interested party has supplied false or misleading information, the information shall be disregarded and use may be made of the facts available. If an interested party does not cooperate or cooperates only partially and findings are therefore based on facts available in accordance with Article 28 of the basic Regulation, the result may be less favourable to that party than if it had cooperated.
[1] OJ L 288, 21.10.97, p. 1.
[2] OJ L 77, 13.3.2004, p. 12.
[3] OJ L 316, 10.12.1999, p. 1.
[4] OJ C 306, 10.12.2004, p. 2.
[5] This means that the document is for internal use only. It is protected pursuant to Article 4 of Regulation (EC) No 1049/2001 of the European Parliament and of the Council regarding public access to European Parliament, Council and Commission documents (OJ L 145, 31.5.2001, p. 43). It is a confidential document pursuant to Article 29 of the basic Regulation and Article 12 of the WTO Agreement on Subsidies and Countervailing Measures.
--------------------------------------------------
