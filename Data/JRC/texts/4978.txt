Commission Regulation (EC) No 870/2005
of 8 June 2005
on granting of import licences for cane sugar for the purposes of certain tariff quotas and preferential agreements
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1260/2001 of 19 June 2001 on the common organisation of the markets in the sugar sector [1],
Having regard to Council Regulation (EC) No 1095/96 of 18 June 1996 on the implementation of the concessions set out in Schedule CXL drawn up in the wake of the conclusion of the GATT XXIV.6 negotiations [2],
Having regard to Commission Regulation (EC) No 1159/2003 of 30 June 2003 laying down detailed rules of application for the 2003/04, 2004/05 and 2005/06 marketing years for the import of cane sugar under certain tariff quotas and preferential agreements and amending Regulations (EC) No 1464/95 and (EC) No 779/96 [3], and in particular Article 5(3) thereof,
Whereas:
(1) Article 9 of Regulation (EC) No 1159/2003 stipulates how the delivery obligations at zero duty of products of CN code 1701, expressed in white sugar equivalent, are to be determined for imports originating in signatory countries to the ACP Protocol and the Agreement with India.
(2) Article 16 of Regulation (EC) No 1159/2003 stipulates how the zero duty tariff quotas for products of CN code 17011110, expressed in white sugar equivalent, are to be determined for imports originating in signatory countries to the ACP Protocol and the Agreement with India.
(3) Article 22 of Regulation (EC) No 1159/2003 opens tariff quotas at a duty of EUR 98 per tonne for products of CN code 17011110 for imports originating in Brazil, Cuba and other third countries.
(4) In the week of 30 May to 3 June 2005 applications were presented to the competent authorities in line with Article 5(1) of Regulation (EC) No 1159/2003 for import licences for a total quantity exceeding a country's delivery obligation quantity of ACP-India preferential sugar determined pursuant to Article 9 of that Regulation.
(5) In these circumstances the Commission must set reduction coefficients to be used so that licences are issued for quantities scaled down in proportion to the total available and must indicate that the limit in question has been reached,
HAS ADOPTED THIS REGULATION:
Article 1
In the case of import licence applications presented from 30 May to 3 June 2005 in line with Article 5(1) of Regulation (EC) No 1159/2003 licences shall be issued for the quantities indicated in the Annex to this Regulation.
Article 2
This Regulation shall enter into force on 9 June 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 8 June 2005.
For the Commission
J. M. Silva Rodríguez
Director-General for Agriculture and Rural Development
[1] OJ L 178, 30.6.2001, p. 1. Regulation as last amended by Commission Regulation (EC) No 39/2004 (OJ L 6, 10.1.2004, p. 2).
[2] OJ L 146, 20.6.1996, p. 1.
[3] OJ L 162, 1.7.2003, p. 25. Regulation as last amended by Regulation (EC) No 568/2005 (OJ L 97, 15.4.2005, p. 9).
--------------------------------------------------
ANNEX
ACP-INDIA preferential sugar
Title II of Regulation (EC) No 1159/2003
Country | Week of 30.5.2005- 3.6.2005: percentage of requested quantity to be granted | Limit |
Barbados | 100 | |
Belize | 0 | reached |
Congo | 100 | |
Fiji | 0 | reached |
Guyana | 0 | reached |
India | 3,5018 | |
Côte d'Ivoire | 100 | |
Jamaica | 100 | |
Kenya | 100 | |
Madagascar | 100 | |
Malawi | 0 | reached |
Mauritius | 0 | reached |
Mozambique | 0 | reached |
Saint Kitts and Nevis | 100 | |
Swaziland | 0 | reached |
Tanzania | 100 | |
Trinidad and Tobago | 100 | |
Zambia | 100 | |
Zimbabwe | 0 | reached |
Country | Week of 30.5.2005- 3.6.2005: percentage of requested quantity to be granted | Limit |
Barbados | — | |
Belize | 100 | |
Congo | — | |
Fiji | 100 | |
Guyana | 100 | |
India | — | |
Côte d'Ivoire | — | |
Jamaica | — | |
Kenya | — | |
Madagascar | — | |
Malawi | 100 | |
Mauritius | 100 | |
Mozambique | 100 | |
Saint Kitts and Nevis | — | |
Swaziland | 100 | |
Tanzania | — | |
Trinidad and Tobago | — | |
Zambia | — | |
Zimbabwe | 100 | |
Country | Week of 30.5.2005- 3.6.2005: percentage of requested quantity to be granted | Limit |
India | 0 | reached |
ACP | 100 | |
Country | Week of 30.5.2005- 3.6.2005: percentage of requested quantity to be granted | Limit |
Brazil | 0 | reached |
Cuba | 0 | reached |
Other third countries | 0 | reached |
--------------------------------------------------
