Decision of the EEA Joint Committee
No 21/2006
of 10 March 2006
amending Annex II (Technical regulations, standards, testing and certification) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex II to the Agreement was amended by Decision of the EEA Joint Committee No 115/2005 of 30 September 2005 [1].
(2) Commission Directive 2005/31/EC of 29 April 2005 amending Council Directive 84/500/EEC as regards a declaration of compliance and performance criteria of the analytical method for ceramic articles intended to come into contact with foodstuffs [2] is to be incorporated into the Agreement.
(3) Commission Directive 2005/46/EC of 8 July 2005 amending the Annexes to Council Directives 86/362/EEC, 86/363/EEC and 90/642/EEC as regards maximum residue levels for amitraz [3] is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
Chapter XII of Annex II to the Agreement shall be amended as follows:
1. The following shall be added in point 34 (Council Directive 84/500/EEC):
", as amended by:
- 32005 L 0031: Commission Directive 2005/31/EC of 29 April 2005 (OJ L 110, 30.4.2005, p. 36)."
2. The following indent shall be added in points 38 (Council Directive 86/362/EEC), 39 (Council Directive 86/363/EEC) and 54 (Council Directive 90/642/EEC):
- "— 32005 L 0046: Commission Directive 2005/46/EC of 8 July 2005 (OJ L 177, 9.7.2005, p. 35)."
Article 2
The texts of Directives 2005/31/EC and 2005/46/EC in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 11 March 2006, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [4].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 10 March 2006.
For the EEA Joint Committee
The President
R. Wright
[1] OJ L 339, 22.12.2005, p. 16.
[2] OJ L 110, 30.4.2005, p. 36.
[3] OJ L 177, 9.7.2005, p. 35.
[4] No constitutional requirements indicated.
--------------------------------------------------
