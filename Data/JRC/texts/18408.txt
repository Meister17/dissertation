*****
COMMISSION DIRECTIVE
of 8 March 1983
amending Directive 82/287/EEC amending the Annexes to Council Directives 66/401/EEC and 69/208/EEC on the marketing of fodder plant seed and seed of oil and fibre plants respectively, and Directives 78/386/EEC and 78/388/EEC
(83/116/EEC)
THE COMMISSION OF THE EUROPEAN
COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Directive 66/401/EEC of 14 June 1966 on the marketing of fodder plant seed (1), as last amended by Directive 82/287/EEC (2), and in particular Article 21a thereof,
Whereas, in the light of the development of scientific and technical knowledge, amendments were made to Annexes I and II to Directives 66/401/EEC and 69/208/EEC by Directive 82/287/EEC, including amendments relating to the varietal purity of seeds of Poa spp;
Whereas 1 January 1983 was fixed as the date for implementing the provisions necessary to comply with those amendments;
Whereas the rules adopted to that end in the schemes for the varietal certification of seed intended for international trade laid down by the Organization for Economic Cooperation and Development (OECD) are at present undergoing thorough scrutiny by that organization;
Whereas the outcome of that scrutiny should be awaited;
Whereas a more suitable date should therefore be fixed for implementing the provisions in question, in order to avoid untimely measures;
Whereas the measures provided for in this Directive are in accordance with the opinion of the Standing Committee on Seeds and Propagating Material for Agriculture, Horticulture and Forestry,
HAS ADOPTED THIS DIRECTIVE;
Article 1
1. In the second indent of Article 7 (1) of Directive 82/287/EEC the words '1 January 1983' are replaced by '1 January 1984 at the latest'.
2. In Article 7 (2) of Directive 82/287/EEC, the words 'the third indent' are replaced by 'the second and third indents'.
Article 2
This Directive is addressed to the Member States.
Done at Brussels, 8 March 1983.
For the Commission
Poul DALSAGER
Member of the Commission
(1) OJ No 125, 11. 7. 1966, p. 2298/66.
(2) OJ No L 131, 13. 5. 1982, p. 24.
