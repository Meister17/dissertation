SIXTEENTH COMMISSION DIRECTIVE 93/47/EEC of 22 June 1993 adapting to technical progress Annexes II, III, V, VI and VII of Council Directive 76/768/EEC on the approximation of the laws of the Member States relating to cosmetic products
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Directive 76/768/EEC of 27 July 1976 on the approximation of the laws of the Member States relating to cosmetic products (1), as last amended by Directive 92/86/EEC (2), and in particular Article 8 (2) thereof,
Whereas, on the basis of the available information, on the one hand an ultraviolet filter may be definitively permitted and on the other hand certain provisionally permitted substances, preservatives and ultraviolet filters must be definitively prohibited or permitted for a further specified period;
Whereas, in order to protect public health, it is necessary to prohibit the use of 4-amino-2-nitrophenol;
Whereas, on the basis of the latest scientific and technical research, the use of strontium peroxide and phenolphthalein may be permitted subject to certain restrictions and the obligatory inclusion of health warnings on the label;
Whereas, on the basis of the latest scientific and technical research, 3-midazol-4-ylacrylic acid and its ethyl ester, may be used as an ultraviolet filter up to 30 July 1994 in cosmetic products subject to certain restrictions and conditions;
Whereas the measures provided for in this Directive are in accordance with the opinion of the Committee on the Adaptation to Technical Progress of the Directives on the removal of technical barriers to trade in the cosmetic products sector,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Directive 76/768/EEC is hereby amended as follows:
1. in Annex II, the following number is added:
'412. 4-Amino-2-nitrophenol';
2. in Annex III, Part 1:
(a) to reference numbers 8, 9 and 10 in paragraph (b) of column (f) the sentence: 'Wear suitable gloves' should be added;
(b) to reference number 12 in column (f) the sentence: '(a) Wear suitable gloves' should be added;
3. in Annex III, Part 2, the following reference numbers are added:
/* Tables: see OJ */
conditions laid down in Annex III, Part 2, reference number 1, . . .';
5. in Annex VI, Part 2, '30 June 1993' in reference numbers 2, 15, 16, 21, 26, 27, 28, 29, 30 is replaced by '30 June 1994';
6. in Annex VII, Part 1:
(a) the following reference number is added:
7. in Annex VII, Part 2:
(a) the following number is added:
(c) '30 June 1993' in reference numbers 2, 5, 6, 12, 13, 17, 24, 25, 26, 28, 29, 32 is replaced by '30 June 1994'.
Article 2
1. Regardless of the dates mentioned in Article 1, Member States shall take all the necessary measures to ensure that as from 1 July 1994 for the substances mentioned in Article 1, neither manufacturers nor importers established in the Community shall place on the market products which do not comply with the requirements of this Directive.
2. Member States shall take the necessary measures to ensure that the products referred to in paragraph 1 containing the substances mentioned in Article 1 shall not be sold or otherwise supplied to the final consumer after 30 June 1995 if they do not comply with the requirements of this Directive.
Article 3
1. Member States shall bring into force the laws, regulations and administrative provisions needed to comply with this Directive no later than 30 June 1994. They shall forthwith inform the Commission thereof.
2. When Member States adopt these provisions, these shall contain a reference to this Directive or shall be accompanied by such reference at the time of their official publication. The procedure for such reference shall be adopted by Member States.
3. Member States shall communicate to the Commission the provisions of national law which they adopt in the field governed by this Directive.
Article 4
This Directive is addressed to the Member States.
Done at Brussels, 22 June 1993.
For the Commission
Christiane SCRIVENER
Member of the Commission
(1) OJ No L 262, 27. 9. 1976, p. 169.
(2) OJ No L 325, 11. 11. 1992, p. 18.
