Judgment of the Court of First Instance of 14 July 2006 — Endesa v Commission
(Case T-417/05) [1]
Parties
Applicant: Endesa SA (Madrid, Spain) (represented by: J. Flynn QC, S. Baxter, Solicitor, and M. Odriozola Alén, M. Muñoz de Juan, M. Merola, J. García de Enterría Lorenzo-Velázquez, J. Varcárcel Martínez, lawyers)
Defendant: Commission of the European Communities (represented by: F. Castillo de la Torre, É. Gippini Fournier, A. Whelan and M. Schneider, Agents)
Interveners in support of the defendant: Kingdom of Spain (represented by: N. Díaz Abad, abogado del Estado) and Gas Natural SDG SA (Barcelona, Spain) (represented by: F. González Díaz, J. Jiménez de la Iglesia and A. Leis García, lawyers)
Re:
Application for annulment of the Commission Decision of 15 November 2005 declaring that a concentration has no Community dimension (Case COMP/M.3986 — Gas Natural/Endesa)
Operative part of the judgment
The Court:
1. Dismisses the action;
2. Orders the applicant to bear its own costs and the costs of the Commission and Gas Natural SDG SA, including those relating to the interlocutory proceedings;
3. Orders the Kingdom of Spain to bear its own costs.
[1] OJ C 22 of 28.1.2006.
--------------------------------------------------
