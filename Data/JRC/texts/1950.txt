Council Decision 2005/806/CFSP
of 21 November 2005
implementing Joint Action 2005/557/CFSP on the European Union civilian-military supporting action to the African Union mission in the Darfur region of Sudan
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to Council Joint Action 2005/557/CFSP of 18 July 2005 on the European Union civilian-military supporting action to the African Union mission in the Darfur region of Sudan [1], and in particular Article 8(1), second subparagraph thereof, in conjunction with Article 23(2) of the Treaty on European Union,
Whereas:
(1) The Council has in accordance with Article 15 of Joint Action 2005/557/CFSP decided to continue the European Union civilian-military supporting action to the African Union mission in the Darfur region of Sudan.
(2) As concerns the civilian component the Council should consequently decide on the reference amount for the continuation of the supporting action.
(3) The EU supporting action to the AMIS II will be conducted in the context of a situation which may deteriorate and could harm the objectives of the CFSP as set out in Article 11 of the Treaty,
HAS DECIDED AS FOLLOWS:
Article 1
1. The financial reference amount intended to cover the expenditure related to the implementation of Section II of Joint Action 2005/557/CFSP from 29 January until 28 July 2006 shall be EUR 2200000.
2. The expenditure financed by the amount stipulated in paragraph 1 shall be managed in accordance with the European Community procedures and rules applicable to the budget, with the exception that any pre-financing shall not remain the property of the Community. Nationals of third states shall be allowed to tender for contracts.
Article 2
The Council shall no later than 30 June 2006 evaluate whether the EU supporting action should be continued.
Article 3
This Decision shall enter into force on the date of its adoption.
The expenditure shall be eligible from 29 January 2006.
Article 4
This Decision shall be published in the Official Journal of the European Union.
Done at Brussels, 21 November 2005.
For the Council
The President
J. Straw
[1] OJ L 188, 20.7.2005, p. 46.
--------------------------------------------------
