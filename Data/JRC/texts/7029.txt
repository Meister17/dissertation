[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 29.07.2005
COM(2005) 348 final
REPORT FROM THE COMMISSION
on the application in 2004 of Regulation (EC) No 1049/2001 of the European Parliament and of the Council regarding public access to European Parliament, Council and Commission documents {SEC(2005) 1025}
FOREWORD
Article 17(1) of Regulation (EC) No 1049/2001 regarding public access to European Parliament, Council and Commission documents[1], applicable since 3 December 2001, stipulates that each institution must publish a report for the preceding year including the number of cases in which the institution refused to grant access to documents, the reasons for such refusals and the number of sensitive documents not recorded in the register.
This report, which covers 2004, is the third report presented by the Commission under that Article.
The Annex to the report contains statistics relating to the processing of requests for access. A series of tables gives the figures for the three years the Regulation has been in force, thereby making it possible to monitor implementation. It should be emphasised that the statistics relate solely to requests for access to unpublished documents and do not include orders for documents already published or requests for information.
The Annex also contains a detailed analysis of requests for access and application of the exceptions rule as well as an overview of the complaints made to the Ombudsman and legal action.
1. SPECIFIC MEASURES RELATING TO APPLICATION OF THE REGULATION
1.1. On 30 January 2004 the Commission adopted the report on the implementation of the principles of the Regulation provided for by Article 17(2).
1.2. On 1 October 2003 the scope of the Regulation was extended to Community agencies in existence on that date, which adopted the rules for implementing it in 2004. The plan is to apply the Regulation systematically to newly established agencies under a provision of the basic act.
1.3. The proposal for a Parliament and Council Regulation on the application of the provisions of the Århus Convention on Access to Information, Public Participation in Decision-making and Access to Justice in Environmental Matters to EC institutions and bodies[2] has been voted on by the European Parliament (first reading) and agreed by the Council.
2. INFORMATION FOR THE PUBLIC
2.1. Following enlargement of the European Union on 1 May 2004, the Commission register of internal documents was adapted to cover the twenty official languages. Final COM documents as well as agendas and minutes of Commission meetings are directly accessible in full. Technical work has continued with a view to adapting the register to make other document series accessible.
2.2. Since December 2003 a specific register covering the work of the committees which assist the Commission in the exercise of its implementing powers complements the internal document register. It covers documents on committee proceedings transmitted to the European Parliament under the "comitology" Decision[3].
2.3. In 2004 the Openness and access to documents site on the EUROPA server registered a total of 1 384 108 consultations, calculated on the number of screens displayed, i.e. an average per month of 115 342 consultations, or 5 250 per working day - a threefold increase over 2003.
2.4. The brochure covering the three institutions published in the eleven Community languages at the time[4] has now been translated into the nine new Community languages. These brochures will be available in representations, delegations, relays and networks.
2.5. The three institutions continued with the feasibility study relating to the creation of an information tool in the area of justice, freedom and security.
3. COOPERATION WITH THE OTHER INSTITUTIONS AND THE MEMBER STATES
3.1. The interinstitutional committee provided for in Article 15(2) of the Regulation did not meet at political level in 2004. However, the services responsible for implementing Regulation 1049/2001 in the Secretariats-General of the European Parliament, the Council and the Commission continued to exchange information and experiences in order to identify best practices and to ensure that the Regulation was being applied consistently.
3.2. On 25 and 26 November 2004 the Dutch Presidency organised a second conference on transparency in Europe in The Hague for the purpose of reviewing for the first time how Regulation 1049/2001 was being applied at the time of accession of the ten new Member States.
4. ANALYSIS OF REQUESTS FOR ACCESS
4.1. The continuing increase in the number of requests for access since the Regulation was adopted was repeated last year:
- between 2002 and 2003, the number of initial requests rose from 991 to 1 523, i.e. an increase of 53.7%;
- in 2004, 2600 initial requests were recorded, i.e. 1077 more than in 2003, or an increase of 70.72%.
4.2. The increase in confirmatory requests was less marked:
- between 2002 and 2003, the number rose from 96 to 143, i.e. an increase of 48.96%;
- in 2004, the number of confirmatory applications rose to 162, i.e. 19 more than in 2003, or an increase of 13.29%.
4.3. The breakdown by area of interest remained more or less the same as in previous years. Competition, customs, indirect taxation, the internal market and the environment continue to rouse most interest, accounting for approximately 40% of requests.
4.4. The breakdown by socio-professional category did not change significantly. Various interest groups, NGOs and companies accounted for over a quarter of the requests.
4.5. More than a quarter of the requests (26.42%) came from persons or bodies established in Belgium, because of the number of multinational companies, law firms and associations or NGOs operating at European level with an office in Brussels. Moreover, the majority of requests (over half) came from Germany, Italy, France, the United Kingdom, Spain and the Netherlands. The percentage from the new Member States remains low (4.31%).
5. EXCEPTIONS TO THE RIGHT OF ACCESS
5.1. The percentage of positive replies in the initial request stage is roughly the same as in 2003, with a slight drop from 69.31% to 68.19%. In 64.8% of cases the whole document was disclosed, and in 3.39% of cases requesters were granted partial access.
The percentage of positive replies does not include the large number of requests for access to documents which had already been disclosed (close to one in five). Moreover, it should be noted that, in approximately 2% of cases, the document in question did not exist.
5.2. The percentage of confirmations of initial decisions increased significantly from 61.57% to 73.21% of cases.
The percentage of totally positive replies after an initial refusal fell from 30.13% to 9.09%. The percentage of decisions granting partial access after an initial refusal more than doubled, from 8.29% to 17.7%.
Following completion of the process for handling requests, the rate of positive replies stands at 70% (compared with 72.82% in 2003); in 65.45% of cases the whole document was disclosed, and in 4.58% of cases partial access was granted.
5.3. At the initial stage, the main reason for refusal was to protect the purpose of inspections, investigations and audits (third indent of Article 4(2)), though the percentage of refusals on those grounds fell slightly from 37.55% to 31.81%. These mostly involved requests for access to letters of formal notice, reasoned opinions or other documents relating to infringement procedures which had not yet been closed or documents relating to investigations concerning competition policy.
The second reason for refusal was the protection of the Commission’s decision-making process (Article 4(3)). The percentage of refusals based on this exception went up from 20.92% to 25.44%.
5.4. The main reasons for confirming refusal of access were the same as for the initial stage:
- protection of the purpose of investigations (26.32 %);
- protection of the decision-making process (21.75 %).
It should be pointed out that the protection of commercial interests was the basis of a significantly bigger number of refusals at the confirmatory stage than in 2003, up from 11.87% to 15.79%. This exception was clearly invoked more at the confirmatory stage than at the initial stage (8.33%).
6. COMPLAINTS SUBMITTED TO THE EUROPEAN OMBUDSMAN
6.1. In 2004 the Ombudsman closed thirteen complaints against the Commission relating to refusals to disclose documents. In five cases the Ombudsman concluded that there was no evidence of maladministration. In five other cases, an amicable solution had been found or the dispute settled in mid-procedure. The Ombudsman made critical remarks in the case of three complaints.
6.2. Eleven new complaints were made to the Ombudsman in 2004. Four of them related to refusals to disclose documents established or received under an infringement procedure (formal notice, reasoned opinions, replies by the Member States).
7. LEGAL ACTION
7.1. The Court of First Instance handed down two judgments rejecting an appeal against a decision refusing to disclose a document. In a third case annulment by the Court of Justice of a Court of First Instance judgment resulted in the annulment of a negative decision by the Council and the Commission:
- T-84/03, 23.11.2004, Turco
v Council
- T-168/02, 30.11.2004, Internationaler Tierschutzfonds v Commission
- C-353/01, 22.01.2004, P Mattila v Council and Commission
7.2. Nine new actions were lodged in 2004 against Commission decisions under Regulation 1049/2001:
- T-36/04 International Press Association v Commission
- T-194/04 Bavarian Lager v Commission
- T-446/04 Co-Frutta v Commission
- T-70/04 Franchet-Byk v Commission
- T-319/04 Port Support Customs Agency v Commission
- T-380/04 Terezakis v Commission
- T-237/04 Ultradent v Commission
- T-284/04 UPS v Commission
- T-161/04 Valero Jordana v Commission
8. CONCLUSIONS
The growing public interest in accessing unpublished Commission documents was confirmed in 2004. As in previous years, applications were made largely by companies, NGOs, law firms and various interest groups (over 40%).
The general picture which emerges from the analysis of requests is that a significant proportion of them relate to Commission activities regarding the monitoring of Community law. In many cases, the purpose is to obtain documents which could support the requester's position in connection with a complaint, relating for example to a presumed infringement of Community law, or an administrative or legal appeal. These requests generally involve a substantial volume of documents and examining them entails considerable administrative work.
The reasons most frequently given for refusal are the protection of the following interests: purpose of investigations, commercial interests of companies and the Commission's decision-making process. This last exception is invoked more to protect the taking of individual decisions than the legislative process, where the trend is to make more and more documents public without waiting for requests for access.
The number of complaints presented to the Ombudsman under Regulation 1049/2001 remains stable despite the increase in the number of requests for access. In most cases, either the Ombudsman concluded that there had been no maladministration or the case was settled to the satisfaction of the complainant.
The Court of First Instance confirmed its interpretation of the exception relating to the right of the Member States to oppose disclosure of documents transmitted by them to the Commission.
[1] OJ L 145, 31.5.2001, p. 43.
[2] COM (2003) 622 final.
[3] Council Decision 1999/468/EC of 28 June 1999 laying down the procedures for the exercise of implementing powers conferred on the Commission (OJ L 184, 17.7.1999, p. 23).
[4] Catalogue number KA-41-01-187-FR-C ISBN 92-894-1904-0
