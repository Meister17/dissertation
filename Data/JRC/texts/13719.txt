Decision of the EEA Joint Committee
No 90/2006
of 7 July 2006
amending Annex XIII (Transport) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex XIII to the Agreement was amended by Decision of the EEA Joint Committee No 67/2006 of 2 June 2006 [1].
(2) Regulation (EC) No 2320/2002 of the European Parliament and of the Council of 16 December 2002 establishing common rules in the field of civil aviation security [2] was incorporated into the Agreement by Decision of the EEA Joint Committee No 61/2004 of 26 April 2004 [3], with country specific adaptations.
(3) Commission Regulation (EC) No 240/2006 of 10 February 2006 amending Regulation (EC) No 622/2003 laying down measures for the implementation of the common basic standards on aviation security [4] is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
The following indent shall be added in point 66i (Commission Regulation (EC) No 622/2003) of Annex XIII to the Agreement:
- "32006 R 0240: Commission Regulation (EC) No 240/2006 of 10 February 2006 (OJ L 40, 11.2.2006, p. 3)."
Article 2
The text of Regulation (EC) No 240/2006 in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 8 July 2006, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [5].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 7 July 2006.
For the EEA Joint Committee
The President
Oda Helen Sletnes
[1] OJ L 245, 7.9.2006, p. 18.
[2] OJ L 355, 30.12.2002, p. 1.
[3] OJ L 277, 26.8.2004, p. 175.
[4] OJ L 40, 11.2.2006, p. 3.
[5] Constitutional requirements indicated.
--------------------------------------------------
