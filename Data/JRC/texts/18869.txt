Commission Regulation (EC) No 829/2003
of 14 May 2003
amending Regulation (EC) No 225/2003 as regards the extension of the validity date for certificates of origin for mushrooms originating in China
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 2201/96 of 28 October 1996 on the common organisation of the markets in processed fruit and vegetable products(1), as last amended by Commission Regulation (EC) No 453/2002(2), and in particular Article 15(1) thereof,
Whereas:
(1) Commission Regulation (EC) No 2125/95(3), as last amended by Regulation (EC) No 225/2003(4), has opened and provided for the administration of tariff quotas for preserved mushrooms.
(2) By Article 2 of Regulation (EC) No 225/2003 the Commission has established that, when applying to release preserved mushrooms originating in China into free circulation in the Community, importers may submit until 31 May 2003 certificates of origin and duplicates bearing the stamps and signatures of the Chinese authorities listed in the Annex to Regulation (EC) No 2125/95, as amended by Regulation (EC) No 1286/2002(5).
(3) Member States have informed the Commission that, in order to ensure that imports continue to run smoothly, this date needs to be postponed until the end of September 2003.
(4) Regulation (EC) No 225/2003 should therefore be amended accordingly.
(5) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Products Processed from Fruit and Vegetables,
HAS ADOPTED THIS REGULATION:
Article 1
Article 2 of Regulation (EC) No 225/2003, is replaced by the following text:
"Article 2
Until 30 September 2003, an importer may submit, when applying to release preserved mushrooms originating in China into free circulation in the Community, certificates of origin and duplicates bearing the stamps and signatures of the Chinese authorities listed in the Annex to Regulation (EC) No 2125/95 as amended by Regulation (EC) No 1286/2002."
Article 2
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 14 May 2003.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 297, 21.11.1996, p. 29.
(2) OJ L 72, 14.3.2002, p. 9.
(3) OJ L 212, 7.9.1995, p. 16.
(4) OJ L 31, 6.2.2003, p. 10.
(5) OJ L 179, 9.7.2002, p. 21.
