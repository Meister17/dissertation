Council Regulation (EC) No 1936/2005
of 21 November 2005
amending Regulation (EC) No 27/2005, as concerns herring, Greenland halibut and octopus
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 2371/2002 of 20 December 2002 on the conservation and sustainable exploitation of fisheries resources under the Common Fisheries Policy [1], and in particular Article 20 thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) Regulation (EC) No 27/2005 [2] fixes for 2005 the fishing opportunities and associated conditions for certain fish stocks and groups of fish stocks, applicable in Community waters and, for Community vessels, in waters where catch limitations are required.
(2) Pursuant to new scientific advice, the International Baltic Sea Fisheries Commission has adopted a recommendation increasing the Community’s fishing opportunities for herring in Subdivisions 30 and 31 of the Baltic Sea by 15000 tonnes to 86856 tonnes. The increase should be implemented.
(3) In accordance with corrected catch statistics, Lithuania should have access to fishing opportunities of 10 tonnes of Greenland halibut in Division IIa (Community waters) and Sub-areas IV, VI (Community waters and international waters). The corrected figure should therefore be implemented.
(4) In order to contribute to the conservation of octopus and in particular to protect the juveniles, it is necessary to establish, in 2005, a minimum size of octopus from the maritime waters under the sovereignty or jurisdiction of third countries and situated in the CECAF region pending the adoption of a Regulation amending Council Regulation (EC) No 850/98 of 30 March 1998 for the conservation of fishery resources through technical measures for the protection of juveniles of marine organisms [3].
(5) Regulation (EC) No 27/2005 should therefore be amended accordingly.
(6) Given the urgency of the matter, it is imperative to grant an exception to the six-week period referred to in paragraph I(3) of the Protocol on the role of national Parliaments in the European Union, annexed to the Treaty on European Union and to the Treaties establishing the European Communities,
HAS ADOPTED THIS REGULATION:
Article 1
Annexes IA, IB and III to Regulation (EC) No 27/2005 shall be amended in accordance with the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 21 November 2005.
For the Council
The President
J. Straw
[1] OJ L 358, 31.12.2002, p. 59.
[2] OJ L 12, 14.1.2005, p. 1. Regulation as last amended by Regulation (EC) No 1300/2005 (OJ L 207, 10.8.2005, p. 1).
[3] OJ L 125, 27.4.1998, p. 1. Regulation as last amended by Regulation (EC) No 1568/2005 (OJ L 252, 28.9.2005, p. 2).
--------------------------------------------------
ANNEX
The Annexes to Regulation (EC) No 27/2005 are amended as follows:
1. In Annex IA:
The entry concerning the species Herring in zone Subdivisions 30-31 is replaced by the following:
"Species__NEWLINE__Herring__NEWLINE__Clupea harengus | Zone__NEWLINE__Subdivisions 30-31__NEWLINE__HER/3D30.; HER/3D31. |
Finland | 72625 | |
Sweden | 14231 | |
EC | 86856 | |
TAC | 86856 | Analytical TAC where Articles 3 and 4 of Regulation (EC) No 847/96 do not apply." |
2. In Annex IB:
The entry concerning the species Greenland Halibut in zone IIa (Community waters) IV, VI (Community waters and international waters) is replaced by the following:
"Species__NEWLINE__Greenland halibut__NEWLINE__Reinhardtius hippoglossoides | ZoneIIa (Community waters), IV, VI (Community waters and international waters) |
Denmark | 10 | |
Germany | 18 | |
Estonia | 10 | |
Spain | 10 | |
France | 168 | |
Ireland | 10 | |
Lithuania | 10 | |
Poland | 10 | |
United Kingdom | 661 | |
EC | 1052 | |
Norway | 145 | |
TAC | Not relevant | |
3. In Annex III:
The following Part is added:
"PART J
CECAF
The minimum size for octopus (Octopus vulgaris) in the maritime waters under the sovereignty or jurisdiction of third countries and situated in the CECAF region shall be 450g (gutted). Octopus under the minimum size of 450g (gutted) shall not be retained on board or be transhipped, landed, transported, stored, sold, displayed or offered for sale, but shall be returned immediately to the sea."
[1] Fishing in VI is restricted to long lines.
[2] To be taken in EC waters of II and VI."
--------------------------------------------------
