Council Regulation (EC) No 1723/2006
of 20 November 2006
amending Regulation (EC) No 379/2004 as regards the increase of the volumes of tariff quotas for certain fishery products for the year 2006
(Text with EEA relevance)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 26 thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) To ensure an adequate supply of certain fishery products to the Community processing industries, autonomous Community tariff quotas have been laid down in Council Regulation (EC) No 379/2004 of 24 February 2004 opening and providing for the management of autonomous Community tariff quotas for certain fishery products for the period 2004 to 2006 [1].
(2) The processing industries in some Member States are facing serious difficulties in securing for themselves sufficient Community supplies of certain fishery products. In order to overcome the shortage of raw materials, the industry is using substitute products originating from third countries.
(3) It is therefore appropriate to increase the volumes of tariff quotas for certain products laid down in Regulation (EC) No 379/2004 for the period from 1 January to 31 December 2006.
(4) Regulation (EC) No 379/2004 should therefore be amended accordingly,
HAS ADOPTED THIS REGULATION:
Article 1
For the quota period from 1 January to 31 December 2006, the Annex to Regulation (EC) No 379/2004 shall be amended as follows:
(a) the quota amount of the tariff quota 09.2759 shall be fixed at 70000 tonnes;
(b) the quota amount of the tariff quota 09.2761 shall be fixed at 20000 tonnes;
(c) the quota amount of the tariff quota 09.2770 shall be fixed at 8000 tonnes;
(d) the quota amount of the tariff quota 09.2785 shall be fixed at 40000 tonnes;
(e) the quota amount of the tariff quota 09.2790 shall be fixed at 5500 tonnes;
(f) the quota amount of the tariff quota 09.2794 shall be fixed at 10000 tonnes.
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Union.
It shall apply from 1 January 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 20 November 2006.
For the Council
The President
J. Korkeaoja
[1] OJ L 64, 2.3.2004, p. 7.
--------------------------------------------------
