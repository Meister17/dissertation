Commission Directive 2006/119/EC
of 27 November 2006
amending for the purposes of adapting to technical progress Directive 2001/56/EC of the European Parliament and of the Council concerning heating systems for motor vehicles and their trailers
(Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 70/156/EEC of 6 February 1970 on the approximation of the laws of the Member States relating to the type-approval of motor vehicles and their trailers [1] and in particular Article 13(2) thereof,
Whereas:
(1) Directive 2001/56/EC of the European Parliament and of the Council [2] is one of the separate directives under the EC type-approval procedure which has been established by Directive 70/156/EEC. Directive 2001/56/EC lays down requirements for the type-approval of vehicles fitted with combustion heaters and of combustion heaters as components.
(2) United Nations ECE Regulation No 122 concerning the approval of vehicles of categories M, N and O with regard to their heating systems entered into force on 18 January 2006. Since this regulation is applicable to the Community, it is necessary to provide equivalence between the requirements laid down in Directive 2001/56/EC and those laid down in United Nations ECE Regulation No 122. Therefore, the specific requirements set out in Annex 9 to United Nations ECE Regulation No 122, concerning the heating systems of vehicles transporting dangerous goods, shall be introduced in Directive 2001/56/EC.
(3) Directive 2001/56/EC should therefore be amended accordingly.
(4) The measures provided for in this Directive are in accordance with the opinion of the Committee for Adaptation to Technical Progress established under Article 13 of Directive 70/156/EEC,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Amendment to Directive 2001/56/EC
Directive 2001/56/EC is amended in accordance with the text set out in the Annex to this Directive.
Article 2
Transitional provisions
1. With effect from 1 October 2007, in respect of a type of vehicle fitted with an LPG fuelled heating system which complies with the requirements laid down in Directive 2001/56/EC as amended by this Directive, Member States may not, on grounds relating to heating systems take either of the following measures:
(a) refuse to grant EC type-approval or national type-approval;
(b) prohibit the registration, sale or entry into service of a vehicle of that type.
2. With effect from 1 October 2007, in respect of a type of LPG fuelled combustion heater as a component which complies with the requirements laid down in Directive 2001/56/EC as amended by this Directive, Member States may not take either of the following measures:
(a) refuse to grant EC type-approval or national type-approval;
(b) prohibit the sale or entry into service of a component of that type.
3. With effect from 1 April 2008, in respect of a type of vehicle fitted with an LPG fuelled heating system, or a type of LPG fuelled combustion heater as a component, which does not comply with the requirements laid down in Directive 2001/56/EC as amended by this Directive, Member States shall refuse to grant EC type-approval and may refuse to grant national type-approval.
Article 3
Transposition
1. Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive by 30 September 2007 at the latest. They shall forthwith communicate to the Commission the text of those provisions and a correlation table between those provisions and this Directive.
When Member States adopt those provisions, they shall contain a reference to this Directive or be accompanied by such a reference on the occasion of their official publication. Member States shall determine how such reference is to be made.
2. Member States shall communicate to the Commission the text of the main provisions of national law which they adopt in the field covered by this Directive.
Article 4
Entry into force
This Directive shall enter into force on the 20th day following that of its publication in the Official Journal of the European Union.
Article 5
Addressees
This Directive is addressed to the Member States.
Done at Brussels, 27 November 2006.
For the Commission
Günter Verheugen
Vice-President
[1] OJ L 42, 23.2.1970, p. 1. Directive as last amended by Directive 2006/40/EC of the European Parliament and of the Council (OJ L 161, 14.6.2006, p. 12).
[2] OJ L 292, 9.11.2001, p. 21. Directive as last amended by the Commission Directive 2004/78/EC (OJ L 231, 30.6.2004, p. 69).
--------------------------------------------------
ANNEX
Directive 2001/56/EC is amended as follows:
1. The following line is added at the end of the "list of Annexes":
"Annex IX Additional provisions applicable to certain vehicles as defined in Directive 94/55/EC"
2. Annex VIII is amended as follows:
(a) The title of section 1 is replaced by the following:
"1. LPG HEATING SYSTEMS FOR ROAD USE IN MOTOR VEHICLES"
(b) Point 1.1.6.2 is replaced by the following:
"1.1.6.2. no uncontrolled release due to an accidental disconnection can occur. Means shall be provided to stop the flow of LPG by installing a device directly after, or in, a cylinder or container mounted regulator, or, if the regulator is mounted remote from the cylinder or container, a device shall be installed directly before the hose or pipe from the cylinder or container and an additional device shall be installed in, or after, the regulator."
(c) The title of Section 2 is replaced by the following:
"2. LPG HEATING SYSTEMS FOR STATIONARY USE ONLY IN MOTOR VEHICLES AND THEIR TRAILERS"
3. The following Annex IX is added:
"ANNEX IX
ADDITIONAL PROVISIONS APPLICABLE TO CERTAIN VEHICLES AS DEFINED IN DIRECTIVE 94/55/EC [1]
1. SCOPE
This annex applies to vehicles to which specific requirements concerning combustion heaters and their installation laid down in Directive 94/55/EC apply.
2. DEFINITIONS
For the purpose of this Annex, the definitions of the vehicle designations EX/II, EX/III, AT, FL and OX set out in Chapter 9.1 of Annex B to Directive 94/55/EC apply.
3. TECHNICAL PROVISIONS
3.1. General (EX/II, EX/III, AT, FL and OX vehicles)
3.1.1. The combustion heaters and their exhaust gas routing shall be designed, located, protected or covered so as to prevent any unacceptable risk of heating or ignition of the load. This requirement shall be considered as fulfilled if the fuel tank and the exhaust system of the appliance conform to the provisions set out in the points 3.1.1.1 and 3.1.1.2. Compliance with those provisions shall be verified on the completed vehicle.
3.1.1.1. Any fuel tanks for supplying the appliance shall meet the following requirements:
(a) in the event of any leakage, the fuel shall drain to the ground without coming into contact with hot parts of the vehicle or the load;
(b) fuel tanks containing petrol shall be equipped with an effective flame trap at the filler opening or with a closure enabling the opening to be kept hermetically sealed.
3.1.1.2. The exhaust system as well as the exhaust pipes shall be so directed or protected to avoid any danger to the load through heating or ignition. Parts of the exhaust system situated directly below the fuel tank (diesel) shall have a clearance of at least 100 mm or be protected by a thermal shield.
3.1.2. The combustion heater shall be switched on manually. Programming devices shall be prohibited.
3.2. EX/II and EX/III vehicles
Combustion heaters using gaseous fuels are not permitted.
3.3. FL vehicles
3.3.1. The combustion heaters shall be put out of operation by at least the following methods:
(a) intentional manual switching off from the driver’s cab;
(b) stopping of the vehicle engine; in this case the heating device may be restarted manually by the driver;
(c) start-up of a feed pump on the motor vehicle for the dangerous goods carried.
3.3.2. After running is permitted after the combustion heaters have been put out of operation. For the methods set out in points (b) and (c) of paragraph 3.3.1 the supply of combustion air shall be interrupted by suitable measures after an after-running cycle of not more than 40 seconds. Only heaters for which proof has been furnished that the heat exchanger is resistant to the reduced after-running cycle of 40 seconds for the time of their normal use shall be used.
[1] OJ L 319, 21.12.1994 p. 7."
--------------------------------------------------
