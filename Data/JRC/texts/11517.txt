Appeal brought on 21 March 2005 by Theodoros Papoulakos against the order of the Court of First Instance (First Chamber) made on 26 November 2001 in Case T-248/01 Theodoros Papoulakos
v Italian Republic and Commission of the European Communities
Parties
Appellant: Theodoros Papoulakos (represented by: D. Koutouvalis, dikigoros)
Other parties to the proceedings: Italian Republic and Commission of the European Communities
--------------------------------------------------
