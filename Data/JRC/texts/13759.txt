Action brought on 27 January 2006 by the Commission of the European Communities against the Kingdom of Belgium
An action against the Kingdom of Belgium was brought before the Court of Justice of the European Communities on 27 January 2006 by the Commission of the European Communities, represented by B. Stromsky, acting as Agent, with an address for service in Luxembourg.
The applicant claims that the Court should:
- declare that, by imposing, in the Brussels-Capital Region, a system of approval of natural and legal persons manufacturing and/or distributing refuse collection bags, the detailed rules of which infringe the principle of proportionality, the Kingdom of Belgium has failed to fulfil its obligations under Articles 28 and 30 of the EC Treaty;
- order the Kingdom of Belgium to pay the costs.
Pleas in law and main arguments
No Community harmonisation governs the approval of natural and legal persons manufacturing and/or distributing refuse collection bags.
In those circumstances, national legislation requiring the approval of natural and legal persons manufacturing and/or distributing refuse collection bags must be evaluated in the light of Articles 28 and 30 of the Treaty establishing the European Community.
According to the case-law of the Court of Justice, a prior authorisation procedure such as that laid down by Article 10a of the Regulations of the Brussels-Capital Region of 15 July 1993 on refuse collection, can restrict the free movement of goods.
To be justified with regard to the fundamental freedom of the free movement of goods, such a prior authorisation procedure must pursue a public interest purpose recognised by Community law and comply with the principle of proportionality, that is to say be appropriate to ensure the attainment of the objective pursued and not go beyond what is necessary for its attainment.
The Commission can conceive that an approval procedure might be such as to pursue the public interest purposes of protection of workers' health and of the environment.
However, the Commission submits that, in this case, the detailed rules of the approval procedure under Article 10a of the Regulations do not comply with the principle of proportionality because it is not easily accessible.
--------------------------------------------------
