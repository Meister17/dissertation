Commission Decision
of 26 August 2005
authorising methods for grading pig carcases in the Netherlands
(notified under document number C(2005) 3234)
(Only the Dutch text is authentic)
(2005/627/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 3220/84 of 13 November 1984 determining the Community scale for grading pig carcases [1], and in particular Article 5(2) thereof,
Whereas:
(1) Article 2(3) of Regulation (EEC) No 3220/84 provides that the grading of pig carcases is to be determined by estimating the content of lean meat in accordance with statistically proven assessment methods based on the physical measurement of one or more anatomical parts of the pig carcase. The authorisation of grading methods is subject to compliance with a maximum tolerance for statistical error in assessment. This tolerance was defined in Article 3(2) of Commission Regulation (EEC) No 2967/85 of 24 October 1985 laying down detailed rules for the application of the Community scale for grading pig carcases [2].
(2) By Commission Decision 87/131/EEC [3], the use of one method for grading pig carcases in the Netherlands was authorised.
(3) Due to technical adaptations, the Government of the Netherlands has requested the Commission to authorise the use of a new formula for the apparatus HGP 2 used at present under the Decision 87/131/EEC as from 1 July 2006, and to authorise a new method for grading pig carcases, and has therefore submitted the elements required in Article 3 of Regulation (EEC) No 2967/85.
(4) The evaluation of this request has revealed that the conditions for authorising these grading methods are fulfilled.
(5) No modification of the apparatuses or grading methods may be authorised except by means of a new Commission Decision adopted in the light of experience gained. For this reason, the present authorisation may be revoked.
(6) For the sake of clarity, Decision 87/131/EEC should be repealed and replaced by a new decision.
(7) The measures provided for in this Decision are in accordance with the opinion of the Management Committee for Pigmeat,
HAS ADOPTED THIS DECISION:
Article 1
The use of the following methods is hereby authorised for grading pig carcases pursuant to Regulation (EEC) No 3220/84 in the Netherlands:
(a) the apparatus termed Hennessy Grading Probe (HGP 2) and the assessment methods related thereto, details of which are given in Part 1 of the Annex;
(b) the apparatus termed VISION system (VCS 2000) and the assessment methods related thereto, details of which are given in Part 2 of the Annex.
Article 2
Modifications of the apparatuses or the assessment methods shall not be authorised.
Article 3
Decision 87/131/EEC is hereby repealed.
Article 4
This Decision is addressed to the Kingdom of the Netherlands.
Done at Brussels, 26 August 2005.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 301, 20.11.1984, p. 1. Regulation as last amended by Regulation (EC) No 3513/93 (OJ L 320, 22.12.1993, p. 5).
[2] OJ L 285, 25.10.1985, p. 39. Regulation as amended by Regulation (EC) No 3127/94 (OJ L 330, 21.12.1994, p. 43).
[3] OJ L 51, 20.2.1987, p. 50. Decision as last amended by Decision 94/566/EC (OJ L 215, 20.8.1994, p. 27).
--------------------------------------------------
ANNEX
Methods for grading pig carcases in the Netherlands
PART 1
Hennessy Grading Probe (HGP 2)
Grading of pig carcases shall be carried out by means of the apparatus termed as Hennessy Grading Probe (HGP 2).
The apparatus shall be equipped with a probe of 5,95 millimetres in diameter (and of 6,3 millimetres at the blade on top of the probe) containing a photodiode (LED Siemens of the type LYU 260-EO and photodetector of the type 58 MR) and having an operating distance of between 0 and 120 millimetres. The results of the measurements shall be converted into estimated lean meat content by means of the HGP 2 itself as well as a computer linked to it.
The lean meat content of the carcase shall be calculated according to the following formula:
= p gilt + (1 – p) castrate (%)
where:
the estimated percentage of lean meat in the carcase,
gilt= 61,38 – 0,74 X1 + 0,13 X2 (%),
castrate= 59,35 – 0,67 X1 + 0,13 X2 (%),
where:
X1= the thickness of back-fat (including rind) in millimetres, measured at six centimetres off the midline of the split carcase, between the third and fourth last ribs,
X2= the thickness of muscle in millimetres, measured at the same time and in the same place as X1,
p= 1/ (1 + exp (– η)),
where:
η= – 3,277 – 0,4580 X1 + 0,3038 X2 + 0,007777 (X1)2 – 0,001792 (X2)2 – 0,002557 X1 * X2
This formula shall be valid for carcases weighing between 50 and 120 kg.
However, as from 1 July 2006, the lean meat content of the carcase shall be calculated according to the following formula:
= 60,85 – 0,745 X1 + 0,133 X2
where:
predicted lean meat percentage,
X1= the thickness of back-fat (including rind) in millimetres, measured at six centimetres off the midline of the split carcase, between the third and fourth last ribs,
X2= the thickness of muscle in millimetres, measured at the same time and in the same place as X1.
This formula shall be valid for carcases weighing between 50 and 120 kilograms.
PART 2
VISION system (VCS 2000)
Grading of pig carcases shall be carried out by means of the apparatus termed VISION system (VCS 2000).
The apparatus VISION system (VCS 2000) is a picture-processing system for automatically determining the trade values of pork-carcass-halves. The system is used online within the slaughtering production system where via a camera system the carcass halves are automatically filmed. The picture data is then processed in a computer by special picture-processing software.
The lean meat content of the carcase shall be calculated according to the following formula:
x1 – 0,02578308
x2 0,01819554
x3 – 0,01880347
x4 0,01336851
x5 0,03402026
x6 – 0,00431013
x7 0,01399586
x8 – 0,01685383
x9 0,03029107
x10 0,00001618
x11 0,00003510
x12 0,00017285
x13 – 0,00006323
x14 – 0,00007814
x15 0,03852565
x16 0,03703442
x17 0,04915633
x18 0,16952262
x19 – 0,07474948
x20 – 0,00000064
x21 – 0,00000045
x22 – 0,00000217
x23 0,00007137
x24 0,00001311
x25 0,00005955
x26 0,00021689
x27 0,00003631
x28 0,00003996
x29 0,00010888
x30 0,00000010
x31 – 0,00002481
x32 – 0,00003633
x33 – 0,00000496
x34 – 0,00000991
x35 – 0,00000980
x36 – 0,00001532
x37 – 0,00000254
x38 – 0,00000886
x39 0,00064604
x40 – 0,00057968
x41 0,00012513
x42 – 0,00002055
x43 – 0,00000035
x44 – 0,00001008
x45 – 0,00017158
x46 0,00019477
x47 – 0,00329811
x48 0,00018512
x49 0,02520693
x50 – 0,02466706
x51 – 0,00286098
x52 – 0,01983356
x53 – 0,00012369
x54 – 0,04044495
x55 0,00069282
x56 0,00025689
x57 0,00403378
x58 0,00028090
x59 10,79585085
x60 – 0,65106186
x61 – 0,29583150
x62 0,16410084
x63 0,06306376
x64 – 2,22708974
x65 0,09886817
x66 0,55302524
x67 – 1,38136772
x68 – 12,51618138
x69 – 2,65248635
x70 9,08361318
x71 – 0,00003025
x72 – 0,00043013
x73 – 0,00083619
x74 – 0,00013210
x75 0,00002221
x76 – 0,00007579
x77 – 0,00031534
x78 – 0,00001867
x79 – 0,00003067
x80 0,00002421
x81 – 0,00026088
x82 0,00067359
x83 0,00024535
x84 – 0,00000847
x85 0,00003206
x86 0,00007141
x87 0,00006951
x88 – 0,04465410
x89 0,01774709
x90 – 0,03618843
x91 – 0,02700865
x92 – 0,02221780
x93 – 0,03644020
x94 – 0,01766434
x95 0,00516267
x96 – 0,02958650
x97 – 0,01189834
x98 0,00184860
x99 0,00894691
x100 0,01586154
x101 – 0,01265988
x102 – 0,00912249
x103 0,02734395
x104 0,13333009
x105 0,18105960
x106 – 0,06387375
x107 0,03471900
x108 0,21910356
x109 – 0,12423421
x110 0,09408101
x111 0,01363443
x112 0,06193271
x113 – 0,02802852
x114 0,02757767
x115 0,00035641
Descriptions of the measurement points and the statistical method can be found in Part II of the Dutch protocol forwarded to the Commission in accordance with Article 3(3) of Regulation (EEC) No 2967/85.
This formula shall be valid for carcases weighing between 50 and 120 kilograms.
--------------------------------------------------
