Commission Decision
of 24 January 2003
amending Decision 2000/258/EC designating a specific institute responsible for establishing the criteria necessary for standardising the serological tests to monitor the effectiveness of rabies vaccines, as regards the approval of laboratories in third countries
(notified under document number C(2003) 325)
(Text with EEA relevance)
(2003/60/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Decision 2000/258/EC, of 20 March 2000, designating a specific institute responsible for establishing the criteria necessary for standardising the serological tests to monitor the effectiveness of rabies vaccines(1), and, in particular, Article 4 thereof,
Whereas:
(1) Afssa Nancy is the reference laboratory for post-antirabies-vaccination serological testing and is in charge of the organisation of the inter-laboratories aptitude test for the approval of laboratories willing to perform those serological tests.
(2) Afssa Nancy has proposed to the Commission that laboratories in third countries which have passed the proficiency test should be approved.
(3) It is therefore necessary to provide for a procedure for the communication of the approval of laboratories in third countries.
(4) Decision 2000/258/EC should be amended accordingly.
(5) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
In Annex II to Decision 2000/258/EC, the fifth indent is replaced by the following:
"- collaborate with the laboratories responsible for carrying out these analyses in third countries and communicate to the Commission the list of these laboratories which have passed the inter-laboratory aptitude test (proficiency test). These lists will be published on the following website:
http://forum.europa.eu.int/ Public/irc/sanco/vets/information".
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 24 January 2003.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 79, 30.3.2000, p. 40.
