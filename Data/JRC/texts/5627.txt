Notice to banana producers
(2005/C 158/07)
Within the framework of compensatory aid regime to banana producers as set up by Article 12 of Council Regulation (EEC) No 404/93, the Commission, taking into account the level of prices in the producing regions, is considering as of 1 July 2005 the suspension of the regime of advances set up by Commission Regulation (EEC) No 1858/93, in order to protect the financial interests of the Community and to avoid the obligation of future repayment by the producers of the amounts paid within the framework of advances in 2005.
--------------------------------------------------
