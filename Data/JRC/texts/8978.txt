Commission Decision
of 24 January 2006
authorising the placing on the market of rye bread with added phytosterols/phytostanols as novel foods or novel food ingredients pursuant to Regulation (EC) No 258/97 of the European Parliament and of the Council
(notified under document number C(2006) 42)
(Only the Finnish and Swedish texts are authentic)
(Text with EEA relevance)
(2006/58/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Regulation (EC) No 258/97 of the European Parliament and of the Council of 27 January 1997 concerning novel foods and novel food ingredients [1], and in particular Article 7 thereof,
Whereas:
(1) On 24 September 2001 Pharmaconsult Oy Ltd (formerly MultiBene Health Oy Ltd) made a request to the competent authorities of Finland for placing phytosterols on the market.
(2) On 17 January 2002 the competent authorities of Finland issued their initial assessment report.
(3) In their initial assessment report, Finland’s competent food assessment body came to the conclusion that the phytosterols/stanols are safe for human consumption.
(4) The Commission forwarded the initial assessment report to all Member States on 5 March 2002.
(5) Within the 60-day period laid down in Article 6(4) of the Regulation, reasoned objections to the marketing of the product were raised in accordance with that provision.
(6) The Scientific Committee on Food (SCF) in its opinion "General view on the long-term effects of the intake of elevated levels of phytosterols from multiple dietary sources, with particular attention to the effects on β-carotene" of 26 September 2002 indicated that there was no evidence of additional benefits at intakes higher than 3 g/day and that high intakes might induce undesirable effects and that it was therefore prudent to avoid plant sterol intakes exceeding 3 g/day.
(7) Furthermore, the SCF, in its opinion on an application from MultiBene for approval of plant sterol enriched foods of 4 April 2003, reiterated its concerns about cumulative intakes from a wide range of foods with added phytosterols. However, at the same time the SCF confirmed that the addition of phytosterols to a wide range of bakery products was safe.
(8) In order to meet the concerns on cumulative intakes of phytosterols/phytostanols from different products Pharmaconsult Oy consequently agreed to reduce the original application on bakery products exclusively to rye bread.
(9) Commission Regulation (EC) No 608/2004 of 31 March 2004 concerning the labelling of foods and food ingredients with added phytosterols, phytosterol esters, phytostanols and/or phytostanol esters [2], ensures that consumers receive the information necessary in order to avoid excessive intake of additional phytosterols.
(10) The Standing Committee on the Food Chain and Animal Health has not given a favourable opinion; the Commission therefore submitted a proposal to the Council on 22 August 2005 in accordance with Article 5(4) of the Council Decision 1999/468/EC [3], the Council being required to act within three months.
(11) However, the Council has not acted within the required time-limit; a Decision should now be adopted by the Commission,
HAS ADOPTED THIS DECISION:
Article 1
Foods and food ingredients as described in Annex I with added phytosterols/phytostanols as specified in Annex II, hereinafter called the products, may be placed on the market in the Community.
Article 2
The products shall be presented in such a manner that they can be easily divided into portions that contain either maximum 3 g (in case of one portion per day) or maximum 1 g (in case of three portions per day) of added phytosterols/phytostanols.
Article 3
This Decision is addressed to Pharmaconsult Oy, Riippakoivunkuja 5, FIN-02130 Espoo.
Done at Brussels, 24 January 2006.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 43, 14.2.1997, p. 1. Regulation as last amended by Regulation (EC) No 1882/2003 (OJ L 284, 31.10.2003, p. 1).
[2] OJ L 97, 1.4.2004, p. 44.
[3] OJ L 184, 17.7.1999, p. 23.
--------------------------------------------------
ANNEX I
PRODUCTS REFERRED TO IN ARTICLE 1
Rye bread with flour containing ≥ 50 % rye (wholemeal rye flour, whole or cracked rye kernels and rye flakes) and ≤ 30 % wheat; and with ≤ 4 % added sugar but no fat added.
--------------------------------------------------
ANNEX II
SPECIFICATIONS OF PHYTOSTEROLS AND PHYTOSTANOLS FOR THE ADDITION TO FOODS AND FOOD INGREDIENTS
Definition
Phytosterols and phytostanols are sterols and stanols that are extracted from plants and may be presented as free sterols and stanols or esterified with food grade fatty acids.
Composition (with GC-FID or equivalent method)
< 80 % β-sitosterol
< 15 % β-sitostanol
< 40 % campesterol
< 5 % campestanol
< 30 % stigmasterol
< 3 % brassicasterol
< 3 % other sterols/stanols
Contamination/Purity (GC-FID or equivalent method)
Phytosterols and phytostanols extracted from sources other than vegetable oil suitable for food have to be free of contaminants, best ensured by a purity of more than 99 % of the phytosterol/phytostanol ingredient.
--------------------------------------------------
