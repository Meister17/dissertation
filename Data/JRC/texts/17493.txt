Commission Regulation (EC) No 316/2004
of 20 February 2004
amending Regulation (EC) No 753/2002 laying down certain rules for applying Council Regulation (EC) No 1493/1999 as regards the description, designation, presentation and protection of certain wine sector products
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1493/1999 of 17 May 1999 on the common organisation of the market in wine(1), and in particular Articles 53 and 80(b) thereof,
Whereas:
(1) After Commission Regulation (EC) No 753/2002(2) had been adopted it was found to contain some technical errors, which should be corrected. For the sake of clarity and consistency, some provisions of the Regulation should also be rearranged.
(2) Regulation (EC) No 753/2002 was notified to the World Trade Organisation. A number of wine-producing third countries entered reservations about the text. In the light of those comments, two consultations have been held in Geneva to explain the new rules on labelling and to listen to third countries' concerns.
(3) Having regard to the claims by third countries, some changes should be made to Regulation (EC) No 753/2002. These involve opening the use of certain traditional terms to third countries provided that they fulfil equivalent conditions to those required from Member States. Furthermore, given that several third countries do not have the same level of centralised regulation as the Community, some requirements should be amended while at the same time ensuring the same guarantees as to the binding nature of those rules.
(4) Given the impossibility of finalising the adoption procedure for this measure before 1 February 2004, the time limit laid down in Article 47 should be extended to 15 March 2004.
(5) Regulation (EC) No 753/2002 should be amended accordingly.
(6) The Management Committee for Wine has not has not delivered an opinion within the time limit set by its chairman,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 753/2002 is hereby amended as follows:
1. in Article 5(1) the second subparagraph is replaced by the following:"Furthermore, the Member State concerned may provide for ad hoc derogations for certain quality wines psr and quality sparkling wines psr as referred to in Article 29 aged in bottles for a long period before sale, provided that they lay down control requirements and rules for circulation for those products.
Member States shall notify the Commission of the control requirements which they have laid down.";
2. in Article 9, paragraphs 4 and 5 are deleted;
3. Article 12(1)(b) is replaced by the following:
"(b) Terms that are not defined in the Community rules but use of which is regulated in the Member State or conforms to the rules applicable to wine producers in the third country concerned, including those emanating from representative trade organisations, provided that they notify them to the Commission, which shall take all appropriate steps to ensure that those terms are publicised.";
4. Article 24 is amended as follows:
(a) in paragraph 5, the introductory phrase is replaced by the following:"To qualify for inclusion in Annex III, a traditional term must:";
(b) paragraph 6 is deleted;
(c) paragraph 8 is deleted;
5. the third paragraph of Article 28 is replaced by the following:"The rules referred to in the second paragraph may, however, allow the term '...' (traditional designation) to be used in conjunction with '...' (retsina) without necessarily being linked to a specified geographical indication.";
6. Article 29 is amended as follows:
(a) paragraph 1(d) is replaced by the following:
"(d) Spain:
- 'Denominación de origen', 'Denominación de origen calificada', 'D.O.', 'D.O.Ca', 'vino de calidad con indicación geográfica', 'vino de pago' and 'vino de pago calificado';
These terms, however, must appear on the label immediately below the name of the specified region;
- 'vino generoso', 'vino generoso de licor', 'vino dulce natural';"
(b) the last indent of paragraph 1(h) is replaced by the following:
"- 'Districtus Austriae Controllatus' or 'DAC'";
(c) paragraph 2(c) is replaced by the following:
"(c) Spain:
- 'Denominación de origen' and 'Denominación de origen calificada'; 'D.O.', 'D.O.Ca', 'vino de calidad con indicación geográfica', 'vino de pago' and 'vino de pago calificado';
These terms, however, must appear on the label immediately below the name of the specified region;";
7. Article 31 is amended as follows:
(a) in point (b) of the second subparagraph of paragraph 3 "31 August 2003" is replaced by "31 August 2005";
(b) in the third subparagraph of paragraph 3 "31 August 2003" is replaced by "31 August 2005";
8. Article 34 is amended as follows:
(a) paragraph 1 is amended as follows:
(i) in the first subparagraph, point (a) is replaced by the following:
"(a) the name, address and occupation of one or more of the persons involved in marketing, provided that the conditions of use conform to the rules applicable to wine producers in the third country concerned, including those emanating from representative trade organisations;";
(ii) in the first subparagraph, point (c) is replaced by the following:
"(c) a specific colour, provided that the conditions of use conform to the rules applicable to wine producers in the third country concerned, including those emanating from representative trade organisations.";
(iii) the second subparagraph is replaced by the following:"In the case of liqueur wine, semi-sparkling wine, aerated semi-sparkling wine and title II products made in third countries, the indication referred to in point (b) of the first subparagraph may be used provided that the conditions of use conform to the rules applicable to wine producers in the third country concerned, including those emanating from representative trade organisations.";
(b) the following paragraph 3 is added:
"3. By way of derogation from paragraphs 1 to 3 of Article 9, certain types of bottle listed in Annex I may be used for the presentation of wines originating in third countries, provided that:
(a) those countries have submitted a reasoned request to the Commission, and
(b) requirements deemed equivalent to those set out in paragraphs 2 and 3 of Article 9 have been met.
The third countries authorised to use each type of bottle are listed in Annex I, together with the rules on their use.
Some bottle types traditionally used in third countries but not included in Annex I may qualify for the protection referred to in this Article for that bottle type with a view to their marketing in the Community, if reciprocal arrangements apply.
The first subparagraph shall be implemented through agreements with the third countries concerned, concluded under the procedure laid down in Article 133 of the Treaty.";
(c) the following paragraph 4 is added:
"4. Article 37(1) shall apply mutatis mutandis to grape must in fermentation intended for direct human consumption with a geographical indication and wine of over-ripe grapes with a geographical indication.";
(d) the following paragraph 5 is added:
"5. Articles 2, 3, 4, 6, 7(c), 8, 12 and 14(1)(a), (b) and (c) shall apply mutatis mutandis.";
9. Article 36 is amended as follows:
(a) in paragraph 3, the third subparagraph is deleted;
(b) paragraphs 4 and 5 are replaced by the following:
"4. The geographical indications referred to in paragraphs 1, 2 and 3 may not be used if, although literally true as to the territory, region or locality in which the goods originate, they falsely represent to the public that the goods originate in another territory.
5. A geographical indication of a third country, as referred to in paragraphs 1 and 2, may be used on the labelling of an imported wine even where only 85 % of the wine in question was obtained from grapes harvested in the production area whose name it bears.";
10. Article 37 is amended as follows:
(a) paragraph 1 is amended as follows:
(i) the introductory phrase is replaced by the following:
"1. For the purposes of Annex VII(B)(2) to Regulation (EC) No 1493/1999, the labelling of wine originating in third countries (excluding sparkling wines, aerated sparkling wines and aerated semi-sparkling wines but including wines of over-ripe grapes) and grape musts in fermentation made in third countries for direct human consumption bearing a geographical indication in accordance with Article 36 may be supplemented by the following:";
(ii) point (a) is replaced by the following:
"(a) the vintage year; this may be used provided that the conditions of use conform to the rules applicable to wine producers in the third country concerned, including those emanating from representative trade organisations, and where at least 85 % of the grapes used to make the wine have been harvested in the year concerned, not including the quantity of products used in any sweetening.
For wines traditionally obtained from grapes harvested in winter, the year of the beginning of the current marketing year shall be shown rather than the vintage year.";
(iii) point (b)(i) is replaced by the following:
"(i) the conditions of use conform to the rules applicable to wine producers in the third country concerned, including those emanating from representative trade organisations;";
(iv) points (d), (e) and (f) are replaced by the following:
"(d) particulars concerning the production method, provided that the conditions of use conform to the rules applicable to wine producers in the third country concerned, including those emanating from representative trade organisations;
(e) in the case of wines of third countries and grape musts in fermentation for direct consumption from third countries, additional traditional indications:
(i) other than those listed in Annex III, in accordance with the rules applicable to wine producers in the third country concerned, including those emanating from representative trade organisations; and
(ii) listed in Annex III, provided that the conditions of use conform to the rules applicable to wine producers in the third country concerned, including those emanating from representative trade organisations, and meet the following requirements:
- these countries have made a substantiated request to the Commission and forwarded the relevant rules justifying recognition of the traditional indications,
- they are specific in themselves,
- they are sufficiently distinctive and/or enjoy an established reputation in the third country concerned,
- they have been traditionally used for at least 10 years in the third country in question,
- they are used for one or more categories of wine of the third country in question,
- the rules laid down by the third country are not such as to mislead consumers about the indication concerned.
In addition, some traditional indications listed in Annex III can be used on the labelling of wines carrying a geographical indication and originating in third countries in the language of the third country of origin or in another language, where use of a language other than the official language of the country is regarded as traditional in connection with a traditional indication if the use of that language is provided for in the legislation of the country concerned and if that language has been used continuously for the traditional indication for at least 25 years.
Article 23 and paragraph 2, paragraph 3, the second subparagraph of paragraph 4 and paragraph 6(c) of Article 24 shall apply mutatis mutandis.
For each traditional indication referred to in point (ii) the countries concerned are indicated in Annex III.
(f) the name of an undertaking, provided that the conditions of use conform to the rules applicable to wine producers in the third country concerned, including those emanating from representative trade organisations. Article 25(1) shall apply mutatis mutandis.";
(v) in point (g), the introductory phrase is replaced by the following:
"information about bottling, provided that the conditions of use conform to the rules applicable to wine producers in the third country concerned, including those emanating from representative trade organisations;"
(b) paragraph 3 is deleted;
11. in Title V, the following Articles 37a and 37b are added:
"Article 37a
'Representative trade organisation' means any producer organisation or association of producer organisations having adopted the same rules, operating in a given wine-growing area where it includes in its membership at least two thirds of the producers in the specified region in which it operates and accounts for at least two thirds of that region's production.
The third countries concerned shall give the Commission prior notification of the rules referred to in Articles 12(1), 34(1) and 37(1). Third countries shall also communicate a list of the representative trade organisations, with details of their members, as set out in Annex IX.
The Commission shall take all appropriate steps to ensure that these measures are publicised.";
"Article 37b
Liqueur wine, semi-sparkling wine, aerated semi-sparkling wine, sparkling wine
1. For the purposes of Annex VII(A)(4) to Regulation (EC) No 1493/1999, the labelling of liqueur wines, semi-sparkling wines and aerated semi-sparkling wines shall include, in addition to the compulsory particulars referred to in A(1) of that Annex, the importer or, where the wine has been bottled in the Community, the bottler.
In the case of the particulars referred to in the first subparagraph, Article 34(1)(a) shall apply mutatis mutandis to products made in third countries.
Article 38(2) shall apply mutatis mutandis.
2. By way of derogation from Annex VII(C)(3) to Regulation (EC) No 1493/1999, third country wines may bear the terms 'liqueur wine', 'semi-sparkling wine' or 'aerated semi-sparkling wine' if they meet the requirements laid down in points (d), (g) and (h) respectively of Annex XI to Commission Regulation (EC) No 883/2001(3).
3. Sparkling wines originating in a third country, as referred to in the third indent of Annex VIII(E)(1) to Regulation (EEC) No 1493/1999, shall be as listed in Annex VIII to this Regulation.";
12. Article 38(1) is replaced by the following:
"1. For the purposes of Annex VII(A)(4) to Regulation (EC) No 1493/1999, the labelling of liqueur wines, semi-sparkling wines and aerated semi-sparkling wines shall include, in addition to the compulsory particulars referred to in A(1) of that Annex, the name or business name and the local administrative district of the Member State of the bottler or, for containers with a nominal volume of more than 60 litres, the consignor. In the case of semi-sparkling wines the bottler's name may be replaced by that of the winemaker.
In the case of the particulars referred to in the first subparagraph, Article 15 shall apply mutatis mutandis to products made in the Community.";
13. Article 40 is deleted;
14. Article 44 is deleted;
15. Article 46 is replaced by the following:
"Article 46
'Pinot' vine varieties
In the case of sparkling wines, quality sparkling wines or quality sparkling wines psr, the variety names used to supplement the description of the product, namely 'Pinot blanc', 'Pinot noir' or 'Pinot gris' and the equivalent names in the other Community languages, may be replaced by the synonym 'Pinot'.";
16. the second subparagraph of Article 47(1) is replaced by the following:"Labels and pre-packaging material bearing particulars which were printed in conformity with the provisions applicable up to the entry into force of this Regulation may continue to be used until 15 March 2004.";
17. Annex II is replaced by Annex I to this Regulation;
18. Annex III is replaced by Annex II to this Regulation;
19. Annex III to this Regulation is added as Annex IX.
Article 2
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Union.
It shall apply from 1 February 2004.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 20 February 2004.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 179, 14.7.1999, p. 1. Regulation as last amended by Commission Regulation (EC) No 1795/2003 (OJ L 262, 14.10.2003, p. 13).
(2) OJ L 118, 4.5.2002, p. 1. Regulation as last amended by Regulation (EC) No 1205/2003 (OJ L 168 5.7.2003, p. 13).
(3) OJ L 128, 10.5.2001, p. 1.
ANNEX I
"ANNEX II
List of vine varieties and their synonyms that include a geographical indication((These variety names and their synonyms correspond, in full or in part, either in translation or in the form of an adjective, to geographical indications used to describe a wine.)) and that may appear on the labelling of wines in accordance with Article 19(2)((Legend:))
>TABLE>"
ANNEX II
"ANNEX III
List of traditional terms referred to in Article 24
>TABLE>"
ANNEX III
"ANNEX IX
>PIC FILE= "L_2004055EN.004203.TIF">"
