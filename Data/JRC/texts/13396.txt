Euro exchange rates [1]
24 August 2006
(2006/C 202/01)
| Currency | Exchange rate |
USD | US dollar | 1,2831 |
JPY | Japanese yen | 149,28 |
DKK | Danish krone | 7,4607 |
GBP | Pound sterling | 0,67740 |
SEK | Swedish krona | 9,1906 |
CHF | Swiss franc | 1,5808 |
ISK | Iceland króna | 89,97 |
NOK | Norwegian krone | 8,0755 |
BGN | Bulgarian lev | 1,9558 |
CYP | Cyprus pound | 0,5761 |
CZK | Czech koruna | 28,203 |
EEK | Estonian kroon | 15,6466 |
HUF | Hungarian forint | 279,23 |
LTL | Lithuanian litas | 3,4528 |
LVL | Latvian lats | 0,6959 |
MTL | Maltese lira | 0,4293 |
PLN | Polish zloty | 3,9333 |
RON | Romanian leu | 3,5322 |
SIT | Slovenian tolar | 239,59 |
SKK | Slovak koruna | 37,775 |
TRY | Turkish lira | 1,8984 |
AUD | Australian dollar | 1,6794 |
CAD | Canadian dollar | 1,4235 |
HKD | Hong Kong dollar | 9,9789 |
NZD | New Zealand dollar | 2,0117 |
SGD | Singapore dollar | 2,0224 |
KRW | South Korean won | 1230,81 |
ZAR | South African rand | 9,1502 |
CNY | Chinese yuan renminbi | 10,2295 |
HRK | Croatian kuna | 7,2915 |
IDR | Indonesian rupiah | 11727,53 |
MYR | Malaysian ringgit | 4,721 |
PHP | Philippine peso | 66,182 |
RUB | Russian rouble | 34,3020 |
THB | Thai baht | 48,337 |
[1] Source: reference exchange rate published by the ECB.
--------------------------------------------------
