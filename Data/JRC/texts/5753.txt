Corrigendum to Commission Decision 2005/87/EC of 2 February 2005 authorising Sweden to make use of the system established by Title I of Regulation (EC) No 1760/2000 of the European Parliament and of the Council to replace surveys of bovine livestock
(Official Journal of the European Union L 30 of 3 February 2005)
The publication of Decision 2005/87/EC should be considered null and void.
--------------------------------------------------
