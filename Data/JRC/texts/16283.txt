COUNCIL DECISION of 14 May 1996 concerning the conclusion of the Cooperation Agreement between the European Community and the Socialist Republic of Vietnam (96/351/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Articles 113 and 130y, in conjunction with the first sentence of Article 228 (2) and the first subparagraph of Article 228 (3) thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Whereas, under Article 130u of the Treaty, Community policy in the sphere of development cooperation shall foster the sustainable economic and social development of the developing countries, their smooth and gradual integration into the world economy and the campaign against poverty in those countries;
Whereas the Community should approve, for the attainment of its aims in the sphere of external relations, the Cooperation Agreement between the European Community and the Socialist Republic of Vietnam,
HAS DECIDED AS FOLLOWS:
Article 1
The Cooperation Agreement between the European Community and the Socialist Republic of Vietnam is hereby approved on behalf of the Community.
The text of this Agreement is attached to this Decision.
Article 2
The President of the Council shall, on behalf of the Community, give the notification provided for in Article 20 of the Agreement.
Article 3
The Commission, assisted by representatives of the Member States, shall represent the Community in the Joint Commission provided for in Article 14 of the Agreement.
Article 4
This Decision shall be published in the Official Journal of the European Communities.
Done at Brussels, 14 May 1996.
For the Council
The President
E. GUZZANTI
(1) OJ No C 12, 17. 1. 1996, p. 4.
(2) OJ No C 47, 19. 2. 1996, p. 25.
