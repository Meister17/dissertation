Commission Decision
of 18 February 2005
amending Decision 97/102/EC laying down special conditions governing imports of fishery and aquaculture products originating in Russia, as regards the designation of the competent authority and the model of health certificate
(notified under document number C(2005) 357)
(Text with EEA relevance)
(2005/155/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/493/EEC of 22 July 1991 laying down the health conditions for the production and the placing on the market of fishery products [1], and in particular Article 11(1) thereof,
Whereas:
(1) In Commission Decision 97/102/EC [2], the "State Fisheries Committee of the Russian Federation" is identified as the competent authority in Russia for verifying and certifying compliance of fishery and aquaculture products with the requirements of Directive 91/493/EEC.
(2) Following a restructuring of the Russian administration, the competent authority has changed to the "Ministry of Agriculture of the Russian Federation". This new authority is capable of effectively verifying the application of the rules in force.
(3) The Ministry of Agriculture has provided official assurances on compliance with the standards for health controls and monitoring of fishery and aquaculture products as set out in Directive 91/493/EEC and on the fulfilment of hygienic requirements equivalent to those laid down in that Directive.
(4) Decision 97/102/EC should therefore be amended accordingly.
(5) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
Decision 97/102/EC is amended as follows:
1. Article 1 is replaced by the following:
"Article 1
The "Ministry of Agriculture of the Russian Federation" assisted by the "National Centre of Quality and Safety of Fishery Products (National Fish Quality)" shall be the competent authority in Russia for verifying and certifying compliance of fishery and aquaculture products with the requirements of Directive 91/493/EEC."
2. Annex A is replaced by the Annex to this Decision.
Article 2
This Decision shall apply from 24 June 2005.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 18 February 2005.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 268, 24.9.1991, p. 15. Directive as last amended by Regulation (EC) No 806/2003 (OJ L 122, 16.5.2003, p. 1).
[2] OJ L 35, 5.2.1997, p. 23. Decision as last amended by Decision 2002/941/EC (OJ L 325, 30.11.2002, p. 45).
--------------------------------------------------
ANNEX
--------------------------------------------------
