COMMISSION DECISION
of 2 July 1999
amending Decision 94/269/EC laying down special conditions governing imports of fishery products originating in Colombia
(notified under document number C(1999) 1826)
(Text with EEA relevance)
(1999/486/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/493/EEC of 22 July 1991 laying down the health conditions for the production and the placing on the market of fishery products(1), as last amended by Directive 97/79/EC(2), and in particular Article 11 thereof,
(1) Whereas the Article 1 of Commission Decision 94/269/EC of 8 April 1994 laying down special conditions governing import of fishery and aquaculture products origination in Colombia(3), as last amended by Decision 96/31/EC(4), which states that the Ministerio de Salud - División de Alimentos shall be the competent authority in Colombia for verifying and certifying compiance of fishery and aquaculture products with the requirements of Directive 91/493/EEC;
(2) Whereas, following a restructuring of the Colombian Movement, the competent authority for health certificates for fishery products has changed from the Ministerio de Salud - División de Alimentos to the Instituto Nacional de Vigilancia de Medicamentos y alimentos (Invima) and this new authority is capable of effectively verifying the application of the laws in force; whereas it is, therefore, necessary to modify the nomination of the competent authority named by Decision 94/269/EC;
(3) Whereas it is convenient to harmonise the wording of Decision 94/269/EC with the wording of the more recently adopted Commission Decisions laying down special conditions governing imports of fishery and aquaculture products originating in certain third countries;
(4) Whereas the measures provided for in the Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
Commission Decision 94/269/EC is amended as follows:
1. Article 1 shall be replaced by the following: "Article 1
The Instituto Nacional de Vigilancia de Medicamentos y Alimentos (Invima) shall be the competent authority in Colombia for verifying and certifying compliance of fishery aquaculture products with the requirements of Directive 91/493/EEC."
2. Article 2 shall be replaced by the following: "Article 2
Fishery and aquaculature products originating in Colombia must meet the following conditions.
1. Each consignment must be accompanied by a numbered original health certificate, duly completed, signed, dated and comprising a single sheet in accordance with the model in Annex A hereto;
2. the products must come from approved establishments, factory vessels, cold stores or registered freezer vessels listed in Annex B hereto;
3. except in the case of frozen fishery products in bulk and intended for the manufacture of preserved foods, all packages must bear the word 'Colombia' and the approval/registration number of the establishment, factory vessel, cold store or freezer vessel of origin in indelible letters."
3. Annex A shall be replaced by the Annex hereto.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 2 July 1999.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ L 268, 24.9.1991, p. 15.
(2) OJ L 24, 30.1.1998, p. 31.
(3) OJ L 115, 6.5.1994, p. 38.
(4) OJ L 9, 12.1.1996, p. 6.
ANNEX
"ANNEX A
>PIC FILE= "L_1999190EN.003403.EPS">
>PIC FILE= "L_1999190EN.003501.EPS">"
