Commission Regulation (EC) No 1944/2004
of 10 November 2004
authorising transfers between the quantitative limits of textiles and clothing products originating in the People's Republic of China
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 3030/93 of 12 October 1993 on common rules for imports of certain textile products from third countries [1], and in particular Article 7 thereof,
Whereas:
(1) Article 5 of the Agreement between the European Economic Community and the People's Republic of China on trade in textile products, initialled on 9 December 1988 and approved by Council Decision 90/647/EEC [2], as last amended and extended by an Agreement in the form of an Exchange of Letters, initialled on 19 May 2000 and approved by Council Decision 2000/787/EC [3], provides that transfers may be made between quota years. Those flexibility provisions were notified to the Textiles Monitoring Body of the World Trade Organisation following China's accession to it.
(2) On 2 August 2004 the People's Republic of China submitted a request for transfers of quantities from the quota year 2003 to the quota year 2004.
(3) The transfers requested by the People's Republic of China fall within the limits of the flexibility provisions referred to in Article 5 of the Agreement between the European Economic Community and the People's Republic of China on trade in textile products, initialled on 9 December 1988, and as set out in Annex VIII, column 9 to Regulation (EEC) No 3030/93.
(4) It is appropriate to grant the request, to the extent that quantities are available.
(5) It is desirable for this Regulation to enter into force on the day after its publication in order to allow operators to benefit from it as soon as possible.
(6) The measures provided for in this Regulation are in accordance with the opinion of the Textile Committee set up by Article 17 of Regulation (EEC) No 3030/93,
HAS ADOPTED THIS REGULATION:
Article 1
Transfers between the quantitative limits for textile goods originating in the People's Republic of China fixed by the Agreement between the European Community and the People's Republic of China on trade in textile products are authorised for the quota year 2004 in accordance with the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 10 November 2004.
For the Commission
Pascal Lamy
Member of the Commission
--------------------------------------------------
[1] OJ L 275, 8.11.1993, p. 1. Regulation as last amended by Regulation (EC) No 1627/2004, (OJ No L 295, 18.9.2004, p. 1).
[2] OJ L 352, 15.12.1990, p. 1.
[3] OJ L 314, 14.12.2000, p. 13.
--------------------------------------------------
+++++ ANNEX 1 +++++
