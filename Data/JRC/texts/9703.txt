Commission Regulation (EC) No 1687/2006
of 15 November 2006
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 16 November 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 15 November 2006.
For the Commission
Jean-Luc Demarty
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 386/2005 (OJ L 62, 9.3.2005, p. 3).
--------------------------------------------------
ANNEX
to Commission Regulation of 15 November 2006 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code [1] | Standard import value |
07020000 | 052 | 71,2 |
204 | 37,3 |
999 | 54,3 |
07070005 | 052 | 114,5 |
204 | 65,9 |
628 | 196,3 |
999 | 125,6 |
07099070 | 052 | 118,2 |
204 | 132,8 |
999 | 125,5 |
08052010 | 204 | 86,5 |
999 | 86,5 |
08052030, 08052050, 08052070, 08052090 | 052 | 65,4 |
092 | 17,6 |
400 | 86,5 |
528 | 40,7 |
999 | 52,6 |
08055010 | 052 | 52,6 |
388 | 62,4 |
528 | 37,8 |
999 | 50,9 |
08061010 | 052 | 114,7 |
388 | 229,6 |
508 | 265,8 |
999 | 203,4 |
08081080 | 096 | 29,0 |
388 | 88,8 |
400 | 104,6 |
404 | 100,1 |
720 | 70,3 |
800 | 140,1 |
999 | 88,8 |
08082050 | 052 | 113,3 |
400 | 216,1 |
720 | 39,3 |
999 | 122,9 |
[1] Country nomenclature as fixed by Commission Regulation (EC) No 750/2005 (OJ L 126, 19.5.2005, p. 12). Code "999" stands for "of other origin".
--------------------------------------------------
