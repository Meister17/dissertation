Action brought on 10 October 2005 — Wilms v Commission
Parties
Applicant: Günter Wilms (Brussels, Belgium) (represented by: M. van der Woude and V. Landes, lawyers)
Defendant: Commission of the European Communities
Form of order sought
The applicant claims that the Court should:
- annul the formal proposal of the Director General of the Legal Service to award the applicant two Directorate-General priority points for the 2004 promotion exercise, which was confirmed and made definitive by the decision of the Director General rejecting his internal appeal;
- annul the decision of the Director General of Personnel and Administration not to award him any "Promotion Committee for additional activity in the interests of the institution" ("PPCP") special priority points for the 2004 promotion exercise;
- annul the decisions of the Director General of Personnel and Administration to award him a total of 17 points for the 2004 promotion exercise and a total of 36 points for the purpose of promotion to Grade A5 during that exercise; the list of officials who were awarded PPCPs; the merit list of Grade A6 officials for the 2004 promotion exercise after Promotion Committees; the list of officials promoted to Grade A5 for the 2004 promotion exercise and, in any event, the decision not to enter his name in those lists
- annul, so far as may be necessary, the decision rejecting his complaint;
- order the defendant to pay the costs.
Pleas in law and main arguments
In support of his action, the applicant advances similar pleas to those advanced in Case T-311/04 [1].
[1] OJ C 262, 23.10.2004, p. 44
--------------------------------------------------
