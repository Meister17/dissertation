Euro exchange rates [1]
9 November 2005
(2005/C 277/04)
| Currency | Exchange rate |
USD | US dollar | 1,1738 |
JPY | Japanese yen | 137,68 |
DKK | Danish krone | 7,4613 |
GBP | Pound sterling | 0,67530 |
SEK | Swedish krona | 9,5645 |
CHF | Swiss franc | 1,5431 |
ISK | Iceland króna | 72,47 |
NOK | Norwegian krone | 7,7485 |
BGN | Bulgarian lev | 1,9557 |
CYP | Cyprus pound | 0,5734 |
CZK | Czech koruna | 29,278 |
EEK | Estonian kroon | 15,6466 |
HUF | Hungarian forint | 248,85 |
LTL | Lithuanian litas | 3,4528 |
LVL | Latvian lats | 0,6964 |
MTL | Maltese lira | 0,4293 |
PLN | Polish zloty | 3,9978 |
RON | Romanian leu | 3,6660 |
SIT | Slovenian tolar | 239,48 |
SKK | Slovak koruna | 38,921 |
TRY | Turkish lira | 1,5990 |
AUD | Australian dollar | 1,5995 |
CAD | Canadian dollar | 1,3951 |
HKD | Hong Kong dollar | 9,1054 |
NZD | New Zealand dollar | 1,7167 |
SGD | Singapore dollar | 1,9968 |
KRW | South Korean won | 1227,91 |
ZAR | South African rand | 7,9217 |
CNY | Chinese yuan renminbi | 9,4910 |
HRK | Croatian kuna | 7,3645 |
IDR | Indonesian rupiah | 11667,57 |
MYR | Malaysian ringgit | 4,434 |
PHP | Philippine peso | 64,242 |
RUB | Russian rouble | 33,8630 |
THB | Thai baht | 48,366 |
[1] Source: reference exchange rate published by the ECB.
--------------------------------------------------
