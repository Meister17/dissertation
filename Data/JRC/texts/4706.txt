Decision of the EEA Joint Committee No 88/2005
of 10 June 2005
amending Protocol 31 to the EEA Agreement, on cooperation in specific fields outside the four freedoms
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Articles 86 and 98 thereof,
Whereas:
(1) Protocol 31 to the Agreement was amended by Decision of the EEA Joint Committee No 183/2004 of 16 December 2004 [1].
(2) It is appropriate to extend the cooperation of the Contracting Parties to the Agreement to include Decision No 2241/2004/EC of the European Parliament and of the Council of 15 December 2004 on a single Community framework for the transparency of qualifications and competences (Europass) [2].
(3) Protocol 31 to the Agreement should therefore be amended in order to allow for this extended cooperation to take place with effect from 1 January 2005,
HAS DECIDED AS FOLLOWS:
Article 1
The following indent shall be added in paragraph 2k of Article 4 of Protocol 31 to the Agreement:
"— 32004 D 2241: Decision No 2241/2004/EC of the European Parliament and of the Council of 15 December 2004 on a single Community framework for the transparency of qualifications and competences (Europass) (OJ L 390, 31.12.2004, p. 6)."
Article 2
This Decision shall enter into force on the day following the last notification to the EEA Joint Committee under Article 103(1) of the Agreement [3].
It shall apply from 1 January 2005.
Article 3
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 10 June 2005.
For the EEA Joint Committee
The President
Richard Wright
[1] OJ L 133, 26.5.2005, p. 48.
[2] OJ L 390, 31.12.2004, p. 6.
[3] No constitutional requirements indicated.
--------------------------------------------------
