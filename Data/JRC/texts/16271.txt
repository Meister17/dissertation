COMMISSION REGULATION (EC) No 1125/96 of 24 June 1996 amending Regulation (EC) No 97/95 laying down detailed rules for the application of Council Regulation (EEC) No 1766/92 as regards the minimum price and compensatory payment to be paid to potato producers and of Council Regulation (EC) No 1868/94 establishing a quota system in relation to the production of potato starch
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 1766/92 of 30 June 1992 on the common organization of the market in cereals (1), as last amended by Commission Regulation (EC) No 923/96 (2), and in particular Article 8 (5) thereof,
Having regard to Council Regulation (EC) No 1868/94 of 27 July 1994 establishing a quota system in relation to the production of potato starch (3), as last amended by Regulation (EC) No 206/96 (4), and in particular Article 8 thereof,
Whereas Commission Regulation (EC) No 97/95 (5), as last amended by Regulation (EC) No 206/96, lays down the detailed rules for the application of Regulation (EEC) No 1766/92 as regards the minimum price and compensatory payment to be paid to potato producers and of Council Regulation (EC) No 1868/94 establishing a quota system in relation to the production of potato starch;
Whereas Regulation (EC) No 97/95 fixes, among other things, the minimum starch content of potatoes delivered to starch producing undertakings at 13 %; whereas however, Commission Regulation (EC) No 2953/95 of 20 December 1995 fixing the minimum starch content for starch potatoes in certain Member States in the 1995/96 marketing year (6) derogates from this rule on the basis of the second subparagraph of Article 6 (2) of Regulation (EC) No 97/95 by fixing the minimum starch content for Austria, France, the Netherlands, Germany and Denmark at 12,8 %;
Whereas, in the light of experience, this derogation should be permanently introduced into Regulation (EC) No 97/95, allowing starch producers to accept consignments of potatoes with a starch content of less than 13 % provided that the quantity of starch that can be manufactured from those potatoes does not exceed 1 % of their sub-quota; whereas the penalties for accepting consignments with a starch content lower than the minimum permitted content should be specified by supplementing Article 13;
Whereas the definition of potatoes in Article 1 (f) must be revised;
Whereas the conditions for the grant of the compensatory payment should be clarified by supplementing Article 7 and the conditions for the grant of the premium should be clarified by supplementing Article 11;
Whereas the Management Committee for Cereals has not delivered an opinion within the time-limit set by its chairman,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 97/95 is amended as follows:
1. Article 1 (f) is replaced by the following:
'(f) potatoes: potatoes intended for the manufacture of potato starch as referred to in Article 8 of Regulation (EEC) No 1766/92 and having a starch content of at least 13 %, except where the second subparagraph of Article 6 (2) applies.`;
2. Article 6 (2) is replaced by the following:
'2. Accepted consignments must have a starch content of not less than 13 %.
However, processing starch undertakings may accept consignments of potatoes with a starch content below 13 % provided that the quantity of starch that can be manufactured from these potatoes does not exceed 1 % of the sub-quota. The minimum price to be paid in this case shall be that valid for a starch content of 13 %.`;
3. the following sentence is added to the end of the first subparagraph of Article 7 (1):
'No premium shall be granted for starch produced from potatoes that are not of sound and fair marketable quality nor for starch produced from potatoes whose starch content is below 13 %, except where the second subparagraph of Article 6 (2) applies.`;
4. the following Article 7 (a) is added:
'Article 7(a)
The compensatory payment shall be granted to producers for potatoes which are of sound and fair marketable quality, on the basis of the starch content of the potatoes delivered, in accordance with the rates fixed in Annex II. No compensatory payment shall be granted for potatoes which are not of sound and fair marketable quality nor for potatoes whose starch content is below 13 %, except where the second subparagraph of Article 6 (2) applies.`;
5. Article 11 (1) (b) is replaced by the following:
'(b) In the case of the premium referred to in Article 5 of Regulation (EC) No 1868/94, to the conditions that:
- the processing undertaking provides proof that it produced the starch in question during the marketing year concerned,
- the undertaking proves that it paid a price not less than that referred to in Article 8 (1) of Regulation (EEC) No 1766/92 at the delivered-to-factory stage for the whole quantity of potatoes produced in the Community and used for the production of starch,
- that the undertaking provides proof that the starch in question was produced using potatoes covered by the cultivation contracts referred to in Article 4.`;
6. in the first indent of Article 13 (4), the words 'percentage in question` are replaced by 'percentage recorded`;
7. the following paragraph is inserted after Article 13 (4):
'4 (a) If, contrary to Article 6 (2), the starch that can be manufactured from consignments accepted with a starch content below 13 %:
- exceeds 1 % of the processing undertaking's sub-quota, no premium shall be granted for the excess quantity. Furthermore, the premium granted for the sub-quota shall be reduced by ten times the excess percentage recorded;
- exceeds 11 % of the processing undertaking's sub-quota, no premium shall be granted for the marketing year in question. Furthermore, the processing undertaking shall be ineligible for the premium for the following marketing year.`
Article 2
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Communities.
It shall apply from 1 July 1996.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 24 June 1996.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ No L 181, 1. 7. 1992, p. 21.
(2) OJ No L 126, 24. 5. 1996, p. 37.
(3) OJ No L 197, 30. 7. 1994, p. 1.
(4) OJ No L 27, 3. 2. 1996, p. 7.
(5) OJ No L 16, 24. 1. 1995, p. 3.
(6) OJ No L 308, 21. 12. 1995, p. 44.
