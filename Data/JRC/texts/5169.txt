Protocol
to the Euro-Mediterranean Agreement establishing an Association between the European Communities and their Member States, of the one part, and the Hashemite Kingdom of Jordan, of the other part, to take account of the accession of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Hungary, the Republic of Latvia, the Republic of Lithuania, the Republic of Malta, the Republic of Poland, the Republic of Slovenia, and the Slovak Republic to the European Union
THE KINGDOM OF BELGIUM,
THE CZECH REPUBLIC,
THE KINGDOM OF DENMARK,
THE FEDERAL REPUBLIC OF GERMANY,
THE REPUBLIC OF ESTONIA,
THE HELLENIC REPUBLIC,
THE KINGDOM OF SPAIN,
THE FRENCH REPUBLIC,
IRELAND,
THE ITALIAN REPUBLIC,
THE REPUBLIC OF CYPRUS,
THE REPUBLIC OF LATVIA,
THE REPUBLIC OF LITHUANIA,
THE GRAND DUCHY OF LUXEMBOURG,
THE REPUBLIC OF HUNGARY,
THE REPUBLIC OF MALTA,
THE KINGDOM OF THE NETHERLANDS,
THE REPUBLIC OF AUSTRIA,
THE REPUBLIC OF POLAND,
THE PORTUGUESE REPUBLIC,
THE REPUBLIC OF SLOVENIA,
THE SLOVAK REPUBLIC,
THE REPUBLIC OF FINLAND,
THE KINGDOM OF SWEDEN,
THE UNITED KINGDOM OF GREAT BRITAIN AND NORTHERN IRELAND,
hereinafter referred to as "EC Member States", represented by the Council of the European Union, and
THE EUROPEAN COMMUNITY, hereinafter referred to as "the Community", represented by the Council of the European Union and the European Commission,
of the one part,
and THE HASHEMITE KINGDOM OF JORDAN, hereinafter referred to as "Jordan",
of the other part,
WHEREAS the Euro-Mediterranean Agreement establishing an Association between the European Communities and their Member States, of the one part, and the Hashemite Kingdom of Jordan, of the other part, hereinafter referred to as "the Euro-Mediterranean Agreement", was signed in Brussels on 24 of November 1997 and entered into force on 1 May 2002;
WHEREAS the Treaty concerning the accession of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic to the European Union and the Act concerning the conditions of accession thereto were signed in Athens on 16 April 2003 and entered into force on 1 May 2004;
WHEREAS, pursuant to Article 6(2) of the 2003 Act of Accession, the accession of the new Contracting Parties to the Euro-Mediterranean Agreement is to be agreed by the conclusion of a Protocol to that Agreement;
WHEREAS consultations pursuant to Article 22(2) of the Euro-Mediterranean Agreement have taken place in order to ensure that account has been taken of the mutual interests of the Community and Jordan,
HAVE AGREED AS FOLLOWS:
Article 1
The Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia, and the Slovak Republic hereby become Contracting Parties to the Euro-Mediterranean Agreement establishing an Association between the European Communities and their Member States, of the one part, and the Hashemite Kingdom of Jordan, of the other part, and shall respectively adopt and take note, in the same manner as the other Member States of the Community, of the texts of the Euro-Mediterranean Agreement, as well as of the Joint Declarations, Declarations and Exchanges of Letters thereto.
Article 2
To take account of recent institutional developments within the European Union, the Parties agree that following the expiry of the Treaty establishing the European Coal and Steel Community, existing provisions in the Euro-Mediterranean Agreement referring to the European Coal and Steel Community shall be deemed to refer to the European Community, which has taken over all rights and obligations contracted by the European Coal and Steel Community.
CHAPTER I
AMENDMENTS TO THE EURO-MEDITERRANEAN AGREEMENT, INCLUDING ITS ANNEXES AND PROTOCOLS
Article 3
Presidency of the Association Committee
Article 93(3) shall be replaced by the following:
"3. The Association Committee shall be chaired in turn by a representative of the Commission of the European Communities and by a representative of the Government of Jordan."
Article 4
Rules of origin
Protocol 3 shall be amended as follows:
1. Article 17(4) shall be replaced by the following:
"4. Movement certificates EUR.1 issued retrospectively must be endorsed with one of the following phrases:
ES "EXPEDIDO A POSTERIORI"
CS "VYSTAVENO DODATEČNĚ"
DA "UDSTEDT EFTERFØLGENDE"
DE "NACHTRÄGLICH AUSGESTELLT"
ET "TAGANTJÄRELE VÄLJA ANTUD"
EL "ΕΚΔΟΘΕΝ ΕΚ ΤΩΝ ΥΣΤΕΡΩΝ"
EN "ISSUED RETROSPECTIVELY"
FR "DÉLIVRÉ A POSTERIORI"
IT "RILASCIATO A POSTERIORI"
LV "IZSNIEGTS RETROSPEKTĪVI"
LT "RETROSPEKTYVUSIS IŠDAVIMAS"
HU "KIADVA VISSZAMENŐLEGES HATÁLLYAL"
M "MAĦRUĠ RETROSPETTIVAMENT"
NL "AFGEGEVEN A POSTERIORI"
PL "WYSTAWIONE RETROSPEKTYWNIE"
PT "EMITIDO A POSTERIORI"
SL "IZDANO NAKNADNO"
SK "VYDANÉ DODATOČNE"
FI "ANNETTU JÄLKIKÄTEEN"
SV "UTFÄRDAT I EFTERHAND"
+++++ TIFF +++++
AR """
2. Article 18(2) shall be replaced by the following:
"2. The duplicate issued in this way must be endorsed with one of the following words:
ES "DUPLICADO"
CS "DUPLIKÁT"
DA "DUPLIKAT"
DE "DUPLIKAT"
ET "DUPLIKAAT"
EL "ΑΝΤΙΓΡΑΦΟ"
EN "DUPLICATE"
FR "DUPLICATA"
IT "DUPLICATO"
LV "DUBLIKĀTS"
LT "DUBLIKATAS"
HU "MÁSODLAT"
MT "DUPLIKAT"
NL "DUPLICAAT"
PL "DUPLIKAT"
PT "SEGUNDA VIA"
SL "DVOJNIK"
SK "DUPLIKÁT"
FI "KAKSOISKAPPALE"
SV "DUPLIKAT"
+++++ TIFF +++++
AR """
3. Annex IV shall be replaced by the following:
"
"ANNEX IV
INVOICE DECLARATION
The invoice declaration, the text of which is given below, must be made out in accordance with the footnotes. However, the footnotes do not have to be reproduced.
Spanish version
El exportador de los productos incluidos en el presente documento (autorización aduanera no… [1]) declara que, salvo indicación en sentido contrario, estos productos gozan de un origen preferencial… [2].
Czech version
Vývozce výrobků uvedených v tomto dokumentu (číslo povolení… [1]) prohlašuje, že kromě zřetelně označených, mají tyto výrobky preferenční původ v… [2].
Danish version
Eksportøren af varer, der er omfattet af nærværende dokument, (toldmyndighedernes tilladelse nr.… [1]), erklærer, at varerne, medmindre andet tydeligt er angivet, har præferenceoprindelse i … [2].
German version
Der Ausführer (Ermächtigter Ausführer; Bewilligungs-Nr. … [1]) der Waren, auf die sich dieses Handelspapier bezieht, erklärt, dass diese Waren, soweit nicht anders angegeben, präferenzbegünstigte … [2] Ursprungswaren sind.
Estonian version
Käesoleva dokumendiga hõlmatud toodete eksportija Maksu-ja (Tolliameti kinnitus nr.… [1]) deklareerib, et need tooted on… [2] sooduspäritoluga, välja arvatud juhul, kui on selgelt näidatud teisiti.
Greek version
Ο εξαγωγέας των προϊόντων που καλύπτονται από το παρόν έγγραφο (άδεια τελωνείου υπ'αριθ.… [1]) δηλώνει ότι, εκτός εάν δηλώνεται σαφώς άλλως, τα προϊόντα αυτά είναι προτιμησιακής καταγωγής… [2].
English version
The exporter of the products covered by this document (customs authorisation No… [1]) declares that, except where otherwise clearly indicated, these products are of… [2] preferential origin.
French version
L’exportateur des produits couverts par le présent document (autorisation douanière no… [1]) déclare que, sauf indication claire du contraire, ces produits ont l’origine préférentielle… [2].
Italian version
L’esportatore delle merci contemplate nel presente documento (autorizzazione doganale n.… [1] dichiara che, salvo indicazione contraria, le merci sono di origine preferenziale… [2].
Latvian version
Eksportētājs produktiem, kuri ietverti šajā dokumentā (muitas pilnvara Nr.… [1]), deklarē, ka, iznemot tur, kur ir citādi skaidri noteikts, šiem produktiem ir priekšrocību izcelsme no… [2].
Lithuanian version
Šiame dokumente išvardintų prekių eksportuotojas (muitinės liudijimo Nr… [1]) deklaruoja, kad, jeigu kitaip nenurodyta, tai yra… [2] preferencinės kilmės prekės.
Hungarian version
A jelen okmányban szereplő áruk exportőre (vámfelhatalmazási szám:… [1]) kijelentem, hogy eltérő egyéztelmü jelzés hiányában az áruk preferenciális… [2] származásúak.
Maltese version
L-esportatur tal-prodotti koperti b’dan id-dokument (awtorizzazzjoni tad-dwana nru.… [1]) jiddikjara li, ħlief fejn indikat b’mod ċar li mhux hekk, dawn il-prodotti huma ta’ oriġini preferenzjali… [2].
Dutch version
De exporteur van de goederen waarop dit document van toepassing is (douanevergunning nr. … [1]), verklaart dat, behoudens uitdrukkelijke andersluidende vermelding, deze goederen van preferentiële … oorsprong zijn [2].
Polish version
Eksporter produktów objętych tym dokumentem (upoważnienie władz celnych nr… [1]) deklaruje, że z wyjątkiem gdzie jest to wyraźnie określone, produkty te mają… [2] preferencyjne pochodzenie.
Portuguese version
O exportador dos produtos cobertos pelo presente documento (autorização aduaneira n.o… [1]), declara que, salvo expressamente indicado em contrário, estes produtos são de origem preferencial… [2].
Slovenian version
Izvoznik blaga, zajetega s tem dokumentom (pooblastilo carinskih organov št… [1]) izjavlja, da, razen če ni drugače jasno navedeno, ima to blago preferencialno… [2] poreklo.
Slovak version
Vývozca výrobkov uvedených v tomto dokumente (číslo povolenia… [1]) vyhlasuje, že okrem zreteľne označených, majú tieto výrobky preferenčný pôvod v… [2].
Finnish version
Tässä asiakirjassa mainittujen tuotteiden viejä (tullin lupan: n:o … [1]) ilmoittaa, että nämä tuotteet ovat, ellei toisin ole selvästi merkitty, etuuskohteluun oikeutettuja … alkuperätuotteita [2].
Swedish version
Exportören av de varor som omfattas av detta dokument (tullmyndighetens tillstånd nr.… [1]) försäkrar att dessa varor, om inte annat tydligt markerats, har förmånsberättigande… ursprung [2].
Arabic version
+++++ TIFF +++++
… [3]
(Place and date)
… [4]
(Signature of the exporter; the name of the person signing the declaration has to be indicated in clear script)
"
CHAPTER II
TRANSITIONAL PROVISIONS
Article 5
Proofs of origin and administrative cooperation
1. Proofs of origin properly issued by either Jordan or a new Member State in the framework of preferential agreements or autonomous arrangements applied between them shall be accepted in the respective countries under this Protocol, provided that:
(a) the acquisition of such origin confers preferential tariff treatment on the basis of either the preferential tariff measures contained in the Euro-Mediterranean Agreement or in the Community scheme of generalised tariff preferences;
(b) the proof of origin and the transport documents were issued no later than the day before the date of accession;
(c) the proof of origin is submitted to the customs authorities within the period of four months from the date of accession.
Where goods were declared for importation in either Jordan or a new Member State, prior to the date of accession, under preferential agreements or autonomous arrangements applied between Jordan and that new Member State at that time, proof of origin issued retrospectively under those agreements or arrangements may also be accepted provided that it is submitted to the customs authorities within the period of four months from the date of accession.
2. Jordan and the new Member States are authorised to retain the authorisations with which the status of "approved exporters" has been granted in the framework of preferential agreements or autonomous arrangements applied between them, provided that:
(a) such a provision is also provided for in the Euro-Mediterranean Agreement concluded prior to the date of accession between Jordan and the Community;
and
(b) the approved exporters apply the rules of origin in force under that Agreement. These authorisations shall be replaced, no later than one year after the date of accession, by new authorisations issued under the conditions of the Euro-Mediterranean Agreement.
3. Requests for subsequent verification of proof of origin issued under the preferential agreements or autonomous arrangements referred to in paragraphs 1 and 2 shall be accepted by the competent customs authorities of either Jordan or the Member States for a period of three years after the issue of the proof of origin concerned and may be made by those authorities for a period of three years after acceptance of the proof of origin submitted to those authorities in support of an import declaration.
Article 6
Goods in transit
1. The provisions of the Euro-Mediterranean Agreement may be applied to goods exported from either Jordan to one of the new Member States or from one of the new Member State to Jordan, which comply with the provisions of Protocol 3 thereto and which on the date of accession are either en route or in temporary storage, in a customs warehouse or in a free zone in Jordan and in that new Member State.
2. Preferential treatment may be granted in such cases, subject to the submission to the customs authorities of the importing country, within four months of the date of accession, of a proof of origin issued retrospectively by the customs authorities of the exporting country and any other documents that provide supporting evidence of the condition of transport.
CHAPTER III
GENERAL AND FINAL PROVISIONS
Article 7
Jordan undertakes that it shall neither make any claim, request or referral nor modify or withdraw any concession pursuant to GATT 1994 Articles XXIV.6 and XXVIII in relation to this enlargement of the Community.
Article 8
This Protocol shall form an integral part of the Euro-Mediterranean Agreement.
Article 9
1. This Protocol shall be approved by the European Community, by the Council of the European Union on behalf of the Member States, and by the Hashemite Kingdom of Jordan in accordance with their own procedures.
2. The instruments of approval shall be deposited with the General Secretariat of the Council of the European Union.
Article 10
1. This Protocol shall enter into force on the first day of the first month following the date of deposit of the last instrument of approval.
2. This Protocol shall apply provisionally as from 1 May 2004.
Article 11
This Protocol shall be drawn up in duplicate in the following languages: Czech, Danish, Dutch, English, Estonian, Finnish, French, German, Greek, Hungarian, Italian, Latvian, Lithuanian, Maltese, Polish, Portuguese, Slovak, Slovenian, Spanish, Swedish and Arabic; the texts in each language being equally authentic.
Article 12
The text of the Euro-Mediterranean Agreement, including the Annexes and Protocols forming an integral part thereof, and the Final Act together with the Declarations annexed thereto, shall be drawn up in the Czech, Estonian, Hungarian, Latvian, Lithuanian, Maltese, Polish, Slovak and Slovenian languages and these texts shall be authentic in the same way as the original texts. The Association Council shall approve these texts.
Hecho en Luxemburgo, el treinta y uno de mayo de dos mil cinco.
V Lucemburku dne třicátého prvního května dva tisíce pět.
Udfærdiget i Luxembourg den enogtredivte maj to tusind og fem.
Geschehen zu Luxemburg am einunddreißigsten Mai zweitausendfünf.
Kahe tuhande viienda aasta maikuu kolmekümne esimesel päeval Luxembourgis.
Έγινε στο Λουξεμβούργο, στις τριάντα μία Μαΐου δύο χιλιάδες πέντε.
Done at Luxembourg on the thirty-first day of May in the year two thousand and five.
Fait à Luxembourg, le trente-et-un mai deux mille cinq.
Fatto a Lussembourgo, addì trentuno maggio duemilacinque.
Luksemburgā, divtūkstoš piektā gada trīsdesmit pirmajā maijā.
Priimta du tūkstančiai penktų metų gegužės trisdešimt pirmą dieną Liuksemburge.
Kelt Luxembourgban, a kettőezer-ötödik év május havanák harmincegyedik napján.
Magħmul fil-Lussemburgu, fil-wieħed u tletin jum ta’ Mejju tas-sena elfejn u ħamsa.
Gedaan te Luxemburg, de eenendertigste mei tweeduizend vijf.
Sporządzono w Luksemburgu dnia trzydziestego pierwszego maja roku dwutysięcznego piątego.
Feito en Luxemburgo, em trinta e um de Maio de dois mil e cinco.
V Luxembourgu, enaintridesetega maja leta dva tisoč pet.
V Luxemburgu, dňa tridsiateho prvého mája dvetisícpät’.
Tehty Luxemburgissa kolmantenakymmenentenäensimmäisenä päivänä toukokuuta vuonna kaksituhattaviisi.
Som skedde i Luxemburg den trettioförsta maj tjugohundrafem.
+++++ TIFF +++++
Por los Estados miembros
Za členské státy
For medlemsstaterne
Für die Mitgliedstaaten
Liikmesriikide nimel
Για τα κράτη μέλη
For the Member States
Pour les États membres
Per gli Stati membri
Dalībvalstu vārdā
Valstybių narių vardu
A tagállamok részéről
Għall-Istati Membri
Voor de lidstaten
W imieniu Państw Członkowskich
Pelos Estados-Membros
Za členské štáty
Za države članice
Jäsenvaltioiden puolesta
På medlemsstaternas vägnar
+++++ TIFF +++++
+++++ TIFF +++++
Por la Comunidad Europea
Za Evropské společenství
For Det Europæiske Fællesskab
Für die Europäische Gemeinschaft
Euroopa Ühenduse nimel
Για την Ευρωπαϊκή Κοινότητα
For the European Community
Pour la Communauté européenne
Per la Comunità europea
Eiropas Kopienas vārdā
Europos bendrijos vārdā
az Európai Közösség részéről
Għall-Komunità Ewropea
Voor de Europese Gemeenschap
W imieniu Wspólnoty Europejskiej
Pela Comunidade Europeia
Za Európske spoločenstvo
Za Evropsko skupnost
Euroopan yhteisön puolesta
På Europeiska gemenskapens vägnar
+++++ TIFF +++++
+++++ TIFF +++++
+++++ TIFF +++++
Por el Reino Hachemita de Jordania
Za Jordánské hášimovské království
For Det Hashemitiske Kongerige Jordan
Für das Haschemitische Königreich Jordanien
Jordaania Hašimiidi Kuningriigi nimel
Για το Χασεμιτικό Βασίλειο της Ιορδανίας
For the Hashemite Kingdom of Jordan
Pour le Royaume hachémite de Jordanie
Per il Regno hashemita di Giordania
Jordānijas Hāšemītu Karalistes vārdā
Jordanijos Hašimitų Karalystės vardu
A Jordán Hasimita Királyság részėről
Għar-Renju Ħaxemit tal-Ġordan
Voor het Hasjemitisch Koninkrijk Jordanië
W imieniu Jordańskiego Królestwa Haszymidzkiego
Pelo Reino Hachemita da Jordânia
Za Jordánske hášimovské kráľovstvo
Za Hašemitsko kraljevino Jordanijo
Jordanian hašemiittisen kuningaskunnan puolesta
På Hashemitiska konungariket Jordaniens vägnar
+++++ TIFF +++++
+++++ TIFF +++++
[1] When the invoice declaration is made out by an approved exporter within the meaning of Article 21 of the Protocol, the authorisation number of the approved exporter must be entered in this space. When the invoice declaration is not made out by an approved exporter, the words in brackets shall be omitted or the space left blank.
[2] Origin of products to be indicated. When the invoice declaration relates in whole or in part, to products originating in Ceuta and Melilla within the meaning of Article 36 of the Protocol, the exporter must clearly indicate them in the document on which the declaration is made out by means of the symbol "CM".
[3] These indications may be omitted if the information is contained on the document itself.
[4] See Article 20(5) of the Protocol. In cases where the exporter is not required to sign, the exemption of signature also implies the exemption of the name of the signatory."
--------------------------------------------------
