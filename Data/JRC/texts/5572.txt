Commission Decision
of 23 December 2005
amending Decision 2003/526/EC as regards classical swine fever control measures in Germany and Slovakia
(notified under document number C(2005) 5631)
(Text with EEA relevance)
(2005/946/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 90/425/EEC of 26 June 1990 concerning veterinary and zootechnical checks applicable in intra-Community trade in certain live animals and products with a view to the completion of the internal market [1], and in particular Article 10(4) thereof,
Whereas:
(1) In response to outbreaks of classical swine fever in certain Member States, Commission Decision 2003/526/EC of 18 July 2003 concerning protection measures relating to classical swine fever in certain Member States [2] was adopted. That Decision establishes certain additional disease control measures concerning classical swine fever.
(2) Germany has informed the Commission about the recent evolution of that disease in feral pigs in the federal state of North Rhine-Westphalia. In the light of the epidemiological information available, the areas in Germany where disease control measures apply should be amended to include certain areas in North Rhine-Westphalia and Rhineland-Palatinate.
(3) The disease situation in Slovakia has significantly improved in District Veterinary and Food Administrations of Trnava (comprising Trnava, Piešťany and Hlohovec districts) and Banská Bystrica (comprising Banská Bystrica and Brezno districts). The measures provided for in Decision 2003/526/EC concerning those areas should therefore no longer apply.
(4) Decision 2003/526/EC should therefore be amended accordingly.
(5) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
The Annex to Decision 2003/526/EC is replaced by the Annex to this Decision.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 23 December 2005.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 224, 18.8.1990, p. 29. Directive as last amended by Directive 2002/33/EC of the European Parliament and of the Council (OJ L 315, 19.11.2002, p. 14).
[2] OJ L 183, 22.7.2003, p. 46. Decision as last amended by Decision 2005/339/EC (OJ L 108, 29.4.2005, p. 87).
--------------------------------------------------
ANNEX
"ANNEX
PART I
Areas of Germany and France referred to in Articles 2, 3, 5, 6, 7 and 8
1. Germany
A. In the federal state Rhineland-Palatinate:
(a) the Kreise: Bad Dürkheim, Donnersbergkreis and Südliche Weinstraße;
(b) the cities of: Speyer, Landau, Neustadt an der Weinstraße, Pirmasens and Kaiserslautern;
(c) in the Kreis Alzey-Worms: the localities Stein-Bockenheim, Wonsheim, Siefersheim, Wöllstein, Gumbsheim, Eckelsheim, Wendelsheim, Nieder-Wiesen, Nack, Erbes-Büdesheim, Flonheim, Bornheim, Lonsheim, Bermershein vor der Höhe, Albig, Bechenheim, Offenheim, Mauchenheim, Freimersheim, Wahlheim, Kettenheim, Esselborn, Dintesheim, Flomborn, Eppelsheim, Ober-Flörsheim, Hangen-Weisheim, Gundersheim, Bermersheim, Gundheim, Framersheim, Gau-Heppenheim, Monsheim and Alzey;
(d) in the Kreis Bad Kreuznach: the localities Becherbach, Reiffelbach, Schmittweiler, Callbach, Meisenheim, Breitenheim, Rehborn, Lettweiler, Abtweiler, Raumbach, Bad Sobernheim, Odernheim a. Glan, Staudernheim, Oberhausen a. d. Nahe, Duchroth, Hallgarten, Feilbingert, Hochstätten, Niederhausen, Norheim, Bad Münster a. Stein-Ebernburg, Altenbamberg, Traisen, Fürfeld, Tiefenthal, Neu-Bamberg, Frei-Laubersheim, Hackenheim, Volxheim, Pleitersheim, Pfaffen-Schwabenheim, Biebelsheim, Guldental, Bretzenheim, Langenlonsheim, Laubenheim, Dorsheim, Rümmelsheim, Windesheim, Stromberg, Waldlaubersheim, Warmsroth, Schweppenhausen, Eckenroth, Roth, Boos, Hüffelsheim, Schloßböckelheim, Rüdesheim, Weinsheim, Oberstreit, Waldböckelheim, Mandel, Hargesheim, Roxheim, Gutenberg and Bad Kreuznach;
(e) in the Kreis Germersheim: the municipalities Lingenfeld, Bellheim and Germersheim;
(f) in the Kreis Kaiserslautern: the municipalities Weilerbach, Otterbach, Otterberg, Enkenbach-Alsenborn, Hochspeyer, Kaiserslautern-Süd, Landstuhl and Bruchmühlbach-Miesau, the localities Ramstein-Miesenbach, Hütschenhausen, Steinwenden and Kottweiler-Schwanden;
(g) in the Kreis Kusel: the localities Odenbach, Adenbach, Cronenberg, Ginsweiler, Hohenöllen, Lohnweiler, Heinzenhausen, Nussbach, Reipoltskirchen, Hefersweiler, Relsberg, Einöllen, Oberweiler-Tiefenbach, Wolfstein, Kreimbach-Kaulbach, Rutsweiler a.d. Lauter, Rothselberg, Jettenbach and Bosenbach;
(h) in the Rhein-Pfalz-Kreis: the municipalities Dudenhofen, Waldsee, Böhl-Iggelheim, Schifferstadt, Römerberg and Altrip;
(i) in the Kreis Südwestpfalz: the municipalities Waldfischbach-Burgalben, Rodalben, Hauenstein, Dahner-Felsenland, Pirmasens-Land and Thaleischweiler-Fröschen, the localities Schmitshausen, Herschberg, Schauerberg, Weselberg, Obernheim-Kirchenarnbach, Hettenhausen, Saalstadt, Wallhalben and Knopp-Labach;
(j) in the Kreis Ahrweiler: the municipalities Adenau and Ahrweiler;
(k) in the Kreis Daun: the munipalities Nohn and Üxheim.
B. In the federal state North Rhine-Westphalia:
- in the Kreis Euskirchen: the city Bad Münstereifel, the municipality Blankenheim (localities Lindweiler, Lommersdorf and Rohr), the city Euskirchen (localities Billig, Euenheim, Flamersheim, Kirchheim, Kreuzweingarten, Niederkastenholz, Rheder, Schweinheim, Stotzheim and Wißkirchen), the city Mechernich (localities Antweiler, Harzheim, Holzheim, Lessenich, Rissdorf, Wachendorf and Weiler am Berge), the municipality Nettersheim (localities Bouderath, Buir, Egelgau, Frohngau, Holzmühlheim, Pesch, Roderath and Tondorf).
2. France
The territory of the Department of Bas-Rhin and Moselle located west of the Rhine and the channel Rhine-Marne, north of the motorway A 4, east of the river Sarre and south of the border with Germany and the municipalities Holtzheim, Lingolsheim and Eckbolsheim.
PART II
Areas of Slovakia referred to in Articles 2, 3, 5, 7 and 8
The territory of the District Veterinary and Food Administrations (DVFA) of Trenčín (comprising Trenčín and Bánovce nad Bebravou districts), Prievidza (comprising Prievidza and Partizánske districts), Púchov (comprising Ilava district only), Žiar nad Hronom (comprising Žiar nad Hronom, Žarnovica and Banská Štiavnica districts), Zvolen (comprising Zvolen, Krupina and Detva districts), Lučenec (comprising Lučenec and Poltár districts) and Veľký Krtíš."
--------------------------------------------------
