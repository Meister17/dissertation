COUNCIL REGULATION (EEC) No 478/92 of 25 February 1992 opening an annual Community tariff quota for dog or cat food, put up for retail sale and falling within CN code 2309 10 11 and an annual Community tariff quota for fish food falling within CN code ex 2309 90 41, originating in, and coming from, the Faroe Islands
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty etablishing the European Economic Community, and in particular Article 113 thereof,
Having regard to the proposal from the Commission,
Whereas an Agreement between the European Economic Community and the Government of Denmark and the local Government of the Faroe Islands was approved by Decision 91/668/EEC (1);
Whereas that Agreement provides for an annual quota of 1 000 tonnes for dog or cat food, put up for retail sale and falling within CN code 2309 10 11 and for an annual quota of 5 000 tonnes for fish food falling within CN code ex 2309 90 41, originating in, and coming from, the Faroe Islands, with a reduction to zero of the levy; whereas these annual quotas should be opened on a permanent basis from 1992 and provision made for operational rules for them to be set by an implementing Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The import levy on dog or cat food, put up for retail sale and falling within CN code 2309 10 11 and on fish food falling within CN code ex 2309 90 41, originating in, and coming from, the Faroe Islands, shall be set at ECU 0 per tonne, within the limit of an annual quota of 1 000 tonnes for CN code 2309 10 11 and of 5 000 tonnes for CN code ex 2309 90 41.
Article 2
Detailed rules for the application of this Regulation shall be adopted in accordance with the procedure laid down in Article 26 of Regulation (EEC) No 2727/75 (2).
Article 3
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Communities.
This Regulation shall apply from 1 January 1992. This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 25 February 1992. For the Council
The President
Vitor MARTINS
(1) OJ No L 371, 31. 12. 1991, p. 1. (2) OJ No L 281, 1. 11. 1975, p. 1. Regulation last amended by Regulation (EEC) No 3653/90 (OJ No L 362, 27. 12. 1990, p. 28).
