Council Decision 2006/229/JHA
of 9 March 2006
fixing the date of application of certain provisions of Decision 2005/211/JHA concerning the introduction of some new functions for the Schengen Information System, including in the fight against terrorism
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to Council Decision 2005/211/JHA of 24 February 2005 concerning the introduction of some new functions for the Schengen Information System, including in the fight against terrorism [1], and in particular to Article 2(4) thereof,
Whereas:
(1) Decision 2005/211/JHA specifies that the provisions of Article 1 of that Decision shall apply from a date fixed by the Council, as soon as the necessary preconditions have been fulfilled, and that the Council may decide to fix different dates for the application of different provisions. Those preconditions have been fulfilled in respect of Article 1(7) of Decision 2005/211/JHA, new Article 100(3)(e).
(2) As regards Switzerland, this Decision constitutes a development of the provisions of the Schengen acquis within the meaning of the Agreement signed between the European Union, the European Community and the Swiss Confederation concerning the association of the Swiss Confederation with the implementation, application and development of the Schengen acquis [2], which falls in the area referred to in Article 1, point G of Council Decision 1999/437/EC [3] read in conjunction with Article 4(1) of Council Decisions 2004/849/EC [4] on the signing on behalf of the European Union and 2004/860/EC [5] on the signing on behalf of the European Community, and on the provisional application of certain provisions of that Agreement,
HAS ADOPTED THIS DECISION:
Article 1
Article 1(7) of Decision 2005/211/JHA, new Article 100(3)(e), shall apply from 31 March 2006.
Article 2
This Decision shall take effect on the date of its adoption. It shall be published in the Official Journal of the European Union.
Done at Brussels, 9 March 2006.
For the Council
The President
J. Pröll
[1] OJ L 68, 15.3.2005, p. 44.
[2] Council document 13054/04 accessible on http://register.consilium.eu.int
[3] OJ L 176, 10.7.1999, p. 31.
[4] OJ L 368, 15.12.2004, p. 26.
[5] OJ L 370, 17.12.2004, p. 78.
--------------------------------------------------
