Opinion of the Advisory Committee on concentrations given at its 128th meeting on 23 September 2004 concerning a preliminary draft decision in Case COMP/M.3431 — Sonoco/Ahlstrom
(2005/C 151/02)
(Text with EEA relevance)
1. The Advisory Committee agrees with the Commission that the notified operation constitutes a concentration with a Community dimension within the meaning of Article 1(3) and Article 3(1)(b) of the EC Merger Regulation, and that also it constitutes a case of cooperation under the EEA Agreement.
2. The Advisory Committee agrees with the Commission's definitions of the relevant product markets as stated in the draft decision.
3. The Advisory Committee agrees with the Commission's definitions of the relevant geographic markets as stated in the draft decision.
4. The Advisory Committee agrees with the Commission that the concentration as notified raises serious doubts as to its compatibility with the Common Market with regard to the market for high-end PMC in Scandinavia and for low-value cores in Norway and Sweden.
5. The Advisory Committee agrees with the Commission that the undertakings offered by the parties are adequate to solve the problems identified by the Commission.
6. The Advisory Committee agrees with the Commission that subject to full compliance with the undertakings offered by the parties, the notified concentration should be declared compatible with the Common Market and with the functioning of the EEA Agreement in accordance with Articles 2(2) and 8(2) of the Merger Regulation and Article 57 of the EEA Agreement.
7. The Advisory Committee agrees with the publication of its opinion in the Official Journal of the European Union.
8. The Advisory Committee asks the Commission to take into account all the other points raised during the discussion.
--------------------------------------------------
