Decision of the EEA Joint Committee
No 141/2005
of 2 December 2005
amending Annex I (Veterinary and phytosanitary matters) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex I to the Agreement was amended by Decision of the EEA Joint Committee No 110/2005 [1].
(2) Commission Decision 2005/200/EC of 2 March 2005 authorising Estonia, Latvia, Lithuania and Malta to adopt stricter requirements concerning the presence of Avena fatua in cereal seed [2] is to be incorporated into the Agreement.
(3) Commission Decision 2005/310/EC of 15 April 2005 providing for the temporary marketing of certain seed of the species Glycine max not satisfying the requirements of Council Directive 2002/57/EC [3] is to be incorporated into the Agreement.
(4) Commission Decision 2005/325/EC of 8 March 2005 releasing the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Malta and Poland from the obligation to apply to certain species Council Directives 66/401/EEC, 66/402/EEC, 68/193/EEC, 1999/105/EC and 2002/57/EC on the marketing of fodder plant seed, cereal seed, material for the vegetative propagation of the vine, forest reproductive material and seed of oil and fibre plants respectively [4] is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
Part 2 of Chapter III of Annex I to the Agreement shall be amended as follows:
1. the following point shall be inserted after point 40 (Commission Decision 2005/114/EC):
"41. 32005 D 0310: Commission Decision 2005/310/EC of 15 April 2005 providing for the temporary marketing of certain seed of the species Glycine max not satisfying the requirements of Council Directive 2002/57/EC (OJ L 99, 19.4.2005, p. 13).";
2. under the heading "ACTS OF WHICH THE EFTA STATES AND THE EFTA SURVEILLANCE AUTHORITY SHALL TAKE DUE ACCOUNT" the following points shall be inserted after point 73 (Commission Decision 1999/416/EC):
"74. 32005 D 0200: Commission Decision 2005/200/EC of 2 March 2005 authorising Estonia, Latvia, Lithuania and Malta to adopt stricter requirements concerning the presence of Avena fatua in cereal seed (OJ L 70, 16.3.2005, p. 19).
75. 32005 D 0325: Commission Decision 2005/325/EC of 8 March 2005 releasing the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Malta and Poland from the obligation to apply to certain species Council Directives 66/401/EEC, 66/402/EEC, 68/193/EEC, 1999/105/EC and 2002/57/EC on the marketing of fodder plant seed, cereal seed, material for the vegetative propagation of the vine, forest reproductive material and seed of oil and fibre plants respectively (OJ L 109, 29.4.2005, p. 1).
The provisions of the Decision shall, for the purposes of this Agreement, be read with the following adaptation:
references to other acts in the Decision shall be considered relevant to the extent and in the form that those acts are incorporated into the Agreement."
Article 2
The texts of Decisions 2005/200/EC, 2005/310/EC and 2005/325/EC in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 3 December 2005, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [5].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 2 December 2005.
For the EEA Joint Committee
The President
H.S.H. Prinz Nikolaus von Liechtenstein
[1] OJ L 339, 22.12.2005, p. 6.
[2] OJ L 70, 16.3.2005, p. 19.
[3] OJ L 99, 19.4.2005, p. 13.
[4] OJ L 109, 29.4.2005, p. 1.
[5] No constitutional requirements indicated.
--------------------------------------------------
