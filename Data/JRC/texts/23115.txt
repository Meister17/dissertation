COMMISSION DECISION of 22 January 1998 on the publication of the reference of EN 692 standard 'Mechanical presses - safety` in accordance with Council Directive 89/392/EEC (Text with EEA relevance) (98/100/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 89/392/EEC of 14 June 1989 on the approximation of the laws of the Member States relating to machinery (1), as last amended by Directive 93/68/EEC (2), and in particular Article 6(1) thereof,
Having regard to the opinion of the committee set up by Council Directive 83/189/EEC (3), as last amended by Commission Decision 96/139/EC (4),
Whereas Article 2 of Directive 89/392/EEC states that machinery may be placed on the market and put into service only if it does not endanger the health or safety of persons and, where appropriate, domestic animals or property, when properly installed and maintained and used for its intended purpose;
Whereas machinery is presumed to conform to the essential health and safety requirements referred to in Article 3 of Directive 89/392/EEC if it is in conformity with the harmonised standards whose references have been published in the Official Journal of the European Communities;
Whereas Member States are required to publish the references of national standards transposing harmonised standards;
Whereas France considers that EN 692 standard 'Mechanical presses - safety` does not fully satisfy the abovementioned essential requirements;
Whereas the Commission, after examining the information notified by France, acknowledges that there are defects in that standard regarding presses with full-revolution clutches, which could expose users to serious danger;
Whereas it appears that the parts of the standard relating to guards and control guards and those relating to the use of closed tools as a means of protection should be clarified; whereas accordingly, a standardisation mandate will have to be given to the European Committee for Standardisation (CEN) to amend the standard as soon as possible;
Whereas, while awaiting the amendment of the standard and in the interests of efficiency, account should be taken of the defects noted, although these do not affect manufacturers of presses with friction clutches covered by the same standard;
Whereas, therefore, EN 692 standard should be published along with an appropriate warning,
HAS ADOPTED THIS DECISION:
Article 1
The publication in the Official Journal of the European Communities, in accordance with Article 5(2) of Directive 89/392/EEC, of harmonised EN 692 standard 'Mechanical presses - safety` adopted by the European Committee for Standardisation (CEN) on 16 March 1996, shall be accompanied by the following warning:
'This publication does not concern:
presses with full-revolution clutches referred to in EN 692 standard, in paragraphs 5.2.3, 5.3.2, 5.4.6 and 5.5.2, tables 2, 3, 4 and 5 and Annexes A and B1,
in respect of which it grants no presumption of conformity to the provisions of Directive 89/392/EEC.`
Article 2
Where, pursuant to Article 5(2) of Directive 89/392/EEC, Member States publish the reference of a national standard transposing harmonised EN 692 standard, they shall accompany that publication by a warning identical to that provided for in Article 1.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 22 January 1998.
For the Commission
Martin BANGEMANN
Member of the Commission
(1) OJ L 183, 29. 6. 1989, p. 9.
(2) OJ L 220, 30. 8. 1993, p. 1.
(3) OJ L 109, 26. 4. 1983, p. 8.
(4) OJ L 32, 10. 2. 1996, p. 31.
