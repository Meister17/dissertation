Commission Regulation (EC) No 1531/2005
of 21 September 2005
establishing the estimated production of unginned cotton for the 2005/06 marketing year and the resulting provisional reduction of the guide price
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to the Act of Accession of Greece, and in particular Protocol 4 on cotton [1],
Having regard to Council Regulation (EC) No 1051/2001 of 22 May 2001 on production aid for cotton [2], and in particular the first indent of Article 19(2) thereof,
Whereas:
(1) Under Article 16(1) of Commission Regulation (EC) No 1591/2001 of 2 August 2001 laying down detailed rules for applying the cotton aid scheme [3], the estimated production of unginned cotton referred to in the first subparagraph of Article 14(3) of Regulation (EC) No 1051/2001 and the resulting provisional reduction of the guide price must be established before 10 September of the marketing year concerned.
(2) Under Article 19(2) of Regulation (EC) No 1051/2001 account must be taken of crop forecasts when establishing the estimated production.
(3) The provisional reduction of the guide price is to be calculated in accordance with the first subparagraph of Article 14(3) of Regulation (EC) No 1051/2001, but replacing actual production with estimated production plus 15 %.
(4) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Natural Fibres,
HAS ADOPTED THIS REGULATION:
Article 1
1. For the 2005/06 marketing year, estimated production of unginned cotton is hereby fixed at:
1050000 tonnes for Greece,
315423 tonnes for Spain,
611 tonnes for Portugal.
2. For the 2005/06 marketing year, the provisional reduction of the guide price is hereby fixed at:
34,654 EUR/100 kg for Greece,
25,299 EUR/100 kg for Spain,
EUR/100 kg for Portugal.
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 21 September 2005.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] Protocol as last amended by Council Regulation (EC) No 1050/2001 (OJ L 148, 1.6.2001, p. 1).
[2] OJ L 148, 1.6.2001, p. 3.
[3] OJ L 210, 3.8.2001, p. 10. Regulation as last amended by Regulation (EC) No 1486/2002 (OJ L 223, 20.8.2002, p. 3).
--------------------------------------------------
