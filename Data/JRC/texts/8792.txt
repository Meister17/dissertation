*****
COUNCIL DECISION
of 22 June 1988
on the establishment, by the Commission, of an inventory of the source materials and substances used in the preparation of flavourings
(88/389/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 213 thereof,
Whereas the Council adopted on 22 June 1988 Directive 87/388/EEC on the approximation of the laws of the Member States relating to flavourings for use in foodstuffs and to source materials for their production (1);
Whereas it became apparent that the acquisition of data on source materials and substances used in the preparation of flavourings was desirable with a view to assessing, on the one hand, all questions relating to flavourings and to source materials for their production and, on the other hand, the resulting action to be taken at Community level;
Whereas acquisition of the data concerned may be made easier by the preparation by the Commission of an inventory of the source materials and substances concerned,
HAS DECIDED AS FOLLOWS:
Article 1
1. The Commission shall, within 24 months of adoption of this Decision and after consultation of the Member States, establish an inventory of:
- flavouring sources composed of foodstuffs, and of herbs and spices normally considered as foods,
- flavouring sources composed of vegetable or animal raw materials not normally considered as foods,
- flavouring substances obtained by appropriate physical processes or by enzymatic or microbiological processes from vegetable or animal raw materials,
- chemically synthesized or chemically isolated flavouring substances chemically identical to flavouring substances naturally present in foodstuffs or in herbs and spices normally considered as foods,
- chemically synthesized or chemically isolated flavouring substances chemically identical to flavouring substances naturally present in vegetable or animal raw materials not normally considered as foods,
- chemically synthesized or chemically isolated flavouring substances other than those referred to in the fourth and fifth indents,
- source materials used for the production of smoke flavourings and process flavourings and the reaction conditions under which they are prepared.
2. The inventory referred to in paragraph 1 shall be regularly updated by the Commission.
Done at Luxembourg, 22 June 1988.
For the Council
The President
M. BANGEMANN
(1) See page 61 of this Official Journal.
