Commission Regulation (EC) No 230/2004
of 10 February 2004
amending Regulation (EC) No 1972/2003 on transitional measures to be adopted in respect of trade in agricultural products on account of the accession of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to the Treaty of Accession of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia, and in particular Article 2(3) thereof,
Having regard to the Act of Accession of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia, and in particular Article 41, first paragraph, thereof,
Whereas:
(1) Article 4(4) of Commission Regulation (EC) No 1972/2003(1) stipulates that the new Member States shall carry out, without delay, an inventory of stocks available as at 1 May 2004 in order to ensure that the charge to be imposed on holders of surplus stocks is applied correctly. In order to facilitate the establishment of an inventory which comprises a considerable number of agricultural and non-Annex I products and to tackle potential risks in an efficient way the new Member States may use a system of risk analysis for identifying holders of surplus stocks.
(2) It is necessary to prevent goods which have attracted an export refund from benefiting from any intervention measure or aid as laid down in Title I, Chapters II and III of Council Regulation (EC) No 1255/1999(2) of 17 May 1999 on the common organisation of the market in milk and milk products.
(3) Regulation (EC) No 1972/2003 should be amended in consequence.
(4) The measures provided for in this Regulation are in accordance with the opinion of all the Management Committees concerned,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 1972/2003 is amended as follows:
1. Article 4 is amended as follows:
(a) paragraph 4 is replaced by the following:
"4. In order to ensure that the charge referred to in paragraph 1 is correctly applied, the new Member States shall, without delay, carry out an inventory of stocks available as at 1 May 2004. To this end, they may use a system for identifying holders of surplus stocks based on a risk analysis taking due account in particular of the following criteria:
- type of activity of the holder,
- capacity of storage facilities,
- level of activity.
The new Member States shall notify the Commission of the quantity of products in surplus stocks, except of those quantities in public stocks as referred to in Article 5, by 31 October 2004 at the latest.";
(b) in the second, third, sixth, ninth and tenth indents of paragraph 5 " 2009 40 " is replaced by " 2009 41 and 2009 49 ";
2. the title of Article 8 "proof of non-payment of refunds/production refund" is replaced by "proof of non-payment of refunds";
3. in Article 9, the second sentence is replaced by the following: "Any product which attracted an export refund shall be eligible neither for production refund when used in the manufacturing of products referred to in Annex I to Commission Regulation (EC) No. 1722/93 or in Annex I to Regulation (EC) No 1265/2001 nor for any intervention measure or aid as laid down in Title I, Chapters II and III of Regulation (EC) No 1255/1999."
Article 2
This Regulation shall enter into force subject to and on the date of the entry into force of the Treaty of Accession of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia to the European Union.
It shall apply until 30 April 2007.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 10 February 2004.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 293, 11.11.2003, p. 3.
(2) OJ L 160, 26.6.1999, p. 48. Regulation as last amended by Regulation (EC) No 1787/2003 (OJ L 270, 21.10.2003, p. 121).
