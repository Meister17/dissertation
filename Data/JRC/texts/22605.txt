REGULATION (EEC) No 2622/71 OF THE COMMISSION of 9 December 1971 on procedures for the importation of rye from Turkey
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community;
Having regard to Council Regulation (EEC) No 1234/71 1 of 7 June 1971 on imports of certain cereals from Turkey, and in particular Article 4 thereof;
Whereas, by Regulation (EEC) No 1234/71, the Council adopted rules of application for the special arrangements for imports of rye from Turkey laid down in the Interim Agreement between the European Economic Community and Turkey and in the Additional Protocol to the Agreement establishing an Association between the European Economic Community and Turkey;
Whereas those special arrangements provide, under certain conditions, for a reduction of the levy on imports of rye from Turkey ; whereas, to that end, the origin of the rye and its direct transportation from Turkey to the Community must be established and proof must be furnished that a special export tax payable by the exporter has in fact been paid;
Whereas methods of administrative cooperation, including proof of origin and of the direct transportation of the rye from Turkey to a Member State were governed by Decision Nos 4/71 2 and 5/71 3 of the Council of Association, the provisions of which were made applicable by Council Regulation (EEC) No 1885/71 4 of 1 September 1971 ; whereas it suffices therefore to fix, pursuant to Article 3 of Regulation (EEC) No 1234/71, the procedure for proving payment of the special export tax by means of movement certificate A.TR.1 ; whereas Commission Regulation (EEC) No 2019/71 5 of 20 September 1971 on procedures for the importation of rye from Turkey should therefore be repealed and replaced by the present Regulation;
Whereas the measures provided for in this Regulation are in accordance with the Opinion of the Management Committee for Cereals,
HAS ADOPTED THIS REGULATION:
Article 1
Proof that the special export tax mentioned in Articles 2 and 3 of Regulation (EEC) No 1234/71 has been paid shall be furnished to the competent authority of the importing Member State by presentation of movement certificate A.TR.1. In that case, one of the following entries shall be made in the "Remarks" section by the competent authority:
"Taxe spéciale à l'exportation selon règlement (CEE) No 1234/71 acquittée pour un montant de ..."
"Besondere Ausfuhrabgabe gemäss Verordnung (EWG) nr. 1234/71 in Höhe von ... entrichtet."
"Tassa speciale per l'esportazione pagata, secondo regolamento (CEE) n 1234/71, per un importo di ..."
"Speciale heffing bij uitvoer bedoeld in Verordening (EEG) nr 1234/71 ten bedrage van ... voldaan".
Special export tax in accordance with Regulation (EEC) No 1234/71 paid in the amount of ... .
Article 2
Commission Regulation (EEC) No 2019/71 of 20 September 1971 is hereby repealed.
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 9 December 1971.
For the Commission
The President
Franco M. MALFATTI 1OJ No L 130, 16.6.1971, p. 53. 2OJ No L 197, 1.9.1971, p. 2. 3OJ No L 197, 1.9.1971, p. 11. 4OJ No L 197, 1.9.1971, p. 1. 5OJ No L 213, 21.9.1971, p. 7.
