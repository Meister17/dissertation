Corrigendum to Council Decision 2004/783/EC of 15 November 2004 appointing four Italian members and three Italian alternate members of the Committee of the Regions
(Official Journal of the European Union L 346 of 23 November 2004)
"Council Decision of 15 November 2004 appointing four Italian members and three Italian alternate members of the Committee of the Regions",
"Council Decision of 15 November 2004 appointing three Italian members and four Italian alternate members of the Committee of the Regions";
"(2) Four members' seats and three alternate members' seats on the Committee of the Regions have become vacant following the expiry of the terms of office of Mr Paolo AGOSTINACCHIO (IT), Mr Gianfranco LAMBERTI (IT), Mr Salvatore TATARELLA (IT) and Mr Riccardo VENTRE (IT), members, and of Mr Gabriele BAGNASCO (IT), Mr Marcello MEROI (IT) and Mr Roberto PELLA (IT), alternate members, of which the Council was informed on 7 October 2004,"
"(2) Three members' seats and four alternate members' seats on the Committee of the Regions have become vacant following the expiry of the mandates of Mr Paolo AGOSTINACCHIO (IT), Mr Gianfranco LAMBERTI (IT) and Mr Riccardo VENTRE (IT), members, and of Mr Gabriele BAGNASCO (IT), Mr Marcello MEROI (IT), Mr Roberto PELLA (IT) and Mr Salvatore TATARELLA (IT), alternate members, of which the Council was informed on 7 October 2004,";
1. in (a) as members: point 3 relating to Mr Savino Antonio SANTARELLA shall be deleted;
2. in (b) as alternate members: the following point 4 shall be added:
"4. Mr Savino Antonio SANTARELLA
Sindaco di Candela
in place of Mr Salvatore TATARELLA,".
--------------------------------------------------
