Council Decision
of 4 October 2004
concerning the conclusion of an Agreement between the European Community and the Republic of Kazakhstan amending the Agreement between the European Coal and Steel Community and the Government of the Republic of Kazakhstan on trade in certain steel products
(2004/814/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 133 in conjunction with the first sentence of the first subparagraph of Article 300(2) thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) The Partnership and Cooperation Agreement between the European Communities and their Member States and the Republic of Kazakhstan [1], entered into force on 1 July 1999.
(2) Article 17(1) of the Partnership and Cooperation Agreement provides for trade in European Coal and Steel Community (hereinafter referred to as the ECSC) products to be governed by Title III of that Agreement, save for Article 11 thereof, and by the provisions of an Agreement on quantitative arrangements concerning exchanges of ECSC products.
(3) On 22 July 2002 the ECSC and the Government of the Republic of Kazakhstan concluded such an Agreement on trade in certain steel products [2] (hereinafter referred to as the Agreement), approved on behalf of the ECSC by Commission Decision 2002/654/ECSC [3].
(4) The ECSC Treaty expired on 23 July 2002 and the European Community took over all rights and obligations contracted by the ECSC.
(5) The Parties agreed pursuant to Article 11(2) of the Agreement that it was to be continued and that all rights and obligations of the Parties under it were to be maintained after such expiry.
(6) The Parties entered into consultations as provided for in Article 2 paragraph 6 of the Agreement and agreed to increase the quantitative limits set out in the Agreement to take into account the enlargement of the European Union.
(7) The amending Agreement should be approved,
HAS DECIDED AS FOLLOWS:
Article 1
1. The Agreement between the European Community and the Government of the Republic of Kazakhstan amending the Agreement between the ECSC and the Government of the Republic of Kazakhstan concerning trade in certain steel products is hereby approved on behalf of the Community.
2. The text of the amending Agreement [4] is attached to this Decision.
Article 2
The President of the Council is hereby authorised to designate the person empowered to sign the Agreement in order to bind the Community.
Done at Luxembourg, 4 October 2004.
For the Council
The President
A. J. de Geus
--------------------------------------------------
[1] OJ L 196, 28.7.1999, p. 3.
[2] OJ L 222, 19.8.2002, p. 20.
[3] OJ L 222, 19.8.2002, p. 19.
[4] See page 23 of this Official Journal.
