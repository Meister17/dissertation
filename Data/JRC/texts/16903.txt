Decision No 804/2004/EC of the European Parliament and of the Council
of 21 April 2004
establishing a Community action programme to promote activities in the field of the protection of the Community's financial interests (Hercule programme)
THE EUROPEAN PARLIAMENT AND THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 280(4) thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the Court of Auditors(1),
Acting in accordance with the procedure laid down in Article 251 of the Treaty(2),
Whereas:
(1) The Community and the Member States have set themselves the objective of countering fraud and any other illegal activities affecting the Community's financial interests. All available means must be deployed to attain that objective, whilst maintaining the current distribution and balance of responsibilities between the national and Community levels.
(2) Activities with the purpose of providing better information, carrying out studies and providing training or technical and scientific assistance in the fight against fraud help significantly to protect the Community's financial interests.
(3) Activities in this field should therefore be promoted and bodies engaged in this field should be supported by awarding operating grants. Experience has shown the value of providing support at Community level as compared with national promotional activities.
(4) Support for bodies and activities was provided until 2003 by credits entered in lines A03600 and A03010 (Conferences, congresses and meetings in connection with the activities of the associations of European lawyers for the protection of the financial interests of the Community) and in line B5-910 (General measures to combat fraud) of the general budget of the European Union.
(5) Article 112 of Council Regulation (EC, Euratom) No 1605/2002 of 25 June 2002 on the Financial Regulation applicable to the general budget of the European Communities(3) lays down strict conditions for financial assistance for measures specified in a basic instrument which have already begun.
(6) It is therefore necessary to adopt that basic instrument so that, by adopting this Decision establishing a structured, specific and multidisciplinary Community action programme for a substantial period, all existing support measures are rationalised and supplemented.
(7) The programme should be opened up to all Member States and neighbouring countries in view of the importance of providing effective and equivalent protection for the Community's financial interests beyond the Member States alone.
(8) The European Parliament, the Council and the Commission, when adopting Regulation (EC, Euratom) No 1605/2002, undertook to achieve the objective of ensuring that this basic act comes into force as from the financial year 2004.
(9) The support measures should also take account of the particular characteristics of bodies involved in protecting the Community's financial interests.
(10) This Decision lays down for the entire duration of the programme a financial framework constituting the prime reference, within the meaning of point 33 of the Interinstitutional Agreement of 6 May 1999 between the European Parliament, the Council and the Commission on budgetary discipline and improvement of the budgetary procedure(4), for the budgetary authority during the annual budgetary procedure.
(11) The Commission should present to the European Parliament and the Council an interim report by the European Anti-Fraud Office (OLAF) on the implementation of this programme and a final report on the attainment of the said programme's objectives.
(12) This Decision complies with the principles of subsidiarity and proportionality.
(13) This Decision is without prejudice to the grants awarded in the field of protection of the Community's financial interests on the basis of programmes concerning the law-enforcement aspect,
HAVE DECIDED AS FOLLOWS:
Article 1
Objective of the programme
1. This Decision establishes a Community action programme to promote activities in the field of the protection of the financial interests of the Community. The programme shall be known as the "Hercule programme".
2. The purpose of the programme shall be to help protect the Community's financial interests by promoting activities and supporting bodies in accordance with the general criteria set out in the Annex and specified in detail in each annual grants programme. It shall take transnational and multidisciplinary aspects into account. It shall focus on aligning the substance of activities so as to guarantee effective and equivalent levels of protection on the basis of mutually agreed best practice while also respecting the distinct traditions of each Member State.
Article 2
Access to the programme
1. To qualify for a Community grant for an activity aimed at protecting the Community's financial interests, grant beneficiaries must comply with the provisions set out in the Annex. The activity must conform to the principles underlying Community activity in the field of the protection of the Community's financial interests and take account of the specific criteria laid down in the related calls for proposals, in accordance with the priorities set out in the annual grants programme detailing the general criteria set out in the Annex.
2. To qualify for a Community operating grant under the ongoing work programme of a body which pursues an objective of general European interest in the field of the protection of the Community's financial interests, the body concerned must satisfy the general criteria set out in the Annex.
3. Applications for operating grants must contain all necessary information, to enable the Commission to select beneficiaries, on:
- the type of body,
- the measures to protect the Community's financial interests,
- the likely cost of implementing the measures,
- all criteria set out in point 4 of the Annex.
Article 3
Participation by countries outside the Community
In addition to beneficiaries and bodies located in the Member States, participation in the Community action programme shall be open to those located in:
(a) acceding countries which signed the Accession Treaty on 16 April 2004;
(b) the EFTA/EEA countries, in accordance with the conditions laid down in the EEA Agreement;
(c) Bulgaria and Romania, in accordance with the conditions laid down in the Europe Agreements, in their additional protocols and in the decisions of the respective Association Councils;
(d) Turkey, in accordance with Council Decision 2002/179/EC of 17 December 2001 concerning the conclusion of a Framework Agreement between the European Community and the Republic of Turkey on the general principles for the participation of the Republic of Turkey in Community programmes(5).
Article 4
Selection of beneficiaries
1. The programme covers one type of awarding procedure by means of a call for proposals for all beneficiaries.
2. The bodies to receive grants for activities shall be selected following a call for proposals, in accordance with the priorities set out in the annual grants programme, which details the general criteria referred to in the Annex. Grants awarded for an activity covered by the programme shall meet the general criteria set out in the Annex.
3. The bodies to receive operating grants shall be selected following a call for proposals. The award of an operating grant on the basis of a beneficiary's ongoing work programme shall meet the general criteria set out in the Annex. On the basis of the call for proposals, the Commission shall draw up a list of the beneficiaries and the amounts approved, in accordance with Article 116 of Regulation (EC, Euratom) No 1605/2002.
Article 5
Awarding of the grant
1. Financial assistance granted for activities may not cover all eligible expenditure. The amount of a grant for an activity awarded under the programme may not exceed the following rates:
(a) 50 % of eligible expenditure for technical support;
(b) 80 % of eligible expenditure for training measures, promoting exchanges of specialised staff and the holding of seminars and conferences, provided that the beneficiaries are those referred to in point 2, first indent of the Annex;
(c) 90 % of eligible expenditure for the holding of seminars and conferences, etc., provided that the beneficiaries are those referred to in point 2, second and third indents of the Annex.
2. The amount of an operating grant awarded under this programme may not exceed 70 % of the eligible expenditure of the body for the calendar year for which the grant is awarded.
Pursuant to Article 113(2) of Regulation (EC, Euratom) No 1605/2002, where such operating grants are renewed, they shall be gradually decreased. If a grant is awarded to a body which received an operating grant the preceding year, the percentage of Community co-financing represented by the new grant shall be at least 10 percentage points lower than the Community co-financing percentage represented by the grant in the preceding year.
Article 6
Financial provisions
1. This programme shall start on 1 January 2004 and end on 31 December 2006.
2. The financial framework for the implementation of the programme for the period 2004 to 2006 shall be EUR 11775000.
3. The annual appropriations shall be authorised by the budgetary authority within the limits of the financial perspective.
Article 7
Monitoring and evaluation
The Commission shall present to the European Parliament and the Council:
(a) by 30 June 2006 at the latest, a report by OLAF on the implementation of the programme and the appropriateness of continuing it;
(b) by 31 December 2007 at the latest, a report by OLAF on the achievement of the objectives of the programme. The report, based on the results obtained by the beneficiaries, shall assess, in particular, their effectiveness in achieving the objectives defined in Article 1 and in the Annex.
Article 8
Entry into force
This Decision shall enter into force on the day following that of its publication in the Official Journal of the European Union.
Done at Strasbourg, 21 April 2004.
For the European Parliament
The President
P. Cox
For the Council
The President
D. Roche
(1) OJ C 318, 30.12.2003, p. 5.
(2) Opinion of the European Parliament of 9 March 2004 (not yet published in the Official Journal) and Council Decision of 5 April 2004.
(3) OJ L 248, 16.9.2002, p. 1. Corrigendum in OJ L 25, 30.1.2003, p. 43.
(4) OJ C 172, 18.6.1999, p. 1. Agreement as amended by Decision 2003/429/EC of the European Parliament and of the Council (OJ L 147, 14.6.2003, p. 25).
(5) OJ L 61, 2.3.2002, p. 27.
ANNEX
1. ACTIVITIES SUPPORTED
The general objective laid down in Article 1 is to reinforce Community action to prevent fraud affecting the Community's financial interests and to combat such fraud by promoting actions in this field and the operation of bodies engaged in it.
Activities of bodies which may help to reinforce and increase the effectiveness of Community action, in accordance with Article 2, include the following:
- organisation of seminars and conferences,
- promotion of scientific studies and discussions on Community policies in the field of the protection of the Community's financial interests,
- coordination of activities relating to the protection of the Community's financial interests,
- training and awareness,
- promoting exchanges of specialised staff,
- dissemination of scientific information on Community action,
- development and supply of specific IT tools,
- technical assistance,
- promoting and expanding the exchange of data.
2. CARRYING OUT THE ACTIVITIES SUPPORTED
The activities carried out by bodies which may receive a Community grant under the programme come under the heading of actions aimed at strengthening Community measures to protect financial interests and pursue objectives of general European interest in this field or an objective which is part of the European Union's policy in this area.
In accordance with Article 2 of the Decision, the following bodies shall have access to the programme:
- all national or regional administrations of a Member State or country outside the Community, as defined in Article 3, which promote the strengthening of the Community's action in the field of the protection of the Community's financial interests,
- all research and education institutes that have had legal personality for at least one year and are established and operating in a Member State or in a country outside the Community, as defined in Article 3, and that promote the strengthening of the Community's action in protecting its financial interests,
- all non-profit-making bodies that have had legal personality for at least one year and are legally established in a Member State or country outside the Community, as defined in Article 3, and that promote the strengthening of Community action to protect the Community's financial interests.
An annual operating grant may be awarded to support implementation of such a body's ongoing programme of work.
3. SELECTION OF BENEFICIARIES
Bodies entitled to receive a grant for the activity or an operating grant under point 2 will be selected on the basis of calls for proposals.
4. CHARACTERISTICS OF GRANT APPLICATIONS ON WHICH ASSESSMENT IS BASED
Applications for grants for activities or, where appropriate, operating grants are assessed in the light of:
- consistency of the proposed activity in relation to the objectives of the programme,
- complementarity of the proposed activity with other assisted activities,
- feasibility of the proposed activity, i.e. the real possibility that it can be carried out using the proposed means,
- the cost-benefit ratio,
- the added utility of the proposed activity,
- size of public targeted by the proposed activity,
- transnational and multidisciplinary aspects of the activity,
- geographic scope of the proposed measure.
5. ELIGIBLE EXPENDITURE
Pursuant to point 2, only the expenditure required for the successful implementation of the activity is taken into account when calculating the grant.
Also eligible is expenditure in connection with the participation of representatives of the Balkan countries forming part of the stabilisation and association process for countries of south eastern Europe(1) and certain countries of the Commonwealth of Independent States(2).
6. CHECKS AND AUDITS
6.1. The beneficiary of an operating grant must keep available for the Commission all the supporting documents, in particular the audited financial statement, regarding expenditure incurred during the grant year for a period of five years following the last payment. The beneficiary of a grant must ensure that, where applicable, supporting documents in the possession of partners or members are made available to the Commission.
6.2. The Commission may have an audit of the use made of the grant carried out either directly by its own staff or by any other qualified outside body of its choice. Such audits may be carried out throughout the lifetime of the agreement and for a period of five years from the date of payment of the balance. Where appropriate, the audit findings may lead to recovery decisions by the Commission.
6.3. Commission staff and outside personnel authorised by the Commission have appropriate right of access to sites and premises where the action is carried out and to all the information, including information in electronic format, needed in order to conduct such audits.
6.4. The Court of Auditors and OLAF must enjoy the same rights, especially of access, as the persons referred to in point 6.3.
6.5. Furthermore, in order to protect the European Community's financial interests against fraud and other irregularities, the Commission carries out on-the-spot checks and inspections under this programme in accordance with Council Regulation (Euratom, EC) No 2185/96 of 11 November 1996 concerning on-the-spot checks and inspections carried out by the Commission in order to protect the European Communities' financial interests against fraud and other irregularities(3). Where necessary, investigations must be conducted by the European Anti-Fraud Office (OLAF) and governed by Regulation (EC) No 1073/1999 of the European Parliament and of the Council(4).
7. MANAGEMENT OF THE PROGRAMME
On the basis of a cost-effectiveness analysis, the Commission may employ experts and make use of any other form of technical and administrative assistance not involving public authority tasks outsourced under ad hoc service contracts. It may also finance studies and organise meetings of experts to facilitate the implementation of the programme, and take information, publication and dissemination measures directly linked to fulfilling the objectives of the programme.
(1) Former Yugoslav Republic of Macedonia, Albania, Serbia and Montenegro, Bosnia-Herzegovina and Croatia.
(2) Belarus, Moldavia, Russian Federation and Ukraine.
(3) OJ L 292, 15.11.1996, p. 2.
(4) OJ L 136, 31.5.1999, p. 1.
