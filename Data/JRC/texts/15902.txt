Council Decision
of 21 December 2005
amending Decision 2004/465/EC on a Community financial contribution towards Member States fisheries control programmes
(2006/2/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 37 thereof,
Having regard to the proposal from the Commission,
Having regard to the Opinion of the European Parliament [1],
Whereas:
(1) The Common Fisheries Policy (CFP) sets out general rules on the conservation, management and responsible exploitation, and processing and marketing of living aquatic resources.
(2) Council Regulation (EC) No 2371/2002 of 20 December 2002 on the conservation and sustainable exploitation of fisheries resources under the Common Fisheries Policy [2] underlined the need to enhance cooperation and coordination among Member States and with the Commission in order to strengthen control and discourage behaviour contrary to CFP rules.
(3) The period covered by Decision 2004/465/EC [3] will expire on 31 December 2005.
(4) Due to domestic budgetary and administrative difficulties, Member States which acceded to the Community on 1 May 2004 have obtained very limited Community financial assistance under the current financial scheme foreseen under Decision 2004/465/EC.
(5) The new financial perspective will cover the period 2007 to 2013. In order to avoid an interruption of Community financial support, it is therefore necessary that the financial assistance available to Member States under Decision 2004/465/EC is continued during 2006.
(6) It is appropriate to include in Decision 2004/465/EC studies on fisheries control as well as arrangements designed to facilitate the implementation of new technologies on control.
(7) Decision 2004/465/EC should therefore be amended accordingly,
HAS ADOPTED THIS DECISION:
Article 1
Decision 2004/465/EC is hereby amended as follows:
1. Article 3(2) shall be replaced by the following:
"2. All Member States shall submit their annual fisheries control programme by 1 June 2004 for 2004 and by 31 January for 2005 and by 31 January 2006 for 2006."
2. The following points shall be added to Article 4(1):
"(i) Administrative arrangements with the Joint Research Centre aimed at implementing new technologies on control;
(j) Studies on control-related areas carried out at the initiative of the Commission."
3. The first sentence of Article 5(1) shall be replaced by the following:
"The financial reference amount for the implementation of the actions for which financial assistance is provided for the period 2004 to 2006 shall be EUR 105 million".
4. The following point shall be added to Article 6(2):
"(d) for the actions referred to in Article 4(1)(i) and (j), the rate may be 100 % of the eligible expenditure."
5. In Article 12(2), " 31 December 2008" shall be replaced by " 31 December 2010".
6. In Article 16(b):
- " 31 December 2006" shall be replaced by " 31 December 2007",
- point (v) shall be replaced by the following:
"(v) the impact of the financial contribution on fisheries control programmes over the whole period from 2001 to 2006."
7. In Article 17, " 30 June 2007" shall be replaced by " 30 June 2008".
Article 2
This Decision shall apply from 1 January 2006.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 21 December 2005.
For the Council
The President
B. Bradshaw
[1] Not yet published in the Official Journal.
[2] OJ L 358, 31.12.2002, p. 59.
[3] OJ L 157, 30.4.2004, p. 114.
--------------------------------------------------
