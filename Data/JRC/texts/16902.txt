Commission Decision
of 29 October 2004
on a Community financial contribution for 2004 to cover expenditure incurred by Belgium and Portugal for the purpose of combating organisms harmful to plants or plant products
(notified under document number C(2004) 4181)
(Only the French, Dutch and Portuguese texts are authentic)
(2004/772/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 2000/29/EC of 8 May 2000 on protective measures against the introduction into the Community of organisms harmful to plants or plant products and against their spread within the Community [1] (hereinafter "the Directive"), and in particular Article 23 thereof,
Whereas:
(1) Pursuant to the Directive, a financial contribution from the Community may be granted to Member States to cover expenditure relating directly to the necessary measures which have been taken or are planned to be taken for the purpose of combating harmful organisms introduced from third countries or from other areas in the Community, in order to eradicate or, if that is not possible, to contain them.
(2) Belgium and Portugal have each established a programme of actions to eradicate organisms harmful to plants introduced in their territories. These programmes specify the objectives to be achieved, the measures carried out, their duration and their cost. They have applied for the allocation of a Community financial contribution to these programmes within the time limit set out in the Directive and in accordance with Commission Regulation (EC) No 1040/2002 of 14 June 2002 establishing detailed rules for the implementation of the provisions relating to the allocation of a financial contribution from the Community for plant-health control and repealing Regulation (EC) No 2051/97 [2] (hereinafter "the Regulation").
(3) The expenditure which Belgium and Portugal have incurred, and which is taken into account in this Decision, relates directly to the matters specified in Article 23(2)(a) and (b) of the Directive.
(4) The technical information provided for by Belgium and Portugal has enabled the Commission to analyse the situation accurately and comprehensively; the information has also been considered by the Standing Committee on Plant Health. The Commission has concluded that the conditions for the granting of a financial contribution in Article 23 of the Directive have been met.
(5) Accordingly, it is appropriate to provide a Community financial contribution to cover the expenditure on these programmes.
(6) The Community financial contribution may cover up to 50 % of eligible expenditure. Excluding the programme for which degression has to be applied in accordance with the third subparagraph of Article 23(5) of the Directive, the Community financial contribution for the purposes of this Decision should be 50 %.
(7) The programme notified by Portugal has already been the subject of Community contributions under Commission Decisions 2001/811/EC [3], 2002/889/EC [4] and 2003/787/EC [5]. An extension of the period in which eradication measures have to take place, as foreseen in the third subparagraph of Article 23(5) of the Directive, has been granted to this existing programme, as the examination of the situation has lead to the conclusion that the objective of these eradication measures is likely to be achieved within a reasonable period. The Community financial contribution for this programme, has been progressively reduced in accordance with the third subparagraph of Article 23(5).
(8) The contribution referred to in Article 2 of this Decision is without prejudice to further actions taken or to be taken and necessary for the achievement of the objective of eradication or control of the relevant harmful organisms.
(9) The present Decision is without prejudice to the outcome of the verification by the Commission under Article 24 of the Directive on whether the introduction of the relevant harmful organism has been caused by inadequate examinations or inspections and the consequences of such verification.
(10) In accordance with Article 3(2) of Council Regulation (EC) No 1258/1999 [6], veterinary and plant health measures undertaken in accordance with Community rules shall be financed under the Guarantee section of the European Agricultural Guidance and Guarantee Fund. Financial control of these measures comes under Articles 8 and 9 of the above Regulation, without prejudice to the provisions of Regulation (EC) No 1040/2002 and paragraphs 8 and 9 of Article 23 of the Directive.
(11) The measures provided in this Decision are in accordance with the opinion of the Standing Committee on Plant Health,
HAS ADOPTED THIS DECISION:
Article 1
The allocation of a Community financial contribution for 2004 to cover expenditure incurred by Belgium and Portugal relating directly to necessary measures as specified in Article 23(2) of Directive 2000/29/EC and taken for the purpose of combating the organisms concerned by the eradication programmes listed in the Annex to this Decision, is hereby approved.
Article 2
1. The total amount of the financial contribution referred to in Article 1 is EUR 576549.
2. The maximum amounts of the Community financial contribution for each eradication programme and for each year of implementation of the eradication programme shall be as indicated in the Annex to this Decision.
3. The resulting maximum Community financial contribution for the concerned Member States shall be as follows:
- EUR 210485 to Belgium
- EUR 366064 to Portugal.
Article 3
Subject to the verifications by the Commission under Article 24 of Directive 2000/29/EC, the Community financial contribution as set out in the Annex shall be paid only when:
(a) evidence of the measures taken has been given to the Commission through appropriate documentation, in accordance with the provisions laid down in the Regulation, in particular Article 1(2) and Article 2 thereof;
(b) a request for payment of the Community financial contribution has been submitted by the Member State concerned to the Commission, in accordance with the provisions laid down in Article 5 of Regulation (EC) No 1040/2002.
Article 4
This Decision is addressed to the Kingdom of Belgium and the Portuguese Republic.
Done at Brussels, 29 October 2004.
For the Commission
David Byrne
Member of the Commission
--------------------------------------------------
[1] OJ L 169, 10.7.2000, p. 1. Directive as last amended by Regulation (EC) No 882/2004 of the European Parliament and of the Council (OJ L 165, 30.4.2004, p. 1).
[2] OJ L 157, 15.6.2002, p. 38.
[3] OJ L 306, 23.11.2001, p. 25.
[4] OJ L 311, 14.11.2002, p. 16.
[5] OJ L 293, 11.11.2003, p. 13.
[6] OJ L 160, 26.6.1999, p. 103.
--------------------------------------------------
+++++ ANNEX 1 +++++
