COMMISSION DECISION of 24 September 1996 amending Commission Decisions 91/270/EEC and 92/471/EEC on the importation of embryos of domestic animals of the bovine species from Argentina (Text with EEA relevance) (96/572/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 89/556/EEC of 25 September 1989 on animal health conditions governing intra-Community trade in, and importation from third countries of embryos of domestic animals of the bovine species (1) as last amended by Directive 94/113/EC (2) and in particular Articles 7, 9 and 10 thereof,
Whereas a list of third countries from which Member States authorize importation of embryos of domestic animals of the bovine species appears in Commission Decision 91/270/EEC (3) as amended by Decision 94/453/EC (4);
Whereas the animal health conditions and veterinary certification for importation of bovine embryos from third countries are laid down in Commission Decision 92/471/EEC (5) as last amended by Decision 94/453/EC;
Whereas the competent authorities of Argentina have undertaken to notify the Commission and the Member States by telex or fax within 24 hours of the confirmation of the occurrence of any of the following diseases: rinderpest, foot-and-mouth disease, contagious bovine pleuropneumonia, bluetongue, epizootic haemorrhagic disease, Rift Valley fever and contagious vesicular stomatitis or changes in the policy of vaccination against them;
Whereas the animal health situation in Argentina is satisfactory from the point of view of imports of bovine embryos;
Whereas the veterinary services in that country are well-structured and organized and whereas the guarantees as to compliance with the rules laid down in Directive 89/556/EEC were given by the competent authorities of that country;
Whereas the competent authorities of Argentina have undertaken to ensure that the embryos have been collected or produced and processed by approved and supervised embryo collection or production teams, that they have, as appropriate, been obtained from animals of satisfactory health status, that they have been stored and transported in accordance with the rules which preserve their health status and are accompanied during transport by an animal health certificate in order to ensure that this obligation has been fulfilled;
Whereas the list of third countries from which Member States authorize importation of embryos of domestic animals of the bovine species should be amended and the animal health conditions for importation of embryos from Argentina should be laid down;
Whereas the measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
To the list of countries appearing in the Annex to Decision 91/270/EEC, the following country is added:
'Argentina`.
Article 2
To the list of countries appearing in Annex A, Part II, to Decision 92/471/EEC, the following country is added:
'Argentina`.
Article 3
This Decision shall apply from the day following that of its publication in the Official Journal of the European Communities.
Article 4
This Decision is addressed to the Member States.
Done at Brussels, 24 September 1996.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ No L 302, 19. 10. 1989, p. 1.
(2) OJ No L 53, 24. 2. 1994, p. 23.
(3) OJ No L 134, 29. 5. 1991, p. 56.
(4) OJ No L 187, 22. 7. 1994, p. 11.
(5) OJ No L 270, 15. 9. 1992, p. 27.
