Commission Decision
of 13 June 2005
on Community cooperation with the Food and Agriculture Organisation with particular regard to activities carried out by the European Commission for the Control of Foot-and-Mouth Disease
(2005/436/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Decision 90/424/EEC of 26 June 1990 on expenditure in the veterinary field [1], and in particular Articles 12 and 13 thereof,
Whereas:
(1) In the context of major epidemics of foot-and-mouth disease (FMD) in the late 1950s both within the Community and in neighbouring countries, the European Commission for the Control of Foot-and-Mouth Disease (EUFMD) was founded within the framework of the Food and Agriculture Organisation (FAO) of the United Nations Organisation.
(2) In the 1960s, due to increased threats from introduction of exotic strains of FMD into Europe, the member countries of the EUFMD were called to establish a Trust Fund aimed at emergency measures to be carried out in the Balkans, the main entrance route of the disease. Later that fund was divided into Trust Fund 911100MTF/003/EEC supported by those member countries that were at the same time Member States of the Community and Trust Fund 909700MTF/004/MUL supported by member countries of EUFMD which at that time were not or are not Member States of the Community.
(3) In accordance with Article 4 of Council Directive 90/423/EEC of 26 June 1990 amending Directive 85/511/EEC introducing Community measures for the control of foot-and-mouth disease, Directive 64/432/EEC on animal health problems affecting intra- Community trade in bovine animals and swine and Directive 72/462/EEC on health and veterinary inspection problems upon importation of bovine animals and swine and fresh meat or meat products from third countries [2], prophylactic vaccination against FMD ceased throughout the Community in 1991.
(4) At the same time Decision 90/424/EEC specifically provided for the possibility to support measures for the control of foot-and-mouth disease in third countries, in particular with the view to protect areas at risk within the Community.
(5) By adopting Council Directive 2003/85/EC of 29 September 2003 on Community measures for the control of foot-and-mouth disease repealing Directive 85/511/EEC and Decisions 89/531/EEC and 91/665/EEC and amending Directive 92/46/EEC [3] the Member States reconfirmed the prohibition of prophylactic vaccination while extending the possibility for the use of emergency vaccination against FMD.
(6) A number of outbreaks of FMD reported since 1992 in particular in parts of the Community adjacent to endemically infected countries and a major epidemic in certain Member States in 2001 are calling for a high level of disease awareness and preparedness, including international cooperation.
(7) Moreover, in countries neighbouring the Member States outbreaks and in some cases severe epidemics have been recorded during the last years which are liable to threaten the health status of Community susceptible livestock.
(8) In the light of the emergence of new virus topotypes and regional deterioration of control measures, the Community, in close cooperation with the EUFMD and by using the Trust Fund 911100MTF/003/EEC, supported emergency vaccination campaigns in Turkey and in Transcaucasia.
(9) In accordance with Commission Decision 2001/300/EC of 30 March 2001 on Community cooperation with the Food and Agriculture Organisation with particular regard to activities carried out by the European Commission for the Control of Foot-and-Mouth Disease [4] the Commission concluded the "Implementing Agreement MTF/INT/003/EEC911100 (TFEU970089129) on EC Funded Permanent Activities carried out by the FAO European Commission for the Control of Foot-and-mouth Disease" which was successfully operated until 31 December 2004.
(10) The European Community and the United Nations signed on 29 November 2003 a new Financial and Administrative Framework Agreement, which provided the enabling environment for the Agreement between the Commission of the European Communities and the Food and Agriculture Organisation of the United Nations, signed on 17 July 2003.
(11) It appears appropriate to renew the Implementing Agreement between the two international organisations and, taking into account the enlarged Community, fix the Community contribution to the Trust Fund 911100MTF/INT/003/EEC at a maximum level of EUR 4.5 million for a period of four years. The budget of the Trust Fund for 2005 should be made up of the final balance of its funds on 25 January 2005 and a Community contribution to bring the amount to an equivalent in USD of EUR 2 million. Subsequently expenditure should be replenished by annual transfers.
(12) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS DECIDED AS FOLLOWS:
Article 1
1. The balance of Trust Fund 911100MTF/INT/003/EEC (TFEU 970089129) shall be struck at USD 55284 as laid down in the final report adopted by the 71st Session of the Executive Committee of the European Commission for the Control of Foot-and-mouth Disease (EUFMD) on 25 January 2005 in Rome.
2. As from 1 January 2005 the financial obligation of the Community to the Fund referred to in paragraph 1 shall be set at a maximum of EUR 4500000 for a period of four years.
3. The first instalment of the amount referred to in paragraph 2 for the year 2005 shall be made up of:
(a) the balance referred to in paragraph 1, and
(b) a Community contribution of the amount necessary to bring the total to an equivalent in USD of EUR 2000000.
4. Expenditure incurred by the Trust Fund during the years 2005, 2006, 2007 and 2008 shall be replenished by annual Community contributions payable in 2006, 2007, 2008 and 2009 respectively. However, these transfers shall be subject to the existence of available funds in the Budget of the Commission.
5. The annual Community contributions referred to in paragraph 4 shall be based on the financial report produced by the EUFMD to either the annual Session of the Executive Committee or the biannual General Session of EUFMD, supported by detailed documentation in accordance with the rules of the Food and Agriculture Organisation.
Article 2
1. An Implementing Agreement on the use and operation of the Trust Fund 911100MTF/INT/003/EEC (TFEU 970089129) shall be concluded between the Commission of the European Communities and the Food and Agriculture Organisation of the United Nations Organisation for the period of four years, starting on 1 January 2005.
2. The Trust Fund referred to in Article 1 shall be operated in agreement between the Commission and the EUFMD in accordance with the Implementing Agreement referred to in paragraph 1 of this Article.
Done at Brussels, 13 June 2005.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 224, 18.8.1990, p. 19. Decision as last amended by Regulation (EC) No 806/2003 (OJ L 122, 16.5.2003, p. 1).
[2] OJ L 224, 18.8.1990, p. 13.
[3] OJ L 306, 22.11.2003, p. 1.
[4] OJ L 102, 12.4.2001, p. 71. Decision amended by Decision 2002/953/EC (OJ L 330, 6.12.2002, p. 39).
--------------------------------------------------
