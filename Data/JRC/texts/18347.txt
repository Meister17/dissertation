COMMISSION DIRECTIVE of 2 May 1980 authorizing, in certain cases, the marketing of compound feedingstuffs in unsealed packages or containers (80/511/EEC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Directive 79/373/EEC of 21 April 1979 on the marketing of compound feedingstuffs for animals (1), and in particular Article 4 thereof,
Whereas under the provisions of the abovementioned Directive compound feedingstuffs must generally be marketed in sealed packages or containers ; whereas, however, derogations from this rule can be granted at Community level;
Whereas the laws at present in force in Member States provide certain exemptions from the obligation to market compound feedingstuffs in sealed packages or containers ; whereas, therefore, in order to facilitate implementation of the provisions of Directive 73/373/EEC derogations which appear justified for practical or economic reasons should be granted at Community level;
Whereas, to ensure that the identity and the quality of compound feedingstuffs conform with the labelling provisions in force, it is necessary to distinguish those cases where, by derogation from present provisions, compound feedingstuffs may be marketed in bulk or in unsealed containers from those cases where they may be marketed either in bulk or in unsealed packages or containers;
Whereas the measures provided for in this Directive are in accordance with the opinion of the Standing Committee for Feedingstuffs,
HAS ADOPTED THIS DIRECTIVE:
Article 1
1. The Member States shall prescribe that compound feedingstuffs may be marketed in bulk or in unsealed packages or containers in case of: (a) deliveries between producers of compound feedingstuffs;
(b) deliveries from producers of compound feedingstuffs to packaging firms;
(c) compound feedingstuffs obtained by mixing grain or whole fruit;
(d) blocks or licks;
(e) small quantities of compound feedingstuffs not exceeding 50 kilograms in weight which are intended for the final user and are taken directly from a package or container which before opening complied with the provisions of Article 4 (1) of Directive 79/373/EEC.
2. The Member States shall prescribe that compound feedingstuffs may be marketed in bulk or in unsealed containers, but not in unsealed packages, in the case of: (a) compound feedingstuffs delivered directly from the producer to the final user;
(b) molassed feedingstuffs consisiting of not more than three ingredients;
(c) pelleted feedingstuffs.
Article 2
The Member States shall bring into force the necessary laws, regulations and administrative provisions to enable them to comply with this Directive with effect from 1 January 1981 and shall forthwith inform the Commission thereof.
Article 3
This Directive is addressed to the Member States.
Done at Brussels, 2 May 1980.
For the Commission
Finn GUNDELACH
Vice-President (1)OJ No L 86, 6.4.1979, p. 30.
