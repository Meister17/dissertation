COMMISSION REGULATION (EEC) No 2399/91 of 6 August 1991 concerning the classification of certain goods in the combined nomenclature
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 2658/87 of 23 July 1987 (1) on the tariff and statistical nomenclature and on the Common Customs Tariff, as last amended by Commission Regulation (EEC) No 2242/91 (2), and in particular Article 9,
Whereas in order to ensure uniform application of the combined nomenclature annexed to the said Regulation, it is necessary to adopt measures concerning the classification of the goods referred to in the Annex to this Regulation;
Whereas Regulation (EEC) No 2658/87 has set down the general rules for the interpretation of the combined nomenclature and these rules also apply to any other nomenclature which is wholly or partly based on it or which adds any additional subdivisions to it and which is established by specific Community provisions, with a view to the application of tariff or other measures relating to trade in goods;
Whereas, pursuant to the said general rules, the goods described in column 1 of the table annexed to the present Regulation must be classified under the appropriate CN codes indicated in column 2, by virtue of the reasons set out in column 3;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the nomenclature Committee,
HAS ADOPTED THIS REGULATION:
Article 1
The goods described in column 1 of the annexed table are now classified within the combined nomenclature under the appropriate CN codes indicated in column 2 of the said table.
Article 2
This Regulation shall enter into force on the 21st day after its publication in the Official Journal of the European Communities. This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 6 August 1991. For the Commission
Jean DONDELINGER
Member of the Commission
(1) OJ No L 256, 7. 9. 1987, p. 1. (2) OJ No L 204, 27. 7. 1991, p. 21.
ANNEX
Description of goods Classification CN Code Reasons (1) (2) (3) 1. Preparation in powder form for the manufacture of confectionery.
Composition:
84,4 % by weight dehydrated egg white (total protein content: 69,1 % by weight),
14,6 % by weight maltodextrin (expressed in starch: 9,3 % by weight),
1 % by weight gelatine. 2106 90 99 Classification is determined by the provisions of General Rules 1 and 6 for the interpretation of the Combined Nomenclature, by additional note 1 to chapter 21 and by the texts of CN codes 2106, 2106 90 and 2106 90 99.
The product is not a protein concentrate of code 2106 10 90. 2. Artificial iron oxide containing approximately 95 % Fe2O3 and approximately 4 % alumina and silica, resulting from the manufacturing process. 2821 10 00 Classification is determined by the provisions of General Rules 1 and 6 for the interpretation of the Combined Nomenclature, note 1 a) to Chapter 28 and by the texts of CN codes 2821 and 2821 10 00 (see also the Explanatory Notes to the HS, heading 28.21, part A).
