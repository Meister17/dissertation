Euro exchange rates [1]
11 July 2006
(2006/C 161/01)
| Currency | Exchange rate |
USD | US dollar | 1,2736 |
JPY | Japanese yen | 145,69 |
DKK | Danish krone | 7,4594 |
GBP | Pound sterling | 0,69225 |
SEK | Swedish krona | 9,1891 |
CHF | Swiss franc | 1,5665 |
ISK | Iceland króna | 94,78 |
NOK | Norwegian krone | 7,9905 |
BGN | Bulgarian lev | 1,9558 |
CYP | Cyprus pound | 0,5750 |
CZK | Czech koruna | 28,428 |
EEK | Estonian kroon | 15,6466 |
HUF | Hungarian forint | 276,52 |
LTL | Lithuanian litas | 3,4528 |
LVL | Latvian lats | 0,6959 |
MTL | Maltese lira | 0,4293 |
PLN | Polish zloty | 4,0243 |
RON | Romanian leu | 3,5640 |
SIT | Slovenian tolar | 239,64 |
SKK | Slovak koruna | 38,410 |
TRY | Turkish lira | 1,9675 |
AUD | Australian dollar | 1,6945 |
CAD | Canadian dollar | 1,4357 |
HKD | Hong Kong dollar | 9,9002 |
NZD | New Zealand dollar | 2,0724 |
SGD | Singapore dollar | 2,0111 |
KRW | South Korean won | 1205,78 |
ZAR | South African rand | 9,0751 |
CNY | Chinese yuan renminbi | 10,1792 |
HRK | Croatian kuna | 7,2525 |
IDR | Indonesian rupiah | 11519,71 |
MYR | Malaysian ringgit | 4,651 |
PHP | Philippine peso | 66,552 |
RUB | Russian rouble | 34,2580 |
THB | Thai baht | 48,182 |
[1] Source: reference exchange rate published by the ECB.
--------------------------------------------------
