Corrigendum to Commission Regulation (EC) No 742/2006 of 17 May 2006 adapting certain fish quotas for 2006 pursuant to Council Regulation (EC) No 847/96 introducing additional conditions for year-to-year management of TACs and quotas
(Official Journal of the European Union L 130 of 18 May 2006)
Annex I, on page 10, the first four lines of the table should read as follows:
"Country identity | Stock Id | Species | Zone | Adapted quantity 2005 | Catches 2005 | % Adapted quantity | Transfers 2006 | Initial quantity 2006 | Revised quantity 2006 | New code |
DEU | WHB/571214 | Blue whiting | V, VI, VII, XII, XIV | 41847 | 20173,8 | 48,0 | 4184,7 | | | |
DEU | WHB/8ABDE. | Blue whiting | VIIIa, b, d, e | 2000 | | 0,0 | 200 | | | |
DEU | WHB/1X14 | Blue whiting | I, II, III, IV, V, VI, VII, VIIIa,b,d,e, XII, XIV (EC and international waters) | 4385 | 20424 | 24809 | 1X14" |
--------------------------------------------------
