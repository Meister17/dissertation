Commission Regulation (EC) No 1072/2000
of 19 May 2000
amending Regulation (EEC) No 1538/91 introducing detailed rules for implementing Regulation (EEC) No 1906/90 on certain marketing standards for poultrymeat
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 1906/90 of 26 June 1990 on certain marketing standards for poultrymeat(1), as last amended by Regulation (EC) No 1101/98(2), and in particular Articles 7 and 9 thereof,
Whereas:
(1) Commission Regulation (EEC) No 1538/91(3), as last amended by Regulation (EC) No 1000/96(4), lays down the detailed rules for implementing marketing standards in the poultrymeat sector.
(2) By Regulation (EC) No 1101/98 the scope of Regulation (EEC) No 1906/90 was extended to cover the control of water content of poultry cuts. It is therefore necessary to lay down the detailed provisions for such controls which are similar to those for whole frozen and quick frozen carcases and which include the list of products concerned and the appropriate method of control.
(3) The provisions on water control should also be adapted regarding national measures for checks at all stages of marketing and to update the list of reference laboratories.
(4) The age of slaughter for young geese, in which the sternum is not yet ossified must be laid down in the context of the indication of particular types of farming.
(5) The measures provided for in this Regulation are in accordance with the opinion of the Committee for Poultrymeat and Eggs,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EEC) No 1538/91 is amended as follows:
1. The following point (n) is added in Article 1(2):
"(n) deboned turkey leg meat: turkey thighs and/or drumsticks, deboned, i.e. without femur, tibia and fibula, whole, diced or cut into strips."
2. Article 14(a) is amended as follows:
- in paragraph 3 first subparagraph, "four" is replaced by "eight",
- paragraph 13 is replaced by the following:
"13. The Member States shall adopt the practical measures for the checks provided for in this Article at all stages of marketing including checks of imports from third countries at the time of customs clearance in accordance with Annexes V and VI. They shall inform the other Member States and the Commission before 1 September 2000 of these measures. Any relevant changes shall be communicated immediately to the other Member States and to the Commission."
3. The following Article 14(b) is inserted:
"Article 14b
1. The following fresh, frozen and quick-frozen poultry cuts may be marketed by way of business or trade within the Community only if the water content does not exceed the technically unavoidable values determined by the method of analysis described in Annex VIa (chemical method):
(a) chicken breast fillet, with or without wishbone, without skin;
(b) chicken breast, with skin;
(c) chicken thighs, drumsticks, legs, legs with a portion of the back, leg-quarters, with skin;
(d) turkey breast fillet, without skin;
(e) turkey breast, with skin;
(f) turkey thighs, drumsticks, legs, with skin;
(g) deboned turkey leg meat, without skin.
2. The competent authorities designated by each Member State shall ensure that the slaughterhouses and cutting plants whether or not attached to slaughterhouses adopt all measures necessary to comply with the provisions of paragraph 1 and in particular that:
- regular checks on water absorbed are carried out in the slaughterhouses in accordance with Article 14a(3) also for chicken and turkey carcases intended for the production of the fresh, frozen and quick-frozen cuts listed in paragraph 1. These checks shall be carried out at least once each working period of eight hours. The limit values fixed in Annex VII(9) shall also apply for turkey carcases,
- results of the checks are recorded and kept for a period of one year,
- each batch is marked in such a way that its date of production can be identified; this batch mark must appear on the production record.
3. At least once every three months checks on the water content referred to in paragraph 1 shall be carried out, by sampling, on frozen and quick-frozen poultry cuts from each cutting-plant producing such cuts, according to Annex VIa. These checks shall not be conducted for poultry cuts in respect of which proof is provided to the satisfaction of the competent authority that they are intended exclusively for export.
After one year of satisfactory testing in a particular cutting plant, the frequency of tests shall be reduced to once every six months. Any failure to comply with the criteria laid down in Annex VIa thereafter shall result in reinstatement of checks at least every three months for a period of not less than two years before the reduced frequency can be applied again.
4. Paragraphs 5 to 13 of Article 14a shall apply, mutatis mutandis, for poultry cuts referred to in paragraph 1."
4. In Annex IV the following is added in relation to minimum age of slaughter:
- under (b) "extensive indoor":
- "young geese (goslings): 60 days or later",
- under (d) "traditional free range":
- "60 days for young geese (goslings)".
5. The Annex to this Regulation is inserted as Annex VIa.
6. Annex VII is amended as follows:
- point 1 is replaced by the following:
"1. At least once each working period of eight hours:
select at random 25 carcases from the evisceration line immediately after evisceration and the removal of the offal and fat and before the first subsequent washing",
- the following point 8a is inserted:
"8a Instead of manual weighing as described under points 1 to 8 automatic weighing lines may be used for the determination of the percentage moisture absorption for the same number of carcases and according to the same principles, provided that the automatic weighing line is approved in advance for this purpose by the competent authority."
7. In Annex VIII, the addresses of the following reference laboratories are changed to read as indicated hereafter:COMMUNITY REFERENCE LABORATORY:
ID/Lelystad Postbus 65 Edelhertweg 15 8200 AB Lelystad The Netherlands
BELGIUM
Faculteit Diergeneeskunde Vakgroep "Diergeneeskundig toezicht op eetwaren"
Universiteit Gent
Salisburylaan 133 B - 9820 Merelbeke
GREECE
Ministry of Agriculture Veterinary Laboratory of Patra 15, Notara Street GR - 264 42 Patra
ITALY
Ispettorato Centrale Repressione Frodi Via Jacopo Cavedone n.29 I - 41100 Modena
NETHERLANDS
ID/Lelystad Postbus 65 Edelhertweg 15 8200 AB Lelystad
UNITED KINGDOM
CSL Food Science Laboratory Sand Hutton York Y04 1LZ United Kingdom
AUSTRIA
Bundesamt und Forschungszentrum für Landwirtschaft Spargelfeldstr. 191 A - 1220 Wien.
Article 2
This Regulation shall enter into force on the seventhieth day following its publication in the Official Journal of the European Communities. It shall be applicable from 1 July 2000. However, points 2, 3 and 5 of Article 1 shall be applicable from 1 September 2000.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 19 May 2000.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 173, 6.7.1990, p. 1.
(2) OJ L 157, 30.5.1998, p. 12.
(3) OJ L 143, 7.6.1991, p. 11.
(4) OJ L 134, 5.6.1996, p. 9.
ANNEX
"ANNEX VIa
DETERMINATION OF THE TOTAL WATER CONTENT OF POULTRY CUTS
(Chemical test)
1. Object and scope
This method shall be used to determine the total water content of certain poultry cuts. The method shall involve determination of the water and protein contents of samples from the homogenised poultry cuts. The total water content as determined shall be compared with the limit value given by the formulae indicated in paragraph 6.4, to determine whether or not excess water has been taken up during processing. If the analyst suspects the presence of any substance which may interfere with the assessment, it shall be for him or her to take the necessary appropriate precautions.
2. Definitions and sampling procedures
The definitions given in Article 1(2) are applicable to the poultry cuts referred to in Article 14b. The sample sizes should be at least as follows:
chicken breast, chicken breast fillet: half of the (deboned) breast
turkey breast, turkey breast fillet and deboned leg meat: portions of about 100 g
other cuts: as defined in Article 1(2).
In the case of frozen or quick frozen bulk products (cuts not individually packed) the large packs from which samples are to be taken may be kept at 0 °C until individual cuts can be removed.
3. Principle
Water and protein contents shall be determined in accordance with recognised ISO (International Organisation for Standardisation) methods or other methods or analysis approved by the Council.
The highest permissible total water content of the poultry cuts will be estimated from the protein content of the cuts, which can be related to the physiological water content.
4. Apparatus and reagents
4.1. Scales for weighing the cuts and wrappings, capable of weighing with an accuracy better than +- 1 g.
4.2. Meat axe or saw for cutting cuts into pieces of appropriate size for the mincer.
4.3. Heavy-duty mincing machine and blender capable of homogenising poultry cuts or parts thereof.
Note:
No special mincer shall be recommended. It should have sufficient power to mince also frozen or quick-frozen meat and bones to produce a homogeneous mixture corresponding to that obtained from a mincer fitted with a 4 mm hole disc.
4.4. Apparatus as specified in ISO 1442, for the determination of water content.
4.5. Apparatus as specified in ISO 937, for the determination of protein content.
5. Procedure
5.1. Five cuts shall be taken at random from the quantity of poultry cuts to be checked and in each case kept frozen or refrigerated as the case may be until analysis in accordance with points 5.2 to 5.6 begins.
Samples from frozen or quick-frozen bulk products referred to under point 2 may be kept at 0 °C until analysis begins.
It may be conducted either as an analysis of each of the five cuts, or as an analysis of a composite sample of the five cuts.
5.2. The preparation shall be commenced within the hour following the removal of the cuts from the freezer or refrigerator.
5.3. (a) The outside of the pack shall be wiped to remove superficial ice and water. Each cut shall be weighed and removed from any wrapping material. After cutting up the cuts into smaller pieces, the weight of the poultry cut shall be determined to the nearest gram after deduction of the weight of any wrapping material removed to give "P1".
(b) In the case of a composite sample analysis, the total weight of the five cuts, prepared in accordance with 5.3(a), shall be determined to give "P5".
5.4. (a) The whole cut of which the weight is P1, shall be minced in a mincer as specified under point 4.3 (and, if necessary, mixed with the use of a blender as well) to obtain a homogeneous material from which a sample representative of each cut may then be taken.
(b) In the case of a composite sample analysis, all five cuts of which the weight is P5 shall be minced in a mincer as specified under point 4.3 (and, if necessary, mixed with the use of a blender as well) to obtain a homogeneous material from which two samples representative of the five cuts may then be taken.
The two samples are to be analysed as described in points 5.5 and 5.6.
5.5. A sample of the homogenised material shall be taken and used immediately to determine the water content in accordance with ISO 1442 to give the water content "a %".
5.6. A sample of the homogenised material shall also be taken and used immediately to determine the nitrogen content in accordance with ISO 937. This nitrogen content shall be converted to crude protein content "b %" by multiplying it by the factor 6,25.
6. Calculation of results
6.1. (a) The weight of water (W) in each cut shall be given by aP1/100 and the weight of protein (RP) by bP1/100, both of which are to be expressed in grams.
The sums of the weights of water (W5) and the weights of protein (RP5) in the five cuts analysed shall be determined.
(b) In the case of a composite sample analysis, the average content of water and protein from the two samples analysed shall be determined to give a % and b %, respectively. The weight of the water (W5) in the five cuts shall be given by aP5/100, and the weight of protein (RP5) by bP5/100, both of which are to be expressed in grams.
6.2. The average weight of water (WA) and protein (RPA) shall be calculated by dividing W5 and RP5 respectively, by five.
6.3. The mean physiological W/RP ratio as determined by this method is as follows:
- chicken breast fillet: 3,19 +- 0,12
- chicken legs and leg quarters: 3,78 +- 0,19
- turkey breast fillet: 3,05 +- 0,15
- turkey legs: 3,58 +- 0,15
- deboned turkey leg meat: 3,65 +- 0,17.
6.4. Assuming that the minimum technically unavoidable water content absorbed during preparation amounts to 2 %, 4 % or 6 %(1) depending on the type of products and chilling methods applied, the highest permissible W/RP ratio as determined by this method shall be as follows:
>TABLE>
If the average WA/RPA ratio of the five cuts as calculated from the values under point 6.2 does not exceed the ratio given in point 6.4, the quantity of poultry cuts subjected to the check shall be considered up to standard.
(1) Calculated on the basis of the cut, exclusive of absorbed extraneous water. For (skinless) fillet and deboned turkey leg meat, the percentage is 2 % for each of the chilling methods."
