Euro exchange rates [1]
7 September 2006
(2006/C 217/01)
| Currency | Exchange rate |
USD | US dollar | 1,2731 |
JPY | Japanese yen | 148,05 |
DKK | Danish krone | 7,4608 |
GBP | Pound sterling | 0,67955 |
SEK | Swedish krona | 9,3285 |
CHF | Swiss franc | 1,5822 |
ISK | Iceland króna | 89,47 |
NOK | Norwegian krone | 8,1875 |
BGN | Bulgarian lev | 1,9558 |
CYP | Cyprus pound | 0,5763 |
CZK | Czech koruna | 28,287 |
EEK | Estonian kroon | 15,6466 |
HUF | Hungarian forint | 276,43 |
LTL | Lithuanian litas | 3,4528 |
LVL | Latvian lats | 0,6961 |
MTL | Maltese lira | 0,4293 |
PLN | Polish zloty | 3,9766 |
RON | Romanian leu | 3,5279 |
SIT | Slovenian tolar | 239,59 |
SKK | Slovak koruna | 37,575 |
TRY | Turkish lira | 1,8825 |
AUD | Australian dollar | 1,6728 |
CAD | Canadian dollar | 1,4066 |
HKD | Hong Kong dollar | 9,9006 |
NZD | New Zealand dollar | 1,9746 |
SGD | Singapore dollar | 1,9975 |
KRW | South Korean won | 1217,53 |
ZAR | South African rand | 9,4086 |
CNY | Chinese yuan renminbi | 10,1097 |
HRK | Croatian kuna | 7,3550 |
IDR | Indonesian rupiah | 11610,67 |
MYR | Malaysian ringgit | 4,644 |
PHP | Philippine peso | 63,986 |
RUB | Russian rouble | 34,0450 |
THB | Thai baht | 47,548 |
[1] Source: reference exchange rate published by the ECB.
--------------------------------------------------
