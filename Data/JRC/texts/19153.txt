Commission Regulation (EC) No 761/2003
of 30 April 2003
approving operations to check conformity to the marketing standards applicable to fresh fruit and vegetables carried out in India prior to import into the Community
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 2200/96 of 28 October 1996 on the common organisation of the market in fruit and vegetables(1), as last amended by Commission Regulation (EC) No 47/2003(2), and in particular Article 10 thereof,
Whereas:
(1) Article 7 of Commission Regulation (EC) No 1148/2001 of 12 June 2001 on checks on conformity to the marketing standards applicable to fresh fruit and vegetables(3), as last amended by Regulation (EC) No 408/2003(4), lays down the conditions for the approval of checking operations performed by certain third countries which so request prior to import into the Community.
(2) On 31 December 2001, the Indian authorities sent the Commission a request for the approval of checking operations performed by the Directorate of Marketing and Inspection (DMI) under the responsibility of the Agricultural Marketing Adviser (AMA) of the Indian Ministry of Agriculture. That request states that DMI has the necessary staff, equipment and facilities to carry out checks, that it uses methods equivalent to those referred to in Article 9 of Regulation (EC) No 1148/2001 and that the fresh fruit and vegetables exported from India to the Community meet either the Community marketing standards or standards at least equivalent.
(3) The information sent by the Member States to the Commission shows that, in the period 1997 to 2000, the incidence of non-conformity with marketing standards among imports of fresh fruit and vegetables from India was relatively low.
(4) Representatives of the Indian authorities have participated in international efforts to agree marketing standards for fruit and vegetables within the Working Party on Standardisation of Perishable Produce and Quality Development of the United Nations Economic Commission for Europe (UNECE).
(5) Checks on conformity carried out by India should therefore be approved with effect from the date of implementation of the administrative cooperation procedure provided for in Article 7(8) of Regulation (EC) No 1148/2001.
(6) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Fresh Fruit and Vegetables,
HAS ADOPTED THIS REGULATION:
Article 1
Checks on conformity to the marketing standards applicable to fresh fruit and vegetables carried out by India prior to import into the Community are hereby approved in accordance with Article 7 of Regulation (EC) No 1148/2001.
Article 2
Details of the official authority and inspection body in India, as referred to in the second subparagraph of Article 7(2) of Regulation (EC) No 1148/2001, are given in Annex I to this Regulation.
Article 3
The certificates referred to in the second subparagraph of Article 7(3) of Regulation (EC) No 1148/2001, issued following the checks referred to in Article 1 of this Regulation, must be drawn up on forms in conformity with the model set out in Annex II to this Regulation.
Article 4
This Regulation shall enter into force on the 20th day following that of its publication in the Official Journal of the European Union.
It shall apply from the date of publication in the C series of the Official Journal of the European Union of the notice referred to in Article 7(8) of Regulation (EC) No 1148/2001, relating to the establishment of administrative cooperation between the Community and India.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 30 April 2003.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 297, 21.11.1996, p. 1.
(2) OJ L 7, 11.1.2003, p. 64.
(3) OJ L 156, 13.6.2001, p. 9.
(4) OJ L 62, 6.3.2002, p. 8.
ANNEX I
- Official authority referred to in Article 7(2) of Regulation (EC) No 1148/2001:
Agricultural Marketing Adviser Ministry of Agriculture, Govt. of India NH-IV, Faridabad India Tel. (91-129) 241 65 68, 241 57 10; (91-11) 23 01 34 45 Fax (91-129) 241 65 68; (91-11) 23 01 34 45 E-mail: pkagarwall123@hotmail.com
- Inspection body referred to in Article 7(2) of Regulation (EC) No 1148/2001:
Directorate of Marketing and Inspection (DMI)
Department of Agriculture and Cooperation
Ministry of Agriculture, Govt. of India NH-IV, Faridabad India Tel. (91-129) 241 65 68, 241 57 10 Fax (91-129) 241 65 68 E-mail: dmifbd@agmark.nic.in
ANNEX II
Model certificate referred to in Article 7(3) of Regulation (EC) No 1148/2001
>PIC FILE= "L_2003109EN.000902.TIF">
