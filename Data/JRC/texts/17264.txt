Regulation (EC) No 868/2004 of the European Parliament and of the Council
of 21 April 2004
concerning protection against subsidisation and unfair pricing practices causing injury to Community air carriers in the supply of air services from countries not members of the European Community
THE EUROPEAN PARLIAMENT AND THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 80(2) thereof,
Having regard to the proposal from the Commission(1),
Having regard to the opinion of the European Economic and Social Committee(2),
Acting in accordance with the procedure laid down in Article 251 of the Treaty(3),
Whereas:
(1) The competitive position of Community air carriers when providing air services to, via or from the Community could be adversely affected by unfair and discriminatory practices of non-Community air carriers providing like air services.
(2) Such unfair and discriminatory practices may result from subsidisation or other forms of aid granted by a government or regional body or other public organisation of a country not being a member of the Community or from certain pricing practices by a non-Community air carrier which benefit from non-commercial advantages.
(3) It is necessary to define the redressive measures to be taken against such unfair practices.
(4) Within the Community there are strict rules regarding the granting of State aid to air carriers, and for Community air carriers not to be placed at a competitive disadvantage and suffer injury there is a need for an instrument to offer protection against non-Community air carriers who are subsidised or receive other benefits from governments.
(5) This Regulation is not intended to replace air services agreements with third countries that can be used to deal effectively with practices covered by this Regulation; where a legal instrument exists at Member State level which would enable a satisfactory response to be made within a reasonable period of time, that instrument would take precedence over this Regulation for that period.
(6) The Community should be able to take action to redress such unfair practices resulting from subsidies granted by the government of a country which is not a member of the Community; the Community should also be able to address unfair pricing practices.
(7) It should be determined when a subsidy shall be deemed to exist and according to which principles it should be countervailable, in particular whether the subsidy has been targeted at certain enterprises or sectors or is contingent upon air service supply to third countries.
(8) In determining the existence of a subsidy, it is necessary to demonstrate that there has been a financial contribution by a government or regional body or other public organisation via a transfer of funds or that debts of any kind representing government revenue that are otherwise due are foregone or not collected, and that a benefit has thereby been conferred on the recipient enterprise.
(9) It should be determined when an unfair pricing practice shall be deemed to exist; an examination of the pricing practices of a third-country air carrier should be restricted to those limited number of cases where the air carrier is benefiting from a non-commercial advantage which cannot be clearly identified as a subsidy.
(10) It should be made clear that an unfair pricing practice can be deemed to exist only in cases where that practice is clearly distinguishable from normal competitive pricing practices; the Commission should develop a detailed methodology for determining the existence of unfair pricing practices.
(11) It is furthermore desirable to lay down clear and detailed guidance as to the factors which may be relevant for the determination of whether the subsidised or unfairly priced air services provided by non-Community air carriers have caused injury or are threatening to cause injury; in order to demonstrate that the pricing practices related to the supply of such air services cause injury to the Community industry, attention should be given to the effect of other factors, and consideration should be given to all relevant and known factors and economic indicators which have a bearing on the state of the industry, and in particular prevailing market conditions in the Community.
(12) It is essential to define the terms "Community air carrier", "Community industry", and "like air service".
(13) It is necessary to specify who may lodge a complaint and the information that such a complaint should contain; a complaint should be rejected where there is insufficient evidence of injury to proceed.
(14) It is desirable to lay down the procedure to be followed in the investigation of unfair practices by non-Community carriers; this procedure should be limited in time.
(15) It is necessary to lay down the manner in which interested parties should be given notice of the information which the authorities require; interested parties should have ample opportunity to present all relevant evidence and to defend their interests; it is also necessary to set out the rules and procedures to be followed during the investigation, in particular the rules whereby interested parties are to make themselves known, present their views and submit information within specified time limits, if such views and information are to be taken into account; whilst respecting commercial confidentiality, it is necessary to allow interested parties access to all information pertaining to the investigation which is relevant to the presentation of their case; it is necessary to provide that, where parties do not cooperate satisfactorily, other information may be used to establish findings and that such information may be less favourable to the parties than if they had cooperated.
(16) It is necessary to lay down the conditions under which provisional measures may be imposed; such measures may in all cases be imposed by the Commission only for a six-month period.
(17) An investigation or proceeding should be terminated whenever there is no need to impose measures, for example if the amount of subsidisation, the degree of unfair pricing or the injury is negligible; a proceeding should not be terminated unless the termination decision is accompanied by a statement of the reasons therefor; those measures should be less than the amount of countervailable subsidies or the degree of unfair pricing, if such lesser amount would remove the injury.
(18) It is necessary to provide that the level of measures should not exceed the value of subsidies or the non-commercial advantages granted as the case may be or the sum corresponding to the injury caused, where this is lower.
(19) It is necessary to provide that measures should remain in force only for as long as it is necessary to counteract the subsidies or unfair pricing practices causing injury.
(20) Preference should be given to duties when it comes to the imposition of measures; where duties prove not to be appropriate, other measures may be considered.
(21) It is necessary to specify procedures for the acceptance of undertakings eliminating or offsetting the countervailable subsidies or unfair pricing practices and the injury caused in lieu of the imposition of provisional or definitive measures; it is also appropriate to lay down the consequences of breach or withdrawal of undertakings.
(22) It is necessary to provide for review of the measures imposed in cases where sufficient evidence is submitted of changed circumstances.
(23) The measures necessary for the implementation of this Regulation should be adopted in accordance with Council Decision 1999/468/EC of 28 June 1999 laying down the procedures for the exercise of implementing powers conferred on the Commission(4).
(24) The form and level of measures and their enforcement should be set out in detail in a Regulation imposing these measures.
(25) It is necessary to ensure that any measures taken by virtue of this Regulation are in full accordance with the Community interest; the assessment of the Community interest involves the identification of any compelling reasons which would lead to the clear conclusion that the taking of measures would not be in the overall interest of the Community. Such compelling reasons could, for example, include cases where the disadvantage to consumers or other interested parties would be clearly disproportionate to any advantages given to the Community industry by the imposition of measures.
(26) Since the objective of this Regulation, namely the protection against subsidisation and unfair pricing practices causing injury to the Community air carriers in the supply of air services from countries not members of the European Community may well not be sufficiently achieved by the Member States and can therefore be better achieved at Community level, the Community may adopt measures, in accordance with the principle of subsidiarity as set out in Article 5 of the Treaty. In accordance with the principle of proportionality, as set out in that Article, this Regulation does not go beyond what is necessary in order to achieve that objective,
HAVE ADOPTED THIS REGULATION:
Article 1
Objective
1. This Regulation lays down the procedure to be followed to provide protection against subsidisation and unfair pricing practices in the supply of air services from countries not members of the European Community in so far as injury is thereby caused to the Community industry.
2. This Regulation shall not preclude the prior application of any special provisions in air services agreements between Member States and countries not members of the European Community.
3. This Regulation shall not preclude the application of any special provisions in agreements between the Community and countries not members of the European Community.
Article 2
Principles
A redressive measure may be imposed for the purpose of offsetting:
1. a subsidy granted, directly or indirectly, to a non-Community air carrier; or
2. unfair pricing practices by non-Community air carriers;
concerning the supply of air services on one or more routes to and from the Community which cause injury to the Community industry.
Article 3
Definitions
For the purposes of this Regulation:
(a) "injury" shall mean material injury to the Community industry, or threat of material injury to the Community industry, determined in accordance with Article 6;
(b) "Community industry" shall mean the Community air carriers supplying like air services as a whole or those of them whose collective share constitutes a major proportion of the total Community supply of those services;
(c) "Community air carrier" shall mean an air carrier with a valid operating licence granted by a Member State in accordance with Council Regulation (EEC) No 2407/92 of 23 July 1992 on licensing of air carriers(5);
(d) "like air service" shall mean air services which are supplied on the same route or routes as the air services under consideration or such air services that are supplied on a route or routes closely resembling the route or routes on which the air service under consideration is supplied.
Article 4
Subsidisation
1. A subsidy shall be deemed to exist if:
(a) there is a financial contribution by a government or regional body or other public organisation of a country not a member of the European Community, that is to say, where:
(i) a practice of a government or regional body or other public organisation involves a direct transfer of funds such as grants, loans or equity infusion, potential direct transfer of funds to the company or the assumption of liabilities of the company such as loan guarantees;
(ii) revenue of a government or regional body or other public organisation that is otherwise due is foregone or not collected;
(iii) a government or regional body or other public organisation provides goods or services other than general infrastructure, or purchases goods or services;
(iv) a government or regional body or other public organisation makes payments to a funding mechanism or entrusts or directs a private body to carry out one or more of the type of functions illustrated under (i), (ii) and (iii) which would normally be vested in the government and, in practice, in no real sense differs from practices normally followed by governments;
(b) and a benefit is thereby conferred.
2. Subsidies shall be subject to redressive measures only if the subsidies are limited, in law or in fact, to an enterprise or industry or group of enterprises or industries within the jurisdiction of the granting authority.
Article 5
Unfair pricing practices
1. Unfair pricing practices shall be deemed to exist on a particular air service to or from the Community where non-Community air carriers:
- benefit from a non-commercial advantage, and
- charge air fares which are sufficiently below those offered by competing Community air carriers to cause injury.
These practices must be clearly distinguishable from normal competitive pricing practices.
2. When comparing airfares, account shall be taken of the following elements:
(a) the actual price at which tickets are offered for sale;
(b) the number of seats proposed at an allegedly unfair price out of the total number of seats available on the aircraft;
(c) the restrictions and conditions attached to the tickets sold at an allegedly unfair price;
(d) the level of service proposed by all carriers providing the like air service in question;
(e) the actual costs of the non-Community carrier providing the services, plus a reasonable margin of profit; and
(f) the situation, in terms of points (a) to (e), on comparable routes.
3. Acting in accordance with the procedure referred to in Article 15(3), the Commission shall develop a detailed methodology for determining the existence of unfair pricing practices. This methodology shall cover, inter alia, the manner in which normal competitive pricing, actual costs and reasonable profit margins shall be assessed in the specific context of the aviation sector.
Article 6
Determination of injury
1. The determination of injury shall be based on positive evidence and shall involve an objective examination of both:
(a) the level of fares of the air services under consideration and the effect of such air services on fares offered by Community air carriers; and
(b) the impact of those air services on the Community industry, as indicated by trends in a number of economic indicators such as number of flights, utilisation of capacity, passenger bookings, market share, profits, return on capital, investment, employment.
No one or more of these factors can necessarily give decisive guidance.
2. It shall be demonstrated, from all the positive evidence presented in relation to paragraph 1, that the air services under consideration are causing injury within the meaning of this Regulation.
3. Known factors other than the air services under consideration, which are injuring the Community industry at the same time shall also be examined to ensure that the injury caused by these other factors is not attributable to the air services under consideration.
4. A determination of threat of injury shall be based on facts and not merely on allegation, conjecture or remote possibility. The change in circumstance, which would create a situation in which the subsidy would cause injury, must be clearly foreseeable and imminent.
Article 7
Initiation of proceedings
1. An investigation under this Regulation shall be initiated upon the lodging of a written complaint on behalf of the Community industry by any natural or legal person or any association, or on the Commission's own initiative, if there is sufficient evidence of the existence of countervailable subsidies (including, if possible, of their amount) or unfair pricing practices within the meaning of this Regulation, injury and a causal link between the allegedly subsidised or unfairly priced air services and the alleged injury.
2. When it is apparent that there is sufficient evidence to initiate a proceeding, the Commission shall, acting in accordance with the procedure referred to in Article 15(2), initiate the proceeding within 45 days of the lodging of the complaint and shall publish a notice in the Official Journal of the European Union. Where the issue in question is being discussed within the framework of a bilateral agreement by the Member State concerned, this 45-day deadline shall, at the request of the Member State, be extended for up to 30 days. Any additional extension to the deadline shall be decided upon by the Commission acting in accordance with the procedure referred to in Article 15(2).
Where insufficient evidence has been presented, the Commission shall, acting in accordance with the procedure referred to in Article 15(2), inform the complainant within 45 days of the date on which the complaint was lodged.
3. The notice of initiation of the proceedings shall announce the initiation of an investigation, indicate the scope of the investigation, the air services on the routes concerned, the countries whose governments allegedly granted the subsidies or license the air carriers allegedly engaged in unfair pricing practices and the period within which interested parties may make themselves known, present their views in writing and submit information, if such views are to be taken into account during the investigation; the notice shall also state the period within which interested parties may apply to be heard by the Commission.
4. The Commission shall inform the air carriers supplying the air services under consideration, the government concerned and the complainants of the initiation of the proceedings.
5. The Commission may, at any time invite the third-country government concerned to take part in consultations with the aim of clarifying the situation as to the matters referred to in paragraph 2 and arriving at a mutually agreed solution. Where appropriate the Commission shall associate with these consultations any Member State concerned. In cases where consultations are already underway between a Member State and the third country government concerned, the Commission shall liaise with the said Member State in advance.
Article 8
The investigation
1. Following the initiation of the proceedings, the Commission shall commence an investigation which shall cover both subsidisation or unfair pricing practices of air services supplied by non-Community carriers on certain routes, and injury. This investigation shall be carried out expeditiously and shall normally be concluded within nine months of the initiation of the proceedings, except in the following circumstances, where it may be prolonged:
- negotiations with the third country government concerned have progressed to a point that a satisfactory resolution of the complaint appears imminent, or
- additional time is needed in order to achieve a resolution which is in the Community interest.
2. The interested parties which have made themselves known in accordance with the time limits set forth in the notice of initiation, shall be heard if they have made a request for a hearing showing that they are an interested party likely to be affected by the result of the proceeding and that there are particular reasons why they should be heard.
3. In cases in which an interested party refuses access to, or otherwise does not provide, necessary information within the appropriate time limits, or significantly impedes the investigation, provisional or final findings, affirmative or negative, may be made on the basis of facts available. Where it is found that the interested party has supplied false or misleading information, the information shall be disregarded and use may be made of the facts available.
Article 9
Redressive measures
Redressive measures, whether provisional or definitive, shall preferably take the form of duties imposed upon the non-Community carrier concerned.
Article 10
Provisional measures
1. Provisional measures may be imposed if a provisional affirmative determination has been made that the non-Community carriers concerned benefit from subsidies or are engaged in unfair pricing practices causing injury to the Community industry and that the Community interest calls for intervention to prevent further such injury.
2. Provisional measures may be taken in accordance with the procedure referred to in Article 15(2). Those measures shall be imposed for a maximum of six months.
Article 11
Termination without measures
1. Where the complaint is withdrawn, or where a satisfactory remedy has been obtained under a Member State's air service agreement with the third country concerned, the proceeding may be terminated by the Commission unless such termination would not be in the Community interest.
2. Where redressive measures are unnecessary, the proceeding shall be terminated in accordance with the procedure referred to in Article 15(2). Any decision to terminate a proceeding shall be accompanied by a statement of the reasons therefor.
Article 12
Definitive measures
1. Where the facts as finally established show the existence of subsidies or unfair pricing practices and the injury caused thereby, and the Community interest calls for intervention in accordance with Article 16, a definitive measure shall be imposed in accordance with the procedure referred to in Article 15(3).
2. The level of measures imposed to offset subsidies shall not exceed the amount of subsidies, calculated in terms of benefit conferred on the recipient enterprise, from which the non-Community carriers have been found to benefit, and should be less than the total amount of subsidies, if such lesser level were to be adequate to remove the injury to the Community industry.
3. The level of measures imposed to offset unfair pricing practices benefiting from a non-commercial advantage, shall not exceed the difference between the fares charged by the non-Community air carrier concerned and the air fares offered by the competing Community air carrier concerned, but should be less if such lesser level were to be adequate to remove the injury to the Community industry. In any event, the level of measures should not exceed the value of the non-commercial advantage granted to the non-Community air carrier.
4. A measure shall be imposed in the appropriate amounts in each case, on a non-discriminatory basis, on air services supplied by all non-Community air carriers found to benefit from subsidies or engaged in unfair pricing practices on the respective routes, except for air services supplied by those non-Community air carriers for which undertakings under the terms of this Regulation have been accepted.
5. A measure shall remain in force only as long as, and to the extent that, it is necessary to offset the subsidies or unfair pricing practices which are causing injury.
Article 13
Undertakings
1. Investigations may be terminated without the imposition of provisional or definitive measures upon receipt of satisfactory voluntary undertakings under which:
(a) the government granting the subsidy or non-commercial advantage agrees to eliminate or limit the subsidy or non-commercial advantage or take other measures concerning its effects; or
(b) any non-Community air carrier undertakes to revise its prices or to cease the supply of air services to the route in question so that the injurious effect of the subsidy or non-commercial advantage is eliminated.
2. Undertakings shall be accepted in accordance with the procedure referred to in Article 15(2).
3. In case of breach or withdrawal of undertakings by any party, a definitive measure shall be imposed in accordance with Article 12, on the basis of the facts established within the context of the investigation which led to the undertaking, provided that the investigation was concluded with a final determination as to subsidisation and that the non-Community air carrier concerned, or the government granting the subsidy, has been given an opportunity to comment, except in the case of withdrawal of the undertaking by the non-Community air carrier or such government.
Article 14
Reviews
1. Where circumstances so warrant, the need for the continued imposition of measures in their initial form may be reviewed, on the initiative of the Commission or upon the request of a Member State or, upon a request by non-Community air carriers subject to measures or by Community air carriers provided that at least two consecutive IATA scheduling seasons have elapsed since the imposition of the definitive measure.
2. Reviews under paragraph 1 shall be initiated by the Commission acting in accordance with the procedure referred to in Article 15(2). The relevant provisions of Articles 7 and 8 shall apply to the reviews under paragraph 1. Reviews shall assess the continued existence of subsidies or unfair pricing, and/or injury caused thereby, together with a new determination as to whether the Community interest calls for continued intervention. Where warranted by reviews, the measures shall be repealed, amended or maintained, as appropriate in accordance with the procedure referred to in Article 15(3).
Article 15
Committee procedure
1. The Commission shall be assisted by the Committee instituted by Article 11 of Council Regulation (EEC) No 2408/92 of 23 July 1992 on access for Community air carriers to intra-Community air routes(6), (hereinafter referred to as the Committee).
2. Where reference is made to this paragraph, Articles 3 and 7 of Decision 1999/468/EC shall apply, having regard to the provisions of Article 8 thereof.
3. Where reference is made to this paragraph, Articles 5 and 7 of Decision 1999/468/EC shall apply, having regard to the provisions of Article 8 thereof.
The period laid down in Article 5(6) of Decision 1999/468/EC shall be set at three months.
4. The Committee shall adopt its Rules of Procedure.
Article 16
Community interest
A determination under Articles 10(1), 11(2) and 12(1) as to whether the Community interest calls for intervention or whether measures should be maintained in accordance with Article 14(2) shall be based on an appraisal of all the various interests taken as a whole. Measures may not be applied where it can be clearly concluded that this is not in the Community interest.
Article 17
General provisions
1. Provisional or definitive redressive measures shall be imposed by Regulation, and enforced by Member States in the form, at the level specified and according to the other criteria laid down in the Regulation imposing such measures. If measures other than duties are imposed, the Regulation shall define the precise form of the measures in accordance with the provisions of this Regulation.
2. Regulations imposing provisional or definitive redressive measures, and Regulations or Decisions accepting undertakings or suspending or terminating investigations or proceedings, shall be published in the Official Journal of the European Union.
Article 18
Entry into force
This Regulation shall enter into force on the twentieth day of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Strasbourg, 21 April 2004.
For the European Parliament
The President
P. Cox
For the Council
The President
D. Roche
(1) OJ C 151 E, 25.6.2002, p. 285.
(2) OJ C 61, 14.3.2003, p. 29.
(3) Opinion of the European Parliament of 14 January 2003 (OJ C 38 E, 12.2.2004, p. 75), Council Common Position of 18 December 2003 (OJ C 66 E, 16.3.2004, p. 14). Position of the European Parliament of 11 March 2004 (not yet published in the Official Journal) and Council Decision of 30 March 2004.
(4) OJ L 184, 17.7.1999, p. 23.
(5) OJ L 240, 24.8.1992, p. 1.
(6) OJ L 240, 24.8.1992, p. 8. Regulation as last amended by European Parliament and Council Regulation (EC) No 1882/2003 (OJ No L 284, 31.10.2003, p. 1).
