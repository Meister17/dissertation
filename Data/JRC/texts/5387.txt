Commission Regulation (EC) No 2001/2005
of 8 December 2005
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 9 December 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 8 December 2005.
For the Commission
J. M. Silva Rodríguez
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 386/2005 (OJ L 62, 9.3.2005, p. 3).
--------------------------------------------------
ANNEX
to Commission Regulation of 8 December 2005 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code | Standard import value |
07020000 | 052 | 58,7 |
204 | 47,4 |
212 | 90,9 |
999 | 65,7 |
07070005 | 052 | 114,6 |
204 | 44,7 |
220 | 147,3 |
999 | 102,2 |
07099070 | 052 | 136,3 |
204 | 102,4 |
999 | 119,4 |
08051020 | 052 | 72,7 |
204 | 65,0 |
382 | 31,4 |
388 | 22,0 |
508 | 13,2 |
524 | 38,5 |
999 | 40,5 |
08052010 | 052 | 73,9 |
204 | 69,7 |
999 | 71,8 |
08052030, 08052050, 08052070, 08052090 | 052 | 71,2 |
400 | 81,1 |
624 | 100,9 |
999 | 84,4 |
08055010 | 052 | 55,8 |
999 | 55,8 |
08081080 | 400 | 105,2 |
404 | 96,0 |
720 | 81,7 |
999 | 94,3 |
08082050 | 052 | 104,1 |
400 | 86,0 |
404 | 53,2 |
720 | 63,1 |
999 | 76,6 |
--------------------------------------------------
