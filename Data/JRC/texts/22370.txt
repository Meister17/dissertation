COUNCIL DECISION of 10 November 1997 concerning the conclusion of the Cooperation Agreement between the European Community and the Lao People's Democratic Republic (97/810/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Articles 113 and 130y in conjunction with the first sentence of Article 228 (2) and the first subparagraph of Article 228 (3) thereof,
Having regard to the proposal from the Commission (1),
Having regard to the Opinion of the European Parliament (2),
Whereas, under Article 130u of the Treaty, Community policy in the sphere of development cooperation should foster the sustainable economic and social development of the developing countries, their smooth and gradual integration into the world economy and the alleviation of poverty in these countries;
Whereas the Community should approve, in pursuit of its objectives in the sphere of external relations, the Cooperation Agreement between the European Community and the Lao People's Democratic Republic,
HAS DECIDED AS FOLLOWS:
Article 1
The Cooperation Agreement between the European Community and the Lao People's Democratic Republic is hereby approved on behalf of the Community.
The text of this Agreement is attached to this Decision.
Article 2
The President of the Council shall give the notification provided for in Article 21 of the Agreement (3).
Article 3
The Commission, assisted by representatives of the Member States, shall represent the Community in the Joint Committee provided for in Article 14 of the Agreement.
Article 4
This Decision shall be published in the Official Journal of the European Communities.
Done at Brussels, 10 November 1997.
For the Council
The President
J. POOS
(1) OJ C 109, 8. 4. 1997, p. 8.
(2) OJ C 325, 27. 10. 1997.
(3) See page 15 of this Official Journal.
