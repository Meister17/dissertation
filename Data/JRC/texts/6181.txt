European Food Safety Authority — Publication of the final accounts for the financial year 2004
(2005/C 269/06)
The complete version of the final accounts may be found at the following address:
http://www.efsa.eu.int/about_efsa/efsa_funding/accounts/catindex_en.html
Table 1
Implementation of the budget for the financial year 2004
NB: Variations in totals are due to the effects of rounding.
(1000 euro) |
Revenue | Expenditure |
Origin of revenue | Revenue entered in the final budget for the financial year | Revenue collected | Allocation of expenditure | Final budget appropriations | Appropriations carried over from the previous financial year | Appropriations available (2004 budget and financial year 2003) |
entered | committed | paid | carried over | cancelled | outstanding commitments | paid | cancelled | appropriations | committed | paid | carried over | cancelled |
Community subsidies | 29092 | 18000 | Title I Staff | 11509 | 8641 | 8251 | 390 | 2868 | 149 | 140 | 9 | 11658 | 8790 | 8392 | 390 | 2877 |
Other subsidies | 0 | 0 | Title II Administration | 5633 | 5094 | 3617 | 1477 | 539 | 1189 | 1059 | 130 | 6822 | 6284 | 4676 | 1477 | 669 |
Other revenues | 0 | 0 | Title III Operating expenditure | 11950 | 7517 | 4051 | 3818 | 4081 | 2895 | 2020 | 875 | 14845 | 10412 | 6071 | 3818 | 4956 |
Total | 29092 | 18000 | Total | 29092 | 21252 | 15919 | 5684 | 7488 | 4233 | 3219 | 1014 | 33325 | 25486 | 19139 | 5684 | 8502 |
Table 2
Revenue and expenditure account for the financial years 2004 and 2003
NB: Variations in totals are due to the effects of rounding.
(1000 euro) |
| 2004 | 2003 |
Operating revenue | 20591 | 10171 |
Total operating revenue | 20591 | 10171 |
Administrative expenditure | | |
Staff expenditure | – 7564 | – 3213 |
Buildings and related expenditure | – 4192 | – 781 |
Other expenditure | – 1263 | – 536 |
Depreciation and write-off | – 333 | – 204 |
Operating expenditure | – 6431 | – 2159 |
Total operating expenditure | – 19783 | – 6894 |
Total operating profit/loss | 808 | 3277 |
Revenue from financial operations | 0 | 1 |
Expenditure from financial operations | – 7 | – 3 |
Profit/(loss) on financial operations | – 6 | – 2 |
Current profit/(loss) | 802 | 3275 |
Extraordinary revenue | | 402 |
Extraordinary expenditure | – 27 | |
Extraordinary profit/(loss) | – 27 | 402 |
Economic outturn for the financial year | 775 | 3677 |
Table 3
Balance sheet at 31 December 2004 and at 31 December 2003
NB: Variations in totals are due to the effects of rounding.
(1000 euro) |
Assets | 2004 | 2003 | Liabilities | 2004 | 2003 |
A.FIXED ASSETS | | | A.OWN CAPITAL | | |
Intangible assets | | | Balance carried over | 3677 | |
Computer programmes | 423 | 362 | Economic outturn for the financial year | 775 | 3677 |
Tangible assets | | | Total | 4452 | 3677 |
Computer equipment | 1035 | 701 | C.LIABILITIES | | |
Furniture and mobile equipment | 151 | 106 | Liabilities of a maximum of one year | | |
Depreciation | – 537 | – 204 | Salary liabilities | 13 | 8 |
| | | Fiscal and social liabilities | 77 | |
Total | 1071 | 965 | Consolidated entities (EC) | 2069 | 146 |
B.CURRENT ASSETS | | | Expenditure to be allocated and revenue to be carried over | 2686 | 1535 |
Stocks | | | | | |
Receivables of a maximum of one year | | | | | |
Advance financing | 323 | | | | |
Staff | 50 | | | | |
Current receivables | 1 | 1 | | | |
Other receivables | 36 | 40 | | | |
Consolidated entities (EC) | 4568 | | | | |
Expenditure to be carried over and revenue accrued | 47 | 2 | | | |
Available assets | | | | | |
Bank accounts | 3202 | 4342 | | | |
Imprest accounts | | 15 | | | |
Total | 8226 | 4400 | Total | 4845 | 1688 |
Total assets | 9297 | 5365 | Total liabilities | 9297 | 5365 |
[1] The data for the financial year 2003 have been restated to make them comply with the principle of accruals-based accounting.
[2] The data for the financial year 2003 have been restated to make them comply with the principle of accruals-based accounting.
[3] Community institutions and bodies.
--------------------------------------------------
