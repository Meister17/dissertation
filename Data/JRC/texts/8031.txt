*****
COMMISSION DECISION
of 10 May 1990
laying down the criteria for the acceptance for breeding purposes of pure-bred breeding sheep and goats and the use of their semen, ova or embryos
(90/257/EEC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Directive 89/361/EEC of 30 May 1989 concerning pure-bred breeding sheep and goats (1), and in particular Article 4 thereof,
Whereas Directive 89/361/EEC is intended gradually to liberalize intra-Community trade in pure-bred breeding sheep and goats; whereas, in this connection, additional harmonization with regard to the acceptance for breeding purposes of such animals and their semen, ova and embryos is necessary;
Whereas the provisions concerning acceptance for breeding relate to animals as well as their semen, ova and embryos;
Whereas it should be provided that semen, ova and embryos may be handled only by officially approved staff in order to ensure the guarantees necessary for attaining the desired end;
Whereas the measures provided for in this Decision are in accordance with the opinion of the Standing Committee on Zootechnics,
HAS ADOPTED THIS DECISION:
Article 1
Without prejudice to Article 2 all pure-bred breeding sheep and goats, whether male or female, which are registered in a flock-book shall be accepted for breeding.
Article 2
1. Male pure-bred breeding sheep and goats shall be accepted for the purposes of artificial insemination and use of their semen if they have undergone tests for monitoring their performance and assessing their genetic value pursuant to Commission Decision 90/256/EEC (1).
2. Male pure-bred breeding and goats shall be accepted for the purposes of official testing and use of their semen, within the quantitative limits required for monitoring their performance and assessing their genetic value pursuant to Decision 90/256/EEC.
3. Female pure-bred breeding sheep and goats shall be accepted for breeding and use of their ova and embryos.
Article 3
The semen, ova and embryos must be collected, treated and stored by an officially approved centre or officially approved personnel.
Article 4
This Decision is addressed to the Member States.
Done at Brussels, 10 May 1990.
For the Commission
Ray MAC SHARRY
Member of the Commission
(1) OJ No L 153, 6. 6. 1989, p. 30.
(2) See page 35 of this Official Journal.
