Commission Regulation (EC) No 1148/2001
of 12 June 2001
on checks on conformity to the marketing standards applicable to fresh fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 2200/96 of 28 October 1996 on the common organisation of the market in fruit and vegetables(1), as last amended by Regulation (EC) No 911/2001(2), and in particular Article 10 thereof,
Whereas:
(1) Commission Regulation (EEC) No 2251/92 of 29 July 1992 on quality inspection of fresh fruit and vegetables(3), as last amended by Regulation (EC) No 766/97(4), must be amended in a number of places to take account of the development of and improved professionalism in the trade in fresh fruit and vegetables and of changes in the common organisation of the markets in this sector, and in order to clarify a number of ambiguities in that Regulation. In the interests of simplification and improved clarity, the rules should be completely recast and Regulation (EEC) No 2251/92 should be repealed.
(2) The Member States must designate the inspection bodies who are responsible for carrying out checks on conformity at each stage of marketing. Given the very diverse circumstances in the Member States, one or other of these bodies in each Member State should be responsible for contacts and coordination between all designated bodies.
(3) Conformity checks should be carried out by sampling and should concentrate on traders most likely to have goods which do not comply with the standards. Depending on the characteristics of their national market, Member States must lay down rules prioritising checks on particular categories of trader. To ensure transparency, these rules should be notified to the Commission. Since knowledge of traders and their main characteristics is an indispensable tool in Member States' analysis, it is essential to set up a database on traders in fresh fruit and vegetables in each Member State, following the register drawn up according to Regulation (EEC) No 2251/92.
(4) Exports of fruits and vegetables to third countries must conform to the standards. Member States must ensure it is the case and certify conformity, in accordance with the Geneva Protocol on standardisation of fresh fruit and vegetables and dry and dried fruit concluded within the United Nations Economical Commission for Europe and the OECD scheme for the application of international standards for fruit and vegetables. Imports of fresh fruit and vegetables from third countries must conform to the marketing standards or to standards at least equivalent to them. Checks on conformity must therefore be carried out before these goods enter Community customs territory, except in the case of small lots which the inspection authorities consider to be low risk. In certain third countries which provide satisfactory guarantees of conformity, checks may be carried out by the inspection bodies of these third countries. Where this option is applied, the Member States are to verify regularly the validity of the checks carried out by third country inspection bodies and inform the Commission of the results of these verifications.
(5) Products intended for processing are not required to conform to these standards, so it must be ensured that they are not sold on the market for fresh products. Such products should be suitably labelled and, in certain cases, accompanied by a processing certificate stating the end-use and allowing checks to be made.
(6) Fruit and vegetables checked for conformity to the marketing standards should be subject to the same type of check at all stages of marketing. The inspection guidelines recommended by the United Nations Economic Commission for Europe, themselves in line with the relevant OECD recommendations, should be adopted to this end. Specific arrangements should however be laid down for checks at the retail sale stage.
(7) The measures provided for in this Regulation are in accordance with the opinion of the Committee for Fresh Fruit and Vegetables,
HAS ADOPTED THIS REGULATION:
Article 1
Member States shall carry out checks at all stages of marketing on conformity to the marketing standards laid down in Articles 7, 8 and 9 of Regulation (EC) No 2200/96 in accordance with this Regulation.
Article 2
Competent bodies
1. Each Member State shall designate a single competent authority responsible for coordination and contacts in the areas covered by this Regulation, hereinafter called "the coordinating authority".
2. The Member States shall communicate to the Commission:
- the name and postal and e-mail address of the coordinating authority they have designated in application of paragraph 1,
- the name and postal and e-mail address of the inspection bodies they appointed in application of the third subparagraph of Article 7 of Regulation (EC) No 2200/96, hereinafter called "the inspection bodies", and
- an exact description of the respective spheres of activity of the inspection bodies they appointed.
3. The coordinating authority may be the inspection body or one of the inspection bodies or any other body designated according to paragraph 1.
4. The Commission shall publish the list of coordinating authorities designated by the Member States in the C series of the Official Journal of the European Communities.
Article 3
Trader database
1. The Member States shall set up a database on traders in fruit and vegetables, which shall list, under the conditions defined by the present Article, traders involved in marketing fresh fruit and vegetables for which standards have been laid down in application of Article 2 of Regulation (EC) No 2200/96.
By "trader" is meant any natural or legal person holding fresh fruit and vegetables subject to marketing standards with a view to their displaying or offering for sale, their sale or their marketing in any other manner for itself or on behalf of a third party in the Community's territory and/or export to third countries.
Member States shall determine the conditions under which the following traders are included or not in the database:
- traders whose activities are exempt from the obligation to comply with the marketing standards pursuant to Article 3 of Regulation (EC) No 2200/96,
- natural or legal persons whose activities in the fruit and vegetables sector are limited either to the transport of goods, or to the sale of small quantities at the retail stage.
2. Where the database is composed of several distinct elements, the coordinating authority shall ensure that the database, its elements and their updating are uniform. The updating is done in particular by the inspection bodies using the information collected during checks carried out at all stages of marketing.
3. This database shall contain, for each trader, the registration number, name, address, information needed for its classification in one of the categories mentioned in Article 4 of this Regulation, in particular, position in the marketing chain, information concerning the importance of the firm, information concerning findings made during previous checks of each trader, as well as any other information considered necessary for checks.
4. Traders must provide the information that the Member States consider necessary to set up and update the database. Member States shall determine the conditions under which traders not established in their territory but trading on it shall be listed in their database.
Article 4
Conformity checks on the internal market
1. The Member States shall introduce a system of sampling checks on the conformity to marketing standards of products held by traders at all stages of marketing.
Under this system, the Member States shall specify the frequency, based on a risk analysis of a trader marketing goods not in conformity with the marketing standards, with which checks must be made by the inspection bodies, which must be sufficient to ensure compliance with Community rules, for each category of trader they have first defined. This risk analysis will relate, in particular, to the size of the firms, their position in the marketing chain, findings made during previous checks and other possible parameters to be defined by the Member States. Traders involved in preparing and packaging fruit and vegetables, particularly in the production region, shall be subject to a higher rate of checks than other categories of trader. Checks may also occur during transport.
Where checks reveal significant irregularities, the inspection bodies shall increase the frequency of checks on the traders concerned.
2. Traders must provide the inspection bodies with all information those bodies judge necessary for organising and carrying out checks.
3. Member States may authorise traders at the stage of dispatch guaranteeing uniform and high conformity rate of the fruit and vegetables subject to marketing standards they dispatch to use the specimen in Annex 3 in the labelling of each package. The authorisation shall be granted for a period of three years and shall be renewable.
In addition, traders benefiting from this possibility must:
- have inspection staff who have received training approved by the Member State,
- have suitable equipment for preparing and packing produce,
- commit themselves to proceed to a conformity check of the goods they dispatch and have a register recording all operations of checks carried out.
When traders can no longer guarantee high and uniform conformity rate, or when one of the above mentioned conditions is no longer fulfilled, the Member State shall withdraw the authorisation for the trader to use the specimen in Annex III in the labelling of each package.
4. The coordinating authority shall communicate the provisions of the inspection system as referred to in paragraph 1 to the Commission before the application of this Regulation. This communication shall include the different categories of traders that were identified and the check frequency specified for each of them, as well as, where appropriate, the detailed conditions of implementation of the provisions of paragraph 3, the detailed conditions of implementation of the provisions of Article 5(1), including the minimum proportions of checks for the different traders concerned. It shall immediately inform the Commission of any subsequent amendments to that system.
Article 5
Conformity checks at the point of export
1. The competent inspection body at the stage of export shall ensure by a conformity check that products intended for export to third countries only leave the Community's customs territory if they conform to the marketing standards.
Exporters must provide the inspection bodies with all information those bodies judge necessary for organising and carrying out checks.
For traders conforming to the conditions of application of the provisions mentioned in Article 4(3), Member States may specify, for each category of trader concerned and based on a risk analysis, the minimum proportion of consignments and quantities subject to conformity checks by the competent inspection body at the export stage. This proportion must be sufficient to ensure compliance with Community rules. Where checks reveal significant irregularities, the inspection bodies shall increase the proportion of consignments checked on the traders concerned.
2. The inspection body shall issue a certificate of conformity as set out in Annex I for each lot intended for export and which they consider to be in conformity with the marketing standards, after completion of the checking operations mentioned in paragraph 1. Where the export consignment consists of several lots, the conformity of those lots may be certified on a single certificate which clearly lists the various lots constituting that consignment.
3. The export declaration may be accepted by the competent customs authority only if:
- the goods are accompanied by either the certificate referred to in paragraph 2 or the certificate referred to in Article 8(2), or if
- the competent inspection body informed the customs authority, by appropriate means, that the relevant lots have been subject to the issue of one of those two certificates.
Article 6
Conformity checks at the point of import
1. Before release for free circulation, products from third countries shall be checked for conformity with the marketing standards.
Importers must provide the inspection bodies with all information those bodies judge necessary to organising and carrying out the checks mentioned in paragraph 2 of this Article and Article 7(5).
2. Notwithstanding Article 7, the official inspection body at the point of import shall carry out a conformity check for each imported lot and, where those products conform with the required standards, issue a certificate of conformity as set out in Annex I. Where the import consignment consists of several lots, the conformity of those lots may be certified on a single certificate which clearly lists the various lots constituting that consignment.
3. The customs authorities shall authorise release for free circulation only if:
- the goods are accompanied by the certificate referred to in paragraph 2, the certificate referred to in Article 7(3) or the certificate referred to in Article 8(2), or if
- the competent inspection body informed the customs authority, by appropriate means, that the relevant lots have been subject to the issuance of one of those certificates.
4. Notwithstanding paragraphs 1, 2 and 3, where the competent inspection body at the point of import considers that there is a low risk of certain lots of a weight less than or equal to 500 kilograms not conforming to the marketing standards, they may choose not to check those lots. They shall send the customs authority a stamped declaration to this end or inform that authority in another manner that it may carry out clearance procedures.
Article 7
Approval of checking operations performed by third countries prior to import into the Community
1. At the request of a third country, the Commission may approve, in accordance with the procedure laid down in Article 46 of Regulation (EC) No 2200/96, conformity checking operations performed by this third country prior to import into the Community.
2. The approval referred to in paragraph 1 may be granted to third countries which so request and on whose territory the Community marketing standards, or standards at least equivalent, are met for products exported to the Community.
The approval specifies the official authority in the third country under the responsibility of which checking operations mentioned in paragraph 1 are performed. This authority shall be responsible for contacts with the Community. The approval also specifies the inspection bodies in charge of the proper checks, hereinafter called "third country inspection bodies".
The approval may only apply to products originating in the third country concerned and may be limited to certain products.
The Commission shall publish the list of approvals granted to third countries under the conditions of the present Article in the C series of the Official Journal of the European Communities.
3. The third country inspection bodies must be official bodies or officially recognised by the authority mentioned in paragraph 2 providing satisfactory guarantees and disposing of the necessary personnel, equipment and facilities to carry out checks according to the methods mentioned in Article 9(1) or equivalent methods.
The third country inspection bodies shall draw up, for each lot checked prior to its entry into Community customs territory, either the certificate of conformity set out in Annex I, or any other form agreed between the Commission and the third country. Where the import consignment consists of several lots, the conformity of these lots may be certified on a single certificate which clearly lists the various lots constituting that consignment.
4. The Commission may suspend approval if it is found that, in a significant number of lots and/or quantities, the goods do not correspond to the information in the certificates of conformity issued by the third country inspection bodies.
5. Member States shall conduct checks at the point of import on products imported under the conditions set out in the present Article for conformity with the standards by carrying out conformity checks for each third country concerned on a significant proportion of the consignments and quantities imported under those conditions. This proportion must be sufficient to ensure compliance with the Community rules by the third country inspection bodies. Member States shall ensure that the measures laid down in Article 9(3) are applied to the lots checked, where those lots do not comply with the marketing standards.
Where checks reveal significant irregularities, Member States shall immediately inform the Commission, and the inspection bodies shall increase the proportion of consignments and quantities checked in accordance with the provisions of this Article.
6. The coordinating authority shall communicate to the Commission each quarter, by the end of the quarter following that quarter at the latest, for each third country and product concerned, the number of lots and total quantities imported in accordance with paragraph 1, the number of lots and the quantities which have been checked for conformity as referred to in paragraph 5 and, of those lots, those which the inspection bodies found not to conform with the data mentioned in the conformity certificates issued by the third country inspection body, specifying the quantities for each of those lots and the type of defects giving rise to that finding.
7. The customs authorities shall provide the coordinating authority and/or the inspection bodies with all information necessary for the application of the provisions of the present Article.
Article 8
Products intended for processing
1. For the purpose of the application of this Regulation, products intended for processing are fresh fruit and vegetables subject to marketing standards that are shipped to processing plants where they are processed into products classified in a CN position different from that of the initial fresh product.
2. The competent inspection bodies shall issue processing certificates as provided for in Annex II for products intended for export to third countries and products imported into the Community where such products are intended for processing and are, therefore, in accordance with Article 3(3)(a) of Regulation (EC) No 2200/96, not subject to conformity with the marketing standards. They shall ensure that the special labelling provisions laid down paragraph 3 are complied with.
3. In the case of imports, after having issued any certificate referred to in paragraph 2, the competent inspection body shall immediately send to the coordinating authority of the Member State where processing is to take place a copy of the certificate and any forth information needed for a possible check of the processing operations. After processing, the processing enterprise shall return the certificate to the competent inspection body, which shall ensure that the products have actually been processed.
4. The packaging of products intended for processing must be clearly marked by the pack with the words "intended for processing" or other equivalent wording. In the case of goods shipped in bulk, directly loaded onto a means of transport, this indication shall be given in a document accompanying the goods or shown on a notice placed in an obvious position inside the means of transport.
5. Members States shall take all the measures they judge necessary, in particular those related to cooperation with the other Member States concerned, to avoid any goods intended for the fresh market being shipped outside the region of production as goods intended for processing.
Article 9
Method of inspection
1. The conformity checks provided for in this Regulation, with the exception of those at the point of retail sale to the end consumer, shall be carried out, save as otherwise provided under this Regulation, in accordance with the methods in Annex IV.
The Member States shall lay down specific arrangements for checking conformity at the point of retail sale to the end consumer.
2. Where inspectors find that the goods conform with the marketing standards, the inspection body may issue a certificate of conformity as set out in Annex I. This certificate shall in any event be issued at the point of import or export.
3. Where the goods do not conform with the standards, the inspection body shall issue a finding of non-conformity for the attention of the trader or their representatives. Goods for which a finding of non-conformity has been issued may not be moved without the authorisation of the inspection body which issued that finding. This authorisation can be subject to the respect of conditions laid down by the inspection body.
Traders may decide to bring all or some of the goods into conformity. Goods brought into conformity may not be marketed before the competent inspection body has ensured by all appropriate means that the goods have actually been brought into conformity. It shall issue, where applicable, a certificate of conformity as set out in Annex I for the lot or part thereof only once the goods have been brought into conformity.
Article 10
Final provisions
1. The inspection bodies of a Member State on whose territory a lot of goods from another Member State is found not to conform with the standards because of defects or deterioration which could have been detected at the time of packaging shall immediately notify such cases of non-conformity discovered up to the wholesale marketing stage, including at distribution centres, to the authorities of the other Member States likely to be concerned.
2. Where, on import from a third country, a lot of goods are found not to conform with the standards, the coordinating authority of the Member State concerned shall immediately notify the Commission and the coordinating authorities of the other Member States likely to be concerned, which shall then pass on this information as necessary in their territory. The Commission shall be notified via the electronic system indicated by the Commission.
3. For the purposes of applying this Regulation, in the case of products for which marketing standards have been adopted, invoices and accompanying documents must indicate the quality class, the country of origin of the products and, where appropriate, the fact that it is intended for processing. This requirement shall not apply to retail sale to the end consumer.
Article 11
1. Regulation (EEC) No 2251/92 is hereby repealed.
2. Traders exempted from inspection of goods at the time of consignment under Article 6 of Regulation (EEC) No 2251/92 may use the packaging affixed with the label provided for in Annex III to that Regulation until 31 December 2002.
3. Inspection certificates issued according to Article 3(9) of Regulation (EEC) No 2251/92 are valid until the date of validity in box 12 of the concerned certificates expires. Processing certificates issued according to Article 10 of Regulation (EEC) No 2251/92 are valid until 31 January 2002.
Article 12
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Communities.
It shall apply from 1 January 2002.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 12 June 2001.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 297, 21.11.1996, p. 1.
(2) OJ L 129, 11.5.2001, p. 3.
(3) OJ L 219, 4.8.1992, p. 9.
(4) OJ L 112, 29.4.1997, p. 10.
ANNEX I
>PIC FILE= "L_2001156EN.001502.EPS">
ANNEX II
>PIC FILE= "L_2001156EN.001702.EPS">
ANNEX III
>PIC FILE= "L_2001156EN.001902.EPS">
ANNEX IV
METHODS OF INSPECTION MENTIONED IN ARTICLE 9(1)
Note:
The following methods of inspection are based on the provisions of the guide for the implementation of quality control of fresh fruit and vegetables adopted by the UN/ECE (United Nations Economic Commission for Europe) Working Party for standardisation of perishable goods and improvement of quality.
1. Definitions
(a) Conformity check
Inspection carried out to check the conformity of fruit and vegetables with the marketing standards provided by Regulation (EC) No 2200/96.
(b) Inspector
Person authorised by the competent inspection body who has an appropriate and regular training enabling them to undertake conformity inspection.
(c) Consignment
Quantity of produce to be sold by a given trader found at the time of control and defined by a document. The consignment may consist of one or several types of produce: it may contain one or several lots of fresh fruit and vegetables.
(d) Lot
Quantity of produce which, at the time of control at one place, has similar characteristics with regard to:
- packer and/or dispatcher,
- country of origin,
- nature of produce,
- class of produce,
- size (if the produce is graded according to size),
- variety or commercial type (according to the relevant provisions of the standard),
- type of packaging and presentation.
However, if during the inspection of consignments, it is not possible to distinguish between different lots and/or presentation of individual lots is not possible, all lots of a specific consignment may be treated as one lot if they are similar in regard to type of produce, dispatcher, country of origin, class and variety or commercial type, if this is provided for in the standard.
(e) Sampling
Collective sample taken temporarily from a lot during conformity check inspection.
(f) Primary sample
Package taken from the lot, or, in the case of bulk produce, a quantity taken from a point in the lot.
(g) Bulk sample
Several representative primary samples taken from the lot whose quantity is sufficient to allow the assessment of the lot with regard to all criteria.
(h) Reduced samples
Representative quantity of produce taken from the bulk sample whose size is sufficient to allow the assessment of certain individual criteria. Several reduced samples may be taken from a bulk sample.
2. Implementation of conformity check
(a) General remark
A conformity check shall take place by assessing bulk samples taken at random from different points in the lot to be controlled. It is based on the principle of presumption that the lot conforms to the bulk sample.
(b) Presentation of produce
The inspector decides which packages are to be controlled. The presentation shall be made by the person authorised to do so or his representative. The procedure should include the presentation of a bulk sample as well as the supply of all information necessary for the identification of the consignments or lots.
If reduced samples are required, these are identified by the inspector himself from the bulk sample.
(c) Identification of lots and/or getting general impression of the consignement
The identification of lots shall be carried out on the basis of their marking or other criteria, such as the indications laid down under Council Directive 89/396/EEC(1). For consignments which are made up of several lots it is necessary for the inspector to get a general impression of the consignment with the aid of accompanying documents or declarations concerning the consignments. He then determines how far the lots presented comply with the information in these documents.
If the produce is to be, or has been loaded onto a means of transport, the registration number of the latter shall be used for identification of the consignment.
(d) Verification of the lot
- assessment of packaging and presentation on the basis of primary samples:
the packaging, including the material used within the package, shall be tested for suitability and cleanness according to the provisions of the marketing standards. If only certain types of packaging are permitted, the inspector checks whether these are being used. If the individual standard includes provisions concerning presentation, their conformity is also checked,
- verification of marking on the basis of primary samples:
first, it is ascertained whether the produce is marked according to the marketing standards. During control a check is made on the accuracy of marking and/or the extent of amendment required,
- verification of conformity of the produce:
the inspector shall determine the size of the bulk sample in such way as to be able to assess the lots. He shall at random select the packages to be inspected or in the case of bulk produce the points of the lot from which individual samples shall be taken.
Damaged packages shall not be used as part of the bulk sample. They should be set aside and may, if necessary, be subject to a separate examination and report.
The bulk sample should comprise the following quantities whenever a consignment is declared unsatisfactory:
>TABLE>
>TABLE>
In the case of bulky fruit and vegetables (over 2 kg per unit), the primary samples should be made up of at least five units.
If the inspector discovers, after an inspection, that a decision cannot be reached, he may carry out another inspection and express the overall result as an average of the two checks.
Certain criteria such as the presence or the absence of internal defects may be checked on the basis of reduced samples; this applies in particular to control which destroys the trade value of the produce. The size of the reduced sample shall be restricted to the minimum quantity absolutely necessary for the assessment of the lot; if, however, defects are ascertained or suspected the size of the reduced sample shall not exceed 10 % of the size of the bulk sample initially taken for the inspection.
(e) Control of produce
The produce has to be removed entirely from its packaging for the control; the inspector may only dispense with this, if the type of packaging and form of presentation allow an inspection without unpacking the produce. The inspection of uniformity, minimum requirements, quality classes and size shall be carried out on the basis of the bulk sample. When defects are detected, the inspector shall ascertain the respective percentage of the produce not in conformity with the standard by number or weight.
(f) Report of control results
Documents mentioned in Article 9(2) and (3) are issued where appropriate.
If defects are found, the trader or his representative must be informed of the reasons of complaint. If the compliance of produce with the standard is possible by a change in marking, the trader or his representative must be informed of it.
If defects are found in a product, the percentage found not to be in conformity with the standard shall be indicated. This is not necessary if it is possible to achieve compliance of produce with the standard by a change in the marking of the product.
(g) Decline in value by conformity check
After the control, the bulk sample is put at the disposal of the trader or his representative.
The inspection body is not bound to hand back the elements of the bulk sample destroyed during the control.
When the conformity check has been limited to the minimum required, no compensation can be claimed from the inspection body concerned if the commercial value of the produce has suffered a loss.
(1) OJ L 186, 30.6.1989, p. 21.
