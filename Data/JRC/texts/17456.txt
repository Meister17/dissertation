Brussels, 7.1.2005
COM(2004) 862 final
2004/0294 (ACC)
2004/0295 (ACC)
.
Proposal for a
COUNCIL DECISION
on the signature and provisional application of a Protocol to the Interim Agreement between the European Community, of the one part, and the Republic of Lebanon, of the other part, to take account of the accession of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia, and the Slovak Republic to the Community
Proposal for a
COUNCIL DECISION
on the conclusion of a Protocol to the Interim Agreement between the European Community, of the one part, and the Republic of Lebanon, of the other part, to take account of the accession of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia, and the Slovak Republic to the Community
.
(presented by the Commission)
EXPLANATORY MEMORANDUM
The attached draft Protocol to the Interim Agreement on trade and trade-related matters between the European Community of the one part and the Republic of Lebanon of the other part, aims at taking into account of the accession of the ten new Member States to the European Union. The EU-Lebanon Interim Agreement entered into force on 1 March 2003, after ratification by Council and by Lebanon. By concluding the attached Protocol by the Council, acting unanimously on behalf of the Member States, and by the third country concerned, the EU-Lebanon Interim Agreement will also introduce the customs language and other alterations needed to allow the administrative aspects of trade between the new Member States and Lebanon to take place.
On 10 February 2004 the Council approved a mandate for the Commission to negotiate such a Protocol with the Republic of Lebanon. These negotiations have since been completed to the satisfaction of the Commission.
The attached proposals are for (1) a Council Decision on the signature of the Protocol and (2) a Council Decision on the conclusion of the Protocol.
The text of the Protocol negotiated with Lebanon is attached. The most important aspects of the Protocol are provisions on the effects of accession of the new Member States to the EU-Lebanon Interim Agreement, and inclusion of the new official languages of the EU. There are no changes to any other parts of the Interim Agreement, nor any adjustments to quotas or arrangements affecting trade in agricultural and non-agricultural goods.
The Commission requests the Council to approve the attached draft Council Decisions for the signature and conclusion of the Protocol.
Since the EU-Lebanon Interim Agreement is trade-specific, not requiring the assent of the European Parliament, the Protocol similarly does not call for the assent of the European Parliament.
2004/0294 (ACC)
Proposal for a
COUNCIL DECISION
on the signature and provisional application of a Protocol to the Interim Agreement between the European Community, of the one part, and the Republic of Lebanon, of the other part, to take account of the accession of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia, and the Slovak Republic to the Community
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 133, in conjunction with the first sentence of the first subparagraph of Article 300(2) thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) On 10 February 2004 the Council authorised the Commission, on behalf of the European Community and its Member States, to open negotiations with Lebanon with a view to adjusting the Interim Agreement between the European Community, on the one part, and the Republic of Lebanon, on the other part, to take account of the accession of the new Member States to the EU,
(2) These negotiations have been concluded to the satisfaction of the Commission,
(3) The text of the Protocol negotiated with the Republic of Lebanon provides, in Article 8 paragraph 2, for the provisional application of the Protocol before its entry into force,
(4) Subject to its possible conclusion at a later date, the Protocol should be signed on behalf of the Community and applied provisionally,
HAS DECIDED AS FOLLOWS:
Article 1
The President of the Council is hereby authorised to designate the person(s) empowered to sign, on behalf of the European Community, the Protocol to the Interim Agreement between the European Community, of the one part, and the Republic of Lebanon, of the other part, to take account of the accession of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia, and the Slovak Republic to the European Union. The text of the Protocol is attached to this decision.
Article 2
The European Community hereby agrees to apply provisionally the terms of the Protocol, subject to its possible conclusion at a later date.
Done at Brussels, […]
For the Council
The President
2004/0295 (ACC)
Proposal for a
COUNCIL DECISION
on the conclusion of a Protocol to the Interim Agreement between the European Community, of the one part, and the Republic of Lebanon, of the other part, to take account of the accession of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia, and the Slovak Republic to the Community
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 133, in conjunction with the first sentence of the first subparagraph of Article 300(2) thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) The Protocol to the Interim Agreement between the European Community, on the one part, and the Republic of Lebanon, on the other part, has been signed on behalf of the European Community on ,
(2) The Protocol should be approved,
HAS DECIDED AS FOLLOWS:
Sole Article
The Protocol to the Interim Agreement between the European Community, of the one part, and the Republic of Lebanon, of the other part, to take account of the accession of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia, and the Slovak Republic to the European Union, is hereby approved on behalf of the European Community. The text of the Protocol is attached to this Decision.
Done at Brussels, […]
For the Council
The President
PROTOCOL TO THE INTERIM AGREEMENT ON TRADE AND TRADE-RELATED MATTERS
between the European Community, of the one part, and the Republic of Lebanon, of the other part, to take account of the accession of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia, and the Slovak Republic to the Community
THE EUROPEAN COMMUNITY,
hereinafter referred to as “the Community” represented by the Council of the European Union and the European Commission
of the one part
and THE REPUBLIC OF LEBANON
hereinafter referred to as “Lebanon”
of the other part
WHEREAS the Interim Agreement between the European Community, of the one part, and the Republic of Lebanon, of the other part, hereinafter referred to as “the Interim Agreement”, was signed in Luxembourg on 17 June 2002 and entered into force on 1 March 2003;
WHEREAS the Treaty concerning the accession of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic to the European Union and the Act to it was signed in Athens on 16 April 2003; and these states subsequently acceded to the European Union and thereby to the Community on 1 May 2004.
WHEREAS consultations pursuant to Article 18 of the Interim Agreement have taken place in order to ensure that account has been taken of the mutual interests of the Community and Lebanon stated in this Agreement;
HAVE AGREED AS FOLLOWS:
Adjustments to the text of the Interim Agreement including its Annexes
Article 1
The Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia, and the Slovak Republic, hereinafter referred to as “the new Member States”, shall respectively adopt and take note, in the same manner, as the other Member States of the Community, of the texts of the Interim Agreement between the European Community, of the one part, and the Republic of Lebanon, of the other part and including the Annexes and Protocols forming an integral part thereof, and the Final Act together with the Declarations attached thereto.
Article 2
To take into account recent institutional developments within the European Union, the Parties agree that following the expiry of the Treaty establishing the European Coal and Steel Community, existing provisions in the Agreement referring to the European Coal and Steel Community shall be deemed to refer to the European Community which has taken over all rights and obligations contracted by the European Coal and Steel Community.
CHAPTER ONE: AMENDMENTS TO THE TEXTE OF THE INTERIM AGREEMENT INCLUDIND ITS ANEXES AND PROTOCOLS
Article 3 (Rules of Origin)
Protocol 4 shall be amended as follows:
1. Article 18 (4) shall be replaced by the following :
Movement certificates EUR.1 issued retrospectively must be endorsed with one of the following phrases:
ES "EXPEDIDO A POSTERIORI"
CS "VYSTAVENO DODATEČNĔ"
DA "UDSTEDT EFTERFØLGENDE"
DE "NACHTRÄGLICH AUSGESTELLT"
ET "VÄLJA ANTUD TAGASIULATUVALT"
EL "ΕΚΔΟΘΕΝ ΕΚ ΤΩΝ ΥΣΤΕΡΩΝ"
EN "ISSUED RETROSPECTIVELY"
FR "DÉLIVRÉ A POSTERIORI"
IT "RILASCIATO A POSTERIORI"
LV "IZSNIEGTS RETROSPEKTĪVI"
LT "RETROSPEKTYVUSIS IŠDAVIMAS"
HU "KIADVA VISSZAMENŐLEGES HATÁLLYAL"
MT "MAĦRUĠ RETROSPETTIVAMENT"
NL "AFGEGEVEN A POSTERIORI"
PL "WYSTAWIONE RETROSPEKTYWNIE"
PT "EMITIDO A POSTERIORI"
SL "IZDANO NAKNADNO"
SK "VYDANÉ DODATOČNE"
FI "ANNETTU JÄLKIKÄTEEN"
SV "UTFÄRDAT I EFTERHAND"
AR [pic]
2. Article 19 (2) shall be replaced by the following :
(…)
The duplicate issued in this way must be endorsed with one of the following words:
ES "DUPLICADO"
CS "DUPLIKÁT"
DA "DUPLIKAT"
DE "DUPLIKAT"
ET "DUPLIKAAT"
EL "ΑΝΤΙΓΡΑΦΟ"
EN "DUPLICATE"
FR "DUPLICATA"
IT "DUPLICATO"
LV "DUBLIKĀTS"
LT "DUBLIKATAS"
HU "MÁSODLAT"
MT "DUPLIKAT"
NL "DUPLICAAT"
PL "DUPLIKAT"
PT "SEGUNDA VIA"
SL "DVOJNIK"
SK "DUPLIKÁT"
FI "KAKSOISKAPPALE"
SV "DUPLIKAT"
AR [pic]
3. Annex V shall be replaced by the following:
Spanish version
El exportador de los productos incluidos en el presente documento (autorización aduanera n° .. …(1).) declara que, salvo indicación en sentido contrario, estos productos gozan de un origen preferencial . …(2).
Czech version
Vývozce výrobků uvedených v tomto dokumentu (číslo povolení …(1)) prohlašuje, že kromě zřetelně označených, mají tyto výrobky preferenční původ v …(2).
Danish version
Eksportøren af varer, der er omfattet af nærværende dokument, (toldmyndighedernes tilladelse nr. ...(1)), erklærer, at varerne, medmindre andet tydeligt er angivet, har præferenceoprindelse i ...(2).
German version
Der Ausführer (Ermächtigter Ausführer; Bewilligungs-Nr. ...(1)) der Waren, auf die sich dieses Handelspapier bezieht, erklärt, dass diese Waren, soweit nicht anders angegeben, präferenzbegünstigte ...(2) Ursprungswaren sind.
Estonian version
Käesoleva dokumendiga hõlmatud toodete eksportija (tolliameti kinnitus nr. ...(1)) deklareerib, et need tooted on ...(2) sooduspäritoluga, välja arvatud juhul kui on selgelt näidatud teisiti.
Greek version
Ο εξαγωγέας των προϊόντων που καλύπτονται από το παρόν έγγραφο (άδεια τελωνείου υπ΄αριθ. ...(1)) δηλώνει ότι, εκτός εάν δηλώνεται σαφώς άλλως, τα προϊόντα αυτά είναι προτιμησιακής καταγωγής ...(2).
English version
The exporter of the products covered by this document (customs authorization No ...(1)) declares that, except where otherwise clearly indicated, these products are of ...(2) preferential origin.
French version
L'exportateur des produits couverts par le présent document (autorisation douanière n° ...(1)) déclare que, sauf indication claire du contraire, ces produits ont l'origine préférentielle ... (2)).
Italian version
L'esportatore delle merci contemplate nel presente documento (autorizzazione doganale n. ...(1)) dichiara che, salvo indicazione contraria, le merci sono di origine preferenziale ...(2).
Latvian version
Eksportētājs produktiem, kuri ietverti šajā dokumentā (muitas pilnvara Nr. …(1)), deklarē, ka, iznemot tur, kur ir citādi skaidri noteikts, šiem produktiem ir priekšrocību izcelsme no …(2).
Lithuanian version
Šiame dokumente išvardintų prekių eksportuotojas (muitinès liudijimo Nr …(1)) deklaruoja, kad, jeigu kitaip nenurodyta, tai yra …(2) preferencinès kilmés prekés.
Hungarian version
A jelen okmányban szereplő áruk exportőre (vámfelhatalmazási szám: …(1)) kijelentem, hogy eltérő jelzés hianyában az áruk kedvezményes …(2) származásúak.
Maltese version
L-esportatur tal-prodotti koperti b’dan id-dokument (awtorizzazzjoni tad-dwana nru. …(1)) jiddikjara li, ħlief fejn indikat b’mod ċar li mhux hekk, dawn il-prodotti huma ta’ oriġini preferenzjali …(2).
Dutch version
De exporteur van de goederen waarop dit document van toepassing is (douanevergunning nr. ...(1)), verklaart dat, behoudens uitdrukkelijke andersluidende vermelding, deze goederen van preferentiële ... oorsprong zijn (2).
Polish version
Eksporter produktów objętych tym dokumentem (upoważnienie władz celnych nr …(1)) deklaruje, że z wyjątkiem gdzie jest to wyraźnie określone, produkty te mają …(2) preferencyjne pochodzenie.
Portuguese version
O exportador dos produtos cobertos pelo presente documento (autorização aduaneira n°. ...(1)), declara que, salvo expressamente indicado em contrário, estes produtos são de origem preferencial ...(2).
Slovenian version
Izvoznik blaga, zajetega s tem dokumentom (pooblastilo carinskih organov št …(1)) izjavlja, da, razen če ni drugače jasno navedeno, ima to blago preferencialno …(2) poreklo.
Slovak version
Vývozca výrobkov uvedených v tomto dokumente (číslo povolenia …(1)) vyhlasuje, že okrem zreteľne označených, majú tieto výrobky preferenčný pôvod v …(2).
Finnish version
Tässä asiakirjassa mainittujen tuotteiden viejä (tullin lupa n:o ...(1)) ilmoittaa, että nämä tuotteet ovat, ellei toisin ole selvästi merkitty, etuuskohteluun oikeutettuja ... alkuperätuotteita (2).
Swedish version
Exportören av de varor som omfattas av detta dokument (tullmyndighetens tillstånd nr. ...(1)) försäkrar att dessa varor, om inte annat tydligt markerats, har förmånsberättigande ... ursprung (2).
Arabic version
[pic]
CHAPTER TWO: TRANSITIONAL PROVISIONS
Article 4 (Proofs of Origin and administrative cooperation)
1. Proofs of origin properly issued by either Lebanon or a new Member State in the framework of preferential agreements or autonomous arrangements applied between them shall be accepted in the respective countries under this Protocol, provided that:
(a) the acquisition of such origin confers preferential tariff treatment on the basis of either the preferential tariff measures contained in the EU-Lebanon Interim Agreement or in the Community System of Generalised Preferences;
(b) the proof of origin and the transport documents were issued no later than the day before the date of accession;
(c) the proof of origin is submitted to the customs authorities within the period of four months from the date of accession.
Where goods were declared for importation in either Lebanon or a new Member State, prior to the date of accession, under preferential agreements or autonomous arrangements applied between Lebanon and that new Member State at that time, proof of origin issued retrospectively under those agreements or arrangements may also be accepted provided that it is submitted to the customs authorities within the period of four months from the date of accession.
2. Lebanon and the new Member States are authorised to retain the authorisations with which the status of “approved exporters” has been granted in the framework of preferential agreements or autonomous arrangements applied between them, provided that:
(a) such a provision is also provided for in the Interim agreement concluded prior to the date of accession between the Lebanon and the Community; and
(b) the approved exporter apply the rules of origin in force under the Interim agreement.
These authorisations shall be replaced no later than one year after the date of accession, by new authorisations issued under the conditions of the Interim Agreement.
3. Requests for subsequent verification of proof of origin issued under the preferential agreements or autonomous arrangements referred to in paragraphs 1 and 2 above shall be accepted by the competent customs authorities of either Lebanon or the Member States for a period of three years after the issue of the proof of origin concerned and may be made by those authorities for a period of three years after acceptance of the proof of origin submitted to those authorities in support of an import declaration.
Article 5 (Goods in transit)
1. The provisions of the Agreement may be applied to goods exported from either Lebanon to one of the new Member States or from one of the new Member States to Lebanon, which comply with the provisions of Protocol 4 and that on the date of accession are either en route or in temporary storage, in a customs warehouse or in a free zone in Lebanon or in that new Member State.
2. Preferential treatment may be granted in such cases, subject to the submission to the customs authorities of the importing country, within four months of the date of accession, of a proof of origin issued retrospectively by the customs authorities of the exporting country.
GENERAL AND FINAL PROVISIONS
Article 6
This Protocol shall form an integral part of the Interim Agreement.
Article 7
1. This Protocol shall be approved by the Council of the European Union and by the Republic of Lebanon in accordance with their own procedures.
2. The Parties shall notify each other of the accomplishment of the corresponding procedures referred to in the preceding paragraph. The instruments of approval shall be deposited with the General Secretariat of the Council of the European Union.
Article 8
1. This Protocol shall enter into force on the first day of the first month following the date of the deposit of the last instrument of approval.
2. This Protocol shall apply provisionally as from 1 May 2004.
Article 9
This Protocol is drawn up in duplicate in each of the official languages of the Contracting Parties, each of these texts being equally authentic.
Article 10
The text of the Interim Agreement, including the Annexes and Protocols formingan integral part thereof, and the Final Act together with the declarations annexed thereto shall be drawn up in Czech, Estonian, Hungarian, Latvian, Lithuanian, Maltese, Polish, Slovak an Slovenian languages and these texts shall be authentic in the same way as the original texts. The Co-operation Council shall approve these texts.
FOR THE EUROPEAN COMMUNITY
FOR THE REPUBLIC OF LEBANON
