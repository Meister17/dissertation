Commission Regulation (EC) No 2157/2005
of 23 December 2005
setting out the licence fees applicable in 2006 to Community vessels fishing in Greenland waters
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1245/2004 of 28 June 2004 on the conclusion of the Protocol modifying the fourth Protocol laying down the conditions relating to fishing provided for in the Agreement on fisheries between the European Economic Community, on the one hand, and the Government of Denmark and the local Government of Greenland, on the other [1], and in particular the second paragraph of Article 4 thereof,
Whereas:
(1) Commission Regulation (EC) No 1245/2004 provides that owners of Community vessels who receive a licence for a Community vessel authorised to fish in waters in the exclusive economic zone of Greenland are to pay a licence fee in accordance with Article 11(5) of the fourth Protocol.
(2) Commission Regulation (EC) No 2140/2004 of 15 December 2004 laying down detailed rules for the application of Regulation No 1245/2004 as regards applications for fisheries licences in waters in the exclusive economic zone of Greenland [2] implements an Administrative Arrangement on fisheries licences as set out in Article 11(5) of the fourth Protocol.
(3) Part B.4 of the Administrative Agreement specifies that license fees for 2006 are to be fixed by an annex to that Arrangement and based on 3 % of the price per tonne per species.
(4) It is appropriate to set out in this Regulation licence fees for 2006, which were agreed by the Community and Greenland on 12 December 2005 in an annex to the Administrative Arrangement.
(5) The measures provided for in this Regulation are in accordance with the opinion of the Committee on fisheries and aquaculture,
HAS ADOPTED THIS REGULATION:
Article 1
The licence fees for 2006 for Community vessels authorised to fish in waters in the exclusive economic zone of Greenland shall be as set out in the Annex to the Administrative Agreement referred to in Regulation (EC) No 2140/2004.
The text of the Annex to the Administrative Agreement is attached to this Regulation.
Article 2
This Regulation shall enter into force on the seventh day following that of its publication in the Official Journal of the European Union.
It shall apply from 1 January 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 23 December 2005.
For the Commission
Joe Borg
Member of the Commission
[1] OJ L 237, 8.7.2004, p. 1.
[2] OJ L 369, 16.12.2004, p. 49.
--------------------------------------------------
ANNEX
The licence fees for 2006 are as follows:
Species | EUR per tonne |
Redfish | 42 |
Greenland Halibut | 77 |
Shrimp | 64 |
Atlantic Halibut | 85 |
Capelin | 3 |
Roundnose Grenadier | 19 |
Snowcrab | 122 |
--------------------------------------------------
