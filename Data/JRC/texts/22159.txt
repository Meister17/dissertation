COMMISSION REGULATION (EC) No 345/97 of 26 February 1997 amending Article 3 of Regulation (EEC) No 207/93 defining the content of Annex VI to Council Regulation (EEC) No 2092/91 on organic production of agricultural products and indications referring thereto on agricultural products and foodstuffs and laying down detailed rules for implementing the provisions of Article 5 (4) thereof
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2092/91 of 24 June 1991 on organic production of agricultural products and indications referring thereto on agricultural products and foodstuffs (1), as last amended by Commission Regulation (EC) No 418/96 (2), and in particular Article 5 (3) (b), (4), (5a) (b) and (7) thereof,
Whereas Council Regulation (EC) No 1935/95 (3) provides, for the categories established in Articles 5 (3) and 5a of Regulation (EEC) No 2092/91, that the ingredients of agricultural origin not produced in accordance with the rules laid down in Article 6 or not imported from third countries under the arrangements laid down in Article 11, have to be included in Annex VI, Section C or have to be provisionally authorized by a Member State;
Whereas the procedure established in Article 3 of Commission Regulation (EEC) No 207/93 (4) has to be revised in the light of the experience gained and of developments concerning the availability on the Community market of certain organically produced ingredients of agricultural origin;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Committee referred to in Article 14 of Regulation (EEC) No 2092/91,
HAS ADOPTED THIS REGULATION:
Article 1
Article 3 of Regulation (EEC) No 207/93 is replaced by the following:
'Article 3
1. As long as an ingredient of agricultural origin has not been included in Section C of Annex VI to Regulation (EEC) No 2092/91, that ingredient may be used according to the derogation provided for in Article 5 (3) (b) and (5a) (b) of that Regulation on the following conditions:
(a) that the operator has notified to the competent authority of the Member State all the requisite evidence showing that the ingredient concerned satisfies the requirements of Article 5 (4); and
(b) that the competent authority of the Member State has provisionally authorized, in accordance with the requirements of Article 5 (4), the use for a maximum period of three months after having verified that the operator has taken the necessary contacts with the other suppliers in the Community to ensure himself on the unavailability of the ingredients concerned with the required quality requirements; the Member State may prolong this authorization maximum three times for seven months each.
2. Where an authorization as referred to in paragraph 1 has been granted, the Member State shall immediately notify to the other Member States and to the Commission, the following information:
(a) the date of the authorization;
(b) the name and, where necessary, the precise description and quality requirements of the ingredient of agricultural origin concerned;
(c) the quantities that are required and the justification for those quantities;
(d) the reasons for, and expected period of, the shortage;
(e) the date on which the Member State sends this notification to the other Member States and the Commission;
(f) the ultimate date for comments from the Member States and/or the Commission, which must be at least 30 days after the date of notification referred to in (e).
3. If the comments submitted, within the 30 days after the date of notification, by any Member State to the Commission and to the Member State which granted the authorization, show that supplies are available during the period of the shortage, the Member State shall consider withdrawal of the authorization or reducing the envisaged period of validity, and shall inform the Commission and the other Member States of the measures it has taken, within 15 days from the date of receipt of the information.
4. In case of a prolongation as referred to in paragraph 1 (b), the procedures of paragraphs 2 and 3, will apply.
5. At the request of a Member State or at the Commission's initiative, the matter shall be submitted for examination to the Committee referred to in Article 14 of the Regulation. It may be decided, in accordance with the procedure laid down in Article 14, that a granted authorization shall be withdrawn or its period of validity amended, or where appropriate, that the ingredient concerned be included in Section C of Annex VI.`
Article 2
This Regulation shall enter into force on the 30th day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 26 February 1997.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ No L 198, 22. 7. 1991, p. 1.
(2) OJ No L 59, 8. 3. 1996, p. 10.
(3) OJ No L 186, 5. 8. 1995, p. 1.
(4) OJ No L 25, 2. 2. 1993, p. 5.
