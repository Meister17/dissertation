COUNCIL DECISION of 8 February 1993 on the conclusion of an Agreement on trade and economic cooperation between the European Economic Community and Mongolia
(93/101/EEC)THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Articles 113 and 235 thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament (1),
Whereas the Agreement on trade and economic cooperation between the European Economic Community and Mongolia should be approved,
HAS DECIDED AS FOLLOWS:
Article 1
The Agreement on trade and economic cooperation between the European Economic Community and Mongolia is hereby approved on behalf of the Community.
The text of the Agreement is attached to this Decision.
Article 2
The President of the Council shall, on behalf of the Community, give the notification provided for in Article 15 of the Agreement (2).
Article 3
The Community shall be represented on the Joint Committee set up under Article 13 of the Agreement by the Commission, assisted by representatives of the Member States.
Article 4
This Decision shall be published in the Official Journal of the European Communities.
Done at Brussels, 8 February 1993.
For the Council
The President
J. TROEJBORG
(1) Opinion delivered on 22 January 1993 (not yet published in the Official Journal).
(2) The date of entry into force of the Agreement will be published in the Official Journal of the European Communities by the General Secretariat of the Council.
