1.
European Parliament decision on the discharge to the Administrative Director of Eurojust in respect of the implementation of its budget for the financial year 2003 (N6-0220/2004 — C6-0239/2004 — 2004/2063(DEC))
The European Parliament,
- having regard to the Court of Auditors' report on the annual accounts of Eurojust for the financial year 2003, together with Eurojust's replies [1],
- having regard to the Council's recommendation of 8 March 2005 (6856/2005 — C6-0063/2005),
- having regard to the EC Treaty, and in particular Article 276 thereof,
- having regard to Council Regulation (EC, Euratom) No 1605/2002 of 25 June 2002 on the Financial Regulation applicable to the general budget of the European Communities [2] and in particular Article 185 thereof, and to Council Decision 2003/659/JHA of 18 June 2003 amending Decision 2002/187/JHA setting up Eurojust with a view to reinforcing the fight against serious crime [3] and in particular Article 36 thereof,
- having regard to Commission Regulation (EC, Euratom) No 2343/2002 of 19 November 2002 on the framework Financial Regulation for the bodies referred to in Article 185 of Council Regulation (EC, Euratom) No 1605/2002 on the Financial Regulation applicable to the general budget of the European Communities [4] and, in particular, Article 94 of Regulation (EC, Euratom) No 2343/2002,
- having regard to Rules 70 and 71 of and Annex V to its Rules of Procedure,
- having regard to the Report of the Committee on Budgetary Control and the opinion of the Committee on Civil Liberties, Justice and Home Affairs (A6-0074/2005),
1. Gives discharge to the Administrative Director of Eurojust in respect of the implementation of its budget for the financial year 2003;
2. Records its comments in the accompanying resolution;
3. Instructs its President to forward this decision and the accompanying resolution to the Administrative Director of Eurojust, the Council, the Commission and the Court of Auditors, and to have them published in the Official Journal of the European Union (L series).
[1] OJ C 324, 30.12.2004, p. 61.
[2] OJ L 248, 16.9.2002, p. 1.
[3] OJ L 245, 29.9.2003, p. 44.
[4] OJ L 357, 31.12.2002, p. 72.
--------------------------------------------------
