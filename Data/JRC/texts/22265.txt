COMMISSION REGULATION (EC) No 143/97 of 27 January 1997 concerning the third list of priority substances as foreseen under Council Regulation (EEC) No 793/93 (Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to the Council Regulation (EEC) No 793/93 of 23 March 1993 on the evaluation and control of the risks of existing substances (1) and in particular Articles 8 and 10 thereof,
Whereas Regulation (EEC) No 793/93 envisages a system of evaluation and control of the risks of existing substances and whereas in order to undertake the risk evaluation of such substances, it is appropriate to identify priority substances requiring attention;
Whereas in consequence, Article 8 of Regulation (EEC) No 793/93 requires that the Commission shall draw up a list of priority substances; whereas Article 8 further indicates the factors which shall be taken into account in drawing up the said list;
Whereas Article 10 of Regulation (EEC) No 793/93 foresees that for each substance on the priority list a Member State shall be given responsibility for its evaluation and whereas the allocation of substances shall ensure a fair sharing of the burden between Member States;
Whereas a first and a second priority list have been adopted by Commission Regulations (EC) No 1179/94 of May 25 1994 (2) and No 2268/95 of 27 September 1995 (3);
Whereas the provisions of this Regulation are in accordance with the opinion of the Committee established under Article 15 of Regulation (EEC) No 793/93,
HAS ADOPTED THIS REGULATION:
Article 1
1. The third list of priority substances as foreseen in Article 8 (1) of Regulation (EEC) No 793/93 is set out in the Annex to this Regulation.
2. This list of priority substances also indicates the Member State which is responsible for each of the substances.
Article 2
This Regulation shall enter into force on its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 27 January 1997.
For the Commission
Ritt BJERREGAARD
Member of the Commission
(1) OJ No L 84, 5. 4. 1993, p. 1.
(2) OJ No L 131, 26. 5. 1994, p. 3.
(3) OJ No L 231, 28. 9. 1995, p. 18.
ANNEX
>TABLE>
