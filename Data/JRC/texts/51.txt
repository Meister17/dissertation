ELEVENTH COUNCIL DIRECTIVE of 21 December 1989 concerning disclosure requirements in respect of branches opened in a Member State by certain types of company governed by the law of another State (89/666/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 54 thereof,
Having regard to the proposal from the Commission (1),
In cooperation with the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas in order to facilitate the exercise of the freedom of establishment in respect of companies covered by Article 58 of the Treaty, Article 54 (3) (g) and the general programme on the elimination of restrictions on the freedom of establishment require coordination of the safeguards required of companies and firms in the Member States for the protection of the interests of members and others;
Whereas hitherto this coordination has been effected in respect of disclosure by the adoption of the First Directive 68/151/EEC (4) covering companies with share capital, as last amended by the 1985 Act of Accession; whereas it was continued in the field of accounting by the Fourth Directive 78/660/EEC (5) on the annual accounts of certain types of companies, as last amended by the 1985 Act of Accession, the Seventh Directive 83/349/EEC (6) on consolidated accounts, as amended by the 1985 Act of Accession, and the Eighth Directive 84/253/EEC (7) on the persons responsible for carrying out the statutory audits of accounting documents;
Whereas these Directives apply to companies as such but do not cover their branches; whereas the opening of a branch, like the creation of a subsidiary, is one of the possibilities currently open to companies in the exercise of their right of establishment in another Member State;
Whereas in respect of branches the lack of coordination, in particular concerning disclosure, gives rise to some
disparities, in the protection of shareholders and third parties, between companies which operate in other Member States by opening branches and those which operate there by creating subsidiaries;
Whereas in this field the differences in the laws of the Member States may interfere with the exercise of the right of establishment; whereas it is therefore necessary to eliminate such differences in order to safeguard, inter alia, the exercise of that right;
Whereas to ensure the protection of persons who deal with companies through the intermediary of branches, measures in respect of disclosure are required in the Member State in which a branch is situated; whereas, in certain respects, the economic and social influence of a branch may be comparable to that of a subsidiary company, so that there is public interest in disclosure of the company at the branch; whereas to effect such disclosure it is necessary to make use of the procedure already instituted for companies with share capital within the Community;
Whereas such disclosure relates to a range of important documents and particulars and amendments thereto;
Whereas such disclosure, with the exception of the powers of representation, the name and legal form and the winding-up of the company and the insolvency proceedings to which it is subject, may be confined to information concerning a branch itself together with a reference to the register of the company of which that branch is part, since under existing Community rules all information covering the company as such is available in that register;
Whereas national provisions in respect of the disclosure of accounting documents relating to a branch can no longer be justified following the coordination of national law in respect of the drawing up, audit and disclosure of companies' accounting documents; whereas it is accordingly sufficient to disclose, in the register of the branch, the accounting documents as audited and disclosed by the company;
Whereas letters and order forms used by a branch must give at least the same information as letters and order forms used by the company, and state the register in which the branch is entered;
Whereas to ensure that the purposes of this Directive are fully realized and to avoid any discrimination on the basis of a
company's country of origin, this Directive must also cover branches opened by companies governed by the law of non-member countries and set up in legal forms comparable to companies to which Directive 68/151/EEC applies; whereas for these branches it is necessary to apply certain provisions different from those that apply to the branches of companies governed by the law of other Member States since the Directives referred to above do not apply to companies from non-member countries;
Whereas this Directive in no way affects the disclosure requirements for branches under other provisions of, for example, employment law on workers' rights to information and tax law, or for statistical purposes;
HAS ADOPTED THIS DIRECTIVE:
SECTION I
Branches of companies from other Member States
Article 1
1. Documents and particulars relating to a branch opened in a Member State by a company which is governed by the law of another Member State and to which Directive 68/151/EEC applies shall be disclosed pursuant to the law of the Member State of the branch, in accordance with Article 3 of that Directive.
2. Where disclosure requirements in respect of the branch differ from those in respect of the company, the branch's disclosure requirements shall take precedence with regard to transactions carried out with the branch.
Article 2
1. The compulsory disclosure provided for in Article 1 shall cover the following documents and particulars only:
(a) the address of the branch;
(b) the activities of the branch;
(c) the register in which the company file mentioned in Article 3 of Council Directive 68/151/EEC is kept, together with the registration number in that register;
(d)
the name and legal form of the company and the name of the branch if that is different from the name of the company;
(e)
the appointment, termination of office and particulars of the persons who are authorized to represent the company in dealings with third parties and in legal proceedings;
- as a company organ constituted pursuant to law or as members of any such organ, in accordance with
the disclosure by the company as provided for in Article 2 (1) (d) of Directive 68/151/EEC,
- as permanent representatives of the company for the activities of the branch, with an indication of the extent of their powers;
(f)
the winding-up of the company, the appointment of liquidators, particulars concerning them and their powers and the termination of the liquidation in accordance with disclosure by the company as provided for in Article 2 (1) (h), (j) and (k) of Directive 68/151/EEC,
- insolvency proceedings, arrangements, compositions, or any analogous proceedings to which the company is subject;
(g)
the accounting documents in accordance with Article 3;
(h)
the closure of the branch.
2. The Member State in which the branch has been opened may provide for the disclosure, as referred to in Article 1, of
(a) the signature of the persons referred to in paragraph
1 (e) and (f) of this Article;
(b) the instruments of constitution and the memorandum and articles of association if they are contained in a separate instrument in accordance with Article 2 (1) (a), (b) and (c) of Directive 68/151/EEC, together with amendments to those documents;
(c) an attestation from the register referred to in paragraph 1 (c) of this Article relating to the existence of the company;
(d) an indication of the securities on the company's property situated in that Member State, provided such disclosure relates to the validity of those securities.
Article 3
The compulsory disclosure provided for by Article 2 (1) (g) shall be limited to the accounting documents of the company as drawn up, audited and disclosed pursuant to the law of the Member State by which the company is governed in accordance with Directives 78/660/EEC, 83/349/EEC and 84/253/EEC.
Article 4
The Member State in which the branch has been opened may stipulate that the documents referred to in Article 2 (2) (b) and Article 3 must be published in another official language of the Community and that the translation of such documents must be certified.
Article 5
Where a company has opened more than one branch in a Member State, the disclosure referred to in Article 2 (2) (b)
and Article 3 may be made in the register of the branch of the company's choice.
In this case, compulsory disclosure by the other branches shall cover the particulars of the branch register of which disclosure was made, together with the number of that branch in that register.
Article 6
The Member States shall prescribe that letters and order forms used by a branch shall state, in addition to the information prescribed by Article 4 of Directive 68/151/EEC, the register in which the file in respect of the branch is kept together with the number of the branch in that register.
SECTION II
Branches of companies from third countries
Article 7
1. Documents and particulars concerning a branch opened in a Member State by a company which is not governed by the law of a Member State but which is of a legal form comparable with the types of company to which Directive 68/151/EEC applies shall be disclosed in accordance with the law of the Member State of the branch as laid down in Article 3 of that Directive.
2. Article 1 (2) shall apply.
Article 8
The compulsory disclosure provided for in Article 7 shall cover at least the following documents and particulars:
(a) the address of the branch;
(b) the activities of the branch;
(c) the law of the State by which the company is governed;
(d) where that law so provides, the register in which the company is entered and the registration number of the company in that register;
(e)
the instruments of constitution, and memorandum and articles of association if they are contained in a separate instrument, with all amendments to these documents;
(f)
the legal form of the company, its principal place of business and its object and, at least annually, the amount of subscribed capital if these particulars are not given in the documents referred to in subparagraph (e);
(g)
the name of the company and the name of the branch if that is different from the name of the company;
(h)
the appointment, termination of office and particulars of the persons who are authorized to represent the company in dealings with third parties and in legal proceedings:
- as a company organ constituted pursuant to law or as members of any such organ,
- as permanent representatives of the company for the activities of the branch.
The extent of the powers of the persons authorized to represent the company must be stated, together with whether they may do so alone or must act jointly;
(i)
- the winding-up of the company and the appointment of liquidators, particulars concerning them and their powers and the termination of the liquidation;
- insolvency proceedings, arrangements, compositions or any analogous proceedings to which the company is subject;
(j)
the accounting documents in accordance with Article 7;
(k)
the closure of the branch.
Article 9
1. The compulsory disclosure provided for by Article 8 (1) (j) shall apply to the accounting documents of the company as drawn up, audited and disclosed pursuant to the law of the State which governs the company. Where they are not drawn up in accordance with or in a manner equivalent to Directives 78/660/EEC and 83/349/EEC, Member States may require that accounting documents relating to the activities of the branch be drawn up and disclosed.
2. Articles 4 and 5 shall apply.
Article 10
The Member States shall prescribe that letters and order forms used by a branch state the register in which the file in respect of the branch is kept together with the number of the branch in that register. Where the law of the State by which the company is governed requires entry in a register, the register in which the company is entered, and the registration number of the company in that register must also be stated.
SECTION III
Indication of branches in the company's annual report
Article 11
The following subparagraph is added to Article 46 (2) of Directive 78/660/EEC:
'(e) the existence of branches of the company'.
SECTION IV
Transitional and final provisions
Article 12
The Member States shall provide for appropriate penalties in the event of failure to disclose the matters set out in Articles 1, 2, 3, 7, 8 and 9 and of omission from letters and order forms of the compulsory particulars provided for in Articles 6 and 10.
Article 13
Each Member State shall determine who shall carry out the disclosure formalities provided for in this Directive.
Article 14
1. Articles 3 and 9 shall not apply to branches opened by credit institutions and financial institutions covered by Directive 89/117/EEC (8).
2. Pending subsequent coordination, the Member States need not apply Articles 3 and 9 to branches opened by insurance companies.
Article 15
Article 54 of Directive 78/660/EEC and Article 48 of Directive 83/349/EEC shall be deleted.
Article 16
1. Member States shall adopt the laws, regulations and administrative provisions necessary to comply with this
Directive not later than 1 January 1992. They shall forthwith inform the Commission thereof.
2. Member States shall stipulate that the provisions referred to in paragraph 1 shall apply from 1 January 1993 and, with regard to accounting documents, shall apply for the first time to annual accounts for the financial year beginning on 1 January 1993 or during 1993.
3. Member States shall communicate to the Commission the texts of the provisions of national law which they adopt in the field covered by this Directive.
Article 17
The Contact Committee set up pursuant to Article 52 of Directive 78/660/EEC shall also:
(a) facilitate, without prejudice to Articles 169 and 170 of the Treaty, the harmonized application of this Directive, through regular meetings dealing, in particular, with practical problems arising in connection with its application;
(b) advise the Commission, if necessary, on any additions or amendments to this Directive.
Article 18
This Directive is addressed to the Member States.
Done at Brussels, 21 December 1989.
For the Council
The President
E. CRESSON
(1) OJ No C 105, 21. 4. 1988, p. 6.
(2) OJ No C 345, 21. 12. 1987, p. 76 and OJ No C 256,
9. 10. 1989, p. 27.
(3) OJ No C 319, 30. 11. 1987, p. 61.
(4) OJ No L 65, 14. 3. 1968, p. 8.
(5) OJ No L 222, 14. 8. 1978, p. 11.
(6) OJ No L 193, 18. 7. 1983, p. 1.
(7) OJ No L 126, 12. 5. 1984, p. 20.(8) OJ No L 44, 16. 2. 1989, p. 40.
