Commission Decision
of 30 November 2001
amending Commission Decision 98/371/EC concerning the animal health conditions and veterinary certifications for import of fresh meat from certain European countries to take into account some aspects in relation with Poland
(notified under document number C(2001) 3818)
(Text with EEA relevance)
(2001/849/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 72/462/EEC of 12 December 1972 on health and veterinary inspection problems upon importation of bovine, ovine and caprine animals and swine, fresh meat products from third countries(1), as last amended by Directive 97/79/EC(2), and in particular Articles 14, 15 and 16 thereof,
Whereas:
(1) The animal health conditions and veterinary certification for imports of fresh meat from certain European countries are laid down in Commission Decision 98/371/EC(3), as last amended by Decision 2001/774/EC(4).
(2) Imports of fresh meat for human consumption of domestic animals of the porcine species from Poland were not authorised due to animal health reasons, concerning in particular the control of classical swine fever.
(3) The Polish authorities have submitted a request to be authorised to export pig meat to the Community and have supported their request with information concerning the health status of the swine in Poland and the control of classical swine fever.
(4) In May 2001 a Commission veterinary mission was carried out to assess the animal health situation in Poland, and in particular concerning the classical swine fever situation.
(5) Based on the report of the mission and the further information provided by the Polish authorities, it appears that the health status of the swine in Poland in relation to classical swine fever is satisfactory.
(6) Therefore, it is appropriate to authorise Poland to export pig meat to the Community, subject to certain conditions in relation to the use of catering waste for the feeding to swine. The Polish authorities have committed themselves that, for the purpose of exporting pig meat, a list of pig holdings kept under regular veterinary supervisions and where appropriate controls are carried out to exclude any use of catering waste for feeding to pigs will be established.
(7) Decision 98/371/EC must be amended accordingly.
(8) The measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
Decision 98/371/EC is amended as follows:
(a) Annex II is replaced by Annex I to the present Decision;
(b) Annex IV is replaced by Annex II to the present Decision.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 30 November 2001.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 302, 31.12.1972, p. 28.
(2) OJ L 24, 30.1.1998, p. 31.
(3) OJ L 170, 16.6.1998, p. 16.
(4) OJ L 291, 8.11.2001, p. 48.
ANNEX I
"ANNEX II
ANIMAL HEALTH GUARANTEES TO BE REQUESTED ON CERTIFICATION OF FRESH MEAT
>TABLE>
NB:
Imports of fresh meat for human consumption are not allowed unless a programme of control of residues in the exporting third country has been approved by the Commission."
ANNEX II
"ANNEX IV
SUPPLEMENTARY GUARANTEES TO BE PROVIDED BY EXPORTING TERRITORY WHEN REQUIRED IN ANNEX II IN APPLICATION OF ARTICLE 2(2)
a: I, the undersigned official veterinarian, hereby declare that the fresh meat described above is obtained from porcine animals coming from holdings where an undertaking has been received that pigs are not fed with catering waste(1), that are subject to official controls and are included in the list established by the competent authority for the purpose of exporting pig meat to the European Union.
(1) Catering waste means: all waste from food intended for human consumption from restaurants, catering facilities or kitchens, including industrial kitchens and household kitchens of the farmer or persons tending pigs."
