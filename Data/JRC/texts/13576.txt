Judgment of the Court of First Instance of 14 December 2005 — Fedon & Figli and Others v Council and Commission
(Case T-135/01) [1]
Parties
Applicants: Giorgio Fedon & Figli SpA (Vallesella di Cadore, Italy), Fedon Srl (Pieve d'Alpago, Italy) and Fedon America, Inc. (Wilmington, Delaware, United States) (represented by: I. Van Bael, A. Cevese and F. Di Gianni, lawyers)
Defendants: Council of the European Union (represented by: S. Marquardt and F. Ruggeri Laderchi, Agents) and Commission of the European Communities (initially represented by: P. Kuijper, E. Righini, V. Di Bucci and B. Jansen and then by: P. Kuijper, E. Righini and V. Di Bucci, Agents)
Application for
compensation in respect of the damage allegedly caused by the levying by the United States of America of increased customs duty on the applicants' imports of spectacle cases, as authorised by the Dispute Settlement Body of the World Trade Organisation (WTO), following a finding that the Community regime governing the import of bananas was incompatible with the agreements and understandings annexed to the Agreement establishing the WTO
Operative part of the judgment
The Court:
1. Dismisses the action;
2. Orders the applicants to bear their own costs and to pay those of the Council and the Commission.
[1] OJ C 275, 29.9.2001.
--------------------------------------------------
