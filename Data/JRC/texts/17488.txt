Agreement
in the form of an Exchange of Letters between the European Community and the Kingdom of Norway concerning Protocol 2 to the bilateral Free Trade Agreement between the European Economic Community and the Kingdom of Norway
A. Letter from the Community
Sir,
I have the honour to confirm that the European Community is in agreement with the "Agreed minutes" attached to this letter concerning Protocol No 2 to the bilateral free trade agreement between the European Economic Community and the Kingdom of Norway.
I should be obliged if you would confirm that the Government of the Kingdom of Norway is in agreement with the content of this letter.
Please accept, Sir, the assurance of my highest consideration.
Hecho en Bruselas, el
V Bruselu dne
Udfærdiget i Bruxelles, den
Geschehen zu Brüssel am
Brüssel,
Έγινε στις Βρυξέλλες, στις
Done at Brussels,
Fait à Bruxelles, le
Fatto a Bruxelles, addí
Briselÿ,
Priimta Briuselyje,
Kelt Brüsszelben,
Magÿmula fi Brussel,
Gedaan te Brussel,
Sporzÿdzono w Brukseli, dnia
Feito em Bruxelas,
V Bruseli,
V Bruslju,
Tehty Brysselissä
Utfärdat i Bryssel den
Întocmit la Bruxelles
Por la Comunidad Europea
Za Evropské společenství
For Det Europæiske Fællesskab
Für die Europäische Gemeinschaft
Euroopa Ühenduse nimel
Για την Ευρωπαϊκή Κοινότητα
For the European Community
Pour la Communauté européenne
Per la Comunità europea
Eiropas Kopienas vārdā
Europos bendrijos vardu
az Európai Közösség részéről
Għall-Komunità Ewropea
Voor de Europese Gemeenschap
W imieniu Wspólnoty Europejskiej
Pela Comunidade Europeia
Za Európske spoločenstvo
za Evropsko skupnost
Euroopan yhteisön puolesta
På Europeiska gemenskapens vägnar
B. Letter from Norway
Sir,
I have the honour to acknowledge receipt of your letter of today's date which reads as follows:
"I have the honour to confirm that the European Community is in agreement with the "Agreed minutes" attached to this letter concerning Protocol No 2 to the bilateral free trade agreement between the European Economic Community and the Kingdom of Norway.
I should be obliged if you would confirm that the Government of the Kingdom of Norway is in agreement with the content of this letter."
I have the honour to confirm that my Government is in agreement with the content of your letter and the proposed date of entry into force of the amendments.
Please accept, Sir, the assurance of my highest consideration.
Done at Brussels,
Hecho en Bruselas, el
V Bruselu dne
Udfærdiget i Bruxelles, den
Geschehen zu Brüssel am
Brüssel,
Έγινε στις Βρυξέλλες, στις
Fait à Bruxelles, le
Fatto a Bruxelles, addí
Briselÿ,
Priimta Briuselyje,
Kelt Brüsszelben,
Magÿmula fi Brussel,
Gedaan te Brussel,
Sporzÿdzono w Brukseli, dnia
Feito em Bruxelas,
V Bruseli,
V Bruslju,
Tehty Brysselissä
Utfärdat i Bryssel den
Întocmit la Bruxelles
For the Government of the Kingdom of Norway
Por el Gobierno del Reino de Noruega
Za vládu Norského království
På vegne af Kongeriget Norges regering
Im Namen des Königreichs Norwegen
Norra Kuningriigi valitsuse nimel
Για την Κυβέρνηση του Βασιλείου της Νορβηγίας
Pour le gouvernement du Royaume de Norvège
Per il Governo del Regno di Norvegia
Norvēģijas Karalistes valdības vārdā
Norvegijos Karalystės Vyriausybės vardu
A Norvég Királyság Kormánya részéről
Għall-Gvern tar-Renju tan-Norveġja
Voor de Regering van het Koninkrijk Noorwegen
W imieniu Rządu Królestwa Norwegii
Pelo Governo do Reino da Noruega
Za vládu Nórskeho kráľovstva
Za vlado Kraljevine Norveške
Norjan kuningaskunnan hallituksen puolesta
För Konungariket Norges regering
--------------------------------------------------
