Commission Regulation (EC) No 2147/2002
of 2 December 2002
amending Regulation (EC) No 1455/1999 laying down the marketing standard for sweet peppers
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 2200/96 of 28 October 1996 on the common organisation of the market in fruit and vegetables(1), as last amended by Regulation (EC) No 545/2002(2), and in particular Article 2(2) thereof,
Whereas:
(1) Commission Regulation (EC) No 1455/1999(3), amended by Regulation (EC) No 2706/2000(4), laid down provisions on the presentation and the marking of packages of sweet peppers.
(2) In order to ensure transparency on the world market, account should be taken of the recent modifications of the standard for sweet peppers recommended by the United Nations Economic Commission for Europe, which stipulates that packages containing peppers of different colours shall not necessarily contain the same number of sweet peppers of each colour provided packages show an appropriate marking. In addition, it is appropriate to lay down that consumer packages may not only contain sweet peppers of different colours but also sweet peppers of different types.
(3) The provisions concerning marking need to be clarified on several points, in particular with regards to the labelling of consumer packages containing sweet peppers of different types and colours.
(4) It is therefore necessary to amend Regulation (EC) No 1455/1999.
(5) In order to apply the provisions of the present Regulation, the traders have to proceed to certain technical adaptations, in particular with regard to their packing facilities. The application of the present Regulation shall therefore start after a period of a sufficient length after the date of its entry into force.
(6) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Fresh Fruit and Vegetables,
HAS ADOPTED THIS REGULATION:
Article 1
The Annex to Regulation (EC) No 1455/1999 is amended according to the Annex to the present Regulation.
Article 2
This Regulation shall enter into force on the 20th day following its publication in the Official Journal of the European Communities.
It shall apply from the first day of the third month following its entry into force.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 2 December 2002.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 297, 21.11.1996, p. 1.
(2) OJ L 84, 28.3.2002, p. 1.
(3) OJ L 167, 2.7.1999, p. 22.
(4) OJ L 311, 12.12.2000, p. 35.
ANNEX
The Annex to Regulation (EC) No 1455/1999 is amended as follows:
1. In title V (Provisions concerning presentation), item A (Uniformity), the second and third subparagraphs are replaced by the following text:
"However, a mixture of sweet peppers of different colours may be packed together, provided they are uniform in origin, quality, commercial type and size (if sized).
Consumer packages of a net weight not exceeding one kilo, may contain mixtures of sweet peppers of different colours and/or commercial types, provided they are uniform in quality, and for each colour and/or commercial type concerned, in origin."
2. Title VI (Provisions concerning marking) is modified as follows:
(a) items B and C are replaced by the following text:
"B. Nature of produce
If the contents are not visible from the outside:
- 'Sweet peppers',
- colour,
- commercial type ('elongated', 'square blunt', 'square tapering', 'flat') or name of the variety.
In the case of packages or consumer packages containing a mixture of different colours and/or commercial types of sweet pepper:
- 'Mixed peppers', or equivalent denomination,
- if the contents are not visible from the outside, colours and/or commercial types of the sweet peppers and number of pieces of each of the colours and/or commercial types concerned.
C. Origin of produce
Country of origin and, optionally, district where grown or national, regional or local place name.
In the case of consumer packages containing a mixture of sweet peppers of different colours and/or commercial types of different origins, the indication of each country of origin shall appear next to the name of the colour and/or commercial type concerned."
(b) In item D (Commercial specifications), the third indent is replaced by the following text:
"- where appropriate, 'Mini peppers', 'Baby peppers', or other appropriate term for miniature produce. Where several types of miniature produce are mixed in the same package, all products and their respective origins must be mentioned."
