Commission Decision
of 12 December 2001
setting out the arrangements for Community comparative trials and tests on propagating material of ornamental plants under Council Directive 98/56/EC
(notified under document number C(2001) 4224)
(2001/898/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 98/56/EC on the marketing of propagating material of ornamental plants(1) in particular Article 14(4) thereof,
Whereas:
(1) The abovementioned Directive provides for the necessary arrangement to be made for Community comparative trials and tests of propagating material to be carried out.
(2) Adequate representation of the samples included in the trials and tests should be ensured, at least for certain selected plants.
(3) Member States should participate in the Community comparative trials and tests, in so far as seed of the abovementioned plants are usually reproduced or marketed in their territories, in order to ensure that proper conclusion may be drawn therefrom.
(4) The Commission is responsible for making the necessary arrangements for the Community comparative trials and tests.
(5) The technical arrangements for the carrying out of the trials and tests have been made within the Standing Committee for Propagating Materials of Ornamental Plants.
(6) Community comparative trials and tests should be carried out from the year 2002 to 2004 on propagating material harvested in 2001 and the details for such trials and tests should also be set out.
(7) For the Community comparative trials and tests lasting more than one year the parts of the trials and tests following the first year should, on condition that the necessary appropriations are available, be authorised by the Commission without further reference to the Standing Committee for Propagating Materials of Ornamental Plants.
(8) The Standing Committee for Propagating Materials of Ornamental Plants has not delivered an opinion within the time-limit laid down by its Chairman,
HAS ADOPTED THIS DECISION:
Article 1
1. Community comparative trials and tests shall be carried out from the year 2002 to 2004 on propagating material of the plants listed in the Annex.
2. The maximum cost for the trials and tests for 2002 shall be as set out in the Annex.
3. All Member States shall participate in the Community comparative trials and tests in so far as seeds and propagating material of the plants listed in the Annex are usually reproduced or marketed in their territories.
4. The details of the trials and tests are set out in the Annex.
Article 2
The Commission may decide to continue the trials and tests set out in the Annex in 2003 and 2004. The maximum cost of a trial or test continued on this basis shall not exceed the amount specified in the Annex.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 12 December 2001.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 226, 13.8.1998, p. 16.
ANNEX
Trials to be carried out in 2002
>TABLE>
Trials to be carried out in 2003
>TABLE>
Trials to be carried out in 2004
>TABLE>
