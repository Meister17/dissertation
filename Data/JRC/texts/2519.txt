Commission Regulation (EC) No 545/2005
of 8 April 2005
determining the extent to which applications lodged in March 2005 for import rights in respect of frozen beef intended for processing may be accepted
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1254/1999 of 17 May 1999 on the common organisation of the market in beef and veal [1],
Having regard to Commission Regulation (EC) No 1206/2004 of 29 June 2004 opening and administering an import tariff quota for frozen beef intended for processing ( 1 July 2004 to 30 June 2005) [2], and in particular Article 5(4) thereof,
Whereas:
(1) Article 9(1) of Regulation (EC) No 1206/2004 provides, where applicable, for a further allocation of quantities not covered by licence applications submitted by 18 February 2005.
(2) Article 2 of Commission Regulation (EC) No 395/2005 of 9 March 2005 providing for reallocation of import rights under Regulation (EC) No 1206/2004 opening and providing for the administration of an import tariff quota for frozen beef intended for processing [3], establishes the quantities of frozen beef for processing which may be imported under special conditions until 30 June 2005.
(3) Article 5(4) of Regulation (EC) No 1206/2004 lays down that the quantities applied for may be reduced. The applications lodged relate to total quantities which exceed the quantities available. Under these circumstances and taking care to ensure an equitable distribution of the available quantities, it is appropriate to reduce proportionally the quantities applied for,
HAS ADOPTED THIS REGULATION:
Article 1
Every application for import rights lodged in accordance with Article 9 of Regulation (EC) No 1206/2004 shall be granted to the following extent, expressed as bone-in beef:
- 8,9347 % of the quantity requested for beef imports intended for the manufacture of "preserves" as defined by Article 2(1) of Regulation (EC) No 1206/2004,
- 50,0474 % of the quantity requested for beef imports intended for the manufacture of products as defined by Article 2(2) of Regulation (EC) No 1206/2004.
Article 2
This Regulation shall enter into force on 9 April 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 8 April 2005.
For the Commission
J. M. Silva Rodríguez
Director-General for Agriculture and Rural Development
[1] OJ L 160, 26.6.1999, p. 21. Regulation as last amended by Regulation (EC) No 1782/2003 (OJ L 270, 21.10.2003, p. 1).
[2] OJ L 230, 30.6.2004, p. 42.
[3] OJ L 63, 10.3.2005, p. 20.
--------------------------------------------------
