COUNCIL REGULATION (EEC) No 1423/78 of 20 June 1978 amending Regulation (EEC) No 2759/75 on the common organization of the market in pigmeat
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 43 thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament (1),
Whereas Article 5 (1) of Council Regulation (EEC) No 2759/75 of 29 October 1975 on the common organization of the market in pigmeat (2), as amended by Regulation (EEC) No 367/76 (3), lays down that the intervention buying-in price for pig carcases of standard quality may not be more than 92 % or less than 85 % of the basic price;
Whereas in view of the considerable variations in the production costs of pig carcases during the last few years, in the Community and on the world market, it is desirable to widen the range within which the intervention buying-in price for pig carcases may be fixed,
HAS ADOPTED THIS REGULATION:
Article 1
Article 5 (1) of Regulation (EEC) No 2759/75 shall be replaced by the following:
"1. The buying-in price for pig carcases of standard quality may not be more than 92 % or less than 78 % of the basic price."
Article 2
This Regulation shall enter into force on 1 July 1978.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Luxembourg, 20 June 1978.
For the Council
The President
P. DALSAGER (1)Opinion delivered on 16 June 1978 (not yet published in the Official Journal). (2)OJ No L 282, 1.11.1975, p. 1. (3)OJ No L 45, 21.2.1976, p. 1.
