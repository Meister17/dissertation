Decision No 2/2006 of the Community/Switzerland Air Transport Committee
of 18 October 2006
amending the Annex to the Agreement between the European Community and the Swiss Confederation on Air Transport
(2006/728/EC)
THE COMMUNITY/SWITZERLAND AIR TRANSPORT COMMITTEE,
Having regard to the Agreement between the European Community and the Swiss Confederation on Air Transport, hereinafter referred to as "the Agreement", and in particular Article 23(4) thereof,
HAS DECIDED AS FOLLOWS:
Article 1
1. The following shall be inserted after the end of point 5 (Aviation Security) of the Annex to the Agreement, as inserted by Article 1(1) of Decision No 1/2005 of the Community/Switzerland Air Transport Committee of 12 July 2005 [1]:
"6. Air Traffic Management"
2. The numbering of point 6 (Others) of the Annex to the Agreement shall be changed to 7.
Article 2
1. The following shall be inserted after the inclusion referred to in Article 1(1) of the present Decision:
"No 549/2004
Regulation (EC) No 549/2004 of the European Parliament and of the Council of 10 March 2004 laying down the framework for the creation of the single European sky (the framework Regulation).
The Commission shall enjoy towards Switzerland the powers granted to it according to Article 6, 8(1), 10, 11 and 12.
Notwithstanding the horizontal adaptation provided in the first indent of the Annex to the Agreement between the European Community and the Swiss Confederation on Air transport, the references to the "Member States" made in Article 5 of Regulation (EC) No 549/2004 or in the provisions of Decision 1999/468/EC mentioned in that provision shall not be understood to apply to Switzerland.".
2. The following shall be inserted after the inclusion referred to in Article 2(1) of the present Decision:
"No 550/2004
Regulation (EC) No 550/2004 of the European Parliament and of the Council of 10 March 2004 on the provision of air navigation services in the single European sky (the service provision Regulation).
The Commission shall enjoy towards Switzerland the powers granted to it according to Article 16 as modified hereinafter.
The text of the Regulation shall, for the purposes of the Agreement, be read with the following adaptations:
(a) Article 3 shall be amended as follows:
In paragraph 2, the words "and Switzerland" shall be inserted after the words "the Community".
(b) Article 7 shall be amended as follows:
In paragraph 1 and paragraph 6, the words "and Switzerland" shall be inserted after the words "the Community".
(c) Article 8 shall be amended as follows:
In paragraph 1, the words "and Switzerland" shall be inserted after the words "the Community".
(d) Article 10 shall be amended as follows:
In paragraph 1, the words "and Switzerland" shall be inserted after the words "the Community".
(e) Article 16 paragraph 3 shall be replaced by the following wording:
"3. The Commission shall address its decision to the Member States and inform the service provider thereof, in so far as it is legally concerned." ".
3. The following shall be inserted after the inclusion referred to in Article 2(2) of the present Decision:
"No 551/2004
Regulation (EC) No 551/2004 of the European Parliament and of the Council of 10 March 2004 on the organisation and use of the airspace in the single European sky (the airspace Regulation).
The Commission shall enjoy towards Switzerland the powers granted to it according to Article 2, 3(5) and 10.".
4. The following shall be inserted after the inclusion referred to in Article 2(3) of the present Decision:
"No 552/2004
Regulation (EC) No 552/2004 of the European Parliament and of the Council of 10 March 2004 on the interoperability of the European Air Traffic Management network (the interoperability Regulation).
The Commission shall enjoy towards Switzerland the powers granted to it according to Article 4, 7 and 10(3).
The text of the Regulation shall, for the purposes of the Agreement, be read with the following adaptations:
(a) Article 5 shall be amended as follows:
In paragraph 2, the words "or Switzerland" shall be inserted after the words "the Community".
(b) Article 7 shall be amended as follows:
In paragraph 4, the words "or Switzerland" shall be inserted after the words "the Community".
(c) Annex III shall be amended as follows:
In section 3, second and last indent, the words "or Switzerland" shall be inserted after the words "the Community" ".
5. The following shall be inserted after the inclusion referred to in Article 2(4) of the present Decision:
"No 2096/2005
Commission Regulation (EC) No 2096/2005 of 20 December 2005 laying down common requirements for the provision of air navigation services.
The Commission shall enjoy in Switzerland the powers granted to it according to Article 9.".
6. The following shall be inserted after the inclusion referred to in Article 2(5) of the present Decision:
"No 2150/2005
Commission Regulation (EC) No 2150/2005 of 23 December 2005 laying down common rules for the flexible use of airspace.".
Article 3
1. In point 3 (Technical Harmonisation) of the Annex to the Agreement, the following shall be deleted:
"No 93/65
Council Directive 93/65/EEC of 19 July 1993 on the definition and the use of compatible technical and operating specifications for the procurement of air traffic management equipment and systems.
(Articles 1-5, 7-10)
No 97/15
Commission Directive 97/15/EC of 25 March 1997 adopting Eurocontrol standards and amending Council Directive 93/65/EEC on the definition and use of compatible technical specifications for the procurement of air traffic management equipment and systems.
(Articles 1-4, 6)"
Article 4
This Decision shall be published in the Official Journal of the European Union, and the Official Compendium of Swiss Federal Law. It will enter into force on the first day of the second month following its adoption.
Done at Brussels, 18 October 2006.
For the Joint Committee
The Head of the Community Delegation
Daniel Calleja Crespo
The Head of the Swiss Delegation
Raymond Cron
[1] OJ L 210, 12.8.2005, p. 46.
--------------------------------------------------
