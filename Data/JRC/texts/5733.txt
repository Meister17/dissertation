Commission Regulation (EC) No 1481/2005
of 13 September 2005
amending Regulation (EC) No 2805/95 fixing the export refunds in the wine sector
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1493/1999 of 17 May 1999 on the common organisation of the market in wine [1], and in particular the second subparagraph of Articles 63(3) and 64(5) thereof,
Whereas:
(1) Pursuant to Article 63(1) of Regulation (EC) No 1493/1999, to the extent necessary to enable the products listed in Article 1(2)(a) and (b) of that Regulation to be exported on the basis of the prices for those products on the world market and within the limits of the Agreements concluded in accordance with Article 300 of the Treaty, the difference between those prices and the prices in the Community may be covered by an export refund.
(2) Under Article 64(3) of Regulation (EC) No 1493/1999, the amounts and destinations for refunds are to be fixed periodically taking account of the existing situation and likely trends with regard to the prices and availability of the products concerned on the Community market and the world market prices for those products.
(3) Commission Regulation (EC) No 2805/95 [2] should therefore be amended accordingly.
(4) The Management Committee for Wine has not delivered an opinion within the time-limit set by its Chairman,
HAS ADOPTED THIS REGULATION:
Article 1
The Annex to Regulation (EC) No 2805/95 is replaced by the text in the Annex to this Regulation.
Article 2
This Regulation shall enter into force on 14 September 2005.
It shall apply from 16 September 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 13 September 2005.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 179, 14.7.1999, p. 1. Regulation as last amended by Regulation (EC) No 1188/2005 (OJ L 193, 23.7.2005, p. 24).
[2] OJ L 291, 6.12.1995, p. 10. Regulation as last amended by Regulation (EC) No 1605/2003 (OJ L 229, 13.9.2003, p. 15).
--------------------------------------------------
ANNEX
--------------------------------------------------
"ANNEX
NB: The product codes and the "A" series destination codes are set out in Commission Regulation (EEC) No 3846/87 (OJ L 366, 24.12.1987, p. 1), amended.
The numeric destination codes are set out in Commission Regulation (EC) No 750/2005 (OJ L 126, 19.5.2005, p. 12).
The other destinations are defined as follows:
W01: Libya, Nigeria, Cameroon, Gabon, Saudi Arabia, United Arab Emirates, India, Thailand, Vietnam, Indonesia, Malaysia, Brunei, Singapore, Philippines, China, Hong Kong SAR, South Korea, Japan, Taiwan, Equatorial Guinea.
W02: All countries of the African continent with the exception of: Algeria, Morocco, Tunisia, South Africa.
W03: All destinations, with the exception of: Africa, America, Australia, Bosnia-Herzegovina, Croatia, Israel, Serbia, Montenegro, Kosovo, Switzerland, the former Yugoslav Republic of Macedonia, Turkey, Bulgaria and Romania."
Product code | Destination | Unit of measurement | Amount of refund |
2009 69 11 9100 | W01 | EUR/hl | 35,121 |
2009 69 19 9100 | W01 | EUR/hl | 35,121 |
2009 69 51 9100 | W01 | EUR/hl | 35,121 |
2009 69 71 9100 | W01 | EUR/hl | 35,121 |
2204 30 92 9100 | W01 | EUR/hl | 35,121 |
2204 30 94 9100 | W01 | EUR/hl | 9,305 |
2204 30 96 9100 | W01 | EUR/hl | 35,121 |
2204 30 98 9100 | W01 | EUR/hl | 9,305 |
2204 21 79 9100 | W02 | EUR/hl | 4,822 |
2204 21 79 9100 | W03 | EUR/hl | 4,822 |
2204 21 80 9100 | W02 | EUR/hl | 5,826 |
2204 21 80 9100 | W03 | EUR/hl | 5,826 |
2204 21 84 9100 | W02 | EUR/hl | 6,585 |
2204 21 84 9100 | W03 | EUR/hl | 6,585 |
2204 21 85 9100 | W02 | EUR/hl | 7,958 |
2204 21 85 9100 | W03 | EUR/hl | 7,958 |
2204 21 79 9200 | W02 | EUR/hl | 5,644 |
2204 21 79 9200 | W03 | EUR/hl | 5,644 |
2204 21 80 9200 | W02 | EUR/hl | 6,820 |
2204 21 80 9200 | W03 | EUR/hl | 6,820 |
2204 21 79 9910 | W02 and W03 | EUR/hl | 3,394 |
2204 21 94 9910 | W02 and W03 | EUR/hl | 12,825 |
2204 21 98 9910 | W02 and W03 | EUR/hl | 12,825 |
2204 29 62 9100 | W02 | EUR/hl | 4,822 |
2204 29 62 9100 | W03 | EUR/hl | 4,822 |
2204 29 64 9100 | W02 | EUR/hl | 4,822 |
2204 29 64 9100 | W03 | EUR/hl | 4,822 |
2204 29 65 9100 | W02 | EUR/hl | 4,822 |
2204 29 65 9100 | W03 | EUR/hl | 4,822 |
2204 29 71 9100 | W02 | EUR/hl | 5,826 |
2204 29 71 9100 | W03 | EUR/hl | 5,826 |
2204 29 72 9100 | W02 | EUR/hl | 5,826 |
2204 29 72 9100 | W03 | EUR/hl | 5,826 |
2204 29 75 9100 | W02 | EUR/hl | 5,826 |
2204 29 75 9100 | W03 | EUR/hl | 5,826 |
2204 29 62 9200 | W02 | EUR/hl | 5,644 |
2204 29 62 9200 | W03 | EUR/hl | 5,644 |
2204 29 64 9200 | W02 | EUR/hl | 5,644 |
2204 29 64 9200 | W03 | EUR/hl | 5,644 |
2204 29 65 9200 | W02 | EUR/hl | 5,644 |
2204 29 65 9200 | W03 | EUR/hl | 5,644 |
2204 29 71 9200 | W02 | EUR/hl | 6,820 |
2204 29 71 9200 | W03 | EUR/hl | 6,820 |
2204 29 72 9200 | W02 | EUR/hl | 6,820 |
2204 29 72 9200 | W03 | EUR/hl | 6,820 |
2204 29 75 9200 | W02 | EUR/hl | 6,820 |
2204 29 75 9200 | W03 | EUR/hl | 6,820 |
2204 29 83 9100 | W02 | EUR/hl | 6,585 |
2204 29 83 9100 | W03 | EUR/hl | 6,585 |
2204 29 84 9100 | W02 | EUR/hl | 7,958 |
2204 29 84 9100 | W03 | EUR/hl | 7,958 |
2204 29 62 9910 | W02 and W03 | EUR/hl | 3,394 |
2204 29 64 9910 | W02 and W03 | EUR/hl | 3,394 |
2204 29 65 9910 | W02 and W03 | EUR/hl | 3,394 |
2204 29 94 9910 | W02 and W03 | EUR/hl | 12,825 |
2204 29 98 9910 | W02 and W03 | EUR/hl | 12,825 |
--------------------------------------------------
