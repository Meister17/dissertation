COUNCIL DIRECTIVE 98/94/EC of 14 December 1998 amending Directive 94/4/EC and extending the temporary derogation applicable to Germany and Austria
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 99 thereof,
Having regard to the Commission proposal (1),
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas Article 3(2) of Directive 94/4/EC (4) provided for the application until 31 December 1997 of a temporary derogation to the Federal Republic of Germany and the Republic of Austria concerning the application of an allowance of not less than ECU 75 to goods imported by travellers entering German or Austrian territory by a land frontier linking the two Member States to countries other than Member States and EFTA members or, where applicable, by means of coastal navigation coming from those countries;
Whereas account is taken of the economic difficulties likely to be caused by the amount of the allowances applicable to travellers importing goods into the Community in the situations described above;
Whereas, by letters of 24 June and 23 July 1997, the Federal Republic of Germany and the Federal Republic of Austria requested an extension of the derogation provided for in Article 3(2) of Directive 94/4/EC; whereas their request is based on the fact that the economic difficulties that had prompted the adoption of Directives 94/4/EC and 94/75/EC had persisted and, in some cases, worsened;
Whereas account should be taken of the situation described by the two Member States;
Whereas an extension of the derogation should, however, be accompanied by the fixing of a deadline for bringing the allowance applied by Germany and Austria into line with that in force on that date in the other Member States, the raising of the limit applicable to the two Member States from 1 January 1999 in order to limit distortions of competition and an undertaking by those Member States that they will gradually and jointly further raise the limit in order to bring it into line with the Community limit by 1 January 2003,
HAS ADOPTED THIS DIRECTIVE:
Article 1
1. With effect from 1 January 1998, the first subparagraph of Article 3(2) of Directive 94/4/EC shall be replaced by the following:
'2. By way of derogation from paragraph 1, the Federal Republic of Germany and the Republic of Austria shall be authorised to bring into force the measures necessary to comply with this Directive by 1 January 2003 at the latest for goods imported by travellers entering German or Austrian territory by a land frontier linking Germany or Austria to countries other than Member States and the EFTA members or, where applicable, by means of coastal navigation coming from the said countries.`
2. With effect from 1 January 1999, the second subparagraph of Article 3(2) of Directive 94/4/EC shall be replaced by the following:
'However, those Member States shall apply an allowance of not less than ECU 100 from 1 January 1999 to imports by the travellers referred to in the preceding subparagraph. They shall jointly increase that amount gradually, with a view to applying the limit in force in the Community to the said imports by 1 January 2003 at the latest.`
Article 2
1. Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive by 1 January 1999. They shall forthwith inform the Commission thereof.
When Member States adopt these measures, they shall contain a reference to this Directive or shall be accompanied by such reference on the occasion of their publication. The methods of making such a reference shall be laid down by the Member States.
2. Member States shall communicate to the Commission the text of the provisions of national law which they adopt in the field covered by this Directive.
Article 3
This Directive shall enter into force on the day of its publication in the Official Journal of the European Communities.
Article 4
This Directive is addressed to the Member States.
Done at Brussels, 14 December 1998.
For the Council
The President
W. MOLTERER
(1) OJ C 273, 2. 9. 1998, p. 8.
(2) Opinion delivered on 3 December 1998 (not yet published in the Official Journal).
(3) Opinion delivered on 15 October 1998 (not yet published in the Official Journal).
(4) OJ L 60, 3. 3. 1994, p. 14. Directive as amended by Directive 94/75/EC (OJ L 365, 31. 12. 1994, p. 52).
