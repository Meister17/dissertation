COMMISSION REGULATION (EC) No 2307/98 of 26 October 1998 on the issue of export licences for dog and cat food falling within CN code 2309 10 90 qualifying for special import treatment in Switzerland
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2931/79 of 20 December 1979 on the granting of assistance for the exportation of agricultural products which may benefit from a special import treatment in a third country (1), and in particular Article 1(2) thereof,
Whereas, as part of the consultations with Switzerland on implementing the results of the Uruguay Round, agreement has been reached on introducing a series of measures that provide, inter alia, for the duty-free import into Switzerland of an annual quota of 6 000 tonnes of dog and cat food of Community origin falling within CN code 2309 10, including CN code 2309 10 90; whereas the Swiss authorities are responsible for administering the quota;
Whereas exports of dog and cat food falling within CN code 2309 10 90 covered by Council Regulation (EEC) No 827/68 (2), as last amended by Commission Regulation (EC) No 195/96 (3), are not subject to the presentation of an export licence;
Whereas, to guarantee the Community origin of the products, export licences for dog and cat food benefiting from special import treatment in Switzerland must be made compulsory; whereas the issue of such licences must be subject to the presentation by the exporter of a declaration attesting to the Community origin of the product;
Whereas the licences provided for in this Regulation are not intended to authorise exports but merely to prove the Community origin of exported products; whereas it is therefore unnecessary to provide for a security guaranteeing fulfilment of an obligation to export;
Whereas the provisions of this Regulation are either supplementary to, or derogate from, Commission Regulation (EEC) No 3719/88 (4), as last amended by Regulation (EC) No 1044/98 (5);
Whereas the measures provided for in this Regulation are in accordance with the opinion of the relevant management committees,
HAS ADOPTED THIS REGULATION:
Article 1
This Regulation lays down the detailed arrangements for exports to Switzerland of dog and cat food of Community origin falling within CN code 2309 10 90 and liable to qualify for import into Switzerland duty free as part of an annual quota of 6 000 tonnes of dog and cat food falling within CN code 2309 10.
Article 2
1. The exports referred to in Article 1 shall be subject to the presentation of an AGREX export licence.
2. Licence applications shall be admissible only where the applicant:
- declares in writing that all the materials used in the manufacture of the products for which the application is made were obtained entirely within the European Union,
- undertakes in writing to provide, at the request of the competent authorities, any further substantiation which the latter consider necessary for the issuing of the licence and to accept, where applicable, any checks by those authorities of the accounts and of the conditions under which the products concerned are manufactured.
If the applicant is not himself the manufacturer of the products, he shall present a similar statement and undertaking by the manufacturer in support of his application.
3. Box 20 of the licence and licence application shall refer to this Regulation and contain the words 'The exported product does not qualify for a refund`.
4. Issue of the licence shall not be subject to the lodging of a security.
5. Licences shall be issued as soon as possible after applications are lodged.
6. Licences shall be valid from their date of issue within the meaning of Article 21(1) of Regulation (EEC) No 3719/88 until the following 31 December.
7. At the request of the party concerned, a certified copy of the endorsed licence shall be issued.
8. Regulation (EEC) No 3719/88 shall apply, save as otherwise provided in this Regulation.
Article 3
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 26 October 1998.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ L 334, 28. 12. 1979, p. 8.
(2) OJ L 151, 30. 6. 1968, p. 16.
(3) OJ L 26, 2. 2. 1996, p. 13.
(4) OJ L 331, 2. 12. 1988, p. 1.
(5) OJ L 149, 20. 5. 1998, p. 11.
