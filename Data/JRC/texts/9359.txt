Council Decision
of 27 April 2006
concerning the conclusion of the Agreement between the European Community and the Kingdom of Denmark on the service of judicial and extrajudicial documents in civil or commercial matters
(2006/326/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 61(c) thereof, in conjunction with the first sentence of the first subparagraph of Article 300(2) and the first subparagraph of Article 300(3) thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament [1],
Whereas:
(1) In accordance with Articles 1 and 2 of the Protocol on the position of Denmark annexed to the Treaty on European Union and the Treaty establishing the European Community, Denmark is not bound by the provisions of Council Regulation (EC) No 1348/2000 of 29 May 2000 on the service in the Member States of judicial and extrajudicial documents in civil or commercial matters [2], nor subject to their application.
(2) The Commission has negotiated an Agreement between the European Community and the Kingdom of Denmark extending to Denmark the provisions of Regulation (EC) No 1348/2000.
(3) The Agreement was signed, on behalf of the European Community, on 19 October 2005, subject to its possible conclusion at a later date, in accordance with Council Decision 2005/794/EC of 20 September 2005 [3].
(4) In accordance with Article 3 of the Protocol on the position of the United Kingdom and Ireland annexed to the Treaty on European Union and the Treaty establishing the European Community, the United Kingdom and Ireland are taking part in the adoption and application of this Decision.
(5) In accordance with Articles 1 and 2 of the Protocol on the position of Denmark, Denmark is not taking part in the adoption of this Decision and is not bound by it or subject to its application.
(6) The Agreement should be approved,
HAS DECIDED AS FOLLOWS:
Article 1
The Agreement between the European Community and the Kingdom of Denmark on the service of judicial and extrajudicial documents in civil or commercial matters is hereby approved on behalf of the Community.
Article 2
The President of the Council is hereby authorised to designate the person empowered to make the notification provided for in Article 10(2) of the Agreement.
Done at Luxembourg, 27 April 2006.
For the Council
The President
L. Prokop
[1] Opinion delivered on 23 March 2006 (not yet published in the Official Journal).
[2] OJ L 160, 30.6.2000, p. 37.
[3] OJ L 300, 17.11.2005, p. 53.
--------------------------------------------------
