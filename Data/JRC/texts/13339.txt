Judgment of the Court of First Instance of 10 May 2006 — R
v Commission
(Case T-331/04) [1]
Parties
Applicant: R (Chaumont-Gistoux, Belgium) (represented by: B. Arians, lawyer)
Defendant: Commission of the European Communities (represented by: V. Joris and H. Kraemer, acting as Agents)
Re:
Application for the annulment of the decision relating to the applicant's classification in grade
Operative part of the judgment
The Court:
1. Dismisses the application;
2. Orders each party to bear its own costs.
[1] OJ C 284, 20.11.2004.
--------------------------------------------------
