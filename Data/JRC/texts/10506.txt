[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 3.11.2006
COM(2006) 651 final
2006/0216 (COD)
Proposal for a
DIRECTIVE OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL
on the field of vision and windscreen wipers for wheeled agricultural or forestry tractors (Codified version)
(presented by the Commission)
EXPLANATORY MEMORANDUM
1. In the context of a people’s Europe, the Commission attaches great importance to simplifying and clarifying Community law so as to make it clearer and more accessible to the ordinary citizen, thus giving him new opportunities and the chance to make use of the specific rights it gives him.
This aim cannot be achieved so long as numerous provisions that have been amended several times, often quite substantially, remain scattered, so that they must be sought partly in the original instrument and partly in later amending ones. Considerable research work, comparing many different instruments, is thus needed to identify the current rules.
For this reason a codification of rules that have frequently been amended is also essential if Community law is to be clear and transparent.
2. On 1 April 1987 the Commission therefore decided[1] to instruct its staff that all legislative acts should be codified after no more than ten amendments, stressing that this is a minimum requirement and that departments should endeavour to codify at even shorter intervals the texts for which they are responsible, to ensure that the Community rules are clear and readily understandable.
3. The Conclusions of the Presidency of the Edinburgh European Council (December 1992) confirmed this[2], stressing the importance of codification as it offers certainty as to the law applicable to a given matter at a given time.
Codification must be undertaken in full compliance with the normal Community legislative procedure.
Given that no changes of substance may be made to the instruments affected by codification , the European Parliament, the Council and the Commission have agreed, by an interinstitutional agreement dated 20 December 1994, that an accelerated procedure may be used for the fast-track adoption of codification instruments.
4. The purpose of this proposal is to undertake a codification of Council Directive 74/347/EEC of 25 June 1974 on the approximation of the laws of the Member States relating to the field of vision and windscreen wipers for wheeled agricultural or forestry tractors[3] . The new Directive will supersede the various acts incorporated in it[4]; this proposal fully preserves the content of the acts being codified and hence does no more than bring them together with only such formal amendments as are required by the codification exercise itself.
5. The codification proposal was drawn up on the basis of a preliminary consolidation, in all official languages, of Directive 74/347/EEC and the instruments amending it, carried out by the Office for Official Publications of the European Communities, by means of a data-processing system. Where the Articles have been given new numbers, the correlation between the old and the new numbers is shown in a table contained in Annex III to the codified Directive.
74/347/EEC (adapted)
2006/0216 (COD)
Proposal for a
DIRECTIVE OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL
on the field of vision and windscreen wipers for wheeled agricultural or forestry tractors
(Text with EEA relevance)
THE EUROPEAN PARLIAMENT AND THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 95 thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Economic and Social Committee[5],
Acting in accordance with the procedure laid down in Article 251 of the Treaty[6],
Whereas:
(1) Council Directive 74/347/EEC of 25 June 1974 on the approximation of the laws of the Member States relating to the field of vision and windscreen wipers for wheeled agricultural or forestry tractors[7] has been substantially amended several times[8]. In the interests of clarity and rationality the said Directive should be codified.
(2) Directive 74/347/EEC is one of the separate Directives of the EC type-approval system provided for in Council Directive 74/150/EEC as replaced by Directive 2003/37/EC of the European Parliament and of the Council of 26 May 2003 on type-approval of agricultural or forestry tractors, their trailers and interchangeable towed machinery, together with their systems, components and separate technical units and repealing Directive 74/150/EEC[9] and lays down technical prescriptions concerning the design and construction of wheeled agricultural or forestry tractors as regards the field of vision and windscreen wipers. These technical prescriptions concern the approximation of the laws of the Member States to enable the EC type-approval procedure provided for in Directive 2003/37/EC, to be applied in respect of each type of tractor. Consequently, the provisions laid down in Directive 2003/37/EC relating to agricultural or forestry tractors, their trailers and interchangeable towed machinery, together with their systems, components and separate technical units apply to this Directive.
(3) This Directive should be without prejudice to the obligations of the Member States relating to the time-limits for transposition into national law and application of the Directives set out in Annex II, Part B,
74/347/EEC
Corrigendum 74/347/EEC (OJ L 226, 18.8.1976, p. 16).
HAVE ADOPTED THIS DIRECTIVE:
Article 1
1. ‘Agricultural or forestry tractor’ means any motor vehicle, fitted with wheels or endless tracks and having at least two axles, the main function of which lies in its tractive power and which is specially designed to tow, push, carry or power certain tools, machinery or trailers intended for agricultural or forestry use. It may be equipped to carry a load and passengers.
82/890/EEC Art. 1(1) (adapted)
97/54/EC Art. 1
2. This Directive shall apply only to tractors defined in paragraph 1 which are equipped with pneumatic tyres and have a maximum design speed of between 6 and 40 km/h .
74/347/EEC (adapted)
Article 2
No Member State may refuse to grant EC type-approval or national type-approval of a tractor on grounds relating to windscreen wipers if these satisfy the requirements set out in Annex I.
Article 3
No Member State may refuse or prohibit the sale, registration, entry into service or use of tractors on grounds relating to windscreen wipers, if these satisfy the requirements set out in Annex I.
Article 4
The amendments necessary to adapt the requirements of Annex I to technical progress shall be adopted in accordance with the procedure referred to in Article 20(2) of Directive 2003/37/EC .
Article 5
Member States shall communicate to the Commission the texts of the main provisions of national law which they adopt in the field covered by this Directive
Article 6
Directive 74/347/EEC, as amended by the Directives listed in Annex II, is repealed, without prejudice to the obligations of the Member States relating to the time-limits for transposition into national law and application of the Directives set out in Annex II, Part B.
References to the repealed Directive shall be construed as references to this Directive and shall be read in accordance with the correlation table in Annex III.
Article 7
This Directive shall enter into force on the twentieth day following that of its publication in the Official Journal of the European Union .
It shall apply from [...].
74/347/EEC
Article 8
This Directive is addressed to the Member States.
Done at Brussels,
For the European Parliament For the Council
The President The President
74/347/EEC Annex
ANNEX I
FIELD OF VISION
DEFINITIONS AND REQUIREMENTS
1. DEFINITIONS
1.1. Field of vision
‘Field of vision’ means all forward and lateral directions in which the driver of the tractor can see.
1.2. Reference point
‘Reference point’ means the position, fixed by convention, of the tractor driver’s eyes notionally located at a single point. The reference point is situated in the plane parallel to the longitudinal median plane of the tractor and passing through the centre of the seat, 700 mm vertically above the line of intersection of that plane and the surface of the seat and 270 mm in the direction of the pelvic support from the vertical plane passing through the front edge of the surface of the seat and perpendicular to the longitudinal median plane of the tractor (Figure 1). The reference point thus determined relates to the seat when unoccupied and fitted in the central position specified by the tractor manufacturer.
1.3. Semi-circle of vision
‘Semi-circle of vision’ means the semi-circle described by a radius of 12 m about a point situated in the horizontal plane of the road vertically below the reference point, in such a way that, when facing the direction of motion, the arc of the semi-circle lies in front of the tractor, while the diameter bounding the semi-circle is at right angles to the longitudinal axis of the tractor (Figure 2).
79/1073/EEC Art. 1 pt. 3 and Annex
1.4. Masking effect
‘Masking effect’ means the chords of the sectors of the semi-circle of vision which cannot be seen owing to structural components such as roof-pillars, air intakes or exhaust stacks and the frame of the windscreen.
74/347/EEC
79/1073/EEC Art. 1 pt. 3 and Annex
1.5. Sector of vision
‘Sector of vision’ means that part of the field of vision bounded:
1.5.1. at the top,
by a horizontal plane passing through the reference point;
1.5.2. in the plane of the road,
by the zone lying outside the semi-circle of vision, and forming the continuation of the sector of the semi-circle of vision, the chord of which is 9.5 m long, perpendicular to the plane parallel to the longitudinal median plane of the tractor passing through the centre of the driver's seat and bisected by that plane.
1.6. Swept area of the windscreen wipers
‘Swept area of the windscreen wipers’ means the area of the outer surface of the windscreen swept by the windscreen wipers.
2. REQUIREMENTS
2.1. General
The tractor shall be constructed and equipped in such a way that, in road traffic and in farm and forest use, the driver has an adequate field of vision, under all the usual conditions pertaining to highway use and to work undertaken in fields and forests. The field of vision is considered adequate when the driver has, as far as possible, a view of part of each front wheel and when the following requirements are fulfilled.
2.2. Checking of the field of vision
2.2.1. Procedure for determining masking effects
2.2.1.1. The tractor must be placed on a horizontal surface as shown in Figure 2. On a horizontal support level with the reference point, there must be mounted two point sources of light, e.g. 2 × 150 W, 12 V, 65 mm apart and symmetrically located with respect to the reference point. The support must be rotatable at its centre point about a vertical axis passing through the reference point. For the purpose of measuring the masking effects, the support must be so aligned that the line joining the two light sources is perpendicular to the line joining the masking component and the reference point.
79/1073/EEC Art. 1 pt. 3 and Annex
The silhouette (deepest shadow) overlaps projected on to the semi-circle of vision by the masking component when the light sources are switched on simultaneously or alternately must be measured in accordance with point 1.4 (Figure 3).
74/347/EEC
79/1073/EEC Art. 1 pt. 3 and Annex
2.2.1.2. Masking effects must not exceed 700 mm.
2.2.1.3. Masking effects due to adjacent structural components over 80 mm in width must be so configured that there is an interval of not less than 2 200 mm — measured as a chord of the semi-circle of vision — between the centres of two masking effects.
2.2.1.4. There may be no more than six masking effects in the semi-circle of vision and no more than two inside the sector of vision defined in point 1.5.
2.2.1.5. Outside the sector of vision, masking effects exceeding 700 mm but not exceeding 1 500 mm are, however, permissible if the components causing them cannot be redesigned or relocated: on each side there may be a total of either two such masking effects, one not exceeding 700 mm and the other not exceeding 1 500 mm, or two such masking effects, neither exceeding 1 200 mm.
2.2.1.6. Blind spots caused by type-approved rear-view mirrors may be disregarded if the design of these mirrors is such that they cannot be installed in any other way.
2.2.2. Mathematical determination of masking effects for binocular vision:
2.2.2.1. as an alternative to the procedure set out in point 2.2.1, the acceptability of individual masking effects can be determined mathematically. The requirements of points 2.2.1.2, 2.2.1.3, 2.2.1.4, 2.2.1.5 and 2.2.1.6 shall apply in respect of the size, distribution and number of the masking effects;
2.2.2.2. for binocular vision with an inter-ocular distance of 65 mm, the masking effect expressed in mm is given by the formula:
74/347/EEC (adapted)
[pic]
74/347/EEC
in which:
a is the distance in millimetres between the component obstructing vision and the reference point measured along the visual radius joining the reference point, the centre of the component and the perimeter of the semi-circle of vision;
b is the width in millimetres of the component obstructing vision measured horizontally and perpendicular to the visual radius.
2.3. The test methods referred to under point 2.2 may be replaced by others if the latter can be shown to be equivalent.
74/347/EEC (adapted)
79/1073/EEC Art. 1 pt. 3 and Annex
2.4 Transparent area of the windscreen
For the purpose of determining the masking effects in the sector of vision, the masking effects due to the frame of the windscreen and to any other obstacle may, in accordance with the provisions of point 2.2.1.4, be considered as a single effect, provided that the distance between the outermost points of this masking effect does not exceed 700 mm.
2.5 Windscreen wipers
2.5.1. Tractors fitted with windscreens must also be equipped with motor-driven windscreen wipers . The area swept by these wipers must ensure an unobstructed forward view corresponding to a chord of the semi-circle of vision at least 8 m long within the sector of vision.
2.5.2. The rate of operation of the windscreen wipers must be at least 20 cycles per minute.
[pic] Figure 1
79/1073/EEC Art. 1 pt. 3 and Annex
[pic] Figure 2
74/347/EEC
[pic] Figure 3
_____________
ANNEX II
Part A
Repealed Directive with its successive amendments (referred to in Article 6)
Council Directive 74/347/EEC (OJ L 191, 15.7.1974, p. 5) |
Commission Directive 79/1073/EEC (OJ L 331, 27.12.1979, p. 20) |
Council Directive 82/890/EEC (OJ L 378, 31.12.1982, p. 45) | Only as regards the references to Directive 74/347/EEC in Article 1(1) |
Directive 97/54/EC of the European Parliament and of the Council (OJ L 277, 10.10.1997, p. 24) | Only as regards the references to Directive 74/347/EEC in the first indent of Article 1 |
Part B
List of time-limits for transposition into national law and application (referred to in Article 6)
Directive | Time-limit for transposition | Date of application |
74/347/EEC | 1 January 1976(*) |
79/1073/EEC | 30 April 1980 |
82/890/EEC | 21 June 1984 |
97/54/EC | 22 September 1998 | 23 September 1998 |
(*) In compliance with Article 3a, inserted by point 2 of Article 1 of Directive 79/1073/EEC:
“1. With effect from 1 May 1980 no Member State may, on grounds relating to the field of vision of tractors:
- refuse, in respect of a type of tractor, to grant EEC type-approval, to issue the document referred to in the last indent of Article 10(1) of Directive 74/150/EEC, or to grant national type-approval,
- or prohibit the entry into service of tractors,
if the field of vision of this type of tractor or of these tractors complies with the provisions of this Directive.
2. With effect from 1 October 1980 Member States:
- may no longer issue the document referred to in the last indent of Article 10(1) of Directive 74/150/EEC in respect of a type of tractor in which the field of vision does not comply with the provisions of this Directive,
- may refuse to grant national type-approval in respect of a type of tractor whose field of vision does not comply with the provisions of this Directive.
3. With effect from 1 January 1983, Member States may prohibit the entry into service of any tractor whose field of vision does not comply with the provisions of this Directive.”
_____________
ANNEX III
Correlation Table
Directive 74/347/EEC | This Directive |
Articles 1 to 3 | Articles 1 to 3 |
Article 3a | Table note (*), Annex II |
Article 4 | Article 4 |
Article 5(1) | - |
Article 5(2) | Article 5 |
- | Articles 6 and 7 |
Article 6 | Article 8 |
Annex | Annex I |
Annex, points 1 to 2.3 | Annex I, points 1 to 2.3 |
Annex, point 2.4 | - |
Annex, point 2.5 | Annex I, point 2.4 |
Annex, point 2.6 | Annex I, point 2.5 |
Annex, figures 1, 2 and 3 | Annex I, figures 1, 2 and 3 |
- | Annex II |
- | Annex III |
_____________
[1] COM(87) 868 PV.
[2] See Annex 3 to Part A of the Conclusions.
[3] Carried out pursuant to the Communication from the Commission to the European Parliament and the Council – Codification of the Acquis communautaire, COM(2001) 645 final.
[4] See Annex II, Part A of this proposal.
[5] OJ C […], […], p. […].
[6] OJ C […], […], p. […].
[7] OJ L 191, 15.7.1974, p. 5. Directive as last amended by Directive 97/54/EC of the European Parliament and of the Council (OJ L 277, 10.10.1997, p. 24).
[8] See Annex II, Part A.
[9] OJ L 171, 9.7.2003, p. 1. Directive as last amended by Commission Directive 2005/67/EC (OJ L 273, 19.10.2005, p. 17).
