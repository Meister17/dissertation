Commission Regulation (EC) No 1185/2005
of 22 July 2005
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 23 July 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 22 July 2005.
For the Commission
J. M. Silva Rodríguez
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 1947/2002 (OJ L 299, 1.11.2002, p. 17).
--------------------------------------------------
ANNEX
to Commission Regulation of 22 July 2005 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code | Standard import value |
07020000 | 052 | 78,8 |
096 | 42,0 |
999 | 60,4 |
07070005 | 052 | 77,1 |
999 | 77,1 |
07099070 | 052 | 73,3 |
999 | 73,3 |
08055010 | 388 | 65,1 |
508 | 58,8 |
524 | 73,5 |
528 | 62,6 |
999 | 65,0 |
08061010 | 052 | 107,1 |
204 | 80,8 |
220 | 176,7 |
508 | 134,4 |
624 | 159,1 |
999 | 131,6 |
08081080 | 388 | 87,1 |
400 | 95,7 |
404 | 86,2 |
508 | 74,8 |
512 | 72,0 |
524 | 52,1 |
528 | 52,4 |
720 | 57,1 |
804 | 84,8 |
999 | 73,6 |
08082050 | 052 | 99,6 |
388 | 77,9 |
512 | 23,3 |
528 | 50,0 |
999 | 62,7 |
08091000 | 052 | 139,2 |
094 | 100,2 |
999 | 119,7 |
08092095 | 052 | 293,1 |
400 | 310,8 |
404 | 385,7 |
999 | 329,9 |
08093010, 08093090 | 052 | 120,2 |
999 | 120,2 |
08094005 | 624 | 87,8 |
999 | 87,8 |
--------------------------------------------------
