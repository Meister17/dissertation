Commission Directive 2006/26/EC
of 2 March 2006
amending, for the purposes of their adaptation to technical progress, Council Directives 74/151/EEC, 77/311/EEC, 78/933/EEC and 89/173/EEC relating to wheeled agricultural or forestry tractors
(Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Directive 2003/37/EC of the European Parliament and of the Council of 26 May 2003 on type-approval of agricultural or forestry tractors, their trailers and interchangeable towed machinery, together with their systems, components and separate technical units and repealing Directive 74/150/EEC [1], and in particular Article 19(1) thereof,
Having regard to Council Directive 74/151/EEC of 4 March 1974 on the approximation of the laws of the Member States relating to certain parts and characteristics of wheeled agricultural or forestry tractors [2], and in particular Article 4 thereof,
Having regard to Council Directive 77/311/EEC of 29 March 1977 on the approximation of the laws of the Member States relating to the driver-perceived noise level of wheeled agricultural or forestry tractors [3], and in particular Article 5 thereof,
Having regard to Council Directive 78/933/EEC of 17 October 1978 on the approximation of the laws of the Member States relating to the installation of lighting and lighting-signalling devices on wheeled agricultural or forestry tractors [4], and in particular Article 5 thereof,
Having regard to Council Directive 89/173/EEC of 21 December 1988 on the approximation of the laws of the Member States relating to certain components and characteristics of wheeled agricultural or forestry tractors [5], and in particular Article 9 thereof,
Whereas:
(1) The requirements introduced by Directive 74/151/EEC for the maximum permissible laden mass and axle load for wheeled agricultural or forestry tractors need to be adapted for modern tractors, taking into account the enhancements in tractor technology with regard to the increase of productivity as well as occupational safety.
(2) In order to facilitate the global operation of Community industry, it is necessary to align Community technical regulations and standards with the corresponding global technical regulations and standards. In relation to the maximum limits laid down in Directive 77/311/EEC for the driver-perceived noise level of wheeled agricultural or forestry tractors, the test speed established in Annexes I and II to that Directive should therefore be harmonised with the test speed required in global technical regulations or standards such as OECD Code 5 and ISO 5131:1996 [6].
(3) It is appropriate to adapt the requirements introduced by Directive 78/933/EEC relating to the installation of lighting and light-signalling devices on wheeled agricultural and forestry tractors, so as to meet present needs for simpler design and better illumination.
(4) The requirements laid down in Directive 89/173/EEC relating to glazing and coupling of wheeled agricultural or forestry tractors should be brought into line with recent technological developments. In particular, polycarbonate/plastic glazing should be permitted for non-windscreen applications to enhance the protection of the occupants in the event of objects penetrating the driver cab area. The requirements concerning mechanical couplings should be aligned with the standard ISO 6489-1. Furthermore, with a view to reducing the number and severity of accidents and to enhancing occupational safety, it is appropriate not only to make additional provision as regards hot surfaces, but also to lay down measures concerning battery terminal covers, as well as measures designed to prevent unintentional uncoupling.
(5) Directives 74/151/EEC, 77/311/EEC, 78/933/EEC and 89/173/EEC should therefore be amended accordingly.
(6) The measures provided for in this Directive are in accordance with the opinion of the Committee established under Article 20(1) of Directive 2003/37/EC,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Amendment to Directive 74/151/EEC
Directive 74/151/EEC is amended in accordance with Annex I to this Directive.
Article 2
Amendment to Directive 77/311/EEC
Directive 77/311/EEC is amended in accordance with Annex II to this Directive.
Article 3
Amendment to Directive 78/933/EEC
Directive 78/933/EEC is amended in accordance with Annex III to this Directive.
Article 4
Amendment to Directive 89/173/EEC
Directive 89/173/EEC is amended in accordance with Annex IV to this Directive.
Article 5
Transitional provisions
1. With effect from 1 January 2007, with respect to vehicles which comply with the requirements laid down respectively in Directives 74/151/EEC, 78/933/EEC, 77/311/EEC and 89/173/EEC as amended by this Directive, Member States shall not, on grounds relating to the subject-matter of the Directive concerned:
(a) refuse to grant EC type-approval or to grant national type-approval;
(b) prohibit the registration, sale or entry into service of such a vehicle.
2. With effect from 1 July 2007, with respect to vehicles which do not comply with the requirements laid down respectively in Directives 74/151/EEC, 78/933/EEC, 77/311/EEC and 89/173/EEC as amended by this Directive, and on grounds relating to the subject-matter of the Directive concerned, Member States:
(a) shall no longer grant EC type-approval;
(b) may refuse to grant national type-approval.
3. With effect from 1 July 2009, with respect to vehicles which do not comply with the requirements laid down respectively in Directives 74/151/EEC, 78/933/EEC, 77/311/EEC and 89/173/EEC as amended by this Directive, and on grounds relating to the subject-matter of the Directive concerned, Member States:
(a) shall consider certificates of conformity which accompany new vehicles in accordance with the provisions of Directive 2003/37/EC to be no longer valid for the purposes of Article 7(1);
(b) may refuse the registration, sale or entry into service of those new vehicles.
Article 6
Transposition
1. Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive by 31 December 2006 at the latest. They shall forthwith communicate to the Commission the text of those provisions and a correlation table between those provisions and this Directive.
When Member States adopt those provisions, they shall contain a reference to this Directive or be accompanied by such a reference on the occasion of their official publication. Member States shall determine how such reference is to be made.
2. Member States shall communicate to the Commission the text of the main provisions of national law which they adopt in the field covered by this Directive.
Article 7
Entry into force
This Directive shall enter into force on 20th day following its publication in the Official Journal of the European Union.
Article 8
Addressees
This Directive is addressed to the Member States.
Done at Brussels, 2 March 2006.
For the Commission
Günter Verheugen
Vice-President
[1] OJ L 171, 9.7.2003, p. 1. Directive as last amended by Commission Directive 2005/67/EC (OJ L 273, 19.10.2005, p. 17).
[2] OJ L 84, 28.3.1974, p. 25. Directive as last amended by Commission Directive 98/38/EC (OJ L 170, 16.6.1998, p. 13).
[3] OJ L 105, 28.4.1977, p. 1. Directive as last amended by Directive 97/54/EC of the European Parliament and of the Council (OJ L 277, 10.10.1997, p. 24).
[4] OJ L 325, 20.11.1978, p. 16. Directive as last amended by Commission Directive 1999/56/EC (OJ L 146, 11.6.1999, p. 31).
[5] OJ L 67, 10.3.1989, p. 1. Directive as last amended by the 2003 Act of Accession.
[6] These documents can be found in the web pages http://www.oecd.org/dataoecd/35/19/34733683.PDF and http://www.iso.org/iso/en/CatalogueDetailPage.CatalogueDetail?CSNUMBER=20842&ICS1=17&ICS2=140&ICS3=20
--------------------------------------------------
ANNEX I
In Annex I to Directive 74/151/EEC, point 1.2. is replaced by the following:
"1.2. the maximum permissible laden mass and the maximum permissible mass per axle depending on the vehicle category does not exceed the values given in Table 1.
Table 1
Maximum Permissible Laden Mass and Maximum Permissible Mass per Axle Depending on the Vehicle Category
Vehicle category | Number of axles | Maximum permissible mass (t) | Maximum permissible mass per axle |
Driven axle (t) | Non-driven axle (t) |
T1, T2, T4.1, | 2 | 18 (laden) | 11,5 | 10 |
3 | 24 (laden) | 11,5 | 10 |
T3 | 2, 3 | 0,6 (unladen) | [1] | [1] |
T4.3 | 2, 3, 4 | 10 (laden) | [1] | [1] |
[1] It is not necessary to establish an axle limit for vehicle categories T3 and T4.3, as they have by definition limitations on the maximum permissible laden and/or unladen mass."
--------------------------------------------------
ANNEX II
Directive 77/311/EEC is amended as follows:
(1) Annex I is amended as follows:
(i) in point 3.2.2., "7.25 km/h" is replaced by "7,5 km/h";
(ii) in point 3.3.1., "7.25 km/h" is replaced by "7,5 km/h";
(2) In point 3.2.3. of Annex II, "7.25 km/h" is replaced by "7,5 km/h".
--------------------------------------------------
ANNEX III
In Directive 78/933/EEC, Annex I is amended as follows:
(1) In point 4.5.1., the following phrase is added:
"Additional direction-indicator lamps optional."
(2) Point 4.5.4.2. is replaced by the following:
"4.5.4.2. Height:
Above the ground:
- not less than 500 mm for direction-indicator lamps in category 5,
- not less than 400 mm for direction-indicator lamps in categories 1 and 2,
- not more than 1900 mm for all categories.
If the structure of the tractor makes it impossible to keep to this maximum figure, the highest point on the illuminating surface may be at 2300 mm for direction-indicator lamps in category 5, for those in categories 1 and 2 of arrangement A, for those in categories 1 and 2 of arrangement B and for those in categories 1 and 2 of arrangement D; it may be at 2100 mm for those in categories 1 and 2 of the other arrangements.
- up to 4000 mm for optional direction-indicator lamps."
(3) In point 4.7.4.2., the value "2100 mm" is replaced by the value "2300 mm".
(4) In point 4.10.4.2., the value "2100 mm" is replaced by the value "2300 mm".
(5) In point 4.14.5.2.2., the value "2100 mm" is replaced by the value "2300 mm".
(6) Point 4.15.7. is replaced by the following:
"4.15.7 May be "grouped" ".
--------------------------------------------------
ANNEX IV
Directive 89/173/EEC is amended as follows:
(1) Annex II is amended as follows:
(a) In point 2.2., the following points are inserted:
"2.2.6. "Normal operation" means the use of the tractor for the purpose intended by the manufacturer and by an operator familiar with the tractor characteristics and complying with the information for operation, service and safe practices, as specified by the manufacturer in the operator’s manual and by signs on the tractor.
2.2.7. "Inadvertent contact" means unplanned contact between a person and a hazard location resulting from the person’s action during normal operation and service of the tractor."
(b) In point 2.3.2., the following points are inserted:
"2.3.2.16. Hot surfaces
Hot surfaces which can be reached by the operator during normal operation of the tractor shall be covered or insulated. This applies to hot surfaces which are near to steps, handrails, handholds and integral tractor parts used as boarding means and which may be inadvertently touched.
2.3.2.17. Battery terminal cover
Non-earthed terminals must be protected against unintentional short-circuiting."
(2) In point 1. of Annex III A, the following point is inserted:
"1.1.3. rigid plastic glazing is permitted for non-windscreen applications as approved in Council Directive 92/22/EEC [1] or in UN-ECE Regulation 43, Annex 14.
(3) Annex IV is amended as follows:
(a) In point 1.1., the second indent is replaced by the following:
- "— towing hook (see Figure 1 — "Hitch-hook dimensions" in ISO 6489-1:2001),"
(b) In Section 2, the following point 2.9. is added:
"2.9. In order to prevent unintentional uncoupling from the hitch ring, the distance between the towing hook tip and the keeper (clamping device) shall not exceed 10 mm at the maximum design load."
(c) In Appendix 1, Figure 3 and the related text are deleted.
[1] OJ L 129, 14.5.1992, p. 11."
--------------------------------------------------
