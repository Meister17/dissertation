COUNCIL REGULATION (EEC) No 1605/91 of 10 June 1991 amending Regulation (EEC) No 1784/77 concerning the certification of hops
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 1696/71 of 26 July 1971 on the common organization of the market in hops (1), as last amended by Regulation (EEC) No 3577/90 (2), and in particular Article 2 (4) thereof,
Having regard to the proposal from the Commission,
Whereas Regulation (EEC) No 1784/77 (3) as last amended by Regulation (EEC) No 2039/85 (4), makes provision for circumstances under which hops may be processed into hop products;
Whereas in practice, owing to technical development, not only cone hops are used for the manufacturing of products prepared from hops, but also hop products; whereas, in order to take account of technical development and management practice, the references made to the processing of hops in Regulation (EEC) No 1784/77 should be amended so that hop products may be used for further processing,
HAS ADOPTED THIS REGULATION: Article 1
Regulation (EEC) No 1784/77 is hereby amended as follows:
1. In Article 7, the expression 'hops certified in the Community' shall be replaced by the term 'hops certified in the Community, certified hop products prepared therefrom'.
2. Article 8 (3) shall be replaced by the following:
'3. However, certified hops of Community origin and certified hop products prepared therefrom which are from the same harvest but of different varieties and from different production areas may be blended in the manufacture of powder and extracts provided that the certificate accompanying the product states:
(a) the varieties used, the production areas and the year of harvesting;
(b) the percentage weight of each variety used in the blend; if hop products have been used in combination with cone hops for the manufacturing of hop products, or if different hop products have been used, the percentage weight of each variety based on the quantity of cone hops which was used for the preparation of the input products;
(c) the reference numbers of the certificates issued for the hops and the hop products used.' Article 2 This Regulation shall enter into force on the seventh day following that of its publication in the Official Journal of the European Communities. This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Luxembourg, 10 June 1991. For the Council
The President
J.-C. JUNCKER (1) OJ No L 175, 4. 8. 1971, p. 1. (2) OJ No L 353, 17. 12. 1990, p. 23. (3) OJ No L 200, 8. 8. 1977, p. 1. (4) OJ No L 193, 25. 7. 1985, p. 1.
