Commission Regulation (EC) No 109/2005
of 24 January 2005
on the definition of the economic territory of Member States for the purposes of Council Regulation (EC, Euratom) No 1287/2003 on the harmonisation of gross national income at market prices
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to the Treaty establishing the European Atomic Energy Community,
Having regard to Council Regulation (EC, Euratom) 1287/2003 of 15 July 2003 on the harmonisation of gross national income at market prices (GNI Regulation) [1], and in particular Article 5(1) thereof,
Whereas:
(1) Article 2(7) of Council Decision 2000/597/EC, Euratom of 29 September 2000 on the system of the Communities’ own resources [2] lays down that gross national product at market prices (GNP) is to be considered equal to gross national income at market prices (GNI) as provided by the Commission in application of the European System of Accounts (ESA). The ESA of 1995 (ESA95), superseding two earlier systems of 1970 and 1979 respectively, was established by Council Regulation (EC) No 2223/96 of 25 June 1996 on the European system of national and regional accounts in the Community [3], and was set out in the Annex thereto. GNI, as used in ESA95, replaced GNP as a criterion for own resource purposes, with effect from budget year 2002.
(2) Council Regulation (EC, Euratom) 1287/2003 lays down the procedures for the forwarding of GNI data by Member States and the procedures and checks on the calculation of GNI, and establishes the GNI Committee.
(3) For the purpose of the definition of gross national income at market prices (GNI) pursuant to Article 1 of Regulation (EC, Euratom) No 1287/2003, it is necessary to clarify the ESA95 definition of economic territory.
(4) For the purpose of implementation of Article 1 of Council Directive 89/130/EEC, Euratom on the harmonisation of the compilation of gross national product at market prices [4], the economic territory of Member States is defined by Commission Decision 91/450/EEC, Euratom [5]. The equivalent definition should now be provided in respect of GNI.
(5) The measures provided for in this Regulation are in accordance with the opinion of the GNI Committee.
HAS ADOPTED THIS REGULATION:
Article 1
For the purposes of Regulation (EC, Euratom) 1287/2003 the term "economic territory" shall have the meaning attributed to it in paragraphs 2.05 and 2.06 of Annex A to Regulation (EC) 2223/96, the term "geographic territory" as used in those paragraphs being understood to comprise the Member States’ territories as listed in the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the twentieth day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 24 January 2005.
For the Commission
Joaquín Almunia
Member of the Commission
--------------------------------------------------
[1] OJ L 181, 19.7.2003, p. 1.
[2] OJ L 253, 7.10.2000, p. 42.
[3] OJ L 310, 30.11.1996, p. 1; Regulation as last amended by Regulation (EC) No 1267/2003 of the European Parliament and of the Council (OJ L 180, 18.7.2003, p. 1).
[4] OJ L 49, 21.2.1989, p. 26; Directive as last amended by Regulation (EC) No 1882/2003 of the European Parliament and of the Council (OJ L 284, 31.10.2003, p. 1).
[5] OJ L 240, 29.08.1991, p. 36; Decision as last amended by the 2003 Act of Accession.
--------------------------------------------------
+++++ ANNEX 1 +++++</br>
