Commission Directive 2006/120/EC
of 27 November 2006
correcting and amending Directive 2005/30/EC amending, for the purposes of their adaptation to technical progress, Directives 97/24/EC and 2002/24/EC of the European Parliament and of the Council, relating to the type-approval of two or three-wheel motor vehicles
(Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Directive 97/24/EC of the European Parliament and of the Council of 17 June 1997 on certain components and characteristics of two- or three-wheel motor vehicles [1], and in particular Article 7 thereof,
Having regard to Directive 2002/24/EC of the European Parliament and of the Council of 18 March 2002 relating to the type-approval of two- and three-wheel motor vehicles and repealing Council Directive 92/61/EEC [2], and in particular Article 17 thereof,
Whereas:
(1) Commission Directive 2005/30/EC contains some errors which must be corrected.
(2) It is necessary to clarify with the retroactive effect that Article 3(1) of Directive 2005/30/EC concerns new replacement catalytic converters, which comply with Directive 97/24/EC as amended by Directive 2005/30/EC.
(3) Editorial corrections should also be made in Annex I to Directive 2005/30/EC.
(4) Furthermore, it is necessary to make clear, by way of a new provision, that the sale or installation on a vehicle of replacement catalytic converters which are not of a type in respect of which a type-approval has been granted in compliance with Directive 97/24/EC, as amended by 2005/30/EC, is forbidden after a certain transitional period.
(5) Directive 2005/30/EC should therefore be corrected and amended accordingly.
(6) The measures provided for this Directive are in accordance with the opinion of the Committee for Adaptation to Technical Progress,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Directive 2005/30/EC is corrected as follows:
1. Article 3(1) is replaced by the following:
"1. With effect from 18 May 2006 Member States shall not, in respect of new replacement catalytic converters complying with the requirements of Directive 97/24/EC as amended by this Directive and which are intended to be fitted on vehicles that have been type-approved in accordance with Directive 97/24/EC:
(a) refuse to grant EC type-approval pursuant to Article 4(1) of Directive 2002/24/EC;
(b) prohibit the sale or installation on a vehicle."
2. In Annex I, "section 5 of Annex VI" is replaced by "section 4a of Annex VI" throughout the text.
Article 2
The following paragraph 3 is added to Article 3 of Directive 2005/30/EC:
"3. With effect from 1 January 2009, Member States shall refuse the sale or installation on a vehicle of replacement catalytic converters which are not of a type in respect of which a type-approval has been granted in compliance with Directive 97/24/EC, as amended by this Directive."
Article 3
1. Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive by 30 September 2007 at the latest. They shall forthwith communicate to the Commission the text of those provisions and a correlation table between those provisions and this Directive.
When Member States adopt those provisions, they shall contain a reference to this Directive or be accompanied by such a reference on the occasion of their official publication. Member States shall determine how such reference is to be made.
2. Member States shall communicate to the Commission the text of the main provisions of national law which they adopt in the field covered by this Directive.
Article 4
This Directive shall enter into force on the day of its publication in the Official Journal of the European Union.
Article 5
This Directive is addressed to the Member States.
Done at Brussels, 27 November 2006.
For the Commission
Günter Verheugen
Vice-President
[1] OJ L 226, 18.8.1997, p. 1. Directive as last amended by Commission Directive 2006/27/EC (OJ L 66, 8.3.2006, p. 7).
[2] OJ L 124, 9.5.2002, p. 1. Directive as last amended by Commission Directive 2005/30/EC (OJ L 106, 27.4.2005, p. 17).
--------------------------------------------------
