Council Decision
of 20 September 2005
appointing the Greek members and alternate members of the Advisory Committee on Social Security for Migrant Workers
(2005/C 242/01)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to Council Regulation (EEC) No 1408/71 of 14 June 1971 on the application of social security schemes to employed persons, to self-employed persons and to members of their families moving within the Community [1], and in particular Article 82 thereof,
Having regard to the lists of candidates submitted to the Council by the Governments of the Member States,
Whereas:
(1) By its Decision of 4 October 2004 [2] the Council appointed the members and alternate members of the Advisory Committee on Social Security for Migrant Workers for the period from 23 September 2004 to 22 September 2006, with the exception of the Greek members and alternate members.
(2) The Greek Government has submitted candidates for the seats to be filled,
HAS DECIDED AS FOLLOWS:
Sole Article
The following are hereby appointed members and alternate members of the Advisory Committee on Social Security for Migrant Workers for the term of office expiring on 23 September 2006:
I. GOVERNMENT REPRESENTATIVES
Country | Members | Alternates |
Greece | Ms Theodora TSOSOROU Ms Anna RIZOU | Mr Spyridon TSIANTIS |
II. TRADE UNION REPRESENTATIVES
Country | Members | Alternates |
Greece | Mr Marinos DIMITRAKOPOULOS Mr Apostolos KOKKINOS | Mr Evangelos KOKOSSIS |
III. REPRESENTATIVES OF EMPLOYERS' ORGANISATIONS
Country | Members | Alternates |
Greece | Mr Lambros PAPAÏOANNOU Mr C. GIANNOULOPOULOS | Mr G. CHATZIS |
Done at Brussels, 20 September 2005.
For the Council
The President
M. Beckett
[1] OL L 149, 5.7.1971, p. 2. Regulation as last amended by Regulation (EEC) No 1945/93 (OJ L 181, 23.7.1993, p. 1).
[2] OJ C 12, 18.1.2005, p. 9.
--------------------------------------------------
