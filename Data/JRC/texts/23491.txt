COUNCIL REGULATION (EC) No 1636/98 of 20 July 1998 amending Regulation (EEC) No 2075/92 on the common organisation of the market in raw tobacco
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Articles 42 and 43 thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
(1) Whereas Article 26 of Regulation (EEC) No 2075/92 (3), requires the Commission to submit proposals on the premium and quota arrangements governing the organisation of the market in raw tobacco;
(2) Whereas the present market situation is one of mismatch of supply and demand, largely due to the poor quality of Community production; whereas that situation calls for fundamental reform of the sector to improve its economic position; whereas such reform must entail a variation of Community aid in line with product quality, greater flexibility and simplicity in the setting of quotas, stricter control procedures and improved observance of public health and environmental protection requirements;
(3) Whereas the premium for flue-cured, light air-cured and dark air-cured tobaccos grown in Belgium, Germany, France and Austria should be increased; whereas the Council will reduce, in accordance with the procedure set out in Article 43(2) of the Treaty, the guarantee thresholds of these Member States in order to ensure the maintenance of budgetary neutrality;
(4) Whereas, in order to encourage improvement of the quality and value of Community production, and at the same time to provide income support to producers, the payment of part of the premium should be linked to the value of the tobacco produced; whereas the extent of this differentiation may vary by variety and tobacco-growing Member State; whereas, if it is to be effective, differentiation should operate within a certain range; whereas, given the magnitude of the changes made, a transitional period should be set; whereas this system should be established within the producer groups, while permitting a comparison to be made of the market prices obtained by individual producers;
(5) Whereas it is essential to reinforce control procedures in the tobacco sector; whereas the definitions of 'producer`, 'first processor` and 'first processing` should be made more precise and control agencies should be allowed access to all information relevant to their task;
(6) Whereas an auction system for cultivation contracts should be established so that contract prices for tobacco truly reflect market conditions; whereas this system should be optional for the Member States, to take account of their different structures;
(7) Whereas by participating in determination of the purchase price of the tobacco delivered the processor plays a central role in determination of the premium to be paid to the individual producer; whereas first processors benefit indirectly from Community aid by acquiring a subsidised product; whereas national authorities should be allowed to take appropriate action against any first processors not complying with Community rules; whereas to that end only approved first processors should be able to sign cultivation contracts, approval being withdrawn for non-compliance;
(8) Whereas to simplify management of the sector, producer groups should be made responsible for paying the variable part of the premium to producers and for allocating production quotas to their members;
(9) Whereas transfer of production quotas between producers should be permitted to improve production structures; whereas quota buy-back arrangements should be introduced to help producers who wish to leave the sector but find no purchasers for their quotas;
(10) Whereas due account should be taken of the requirements of public health and respect for the environment; whereas to that end the premium deduction financing the Community fund for tobacco research and information should be doubled and the specific aid used not only to help producer groups discharge their new management functions but also to finance action aimed at increasing respect for the environment,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EEC) No 2075/92 is amended as follows:
1. Article 3(1) and (2) shall be replaced by the following:
'1. From the 1999 harvest onwards, a premium scheme shall apply to tobacco, its amount being set for all the tobacco varieties shown in each of the different groups.
2. A supplementary amount shall, however, be granted on flue-cured, light air-cured and dark air-cured tobaccos grown in Austria, Belgium, France and Germany. That amount shall be equal to 65 % of the difference between the premium granted for those tobaccos in accordance with paragraph 1 and the premium applicable to the 1992 harvest.`;
2. the following Article shall be inserted:
'Article 4a
1. The premium shall comprise a fixed part, a variable part and a specific aid.
2. The variable part of the premium shall account for 30 % to 45 % of the total premium. It shall be introduced in stages up to the 2001 harvest. It may be adjusted within that specified range, according to variety group and Member State.
3. The fixed part of the premium shall be paid either to producer groups for distribution to the members of the group or to individual producers who are not members of a group.
4. The variable part shall be paid to producer groups for distribution to each member in proportion to the purchase price paid by the first processor for his crop.
5. Specific aid, not exceeding 2 % of the total premium, shall be paid to producer groups.`;
3. Articles 6 and 7 shall be replaced by the following:
'Article 6
1. Cultivation contracts shall be concluded between first processors of tobacco and producer groups or individual producers who are not members of a group.
2. For the purposes of this Regulation:
- the term "producer" shall mean individual producers who are not members of a group, individual producers who are members of a group, or producer groups, all of whom deliver their crop of raw tobacco to a first processor under a cultivation contract,
- a "first processor" shall mean any approved natural or legal person who carries out first processing of raw tobacco by operation, in his own name and on his own account, of one or more first tobacco-processing establishments suitably equipped for that purpose,
- "first processing" shall mean the processing of raw tobacco delivered by a producer into a stable, storable product put up in uniform bales of a quality meeting final user (manufacturer) requirements.
3. The cultivation contract shall include:
- a commitment by the first processor to pay the producer the purchase price according to quality grade,
- a commitment by the producer to deliver to the first processor the raw tobacco meeting the quality requirements specified in the contract.
4. The Member State's competent body shall, on submission of proof of delivery of the tobacco and of payment of the price as referred to in the first indent of paragraph 3, pay:
- the fixed part of the premium to the producer group or to the individual producer not a member of a group,
- the variable part of the premium and the specific aid to the producer group.
However, on a transitional basis and for a period not exceeding two harvests, the premium may be paid through the intermediary of the first processor.
5. The Member State may, if its structures make it appropriate, apply a cultivation contract auction scheme covering all contracts referred to in paragraph 1 that were concluded before the date on which delivery of the tobacco commences.
Article 7
Rules for the application of this Title shall be adopted in accordance with the procedure laid down in Article 23.
Their scope shall include:
- delimitation of production zones for each variety,
- quality requirements for tobacco delivered,
- other details of the cultivation contract and the closing date for its conclusion,
- any requirement of a security to be lodged by producers applying for an advance, and the terms of its provision and release,
- determination of the variable part of the premium,
- specific premium terms for cultivation contracts concluded with producer groups,
- action to be taken if the producer or first processor fails to meet his obligations,
- the cultivation-contract auction scheme, including the option for the first purchaser to match any offers.`;
4. Articles 8 to 14 shall be replaced by the following:
'Article 8
A maximum overall guarantee threshold of 350 600 tonnes of raw leaf tobacco per harvest shall be set for the Community.
Within that quantity the Council shall set individual guarantee thresholds for each variety group for three consecutive harvests, in accordance with the procedure laid down in Article 43(2) of the Treaty.
Article 9
1. To ensure observance of the guarantee thresholds, production quotas shall be imposed.
2. The Council, acting in accordance with the procedure laid down in Article 43(2) of the Treaty, shall allocate the quantity available for each variety group between producer Member States for three consecutive harvests.
3. On the basis of the quantities set pursuant to paragraph 2 and without prejudice to paragraphs 4 and 5, Member States shall assign production quotas to individual producers who are not members of a producer group and to producer groups, in proportion to the average quantity of tobacco of the particular variety group delivered for processing by each individual producer over the three years preceding that of the most recent harvest.
4. Before the final date for the conclusion of cultivation contracts, Member States may be authorised to transfer parts of their guarantee threshold allocations to other variety groups, in accordance with paragraph 3.
Subject to the third subparagraph, a one-tonne reduction in the allocation for one variety group shall give rise to an increase of at most one tonne in the allocation for the other variety group.
No transfer of parts of guarantee threshold allocations from one variety group to another may give rise to additional costs to the EAGGF.
The quantities authorised for transfer shall be determined in accordance with the procedure laid down in Article 23.
5. National quota reserves shall be set up, the rules of operation of which shall be adopted in accordance with the procedure laid down in Article 23.
Article 10
1. No premium may be granted on any quantity in excess of a producer's quota.
2. Notwithstanding paragraph 1, a producer may deliver excess production of up to 10 % of his quota for each variety group, this surplus being eligible for the premium granted on the following harvest, provided that he reduces his production for that harvest accordingly so that the combined quota for the two harvests is observed.
3. Member States shall keep accurate data on the production of all individual producers so that, where appropriate, production quotas can be assigned to them.
4. Production quotas may be transferred between individual producers in the same Member State.
Article 11
Rules for the application of this Title shall be adopted in accordance with the procedure laid down in Article 23.
TITLE III
Measures to convert production
Article 12
The specific aid referred to in Article 4a shall be paid to producer groups for the purposes of improving respect for the environment, boosting production quality, strengthening management and ensuring compliance with Community rules within the group.
Article 13
1. A Community Tobacco Fund financed by a deduction of 2 % of the premium shall be set up.
2. The Fund shall finance action in the following areas:
(a) combating tobacco-smoking, in particular informing the public of the dangers of tobacco consumption,
(b) - research to create or develop new varieties and cultivation methods that result in less harm to human health and are better geared to market conditions and more environment-friendly,
- the creation or development of alternative uses for raw tobacco,
(c) studies of the possibilities for producers of raw tobacco of switching to other crops or activities,
(d) the dissemination of the results obtained in the above areas to national authorities and to the sectors concerned.
Article 14
1. In order to facilitate the voluntary departure from the sector by individual producers, a quota buy-back programme with corresponding reduction of the guarantee thresholds referred to in Article 8 shall be set up.
2. Rural development programmes for the conversion of tobacco-growing regions in difficulty to other activities may be implemented under Community structural policies.`;
5. the following Article shall be inserted:
'Article 14a
Rules for the application of this Title shall be adopted in accordance with the procedure laid down in Article 23. Those rules shall cover:
- fixing of the specific aid amount,
- the definition of producer groups eligible for specific aid,
- terms for the recognition of groups,
- use of the specific aid, and in particular its allocation between the purposes specified in Article 12(1),
- setting the level of the quota buy-back price, which should not be such as to encourage any excessive exodus of producers from the sector,
- definition, on the basis of a proposal from the Member State, of sensitive production areas and/or groups of high-quality varieties to be exempted from the quota buy-back system, which may not affect more than 25 % of each Member State's guarantee threshold,
- definition of a period of not more than four months between the individual producer's intention to sell his quota and the actual buy-back; during that period the Member State shall make public the intention to sell so that other producers may buy the quota before it is actually bought back.`;
6. the heading of Title V shall be replaced by the following:
'TITLE V
Control measures`;
7. Article 17 shall be replaced by the following:
'Article 17
1. Member States shall take all necessary action to ensure and verify compliance with Community provisions concerning raw tobacco.
2. Member States shall make arrangements for granting entitlement to first processors to sign cultivation contracts.
3. Entitlement shall be withdrawn by the Member State if the processor deliberately or through serious negligence fails to comply with the Community provisions concerning raw tobacco.
4. Member States shall take the action necessary for their control bodies to be able to verify compliance with Community provisions, and in particular:
- to have access to production and processing facilities,
- to be able to acquaint themselves with first processors' accounting and stock records and with other relevant documents and take copies or extracts,
- to be able to obtain all relevant information, particularly in order to check that tobacco delivered has actually been processed,
- to obtain exact figures for the volume and purchase price of the production of all individual producers,
- to check the quality of the tobacco and payment by the processor of a purchase price to the individual producer,
- to check each year the areas planted by individual producers.
5. Rules for the application of this Title shall be adopted in accordance with the procedure laid down in Article 23.`;
8. the following heading shall be inserted after Article 17:
'TITLE VI
General and transitional provisions`;
9. Article 20 shall be replaced by the following:
'Article 20
To deal with unforeseen circumstances, exceptional market support measures may be taken in accordance with the procedure laid down in Article 23. Their scope and duration shall be strictly limited to what is necessary to support the market.`;
10. Article 26 shall be replaced by the following:
'Article 26
Before 1 April 2002, the Commission shall submit a report to the European Parliament and to the Council on the functioning of the common organisation of the market in raw tobacco.`;
11. the following paragraph shall be added to Article 27:
'Where transitional measures prove necessary to facilitate the application of the amendments to this Regulation introduced by Regulation (EC) No 1636/98 (*), such measures shall be adopted in accordance with the procedure laid down in Article 23.
(*) OJ L 210, 28.7.1998, p. 23.`
Article 2
This Regulation shall enter into force on the seventh day following that of its publication in the Official Journal of the European Communities.
It shall apply from the 1999 harvest.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 20 July 1998.
For the Council
The President
W. MOLTERER
(1) OJ C 108, 7. 4. 1998, p. 87.
(2) OJ C 210, 6. 7. 1998.
(3) OJ L 215, 30. 7. 1992, p. 70. Regulation as last amended by Regulation (EC) No 2595/97 (OJ L 351, 23. 12. 1997, p. 11).
