[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 26.4.2006
COM(2006) 188 final
2006/0062 (CNS)
Proposal for a
COUNCIL DECISION
on the signature of the Agreement between the European Community and the Russian Federation on the facilitation of issuance of short-stay visas
Proposal for a
COUNCIL DECISION
on the conclusion of the Agreement between the European Community and the Russian Federation on the facilitation of issuance of short-stay visas
(presented by the Commission)
EXPLANATORY MEMORANDUM
I. POLITICAL AND LEGAL BACKGROUND
The EU and Russia agreed at the St. Petersburg Summit to conclude timely the negotiations on a readmission agreement and to promote the better use of existing flexibilities in the Schengen agreement regarding visa.
Following the authorization given by the Council to the Commission on 12 July 2004, negotiations with the Russian Federation on the facilitation of the issuance of short-say visas were opened in Brussels on 20-21 October 2004, back-to-back with the continuation of negotiations on a readmission agreement. Six rounds of negotiations and four technical meetings were held. The final texts of the readmission and visa facilitation agreements were initialled (first and last page) by the Vice-President of the European Commission, Mr Franco Frattini, and Presidential Aide, Mr Viktor Ivanov, at the occasion of the PPC JHA Troika meeting between the Russian Federation and the EU on 13 October 2005 in Luxembourg. The formal initialling of the complete texts of the two agreements took place in Moscow on 4 April 2006.
Member States have been regularly informed and consulted in relevant Council Working groups and committees at all stages of the negotiations.
On the part of the Community, the legal basis for the Agreement is Article 62 par. 2 lit. b), in conjunction with Article 300 TEC.
The attached proposals constitute the legal instruments for the signature and conclusion of the Agreement. The Council will decide by qualified majority. The European Parliament will have to be formally consulted on the conclusion of the Agreement, in accordance with Art. 300 par. 3 EC Treaty.
The proposed decision concerning the conclusion sets out the necessary internal arrangements for the practical application of the Agreement. In particular, it specifies that the European Commission, assisted by experts from Member States, represents the Community within the Joint Committee set up by Article 13 of the Agreement.
Under Article 13(4), the visa facilitation Joint Committee may adopt its own rules of procedure. The Community position in this regard shall be established by the Commission in consultation with a special committee designated by the Council.
II. OUTCOME OF NEGOTIATIONS
The Commission considers that the objectives set by the Council in its negotiating directives were attained and that the draft visa facilitation agreement is acceptable to the Community.
The final content of it can be summarised as follows:
- in principle, for all visa applicants, a decision on whether or not to issue a visa will have to be taken within 10 calendar days. This period may be extended up to 30 days when further scrutiny is needed. In urgent cases, the period for taking a decision may be reduced to 3 days or less;
- the visa fee applied by Russia have been very substantially reduced by aligning it to 35 € (current Schengen visa fee). This fee will be applied to all EU and Russian citizens (including tourists) and concerns both single and multiple-entry visas. There is a possibility of charging a higher fee of 70 € in case of urgent requests, where the visa application and supporting documents are submitted by the visa applicant without justification only three days or less before his/her departure. This does not apply to cases related to humanitarian or health reasons, and death of relatives. Moreover, certain categories of persons benefit from a waiving of the visa fee: close relatives, officials participating in government activities, students, persons participating in cultural and educational exchange programmes or sporting events and humanitarian cases;
- the documents to be presented have been simplified for some categories of persons: close relatives, business people, members of official delegations, students, participants in scientific, cultural and sporting events, journalists, persons visiting military and civil burials grounds, drivers conducting international cargo and passenger transportation services. For these categories of persons, only the documents listed in the agreement can be requested for justifying the purpose of the journey. No other justification, invitation or validation provided for by the legislation of the Parties is required;
- there are also simplified criteria for issuing multiple-entry visas for the following categories of persons:
a) for members of national and regional governments and parliaments, Constitutional and Supreme Courts, and spouses and children visiting citizens of the EU or the Russian Federation legally residing in the other Party: visa valid up to five years (or shorter, limited to the period of the validity of their mandate or authorisation for legal residence, respectively).
b) for members of official delegations, business people, participants in scientific, cultural and sporting events, journalists and drivers and train crews, provided that during the previous two years they have made good use of a 1 year multiple-entry visas and the reasons for requesting a multiple-entry are still valid: visas valid for a minimum of 2 years and a maximum of 5 years are issued;
- holders of diplomatic passports are exempted from the visa requirement for short-stays. A Joint Declaration states that each Party might invoke a suspension of the provision related to visa waiver for holders of diplomatic passports (article 11), if the implementation of this provision is abused by the other Party or leads to a threat to public security.
- regarding registration procedures, both Parties agree to undertake measures as soon as possible with a view to simplify them. In an exchange of letters between the UK Presidency and the Russian Ambassador to the EU dated 6 October 2005, it was agreed that this issue be considered regularly in the framework of JHA related meeting between the EU and Russia. The EU-Russia Permanent Partnership Council (JHA), held in Luxembourg on 13 October 2005, offered a first opportunity for a positive exchange of views on this issue.
- a protocol was agreed, according to which the European Community would undertake measures to simplify the transit of holders of Schengen visa or Schengen residence permits through the territory of the Member States that do not fully apply the Schengen acquis yet. The Commission presented on 22 August 2005 a proposal for a simplified regime for the control of persons at the external borders based on the unilateral recognition by new Member States of certain documents issued by Schengen States. Negotiations on this proposal are progressing and should be concluded in the coming months.
The specific situations of Denmark, the United Kingdom and Ireland are reflected in the preamble and in two joint declarations attached to the Agreement. The close association of Norway and Iceland to the implementation, application and development of the Schengen Acquis is likewise reflected in a joint declaration to the Agreement.
Since the two agreements on visa facilitation and readmission are linked, both agreements should be signed, concluded and enter into force simultaneously.
III. CONCLUSIONS
In the light of the above-mentioned results, the Commission proposes that the Council
- decide that the Agreement be signed on behalf of the Community and authorise the President of the Council to appoint the person(s) duly empowered to sign on behalf of the Community;
- approve, after consultation of the European Parliament, the attached Agreement between the European Community and the Russian Federation on the facilitation of the issuance of short-stay visas.
Proposal for a
COUNCIL DECISION
on the signature of the Agreement between the European Community and the Russian Federation on the facilitation of issuance of short-stay visas
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 62,(2)(b)(i) and (ii), in conjunction with the first sentence of the first subparagraph of Article 300(2) thereof,
Having regard to the proposal from the Commission[1],
Whereas:
(1) By its decision of 12 July 2004, the Council authorised the Commission to negotiate an agreement between the European Community and the Russian Federation regarding the facilitation of the issuance of short-stay visas.
(2) Negotiations on the agreement were opened on 20-21 October 2004 and concluded on 12 October 2005.
(3) Subject to its possible conclusion at a later date, it is desirable to sign the agreement that was initialled in Moscow on 4 April 2006.
(4) In accordance with the Protocol on the position of the United Kingdom and Ireland, and the Protocol integrating the Schengen acquis into the framework of the European Union, the United Kingdom and Ireland do not take part in the adoption of this Decision and are therefore not bound by it or subject to its application.
(5) In accordance with the Protocol on the position of Denmark, annexed to the Treaty on European Union and to the Treaty establishing the European Community, Denmark does not take part in the adoption of this Decision and is therefore not bound by it or subject to its application.
HAS DECIDED AS FOLLOWS:
Sole Article
Subject to a possible conclusion at a later date, the President of the Council is hereby authorised to designate the person(s) empowered to sign, on behalf of the European Community, the Agreement between the European Community and the Russian Federation on the facilitation of the issuance of short-stay visas and the related documents consisting of the text of the agreement, a Protocol and seven joint declarations.
Done in Brussels, the………..of…………2006.
For the Council
The President
2006/0062 (CNS)
Proposal for a
COUNCIL DECISION
on the conclusion of the Agreement between the European Community and the Russian Federation on the facilitation of issuance of short-stay visas
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 62(2)(b)(i) and (ii), in conjunction with the first sentence of the first subparagraph of Article 300 (2) and the first subparagraph of Article 300 (3) thereof,
Having regard to the proposal from the Commission[2],
Having regard to the opinion of the European Parliament[3]
Whereas:
(1) The Commission has negotiated on behalf of the European Community an Agreement with the Russian Federation on the facilitation of the issuance of short-stay visas;
(2) This Agreement has been signed, on behalf of the European Community, on …….2006 subject to its possible conclusion at a later date, in accordance with Decision……../……/EC of the Council of [………….]
(3) This Agreement should be approved.
(4) The Agreement establishes a Joint Committee for the management of the Agreement, which may adopt its rules of procedure. It is appropriate to provide for a simplified procedure for the establishment of the Community position in this case.
(5) In accordance with the Protocol on the position of the United Kingdom and Ireland, and the Protocol integrating the Schengen acquis into the framework of the European Union, the United Kingdom and Ireland do not take part in the adoption of this Decision and are therefore not bound by it or subject to its application.
(6) In accordance with the Protocol on the position of Denmark, annexed to the Treaty on European Union and to the Treaty establishing the European Community, Denmark does not take part in the adoption of this Decision and is therefore not bound by it or subject to its application.
HAS DECIDED AS FOLLOWS:
Article 1
The Agreement between the European Community and the Russian Federation on the facilitation of the issuance of short-stay visas is hereby approved on behalf of the Community.
The text of the agreement is attached to this Decision.
Article 2
The President of the Council shall give the notification provided for in Article 15 paragraph 1 of the Agreement[4].
Article 3
The Commission, assisted by experts from Member States, shall represent the Community in the Joint Committee of experts established by Article 13 of the Agreement.
Article 4
The position of the Community within the Joint Committee of experts with regard to the adoption of its rules of procedure as required under Article 13 (4) of the Agreement shall be taken by the Commission after consultation with a special committee designated by the Council.
Done in Brussels, the………of…………2006.
For the Council
The President
Annex
AGREEMENT
between
the European Community and the Russian Federation
on the facilitation of the issuance of visas
to the citizens of the European Union and the Russian Federation
THE PARTIES,
THE EUROPEAN COMMUNITY, hereinafter referred to as “the Community” and THE RUSSIAN FEDERATION,
Desiring to facilitate people to people contacts as an important condition for a steady development of economic, humanitarian, cultural, scientific and other ties, by facilitating the issuing of visas to the citizens of the European Union and the Russian Federation on the basis of reciprocity;
Having regard to the Joint Statement agreed on the occasion of the St.Petersburg Summit held on 31 May 2003 stating that the European Union and the Russian Federation agree to examine the conditions for visa-free travel as a long term perspective;
Reaffirming the intention to establish the visa-free travel regime between the Russian Federation and the European Union;
Bearing in mind the Agreement on Partnership and Co-operation of 24 June 1994 establishing a Partnership between the European Communities and their Member States, on the one part, and the Russian Federation, on the other part;
Having regard to the Joint Statement on EU Enlargement and EU-Russia Relations agreed on 27 April 2004 confirming the intention of the European Union and the Russian Federation to facilitate visa issuance for the citizens of the European Union and the Russian Federation on a reciprocal basis and to launch negotiations with a view to concluding an agreement;
Recognising that this facilitation should not lead to illegal migration and paying special attention to security and readmission;
Taking into account the Protocol on the position of the United Kingdom of Great Britain and Northern Ireland and Ireland and the Protocol integrating the Schengen acquis into the framework of the European Union, annexed to the Treaty on European Union of 7 February 1992 and the Treaty establishing the European Community of 25 March 1957 and confirming that the provisions of this agreement do not apply to the United Kingdom of Great Britain and Northern Ireland and Ireland;
Taking into account the Protocol on the position of Denmark annexed to the Treaty on European Union of 7 February 1992 and the Treaty establishing the European Community of 25 March 1957 and confirming that the provisions of this agreement do not apply to the Kingdom of Denmark.
HAVE AGREED AS FOLLOWS:
Article 1- Purpose and scope of application
The purpose of this Agreement is to facilitate, on the basis of reciprocity, the issuance of visas for an intended stay of no more than 90 days per period of 180 days to the citizens of the European Union and the Russian Federation.
Article 2- General clause
1. The visa facilitations provided in this Agreement shall apply to citizens of the European Union and of the Russian Federation only insofar as they are not exempted from the visa requirement by the laws and regulations of the Russian Federation, of the Community or the Member States, the present agreement or other international agreements.
2. The national law of the Russian Federation, or of the Member States or Community law shall apply to issues not covered by the provisions of this Agreement, such as the refusal to issue a visa, recognition of travel documents, proof of sufficient means of subsistence and the refusal of entry and expulsion measures.
Article 3- Definitions
For the purpose of this Agreement:
a) " Member State " shall mean any Member State of the European Union, with the exception of the Kingdom of Denmark, Ireland and the United Kingdom of Great Britain and Northern Ireland;
b) “ Citizen of the European Union ” shall mean a national of a Member State as defined in point (a);
c) “ Citizen of the Russian Federation ” shall mean a person, who possesses or has acquired citizenship of the Russian Federation in accordance with its national legislation.
d) “ Visa ” shall mean an authorization/permission issued or a decision taken by a Member State or by the Russian Federation which is required with a view to:
- entry for an intended stay of no more than 90 days in total in that Member State or in several Member States or in the Russian Federation,
- entry for transit through the territory of that Member State or several Member States or of the Russian Federation.
e) “legally residing person” shall mean:
- for the Russian Federation, a citizen of the European Union who acquired a permission for temporary residing, a residence permit or an educational or working visa for a period of more than 90 days in the Russian Federation;
- for the European Union, a citizen of the Russian Federation authorized or entitled to stay for more than 90 days in the territory of a Member State, on the basis of Community or national legislation.
Article 4- Documentary evidence regarding the purpose of the journey
1. For the following categories of citizens of the European Union and of the Russian Federation, the following documents are sufficient for justifying the purpose of the journey to the other Party:
a) for members of official delegations who, following an official invitation addressed to the Member States, the European Union or the Russian Federation, shall participate in meetings, consultations, negotiations or exchange programmes, as well as in events held in the territory of the Russian Federation or one of the Member States by intergovernmental organisations:
- a letter issued by a competent authority of a Member State or of the Russian Federation, or by a European institution confirming that the applicant is a member of its delegation travelling to the territory of the other Party to participate at the aforementioned events, accompanied by a copy of the official invitation;
b) for business people and representatives of business organisations:
- a written request from a host legal person or company, organisation, or an office or their branches, state and local authorities of the Russian Federation and the Member States or organizing committees of trade and industrial exhibitions, conferences and symposia held in the territories of the Russian Federation or one of the Member States;
c) for drivers conducting international cargo and passenger transportation services between the territories of the Russian Federation and the Member States in vehicles registered in the Member States or in the Russian Federation:
- a written request from the national association (union) of carriers of the Russian Federation or the national associations of carriers of the Member States providing for international road transportation, stating the purpose, duration and frequency of the trips;
d) for members of train, refrigerator and locomotive crews in international trains, traveling between the territories of the Member States and the Russian Federation:
- a written request from the competent railway company of the Russian Federation or the Member States stating the purpose, duration and frequency of the trips;
e) for journalists:
- a certificate or other document issued by a professional organisation proving that the person concerned is a qualified journalist and a document issued by his/her employer stating that the purpose of the journey is to carry out journalistic work;
f) for persons participating in scientific, cultural and artistic activities, including university and other exchange programmes:
- a written request from the host organisation to participate in those activities;
g) for pupils, students, post-graduate students and accompanying teachers who undertake trips for the purposes of study or educational training, including in the framework of exchange programmes as well as other school related activities:
- a written request or a certificate of enrolment from the host university, academy, institute, college or school or student cards or certificates of the courses to be attended.
h) for participants in international sports events and persons accompanying them in a professional capacity:
- a written request from the host organisation: competent authorities, national sport Federations of the Member States or the Russian Federation and National Olympic Committee of the Russian Federation or National Olympic Committees of the Member States;
i) for participants in official exchange programmes organised by twin cities:
- a written request of the Head of Administration/Mayor of these cities.
j) for close relatives - spouses, children (including adopted), parents (including custodians), grandparents and grandchildren - visiting citizens of the European Union or the Russian Federation legally residing in the territory of the Russian Federation or the Member States:
- a written request from the host person.
k) for visiting military and civil burial grounds:
- an official document confirming the existence and preservation of the grave as well as family or other relationship between the applicant and the buried.
2. The written request mentioned in paragraph 1 of this Article shall contain the following items:
a) for the invited person - name and surname, date of birth, sex, citizenship, number of the identity document, time and purpose of the journey, number of entries and name of minor children accompanying the invited person;
b) for the inviting person - name, surname and address or
c) for the inviting legal person, company or organisation - full name and address and
- if the request is issued by an organisation, the name and position of the person who signs the request;
- if the inviting person is a legal person or company or an office or their branch established in the territory of a Member State, the registration number as required by the national law of the Member State concerned;
- if the inviting person is a legal person or company or an office or their branch established in the territory of the Russian Federation, the tax identification number.
3. For the categories of citizens mentioned in paragraph 1 of this Article, all categories of visas are issued according to the simplified procedure without requiring any other justification, invitation or validation concerning the purpose of the journey, provided for by the legislation of the Parties.
Article 5 - Issuance of multiple-entry visas
1. Diplomatic missions and consular posts of the Member States and of the Russian Federation shall issue multiple-entry visas with the term of validity of up to five years to the following categories of citizens:
a) members of national and regional Governments and Parliaments, Constitutional Courts and Supreme Courts, if they are not exempted from the visa requirement by the present Agreement, in the exercise of their duties, with a term of validity limited to their term of office if this is less than 5 years;
b) spouses and children (including adopted), who are under the age of 21 or are dependant, visiting citizens of the European Union and the Russian Federation legally residing in the territory of the Russian Federation or the Member States, with the term of validity limited to the duration of the validity of their authorisation for legal residence.
2. Diplomatic missions and consular posts of the Member States and of the Russian Federation shall issue multiple-entry visas with the term of validity of up to one year to the following categories of citizens, provided that during the previous year they have obtained at least one visa, have made use of it in accordance with the laws on entry and stay in the territory of the visited State and that there are reasons for requesting a multiple-entry visa:
a) for members of official delegations who, following an official invitation addressed to the Member States, the European Union or the Russian Federation, shall participate in official meetings, consultations, negotiations or exchange programmes, as well as in events held in the territory of the Russian Federation or one of the Member States by intergovernmental organisations;
b) business people and representatives of business organisations who regularly travel to the Russian Federation or the Member States;
c) drivers conducting international cargo and passenger transportation services between the territories of the Russian Federation and the Member States in vehicles registered in the Member States or the Russian Federation;
d) members of train, refrigerator and locomotive crews in international trains, traveling between the territories of the Russian Federation and the Member States;
e) persons participating in scientific, cultural and artistic activities, including university and other exchange programmes, who regularly travel to the Russian Federation or the Member States;
f) participants in international sports events and persons accompanying them in a professional capacity;
g) journalists;
h) participants in official exchange programmes organised by twin cities.
3. Diplomatic missions and consular posts of the Member States and of the Russian Federation shall issue multiple-entry visas with the term of validity of a minimum of 2 years and a maximum of 5 years to the categories of citizens referred to in paragraph 2 of this Article, provided that during the previous two years they have made use of the one year multiple-entry visas in accordance with the laws on entry and stay in the territory of the visited State and that the reasons for requesting a multiple-entry visa are still valid.
4. The total period of stay of persons referred to in paragraphs 1 to 3 of this Article shall not exceed 90 days per period of 180 days in the territory of the Member States or in the Russian Federation.
Article 6-Fees for processing visa applications
1. The fee for processing visa applications shall amount to 35€.
The aforementioned amount may be reviewed in accordance with the procedure provided for in Article 15(4).
2. The Parties shall charge a fee of 70 € for processing visas in cases where the visa application and the supporting documents have been submitted by the visa applicant within three days before his/her envisaged date of departure. This will not apply to cases pursuant to Article 6(3), (b), (e) and (f) and Article 7(3).
3. Fees for processing the visa application are waived for the following categories of persons:
a) for close relatives - spouses, children (including adopted) parents (including custodians), grandparents and grandchildren-of citizens of the European Union and of the Russian Federation legally residing in the territory of the Russian Federation or the Member States;
b) for members of official delegations who, following an official invitation addressed to the Member States, the European Union or the Russian Federation, shall participate in meetings, consultations, negotiations or exchange programmes, as well as in events held in the territory of the Russian Federation or one of the Member States by intergovernmental organisations;
c) members of national and regional Governments and Parliaments, Constitutional Courts and Supreme Courts, if they are not exempted from the visa requirement by the present Agreement;
d) pupils, students, post-graduate students and accompanying teachers who undertake trips for the purpose of study or educational training;
e) disabled persons and the person accompanying them, if necessary;
f) persons who have presented documents proving the necessity of their travel on humanitarian grounds, including to receive urgent medical treatment and the person accompanying such person, or to attend a funeral of a close relative, or to visit a close relative seriously ill;
g) participants in youth international sports events and persons accompanying them.
h) persons participating in scientific, cultural and artistic activities including university and other exchange programmes;
i) participants in official exchange programmes organised by twin cities.
Article 7- Length of procedures for processing visa applications
1. Diplomatic missions and consular posts of the Member States and the Russian Federation shall take a decision on the request to issue a visa within 10 calendar days of the date of the receipt of the application and documents required for issuing the visa.
2. The period of time for taking a decision on a visa application may be extended up to 30 calendar days in individual cases, notably when further scrutiny of the application is needed.
3. The period of time for taking a decision on a visa application may be reduced to 3 working days or less in urgent cases.
Article 8- Departure in case of lost or stolen documents
Citizens of the European Union and of the Russian Federation who have lost their identity documents, or from whom these documents have been stolen while staying in the territory of the Russian Federation or the Member States, may leave that territory on the grounds of valid identity documents entitling to cross the border issued by diplomatic missions or consular posts of the Member States or of the Russian Federation without any visa or other authorization.
Article 9- Extension of visa in exceptional circumstances
The citizens of the European Union and of the Russian Federation who do not have the possibility to leave the territory of the Russian Federation and of the Member States by the time stated in their visas for reasons of force majeure shall have the term of their visas extended free of charge in accordance with the legislation applied by the receiving State for the period required for their return to the State of their residence.
Article 10- Registration procedures
The Parties agree to undertake measures as soon as possible to simplify the procedures of registration, with the view to entitle the citizens of the Russian Federation and citizens of the European Union to the equal treatment regarding registration procedures while staying in the territory of the Russian Federation or of the Member States, respectively.
Article 11- Diplomatic passports
1. Citizens of the Russian Federation or the Member States, holders of valid diplomatic passports can enter, leave and transit through the territories of the Member States or the Russian Federation without visas.
2. Citizens mentioned in paragraph 1 of this Article may stay in the territories of the Russian Federation or the Member States for a period not exceeding 90 days per period of 180 days.
Article 12- Territorial validity of visas
Subject to the national rules and regulations concerning national security of the Russian Federation and of the Member States and subject to EU rules on visas with limited territorial validity, the citizens of the Russian Federation and of the European Union shall be entitled to travel within the territory of the Member States and of the Russian Federation on equal basis with European Union and Russian citizens.
Article 13- Joint Committee for management of the Agreement
1. The Parties shall set up a Joint Committee for management of the Agreement (hereinafter referred to as “the Committee”), composed by representatives of the European Community and of the Russian Federation. The Community shall be represented by the European Commission, assisted by experts from the Member States.
2. The Committee shall, in particular, have the following tasks:
(a) monitoring the implementation of the present Agreement;
(b) suggesting amendments or additions to the present Agreement;
(c) examine and, if deemed necessary, propose amendments to this agreement in case of new accessions to the European Union.
3. The Committee shall meet whenever necessary at the request of one of the Parties and at least once a year.
4. The Committee shall establish its rules of procedure.
Article 14- Relation of this Agreement with Agreements between Member States and the Russian Federation
As from its entry into force, this Agreement shall take precedence over provisions of any bilateral or multilateral agreements or arrangements concluded between Member States and the Russian Federation, insofar as the provisions of the latter agreements or arrangements cover issues that are dealt with by the present Agreement.
Article 15- Final clauses
1. This Agreement shall be ratified or approved by the Parties in accordance with their respective procedures and shall enter into force on the first day of the second month following the date on which the Parties notify each other that the procedures referred to above have been completed.
2. By way of derogation to paragraph 1 of this Article, the present agreement shall only enter into force at the date of the entry into force of the agreement between the Russian Federation and the European Community on readmission if this date is after the date provided for in paragraph 1 of this Article.
3. This Agreement is concluded for an indefinite period of time, unless terminated in accordance with paragraph 6 of this Article.
4. This Agreement may be amended by written agreement of the Parties. Amendments shall enter into force after the Parties have notified each other of the completion of their internal procedures necessary for this purpose.
5. Each Party may suspend in whole or in part this Agreement for reasons of public order, protection of national security or protection of public health. The decision on suspension shall be notified to the other Party not later than 48 hours before its entry into force. The Party that has suspended the application of this Agreement shall immediately inform the other Party once the reasons for the suspension no longer apply.
6. Each Party may terminate this Agreement by giving written notice to the other Party. This Agreement shall cease to be in force 90 days after the date of receipt of such notification.
Done in XXX on XXX, in duplicate each in the Czech, Danish, Dutch, English, Estonian, Finnish, French, German, Greek, Hungarian, Italian, Latvian, Lithuanian, Maltese, Polish, Portuguese, Slovak, Slovenian, Spanish, Swedish and Russian languages, each of these texts being equally authentic.
For the European Community | For the Russian Federation |
PROTOCOL TO THE AGREEMENT ON THE MEMBER STATES THAT DO NOT FULLY APPLY THE SCHENGEN ACQUIS
Those Member States which are bound by the Schengen acquis but which do not yet issue Schengen visas, while awaiting the relevant decision of the Council to that end, shall issue national visas the validity of which is limited to their own territory.
The European Community will undertake as soon as possible measures to simplify the transit of holders of Schengen visa or Schengen residence permits through the territory of the Member States that do not fully apply the Schengen acquis yet.
JOINT DECLARATION ON ARTICLE 6(2) OF THE AGREEMENT ON FEES FOR PROCESSING VISA APPLICATIONS
The Parties shall examine the implementation of Article 6(2) of this Agreement in the framework of the Committee set up by the Agreement.
JOINT DECLARATION ON ARTICLE 11 OF THE AGREEMENT ON DIPLOMATIC PASSPORTS
Each Party may invoke a partial suspension of the agreement and in particular of Article 11, in accordance with the procedure set up by Article 15 (5) of this Agreement, if the implementation of Article 11 is abused by the other Party or leads to a threat to public security.
If the implementation of Article 11 is suspended, both Parties shall initiate consultations in the framework of the Committee set up by the Agreement with a view to solve the problems that lead to the suspension.
As a priority, both Parties declare their commitment to ensure a high level of document security for diplomatic passports, in particular by integrating biometric identifiers. For the European Union side, this will be ensured in compliance with the requirements set out in Regulation (EC) 2252/2004.
JOINT DECLARATION ON ISSUANCE OF SHORT-STAY VISAS FOR VISITS TO MILITARY AND CIVIL BURIAL GROUNDS
The Parties agree that, as a rule, short-stay visas for persons visiting military and civil burial grounds shall be issued for a period of up to 14 days.
JOINT DECLARATION ON THE HARMONISATION OF INFORMATION ON PROCEDURES FOR ISSUING SHORT STAY VISA AND DOCUMENTS TO BE SUBMITTED WHEN APPLYING FOR SHORT STAY VISAS
Recognising the importance of transparency for visa applicants, the Parties to the present Agreement consider that appropriate measures should be taken:
- In general, to draw up basic information for applicants on the procedures and conditions for applying for visas, the visas and on the validity of visas issued.
- Each Party, on its own, to draw up a list of minimum requirements in order to ensure that applicants are given coherent and uniform basic information and are required to submit, in principle, the same supporting documents.
The information mentioned above is to be disseminated widely (on the information board of consulates, in leaflets, on websites in the Internet etc.).
JOINT DECLARATION CONCERNING KINGDOM OF DENMARK
The Parties take note that the present Agreement does not apply to the procedures for issuing visas by the diplomatic missions and consular posts of the Kingdom of Denmark.
In such circumstances, it is desirable that the authorities of the Kingdom of Denmark and of the Russian Federation conclude, without delay, a bilateral agreement on the facilitation of the issuance of short-stay visas in similar terms as the Agreement between the European Community and the Russian Federation.
JOINT DECLARATION CONCERNING THE UNITED KINGDOM OF GREAT BRITAIN AND NORTHERN IRELAND AND IRELAND
The Parties take note that the present Agreement does not apply to the territory of the United Kingdom of Great Britain and Northern Ireland and Ireland.
In such circumstances, it is desirable that the authorities of the United Kingdom of Great Britain and Northern Ireland, Ireland and the Russian Federation, conclude bilateral agreements on the facilitation of the issuance of visas.
JOINT DECLARATION CONCERNING THE REPUBLIC OF ICELAND AND THE KINGDOM OF NORWAY
The Parties take note of the close relationship between the European Community and the Republic of Iceland and the Kingdom of Norway, particularly by virtue of the Agreement of 18 May 1999 concerning the association of these countries with the implementation, application and development of the Schengen acquis.
In such circumstances, it is desirable that the authorities of the Republic of Iceland, the Kingdom of Norway and the Russian Federation conclude, without delay, bilateral agreements on the facilitation of the issuance of short-stay visas in similar terms as this Agreement.
[1] OJ C, p. .
[2] OJ C…
[3] OJ C…
[4] The date of entry into force of the Agreement will be published in the Official Journal of the European Union [by the General Secretariat of the Council].
