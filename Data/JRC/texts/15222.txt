Notice of the expiry of certain anti-dumping measures
(2006/C 255/04)
Further to the publication of a notice of impending expiry [1], following which no request for a review was received, the Commission gives notice that the anti-dumping measures mentioned below will shortly expire.
This notice is published in accordance with Article 11(2) of Council Regulation (EC) No 384/96 of 22 December 1995 [2] on protection against dumped imports from countries not members of the European Community.
Product | Country(ies) of origin or exportation | Measures | Reference | Date of expiry |
Internal gear hubs for bicycles | Japan | Anti-dumping duty | Council Regulation (EC) No 2080/2001 (OJ L 282, 26.10.2001, p. 1). | 26.10.2006 |
[1] OJ C 30, 7.2.2006, p. 2.
[2] OJ L 56, 6.3.1996, p. 1. Regulation as last amended by Council Regulation (EC) No 2117/2005 (OJ L 340, 23.12.2005, p. 17).
--------------------------------------------------
