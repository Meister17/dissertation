COUNCIL REGULATION (EEC) No 1946/81 of 30 June 1981 restricting investment aids for milk production
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 43 thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas Article 8 (1) (b) of Council Directive 72/159/EEC of 17 April 1972 on the modernization of farms (4), as last amended by Directive 81/528/EEC (5), provides for the granting of aid for the investments necessary for carrying out a development plan ; whereas, in order to take account of the objective of market equilibrium in the Community, aid should not be granted for investment in milk production except under certain specific conditions,
HAS ADOPTED THIS REGULATION:
Article 1
1. Aids for investments in milk production shall be prohibited except in the case of those granted to farmers implementing a development plan in accordance with Directive 72/159/EEC, or farm improvement plans in the framework of common measures.
2. The aids referred to in paragraph 1 shall be limited to the investment such as will permit the achievement of the comparable earned income defined in Article 4 (2) of Directive 72/159/EEC for a maximum number of 1 75 man-work units per farm, and shall be subject to the condition that the investment does not increase the number of dairy cows at the end of the plan to more than 40 per man-work unit.
However, for farms with more than 1 75 man-work units, aids may be granted for investments such as will permit the increase in the number of dairy cows by no more than 15 % at the end of the plan.
3. Member States are authorized to grant investment aids to farmers who do not present a development plan provided that the investment does not increase the number of dairy cows to more than 40 per farm.
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Luxembourg, 30 June 1981.
For the Council
The President
G. BRAKS
(1) OJ No C 124, 17.5.1979, p. 1. (2) OJ No C 85, 8.4.1980, p. 57. (3) OJ No C 53, 3.3.1980, p. 22. (4) OJ No L 96, 23.4.1972, p. 1. (5) See page 41 of this Official Journal.
