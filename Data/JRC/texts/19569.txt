Commission Decision
of 7 March 2003
amending Regulation (EC) No 2037/2000 of the European Parliament and of the Council with regard to the use of halon 1301 and halon 1211
(notified under document number C(2003) 691)
(2003/160/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Regulation (EC) No 2037/2000 of the European Parliament and of the Council of 29 June 2000, on substances that deplete the ozone layer(1), as last amended by Regulation (EC) No 2039/2000(2), and in particular Article 4(4)(iv) thereof,
Whereas:
(1) The Commission has come, during the course of the review provided for in Article 4(4)(iv) of Regulation (EC) No 2037/2000, and after consultation with the military and other stakeholders, to the following findings with regard to the use of halon 1301 and halon 1211.
(2) Halon 1301 is currently used for fuel tank inerting in F-16 fighter aircraft. At present there are no alternatives available that have the ability to extinguish a fire and suppress an explosion in a volume-to-weight ratio acceptable for fuel tank inerting in a fighter aircraft. An alternative installed and operational on F-16 fighter aircraft is unlikely to be available in the foreseeable future and certainly not before 31 December 2003, when all non-exempted uses must be decommissioned pursuant to Article 4(4)(v) of Regulation (EC) No 2037/2000. This use of halon 1301 should therefore be added to the list of exempted halon uses in Annex VII in Regulation (EC) No 2037/2000.
(3) Both halon 1301 and halon 1211 are currently used in military land vehicles and naval vessels for the protection of spaces occupied by personnel and engine compartments. However, only the use of halon 1301 is covered by the exemption provided for in Regulation (EC) No 2037/2000. The conversion of that military equipment from the use of halon 1211 to the use of halon 1301 would be costly and counterproductive with regard to the protection of the ozone layer, since halon 1301 has an ozone-depleting potential more than three-times higher than halon 1211. Moreover, diversion of financial resources to convert equipment to the use of halon 1301 would most likely delay the development of alternatives with no ozone-depleting potential. The use of halon 1211 for the protection of spaces occupied by personnel and engine compartments in military land vehicles and naval vessels should therefore be added to the list of exempted halon uses in Annex VII in Regulation (EC) No 2037/2000.
(4) Regulation (EC) No 2037/2000 should therefore be amended accordingly.
(5) The measures provided for in this Decision are in accordance with the opinion of the Committee established by Article 18(1) of Regulation (EC) No 2037/2000,
HAS ADOPTED THIS DECISION:
Article 1
Annex VII to Regulation (EC) No 2037/2000 is replaced by the text in the Annex to this Decision.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 7 March 2003.
For the Commission
Margot Wallström
Member of the Commission
(1) OJ L 244, 29.9.2000, p. 1.
(2) OJ L 244, 29.9.2000, p. 26.
ANNEX
Annex VII to Regulation (EC) No 2037/2000 is replaced by the following:
"ANNEX VII
Critical uses of halon
Use of halon 1301:
- in aircraft for the protection of crew compartments, engine nacelles, cargo bays and dry bays, and fuel tank inerting,
- in military land vehicles and naval vessels for the protection of spaces occupied by personnel and engine compartments,
- for the making inert of occupied spaces where flammable liquid and/or gas release could occur in the military and oil, gas and petrochemical sector, and in existing cargo ships,
- for the making inert of existing manned communication and command centres of the armed forces or others, essential for national security,
- for the making inert of spaces where there may be a risk of dispersion of radioactive matter,
- in the Channel Tunnel and associated installations and rolling stock.
Use of halon 1211:
- in military land vehicles and naval vessels for the protection of spaces occupied by personnel and engine compartments,
- in hand-held fire extinguishers and fixed extinguisher equipment for engines for use on board aircraft,
- in aircraft for the protection of crew compartments, engine nacelles, cargo bays and dry bays,
- in fire extinguishers essential to personal safety used for initial extinguishing by fire brigades,
- in military and police fire extinguishers for use on persons."
