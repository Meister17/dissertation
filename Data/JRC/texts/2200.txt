Commission Regulation (EC) No 824/2005
of 30 May 2005
fixing certain indicative quantities and individual ceilings for the issue of licences for the purposes of the additional quantity in respect of banana imports to the new Member States for the third quarter of 2005
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to the Treaty of Accession of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia,
Having regard to the Act of Accession of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia, and in particular the first paragraph of Article 41 thereof,
Having regard to Council Regulation (EEC) No 404/93 of 13 February 1993 on the common organisation of the market in bananas [1],
Whereas:
(1) Commission Regulation (EC) No 1892/2004 [2] adopted the transitional measures needed to facilitate the transition from the arrangements in force in the new Member States prior to their accession to the European Union to the import arrangements in force under the common organisation of the markets in the banana sector for the year 2005. In order to ensure market supply, in particular in the new Member States, that Regulation fixed an additional quantity on a transitional basis for the purpose of issuing import licences. This additional quantity must be managed using the mechanisms and instruments put in place by Commission Regulation (EC) No 896/2001 [3] of 7 May 2001 laying down detailed rules for applying Council Regulation (EEC) No 404/93 as regards the arrangements for importing bananas into the Community.
(2) Article 14(1) and (2) of Regulation (EC) No 896/2001 provides that indicative quantities and individual ceilings may be fixed for the purposes of issuing import licences for each of the first three quarters of the year.
(3) For the purpose of issuing licences for the third quarter of the year 2005, it is appropriate to fix those indicatives quantities and individual ceilings at the same percentages as those fixed for the management of A/B and C tariff quotas in Commission Regulation (EC) No 825/2005 [4], so as to ensure adequate supplies and the continuation of trade flows between the production and marketing sectors.
(4) In view of the fact that this Regulation must apply before the start of the period for the submission of licence applications for the third quarter of 2005, provision should be made for this Regulation to enter into force immediately.
(5) This Regulation must apply to operators established in the Community and being registered in accordance with Articles 5 and 6 of Regulation (EC) No 1892/2004.
(6) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Bananas,
HAS ADOPTED THIS REGULATION:
Article 1
The indicative quantity referred to in Article 14(1) of Regulation (EC) No 896/2001 for the issue of import licences for bananas under the additional quantity provided for in Article 3(1) of Regulation (EC) No 1892/2004 is fixed, for the third quarter of 2005, at 23 % of the quantities available for respectively traditional operators and non-traditional operators as established in Article 4(2) of that Regulation.
Article 2
The maximum authorised quantity referred to in Article 14(2) of Regulation (EC) No 896/2001 for licence applications for the import of bananas under the additional quantity provided for in Article 3(1) of Regulation (EC) No 1892/2004 is fixed, for the third quarter of 2005, at:
(a) 23 % of the specific reference quantity notified in accordance with Article 5(5) of Regulation (EC) No 1892/2004, in the case of traditional operators;
(b) 23 % of the specific allocation notified in accordance with Article 6(6) of Regulation (EC) No 1892/2004, in the case of non-traditional operators.
Article 3
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 30 May 2005.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 47, 25.2.1993, p. 1. Regulation as last amended by the 2003 Act of Accession.
[2] OJ L 328, 30.10.2004, p. 50.
[3] OJ L 126, 8.5.2001, p. 6. Regulation as last amended by Regulation (EC) No 838/2004 (OJ L 127, 29.4.2004, p. 52).
[4] See page 13 of this Official Journal.
--------------------------------------------------
