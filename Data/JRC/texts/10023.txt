Commission Regulation (EC) No 560/2006
of 5 April 2006
fixing the definitive rate of refund and the percentage of system B export licences to be issued in the fruit and vegetables sector (tomatoes, oranges, lemons and apples)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 2200/96 of 28 October 1996 on the common organisation of the market in fruit and vegetables [1],
Having regard to Commission Regulation (EC) No 1961/2001 of 8 October 2001 on detailed rules for implementing Council Regulation (EC) No 2200/96 as regards export refunds on fruit and vegetables [2], and in particular Article 6(7) thereof,
Whereas:
(1) Commission Regulation (EC) No 2044/2005 [3] fixed the indicative quantities for the issue of B system export licences.
(2) The definitive rate of refund for tomatoes, oranges, lemons and apples covered by licences applied for under system B between 17 January and 16 March 2006, should be fixed at the indicative rate, and the percentage of licences to be issued for the quantities applied for should be laid down,
HAS ADOPTED THIS REGULATION:
Article 1
For applications for system B export licences submitted pursuant to Article 1 of Regulation (EC) No 2044/2005 between 17 January and 16 March 2006, the percentages of licences to be issued and the rates of refund applicable are fixed in the Annex hereto.
Article 2
This Regulation shall enter into force on 6 April 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 5 April 2006.
For the Commission
J. L. Demarty
Director-General for Agriculture and Rural Development
[1] OJ L 297, 21.11.1996, p. 1. Regulation as last amended by Commission Regulation (EC) No 47/2003 (OJ L 7, 11.1.2003, p. 64).
[2] OJ L 268, 9.10.2001, p. 8. Regulation as amended by Regulation (EC) No 386/2005 (OJ L 62, 9.3.2005, p. 3).
[3] OJ L 328, 15.12.2005, p. 54.
--------------------------------------------------
ANNEX
Percentages for the issuing of licences and rates of refund applicable to system B licences applied for between 17 January to 16 March 2006 (tomatoes, oranges, lemons and apples)
Product | Rate of refund (EUR/t net) | Percentages of licences to be issued for the quantities applied for |
Tomatoes | 30 | 100 % |
Oranges | 36 | 100 % |
Lemons | 60 | 100 % |
Apples | 34 | 100 % |
--------------------------------------------------
