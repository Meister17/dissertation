Commission Directive 2006/75/EC
of 11 September 2006
amending Council Directive 91/414/EEC to include dimoxystrobin as active substance
(Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/414/EEC of 15 July 1991 concerning the placing of plant protection products on the market [1], and in particular Article 6(1) thereof,
Whereas:
(1) In accordance with Article 6(2) of Directive 91/414/EEC the United Kingdom received on 28 November 2001 an application from BASF for the inclusion of the active substance dimoxystrobin in Annex I to Directive 91/414/EEC. Commission Decision 2002/593/EC [2] confirmed that the dossier was "complete" in the sense that it could be considered as satisfying, in principle, the data and information requirements of Annexes II and III to Directive 91/414/EEC.
(2) For this active substance, the effects on human health and the environment have been assessed, in accordance with the provisions of Article 6(2) and (4) of Directive 91/414/EEC, for the uses proposed by the applicants. The designated rapporteur Member State submitted draft assessment report concerning the substance dimoxystrobin to the European Food Safety Authority (EFSA) on 14 August 2003.
(3) The draft assessment report has been peer reviewed by the Member States and the EFSA within its Working Group Evaluation and presented to the Commission on 10 August 2005 in the format of the EFSA Scientific Report for dimoxystrobin [3]. This report has been reviewed by the Member States and the Commission within the Standing Committee on the Food Chain and Animal Health and finalised on 4 April 2006 in the format of the Commission review report for dimoxystrobin.
(4) It has appeared from the various examinations made that plant protection products containing the active substance concerned may be expected to satisfy, in general, the requirements laid down in Article 5(1)(a) and (b) and Article 5(3) of Directive 91/414/EEC, in particular with regard to the uses which were examined and detailed in the Commission review report. It is therefore appropriate to include dimoxystrobin in Annex I to that Directive, in order to ensure that in all Member States the authorisations of plant protection products containing this active substance may be granted in accordance with the provisions of that Directive.
(5) Without prejudice to the obligations defined by Directive 91/414/EEC as a consequence of including an active substance in Annex I, Member States should be allowed a period of six months after inclusion to review existing provisional authorisations of plant protection products containing dimoxystrobin to ensure that the requirements laid down by Directive 91/414/EEC, in particular in its Article 13 and the relevant conditions set out in Annex I, are satisfied. Member States should transform existing provisional authorisations into full authorisations, amend them or withdraw them in accordance with the provisions of Directive 91/414/EEC. By derogation from the above deadline, a longer period should be provided for the submission and assessment of the complete Annex III dossier of each plant protection product for each intended use in accordance with the uniform principles laid down in Directive 91/414/EEC.
(6) It is therefore appropriate to amend Directive 91/414/EEC accordingly.
(7) The measures provided for in this Directive are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Annex I to Directive 91/414/EEC is amended as set out in the Annex to this Directive.
Article 2
1. Member States shall adopt and publish by 31 March 2007 at the latest the laws, regulations and administrative provisions necessary to comply with this Directive. They shall forthwith communicate to the Commission the text of those provisions and a correlation table between those provisions and this Directive.
They shall apply those provisions from 1 April 2007.
When Member States adopt those provisions, they shall contain a reference to this Directive or shall be accompanied by such a reference on the occasion of their official publication. Member States shall determine how such reference is to be made.
2. Member States shall communicate to the Commission the text of the main provisions of national law which they adopt in the field covered by this Directive.
Article 3
1. Member States shall in accordance with Directive 91/414/EEC, where necessary, amend or withdraw existing authorisations for plant protection products containing dimoxystrobin as active substance by 31 March 2007. By that date, they shall in particular verify that the conditions in Annex I to that Directive relating to dimoxystrobin are met, with the exception of those identified in part B of the entry concerning those active substances, and that the holder of the authorisation has, or has access to, a dossier satisfying the requirements of Annex II to that Directive in accordance with the conditions of Article 13.
2. By way of derogation from paragraph 1, for each authorised plant protection product containing dimoxystrobin as either the only active substance or as one of several active substances all of which were listed in Annex I to Directive 91/414/EEC by 30 September 2006 at the latest, Member States shall re-evaluate the product in accordance with the uniform principles provided for in Annex VI to Directive 91/414/EEC, on the basis of a dossier satisfying the requirements of Annex III to that Directive and taking into account part B of the entry in Annex I to that Directive concerning dimoxystrobin. On the basis of that evaluation, they shall determine whether the product satisfies the conditions set out in Article 4(1)(b), (c), (d) and (e) of Directive 91/414/EEC.
Following that determination Member States shall:
(a) in the case of a product containing dimoxystrobin as the only active substance, where necessary, amend or withdraw the authorisation by 31 March 2008 at the latest; or
(b) in the case of a product containing dimoxystrobin as one of several active substances, where necessary, amend or withdraw the authorisation by 31 March 2008 or by the date fixed for such an amendment or withdrawal in the respective Directive or Directives which added the relevant substance or substances to Annex I to Directive 91/414/EEC, whichever is the latest.
Article 4
This Directive shall enter into force on 1 October 2006.
Article 5
This Directive is addressed to the Member States.
Done at Brussels, 11 September 2006.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 230, 19.8.1991, p. 1. Directive as last amended by Commission Directive 2006/74/EC (OJ L 235, 30.8.2006, p. 17).
[2] OJ L 192, 20.7.2002, p. 60.
[3] EFSA Scientific Report (2005) 46, 1-82: conclusion regarding the peer review of the pesticide risk assessment of the active substance dimoxystrobin (finalised: 10 August 2005).
--------------------------------------------------
ANNEX
In Annex I to Directive 91/414/EEC, the following rows are added at the end of the table:
No | Common Name, Identification Numbers | IUPAC Name | Purity [1] | Entry into force | Expiration of inclusion | Specific provisions |
"129 | Dimoxystrobin CAS No 149961-52-4 CIPAC No 739 | (E)-o-(2,5-dimethylphenoxymethyl)-2-methoxyimino-N-methylphenylacetamide | ≥ 980 g/kg | 1 October 2006 | 30 September 2016 | PART AOnly uses as fungicide may be authorised.PART BIn assessing applications to authorise plant protection products containing dimoxystrobin for indoor uses, Member States shall pay particular attention to the criteria in Article 4(1)(b), and shall ensure that any necessary data and information is provided before such an authorisation is granted.For the implementation of the uniform principles of Annex VI, the conclusions of the review report on dimoxystrobin, and in particular Appendices I and II thereof, as finalised in the Standing Committee on the Food Chain and Animal Health on 27 January 2006 shall be taken into account.In this overall assessment Member Statesmust pay particular attention to the protection of groundwater, when the active substance is applied in a situation with a low crop interception factor, or in regions with vulnerable soil and/or climate conditions;must pay particular attention to the protection of aquatic organisms.Conditions of use shall include risk mitigation measures, where appropriate.The concerned Member States shall request the submission ofa refined risk assessment for birds and mammals considering the formulated active substance;a comprehensive aquatic risk assessment considering the high chronic risk to fish and the effectiveness of potential risk mitigation measures, particularly taking into account run-off and drainage.They shall ensure that the notifiers at whose request dimoxystrobin has been included in this Annex provide such studies to the Commission within two years from the entry into force of this Directive." |
[1] Further details on identity and specification of active substances are provided in the review report.
--------------------------------------------------
