Brussels, 04.05.2005
COM(2005)182 final
Proposal for a
COUNCIL REGULATION
amending Regulation (EC) No 131/2004 concerning certain restrictive measures in respect of Sudan
(presented by the Commission)
EXPLANATORY MEMORANDUM
Common Position 2004/31/CFSP of 9 January 2004 provides for an embargo on exports of arms, munitions and military equipment to Sudan, including a ban on the provision of technical and financial assistance related to military activities in Sudan. The ban on the provision of technical and financial assistance related to military activities has been implemented by Council Regulation (EC) No 131/2004 of 26 January 2004.
In view of recent developments in Sudan, and notably the continued violations of the N’djamena Ceasefire agreement of 8 April 2004 and the Abuja Protocols of 9 November 2004 by all sides in Darfur, and the failure of the Government of Sudan and rebel forces and all other armed groups in Darfur to comply with their commitments and the demands of the Security Council, the United Nations Security Council decided on 29 March 2005 to impose an arms embargo against all the parties of the N’djamena Ceasefire Agreement and any other belligerents in Darfur. The United Nations Security Council made provision for certain exemptions to the embargo.
In order to align the existing EU embargo, which, unlike the UN embargo, affects all exports to Sudan, with the United Nations Security Council decision of 29 March, the Council has adopted Common Position 2005/…./CFSP of ….., providing for an additional exemption to the embargo. The additional exemption relates to the embargo on technical and financial assistance as implemented by means of Council Regulation (EC) No 131/2004. The Commission therefore proposes to bring the Regulation in line with that Common Position.
Proposal for a
COUNCIL REGULATION
amending Regulation (EC) No 131/2004 concerning certain restrictive measures in respect of Sudan
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Articles 60 and 301 thereof,
Having regard to Council Common Position 2005/…/CFSP amending Common Position 2004/31/CFSP concerning the imposition of an embargo on arms, munitions and military equipment on Sudan[1],
Having regard to the proposal from the Commission,
Whereas:
(1) Common Position 2004/31/CFSP of 9 January 2004[2] provides for an embargo on exports of arms, munitions and military equipment to Sudan, including a ban on the provision of technical and financial assistance related to military activities in Sudan. The ban on the provision of technical and financial assistance related to military activities has been implemented by Council Regulation (EC) No 131/2004 of 26 January 2004 concerning certain restrictive measures in respect of Sudan[3].
(2) In view of recent developments in Sudan, and notably the continued violations of the N’djamena Ceasefire agreement of 8 April 2004 and the Abuja Protocols of 9 November 2004 by all sides in Darfur and the failure of the Government of Sudan and rebel forces and all other armed groups in Darfur to comply with their commitments and the demands of the Security Council, the United Nations Security Council on 29 March 2005 adopted Resolution 1591 (2005) imposing, inter alia , an arms embargo and a ban on the provision of related assistance against all the parties of the N’djamena Ceasefire Agreement and any other belligerents in Darfur. Resolution 1591 (2005) provides for certain exemptions to the embargo.
(3) Common Position 2005/…./CFSP confirms the embargo and ban of Common Position 2004/31/CFSP and makes provision for an additional exemption to the arms embargo and the ban on the provision of related assistance, which affects all persons and entities in Sudan, in order to bring the list of exemptions in line with Resolution 1591 (2005). Since this exemption applies to the ban on the provision of certain financial and technical assistance, Regulation (EC) No 131/2004 should be amended accordingly.
(4) The additional exemption should have retroactive effect from the date on which Resolution 1591 (2005) was adopted,
HAS ADOPTED THIS REGULATION:
Article 1
Article 4 of Regulation (EC) No 131/2004 is replaced by the following:
“Article 4
1. By way of derogation from Articles 2 and 3, the competent authorities of Member States as listed in the Annex, may authorise the provision of financing and financial assistance and technical assistance related to
(a) non-lethal military equipment intended solely for humanitarian or protective use, or for institution-building programmes of the United Nations, the African Union, the European Union and the Community;
(b) materiel intended for European Union and United Nations crisis management operations;
(c) mine clearance equipment and materiel for use in mine clearance;
(d) African Union crisis management operations, including materiel intended for such operations;
(e) the implementation of the Comprehensive Peace Agreement signed by the Government of Sudan and the Sudan People’s Liberation Movement/Army in Nairobi, Kenya, on 9 January 2005.
2. No authorisations shall be granted for activities that have already taken place.”
Article 2
This Regulation shall enter into force on the day following that of its publication in the Official Journal of the European Union . It shall apply from 29 March 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels,
For the Council
The President
[1] OJ L….,….2005, p…
[2] OJ L 6, 10.1.2004, p. 55.
[3] OJ L 21, 28.1.2004, p.1. Regulation as last amended by Regulation (EC) No 1516/2004 (OJ L 278, 27.8.2004, p.15).
