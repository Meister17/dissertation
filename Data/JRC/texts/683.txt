Council Regulation (EC) No 1593/2000
of 17 July 2000
amending Regulation (EEC) No 3508/92 establishing an integrated administration and control system for certain Community aid schemes
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 37 thereof,
Having regard to the proposal from the Commission(1),
Having regard to the opinion of the European Parliament,
Whereas:
(1) The decisions on the reform of the common agricultural policy require adjustments to be made to the integrated administration and control system.
(2) The application of the integrated management and control system in those areas to which it applies has led to significant progress in reducing the risk to the EAGGF Guarantee Section from agricultural expenditure. Provision should be made that the Member States are to design their own systems, compatible with the essential elements of the integrated administration and control system, for the application of other Community aid schemes.
(3) In view of the difficulties encountered when carrying out administrative checks on areas declared, and in particular the costs and time involved in clearing up anomalies in declarations and in view of experience in a number of Member States which have created a special parcel identification system and progress in digital orthoimagery and geographical information systems, provision should be made for the introduction of computerised geographical information system techniques for the identification of agricultural parcels.
(4) The measures necessary for the implementation of Regulation (EEC) No 3508/92 should be adopted in accordance with Council Decision 1999/468/EC of 28 June 1999 laying down the procedures for the exercise of implementing powers conferred on the Commission(2).
(5) Now that the introductory phase of the integrated administration and control system has largely been completed, the Commission should continue to be kept informed about the application and effectiveness of that system in the Member States,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EEC) No 3508/92(3) is hereby amended as follows:
1. in Article 1:
(a) paragraph 1 shall be replaced by the following:
"1. Each Member State shall set up an integrated administration and control system, hereinafter referred to as the 'integrated system', applying:
(a) in the crop sector:
(i) to the support system for producers of certain arable crops established by Regulation (EC) No 1251/1999(4);
(ii) to the aid scheme for rice producers established by Article 6 of Regulation (EC) No 3072/95(5);
(iii) to the specific measure in respect of certain grain legumes introduced by Regulation (EC) No 1577/96(6);
(b) in the livestock sector:
(i) to the premium and payment arrangements for beef and veal producers established by Chapter 1 of Title 1 of Regulation (EC) No 1254/1999(7);
(ii) to the premium arrangements for sheepmeat and goatmeat producers introduced by Regulation (EC) No 2467/98(8);
(iii) to direct payments under Article 19 of Council Regulation (EC) No 1255/1999 of 17 May 1999 on the common organisation of the market in milk and milk products(9)
hereinafter referred to as 'Community schemes'.";
(b) paragraph 3 shall be deleted;
(c) paragraph 4 shall become paragraph 3;
2. in Article 2, letters (b) and (c) shall be replaced by the following:
"(b) an identification system for agricultural parcels,
(c) a system for the identification and registration of animals,";
3. Article 4 shall be replaced by the following:
"Article 4
An identification system for agricultural parcels shall be established on the basis of maps or land registry documents or other cartographic references. Use shall be made of computerised geographical information system techniques including preferably aerial or spatial orthoimagery, with an homogenous standard guaranteeing accuracy at least equivalent to cartography at a scale of 1:10000.";
4. in Article 6:
(a) paragraph 2 shall be replaced by the following:
"2. The area aid application shall be submitted by a date to be fixed by the Member State which shall not be after that laid down for the submission of applications in, or fixed in accordance with, Regulation (EC) No 1251/1999.
In all cases, the date shall be set bearing in mind, inter alia, the period required for all relevant data to be available for the proper administrative and financial management of the aid and for the checks provided for in Article 8 to be carried out.
Notwithstanding Article 5(1) of Council Regulation (EEC, Euratom) No 1182/71 of 3 June 1971 determining the rules applicable to periods, dates and time limits(10), where the submission date is a public holiday, a Saturday or a Sunday, it shall be deemed to fall on the first working day following.";
(b) paragraph 3 shall be replaced by the following:
"3. A Member State may decide that an area aid application need contain only changes with respect to the area aid application submitted the previous year. Member States shall simplify the application process by distributing pre-printed forms based on the areas determined in the previous year and supplying graphic material, as referred to in Article 4, indicating the location of those areas.";
(c) paragraph 4 shall be replaced by the following:
"4. Certain amendments may be made to the area aid application provided that they are received by the competent authorities no later than the date laid down for sowing in, or fixed in accordance with, Regulation (EC) No 1251/1999.";
(d) paragraph 6 shall be replaced by the following:
"6. For each of the agricultural parcels declared, farmers shall indicate the area and its location, which information must enable the parcel to be identified in the identification system for agricultural parcels.";
5. the following Article shall be inserted:
"Article 9a
1. For the purposes of applying Community aid schemes listed in the Annex and which are not referred to in Article 1, the Member States shall ensure that the administration and control systems applied to them are compatible with the integrated system in the following respects:
(a) the computerised data base;
(b) the parcel and animal identification systems;
(c) administrative checks.
In order to be 'compatible' within the meaning of the first subparagraph, the administration and control systems applied to the Community aid schemes concerned have to be set up so as to allow, without any problems or conflicts, a common functioning of, or the exchange of data between the systems.
In accordance with the procedure referred to in Article 12a(2), the Commission shall adopt amendments to the Annex as may become necessary taking into account the criteria set out in the first and second subparagraph.
2. Without prejudice to the provisions in paragraph 1, the Member States may, for the purposes of applying Community aid schemes not referred to in Article 1, incorporate in their administration and control procedure one or more components of the integrated system.
3. The specific provisions laid down under the schemes referred to in paragraphs 1 and 2, in particular those concerning the terms on which aid may be granted, shall not be affected by this Regulation.
4. Member States may extend the possibility provided for in paragraph 2 to national schemes.
They may use the information from the integrated system for statistical purposes.
5. Member States shall inform the Commission if they avail themselves of the possibilities referred to in paragraphs 2 and 4.
The Commission shall ensure that recourse to these possibilities does not prejudice compliance with the sectoral Regulations or this Regulation.";
6. Article 10 shall be deleted;
7. in Article 11:
(a) paragraph 1 shall be replaced by the following:
"1. The Commission shall be kept regularly informed of the application of the integrated system.
It shall organise exchanges of views on this subject with the Member States.";
(b) the first indent of paragraph 2 shall be replaced by the following:
"- any examination or control relating to the body of measures taken in order to establish and to implement the integrated system";
8. in Article 12:
(a) the first sentence shall be replaced by the following:"The Commission shall adopt detailed rules for the application of this Regulation in accordance with the procedure laid down in Article 12a(2).";
(b) point (a) shall be replaced by the following:
"(a) the basic features of the identification system for agricultural parcels";
9. the following Article shall be inserted:
"Article 12a
1. The Commission shall be assisted by the Fund Committee established by Article 11 of Regulation (EC) No 1258/1999(11) (hereinafter referred to as 'the Committee').
2. Where reference is made to this paragraph, Articles 4 and 7 of Decision 1999/468/EC(12) shall apply.
The period provided for in Article 4(3) of Decision 1999/468/EC shall be set at one month.
3. The Committee may examine any matter raised by its Chairman, either on his own initiative or at the request of a Member State."
10. in Article 13(1), the following points shall be added:
"(c) at the latest as from 1 January 2005 as regards the geographical part of the parcel identification system as provided for in Article 4;
(d) at the latest as from 1 January 2003 as regards the compatibility of the administration and control systems with the integrated system as provided for in Article 9a.;".
11. the Annex attached to this Regulation shall be added.
Article 2
This Regulation shall enter into force on the seventh day following that of its publication in the Official Journal of the European Communities.
The Commission shall, where appropriate, adopt provisions under the procedure laid down in Article 12a(2) of Regulation (EEC) No 3508/92 to facilitate the transition from the provisions of that Regulation to those laid down by this Regulation.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 17 July 2000.
For the Council
The President
J. Glavany
(1) OJ C 89, 29.3.2000, p. 8.
(2) OJ L 184, 17.7.1999, p. 23.
(3) OJ L 355, 5.12.1992, p. 1. Regulation as last amended by Regulation (EC) No 1036/1999 (OJ L 327, 21.5.1999, p. 4).
(4) OJ L 160, 26.6.1999, p. 1. Regulation as amended by Regulation (EC) No 2704/1999 (OJ L 327, 21.12.1999, p. 12).
(5) OJ L 329, 30.12.1995, p. 18. Regulation as last amended by Regulation (EC) No 2072/98 (OJ L 265, 30.9.1998, p. 4).
(6) OJ L 206, 16.8.1996, p. 4. Regulation as amended by Commission Regulation (EC) No 1826/97 (OJ L 260, 23.9.1997, p. 11).
(7) OJ L 160, 26.6.1999, p. 21.
(8) OJ L 312, 20.11.1998, p. 1.
(9) OJ L 160, 26.6.1999, p. 48.
(10) OJ L 124, 8.6.1971, p. 1.
(11) OJ L 160, 26.6.1999, p. 103.
(12) OJ L 184, 17.7.1999, p. 23.
ANNEX
"ANNEX
>TABLE>"
