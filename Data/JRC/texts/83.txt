*****
COMMISSION REGULATION (EEC) No 226/89
of 26 January 1989
on the procedure for determining the meat content of products falling within CN codes 1602 49 11, 1602 49 13, 1602 49 15, 1602 49 19, 1602 49 30 and 1602 49 50
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 2658/87 of 23 July 1987 on the tariff and statistical nomenclature and on the Common Customs Tariff (1), as last amended by Regulation (EEC) No 28/89 (2), and in particular Article 9 thereof,
Whereas in order to ensure uniform application of the combined nomenclature it is necessary to adopt measures concerning the classification of prepared or preserved meat or meat offal of domestic swine containing meat or offal, of any kind, including fats of any kind or origin;
Whereas, in accordance with the provisions of CN codes 1602 49 11, 1602 49 13, 1602 49 15, 1602 49 19, 1602 49 30 and 1602 49 50, prepared or preserved meat containing meat or meat offal, of any kind, including fats of any kind or origin are to be classified according to the percentage, by weight, of those ingredients;
Whereas it is necessary to define a procedure for determining the percentage by weight of meat or meat offal, of any kind, including fats of any kind or origin; whereas experience has shown that the procedure set out in the Annex provides the best safeguards;
Whereas the entry into force of this Regulation involves the repeal of Commission Regulation (EEC) No 3530/83 of 12 December 1983 concerning the procedure for determining the meat content in products falling within subheading 16.02 B III c) 2 aa), bb) and cc) of the Common Customs Tariff (1);
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Nomenclature Committee,
HAS ADOPTED THIS REGULATION:
Article 1
The percentage, by weight, of meat or meat offal, of any kind, including fats of any kind or origin, in prepared or preserved meat or meat offal falling within CN codes 1602 49 11, 1602 49 13, 1602 49 15, 1602 49 19, 1602 49 30 and 1602 49 50 shall be determined in accordance with the procedure described in the Annex.
Article 2
Regulation (EEC) No 3530/83 is hereby repealed.
Article 3
This Regulation shall enter into force on the 21st day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 26 January 1989.
For the Commission
Christiane SCRIVENER
Member of the Commission
(1) OJ No L 256, 7. 9. 1987, p. 1.
(2) OJ No L 4, 6. 1. 1989, p. 19.
(3) OJ No L 352, 15. 12. 1983, p. 34.
ANNEX
ANALYTICAL PROCEDURE
For the purposes of this Annex any reference to meat shall be taken to include meat offal, and the term 'overall meat content' includes meat as defined above and fats of any kind and origin.
The overall meat content is determined as follows:
1.2 // 1. // Analytical methods // 1.1. // Homogeneous and representative samples of the meat product must be prepared for the purposes of analysis. // 1.2. // Analytical methods to be used are as follows: // 1.2.1. // nitrogen: Determination of the nitrogen content in meat and meat products - Kjeldahl method, // 1.2.2. // moisture: Determination of the moisture content in meat and meat products - ISO 1442 - 1973, // 1.2.3. // fats: Determination of the total fat content in meat and meat products - Extraction with light petroleum after hydrolysis with hydrochloric acid, // 1.2.4. // ash: Determination of the ash content in meat and meat products - ISO 936 - 1978. // 1.3. // The abovementioned ISO standard requirements concerning sampling are not binding for the purposes of the present Regulation. // 2. // Calculation of the overall meat content // // The overall meat content in a product is calculated by means of the following formula: 1.2.3 // % of defatted meat DM = // NT - NX f // × 100 // % meat total = // DM + F // // where // // 1.2 // NT // = nitrogen determined by analysis (%) // NX // = nitrogen of non-meat origin (%) // f // = average nitrogen content (%) in the fat-free meat contained in the product; the value of this factor is 3,5 for any kind of meat and mixtures of meat with the exception of: // // - products whose meat content consists solely of tongue for which the value of this factor is 3,0, // // - products whose meat content consists solely of kidney for which the value of this factor is 2,7 // F // = quantity of extractable fat (%) determined by analysis.
The total nitrogen and extractable fat content is determined by the methods mentioned in paragraphs 1.2.1 and 1.2.3. It is also possible to assess the ash (1.2.4) and moisture (1.2.2) content and to obtain, by deduction, the other ingredients.
In order to correct the value of nitrogen of non-meat origin (Nx factor), it would be necessary to know the quantity of each ingredient containing nitrogen as well as the nitrogen content of these ingredients.
The following table shows the nitrogen content of several typical ingredients of non-meat origin containing nitrogen, which are generally found in meat products:
1.2 // // // Non-meat products // % of nitrogen // // // Rusk // 2,0 // Casein // 15,8 // Sodium caseinate // 14,8 // Soya isolate // 14,5 // Textured soya // 8,0 // Soya flour // 8,0 // Monosodium glutamate (MSG) // 8,3 // //
As far as repeatability of analytical procedure is concerned, reference should be made to the relevant ISO standard.
The average result of at least two determinations must be taken into account.
