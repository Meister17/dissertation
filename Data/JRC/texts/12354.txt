Commission Decision
of 16 October 2006
on emergency measures applying to fishery products imported from Brazil and intended for human consumption
(notified under document number C(2006) 4819)
(Text with EEA relevance)
(2006/698/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Regulation (EC) No 178/2002 of the European Parliament and of the Council of 28 January 2002 laying down the general principles and requirements of food law, establishing the European Food Safety Authority and laying down procedures in matters of food safety [1], and in particular Article 53(1)(b) thereof,
Whereas:
(1) In accordance with Regulation (EC) No 178/2002, the necessary measures must be adopted where it is evident that food imported from a third country is likely to constitute a serious risk to human health, animal health or the environment and that such risk cannot be contained satisfactorily by means of measures taken by the Member State(s) concerned.
(2) Under Regulation (EC) No 853/2004 of the European Parliament and of the Council of 29 April 2004 laying down specific hygiene rules for food of animal origin [2], food business operators must ensure that the limits with regard to histamine in fishery products are not exceeded. Those limits, together with the sampling and analytical methods, were established by Commission Regulation (EC) No 2073/2005 of 15 November 2005 on microbiological criteria for foodstuffs [3].
(3) A recent Community inspection in Brazil has revealed serious hygiene shortcomings in the handling of fishery products. As a result of those shortcomings the fish is not as fresh as it should be and spoils quickly and there may be high levels of histamine in certain fish species (in particular those belonging to the families: Scombridae, Clupeidae, Engraulidae, Coryfenidae, Pomatomidae and Scombresosidae). The inspections have also revealed a limited capacity of the Brazilian authorities to carry out the necessary checks of fish, in particular to detect histamine in those species.
(4) Excessive levels of histamine in fishery products constitute a serious risk for human health.
(5) It is appropriate to adopt at Community level measures applicable to imports of fishery products which might be contaminated to ensure effective and uniform protection in all the Member States.
(6) Imports into the Community of fishery products from fish species associated with a high amount of histidine, should be authorised only if it can be shown that they have been subjected to a systematic check at origin to verify that their histamine levels do not exceed the limits set by Regulation (EC) No 2073/2005.
(7) However, it is appropriate to temporarily authorise, the import of consignments that are not accompanied by the results of checks at origin, provided that Member States ensure that those consignments undergo appropriate checks on arrival at the Community border to verify that their histamine levels do not exceed the limits set by Regulation (EC) No 2073/2005. This temporary authorisation should be limited to the time that would allow the Brazilian authorities to build their own check capacity.
(8) Regulation (EC) No 178/2002 sets up the rapid alert system for food and feed, which should be used to implement the mutual information requirement provided for in Article 22(2) of Council Directive 97/78/EC [4]. In addition the Member States should keep the Commission informed through periodical reports of all analytical results of official controls carried out in respect of consignments of fishery products from Brazil.
(9) This Decision should be reviewed in the light of the guarantees offered by Brazil and on the basis of the results of the tests carried out by Member States.
(10) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
Scope
This Decision shall apply to fishery products of fish belonging to the families Scombridae, Clupeidae, Engraulidae, Coryfenidae, Pomatomidae and Scombresosidae, imported from Brazil and intended for human consumption.
Article 2
Analytical tests for histamine
1. Member States shall authorise the import into the Community of products referred to in Article 1 only if they are accompanied by the results of an analytical test for histamine carried out in Brazil before consignment which reveals histamine levels below the limits set by Regulation (EC) No 2073/2005. These tests must be carried out following the sampling and the analytical method referred in Regulation (EC) No 2073/2005.
2. By way of derogation from paragraph 1, Member States shall authorise the import of products referred to in Article 1 that are not accompanied by the results of the analytical test as referred to in paragraph 1 provided that the importing Member State ensures that each consignment of those products undergoes tests to verify that the histamine levels are below the limits set by Regulation (EC) No 2073/2005. These tests must be carried out following the sampling and the analytical method referred in Regulation (EC) No 2073/2005.
Article 3
Reports
Member States shall immediately inform the Commission if tests carried out pursuant to Article 2(2) reveal histamine levels exceeding the limits set by Regulation (EC) No 2073/2005 for fishery products.
Member States shall submit to the Commission a report on all tests carried out pursuant to Article 2(2).
Member States shall use the rapid alert system for food and feed set up by Regulation (EC) No 178/2002 for the submission of that information and those reports.
Article 4
Charging of expenditure
All expenditure incurred in the application of this Decision shall be charged to the consignor, the consignee or the agent of either.
Article 5
Compliance
Member States shall immediately inform the Commission of the measures they take to comply with this Decision.
Article 6
Period of application
This Decision shall apply until 31 December 2006.
Article 7
Addressees
This Decision is addressed to the Member States.
Done at Brussels, 16 October 2006.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 31, 1.2.2002, p. 1. Regulation as last amended by Commission Regulation (EC) No 575/2006, (OJ L 100, 8.4.2006, p. 3).
[2] OJ L 139, 30.4.2004, p. 55; corrected by OJ L 226, 25.6.2004, p. 22. Regulation as last amended by Commission Regulation (EC) No 2076/2005 (OJ L 338, 22.12.2005, p. 83).
[3] OJ L 338, 22.12.2005, p. 1.
[4] OJ L 24, 30.1.1998, p. 9. Directive as last amended by Regulation (EC) No 882/2004 of the European Parliament and of the Council (OJ L 191, 28.5.2004, p. 1).
--------------------------------------------------
