[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 14.12.2006
COM(2006) 801 final
REPORT FROM THE COMMISSION TO THE EUROPEAN PARLIAMENT AND TO THE COUNCIL
report for 2006 on the implementation of Regulation (EC) No 450/2003 of the European Parliament and of the Council
1. INTRODUCTION
Labour costs are generally regarded as an important factor in the analysis of short- and medium-term economic development. The timely production of a labour cost index in the euro area is thus considered by the Commission and the European Central Bank to be of the utmost importance for assessing the inflationary pressure that may arise from short-term developments in the labour market. The labour cost index is also important for the social partners in wage negotiation and for the Commission itself in monitoring short-term developments in labour costs.
Regulation (EC) No 450/2003 of the European Parliament and of the Council of 27 February 2003 concerning the labour cost index (LCI) establishes a common framework for the production, transmission and evaluation of comparable labour cost indices in the Community (OJ L 69, 13.3.2003, p. 1). The Commission (Eurostat) publishes a quarterly news release on the labour cost index.
Article 13 of the above Regulation requires the submission of biennial reports on the implementation of the Regulation. These reports must evaluate in particular the quality of the transmitted LCI series data and the quality of the transmitted back data.
An implementing regulation was adopted by the Commission in July 2003 (Regulation EC No 1216/2003, OJ L 169, 8.7.2003, p. 37), setting out in more detail the procedures for transmitting the index, the specific (seasonal) adjustments to be made and the content of the national quality reports.
The provision of annual national quality reports is required under Article 8(2) of Regulation (EC) No 450/2003. There the quality of the labour cost index has been reported in terms of the following aspects: relevance, accuracy , timeliness and punctuality, accessibility and clarity, comparability , coherence and completeness . In the present report, emphasis has been put on the highlighted items.
Small irregularities in transmission delays have not been reported, as by their nature these are normally temporary.
Annex of this report indicates coming national improvement actions for those Member States who have provided this information.
2. GENERAL PROGRESS SINCE THE LAST REPORT
Since the implementation of reporting in 2004[1], substantial progress has been achieved, especially in the general availability of the labour cost index. By now, all EU Member States have implemented the LCI and the data are delivered regularly to the Commission (Eurostat) although not necessarily within the mandatory deadlines.
The availability of the LCI from all EU Member States allows the European Union aggregates to be compiled and makes it possible to compare with sufficient accuracy the development of the hourly labour costs between the Member States. There are, however, a number of quality issues which still require extra effort from certain Member States in order to complete the harmonisation process. These issues are discussed below.
Just as the Member States have implemented the necessary facilities for the production of the LCI, the Commission (Eurostat) has also implemented a production system that allows reception, verification, manipulation, storage and dissemination of the Labour Cost Index data in a timely manner. These processes became fully operational in 2005.
3. Quality evaluation ( UP TO OCTOBER 2006)
3 .1 Member States with a sufficient quality of labour cost index
The following Member States have undertaken all necessary measures to comply fully with the Regulation: Czech Republic, Denmark, Germany, Estonia, Spain, Latvia, Lithuania Luxembourg, the Netherlands, Austria, Poland, Portugal, Slovenia, Slovak Republic and the United Kingdom.
Based on the transmitted index series and especially the accompanying metadata from these Member States, the quality of the labour cost index time series has not been called into question and is therefore considered to be sufficient.
However, the Netherlands has not delivered the regulatory quality report. The quality evaluation for the Netherlands is therefore still uncertain and a full guarantee on quality cannot be given. Also, even if the quality is evaluated as sufficient for the current part of the LCI series, shortcomings with historical time series still remain in two Member States:
Latvia
Comparability. The historical data are missing (1996).
United Kingdom
Completeness. The index series are not provided in working-day adjusted or in seasonally and working-day adjusted form (1996-1999).
3 .2 Member States with quality shortcomings
After inspection, the Commission (Eurostat) found that the quality of the LCI is not sufficient in all respects to fulfil the requirements of Regulation (EC) No 450/2003 for the following Member States: Belgium, Greece, France, Ireland, Italy, Cyprus, Hungary, Malta, Finland and Sweden.
Cyprus has not delivered the standard regulatory quality report, a fact which may make the evaluation of all the quality aspects less precise.
Detailed breakdown of detected quality shortcomings:
Belgium
Timeliness. The timeliness of the transmitted index series does not fulfil the legal obligation. The delay was up to more than 200 days in 2005 (instead of the required 70 days). After trying several solutions in 2006, the Belgian authorities have managed to shorten the delay in making administrative data available.
Completeness. The index series are not provided either in working-day or in seasonally and working-day adjusted form.
Greece
Timeliness. The timeliness of the transmitted index series does not fulfil the legal obligation. The delay was up to almost 270 days in 2005 (instead of the required 70 days). Some improvement has been seen in 2006, but the transmission delay still exceeds the legal deadline.
Comparability. The historical series are missing (1996-1999).
France
Accuracy. The hours worked measure has been replaced by the contractual paid hours of full-time employees in enterprises of 10 and more employees.
Ireland
Accuracy. The hours worked measure reflects hours paid rather than hours worked. One defect is that irregular payments to employees are not included in the measurement of labour costs in the labour cost index.
Timeliness. The timeliness of the transmitted index series does not fulfil the legal obligation. The delay was up to around 100 days in 2005 (instead of the requisite 70 days). Some improvement has been seen in 2006, but the transmission delay still exceeds the legal deadline.
Comparability. The historical series are partially missing (1996-1997)
Completeness. The index series are not supplemented by working-day or seasonally and working-day adjusted form (partially supplemented in 1996-1997).
Italy
Accessibility. The Italian labour cost index has been classified by the Italian authorities as 'confidential' from the second quarter of 2005; therefore it cannot be published but can be used for European Union aggregates only. This is due to uncertainties about the quality of the related hours worked series.
Cyprus
Comparability. The historical series are partially missing (1996-1999)
Completeness. The index series are not complete, as they are not delivered in seasonally and working-day adjusted form.
Hungary
Accuracy. The data relate to private enterprises employing at least 5 persons and government organizations of all size classes.
Malta
Comparability. The historical series are partially missing (1996-1999)
Completeness. The index series are not complete, as they are not delivered in working-day adjusted or in seasonally and working-day adjusted form.
Finland
Accuracy. The data relate only to full-time employees. The quarterly change in labour costs since the first quarter of 2005 has been measured by the change in earnings for regular working time. In general, there are significant delays – up to about 2 years - in several of the sources used for compiling the LCI. Consequently, the present application of the labour cost index does not fully take account of the short-term development of hourly labour costs with the desirable precision in Finland, and major revisions limit its usability.
Sweden
Accuracy. One shortcoming is that irregular bonuses are not included in the measurement of labour costs. Data cover the private sector only.
Completeness. The index series are not delivered in working-day adjusted or in seasonally and working-day adjusted form.
4 . RECENT ACTIONS BY THE EUROPEAN COMMISSION (EUROSTAT) AND MEMBER STATES
In 2006, the Commission (Eurostat) together with the Member States completed the evaluation of the feasibility studies on extending the scope of the LCI to cover economic activities such as public administration, education and health care (NACE Rev.1 sections L to O) and to develop an index excluding irregular bonuses (Article 10 of Regulation (EC) No 450/2003). The draft Commission Regulation for the extended data collection has been introduced in November 2006. On a contrary, a decision over an index excluding irregular bonuses has not yet been taken as investigations showed that more efforts are still needed in the definitions and especially searching the means making this sub-index operational with acceptable quality.
Further issues concerning the relevance and accuracy of the LCI have been given preliminary consideration. First, a user requirement for separating the index into private and public sectors has been identified. A partial solution will be the above-mentioned NACE extension and the breakdown by economic activity which it offers. Secondly, as the high volatility of some of the index series has been traced to the high volatility of the underlying hours worked series, there might be an interest in and possibly a need for receiving separately the two components of the LCI – quarterly labour costs and quarterly hours worked.
5 . CONCLUSIONS
Fifteen Member States are in compliance with the Regulation as regards the quality aspects evaluated. However, two of these are still having problems as regards the availability, comparability or working-day and seasonal adjustment of the historical LCI series.
Ten Member States still have shortcomings concerning one or more quality aspects of the LCI. These are mainly related to the accuracy element of quality. Definition and coverage of some of the aspects of the LCI do not fully correspond to the legal requirement. Insufficient timeliness, missing working-day and seasonally adjusted series and incomplete historical series also play their part in lowering the quality of the LCI series.
Two Member States have not completed the regulatory quality reporting.
The Commission (Eurostat) will be monitoring the remaining non-compliance and quality issues regularly through the data delivered and other national documentation. Where desired or planned improvements have not advanced appropriately, the corresponding national authorities will be contacted.
As the European Union labour cost index series are being used frequently in monitoring labour cost developments for the EU and its Member States, it is of the utmost importance that the national authorities provide data of the highest quality. However, it should be underlined that the situation has improved substantially since the Regulation became fully applicable at the beginning of 2005. All Member States - not only those applying the Regulation fully - have directed resources into implementing actions to achieve more comparable and timely index series. This has clearly raised the overall quality and thus increased the usefulness of the data.
ANNEX
Details of forthcoming National Improvement Activities (Member States' contributions)
Spain
The main problem of the Labour Cost Index in Spain is timeliness.
The source of information for compiling the LCI is the Quarterly Labour Cost Survey (QLCS) which has been conducted by the National Statistics Institute (INE) since 2000. The ongoing work to obtain the results takes 80 days or more, depending on the quarter. During the summer months, the industrial holidays have the effect of extending the data collection period.
Since wages and salaries in Spain are usually paid on a monthly basis and payments of Social Security contributions are also made monthly by law, the QLCS questionnaire asks for monthly data. The information requested is based on the official documents that employers have to submit to the worker and to the Social Security. Therefore, the legal time limits for the submission of these documents are observed and, consequently, it is not possible to bring forward the final results.
In order to obtain the LCI results on time, at t+70 days, a provisional LCI of one quarter t is obtained and published with the information received and validated up to that time. The validation and imputation processes continue until t+80, when the QLCS is published with the final results. The revised LCI is obtained at t+80. At (t+1) +70, the provisional LCI for quarter t+1 is published, together with the revised LCI for quarter t.
Ireland
Ireland is in the process of implementing a strategy to improve the coverage of information on earnings and labour costs. One element of this is a new annual structural survey of employees (the National Employment Survey, or NES) that will collect information on earnings and the factors that influence earnings (age, occupation, length of service etc.). The NES will monitor the structure of earnings on an annual basis, as well as providing a vehicle for collecting other enterprise-related data on matters such as vocational training etc.
The other element is a new quarterly survey: the Earnings, Hours and Employment Costs Survey (EHECS). This survey will provide comparable and timely data on labour costs across all sectors of the economy and will measure earnings and employment in a more comprehensive and consistent way across the different sectors. It will be aimed at enterprises employing three or more persons.
This survey will collect data on wages and salaries, employers’ social contributions, other non-wage costs, and hours worked for the entire quarter. The expansion of coverage to include these non-wage costs, which are not traditionally included as earnings, will enable the survey to better reflect the cost of employment and the pressure on competitiveness.
The main output of the survey will be an index of labour costs per hour worked, and the survey is designed to monitor the short-term change in labour costs. Given the breakdown requested in the survey, it will be possible to routinely disseminate hourly earnings information with and without bonuses and by broad occupational group.
The EHECS survey is designed to meet national needs and EU requirements and, once it is fully up and running, Ireland will be able to supply LCI data which will be fully compliant with Regulation (EC) No 450/2003. The new EHECS survey will also mean that, in the future, it will not be necessary to carry out a dedicated four-yearly Labour Costs Survey (LCS).
The EHECS survey will ultimately replace all existing short-term earnings surveys currently being carried out by the CSO. The EHECS was launched in 2005 for the industrial and financial sectors. In early 2007 it will be extended to cover the public sector and then, later in the year, the distribution sector. By 2008 it will cover most sectors of the economy from industry to personal services (NACE C to O).
Finland
The Finnish model of compiling the Labour Cost Index is based on data from several statistical sources. The accuracy of the index improves gradually with each revision, as slower statistical sources become available.
In 2005, Statistics Finland started a project for implementing a new production model for the LCI based on direct quarterly collection of primary data from enterprises. Through these measures Finland should be able to comply fully with the Regulations of the LCI from 2009.
[1] COM(2004) 833 final of 27.12.2004.
