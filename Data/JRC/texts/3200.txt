Publication of the final accounts for the financial year 2003 — Eurojust
(2005/C 78/08)
The complete version of the final accounts may be found at the following address:
www.eurojust.eu.int
Table 1
Implementation of the budget for the financial year 2003
(million euro) |
Revenue | Expenditure |
Source of revenue | Revenue entered under the final budget for the financial year | Revenue received | Allocation of expenditure | Appropriations under the final budget | Appropriations carried over from the previous financial year | Available appropriations(2003 budget and the financial year 2002) |
entered | committed | paid | carried over | cancelled | outstanding commitments | paid | cancelled | appropriations | committed | paid | carried over | cancelled |
Community subsidies | 8,0 | 7,2 | Title I Staff | 2,5 | 2,2 | 2,0 | 0,2 | 0,3 | 0,0 | 0,0 | 0,0 | 2,5 | 2,2 | 2,0 | 0,2 | 0,3 |
Financial interest | 0,0 | 0,0 | Title II Administration | 4,0 | 3,0 | 2,3 | 1,0 | 0,7 | 0,3 | 0,3 | 0,0 | 4,3 | 3,3 | 2,6 | 1,0 | 0,7 |
Miscellaneous revenue | 0,1 | 0,0 | Title III Operational activities | 1,6 | 1,0 | 0,9 | 0,1 | 0,6 | 0,1 | 0,1 | 0,0 | 1,7 | 1,1 | 1,0 | 0,1 | 0,6 |
Total | 8,1 | 7,2 | Total | 8,1 | 6,2 | 5,2 | 1,3 | 1,6 | 0,4 | 0,4 | 0,0 | 8,5 | 6,6 | 5,6 | 1,3 | 1,6 |
Table 2
Economic outturn accounts for the financial years 2003 and 2002
(1000 euro) |
| 2003 | 2002 |
Operating revenue
Community subsidies | 7125 | 1478 |
Miscellaneous revenue | 12 | 0 |
Total (a) | 7137 | 1478 |
Operating expenditure
Purchases of goods and services | 3228 | 378 |
Staff costs | 2112 | 256 |
Depreciation | 211 | 29 |
Total (b) | 5551 | 663 |
Outturn for the financial year (a – b) | 1586 | 815 |
Table 3
Balance sheet at 31 December 2003 and 31 December 2002
(1000 euro) |
Assets | 2003 | 2002 | Liabilities | 2003 | 2002 |
Intangible assets | 62 | 6 | Own capital | | |
| | | Budget outturn (a) | 778 | – 80 |
Fixed assets | | | Result of adjustments (b) | 808 | 895 |
Equipment, machinery and tools | 114 | 20 | Economic outturn (a + b) | 1586 | 815 |
Furniture and vehicles | 492 | 207 | Result carried over from previous years | 815 | 0 |
Computer equipment | 460 | 451 | Subtotal | 2401 | 815 |
Subtotal | 1066 | 678 | | | |
| | | Provisions for risks and charges | 396 | 0 |
Financial assets | 1 | 0 | | | |
| | | Current liabilities | | |
Current assets | | | Current liabilities | 24 | 173 |
Current assets | 232 | 158 | Other liabilities | 305 | 125 |
Sundry accounts receivable | 34 | 30 | Subtotal | 329 | 298 |
Subtotal | 266 | 188 | | | |
Disposable assets | 1731 | 241 | | | |
Total | 3126 | 1113 | Total | 3126 | 1113 |
--------------------------------------------------
