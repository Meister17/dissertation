COUNCIL DIRECTIVE of 29 July 1991 on the development of the Community's railways (91/440/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the Europaen Economic Community, and in particular Article 75 thereof,
Having regard to the proposal from the Commission(1),
Having regard to the opinion of the European Parliament(2),
Having regard to the opinion of the Economic and Social Committee(3),
Whereas greater integration of the Community transport sector is an essential element of the internal market, and whereas the railways are a vital part of the Community transport sector;
Whereas the efficiency of the railway system should be improved, in order to integrate it into a competitive market, whilst taking account of the special features of the railways;
Whereas, in order to render railway transport efficient and competitive as compared with other modes of transport, Member States must guarantee that railway undertakings are afforded a status of independent operators behaving in a commercial manner and adapting to market needs;
Whereas the future development and efficient operation of the railway system may be made easier if a distinction is made between the provision of transport services and the operation of infrastructure; whereas given this situation, it is necessary for these two activities to be separately managed and have separate accounts;
Whereas, in order to boost competition in railway service management in terms of improved comfort and the services provided to users, it is appropriate for Member States to retain general responsibility for the development of the appropriate railway infrastructure;
Whereas, in the absence of common rules on allocation of infrastructure costs, Member States shall, after consulting the infrastructure management, lay down rules providing for the payment by railway undertakings and their groupings for the use of railway infrastructure; whereas such payments must comply with the principle of non-discrimination between railway undertakings;
Whereas Member States should ensure in particular that existing publically owned or controlled railway transport undertakings are given a sound financial structure, whilst taking care that any financial rearrangement as may be necessary shall be made in accordance with the relevant rules laid down in the Treaty;
Whereas, in order to facilitate transport between Member States, railway undertakings should be free to form groupings with railway undertakings established in other Member States;
Whereas, such international groupings should be granted rights of access and transit in the Member States of establishment of their constituent undertakings, as well as transit rights in other Member States as required for the international service concerned;
Whereas, with a view to encouraging combined transport, it is appropriate that access to the railway infrastructure of the other Member States should be granted to railway undertakings engaged in the international combined transport of goods;
Whereas it is necessary to establish an advisory committee to monitor and assist the Commission with the implementation of this Directive;
Whereas, as a result, Council Directive 75/327/EEC of 20 May 1975 on the improvement of the situation of railway undertakings and the harmonization of rules governing financial relations between such undertakings and States(4) should be repealed,
HAS ADOPTED THIS DIRECTIVE:
SECTION I Objective and scope
Article 1
The aim of this Directive is to facilitate the adoption of the Community railways to the needs of the Single Market and to increase their efficiency;
-by ensuring the management independance of railway undertakings;
-by separating the management of railway operation and infrastructure from the provision of railway transport services, separation of accounts being compulsory and organizational or institutional separation being optional,
-by improving the financial structure of undertakings,
-by ensuring access to the networks of Member states for international groupings of railway undertakings and for railway undertakings engaged in the international combined transport of goods.
Article 2
1. This Directive shall apply to the management of railway infrastructure and to rail transport activities of the railway undertakings established or to be established in a Member State.
2. Member States may exclude from the scope of this Directive railway undertakings whose activity is limited to the provision of solely urban, suburban or regional services.
Article 3
For the purpose of this Directive:
-'railway undertaking` shall mean any private or public undertaking whose main business is to provide tail transport services for goods and/or passengers with a requirement that the undertaking should ensure traction,
-'infrastructure manager` shall mean any public body or undertaking responsible in particular for establishing and maintaining railway infrastructure, as well as for operating the control and safety systems,
-'railway infrastructure` shall mean all the items listed in Annex I.A to Commission Regulation (EEC) N° 2598/70 of 18 December 1970 specifying the items to be included under the various headings in the forms of accounts shown in Annex I to Regulation (EEC) N° 1108/70(1), with the exception of the final indent which, for the purposes of this Directive only, shall read as follows: 'Buildings used by the infrastructure department`,
-'international grouping` shall mean any association of at least two railway undertakings established in different Member States for the purpose of providing international transport services between Member States;
-'urban and suburban services` shall mean transport services operated to meet the transport needs of an urban centre or conurbation, as well as the transport needs bewtween such centre or conurbation and surrounding areas;
-'regional services` shall mean transport services operated to meet the transport needs of a region.
SECTION II Management independence of railway undertakings
Article 4
Member States shall take the measures necessary to ensure that as regards management, administration and internal control over administrative, economic and accounting matters railway undertakings have independent status in accordance with which they will hold, in particular, assets, budgets and accounts which are separate from those of the State.
Article 5
1. Member States shall take the measure necessary to enable railway undertakings to adjust their activities to the market and to manage those activities under the responsibility of their management bodies, in the interests of providing efficient and appropriate services at the lowest possible cost for the quality of service required.
Railway undertakings shall be managed according to the principles which apply to commercial companies; this shall also apply to their public services obligations imposed by the State and to public services contracts which they conclude with the competent authorities of the Member State.
2. Railway undertakings shall determine their business plans, including their investment and financing programmes. Such plans shall be designed to achieve the undertakings' financial equilibrium and the other technical, commercial and financial management objectives; they shall also lay down the method of implementation.
3. In the context of the general policy guidelines determined by the State and taking into account national plans and contracts (which may be multiannual) including investment and financing plans, railway undertakings shall, in particular, be free to:
-establish with one or more other railway undertakings an international grouping;
-establish their internal organization, without prejudice to the provisions of Section III;
-control the supply and marketing of services and fix the pricing thereof, without prejudice to Council Regulation (EEC) N° 1191/69 of 26 June 1969 on action by Member States concerning the obligation inherent in the concept of a public service in transport by rail, road and inland waterway(1),
-take decisions on staff, assets and own procurement,
-expand their market share, develop new technologies and new services and adopt any innovative management techniques;
-establish new activities in fields associated with railway business.
SECTION III Separation between infrastructure management and transport operations
Article 6
1. Member States shall take the measures necessary to ensure that the accounts for business relating to the provision of transport services and those for business relating to the management of railway infrastructure are kept separate. Aid paid to one of these two areas of activity may not be transferred to the other.
The accounts for the two areas of activity shall be kept in a way which reflects this prohibition.
2. Member States may also provide that this separation shall require the organization of distinct divisions within a single undertaking or that the infrastructure shall be managed by a separate entity.
Article 7
1. Member States shall take the necessary measures for the development of their national railway infrastructure taking into account, where necessary, the general needs of the Community.
They shall ensure that safety standards and rules are laid down and that their application is monitored.
2. Member States may assign to railway undertakings or any other manager the responsibility for managing the railway infrastructure and in particular for the investment, maintenance and funding required by the technical, commercial and financial aspects of that management.
3. Member States may also accord the infrastructure manager, having due regard to Articles 77, 92 and 93 of the Treaty, financing consistent with the tasks, size and financial requirements, in particular in order to cover new investments.
Article 8
The manager of the infrastructure shall charge a fee for the use of the railway infrastructure for which he is responsible, payable by railway undertakings and international groupings using that infrastructure. After consulting the manager, Member States shall lay down the rules for determining this fee.
The user fee, which shall be calculated in such a way as to avoid any discrimination between railway undertakings, may in particular take into account the mileage, the composition of the train and any specific requirements in terms of such factors as speed, axle load and the degree or period of utilization of the infrastructure.
SECTION IV Improvement of the financial situation
Article 9
1. In conjunction with the existing publicity owned or controlled railway undertakings, Member States shall set up appropriate mechanisms to help reduce the indebtedness of such undertakings to a level which does not impede sound financial management and to improve their financial situation.
2. To that end, Member States may take the necessary measures requiring a separate debt amortization unit to be set up within the accounting departments of such undertakings.
The balance sheet of the unit may be charged, until they are extinguished, with all the loans raised by the undertaking both to finance investment and to cover excess operating expenditure resulting from the business of rail transport or from railway infrastructure management. Debts arising from subsidiaries' operations may not be taken into account.
3. Aid accorded by Member States to cancel the debts referred to in this Article shall be granded in accordance with Articles 77, 92 and 93 of the EEC Treaty.
SECTION V Access to railway infrastructure
Article 10
1. International groupings shall be granted access and transit rights in the Member States of establishment of their constituent railway undertakings, as well as transit rights in other Member States, for international services between the Member States where the undertakings constituting the said groupings are established.
2. Railway undertakings within the scope of Article 2 shall be granted acces on equitable conditions to the infrastructure in the other Member States for the purpose of operating international combined transport goods services.
3. Railway undertakinks engaged in international combined transport of goods and international groupings shall conclude the necessary administrative, technical and financial agreements with the managers of the railway infrastructure used with a view to regulating traffic control and safety issues concerning the international transport services referred to in paragraphs 1 and 2. The conditions governing such agreements shall be non-discriminatory.
SECTION VI Final provisions
Article 11
1. Member States may bring any question concerning the implementation of this Directive to the attention of the Commission. After consulting the committee provided for in paragraph 2 on these questions, the Commission shall take the appropriate decisions.
2. The Commission shall be assisted by an advisory committee composed of the representatives of the Member States and chaired by the representative of the Commission.
The representative of the Commission shall submit to the committee a draft of the measures to be taken. The committee shall deliver its opinion on the draft, within a time limit which the chairman may lay down according to the urgency of the matter, if necessary by taking a vote.
The opinion shall be recorded in the minutes; in addition, each Member State shall have the right to ask to have its position recorded in the minutes.
The Commission shall take the utmost account of the opinion delivered by the committee. It shall inform the committee of the manner in which its opinion has been taken into account.
Article 12
The provisions of this Directive shall be without prejudice to Council Directive 90/531/EEC of 17 September 1990 on the procurment procedure of entities operating in the water, energy, transport and telecommunications sectors(1).
Article 13
Decision 75/327/EEC is hereby repealed as from 1 January 1993.
Reference to the repealed Decision shall be understood to refer to this Directive.
Article 14
Before 1 January 1995, the Commission shall submit to the Council a report on the implementation of this Directive accompanied, if necessary, by suitable proposals on continuing Community action to develop railways, in particular in the field of the international transport of goods.
Article 15
Member States shall, after consultation with the Commission, adopt the laws, regulations and administrative provisions necessary to comply with this Directive not later than 1 January 1993. They shall forthwith inform the Commission thereof.
When Member States adopt these provisions, they shall contain a reference to this Directive or be accompanied by such reference on the occasion of their official publication. The methods of making such a reference shall be laid down by the Member States.
Article 16
This Directive is addressed to the Member States.
Done at Brussels, 29 July 1991.
For the council The president H. VAN DEN BROEK
(1)OJ N° C 34, 14.2.1990, p. 8 and OJ N° C 87, 4.4.1991, p. 7.
(2)OJ N° C 19, 28.1.1991, p. 254.
(3)OJ N° C 225, 10.9.1990, p. 27.
(4)OJ N° L 152, 12.6.1975, p. 3.
(1)OJ N° L 278, 23.12.1970, p. 1, Regulation amended by Regulation (EEC) N° 2116/78 (OJ N° L 246, 8.9.1978, p. 7).
(1)OJ N° L 156, 28.6.1969, p. 1; Regulation last amended by Regulation (EEC) N° 1893/91 (OJ N° L 169, 29.6.1991, p. 1).
(1)OJ N° L 297, 29.10.1990, p. 1.
