Council Decision
of 10 April 2006
appointing three Slovak members and three Slovak alternate members of the Committee of the Regions
(2006/309/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 263 thereof,
Having regard to the proposal from the Slovak Government,
Whereas:
(1) On 24 January 2006 the Council adopted Decision 2006/116/EC appointing the members and alternate members of the Committee of the Regions for the period from 26 January 2006 to 25 January 2010 [1].
(2) Three members' seats on the Committee of the Regions have become vacant following the expiry of the mandates of Mr BAUER, Mr MARČOK and Mr TARČÁK (members). Three alternate members' seats on the Committee of the Regions have become vacant following the expiry of the mandates of Mr ŠTEFANEC, Mr TOMEČEK and Ms DEMETEROVÁ (alternate members),
HAS DECIDED AS FOLLOWS:
Article 1
The following are hereby appointed to the Committee of the Regions:
(a) as members:
- Mr Juraj BLANÁR, President of the Zilina Self-governing Region,
- Mr Milan MURGAŠ, President of the Banská Bystrica Self-governing Region,
- Mr Zdenko TREBUL'A, President of the Kosice Self-governing Region;
(b) as alternate members:
- Mr Pavol SEDLÁČEK, President of the Trencin Self-governing Region,
- Mr Vladimír BAJAN, President of the Bratislava Self-governing Region,
- Mr Tibor MIKUŠ, President of the Trnava Self-governing Region,
for the remainder of the term of office, which ends on 25 January 2010.
Article 2
This Decision shall be published in the Official Journal of the European Union.
It shall take effect on the day of its adoption.
Done at Luxembourg, 10 April 2006.
For the Council
The President
U. Plassnik
[1] OJ L 56, 25.2.2006, p. 75.
--------------------------------------------------
