COUNCIL DECISION of 29 March 1996 concerning the signing and provisional application of the International Tropical Timber Agreement 1994 on behalf of the European Community (96/493/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community,
Having regard to the proposal from the Commission,
Whereas the International Tropical Timber Agreement 1994 negotiated on the basis of Resolution 93 (IV) of the text entitled 'A new partnership for development: Cartagena Commitment` and the relevant objectives in the final document 'Cartagena Spirit` adopted by the United Nations Trade and Development Conference was opened for signing from 1 April 1994 until one month after the date of its entry into force;
Whereas the said Agreement has not yet entered into force;
Whereas, pursuant to Article 42 (3) of the 1983 International Tropical Timber Agreement, that Agreement was extended until the provisional or definitive entry into force of the new Agreement;
Whereas the objectives pursued by the new Agreement fit into the context of the common commercial policy;
Whereas the Member States participate, through financial contributions, in the measures provided for in that Agreement;
Whereas all the Member States have expressed their intention to sign and to contribute towards the provisional application of the Agreement; whereas the Community therefore should sign the Agreement lodged with the United Nations Secretary-General and, as soon as possible, notify its intention to apply the Agreement provisionally,
HAS DECIDED AS FOLLOWS:
Article 1
1. The Community shall sign the International Tropical Timber Agreement 1994 lodged with the United Nations Secretary-General.
The text of the Agreement is attached to this Decision.
2. The Community shall notify the United Nations Secretary-General of its intention to apply the Agreement referred to in paragraph 1 provisionally, in accordance with Articles 40 and 41 (2) thereof.
Article 2
The President of the Council is hereby authorized to designate the persons empowered to sign the Agreement on behalf of the Community and deposit the notification of its provisional application by the Community, together with the declaration attached to this Decision.
Done at Brussels, 29 March 1996.
For the Council
The President
T. TREU
ANNEX
Declaration by the European Community and its Member States
The European Community and its Member States interpret the terms of the International Tropical Timber Agreement 1994 as follows:
(a) unless the scope of the Agreement is changed pursuant to Article 35 of the Agreement, the Agreement shall refer solely to tropical timber and tropical forests;
(b) any financial contribution other than the contribution to the administrative budget provided for in Article 19 of the Agreement shall be entirely voluntary.
