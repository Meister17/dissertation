Administrative commission on social security for migrant workers
(2006/C 146/06)
The annual average costs do not take into account the reduction of 20 % provided for in Articles 94(2) and 95(2) of Regulation (EEC) No 574/72.
The net monthly average costs have been reduced by 20 %.
AVERAGE COSTS OF BENEFITS IN KIND — 1996 [1]
I. Application of Article 94 of Council Regulation (EEC) No 574/72
The amounts to be refunded with regard to the benefits in kind provided in 1996 to members of the family as referred to in Article 19 (2) of Council Regulation (EEC) No 1408/71 will be determined on the basis of the following average costs:
| Annual | Net Monthly |
Liechtenstein | Not requested | Not requested |
II. Application of Article 95 of Council Regulation (EEC) No 574/72
The amounts to be refunded with regard to benefits in kind provided in 1996 under Articles 28 and 28a of Council Regulation (EEC) No 1408/71 will be determined on the basis of the following average costs:
| Annual | Net Monthly |
Liechtenstein
—per family | CHF 5710,08 | CHF 380,67 |
AVERAGE COSTS OF BENEFITS IN KIND — 1997 [2]
I. Application of Article 94 of Council Regulation (EEC) No 574/72
The amounts to be refunded with regard to the benefits in kind provided in 1997 to members of the family as referred to in Article 19 (2) of Council Regulation (EEC) No 1408/71 will be determined on the basis of the following average costs:
| Annual | Net Monthly |
Liechtenstein | Not requested | Not requested |
II. Application of Article 95 of Council Regulation (EEC) No 574/72
The amounts to be refunded with regard to benefits in kind provided in 1997 under Articles 28 and 28a of Council Regulation (EEC) No 1408/71 will be determined on the basis of the following average costs:
| Annual | Net Monthly |
Liechtenstein
—per family | CHF 6116,94 | CHF 407,80 |
AVERAGE COSTS OF BENEFITS IN KIND — 1998 [3]
I. Application of Article 94 of Council Regulation (EEC) No 574/72
The amounts to be refunded with regard to the benefits in kind provided in 1998 to members of the family as referred to in Article 19 (2) of Council Regulation (EEC) No 1408/71 will be determined on the basis of the following average costs:
| Annual | Net Monthly |
Liechtenstein | Not requested | Not requested |
II. Application of Article 95 of Council Regulation (EEC) No 574/72
The amounts to be refunded with regard to benefits in kind provided in 1998 under Articles 28 and 28a of Council Regulation (EEC) No 1408/71 will be determined on the basis of the following average costs:
| Annual | Net Monthly |
Liechtenstein
—per family | CHF 6693,41 | CHF 446,23 |
—per person | CHF 6255,52 | CHF 417,03 |
AVERAGE COSTS OF BENEFITS IN KIND — 1999 [4]
I. Application of Article 94 of Council Regulation (EEC) No 574/72
The amounts to be refunded with regard to the benefits in kind provided in 1999 to members of the family as referred to in Article 19 (2) of Council Regulation (EEC) No 1408/71 will be determined on the basis of the following average costs:
| Annual | Net Monthly |
Liechtenstein | Not requested | Not requested |
II. Application of Article 95 of Council Regulation (EEC) No 574/72
The amounts to be refunded with regard to benefits in kind provided in 1999 under Articles 28 and 28a of Council Regulation (EEC) No 1408/71 will be determined on the basis of the following average costs:
| Annual | Net Monthly |
Liechtenstein
—per family | CHF 7055,38 | CHF 470,36 |
—per person | CHF 6656,02 | CHF 443,73 |
AVERAGE COSTS OF BENEFITS IN KIND — 2000 [5]
I. Application of Article 94 of Council Regulation (EEC) No 574/72
The amounts to be refunded with regard to the benefits in kind provided in 2000 to members of the family as referred to in Article 19 (2) of Council Regulation (EEC) No 1408/71 will be determined on the basis of the following average costs:
| Annual | Net Monthly |
Liechtenstein | Not requested | Not requested |
II. Application of Article 95 of Council Regulation (EEC) No 574/72
The amounts to be refunded with regard to benefits in kind provided in 2000 under Articles 28 and 28a of Council Regulation (EEC) No 1408/71 will be determined on the basis of the following average costs:
| Annual | Net Monthly |
Liechtenstein
—per family | CHF 7428,71 | CHF 495,25 |
—per person | CHF 6942,72 | CHF 462,85 |
AVERAGE COSTS OF BENEFITS IN KIND — 2002 [6]
I. Application of Article 94 of Council Regulation (EEC) No 574/72
The amounts to be refunded with regard to the benefits in kind provided in 2002 to members of the family as referred to in Article 19 (2) of Council Regulation (EEC) No 1408/71 will be determined on the basis of the following average costs:
| Annual | Net Monthly |
Greece | EUR 670,52 | EUR 44,70 |
Norway | NOK 26668 | NOK 1778 |
II. Application of Article 95 of Council Regulation (EEC) No 574/72
The amounts to be refunded with regard to benefits in kind provided in 2002 under Articles 28 and 28a of Council Regulation (EEC) No 1408/71 will be determined on the basis of the following average costs (only per capita from 2002):
| Annual | Net Monthly |
Greece | EUR 1276,62 | EUR 85,11 |
Norway | NOK 48745 | NOK 3250 |
AVERAGE COSTS OF BENEFITS IN KIND — 2003 [7]
I. Application of Article 94 of Council Regulation (EEC) No 574/72
The amounts to be refunded with regard to the benefits in kind provided in 2003 to members of the family as referred to in Article 19 (2) of Council Regulation (EEC) No 1408/71 will be determined on the basis of the following average costs:
| Annual | Net Monthly |
Luxembourg | EUR 2234,06 | EUR 148,94 |
Greece | EUR 766,13 | EUR 51,08 |
United Kingdom | GBP 1724,50 | GBP 114,97 |
II. Application of Article 95 of Council Regulation (EEC) No 574/72
The amounts to be refunded with regard to benefits in kind provided in 2003 under Articles 28 and 28a of Council Regulation (EEC) No 1408/71 will be determined on the basis of the following average costs (only per capita from 2002):
| Annual | Net Monthly |
Luxembourg | EUR 6019,65 | EUR 401,31 |
Greece | EUR 1490,78 | EUR 99,39 |
United Kingdom | GBP 2605,81 | GBP 173,72 |
AVERAGE COSTS OF BENEFITS IN KIND — 2004 [8]
I. Application of Article 94 of Council Regulation (EEC) No 574/72
The amounts to be refunded with regard to the benefits in kind provided in 2004 to members of the family as referred to in Article 19 (2) of Council Regulation (EEC) No 1408/71 will be determined on the basis of the following average costs:
| Annual | Net Monthly |
Czech Republic
—insured persons and pensioners younger than 65 years of age | CZK 11398,00 | CZK 759,85 |
Luxembourg | EUR 2362,70 | EUR 157,51 |
Germany
—per person | EUR 1034,73 | EUR 68,98 |
Liechtenstein | CHF 3607,62 | CHF 240,51 |
Sweden | SEK 14557,99 | SEK 970,53 |
Slovak Republic
—insured persons and pensioners younger than 65 years of age | SKK 8721,33 | SKK 581,42 |
France | EUR 1834,34 | EUR 122,29 |
Malta | MTL 230,25 | MTL 15,35 |
II. Application of Article 95 of Council Regulation (EEC) No 574/72
The amounts to be refunded with regard to benefits in kind provided in 2004 under Articles 28 and 28a of Council Regulation (EEC) No 1408/71 will be determined on the basis of the following average costs (only per capita from 2002):
| Annual | Net Monthly |
Czech Republic
—pensioners and members of pensioners' families aged 65 and over | CZK 36037,41 | CZK 2402,49 |
Luxembourg | EUR 7161,42 | EUR 477,43 |
Germany
—per person | EUR4184,79 | EUR 278,99 |
Liechtenstein | CHF 7812,50 | CHF 520,83 |
Sweden | SEK 39006,75 | SEK 2600,45 |
Slovak Republic
—pensioners and members of pensioners' families aged 65 and over | SKK 25653,29 | SKK 1710,22 |
France | EUR 4621,96 | EUR 308,13 |
Malta | MTL 595,48 | MTL 39,70 |
[1] Average costs 1996:
- Spain and Luxembourg (OJ C 303 of 2.10.1998).
- Belgium, Ireland, the Netherlands and Portugal (OJ C 56 of 26.2.1999)
- Germany, Austria and United Kingdom (OJ C 228 of 11.8.1999).
- Greece, France and Sweden (OJ C 27 of 29.1.2000).
- Italy (OJ C 211 of 28.7.2001).
- Norway (OJ C 182 of 31.7.2002).
[2] Average costs 1997:
- Spain (OJ C 228 of 11.8.1999)
- Belgium, Greece, Ireland, Luxembourg, United Kingdom, the Netherlands, Portugal (OJ C 27 of 29.1.2000).
- Germany, France, Austria (OJ C 207 of 20.7.2000).
- Sweden (OJ C 76 of 8.3.2001).
- Italy (OJ C 211 of 28.7.2001).
- Norway (OJ C 182 of 31.7.2002).
[3] Average costs 1998:
- Spain and Luxembourg (OJ C 27 of 29.1.2000).
- The Netherlands and Austria (OJ C 207 of 20.7.2000).
- Belgium, Germany and Portugal ( OJ C 76 of 8.3.2001).
- United Kingdom (OJ C 211 of 28.7.2001).
- Greece, France and Sweden (OJ C 20 of 23.1.2002), OJ C 34 of 7.2.2002 (rectification)
- Italy (OJ C 182 of 31.7.2002).
- Ireland (OJ C 3 of 8.1.2003).
- Norway (OJ C 163 of 12.7.2003).
[4] Average costs 1999:
- Spain and Austria (OJ C 76 of 8.3.2001)
- Germany (OJ C 211 of 28.7.2001)
- Belgium, Greece, France, Luxembourg, the Netherlands, Portugal, United Kingdom (OJ C 20 of 23.1.2002), (OJ C 34 of 7.2.2002 rectification)
- Italy and Sweden (OJ C 182 of 31.7.2002)
- Ireland and Norway (OJ C 163 of 12.7.2003)
[5] Average costs 2000:
- Spain and Luxembourg (OJ C 20 of 23.1.2002)
- Belgium, Germany, the Netherlands and Austria (OJ C 182 of 31.7.2002)
- Italy, Portugal and Sweden (OJ C 3 of 8.1.2003)
- Norway and United Kingdom (OJ C 163 of 12.7.2003)
- Greece, France and Ireland (OJ C 37 of 11.2.2004)
[6] Average costs 2002:
- Luxembourg and Austria (OJ C 37 of 11.2.2004)
- Belgium, France, Portugal, Sweden (OJ C 27 of 3.2.2005, p.4)
- Germany, Italy, United Kingdom (OJ C 232 of 21.9.2005, p.3)
- Liechtenstein (OJ C 17 of 24.1.2006)
[7] Average costs 2003:
- Austria, Spain and Switzerland (OJ C 27 of 3.2.2005, p. 4)
- Germany, France, the Netherlands (OJ C 232 of 21.9.2005, p.3)
- Belgium, Portugal, Sweden and Liechtenstein (OJ C 17 of 24.1.2006)
[8] Average costs 2004:
- Latvia: (OJ C 232 of 21.9.2005)
- Spain, Austria, Switzerland and Slovenia (OJ C 17 of 24.1.2006)
--------------------------------------------------
