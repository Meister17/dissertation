Council Decision 2006/631/JHA
of 24 July 2006
fixing the date of application of certain provisions of Decision 2005/211/JHA concerning the introduction of some new functions for the Schengen Information System, including in the fight against terrorism
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to Council Decision 2005/211/JHA of 24 February 2005 concerning the introduction of some new functions for the Schengen Information System, including in the fight against terrorism [1], and in particular Article 2(4) thereof,
Whereas:
(1) Article 2(4) of Decision 2005/211/JHA specifies that certain provisions of Article 1 of that Decision shall take effect from a date to be fixed by the Council, as soon as the necessary preconditions have been fulfilled, and that the Council may decide to fix different dates concerning the taking effect of those provisions.
(2) The preconditions mentioned in said Article 2(4) have been fulfilled in respect of Article 1(9) (new Articles 101A and 101B) of Decision 2005/211/JHA.
(3) As regards Switzerland, this Decision constitutes a development of the provisions of the Schengen acquis within the meaning of the Agreement signed between the European Union, the European Community and the Swiss Confederation on the Swiss Confederation’s association with the implementation, application and development of the Schengen acquis, which falls within the area referred to in Article 1, point G of Council Decision 1999/437/EC of 17 May 1999 on certain arrangements for the application of the Agreement concluded by the Council of the European Union and the Republic of Iceland and the Kingdom of Norway concerning the association of those two States with the implementation, application and development of the Schengen acquis [2], read in conjunction with Article 4(1) of Council Decision 2004/849/EC [3] and Council Decision 2004/860/EC [4] both of 25 October 2004 on the signing, on behalf of the European Union, on the signing, on behalf of the European Community, and on the provisional application of certain provisions of the said Agreement with the Swiss Confederation,
HAS DECIDED AS FOLLOWS:
Article 1
Article 1(9) (new Articles 101A and 101B) of Decision 2005/211/JHA shall apply from 1 October 2006.
Article 2
This Decision shall enter into force on the date of its adoption.
It shall be published in the Official Journal of the European Union.
Done at Brussels, 24 July 2006.
For the Council
The President
K. Rajamäki
[1] OJ L 68, 15.3.2005, p. 44.
[2] OJ L 176, 10.7.1999, p. 31.
[3] OJ L 368, 15.12.2004, p. 26.
[4] OJ L 370, 17.12.2004, p. 78.
--------------------------------------------------
