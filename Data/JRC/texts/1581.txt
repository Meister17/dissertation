[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 6.6.2005
SEC(2005) 758 final
PRELIMINARY DRAFT AMENDING BUDGET No 5 TO THE GENERAL BUDGET FOR 2005
GENERAL STATEMENT OF REVENUE
PRELIMINARY DRAFT AMENDING BUDGET No 5 TO THE GENERAL BUDGET FOR 2005
GENERAL STATEMENT OF REVENUE
Having regard to:
- the Treaty establishing the European Community, and in particular Article 272 thereof,
- the Treaty establishing the European Atomic Energy Community, and in particular Article 177 thereof,
- the Council Regulation (EC, Euratom) No 1605/2002 of 25 June 2002 on the Financial Regulation applicable to the general budget of the European Communities[1], and in particular Article 37 thereof,
The European Commission hereby presents to the budgetary authority the Preliminary Draft Amending Budget No 5 to the 2005 budget for the reasons set out in the explanatory memorandum.
TABLE OF CONTENTS
EXPLANATORY MEMORANDUM
1. Introduction 4
2. Own Resources 4
2.1. Revision of the forecast of TOR, VAT and GNI bases 5
2.2. 2001 UK correction and 2004 UK correction 7
2.2.1. Calculation of the corrections 8
2.2.2. Entry in the PDAB 5/2005 of the 1st update of the 2004 UK correction and the definitive amount of the 2001 UK correction 10
3. Mobilisation of the EU Solidarity Fund 13
3.1. Financing 14
SUMMARY TABLE BY HEADING OF THE FINANCIAL PERSPECTIVE 15
GENERAL STATEMENT OF REVENUE
The general statement of revenue is forwarded separately via the SEI-BUD system. A language version is attached as a technical annex by way of example.
EXPLANATORY MEMORANDUM
1. INTRODUCTION
The Preliminary Draft Amending Budget No 5 to the Budget for 2005 covers the following points:
- The revision of the forecast of own resources , both traditional i.e. customs duties, agricultural duties and sugar levies, as well as VAT and GNI bases and the budgeting of the relevant UK corrections as well as their financing, resulting in a change in the distribution between Member States of their own resources contributions to the EU budget.
- Article 7 of Council Decision 2000/597/EC, Euratom on the system of the European Communities' own resources states that "… surplus from the Guarantee Fund arising from external measures, transferred to the revenue account in the budget, shall be regarded as constituting own resources". Thus in addition to the revision of the forecasts of own resources, this PDAB enters the surplus in the general budget, at 31 December 2004, from the Guarantee Fund for external measures. This amounts to EUR 187 130 000.
- Moreover, an additional amount of EUR 338 831 402 is being budgeted as a result of the repayment stipulated in Article 1 point 2 of Council Regulation (EC, Euratom) No 2273/2004[2] of 22 December 2004. The reason for this special "surplus" is that once a country accedes to the European Union, all operations carried out for its benefit prior to accession now fall outside the scope of Council Regulation (EC, Euratom) No 2728/94 of 31 October 1994 establishing a Guarantee Fund for external actions. Therefore this amount results from the repayment to the general budget of the amount corresponding to the provision (9%) of outstanding capital liabilities for 10 new Member States at 1 May 2004.
- The mobilisation of the EU Solidarity Fund relating to a wind storm that struck Slovakia on 19/20 November 2004. This amounts to additional commitment appropriations of EUR 5 667 578.
2. OWN RESOURCES
The following summary table shows the distribution of total own resources payments between Member States as entered in the 2005 budget, in the Preliminary Draft Amending Budget (PDAB) 2/2005 (that modifies the 2005 budget as far as administrative expenditure and other revenue is concerned), in the PDAB 4/2005 (that includes the surplus from the year 2004), and in the PDAB 5/2005.
amounts in euro
Budget 2005 | PDAB 2/2005 | PDAB 4/2005 | PDAB 5/2005 | PDAB 5/2005 vs. PDAB 4/2005 | PDAB 4/2005 vs. PDAB 2/2005 |
(1) | (2) | (3) | (4) | in % | (5) = (4) – (3) | (6) = (3) – (2) |
BE | 4 035 286 807 | 4 034 071 061 | 3 958 006 808 | 4 090 787 847 | 4.01% | 132 781 039 | - 76 064 253 |
CZ | 932 392 859 | 932 050 540 | 910 633 033 | 999 336 079 | 0.98% | 88 703 046 | - 21 417 507 |
DK | 2 130 860 212 | 2 130 027 462 | 2 077 925 711 | 2 065 619 281 | 2.03% | - 12 306 430 | - 52 101 751 |
DE | 22 218 438 941 | 22 209 231 420 | 21 633 154 662 | 21 313 216 164 | 20.90% | - 319 938 498 | - 576 076 758 |
EE | 100 756 308 | 100 721 319 | 98 532 167 | 99 285 184 | 0.10% | 753 017 | - 2 189 152 |
EL | 1 882 611 879 | 1 881 884 866 | 1 836 398 637 | 1 848 094 066 | 1.81% | 11 695 429 | - 45 486 229 |
ES | 8 957 286 488 | 8 953 832 090 | 8 737 704 564 | 8 901 093 246 | 8.73% | 163 388 682 | - 216 127 526 |
FR | 17 303 107 859 | 17 296 186 826 | 16 863 166 150 | 16 888 265 374 | 16.56% | 25 099 224 | - 433 020 676 |
IE | 1 341 281 313 | 1 340 761 256 | 1 308 223 404 | 1 366 497 622 | 1.34% | 58 274 218 | - 32 537 852 |
IT | 14 359 479 157 | 14 353 744 719 | 13 994 964 477 | 13 995 632 301 | 13.73% | 667 824 | - 358 780 242 |
CY | 144 556 416 | 144 503 398 | 141 186 263 | 157 346 783 | 0.15% | 16 160 520 | - 3 317 135 |
LV | 115 205 431 | 115 161 253 | 112 397 198 | 125 829 063 | 0.12% | 13 431 865 | - 2 764 055 |
LT | 221 997 405 | 221 920 314 | 217 097 039 | 210 703 070 | 0.21% | - 6 393 969 | - 4 823 275 |
LU | 241 439 011 | 241 342 280 | 235 290 205 | 237 736 959 | 0.23% | 2 446 754 | - 6 052 075 |
HU | 1 003 119 411 | 1 002 775 247 | 981 242 315 | 895 883 747 | 0.88% | - 85 358 568 | - 21 532 932 |
MT | 57 409 269 | 57 390 176 | 56 195 638 | 50 902 858 | 0.05% | - 5 292 780 | - 1 194 538 |
NL | 5 552 933 781 | 5 550 990 856 | 5 429 430 031 | 5 411 974 358 | 5.31% | - 17 455 673 | - 121 560 825 |
AT | 2 308 432 030 | 2 307 450 646 | 2 246 049 473 | 2 208 751 026 | 2.17% | - 37 298 447 | - 61 401 173 |
PL | 2 099 087 114 | 2 098 288 261 | 2 048 307 347 | 2 366 512 579 | 2.32% | 318 205 232 | - 49 980 914 |
PT | 1 443 049 602 | 1 442 480 708 | 1 406 887 342 | 1 385 150 124 | 1.36% | - 21 737 218 | - 35 593 366 |
SI | 299 993 572 | 299 881 429 | 292 865 046 | 284 538 639 | 0.28% | - 8 326 407 | - 7 016 383 |
SK | 393 148 777 | 393 005 429 | 384 036 744 | 381 992 797 | 0.37% | - 2 043 947 | - 8 968 685 |
FI | 1 544 832 284 | 1 544 199 145 | 1 504 586 190 | 1 512 359 920 | 1.48% | 7 773 730 | - 39 612 955 |
SE | 2 832 862 800 | 2 831 678 451 | 2 757 578 600 | 2 816 998 182 | 2.76% | 59 419 582 | - 74 099 851 |
UK | 13 739 900 046 | 13 732 148 420 | 13 247 160 965 | 12 338 551 338 | 12.10% | - 908 609 627 | - 484 987 455 |
EU | 105 259 468 772 | 105 215 727 572 | 102 479 020 009 | 101 953 058 607 | 100.00% | - 525 961 402 | - 2 736 707 563 |
2.1. Revision of the forecast of TOR, VAT and GNI bases
According to established practice the Commission proposes to revise the financing of the budget on the basis of more recent economic forecasts, adopted at a meeting of the Advisory Committee on Own Resources (ACOR) that is normally held in April each year.
The revision concerns the forecast of TOR (customs duties, agricultural duties and sugar levies) to be paid to the budget in 2005 as well as the forecast of the 2005 VAT and GNI bases. The forecast in the 2005 budget (and in the PDABs 1-4/2005) was established at the 131st ACOR meeting on 20 April 2004. The revised forecast taken into account in the present PDAB 5/2005 was adopted at the meeting of the 133rd ACOR meeting on 8 April 2005. The use of an updated forecast of own resources improves the accuracy of the payments that Member States are asked to make during the budgetary year and reduces the unavoidable forecasting errors from the previous year.
As compared to the forecast agreed in April 2004, the forecast agreed in April 2005 has been revised as follows:
- The total forecast of 2005 net agricultural duties is unchanged at EUR 819.4 million (after deduction of 25 % in collection costs). Only the breakdown of this total forecast between Member States has been updated, using the latest information available on collected 2004 net agricultural duties.
- The total forecast of 2005 net sugar levies is nearly unchanged at EUR 793.8 million (after deduction of 25 % in collection costs). Only the breakdown of this total forecast between Member States has been updated, for the 10 new Member States only, using the latest information available on the forecast of 2006 sugar levies provided by DG AGRI.
- Total 2005 net customs duties are now forecast at EUR 12 030.8 million (after deduction of 25 % in collection costs), which represents an increase by + 11.9 % relative to the April 2004 forecast of EUR 10 749.9 million. The main reason for this increase is a substantial upward revision of the forecast growth rate 2004-2005 of extra EU imports (from 7.2 % up to 10.5 %). It has to be noted that the April 2005 forecast was made on a Member State basis, as DG ECFIN published forecast growth rates of extra EU imports for all 25 Member States in its spring 2005 economic forecasts.
- The total 2005 EU uncapped VAT base is now forecast at EUR 5 083 569.6 million, which represents an increase of + 1.6 % compared to the April 2004 forecast of EUR 5 003 621.2 million. The total 2005 EU capped VAT base[3] is forecast at EUR 4 893 151.0 million, which represents an increase of + 0.4 % compared to the April 2004 forecast of EUR 4 871 346.6 million.
- The total 2005 EU GNI base is forecast at EUR 10 568 696.6 million, which represents a decrease by - 0.1 % compared to the April 2004 forecast of EUR 10 584 036.6 million.
The exchange rates of 31 December 2004 have been used for converting the forecast VAT and GNI bases in national currency into euro (for the thirteen Member States that are not members of the euro zone). This avoids distortions since it is this rate which is used to convert own resources estimates from euro into national currency when the amounts are called in (as stipulated in Article 10(3) of Council Regulation No 1150/2000). Thus, the forecast value in euro of the VAT and GNI bases are increased for those Member States which have experienced an appreciation of their national currency against the euro since the ACOR meeting of the previous year, and vice versa.
The revised forecasts of TOR (customs duties, agricultural duties and sugar levies), uncapped VAT bases and GNI bases for 2005, as adopted at the 133rd ACOR meeting on 8 April 2005, are set out in the following table (rounded figures):
Revised forecasts of TOR, VAT & GNI bases for 2005 (in million euro)
Agricultural Duties (75 %) | Sugar levies (75 %) | Customs duties (75 %) | Uncapped VAT bases | GNI bases | Capped VAT base |
BE | 11.6 | 44.7 | 1 342.9 | 125 470.5 | 299 994.9 | 125 470.5 |
CZ | 2.8 | 8.6 | 134.0 | 56 081.5 | 91 639.7 | 45 819.9 |
DK | 16.9 | 25.7 | 230.4 | 79 270.2 | 202 208.9 | 79 270.2 |
DE | 124.0 | 215.0 | 2 302.6 | 954 066.8 | 2 218 037.5 | 954 066.8 |
EE | 0.5 | 0.0 | 15.2 | 5 171.6 | 8 969.9 | 4 485.0 |
EL | 7.9 | 10.4 | 188.1 | 101 189.5 | 176 960.5 | 88 480.3 |
ES | 40.8 | 21.5 | 1 008.0 | 511 218.5 | 843 306.0 | 421 653.0 |
FR | 63.2 | 205.3 | 960.6 | 808 893.5 | 1 693 486.5 | 808 893.5 |
IE | 0.4 | 6.4 | 133.7 | 73 298.0 | 132 064.7 | 66 032.4 |
IT | 63.2 | 72.4 | 1 271.5 | 606 559.0 | 1 387 628.2 | 606 559.0 |
CY | 2.0 | 0.0 | 35.8 | 10 260.5 | 12 829.1 | 6 414.6 |
LV | 0.4 | 0.8 | 17.6 | 5 203.1 | 11 707.8 | 5 203.1 |
LT | 1.3 | 1.3 | 29.8 | 12 207.2 | 19 134.5 | 9 567.3 |
LU | 0.1 | 0.0 | 13.1 | 16 229.6 | 24 153.0 | 12 076.5 |
HU | 3.4 | 7.0 | 116.9 | 37 319.1 | 84 093.0 | 37 319.1 |
MT | 1.3 | 0.0 | 8.6 | 3 477.5 | 4 400.2 | 2 200.1 |
NL | 182.4 | 50.1 | 1 136.8 | 230 749.0 | 472 707.0 | 230 749.0 |
AT | 4.3 | 20.4 | 165.6 | 104 603.5 | 239 049.5 | 104 603.5 |
PL | 22.4 | 40.9 | 202.7 | 124 808.7 | 225 415.4 | 112 707.7 |
PT | 21.4 | 2.8 | 89.5 | 90 762.0 | 137 076.0 | 68 538.0 |
SI | 0.1 | 0.6 | 28.6 | 15 032.0 | 27 390.8 | 13 695.4 |
SK | 0.7 | 6.9 | 42.9 | 15 429.0 | 36 543.9 | 15 429.0 |
FI | 3.2 | 4.7 | 95.9 | 67 268.0 | 155 153.5 | 67 268.0 |
SE | 9.4 | 11.6 | 308.9 | 123 447.1 | 298 341.6 | 123 447.1 |
UK | 235.7 | 36.7 | 2 151.1 | 905 554.2 | 1 766 404.5 | 883 202.3 |
EU | 819.4 | 793.8 | 12 030.8 | 5 083 569.6 | 10 568 696.6 | 4 893 151.0 |
2.2. 2001 UK correction and 2004 UK correction
The correction of budgetary imbalances in favour of the U nited Kingdom (UK correction) to be budgeted in the present PDAB concerns two years: 2001 and 2004. As the UK correction of a certain year is to be financed in the following year, all Member States, except the UK, participate in the financing of the 2004 UK correction. The financing of the 2001 UK correction does, however, only concern the 15 countries that were Member States in 2002.
Both the 2001 UK correction and 2004 UK correction are subject to the rules of Council Decision 2000/597 and its accompanying working document, the 2000 Calculation Method [4].
Pursuant to the above rules, the net “windfall gains” of the UK resulting from the increase as from 2001 in the percentage of TOR retained by Member States as a compensation for their collection costs are neutralised in the calculation of the UK correction, as well as pre-accession expenditure (PAE) paid under appropriations for payments relating to the year preceding the enlargement. The same adjustment for PAE will be followed at the occasion of each future enlargement of the Union.
Furthermore, the share of Austria, Germany, the Netherlands and Sweden in the financing of the UK correction is reduced to one fourth of their normal share. The reduction is financed by the other Member States, excluding the UK. These new financing rules have an impact on the uniform rate of call of VAT, calculated as the difference between the maximum rate of call (0.50 % of the capped VAT base) and the frozen rate (whose calculation is based on the 2004 UK correction).
Pursuant to the above rules, the difference between the 2001 UK correction ( definitive amount ) and the amount previously budgeted (in 2002) as well as the result of the entire recalculation of the financing of the whole amount of the correction on the basis of the latest data for 2002 is entered under chapter 35[5] of the budget, without any impact on the uniform rate of call of VAT. This is a simplification, as compared to the practice under the previous rules (Council Regulation 94/728 and its accompanying working document, the 1994 Calculation Method ) according to which the difference between the definitive amount of a UK correction and the amount previously budgeted was recorded under chapter 15[6] and the result of the recalculation of the financing of the whole amount of the correction was budgeted under chapter 35 (for further details on the recalculation of the financing see § 2.2.2.2 below).
2.2.1. Calculation of the corrections
In the present PDAB, the calculation and financing of the 1 st update of the 2004 UK correction as well as of the definitive amount of the 2001 UK correction are entered.
As far as the 2 nd update of the 2003 UK correction and the 3 rd update of the 2002 UK correction are concerned, the Commission shall – according to the 2000 Calculation Method – propose to budget such updates if they differ significantly from the corresponding previously budgeted calculation. The 2 nd update of the 2003 UK correction did not differ significantly from the 1 st update of the 2003 UK correction entered in the AB 8/2004. The 3 rd update of the 2002 UK correction did not differ significantly from the 1 st update of the 2002 UK correction entered in the AB 4/2003. Consequently, these updates are not proposed for budgeting in the present PDAB.
2.2.1.1. 2004 UK correction
The following table summarises the changes between the provisional amount of the 2004 UK correction entered in the budget 2005 and the 1 st update of the 2004 UK correction to be entered in the PDAB 5/2005.
2004 UK correction | Provisional Budget 2005 | 1st update PDAB 5/2005 | Difference |
(1) | UK share of notional uncapped VAT base | 18.1050% | 17.8653% | -0.2397% |
(2) | UK share of PAE-adjusted total allocated expenditure | 7.2284% | 7.9423% | 0.7139% |
(3) | = (1) - (2) | 10.8765% | 9.9229% | -0.9536% |
(4) | Total allocated expenditure | 84 685 362 853 | 92 293 901 043 | 7 608 538 189 |
(5) | Pre-accession expenditure (PAE) | 1 713 442 127 | 1 716 810 015 | 3 367 887 |
(6) | PAE-adjusted total allocated expenditure = (4) - (5) | 82 971 920 726 | 90 577 091 028 | 7 605 170 302 |
(7) | UK correction original amount = (3) x (6) x 0.66 | 5 956 157 923 | 5 932 026 743 | - 24 131 179 |
(8) | UK advantage | 820 148 901 | 725 367 786 | - 94 781 115 |
(9) | Core UK correction = (7) - (8) | 5 136 009 022 | 5 206 658 957 | 70 649 936 |
(10) | TOR windfall gains | 20 858 069 | 20 975 278 | 117 209 |
(11) | UK correction = (9) - (10) | 5 115 150 953 | 5 185 683 679 | 70 532 727 |
The 1 st update of the 2004 UK correction is EUR 70.5 million higher compared to the provisional amount of the 2004 UK correction entered in the 2005 budget. The difference is due to the combined effect of five factors, which can be quantified as follows:
- The decrease in the UK share of notional uncapped VAT base decreases the 2004 UK correction by around EUR 131.2 million.
- The increase in the UK share of PAE-adjusted total allocated expenditure decreases the 2004 UK correction by around EUR 382.9 million.
- The increase in PAE-adjusted total allocated expenditure increases the 2004 UK correction by around EUR 498.1 million.
- The decrease in the so-called “UK advantage” increases the 2004 UK correction by EUR 94.8 million. The “UK advantage” is the benefit that the UK derives from the current own resources system (due to the introduction of the GNP/GNI resource and the capping of the VAT bases) as compared to the system in force when the current UK correction was introduced in 1985.
- The increase in the so-called “TOR windfall gains” decreases the 2004 UK correction by EUR 0.1 million.
2.2.1.2. 2001 UK correction
The following table summarises the changes between the 1 st update of the 2001 UK correction entered in the SAB 3/2002 and the definitive amount of the 2001 UK correction to be entered in the PDAB 5/2005.
2001 UK correction | 1st update SAB 3/2002 | Definitive PDAB 5/2005 | Difference |
(1) | UK share of notional uncapped VAT base | 18.6820% | 19.1829% | 0.5009% |
(2) | UK share of PAE-adjusted total allocated expenditure | 8.1585% | 8.5584% | 0.3999% |
(3) | = (1) - (2) | 10.5234% | 10.6245% | 0.1010% |
(4) | Total allocated expenditure | 73 999 164 114 | 73 627 809 571 | - 371 354 543 |
(5) | Pre-accession expenditure (PAE) | 0 | 0 | 0 |
(6) | PAE-adjusted total allocated expenditure = (4) - (5) | 73 999 164 114 | 73 627 809 571 | - 371 354 543 |
(7) | UK correction original amount = (3) x (6) x 0.66 | 5 139 588 647 | 5 162 886 020 | 23 297 373 |
(8) | UK advantage | 48 290 792 | 212 371 624 | 164 080 832 |
(9) | Core UK correction = (7) - (8) | 5 091 297 855 | 4 950 514 396 | - 140 783 459 |
(10) | TOR windfall gains | 64 290 283 | 54 179 356 | - 10 110 927 |
(11) | UK correction = (9) - (10) | 5 027 007 572 | 4 896 335 040 | - 130 672 532 |
The definitive amount of the 2001 UK correction is EUR 130.7 million lower compared to the 1 st update of the 2001 UK correction entered in the SAB 3/2002. The difference is due to the combined effect of five factors, which can be quantified as follows:
- The increase in the UK share of notional uncapped VAT base increases the 2001 UK correction by around EUR 244.6 million.
- The increase in the UK share of PAE-adjusted total allocated expenditure decreases the 2001 UK correction by around EUR 195.3 million.
- The decrease in PAE-adjusted total allocated expenditure decreases the 2001 UK correction by around EUR 26.0 million.
- The increase in the so-called “UK advantage” decreases the 2001 UK correction by EUR 164.1 million.
- The decrease in the so-called “TOR windfall gains” increases the 2001 UK correction by EUR 10.1 million.
2.2.2. Entry in the PDAB 5/2005 of the 1 st update of the 2004 UK correction and the definitive amount of the 2001 UK correction
2.2.2.1. 2004 UK correction (chapter 15)
The amount of the UK correction to be budgeted in chapter 15 of the present PDAB 5/2005 is the amount of the 1 st update of the 2004 UK correction (i.e. EUR 5 185 683 679, replacing the EUR 5 115 150 953 entered in the budget 2005).
This amount is to be financed along the revised 2005 GNI bases of the present PDAB. The budgeting of this amount in chapter 15 is summarised below:
2004 UK correction chapter 15 |
BE | 253 499 702 |
CZ | 77 436 772 |
DK | 170 869 224 |
DE | 326 677 437 |
EE | 7 579 685 |
EL | 149 533 989 |
ES | 712 604 846 |
FR | 1 431 018 736 |
IE | 111 596 437 |
IT | 1 172 564 383 |
CY | 10 840 761 |
LV | 9 893 248 |
LT | 16 168 908 |
LU | 20 409 608 |
HU | 71 059 709 |
MT | 3 718 228 |
NL | 69 621 326 |
AT | 35 207 736 |
PL | 190 479 027 |
PT | 115 831 053 |
SI | 23 145 592 |
SK | 30 880 084 |
FI | 131 106 782 |
SE | 43 940 406 |
UK | - 5 185 683 679 |
Total | 0 |
2.2.2.2. 2001 UK correction (chapter 35)
The financing of the definitive amount of the 2001 UK correction is entered in chapter 35 of the present PDAB 5/2005 along the 2002 GNI (and VAT) bases as known at the end of 2004. The amount budgeted in chapter 35 takes into account:
- the adjustment as regards the direct effect , i.e. the difference between what each Member State should pay for the definitive amount of the 2001 UK correction (along the 2002 GNI bases as known at the end of 2004) and the corresponding amounts previously budgeted (i.e. the payments for the financing of the 1 st update of the 2001 UK correction budgeted in the SAB 3/2002),
- the adjustment as regards the indirect effect , i.e. the difference between the implicit impact[7] on Member States’ VAT and GNI payments of the definitive amount of the 2001 UK correction (along the 2002 VAT & GNI bases as known at the end of 2004) and the implicit impact on Member States’ VAT & GNI payments of the 1 st update of the 2001 UK correction in the SAB 3/2002 (along the 2002 VAT & GNI bases in the SAB 3/2002).
- The financing of the definitive amount of the 2001 UK correction in chapter 35 of the present PDAB 5/2005 is summarised below:
Slovakia | EUR 194 966 211 | EUR 4 307 425 | EUR 1 360 153 | EUR 5 667 578 |
1. This amount of compensation will leave at least 25% of the European Union Solidarity Fund available for allocation during the last quarter of the year, as required by Article 4(2) of Regulation 2012/2002 on the establishment of the EU Solidarity Fund.
The necessary payment credits will be made available by means of a transfer of appropriations from other budget lines, which the Commission will propose to the budgetary authority in due time.
SUMMARY TABLE BY HEADING OF THE FINANCIAL PERSPECTIVE
Financial perspective Heading/subheading | 2005 Financial perspective | Budget 2005 incl. PDAB 1-4/2005 | PDAB 5/2005 | Budget 2005 incl. PDAB 1-5/2005 |
|CA |PA |CA |PA |CA |PA |CA |PA | |1. AGRICULTURE | | | | | | | | | |- Agricultural expenditure |44 598 000 000 | |42 835 450 000 |42 835 450 000 | | |42 835 450 000 |42 835 450 000 | |- Rural development and accompanying measures |6 841 000 000 | |6 841 000 000 |6 279 400 000 | | |6 841 000 000 |6 279 400 000 | |Total |51 439 000 000 | |49 676 450 000 |49 114 850 000 | | |49 676 450 000 |49 114 850 000 | | Margin | | |1 762 550 000 | | | |1 762 550 000 | | |2. STRUCTURAL ACTIONS | | | | | | | | | |- Structural funds |37 247 000 000 | |37 291 564 455 |29 390 527 704 | | |37 291 564 455 |29 390 527 704 | |- Cohesion fund |5 194 000 000 | |5 131 932 989 |3 005 500 000 | | |5 131 932 989 |3 005 500 000 | |Total |42 441 000 000 | |42 423 497 444 |32 396 027 704 | | |42 423 497 444 |32 396 027 704 | | Margin | | |17 502 556 | | | |17 502 556 | | |3. INTERNAL POLICIES[9] |9 012 000 000 | |9 052 000 000 |7 923 781 439 |5 667 578 | |9 057 667 578 |7 923 781 439 | | Margin | | |-40 000 000 | | | |-40 000 000 | | |4. EXTERNAL ACTIONS[10] |5 119 000 000 | |5 317 000 000 |5 476 162 603 | | |5 317 000 000 |5 476 162 603 | | Margin | | |-198 000 000 | | | |-198 000 000 | | |5. ADMINISTRATION |6 360 000 000 | |6 292 686 171 |6 292 686 171 | | |6 292 686 171 |6 292 686 171 | | Margin | | |67 313 829 | | | | 67 313 829 | | |6. RESERVES | | | | | | | | | |- Guarantee reserve |223 000 000 | |223 000 000 |223 000 000 | | |223 000 000 |223 000 000 | |- Reserve for emergency aid[11] |223 000 000 | |223 000 000 |223 000 000 | | |223 000 000 |223 000 000 | |Total |446 000 000 | |446 000 000 |446 000 000 | | |446 000 000 |446 000 000 | | Margin | | |0 | | | |0 | | |7. PRE-ACCESSION AID |3 472 000 000 | |2 081 000 000 |3 286 990 000 | | |2 081 000 000 |3 286 990 000 | | Margin | | | 1 391 000 000 | | | | 1 391 000 000 | | |8. COMPENSATION |1 305 000 000 | |1 304 988 996 |1 304 988 996 | | |1 304 988 996 |1 304 988 996 | | Margin | | | 11 004 | | | | 11 004 | | | TOTAL |119 594 000 000 |114 235 000 000 |116 593 822 611 |106 241 486 913 |5 667 578 | | 116 599 490 189 |106 241 486 913 | | Margin | | |3 000 177 389 |7 993 513 087 | | |3 000 177 389 |7 993 513 087 | |
[1] OJ L 248, 16.09.2002, p.1.
[2] This Regulation amends Regulation No 2728/94.
[3] In accordance with Council Decision No 2000/597, if the VAT base of a Member State exceeds 50 % of its GNI, then it is capped at these 50 %. For the PDAB 5/2005, 13 Member States will have their VAT base capped at 50 % of GNI: the Czech Republic, Estonia, Greece, Spain, Ireland, Cyprus, Lithuania, Luxembourg, Malta, Poland, Portugal, Slovenia and the United Kingdom.
[4] Council Decision n° 2000/597 of 29 September 2000 on the system of the EU own resources, available on: http://europa.eu.int/eur-lex/pri/en/oj/dat/2000/l_253/l_25320001007en00420046.pdf
Commission working document of 21 September 2000 "Calculation, financing, payment and entry in the budget of the correction of budgetary imbalances in accordance with Articles 4 and 5 of Council Decision [2000/597] on the system of the EU own resources” referred as the 2000 Calculation Method and available on: http://europa.eu.int/comm/budget/pdf/financing/decisionrp_09_2000/calculation/en.pdf.
[5] Chapter 35: “Result of the definitive calculation of the correction of budgetary imbalances for the UK”
[6] Chapter 15: “Correction of budgetary imbalances”
[7] Due to the effect of the UK correction on the uniform rate of call of VAT (reduced by the ‘frozen rate’) and therefore also on the uniform rate of call of GNI (increased to compensate for the reduced VAT payments).
[8] Council Regulation (EC) N° 2012/2002 of 11 November 2002 establishing the European Union Solidarity Fund, OJ L 311 of 14.11.2002, p.3.
[9] The EUSF amount is entered over and above the relevant headings as foreseen by the IIA of 7 November 2002 (OJ C283 of 20.11.2002).
[10] Including EUR 98 million in excess of the ceiling due to the mobilisation of the Flexibility Instrument for the reconstruction of the Asian countries struck by the tsunami, as proposed in PDAB n°3/2005.
[11] Including EUR 100 million, which have been transferred to the emergency aid line.
