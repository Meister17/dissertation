COMMISSION REGULATION (EEC) No 2325/88 of 26 July 1988 amending certain Regulations applying to the common organization of the market in rice
THE COMMISSION OF THE EUROPEAN COMMUNITIES;
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 1418/76 of 21 June 1976 on the common organization of the market in rice (1), as last amended by Regulation (EEC) No 2229/88 (2), and in particular Article 16 (5) and 19 thereof,
Whereas under Regulation (EEC) No 1418/76 from 1 September 1988 rice will no longer be classified into two but into three categories, round-grained, medium-grained and long-grained; whereas the latter two categories correspond to the old long-grain category;
Whereas it is consequently necessary to amend Commission Regulation No 467/67/EEC of 21 August 1967 fixing the conversion rates, the processing costs and the value of the by-products for the various stages of rice processing (3), as last amended by Regulation (EEC) No 2249/85 (4), and Commission Regulation (EEC) No 1613/71 of 26 July 1971 laying down detailed rules for fixing cif prices and levies on rice and broken rice and the corrective amounts relating thereto (5), as last amended by Regulation (EEC) No 2117/80 (6);
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Cereals,
HAS ADOPTED THIS REGULATION:
Article 1 In Regulation No 467/67/EEC in Article 1 (2) and (3) and in (7) OJ No L 166, 25. 6. 1976, p. 1.
(8) OJ No L 197, 26. 7. 1988, p. 30.
(9) OJ No 204, 24. 8. 1967, p. 1.
(10) OJ No L 210, 7. 8. 1985, p. 13.
(11) OJ No L 168, 27. 7. 1971, p. 28.
(12) OJ No L 206, 8. 8. 1980, p. 15.
Article 3 (2) (b) and (3) (b) ´long-grained rice' is hereby replaced by ´medium-grained or long-grained rice'.
Article 2 Regulation (EEC) No 1613/71 is hereby amended as follows:
1. Article 4 (2) is replaced by the following:
´2. For long-grained or medium-grained husked rice:
(a) to medium-grained or long-grained husked rice, adjusted for any differences in quality compared with the standard quality;
(b) where appropriate, to medium-grained and long-grained paddy rice, adjusted for the conversion rates, milling costs and the value of the by-products and any differences in quality compared with the standard quality.'.
2. Article 4 (4) is replaced by:
´4. For medium-grained or long-grained milled rice:
(a) to medium-grained or long-grained milled rice, adjusted for any differences in quality compared with the standard quality for which the threshold price for husked rice is fixed, those differences being themselves adjusted in line with the rate applicable on conversion of long-grained husked rice into the long-grained milled rice;
(b) where appropriate, to medium-grained or long-grained semi-milled rice, adjusted for the conversion rate, milling costs and the value of the by-products for the purpose of obtaining medium-grained or long-grained milled rice, that price itself to be adjusted in accordance with the provisions of (a) above.'.
Article 3 This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
It shall apply from 1 September 1988.
27. 7. 88 Official Journal of the European Communities This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 26 July 1988.
For the Commission Frans ANDRIESSEN Vice-President EWG:L202UMBE29.97 FF: 7UEN; SETUP: 01; Hoehe: 719 mm; 83 Zeilen; 3563 Zeichen;
Bediener: WILU Pr.: C;
Kunde:
