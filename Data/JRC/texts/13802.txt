[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 23.6.2006
COM(2006) 330 final
2006/0108 (CNS)
Proposal for a
COUNCIL REGULATION
amending Regulation (EC) No 1788/2003 establishing a levy in the milk and milk products sector
(presented by the Commission)
EXPLANATORY MEMORANDUM
Due to the new CAP financing regulation (Council Regulation (EC) No 1290/2005 of 21 June 2005), the milk levy paid by Member States shall be regarded as assigned revenue as of 1 January 2007.
In this context, it is appropriate to make the levy available at the start of the budget year in order to improve budgetary forecasting and make budgetary management more flexible.
Council Regulation (EC) No 1788/2003 should be amended accordingly so that the levy due shall be paid by Member States in the period between 16 October to 30 November of each year rather than before 1 October under the current rules.
Furthermore, in some new Member States, the dairy sector has quickly adapted to the new commercial opportunities in an extended European market, generating a substantial restructuring process. Due to this situation but also due to stricter hygiene provisions for direct sales after Accession it appears that individual producers to a large extent have chosen not to apply for individual reference quantities for direct sales. Consequently, in some new Member States, the total individual reference quantities allocated to the producers for direct sales are substantially below the national reference quantities and unused quantities remain therefore in the national reserves for direct sales, hindering a further restructuration.
For the quota year 2005/06 it is therefore appropriate exceptionally to provide for a single transfer of the national reference quantity from direct sales to deliveries upon request by the new Member State concerned. Council Regulation (EC) No 1788/2003 should therefore be amended accordingly.
Due to the urgency of the two amendments the draft Regulation should enter into force as soon as possible.
2006/0108 (CNS)
Proposal for a
COUNCIL REGULATION
amending Regulation (EC) No 1788/2003 establishing a levy in the milk and milk products sector
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 37 thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament[1],
Whereas:
(1) According to Article 34(1)(b) of Council Regulation (EC) No 1290/2005 of 21 June 2005 on the financing of the common agricultural policy[2], sums which are collected or recovered under Council Regulation (EC) No 1788/2003[3] shall be regarded as assigned revenue within the meaning of Article 18 of Council Regulation (EC, Euratom) No 1605/2002 of 25 June 2002 on the Financial Regulation applicable to the general budget of the European Communities[4].
(2) In order to improve budgetary forecasting and make budgetary management more flexible, it is appropriate to make the levy introduced by Regulation (EC) No 1788/2003 available at the start of the budget year. Provision should be therefore made for the levy due to be paid in the period running from 16 October to 30 November of each year.
(3) For the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Poland, Slovenia and Slovakia (hereinafter referred to as the “new Member States”) the reference quantities for deliveries and direct sales were initially set out in table (f) of Annex I to Regulation (EC) No 1788/2003. Subsequently and in the light of the conversions requested by producers, those quantities were adapted for each Member State by the Commission in accordance with Article 8 of that Regulation.
(4) The national reference quantities for direct sales were fixed on the basis of the situation before the accession of the new Member States. However, following the restructuring process in the dairy sectors in the new Member States and stricter hygiene provisions for direct sales, it appears that individual producers to a large extent have chosen not to apply for individual reference quantities for direct sales. Consequently, the total individual reference quantities allocated to the producers for direct sales are substantially below the national reference quantities and important unused quantities therefore remain in the national reserves for direct sales.
(5) In order to solve this problem and to make possible the use of the direct sales quantities that may remain unused in the national reserve, it is appropriate to allow in the 2005/2006 period for a single transfer of the reference quantities for direct sales to the reference quantities for deliveries if requested by a new Member State.
(6) Regulation (EC) No 1788/2003 should therefore be amended accordingly,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 1788/2003 is amended as follows:
(1) In Article 3, paragraph 1 is replaced by the following:
“1. Member States shall be liable to the Community for the levy resulting from overruns of the national reference quantity fixed in Annex I, determined nationally and separately for deliveries and direct sales, and between 16 October and 30 November following the twelve-month period concerned, shall pay it, within the limit of 99% of the amount due, into the European Agricultural Guidance and Guarantee Fund (EAGGF).”
(2) In Article 8(1), the following subparagraph is added:
“For the 2005/2006 period, in accordance with the same procedure, and for the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Poland, Slovenia and Slovakia, the Commission may also adapt the division between “deliveries” and “direct sales” of the national reference quantities after the end of that period at the request of the Member State concerned. This request shall be submitted to the Commission before dd/mm/yyyy [i.e X weeks following the publication of this regulation, to be fixed depending on date of publication]. The Commission shall subsequently adapt the division as soon as possible.”
Article 2
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Union .
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels,
For the Council
The President
FINANCIAL STATEMENT |
1. | BUDGET HEADING (2006 budget): 05 02 12 07 | APPROPRIATIONS: –EUR 389 million |
2. | TITLE: Council Regulation amending Regulation (EC) No 1788/2003 establishing a levy in the milk and milk products sector |
3. | LEGAL BASIS: Article 37 of the Treaty |
4. | AIMS: 1) to change the date of payment of the levy due to the Community budget, i.e. in the period running from 16 October to 30 November following the 12-month period of the quota year; 2) to allow 8 new Member States to transfer totally or partially the national reference quantities for direct sales to the reference quantities for deliveries. |
5. | FINANCIAL IMPLICATIONS | 12 MONTH PERIOD (EUR million) | CURRENT FINANCIAL YEAR 2006 (EUR million) | FOLLOWING FINANCIAL YEAR 2007 (EUR million) |
5.0 | EXPENDITURE CHARGED TO – THE EC BUDGET (REFUNDS/INTERVENTION) – NATIONAL ADMINISTRATION – OTHER | – | 1) +306 2) – | – |
5.1 | REVENUE – ASSIGNED REVENUE (LEVIES/CUSTOMS DUTIES) – NATIONAL | – | – | 1) – 2) –30,6 |
2008 | 2009 | 2010 | 2011 |
5.0.1 | ESTIMATED EXPENDITURE | – | – | – | – |
5.1.1 | ESTIMATED REVENUE | – | – | – | – |
5.2 | CALCULATION METHOD: 1) changing the date of payment of the levy due: between 16 October and 30 November following the quota year instead of before 16 October following the quota year The measure proposed concerns a defer the date of the perception of the milk quota levy and this for the first time from financial year 2006 to financial year 2007. There will thus be an impact on financial year 2006 which will be deprived of this amount. – budgetary impact for the 2006 budget: Article 05 02 12 07: 1 000 000 t x –309,1 €/t x 99% = €306 million (2005/06 quota year) The revenue of € 306 Mio as the levy for marketing year 2005/2006 transferred to financial year 2007 will be neutralised by a lesser revenue for the levy for the marketing year 2006/07 (postponed to budget year 2008) using the same underlying hypothesis in 2007. In conformity with Article 34 § 1b of Regulation 1782/2005 the levy will be classified as assigned revenue as from 2007 onwards, – budgetary impact for the 2007 budget (assigned revenue): Article 67 03: € 0 million It should be noted that the PDB 2007 already takes this proposed change into account. 2) transfer between 'direct sales' and 'deliveries' in the new Member States The hypothesis of a transfer of 100 000 t from the 'direct sales' quota to the 'deliveries' quota in Poland, which would already exceed its present reference quantities by about 365 000 t, would lead to a loss of levy for 2005/2006 (2007 budget) of: Article 67 03: 100 000 t x –309,1 €/t x 99% = –€30,6 million Considering that, in the other MS concerned, the existing reference quantities will probably not be exceeded, a possible transfer in these MS would not lead to exceeding their quota and, consequently, not entail a loss of levy. |
6.0 | CAN THE PROJECT BE FINANCED FROM APPROPRIATIONS ENTERED IN THE RELEVANT CHAPTER OF THE CURRENT BUDGET? | YES NO |
6.1 | CAN THE PROJECT BE FINANCED BY TRANSFER BETWEEN CHAPTERS OF THE CURRENT BUDGET? | YES NO |
6.2 | IS A SUPPLEMENTARY BUDGET NECESSARY? | YES NO |
6.3 | WILL APPROPRIATIONS NEED TO BE ENTERED IN FUTURE BUDGETS? | YES NO |
OBSERVATIONS: |
[1] OJ C …, …, p. ….
[2] OJ L 209, 11.8.2005, p. 1. Regulation as amended by Regulation (EC) No 320/2006 (OJ L 58, 28.2.2006, p. 42).
[3] OJ L 270, 21.10.2003, p. 123. Regulation as last amended by the Act of Accession of 2003.
[4] OJ L 248, 16.9.2002, p. 1.
