COUNCIL DECISION
of 17 December 1999
authorising the Deputy Secretary-General of the Council of the European Union to act as representative of certain Member States for the purpose of concluding contracts relating to the installation and the functioning of the communication infrastructure for the Schengen environment, "SISNET", and to manage such contracts
(1999/870/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Acting on the basis of Article 7 of the Protocol annxed to the Treaty on European Union and the Treaty establishing the European Community, integrating the Schengen acquis into the framework of the European Union (hereinafter the "Schengen Protocol"),
Whereas:
(1) The Secretary-General of the Council has been authorised to manage on behalf of those Member States concerned the contract relating to the installation and the functioning of the SIRENE Network Phase II(1);
(2) The Member States concerned have decided not to extend the contract for the SIRENE Network Phase II, which will therefore terminate on 23 August 2001;
(3) A new communication infrastructure for the Schengen environment, to be called "SISNET" must accordingly be provided for by 23 August 2001, which will require the execution of preparatory measures for the conclusion of the relevant contracts, the conclusion of the contracts themselves and the management of those contracts;
(4) The Member States concerned have requested the Deputy Secretary-General of the Council to represent them with respect to the execution of the necessary preparatory measures as well as the conclusion and management of the contracts in question;
(5) The performance of such a task by the Deputy Secretary-General of the Council on behalf of certain Member States constitutes a task distinct from the tasks performed by the Deputy Secretary-General pursuant to his obligations under the Treaty establishing the European Community and the Treaty on European Union;
(6) It is therefore appropriate to have this task assigned to the Deputy Secretary-General by way of an explicit decison of the Council,
HAS DECIDED AS FOLLOWS:
Article 1
1. The Council hereby authorises the Deputy Secretary-General of the Council to act as the representative of the member States concerned (Belgium, Denmark, Germany, Greece, Spain, France, Italy, Luxembourg, the Netherlands, Austria, Portugal, Finland and Sweden), with respect to:
(a) the execution of a call for tenders for the delivery, installation and management of the SISNET and any other preparatory measures which may be necessary in this regard; and
(b) the conclusion and management of the contracts for the delivery, installation and management of the SISNET and the supply of services relating to the use thereof,
2. The authorisation under paragraph 1 above shall apply so long as payments under the aforementioned contracts are not charged to the general budget of the European Union but continue to be charged to the Member States concerned.
3. The Deputy Secretary-General shall aslo be authorised to act as the representative of Ireland and/or of the United Kingdom with respect to the matters set out in paragraph 1 above, pursuant to any future Council decision on the participation of either of these Member States in some or all of the provisions of the Schengen acquis, in accordance with the provisions of Article 4 of the Schengen Protocol.
Article 2
The work involved in preparing the call for tenders and managing the subsequent contracts referred to in Article 1(1) on behalf of the Member States concerned shall be performed by the General Secretariat of the Council as part of its normal administrative tasks.
Article 3
All questions relating to any non-contractual liability resulting from the acts or omissions of the General Secretariat of the Council in the performance of its administrative tasks pursuant to this Decision shall be governed by Article 288, second paragraph, of the Treaty establishing the European Community. Article 235 of that Treaty shall therefore apply to any disputes relating to compensation for damage.
Article 4
1. The special bank account in the name of the Secretary-General of the Council, opened for the purpose of managing the contracts mentioned in Decision 1999/322/EC, shall be used in respect of the budget relating to the conclusion and management of the contracts mentioned in Article 1(1).
2. The Deputy Secretary-General shall be authorised to use the bank account referred to in paragraph 1 for the purposes of carrying out his obligations pursuant to this Decision.
Article 5
This Decision shall take effect on the day of its adoption.
Article 6
This Decision shall be published in the Official Journal of the European Communities.
Done at Brussels, 17 December 1999.
For the Council
The President
K. HEMILÄ
(1) Council Decision 1999/322/EC of 3 May 1999, OJ L 123, 13.5.1999, p. 49.
