*****
COMMISSION REGULATION (EEC) No 3786/89
of 15 December 1989
authorizing Portugal to abolish import duties on oil cake from other Member States and to apply Common Customs Tariff duties to imports of oil cake from third countries
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to the Act of Accession of Spain and Portugal, and in particular Article 243 (4) thereof,
Whereas the first indent of Article 243 (4) (a) of the Act of Accession provides that Portugal may, at its request, abolish the customs duties on oil seeds, oleaginous fruit and products derived therefrom or move towards their alignment;
Whereas Commission Regulation (EEC) No 566/87 (1) authorizes Portugal to suspend partially the import duties on oil cake until 31 December 1987; whereas Commission Regulation (EEC) No 1692/88 (2) provides for the same authorization until 31 December 1988; whereas the aim of that measure was to facilitate the supply of oil cake to the Portuguese feedingstuffs industry; whereas, since then, the factors which justified that measure have remained valid; whereas Portugal has requested authorization to abolish, pursuant to Article 243 mentioned above, the customs duties on oil cake vis-à-vis the rest of the Community and to apply the Common Customs Tariff vis-à-vis third countries from 1 January 1990;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Oils and Fats,
HAS ADOPTED THIS REGULATION:
Article 1
Portugal is hereby authorized to abolish duties applicable to imports of oil cake and other solid residues falling within CN codes 2304 00 00, 2305 00 00 and 2306, with the exception of CN codes 2306 90 11 and 2306 90 19, originating in the other Member States and to apply the Common Customs Tariff duties to imports of those products originating in third countries.
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
It shall apply from 1 January 1990.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 15 December 1989.
For the Commission
Ray MAC SHARRY
Member of the Commission
(1) OJ No L 57, 27. 2. 1987, p. 17.
(2) OJ No L 151, 17. 6. 1988, p. 35.
