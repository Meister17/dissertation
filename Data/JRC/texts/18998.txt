Commission Decision
of 15 October 2003
laying down special conditions governing imports of fishery products from the Netherlands Antilles
(notified under document number C(2003) 3649)
(Text with EEA relevance)
(2003/762/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/493/EEC of 22 July 1991 laying down the health conditions for the production and the placing on the market of fishery products(1), as last amended by Regulation (EC) No 806/2003(2), and in particular Article 11 thereof,
Whereas:
(1) An inspection has been carried out on behalf of the Commission in the Netherlands Antilles to verify the conditions under which fishery products are produced, stored and dispatched to the Community.
(2) The requirements in the legislation of the Netherlands Antilles on health inspection and monitoring of fishery products may be considered equivalent to those laid down in Directive 91/493/EEC.
(3) In particular, the Inspectorate for Public Health (IPH) of the Ministry of Public Health and Social Development, is capable of effectively verifying the implementation of the legislation in force.
(4) The IPH has provided official assurances regarding compliance with the standards for health controls and monitoring of fishery products as set out in Chapter V of the Annex to Directive 91/493/EEC and regarding the fulfilment of hygienic requirements equivalent to those laid down by that Directive.
(5) It is appropriate to lay down detailed provisions concerning fishery products imported into the Community from the Netherlands Antilles, in accordance with Directive 91/493/EEC.
(6) It is also necessary to draw up a list of approved establishments, factory vessels, or cold stores, and a list of freezer vessels equipped in accordance with the requirements of Council Directive 92/48/EEC of 16 June 1992 laying down the minimum hygiene rules applicable to fishery products caught on board of certain vessels in accordance with Article 3(1)(a)(i) of Directive 91/493/EEC(3). These lists should be drawn up on the basis of a communication from the IPH to the Commission.
(7) However, at the time of the inspection visit there were no establishments approved in conformity with the Community legislation and the inspection team was not able to verify the inspection capacity of the competent authorities as regards the land establishments. Therefore, the inclusion of establishments in the list will require a further evaluation.
(8) It is appropriate for the present Decision to be applied 45 days after its publication to provide for the necessary transitional period.
(9) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
The Inspectorate for Public Health (IPH) of the Ministry of Public Health and Social Development, shall be the competent authority in the Netherlands Antilles identified for the purposes of verifying and certifying compliance of fishery products with the requirements of Directive 91/493/EEC.
Article 2
Fishery products imported into the Community from the Netherlands Antilles shall comply with Articles 3, 4 and 5.
Article 3
1. Each consignment shall be accompanied by a numbered original health certificate in accordance with the model in Annex I and comprising a single sheet, duly completed, signed and dated.
2. The certificate shall be drawn up in at least one official language of the Member State where the checks are carried out.
3. The certificate shall bear the name, capacity and signature of the representative of the IPH, and the latter's official stamp in a colour different from that of the endorsements.
Article 4
The fishery products shall come from approved factory vessels, or from registered freezer vessels listed in Annex II.
Article 5
All packages shall bear the words "NETHERLANDS ANTILLES" and the approval/registration number of the factory or freezer vessel of origin in indelible letters, except in the case of frozen fishery products in bulk and intended for the manufacture of preserved foods.
Article 6
The inclusion of establishments in the list of Annex II shall only be done following the results of a Community evaluation.
Article 7
This Decision shall apply from 8 December 2003.
Article 8
This Decision is addressed to the Member States.
Done at Brussels, 15 October 2003.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 268, 24.9.1991, p. 15.
(2) OJ L 122, 16.5.2003, p. 1.
(3) OJ L 187, 7.7.1992, p. 41.
ANNEX I
>PIC FILE= "L_2003273EN.003502.TIF">
>PIC FILE= "L_2003273EN.003601.TIF">
ANNEX II
List of establishments and vessels
>TABLE>
Legend:
ZV Freezer vessel.
