Commission Decision
of 29 December 2004
amending Decisions 2003/743/EC and 2003/849/EC as regards the reallocation of the Community's financial contribution to certain Member States for their programmes for the eradication and monitoring of animal diseases and for checks aimed at the prevention of zoonoses for 2004
(notified under document number C(2004) 5397)
(Text with EEA relevance)
(2004/923/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Decision 90/424/EEC of 26 June 1990 on expenditure in the veterinary field [1], and in particular Article 24(5) and (6), and Articles 29 and 32 thereof,
Whereas:
(1) Decision 90/424/EEC provides for the possibility of financial participation by the Community towards the programmes of Member States aimed at the eradication and monitoring of animal diseases and for checks aimed at the prevention of zoonoses.
(2) Commission Decision 2003/743/EC of 14 October 2003 on the lists of programmes for the eradication and monitoring of animal diseases and of checks aimed at the prevention of zoonoses qualifying for a financial contribution from the Community in 2004 [2] sets out the proposed rate and maximum amount of the Community’s financial contribution for each programme submitted by the Member States.
(3) Commission Decision 2003/849/EC of 28 November 2003 approving programmes for the eradiction and monitoring of certain animal diseases and for the prevention of zoonoses presented by the Member States for the year 2004 and fixing the level of the Community’s financial contribution [3] sets out the maximum amount of the Community’s financial contribution for each programme submitted by the Member States.
(4) The Commission has analysed the reports forwarded by the Member States on the expenditures of those programmes. The results of that analysis show that certain Member States will not utilise their full allocation for 2004 while others will spend in excess of the allocated amount.
(5) The Community’s financial contribution to certain of those programmes therefore needs to be adjusted. It is appropriate to reallocate funding from programmes of Member States, which are not using their full allocation to those that are exceeding it. The reallocation should be based on the most recent information on the expenditure actually incurred by the concerned Member States.
(6) Decisions 2003/743/EC and 2003/849/EC should therefore be amended accordingly.
(7) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
Annexes I and II to Decision 2003/743/EC are amended in accordance with the Annex to this Decision.
Article 2
Decision 2003/849/EC is amended as follows:
1. In Article 1(2) "EUR 200000" is replaced by "EUR 190000";
2. In Article 2(2) "EUR 650000" is replaced by "EUR 700000";
3. In Article 3(2) "EUR 800000" is replaced by "EUR 600000";
4. In Article 4(2) "EUR 70000" is replaced by "EUR 80000";
5. In Article 5(2) "EUR 370000" is replaced by "EUR 0";
6. In Article 6(2) "EUR 1800000" is replaced by "EUR 1695000";
7. In Article 7(2) "EUR 110000" is replaced by "EUR 0";
8. In Article 8(2) "EUR 400000" is replaced by "EUR 410000";
9. In Article 9(2) "EUR 85000" is replaced by "EUR 55000";
10. In Article 11(2) "EUR 4000000" is replaced by "EUR 4150000";
11. In Article 12(2) "EUR 5000000" is replaced by "EUR 5055000";
12. In Article 13(2) "EUR 1500000" is replaced by "EUR 1545000";
13. In Article 15(2) "EUR 150000" is replaced by "EUR 50000";
14. In Article 16(2) "EUR 1800000" is replaced by "EUR 2000000";
15. In Article 17(2) "EUR 110000" is replaced by "EUR 125000";
16. In Article 18(2) "EUR 2000000" is replaced by "EUR 2700000";
17. In Article 20(2) "EUR 5000000" is replaced by "EUR 4935000";
18. In Article 22(2) "EUR 1200000" is replaced by "EUR 1900000";
19. In Article 24(2) "EUR 150000" is replaced by "EUR 165000";
20. In Article 25(2) "EUR 400000" is replaced by "EUR 540000";
21. In Article 26(2) "EUR 40000" is replaced by "EUR 255000";
22. In Article 28(2) "EUR 100000" is replaced by "EUR 110000";
23. In Article 30(2) "EUR 100000" is replaced by "EUR 115000";
24. In Article 33(2) "EUR 725000" is replaced by "EUR 195000";
25. In Article 35(2) "EUR 6500000" is replaced by "EUR 6000000";
26. In Article 36(2) "EUR 300000" is replaced by "EUR 395000";
27. In Article 37(2) "EUR 3500000" is replaced by "EUR 4500000";
28. In Article 38(2) "EUR 17000" is replaced by "EUR 2000";
29. In Article 39(2) "EUR 2000000" is replaced by "EUR 1600000";
30. In Article 40(2) "EUR 70000" is replaced by "EUR 0";
31. In Article 41(2) "EUR 150000" is replaced by "EUR 355000";
32. In Article 43(2) "EUR 700000" is replaced by "EUR 1205000";
33. In Article 44(2) "EUR 150000" is replaced by "EUR 100000";
34. In Article 45(2) "EUR 260000" is replaced by "EUR 210000";
35. In Article 46(2) "EUR 700000" is replaced by "EUR 150000";
36. In Article 47(2) "EUR 90000" is replaced by "EUR 100000";
37. In Article 48(2) "EUR 400000" is replaced by "EUR 50000";
38. In Article 49(2) "EUR 400000" is replaced by "EUR 200000";
39. In Article 50(2) "EUR 400000" is replaced by "EUR 10000";
40. In Article 54(2) "EUR 75000" is replaced by "EUR 95000";
41. In Article 55(2) "EUR 800000" is replaced by "EUR 900000";
42. In Article 58(2) "EUR 30000" is replaced by "EUR 25000";
43. In Article 60(2) "EUR 700000" is replaced by "EUR 550000";
44. In Article 62(2) "EUR 100000" is replaced by "EUR 160000";
45. In Article 63(2) "EUR 50000" is replaced by "EUR 10000";
46. In Article 65(2) "EUR 5000" is replaced by "EUR 0";
47. In Article 67(2) "EUR 60000" is replaced by "EUR 30000".
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 29 December 2004.
For the Commission
Markos Kyprianou
Member of the Commission
--------------------------------------------------
[1] OJ L 224, 18.8.1990, p. 19. Decision as last amended by Regulation (EC) No 806/2003 (OJ L 122, 16.5.2003, p. 1).
[2] OJ L 268, 18.10.2003, p. 77.
[3] OJ L 322, 9.12.2003, p. 16.
--------------------------------------------------
+++++ ANNEX 1 +++++</br>
