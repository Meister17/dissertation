Decision of the EEA Joint Committee
No 77/2004
of 8 June 2004
amending Annex XIII (Transport) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex XIII to the Agreement was amended by the Agreement on the participation of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Hungary, the Republic of Latvia, the Republic of Lithuania, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic in the European Economic Area signed on 14 October 2003 in Luxembourg [1].
(2) Regulation (EC) No 1726/2003 of the European Parliament and of the Council of 22 July 2003 amending Regulation (EC) No 417/2002 on the accelerated phasing-in of double-hull or equivalent design requirements for single-hull oil tankers [2] is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
The following indent shall be added in point 56m (Regulation (EC) No 417/2002 of the European Parliament and of the Council) of Annex XIII to the Agreement:
"— 32003 R 1726: Regulation (EC) No 1726/2003 of the European Parliament and of the Council of 22 July 2003 (OJ L 249, 1.10.2003, p. 1)."
Article 2
The texts of Regulation (EC) No 1726/2003 in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 9 June 2004, provided that all the notifications pursuant to Article 103(1) of the Agreement have been made to the EEA Joint Committee [3].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 8 June 2004.
For the EEA Joint Committee
The President
S. Gillespie
--------------------------------------------------
[1] OJ L 130, 29.4.2004, p. 3.
[2] OJ L 249, 1.10.2003, p. 1.
[3] No constitutional requirements indicated.
