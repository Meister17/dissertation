Reference for a preliminary ruling from the Krajský soud v Ostravě lodged on 24 March 2006 — Skoma-Lux, s.r.o. v Celní ředitelství Olomouc
Referring court
Krajský soud v Ostravě (Czech Republic)
Parties to the main proceedings
Applicant: Skoma-Lux, s.r.o.
Defendant: Celní ředitelství Olomouc
Questions referred
1. May Article 58 of the Act concerning the conditions of accession of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic and the adjustments to the Treaties on which the European Union is founded, on the basis of which the Czech Republic became a Member State of the European Union from 1 May 2004, be interpreted as meaning that a Member State may apply against an individual a regulation which at the time of its application has not been properly published in the Official Journal in the official language of that Member State?
2. If Question 1 is answered in the negative, is the unenforceability of the regulation concerned against an individual a question of the interpretation or of the validity of Community law within the meaning of Article 234 of the Treaty establishing the European Community?
3. Should the Court of Justice conclude that the present reference for a preliminary ruling concerns the validity of a Community act within the meaning of the judgment in Case 314/85 Foto-Frost [1987] ECR 4199, is Regulation No 2454/93 invalid in relation to the applicant and its dispute with the customs authorities of the Czech Republic on the ground of the absence of proper publication in the Official Journal of the EU in accordance with Article 58 of the Act concerning the conditions of accession?
--------------------------------------------------
