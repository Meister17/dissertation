Commission Regulation (EC) No 1085/2000
of 15 May 2000
laying down detailed rules for the application of control measures applicable in the area covered by the Convention on Future Multilateral Cooperation in the North-East Atlantic Fisheries
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 2791/1999 of 16 December 1999 laying down certain control measures applicable in the area covered by the Convention on Future Multilateral Cooperation in the North-East Atlantic Fisheries(1), and in particular Articles 4(3), 6(5), 7(3), 8, 11(5) and (6), 12(1), 13(1), 14(1)(h), 19(7) and 27 thereof,
Whereas:
(1) Regulation (EC) No 2791/1999 lays down certain specific control measures to monitor Community fishing activities in the North-east Atlantic Fisheries Commission (NEAFC) regulatory area and to supplement the control measures provided for in Council Regulation (EEC) No 2847/93 of 12 October 1993 establishing a control system applicable to the common fisheries policy(2), as last amended by Regulation (EC) No 2846/98(3). Detailed rules should be laid down for the application of that Regulation.
(2) The Annexes to the NEAFC scheme of control and enforcement in respect of fishing vessels fishing in areas beyond the limits of national fisheries' jurisdiction in the convention area adopted by the NEAFC set out the formats for communicating data and models for certain inspection tools which should be adopted at Community level. These formats and models are shown in the Annex to this Regulation.
(3) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Fisheries and Aquaculture,
HAS ADOPTED THIS REGULATION:
CHAPTER I
SCOPE
Article 1
Aim
This Regulation sets out detailed rules for the application of Regulation (EC) No 2791/1999.
Article 2
Definitions
For the purposes of this Regulation:
1. "authorised vessels" means vessels which, in accordance with Article 7(3) of Council Regulation (EC) No 1627/94(4), have been issued a special fishing permit authorising, in accordance with Article 4(1) of Regulation (EC) No 2791/1999, fishing activities in the NEAFC regulatory area for one or more regulated resources under Regulation (EC) No 2791/1999;
2. "FMC" means fisheries monitoring centres as set up by the Member States to manage the satellite-based monitoring system;
3. "message" means the report on the vessel's position automatically transmitted by the satellite-based monitoring system to the FMC of the flag Member State;
4. "position report" means the manual report made by the captain in the circumstances provided for in Article 6 of Commission Regulation (EC) No 1489/97(5);
5. "scheme" means the scheme of control and enforcement referred to in Article 1(a) of Regulation (EC) No 2791/1999.
CHAPTER II
DATA COMMUNICATION
Article 3
Community participation
1. The communication referred to in Article 4(2) of Regulation (EC) No 2791/1999 shall include:
(a) the list of vessels authorised to fish in the regulatory area in accordance with Article 4(1) of that Regulation;
(b) the list of vessels authorised to fish for one or more regulated resources, broken down by species, and
(c) amendments to these lists.
The lists shall refer to the internal number allocated to each vessel in the fishing vessel register, in accordance with Article 5 of Commission Regulation (EC) No 2090/98(6).
2. Member States shall immediately forward to the Commission, by computer transmission, the internal numbers of authorised vessels the special permits of which have been withdrawn or suspended.
Article 4
Reporting of catches of regulated resources
The content and format of the communications to be forwarded under Article 6(5) of Regulation (EC) No 2791/1999 shall be as set out in Annex I.
Article 5
Global reporting of catches
The list of resources referred to in Article 7(1) of Regulation (EC) No 2791/1999 shall be as set out in Annex II.
For the global reporting of catches referred to in Article 7(1), Member States shall use the format set out in Annex II.
Article 6
Reporting of positions
1. The communications referred to in Article 8 of Regulation (EC) No 2791/1999 shall be transmitted by the FMCs.
2. The content and format of these reports shall be as set out in Annex III.
3. The FMCs shall forward in particular the information contained in:
(a) the first message received after the vessel enters the regulatory area;
(b) at least one message every six hours while the vessel is in the regulatory area, and
(c) the first message received after the vessel leaves the regulatory area.
4. Where applicable, the position reports shall be forwarded to the NEAFC secretariat at least once every 24 hours. Where necessary, the Member States may authorise the captain of the vessel to send a copy of this report immediately to the NEAFC secretariat.
CHAPTER III
SECURITY AND CONFIDENTIALITY
Article 7
Secure and confidential treatment of electronic reports and messages
1. The provisions of paragraphs 2 to 9 shall apply to all electronic reports and messages under this Regulation and Regulation (EC) No 2791/1999, with the exception of the global reporting of catches referred to in Article 5 of this Regulation.
2. The relevant authorities in the Member States responsible for processing the reports and messages shall take all necessary measures to comply with the security and confidentiality provisions set out in paragraphs 4 to 9.
3. Each Member State shall, where necessary, at the request of the NEAFC secretariat, rectify or erase reports or messages which have not been dealt with in accordance with Regulation (EC) No 2791/1999 and this Regulation.
4. The reports and messages shall be used only for the purposes stipulated in the scheme. Member States carrying out an inspection shall make the reports and messages available for inspection purposes and to inspectors assigned to the scheme only.
5. Member States carrying out an inspection:
(a) may retain and store reports and messages transmitted by the NEAFC secretariat within 24 hours of the departure of the vessels to which the data pertain from the regulatory area without re-entry. Departure is deemed to have been effected six hours after the transmission of the intention to exit from the regulatory area;
(b) shall ensure the secure processing of reports and messages in their respective electronic data-processing facilities, in particular where the processing involves transmission over a network. Member States must adopt appropriate technical and organisational measures to protect reports and messages against accidental or unlawful destruction or accidental loss, alteration, unauthorised disclosure or access and against all inappropriate forms of processing.
In view of the expertise acquired with respect to the secure and confidential treatment of electronic reports and messages and the cost of their implementation, such measures shall ensure a level of security appropriate to the risks represented by the processing of reports and messages.
6. The Member States and the Commission shall comply with the minimum security requirements set out in Annex IV.
7. For their main computer systems the Member States shall aim to meet the criteria set out in Annex V.
8. The X.400 protocol can be used for communication of data under the scheme. In this case, appropriate encryption protocols shall be applied to ensure confidentiality and authenticity.
9. Access limitation to the data shall be secured via a flexible user identification and password mechanism. Each user shall be given access only to the data necessary for his/her task.
CHAPTER IV
INSPECTION
Article 8
Identification of inspection services
The pennants or special flags referred to in Article 11(5) of Regulation (EC) No 2791/1999 shall comply with the models set out in Annex VI(A).
The special identity document referred to in Article 12 of Regulation (EC) No 2791/1999 shall be drawn up in accordance with the model in Annex VI(B).
Article 9
Inspection activities
The format of messages at the start and end of the activities of inspection vessels and aircraft referred to in Article 11(6) of Regulation (EC) No 2791/1999 shall be as set out in Annex VII.
Article 10
Observation report
Observation reports as referred to in Article 13 of Regulation (EC) No 2791/1999 shall be drawn up in accordance with the model in Annex VIII(A).
The format for transmission of the report shall be as set out in Annex VIII(B).
Article 11
Inspection report
Inspection reports as referred to in Article 14(1)(h) of Regulation (EC) No 2791/1999 shall be drawn up in accordance with the model in Annex IX.
Article 12
Follow-up in the case of serious infringements
The list of competent authorities referred to in Article 19 of Regulation (EC) No 2791/1999 authorised to receive information on serious infringements is set out in Annex X.
CHAPTER V
FINAL PROVISIONS
Article 13
General rules applicable to notifications to the Secretariat
1. Notifications sent to the NEAFC Secretariat in accordance with Articles 4, 6 and 10 of this Regulation shall comply with the general rules set out in Annex XI. Each transmission shall be given a serial number by the transmitting Member State.
2. The codes used in notifications shall be in conformity with the international codes set out in Annex XII.
Article 14
Entry into force
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Communities.
The provisions referred to in Articles 4 and 6 shall remain in force until 31 December 2000 or until the adoption by the Council, in accordance with Article 30 of Regulation (EC) No 2791/1999, of the necessary measures instituting a definitive regime.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 15 May 2000.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 337, 30.12.1999, p. 1.
(2) OJ L 261, 20.10.1993, p. 1.
(3) OJ L 358, 31.12.1998, p. 5.
(4) OJ L 171, 6.7.1994, p. 7.
(5) OJ L 202, 30.7.1997, p. 18.
(6) OJ L 266,1.10.1998, p. 27.
ANNEX I
REPORTING OF INFORMATION ON CATCHES FROM FISHING VESSELS
1. "ENTRY" report
>TABLE>
2. "EXIT" report
>TABLE>
3. "CATCH" report
>TABLE>
4. "TRANSHIPMENT" report
>TABLE>
ANNEX II
REPORTING OF AGGREGATE CATCH AND FISHING EFFORT
A. List of species subject to catch report
>TABLE>
B. "REPORT" concerning catches taken in the regulatory area
>TABLE>
C. "JURISDICTION": report concerning catches of regulated resources taken in areas under fisheries jurisdiction of the Contracting Parties
>TABLE>
ANNEX III
COMMUNICATION OF CATCHES, VMS MESSAGES AND HAIL REPORTS BY FISHING VESSELS
"POSITION" report
>TABLE>
ANNEX IV
Minimum security requirements to be met by the Member States and the Commission:
(a) System access control: The system has to withstand a break-in attempt from unauthorised persons
(b) Authenticity and data access control: The system has to be able to limit the access of authorised parties to a predefined set of data only
(c) Communication security: It shall be guaranteed that the reports and messages are securely communicated
(d) Data security: It is guaranteed that all reports and messages that enter the system are securely stored for the required time and that they will not be tampered with
(e) Security procedures: Security procedures shall be designed addressing access to the system (both hardware and software), system administration and maintenance, backup and general usage of the system.
ANNEX V
The criteria the Member States shall aim to meet for their main computer systems are those of a C2-level trusted system.
Minimum requirements to be met by the Member States and the Commission for a C2-level trusted system:
(a) A stringent password and authentication system. Each user of the system is assigned a unique user identification and associated password. Each time the user logs on to the system he/she has to provide the correct password. Even when successfully logged on the user only has access to those and only those functions and data that he/she is configured to have access to. Only a privileged user has access to all the data.
(b) Physical access to the computer system is controlled.
(c) Auditing; selective recording of events for analysis and detection of security breaches.
(d) Time-based access control; access to the system can be specified in terms of times of day and days of week that each user is allowed to log in to the system.
(e) Terminal access control; specifying for each workstation which users are allowed to access.
ANNEX VI
A. NEAFC inspection pennants
1. Pennants are to be shown during daylight hours, in conditions of normal visibility.
2. The boarding craft shall display one of the inspection pennants in the above conditions. This pennant may be half-scale. It may also be painted on the side or on any vertical surface of the boarding craft (there is no need to reproduce the letters "NE" in this case
>PIC FILE= "L_2000128EN.001602.EPS">
The distance between the pennants should not exceed one metre.
>PIC FILE= "L_2000128EN.001603.EPS">
B. Inspector's identity card
>PIC FILE= "L_2000128EN.001701.EPS">
>PIC FILE= "L_2000128EN.001702.EPS">
This card should be 10 x 7 cm in size and may be plasticised.
The colours of the NEAFC inspection pennant are shown in Annex VI(A).
The card number shall consist of the country's ISO-3 code followed by the four-figure ID number of the Contracting Party.
ANNEX VII
Notification of surveillance activities
A. Report of entry of surveillance craft in the regulatory area
>TABLE>
B. Report of exit of surveillance craft from the regulatory area
>TABLE>
ANNEX VIII
REPORT CONCERNING OBSERVATIONS OF VESSELS
A. Observation report
>PIC FILE= "L_2000128EN.002002.EPS">
B. Report concerning observation of vessels
>TABLE>
Positive identification can only be achieved by means of visual verification of the radio call sign or the external registration number displayed on the vessel.
If positive identification is not possible the reason shall be specified in the comments field.
ANNEX IX
INSPECTION REPORT
(Format as follows)
>PIC FILE= "L_2000128EN.002302.EPS">
>PIC FILE= "L_2000128EN.002401.EPS">
>PIC FILE= "L_2000128EN.002501.EPS">
>PIC FILE= "L_2000128EN.002601.EPS">
>PIC FILE= "L_2000128EN.002701.EPS">
>PIC FILE= "L_2000128EN.002801.EPS">
>PIC FILE= "L_2000128EN.002901.EPS">
>PIC FILE= "L_2000128EN.003001.EPS">
ANNEX X
LIST OF COMPETENT AUTHORITIES AUTHORISED TO RECEIVE INFORMATION ON SERIOUS INFRINGEMENTS
>TABLE>
ANNEX XI
DATA EXCHANGE FORMAT AND PROTOCOLS
A. Data transmission format
Each data transmission is structured as follows:
- double slash (//) and the characters "SR" indicate the start of a message;
- a double slash (//) and field code indicate the start of a data element;
- a single slash (/) separates the field code and the data;
- pairs of data are separated by space;
- the characters "ER" and a double slash (//) indicate the end of a record.
B. Data exchange protocols
Authorised data exchange protocols for electronic transmission of reports and messages between Contracting Parties and the Secretariat shall be X.25 and X.400.
C. Format for electronic exchange of fisheries monitoring, inspection and surveillance information (The North Atlantic Format)
>TABLE>
D.1. Structure of reports and messages as laid down in Annexes I, III and VII when forwarded by Member States to the NEAFC Secretariat
Where appropriate, each Member State retransmits to the NEAFC Secretariat data received from its vessels, in accordance with Articles 4, 6 and 10; subject to the following amendments:
- the address (AD) shall be replaced by the address of the Secretariat (XNE)
- the data elements "record date" (RD), "record time" (RT), "record number" (RN) and "from" (FR) shall be inserted
D.2. Return messages
A return message should be sent every time a report or message transmitted electronically is received by the Member States or the Secretariat.
Return message format:
>TABLE>
ANNEX XII
CODES TO USE IN ALL COMMUNICATIONS
A. Types of fishing gear
1. Main gear types
>TABLE>
2. Main categories of devices and attachments of gear
>TABLE>
B. Product form and type of packaging
1. Product form codes
>TABLE>
2. Type of packing
>TABLE>
C. Fishing vessel codes
1. Main vessel types
>TABLE>
2. Main vessel activities
>TABLE>
