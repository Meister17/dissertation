Commission Regulation (EC) No 1407/2006
of 25 September 2006
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 26 September 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 25 September 2006.
For the Commission
Jean-Luc Demarty
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 386/2005 (OJ L 62, 9.3.2005, p. 3).
--------------------------------------------------
ANNEX
to Commission Regulation of 25 September 2006 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code [1] | Standard import value |
07020000 | 052 | 70,2 |
096 | 43,7 |
999 | 57,0 |
07070005 | 052 | 80,1 |
999 | 80,1 |
07099070 | 052 | 87,2 |
999 | 87,2 |
08055010 | 052 | 70,1 |
388 | 62,5 |
524 | 53,8 |
528 | 53,6 |
999 | 60,0 |
08061010 | 052 | 77,3 |
400 | 166,0 |
624 | 112,6 |
999 | 118,6 |
08081080 | 388 | 93,2 |
400 | 96,0 |
508 | 80,0 |
512 | 89,4 |
528 | 74,1 |
720 | 80,0 |
800 | 165,4 |
804 | 91,7 |
999 | 96,2 |
08082050 | 052 | 116,3 |
388 | 86,7 |
720 | 74,4 |
999 | 92,5 |
08093010, 08093090 | 052 | 120,8 |
999 | 120,8 |
08094005 | 052 | 111,4 |
066 | 74,6 |
098 | 29,3 |
624 | 135,3 |
999 | 87,7 |
[1] Country nomenclature as fixed by Commission Regulation (EC) No 750/2005 (OJ L 126, 19.5.2005, p. 12). Code "999" stands for "of other origin".
--------------------------------------------------
