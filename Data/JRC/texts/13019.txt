Reference for a preliminary ruling from the Tribunale di Lecce — Sezione distaccata di Gallipoli — lodged on 14 April 2006 — Criminal proceedings against Aniello Gallo and Gianluca Damonte
Referring court
Tribunale di Lecce — Sezione distaccata di Gallipoli
Parties to the main proceedings
Aniello Gallo and Gianluca Damonte
Question referred
Is Article 4(4 bis) of Law No 401/89 incompatible — with resultant effects on the domestic legal system — with the principles laid down in Articles 43 and 49 of the EC Treaty on establishment and freedom to provide cross-border services, also in light of the conflict between the interpretations in the decisions of the European Court of Justice (in particular in the judgment in Case C-243/01 Gambelli and Others [2003] ECR I-13031) and decision No 23271/04 of the Suprema Corte di Cassazione a Sezioni Unite (United Chambers of the Supreme Court of Cassation)? In particular, clarification is sought as to the applicability of the criminal provisions set out in the charge brought against Aniello GALLO and Gianluca DAMONTE in the Italian State.
--------------------------------------------------
