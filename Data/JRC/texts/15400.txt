Judgment of the Court (Third Chamber) of 7 September 2006 (reference for a preliminary ruling from the Verwaltungsgerichtshof — Austria) — Heger Rudi GmbH v Finanzamt Graz-Stadt
(Case C-166/05) [1]
Referring court
Verwaltungsgerichtshof — Austria
Parties to the main proceedings
Applicant: Heger Rudi GmbH
Defendant: Finanzamt Graz-Stadt
Re:
Request for a preliminary ruling — Verwaltungsgerichtshof (Higher Administrative Court, Austria) — Interpretation of Article 9(2)(a) of Sixth Council Directive 77/388/EEC of 17 May 1977 on the harmonisation of the laws of the Member States relating to turnover taxes — Common system of value added tax: uniform basis of assessment (OJ 1977 L 145, p. 1) — Determination of place of supply for tax purposes — Concept of supply of services connected with immoveable property — Transmission for valuable consideration of the fishing rights over a particular part of a river
Operative part of the judgment
The transmission of the right to fish by means of a transfer of fishing permits for valuable consideration constitutes a supply of services connected with immovable property within the meaning of Article 9(2)(a) of Sixth Council Directive 77/388/EEC of 17 May 1977 on the harmonisation of the laws of the Member States relating to turnover taxes — Common system of value added tax: uniform basis of assessment.
[1] OJ C 143, 11.06.2005.
--------------------------------------------------
