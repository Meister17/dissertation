Commission Regulation (EC) No 323/2003
of 20 February 2003
amending Regulation (EC) No 896/2001 as regards the list of national authorities competent to apply the arrangements for importing bananas into the Community
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 404/93 of 13 February 1993 on the common organisation of the market in bananas(1), as last amended by Regulation (EC) No 2587/2001(2),
Having regard to Commission Regulation (EC) No 896/2001 of 7 May 2001 laying down detailed rules for applying Council Regulation (EC) No 404/93 as regards the arrangements for importing bananas into the Community(3), as last amended by Regulation (EC) No 349/2002(4), and in particular Article 5(4) thereof,
Whereas:
(1) The Annex to Regulation (EC) No 896/2001 lists the authorities competent in the Member States to apply the import arrangements. Following a notification from a Member State, the list of authorities must be amended.
(2) Regulation (EC) No 896/2001 has therefore to be amended,
HAS ADOPTED THIS REGULATION:
Article 1
The Annex to Regulation (EC) No 896/2001 is replaced by the text in the Annex hereto.
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 20 February 2003.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 47, 25.2.1993, p. 1.
(2) OJ L 345, 29.12.2001, p. 13.
(3) OJ L 126, 8.5.2001, p. 6.
(4) OJ L 55, 26.2.2002, p. 17.
ANNEX
"ANNEX
The competent authorities of the Member States:
Belgium
Bureau d'intervention et de restitution belge/Belgisch Interventie- en Restitutiebureau Rue de Trèves, 82/Trierstraat 82 B - 1040 Bruxelles/Brussel
Denmark
Ministeriet for Fødevarer, Landbrug og Fiskeri Direktoratet for Fødevareerhverv; Eksportstøttekontoret Kampmannsgade 3 DK - 1780 København V
Germany
Bundesanstalt für Landwirtschaft und Ernährung Referat 322 Adickesallee, 40 D - 60322 Frankfurt am Main
Greece
OPEKEPE (ex-GEDIDAGEP) Directorate Fruits and Vegetables, Wine and Industrial Products 241, Acharnon Street GR - 10446 Athens
Spain
Ministerio de Economía Secretaría General de Comercio Exterior Paseo de la Castellana, 162 E - 28046 Madrid
France
Office de développement de l'économie agricole des départements d'outre-mer (ODEADOM) 31, quai de Grenelle F - 75738 Paris Cedex 15
Ireland
Department of Agriculture and Rural Development Horticulture Division Agriculture House (7W)
Kildare Street
Dublin 2 Ireland
Italy
Ministero delle Attività Produttive DG Politica Commerciale e Gestione Regime Scambi - Div. II Viale Boston 25 I - 00144 Roma
Luxembourg
Ministère de l'agriculture/Administration des services techniques de l'agriculture Service de l'horticulture 16, route d'Esch Boîte postale 1904 L - 1014 Luxembourg
Netherlands
Produktschap Tuinbouw Louis Pasteurlaan 6 Postbus 280 2700 AG Zoetermeer Netherlands
Austria
Bundesministerium für Land- und Forstwirtschaft, Umwelt und Wasserwirtschaft Abteilung III 10 - Obst, Gemüse, Sonderkulturen Stubenring 1 A - 1012 Wien
Portugal
Ministério das Finanças Direcção-Geral das Alfândegas e dos Impostos Especiais sobre o Consumo
Direcção de Serviços de Licenciamento
Rua Terreiro do Trigo - Edifício da Alfândega P - 1149-060 Lisboa
Finland
Maa- ja Metsätalousministeriö PL 30 FIN - 00023 Valtioneuvosto, Helsinki
Sweden
Jordbruksverket Vallgatan 8-10 S - 551 82 Jönköping
United Kingdom
Rural Payments Agency External Trade Division Lancaster House
Hampshire Court
Newcastle Upon Tyne NE4 7YH United Kingdom"
