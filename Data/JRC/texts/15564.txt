Action brought on 7 February 2006 by the Commission of the European Communities against the Hellenic Republic
An action against the Hellenic Republic was brought before the Court of Justice of the European Communities on 7 February 2006 by the Commission of the European Communities, represented by Maria Kondou Durande and Carmel O'Reilly, Legal Advisers, acting as Agents, with an address for service in Luxembourg.
The Commission claims that the Court should:
1. declare that, by failing to adopt the laws, regulations and administrative provisions necessary to comply with Council Directive 2003/9/EC [1] of 27 January 2003 laying down minimum standards for the reception of asylum seekers and in any event by failing to inform the Commission thereof, the Hellenic Republic has failed to fulfil its obligations under Article 26 of that directive;
2. order the Hellenic Republic to pay the costs.
Pleas in law and main arguments
The period prescribed for transposing the directive into national law expired on 6 February 2005.
[1] OJ L 31 of 6.2.2003, p. 18.
--------------------------------------------------
