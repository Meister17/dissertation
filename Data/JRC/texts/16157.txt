Euro exchange rates [1]
26 April 2006
(2006/C 100/01)
| Currency | Exchange rate |
USD | US dollar | 1,2425 |
JPY | Japanese yen | 142,87 |
DKK | Danish krone | 7,4601 |
GBP | Pound sterling | 0,696 |
SEK | Swedish krona | 9,3135 |
CHF | Swiss franc | 1,5776 |
ISK | Iceland króna | 92,87 |
NOK | Norwegian krone | 7,797 |
BGN | Bulgarian lev | 1,9558 |
CYP | Cyprus pound | 0,5761 |
CZK | Czech koruna | 28,417 |
EEK | Estonian kroon | 15,6466 |
HUF | Hungarian forint | 265,78 |
LTL | Lithuanian litas | 3,4528 |
LVL | Latvian lats | 0,696 |
MTL | Maltese lira | 0,4293 |
PLN | Polish zloty | 3,8873 |
RON | Romanian leu | 3,4747 |
SIT | Slovenian tolar | 239,58 |
SKK | Slovak koruna | 37,26 |
TRY | Turkish lira | 1,6505 |
AUD | Australian dollar | 1,6623 |
CAD | Canadian dollar | 1,4058 |
HKD | Hong Kong dollar | 9,6355 |
NZD | New Zealand dollar | 1,9734 |
SGD | Singapore dollar | 1,974 |
KRW | South Korean won | 1176,09 |
ZAR | South African rand | 7,6541 |
CNY | Chinese yuan renminbi | 9,9673 |
HRK | Croatian kuna | 7,299 |
IDR | Indonesian rupiah | 10949,53 |
MYR | Malaysian ringgit | 4,5233 |
PHP | Philippine peso | 64,486 |
RUB | Russian rouble | 34,036 |
THB | Thai baht | 46,863 |
[1] Source: reference exchange rate published by the ECB.
--------------------------------------------------
