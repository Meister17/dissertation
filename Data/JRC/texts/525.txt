Council Decision
of 20 March 2000
designating a specific institute responsible for establishing the criteria necessary for standardising the serological tests to monitor the effectiveness of rabies vaccines
(2000/258/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 92/65/EEC of 13 July 1992 laying down animal health requirements governing trade in, and imports into, the Community of animals, semen, ova and embryos not subject to animal health requirements laid down in specific Community rules referred to in Annex A(1) to Directive 90/425/EEC(1), and in particular Article 10(6) thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) Directive 92/65/EEC provides for an alternative system to quarantine for the entry of certain domestic carnivores into the territory of certain Member States free from rabies. That system requires checks on the effectiveness of the vaccination of those animals by titration of antibodies.
(2) In order to guarantee an effective system of monitoring the laboratories which will carry out these analyses, it is appropriate to establish a system of Community approval of such laboratories.
(3) The approval of those laboratories should be coordinated by a Community reference laboratory for those matters.
(4) The Agence française de sécurité sanitaire des aliments de Nancy (French Food Safety Agency, Nancy) laboratory meets the conditions required for designation as Community reference laboratory for those matters.
(5) That reference laboratory may receive Community aid as provided for in Article 28 of Council Decision 90/424/EEC of 26 June 1990 on expenditure in the veterinary field(2).
(6) The measures necessary for the implementation of this Decision should be adopted in accordance with Council Decision 1999/468/EC of 28 June 1999 laying down the procedures for the exercise of implementing powers conferred on the Commission(3),
HAS ADOPTED THIS DECISION:
Article 1
The laboratory of the Agence française de sécurité sanitaire des aliments de Nancy (AFSSA, Nancy), the details of which are set out in Annex I, is hereby designated as the specific institute responsible for establishing the criteria necessary for standardising the serological tests to monitor the effectiveness of rabies vaccines.
Article 2
The duties of the laboratory referred to in Article 1 are set out in Annex II.
Article 3
The laboratory referred to in Article 1 shall send the Commission the list of Community laboratories to be authorised to carry out serological tests to monitor the effectiveness of rabies vaccines. These laboratories shall be authorised in accordance with Article 5(2).
Article 4
The Annexes to this Decision shall be amended in accordance with the procedure laid down in Article 5(2).
Article 5
1. The Commission shall be assisted by the Standing Veterinary Committee, hereinafter referred to as the "Committee", set up by Article 1 of Decision 68/361/EEC(4).
2. Where reference is made to this paragraph, Articles 5 and 7 of Decision 1999/468/EC shall apply.
The period provided for in Article 5(6) of Decision 1999/468/EC shall be three months.
3. The Committee shall adopt its rules of procedure.
Article 6
This Decision is addressed to the Member States.
Done at Brussels, 20 March 2000.
For the Council
The President
L. Capoulas Santos
(1) OJ L 268, 14.9.1992, p. 54. Directive as last amended by Commission Decision 95/176/EC (OJ L 117, 24.5.1995, p. 23).
(2) OJ L 224, 18.8.1990, p. 19. Decision at last amended by Regulation (EC) No 1258/1999 (OJ L 160, 26.6.1999, p. 103).
(3) OJ L 184, 17.7.1999, p. 23.
(4) OJ L 255, 18.10.1968, p. 23.
ANNEX I
AFSSA, Nancy Laboratoire d'études sur la rage et la pathologie des animaux sauvages Domaine de Pixérécourt BP 9 F - 54220 Malzéville Tel: (00-33) 383 29 89 50 Fax: (00-33) 383 29 89 59 E-mail: maubert@fitech.fr
ANNEX II
The specific institute responsible for establishing the criteria necessary for standardising the serological test to monitor the action of rabies vaccines shall:
- coordinate the establishment, improvement and standardisation of methods of serological titration on carnivores vaccinated against rabies,
- appraise those laboratories for which Member States have submitted an application for approval to perform the analyses refered to in the first indent; the result of this appraisal must be sent to the Comission for the purposes of such approval,
- draw up a list of approved Community laboratories authorised to carry out these analyses,
- provide any useful information on analysis methods and comparative trials to these laboratories and organise training sessions and further training courses for their staff,
- organise interlaboratory aptitude tests,
- collaborate with the laboratories responsible for carrying out these analyses in third countries and propose to the Commission, a procedure for approving those laboratories,
- provide scientific and technical assistance to the Commission and to Member States on these matters, in particular in cases of disagreement between Member States on analysis results.
