*****
COMMISSION REGULATION (EEC) No 1323/86
of 5 May 1986
amending Regulation (EEC) No 1351/72 on the recognition of producer groups for hops
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 1696/7 of 26 July 1971 on the common organization of the market in hops (1), as last amended by Council Regulation (EEC) No 3800/85 of 31 December 1985 (2), and in particular Article 7 (5) thereof,
Whereas Council Regulation (EEC) No 3332/85 of 26 November 1985 amending Regulation (EEC) No 1696/71 on the common organization of the market in hops (3) amended Article 7 of the latter in such a way as to permit producer groups to use aid to take not only measures leading to a greater concentration of supply and greater market stability by marketing the entire production of their members but also measures by means of which production can be improved and adapted to the requirements of the market; whereas Commission Regulation (EEC) No 1351/72 (4), containing the rules for the application of the said Article 7 should be adapted accordingly;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Hops,
HAS ADOPTED THIS REGULATION:
Article 1
In Article 1 (1) (b) (cc) the following is inserted after the words 'market stabilization measures':
'as also for measures to improve and adapt production to market requirements'.
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 5 May 1986.
For the Commission
Frans ANDRIESSEN
Vice-President
(1) OJ No L 175, 4. 8. 1971, p. 1.
(2) OJ No L 367, 31. 12. 1985, p. 32.
(3) OJ No L 318, 29. 11. 1985, p. 1.
(4) OJ No L 148, 30. 6. 1972, p. 13.
