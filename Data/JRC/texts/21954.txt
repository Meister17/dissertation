COUNCIL DIRECTIVE of 20 February 1978 on waste from the titanium dioxide industry (78/176/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Articles 100 and 235 thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament (1),
Having regard to the opinion of the Economic and Social Committee (2),
Whereas waste from the titanium dioxide industry is liable to be harmful to human health and the environment ; whereas it is therefore necessary to prevent and gradually reduce pollution caused by such waste with a view to eliminating it;
Whereas the 1973 (3) and 1977 (4) European Communities' Programmes of Action on the Environment refer to the need to undertake Community action against waste from the titanium dioxide industry;
Whereas any disparity between the provisions on waste from the titanium dioxide industry already applicable or in preparation in the various Member States may create unequal conditions of competition and thus directly affect the functioning of the common market ; whereas it is therefore necessary to approximate laws in this field, as provided for in Article 100 of the Treaty;
Whereas it seems necessary for this approximation of laws to be accompanied by Community action so that one of the aims of the Community in the sphere of protection of the environment and improvement of the quality of life can be achieved by more extensive rules ; whereas certain specific provisions to this effect should therefore be laid down ; whereas Article 235 of the Treaty should be invoked as the powers required for this purpose have not been provided for by the Treaty;
Whereas Directive 75/442/EEC (5), concerns waste disposal in general ; whereas for waste from the titanium dioxide industry it is advisable to lay down a special system which will ensure that human health and the environment are protected against the harmful effects caused by the uncontrolled discharge, dumping or tipping of such waste;
Whereas in order to attain these objectives there should be a system of prior authorization as regards the discharge, dumping, storage, tipping or injecting of waste ; whereas the issue of this authorization should be made subject to specific conditions;
Whereas discharge, dumping, storage, tipping and injecting of waste must be accompanied both by monitoring of the waste and monitoring and surveillance of the environment concerned;
Whereas for existing industrial establishments Member States must, by 1 July 1980, draw up programmes for the progressive reduction of pollution caused by such waste with a view to its elimination ; whereas these programmes must fix the general reduction targets to be attained by 1 July 1987 at the latest and indicate the measures to be taken for each establishment;
Whereas for new industrial establishments Member States must issue a prior authorization ; whereas such authorization must be preceded by an environmental impact study and may be granted only to firms which undertake to use only those materials, processes and techniques available on the market that are least damaging to the environment,
HAS ADOPTED THIS DIRECTIVE:
Article 1
1. The aim of this Directive is the prevention and progressive reduction, with a view to its elimination, of pollution caused by waste from the titanium dioxide industry. (1)OJ No C 28, 9.2.1976, p. 16. (2)OJ No C 131, 12.6.1976, p. 18. (3)OJ No C 112, 20.12.1973, p. 3. (4)OJ No C 139, 13.6.1977, p. 3. (5)OJ No L 194, 25.7.1975, p. 39.
2. For the purpose of this Directive: (a) "pollution" means the discharge by man, directly or indirectly, of any residue from the titanium dioxide manufacturing process into the environment, the results of which are such as to cause hazards to human health, harm to living resources and to ecosystems, damage to amenities or interference with other legitimate uses of the environment concerned;
(b) "waste" means: - any residue from the titanium dioxide manufacturing process of which the holder disposes or is obliged to dispose under current national legislation;
- any residue from a treatment process of a residue referred to in the first indent;
(c) "disposal" means: - the collection, sorting, transport and treatment of waste as well as its storage and tipping above ground or underground and its injection into the ground;
- the discharge thereof into surface water, ground water and the sea, and dumping at sea;
- the transformation operations necessary for its re-use, recovery or recycling;
(d) "existing industrial establishments" means those industrial establishments already set up on the date of notification of this Directive;
(e) "new industrial establishments" means those industrial establishments which are in the process of being set up on the date of entry into force of this Directive or which are set up after that date. Extensions to existing industrial establishments leading to an increase of 15 000 tonnes per year or more in the titanium dioxide on-site production capacity of the establishment concerned shall be treated as new industrial establishments.
Article 2
Member States shall take the necessary measures to ensure that waste is disposed of without endangering human health and without harming the environment, and in particular: - without risk to water, air, soil and plants and animals;
- without deleteriously affecting beauty-spots or the countryside.
Article 3
Member States shall take appropriate measures to encourage the prevention, recycling and processing of waste, the extraction of raw materials and any other process for the re-use of waste.
Article 4
1. The discharge, dumping, storage, tipping and injection of waste are prohibited unless prior authorization is issued by the competent authority of the Member State in whose territory the waste is produced. Prior authorization must also be issued by the competent authority of the Member State - in whose territory the waste is discharged, stored, tipped or injected;
- from whose territory it is discharged or dumped.
2. Authorization may be granted for a limited period only. It may be renewed.
Article 5
In the case of discharge or dumping, the competent authority may, in accordance with Article 2 and on the basis of the information supplied in accordance with Annex I, grant the authorization referred to in Article 4 provided that: (a) the waste cannot be disposed of by more appropriate means;
(b) an assessment carried out in the light of available scientific and technical knowledge shows that there will be no deleterious effect, either immediate or delayed, on the aquatic environment;
(c) there is no deleterious effect on boating, fishing, leisure activities, the extraction of raw materials, desalination, fish and shellfish breeding, on regions of special scientific importance or on other legitimate uses of the waters in question.
Article 6
In the case of storage, tipping or injection, the competent authority may, in accordance with Article 2, and on the basis of the information supplied in accordance with Annex I, grant the authorization referred to in Article 4, provided that: (a) the waste cannot be disposed of by more appropriate means;
(b) an assessment carried out in the light of available scientific and technical knowledge shows that there will be no detrimental effect, either immediate or delayed, on underground waters, the soil or the atmosphere;
(c) there is no deleterious effect on leisure activities, the extraction of raw materials, plants, animals, on regions of special scientific importance or on other legitimate uses of the environment in question.
Article 7
1. Irrespective of the method and extent of treatment of the waste in question, its discharge, dumping, storage, tipping and injection shall be accompanied by the monitoring referred to in Annex II of the waste and of the environment concerned having regard to its physical, chemical, biological and ecological aspects.
2. The monitoring operations shall be carried out periodically by one or more bodies appointed by the Member State the competent authority of which has issued the authorization provided for in Article 4. In the case of cross-frontier pollution between Member States, the body in question shall be appointed jointly by the parties concerned.
3. Within one year of notification of this Directive, the Commission shall submit to the Council a proposal on the procedures for the surveillance and monitoring of the environments concerned. The Council shall act on this proposal within six months of the publication of the opinion of the European Parliament and that of the Economic and Social Committee in the Official Journal of the European Communities.
Article 8
1. The competent authority in the Member State concerned shall take all appropriate steps to remedy one of the following situations and, if necessary, shall require the suspension of discharge, dumping, storage, tipping or injection operations: (a) if the results of the monitoring provided for in Annex II (A) (1) show that the conditions for the prior authorization referred to in Articles 4, 5 and 6 have not been fulfilled, or
(b) if the results of the acute toxicity tests referred to in Annex II (A) (2) show that the limits laid down therein have been exceeded, or
(c) if the results of the monitoring provided for in Annex II (B) reveal a deterioration in the environment concerned in the area under consideration, or
(d) if discharge or dumping produces a deleterious effect on boating, fishing, leisure activities, the extraction of raw materials, desalination, fish and shellfish breeding, on regions of special scientific importance or on other legitimate uses of the waters in question, or
(e) if storage, tipping or injection produces a deleterious effect on leisure activities, the extraction of raw materials, plants, animals, on regions of special scientific importance or on other legitimate uses of the environments in question.
2. If several Member States are concerned, the measures shall be taken after consultation.
Article 9
1. Member States shall draw up programmes for the progressive reduction and eventual elimination of pollution caused by waste from existing industrial establishments.
2. The programmes mentioned in paragraph 1 shall set general targets for the reduction of pollution from liquid, solid and gaseous waste, to be achieved by 1 July 1987 at the latest. The programmes shall also contain intermediate objectives. They shall, moreover, contain information on the state of the environment concerned, on measures for reducing pollution and on methods for treating waste that is directly caused by the manufacturing processes.
3. The programmes referred to in paragraph 1 shall be sent to the Commission by 1 July 1980 at the latest so that it may, within a period of six months after receipt of all the national programmes, submit suitable proposals to the Council for the harmonization of these programmes in regard to the reduction and eventual elimination of pollution and the improvement of the conditions of competition in the titanium dioxide industry. The Council shall act on these proposals within six months of the publication of the opinion of the European Parliament and that of the Economic and Social Committee in the Official Journal of the European Communities.
4. Member States shall introduce a programme by 1 January 1982 at the latest.
Article 10
1. The programmes referred to in Article 9 (1) must cover all existing industrial establishments and must set out the measures to be taken in respect of each of them.
2. Where, in particular circumstances, a Member State considers that, in the case of an individual establishment, no additional measures are necessary to fulfil the requirements of this Directive, it shall, within six months of notification of this Directive, provide the Commission with the evidence which has led it to that conclusion.
3. After conducting any independent verification of the evidence that may be necessary, the Commission may agree with the Member State that it is not necessary to take additional measures in respect of the individual establishment concerned. The Commission must give its agreement, with reasons, within six months.
4. If the Commission does not agree with the Member State, additional measures in respect of that establishment shall be included in the programme of the Member State concerned.
5. If the Commission does agree, its agreement will be periodically reviewed in the light of the results of the monitoring carried out pursuant to this Directive and in the light of any significant change in the manufacturing processes or in environmental policy objectives.
Article 11
New industrial establishments shall be subject to applications for prior authorization made to the competent authorities of the Member State on whose territory it is proposed to build the establishments. Such authorizations must be preceded by environmental impact surveys. They may be granted only to firms which give an undertaking to use only such of the materials, processes and techniques available on the market as are least damaging to the environment.
Article 12
Without prejudice to this Directive, Member States may adopt more stringent regulations.
Article 13
1. For the purposes of this Directive, Member States shall supply the Commission with all the necessary information relating to: - the authorizations issued pursuant to Articles 4, 5 and 6,
- the results of the monitoring of the environment concerned carried out pursuant to Article 7,
- the measures taken pursuant to Article 8.
They shall also supply the Commission with general information concerning the materials, processes and techniques notified to them pursuant to Article 11.
2. Information acquired as a result of the application of this Article may be used only for the purposes of this Directive.
3. The Commission and the competent authorities of the Member States, their officials and other servants shall not disclose information acquired by them pursuant to this Directive and of a kind covered by the obligation of professional secrecy.
4. Paragraphs 2 and 3 shall not prevent publication of general information or surveys which do not contain information relating to particular undertakings or associations of undertakings.
Article 14
Every three years the Member States shall prepare a report on the prevention and progressive reduction of pollution caused by waste from the titanium dioxide industry and shall forward it to the Commission, which shall communicate it to the other Member States.
The Commission shall report every three years to the Council and the European Parliament on the application of this Directive.
Article 15
1. Member States shall bring into force the measures needed to comply with this Directive within 12 months, of its notification and shall forthwith inform the Commission thereof.
2. Member States shall communicate to the Commission the texts of the national laws which they adopt in the field covered by this Directive.
Article 16
This Directive is addressed to the Member States.
Done at Brussels, 20 February 1978.
For the Council
The President
Per HÆKKERUP
ANNEX I PARTICULARS WHICH MUST BE SUPPLIED IN ORDER TO OBTAIN THE PRIOR AUTHORIZATION REFERRED TO IN ARTICLES 4, 5 AND 6
A. Characteristics and composition of the matter: 1. total amount and average compositions of matter dumped (e.g. per year);
2. form (e.g. solid, sludge, liquid or gaseous);
3. properties : physical (e.g. solubility and density), chemical and biochemical (e.g. oxygen demand) and biological;
4. toxicity;
5. persistence : physical, chemical and biological;
6. accumulation and biotransformation in biological materials or sediments;
7. susceptibility to physical, chemical and biochemical changes and interaction in the environment concerned with other organic and inorganic materials;
8. probability of production of taints or other changes reducing marketability of resources (fish, shellfish, etc.).
B. Characteristics of dumping or discharge site and methods of disposal: 1. location (e.g. coordinates of the dumping or discharge area, depth and distance from the coast), location in relation to other areas (e.g. amenity areas, spawning, nursery and fishing areas and exploitable resources);
2. rate of disposal per specific period (e.g. quantity per day, per week, per month);
3. methods of packaging and containment, if any;
4. initial dilution achieved by proposed method of release, particularly the speed of the ship;
5. dispersal characteristics (e.g. effects of currents, tides, and wind on horizontal transport and vertical mixing);
6. water characteristics (e.g. temperature, pH, salinity, stratification, oxygen indices of pollution - dissolved oxygen (DO), chemical oxygen demand (COD), biochemical oxygen demand (BOD), nitrogen present in organic and inorganic form, including ammonia, suspended matter, other nutrients and productivity);
7. bottom characteristics (e.g. topography, geochemical and geological characteristics and biological productivity);
8. existence and effects of other dumpings or discharges which have been made in the area concerned (e.g. heavy metal background reading and organic carbon content).
C. Characteristics of the tipping, storage or injection area and disposal methods: 1. geographical situation;
2. characteristics of adjacent areas;
3. methods of packaging and containment, if any;
4. characteristics of the methods of tipping, storage and injection, including an assessment of precautions taken to avoid the pollution of waters, the soil and the atmosphere.
ANNEX II SURVEILLANCE AND MONITORING OF DISPOSAL
A. Monitoring of waste
Disposal operations shall be accompanied by: 1. checks on the quantity, composition and toxicity of the waste to ensure that the conditions for prior authorization referred to in Articles 4, 5 and 6 are fulfilled;
2. tests for acute toxicity on certain species of molluscs, crustaceans, fish and plankton, preferably species commonly found in the discharge areas. In addition, tests shall be carried out on samples of the brine shrimp species (Artemia salina).
Over a period of 36 hours and at an effluent dilution of 1/5 000, these tests must not reveal: - more than 20 % mortality for adult forms of the species tested,
- and for larval forms, mortality exceeding that of a control group.
B. Surveillance and monitoring of the environment concerned I. In The case of discharge into fresh water or into the sea or in the case of dumping, such checks shall relate to the three following items : water column, living matter and sediments. Periodic checks on the state of the area affected by the discharges will make it possible to follow the development of the environments concerned.
Monitoring shall include the determination of: 1. pH;
2. dissolved oxygen;
3. turbidity;
4. hydrated iron oxides and hydroxides in suspension;
5. toxic metals in water, suspended solids, sediments and in accumulation in selected benthic and pelagic organisms;
6. the diversity and the relative and absolute abundance of flora and fauna.
II. In the case of storage, tipping or injection the monitoring shall include: 1. tests to ensure that surface waters and ground waters are not contaminated. These tests shall include the measurement of: - acidity,
- iron content (soluble and particulate),
- calcium content,
- toxic metal content (soluble and particulate) if any;
2. where necessary, tests to determine any adverse effects on the structure of the subsoils;
3. a general assessment of the ecology of the area in the vicinity of the tipping, storage or injection point.
