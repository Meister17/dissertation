Euro exchange rates [1]
7 August 2006
(2006/C 184/01)
| Currency | Exchange rate |
USD | US dollar | 1,2850 |
JPY | Japanese yen | 147,87 |
DKK | Danish krone | 7,4613 |
GBP | Pound sterling | 0,67400 |
SEK | Swedish krona | 9,1850 |
CHF | Swiss franc | 1,5720 |
ISK | Iceland króna | 90,40 |
NOK | Norwegian krone | 7,8785 |
BGN | Bulgarian lev | 1,9558 |
CYP | Cyprus pound | 0,5756 |
CZK | Czech koruna | 28,273 |
EEK | Estonian kroon | 15,6466 |
HUF | Hungarian forint | 270,30 |
LTL | Lithuanian litas | 3,4528 |
LVL | Latvian lats | 0,6960 |
MTL | Maltese lira | 0,4293 |
PLN | Polish zloty | 3,8717 |
RON | Romanian leu | 3,5144 |
SIT | Slovenian tolar | 239,67 |
SKK | Slovak koruna | 37,680 |
TRY | Turkish lira | 1,8839 |
AUD | Australian dollar | 1,6863 |
CAD | Canadian dollar | 1,4470 |
HKD | Hong Kong dollar | 9,9916 |
NZD | New Zealand dollar | 2,0623 |
SGD | Singapore dollar | 2,0231 |
KRW | South Korean won | 1236,94 |
ZAR | South African rand | 8,7467 |
CNY | Chinese yuan renminbi | 10,2400 |
HRK | Croatian kuna | 7,2780 |
IDR | Indonesian rupiah | 11654,95 |
MYR | Malaysian ringgit | 4,706 |
PHP | Philippine peso | 66,068 |
RUB | Russian rouble | 34,3230 |
THB | Thai baht | 48,496 |
[1] Source: reference exchange rate published by the ECB.
--------------------------------------------------
