Commission Regulation (EC) No 806/2006
of 31 May 2006
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 1 June 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 31 May 2006.
For the Commission
J. L. Demarty
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 386/2005 (OJ L 62, 9.3.2005, p. 3).
--------------------------------------------------
ANNEX
to Commission Regulation of 31 May 2006 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code [1] | Standard import value |
07020000 | 052 | 83,2 |
204 | 55,6 |
999 | 69,4 |
07070005 | 052 | 77,5 |
999 | 77,5 |
07099070 | 052 | 91,2 |
999 | 91,2 |
08055010 | 388 | 54,9 |
508 | 52,4 |
528 | 50,1 |
999 | 52,5 |
08081080 | 388 | 88,5 |
400 | 127,7 |
404 | 101,0 |
508 | 74,8 |
512 | 81,2 |
524 | 88,5 |
528 | 89,3 |
720 | 92,5 |
804 | 103,0 |
999 | 94,1 |
08092095 | 052 | 227,5 |
999 | 227,5 |
[1] Country nomenclature as fixed by Commission Regulation (EC) No 750/2005 (OJ L 126, 19.5.2005, p. 12). Code "999" stands for "of other origin".
--------------------------------------------------
