Commission Decision
of 20 October 2004
on the aid scheme "Invest Northern Ireland Venture 2003" which the United Kingdom is planning to implement for SMEs in Northern Ireland
(notified under document number C(2004) 3917)
(Only the English text is authentic)
(Text with EEA relevance)
(2005/644/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community, and in particular the first subparagraph of Article 88(2) thereof,
Having regard to the Agreement on the European Economic Area, and in particular Article 62(1)(a) thereof,
Having called on interested parties to submit their comments pursuant to those provisions [1] and having regard to their comments,
Whereas:
I. PROCEDURE
(1) By letter dated 20 March 2003, registered at the Commission on 26 March 2003, the United Kingdom authorities notified the Invest Northern Ireland Venture 2003 scheme to the Commission.
(2) By letter D/53203 dated 15 May 2003 and by letter D/55504 dated 29 August 2003, the Commission requested further information concerning the notified measure.
(3) By letter dated 24 June 2003 and by letter dated 30 September 2003, registered at the Commission on 1 July 2003 and 1 October 2003 respectively, the United Kingdom authorities submitted the information requested.
(4) By letter dated 26 November 2003, the Commission informed the United Kingdom that it had decided to initiate the procedure laid down in Article 88(2) of the EC Treaty in respect of the measure.
(5) The Commission’s decision to initiate the procedure was published in the Official Journal of the European Union [2]. The Commission called on interested parties to submit their comments.
(6) By letter dated 6 January 2004, registered at the Commission on 9 January 2004, the United Kingdom submitted a response to the Commission’s decision to initiate the procedure.
(7) By letters dated 25 February 2004, 27 February 2004, 1 March 2004, 2 March 2004, 3 March 2004, 4 March 2004, and 5 March 2004, registered at the Commission on 2 March 2004, 3 March 2004, 4 March 2004, 8 March 2004, and 10 March 2004, the Commission received observations from 11 interested parties.
(8) By letter D/52015 dated 18 March 2004, the Commission forwarded those comments to the United Kingdom, which was given the opportunity to react.
(9) The United Kingdom’s opinion on the third parties’ comments was received by letter dated 29 April 2004, registered at the Commission on 3 May 2004.
(10) By letter dated 23 June 2004, registered at the Commission on 24 June 2004, the United Kingdom submitted additional information concerning the notified measure.
II. DETAILED DESCRIPTION OF THE MEASURE
1. Objective of the measure
(11) The measure is intended to provide risk capital funding for small and medium-sized enterprises [3] (SMEs) in Northern Ireland by establishing a new venture capital fund.
(12) The Invest Northern Ireland Venture 2003 Fund (hereafter referred to as the fund) is intended to address the particular financing difficulties encountered by SMEs in Northern Ireland.
(13) On 4 February 2003 the Commission approved the Small and Medium Enterprises Venture Capital and Loan Fund [4], an umbrella scheme for all regions in the United Kingdom, including Northern Ireland.
(14) The Small and Medium Enterprises Venture Capital and Loan Fund regulates the establishment of venture capital funds specialising in risk capital funding for SMEs across the United Kingdom and in Gibraltar.
(15) In its decision of 4 February 2003, the Commission approved State-aided venture capital in certain economic areas qualifying for assistance pursuant to Article 87(3)(c) of the EC Treaty up to a maximum of EUR 750000 in a single tranche.
(16) As Northern Ireland is currently classified as an area qualifying for assistance under Article 87(3)(c) of the EC Treaty [5], investments to SMEs in Northern Ireland under the Small and Medium Enterprises Venture Capital and Loan Fund umbrella scheme are restricted to EUR 750000 in a single tranche.
(17) Given the exceptional challenges encountered by Northern Ireland, the United Kingdom authorities, by means of the notified measure, intend to provide risk capital finance of up to GBP 1500000 (EUR 2,2 million) in a single tranche to SMEs in Northern Ireland, thereby exceeding the maximum investment sizes per tranche as approved in the decision concerning the Small and Medium Enterprises Venture Capital and Loan Fund.
(18) All other material aspects of the notified measure respect the conditions laid down in the decision concerning the Small and Medium Enterprises Venture Capital and Loan Fund.
2. Description of the measure
Legal basis of the scheme
(19) The legal basis of the scheme is Article 7 of the Industrial Development (Northern Ireland) Order 1982.
Budget of the scheme
(20) The United Kingdom proposes to establish a GBP 15 million (EUR 22 million) to GBP 20 million (EUR 29 million) venture capital fund.
(21) If less than GBP 20 million (EUR 29 million) is raised, the government contribution will reduce pro rata.
(22) If more than GBP 20 million (EUR 29 million) is raised, the fund will accept the additional investments from private sector investors. The government funding will not be increased to a corresponding level.
(23) Under no circumstances will the public contribution to the fund exceed 50 % of the total fund volume.
Duration of the scheme
(24) The fund will be newly set up as a 10-year limited partnership that can be extended with the consent of all investors to a maximum of 12 years to facilitate successful exits from the fund.
Beneficiaries of the scheme
(25) The scheme will only apply to small and medium-sized enterprises in Northern Ireland.
(26) Firms in difficulties are excluded under the scheme.
(27) Funding will not be provided to businesses in sectors suffering from overcapacity, including shipbuilding, and European Coal and Steel Community sectors.
Size of investments
(28) The fund will make investments in beneficiary SMEs in the range of GBP 250000 (EUR 367000) to GBP 1,5 million (EUR 2,2 million). On occasions, follow-on investments may be made for a further round of funding. These decisions will be independent of previous investment decisions and will be based on the performance of the recipient SME.
Cumulation of aid under the scheme
(29) The United Kingdom authorities have stated that any SMEs receiving risk capital funding under the scheme would, for the duration of the investment, have any further eligible regional aid and SME aid reduced by 30 % of the aid intensity that would otherwise have been found compatible by the Commission.
Grounds for initiating the procedure
(30) The Commission Communication on State aid and risk capital (hereafter referred to as the Communication) [6] recognises a role for public funding of risk capital measures limited to addressing identifiable market failures.
(31) The Communication states that specific factors adversely affecting the access of SMEs to capital, such as imperfect or asymmetric information or high transaction costs can cause a market failure that would justify State aid.
(32) The Communication further states that there is no general risk capital market failure in the Community, but rather market gaps for some types of investments at certain stages of enterprises’ lives as well as particular difficulties in regions qualifying for assistance under Article 87(3)(a) and (c) of the EC Treaty (assisted areas).
(33) The Communication goes on to explain that in general, the Commission will require provision of evidence of market failure before being prepared to authorise risk capital measures.
(34) The Commission may however be prepared to accept the existence of market failure without further provision of evidence in cases where each tranche of finance for an enterprise from risk capital measures which are wholly or partially financed through State aid will comprise a maximum of EUR 500000 in non-assisted areas, EUR 750000 in areas qualifying for assistance under Article 87(3)(c) of the Treaty, or EUR 1 million in areas qualifying for assistance under Article 87(3)(a) of the Treaty.
(35) It follows that, for those cases where those amounts are exceeded, the Commission will require a demonstration of market failure justifying the proposed risk capital measure before assessing the compatibility of the measure in accordance with the positive and negative criteria listed under point VIII.3 of the Communication.
(36) The Invest Northern Ireland Venture 2003 scheme proposed by the United Kingdom foresees risk capital investments in the range of GBP 250000 (EUR 367000) to GBP 1,5 million (EUR 2,2 million) per investment tranche for SMEs in Northern Ireland.
(37) According to the Regional Aid Map 2000-2006 for the United Kingdom, Northern Ireland is currently classified as an area qualifying for assistance pursuant to Article 87(3)(c) of the EC Treaty.
(38) In line with the provisions of the Communication, the Commission would hence be prepared to accept the existence of market failure without further provision of evidence if risk capital funding for SMEs in Northern Ireland wholly or partially financed through State aid would be limited to the maximum amount of EUR 750000 as foreseen for assisted areas pursuant to Article 87(3)(c) of the EC Treaty under point VI.5 of the Communication.
(39) In the notification, the United Kingdom has clarified that with the exception of the proposed maximum tranche size of GBP 1,5 million (EUR 2,2 million), all other material aspects of the proposed measure will be in line with the already approved Small and Medium Enterprises Venture Capital and Loan Fund.
(40) The Commission therefore considers that its decision on the Small and Medium Enterprises Venture Capital and Loan Fund covers all those investments proposed under the Invest Northern Ireland Venture 2003 scheme ranging from GBP 250000 (EUR 367000) to GBP 510000 (EUR 750000) per single investment tranche.
(41) In line with the provisions of the Communication, risk capital investments proposed under the Invest Northern Ireland Venture 2003 scheme ranging from GBP 510000 (EUR 750000) to GBP 1,5 million (EUR 2,2 million) would necessitate the provision of evidence of market failure by the United Kingdom.
(42) In order to demonstrate the existence of market failure, the United Kingdom referred to several distinctive particularities of the venture capital market in Northern Ireland as underlined by a study [7]. The main arguments brought forward were:
(a) there is a gap in the provision of venture capital for SMEs in Northern Ireland in the deal size range of GBP 250000 (EUR 367000) to GBP 1,5 million (EUR 2,2 million);
(b) whilst such a market failure also exists in other regional economies, it is more pronounced in Northern Ireland for the following reasons:
(i) the venture capital market in Northern Ireland lags many years behind the rest of the United Kingdom in terms of total investment, number of deals, number of funds and availability of fund management skills;
(ii) the size of the Northern Ireland market, the fact that it is physically separated from the rest of the United Kingdom, and the long lasting impact of civil unrest in Northern Ireland have resulted in an absence of any wholly private sector venture capital funds, either locally based or investing in Northern Ireland SMEs from mainland United Kingdom;
(iii) in addition, it is difficult to attract experienced fund managers to Northern Ireland.
(43) By letter dated 26 November 2003, the Commission informed the United Kingdom of its decision to initiate the procedure laid down in Article 88(2) of the EC Treaty in respect of the Invest Northern Ireland Venture 2003 scheme.
(44) In its letter, the Commission stated that it had doubts as to whether the arguments presented by the United Kingdom in support of the existence of market failure could sufficiently justify the granting of risk capital investment tranches considerably exceeding the maximum amounts foreseen by the Communication for areas qualifying for assistance pursuant to Article 87(3)(c) of the EC Treaty.
(45) The Commission went on to explain that it considered a more thorough analysis of the issue to be necessary. Such an analysis would need to include any observations made by interested parties. Only after consideration of third party comments could the Commission decide whether the measure proposed by the United Kingdom affects trading conditions to an extent contrary to the common interest.
III. COMMENTS FROM INTERESTED PARTIES
(46) In response to the publication of its decision to open the formal procedure in the Official Journal of the European Union, the Commission received observations from the following interested parties:
- Enterprise Equity (NI) Limited,
- Momentum Northern Ireland,
- Qubis Limited,
- Ulster Farmers’ Union,
- CBI Northern Ireland,
- Institute of Directors Northern Ireland,
- Investment Belfast Limited,
- Inter Trade Ireland,
- BDO Stoy Hayward,
- The Ulster Society of Chartered Accountants,
- International Fund for Ireland.
(47) All comments received were positive and underlined the importance of the measure as well as the appropriateness of the proposed maximum investment amounts.
(48) The arguments put forward by the interested parties highlight the difficulty for local companies in Northern Ireland to raise equity funding of GBP 1 million (EUR 1,5 million) to GBP 1,5 million (EUR 2,2 million). This is mainly a result of the following factors:
(a) venture funds in the rest of the United Kingdom are increasing minimum investment levels and this is also being mirrored in Northern Ireland;
(b) the peripherality of Northern Ireland adversely influences funds in terms of cost;
(c) funds from the rest of the United Kingdom and the Republic of Ireland are largely uninterested in investments of this deal size and there is no local Northern Ireland fund capable of undertaking such investments;
(d) a general misplaced negative perception due to civil unrest in the past that continues to adversely affect the attitude of investors when considering Northern Ireland as an investment location.
IV. COMMENTS FROM THE UNITED KINGDOM
(49) The United Kingdom’s comments on the Commission’s decision to open the formal procedure laid down in Article 88(2) of the EC Treaty, as well as on the observations from third parties will be summarised in the following recitals.
(50) There are a number of general economic weaknesses that must be addressed in order to improve the competitive position of Northern Ireland:
(a) Northern Ireland’s Gross Domestic Product (GDP) per capita is below the United Kingdom average (77,5 % of United Kingdom average). The gap has only narrowed marginally over the course of the 1990s (three percentage points since 1989);
(b) whole economy productivity as measured by GDP per employee in Northern Ireland is below that of the United Kingdom (89 % of United Kingdom average);
(c) high dependency on small businesses: 85000 businesses in total, of which 99 % employ fewer than 50 people, and 93 % employ fewer than 10 people;
(d) manufacturing remains dependent on low value-added "traditional sectors", the high value added business services sector is only half the size of its United Kingdom equivalent;
(e) a small number of knowledge-based businesses compared with other United Kingdom regions;
(f) low levels of innovation and R&D activity;
(g) one of the lowest start-up rates of any United Kingdom region;
(h) shortage of debt and equity finance available for start-up companies.
(51) In addition to the general economic weaknesses, the venture capital market in Northern Ireland is underdeveloped compared to other United Kingdom regions:
(a) the level of venture capital investment in Northern Ireland over the period 1985-2002 represented only 0,7 % of the United Kingdom’s total venture capital investment as compared with Northern Ireland’s share of United Kingdom GDP of 2,2 %;
(b) venture capital activity in Northern Ireland would have to increase by four times its current level to match the per capita levels of other United Kingdom regions such as Wales or Scotland.
(52) There are a number of general factors that are related to this phenomenon:
(a) peripheral location: Northern Ireland is significantly disadvantaged by its geographical location;
(b) growth of indigenous business: Growth in the local economy has been severely limited over the last years due to civil unrest;
(c) grant culture: This has arisen due to the government’s need to intervene in Northern Ireland industrial development activities at a much higher level than elsewhere in the United Kingdom. Accordingly, businesses have not been disposed to consider taking in new equity.
(53) While some funds have been established in recent years, much more needs to be done, especially in generating adequate deal flow, but also in broadening the range and quantity of investors:
(a) Northern Ireland is poorly represented for venture capital and has only a small number of locally based funds which are mainly focused on small deal sizes;
(b) only three venture capital fund managers maintain a full-time presence in the region;
(c) the majority of venture capital funds available offer investment of below GBP 500000 (EUR 730000);
(d) studies suggest that the average size of early stage funding for technology businesses increases as the market matures. For Northern Ireland, the equity gap for first round funding within the next five years is expected to be in the range of GBP 250000 (EUR 367000) to GBP 1,5 million (EUR 2,2 million);
(e) in recognition of the increased scale of projects and predicted increases in demand, two or more commercial-sized funds of around GBP 15 million (EUR 22 million) each should be established in Northern Ireland over the next five years, one of which should be a government assisted fund.
(54) The United Kingdom had no further comments to make on the responses from third parties except to highlight the strong support expressed by those third parties for the establishment of the proposed fund.
V. ASSESSMENT OF THE MEASURE
(55) The Commission has examined the scheme in the light of Article 87 of the EC Treaty and in particular on the basis of the Communication. The results of this assessment are summarised in recitals 56 and following.
1. Legality
(56) By notifying the scheme, the United Kingdom authorities respected their obligations under Article 88(3) of the EC Treaty.
2. Existence of State aid
(57) The United Kingdom confirms that, with the exception of the allowable maximum amount of risk capital funding per single tranche, all other material aspects of the Invest Northern Ireland Venture 2003 scheme are in line with the Commission’s decision concerning the Small and Medium Enterprises Venture Capital and Loan Fund.
(58) The Commission’s assessment of the existence of State aid in this case, therefore follows the assessment made in that decision.
(59) In its decision concerning the Small and Medium Enterprises Venture Capital and Loan Fund, the Commission stated that, in line with point IV.2 of the Communication, the assessment of the presence of State aid would need to take into account the possibility that a risk capital measure may confer aid at different levels.
(60) The Commission went on to conclude that the Small and Medium Enterprises Venture Capital and Loan Fund involved State aid within the meaning of Article 87(1) of the EC Treaty at the level of the investors and at the level of the beneficiary SMEs. The Commission also considered that State aid within the meaning of Article 87(1) of the EC Treaty was not present at the level of the fund, or for unsecured loans granted to SMEs at the prevailing reference rate plus 4 percentage points or more, or for secured loans at the prevailing reference rate.
(61) That assessment is still valid for the assessment of the notified measure in this Decision.
3. Evidence of market failure
(62) In line with the provisions of the Communication, the Commission is prepared to accept the existence of market failure without further provision of evidence if risk capital funding wholly or partially financed through State aid for SMEs in areas qualifying for assistance pursuant to Article 87(3)(c) of the EC Treaty is limited to the maximum amount of EUR 750000 as foreseen under point VI.5 of the Communication.
(63) The measure proposed by the United Kingdom foresees risk capital investments in the range of GBP 250000 (EUR 367000) to GBP 1,5 million (EUR 2,2 million) per investment tranche for SMEs in Northern Ireland.
(64) According to the Regional Aid Map 2000-2006 for the United Kingdom, Northern Ireland is currently classified as an area qualifying for assistance pursuant to Article 87(3)(c) of the EC Treaty. However, according to the Regional Aid Map 2000-2006 for the United Kingdom, Northern Ireland is considered to be an "atypical" Article 87(3)(c) region with a corresponding regional aid ceiling of 40 %, generally reserved for regions qualifying for assistance under Article 87(3)(a).
(65) In line with the provisions of the Communication, the Commission has informed the United Kingdom that in view of the fact that the proposed risk capital investments under the notified scheme exceed the EUR 750000 threshold foreseen for areas qualifying for assistance pursuant to Article 87(3)(c), the United Kingdom would have to provide evidence of market failure.
(66) In order to demonstrate the existence of market failure for risk capital investments exceeding EUR 750000 for SMEs in Northern Ireland, the United Kingdom presented arguments indicating that the venture capital market in Northern Ireland is characterised by unique particularities distinguishing it from other regions in the United Kingdom.
(67) The arguments put forward by the United Kingdom were supported by a study underlining that there exists a gap in the provision of private venture capital for SMEs in Northern Ireland in the deal size range of GBP 250000 (EUR 367000) to GBP 1,5 million (EUR 2,2 million).
(68) Although such a market gap also occurs in other regional economies, the United Kingdom submitted that it is more pronounced in Northern Ireland. This has been demonstrated by the study submitted by the United Kingdom.
(69) According to that study, venture capital activity in Northern Ireland, in terms of the number of deals and the value of the deals, varies significantly from other United Kingdom regions such as Scotland and the North West and Merseyside region. Whereas the number of deals for early stage companies in Scotland for the period 2000-2002 was 9,9 % of the United Kingdom total, and in the North West and Merseyside region was 8,1 % of the United Kingdom total, Northern Ireland only accounted for 4,4 % of the United Kingdom total. For companies in the expansion stage, the respective figures were 10,2 % of the United Kingdom total in Scotland, 7,9 % of the United Kingdom total in the North West and Merseyside region, and only 2,4 % of the United Kingdom total in Northern Ireland.
(70) With regard to the value of the deals in the early stage and expansion phases, the study shows that for early stage investments, Scotland represented 8,1 % of the United Kingdom total for the period 2000-2002, the North West and Merseyside region 5,7 %, and Northern Ireland 2,2 % of the United Kingdom total. The difference is even more accentuated if one looks at the value of deals for companies in the expansion stage. There, Scotland represents 9,5 % of the United Kingdom total, the North West and Merseyside region 21,2 % of the United Kingdom total, and Northern Ireland only 0,7 %.
(71) This fact is further underlined by data in the study comparing the average deal sizes for the period 2000-2002. Here the United Kingdom average for early stage investments is GBP 1,14 million compared to GBP 0,93 million in Scotland, GBP 0,81 million for the North West and Merseyside region, and GBP 0,36 million for Northern Ireland. For SMEs in the expansion stage, the difference is once again much more pronounced. Whereas the average for the United Kingdom is GBP 2,82 million, GBP 2,63 million for Scotland, and GBP 7,62 million for the North West and Merseyside region, it reaches only GBP 0,86 million in Northern Ireland.
(72) These figures underlining the relative weakness of the venture capital market in Northern Ireland — according to the study Northern Ireland generally accounts for only 0,7 % of total investments in the United Kingdom although it represents a 2,2 % share of the United Kingdom GDP — especially for SMEs in the expansion phase, can be explained by looking at the venture capital funds currently active in Northern Ireland. Out of eight existing and active venture capital funds, only one offers financing for deal sizes ranging from GBP 250000 to GBP 1,5 million, but has already reached the end of its investment period. All other funds only offer financing for deals below GBP 250000.
(73) The study submitted to the Commission by the United Kingdom has argued that this gap for the provision of equity financing in the deal size range between GBP 250000 and GBP 1,5 million is clearly related to the general socioeconomic characteristics of Northern Ireland as described under recitals 50 and following. These particularities of Northern Ireland, namely its peripherality and the legacy of civil unrest, have aggravated the two sources of market failure — imperfect or asymmetric information and high transaction costs — as described by the Communication.
(74) In its decision to initiate the procedure laid down in Article 88(2) of the EC Treaty in respect of the proposed aid measure, the Commission stated that in view of the maximum investment amounts proposed under the scheme, which considerably exceed the maximum investment amounts foreseen by the Communication, it was necessary to seek observations from interested parties in order to decide whether the measure affects trading conditions to an extent contrary to the common interest.
(75) All comments received from interested third parties were positive and underlined the importance of the measure in general as well as the appropriateness of the proposed maximum investment amounts.
(76) Taking into account the information presented in the initial notification, the comments submitted by interested third parties as well as the additional information delivered by the United Kingdom following the Commission’s decision to open the procedure laid down in Article 88(2) of the EC Treaty, it is concluded that the United Kingdom has provided sufficient evidence of the existence of market failure in the venture capital market in Northern Ireland.
4. Compatibility of the measure
(77) The United Kingdom has confirmed that, with the exception of the allowable maximum amount of risk capital funding per single tranche, all other material aspects of the notified scheme are in line with the Commission’s decision concerning the Small and Medium Enterprises Venture Capital and Loan Fund.
(78) The assessment of the compatibility of the notified State aid therefore follows the assessment made in the decision concerning the Small and Medium Enterprises Venture Capital and Loan Fund. In its decision in that case, the Commission concluded that all positive elements as outlined under point VIII.3 of the Communication were fulfilled. This is also true in respect of the Invest Northern Ireland Venture 2003 fund.
(79) The following elements are regarded as positive elements:
(a) restriction of investments to SMEs in their start up and early stages and/or SMEs seeking to expand or diversify;
(b) focus on risk capital market failure: delivery of finance to SMEs principally in the form of equity or quasi-equity;
(c) profit-driven nature of the investment decisions: Market economy investors will provide at least 40 % of the capital of the fund. Professional fund managers will take all investment decisions and their remuneration will be directly linked to the performance of the fund;
(d) minimisation of the level of distortion between investors and investment funds: The investors will be chosen by competitive tender. Private investors will be given notice of the opportunity to invest by virtue of advertising. No person or organisation will be debarred from investing in the fund. Investors will be chosen by selecting the least preferential terms necessary in order to induce the private sector to invest. In order to prevent overcompensation to investors, the benefits will be split between the public and private investors according to their respective level of investment;
(e) sectoral focus: sensitive sectors are excluded;
(f) investment on the basis of business plans: All investments will be made on the basis of robust business plans together with a variety of other standard commercial tests to ensure the viability of the project and the expected commercial return;
(g) avoidance of cumulation of aid measures to a single enterprise: The United Kingdom authorities have stated that any SMEs receiving risk capital funding under the scheme would, for the duration of the investment, have any further eligible regional aid and SME aid reduced by 30 % of the aid intensity that would otherwise have been found compatible by the Commission.
(80) It is therefore concluded that all positive aspects as required by the Communication are present in respect of the notified scheme.
VI. CONCLUSION
(81) It is therefore concluded that the Invest Northern Ireland Venture 2003 scheme fulfils the conditions set out in the Communication. The notified measure should therefore be declared compatible with the common market pursuant to Article 87(3)(c) of the EC Treaty as aid to facilitate the development of certain economic activities or of certain economic areas without adversely affecting trading conditions to an extent contrary to the common interest,
HAS ADOPTED THIS DECISION:
Article 1
The State aid which the United Kingdom is planning to implement under the Invest Northern Ireland Venture 2003 scheme is compatible with the common market within the meaning of Article 87(3)(c) of the Treaty. Implementation of the aid is accordingly authorised.
Article 2
The United Kingdom shall submit an annual report on the implementation of the aid.
Article 3
This Decision is addressed to the United Kingdom of Great Britain and Northern Ireland.
Done at Brussels, 20 October 2004.
For the Commission
Mario Monti
Member of the Commission
[1] OJ C 33, 6.2.2004, p. 2.
[2] See footnote 1.
[3] The definition of small and medium-sized enterprises applied by the United Kingdom authorities for the purposes of the scheme is always in line with the definition given in Annex I to Commission Regulation (EC) No 70/2001 of 12 January 2001 on the application of Articles 87 and 88 of the EC Treaty to State aid to small and medium-sized enterprises (OJ L 10, 13.1.2001, p. 33). Regulation as amended by Regulation (EC) No 364/2004 (OJ L 63, 28.2.2004, p. 22).
[4] State Aid N 620/2002 — United Kingdom: "Small and Medium Enterprises Venture Capital and Loan Fund". Commission Decision of 4 February 2003 (C/2003/110).
[5] State Aid N 265/2000 — United Kingdom: "Regional Aid Map 2000-2006" (OJ C 272, 23.9.2000, p. 43).
[6] OJ C 235, 21.8.2001, p. 3.
[7] "Market Failure in the Supply of Venture Capital Funds for SMEs in Northern Ireland". Invest NI, October 2002.
--------------------------------------------------
