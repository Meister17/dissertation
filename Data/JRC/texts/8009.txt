*****
COUNCIL DIRECTIVE
of 8 November 1990
amending, particularly as regards motor vehicle liability insurance, Directive 73/239/EEC and Directive 88/357/EEC which concern the coordination of laws, regulations and administrative provisions relating to direct insurance other than life assurance
( 90/618/EEC )
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Articles 57 ( 2 ) and 66 thereof,
Having regard to the proposal from the Commission ( 1 ),
In cooperation with the European Parliament ( 2 ),
Having regard to the opinion of the Economic and Social Committee ( 3 ),
Whereas in order to develop the internal insurance market, the Council adopted on 24 July 1973 Directive 73/239/EEC on the coordination of laws, regulations and administrative provisions relating to the taking-up and pursuit of the business of direct insurance other than life assurance ( 4 ) ( also referred to as the "First Directive ') and on 22 June 1988 Directive 88/357/EEC on the coordination of laws, regulations and administrative provisions relating to direct insurance other than life assurance and laying down provisions to facilitate the effective exercise of freedom to provide services and amending Directive 73/239/EEC ( 5 ) ( also referred to as the "Second Directive ');
Whereas Directive 88/357/EEC made it easier for insurance undertakings having their head office in the Community to provide services in the Member States, thus making it possible for policyholders to have recourse not only to insurers established in their own country, but also to insurers who have their head office in the Community and are established in other Member States;
Whereas the scope of the provisions of Directive 88/357/EEC specifically concerning freedom to provide services excluded certain risks, the application to which of the said provisions was rendered inappropriate at that stage by the specific rules adopted by the Member States' authorities, owing to the nature and social implications of such provisions; whereas those exclusions were to be re-examined after that Directive had been implemented for a certain period;
Whereas one of the exclusions concerned motor vehicle liability insurance, other than carrier's liability;
Whereas, however, when the abovementioned Directive was adopted the Commission gave an undertaking to present to the Council as soon as possible a proposal concerning freedom to provide services in the area of insurance against civil liability in respect of the use of motor vehicles ( other than carrier's liability );
Whereas, subject to the provisions of the said Directive concerning compulsory insurance, it is appropriate to provide for the possibility of large risk treatment, within
the meaning of Article 5 of the said Directive, for the said insurance class of motor vehicle liability;
Whereas large risk treatment should also be envisaged for insurance covering damage to or loss of land motor vehicles and land vehicles other than motor vehicles;
Whereas Directive 88/357/EEC laid down that the risks which may be covered by way of Community co-insurance within the meaning of Council Directive 78/473/EEC of 30 May 1978 on the coordination of laws, regulations and administrative provisions relating to Community co-insurance ( 1 ) were to be large risks as defined in Directive 88/357/EEC whereas the inclusion by the present Directive of the motor insurance classes in the large risks definition of Directive 88/357/EEC will have the effect of including those classes in the list of classes which may be covered by way of Community co-insurance;
Whereas Council Directive 72/166/EEC of 24 April 1972 on the approximation of the laws of the Member States relating to insurance against civil liability in respect of the use of motor vehicles, and to the enforcement of the obligation to insure against such liability ( 2 ), as last amended by Directive 90/232/EEC ( 3 ), built on the green card system and the agreements between the national motor insurers' bureaux in order to enable green card checks to be abolished;
Whereas it is desirable, however, to grant Member States transitional arrangements for the gradual application of the specific provisions of this Directive relating to large risk treatment for the said insurance classes, including where risks are covered by co-insurance;
Whereas to ensure the continued proper functioning of the green card system and the agreements between the national motor insurers' bureaux it is appropriate to require insurance undertakings providing motor liability insurance in a Member State by way of provision of services to join and participate in the financing of the bureau of that Member State;
Whereas Council Directive 84/5/EEC of 30 December 1983 on the approximation of the laws of the Member States relating to insurance against civil liability in respect of the use of motor vehicles ( 4 ), as last amended by Directive 90/232/EEC, required the Member States to set up or authorize a body ( guarantee fund ) with the task of providing compensation to victims of accidents caused by uninsured or unidentified vehicles;
Whereas it is also appropriate to require insurance undertakings providing motor liability insurance in a Member State by way of provision of services to join and participate in the financing of the guarantee fund set up in that Member State;
Whereas the rules in force in some Member States concerning the cover of aggravated risks apply to all undertakings covering risks through an establishment situated there; whereas the purpose of those rules is to ensure that the compulsory nature of motor liability insurance is balanced by the possibility for motorists to obtain such insurance; whereas Member States should be permitted to apply those rules to undertakings providing services in their territories to the extent that the rules are justified in the public interest and do not exceed what is necessary to achieve the abovementioned purpose;
Whereas in the field of motor liability insurance the protection of the interests of persons suffering damage who could pursue claims in fact concerns each and everyone and that it is therefore advisable to ensure that these persons are not prejudiced or put to greater inconvenience where the motor liability insurer is operating by way of provision of services rather than by way of establishment; whereas for this purpose, and insofar as the interests of these persons are not sufficiently safeguarded by the rules applying to the supplier of services in the Member State in which it is established, it should be provided that the
Member State of provision of services shall require the undertaking to appoint a representative resident or established in its territory to collect all necessary information in relation to claims and shall possess sufficient powers to represent the undertaking in relation to persons suffering damage who could pursue claims, including the payment of such claims, and to represent it or, where necessary, to have it represented before the courts and authorities of that Member State in relation to these claims;
Whereas this representative may also be required to represent the undertaking before the competent authorities of the Member State of provision of services in relation to the control of the existence and validity of motor vehicle liability insurance policies;
Whereas provision should be made for a flexible procedure to make it possible to assess reciprocity with third countries on a Community basis; whereas the aim of this procedure is not to close the Community's financial markets but rather, as the Community intends to keep its financial markets open to the rest of the world, to improve the liberalization of the global financial markets
in third countries; whereas, to that end, this Directive provided for procedures for negotiating with third countries and, as a last resort, for the possibility of taking measures involving the suspension of new applications for authorization or the restriction of new authorizations,
HAS ADOPTED THIS DIRECTIVE :
Article 1
For the purposes of this Directive :
( a ) "vehicle' means a vehicle as defined in Article 1 ( 1 ) of Directive 72/166/EEC;
( b ) "bureau' means a national insurers' bureau as defined in Article 1 ( 3 ) of Directive 72/166/EEC;
( c ) "guarantee fund' means the body referred to in Article 1 ( 4 ) of Directive 84/5/EEC;
( d ) "parent undertaking' means a parent undertaking as defined in Articles 1 and 2 of Directive 83/349 / EEC ( 1 );
( e ) "subsidiary' means a subsidiary undertaking as defined in Articles 1 and 2 of Directive 83/349/EEC; any subsidiary undertaking of a subsidiary undertaking shall also be regarded as a subsidiary of the parent undertaking which is at the head of those undertakings .
Article 2
In Article 5 ( d ) of Directive 73/239/EEC, "risks classified under classes 8, 9, 13 and 16 of point A of the Annex' in the first paragraph of point ( iii ) is hereby replaced by the following :
"risks classified under classes 3, 8, 9, 10, 13 and 16 of point A of the Annex '.
Article 3
1 . The heading of Title III of Directive 73/239/EEC is hereby replaced by the following :
"TITLE III A
Rules applicable to agencies or branches established within the Community and belonging to undertakings whose head offices are outside the Community '.
2 . The following heading is placed after Article 29 of Directive 73/239/EEC :
"TITLE III B
Rules applicable to subsidiaries of parent undertakings governed by the laws of a third country and to acquisitions of holdings by such parent undertakings '.
Article 4
The following Articles 29a and 29b shall be added to Title III B of Directive 73/239/EEC .
"Article 29a
The competent authorities of the Member States shall inform the Commission :
( a ) of any authorization of a direct or indirect subsidiary, one or more parent undertakings of which are governed by the laws of a third country . The Commission shall inform the Insurance Committee to be established by the Council on proposal by the Commission;
( b ) whenever such a parent undertaking acquires a holding in a Community insurance undertaking which would turn the latter into its subsidiary . The Commission shall inform the Insurance Committee to be established by the Council on proposal by the Commission accordingly .
When authorization is granted to the direct or indirect subsidiary of one or more parent undertakings governed by the law of third countries, the structure of the group shall be specified in the notification which the competent authorities shall address to the Commission .
Article 29b
1 . Member States shall inform the Commission of any general difficulties encountered by their insurance undertakings in establishing themselves or carrying on their activities in a third country .
2 . Initially not later than six months before the application of this Directive, and thereafter periodically, the Commission shall draw up a report examining the treatment accorded to Community insurance undertakings in third countries, in the terms referred to in paragraphs 3 and 4, as regards establishment and the carrying on of insurance activities, and the acquisition of holdings in third-country insurance undertakings . The Commission shall submit those reports to the Council, together with any appropriate proposals .
3 . Whenever it appears to the Commission, either on the basis of the reports referred to in paragraph 2 or on the basis of other information, that a third country is not granting Community
insurance undertakings effective market access comparable to that granted by the Community to insurance undertakings from that third country, the Commission may submit proposals to the Council for the appropriate mandate
for negotiation with a view to obtaining comparable competitive opportunities for Community insurance undertakings . The Council shall decide by a qualified majority .
4 . Whenever it appears to the Commission, either on the basis of the reports referred to in paragraph 2 or on the basis of other information, that Community insurance undertakings in a third country are not receiving national treatment offering the same competitive opportunities as are available to domestic insurance undertakings and that the conditions of effective market access are not being fulfilled, the Commission may initiate negotiations in order to remedy the situation .
In the circumstances described in the first subparagraph, it may also be decided at any time, and in addition to initiating negotiations, in accordance with the procedure laid down in the Act establishing the Insurance Committee referred to in Article 29a, that the competent authorities of the Member States must limit or suspend their decisions :
_ regarding requests pending at the moment of the decision or future requests for authorizations, and
_ regarding the acquisition of holdings by direct or indirect parent undertakings governed by the laws of the third country in question .
The duration of the measures referred to may not exceed three months .
Before the end of that three-month period, and in the light of the results of the negotiations, the Council may, acting on a proposal from the Commission, decide by a qualified majority that the measures shall be continued .
Such limitations or suspension may not apply to the setting up of subsidiaries by insurance undertakings or their subsidiaries duly authorized in the Community, or to the acquisition of holdings in Community insurance undertakings by such undertakings or subsidiaries .
5 . Whenever it appears to the Commission that one of the situations described in paragraphs 3 and 4 has arisen, the Member States shall inform it at its request :
( a ) of any request for the authorization of a direct or indirect subsidiary, one or more parent undertakings of which are governed by the laws of the third country in question;
( b ) of any plans for such an undertaking to acquire a holding in a Community insurance undertaking such that the latter would become the subsidiary of the former .
This obligation to provide information shall lapse once an agreement is concluded with the third country referred to in paragraph 3 or 4 or when the measures referred to in the second and third subparagraphs of paragraph 4 cease to apply .
6 . Measures taken under this Article shall comply with the Community's obligations under any international agreements, bilateral or multilateral, governing the taking-up and pursuit of the business of insurance undertakings .'
Article 5
The second and third indents in the second paragraph of Article 12 ( 2 ) of Directive 88/357/EEC are hereby deleted .
Article 6
The following Article is hereby inserted in Title III of Directive 88/357/EEC :
"Article 12a
1 . This Article shall apply where an undertaking, through an establishment situated in a Member State, covers a risk, other than carrier's liability, classified under class 10 of point A of the Annex to Directive 73/239/EEC which is situated in another Member State .
2 . The Member State of provision of services shall require the undertaking to become a member of and participate in the financing of its national bureau and its national guarantee fund .
The undertaking shall not, however, be required to make any payment or contribution to the bureau and fund of the Member State of provision of services in respect of risks covered by way of provision of services other than one calculated on the same basis as for undertakings covering risks, other than carrier's liability, i
class 10 through an establishment situated in that Member State, by reference to its premium income from that class in that Member State or the number of risks in that class covered there .
3 . This Directive shall not prevent an insurance undertaking providing services from being required to comply with the rules in the Member State of provision of services concerning the cover of aggravated risks, insofar as they apply to established undertakings .
4 . The Member State of provision of services shall require the undertaking to ensure that persons pursuing claims arising out of events occurring in its territory are not placed in a less favourable situation as a result of the fact that the undertaking is covering a risk, other than carrier's liability, in class 10 by way of provision of services rather than through an establishment situated in that State .
For this purpose, the Member State of provision of services shall require the undertaking to appoint a representative resident or established in its territory who shall collect all necessary information in relation to claims, and shall possess sufficient powers to repre
sent the undertaking in relation to persons suffering damage who could pursue claims, including the payment of such claims, and to represent it or, where necessary, to have it represented before the courts and authorities of that Member State in relation to these claims .
The representative may also be required to represent the undertaking before the competent authorities of the State of provision of services with regard to checking the existence and validity of motor vehicle liability insurance policies .
The Member State of provision of services may not require that appointee to undertake activities on behalf of the undertaking which appointed him other than those set out in the second and third subparagraphs . The appointee shall not take up the business of direct insurance on behalf of the said undertaking .
The appointment of the representative shall not in itself constitute the opening of a branch or agency for the purpose of Article 6 ( 2 ) ( b ) of Directive 73/239/EEC and the representative shall not be an establishment within the meaning of Article 2 ( c ) of this Directive .'
Article 7
1 . The following subparagraph is hereby added to Article 15 ( 1 ) and to Article 16 ( 1 ) of Directive 88/357/EEC :
"Each Member State within the territory of which an undertaking intends to provide services covering risks in class 10, other than carrier's liability, may require that the undertaking :
_ notify the name and address of the claims representative referred to in Article 12a ( 4 );
_ produce a declaration that the undertaking has become a member of the national bureau and the national guarantee fund of the Member State of provision of services .'
Article 8
The following subparagraph is hereby added to Article 21 ( 2 ) of Directive 88/357/EEC :
"Each Member State may require that the name and address of the representative of the insurance undertaking also appear in the abovementioned documents .'
Article 9
Article 22 ( 1 ) of Directive 88/357/EEC is hereby replaced by the following :
"1 . Every establishment must inform its supervisory authority in respect of operations effected by way of provision of services of the amount of the premiums, without deduction of reinsurance, receivable by Member State and by group of classes . The groups of classes shall be defined as follows :
_ accident and sickness ( 1 and 2 ),
_ motor insurance ( 3, 7 and 10, the figures relating to class 10, excluding carrier's liability, being specified ),
_ fire and other damage to property ( 8 and 9 ),
_ aviation, marine and transport ( 4, 5, 6, 7, 11 and 12 ),
_ general liability ( 13 ),
_ credit and suretyship ( 14 and 15 ),
_ other classes ( 16, 17 and 18 ).
The supervisory authority of each Member State shall forward this information to the supervisory authorities of each of the Member States of provision of services .'
Article 10
The last subparagraph of Article 27 ( 1 ) of Directive 88/357/EEC is hereby replaced by the following :
"The derogation allowed from 1 January 1995 shall only apply to contracts covering risks classified under classes 3, 8, 9, 10, 13 and 16 situated exclusively in one of the four Member States benefiting from the transitional arrangements .'
Article 11
Notwithstanding Article 23 ( 2 ) of Directive 88/357/EEC, in the case of a large risk within the meaning of Article 5 ( d ) of Directive 73/239/EEC, classified under class 10, other than carrier's liability, the Member State of provision of services may provide that :
_ the amount of the technical reserves relating to the contract concerned shall be determined, under the supervision of the authorities of that Member State, in accordance with its rules or, failing such rules, in accordance with established practice in that Member State, until the date by which the Member States must comply
with a Directive coordinating the annual accounts of insurance undertakings,
_ the covering of these reserves by equivalent and matching assets shall be under the supervision of the authorities of that Member State in accordance with its rules or practice, until the notification of a Third Directive on non-life insurance,
_ the localization of the assets referred to in the second indent shall be under the supervision of the authorities of that Member State in accordance with its rules or practice until the date by which the Member States must comply with a Third Directive on non-life insurance .
Article 12
Member States shall amend their national provisions to comply with this Directive within 18 months of the date of its notification ( 1 ) and shall forthwith inform the Commission thereof .
The provisions amended pursuant to the first subparagraph shall be applied within 24 months of the date of the notification of this Directive .
Article 13
This Directive is addressed to the Member States .
Done at Brussels, 8 November 1990 .
For the Council
The President
P . ROMITA
( 1 ) OJ No C 65, 15 . 3 . 1989, p . 6, and
OJ No C 180, 20 . 7 . 1990, p . 6 .
( 2 ) OJ No C 68, 19 . 3 . 1990, p . 85, and
Decision of 10 October 1990 ( not yet published in the Official Journal ).
( 3 ) OJ No C 194, 31 . 7 . 1989, p . 3 .
( 4 ) OJ No L 228, 16 . 8 . 1973, p . 3 .
( 5 ) OJ No L 172, 4 . 7 . 1988, p . 1 .
( 1 ) OJ No L 151, 7 . 6 . 1978, p . 25 .
( 2 ) OJ No L 103, 2 . 5 . 1972, p . 1 .
( 3 ) OJ No L 129, 19 . 5 . 1990, p . 33 .
( 4 ) OJ No L 8, 11 . 1 . 1984, p . 17 .
( 1 ) OJ No L 193, 18 . 7 . 1983, p . 1 .
( 1 ) This Directive was notified to the Member States on 20 November 1990 .
