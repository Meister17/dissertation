COUNCIL DECISION of 18 March 1991 on the notification of the application by the Community of the 1989 International Agreement on Jute and Jute Products (91/152/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Articles 113 and 116 thereof,
Having regard to the proposal from the Commission,
Whereas the 1989 International Agreement on Jute and Jute Products (1) was signed by the Community and its Member States on 20 December 1990;
Whereas all Member States have indicated their intention to apply the Agreement;
Whereas the Community and its Member States should notify the Secretary-General of the United Nations Organization that they intend to apply the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
The Community and its Member States, after completion of the necessary internal procedures, shall, notify the Secretary-General of the United Nations Organization that they will apply the International Agreement on Jute and Jute Products (1989), as importing members, when it enters into force in accordance with Article 40 (3).
Article 2
The President of the Council is hereby authorized to designate the persons empowered to lodge the instrument of notification on behalf of the European Economic Community.
Done at Brussels, 18 March 1991. For the Council
The President
J.-C. JUNCKER
(1) OJ No L 29, 4. 2. 1991, p. 4.
