COUNCIL DECISION of 13 July 1998 on financing the fixed costs of the system of managing technical assistance for the African, Caribbean and Pacific (ACP) States and the overseas countries and territories (OCT) (98/461/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the fourth ACP-EC Convention, signed at Lomé on 15 December 1989 and amended by the agreement signed in Mauritius on 4 November 1995,
Having regard to Council Decision 91/482/EEC of 25 July 1991 on the association of the overseas countries and territories with the European Economic Community (1),
Having regard to the Internal Agreement on the financing and administration of Community aid under the second financial protocol of the fourth ACP-EC Convention, hereafter called the 'Internal Agreement`, and in particular Article 9 thereof,
Having regard to the proposal from the Commission,
Whereas it is necessary to cover, for a period of four years, the fixed costs arising from the replacement of the European Association for Cooperation for the Management of Technical Assistance for the ACP States and the OCT;
Whereas the revenue accruing from the interest on the deposited funds referred to in Article 9(2) of the Internal Agreement would cover these fixed costs,
HAS DECIDED AS FOLLOWS:
Article 1
The sum of ECU 5,5 million shall be deducted from the revenue accruing from the interest on the funds deposited with paying agents in Europe referred to in Article 319(4) of the fourth ACP-EC Convention, to finance the fixed costs arising from the replacement of the European Association for Cooperation for the management of technical assistance for the ACP States and the OCT.
Article 2
This Decision shall enter into force on the day of its adoption.
Done at Brussels, 13 July 1998.
For the Council
The President
W. SCHÜSSEL
(1) OJ L 263, 19. 9. 1991, p. 1. Decision as last amended by Decision 97/803/EC (OJ L 329, 29. 11. 1997, p. 50).
