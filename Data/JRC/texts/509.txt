Commission Regulation (EC) No 1159/2000
of 30 May 2000
on information and publicity measures to be carried out by the Member States concerning assistance from the Structural Funds
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1260/1999 of 21 June 1999 laying down general provisions on the Structural Funds(1), and in particular Article 53(2) thereof,
Whereas:
(1) Article 46 of Regulation (EC) No 1260/1999 requires information and publicity measures about the activities of the Structural Funds to be carried out.
(2) Article 34(1)(h) of Regulation (EC) No 1260/1999 stipulates that the managing authority in charge of implementing a package of structural assistance is responsible for compliance with the requirements on information and publicity.
(3) Article 46(2) of Regulation (EC) No 1260/1999 states that the managing authority is responsible for ensuring publicity about an assistance package and in particular for informing potential final beneficiaries, trade and professional bodies, the economic and social partners, bodies promoting equality between men and women and non-governmental organisations about the opportunities afforded by the assistance and for informing the general public about the role played by the Community in the assistance concerned and its results.
(4) Article 46(3) requires the Member States to consult the Commission and inform it each year of the initiatives taken with regard to information and publicity measures.
(5) Under Articles 18(3) and 19(4) of Regulation (EC) No 1260/1999, the programme complement for each operational programme or single programming document is to include the measures intended to publicise the assistance in accordance with Article 46.
(6) Article 35(3)(e) of Regulation (EC) No 1260/1999 states that the Monitoring Committees are to consider and approve the annual and final reports on assistance before they are sent to the Commission and Article 37(2) of that Regulation requires those reports to include the steps taken by the managing authority and the Monitoring Committee to ensure the quality and effectiveness of the measures taken to ensure publicity for the assistance. Article 40(4) states in particular that the results of the evaluations are to be made available to the public on request, with the agreement of the Monitoring Committee in the case of the mid-term evaluation to be carried out no later than 31 December 2003.
(7) Commission Decision 94/342/EC of 31 May 1994 concerning information and publicity measures to be carried out by the Member States concerning assistance from the Structural Funds and the Financial Instrument for Fisheries Guidance (FIFG)(2) continues to apply to assistance granted under Council Regulation (EEC) No 2052/88 of 24 June 1988 on the tasks of the Structural Funds and their effectiveness and on coordination of their activities between themselves and with the operations of the European Investment Bank and the other existing financial instruments(3), as last amended by Regulation (EC) No 3193/94(4), as well as the regulations adopted to implement that Regulation.
(8) The Committee referred to in Article 147 of the Treaty, the Committee on Agricultural Structures and Rural Development and the Committee on Structures for Fisheries and Aquaculture have been consulted on this Regulation. The measures provided for in this Regulation are in accordance with the opinion of the Committee for the Development and Conversion of Regions,
HAS ADOPTED THIS REGULATION:
Article 1
The detailed provisions applicable to information and publicity concerning assistance from the Structural Funds under Regulation (EC) No 1260/1999 shall be as set out in the Annex hereto.
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 30 May 2000.
For the Commission
Michel Barnier
Member of the Commission
(1) OJ L 161, 26.6.1999, p. 1.
(2) OJ L 152, 18.6.1994, p. 39.
(3) OJ L 185, 15.7.1988, p. 9.
(4) OJ L 337, 24.12.1994, p. 11.
ANNEX
IMPLEMENTING RULES FOR INFORMATION AND PUBLICITY ABOUT ASSISTANCE FROM THE STRUCTURAL FUNDS
1. General principles and scope
Information and publicity about assistance from the Structural Funds is intended to increase public awareness and transparency regarding the activities of the European Union and create a coherent picture of the assistance concerned across all Member States. Such information and publicity shall cover operations assisted by the European Regional Development Fund, the European Social Fund, the European Agricultural Guidance and Guarantee Fund, Guidance Section or the Financial Instrument for Fisheries Guidance.
The information and publicity measures described below relate to the Community support frameworks (CSFs), operational programmes, single programming documents (SPDs) and Community initiative programmes defined in Regulation (EC) No 1260/1999.
The managing authority responsible for implementing the assistance shall be responsible for publicity on the spot. Publicity shall be carried out in cooperation with the European Commission, which shall be informed of measures taken for this purpose.
The competent national and regional authorities shall take all the appropriate administrative steps to ensure the effective application of these arrangements and to collaborate with the Commission.
2. The aims of information and publicity measures and the target groups
The aim of information and publicity measures shall be to:
2.1. inform potential and final beneficiaries, as well as
- regional and local authorities and other competent public authorities,
- trade organisations and business circles,
- the economic and social partners,
- non-governmental organisations, especially bodies to promote equality between men and women and bodies working to protect and improve the environment,
- project operators and promoters,
about the opportunities offered by joint assistance from the European Union and the Member States in order to ensure the transparency of such assistance;
2.2. inform the general public about the role played by the European Union in cooperation with the Member States in the assistance concerned and its results.
3. Implementation of information and publicity measures
3.1. General provisions
3.1.1. Preparation of measures
The information and publicity measures shall be presented in the form of a communications action plan covering each operational programme and each single programming document (SPD). Where appropriate, such a plan shall be submitted for the CSF. It shall be implemented under the responsibility of the managing authority.
Each communications action plan shall include:
- its aims and target groups,
- the content and strategy of the resulting communications and information measures, stating the measures to be taken under the different objectives under each Fund.
- its indicative budget,
- the administrative departments or bodies responsible for implementation,
- the criteria to be used to evaluate the measures carried out.
The communications action plan shall be set out in the programme complement in accordance with Article 18(3)(d) of Regulation (EC) No 1260/1999.
3.1.2. Finance
The amounts set aside for information and publicity shall appear in the financing plans for the Community support frameworks (CSFs), SPDs and operational programmes under the heading of technical assistance (the appropriations required to prepare, monitor and evaluate assistance under Articles 17(2)(e), 18(2)(b) and 19(3)(b) of Regulation (EC) No 1260/1999).
3.1.3. Identification of those responsible
Each managing authority shall designate one or more persons to be responsible for information and publicity. The managing authorities shall inform the Commission of those designated.
3.1.4. Accountability
The managing authority shall inform the Commission about the implementation of this Regulation at the annual meeting referred to in Article 34(2) of Regulation (EC) No 1260/1999.
3.2. Content and strategy of information and publicity measures
The measures to be implemented shall be designed to achieve the aims referred to in point 2 above. These are:
- ensuring transparency for potential and final beneficiaries,
- informing the public.
3.2.1. Ensuring transparency for potential and final beneficiaries and the groups referred to in 2.1
3.2.1.1. The managing authority designated for each assistance package shall in particular ensure:
- publication of the content of the package, including an indication of the involvement of the Structural Funds concerned, circulation of these documents and their availability to those who request them,
- the establishment of appropriate reporting on the progress of the assistance throughout the programming period,
- the implementation of information measures relating to the management, monitoring and evaluation of assistance from the Structural Funds, financed where appropriate from the appropriations for technical assistance under the assistance package concerned.
>TABLE>
3.2.1.2. The managing authority designated to implement an assistance package shall ensure the existence of appropriate channels for circulating information in order to ensure transparency for the various potential partners and beneficiaries, particularly small and medium-sized businesses.
This information should include a clear outline of the administrative procedures to be followed, a description of the system for managing applications, information on the criteria used in selection procedures and on the mechanisms for evaluation and names of persons or contact points at national, regional or local level who can explain how the assistance packages operate and the criteria for eligibility.
In the case of support for local potential, public aid for businesses and global grants, this information shall be distributed principally through the intermediary bodies and organisations representing the firms concerned.
3.2.1.3. Managing authorities shall ensure the existence of appropriate channels for providing information to those persons who could benefit from schemes involving training, employment or the development of human resources. To this end, they shall secure the cooperation of vocational training bodies, bodies concerned with employment, businesses and groups of businesses, training centres and non-governmental organisations.
3.2.2. Informing the public
3.2.2.1. In order to make the public more aware of the part played by the European Union in the assistance packages concerned and the results they achieve, the designated managing authority shall inform the media in the most appropriate way about the structural assistance part-financed by the Union. This information shall include a balanced reference to the Union's contribution and the messages shall state the tasks of each Fund by setting out the specific priorities of the assistance packages concerned in accordance with 3.2.1.1.
Steps shall be taken, at the time of the original launch of assistance following approval by the Commission and of the main phases of implementation, to alert the national and regional media (press, radio and television) as appropriate; such steps may include press releases, the placing of articles, supplements in the most suitable newspapers and site visits. Other means of information and communication may also be used such as websites, publications describing successful projects and competitions to identify best practice.
Where use is made of advertising inserts, for example press briefings or publicity notices, the participation of the European Union shall be clearly indicated.
Appropriate collaboration with the Commission's office in the country concerned must be ensured.
3.2.2.2. Information and publicity measures for the general public shall include:
- in the case of infrastructure investments whose total cost exceeds EUR 500000 for operations part-financed by the FIFG and EUR 3 million in the case of all other operations:
- billboards erected on site,
- permanent commemorative plaques for infrastructures accessible to the general public, to be installed in accordance with point 6;
- in the case of part-financed training and employment measures:
- measures making beneficiaries of training schemes aware that they are participating in an operation part-financed by the European Union;
- measures making the general public aware of the role played by the European Union in relation to operations financed in the field of vocational training, employment and the development of human resources;
- in the case of investments in business, measures to develop local potential and all other measures receiving financial assistance from the Community:
- information for beneficiaries about their participation in an operation part-financed by the European Union in one of the ways described in point 6.
4. The work of Monitoring Committees
4.1. Monitoring Committees shall ensure that there is adequate information about their work. To this end, wherever possible they shall keep the media informed of the progress of the assistance packages for which they are responsible. Contacts with the press shall be under the responsibility of the chairman. The Commission representatives shall be involved in contacts with the press.
Appropriate arrangements shall also be made when important events are held in connection with the Monitoring Committee's meetings, such as high-level meetings or inaugural sessions. The Commission and its offices in the Member States shall be kept informed of these arrangements.
4.2. Monitoring Committees shall debate the annual implementing report referred to in Article 37 of Regulation (EC) No 1260/1999 which, in accordance with Article 35 of that Regulation, must contain a section on information and publicity. Information on the quality and effectiveness of the information and publicity measures and suitable evidence such as photographs shall be submitted to Monitoring Committees by the managing authority.
In accordance with Article 46 of Regulation (EC) No 1260/1999, the Member States shall send the Commission all the information which it needs to take into account for its annual report as provided for in Article 45 of that Regulation.
Such information must enable the Commission to ascertain that the provisions of this Regulation have been complied with.
5. Partnership and exchanges of experience
Managing authorities are at liberty to carry out any additional measures, including those which contribute to the proper implementation of the policy pursued under the Structural Funds.
They shall inform the Commission of the initiatives they take so that it may participate appropriately in their realisation.
In order to facilitate implementation of this Regulation, the Commission shall provide technical assistance where necessary. It shall make its expertise and material available to the authorities concerned in a spirit of partnership and in the common interest. It shall support exchanges of experience about the implementation of Article 46 of Regulation (EC) No 1260/1999 and promote informal networks among those responsible for providing information. To that end, it would be desirable for Member States to designate a national coordinator for each Fund.
6. Rules on the technical means of information and publicity
In order to ensure the visibility of measures part-financed by one of the Structural Funds, the appropriate managing authority shall be responsible for seeing that the following information and publicity measures are complied with:
6.1. Billboards
Billboards shall be erected on the sites of projects involved in part-financed infrastructure investments whose volume exceeds the amounts given at 3.2.2.2. Such billboards shall include a space reserved for the indication of the European Union's contribution.
Billboards must be of a size which is appropriate to the scale of the operation.
The section of the billboard reserved for the Community contribution must meet the following criteria:
- it shall take up at least 25 % of the total area of the billboard;
- it shall bear the standardised Community emblem and the following text, to be set out as shown below:
>PIC FILE= "L_2000130EN.003501.EPS">
- the emblem shall be presented in accordance with the current specifications;
- the lettering used to indicate the financial contribution of the European Union must be at least the same size as the lettering for the national indications, although the typeface may be different;
- the Fund concerned may be mentioned.
Where the competent authorities do not erect a billboard announcing their own involvement in the financing of a project, the assistance from the European Union shall be announced on a billboard specifically for that purpose. In such cases, the above provisions shall apply by analogy.
Billboards shall be removed not later than six months after completion of the work and replaced by a commemorative plaque in accordance with 6.2.
6.2. Commemorative plaques
Permanent commemorative plaques shall be placed at sites accessible to the general public (congress centres, airports, stations, etc.) which represent projects part-financed by the Structural Funds. In addition to the Community emblem, such plaques must indicate the Community's contribution and may mention the Fund concerned.
In the case of physical investments in commercial business premises, commemorative plaques shall be installed for a period of one year.
If a competent authority or final beneficiary decides to erect billboards or commemorative plaques, produce publications or undertake any other information measure regarding projects whose total volume is less than EUR 500000 in the case of operations part-financed by the FIFG and EUR 3 million in the case of all other operations, the Community contribution shall likewise be indicated.
6.3. Posters
In order to inform beneficiaries and the general public of the role played by the European Union in the development of human resources, vocational training and employment, investment in firms and rural development, the managing authorities shall display posters indicating the Union's contribution and possibly the Fund concerned on the premises of bodies implementing or benefiting from measures financed by the Structural Funds (employment agencies, vocational training centres, chambers of commerce and industry, chambers of agriculture, regional development agencies, etc.).
6.4. Notification to beneficiaries
All notifications of aid to beneficiaries sent by the competent authorities shall mention the fact of part-financing by the European Union and may state the amount or percentage of the assistance funded by the Community instrument concerned.
6.5. Information and communication material
6.5.1. Publications (such as booklets, leaflets and newsletters) about regional assistance part-financed by the Structural Funds shall contain a clear indication on the title page of the European Union's participation and, where appropriate, that of the Fund concerned as well as the Community emblem if the national or regional emblem is also used.
Publications shall include references to the body responsible for the information content and to the managing authority designated to implement the assistance package in question.
6.5.2. In the case of information made available by electronic means (websites, databases for potential beneficiaries) or as audio-visual material, the principles set out above shall apply by analogy. In drawing up the communications action plan, due regard must be had to new technologies which permit the rapid and efficient distribution of information and facilitate a dialogue with the general public.
Websites concerning the Structural Funds should
- mention the contribution of the European Union and, if appropriate, that of the Fund concerned at least on the home page,
- include a hyperlink to the other Commission websites concerning the Structural Funds.
6.6. Information events
The organisers of information events such as conferences, seminars, fairs and exhibitions in connection with the implementation of operations part-financed by the Structural Funds shall make the Community contribution to these assistance packages explicit by displaying the European flag in meeting rooms and using the Community emblem on documents.
The Commission's offices in the Member States shall assist, as necessary, in the preparation and implementation of such events.
