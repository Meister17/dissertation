Information communicated by Member States regarding State aid granted under Commission Regulation (EC) No 68/2001 of 12 January 2001 on the application of Articles 87 and 88 of the EC Treaty to training aid
(Text with EEA relevance)
(2006/C 308/08)
Aid No | XT 20/06 |
Member State | Italy |
Region | Puglia |
Title of aid scheme or name of company receiving individual aid | Funding of external training for apprenticesIncentives to convert apprenticeships into permanent work contracts |
Legal basis | Legge regionale n. 13 del 22.11.2005 attuativa dell'art. 49 del decreto leg.vo 10.9.2003 No 276 |
Annual expenditure planned or overall amount of individual aid granted to the company | Apprentice training is estimated to cost some EUR 11 million per year. The amount of the incentive given to companies is established annually by the Regional Budget Law. |
Maximum aid intensity | As established by EC regulations. |
Date of implementation | From 25.11.2005 (date of entry into force of Regional Law No 13/2005). |
Duration of scheme or individual aid award | To be established by the relevant quarterly public notices. |
Objective of aid | Funding of external training for apprenticesIncentives to convert apprenticeships into permanent work contracts |
Economic sectors concerned | All |
Name and address of the granting authority | Region of Puglia |
Aid No | XT 49/06 |
Member State | Estonia |
Region | Estonia |
Title of aid scheme or name of company receiving individual aid | National Development Plan Action 1.2 "Training support" |
Legal basis | "Eesti riikliku arengukava Euroopa Liidu struktuurifondide kasutuselevõtuks — ühtne programmdokument aastateks 2004–2006" meetme nr 1.2 "Inimressursi arendamine ettevõtete majandusliku konkurentsivõime suurendamiseks" osa "Koolitustoetus" tingimused. Majandus- ja Kommunikatsiooniministri 22. septembri 2006. a määrus nr 80 (RTL, 3.10.2006, 72, 1316). |
Annual expenditure planned or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount | Estonia: EUR 0,64 million; ERDF: EUR 1,92 million; Total: EUR 2,56 million |
Loans guaranteed | |
Individual aid | Overall aid amount | |
Loans guaranteed | |
Maximum aid intensity | In conformity with Article 4(2)-(7) of the Regulation | Yes |
Date of implementation | 6.10.2006 |
Duration of scheme or individual aid award | Until 31.12.2006 |
Objective of aid | General training | Yes |
Specific training | Yes |
Economic sectors concerned | All sectors | No |
Limited to specific sectors | Yes |
Agriculture | Yes |
Fisheries and aquaculture | Yes |
Coalmining | No |
All manufacturing | Yes |
All services | |
or | |
Maritime transport services | Yes |
Other transport services | Yes |
Financial services | No |
Other services | Yes |
Name and address of the granting authority | Ettevõtluse Arendamise Sihtasutus |
Liivalaia 13/15, EE-10118 Tallinn |
Large individual aid grants | In conformity with Article 5 of the Regulation | Yes |
Aid No | XT 50/06 |
Member State | Estonia |
Region | Estonia |
Title of aid scheme or name of company receiving individual aid | National Development Plan Action 1.2 "Training scheme" |
Legal basis | "Eesti riikliku arengukava Euroopa Liidu struktuurifondide kasutuselevõtuks — ühtne programmdokument aastateks 2004–2006" meetme nr 1.2 "Inimressursi arendamine ettevõtete majandusliku konkurentsivõime suurendamiseks" osa "Koolituskava" tingimused. Majandus- ja Kommunikatsiooniministri 22. septembri 2006. a määrus nr 83 (RTL, 3.10.2006, 72, 1319). |
Annual expenditure planned or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount | Estonia: EUR 0,67 million; ERDF: EUR 2 million; Total: EUR 2,67 million |
Loans guaranteed | |
Individual aid | Overall aid amount | |
Loans guaranteed | |
Maximum aid intensity | In conformity with Article 4(2)-(7) of the Regulation | Yes |
Date of implementation | 6.10.2006 |
Duration of scheme or individual aid award | Until 31.12.2006 |
Objective of aid | General training | Yes |
Specific training | Yes |
Economic sectors concerned | All sectors | No |
Limited to specific sectors | Yes |
Agriculture | No |
Fisheries and aquaculture | No |
Coalmining | No |
All manufacturing | Yes |
All services | |
or | |
Maritime transport services | No |
Other transport services | No |
Financial services | No |
Other services | Yes |
Name and address of the granting authority | Ettevõtluse Arendamise Sihtasutus |
Liivalaia 13/15, EE-10118 Tallinn |
Large individual aid grants | In conformity with Article 5 of the Regulation | Yes |
--------------------------------------------------
