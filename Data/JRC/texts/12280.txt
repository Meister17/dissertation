Communication from the French Government concerning Directive 94/22/EC of the European Parliament and of the Council of 30 May 1994 on the conditions for granting and using authorisations for the prospection, exploration and production of hydrocarbons [1]
(Notice regarding an application for an exclusive licence to prospect for oil and gas, designated the "Permis de Lons-le-Saunier")
(2006/C 163/04)
(Text with EEA relevance)
On 30 June 2005, European Gas Limited, with registered offices at 11, rue Tonduti de l'Escarène, 06000 Nice (France), applied for an exclusive five-year licence, designated the "Permis de Lons-le-Saunier", to prospect for oil and gas in an area of approximately 3795 km2 covering parts of the departments of Ain, Saône et Loire, Jura and Doubs.
The perimeter of the area covered by this licence consists of the meridian and parallel arcs connecting in turn the points defined below by their geographical coordinates, the original meridian being that of Paris.
POINTS | LONGITUDE | LATITUDE |
A | 4,20° E | 52,20° N |
B | 4,20° E | 52,00° N |
C | 4,10° E | 52,00° N |
D | 4,10° E | 51,90° N |
E | 3,90° E | 51,90° N |
F | 3,90° E | 51,70° N |
G | 3,70° E | 51,70° N |
H | 3,70° E | 51,60° N |
I | 3,40° E | 51,60° N |
J | 3,40° E | 51,50° N |
K | 3,30° E | 51,50° N |
L | 3,30° E | 51,40° N |
M | 3,10° N | 51,40° N |
N | 3,10° E | 51,30° N |
E | 2,80° E | 51,30° N |
P | 2,80° E | 51,50° N |
Q | 2,90° E | 51,50° N |
R | 2,90° E | 51,80° N |
S | 3,10° E | 51,80° N |
T | 3,10° E | 51,90° N |
U | 3,30° E | 51,90° N |
V | 3,30° E | 52,00° N |
W | 3,70° E | 52,00° N |
X | 3,70° E | 52,10° N |
Y | 3,90° E | 52,10° N |
Z | 3,90° E | 52,20° N |
Submission of applications
The initial applicants and competing applicants must prove that they comply with the requirements for obtaining the licence, as specified in Articles 3, 4 and 5 of Decree No 95-427 of 19 April 1995, as amended, concerning mining rights.
Interested companies may, within 90 days of the publication of this notice, submit a competing application in accordance with the procedure summarised in the "Notice regarding the granting of mining rights for hydrocarbons in France" published in Official Journal of the European Communities C 374 of 30 December 1994, p. 11, and established by Decree No 95-427 of 19 April 1995, as amended, concerning mining rights (Journal officiel de la République française, 22 April 1995).
Competing applications must be sent to the Minister responsible for mines at the address below. Decisions on the initial application and competing applications will be taken by 15 February 2008 at the latest.
Conditions and requirements regarding performance of the activity and cessation thereof
Applicants are referred to Articles 79 and 79(1) of the Mining Code and to Decree No 95-696 of 9 May 1995, as amended, on the start of mining operations and the mining regulations (Journal officiel de la République française, 11 May 1995).
Further information can be obtained from the Ministry of Economic Affairs, Finance and Industry (Directorate-General for Energy and Raw Materials, Directorate for Energy and Mineral Resources, Bureau of Mining Legislation), 61, boulevard Vincent Auriol, Télédoc 133, F-75703 Paris Cedex 13, France (telephone: (33) 144 97 23 02, fax: (33) 144 97 05 70).
The abovementioned laws and regulations can be consulted at http://www.legifrance.gouv.fr
[1] OJ L 164 of 30.6.1974, p. 3.
--------------------------------------------------
