Notice of initiation of an expiry review of the countervailing measures applicable to imports of polyethylene terephthalate (PET) film originating in India
(2004/C 306/02)
Following the publication of a notice of impending expiry [1] of the countervailing measures in force on imports of polyethylene terephthalate (PET) film originating in India, (country concerned), the Commission has received a request for review pursuant to Article 18 of Council Regulation (EC) No 2026/97 [2] (the basic Regulation).
1. Request for review
The request was lodged on 10 September 2004 by the following Community producers: DuPont Teijin Films, Mitsubishi Polyester Film GmbH, Nuroll SpA, Toray Plastics Europe (the applicants), representing a major proportion, in this case more than 50 %, of the total Community production of PET film.
2. Product
The product under review is polyethylene terephthalate (PET) film originating in India (the product concerned), currently classifiable within CN codes ex39206219 and ex39206290. These CN codes are given only for information.
3. Existing measures
The measures currently in force are a definitive countervailing duty imposed by Council Regulation (EC) No 2597/1999s [3].
4. Grounds for the review
The applicants have provided evidence that the expiry of the measures would lead to a continuation of subsidisation and injury.
It is alleged that the exporters/producers of the product concerned have benefited and will continue to benefit from a number of subsidies granted by the Government of India. These alleged subsidies consist of schemes of benefits to industries located in special economic zones/export oriented units; the advance licenses; advance release orders scheme; the duty entitlement passbook scheme; an income tax exemption; the export promotion capital goods scheme; the duty free replenishment certificate; the package scheme of incentives of the Government of Maharashtra, the sales tax incentives scheme of the State of Uttaranchal, special import licences, capital infusions and export credits.
The total subsidy is estimated to be significant.
It is alleged that the above schemes are subsidies since they involve a financial contribution from the Government of India or other regional governments and confer a benefit to the recipients, i.e. to exporters/producers of PET film. They are alleged to be contingent upon export performance and therefore specific and countervailable or to be otherwise specific and countervailable.
The applicants have provided evidence that imports of the product concerned from India have increased overall in absolute terms.
It is also alleged that the volumes and the prices of the imported product concerned have continued, among other consequences, to have a negative impact on the level of prices charged by the Community industry, resulting in substantial adverse effects on the overall performance, the financial situation and the employment situation of the Community industry.
5. Procedure
Having determined, after consulting the Advisory Committee, that sufficient evidence exists to justify the initiation of an expiry review, the Commission hereby initiates a review in accordance with Article 18 of the basic Regulation.
5.1. Procedure for the determination of likelihood of subsidisation and injury
The investigation will determine whether the expiry of the measures would be likely, or unlikely, to lead to a continuation or recurrence of subsidization and injury.
(a) Sampling
In view of the apparent number of parties involved in this proceeding, the Commission may decide to apply sampling, in accordance with Article 27 of the basic Regulation.
(i) Sampling for investigation of subsidisation in India
In order to enable the Commission to decide whether sampling is necessary and, if so, to select a sample, all exporters/producers, or representatives acting on their behalf, are hereby requested to make themselves known by contacting the Commission and providing the following information on their company or companies within the time limit set in paragraph 6(b)(i) and in the formats indicated in paragraph 7 of this notice:
- name, address, e-mail address, telephone, and fax, and/or telex numbers and contact person,
- the turnover in local currency and the volume in tonnes of the product concerned sold for export to the Community and exports to other countries (separately) during the period 1 October 2003 to 30 September 2004,
- the turnover in local currency and the sales volume in tonnes for the product concerned on the domestic market during the period 1 October 2003 to 30 September 2004,
- whether the company intends to claim an individual subsidy rate (individual subsidy rates can only be claimed by producers [4]),
- the precise activities of the company with regard to the production of the product concerned,
- the volume in tonnes of the product concerned produced, the production capacity and the investments in production capacity during the period 1 October 2003 to 30 September 2004,
- the names and the precise activities of all related companies [5] involved in the production and/or selling (export and/or domestic) of the product concerned,
- any other relevant information that would assist the Commission in the selection of the sample,
- an indication of whether the company or companies agree to their inclusion in the sample, which implies replying to a questionnaire and accepting an on-the-spot investigation of their response,
- an indication on whether the company is recognised as an export oriented unit,
- an indication on whether the company is located in a special economic zone.
In order to obtain the information it deems necessary for the selection of the sample of exporters/producers, the Commission will, in addition, contact the authorities of the exporting country, and any known associations of exporters/producers.
(ii) Sampling for Community producers
In view of the large number of Community producers supporting the request, the Commission intends to investigate injury to the Community industry by applying sampling.
In order to enable the Commission to select a sample, all Community producers are hereby requested to provide the following information on their company or companies within the time limit set in paragraph 6(b)(i) of this notice:
- name, address, e-mail address, telephone, and fax, and/or telex numbers and contact person,
- the total turnover in euro of the company during the period 1 October 2003 to 30 September 2004,
- the precise activities of the company with regard to the production of the product concerned,
- the value (in EUR) of sales of the product concerned made in the Community market during the period 1 October 2003 to 30 September 2004,
- the volume in tonnes of sales of the product concerned made in the Community market during the period 1 October 2003 to 30 September 2004,
- the volume in tonnes of the production of the product concerned during the period 1 October 2003 to 30 September 2004,
- the names and the precise activities of all related companies involved in the production and/or selling of the product concerned,
- any other relevant information that would assist the Commission in the selection of the sample,
- an indication of whether the company or companies agree to their inclusion in the sample, which implies replying to a questionnaire and accepting an on-the-spot investigation of their response.
(iii) Final selection of the samples
All interested parties wishing to submit any relevant information regarding the selection of the samples must do so within the time limit set in paragraph 6(b)(ii) of this notice.
The Commission intends to make the final selection of the samples after having consulted the parties concerned that have expressed their willingness to be included in the sample.
Companies included in the samples must reply to a questionnaire within the time limit set in paragraph 6(b)(iii) of this notice and must co-operate within the framework of the investigation.
If sufficient cooperation is not forthcoming, the Commission may base its findings, in accordance with Articles 27(4) and 28 of the basic Regulation, on the facts available. A finding based on facts available may be less advantageous to the party concerned, as explained in paragraph 8 of this notice.
(b) Questionnaires
In order to obtain the information it deems necessary for its investigation, the Commission will send questionnaires to the sampled Community industry and to any association of producers in the Community, to the sampled exporters/producers in India, to any association of exporters/producers, to the importers, to any association of importers named in the request or which cooperated in the investigation leading to the measures subject to the present review, and to the authorities of the exporting country concerned.
In any event, all parties should contact the Commission forthwith by fax in order to find out whether they are listed in the request and if necessary request a questionnaire within the time limit set in paragraph 6(a)(i), given that the time limit set in paragraph 6(a)(ii) of this notice applies to all interested parties.
(c) Collection of information and holding of hearings
All interested parties are hereby invited to make their views known, submit information other than questionnaire replies and to provide supporting evidence. This information and supporting evidence must reach the Commission within the time limit set in paragraph 6(a)(ii) of this notice.
Furthermore, the Commission may hear interested parties, provided that they make a request showing that there are particular reasons why they should be heard. This request must be made within the time limit set in paragraph 6(a)(iii) of this notice.
5.2. Procedure for the assessment of Community interest
In accordance with Article 31 of the basic Regulation and in the event that the likelihood of continuation or recurrence of subsidisation and injury is confirmed, a determination will be made as to whether to maintain or repeal the anti-subsidy measures would not be against the Community interest. For this reason the Community industry, importers, their representative associations, representative users and representative consumer organisations, provided that they prove that there is an objective link between their activity and the product concerned, may, within the general time limits set in paragraph 6(a)(ii) of this notice, make themselves known and provide the Commission with information. The parties which have acted in conformity with the previous sentence may request a hearing, setting the particular reasons why they should be heard, within the time limit set in paragraph 6(a)(iii) of this notice. It should be noted that any information submitted pursuant to Article 31 will only be taken into account if supported by factual evidence at the time of submission.
6. Time limits
(a) General time limits
(i) For parties to request a questionnaire
All interested parties who did not cooperate in the investigation leading to the measures subject to the present review should request a questionnaire as soon as possible, but not later than 15 days after the publication of this notice in the Official Journal of the European Union.
(ii) For parties to make themselves known, to submit questionnaire replies and any other information
All interested parties, if their representations are to be taken into account during the investigation, must make themselves known by contacting the Commission, present their views and submit questionnaire replies or any other information within 40 days of the date of publication of this notice in the Official Journal of the European Union, unless otherwise specified. Attention is drawn to the fact that the exercise of most procedural rights set out in the basic Regulation depends on the party's making itself known within the aforementioned period
Companies selected in a sample must submit questionnaire replies within the time limit specified in paragraph 6(b)(iii) of this notice.
(iii) Hearings
All interested parties may also apply to be heard by the Commission within the same 40 day time limit.
(b) Specific time limit in respect of sampling
(i) The information specified in paragraph 5.1(a)(i) should reach the Commission within 15 days of the date of publication of this notice in the Official Journal of the European Union, given that the Commission intends to consult parties concerned that have expressed their willingness to be included in the sample on its final selection within a period of 21 days of the publication of this notice in the Official Journal of the European Union.
(ii) All other information relevant for the selection of the sample as referred to in 5.1(a)(iii) must reach the Commission within a period of 21 days of the publication of this notice in the Official Journal of the European Union.
(iii) The questionnaire replies from sampled parties must reach the Commission within 37 days from the date of the notification of their inclusion in the sample.
7. Written submissions, questionnaire replies and correspondence
All submissions and requests made by interested parties must be made in writing (not in electronic format, unless otherwise specified) and must indicate the name, address, e-mail address, telephone and fax, and/or telex numbers of the interested party. All written submissions, including the information requested in this notice, questionnaire replies and correspondence provided by interested parties on a confidential basis shall be labelled as "Limited [6]" and, in accordance with Article 29(2) of the basic Regulation, shall be accompanied by a non-confidential version, which will be labelled "For inspection by interested parties".
Commission address for correspondence:
European Commission
Directorate General for Trade
Directorate B
Office: J-79 5/16
B-1049 Brussels
Fax (32-2) 295 65 05
Telex COMEU B 21877.
8. Non-cooperation
In cases in which any interested party refuses access to or does not provide the necessary information within the time limits, or significantly impedes the investigation, provisional or final findings, affirmative or negative, may be made in accordance with Article 28 of the basic Regulation, on the basis of the facts available.
Where it is found that any interested party has supplied false or misleading information, the information shall be disregarded and use may be made of the facts available. If an interested party does not cooperate or cooperates only partially and findings are therefore based on facts available in accordance with Article 28 of the basic Regulation, the result may be less favourable to that party than if it had cooperated.
9. Schedule of the review investigation
The review investigation will be concluded, according to Article 22(1) of the basic Regulation, within 15 months of the date of the publication of this notice in the Official Journal of the European Union.
[1] OJ C 62, 11.3.2004, p. 4.
[2] OJ L 288, 21.10.1997, p. 1. Regulation as last amended by Council Regulation (EC) No 461/2004 (OJ L 77, 13.3.2004, p. 12).
[3] OJ L 316, 10.12.1999, p. 1.
[4] Individual margins may be claimed pursuant to Article 27(3) of the basic Regulation for companies not included in the sample.
[5] For guidance on the meaning of related companies, please refer to Article 143 of Commission Regulation (EEC) No 2454/93 of 2 July 1993 laying down provisions for the implementation of Council Regulation (EEC) No 2913/92 establishing the Community Customs Code (OJ L 253, 11.10.1993, p. 1).
[6] This means that the document is for internal use only. It is protected pursuant to Article 4 of Regulation (EC) No 1049/2001 of the European Parliament and of the Council (OJ L 145, 31.5.2001, p. 43). It is a confidential document pursuant to Article 29 of Council Regulation (EC) No 2026/97 (OJ L 288, 21.10.1997, p. 1) and Article 12 of the WTO Agreement on Subsidies and Countervailing Measures.
--------------------------------------------------
