Commission Regulation (EC) No 780/2006
of 24 May 2006
amending Annex VI to Council Regulation (EEC) No 2092/91 on organic production of agricultural products and indications referring thereto on agricultural products and foodstuffs
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2092/91 of 24 June 1991 on organic production of agricultural products and indications referring thereto on agricultural products and foodstuffs [1], and in particular the second indent of Article 13 thereof,
Whereas:
(1) Pursuant to Article 5(8) of Regulation (EEC) No 2092/91, limitative lists of the ingredients and substances referred to in paragraph 3(c) and (d) and paragraph 5a(d) and (e) of that Article shall be established in Sections A and B of Annex VI to that Regulation. The conditions of use of these ingredients and substances may be specified.
(2) Further to the introduction of rules for organic production of livestock and livestock products in Regulation (EEC) No 2092/91, it is necessary to adapt those lists in order to include substances used in the processing of products intended for human consumption which contain ingredients from animal origin.
(3) It is also necessary to define additives that may be used for the preparation of fruit wines other than wines covered by Council Regulation (EC) No 1493/1999 of 17 May 1999 on the common organisation of the market in wine [2].
(4) Regulation (EEC) No 2092/91 should therefore be amended accordingly.
(5) The measures provided for in this Regulation are in accordance with the opinion of the Committee set up in accordance with Article 14 of Regulation (EEC) No 2092/91,
HAS ADOPTED THIS REGULATION:
Article 1
Annex VI to Regulation (EEC) No 2092/91 is amended in accordance with the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Union.
It shall apply from 1 December 2007.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 24 May 2006.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 198, 22.7.1991, p. 1. Regulation as last amended by Regulation (EC) No 699/2006 (OJ L 121, 6.5.2006, p. 36).
[2] OJ L 179, 14.7.1999, p. 1. Regulation as last amended by Commission Regulation (EC) No 2165/2005 (OJ L 345, 28.12.2005, p. 1).
--------------------------------------------------
ANNEX
Annex VI to Regulation (EEC) No 2092/91 is amended as follows:
1. The text under the heading "GENERAL PRINCIPLES" is amended as follows:
(a) The first paragraph is replaced by the following:
"Sections A, B and C cover the ingredients and processing aids which may be used in the preparation of foodstuffs composed essentially of one or more ingredients of plant and/or animal origin, referred to in Article 1(1)(b) of this Regulation, with the exception of wines covered by Council Regulation (EC) No 1493/1999 [1].
Products of animal origin bearing an indication referring to the organic production method, which have been lawfully produced before the date of application of Commission Regulation (EC) No 780/2006 [2], may be marketed until stocks are exhausted.
(b) The second paragraph is replaced by the following:
"When a foodstuff is composed of ingredients of plant and animal origin, the rules established in Article 3 of Directive 95/2/EC of the European Parliament and of the Council [3] shall apply.
The inclusion in Sub-section A.1 of sodium nitrite and potassium nitrate shall be re-examined before 31 December 2007, with a view to limiting or withdrawing the use of these additives.
2. Section A is amended as follows:
(a) Subsection A.1 is replaced by the following:
"A.1. Food additives, including carriers
Code | Name | Preparation of foodstuffs of plant origin | Preparation of foodstuffs of animal origin | Specific conditions |
E 153 | Vegetable carbon | | X | Ashy goat cheese Morbier cheese |
E 160b | Annatto, Bixin, Norbixin | | X | Red Leicester cheese Double Gloucester cheese Scottish cheddar Mimolette cheese |
E 170 | Calcium carbonate | X | X | Shall not be used for colouring or calcium enrichment of products |
E 220 Or | Sulphur dioxide | X | X | In fruit wines [5] without added sugar (including cider and perry) or in mead: 50 mg [4] For cider and perry prepared with addition of sugars or juice concentrate after fermentation: 100 mg [4] |
E 224 | Potassium metabisulphite | X | X |
E 250 Or | Sodium nitrite | | X | Meat products [8] For E 250: indicative ingoing amount expressed as NaNO2: 80 mg/kg For E 252: indicative ingoing amount expressed as NaNO3: 80 mg/kg For E 250: maximum residual amount expressed as NaNO2: 50 mg/kg For E 252: maximum residual amount expressed as NaNO3: 50 mg/kg |
E 252 | Potassium nitrate | | X |
E 270 | Lactic acid | X | X | |
E 290 | Carbon dioxide | X | X | |
E 296 | Malic acid | X | | |
E 300 | Ascorbic acid | X | X | Meat products [7] |
E 301 | Sodium ascorbate | | X | Meat products in connection with nitrites or nitrates [7] |
E 306 | Tocopherol-rich extract | X | X | Anti-oxidant for fats and oils |
E 322 | Lecithins | X | X | Milk products [7] |
E 325 | Sodium lactate | | X | Milk-based and meat products |
E 330 | Citric acid | X | | |
E 331 | Sodium citrates | | X | |
E 333 | Calcium citrates | X | | |
E 334 | Tartaric acid (L(+)–) | X | | |
E 335 | Sodium tartrates | X | | |
E 336 | Potassium tartrates | X | | |
E 341 (i) | Monocalcium-phosphate | X | | Raising agent for self raising flour |
E 400 | Alginic acid | X | X | Milk-based products [7] |
E 401 | Sodium alginate | X | X | Milk-based products [7] |
E 402 | Potassium alginate | X | X | Milk-based products [7] |
E 406 | Agar | X | X | Milk-based and meat products [7] |
E 407 | Carrageenan | X | X | Milk-based products [7] |
E 410 | Locust bean gum | X | X | |
E 412 | Guar gum | X | X | |
E 414 | Arabic gum | X | X | |
E 415 | Xanthan gum | X | X | |
E 422 | Glycerol | X | | For plant extracts |
E 440 (i) | Pectin | X | X | Milk-based products [7] |
E 464 | Hydroxypropyl methyl cellulose | X | X | Encapsulation material for capsules |
E 500 | Sodium carbonates | X | X | "Dulce de leche" [6] and soured-cream butter [7] |
E 501 | Potassium carbonates | X | | |
E 503 | Ammonium carbonates | X | | |
E 504 | Magnesium carbonates | X | | |
E 509 | Calcium chloride | | X | Milk coagulation |
E 516 | Calcium sulphate | X | | Carrier |
E 524 | Sodium hydroxide | X | | Surface treatment of "Laugengebäck" |
E 551 | Silicon dioxide | X | | Anti-caking agent for herbs and spices |
E 553b | Talc | X | X | Coating agent for meat products |
E 938 | Argon | X | X | |
E 939 | Helium | X | X | |
E 941 | Nitrogen | X | X | |
E 948 | Oxygen | X | X | |
(b) Subsection A.4, is replaced by the following:
"A.4. Micro-organism preparations
Any preparations of micro-organisms normally used in food processing, with the exception of genetically modified micro-organisms within the meaning of Directive 2001/18/EC of the European Parliament and of the Council [9].
(c) The following Sub-section A.6. is added:
"A.6. Use of certain colours for stamping products
In the case where colours are used for stamping eggshells, Article 2(9) of Directive 94/36/EC of the European Parliament and of the Council [10] shall apply.
3. Section B is replaced by the following:
"SECTION B — PROCESSING AIDS AND OTHER PRODUCTS, WHICH MAY BE USED FOR PROCESSING OF INGREDIENTS OF AGRICULTURAL ORIGIN FROM ORGANIC PRODUCTION REFERRED TO IN ARTICLE 5(3)(D) AND ARTICLE 5(5A)(E) OF REGULATION (EEC) No 2092/91
Name | Preparation of foodstuffs of plant origin | Preparation of foodstuffs of animal origin | Specific conditions |
Water | X | X | Drinking water within the meaning of Council Directive 98/83/EC [12] |
Calcium chloride | X | | Coagulation agent |
Calcium carbonate | X | | |
Calcium hydroxide | X | | |
Calcium sulphate | X | | Coagulation agent |
Magnesium chloride (or nigari) | X | | Coagulation agent |
Potassium carbonate | X | | Drying of grapes |
Sodium carbonate | X | | Sugar(s) production |
Citric acid | X | | Oil production and hydrolysis of starch |
Sodium hydroxide | X | | Sugar(s) production Oil production from rape seed (Brassica spp) |
Sulphuric acid | X | | Sugar(s) production |
Isopropanol (propanol-2-ol) | X | | In the crystallisation process in sugar preparation; in due respect of the provisions of Council Directive 88/344/EEC, for a period expiring on 31/12/2006 |
Carbon dioxide | X | X | |
Nitrogen | X | X | |
Ethanol | X | X | Solvent |
Tannic acid | X | | Filtration aid |
Egg white albumen | X | | |
Casein | X | | |
Gelatin | X | | |
Isinglass | X | | |
Vegetal oils | X | X | Greasing, releasing or anti-foaming agent |
Silicon dioxide gel or colloïdal solution | X | | |
Activated carbon | X | | |
Talc | X | | |
Bentonite | X | X | Sticking agent for mead [11] |
Kaolin | X | X | Propolis [11] |
Diatomaceous earth | X | | |
Perlite | X | | |
Hazelnut shells | X | | |
Rice meal | X | | |
Beeswax | X | | Releasing agent |
Carnauba wax | X | | Releasing agent |
[1] OJ L 179, 14.7.1999, p. 1.
[2] OJ L 137, 25.5.2006, p. 9."
[3] OJ L 61, 18.3.1995, p. 1."
[4] Maximum levels available from all sources, expressed as SO2 in mg/l.
[5] In this context, "fruit wine" is defined as wine made from fruits other than grapes.
[6] "Dulce de leche" or "Confiture de lait" refers to a soft, luscious, brown cream, made of sweetened, thickened milk
[7] The restriction concerns only animal products.
[8] This additive can only be used if it has been demonstrated to the satisfaction of the competent authority that no technological alternative giving the same sanitary guarantees and/or allowing to maintain the specific features of the product, is available."
[9] OJ L 106, 17.4.2001, p. 1".
[10] OJ L 237, 10.9.1994, p. 13."
[11] The restriction concerns only animal products.Preparations of micro-organisms and enzymes:Any preparations of micro-organisms and enzymes normally used as processing aids in food processing, with the exception of genetically modified micro-organisms and with the exception of enzymes derived from "genetically modified organisms" within the meaning of Directive 2001/18/EC.
[12] OJ L 330, 5.12.1998, p. 32."
--------------------------------------------------
