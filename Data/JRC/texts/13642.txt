Agreement between the European Community and the Government of Canada on the processing of Advance Passenger Information and Passenger Name Record Data [1]
The procedures necessary for the entry into force of the Agreement between the European Community and the Government of Canada on the processing of Advance Passenger Information and Passenger Name Record Data, signed in Luxembourg on 3 October 2005, have been completed on 22 March 2006. According to Art. 9 of the Agreement, it entered into force on that same day.
[1] OJ L 82, 21.3.2006, p. 15.
--------------------------------------------------
