*****
COMMISSION REGULATION (EEC) No 489/89
of 24 February 1989
concerning the classification of certain goods in the combined nomenclature
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 2658/87 (1) on the tariff and statistical nomenclature and on the Common Customs Tariff, as last amended by Regulation (EEC) No 20/89 (2), and in particular Article 9,
Whereas in order to ensure uniform application of the combined nomenclature annexed to the said Regulation, it is necessary to adopt measures concerning the classification of the good referred to in the Annex to this Regulation;
Whereas Regulation (EEC) No 2658/87 has set down the general rules for the interpretation of the combined nomenclature and these rules also apply to any other nomenclature which is wholly or partly based on it or which adds any additional subdivisions to it and which is established by specific Community provisions, with a view to the application of tariff or other measures relating to trade in goods;
Whereas, pursuant to the said general rules, the goods described in column 1 of the table annexed to the presente Regulation must be classified under the appropriate CN code indicated in column 2, by virtue of the reasons set out in column 3;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the nomenclature Committee,
HAS ADOPTED THIS REGULATION:
Article 1
The good described in column 1 of the annexed table are now classified within the combined nomenclature under the appropriate CN code indicated in column 2 of the said table.
Article 2
This Regulation shall enter into force on the 21st day after its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 24 February 1989.
For the Commission
Christiane SCRIVENER
Member of the Commission
(1) OJ No L 256, 7. 9. 1987, p. 1.
(2) OJ No L 4, 6. 1. 1989, p. 19.
ANNEX
1.2.3 // // // // Description of the goods // CN code // Reasons // // // // (1) // (2) // (3) // // // // Slippers with textile uppers and outer applied soles of cotton fabric coated on the surface in contact with the floor with a visible layer of plastics // 6404 19 10 // Classification is determined by the provisions of general rules 1 and 6 for the interpretation of the combined nomenclature, Note 3 to Chapter 64 and by the texts of CN codes 6404 and 6404 19 10 The products cannot be classified in CN code 6405 20 91 as, by application of the abovementiond note, the soles must be regarded as being of plastics // // //
