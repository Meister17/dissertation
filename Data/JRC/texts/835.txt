Council Common Position
of 23 July 2001
on the fight against ballistic missile proliferation
(2001/567/CFSP)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty on European Union, and in particular Article 15 thereof,
Whereas:
(1) The European Council meeting in Göteborg on 15 and 16 June 2001 adopted a Declaration on Prevention of Proliferation of Ballistic Missiles.
(2) The Council (General Affairs) adopted on 14 May 2001 conclusions on Missile non-proliferation.
(3) The proliferation of ballistic missiles capable of carrying weapons of mass destruction is an issue of concern to the Union.
(4) The strengthening of international norms and political instruments to prevent the proliferation of weapons of mass destruction and their means of delivery is of prime importance to the Union which is committed to the reinforcement of disarmament and non-proliferation multilateral instruments.
(5) The Union sees an urgent need for a global and multilateral approach to complement existing efforts against ballistic missile proliferation.
(6) The Union therefore strongly supports the draft International Code of Conduct against ballistic missile proliferation (hereafter referred to as "the Code") which has been elaborated among the Members of the Missile Technology Control Regime (MTCR), and which has been distributed to all countries with an invitation to join this effort. The text of the Code will be reconsidered in light of the comments received from other countries.
(7) The Code is the most concrete and advanced initiative in this field and as such poses the best chance to achieve results in the short term.
(8) The Code will be a politically binding document. The Union expects that it will have a positive influence on other initiatives to address the proliferation of ballistic missiles. The Union considers that after its adoption the Code could be of interest to the United Nations,
HAS ADOPTED THIS COMMON POSITION:
Article 1
The objective of the Union shall be to curb ballistic missile proliferation. In order to achieve this objective, the Union supports the universalisation of the Code.
Article 2
The Union shall actively support an ad hoc international negotiating process to finalise the Code, leading to an International Conference for its adoption no later than 2002.
The process, which should be based on the Code, should be kept open, transparent and inclusive, enabling any State wishing to subscribe to the code to participate in its elaboration on an equal basis.
Article 3
The Union will pursue this initiative in full transparency with all other interested parties.
Article 4
This Common Position shall take effect on the date of its adoption.
Article 5
This Common Position shall be published in the Official Journal.
Done at Brussels, 23 July 2001.
For the Council
The President
A. Neyts-Uyttebroeck
