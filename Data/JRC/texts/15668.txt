Opinion of the Advisory Committee on concentrations given at its 133rd meeting on 29 June 2005 concerning a draft decision relating to Case COMP/M.3653 — SIEMENS/VA Tech
(2006/C 303/10)
The Advisory Committee agrees with the Commission that the notified operation constitutes a concentration within the meaning of Article 3(1)( b) of Regulation 139/2004 and that it has a Community dimension.
1. The Advisory Committee agrees with the Commission that for the purposes of assessing the present operation, the relevant product markets are:
In power generation:
a) the equipment for hydro power plants;
b) the provision of turnkey combined cycle gas-fired power plants;
c) the supply of gas turbines, the exact delineation of this(these) market(s) can be left open;
d) the supply of generators, the exact delineation of this(these) market(s) can be left open;
In transmission and distribution:
e) High voltage products (> 52kV);
f) Transformers;
g) Energy automation and — information;
h) Turnkey projects;
i) T&D services;
with a possible further delineation according to individual components; the exact scope of the relevant market being left open;
In rail:
j) Electrical traction for trams, metros, regional trains and locomotives;
k) Trams, metros, electrical and diesel powered regional trains and locomotives;
l) Catenary wire, the exact delineation of this(these) market(s) can be left open;
m) Rail power supply: substations, components for substations and servicing of rail power generation plants;
n) Level crossings;
Frequency inverters:
o) the exact delineation of this (these) market(s) can be left open;
In metallurgy:
p) Mechanical metallurgical plant building (limited to iron/steel or including non-ferrous metals) or mechanical metallurgical plant building per process step and metal, whereby the exact delineation of this(these) market(s) can be left open;
q) Electrical metallurgical plant building (as a whole) or electrical metallurgical plant building per process area, process step and metal, or Level 1 and 2 automation of metallurgical plants (as a whole or parts thereof, for entire metallurgy or per process step and metal), or Level 3 automation, whereby the exact delineation of this(these) market(s) can be left open;
r) Maintenance services for metallurgical plants;
s) Electrical plant building for non-metallurgical industrial plants, whereby the exact delineation of this(these) market(s) can be left open;
In LV-switchgear:
t) Fully fitted LV-switchboards, or, in the alternative, separate for the three components ACD, MCB and MCCB;
u) Components: busways, the exact delineation of this(these) market(s) can be left open;
v) Components: PLC [the exact delineation of this(these) market(s) can be left open] and load feeders;
In building technology and facility management:
w) Components for building control technology, safety technology separate for fire alarm and access/intruder control and electrical installation technology;
x) Systems: entire security systems and control systems;
y) Electrical and mechanical contracting, possibly also a market for general technical contractors;
z) Facility management, the exact delineation of this(these) market(s) can be left open;
In infrastructure and ropeways
aa) Traffic infrastructure: Street lighting, traffic lights, parking space control, the exact delineation of these markets can be left open;
bb) Traffic control, the exact delineation of this(these) market(s) can be left open;
cc) Water purification plants;
dd) Electrical equipment for ropeways, the exact delineation of these markets can be left open.
2. The Advisory Committee agrees with the Commission that for the purposes of assessing the present operation, the relevant geographic markets are as follows:
a) the markets for power generation are EEA-wide in scope;
b) the markets for T&D is EEA-wide in scope;
c) the markets for electrical traction are EEA-wide in scope;
d) the markets for trams, metros, electrical and diesel powered regional trains and locomotives are national where there is a strong national industry (here: Austria, Belgium, Germany, Poland, Czech Republic, Spain), and the EEA for the rest;
e) the market for catenary wire is national in scope;
f) the markets for rail power supply are assessed on a national basis but it can be left open whether they are national or EEA-wide;
g) the market for level crossings is assessed on a national basis;
h) the market for frequency inverters is EEA-wide in scope;
i) the markets for electrical and mechanical metallurgical plant building are at least EEA-wide in scope, the market(s) for maintenance services is/are EEA-wide in scope, and the market(s) for non-metallurgical plant building is/are national or EEA-wide in scope;
j) the markets for LV-switchgear and components are assessed on a national basis but it can be left open whether they are national or EEA-wide;
k) the markets for building technology and facility management are assessed on a national basis but it can be left open whether they are national or EEA-wide;
l) the markets for infrastructure and ropeways are assessed on a national basis but it can be left open whether they are national or EEA-wide.
3. The Advisory Committee agrees with the Commission that the notified concentration will significantly impede effective competition in a substantial part of the common market within the meaning of Article 2(3) of the Merger Regulation:
a) In the market for hydro power generation;
b) In the market for mechanical metallurgical plant building or in the markets for mechanical plant building for steelmaking and for continuous casting.
4. The Advisory Committee agrees with the Commission that the commitments submitted by the parties are sufficient to remove:
a) the competitive concern in the market for hydro power generation resulting from the horizontal overlap of the concentration;
b) the competitive concerns in the market(s) for metallurgical plant building resulting from horizontal effect of the concentration, in particular the privileged access of Siemens to strategic information of SMS Demag;
and that, as a result, the concentration should be declared compatible with the Common Market.
5. The Advisory Committee asks the Commission to take into account all the other points raised during the discussion.
--------------------------------------------------
