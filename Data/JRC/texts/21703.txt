COUNCIL DECISION of 25 February 1992 on radionavigation systems for Europe (92/143/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 84 (2) thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas the 1974 International Convention for the Safety of Life at Sea (Solas 1974) drafted under the auspices of the International Maritime Organization (IMO), requires contracting parties to arrange for the establishment and maintenance of such aids to navigation as the volume of traffic and the degree of risk warrant;
Whereas, in its Resolution A.666 (16), the IMO refers both to the need for a worldwide radionavigation system and to the possibility of regional applications of terrestrial systems such as Chayka, Decca Navigator, Loran-C, Omega and Differential Omega;
Whereas the International Association of Lighthouse Authorities confirms that, in order to meet the needs of maritime navigation, a terrestrial radionavigation system is necessary until general-purpose satellite systems are widely available, and also thereafter in parallel to such systems for the foreseeable future;
Whereas it is a concern of the Community to ensure the highest degree of safety of navigation and protection of the marine environment;
Whereas the United States Coastguard has decided to terminate its Loran-C commitments outside the United States of America as from 1994 and intends to offer the Loran-C facilities to the relevant host nations;
Whereas certain Member States intend to participate in one or more regional agreements on the establishment of Loran-C chains covering North-West Europe and the North Atlantic, the Mediterranean, the Iberian Peninsula and the Baltic, while a number of these areas are already covered by terrestrial systems, such as Decca and Omega;
Whereas Member States are not required to abandon existing radionavigation systems, such as Decca and Omega, provided they fulfil their Loran-C obligations in accordance with regional agreements;
Whereas the Loran-C system in particular satisfies international requirements and its more general application does not prejudice the development of satellite aids to navigation, especially as combined coverage by satellite and Loran-C will offer an excellent degree of system verification and continuity of radionavigation coverage for the benefit of maritime safety and environmental protection;
Whereas the establishment of regional Loran-C systems must ensure coherent and complete coverage of the European maritime area, avoiding as much as possible the imposition of additional costs upon the users of existing terrestrial radionavigation systems,
HAS ADOPTED THIS DECISION:
Article 1
1. Without prejudice to existing radionavigation systems, Member States which participate in or join regional agreements on Loran-C shall do so in a way which fulfils international objectives.
2. With regard to their participation in the regional agreements referred to in paragraph 1, Member States shall seek to achieve the radionavigation configurations which cover the widest possible geographical area in Europe and in neighbouring waters.
Article 2
The Commission shall:
- ensure coordination between the Member States participating in the regional agreements referred to in paragraph 1 with a view to ensuring compatibility between the Loran-C chains introduced at regional level,
- encourage the development of receivers, which take account of the ongoing development of satellite systems and the enhancement of the present Loran-C system,
- pursue its work with a view to setting up a radionavigation plan which takes into account the development of satellite navigation systems, of existing terrestrial systems and of the radionavigation plans of the Member States, and
- propose the necessary measures to the Council should the need arise.
Article 3
As members or observers within the International Association of Lighthouse Authorities, Member States and the Commission shall support efforts to set up a worldwide radionavigation system including European regional Loran-C chains with the purpose of enlarging worldwide Loran-C coverage in order to improve the safety of navigation and protection of the marine environment.
Article 4
This Decision is addressed to the Member States.
Done at Brussels, 25 February 1992. For the Council
The President
Vitor MARTINS
(1) OJ No C 53, 28. 2. 1991, p. 71, and OJ No C 317, 7. 12. 1991, p. 16. (2) OJ No C 280, 28. 10. 1991, p. 32. (3) OJ No C 159, 17. 6. 1991, p. 22.
