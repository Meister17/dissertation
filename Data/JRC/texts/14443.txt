Reference for a preliminary ruling from the Conseil d' Etat lodged on 2 May 2006 — Centre d'exportation du livre francais (CELF), Ministre de la culture et de la communication v Société internationale de diffusion et d'édition
Referring court
Conseil d' Etat (France)
Parties to the main proceedings
Appellants: Centre d'exportation du livre francais (CELF), Ministre de la culture et de la communication
Respondent: Société internationale de diffusion et d'édition
Questions referred
(1) Is it permissible under Article 88 (EC) for a State which has granted to an undertaking aid which is unlawful, and which the courts of that State have found to be unlawful on the ground that it had not previously been notified to the European Commission as required under Article 88(3) EC, not to recover that aid from the economic operator which received it on the ground that, after receiving a complaint from a third party, the Commission declared that aid to be compatible with the rules of the common market, thus effectively exercising its exclusive right to determine such compatibility?
(2) If that obligation to repay the aid is confirmed, must the periods during which the aid in question was declared by the European Commission to be compatible with the rules of the common market, before those decisions were annulled by the Court of First Instance of the European Communities, be taken into account for the purpose of calculating the sums to be repaid?
--------------------------------------------------
