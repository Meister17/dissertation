ANNEX
"Imports shall fulfil the appropriate animal and public health requirements.
PART 1
LIVE ANIMALS, FRESH MEAT AND MEAT PRODUCTS
>TABLE>
B= bovines (including buffalo and bison)
S/G= sheep/goat
P= pig
E= equidae
C/H= clovenhoofed animals
x= authorised
o= unauthorised
s= suspended, from 15 February 2000, for export of fresh meat and meat products for human consumption
Special remarks
Additional notes
(a) Imports of live bovine animals are restricted to animals intended for reproduction and to veal calves aged under two weeks intended for fattening.
(b) Imports of beef and veal intended for human consumption are restricted to:
(i) meat from cows which have been used exclusively for dairy production, or
(ii) meat:
- complying with the conditions agreed between the United States of America and the European Community;
and
- which has been obtained from meat establishments supplied with slaughter animals from holdings approved by the Commission. The names of these establishments are the subject of a specific communication from the Commission to the Member States."
COMMISSION DECISION
of 18 February 2000
amending Commission Decision 92/160/EEC with regard to imports of equidae from Brazil
(notified under document number C(2000) 365)
(Text with EEA relevance)
(2000/163/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 90/426/EEC of 26 June 1990 on animal health conditions governing the movement and imports from third countries of equidae(1), as last amended by the Act of Accession of Austria, Finland and Sweden, and in particular Article 13(2) thereof,
Whereas:
(1) Commission Decision 92/160/EEC(2), as last amended by Decision 1999/558/EC(3), establishes the regionalisation of certain third countries for imports of equidae.
(2) The States Alagoas and Pernambuco of Brazil are included in the list of States of Brazil in the Annex to Decision 92/160/EEC from where Member States authorise imports of equidae.
(3) Brazil reported cases of glanders in working horses in certain districts of the States Alagoas and Pernambuco. The origin of the infection remains so far unknown.
(4) In accordance with Community legislation Member States are authorised to import equidae from third countries or in the case of official regionalisation from parts of the territory of a third country which have been free from glanders for the past six months prior to export. It is therefore appropriate to adapt the regionalisation to the disease situation in the country concerned.
(5) The measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
The words "Alagoas, Pernambuco" are deleted from the list of States of Brazil in the Annex of Decision 92/160/EEC.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 18 February 2000.
For the Commission
David BYRNE
Member of the Commission
(1) OJ L 224, 18.8.1990, p. 42.
(2) OJ L 71, 18.3.1992, p. 27.
(3) OJ L 211, 11.8.1999, p. 53.
