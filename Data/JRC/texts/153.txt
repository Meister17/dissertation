*****
COMMISSION REGULATION (EEC) No 231/89
of 30 January 1989
amending Regulation (EEC) No 3143/85 on the sale at reduced prices of intervention butter intended for direct consumption in the form of concentrated butter
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 804/68 of 27 June 1968 on the common organization of the market in milk and milk products (1), as last amended by Regulation (EEC) No 1109/88 (2), and in particular Article 6 (7) thereof,
Whereas Commission Regulation (EEC) No 3143/85 (3), as last amended by Regulation (EEC) No 3036/88 (4), introduces a scheme for the sale at reduced prices of intervention butter intended for direct consumption in the form of concentrated butter;
Whereas, in view of the market situation and of the reduction of public stocks of butter, the reductions in the prices at which the butter is sold by the intervention agencies under this scheme should be adjusted; whereas experience indicates that the maximum net content of the packs of concentrated butter should be reduced;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Milk and Milk Products,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EEC) No 3143/85 is hereby amended as follows:
1. In Article 2 (1):
- '240 ECU' is replaced by 'ECU 225',
- '238 ECU' is replaced by 'ECU 223',
and in Article 2 (4):
- '300 ECU' is replaced by 'ECU 285'.
2. In Article 5 (5) '10 kilograms' is replaced by 'three kilograms'.
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Communities.
In Article 1:
- point 1 shall apply from 1 February 1989, and
- point 2 to butter put up in packs from 1 April 1989 onwards.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 30 January 1989.
For the Commission
Ray MAC SHARRY
Member of the Commission
(1) OJ No L 148, 28. 6. 1968, p. 13.
(2) OJ No L 110, 29. 4. 1988, p. 27.
(3) OJ No L 298, 12. 11. 1985, p. 9.
(4) OJ No L 271, 1. 10. 1988, p. 93.
