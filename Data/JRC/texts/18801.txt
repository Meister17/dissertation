COUNCIL REGULATION (EEC) No 2617/93 of 21 September 1993 amending Regulation (EEC) No 1907/90 on certain marketing standards on eggs
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 2771/75 of 29 October 1975 on the common organization of the market in eggs (1), and in particular Article 2 (2) thereof,
Having regard to the proposal from the Commission,
Whereas Regulation (EEC) No 1907/90 (2) lays down certain marketing standards for eggs;
Whereas the provisions regarding direct deliveries from producers to packing stations, certain markets and food industry undertakings should also apply for such deliveries to the non-food industry;
Whereas the delivery of down-graded eggs should be restricted to food industry undertakings approved in accordance with Council Directive 89/437/EEC of 20 June 1989 on hygiene and health problems affecting the production and the placing on the market of egg products (3) in order to assure proper handling of such eggs;
Whereas it should be made clear that optional indications serving publicity purposes on egg packs may comprise symbols and refer to eggs and to other items;
Whereas experience has shown that provisions for the indication of dates on grade A eggs and egg packs containing them should be modified to require mandatory indication of the date of minimum durability as for other foodstuffs in conformity with Council Directive 79/112/EEC of 18 December 1978 on the approximation of the laws of the Member States relating to the labelling, presentation and advertising of foodstuffs (4); whereas for imported grade A eggs with a view to facilitate control, packs should also bear the indication of the packing date,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EEC) No 1907/90 is hereby amended as follows:
1. In Article 2 (2) points (a) and (b), the following phrase shall be added:
'and to the non-food industry'.
2. Article 4 (1) point (a) shall be replaced by the following:
'(a) eggs to anyone but collectors, packing centres, markets within the meaning of Article 2 (2) (a), food industry undertakings approved in accordance with Directive 89/437/EEC and the non-food industry';
3. In Articles 6 (1) and 8 (3), the term 'food industry' shall be replaced by 'food industry undertakings approved in accordance with Directive 89/437/EEC'.
4. Article 7 (a) shall be replaced by the following:
'(a) the date of minimum durability ("best-before date")'.
5. Article 10 (1) point (a) shall be replaced by the following:
'(a) the name or the business name, and address of the undertaking which has packed the eggs or had them packed; the name, business name or the trade mark used by that undertaking, which may be a trade mark used collectively by a number of undertakings, may be shown if it contains no statement or symbol incompatible with this Regulation relating to the quality or freshness of the eggs, to the type of farming used for their production or to the origin of the eggs;'
6. Article 10 (1) (e) shall be replaced by the following:
'(e) the date of minimum durability ("best-before date") followed by appropriate storage recommendations for grade A eggs, and the packing date for eggs of other grades'.
7. Article 10 (2) point (e) shall be replaced by the following:
'(e) statements or symbols designed to promote sales of eggs or other items, provided that such statements or symbols and the manner in which they are made are not likely to mislead the purchaser'.
8. Article 13 (2) shall be replaced by the following:
'(2) in the case of loose egg sales, the identification number of the packing centre which graded the eggs, or in the case of imported eggs the third country of origin, and the date of minimum durability followed by the appropriate storage recommendations shall also be indicated'.
9. Article 15 (b) (ee) shall be replaced by the following:
'(ee) the date of packing and the date of minimum durability followed by appropriate storage recommendations, for grade A eggs, and the packing date for eggs of other grades'.
Article 2
In accordance with the procedure laid down in Article 17 of Regulation (EEC) No 2771/75, the Commission shall lay down the transitional measures necessary for the implementation of this Regulation, in particular those referring to the use of existing packing material.
Article 3
This Regulation shall enter into force on 1 December 1993.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 21 September 1993.
For the Council
The President
A. BOURGEOIS
(1) OJ L 282, 1. 11. 1975, p. 49. Regulation as last amended by Regulation (EEC) No 1574/93 (OJ L 152, 24. 6. 1993, p. 1).
(2) OJ L 173, 6. 7. 1990, p. 5.
(3) OJ L 212, 22. 7. 1989, p. 87. Directive as last amended by Directive 91/684/EEC (OJ L 376, 31. 12. 1991, p. 38).
(4) OJ L 33, 8. 2. 1979, p. 1. Directive as last amended by Directive 91/72/EEC (OJ L 42, 15. 2. 1991, p. 27).
