Action brought on 26 July 2006 — IBERDROLA v Commission of the European Communities
Parties
Applicant: IBERDROLA S.A. (Bilbao, Spain) (represented by: J. Alfaro Aguila-Real, P. Liñán Hernández, S. Martínez Lage, H. Brokelmann and J. Ruiz Calzado, lawyers)
Defendant: Commission of the European Communities
Form of order sought
- Annul the contested decision;
- Order the Commission to pay all the costs of the proceedings.
Pleas in law and main arguments
The present action is directed against the Decision of the European Commission of 25 April 2006 (Case No COMP/M.4110 — E.ON/ENDESA) declaring compatible with the common market the concentration whereby E.ON AG proposes to acquire control of the whole of the shares in ENDESA SA.
The applicant claims in that regard that, in its opinion, the contested decision disregards the serious risks that coordinated effects will be produced in the relations of competition between the principal operators with a pan-European presence, which must be examined in a transaction of this magnitude and having these characteristics, that it affects the dominant undertakings in two of the main national energy markets.
In support of its claims, the applicant maintains that the defendant has:
- infringed the principle of sound administration by carrying out a biased and inadequate investigation of the operation of the affected markets and of the impact of the transaction in those markets;
- made an error of law by basing its examination of the transaction on a consideration of the national markets, contrary to the Merger Regulation and the Community case-law;
- failed to fulfil its obligation to ensure the coherent application of the rules on the control of concentrations and the abuse of a dominant position in examining the notified transaction, authorising it without having examined the origin of the funds which E.ON proposed to use for the acquisition of ENDESA, in order to determine whether such a concentration could be the result of an abuse of a dominant position;
- made a series of manifest errors of assessment and ignored certain relevant elements by concluding that the compatibility of the transaction does not raise serious doubts and adopting the decision authorising it in Phase 1. The applicant emphasises in that regard that, although it was adopted on the basis of the new Merger Regulation, which requires a more sophisticated and rigorous analysis, the contested decision:
- wholly ignores the negative impact which the transaction will have on the process of the integration of the national wholesale markets, which will culminate in the completion of the single European market in gas and electricity,
- did not sufficiently evaluate the impact of the proposed transaction on the emergent pan-European market for the supply of electricity to multinational undertakings,
- considers that, in spite of its pan-European objective and its economic strength, E.ON was not a significant potential competitor of ENDESA in the Spanish markets for electricity generation and the supply of gas,
- concludes that the transaction will not strengthen a dominant position in the German market and that the disappearance of a recent entrant, ENDESA, does not alter the competitive context.
Finally the applicant claims that there has been a breach of the duty to state the reasons on which measures are based.
--------------------------------------------------
