*****
COUNCIL DIRECTIVE
of 16 June 1988
amending Annex II to Directive 86/280/EEC on limit values and quality objectives for discharges of certain dangerous substances included in List I of the Annex to Directive 76/464/EEC
(88/347/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 130 S thereof,
Having regard to Council Directive 76/464/EEC of 4 May 1976 on pollution caused by certain dangerous substances discharged into the aquatic environment of the Community (1), and in particular Articles 6 and 12 thereof,
Having regard to Council Directive 86/280/EEC of 12 June 1986 on limit values and quality objectives for discharges of certain dangerous substances included in List I of the Annex to Directive 76/464/EEC (2),
Having regard to the proposal from the Commission (3),
Having regard to the opinion of the European Parliament (4),
Having regard to the opinion of the Economic and Social Committee (5),
Whereas, in order to protect the aquatic environment of the Community against pollution by certain dangerous substances, Article 3 of Directive 76/464/EEC introduces a system of prior authorization laying down emission standards for discharges of the substances in List I in the Annex thereto; whereas Article 6 of the said Directive provides that limit values shall be laid down for such emission standards and also quality objectives for the aquatic environment affected by discharges of the substances;
Whereas Member States are required to apply the limit values except in cases where they may employ quality objectives;
Whereas Directive 86/280/EEC will have to be amended and supplemented, on proposals from the Commission, in line with developments in scientific knowledge relating principally to the toxicity, persistence and accumulation of the substances referred to in living organisms and sediments, or in the event of an improvement in the best technical means available; whereas it is necessary, for that purpose, to provide for additions to the said Directive, relating to measures in respect of other dangerous substances, and for amendments to the content of Annex II;
Whereas, on the basis of the criteria laid down in Directive 76/464/EEC, aldrin, dieldrin, endrin, isodrin, hexachlorobenzene, hexachlorobutadiene and chloroform should be made subject to the provisions of Directive 86/280/EEC,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Annex II to Council Directive 86/280/EEC is amended as follows:
1. The following are added below the title:
'4. Relating to aldrin, dieldrin, endrin and isodrin
5. Relating to hexachlorobenzene
6. Relating to hexachlorobutadiene
7. Relating to chloroform'.
2. The following sections are added:
'IV. Specific provisions relating to:
- aldrin (No 1) (1) CAS-No 309-00-2
- dieldrin (No 71) (2) CAS-No 60-57-1
- endrin (No 77) (3) CAS-No 72-20-8
- isodrin (No 130) (4) CAS-No 465-73-6
(1) Aldrin is the chemical compound C12H8Cl6
1, 2, 3, 4, 10, 10-hexachloro-1, 4, 4a, 5, 8, 8a-hexahydro-1, 4-endo-5, 8-exo-dimethanonaphtalene.
(2) Dieldrin is the chemical compound C12H8C16O
1, 2, 3, 4, 10, 10-hexachloro-6, 7-epoxy-1, 4, 4a, 5, 6, 7, 8, 8a-octahydro-1, 4-endo-5, 8-exo-dimethanonaphtalene.
(3) Endrin is the chemical compound C12H8C16O
1, 2, 3, 4, 10, 10-hexachloro-6, 7-epoxy-1, 4, 4a, 5, 6, 7, 8, 8a-octahydro-1, 4-endo-5, 8-endo-dimethanonaphtalene.
(4) Isodrin is the chemical compound C12H8C16
1, 2, 3, 4, 10, 10-hexachloro-1, 4, 4a, 5, 8, 8a-hexahydro-1, 4-endo-5, 8-endo-dimethanonaphtalene.
Heading A (1, 71, 77, 130): Limit values for emission standards (1)
1.2.3,4.5 // // // // // Type of industrial plant (2) // Type of average value // Limit value expressed as // To be complied with as from // 1.2.3.4.5 // // // Weight // Concentration in effluent mg/l of water discharged (3) // // // // // // // Production of aldrin and/or dieldrin and/or endrin including formulation of these substances on the same site // Monthly // 3 g per tonne of total production capacity (g/tonne) // 2 // 1. 1. 1989 // // Daily // 15 g per tonne of total production capacity (g/tonne) (4) // 10 (4) // 1. 1. 1989 // // // // //
(1) The limit values indicated in this heading shall apply to the total discharge of aldrin, dieldrin and endrin.
If the effluent resulting from the production or use of aldrin, dieldrin and/or endrin (including formulation of these substances) also contains isodrin, the limit values laid down above shall apply to the total discharges of aldrin, dieldrin, endrin and isodrin.
(2) Among the industrial plants referred to under heading A, point 3, of Annex I, reference is made in particular to plants formulating aldrin, and/or dieldrin and/or endrin away from the production site.
(3) These figures take account of the total amount of water passing through the plant.
(4) If possible, daily values should not exceed twice the monthly value.
Heading B (1, 71, 77, 130): Quality objectives
1.2.3,4 // // // // Environment // Substance // Quality objectives ng/l to be complied with as from // 1.2.3.4 // // // 1. 1. 1989 // 1. 1. 1994 // // // // // Inland surface waters Estuary waters // Aldrin Dieldrin // 30 for the four substances in total with a maximum of 5 for endrin // 10 10 // Internal coastal waters other than estuary waters // Endrin // // 5 // Territorial waters // Isodrin // // 5 // // // //
Standstill: The concentration(s) of aldrin and/or dieldrin and/or endrin and/or isodrin in sediments and/or molluscs and/or shellfish and/or fish must not increase significantly with time. Heading C (1, 71, 77, 130): Reference method of measurement
1. The reference method of measurement to be used for determining aldrin, dieldrin, endrin and/or isodrin in effluents and the aquatic environment is gas chromatography with electron-capture detection after extraction by means of an appropriate solvent. The limit of determination (1) for each substance is 2,5 ng/l for the aquatic environment and 400 ng/l for effluents, depending on the number of parasite substances present in the sample.
2. The reference method to be used for determining aldrin, dieldrin and/or endrin and/or isodrin in sediments and organisms is gas chromatography with electron-capture detection after appropriate preparation of samples. The limit of determination is 1 mg/kg dry weight for each separate substance.
3. The accuracy and precision of the method must be ± 50 % at a concentration which represents twice the value of the limit of determination.
(1) The "limit of determination" x g of a given substance is the smallest quantity, quantitatively determinable in a sample on the basis of a given working method, which can still be distinguished from zero.
V. Specific provisions relating to hexachlorobenzene (HCB) (No 83)
CAS-118-74-1
Heading A (83): Limit values for emission standards
Standstill: There must be no significant direct or indirect increase over time in pollution arising from discharges of HCB and affecting concentrations in sediments and/or molluscs and/or shellfish and/or fish.
1.2.3,4.5 // // // // // Type of industrial plant (1) (2) (3) // Type of average value // Limit values expressed as // To be complied with as from // 1.2.3.4.5 // // // weight // concentration // // // // // // // 1. HCB production and processing // monthly // 10 g HCB/tonne of HCB production capacity // 1 mg/l of HCB // // // daily // 20 g HCB/tonne of HCB production capacity // 2 mg/l of HCB // 1. 1. 1990 // // // // // // 2. Production of perchloro ethylene (PER) and carbon tetrachloride (CCl4) by perchlorination // monthly // 1,5 g HCB/tonne of PER + CCl4 total production capacity // 1,5 mg/l of HCB // // // daily // 3 g HCB/tonne of PER + CCl4 total production capacity // 3 mg/l of HCB // 1. 1. 1990 // // // // // // 3. Production of trichloroethylene and/or perchloroethylene by any other process (4) // monthly // - // - // - // // daily // - // - // -
// // // // // (1) A simplified monitoring procedure may be introduced if annual discharges do not exceed 1 kg a year.
(2) Among the industrial plants referred to in Annex I, heading A, point 3, reference is made in particular to industrial plants producing quintozene and tecnazene, industrial plants producing chlorine by chlor-alkali electrolysis with graphite electrodes, industrial rubber processing plants, plants manufacturing pyrotechnic products and plants producing vinylchloride.
(3) On the basis of experience gained in implementing the Directive, and taking into account the fact that the use of best technical means already makes it possible to apply in some cases much more stringent values than those indicated above, the Council shall decide, on the basis of proposals from the Commission, upon more stringent limit values, such decision to be taken by 1 January 1995.
(4) It is not possible at present to adopt limit values for this sector. The Council shall adopt such limit values at a later stage, acting on a proposal from the Commission. In the meantime, Member States will apply national emission standards in accordance with Annex I, heading A, point 3.
(1) OJ No L 129, 18. 5. 1976, p. 23.
(2) OJ No L 181, 4. 7. 1986, p. 16.
(3) OJ No C 146, 12. 6. 1979, p. 5, OJ No C 309, 3. 12. 1986, p. 3, OJ No C 314, 26. 11. 1987, p. 5 and OJ No C 70, 18. 3. 1985, p. 15.
(4) OJ No C 122, 9. 5. 1988 and OJ No C 120, 20. 5. 1986, p. 164.
(5) OJ No C 232, 31. 8. 1987, p. 2, OJ No C 356, 31. 12. 1987, p. 69 and OJ No C 188, 29. 7. 1985, p. 19.
Heading B (83): Quality objectives (1)
Standstill: The concentration of HCB in sediments and/or molluscs and/or shellfish and/or fish must not increase significantly with time.
(1) The Commission shall keep under review the possibility of setting more stringent quality objectives, taking into account measured concentrations of HCB in sediments and/or molluscs and/or shellfish and/or fish, and will report to the Council, by 1 January 1995, for decision as to whether any changes should be made to the Directive.
1.2.3.4 // // // // // Environment // Quality objective // Unit of measurement // To be complied with as from // // // // // Inland surface waters // // // // Estuary waters // // // // Internal coastal waters other than estuary waters // 0,03 // mg/l // 1. 1. 1990 // Territorial waters // // // // // // //
Heading C (83): Reference method of measurement
1. The reference method of measurement to be used for determining the presence of HCB in effluents and waters is gas chromatography with electron-capture detection after extraction by means of an appropriate solvent.
The limit of determination (1) for HCB shall be within the range 1 to 10 ng/l for waters and 0,5 to 1 mg/l for effluents depending on the number of extraneous substances present in the sample.
2. The reference method to be used for determining HCB in sediments and organisms is gas chromatography with electron-capture detection after appropriate preparation of the sample. The limit of determination (1) shall be within the range 1 to 10 mg/kg of dry matter.
3. The accuracy and precision of the method must be ± 50 % at a concentration which represents twice the value of the limit of determination (1).
(1) The "limit of determination" x g of a given substance is the smallest quantity, quantitatively determinable in a sample on the basis of a given working method, which can still be distinguished from zero.
VI. Specific provisions relating to hexachlorobutadiene (HCBD) (No 84)
CAS-87-68-3
Heading A (84): Limit values for emission standards
Standstill: There must be no significant direct or indirect increase over time in pollution arising from discharges of HCB and affecting concentrations in sediments and/or molluscs and/or shellfish and/or fish.
1.2.3,4.5 // // // // // Type of industrial plant (1) (2) (3) // Type of average value // Limit values expressed as // To be complied with as from // 1.2.3.4.5 // // // weight // concentration // // // // // // // 1. Production of perchloroethylene (PER) and carbon tetrachloride (CCl4) by perchlorination // monthly // 1,5 g HCBD/tonne of total production capacity of PER + CCl4 // 1,5 mg/l of HCBD // // // daily // 3 g HCBD/tonne of total production capacity of PER + CCl4 // 3 mg/l of HCBD // 1. 1. 1990 // // // // // // 2. Production of trichloroethylene and/or perchloroethylene by any other process (4) // monthly // - // - // - // // daily // - // - // -
// // // // // (1) A simplified monitoring procedure may be introduced if annual discharges do not exceed 1 kg a year.
(2) Among the industrial plants referred to in Annex I, heading A, point 3, reference is made in particular to industrial plants using HCBD for technical purposes.
(3) On the basis of experience gained in implementing this Directive, and taking into account the fact that the use of best technical means already makes it possible to apply in some cases much more stringent values than those indicated above, the Council shall decide, on the basis of proposals from the Commission, upon more stringent limit values, such decision to be taken by 1 January 1995.
(4) It is not possible at present to adopt limit values for this sector. The Council shall adopt such limit values at a later stage, acting on a proposal from the Commission. In the meantime, Member States will apply national emission standards in accordance with Annex I, heading A, point 3.
Heading B (84): Quality objectives (1)
Standstill: The concentration of HCBD in sediments and/or molluscs and/or shellfish and/or fish must not increase significantly with time.
(1) The Commission shall keep under review the possibility of setting more stringent quality objectives, taking into account measured concentrations of HCBD in sediments and/or molluscs and/or shellfish and/or fish, and will report to the Council, by 1 January 1995, for decision as to whether any changes should be made to the Directive.
1.2.3.4 // // // // // Environment // Quality objective // Unit of measurement // To be complied with as from // // // // // Inland surface waters // // // // Estuary waters // // // // Internal coastal waters other than estuary waters // 0,1 // mg/l // 1. 1. 1990 // Territorial waters // // // // // // //
Heading C (84): Reference method of measurement
1. The reference method of measurement to be used for determining HCBD in effluents and waters is gas chromatography with electron-capture detection after extraction by means of an appropriate solvent.
The limit of determination (1) for HCBD shall be within the range 1 to 10 ng/l for waters and 0,5 to 1 mg/l for effluents, depending on the number of extraneous substances present in the sample.
2. The reference method to be used for determining HCBD in sediments and organisms is gas chromatography with electron-capture detection after appropriate preparation of the sample. The limit of determination (1) shall be within the range 1 to 10 mg/kg of dry matter. 3. The accuracy and precision of the method must be ± 50 % at a concentration which represents twice the value of the limit of determination (1).
(1) The "limit of determination" x g of a given substance is the smallest quantity, quantitatively determinable in a sample on the basis of a given working method, which can still be distinguished from zero.
VII. Specific provisions relating to chloroform (CHCl3) (No 23) (1)
CAS-67-66-3
Heading A (23): Limit values for emission standards
1.2,3.4 // // // // Type of industrial plant (2) (3) // Limit value (monthly averages) expressed as (4) (5) // To be complied with as from // 1.2.3.4 // // weight // concentration // // // // // // 1. Production of chloromethanes from methanol or from a combination of methanol and methane (6) // 10 g CHCl3/tonne of total production capacity of chloromethanes // 1 mg/l // 1. 1. 1990 // // // // // 2. Production of chloromethanes by chlorination of methane // 7,5 g CHCl3/tonne of total production capacity of chloromethanes // 1 mg/l // 1. 1. 1990 // 3. Production of chlorofluorocarbon (CFC) (7) // - // - // -
// // // // (1) In the case of chloroform, Article 3 of Directive 76/464/EEC shall apply to discharges from industrial processes which may in themselves contribute significantly to the level of chloroform in the aqueous effluent; in particular it shall apply to those mentioned under Heading A of this Annex. Article 5 of this Directive applies if sources other than those listed in this Annex are identified.
(2) Among the industrial plants referred to under heading A, point 3 of Annex I, special reference is made, in the case of chloroform, to plants manufacturing monomer vinyl chloride using dichlorethane pyrolysis, those producing bleached pulp and other plants using CHCl3 as a solvent and plants in which cooling waters or other effluents are chlorinated. The Council shall adopt limit values for these sectors at a later stage, acting on proposals from the Commission.
(3) A simplified monitoring procedure may be introduced if annual discharges do not exceed 30 kg a year.
(4) Daily average limit values are equal to twice the monthly average values.
(5) In view of the volatility of chloroform and in order to ensure compliance with Article 3 (6), where a process involving agitation in the open air of effluent containing chloroform is used, the Member States shall require compliance with the limit values upstream of the plant concerned; they shall ensure that all water likely to be polluted is taken fully into account.
(6) I.e. by hydrochlorination of methanol, then chlorination of methyl chloride.
(7) It is not possible at present to adopt limit values for this sector. The Council shall adopt such limit values at a later date, acting on a proposal from the Commission. In the meantime, Member States will apply national emission standards in accordance with Annex I, heading A, point 3.
Heading B (23): Quality objectives (1)
(1) Without prejudice to Article 6 (3) of Directive 76/464/EEC, where there is no evidence of any problem in meeting and continuously maintaining the quality objective set out above, a simplified monitoring procedure may be introduced.
1.2.3.4 // // // // // Environment // Quality objectives // Unit of measurement // To be complied with as from // // // // // Inland surface waters // // // // Estuary waters // // // // Internal coastal waters other than estuary waters // 12 // mg/l // 1. 1. 1990 // Territorial waters // // // // // // //
Heading C (23): Reference method of measurement
1. The reference method of measurement to be used for determining the presence of chloroform in effluents and the aquatic environment is gas chromatography.
A sensitive detector must be used when concentration levels are below 0,5 mg/l and in this case the determination limit (1) is 0,1 mg/l. For concentration levels higher than 0,5 mg/l a determination limit of 0,1 mg/l is acceptable.
2. The accuracy and precision of the method must be ± 50 % at a concentration which represents twice the value of the determination limit.
(1) The "determination limit" x g of a given substance is the smallest quantity, quantitatively determinable in a sample on the basis of a given working method, which can still be distinguished from zero.'
Article 2
Member States shall take the measures necessary to comply with this Directive by 1 January 1989 with regard to aldrin, dieldrin, endrin and isodrin, and by 1 January 1990 with regard to the other substances. They shall forthwith inform the Commission thereof.
Member States shall communicate to the Commission, the provisions of national law which they adopt in the field governed by this Directive.
Article 3
This Directive is addressed to the Member States.
Done at Luxembourg, 16 June 1988.
For the Council
The President
K. TOEPFER
