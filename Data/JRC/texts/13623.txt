Air traffic services on a route with limited traffic rights
(2006/C 127/06)
The Civil Aviation Administration adopted the following decision on the basis of Section 61 of the Air Traffic Act of 3 March 1995 (281/1995) and with reference to Regulation (EC) No 847/2004 of the European Parliament and of the Council of 29 April 2004 on the negotiation and implementation of air service agreements between Member States and third countries.
1. Scope
This decision lays down more detailed provisions on the issue of permits to EC air carriers to operate a route subject to limited traffic rights (hereinafter: route with limited traffic rights). For the purposes of this decision, however, public service obligations imposed pursuant to Article 4 of Council Regulation (EEC) No 2408/92 of 23 July 1992 on access for Community air carriers to intra-Community air routes will not be regarded as a limitation.
2. Traffic rights and air carriers' need to use them
2.1 Information on traffic rights and their use on routes between Finland and States which are not Members of the European Union and with which Finland has concluded air traffic agreements is available from the Civil Aviation Administration's aviation policy unit, and on the Administration's website (www.ilmailulaitos.fi).
2.2 The aviation policy unit shall publish, on the Civil Aviation Administration's website, information on forthcoming air traffic negotiations between Finland and third countries.
2.3 EC air carriers interested in operating air services on a route with limited traffic rights or on a route between Finland and a State which is not a Member of the European Union and with which Finland has not concluded an air traffic agreement may inform the aviation policy unit of their plans and requirements. Information received by the aviation policy unit will be taken into account when preparing Finland's negotiations on air traffic agreements.
3. Applying for a permit
3.1 When an EC air carrier expresses interest in unused capacity on a route with limited traffic rights, the Civil Aviation Administration's aviation policy unit shall issue a notice inviting applications for a permit to use the route. The notice shall be sent electronically to all EC air carriers operating traffic services in, to and/or from Finland, or which have asked the aviation policy unit for information on unused capacity.
The notice shall also be published on the Civil Aviation Administration's website. The notice shall specify the deadline for applying for a permit.
3.2 Applications for a permit shall contain the following information as a minimum:
(a) a copy of the air carrier's operating licence;
(b) a description of the air traffic services being planned for the route (number of trips per week, aeronautical equipment, any stopovers, annual or seasonal nature of the service);
(c) the start date for the planned service;
(d) the nature of the service (passenger, freight or other);
(e) the accessibility of the services and customer support (tickets sales network, internet-based services, etc.);
(f) any connecting traffic;
(g) the pricing policy for the route.
3.3 Applications for permits shall be written in Finnish or Swedish and submitted to the aviation policy unit by the specified deadline.
4. Permits to use traffic rights
4.1 The Civil Aviation Administration's aviation policy unit shall grant a permit for a route with limited traffic rights to the applicant(s) considered to be the best after an overall assessment taking account of the need for passenger and/or freight services, the promotion of competition and the balanced development of Community air traffic. A permit to operate traffic services shall remain in force until further notice or for a period specified by the aviation policy unit.
4.2 During the assessments resulting in the selection of air carriers, the aviation policy unit shall carry out a financial analysis of the various alternatives from the point of view of passenger and freight traffic. The analysis shall describe the market and competition situation on the route.
4.3 When assessing the applications, the aviation policy unit may arrange a public hearing which all the applicants must be given an opportunity to attend. The aviation policy unit shall draw up minutes of the hearing for distribution to all the air carriers which applied for a permit for the route in question. Applicants may submit written comments on the minutes within a specified period. If a public hearing is arranged, the aviation policy unit may not reach a decision about granting permits before the deadline for the submission of comments has expired.
4.4 Decisions concerning the granting of permits are published in accordance with Section 54 of the Administration Act (434/2003) and also published on the Civil Aviation Administration's website.
4.5 Decisions taken by the Civil Aviation Administration's aviation policy unit concerning the granting of permits are administrative decisions, amendments to which may be proposed in accordance with Section 3(3) of the Civil Aviation Administration Act (1123/1990).
5. Reassessment of permits to use traffic rights
5.1 The Civil Aviation Administration's aviation policy unit may reconsider a decision to grant a permit to use traffic rights. Permits must always be reassessed at the request of an EC air carrier operating traffic services in Finland, from Finland and/or to Finland. A reassessment may not be carried out within five years of a permit being granted or of a previous reassessment, however.
5.2 The permit holder must be informed of any reassessment, notice of which must also be published on the Civil Aviation Administration's website. The notice shall specify a deadline by which EC air carriers interested in using the traffic rights covered by the permit must submit permit applications.
5.3 When reassessing permits to use traffic rights, paragraphs 3.2, 3.3 and 4 shall be applied in such a way that existing permits are not affected if the traffic rights covered by the permit are being used effectively and in accordance with European Community competition law and the corresponding national legislation.
5.4 Notwithstanding paragraph 5.1, a permit for a route with limited traffic rights which was granted before this decision entered into force and which applies to a route for which only one air carrier may be selected in accordance with the provisions of the air traffic agreement in question, may be reassessed three years after this decision enters into force.
6. Cancellation and withdrawal of permits
6.1 If the traffic for which the permit has been granted
- is not started in the scheduling period following the day specified for the start of the traffic service, or
- if the traffic is interrupted and not resumed within the next two full scheduling periods,
and the permit holder has not shown, within a reasonable period specified by the Civil Aviation Administration's aviation policy unit, that this is due to exceptional circumstances beyond his control, the permit shall be cancelled.
6.2 A permit shall be cancelled if the permit holder informs the Civil Aviation Administration's aviation policy unit in writing that he no longer plans to use the traffic rights covered by the permit.
6.3 The Civil Aviation Administration's aviation policy unit may withdraw a permit completely or for a certain period, or limit the activities covered by the permit, if the permit holder:
- fails to operate the traffic service in accordance with the terms of the permit;
- fails to observe the provisions in the air traffic agreement on the basis of which the permit was granted, or other international obligations; or
- fails to comply with the terms for the operation of the traffic service and regulations governing such activity.
7. Entry into force
This decision shall enter into force on 2 August 2004. The Civil Aviation Administration, Vanda, 24 June 2004.
--------------------------------------------------
