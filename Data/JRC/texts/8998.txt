Reference for a preliminary ruling from the Consiglio di Stato (Italy) lodged on 27 June 2006 — Autorità Garante della Concorrenza e del Mercato v Ente Tabacchi Italiani — ETI SpA, Philip Morris Products SA, Philip Morris Holland BV, Philip Morris GmbH, Philip Morris Products Inc. Philip Morris International Management SA
Referring court
Consiglio di Stato
Parties to the main proceedings
Applicant: Autorità Garante della Concorrenza e del Mercato
Defendants: Autorità Garante della Concorrenza e del Mercato v Ente Tabacchi Italiani — ETI SpA, Philip Morris Products SA, Philip Morris Holland BV, Philip Morris GmbH, Philip Morris Products Inc. Philip Morris International Management SA
Questions referred
1. What, in accordance with Article 87 et seq. of the Treaty and with the general principles of Community law, is the criterion to be adopted in determining the undertaking on which a penalty is to be imposed for contravention of the rules in the sphere of competition when, in connection with conduct penalised as a whole, the last part of those actions was carried out by an undertaking having succeeded the original undertaking in the economic sphere concerned whenever the original body, while still in existence, no longer operates as a commercial undertaking, or at least not in the economic sector affected by the penalty?
2. Does it fall to the national authority responsible for the application of "anti-trust" rules, when determining the person to be penalised, to assess at its own discretion whether circumstances exist which warrant the attribution to the economic successor of responsibility for contraventions of the competition rules committed by the legal person which it has succeeded, even when that latter has not ceased to exist at the date of the decision, so that the effectiveness of the competition rules is not compromised by alterations made to the legal form of the undertakings?
--------------------------------------------------
