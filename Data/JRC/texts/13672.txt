Action brought on 16 June 2006 — Lesniak v Commission
Parties
Applicant: Christophe Lesniak (Brussels, Belgium) (represented by: S. Orlandi, A. Coolen, J.-N. Louis and E. Marchal, lawyers)
Defendant: Commission of the European Communities
Form of order sought
- declare that Article 12 of Annex XIII to the Staff Regulations is unlawful;
- annul the decision of 8 August 2005 appointing the applicant as an official of the European Communities, inasmuch as it fixes his classification at Grade A*6, step 2, and the taking effect of his seniority in step at 1 September 2005;
- order the Commission of the European Communities to pay the costs.
Pleas in law and main arguments
As a successful candidate in competition PE/99/A, the notice of which had been published under the former Staff Regulations, the applicant, at the time a member of the temporary staff in Grade A6 (now A*10), was recruited as an official after the entry into force of the new Staff Regulations and classified in Grade A*6.
In addition to raising pleas very similar to those raised in Case F-12/06 [1], the applicant maintains that the Commission should have awarded him the same classification which he enjoyed as a member of the temporary staff in accordance with Article 5(4) of Annex XIII to the Staff Regulations. According to the applicant, that provision should also apply to successful candidates in open competitions.
[1] OJ C 86, 8.4.2006, p. 48.
--------------------------------------------------
