Communication from the French Government concerning Directive 94/22/EC of the European Parliament and of the Council of 30 May 1994 on the conditions for granting and using authorisations for the prospection, exploration and production of hydrocarbons [1]
(Notice regarding an application for an exclusive licence to prospect for liquid and gaseous hydrocarbons, designated the "Rigny-le-Ferron Licence")
(2006/C 27/05)
(Text with EEA relevance)
By request of 10 June 2005, Madison Energy France S.C.S., a company with registered offices at 13/15 boulevard de la Madeleine, F-75001 Paris, applied for an exclusive licence, designated the "Rigny-le-Ferron Licence", to prospect for liquid and gaseous hydrocarbons in an area of around 335 km2 covering part of the departments of Aube and Yonne.
The perimeter of the area covered by this licence is made up of the meridian and parallel arcs successively joining the vertices defined below by their geographical coordinates, the original meridian being that of Paris.
Vertices | Longitude | Latitude |
A | 1,10 degrees E | 53,60 degrees N |
B | 1,20 degrees E | 53,60 degrees N |
C | 1,20 degrees E | 53,70 degrees N |
D | 1,30 degrees E | 53,70 degrees N |
E | 1,30 degrees E | 53,60 degrees N |
F | 1,50 degrees E | 53,60 degrees N |
G | 1,50 degrees E | 53,50 degrees N |
H | 1,10 degrees E | 53,50 degrees N |
Submission of applications
The initial applicant and competing applicants must prove that they comply with the requirements for obtaining the licence, specified in Articles 3, 4 and 5 of Decree No 95-427 of 19 April 1995, as amended, concerning mining rights.
Interested companies may, within 90 days of the publication of this notice, submit a competing application in accordance with the procedure summarised in the "Notice regarding the granting of mining rights for hydrocarbons in France" published in Official Journal of the European Communities C 374 of 30 December 1994, p. 11, and established by Decree No 95-427 of 19 April 1995, as amended, concerning mining rights (Journal officiel de la République française, 22 April 1995).
Competing applications must be sent to the Minister responsible for mines at the address below. Decisions on the initial application and competing applications will be taken within two years of the date on which the French authorities received the initial application, i.e. by 10 June 2007 at the latest.
Conditions and requirements regarding performance of the activity and cessation thereof
Applicants are referred to Articles 79 and 79.1 of the mining code and to Decree No 95-696 of 9 May 1995, as amended, on the start of mining operations and the mining regulations (Journal officiel de la République française, 11 May 1995).
Further information can be obtained from the Ministry of Economic Affairs, Finance and Industry (Directorate-General for Energy and Raw Materials, Directorate for Energy and Mineral Resources, Bureau of Mining Legislation), 61, boulevard Vincent Auriol, Télédoc 133, F-75703 Paris Cedex 13 (tel. (33-1) 44 97 23 02, fax: (33-1) 44 97 05 70).
The abovementioned laws and regulations can be consulted at http://www.legifrance.gouv.fr
[1] OJ L 164, 30.6.1994, p. 3.
--------------------------------------------------
