Commission Regulation (EC) No 40/2005
of 13 January 2005
amending the rates of the refunds applicable to certain milk products exported in the form of goods not covered by Annex I to the Treaty
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1255/1999 of 17 May 1999 on the common organisation of the markets in the milk and milk products sector [1], and in particular Article 31(3) thereof,
Whereas:
(1) The rates of the refunds applicable from 17 December 2004 to the products listed in the Annex, exported in the form of goods not covered by Annex I to the Treaty, were fixed by Commission Regulation (EC) No 2153/2004 [2].
(2) It follows from applying the rules and criteria contained in Regulation (EC) No 2153/2004 to the information at present available to the Commission that the export refunds at present applicable should be altered as shown in the Annex hereto,
HAS ADOPTED THIS REGULATION:
Article 1
The rates of refund fixed by Regulation (EC) No 2153/2004 are hereby altered as shown in the Annex hereto.
Article 2
This Regulation shall enter into force on 14 January 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 13 January 2005.
For the Commission
Günter Verheugen
Vice-President
--------------------------------------------------
[1] OJ L 160, 26.6.1999, p. 48. Regulation as last amended by Regulation (EC) No 1787/2003 (OJ L 270, 21.10.2003, p. 121).
[2] OJ L 370, 17.12.2004, p. 41.
--------------------------------------------------
+++++ ANNEX 1 +++++</br>
