Commission Decision
of 8 September 2004
on the aid scheme implemented by Italy providing for tax credits for investments
(notified under document number C(2004) 2638)
(Only the Italian text is authentic)
(Text with EEA relevance)
(2005/655/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community, and in particular the first subparagraph of Article 88(2) thereof,
Having regard to the Agreement on the European Economic Area, and in particular Article 62(1)(a) thereof,
Having called on interested parties to submit their comments pursuant to the provisions cited above [1],
Whereas:
I. PROCEDURE
(1) In April 2003 the Italian authorities notified the Commission, pursuant to Article 88(3) of the Treaty, of an aid scheme for investments in selected areas. As the entry into force of the scheme was not made subject to prior approval by the Commission, it was registered as illegal aid under number NN 53/03.
(2) Additional information was submitted by the Italian authorities on 4 July 2003. On 17 September 2003 the Commission decided to initiate the formal investigation procedure pursuant to Article 88(2) of the Treaty (the decision initiating the procedure) and requested Italy to provide all the necessary information for assessing the measure. The Commission decision was published in the Official Journal of the European Union [2]. No comments were received from interested parties or from Italy.
(3) On 16 December 2003 the Commission again invited Italy to provide information, indicating that, in the absence of a reply, it was empowered to adopt an information injunction pursuant to Article 10(3) of Council Regulation (EC) No 659/1999 of 22 March 1999 laying down detailed rules for the application of Article 93 of the EC Treaty [3]. No information was received from Italy. By decision of 22 April 2004, the Commission enjoined Italy to submit all the relevant information necessary for the complete assessment of the measure within one month. No information was received from Italy within the deadline.
(4) On 26 May 2004 Italy requested an extension of 15 working days in order to reply to the information injunction. The Commission, while informing Italy that the deadline set by Commission decision had expired, acknowledged Italy’s intention to submit information by 17 June 2004.
(5) By letter dated 17 June 2004 (registered on 22 June 2004), Italy submitted additional information.
II. DESCRIPTION OF THE AID
(6) The legal basis for the aid scheme is Article 94(14) of Law No 289 of 27 December 2002 (Finance Act for 2003).
(7) The aim of the aid scheme is to promote investments in specific areas of the Italian territory, for regional development objectives. The scheme is to last until 31 December 2006 and the annual budget for 2003, 2004 and 2005 is EUR 2 million.
(8) As in the case of two other Italian regional aid schemes approved by the Commission [4], of which the notified scheme is in practice a territorial extension, the aid is given in the form of a tax credit for investments in depreciable assets. The aid is granted for "net" investments, identified as the difference between (1) the amount of investments in new assets over a given period and (2) the amount of sales and depreciation of all the enterprise’s assets in the same period. The aid intensity is indicated as 8 % nge, with a top-up of 10 percentage points gross for small enterprises and 6 percentage points gross for medium-sized enterprises.
(9) Beneficiaries of the aid scheme are enterprises belonging to a wide range of sectors [5] that carry out investments in the areas listed in Article 4 of Law No 448 of 23 December 1998 and in Ministry of Finance Circular No 161 of 25 August 2000. These areas are:
- the provinces of Agrigento, Avellino, Bari, Benevento, Brindisi, Cagliari, Caltanissetta, Campobasso, Caserta, Catania, Catanzaro, Cosenza, Crotone Enna, Foggia, Frosinone, Isernia, Lecce, Massa, Matera, Messina, Napoli, Nuoro, Oristano, Palermo, Potenza, Reggio Calabria, Salerno, Sassari Siracusa, Taranto, Trapani, Vibo Valentia and Viterbo;
- the municipalities of Tivoli (Rome), Formia (Latina), Sora (Frosinone) and Cassino (Frosinone).
(10) The scheme does not apply to the production, processing and marketing of agricultural products, to the transport sector or to the steel industry as defined in the multisectoral framework on regional aid for large investment projects [6]. Neither does it apply to firms in difficulty as defined in the Community guidelines on state aid for rescuing and restructuring firms in difficulty [7]. A detailed description of all the conditions that enterprises must meet to qualify for the aid is given in the decision initiating the procedure [8].
III. GROUNDS FOR INITIATING THE PROCEDURE
(11) In the decision initiating the procedure, the Commission considered that the measure constituted state aid within the meaning of Article 87(1) of the Treaty and expressed doubts regarding its compatibility with the common market, as it provides for regional investment aid in areas which are not eligible for the regional derogations in Article 87(3)(a) and (c) of the Treaty. A detailed description of the grounds for initiating the procedure, which were not challenged by Italy, is given in the decision initiating the procedure [9].
IV. ASSESSMENT OF THE AID
1. Existence of aid
(12) The scheme provides for tax credits for investments in specific areas, listed in legislative and administrative provisions. The Commission considers that this confers an economic advantage on certain undertakings through state resources and that it distorts competition and affects trade between Member States, for the reasons set out in the decision initiating the procedure [10].
(13) Consequently, the measure under examination constitutes aid within the meaning of Article 87(1) of the Treaty and is in principle prohibited. It can be considered compatible with the common market only if it qualifies for one of the derogations provided for in the Treaty.
2. Legality of the aid
(14) In so far as the measure constitutes aid, the Commission regrets that, by putting the measure into effect before approval by the Commission, Italy has not fulfilled its obligation under Article 88(3) of the Treaty.
3. Compatibility of the aid
(15) The aid scheme does not qualify for the derogations in Article 87(2) of the Treaty or for the derogations in Article 87(3)(b) and (d), for obvious reasons, as already indicated in the decision initiating the procedure [11]. As the aid scheme concerns the promotion of investments for regional development, and was notified by Italy as regional aid, the Commission has examined its compatibility with the common market in the light of the Community guidelines on national regional aid [12].
(16) In this respect, in the decision initiating the procedure the Commission expressed several doubts as to the compatibility of the aid scheme with the common market. The Commission considers that all the doubts expressed at that time are confirmed. In particular:
(a) The scheme provides for aid to be granted in areas that are not eligible for the derogations in Article 87(3)(a) and (c) of the Treaty. On the basis of the Italian regional aid map for 2000-2006 [13], the provinces of Campobasso, Frosinone, Isernia, Massa Carrara and Viterbo and the municipalities of Tivoli and Formia are not entirely eligible for the derogations in question. Therefore, no regional aid scheme can be applied in these areas;
(b) The scheme appears to provide for aid also in areas that are eligible for the derogations in Article 87(3)(a) and (c) of the Treaty. However, in these areas aid schemes providing for an identical tax credit for investments have already been approved by the Commission [14] and can be applied. The Commission therefore considers it possible that the scheme under examination will be applied mainly in the areas indicated in point (a) above;
(c) The scheme does not clearly indicate the commitment to comply with the specific rules laid down in the guidelines for the examination of state aid to fisheries and aquaculture [15], in Council Regulation (EC) No 2792/1999 of 17 December 1999 laying down the detailed rules and arrangements regarding Community structural assistance in the fisheries sector [16] and in Council Regulation (EC) No 2369/2002 of 20 December 2002 amending Regulation (EC) No 2792/1999 laying down the detailed rules and arrangements regarding Community structural assistance in the fisheries sector [17].
(17) The Commission was unaware of whether aid had been awarded on the basis of the scheme and in which areas, since Italy had not provided any information. The Commission therefore issued an information injunction. Pursuant to Regulation (EC) No 659/1999, where, despite an information injunction, the Member State concerned does not provide the information requested the Commission can take a decision on unlawful aid on the basis of the information available.
(18) Italy did not reply to the information injunction within the deadline set by Commission decision. However, the Commission intends to take into account the information submitted by Italy outside that deadline, in which Italy stated that:
- the aid scheme has so far not been applied,
- no undertaking has received aid under the scheme because, since its inception in 2002, the corresponding administrative instructions have not been adopted, nor has the necessary tax code for using the tax credit procedure been established.
(19) The Commission takes note of Italy’s statement that no undertaking has received aid under the scheme. It also notes that the legislative provisions introducing the tax credit entered into force on 1 January 2003, conferring the right to the tax incentive without reference to subsequent administrative instructions, and are still in force. Italy has not given any commitment or indication as to the withdrawal of the aid scheme in the future. The Commission therefore considers it appropriate to take a decision on the scheme.
(20) On the basis of the information available, the regional aid scheme under examination is applicable in areas which are not eligible for regional aid. The Commission cannot consider the aid scheme compatible with the common market on the basis of the Community guidelines on national regional aid, for the reasons given in paragraphs 15 and 16 above and set out in detail in the decision initiating the procedure [18].
V. CONCLUSION
(21) The Commission finds that the measure under examination constitutes state aid within the meaning of Article 87(1) of the Treaty. Italy has unlawfully implemented the aid in question, in breach of Article 88(3) of the Treaty. On the basis of the analysis carried out in points 15 to 20 above, the Commission considers that the aid is incompatible with the common market.
(22) This Decision, concerning the aid scheme providing for tax credits for investment and any cases in which it has been applied, must be implemented immediately, including recovery of any individual award of incompatible aid already granted,
HAS ADOPTED THIS DECISION:
Article 1
1. The state aid scheme providing for tax credits for investments, which Italy has unlawfully put into effect in breach of Article 88(3) of the Treaty, is incompatible with the common market.
2. Italy shall withdraw the aid scheme in question, in so far as it continues to have effect, and shall from the date of this Decision refrain from making any aid payment under the scheme.
Article 2
In so far as any aid has already been granted under the scheme referred to in Article 1, Italy shall take all necessary measures to recover it from the recipients.
Recovery shall be effected without delay in accordance with the procedures of national law provided that they allow the immediate and effective implementation of this Decision.
The aid to be recovered shall include interest from the date on which it was made available to the recipient until the date of its recovery.
Interest shall be calculated in accordance with the provisions of Commission Regulation (EC) No 794/2004 [19].
Article 3
Italy shall inform the Commission, within two months of notification of this Decision, of the action taken to comply with it, indicating in detail the measures adopted to withdraw the incompatible scheme and providing documentation on those measures.
Article 4
This Decision is addressed to the Italian Republic.
Done at Brussels, 8 September 2004.
For the Commission
Neelie Kroes
Member of the Commission
[1] OJ C 300, 11.12.2003, p. 2.
[2] See footnote 1.
[3] OJ L 83, 27.3.1999, p. 1. Regulation as amended by 2003 Act of Accession.
[4] N 324/02 for areas eligible for the derogation in Article 87(3)(a) of the Treaty, and N 198/03 for areas eligible for the derogation in Article 87(3)(c) of the Treaty.
[5] Extraction and manufacturing activities, services, tourism, commerce, construction, production and distribution of electricity, steam and hot water, and processing of fisheries and aquaculture products.
[6] OJ C 70, 19.3.2002, p. 8.
[7] OJ C 288, 9.10.1999, p. 2.
[8] See footnote 1.
[9] See footnote 1.
[10] See footnote 1.
[11] See footnote 1.
[12] OJ C 74, 10.3.1998, p. 9.
[13] Commission Decision 2002/282/EC of 20 September 2000 on the part of the Italian regional aid map for the period 2000 to 2006 concerning the areas eligible for the derogation in Article 87(3)(c) of the Treaty (OJ L 105, 20.4.2002, p. 1).
[14] N 324/02 for areas eligible for the derogation in Article 87(3)(a) of the Treaty, and N 198/03 for areas eligible for the derogation in Article 87(3)(c) of the Treaty.
[15] OJ C 19, 20.1.2001, p. 7.
[16] OJ L 337, 30.12.1999, p. 10. Regulation as last amended by Regulation (EC) No 485/2005 (OJ L 81, 30.3.2005, p. 1).
[17] OJ L 358, 31.12.2002, p. 49.
[18] See footnote 1.
[19] OJ L 140, 30.4.2004, p. 1.
--------------------------------------------------
