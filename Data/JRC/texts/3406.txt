Commission Regulation (EC) No 1068/2005
of 6 July 2005
amending Regulation (EC) No 824/2000 establishing procedures for the taking-over of cereals by intervention agencies and laying down methods of analysis for determining the quality of cereals
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1784/2003 of 29 September 2003 on the common organisation of the market in cereals [1], and in particular Article 6 thereof,
Whereas:
(1) Regulation (EC) No 1784/2003 provides for no further intervention for rye from the 2004/05 marketing year. Commission Regulation (EC) No 824/2000 [2] should therefore be adapted to take account of this new situation.
(2) Common wheat and durum wheat are covered by minimum quality criteria for human consumption and must satisfy the health standards laid down by Council Regulation (EEC) No 315/93 of 8 February 1993 laying down Community procedures for contaminants in food [3]. The other cereals are mainly intended for animal feed and must comply with Directive 2002/32/EC of the European Parliament and of the Council of 7 May 2002 on undesirable substances in animal feed [4]. Provision should be made for those standards to be applicable when the products concerned are taken over under the present intervention scheme.
(3) As from 1 July 2006, some of those standards are applicable when the products are first processed. In order to guarantee that cereals taken over before that date may be marketed under optimum conditions at the time of supply following their removal from the intervention scheme, provision should be made for products offered for intervention to comply with those standards as from the 2005/06 marketing year.
(4) The potential for mycotoxin formation has proved to be linked to specific conditions, identifiable essentially on the basis of the weather conditions recorded during the period of growth and, in particular, flowering of the cereals.
(5) The risks entailed by exceeding the maximum thresholds for admissible contaminants can be identified by the intervention agencies on the basis of the information received from applicants and their own analysis criteria. In order to limit the financial costs, therefore, there is justification for requiring analyses, on the responsibility of the intervention agencies prior to the taking-over of the products, only on the basis of a risk analysis enabling the quality of the products to be guaranteed on entry into the intervention scheme.
(6) Articles 2 and 5 of Council Regulation (EEC) No 3492/90 of 27 November 1990 laying down the factors to be taken into consideration in the annual accounts for the financing of intervention measures in the form of public storage by the European Agricultural Guidance and Guarantee Fund, Guarantee Section [5], lay down the rules on responsibility. Those Articles specify in particular that Member States are to take all measures necessary to ensure the proper preservation of products which have been the subject of Community intervention and that quantities which have deteriorated on account of the normal physical storage conditions or by reason of overlong preservation are to be recorded in the accounts as having left the intervention stock on the date when the loss or deterioration was established. They also specify that a product is to be deemed to have deteriorated if it no longer meets the quality requirements applicable when it was bought in. Consequently, only such deterioration as that laid down in those provisions may be covered by the Community budget. Where a decision taken by a Member State at the time of purchase of a product is inadequate in the light of the risk analysis required by these rules, that Member State should therefore be liable if it later emerges that the product did not comply with the minimum standards. Such a decision would not make it possible to guarantee the quality of the product and, therefore, ensure its proper preservation. Consequently, the circumstances in which a Member State is to be held liable should be specified.
(7) For the purpose of determining the quality of the cereals offered for intervention, Article 3 of Regulation (EC) No 824/2000 sets out a list of methods according to the criteria for analysis. One of those methods, the Hagberg falling number test, has been adapted by the International Organisation for Standardisation. The relevant reference should be adapted. The analysis methods for assessing compliance with contaminant standards should also be specified.
(8) For reasons of clarity and precision, Article 6 of Regulation (EC) No 824/2000 requires redrafting, particularly as regards the order of the relevant provisions. Taking account of the principle of risk analysis adopted for mycotoxin control, there is justification for including analyses for determining mycotoxin rates among those whose costs are payable by the applicant.
(9) Regulation (EC) No 824/2000 should be amended accordingly.
(10) The Management Committee for Cereals has not delivered an opinion within the time limit set by its chairman,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 824/2000 is hereby amended as follows:
1. The first paragraph of Article 1 is replaced by the following:
"During the periods referred to in Article 5(2) of Regulation (EC) No 1784/2003, any holder of a homogeneous batch of not less than 80 tonnes of common wheat, barley, maize or sorghum or 10 tonnes of durum wheat, harvested within the Community, shall be entitled to offer the batch to the intervention agency."
2. The first and second subparagraphs of Article 2(2) are replaced by the following:
"The cereals shall be considered sound, fair and of marketable quality if they are of the typical colour of the cereal in question, are free from abnormal smell and live pests (including mites) at every stage of their development, if they meet the minimum quality requirements set out in Annex I hereto, and if their levels of contaminants, including radioactivity, do not exceed the maximum levels permitted under Community regulations. The maximum contaminant level which must not be exceeded shall be as follows:
- for common wheat and durum wheat, those permitted under Council Regulation (EEC) No 315/93 [6], including the requirements regarding the Fusarium-toxin level for common wheat and durum wheat laid down in points 2.4 to 2.7 of Annex I to Commission Regulation (EC) No 466/2001 [7].
- for barley, maize and sorghum, those set by Directive 2002/32/EC of the European Parliament and of the Council [8].
Member States shall check levels of contaminants, including radioactivity, on the basis of a risk analysis, taking account in particular of the information supplied by the applicant and the commitments of the latter regarding compliance with the standards set, especially in the light of the results of the analyses. If necessary, the rate and scope of the controls shall be determined in accordance with the procedure laid down in Article 25 of Regulation (EC) No 1784/2003, particularly where the market situation may be seriously disrupted by contaminants.
3. Article 3 is amended as follows:
(a) point 3.7 is replaced by the following:
"3.7. the method for determining the Hagberg falling number (amylase activity test) shall comply with ISO 3093:2004;"
(b) the following point 3.10 is added:
"3.10. the sampling methods and reference analysis methods for determining mycotoxin rates shall be those mentioned in Annex I to Regulation (EC) No 466/2001".
4. Article 6 is replaced by the following:
"Article 6
1. The intervention agency shall see that the physical and technical characteristics of the samples taken are analysed under its responsibility within 20 working days of the representative sample being made up.
2. The offerer shall bear the costs relating to:
(a) determination of the tannin content of sorghum,
(b) the amylasic activity (Hagberg) test,
(c) determination of the protein content of durum wheat and common wheat,
(d) the Zeleny test,
(e) the machinability test,
(f) analyses of contaminants.
3. If the analyses referred to in paragraph 1 show that the cereals offered do not meet the minimum quality required for intervention, those cereals shall be withdrawn at the offerer's expense. The offerer shall also bear all the costs incurred.
4. In cases of dispute, the intervention agency shall have the necessary tests on the cereals in question carried out again, the cost being met by the losing party."
5. Article 9 is amended as follows:
(a) Points (c) and (d) shall be replaced by the following:
"(c) where the percentage of broken grains exceeds 3 % for durum wheat, common wheat and barley, and 4 % for maize and sorghum, a reduction of EUR 0,05 shall be applied for each additional 0,1 percentage point;
(d) where the percentage of grain impurities exceeds 2 % for durum wheat, 4 % for maize and sorghum, and 5 % for common wheat and barley, a reduction of EUR 0,05 shall be applied for each additional 0,1 percentage point;"
(b) Point (f) is replaced by the following:
"(f) where the percentage of miscellaneous impurities (Schwarzbesatz) exceeds 0,5 % for durum wheat and 1 % for common wheat, barley, maize and sorghum, a reduction of EUR 0,1 shall be applied for each additional 0,1 precentage point."
6. The following paragraph 3 is added to Article 10:
"3. Where the checks provided for under this Regulation are to be carried out on the basis of the risk analysis referred to in the second subparagraph of Article 2(2), the Member States shall be liable for the financial consequences of any failure to comply with the maximum admissible contaminant level. Such liability shall be established, without prejudice to any action which the Member State may itself take against the offerer or storekeeper, in the event of a failure to respect their commitments or obligations.
However, in the case of ochratoxin A and aflatoxin, if the Member State concerned is able to prove to the Commission’s satisfaction that the standards were met on entry, that normal storage conditions were observed and that the storekeeper’s other commitments were respected, the financial liability shall be borne by the Community budget."
7. In Annex I, the "rye" column is deleted.
8. Annex II is amended as follows:
(a) In point 1.2(a), the first paragraph is replaced by the following:
"grains which, after elimination from the sample of all other matter referred to in this Annex, pass through sieves with apertures of the following dimensions: common wheat 2,0 mm, durum wheat 1,9 mm, barley 2,2 mm."
(b) Point 2.3 is deleted.
9. Annex III, point 1 is amended as follows:
(a) The first subparagraph is replaced by the following:
"For common wheat, durum wheat and barley, an average sample of 250 g shall be passed through two sieves, one with slotted perforations of 3,5 mm and the other with slotted perforations of 1,0 mm, for half a minute each."
(b) The seventh subparagraph is replaced by the following:
"The partial sample shall be passed for half a minute through a sieve with a mesh size of 2,0 mm for common wheat, 1,9 mm for durum wheat and 2,2 mm for barley. Matter which passes through this sieve shall be considered as shrivelled grains. Grains damaged by frost and unripe green grains shall belong to the "shrivelled grains" group."
10. In footnote 2 of Annex IV, the second paragraph is replaced by the following:
"Ventilation should be such that, when small-grained cereals (common wheat, durum wheat, barley and sorghum) are dried for two hours and maize for four hours, the results from all the test samples of semolina or, as the case may be, maize that the heating chamber can hold differ by less than 0,15 % from the results obtained after drying small-grained cereals for three hours and maize for five hours."
Article 2
This Regulation shall enter into force on the seventh day following that of its publication in the Official Journal of the European Union.
However, the provisions on Fusarium-toxins and the method for checking contaminant levels introduced by point 2 shall be applicable only to cereals harvested and taken over as from the 2005/06 marketing year.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 6 July 2005.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 270, 21.10.2003, p. 78.
[2] OJ L 100, 20.4.2000, p. 31. Regulation as last amended by Commission Regulation (EC) No 777/2004 (OJ L 123, 27.4.2004, p. 50).
[3] OJ L 37, 13.2.1993, p. 1. Regulation as amended by Regulation (EC) No 1882/2003 of the European Parliament and of the Council (OJ L 284, 31.10.2003, p. 1).
[4] OJ L 140, 30.5.2002, p. 10. Directive as last amended by Commission Directive 2005/8/EC (OJ L 27, 29.1.2005, p. 44).
[5] OJ L 337, 4.12.1990, p. 3.
[6] OJ L 37, 13.02.1993, p. 1.
[7] OJ L 77, 16.3.2001, p. 1. Regulation as last amended by Commission Regulation (EC) No 856/2005 (OJ L 143, 7.6.2005, p. 3).
[8] OJ L 140, 30.5.2002, p. 10."
--------------------------------------------------
