Council Regulation (EC) No 2003/2004
of 21 October 2004
on the conclusion of the Protocol defining, for the period 3 December 2003 to 2 December 2007, the fishing opportunities and the financial compensation provided for by the Agreement between the European Economic Community and the Government of Mauritius on fishing in Mauritian waters
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 37 in conjunction with Article 300(2) and the first subparagraph of Article 300(3) thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament [1],
Whereas:
(1) In accordance with Article 12(3) of the Agreement between the European Economic Community and the Government of Mauritius on fishing in Mauritian waters [2], the Contracting Parties held negotiations, at the end of the period of application of the Protocol, in order to determine by common accord the terms of the Protocol for the following period and where appropriate, any necessary amendments or additions to the Annex.
(2) As a result of these negotiations, a new Protocol defining, for the period 3 December 2003 to 2 December 2007, the fishing opportunities and the financial compensation provided for by the said Agreement was initialled on 11 September 2003.
(3) It is in the Community's interest to approve this Protocol.
(4) The method for allocating the fishing opportunities among Member States should be defined on the basis of the traditional allocation of fishing opportunities under the Fisheries Agreement,
HAS ADOPTED THIS REGULATION:
Article 1
The Protocol defining, for the period 3 December 2003 to 2 December 2007, the fishing opportunities and the financial compensation provided for by the Agreement between the European Economic Community and the Government of Mauritius on fishing in Mauritian waters is hereby approved on behalf of the European Community.
The text of the Protocol is attached to this Regulation [3].
Article 2
The fishing opportunities fixed in the Protocol shall be allocated among the Member States as follows:
- tuna seiners: France 16, Spain 22, Italy 2, United Kingdom 1,
- surface long-liners: Spain 19, France 23, Portugal 7,
- vessels fishing by line: France 25 gross registered tonnes (GRT) per month on an annual average.
If licence applications from these Member States do not cover all the fishing opportunities fixed by the Protocol, the Commission may take into consideration licence applications from any other Member State.
Article 3
The Member States whose vessels fish under this Agreement shall notify the Commission of the quantities of each stock caught within the Mauritius fishing zone in accordance with Commission Regulation (EC) No 500/2001 [4].
Article 4
The President of the Council is hereby authorised to designate the persons empowered to sign the Protocol in order to bind the Community.
Article 5
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Luxembourg, 21 October 2004.
For the Council
The President
G. Zalm
[1] Opinion delivered on 15 September 2004 (not yet published in the Official Journal).
[2] OJ L 159, 10.6.1989, p. 2.
[3] See page 3 of this Official Journal.
[4] OJ L 73, 15.3.2001, p. 8.
--------------------------------------------------
