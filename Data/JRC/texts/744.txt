Commission communication
concerning the guidelines on State aid for employment
(2000/C 371/05)
(Text with EEA relevance)
The Commission will continue to assess employment aid schemes against the criteria set out in the existing guidelines(1), which will therefore remain in force, until the entry into force of revised guidelines on aid for employment or of an exemption regulation on aid for employment under Council Regulation (EC) No 994/98 of 7 May 1998(2) on the application of Articles 92 and 93 (now 87 and 88) of the Treaty to certain categories of horizontal State aid.
The report foreseen in paragraph 30 of the existing guidelines, including an invitation to comment on any issues raised in it, is available on the Internet at:
http://europa.eu.int/comm/competition/
(1) OJ C 334, 12.12.1995.
(2) OJ L 142, 14.5.1998.
