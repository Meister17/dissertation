Council Regulation (EC) No 2166/2005
of 20 December 2005
establishing measures for the recovery of the Southern hake and Norway lobster stocks in the Cantabrian Sea and Western Iberian peninsula and amending Regulation (EC) No 850/98 for the conservation of fishery resources through technical measures for the protection of juveniles of marine organisms
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 37 thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament [1],
Whereas:
(1) Recent scientific advice from the International Council for the Exploration of the Sea (ICES) has indicated that the Southern hake and Norway lobster stocks in ICES Divisions VIIIc and IXa have been subjected to levels of mortality by fishing which have eroded the quantities of mature individuals in the sea to the extent that these stocks may not be able to replenish themselves by reproduction, and as result are threatened with collapse.
(2) Measures should be taken to establish multi-annual plans for the recovery of these stocks in conformity with Article 5 of Council Regulation (EC) No 2371/2002 of 20 December 2002 on the conservation and sustainable exploitation of fisheries resources under the Common Fisheries Policy [2].
(3) The objective of the plans should be to rebuild these stocks to safe biological limits within 10 years.
(4) The objective should be considered to be achieved when the stocks concerned are assessed by the Scientific, Technical and Economic Committee for Fisheries (STECF) to be within safe biological limits in the light of the most recent advice from ICES.
(5) In order to achieve that objective, the levels of the fishing mortality rates should be controlled so that the rates may be reduced from year to year.
(6) Such control of the fishing mortality rates can be achieved by establishing an appropriate method for the establishment of the level of Total Allowable Catches (TACs) of the stocks concerned, and a system including closed areas and limitations on kilowatt-days whereby fishing efforts on those stocks are restricted to levels at which the TACs may not be exceeded.
(7) Once recovery has been achieved, the Council should decide on a proposal from the Commission on follow-up measures in accordance with Article 6 of Regulation (EC) No 2371/2002.
(8) Control measures in addition to those laid down in Council Regulation (EEC) No 2847/93 of 12 October 1993 establishing a control system applicable to the common fisheries policy [3] should be included in order to ensure compliance with the measures laid down in this Regulation.
(9) The recovery of Norway lobster stocks requires certain areas of reproduction of the species to be protected from fishing. Therefore Regulation (EC) No 850/98 [4] should be amended accordingly,
HAS ADOPTED THIS REGULATION:
CHAPTER I
SUBJECT MATTER AND OBJECTIVE
Article 1
Subject matter
This Regulation establishes a recovery plan for the following stocks (hereinafter referred to as the stocks concerned):
(a) the Southern hake stock which inhabits Divisions VIIIc and IXa, as delineated by the International Council for the Exploration of the Sea (ICES);
(b) the Norway lobster stock which inhabits ICES Division VIIIc;
(c) the Norway lobster stock which inhabits ICES Division IXa.
Article 2
Objective of the recovery plan
The recovery plan shall aim to rebuild the stocks concerned to within safe biological limits, in keeping with ICES information. This shall mean:
(a) as regards the stock referred to in Article 1(a), reaching a spawning stock biomass of 35000 tonnes during two consecutive years, according to the available scientific reports, or increasing the quantities of mature individuals within a period of 10 years so that values are reached equal to or higher than 35000 tonnes. This figure shall be adjusted in the light of new scientific data from the STECF;
(b) as regards the stocks referred to in Article 1(b) and (c), rebuilding the stocks to within safe biological limits within a period of 10 years.
Article 3
Evaluation of recovery measures
1. The Commission shall, on the basis of advice from ICES and STECF, evaluate the impact of the recovery measures on the stocks concerned and the fisheries on those stocks in the second year of application of this Regulation and in each of the following years.
2. Where the Commission finds, on the basis of the annual evaluation, that any of the stocks concerned have reached the objective set out in Article 2, the Council shall decide by qualified majority on a proposal from the Commission to replace, for that stock, the recovery plan provided for in this Regulation by a management plan in accordance with Article 6 of Regulation (EC) No 2371/2002.
3. Where the Commission finds, on the basis of the annual evaluation, that any of the stocks concerned do not show proper signs of recovery, the Council shall decide by qualified majority on a proposal from the Commission on additional and/or alternative measures in order to ensure recovery of the stock concerned.
CHAPTER II
TOTAL ALLOWABLE CATCHES
Article 4
Setting of TACs
1. Each year, the Council shall decide by qualified majority on the basis of a proposal from the Commission on a TAC for the following year for the stocks concerned.
2. The TAC for the stock referred to in Article 1(a) shall be set in accordance with Article 5.
3. The TACs for the stocks referred to in Article 1(b) and (c) shall be set in accordance with Article 6.
Article 5
Procedure for setting the TAC for the Southern hake stock
1. Where the fishing mortality rate for the stock referred to in Article 1(a) has been estimated by the STECF in the light of the most recent report of ICES to be above 0,3 per year, the TAC shall not exceed a level of catches which, according to a scientific evaluation carried out by the STECF in the light of the most recent report of ICES, will result in a reduction of 10 % in the fishing mortality rate in the year of its application as compared with the fishing mortality rate estimated for the preceding year.
2. Where the fishing mortality rate for the stock referred to in Article 1(a) has been estimated by the STECF in the light of the most recent report of ICES to be equal to or below 0,3 per year, the TAC shall be set at a level of catches which, according to a scientific evaluation carried out by the STECF in the light of the most recent report of ICES, will result in a fishing mortality rate of 0,27 per year in the year of its application.
3. Where STECF, in the light of the most recent report of ICES, is able to calculate a level of catches corresponding to the mortality rates specified in paragraphs 1 and 2 for only a part of ICES Divisions VIIIc and IXa, the TAC shall be set at a level that is compatible with both:
(a) the level of catch corresponding to the specified mortality rate in the area covered by the scientific advice, and
(b) maintaining a constant ratio of catches between that area covered by the scientific advice and the totality of Divisions VIIIc and IXa. The ratio shall be calculated on the basis of catches in the three years preceding the year in which the decision is taken.
The method of calculation used shall be that provided in the Annex to this Regulation.
Article 6
Procedure for setting the TACs for the Norway lobster stocks
Based on the latest scientific evaluation of the STECF, the TACs for the stocks referred to in Article 1(b) and (c) shall be set at a level that will result in the same relative change in its fishing mortality rate as the change in fishing mortality rate achieved for the stock referred to in Article 1(a) when applying Article 5.
Article 7
Constraints on variation in TACs
As from the first year of application of this Regulation, the following rules shall apply:
(a) where application of Article 5 or Article 6 would result in a TAC which exceeds the TAC of the preceding year by more than 15 %, the Council shall adopt a TAC which shall not be more than 15 % greater than the TAC of that year;
(b) where application of Article 5 or Article 6 would result in a TAC which is more than 15 % less than the TAC of the preceding year, the Council shall adopt a TAC which is not more than 15 % less than the TAC of that year.
CHAPTER III
FISHING EFFORT LIMITATION
Article 8
Effort limitation
1. The TACs referred to in Chapter II shall be complemented by a system of fishing effort limitation based on the geographical areas and groupings of fishing gear, and the associated conditions for the use of these fishing opportunities specified in Annex IVb to Council Regulation (EC) No 27/2005 of 22 December 2004 fixing for 2005 the fishing opportunities and associated conditions for certain fish stocks and groups of fish stocks, applicable in Community waters and, for Community vessels, in waters where catch limitations are required [5].
2. Each year, the Council shall decide by qualified majority on the basis of a proposal from the Commission on an adjustment to the maximum number of fishing days available for vessels subject to the system of fishing effort limitation referred to in paragraph 1. The adjustment shall be in the same proportion as the annual adjustment in fishing mortality that is estimated by ICES and STECF as being consistent with the application of the fishing mortality rates established according to the method described in Article 5.
3. By way of derogation from paragraphs 1 and 2, each Member State concerned may implement a different method of effort management in that part of Area IXa lying east of longitude 7°23′48″ W as measured according to the WGS84 standard. Such a method shall establish a reference level of fishing effort equal to the fishing effort deployed in the year 2005. For 2006 and subsequent years, the fishing effort shall be adjusted by an amount that shall be decided by qualified majority by the Council on the basis of a proposal by the Commission. This adjustment shall be proposed after considering the most recent advice from STECF in the light of the most recent report from ICES. In the absence of a decision by the Council, Member States concerned shall ensure that the fishing effort does not exceed the reference level.
4. Each Member State taking up the derogation in paragraph 3 may be requested by the Commission to provide a report on the implementation of any different method of effort management. The Commission will communicate this report to all other Member States.
5. For the purposes of paragraph 3, fishing effort shall be measured as the sum, in any calendar year, of the products across all relevant vessels of their installed engine power measured in kW and their number of days fishing in the area.
CHAPTER IV
MONITORING, INSPECTION AND SURVEILLANCE
Article 9
Margin of tolerance
1. By way of derogation from Article 5(2) of Commission Regulation (EEC) No 2807/83 of 22 September 1983 laying down detailed rules for recording information on Member States’ catches of fish [6], the permitted margin of tolerance, in estimation of quantities of the stocks concerned, in kilograms retained on board of vessels shall be 8 % of the logbook figure. In the event that no conversion factor is laid down in Community legislation, the conversion factor adopted by the Member State whose flag the vessel is flying shall apply.
2. Paragraph 1 shall not apply if the quantity of the stocks concerned on board is less than 50 kg.
Article 10
Weighing of landings
The competent authorities of a Member State shall ensure that any quantity of the stock referred to in Article 1(a) exceeding 300 kg and/or 150 kg of the stocks referred to in Article 1(b) and/or (c) caught in any of the areas referred to in Article 1 shall be weighed using auction room scales before sale.
Article 11
Prior notification
The master of a Community fishing vessel that has been present in the areas referred to in Article 1 and who wishes to tranship any quantity of the stocks concerned that is retained on board, or to land any quantity of the stocks concerned in a port or a landing location of a third country, shall provide the competent authorities of the flag Member State with the following information at least 24 hours prior to transhipping or to landing in a third country:
- the name of the port or landing location,
- the estimated time of arrival at that port or landing location,
- the quantities in kilograms live weight of all species of which more than 50 kg is retained onboard.
This notification may also be made by a representative of the master of the fishing vessel.
Article 12
Separate stowage of Southern hake and Norway lobster
1. When quantities of the stock referred to in Article 1(a) greater than 50 kg are stowed on board a vessel, it shall be prohibited to retain on board a Community fishing vessel in a container any quantity of the stocks referred to in Article 1 mixed with any other species of marine organisms.
2. The masters of Community fishing vessels shall give Member States’ inspectors such assistance as will enable the quantities declared in the logbook and the catches of the stocks concerned that are retained on board to be cross-checked.
Article 13
Transport of Southern hake and Norway lobster
1. The competent authorities of a Member State may require that any quantity of the stock referred to in Article 1(a) exceeding 300 kg or the stocks referred to in Article 1(b) and/or (c) exceeding 150 kg caught in any of the geographical areas referred to in Article 1 and first landed in that Member State is weighed before being transported elsewhere from the port of first landing.
2. By way of derogation from Article 13 of Regulation (EEC) No 2847/93, quantities of the stock referred to in Article 1(a) exceeding 300 kg which are transported to a place other than that of landing or import shall be accompanied by a copy of one of the declarations provided for in Article 8(1) of Regulation (EEC) No 2847/93 pertaining to the quantities of these species transported. The exemption provided for in Article 13(4)(b) of Regulation (EEC) No 2847/93 shall not apply.
Article 14
Specific monitoring programme
By way of derogation from Article 34c(1) of Regulation (EEC) No 2847/93, the specific monitoring programme for the stocks concerned may last for more than two years from its date of entry into force.
CHAPTER V
AMENDMENTS TO REGULATION (EC) NO 850/98
Article 15
Restrictions on fishing for Norway lobster
The following Article shall be inserted in Regulation (EC) No 850/98:
"Article 29b
Restrictions on fishing for Norway lobster
1. During the periods set out below fishing with:
(i) bottom trawls or similar towed nets operating in contact with the bottom of the sea, and
(ii) creels shall be prohibited in the geographical areas bounded by rhumb lines joining the following positions as measured according to the WGS84 standard:
(a) from 1 June to 31 August:
latitude 42°23′ N, longitude 08°57′ W
latitude 42°00′ N, longitude 08°57′ W
latitude 42°00′ N, longitude 09°14′ W
latitude 42°04′ N, longitude 09°14′ W
latitude 42°09′ N, longitude 09°09′ W
latitude 42°12′ N, longitude 09°09′ W
latitude 42°23′ N, longitude 09°15′ W
latitude 42°23′ N, longitude 08°57′ W;
(b) from 1 May to 31 August:
latitude 37°45′ N, longitude 09°00′ W
latitude 38°10′ N, longitude 09°00′ W
latitude 38°10′ N, longitude 09°15′ W
latitude 37°45′ N, longitude 09°20′ W.
2. By way of derogation from the prohibition laid down in paragraph 1, fishing with bottom trawls or similar towed nets operating in contact with the bottom of the sea in the geographical areas and during the period set out in paragraph 1(b) shall be authorised provided that the by-catch of Norway lobster does not exceed 2 % of the total weight of the catch.
3. By way of derogation from the prohibition laid down in paragraph 1, fishing with creels that do not catch Norway lobster shall be authorised in the geographical areas and during the period set out in paragraph 1(b).
4. In the geographical areas and outside the periods referred to in paragraph 1, the by-catch of Norway lobster may not exceed 5 % of the total weight of the catch.
5. In the geographical areas and outside the periods set out in paragraph 1, Member States shall ensure that the fishing effort levels of vessels fishing with bottom trawls or similar towed nets operating in contact with the bottom of the sea do not exceed the levels of fishing effort carried out by the vessels of the Member State concerned during the same periods and in the same geographical areas in 2004.
6. Member States shall communicate to the Commission their measures to fulfil the obligation laid down in paragraph 5. If the Commission finds that the measures of a Member State do not fulfil that obligation, it may propose amendments to those measures. In the absence of agreement on measures between the Commission and the Member State concerned, the Commission may adopt measures in accordance with the procedure referred to in Article 30(2) of Regulation (EC) No 2371/2002 [7].
Article 16
Report on the recovery plan
The Commission shall submit a report to the European Parliament and the Council setting out the conclusions relating to the application of the recovery plan for the stocks concerned, including available socioeconomic data linked to the plan. This report shall be submitted by 17 January 2010.
CHAPTER VI
FINAL PROVISIONS
Article 17
Entry into force
This Regulation shall enter into force on the 20th day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 20 December 2005.
For the Council
The President
M. Beckett
[1] Opinion delivered on 14 April 2005 (Not yet published in the Official Journal).
[2] OJ L 358, 31.12.2002, p. 59.
[3] OJ L 261, 20.10.1993, p. 1. Regulation as last amended by Regulation (EC) No 768/2005 (OJ L 128, 21.5.2005, p. 1).
[4] OJ L 125, 27.4.1998, p. 1. Regulation as last amended by Regulation (EC) No 1568/2005 (OJ L 252, 28.9.2005, p. 2).
[5] OJ L 12, 14.1.2005, p. 1. Regulation as last amended by Regulation (EC) No 1936/2005 (OL L 311, 26.11.2005, p. 1).
[6] OJ L 276, 10.10.1983, p. 1. Regulation as last amended by Regulation (EC) No 1804/2005 (OL L 290, 4.11.2005, p. 10).
[7] OJ L 358, 31.12.2002, p. 59."
--------------------------------------------------
ANNEX
Method for calculating a TAC for Divisions VIIIc and IXa for Southern Hake in the event that a scientific catch forecast is only available for part of the area
If scientific advice for catches from a subarea within Divisions VIIIc and IXa corresponding to the fishing mortality rate specified in Article 5 is x tonnes, the average catch from the same subarea in the three previous years is y tonnes, and the average catch from all of Divisions VIIIc and IXa in the previous three years is z tonnes, the TAC shall be calculated as zx/y tonnes.
--------------------------------------------------
