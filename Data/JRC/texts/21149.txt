Commission Regulation (EC) No 1823/2002
of 11 October 2002
amending for the fifth time Council Regulation (EC) No 881/2002 imposing certain specific restrictive measures directed against certain persons and entities associated with Usama bin Laden, the Al-Qaida network and the Taliban, and repealing Council Regulation (EC) No 467/2001
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 881/2002 of 27 May 2002 imposing certain specific restrictive measures directed against certain persons and entities associated with Usama bin Laden, the Al-Qaida network and the Taliban, and repealing Council Regulation (EC) No 467/2001 prohibiting the export of certain goods and services to Afghanistan, strengthening the flight ban and extending the freeze of funds and other financial resources in respect of the Taliban of Afghanistan(1), as last amended by Commission Regulation (EC) No 1754/2002(2), and in particular Article 7(1), first indent, thereof,
Whereas:
(1) Annex I to Regulation (EC) No 881/2002 lists the persons, groups and entities covered by the freezing of funds and economic resources under that Regulation.
(2) On 10 October 2002, the Sanctions Committee decided to amend the list of persons, groups and entities to whom the freezing of funds and economic resources shall apply and, therefore, Annex I should be amended accordingly.
(3) In order to ensure that the measures provided for in this Regulation are effective, this Regulation must enter into force immediately,
HAS ADOPTED THIS REGULATION:
Article 1
Annex I to Regulation (EC) No 881/2002 is amended in accordance with the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 11 October 2002.
For the Commission
Christopher Patten
Member of the Commission
(1) OJ L 139, 29.5.2002, p. 9.
(2) OJ L 264, 2.10.2002, p. 23.
ANNEX
Annex I to Regulation (EC) No 881/2002 is amended as follows:
The following legal persons, groups or entities shall be added to the heading "Legal persons, groups and entities":
1. Moroccan Islamic Combatant Group (aka GICM or Groupe Islamique Combattant Marocain)
2. Tunisian Combatant Group (aka GCT or Groupe Combattant Tunisien).
