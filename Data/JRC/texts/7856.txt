COMMISSION REGULATION (EEC) N° 3985/87
of 22 December 1987
amending Council Regulation (EEC) N° 2658/87 on the tariff and statistical nomenclature and on the Common Customs Tariff
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) N° 2658/87
of 23 July 1987 on the tariff and statistical nomenclature
and on the Common Customs Tariff and, in particular, its
Article 9,
Whereas Regulation (EEC) N° 2658/87 established a goods nomenclature, hereinafter called the 'combined nomenclature', to meet, at one and the same time, the requirements both of the Common Customs Tariff and of the external trade statistics of the Community;
Whereas certain Regulations on the common organization of agricultural markets provide that the tariff nomenclature resulting from their application shall be included in the combined nomenclature; whereas they amend, if necessary, customs duties; whereas it is therefore appropriate to include in this Regulation all the amendments from Regulations adopted under the common agricultural policy;
Whereas on 24 November 1987 the Council decided upon the conventional rates of duty to be applied to certain products falling within Chapter 85 of the combined
nomenclature; whereas it is appropriate to include those rates in the combined nomenclature;
Whereas in order to ensure the uniform interpretation and application of the combined nomenclature, it is appropriate to clarify the meaning of certain terms used in Section XI of that nomenclature by the introduction of three additional notes;
Whereas in order to improve the wording of the combined nomenclature certain textual amendments are required;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Nomenclature Committee,
HAS ADOPTED THIS REGULATION:
Article 1
The combined nomenclature annexed to Regulation (EEC) N° 2658/87 is hereby amended in accordance with the Annex hereto.
Article 2
This Regulation shall enter into force on 1 January 1988.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 22 December 1987.
For the Commission
COCKFIELD
Vice-President
POR:L376UMBE00.95
FF: 4UEN; SETUP: 01; Hoehe: 376 mm; 56 Zeilen; 2405 Zeichen;
Bediener: UTE0 Pr.: C;
Kunde: ................................
(1) OJ N° L 256, 7. 9. 1987, p. 1.
ANNEX
>TABLE>
CN code
Description
Rate of duty
autonomous
(%)
or levy
(AGR)
conventional
(%)
Supplementary
unit
1
2
3
4
5
.
>TABLE>
The following amendments shall be made to the footnotes to Chapter 3:
Replace footnote (4) on page 46, footnote (5) on pages 47 and 48, footnote (3) on page 49 and footnote (4) on page 51 by the following:
'Duty exemption in respect of tuna and fish of the genus Euthynnus, falling within headings Nos 0302 and 0303, intended for the canning industry, within the limits of a global annual tariff quota of 17 250 tonnes to be granted by the competent Community authorities and subject to compliance with the reference price. Qualification for this quota is subject to conditions laid down in the relevant Community provisions.'
Replace footnote (1) on page 48, footnote (3) on page 50 and footnote (4) on page 53 by the following:
'Duty rate reduced to 8 % for silver hake (Merluccius bilinearis) falling within subheadings 0302 69 65, 0303 78 10 and 0304 90 47, within the limits of a global annual tariff quota of 2 000 tonnes to be granted by the competent Community authorities.'
Replace footnote (5) on pages 46 and 49, footnote (1) on page 52 and footnote (2) on page 53 by the following:
'Duty exemption for herring falling within subheadings 0302 40 90, 0303 50 90, 0304 10 99 and 0304 90 25, within the limits of a global annual tariff quota of 34 000 tonnes to be granted by the competent Community authorities and subject to compliance with the reference price.'
Replace footnote (1) on page 47 and footnote (2) on page 50 by the following:
'Duty rate reduced to 6 % in respect of piked dogfish (squalus acanthias) falling within subheadings 0302 65 10 and 0303 75 10 within the limits of a global annual tariff quota of 5 000 tonnes to be granted by the competent Community authorities.'
POR:L376UMBE02.96
FF: 4UEN; SETUP: 01; Hoehe: 265 mm; 77 Zeilen; 2185 Zeichen;
Bediener: UTE0 Pr.: C;
Kunde: 40772 England 02
CN code
Description
Rate of duty
autonomous
(%)
or levy
(AGR)
conventional
(%)
Supplementary
unit
1
2
3
4
5
.
>TABLE>
The following Additional Notes shall be introduced.
(a) 'SECTION XI
Additional Note
For the application of Note 13 to this Section, the term ''textile garments'' means garments of headings Nos 6101 to 6114 and of headings Nos 6201 to 6211.'
(b)
'CHAPTER 53
Additional Note
(A) For the purposes of subsections 5306 1090, 5306 2090 and 5308 2090, the expression ''put up for retail sale'' in relation to yarn (single, multiple or cabled) means, subject to the exceptions in paragraph (B) below, yarn put up:
(a) in balls or on cards, reels, tubes or similar supports, of a weight (including support) not exceeding
200 grams;
(b)
in hanks or skeins of a weight not exceeding 125 grams;
(c)
in hanks or skeins comprising several smaller hanks or skeins separated by dividing threads which render them independent one of the other, each of uniform weight not exceeding 125 grams.
(B)
Exceptions:
(a)
multiple or cabled yarn, unbleached, in hanks or skeins;
(b)
multiple or cabled yarn, put up:
i(i) in cross-reeled hanks or skeins; or
(ii)
put up on supports or in some other manner indicating its use in the textile industry (for example, on cops, twisting mill tubes, pirns, conical bobbins or spindles, or reeled in the form of cocoons for embroidery looms).'
(c)
'CHAPTER 61
Additional Note
For the purposes of heading N° 6109, the terms singlets and other vests include garments, even if of a fancy design, worn next to the body, without collar, with or without sleeves, including those with shoulder straps.
These garments, which are intended to cover the upper part of the body, often possess many characteristics
in common with those of T-shirts or with other more traditional kinds of singlets and other vests of heading
N° 6109.
However, garments having a ribbed waistband, drawstring or other means of tightening at the bottom are excluded from heading N° 6109.'
POR:L376UMBE03.96
FF: 4UEN; SETUP: 01; Hoehe: 281 mm; 87 Zeilen; 2396 Zeichen;
Bediener: UTE0 Pr.: C;
Kunde: 40772 Montan England 03
CN-code
Description
Rate of duty
autonomous
(%)
or levy
(AGR)
conventional
(%)
Supplementary
unit
1
2
3
4
5
.
>TABLE>
