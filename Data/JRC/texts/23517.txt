COUNCIL REGULATION (EC) No 2411/98 of 3 November 1998 on the recognition in intra-Community traffic of the distinguishing sign of the Member State in which motor vehicles and their trailers are registered
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 75(1)(d) thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the Economic and Social Committee (2),
Acting in accordance with the procedure laid down in Article 189c of the Treaty (3),
(1) Whereas the Community has adopted a certain number of measures that are intended to ensure the smooth functioning of an internal market comprising an area without frontiers in which the free movement of goods, persons, services and capital is ensured in accordance with the provisions of the Treaty;
(2) Whereas several Member States are contracting parties to the Vienna Convention of 1968 on road traffic (4), hereinafter referred to as the 'Convention`, Article 37 of which provides that every motor vehicle in international traffic shall display at the rear, in addition to its registration number, a distinguishing sign of the State in which it is registered;
(3) Whereas the Community is not a contracting party to the convention and whereas some of its Member States which are parties to it have recourse to the provisions of Article 37 of the Convention; whereas those Member States thus require vehicles from other Member States to display the distinguishing sign provided for by Annex 3 to the Convention; whereas some of those Member States do not recognise other distinguishing signs such as those displayed on registration plates which, while indicating the Member State in which the vehicle is registered, do not conform to Annex 3 to the Convention;
(4) Whereas several Member States have introduced a model registration plate which, on the extreme left, displays a blue zone containing the 12 yellow stars representing the European flag plus the distinguishing sign of the Member State of registration; whereas for the purpose of intra-Community transport this distinguishing sign meets the objective of identifying the State of registration as provided for in Article 37 of the Convention;
(5) Whereas Member States requiring vehicles from other Member States to display the distinguishing sign of the State of registration should also recognise the sign as provided for in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
This Regulation shall apply to vehicles registered in the Member States that are driven within the Community.
Article 2
For the purposes of this Regulation:
1. 'distinguishing sign of the Member State of registration` shall mean a set composed of one to three letters in Latin capitals indicating the Member State in which the vehicle is registered;
2. 'vehicle` shall mean any motor vehicle and its trailer as defined in:
- Council Directive 70/156/EEC of 6 February 1970 on the approximation of the laws of the Member States relating to the type-approval of motor vehicles and their trailers (5),
- Council Directive 92/61/EEC of 30 June 1992 relating to the type-approval of two or three-wheel motor vehicles (6).
Article 3
Member States requiring vehicles registered in another Member State to display a distinguishing registration sign when they are being driven on their territory shall recognise the distinguishing sign of the Member State of registration displayed on the extreme left of the registration plate in accordance with the Annex to this Regulation as being equivalent to any other distinguishing sign that they recognise for the purpose of identifying the State in which the vehicle is registered.
Article 4
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 3 November 1998.
For the Council
The President
B. PRAMMER
(1) OJ C 290, 24. 9. 1997, p. 25 and OJ C 159, 26. 5. 1998, p. 16.
(2) OJ C 95, 30. 3. 1998, p. 32.
(3) Opinion of the European Parliament of 19 February 1998 (OJ C 80, 16. 3. 1998, p. 210), Council Common Position of 5 June 1998 (OJ C 227, 20. 7. 1998, p. 31) and Decision of the European Parliament of 7 October 1998 (not yet published in the Official Journal).
(4) Vienna Convention of 8 November 1968, concluded under the auspices of the United Nations Economic Commission for Europe.
(5) OJ L 42, 23. 2. 1970, p. 1. Directive as last amended by Directive 97/27/EC (OJ L 233, 25. 8. 1997, p. 1).
(6) OJ L 225, 10. 8. 1992, p. 72. Directive as amended by the 1994 Act of Accession.
ANNEX
SPECIFICATIONS FOR THE DISTINGUISHING SIGN OF THE MEMBER STATE OF REGISTRATION TO BE AFFIXED AT THE EXTREME LEFT OF THE REGISTRATION PLATE
MODEL 1 (example)
>REFERENCE TO A FILM>
MODEL 2 (example)
>REFERENCE TO A FILM>
Colours:
1. Retro-reflecting blue background (Munsell reference 5,9 pb 3,4/15,1)
2. 12 retro-reflecting yellow stars
3. Retro-reflecting distinguishing sign of the Member State of registration, of a white or yellow colour
Composition and dimensions:
1. Blue background:
height = minimum 98 mm
width = minimum 40 mm, maximum 50 mm
2. The centres of the 12 stars to be arranged in a 15 mm radius circle; distance between two opposing peaks of any star = 4 to 5 mm
3. Distinguishing sign of the Member State of registration:
height = minimum 20 mm
width of character stroke = 4 to 5 mm
Where the dimensions of the blue background have been reduced for registration plates taking up two lines (see model 2) and/or for registration plates intended for two or three-wheel motor vehicles, the dimensions of the stars and of the distinguishing sign of the Member State of registration may be proportionately reduced.
