Commission Regulation (EC) No 1250/2006
of 18 August 2006
amending Regulation (EC) No 1973/2004 laying down detailed rules for the application of Council Regulation (EC) No 1782/2003 as regards the support schemes provided for in Titles IV and IVa of that Regulation and the use of land set aside for the production of raw materials
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1782/2003 of 29 September 2003 establishing common rules for direct support schemes under the common agricultural policy and establishing certain support schemes for farmers and amending Regulations (EEC) No 2019/93, (EC) No 1452/2001, (EC) No 1453/2001, (EC) No 1454/2001, (EC) No 1868/94, (EC) No 1251/1999, (EC) No 1254/1999, (EC) No 1673/2000, (EEC) No 2358/71 and (EC) No 2529/2001 [1], and in particular Article 145 thereof,
Whereas:
(1) Commission Regulation (EC) No 1973/2004 [2] lays down the implementing rules for the coupled support schemes provided for in Titles IV and IVa of Regulation (EC) No 1782/2003 and as regards the use of land set aside for the production of raw materials under the single payment scheme provided for in Title III of that Regulation and under arable crops area payment provided for in Chapter 10 of Title IV of that Regulation.
(2) Article 3 of Regulation (EC) No 1973/2004 provides for the data to be communicated to the Commission by the Member States and for the different dates for the sending of this information. Regulation (EC) No 1973/2004 also requires additional information to be sent on the different support schemes. In the interest of simplification, it is appropriate to clarify the necessary information to be sent to the Commission throughout the year.
(3) Article 64 of Regulation (EC) No 1973/2004 defines "set-aside" as leaving fallow an area eligible for area payments pursuant to Article 108 of Regulation (EC) No 1782/2003. In order to cover all set-aside schemes covered by Article 107 of that Regulation, it is appropriate to extend the scope of this definition beyond paragraph 1 of Article 107.
(4) Article 171cj of Regulation (EC) No 1973/2004 provides that the weight of tobacco on the basis of which aid is calculated is to be adjusted where the moisture content differs from the levels laid down in Annex XXVIII to that Regulation for the variety concerned. Those levels should be adapted to include the correct version as facilitated by the Member States concerned.
(5) Regulation (EC) No 1973/2004 should therefore be amended accordingly.
(6) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Direct Payments,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 1973/2004 is amended as follows:
1. Article 3 is replaced by the following:
"Article 3
Communications
1. The Member States shall communicate the following data to the Commission by electronic transmission:
(a) by 1 September of the year concerned at the latest:
(i) the total area for which the aid has been claimed in the case of:
- the specific quality premium for durum wheat provided for in Article 72 of Regulation (EC) No 1782/2003,
- the protein crop premium provided for in Article 76 of Regulation (EC) No 1782/2003,
- the crop-specific payment for rice provided for in Article 79 of Regulation (EC) No 1782/2003, broken down for the indica and japonica varieties,
- the area payment for nuts provided for in Article 83 of Regulation (EC) No 1782/2003, expressed by categories of nut trees,
- the aid for energy crops provided for in Article 88 of Regulation (EC) No 1782/2003,
- the arable crops area payment provided for in Article 100 of Regulation (EC) No 1782/2003, by base area as referred to in Annex IV to this Regulation and in the standardised format described in Annex IX to this Regulation,
- the crop-specific payment for cotton provided for in Article 110a of Regulation (EC) No 1782/2003,
- the aid for olive groves provided for in Article 110g of Regulation (EC) No 1782/2003, by categories,
- the hops area aid provided for in Article 110n of Regulation (EC) No 1782/2003,
- the single area payment scheme (SAPS) provided for in Article 143b of Regulation (EC) No 1782/2003;
(ii) the total quantity for which the aid has been claimed in the case of:
- the dairy premium provided for in Article 95 of Regulation (EC) No 1782/2003,
- the additional payments to producers of dairy products provided for in Article 96 of Regulation (EC) No 1782/2003;
(iii) the total number of applications in the case of the sheep and goat premiums provided for in Article 111 of Regulation (EC) No 1782/2003, using the model form set out in Annex XI to this Regulation;
(b) by 15 October of the year concerned at the latest, the total determined area in the case of:
- the protein crop premium provided for in Article 76 of Regulation (EC) No 1782/2003,
- the aid for energy crops provided for in Article 88 of Regulation (EC) No 1782/2003;
(c) by 15 November of the year concerned at the latest:
(i) the total determined area used for the calculation of the coefficient of reduction in the case of:
- the specific quality premium for durum wheat provided for in Article 72 of Regulation (EC) No 1782/2003,
- the crop-specific payment for rice provided for in Article 79 of Regulation (EC) No 1782/2003, broken down for the indica and japonica varieties, and also the detailed information by variety of rice and by base area and sub-base area using the model form set out in Annex III to this Regulation,
- the area payment for nuts provided for in Article 83 of Regulation (EC) No 1782/2003, expressed by categories of nut trees,
- the arable crops area payment provided for in Article 100 of Regulation (EC) No 1782/2003, by base area as referred to in Annex IV to this Regulation and in the standardised format described in Annex IX to this Regulation,
- the crop-specific payment for cotton provided for in Article 110a of Regulation (EC) No 1782/2003,
- the aid for olive groves provided for in Article 110g of Regulation (EC) No 1782/2003, by categories,
- the hops area aid provided for in Article 110n of Regulation (EC) No 1782/2003,
- the single area payment scheme (SAPS) provided for in Article 143b of Regulation (EC) No 1782/2003;
(ii) the total determined quantity used for the calculation of the coefficient of reduction, in the case of the dairy premium provided for in Article 95 of Regulation (EC) No 1782/2003;
(iii) the total determined quantity in the case of:
- the additional payments to producers of dairy products provided for in Article 96 of Regulation (EC) No 1782/2003,
- the tobacco aid provided for in Article 110j of Regulation (EC) No 1782/2003, by tobacco varieties as listed in Annex XXV to this Regulation;
(iv) the amount of aid per hectare to be granted for each category of olive groves in the case of the aid for olive groves provided for in Article 110g of Regulation (EC) No 1782/2003;
(d) by 31 March of the following year at the latest, the indicative aid amount per kg in the case of the tobacco aid provided for in Article 110j of Regulation (EC) No 1782/2003, by group of tobacco varieties as listed in Annex XXV to this Regulation and, where applicable, per quality grade;
(e) by 31 July of the following year at the latest:
(i) the total area for which the aid has actually been paid in the case of:
- the specific quality premium for durum wheat provided for in Article 72 of Regulation (EC) No 1782/2003,
- the protein crop premium provided for in Article 76 of Regulation (EC) No 1782/2003,
- the crop-specific payment for rice provided for in Article 79 of Regulation (EC) No 1782/2003, broken down for the indica and japonica varieties,
- the area payment for nuts provided for in Article 83 of Regulation (EC) No 1782/2003, expressed by categories of nut trees,
- the aid for energy crops provided for in Article 88 of Regulation (EC) No 1782/2003,
- the arable crops area payment provided for in Article 100 of Regulation (EC) No 1782/2003, by base area as referred to in Annex IV to this Regulation and in the standardised format described in Annex IX to this Regulation,
- the crop-specific payment for cotton provided for in Article 110a of Regulation (EC) No 1782/2003,
- the aid for olive groves provided for in Article 110g of Regulation (EC) No 1782/2003, by categories,
- the hops area aid provided for in Article 110n of Regulation (EC) No 1782/2003,
- the single area payment scheme (SAPS) provided for in Article 143b of Regulation (EC) No 1782/2003;
(ii) the total quantity for which the aid has actually been paid in the case of:
- the aid for starch potato (in starch-equivalent) provided for in Article 93 of Regulation (EC) No 1782/2003,
- the dairy premium provided for in Article 95 of Regulation (EC) No 1782/2003,
- the additional payments to producers of dairy products provided for in Article 96 of Regulation (EC) No 1782/2003,
- the seed aid provided for in Article 99 of Regulation (EC) No 1782/2003, by seed species as listed in Annex XI to Regulation (EC) No 1782/2003,
- the tobacco aid provided for in Article 110j of Regulation (EC) No 1782/2003, by tobacco varieties as listed in Annex XXV to this Regulation and by quality,
- the transitional sugar payment provided for in Article 110p of Regulation (EC) No 1782/2003;
(iii) the final aid amount per kg in the case of the tobacco aid provided for in Article 110j of Regulation (EC) No 1782/2003, by group of tobacco varieties as listed in Annex XXV to this Regulation and, where applicable, per quality grade;
(iv) the total number of sheep and goat premiums paid in the case of the sheep and goat premiums provided for in Article 111 of Regulation (EC) No 1782/2003, using the model form set out in Annex XII to this Regulation.
2. In the communications provided for in paragraph 1, the areas shall be expressed in hectares to two decimal places, the quantities shall be expressed in tonnes to three decimal places, and the number of applications shall be expressed with no decimal places.
3. Where the information required under paragraph 1 changes, in particular as a result of the checks or corrections or improvements of previous figures, an update shall be communicated to the Commission within one month after the occurrence of the change."
2. Article 4 is amended as follows:
(a) paragraph 1 is replaced by the following:
"1. The coefficient of reduction of area in the case referred to in Articles 75, 78(2), 82, 85, 89(2), 98, 143 and 143b(7) of Regulation (EC) No 1782/2003 or of the quantities and the objective criteria in the case referred to in Article 95(4) of that Regulation shall be fixed at the latest by 15 November of the year concerned on the basis of the data communicated in accordance with Article 3(b) and (c) of this Regulation.";
(b) paragraph 2 is deleted.
3. In Article 14, paragraphs 1 and 2 are replaced by the following:
"1. Member States shall communicate to the Commission by 1 September at the latest the list of the varieties registered in the national catalogue, classified according to the criteria defined in item 2 of Annex I to Council Regulation (EC) No 1785/2003 [3] in case of amendments to that list.
2. For French Guiana, the information concerning the areas referred to in Article 3(1)(c)(i) shall be communicated on the basis of the average of the areas sown during the two sowing cycles as referred to in the second paragraph of Article 12.
4. In Article 44, the second paragraph is replaced by the following:
"Such information shall include, in particular:
(a) the areas corresponding to each species of raw material;
(b) the quantities of each type of raw material and end product obtained."
5. In Article 62, the introductory terms are replaced by the following:
"For the purposes of Article 102(5) of Regulation (EC) No 1782/2003, the Member States shall, by 1 September of the year in respect of which the area payment is applied for at the latest, determine the following:".
6. Article 64 is replaced by the following:
"Article 64
Definition
For the purposes of Article 107 of Regulation (EC) No 1782/2003, "set-aside" shall mean leaving fallow an area eligible for area payments pursuant to Article 108 of that Regulation."
7. Article 69 is replaced by the following:
"Article 69
Communications
Where the areas referred to in Articles 59 and 60 are found to have been exceeded, the Member State concerned shall fix the definitive rate of the overshoot immediately and by 15 November of the year concerned at the latest and shall communicate it to the Commission by 1 December of the year concerned at the latest. The data used to calculate the rate by which a base area is exceeded shall be forwarded using the form set out in Annex VI."
8. Article 76 is replaced by the following:
"Article 76
Notification
Member States shall notify the Commission, by 31 October of each year at the latest, of any changes in the list of geographical areas practising transhumance referred to in Article 114(2) of Regulation (EC) No 1782/2003 and Article 73 of this Regulation."
9. Article 84 is amended as follows:
(a) paragraph 1 is replaced by the following:
"1. Member States shall notify the Commission, before 1 January each year , of any amendment to the part of the premiums rights transferred which shall be surrendered to the national reserve in accordance with Article 117(2) of Regulation (EC) No 1782/2003 and, where applicable, of the measures taken under Article 117(3) of that Regulation.";
(b) paragraph 2 is amended as follows:
(i) the introductory terms are replaced by the following:
"Using the table set out in Annex XIII, Member States shall notify the Commission by 30 April at the latest for each year of:";
(ii) point (e) is deleted.
10. Article 106 is replaced by the following:
"Article 106
Notification
1. Member States shall notify the Commission before 1 January each year:
(a) of any change to the reduction referred to in the second subparagraph of Article 127(1) of Regulation (EC) No 1782/2003;
(b) where applicable, of any amendments to the measures taken pursuant to Article 127(2)(a) of that Regulation.
2. Using the table set out in Part 3 of Annex XVIII, Member States shall notify the Commission by 31 July at the latest for each calendar year of:
(a) the number of premium rights returned without compensatory payment to the national reserve following transfers of rights without transfers of holdings during the preceding calendar year;
(b) the number of unused premium rights as referred to in Article 109(2) transferred to the national reserve during the preceding calendar year;
(c) the number of rights granted under Article 128(3) of Regulation (EC) No 1782/2003 during the preceding calendar year."
11. Article 131 is amended as follows:
(a) in paragraph 1, point (a) is replaced by the following:
"(a) by 1 March at the latest for information relating to the previous year, of the number of calves in respect of which the slaughter premium has been applied for and indicating whether the animals were slaughtered or exported;";
(b) in paragraph 2, the introductory terms of point (a) are replaced by the following:
"by 1 February at the latest for information relating to the previous year, of:";
(c) in paragraph 3, point (a) is replaced by the following:
"(a) by 1 March at the latest for information relating to the previous year, of the number of bovine animals other than calves in respect of which the slaughter premium has been applied for and indicating whether the animals were slaughtered or exported;";
(d) in paragraph 4, point (a) is replaced by the following:
"(a) by 1 February at the latest for information relating to the previous year, of the number of male bovines in respect of which the special premium has been applied for, broken down by age bracket and type of animal (bull or steer);";
(e) in paragraph 6, point (a) is replaced by the following:
"(a) where applicable, annually by 1 February at the latest for information relating to the previous year, of the number of animals for which the deseasonalisation premium was actually granted, broken down according to whether they benefited from the first or second tranche of the special premium, and the number of farmers corresponding to each of the two age brackets;".
12. Article 169 is amended as follows:
(a) point (b) is replaced by the following:
"(b) the quantities of each type of raw material and end product obtained.";
(b) points (c), (d) and (e) are deleted.
13. In Article 171ai, the introductory terms of paragraph 1 are replaced by the following:
"Before January 31 of the year in question, Member States shall notify cotton growers of:".
14. Article 171bd is deleted.
15. Annex III is replaced by the text in Annex I to this Regulation.
16. Annex VI is replaced by the text in Annex II to this Regulation.
17. Annex IX is replaced by the text in Annex III to this Regulation.
18. Annexes XI and XII are replaced by the text in Annex IV to this Regulation.
19. Annex XIV is deleted.
20. Annex XVIII is replaced by the text in Annex V to this Regulation.
21. Annex XXVIII is replaced by the text in Annex VI to this Regulation.
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 18 August 2006.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 270, 21.10.2003, p. 1. Regulation as last amended by Commission Regulation (EC) No 1156/2006 (OJ L 208, 29.7.2006, p. 3).
[2] OJ L 345, 20.11.2004, p. 1. Regulation as last amended by Regulation (EC) No 660/2006 (OJ L 116, 27.4.2006, p. 27).
[3] OJ L 270, 21.10.2003, p. 96."
--------------------------------------------------
ANNEX I
"
"ANNEX III
CROP-SPECIFIC PAYMENT FOR RICE
referred to in Article 3(1)(c)(i)
TOTAL DETERMINED AREA, used for the calculation of the coefficient of reduction
TRANSMISSION DEADLINE: 15 NOVEMBER EACH YEAR
CLAIMED YEAR:
MEMBER STATE:
(for France only) base area:
Sub-area | Reference area (in hectares) [1] | Variety | Total determined area (in hectares) [2] |
Name of sub-area 1 | | Variety 1 | |
Variety 2 | |
Variety 3 | |
… | |
Total | |
Name of sub-area 2 | | Variety 1 | |
Variety 2 | |
Variety 3 | |
… | |
Total | |
Name of sub-area 3 | | Variety 1 | |
Variety 2 | |
Variety 3 | |
… | |
Total | |
… | | Variety 1 | |
Variety 2 | |
Variety 3 | |
… | |
Total | |
Total | | | |
"
[1] Article 81 of Regulation (EC) No 1782/2003.
[2] Article 80(1) of Regulation (EC) No 1782/2003."
--------------------------------------------------
ANNEX II
"
"ANNEX VI
referred to in Articles 59, 60 and 69
+++++ TIFF +++++
"
--------------------------------------------------
ANNEX III
"
"ANNEX IX
referred to in Article 3(1)(a)(i), (c)(i) and (e)(i)
ARABLE CROPS AREA PAYMENTS
The information is to be presented in the form of a series of tables drawn up in accordance with the model described below:
- a set of tables giving information in respect of each base area region within the meaning of Annex IV to this Regulation,
- a single table summarising the information for each Member State.
The tables are to be sent in hard copy and in computerised form.
Notes:
Each table must quote the region in question.
Line 1 relates only to durum wheat eligible for the supplement to the area payment provided for in Article 105(1) of Regulation (EC) No 1782/2003.
Line 5, "Arable crops declared as fodder areas for premiums for bovine animals and sheep", corresponds to the areas referred to in Article 102(3) of Regulation (EC) No 1782/2003.
Model:
Crop | Area (hectares) |
Durum wheat, Article 105(1) | |
Maize (separate base asea) | |
Other crops, as listed in Annex IX to Council Regulation (EC) No 1782/2003 | |
Voluntary set-aside, Article 107(6) | |
Arable crops declared as fodder areas for premiums for bovine animals and sheep | |
Total" | |
"
--------------------------------------------------
ANNEX IV
"
ANNEX XI
referred to in Article 3(1)(e)(iv)
APPLICATIONS FOR EWE AND SHE-GOAT PREMIUMS
TRANSMISSION DEADLINE: 1 SEPTEMBER EACH YEAR
CLAIMED YEAR:
MEMBER STATE:
| Type of female | Total females |
Non-milking ewes | Milking ewes | She-goats |
Number of premiums claimed (Article 113 of Regulation (EC) No 1782/2003) | | | | |
Number of supplementary premiums claimed [1] (Article 114 of Regulation (EC) No 1782/2003) | | | | |
""
ANNEX XII
referred to in Article 3
PAYMENTS FOR EWE AND SHE-GOAT PREMIUMS
TRANSMISSION DEADLINE: 31 JULY EACH YEAR
CLAIMED YEAR:
MEMBER STATE:
| Type of female | Total females |
Non-milking ewes | Milking ewes | She-goats |
Number of premiums paid (Heads) | Number of additional payments per head [1] | | | | |
Number of supplementary premiums [2] | | | | |
Number of ewe or goat premiums | | | | |
"
[1] In accordance with Articles 72 and 73 of this Regulation (Less favoured areas).
[1] Where Article 71 of Regulation (EC) No 1782/2003 applies (transitional period).
[2] In accordance with Articles 72 and 73 of this Regulation (Less favoured areas).
--------------------------------------------------
ANNEX V
"
"ANNEX XVIII
referred to in Articles 106(3) and 131
BEEF AND VEAL PAYMENTS
CLAIMED YEAR:
MEMBER STATE:
1. SPECIAL PREMIUM
Number of animals
| Deadline for submission | Ref. | Information required | General scheme | Slaughter scheme |
Single age bracket or first age bracket | Second age bracket | Both age brackets together |
Bulls | Steers | Steers | Steers |
Article 131(4)(a) | 1 February 2007 | 1.2 | Number of animals applied for (full year) | | | | |
Article 131(4)(b)(i) | 31 July 2007 | 1.3 | Number of animals accepted (full year) | | | | |
Article 131(4)(b)(ii) | 31 July 2007 | 1.4 | Number of animals not accepted on account of the application of the ceiling | | | | |
Number of producers
| Deadline for submission | Ref. | Information required | General scheme | Slaughter scheme |
Single age bracket or first age bracket only | Second age bracket | Both age brackets together | Both age brackets together only |
Article 131(4)(b)(i) | 31 July 2007 | 1.5 | Number of producers granted premium | | | | |
2. DESEASONALISATION PREMIUM
| Deadline for submission | Ref. | Information required | Single age bracket or first age bracket | Second age bracket | Both age brackets together |
Article 131(6)(a) | 15 September 2006 | 2.1 | Number of animals applied for | | | |
2.2 | Number of producers | | | |
1 March 2007 | 2.3 | Number of animals accepted | | | |
2.4 | Number of producers | | | |
3. SUCKLER COW PREMIUM
| Deadline for submission | Ref. | Information required | Pure suckler herds | Mixed herds |
Article 131(2)(a)(i) | 1 February 2007 | 3.2 | Number of animals applied for (full year) | | |
Article 131(2)(b)(i) Article 131(6)(b)(ii) | 31 July 2007 | 3.3 | Number of cows accepted (full year) | | |
3.4 | Number of heifers accepted (full year) | | |
3.5 | Number of producers granted premium (full year) | | |
| | | | Amount per head | |
Article 131(2)(b)(iii) | 31 July 2007 | 3.6 | National premium | | |
Article 131(2)(b)(ii) | 31 July 2007 | 3.7 | Number of animals not accepted on account of the application of the national ceiling for heifers | | |
4. EXTENSIFICATION PAYMENT
4.1 Application of the single stocking density (first subparagraph of Article 132(2) of Regulation (EC) No 1782/2003)
| Deadline for submission | Ref. | Information required | Special premium | Suckler cow premium | Dairy cows | Total |
Article 131(6)(b)(i) Article 131(6)(b)(ii) Article 131(6)(b)(iii) | 31 July 2007 | 4.1.1 | Number of animals accepted | | | | |
4.1.2 | Number of producers granted payments | | | | |
4.2 Application of the two stocking density (second subparagraph of Article 132(2) of Regulation (EC) No 1782/2003)
| Deadline for submission | Ref. | Information required | Special premium | Suckler cow premium | Dairy cows | Total |
1,4-1,8 | < 1,4 | 1,4-1,8 | < 1,4 | 1,4-1,8 | < 1,4 | 1,4-1,8 | < 1,4 |
Article 131(6)(b)(i) Article 131(6)(b)(ii) Article 131(6)(b)(iii) | 31 July 2007 | 4.2.1 | Number of animals accepted | | | | | | | | |
4.2.2 | Number of producers granted payments | | | | | | | | |
5. PREMIUM EXEMPT THE DENSITY FACTOR
| Deadline for submission | Ref. | Information required | Animals | Producers |
Article 131(6)(b)(iv) | 31 July 2007 | 5 | Number of animals and producers in respect of which the premium exempt from the application of the density factor was granted | | |
6. SLAUGHTER PREMIUM
Number of animals
| Deadline for submission | Ref. | Information required | Slaughter | Export |
Adults | Calves | Adults | Calves |
Article 131(1)(a) Article 131(2)(a)(ii) Article 131(3)(a) | 1 March 2007 | 6.2 | Number of animals applied for (full year) | | | | |
Article 131(1)(b)(i) Article 131(2)(b)(iv) Article 131(3)(b)(i) | 31 July 2007 | 6.3 | Number of animals accepted (full year) | | | | |
Article 131(1)(b)(ii) Article 131(2)(b)(v) Article 131(3)(b)(ii) | 31 July 2007 | 6.4 | Number of animals not accepted on account of the application of the ceiling | | | | |
Number of producers
| Deadline for submission | Ref. | Information required | Slaughter | Export |
Adults | Calves | Adults | Calves |
Article 131(1)(b)(i) Article 131(2)(b)(iv) Article 131(3)(b)(i) | 31 July 2007 | 6.5 | Number of producers granted premium | | | | |
AMOUNT OF THE PREMIUMS ACTUALLY PAID
TRANSMISSION DEADLINE: 31 JULY EACH YEAR
| Up to 100 % Slaughter premium (calves) | Up to 100 % Suckler cow premium | Up to 40 % Slaughter premium (bovine animals other than calves) | Up to 100 % Slaughter premium (bovine animals other than calves) | Up to 75 % Special premium |
Reference to Regulation (EC) No 1782/2003 | Article 68(1) | Article 68(2)(a)(i) | Article 68(2)(a)(ii) | Article 68(2)(b)(i) | Article 68(2)(b)(ii) |
Amount actually paid in EUR (after reduction laid down in Article 139 — coefficient of reduction)" | | | | | |
"
--------------------------------------------------
ANHANG VI
"
"ANNEX XXVIII
MOISTURE CONTENT
as referred to in Article 171cj
Group of varieties | Moisture content (%) | Tolerances (%) |
I.Flue-cured | 16 | 4 |
II.Light air-cured
Germany, France, Belgium, Austria, Portugal — Autonomous Region of the Azores | 22 | 4 |
Other Member States and other recognised production areas in Portugal | 20 | 6 |
III.Dark air-cured
Belgium, Germany, France, Austria | 26 | 4 |
Other Member States | 22 | 6 |
IV.Fire-cured | 22 | 4 |
V.Sun-cured | 16 | 4 |
VI.Basmas | 16 | 4 |
VII.Katerini | 16 | 4 |
VIII.Kaba Koulak (classic) Elassona, Myrodata Agrinion, Zichnomyrodata | 16 | 4" |
"
--------------------------------------------------
