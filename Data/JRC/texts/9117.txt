Commission Regulation (EC) No 1789/2006
of 5 December 2006
opening and providing for the administration of the tariff quota for the import of bananas falling under CN code 08030019 originating in ACP countries for the period 1 January to 31 December 2007
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1964/2005 of 29 November 2005 on the tariff rates for bananas [1], and in particular Article 2 thereof,
Whereas:
(1) Article 1(2) of Regulation (EC) No 1964/2005 provides that each year from 1 January, an autonomous tariff quota of 775000 tonnes net weight subject to a zero-duty rate is to be opened for imports of bananas under CN code 08030019 originating in ACP countries.
(2) The tariff quota provided for by Regulation (EC) No 1964/2005 for 2007 should therefore be opened and the provisions for its administration laid down for the period until 31 December 2007.
(3) As is the case for non preferential imports, a method of administering the tariff quota should be adopted so as to favour international trade and smoother trade flows. The most appropriate method for this purpose would be that using the quota by chronological order of acceptance of the declarations of release for free circulation (the "first come, first served" method). Nevertheless, in order to ensure continuity of trade with ACP countries and, therefore, satisfactory supplies for the Community market while avoiding disturbances in trade flows, Commission Regulation (EC) No 219/2006 [2] reserved, on a transitional basis, part of the tariff quota for operators who supplied the Community with ACP bananas in the framework of the import regime previously in force. Taking into account the transitional nature of that provision, it appears appropriate to progressively eliminate it and hence ensuring for 2007 a substantial increase in the part of the tariff quota managed by the first come, first served method, by increasing the proportion of imports carried out under that system from 60 % to 81 %.
(4) Provision should therefore be made for a total quantity of 146848 tonnes of the tariff quota to be reserved for the operators who actually imported bananas originating in ACP countries into the Community during 2006 That proportion of the tariff quota should be administered by means of import licences issued to each operator in proportion to the quantities imported on the basis of licences received by these operators under Chapter II of Regulation (EC) No 219/2006.
(5) In view of the quantities available, a ceiling should be set for the licence application which each operator may lodge for the period until 31 December 2007.
(6) Access to the rest of the tariff quota should be open to all operators established in the Community on a "first come, first served" basis in accordance with Articles 308a, 308b and 308c of Commission Regulation (EEC) No 2454/93 of 2 July 1993 laying down provisions for the implementation of Council Regulation (EEC) No 2913/92 establishing the Community Customs Code [3].
(7) This Regulation should enter into force immediately in order to enable licence applications to be lodged in time.
(8) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Bananas,
HAS ADOPTED THIS REGULATION:
CHAPTER I
GENERAL PROVISIONS
Article 1
Subject
The zero-duty tariff quota for the import of bananas falling under CN code 08030019 originating in ACP countries provided for in Article 1(2) of Regulation (EC) No 1964/2005 is hereby opened for the period 1 January to 31 December 2007.
Article 2
Available quantities
The quantities available under the tariff quota are set at 775000 tonnes, of which:
(a) 146848 tonnes is to be administered in accordance with Chapter II and have the serial number 09.4164.
(b) 628152 tonnes is to be administered in accordance with Chapter III and have the order numbers: 09.1634, 09.1638, 09.1639, 09.1640, 09.1642, 09.1644.
CHAPTER II
IMPORTS OF THE QUANTITIES PROVIDED FOR IN ARTICLE 2(A)
Article 3
Import licences
1. All imports under the quantity fixed in Article 2(a) shall be subject to the lodging of an import licence issued in accordance with the provisions of this Chapter.
2. Commission Regulation (EC) No 1291/2000 [4] shall be applicable, with the exception of Article 8(4) and (5), subject to the provisions of this Regulation.
Article 4
Lodging licence applications
1. Economic operators established in the Community who actually imported bananas originating in ACP countries into the Community in 2006, on the basis of licences issued under Chapter II of Regulation (EC) No 219/2006, shall be entitled to lodge import licence applications.
2. The quantities applied for by each operator may not exceed 110 % of the quantity imported on the basis of licences allocated to him under chapter II of Regulation (EC) No 219/2006.
3. Import licence applications must be lodged by each operator on 8 and 9 January 2007 with the competent authorities of the Member State which issued him in 2006 with the import licences for the quantities referred to in paragraph 2.
The competent authorities in each Member State shall be as listed in the Annex. That list shall be amended by the Commission at the request of the Member States concerned.
4. Licence applications shall be accompanied by a copy of the licence(s) used in 2006 to import bananas originating in ACP countries, duly endorsed, and the documents proving the ACP origin of the quantities under those licences, and the proof of lodging of a security in accordance with Title III of Commission Regulation (EEC) No 2220/85 [5]. The security shall be EUR 150 per tonne.
5. Applications not lodged in accordance with this Article shall not be admissible.
6. Box 20 of licence applications and licences shall contain the entry "licence under Chapter II of Regulation (EC) No 1789/2006".
Article 5
Issuing of licences
1. Member States shall notify the Commission not later than 15 January 2007 of the total quantity for which admissible licence applications have been lodged.
2. If the quantities applied for exceed the quantity referred to in Article 2(a) the Commission shall, not later than 18 January 2007, set an allocation coefficient to be applied to each application.
3. The competent authorities shall issue the import licences from 22 January 2007, where appropriate applying the allocation coefficient referred to in paragraph 2.
4. Where, if an allocation coefficient is applied, a licence is issued for a quantity less than that applied for, the security referred to in Article 4(4) shall be released without delay for the quantity not awarded.
Article 6
Period of validity of licences and Member State notifications
1. The import licences issued in accordance with Article 5(3) shall be valid until 31 December 2007.
2. From February 2007 to January 2008 inclusive, Member States shall notify the Commission, not later than the 15th of each month, of the quantities of bananas released into free circulation during the previous month on the basis of licences issued in accordance with Article 5(3).
The information referred to in the first subparagraph shall be sent via by the electronic system indicated by the Commission.
3. Member States shall transmit to the Commission, not later than 26 January 2007, the list of operators operating under this Regulation.
The Commission may communicate these lists to the other Member States.
Article 7
Formalities for release for free circulation
1. The customs offices at which the import declarations are lodged with a view to the release into free circulation of bananas shall:
(a) keep a copy of each import licence and extract therefrom endorsed on acceptance of a declaration of release into free circulation; and
(b) forward at the end of each fortnight a second copy of each import licence and extract endorsed to their Member State authorities listed in the Annex.
2. The authorities referred to in paragraph 1(b) shall, at the end of each fortnight, forward a copy of the licences and extracts received to the competent authorities of the Member States listed which issued those documents.
3. Where there is doubt as to the authenticity of the licence, the extract, or any information in or signatures on the documents presented, or as to the identity of the operators completing the formalities for release into free circulation or for the account of whom those formalities are completed, and where irregularities are suspected, the customs offices at which those documents were presented shall immediately inform the competent authorities of their Member State thereof. The latter shall immediately forward that information to the competent authorities of the Member State which issued the documents and to the Commission, for the purposes of a thorough check.
4. On the basis of the information received under paragraphs 1, 2 and 3, the Member States' competent authorities listed in the Annex shall carry out the additional checks needed to ensure the proper administration of the tariff quota arrangements, in particular verification of the quantities imported under those arrangements, by means of a precise comparison of the licences and extracts issued with the licences and extracts used. To that end, they shall verify in particular the authenticity and conformity of the documents used and that the documents have been used by operators.
CHAPTER III
IMPORTS OF THE QUANTITIES PROVIDED FOR IN ARTICLE 2(B)
Article 8
Administration
1. The quantity provided for in Article 2(b) shall be divided into six tranches, each of 104692 tonnes, as follows:
Order number | Quota period |
09.1634 | 1 January to 28 February |
09.1638 | 1 March to 30 April |
09.1639 | 1 May to 30 June |
09.1640 | 1 July to 31 August |
09.1642 | 1 September to 31 October |
09.1644 | 1 November to 31 December |
2. The tranches provided for in paragraph 1 shall be administered in accordance with Articles 308a, 308b and 308c of Regulation (EEC) No 2454/93.
CHAPTER IV
FINAL PROVISIONS
Article 9
Entry into force
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 5 December 2006.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 316, 2.12.2005, p. 1.
[2] OJ L 38, 9.2.2006, p. 22. Regulation as last amended by Regulation (EC) No 1261/2006 (OJ L 230, 24.8.2006, p. 3).
[3] OJ L 253, 11.10.1993, p. 1. Regulation as last amended by regulation (EC) No 402/2006 (OJ L 70, 9.3.2006, p. 35).
[4] OJ L 152, 24.6.2000, p. 1. Regulation as last amended by Regulation (EC) No 1282/2006 (OJ L 234, 29.8.2006, p. 4).
[5] OJ L 205, 3.8.1985, p. 5. Regulation as last amended by Regulation (EC) No 673/2004 (OJ L 105, 14.4.2004, p. 17).
--------------------------------------------------
ANNEX
Competent authorities of the Member States:
Belgium
Bureau d'intervention et de restitution belge/Belgisch Interventie- en Restitutiebureau
Rue de Trèves, 82/Trierstraat 82
B-1040 Bruxelles/Brussel
Czech Republic
Státní zemědělský intervenční fond
Ve Smečkách 33
CZ-110 00 Praha 1
Denmark
Ministeriet for Fødevarer, Landbrug og Fiskeri
Direktoratet for Fødevareerhverv; Eksportstøttekontoret
Nyropsgade 30
DK-1780 København V
Germany
Bundesanstalt für Landwirtschaft und Ernährung
Referat 322
Deichmanns Aue 29
D-53179 Bonn
Estonia
Põllumajanduse Registrite ja Informatsiooni Amet
Toetuste osakond, kaubandustoetuste büroo
Narva mnt 3
EE-51009 Tartu
Greece
OKEPEKE (ex-GEDIDAGEP)
Directorate Fruits and Vegetables, Wine and Industrial Products
241, Acharnon Street
GR-10446 Athens
ΟΠΕΚΕΠΕ
Δ/νση οπωροκηπευτικών, αμπελοοινικών και βιομηχανικών προϊόντων
Αχαρνών 2
Τ.Κ. 10446, Αθήνα
Spain
Ministerio de Industria, Turismo y Comercio
Secretaría General de Comercio Exterior
Paseo de la Castellana, 162
E-28046 Madrid
France
Office de développement de l'économie agricole des départements d'outre-mer (Odeadom)
46-48, rue de Lagny
F-93104 Montreuil Cedex
Ireland
Department of Agriculture & Food
Crops Policy & State Bodies Division
Agriculture House (3W)
Kildare Street
Dublin 2
Ireland
Italy
Ministero del Commercio internazionale
Direzione generale per la Politica commerciale — Div. II
Viale Boston, 25
I-00144 Roma
Cyprus
Υπουργείο Εμπορίου, Βιομηχανίας και Τουρισμού
Μονάδα Αδειών Εισαγωγών — Εξαγωγών
CY 1421 Κύπρος
Ministry of Commerce, Industry and Tourism
Import & Export Licensing Unit
CY 1421 Cyprus
Latvia
Zemkopības ministrijas
Lauku atbalsta dienests
Tirdzniecības mehānismu departaments
Licenču daļa
Republikas laukums 2
LV-1981 Rīga
Lithuania
Nacionalinė mokėjimo agentūra
Užsienio prekybos departamentas
Blindžių g. 17
LT-08111 Vilnius
Luxembourg
Direction des douanes et accises
Division "douane/valeur"
26, place de la Gare
L-1616 Luxembourg
Hungary
Magyar Kereskedelmi Engedélyezési Hivatal
Margit krt. 85
HU-1024 Budapest
Malta
Ministeru ghall-Affarijiet Rurali u l-Ambjent
Divizjoni tas-Servizzi Agrikoli u Zvilupp Rurali
Agenzija tal-Pagamenti
Trade Mechanisims
Centru Nazzjonali tas Servizzi Agrikoli u Zvilupp Rurali Ghammieri
Marsa CMR 02 Malta
The Netherlands
Produktschap Tuinbouw
Louis Pasteurlaan 6
Postbus 280
2700 AG Zoetermeer
Nederland
Austria
Agrarmarkt Austria
Dresdner Straße 70
A-1200 Wien
Poland
Agencja Rynku Rolnego
Biuro Administrowania Obrotem Towarowym z Zagranicą
ul. Nowy Świat 6/12
00-400 Warszawa
Polska
Portugal
Ministério das Finanças e da Administração Pública
Direcção-Geral das Alfândegas e dos Impostos Especiais sobre o Consumo
Direcção de Serviços de Licenciamento
Rua Terreiro do Trigo — Edifício da Alfândega
P-1149-060 Lisboa
Slovenia
Agencija RS za kmetijske trge in razvoj podeželja
Oddelek za zunanjo trgovino
Dunajska cesta 160
SI-1000 Ljubljana
Slovakia
Pôdohospodárska platobná agentúra
Dobrovičova 12
815 26 Bratislava
Slovenská republika
Finland
Maa- ja Metsätalousministeriö
PL 30
FIN-00023 Valtioneuvosto, Helsinki
Sweden
Jordbruksverket
Interventionsenheten
S-551 82 Jönköping
United Kingdom
Rural Payment Agency
External Trade Division
Lancaster House
Hampshire Court
Newcastle Upon Tyne
NE4 7YH
United Kingdom
Bulgaria
Министерство на земеделието и горите
Дирекция "Маркетинг и регулаторни режими"
Бул. "Христо Ботев", 55
София, 1040
България
Ministry of Agriculture and Forestry
Marketing and Regulatory Regimes Directorate
55, Hristo Botev blvd.
Sofia, 1040
Romania
Agentia de Plati si Interventie pentru Agricultura
Directia de Masuri de Piata – Comert Exterior
B-dul Carol l nr. 17, sector 2
Bucuresti
Romania
--------------------------------------------------
