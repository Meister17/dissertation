COMMISSION DECISION of 18 December 1996 setting specific public health requirements for imports of egg products for human consumption (Text with EEA relevance) (97/38/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 92/118/EEC of 17 December 1992 laying down animal health and public health requirements governing trade in and imports into the Community of products not subject to the said requirements laid down in specific Community rules referred to in Annex A (I) to Directive 89/662/EEC and, as regards pathogens, to Directive 90/425/EEC (1), as last amended by Commission Decision 96/405/EC (2), and in particular the first indent of Chapter 2 of Annex II,
Having regard to Council Directive 89/437/EEC of 20 June 1989 on hygiene and health problems affecting the production and the placing on the market of egg products (3), as last amended by Directive 96/23/EC (4), and in particular Article 11 (4) thereof,
Whereas Chapter III of Directive 92/118/EEC lays down general provisions applying to imports into the Community; whereas specific public health requirements should be laid down for egg product imports; whereas these must be at least as stringent as those in force for placing on the market;
Whereas at this first stage a standard public health certificate that must accompany imported egg products should be specified, whereas the Community list of third country establishments and the treatments approved at Community level will be determined at a later date; whereas pending these decisions it is for the competent authority of each third country to certify that the egg products come from an approved establishment and have undergone treatment enabling them to meet the analytical specifications set out in Chapter VI of the Annex to Directive 89/437/EEC;
Whereas, in addition, where it is possible to recognize conditions offering equivalent guarantees, a third country may submit a proposal for such recognition to the Commission for appropriate consideration;
Whereas the requirements set by this Decision are in accordance with the opinion of the Standing Veterinary Committee;
HAS ADOPTED THIS DECISION:
Article 1
This Decision sets specific public health requirements for egg products for direct human consumption or manufacture of foodstuffs.
Article 2
For the purposes of this Decision the definitions set out in Article 2 of Directive 89/437/EEC shall apply.
Article 3
Importation of egg products is subject to the requirement that they:
1. were obtained from hen, duck, goose, turkey, guinea fowl or quail eggs but not from a mixture of eggs of different species;
2. were treated and prepared in an establishment/establishments approved by the competent authority and meeting the requirements of Chapters I and II of the Annex to Directive 89/437/EEC;
3. were prepared in observance of the hygiene requirements of Chapters III and V of the Annex to Directive 89/437/EEC and from eggs meeting the requirements of Chapter IV of that Annex;
4. were treated so as to enable them to meet the analytical specifications of Chapter VI of the Annex to Directive 89/437/EEC;
5. meet the analytical specifications of Chapter VI of the Annex to Directive 89/437/EEC;
6. were subjected to health control as specified in Chapter VII of the Annex to Directive 89/437/EEC;
7. were packaged as specified in Chapter VIII of the Annex to Directive 89/437/EEC;
8. were stored and transported as specified in Chapters IX and X of the Annex to Directive 89/437/EEC;
9. meet Community standards on residues of substances that are harmful or might alter the organoleptic characteristics of the product or make its consumption dangerous or harmful to human health.
Article 4
Every consignment of egg products must be accompanied by the original of a numbered public health certificate, completed, signed and dated, made out on a single sheet and in accordance with the specimen annexed hereto.
Article 5
The certificate shall be drawn up in at least one of the official languages of the Member State of introduction into the Community.
Article 6
This Decision shall apply from 1 January 1997.
Article 7
This Decision is addressed to the Member States.
Done at Brussels, 18 December 1996.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ No L 62, 15. 3. 1993, p. 49.
(2) OJ No L 165, 4. 7. 1996, p. 40.
(3) OJ No L 212, 22. 7. 1989, p. 87.
(4) OJ No L 125, 23. 5. 1996, p. 10.
ANNEX
>START OF GRAPHIC>
PUBLIC HEALTH CERTIFICATE
for egg products
Number: Exporting country:
Competent authority:
Ministry:
I. Identification of products
Egg product of: (species of origin)
Nature of product:
Egg content percentage (1):
Type of package:
Number of packages:
Date of treatment:
Storage and transport temperature:
Assured conservation period:
Net weight:
II. Provenance of products
Address(es) and approval number(s) of production establishment(s):
III. Destination of products
Consignment:
from: (place whence consigned)
to: (country and place of destination)
by the following means of transport:
Name and address of consignor:
Name of consignee and address of destination:
IV. Health declaration
The undersigned certifies that the egg products indicated above:
1. were obtained from hen, duck, goose, turkey, guinea fowl or quail eggs but not from a mixture of eggs of different species;
2. were treated and prepared in an establishment/establishments approved by the competent authority and meeting the requirements of Chapter I and II of the Annex to Directive 89/437/EEC;
(1) Where the products are partly supplemented by other foodstuffs or by authorized additives within the permitted limits.
3. were prepared in observance of the hygiene requirements of Chapters III and V of the Annex to Directive 89/437/EEC and from eggs meeting the requirements of Chapter IV of that Annex;
4. were treated so as to enable them to meet the analytical specifications of Chapter VI of the Annex to Directive 89/437/EEC;
5. meet the analytical specifications of Chapter IV of the Annex to Directive 89/437/EEC;
6. were subjected to health control as specified in Chapter VII of the Annex to Directive 89/437/EEC;
7. were packaged as specified in Chapter VIII of the Annex to Directive 89/437/EEC;
8. were stored and transported as specified in Chapters IX and X of the Annex to Directive 89/437/EC;
9. meet Community standards on residues of substances that are harmful or might alter the organoleptic characteristics of the product or make its consumption dangerous or harmful to human health.
Stamp (1) Done at ,
(place) on (date)
(signature of competent authority) (1) (name in capital letters and capacity)
(1) Signature and stamp must be in a different colour from that of the print of the form.
>END OF GRAPHIC>
