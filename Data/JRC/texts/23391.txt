COMMISSION REGULATION (EC) No 1285/98 of 22 June 1998 amending Regulation (EEC) No 2123/89 establishing the list of representative markets for pigmeat in the Community
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2759/75 of 29 October 1975 on the common organisation of the market in pigmeat (1), as last amended by Commission Regulation (EC) No 3290/94 (2), and in particular Article 4(6) thereof,
Whereas Commission Regulation (EEC) No 2123/89 (3), as last amended by Regulation (EC) No 532/96 (4), established the list of representative markets for pigmeat in the Community;
Whereas in Ireland, in Luxembourg and in Spain a change to the representative markets has taken place; whereas the list of representative markets for pigmeat in the Community listed in the Annex to Regulation (EEC) No 2123/89 should consequently be amended;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Pigmeat,
HAS ADOPTED THIS REGULATION:
Article 1
The Annex to Regulation (EEC) No 2123/89 is amended as follows:
1. point 4 is replaced by the following:
'4. The following group of quotation centres: Ebro (Zaragoza), Mercolleida (Lleida), Campillos (Málaga), Segovia, Segura (Murcia), Silleda (Pontevedra)
and the following group of markets: Alhama (Murcia), Barcelona, Binefar (Huesca), Burgos, Calamocha (Teruel), Mollerussa (Lleida), Pamplona, Porriño (Pontevedra), Segovia, Sierra de Yeguas (Málaga), Valdepeñas (Ciudad Real).`;
2. point 7 is replaced by the following:
'7. The following group of markets: Rooskey, Waterford, Tralee and Mitchelstown.`;
3. point 9 is replaced by the following:
'9. The following market: Esch.`
Article 2
This Regulation shall enter into force on 1 July 1998.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 22 June 1998.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ L 282, 1. 11. 1975, p. 1.
(2) OJ L 349, 31. 12. 1994, p. 105.
(3) OJ L 203, 15. 7. 1989, p. 23.
(4) OJ L 78, 28. 3. 1996, p. 14.
