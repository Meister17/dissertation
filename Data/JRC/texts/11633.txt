Appeal brought on 9 January 2006 by Rafael De Bustamante Tello against the judgment of the Court of First Instance (First Chamber) of 25 October 2005 in Case T-368/03 Rafael De Bustamante Tello v Council of the European Union
An appeal against the judgment of the Court of First Instance (First Chamber) of 25 October 2005 in Case T-368/03 Rafael De Bustamante Tello v Council of the European Union was brought before the Court of Justice of the European Communities on 9 January 2006 by Rafael De Bustamante Tello, represented by R. García-Gallardo Gil-Fournier, D. Domínguez Pérez and A. Sayagués Torres, lawyers.
The applicant claims that the Court of Justice should:
(1) Declare this appeal admissible;
(2) Annul the judgment of the First Chamber of the Court of First Instance of 25 October 2005;
(3) Order the Council to pay all the costs of the proceedings before the Court of Justice and of those before the Court of First Instance.
Pleas in law and main arguments
This appeal is based on a single plea: infringement by the CFI of Community law in paragraphs 24 to 45 of the judgment under appeal. In particular, the appellant takes the view that the CFI incorrectly interpreted the term "circumstances arising from work done for another State" used in the second indent of Article 4(1)(a) of Annex VII of the Staff Regulations.
--------------------------------------------------
