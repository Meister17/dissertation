Brussels, 29.4.2005
COM(2005) 180 final
2005/0068 (CNS)
Proposal for a
COUNCIL REGULATION
imposing certain specific restrictive measures directed against certain persons impeding the peace process and breaking international law in the conflict in the Darfur region in Sudan
(presented by the Commission)
EXPLANATORY MEMORANDUM
1. In view of recent developments in Sudan, and notably the continued violations of the N’djamena Ceasefire agreement of 8 April 2004 and the Abuja Protocols of 9 November 2004 by all sides in Darfur and the failure of the Government of Sudan and rebel forces and all other armed groups in Darfur to comply with their commitments and the demands of the Security Council, the United Nations Security Council decided on 29 March 2005 to enlarge the scope of the restrictive measures in force against Sudan.
2. The extended restrictive measures decided by the Security Council in its Resolution 1591 (2005) include, inter alia , the application, as of 28 April 2005, of the freezing of funds and economic resources of persons designated by the United Nations as impeding the peace process, constituting a threat to stability in Darfur and the region, committing either violations of international humanitarian or human rights law, or other atrocities, violating the arms embargo or being responsible for certain offensive military flights.
3. The freezing of funds and economic resources of designated persons falls within the scope of the Treaty. The proposed measures are similar to those imposed by means of Regulation (EC) No 881/2002 imposing certain specific restrictive measures directed against certain persons and entities associated with Usama bin Laden, the Al-Qaida network and the Taliban, and by means of Regulation (EC) No 1763/2004 imposing certain restrictive measures in support of effective implementation of the mandate of the International Criminal Tribunal for the former Yugoslavia (ICTY), to give but two examples.
4. As the freezing of funds and economic resources serves the objective of promoting international peace and security, the Council should adopt a Common Position calling for Community action, based on Article 15 of the Treaty on European Union (CFSP) for the Regulation to be in line with the Treaty establishing the European Community.
2005/0068 (CNS)
Proposal for a
COUNCIL REGULATION
imposing certain specific restrictive measures directed against certain persons impeding the peace process and breaking international law in the conflict in the Darfur region in Sudan
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Articles 60, 301 and 308 thereof,
Having regard to Common Position 2005/xxx/CFSP of xx April 2005 concerning restrictive measures against Sudan,[1]
Having regard to the proposal from the Commission[2],
Having regard to the opinion of the European Parliament[3],
Whereas:
(1) In its Resolution 1591 (2005) of 29 March 2005, the UN Security Council, acting under Chapter VII of the Charter of the United Nations, and deploring strongly that the Government of Sudan and rebel forces and all other armed groups in Darfur have failed to comply with their commitments and the demands of the Security Council, decided to impose certain additional restrictive measures in respect of Sudan.
(2) Common Position 2005/XXX/CFSP provides, inter alia, for implementation of the freezing of funds and economic resources of those persons designated by the competent United Nations Sanctions Committee, who impede the peace process, constitute a threat to stability in Darfur and the region, commit violations of international humanitarian or of human rights law or other atrocities, violate the arms embargo or are responsible for certain offensive military flights. These measures fall within the scope of the Treaty and, therefore, in order to avoid any distortion of competition, Community measures are necessary to implement them as far as the Community is concerned.
(3) For the purpose of this Regulation, the territory of the Community is deemed to encompass the territories of the Member States to which the Treaty is applicable, under the conditions laid down in that Treaty.
(4) In order to ensure that the measures provided for in this Regulation are effective, this Regulation should enter into force on the day of its publication,
HAS ADOPTED THIS REGULATION:
Article 1
For the purposes of this Regulation, the following definitions shall apply:
1. ‘Sanctions Committee’ means the Committee of the Security Council of the United Nations which was established pursuant to paragraph 3 of UNSCR 1591 (2005);
2. ‘funds’ means financial assets and benefits of every kind, including but not limited to:
(a) cash, cheques, claims on money, drafts, money orders and other payment instruments;
(b) deposits with financial institutions or other entities, balances on accounts, debts and debt obligations;
(c) publicly and privately traded securities and debt instruments, including stocks and shares, certificates representing securities, bonds, notes, warrants, debentures and derivatives contracts;
(d) interest, dividends or other income on or value accruing from or generated by assets;
(e) credit, right of set-off, guarantees, performance bonds or other financial commitments;
(f) letters of credit, bills of lading, bills of sale;
(g) documents evidencing an interest in funds or financial resources;
(h) any other instrument of export-financing;
3. ‘freezing of funds’ means preventing any move, transfer, alteration, use of, access to, or dealing with funds in any way that would result in any change in their volume, amount, location, ownership, possession, character, destination or other change that would enable the funds to be used, including portfolio management;
4. ‘economic resources’ means assets of every kind, whether tangible or intangible, movable or immovable, which are not funds but can be used to obtain funds, goods or services;
5. ‘freezing of economic resources’ means preventing their use to obtain funds, goods or services in any way, including, but not limited to, the selling, hiring or mortgaging of them.
Article 2
1. All funds and economic resources belonging to, or owned or held by, the natural or legal persons, entities or bodies listed in Annex I shall be frozen.
2. No funds or economic resources shall be made available, directly or indirectly, to or for the benefit of the natural or legal persons or entities listed in Annex I.
3. The participation, knowingly and intentionally, in activities the object or effect of which is, directly or indirectly, to circumvent the measures referred to in paragraphs 1 and 2 shall be prohibited.
Article 3
1. By way of derogation from Article 2, and provided they have notified the intention to authorise access to such funds and economic resources to the Sanctions Committee and have not received a negative decision by the Sanctions Committee within two working days of such notification, the competent authorities of the Member States, as listed in Annex II, may authorise the release of certain frozen funds or economic resources or the making available of certain frozen funds or economic resources, under such conditions as they deem appropriate, after having determined that the funds or economic resources concerned are:
(a) necessary for basic expenses, including payments for foodstuffs, rent or mortgage, medicines and medical treatment, taxes, insurance premiums, and public utility charges;
(b) intended exclusively for payment of reasonable professional fees and reimbursement of incurred expenses associated with the provision of legal services;
(c) intended exclusively for payment of fees or service charges for routine holding or maintenance of frozen funds or economic resources;
2. By way of derogation from Article 2, and provided they have notified that determination to the Sanctions Committee and that the determination has been approved by that Committee, the competent authorities of the Member States, as listed in Annex II, may authorise the release of certain frozen funds or economic resources or the making available of certain frozen funds or economic resources, after having determined that the funds or economic resources are necessary for extraordinary expenses.
Article 4
By way of derogation from Article 2, the competent authorities of the Member States listed in Annex II may authorise the release of certain frozen funds or economic resources, if the following conditions are met:
(a) the funds or economic resources are subject of a judicial, administrative or arbitral lien established prior to 29 March 2005 or of a judicial, administrative or arbitral judgment rendered prior to that date;
(b) the funds or economic resources will be used exclusively to satisfy claims secured by such a lien or recognised as valid in such a judgment, within the limits set by applicable laws and regulations governing the rights of persons having such claims;
(c) the lien or judgment is not for the benefit of a person, entity or body listed in Annex I;
(d) recognising the lien or judgment is not contrary to public policy in the Member State concerned;
(e) the competent authorities have notified the lien or judgement to the Sanctions Committee.
Article 5
1. Article 2(2) shall not apply to the addition to frozen accounts of:
(a) interest or other earnings on those accounts; or
(b) payments due under contracts, agreements or obligations that were concluded or arose prior to the date on which those accounts became subject to this Regulation,
provided that any such interest, other earnings and payments are frozen in accordance with Article 2(1).
2. Article 2(2) shall not prevent the crediting of the frozen accounts by financial institutions that receive funds transferred by third parties to the account of the listed person or entity, provided that any such additions to such accounts will also be frozen. The financial institution shall inform the competent authorities about such transactions without delay.
Article 7
1. Without prejudice to the applicable rules concerning reporting, confidentiality and professional secrecy and to the provisions of Article 284 of the Treaty, natural and legal persons, entities and bodies shall:
(a) supply immediately any information which would facilitate compliance with this Regulation, such as accounts and amounts frozen in accordance with Article 2, to the competent authorities of the Member States listed in Annex II where they are resident or located, and shall transmit such information, directly or through these competent authorities, to the Commission;
(b) cooperate with the competent authorities listed in Annex II in any verification of this information.
2. Any additional information directly received by the Commission shall be made available to the competent authorities of the Member State concerned.
3. Any information provided or received in accordance with paragraphs 1 and 2 shall be used only for the purposes for which it was provided or received.
Article 8
The freezing of funds and economic resources or the refusal to make funds or economic resources available, carried out in good faith on the basis that such action is in accordance with this Regulation, shall not give rise to liability of any kind on the part of the natural or legal person or entity implementing it, or its directors or employees, unless it is proved that the funds and economic resources were frozen as result of negligence.
Article 9
The Commission and Member States shall immediately inform each other of the measures taken under this Regulation and shall supply each other with any other relevant information at their disposal in connection with this Regulation, in particular information in respect of violation and enforcement problems and judgments handed down by national courts.
Article 10
1. The Commission shall be empowered to:
(a) amend Annex I on the basis of determinations made by either the United Nations Security Council or the Sanctions Committee; and
(b) amend Annex II on the basis of information supplied by Member States.
2. Without prejudice to the rights and obligations of the Member States under the Charter of the United Nations, the Commission shall maintain all necessary contacts with the Sanctions Committee for the purpose of the effective implementation of this Regulation.
Article 11
Member States shall lay down the rules on penalties applicable to infringements of the provisions of this Regulation and shall take all measures necessary to ensure that they are implemented. The sanctions provided for must be effective, proportionate and dissuasive.
Member States shall notify those rules to the Commission without delay after the entry into force of this Regulation and shall notify it of any subsequent amendment.
Article 12
This Regulation shall apply
(a) within the territory of the Community, including its airspace and on board any aircraft or any vessel under the jurisdiction of a Member State;
(b) to any person inside or outside the territory of the Community who is a national of a Member State;
(c) to any legal person, group or entity which is incorporated or constituted under the law of a Member State;
(d) to any legal person, group or entity doing business within the Community.
Article 13
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Union .
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels,
For the Council
The President
ANNEX I
List of natural and legal persons or entities referred to in Article 2
ANNEX II
List of competent authorities referred to in Articles 3, 4, 5, 6 and 7
(to be completed by the Member States)
BELGIUM
CZECH REPUBLIC
DENMARK
GERMANY
ESTONIA
GREECE
SPAIN
FRANCE
IRELAND
ITALY
CYPRUS
LATVIA
LITHUANIA
LUXEMBOURG
HUNGARY
MALTA
NETHERLANDS
AUSTRIA
POLAND
PORTUGAL
SLOVENIA
SLOVAKIA
FINLAND
SWEDEN
UNITED KINGDOM
EUROPEAN COMMUNITY
Commission of the European Communities
Directorate-General for External Relations
Directorate CFSP
Unit A.2: Legal and institutional matters, CFSP Joint Actions, Sanctions, Kimberley Process
CHAR 12/163
B - 1049 Bruxelles/Brussel
Tel. (32-2) 296 25 56
Fax (32-2) 296 75 63
Relex-Sanctions@cec.eu.int
[1] OJ L
[2] OJ C , , p. .
[3] OJ C , , p. .
