Order of the President of the Court of 7 February 2006 — Energy Technologies ET SA
v Office for Harmonisation in the Internal Market (Trade Marks and Designs), Aparellaje eléctrico, SL
(Case C-197/05 P) [1]
(2006/C 154/34)
Language of the case: English
The President of the Court has ordered that the case be removed from the register.
[1] OJ C 243, 1.10.2005.
--------------------------------------------------
