Commission Regulation (EC) No 534/2005
of 5 April 2005
establishing unit values for the determination of the customs value of certain perishable goods
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2913/92 of 12 October 1992 establishing the Community Customs Code [1],
Having regard to Commission Regulation (EEC) No 2454/93 [2] laying down provisions for the implementation of Regulation (EEC) No 2913/92, and in particular Article 173(1) thereof,
Whereas:
(1) Articles 173 to 177 of Regulation (EEC) No 2454/93 provide that the Commission shall periodically establish unit values for the products referred to in the classification in Annex 26 to that Regulation.
(2) The result of applying the rules and criteria laid down in the abovementioned Articles to the elements communicated to the Commission in accordance with Article 173(2) of Regulation (EEC) No 2454/93 is that unit values set out in the Annex to this Regulation should be established in regard to the products in question,
HAS ADOPTED THIS REGULATION:
Article 1
The unit values provided for in Article 173(1) of Regulation (EEC) No 2454/93 are hereby established as set out in the table in the Annex hereto.
Article 2
This Regulation shall enter into force on 8 April 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 5 April 2005.
For the Commission
Günter Verheugen
Vice-President
[1] OJ L 302, 19.10.1992, p. 1. Regulation as last amended by Regulation (EC) No 2700/2000 (OJ L 311, 12.12.2000, p. 17).
[2] OJ L 253, 11.10.1993, p. 1. Regulation as last amended by Commission Regulation (EC) No 2286/2003 (OJ L 343, 31.12.2003, p. 1).
--------------------------------------------------
ANNEX
Code | Description | Amount of unit values per 100 kg |
Species, varieties, CN code | EURLTLSEK | CYPLVLGBP | CZKMTL | DKKPLN | EEKSIT | HUFSKK |
1.10 | New potatoes07019050 | 30,74 | 17,95 | 923,18 | 229,03 | 480,99 | 7600,71 |
106,14 | 21,40 | 13,23 | 125,66 | 7368,62 | 1191,21 |
281,86 | 21,11 | | | | |
1.30 | Onions (other than seed)07031019 | 33,59 | 19,62 | 1008,71 | 250,24 | 525,55 | 8304,83 |
115,98 | 23,38 | 14,46 | 137,30 | 8051,24 | 1301,57 |
307,97 | 23,06 | | | | |
1.40 | Garlic07032000 | 138,34 | 80,79 | 4154,49 | 1030,66 | 2164,55 | 34204,57 |
477,66 | 96,28 | 59,54 | 565,48 | 33160,10 | 5360,68 |
1268,40 | 94,99 | | | | |
1.50 | Leeksex07039000 | 59,67 | 34,85 | 1791,98 | 444,56 | 933,65 | 14753,68 |
206,03 | 41,53 | 25,68 | 243,91 | 14303,16 | 2312,26 |
547,11 | 40,97 | | | | |
1.60 | Cauliflowers07041000 | — | — | — | — | — | — |
1.80 | White cabbages and red cabbages07049010 | 60,89 | 35,56 | 1828,59 | 453,64 | 952,72 | 15055,05 |
210,24 | 42,38 | 26,21 | 248,89 | 14595,33 | 2359,49 |
558,28 | 41,81 | | | | |
1.90 | Sprouting broccoli or calabrese (Brassica oleracea L. convar. botrytis (L.) Alef var. italica Plenck)ex07049090 | — | — | — | — | — | — |
— | — | — | — | — | — |
— | — | — | — | — | — |
1.100 | Chinese cabbageex07049090 | 104,01 | 60,74 | 3123,52 | 774,90 | 1627,40 | 25716,47 |
359,13 | 72,39 | 44,77 | 425,15 | 24931,20 | 4030,39 |
953,64 | 71,42 | | | | |
1.110 | Cabbage lettuce (head lettuce)07051100 | — | — | — | — | — | — |
1.130 | Carrotsex07061000 | 37,34 | 21,81 | 1121,36 | 278,19 | 584,24 | 9232,32 |
128,93 | 25,99 | 16,07 | 152,63 | 8950,40 | 1446,93 |
342,36 | 25,64 | | | | |
1.140 | Radishesex07069090 | 71,65 | 41,84 | 2151,72 | 533,81 | 1121,08 | 17715,46 |
247,39 | 49,87 | 30,84 | 292,88 | 17174,51 | 2776,44 |
656,94 | 49,20 | | | | |
1.160 | Peas (Pisum sativum)07081000 | 367,79 | 214,79 | 11045,09 | 2740,11 | 5754,66 | 90935,98 |
1269,90 | 255,98 | 158,30 | 1503,38 | 88159,17 | 14251,85 |
3372,15 | 252,54 | | | | |
1.170 | Beans: | | | | | | |
1.170.1 | —Beans (Vigna spp., Phaseolus spp.)ex07082000 | 191,68 | 111,94 | 5756,39 | 1428,07 | 2999,17 | 47393,28 |
661,84 | 133,41 | 82,50 | 783,52 | 45946,08 | 7427,66 |
1757,47 | 131,62 | | | | |
1.170.2 | —Beans (Phaseolus spp., vulgaris var. Compressus Savi)ex07082000 | 255,23 | 149,05 | 7664,81 | 1901,51 | 3993,48 | 63105,62 |
881,26 | 177,64 | 109,85 | 1043,28 | 61178,63 | 9890,16 |
2340,13 | 175,25 | | | | |
1.180 | Broad beansex07089000 | — | — | — | — | — | — |
1.190 | Globe artichokes07091000 | — | — | — | — | — | — |
1.200 | Asparagus: | | | | | | |
1.200.1 | —greenex07092000 | 266,99 | 155,92 | 8018,02 | 1989,14 | 4177,51 | 66013,65 |
921,87 | 185,83 | 114,91 | 1091,35 | 63997,86 | 10345,92 |
2447,96 | 183,33 | | | | |
1.200.2 | —otherex07092000 | 552,61 | 322,72 | 16595,37 | 4117,04 | 8646,43 | 136632,30 |
1908,04 | 384,62 | 237,84 | 2258,84 | 132460,11 | 21413,56 |
5066,70 | 379,45 | | | | |
1.210 | Aubergines (eggplants)07093000 | 164,01 | 95,78 | 4925,29 | 1221,88 | 2566,15 | 40550,68 |
566,28 | 114,15 | 70,59 | 670,39 | 39312,43 | 6355,26 |
1503,73 | 112,62 | | | | |
1.220 | Ribbed celery (Apium graveolens L., var. dulce (Mill.) Pers.)ex07094000 | 117,15 | 68,41 | 3518,09 | 872,78 | 1832,98 | 28965,02 |
404,49 | 81,54 | 50,42 | 478,86 | 28080,54 | 4539,51 |
1074,10 | 80,44 | | | | |
1.230 | Chantarelles07095910 | 926,44 | 541,04 | 27821,92 | 6902,16 | 14495,64 | 229062,29 |
3198,81 | 644,80 | 398,74 | 3786,92 | 222067,67 | 35899,55 |
8494,25 | 636,14 | | | | |
1.240 | Sweet peppers07096010 | 178,38 | 104,17 | 5356,95 | 1328,97 | 2791,05 | 44104,65 |
615,91 | 124,15 | 76,78 | 729,15 | 42757,88 | 6912,26 |
1635,52 | 122,49 | | | | |
1.250 | Fennel07099050 | — | — | — | — | — | — |
1.270 | Sweet potatoes, whole, fresh (intended for human consumption)07142010 | 116,78 | 68,20 | 3507,04 | 870,04 | 1827,22 | 28873,98 |
403,22 | 81,28 | 50,26 | 477,35 | 27992,29 | 4525,24 |
1070,73 | 80,19 | | | | |
2.10 | Chestnuts (Castanea spp.) freshex08024000 | — | — | — | — | — | — |
2.30 | Pineapples, freshex08043000 | 94,92 | 55,43 | 2850,51 | 707,16 | 1485,16 | 23468,67 |
327,74 | 66,06 | 40,85 | 387,99 | 22752,04 | 3678,10 |
870,28 | 65,18 | | | | |
2.40 | Avocados, freshex08044000 | 142,78 | 83,38 | 4287,89 | 1063,76 | 2234,06 | 35302,90 |
493,00 | 99,38 | 61,45 | 583,64 | 34224,89 | 5532,81 |
1309,13 | 98,04 | | | | |
2.50 | Guavas and mangoes, freshex080450 | — | — | — | — | — | — |
2.60 | Sweet oranges, fresh: | | | | | | |
2.60.1 | —Sanguines and semi-sanguines08051010 | — | — | — | — | — | — |
— | — | — | — | — | — |
— | — | | | | |
2.60.2 | —Navels, navelines, navelates, salustianas, vernas, Valencia lates, Maltese, shamoutis, ovalis, trovita and hamlins08051030 | — | — | — | — | — | — |
— | — | — | — | — | — |
— | — | | | | |
2.60.3 | —Others08051050 | — | — | — | — | — | — |
— | — | — | — | — | — |
— | — | | | | |
2.70 | Mandarins (including tangerines and satsumas), fresh; clementines, wilkings and similar citrus hybrids, fresh: | | | | | | |
2.70.1 | —Clementinesex08052010 | 106,21 | 62,03 | 3189,59 | 791,29 | 1661,83 | 26260,42 |
366,72 | 73,92 | 45,71 | 434,14 | 25458,54 | 4115,64 |
973,81 | 72,93 | | | | |
2.70.2 | —Monreales and satsumasex08052030 | 105,90 | 61,85 | 3180,41 | 789,01 | 1657,04 | 26184,81 |
365,67 | 73,71 | 45,58 | 432,89 | 25385,24 | 4103,79 |
971,00 | 72,72 | | | | |
2.70.3 | —Mandarines and wilkingsex08052050 | 66,63 | 38,91 | 2001,08 | 496,43 | 1042,59 | 16475,18 |
230,07 | 46,38 | 28,68 | 272,37 | 15972,10 | 2582,06 |
610,94 | 45,75 | | | | |
2.70.4 | —Tangerines and othersex08052070ex08052090 | 74,24 | 43,36 | 2229,56 | 553,12 | 1161,64 | 18356,36 |
256,34 | 51,67 | 31,95 | 303,47 | 17795,83 | 2876,88 |
680,70 | 50,98 | | | | |
2.85 | Limes (Citrus aurantifolia, Citrus latifolia), fresh08055090 | 58,11 | 33,94 | 1745,14 | 432,94 | 909,25 | 14368,04 |
200,65 | 40,45 | 25,01 | 237,54 | 13929,30 | 2251,82 |
532,81 | 39,90 | | | | |
2.90 | Grapefruit, fresh: | | | | | | |
2.90.1 | —whiteex08054000 | 71,09 | 41,52 | 2135,01 | 529,66 | 1112,37 | 17577,84 |
245,47 | 49,48 | 30,60 | 290,60 | 17041,09 | 2754,87 |
651,83 | 48,82 | | | | |
2.90.2 | —pinkex08054000 | 87,96 | 51,37 | 2641,45 | 655,30 | 1376,23 | 21747,47 |
303,70 | 61,22 | 37,86 | 359,53 | 21083,39 | 3408,35 |
806,46 | 60,40 | | | | |
2.100 | Table grapes08061010 | 139,35 | 81,38 | 4184,97 | 1038,22 | 2180,43 | 34455,52 |
481,16 | 96,99 | 59,98 | 569,63 | 33403,39 | 5400,01 |
1277,70 | 95,69 | | | | |
2.110 | Water melons08071100 | 34,38 | 20,08 | 1032,47 | 256,14 | 537,93 | 8500,45 |
118,71 | 23,93 | 14,80 | 140,53 | 8240,89 | 1332,23 |
315,22 | 23,61 | | | | |
2.120 | Melons (other than water melons): | | | | | | |
2.120.1 | —Amarillo, cuper, honey dew (including cantalene), onteniente, piel de sapo (including verde liso), rochet, tendral, futuroex08071900 | 57,63 | 33,66 | 1730,68 | 429,35 | 901,71 | 14248,99 |
198,98 | 40,11 | 24,80 | 235,57 | 13813,89 | 2233,16 |
528,39 | 39,57 | | | | |
2.120.2 | —Otherex08071900 | 106,01 | 61,91 | 3183,72 | 789,83 | 1658,76 | 26212,04 |
366,05 | 73,79 | 45,63 | 433,34 | 25411,63 | 4108,05 |
972,01 | 72,79 | | | | |
2.140 | Pears | | | | | | |
2.140.1 | —Pears — nashi (Pyrus pyrifolia),Pears — Ya (Pyrus bretscheideri)ex08082050 | — | — | — | — | — | — |
— | — | — | — | — | — |
— | — | | | | |
2.140.2 | —Otherex08082050 | — | — | — | — | — | — |
— | — | — | — | — | — |
— | — | | | | |
2.150 | Apricots08091000 | 705,36 | 411,93 | 21182,67 | 5255,07 | 11036,49 | 174400,26 |
2435,47 | 490,93 | 303,59 | 2883,23 | 169074,79 | 27332,70 |
6467,23 | 484,34 | | | | |
2.160 | Cherries0809209508092005 | 610,83 | 356,72 | 18343,84 | 4550,81 | 9557,41 | 151027,72 |
2109,07 | 425,14 | 262,90 | 2496,83 | 146415,95 | 23669,66 |
5600,52 | 419,43 | | | | |
2.170 | Peaches08093090 | 147,21 | 85,97 | 4420,95 | 1096,76 | 2303,38 | 36398,36 |
508,30 | 102,46 | 63,36 | 601,75 | 35286,91 | 5704,50 |
1349,75 | 101,08 | | | | |
2.180 | Nectarinesex08093010 | 118,41 | 69,15 | 3555,96 | 882,17 | 1852,71 | 29276,75 |
408,84 | 82,41 | 50,96 | 484,01 | 28382,76 | 4588,37 |
1085,66 | 81,31 | | | | |
2.190 | Plums08094005 | 90,22 | 52,69 | 2709,25 | 672,12 | 1411,56 | 22305,71 |
311,50 | 62,79 | 38,83 | 368,76 | 21624,58 | 3495,84 |
827,16 | 61,95 | | | | |
2.200 | Strawberries08101000 | 179,72 | 104,96 | 5397,24 | 1338,97 | 2812,04 | 44436,34 |
620,55 | 125,09 | 77,35 | 734,63 | 43079,44 | 6964,24 |
1647,82 | 123,41 | | | | |
2.205 | Raspberries08102010 | 304,95 | 178,09 | 9157,95 | 2271,94 | 4771,43 | 75398,89 |
1052,93 | 212,25 | 131,25 | 1246,51 | 73096,51 | 11816,81 |
2796,00 | 209,39 | | | | |
2.210 | Fruit of the species Vaccinium myrtillus08104030 | 1455,44 | 849,98 | 43708,32 | 10843,32 | 22772,69 | 359857,54 |
5025,34 | 1012,99 | 626,42 | 5949,26 | 348868,97 | 56398,30 |
13344,49 | 999,38 | | | | |
2.220 | Kiwi fruit (Actinidia chinensis Planch.)08105000 | 64,65 | 37,76 | 1941,50 | 481,66 | 1011,55 | 15984,71 |
223,22 | 45,00 | 27,83 | 264,26 | 15496,61 | 2505,19 |
592,76 | 44,39 | | | | |
2.230 | Pomegranatesex08109095 | 314,59 | 183,72 | 9447,45 | 2343,76 | 4922,26 | 77782,38 |
1086,22 | 218,95 | 135,40 | 1285,92 | 75407,22 | 12190,36 |
2884,38 | 216,01 | | | | |
2.240 | Khakis (including sharon fruit)ex08109095 | 143,42 | 83,76 | 4306,99 | 1068,49 | 2244,00 | 35460,10 |
495,19 | 99,82 | 61,73 | 586,24 | 34377,29 | 5557,45 |
1314,96 | 98,48 | | | | |
2.250 | Lycheesex081090 | — | — | — | — | — | — |
--------------------------------------------------
