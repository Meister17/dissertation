COMMISSION REGULATION (EC) No 2042/98 of 25 September 1998 on special conditions for the granting of private storage aid for pigmeat
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2759/75 of 29 October 1975 on the common organisation of the market in pigmeat (1), as last amended by Regulation (EC) No 3290/94 (2), and in particular Articles 4(6) and 5(4) thereof,
Whereas intervention measures may be taken in respect of pigmeat if, on the representative markets of the Community, the average price for pig carcases is less than 103 % of the basic price and is likely to remain below that level;
Whereas the market situation has been characterized by a marked fall in prices below the level mentioned; whereas, in view of seasonal and cyclical trends, this situation could persist;
Whereas intervention measures must be taken; whereas these can be limited to the granting of private storage aid;
Whereas it is appropriate, in view of the possible length of the present crisis in the pigmeat sector, to oblige operators to export the products under storage contracts in order to prevent them from coming back on the Community market after removal from storage; whereas these exports should take place according to certain provisions provided for in Commission Regulation (EEC) No 3444/90 (3), as last amended by Regulation (EC) No 3533/93 (4);
Whereas Article 3 of Council Regulation (EEC) No 2763/75 (5) provides for the possibility of curtailing or extending the storage period; whereas, therefore, provision should be made to fix not only the amounts of aid for a specific period of storage but also the amounts to be added or deducted if this period is curtailed or extended;
Whereas, in order to facilitate administrative and control work resulting from the conclusion of contracts, minimum quantities should be fixed;
Whereas the security should be fixed at a level such as will oblige the storer to fulfil the obligations undertaken by him;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Pigmeat,
HAS ADOPTED THIS REGULATION:
Article 1
1. As from 28 September 1998 applications for private storage aid may be introduced in accordance with the provisions of Regulation (EEC) No 3444/90. The list of products which qualify for aid and the relevant amounts are set out in the Annex hereto.
2. Without prejudice to the provisions provided for in Article 3(4) of Regulation (EEC) No 3444/90, all the products under contract must be exported at the latest on the 60th day following the maximum contractual storage period, in accordance with one of the three possibilities provided for in Article 9(4) of the said Regulation.
3. The provisions of Article 6(3) of Regulation (EEC) No 3444/90 apply mutatis mutandis to the unexported quantities.
4. If the period of storage is extended or curtailed, the amount of the aid shall be adjusted accordingly. The amounts of the supplements and deductions per month and per day are set out in columns 6 and 7 of the said Annex.
Article 2
The minimum quantities per contract and per product shall be as follows:
(a) 10 tonnes for boned products;
(b) 15 tonnes for all the other products.
Article 3
The security shall be 20 % of the amounts of aid set out in the Annex.
Article 4
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 25 September 1998.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ L 282, 1. 11. 1975, p. 1.
(2) OJ L 349, 31. 12. 1994, p. 105.
(3) OJ L 333, 30. 11. 1990, p. 22.
(4) OJ L 321, 23. 12. 1993, p. 9.
(5) OJ L 282, 1. 11. 1975, p. 19.
ANNEX
>TABLE>
