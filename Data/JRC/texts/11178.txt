Council Common Position 2006/51/CFSP
of 30 January 2006
renewing restrictive measures against Zimbabwe
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty on European Union, and in particular Article 15 thereof,
Whereas:
(1) On 19 February 2004, the Council adopted Common Position 2004/161/CFSP [1] renewing restrictive measures against Zimbabwe for a period of 12 months from 21 February 2004.
(2) Council Common Position 2005/146/CFSP [2], adopted on 21 February 2005, extended Common Position 2004/161/CFSP until 20 February 2006.
(3) In view of the situation in Zimbabwe, Common Position 2004/161/CFSP should be extended for a further period of 12 months,
HAS ADOPTED THIS COMMON POSITION:
Article 1
Common Position 2004/161/CFSP shall be extended until 20 February 2007.
Article 2
This Common Position shall take effect on the date of its adoption.
Article 3
This Common Position shall be published in the Official Journal of the European Union.
Done at Brussels, 30 January 2006.
For the Council
The President
U. Plassnik
[1] OJ L 50, 20.2.2004, p. 66.
[2] OJ L 49, 22.2.2005, p. 30.
--------------------------------------------------
