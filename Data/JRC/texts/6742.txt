Commission Regulation (EC) No 757/2005
of 18 May 2005
amending for the 46th time Council Regulation (EC) No 881/2002 imposing certain specific restrictive measures directed against certain persons and entities associated with Usama bin Laden, the Al-Qaida network and the Taliban, and repealing Council Regulation (EC) No 467/2001
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 881/2002 of 27 May 2002 imposing certain specific restrictive measures directed against certain persons and entities associated with Usama bin Laden, the Al-Qaida network and the Taliban, and repealing Council Regulation (EC) No 467/2001 prohibiting the export of certain goods and services to Afghanistan, strengthening the flight ban and extending the freezing of funds and other financial resources in respect of the Taliban of Afghanistan [1], and in particular Article 7(1), first indent, thereof,
Whereas:
(1) Annex I to Regulation (EC) No 881/2002 lists the persons, groups and entities covered by the freezing of funds and economic resources pursuant to that Regulation.
(2) On 16 May 2005, the Sanctions Committee of the United Nations Security Council decided to amend the list of persons, groups and entities to whom the freezing of funds and economic resources should apply. Annex I should therefore be amended accordingly.
(3) In order to ensure that the measures provided for in this Regulation are effective, this Regulation must enter into force immediately,
HAS ADOPTED THIS REGULATION:
Article 1
Annex I to Regulation (EC) No 881/2002 is amended as set out in the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 18 May 2005.
For the Commission
Eneko Landáburu
Director-General for External Relations
[1] OJ L 139, 29.5.2002, p. 9. Regulation as last amended by Commission Regulation (EC) No 717/2005 (OJ L 121, 13.5.2005, p. 62).
--------------------------------------------------
ANNEX
Annex I to Regulation (EC) No 881/2002 is amended as follows:
The following entries shall be added under the heading "Natural persons":
1. Joko Pitono (alias (a) Joko Pitoyo, (b) Joko Pintono, (c) Dulmatin, (d) Dul Matin, (e) Abdul Martin, (f) Abdul Matin, (g) Amar Umar, (h) Amar Usman, (i) Anar Usman, (j) Djoko Supriyanto, (k) Jak Imron, (l) Muktamar, (m) Novarianto, (n) Topel). Date of birth: (a) 16.6.1970, (b) 6.6.1970. Place of birth: Petarukan village, Pemalang, Central Java, Indonesia. Nationality: Indonesian.
2. Abu Rusdan (alias (a) Abu Thoriq, (b) Rusdjan, (c) Rusjan, (d) Rusydan, (e) Thoriquddin, (f) Thoriquiddin, (g) Thoriquidin, (h) Toriquddin). Date of birth: 16.8.1960. Place of birth: Kudus, Central Java, Indonesia.
3. Zulkarnaen (alias (a) Zulkarnan, (b) Zulkarnain, (c) Zulkarnin, (d) Arif Sunarso, (e) Aris Sumarsono, (f) Aris Sunarso, (g) Ustad Daud Zulkarnaen, (h) Murshid). Date of birth: 1963. Place of birth: Gebang village, Masaran, Sragen, Central Java, Indonesia. Nationality: Indonesian.
--------------------------------------------------
