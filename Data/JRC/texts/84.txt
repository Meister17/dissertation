COUNCIL DIRECTIVE of 17 April 1989 coordinating the requirements for the drawing-up, scrutiny and distribution of the prospectus to be published when transferable securities are offered to the public (89/298/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 54 thereof,
Having regard to the proposal from the Commission (1),
In cooperation with the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas investment in transferable securities, like any other form of investment, involves risks; whereas the protection of investors requires that they be put in a position to make a correct assessment of such risks so as to be able to take investment decisions in full knowledge of the facts;
Whereas the provision of full, appropriate information Whereas the provision of full, appropriate information concerning transferable securities and the issuers of such securities promotes the protection of investors;
Whereas, moreover, such information is an effective means of increasing confidence in transferable securities and thus contributes to the proper functioning and development of transferable securities markets;
Whereas a genuine Community information policy relating to transferable securities should therefore be introduced; whereas, by virtue of the safeguards that it offers investors and its impact on the proper functioning of transferable securities markets, such an information policy is capable of promoting the interpenetration of national transferable securities markets and thus encouraging the creation of a genuine European capital market;
Whereas Council Directive 80/390/EEC of 17 March 1980 Whereas Council Directive 80/390/EEC of 17 March 1980 coordinating the requirements for the drawing-up, scrutiny and distribution of the listing particulars to be published for the admission of securities to official stock exchange listing (4), as last amended by Directive 87/345/EEC (5), represents an important step in the implementation of such a
Community information policy; whereas that Directive coordinates the information to be published when securities are admitted to stock exchange listing concerning the nature of the securities offered and the issuers of such securities, so of the securities offered and the issuers of such securities, so as to enable investors to make an informed assessment of the assets and liabilities, financial position, profits and losses and prospects of issuers and of the rights attaching to such securities;
Whereas such an information policy also requires that when transferable securities are offered to the public for the first time in a Member State, whether by, or on behalf of the issuer or a third party, whether or not they are subsequently listed, a prospectus containing information of this nature must be made available to investors; whereas it is also necessary to coordinate the contents of that prospectus in order to achieve equivalence of the minimum safeguards afforded to investors in the various Member States;
Whereas, so far, it has proved impossible to furnish a common definition of the term ´public offer' and all its constituent parts;
Whereas, in cases where a public offer is of transferable securities which are to be admitted to official listing on a stock exchange, information similar to that required by Directive 80/390/EEC, whilst being adapted to the circumstances of the public offer, must be supplied; whereas, for public offers of transferable securities that are not to be admitted to official stock exchange listing, less detailed information can be required so as not to burden small and medium-sized issuers unduly; whereas, for public offers of transferable securities that are to be admitted to official stock exchange listing, the degree of coordination achieved is such that a prospectus approved by the competent authorities of a Member State can be used for public offers of the same securities in another Member State on the basis of mutual recognition; whereas mutual recognition should also apply where public offer prospectuses comply with the basic standards laid down in Directive 80/390/EEC and are approved by the competent authorities even in the absence of a request for admission to official stock exchange listing;
Whereas in order to ensure that the purposes of this Directive will be fully realized it is necessary to include within the scope of this Directive transferable securities issued by companies or firms governed by the laws of third countries;
Whereas it is advisable to provide for the extension, by means of agreements to be concluded by the Community with third countries, of the recognition of prospectuses from those countries on a reciprocal basis:
HAS ADOPTED THIS DIRECTIVE:
SECTION I
General provisions
Article 1 1. This Directive shall apply to transferable securities which are offered to the public for the first time in a Member State provided that these securities are not already listed on a stock exchange situated or operating in that Member State.
2. Where an offer to the public is for part only of the transferable securities from a single issue, the Member States need not require that another prospectus be published if the other part is subsequently offered to the public.
Article 2 This Directive shall not apply:
1. to the following types of offer:
(a) where transferable securities are offered to persons in the context of their trades, professions or occupations, and/or
(b) where transferable securities are offered to a restricted circle of persons, and/or
(c) where the selling price of all the transferable securities offered does not exceed ECU 40 000, and/or
(d) where the transferable securities offered can be acquired only for a consideration of at least ECU 40 000 per investor;
2. to transferable securities of the following types:
(a) to transferable securities offered in individual denominations of at least ECU 40 000;
(b)
to units issued by collective investment undertakings other than of the closed-end type;
(c)
to transferable securities issued by a State or by one of a State's regional or local authorities or by public international bodies of which one or more Member States are members;
(d)
to transferable securities offered in connection with a take-over bid;
(e)
to transferable securities offered in connection with a merger;
(f)
to shares allotted free of charge to the holders of shares;
(g)
to shares or transferable securities equivalent to shares offered in exchange for shares in the same company if the offer of such new securities does not involve any overall increase in the company's issued shares capital;
(h)
to transferable securities offered by their employer or by an affiliated undertaking to or for the benefit of serving or former employees;
(i)
to transferable securities resulting from the conversion of convertible debt securities or from the exercise of the rights conferred by warrants or to shares offered in exchange for exchangeable debt securities, provided that a public offer prospectus or listing particulars relating to those convertible or exchangeable debt securities or those warrants were published in the same Member State;
(j)
to transferable securities issued, with a view to their obtaining the means necessary to achieve their disinterested objectives, by associations with legal status or non-profit-making bodies, recognized by the State;
(k)
to shares or transferable securities equivalent to shares, ownership of which entitles the holder to avail himself of the services rendered by bodies such as ´building societies', ´Crédits populaires', ´Genossenschaftsbanken', or ´Industrial and Provident Societies', or to become a member of such a body;
(l)
to Euro-securities which are not the subject of a generalized campaign of advertising or canvassing.
Article 3 For the purposes of this Directive:
(a) ´collective investment undertakings other than of the closed-end type' shall mean unit trusts and investment companies:
- the object of which is the collective investment of capital provided by the public, and which operate on the principle of risk spreading, and
- the units of which are, at the holders' request, repurchased or redeemed, directly or indirectly, out of the assets of those undertakings. Action taken by such undertakings to ensure that the stock exchange value of their units does not significantly vary from their net asset value shall be regarded as equivalent to such repurchase or redemption;
(b)
´units of a collective investment undertaking' shall mean transferable securities issued by a collective investment undertaking representing the rights of the participants in such an undertaking over its assets;
(c)
´issuers' shall mean companies and other legal persons and any undertakings the transferable securities of which are offered to the public;
(d)
´credit institution' shall mean an undertaking the business of which is to receive deposits or other repayable funds from the public and to grant credits for its own account, including credit institutions such as referred to in Article 2 of Directive 77/780/EEC (6), as last amended by Directive 86/524/EEC (7);
(e)
´transferable securities' shall mean shares in companies and other transferable securities equivalent to shares in companies, debt securities having a maturity of at least one year and other transferable securities equivalent to debt securities, and any other transferable security giving the right to acquire any such transferable securities by subscription or exchange;
(f)
´Euro-securities' shall mean transferable securities which:
- are to be underwritten and distributed by a syndicate at least two of the members of which have their registered offices in different States, and
- are offered on a significant scale in one or more States other than that of the issuer's registered office, and
- may be subscribed for or initially acquired only through a credit institution or other financial institution.
Article 4 Member States shall ensure that any offer of transferable securities to the public within their territories is subject to the publication of a prospectus by the person making the offer.
Article 5 Member States may provide for partial or complete exemption from the obligation to publish a prospectus where the transferable securities being offered to the public are:
(a) debt securities or other transferable securities equivalent to debt securities issued in a continuous or repeated manner by credit institutions or other financial institutions equivalent to credit institutions which regularly publish their annual accounts and which, within the Community, are set up or governed by a special law or pursuant to such a law, or are subject to public supervision intended to protect savings;
(b) debt securities or other transferable securities equivalent to debt securities issued by companies and other legal persons which are nationals of a Member State and which:
- in carrying on their business, benefit from State monopolies, and
- are set up or governed by a special law or pursuant to such a law or whose borrowings are unconditionally and irrevocably guaranteed by a Member State or one of a Member State's regional or local authorities;
(c)
debt securities issued by legal persons, other than companies, which are nationals of a Member State, and
- were set up by special law, and
- whose activities are governed by that law and consist solely in
i(i) raising funds under state control through the issue of debt securities; and
(ii) financing production by means of the resources which they have raised and resources provided by a Member State and/or acquiring a holding in such production, and
- the debt securities of which are, for the purposes of admission to official listing, considered by national law as debt securities issued or guaranteed by the State.
Article 6 If a full prospectus has been published in a Member State within the previous 12 months, the following prospectus drawn up by the same issuer in the same State, but relating to different transferable securities, may indicate only those changes likely to influence the value of the securities which have occurred since publication of the full prospectus.
However, that prospectus may be made available only accompanied by the full prospectus to which it relates or by a reference thereto.
SECTION II
Contents and arrangements for the scrutiny and distribution of the prospectus for transferable securities for which admission to official stock exchange listing is sought
Article 7 Where a public offer relates to transferable securities which at the time of the offer are the subject of an application for admission to official listing on a stock exchange situated or operating within the same Member State, the contents of the prospectus and the procedures for scrutinizing and distributing it shall, subject to adaptations appropriate to the circumstances of a public offer, be determined in accordance with Directive 80/390/EEC.
Article 8 1. Where a public offer is made in one Member State and admission is sought to official listing on a stock exchange situated in another Member State, the person making the public offer shall have the possibility in the Member State in which the public offer is to be made of drawing up a prospectus the contents and procedures for scrutiny and distribution of which shall, subject to adaptations appropriate to the circumstances of a public offer, be determined in accordance with Directive 80/390/EEC.
2. Paragraph 1 shall apply only in those Member States which in general provide for the prior scrutiny of public offer prospectuses.
Article 9 A prospectus must be published or made available to the public not later than the time when an offer is made to the public.
Article 10 1. Where a prospectus in accordance with Article 7 or 8 is or is to be published, the advertisements, notices, posters and documents announcing the public offer must be communicated in advance to the competent authorities. The aforementioned documents must mention that there is a prospectus and state where the prospectus is published.
2. If the Member States authorize the distribution of the documents referred to in paragraph 1 before the prospectus is available, those documents must state that a prospectus will be published and indicate where members of the public will be able to obtain it.
3. The prospectus must be published either:
- by insertion in one or more newspapers circulated throughout the Member State in which the public offer is made, or
- in the form of a brochure to be made available, free of charge, to the public in the Member State in which the public offer is made and at the registered office of the person making the public offer and at the offices of the financial organizations retained to act as paying agents of the latter in the Member State where the offer is made.
4. In addition, either the complete prospectus or a notice stating where the prospectus has been published and where it may be obtained by the public must be inserted in a publication designated by the Member State in which the public offer is made.
SECTION III
Contents and arrangements for the distribution of the prospectus for transferable securities for which admission to official stock-exchange listing is not sought
Article 11 1. Where a public offer relates to transferable securities other than those referred to in Articles 7 and 8, the prospectus must contain the information which, according to the particular nature of the issuer and of the transferable securities offered to the public, is necessary to enable investors to make an informed assessment of the assets and liabilities, financial position, profits and losses and prospects of the issuer and of the rights attaching to the transferable securities.
2. In order to fulfil the obligation referred to in
paragraph 1, the prospectus shall, subject to the possibilities for exemption provided for in Articles 5 and 13, contain in as easily analysable and comprehensible a form a possible, at least the information listed below:
(a) those responsible for the prospectus (names, functions and declarations by them that to the best of their knowledge the information contained in the prospectus is in accordance with the facts and that the prospectus makes no omission likely to affect its import);
(b) the offer to the public and the transferable securities being offered (nature of the securities being offered, the amount and purpose of the issue, the number of securities issued and the rights attaching to them; the income tax withheld at source; the period during which the offer is open; the date on which entitlement to dividends or interest arises; the persons underwriting or guaranteeing the offer; any restrictions on the free transferability of the securities being offered and the markets on which they may be traded; the establishments serving as paying agents; if known, the price at which the securities are offered, or else, if national rules so provide, the procedure and timetable for fixing the price if it is not known when the prospectus is being drawn up; methods of payment; the procedure for the exercise of any right of pre-emption and the methods of and time-limits for delivery of the securities);
(c) the issuer (name, registered office; its date of incorporation, the legislation applicable to the issuer and the issuer's legal form, its objects, indication of the register and of the entry number therein) and its capital (amount of the subscribed capital, the number and main particulars of the securities of which the capital consists and any part of the capital still to be paid up; the amount of any convertible debt securities, exchangeable debt securities or debt securities with warrants and the procedures for conversion, exchange or subscription; where appropriate, the group of undertakings to which the issuer belongs; in the case of shares, the following additional information must be supplied: any shares not representing capital, the amount of the authorized capital and the duration of the authorization; in so far as
they are known, indication of the shareholders who directly or indirectly exercise or could exercise a determining role in the management of the issuer);
(d)
the issuer's principal activities (description of its principal activities, and, where appropriate, any exceptional factors which have influenced its activities; any dependence on patents, licences or contracts if these are of fundamental importance; information regarding investments in progress where they are significant; any legal proceedings having an important effect on the issuer's financial position);
(e)
the issuer's assets and liabilities, financial position and profits and losses (own accounts and, where appropriate, consolidated accounts; if the issuer prepares consolidated annual accounts only, it shall include those accounts in the prospectus; if the issuer prepares both own and consolidated accounts, it shall include both types of account in the prospectus; however, the issuer may include only one of the two, provided that the accounts which are not included do not provide any significant additional information); interim accounts if any have been published since the end of the previous financial year; the name of the person responsible for auditing the accounts; if that person has qualified them or refused an audit report, the fact must be stated and the reasons given;
(f)
the issuer's administration, management and supervision (names, addresses, functions; in the case of an offer to the public of shares in a limited-liability company, remuneration of the members of the issuer's administrative, management and supervisory bodies);
(g)
to the extent that such information would have a significant impact on any assessment that might be made of the issuer, recent developments in its business and prospects (the most significant recent trends concerning the development of the issuer's business since the end of the proceding financial year, information on the issuer's prospects for at least the current financial year).
3. Where a public offer relates to debt securities guaranteed by one or more legal persons, the information specified in paragraph 2 (c) to (g) must also be given with respect to the guarantor or guarantors.
4. Where a public offer relates to convertible debt securities, exchangeable debt securities or debt securities with warrants or to the warrants themselves, information must also be given with regard to the nature of the shares or debt securities to which they confer entitlement and the conditions of and procedures for conversion, exchange or subscription. Where the issuer of the shares or debt securities is not the issuer of the debt securities or warrants the information specified in paragraph 2 (c) to (g) must also be given with respect to the issuer of the shares or debt securities.
5. If the period of existence of the issuer is less than any period mentioned in paragraph 2, the information need be provided only for the period of the issuer's existence.
6. Where certain information specified in paragraph 2 is found to be inappropriate to the issuer's sphere of activity or its legal form or to the transferable securities being offered, a prospectus giving equivalent information must be drawn up.
7. Where shares are offered on a pre-emptive basis to shareholders of the issuer on the occasion of their admission to dealing on a stock exchange market, the Member States
or bodies designated by them may allow some of the information specified in paragraph 2 (d), (e) and (f) to be omitted, provided that investors already possess up-to-date information about the issuer equivalent to that required by Section III as a result of stock exchange disclosure requirements.
8. Where a class of shares has been admitted to dealing on a stock exchange market, the Member States or bodies designated by them may allow a partial or complete exemption from the obligation to publish a prospectus if the number or estimated market value or the nominal value or, in the absence of a nominal value, the accounting par value of the shares offered amounts to less than 10 % of the number or of the corresponding value of shares of the same class already admitted to dealing, provided that investors already possess up-to-date information about the issuer equivalent to that required by Section III as a result of stock exchange disclosure requirements.
Article 12 1. However, the Member States may provide that the person making a public offer shall have the possibility of drawing up a prospectus the contents of which shall, subject to adaptations appropriate to the circumstances of a public offer, be determined in accordance with Directive 80/390/EEC.
2. The prior scrutiny of the prospectus referred to in paragraph 1 must be carried out by the bodies designated by the Member States even in the absence of a request for admission to official stock-exchange listing.
Article 13 1. The Member States or the bodies designated by them may authorize the omission from the prospectus referred to in Article 11 of certain information prescribed by this Directive:
(a) if that information is of minor importance only and is not likely of influence assessment of the issuer's assets and liabilities, financial position, profits and losses and prospects; or
(b) if disclosure of that information would be contrary to the public interest or seriously detrimental to the issuer,
provided that, in the latter case, omission would not
be likely to mislead the public with regard to facts
and circumstances essential for assessment of the transferable securities.
2. Where the initiator of an offer is neither the issuer nor a third party acting on the issuer's behalf, the Member States or the bodies designated by them may authorize omission from the prospectus of certain information which would not normally be in the initiator's possession.
3. The Member States or the bodies designated by them may provide for partial or complete exemption from the obligation to publish a prospectus where the information which those making the offer are required to supply by law, regulation or rules made by bodies enabled to do so by national laws is available to investors not later than the time when the prospectus must be or should have been published or made available to the public, in accordance with this Directive, in the form of documents giving information at least equivalent to that required by Section III.
Article 14 Article 14 A prospectus must be communicated, before its publication, to the bodies designated for that purpose in each Member State in which the transferable securities are offered to the public for the first time.
Article 15 A prospectus must be published or made available to the public in the Member State in which an offer to the public is made in accordance with the procedures laid down by that Member State.
Article 16 A prospectus must be published or made available to the public not later than the time when an offer is made to the public.
Article 17 1. When a prospectus complying with Article 11 or 12 is or must be published, the advertisements, notices, posters and documents announcing the public offer distributed or made available to members of the public by the person making the public offer, must be communicated in advance to the bodies designated in accordance with Article 14, if such bodies carry out prior scrutiny of public offer prospectuses. In such a case, the latter shall determine whether the documents concerned should be checked before publication. Such documents must state that a prospectus exists and indicate where it is published.
2. If Member States authorize the dissemination of the documents referred to in paragraph 1 before the prospectus is available, those documents must state that a prospectus will be published and indicate where members of the public will be able to obtain it.
Article 18 Any significant new factor or significant inaccuracy in a prospectus capable of affecting assessment of the transferable securities which arises or is noted between the publication of the prospectus and the definitive closure of a public offer must be mentioned or rectified in a supplement to the prospectus, to be published or made available to the public in accordance with at least the same arrangements as were applied when the original prospectus was disseminated or in accordance with procedures laid down by the Member States or by the bodies designated by them.
SECTION IV
Cooperation between Member States Article 19 The Member States shall designate the bodies, which may be the same as those referred to in Article 14, which shall cooperate with each other for the purposes of the proper application of this Directive and shall use their best endeavours, within the framework of their responsibilities, to exchange all the information necessary to that end. Member States shall inform the Commission of the bodies thus designated. The Commission shall communicate that information to the other Member States.
Member States shall ensure that the bodies designated have the powers required for the accomplishment of their task.
Article 20 1. Where, for the same transferable securities, public offers are made simultaneously or within a short interval of one another in two or more Member States and where a public offer prospectus is drawn up in accordance with Arcticle 7, 8 or 12, the authority competent for the approval of the prospectus shall be that of the Member State in which the issuer has its registered office if the public offer or any application for admission to official listing on a stock exchange is made in that Member State.
2. However, if the Member State referred to in paragraph 1 does not provide in general for the prior scrutiny of public offer prospectuses and if only the public offer or an application for admission to listing is made in that Member State, as well as in all other cases, the person making the public offer must choose the supervisory authority from
those in the Member States in which the public offer is made and which provide in general for the prior scrutiny of public offer prospectuses.
SECTION V
Mutual recognition
Article 21 1. If approved in accordance with Article 20, a prospectus must, subject to translation if required, be recognized as complying or be deemed to comply with the laws of the other Member States in which the same transferable securities are offered to the public simultaneously or within a short interval of one another, without being subject to any form of approval there and without those States being able to require that additional information be included in the prospectus. Those Member States may, however, require that the prospectus include information specific to the market of the country in which the public offer is made concerning in particular the income tax system, the financial organizations retained to act as paying agents for the issuer in that country, and the way in which notices to investors are published.
2. A prospectus approved by the competent authorities within the meaning of Article 24a of Directive 80/390/EEC must be recognized as complying or be deemed to comply with the laws of another Member State in which the public offer is made, even if partial exemption or partial derogation has been granted pursuant to this Directive, provided, however, that:
(a) the partial exemption or partial derogation in question is of a type that is recognized in the rules of the other Member State concerned; and
(b) the circumstances that justify the partial exemption or partial derogation also exist in the other Member State concerned.
Even if the conditions laid down in (a) and (b) of the first subparagraph are not fulfilled, the Member State concerned may deem a prospectus approved by the competent authorities within the meaning of Article 20 to comply with its laws.
3. The person making the public offer shall communicate to the bodies designated by the other Member States in which the public offer is to be made the prospectus that it intends to use in that State. That prospectus must be the same as the prospectus approved by the authority referred to in Article 20.
4. The Member States may restrict the application of this Article to prospectuses concerning transferable securities of issuers who have their registered office in a Member State.
SECTION VI
Cooperation
Article 22 1. The competent authorities shall cooperate wherever necessary for the purpose of carrying out their duties and shall exchange any information required for that purpose.
2. Where a public offer concerning transferable securities giving a right to participate in company capital, either immediately or at the end of a maturity period, is made in one or more Member States other than that in which the registered office of the issuer of the shares to which those securities give entitlement is situated, while that issuer's shares have already been admitted to official listing in that Member State, the competent authorities of the Member State of the offer may act only after having consulted the competent authorities of the Member State in which the registered office of the issuer of the shares in question is situated in cases where the public offer prospectus is scrutinized.
Article 23 1. Member States shall provide that all persons then or previously employed by the authorities referred to in Article 20 shall be bound by the obligation of professional secrecy. This shall mean that they may not divulge any confidential information received in the course of their duties to any person or authority whatsoever, except by virtue of provisions laid down by law.
2. Paragraph 1 shall not prevent the various Member State authorities referred to in Article 20 from forwarding information as provided for in this Directive. The information thus exchanged shall be covered by the obligation of professional secrecy applying the persons employed then or previously by the authority receiving such information.
3. Without prejudice to cases covered by criminal law, the authorities referred to in Article 20 receiving information pursuant to Article 21 may use it only to carry out their functions or in the context of an administrative appeal or in court proceedings relating to the carrying out of those functions.
SECTION VII
Negotiations with non-member countries
Article 24 The Community may, by means of agreements with one or more non-member countries concluded pursuant to the Treaty, recognize public offer prospectuses drawn up and scrutinized in accordance with the rules of the non-member
country or countries concerned as meeting the requirements of this Directive, subject to reciprocity, provided that the rules concerned give investors protections equivalent to that afforded by this Directive, even if those rules differ from the provisions of this Directive.
SECTION VIII
Contact Committee
Article 25 1. The Contact Committee set up by Article 20 of Council Directive 79/279/EEC of 5 March 1979 coordinating the conditions for the admission of transferable securities to official stock-exchange listing (8), as last amended by Directive 82/148/EEC (9), shall also have as its function:
(a) to facilitate, without prejudice to Articles 169 and 170 of the Treaty, the harmonized implementation of this Directive through regular consultations on any practical problems arising from its application on which exchanges of views are deemed useful;
(b) to facilitate consultation between the Member States on the supplements and improvements to prospectuses which they are entitled to require or recommend at national level;
(c) to advise the Commission, if necessary, on any additions or amendments to be made to this Directive.
2. It shall not be the function of the Contact Committee to appraise the merits of decisions taken in individual cases.
SECTION IX
Final provisions
Article 26 1. Member States shall take the measures necessary for them to comply with this Directive by 17 April 1989. They shall forthwith inform the Commission thereof.
2. Member States shall communicate to the Commission the texts of the main provisions of national law which they adopt in the field governed by this Directive.
Article 27 This Directive is addressed to the Member States.
Done at Luxembourg, 17 April 1989.
For the Council
The President
C. SOLCHAGA CATALAN
(1) OJ No C 226, 31. 8. 1982, p. 4.
(2) OJ No C 125, 17. 5. 1982, p. 176 and OJ No C 69, 20. 3. 1989.
(3) OJ No C 310, 30. 11. 1981, p. 50.
(4) OJ No L 100, 17. 4. 1980, p. 1.
(5) OJ No L 185, 4. 7. 1987, p. 81.(6) OJ No L 322, 17. 12. 1977, p. 30.
(7) OJ No L 309, 4. 11. 1986, p. 15.
