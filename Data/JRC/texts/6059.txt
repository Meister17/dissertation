Commission Regulation (EC) No 420/2005
of 14 March 2005
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 15 March 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 14 March 2005.
For the Commission
J. M. Silva Rodríguez
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 1947/2002 (OJ L 299, 1.11.2002, p. 17).
--------------------------------------------------
ANNEX
to Commission Regulation of 14 March 2005 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code | Standard import value |
07020000 | 052 | 114,3 |
204 | 70,1 |
212 | 143,7 |
624 | 193,8 |
999 | 130,5 |
07070005 | 052 | 162,2 |
068 | 170,0 |
096 | 128,5 |
204 | 70,7 |
999 | 132,9 |
07091000 | 220 | 18,4 |
999 | 18,4 |
07099070 | 052 | 164,1 |
204 | 98,1 |
999 | 131,1 |
08051020 | 052 | 54,5 |
204 | 50,9 |
212 | 56,9 |
220 | 47,8 |
400 | 51,1 |
624 | 64,6 |
999 | 54,3 |
08055010 | 052 | 69,2 |
220 | 70,4 |
400 | 67,6 |
999 | 69,1 |
08081080 | 388 | 83,6 |
400 | 96,9 |
404 | 74,7 |
508 | 64,2 |
512 | 75,9 |
528 | 64,0 |
720 | 59,8 |
999 | 74,2 |
08082050 | 052 | 186,2 |
388 | 69,6 |
400 | 92,6 |
512 | 54,2 |
528 | 64,9 |
720 | 42,6 |
999 | 85,0 |
--------------------------------------------------
