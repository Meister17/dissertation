Action brought on 16 March 2006 — Studio Bichara e.a. v Commission
Parties
Applicants: Studio Bichara SrL, Riccardo Bichara and Maria Proietti (Rome, Italy) (represented by: M. Pappalardo and M.C. Santacroce, lawyers)
Defendant: Commission of the European Communities
Form of order sought
- Hold the Commission Delegation in Papua New Guinea liable for non-contractual damages and OLAF liable for non-contractual damages in connection with Project No 8.ACP.PNG.003;
- order the Commission and OLAF to pay compensation for damage suffered as a result of unlawful conduct in the execution of Project No 8.ACP.PNG.003 provisionally assessed at Euro 5884873,99;
- order the Commission to pay the costs.
Pleas in law and main arguments
This action concerns a claim for compensation for damage suffered by the applicant company, an Italian engineering consultancy which operated for a number of years in the context of EU funded programmes, as a result of the conduct of officials of the Commission Delegation in Papua New Guinea and the European Anti-Fraud Office (OLAF) in relation to contract for services No 8.ACP.PNG.003, which was supported by the European Development Fund.
It should be noted in this regard that in December 1999 the applicant company was awarded the contract in question to design improvement works for nine education establishments in a number of regions in Papua New Guinea.
The applicant company considers, together with two other applicants, that the Community has incurred non-contractual liability in this case as a consequence of:
- improper interference by the Commission Delegation in Papua New Guinea in the contractual relationship between Studio Bichara and the local Government with regard to the contract for services at issue. That interference obliged the applicant company to terminate the contract prematurely and made it impossible for there to be any amicable solution to the dispute between the contracting parties.
- the conduct on the part of OLAF in the course of investigations OF/2002/0261 and OF/2002/0322. That conduct is to be regarded as inconsistent both with OLAF's duty to carry out its investigations in complete independence, even in its dealings with the European Commission, and the principles of justice, impartiality and the presumption of innocence of those under investigation.
--------------------------------------------------
