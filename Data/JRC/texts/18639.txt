COUNCIL DIRECTIVE 93/32/EEC of 14 June 1993 on passenger hand-holds on two-wheel motor vehicles
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 100a thereof,
Having regard to Council Directive 92/61/EEC of 30 June 1992 relating to the type-approval of two- or three-wheel motor vehicles (1),
Having regard to the proposal from the Commission (2),
In cooperation with the European Parliament (3)
Having regard to the opinion of the Economic and Social Committee (4),
Whereas the internal market comprises and area without internal frontiers in which the free movement of goods, persons, services and capital is ensured; whereas the measures required for that purpose need to be adopted;
Whereas, with regard to their passenger hand-holds, in each Member State two-wheel motor vehicles must display certain technical characteristics laid down by mandatory provisions which differ from one Member State to another; whereas, as a result of their differences, such provisions constitute a barrier to trade within the Community;
Whereas these obstacles to the operation of the internal market may be removed if the same requirements are adopted by all Member States in place of their national rules;
Whereas it is necessary to draw up harmonized requirements concerning passenger hand-holds on two-wheel motor vehicles in order to enable the type-approval and component type-approval procedures laid down in Directive 92/61/EEC to be applied for each type of such vehicle;
Whereas, given the scale and impact of the action proposed in the sector in question, the Community measures covered by this Directive are necessary, indeed essential, to achieve the aim in view, which is to establish Community vehicle type-approval; whereas that aim cannot be adequately achieved by the Member States individually,
HAS ADOPTED THIS DIRECTIVE:
Article 1
This Directive and its Annex apply to passenger hand-holds of all types of two-wheel vehicles as defined in Article 1 of Council Directive 92/61/EEC.
Article 2
The procedure for the granting of component type-approval in respect of passenger hand-holds on a type of two-wheel motor vehicle and the conditions governing the free movement of said vehicles shall be as laid down in Chapters II and III of Directive 92/61/EEC.
Article 3
Any amendments necessary to adapt the requirements of the Annexes to technical progress shall be adopted in accordance with the procedure laid down in Article 13 of Directive 70/156/EEC (5).
Article 4
1. Member States shall adopt and publish the provisions necessary to comply with this Directive not later than 14 December 1993. They shall forthwith inform the Commission thereof.
When the Member States adopt these provisions, they shall contain a reference to this Directive or shall be accompanied by such a reference on the occasion of their official publication. The methods of making such a reference shall be laid down by the Member States.
From the date mentioned in the first subparagraph Member States may not, for reasons connected with the passenger hand-holds, prohibit the initial entry into service of vehicles which conform to this Directive.
They shall apply the provisions referred to in the first subparagraph as from 14 June 1995.
2. Member States shall communicate to the Commission the texts of the provisions of national law which they adopt in the field covered by this Directive.
Artikel 5
This Directive is addressed to the Member States.
Done at Luxembourg, 14 June 1993.
For the Council
The President
J. TROEJBORG
(1) OJ No L 225, 10. 8. 1992, p. 72.(2) OJ No C 293, 9. 11. 1992, p. 49.(3) OJ No C 337, 21. 12. 1992, p. 103 and OJ No C 150, 31. 5. 1993.(4) OJ No C 73, 15. 3. 1993, p. 22.(5) OJ No L 42, 23. 2. 1970, p. 1. Directive as last amended by Directive 92/53/EEC (OJ No L 225, 10. 8. 1992, p. 1).
ANNEX
1. GENERAL REQUIREMENTS
Where provision is made for carriage of a passenger, the vehicle must be fitted with a passenger hand-hold system. That must take the form of a strap or a hand-grip or hand-grips.
1.1. Strap
The strap must be fitted to the seat in such a way that it may easily be used by the passenger. The strap and its attachment must be designed in such a way that they withstand, without snapping, a vertical traction force of 2 000 N applied statically to the centre of the surface of the strap at a maximum pressure of 2 MPa.
1.2. Hand-grip
If a hand-grip is used it must be close to the saddle and symmetrical to the median longitudinal plane of the vehicle.
This hand-grip must be designed in such a way that it is able to withstand, without snapping, a vertical traction force of 2 000 N applied statically to the centre of the surface of the hand-grip at a maximum presssure of 2 MPa.
If two hand-grips are used they must be fitted one on each side in a symmetrical manner.
These hand-grips must be designed in such a way that each is able to withstand, without snapping, a vertical traction force of 1 000 N applied statically to the centre of the surface of the hand grip at a maximum pressure of 1 MPa.
Appendix 1 Information document in respect of passenger hand-holds on a type of two-wheel motor vehicle
(to be attached to the application for component type-approval if this is submitted separately from the application for vehicle type-approval)
Order No (assigned by the applicant): .
The application for component type-approval in respect of passenger hand-holds on a two-wheel motor vehicle must contain the information set out under the following points in Annex II to Council Directive 92/61/EEC:
- Part A, sections:
- 0.1
- 0.2
- 0.4 to 0.6;
- Part B, sections:
- 1.5 to 1.5.2.
Appendix 2 Name of administration
Component type-approval certificate in respect of restraint devices for passengers on a type of two-wheel motor vehicle
MODEL
Report No . by technical service . date .
Component type-approval No: . Extension No: .
1. Trade mark or name of vehicle: .
2. Type of vehicle: .
3. Manufacturer's name and address: .
.
4. Name and address of manufacturer's representative (if any):.
.
5. Date vehicle submitted for test: .
6. Component type-approval granted/refused (1):
7. Place: .
8. Date: .
9. Signature: .
(1) Delete as appropriate.
