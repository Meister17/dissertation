[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 20.01.2006
COM(2006) 11 final
2006/0004(COD)
Proposal for a
REGULATION OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL
on the European system of integrated social protection statistics (ESSPROS)
(presented by the Commission)
EXPLANATORY MEMORANDUM
CONTEXT OF THE PROPOSAL |
110 | Grounds for and objectives of the proposal The Lisbon European Council of 23 and 24 March 2000 gave impetus to a process of policy exchange among the Member States of the EU on the modernisation of social protection systems. The Commission Communication (2003) 261 of May 2003 entitled "Strengthening the social dimension of the Lisbon strategy: Streamlining open coordination in the field of social protection" includes a statement on the need for indicators. ESSPROS is mentioned as a key instrument. The proposal for a legal framework for ESSPROS (European System of Integrated Social Protection Statistics) will improve the usefulness of current data collections in terms of timeliness, coverage and comparability. |
120 | General context As agreed at the European Council of October 2003 in the context of streamlining the open method of coordination (OMC), an annual Joint Report on Social Inclusion and Social Protection will become the core reporting instrument. OMC in the fields of both Social inclusion and Pensions requires data from ESSPROS, thus reflecting the need for comparable, timely and reliable statistics in the area of social policy. The current data from ESSPROS are recognised as being the lowest statistical common denominator to which the numerous variations in the national welfare systems can be reduced. The current yearly data collection on social protection is done on a voluntary basis (gentlemen's agreement) and is now based on a common comprehensive methodology (ESSPROS Manual 1996). The collection was previously based on a 1981 methodology, which replaced the Social Accounts started in 1963 for six Member States. There is a need to improve the timeliness of data collections, which can only be solved by the networking of national data providers collecting data from a wide variety of sources. Widening coverage to include qualitative data in order to complete and improve quantitative data on expenditure and receipts by scheme will provide comprehensive knowledge of the different systems and thus make for comparative analysis. Widening the coverage by completing gross benefits with net benefits and identifying the impact of the fiscal system on social protection will allow more comparable data between Member States to be available, in particular on the amount of social benefits really received by beneficiaries. The OMC on pensions requires information on these beneficiaries. As a unique data source, the output of the module on pension beneficiaries will make it possible to assess how pension systems are evolving. |
130 | Existing provisions in the area covered by the proposal Council Recommendation of 27 July 1992 on the convergence of social protection objectives and policies (92/442/EEC) |
141 | Consistency with the other policies and objectives of the Union Not applicable. |
CONSULTATION OF INTERESTED PARTIES AND IMPACT ASSESSMENT |
Consultation of interested parties |
219 | Not relevant |
Collection and use of expertise |
221 | Scientific/expertise domains concerned The experts consulted came from the National Statistical Systems, representing National Statistical Institutes, Ministries of Social Affairs and other bodies involved in the production of ESSPROS data. |
222 | Methodology used Written consultation and discussion in Working Group meetings. |
223 | Main organisations/experts consulted A first draft of the proposed text was presented to the Working Group on Social Protection Statistics held on 12 and 13 April 2005. Member States were invited to send detailed comments on the draft proposal to Eurostat. A second draft was then prepared and sent for a second round of written comments on 20 June 2005. A revised proposal was submitted for opinion at the meeting of the Directors of Social Statistics on 28 and 29 September 2005. |
2249 | Summary of advice received and used The existence of potentially serious risks with irreversible consequences has not been mentioned. |
225 | Most of the comments received throughout this process were taken into account and helped to clarify, refine and simplify the proposed text. |
226 | Means used to make the expert advice publicly available The results of the first expert discussions were included in the minutes of the meeting of the Working Group on "Social Protection Statistics" and in the minutes of the meeting of Directors of Social Statistics. |
230 | Impact assessment The alternative would be to continue working under a gentlemen's agreement for the production of social protection statistics. However, this would not guarantee the availability of all statistics and indicators required for policy-making at EU level. The second alternative would be for the proposal not to include modules. This would mean having to present any module as one or more separate Council and European Parliament Regulations. |
LEGAL ELEMENTS OF THE PROPOSAL |
305 | Summary of the proposed action The objective of this Regulation is to establish a framework for current and foreseeable activities in the field of social protection statistics. The current yearly data collection on social protection (gentlemen's agreement) covers data on expenditure and receipts by social protection scheme (the so-called ESSPROS core system) and, from 2004, data on pension beneficiaries. Pilot data collections on net social benefits will be prepared. The proposed Regulation will be supplemented by Commission implementing Regulations: firstly, the core system includes both quantitative and qualitative information; the second will be the module on pension beneficiaries. The module on net social protection benefits will be the subject of a Commission Regulation should the results of feasibility studies and pilot data collections be positive. |
310 | Legal basis Article 285 provides the legal basis for Community statistics. The Council, acting in accordance with the co-decision procedure, has to adopt measures for the production of statistics where necessary for the performance of the activities of the Community. This Article sets out requirements relating to the production of Community statistics and requires conformity to standards of impartiality, reliability, objectivity, scientific independence, cost-effectiveness and statistical confidentiality. |
320 | Subsidiarity principle The subsidiarity principle applies insofar as the proposal does not fall under the exclusive competence of the Community. |
The objectives of the proposal cannot be sufficiently achieved by the Member States alone for the following reasons: |
321 | In accordance with the principle of subsidiarity as set out in Article 5 of the EC Treaty, the objective of the proposed action, namely the systematic submission of harmonised data on Community Social Protection Statistics, cannot be sufficiently achieved by the Member States acting individually and can therefore be better achieved by the Community. |
323 | Experience has shown that Member States collect data on Social Protection but use a variety of concepts and definitions which result in the non-comparability of data at EU level and thus seriously reduce their usefulness for the purposes of analysing social protection systems in the EU. |
Community action will better achieve the objectives of the proposal for the following reasons: |
324 | The Regulation provides for the coordination and harmonisation of social protection data at EU level. Although the Commission is best able to organise the collection of Community statistics, the Member States are responsible for organising and operating the national statistical systems. The statistics covered are broadly based on Eurostat's existing annual social protection data collection and the recent collection on pension beneficiaries. However, the Regulation accommodates the possibility of using other sources and developing additional statistical tools if necessary to cover clearly identified emerging information needs. |
325 | It is essential that EU-wide information is available to support the open method of coordination in the fields of Social inclusion and Pensions. |
327 | The analysis of Community Social Protection Statistics has to be undertaken at EU level using harmonised and comparable data. |
The proposal therefore complies with the subsidiarity principle. |
Proportionality principle The proposal complies with the proportionality principle for the following reasons: |
331 | It is recognised that there are wide variations in the structures of the Social Protection Systems in the Member States and, consequently, that there are differences in the tasks of the national authorities in collecting and compiling the data on Social Protection. Accordingly, the national authorities may use the techniques for the collection of data that are appropriate for the way the systems are organised. In accordance with the principle of proportionality, this Regulation confines itself to the minimum required to achieve this objective and does not go beyond what is necessary for that purpose. |
332 | The Regulation will establish a well defined framework for the production of Community social protection statistics and in this way facilitate the availability, planning and more efficient use of resources both at the Community and other levels involved (national, regional, local). |
Choice of instruments |
341 | Proposed instrument: regulation. |
342 | Other means would not be adequate for the following reason: Selection of the appropriate category for an act of the EP/Council depends on the legislative goal. Given the information needs at European level, the trend for Community statistics has been to use regulations rather than directives for basic acts. A regulation is preferable because it lays down the same law throughout the Community, leaving the Member States with no power to apply them incompletely or selectively; it is directly applicable, which means that it need not be transposed into national law. In contrast, directives, which aim at harmonisation of national laws, are binding on Member States as regards their objectives, but leave the national authorities the choice of form and methods used to obtain the objectives agreed upon at Community level; they must be transposed into national law. |
BUDGETARY IMPLICATION |
409 | The proposal has no implication for the Community budget. |
ADDITIONAL INFORMATION |
560 | European Economic Area The proposed act concerns an EEA matter and should therefore extend to the European Economic Area. |
1. 2006/0004 (COD)
Proposal for a
REGULATION OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL
on the European system of integrated social protection statistics (ESSPROS)
(Text with EEA relevance)
THE EUROPEAN PARLIAMENT AND THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 285(1) thereof,
Having regard to the proposal from the Commission[1],
Having regard to the opinion of the European Economic and Social Committee[2],
Acting in accordance with the procedure laid down in Article 251 of the Treaty[3],
Whereas:
(1) Article 2 of the Treaty establishing the European Community lists the promotion of a high level of social protection as one of the tasks of the European Community.
(2) The Lisbon European Council of March 2000 gave impetus to a process of policy exchange among the Member States on the modernisation of social protection systems.
(3) A Social Protection Committee was established by Council Decision of 29 June 2000 (2000/436/EC) in order to serve as a vehicle for cooperative exchange between the European Commission and the Member States of the EU about modernising and improving social protection systems.
(4) The Commission Communication (2003)261 of May 2003 outlined a strategy for streamlining the processes of open co-ordination in the social policy area with a view to strengthening the position of social protection and social inclusion within the Lisbon strategy. As agreed by the Council in October 2003, streamlining will come into effect from 2006 onwards. In this context, an annual Joint Report will become the core reporting instrument, with the task of bringing together the key analytical findings and political messages pertaining both to the OMC in the different strands where it is applied and to cross-cutting issues in social protection.
(5) The Open Method of Coordination (OMC) has put a new focus on the need for comparable, timely and reliable statistics in the social policy area. In particular, comparable statistics on social protection will be used in the annual Joint Reports.
(6) The Commission (Eurostat) is already collecting yearly data on social protection from the Member States on a voluntary basis. This action has become a consolidated practice in the Member States and is based on common methodological principles designed to ensure comparability of data.
(7) The production of specific Community statistics is governed by the rules set out in Council Regulation (EC) No 322/97 of 17 February 1997 on Community Statistics.
(8) The measures necessary for the implementation of this Regulation should be adopted in accordance with Council Decision 1999/468/EC of 28 June 1999 laying down the procedures for the exercise of implementing powers conferred on the Commission.
(9) After consulting the Statistical Programme Committee,
HAVE ADOPTED THIS REGULATION:
Article 1
Subject matter
The objective of this Regulation is to set up the European System of integrated Social Protection Statistics, hereinafter referred to as “ESSPROS”, by providing:
2. a methodological framework (based on common standards, definitions, classifications and accounting rules) to be used for compiling statistics on a comparable basis for the benefit of the Community;
3. time limits for the transmission of statistics compiled in accordance with ESSPROS.
Article 2
Definitions
For the purpose of this Regulation, the following definitions shall apply:
4. “Community statistics” shall have the meaning assigned to it in Article 2 of Regulation (EC) No 322/97.
5. “Social protection” encompasses all interventions from public or private bodies intended to relieve households and individuals of the burden of a defined set of risks or needs, provided that there is neither a simultaneous reciprocal nor an individual arrangement involved. The list of risks or needs that may give rise to social protection is fixed by convention as follows: Sickness/Health care; Disability; Old age; Survivors; Family/children; Unemployment; Housing; Social exclusion not elsewhere classified.
6. “Social protection scheme”: distinct body of rules, supported by one or more institutional units, governing the provision of social protection benefits and their financing.
7. “Social protection benefits”: transfers, in cash or in kind, by social protection schemes to households and individuals to relieve them of the burden of the defined set of risks or needs.
Article 3
Scope of the system
1. The statistics relating to the ESSPROS core system shall cover the financial flows on social protection expenditure and receipts.
These data shall be transmitted at social protection schemes level; for each scheme, detailed expenditure and receipts shall be provided following the ESSPROS classification.
The data to be transmitted in reference to aggregated classification, data provision and dissemination for quantitative information as well as the subject covered, updating and dissemination of qualitative information by schemes and detailed benefits are laid down in Annex I.
2. In addition to the core system, modules covering supplementary statistical information on particular aspects of social protection shall be added.
Article 4
Module on pension beneficiaries
A module on pension beneficiaries shall be added annually from the first year of data collection under this Regulation.
The subjects to be covered, data provision and dissemination are laid down in Annex II.
Article 5
Additional modules
1. For the purposes of a module on Net Social Protection Benefits, pilot data collections for the year 2005 shall be carried out in all Member States by the end of 2008. The subjects to be covered, data provision and dissemination are laid down in Annex III.
2. Based on a synthesis of these national pilot data collections, the decision to introduce this module and to launch full data collection not before 2010 shall be in accordance with the procedure set out in Article 8.
Article 6
Data sources
The statistics shall be based on the following data sources, according to their availability in the Member States and in accordance with national laws and practices:
8. registers and other administrative sources;
9. surveys;
10. estimates.
Article 7
Arrangements for implementation
The arrangements for implementing this Regulation, concerning the detailed classification of data covered, the definitions to be used, the formats for the transmission of data, the results to be transmitted, the criteria for the measurement of quality, the updating of the rules for dissemination shall be laid down in accordance with the regulatory procedure set out in Article 8, in particular for:
11. the ESSPROS core system,
12. the module on pension beneficiaries,
13. the module on Net Social Protection Benefits.
Article 8
Procedure
1. The Commission shall be assisted by the Statistical Programme Committee (SPC) established by Decision 89/382/EEC, Euratom.
2. Where reference is made to this paragraph, Articles 5 and 7 of Decision 1999/468/EC shall apply, having regard to the provisions of Article 8 thereof.
3. The period laid down in Article 5 (6) of Decision 1999/468/EC shall be set at three months.
Article 9
Entry into force
This Regulation shall enter into force on the 20th day following that of its publication in the Official Journal of the European Union .
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels,
For the European Parliament For the Council
The President The President
ANNEX I
ESSPROS core system
1. Quantitative data
1.1. Data transmitted
With reference to aggregated classification, transmitted data shall cover:
1.1.1. Expenditure
1.1.1.1. Social protection benefits classified by:
14. functions (corresponding to each risk or need), and
15. for each function by dual breakdown: means-tested versus non-means-tested, cash benefits (breakdown by periodic and lump sum benefits) versus benefits in kind.
1.1.1.2. Administration costs
1.1.1.3. Transfers to other schemes
1.1.1.4. Other expenditure
1.1.2. Receipts
1.1.2.1. Social contributions
1.1.2.2. General government contributions
1.1.2.3. Transfers from other schemes
1.1.2.4. Other receipts
Data covered (in reference to detailed classification) will be provided in accordance with the procedure laid down in Article 8.
1.2. Data provision
Statistics will be provided annually. Data will refer to the calendar year according to national practices. The deadline for data transmission is N+18 months, i.e. data for the calendar year N together with any revision of previous years have to be transmitted in June N+2 at the latest.
1.3. Dissemination
The Commission (Eurostat) will publish data on social protection expenditure at total schemes level by the end of N+22 months (October of the year N+2) based on the data referring to the financial year N. The Commission (Eurostat) will at the same time disseminate detailed data by schemes to specific users (national institutions compiling ESSPROS data, Commission departments and international institutions). These specific users will only be allowed to publish groups of schemes.
2. Qualitative information by schemes and detailed benefits
2.1. Subjects covered
For each scheme, qualitative information includes a general description of the scheme, a detailed description of the benefits and information on recent changes and reforms.
2.2. Data provision and updating of qualitative information
Annual updating of a complete set of qualitative information already provided shall be limited to changes in the social protection system and shall be transmitted together with quantitative data.
2.3. Dissemination
The Commission (Eurostat) will disseminate qualitative information at scheme level by the end of N+22 months (October of the year N+2).
ANNEX II
Module on Pension Beneficiaries
1. Subjects covered
This module covers data on pension beneficiaries, who are defined as recipients of one or more of the following periodic cash benefits of a social protection scheme:
16. Disability pension
17. Early retirement benefit due to reduced capacity to work
18. Old-age pension
19. Anticipated old-age pension
20. Partial pension
21. Survivor's pension
22. Early retirement benefit due to labour market reasons.
2. Data provision
Statistics will be provided annually. Data shall be stock data referring to the end of the year (31.12./1.1). The deadline for data transmission of the year N is the end of May of year N+2, broken down as follows:
23. by social protection scheme,
24. by gender for the total of schemes,
25. by type of management of the scheme (subject to positive feasibility study results).
3. Dissemination
The Commission (Eurostat) shall publish data for all schemes by the end of N+22 months (October of the year N+2) based on the data referring to the financial year N. The Commission (Eurostat) shall at the same time disseminate detailed data by schemes to specific users (national institutions compiling ESSPROS data, Commission departments and international institutions). These specific users shall only be allowed to publish groups of schemes.
ANNEX III
Pilot data collection on Net social protection benefits
1. Subjects covered
This collection covers the calculation of “Net social protection benefits”. Net social protection benefits are defined as the value of social protection benefits excluding taxes and social contributions paid by the benefits recipients complemented by the value of “Fiscal benefits”.
“Fiscal benefits” are defined as social protection provided in the form of tax breaks that would be defined as social protection benefits if they were provided in cash. Tax breaks promoting the provision of social protection or promoting private insurance plans are excluded.
2. Data provision
The appropriate fraction of personal income tax and social contributions levied on social protection benefits for the year 2005 must be indicated according to the different types of cash social protection benefits, preferably further subdivided for particular groups of homogenously taxed schemes. In difficult cases, results might be reported by appropriate groups of benefits, e.g. the total of the seven pension categories listed in Annex II or the total of the cash benefits of a specific function. Fiscal benefits should be provided for each item separately using the revenue forgone method.
[1] OJ C , , p. .
[2] OJ C , , p. .
[3] OJ C , , p. .
