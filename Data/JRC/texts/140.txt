COUNCIL DIRECTIVE of 14 June 1989 on indications or marks identifying the lot to which a foodstuff belongs (89/396/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 100a thereof,
Having regard to the proposal from the Commission (1),
In cooperation with the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas it is necessary to adopt measures with the aim of progressively establishing the internal market over the period expiring on 31 December 1992; whereas the internal market comprises an area without internal frontiers in which the free movement of goods, persons, services and capital is ensured;
Whereas trade in foodstuffs occupies a very important place in the internal market;
Whereas indication of the lot to which a foodstuff belongs meets the need for better information on the identity of products; whereas it is therefore a useful source of information when foodstuffs are the subject of dispute or constitute a health hazard for consumers;
Whereas Council Directive 79/112/EEC of 18 December 1978 on the approximation of the laws of the Member States relating to the labelling, presentation and advertising
of foodstuffs (4), as last amended by Directive
89/395/EEC (5), contains no provisions on indication of lot identification; whereas some Member States have meanwhile adopted national measures requiring such indication;
Whereas at international level there is now a general obligation to provide a reference to the manufacturing or packaging lot of prepackaged foodstuffs; whereas it is the Community's duty to contribute to the development of international trade;
Whereas it is therefore advisable to adopt rules of a general and horizontal nature in order to establish a common lot identification system;
Whereas the efficiency of this system depends on its application at the various marketing stages; whereas it is nevertheless desirable to exclude certain products and operations in particular those taking place at the start of the distribution network for agricultural products;
Whereas the concept of a lot implies that several sales units of a foodstuff have almost identical production, manufacture or packaging characteristics; whereas that concept therefore could not apply to bulk products or products which, owing to their individual specificity or heterogeneous nature, could not be considered as forming a homogeneous batch;
Whereas, in view of the variety of identification methods used, it is up to the trader to determine the lot and to affix the corresponding indication or mark;
Whereas, in order to satisfy the information requirements for which it is intended, this indication must be clearly distinguishable and recognizable as such;
Whereas the date of minimum durability or 'use by' date, may, in conformity with Directive 79/112/EEC, serve as the lot identification, provided it is indicated precisely,
HAS ADOPTED THIS DIRECTIVE:
Article 1
1. This Directive concerns the indication which allows identification of the lot to which a foodstuff belongs.
2. For the purposes of this Directive, 'lot' means a batch of sales units of a foodstuff produced, manufactured or packaged under practically the same conditions.
Article 2
1. A foodstuff may not be marketed unless it is accompanied by an indication as referred to in Article 1 (1).
2. However, paragraph 1 shall not apply:
(a) to agricultural products which, on leaving the holding are:
- sold or delivered to temporary storage, preparation or packaging stations,
- transported to producers' organizations, or
- collected for immediate integration into an operational preparation or processing system;
(b) when, at the point of sale to the ultimate consumer, the foodstuffs are not prepackaged, are packaged at the request of the purchaser or are prepackaged for immediate sale;
(c) to packagings or containers, the largest side of which has an area of less than 10 cm$.
3. Member States may, until 31 December 1996, refrain from requiring the indication referred to in Article 1 (1) to be mentioned in the case of the glass bottles intended for re-use which are indelibly marked and which therefore bear no label, ring or collar.
Article 3
The lot shall be determined in each case by the producer, manufacturer or packager of the foodstuff in question, or the first seller established within the Community.
The indication referred to in Article 1 (1) shall be determined and affixed under the responsibility of one or other of those operators. It shall be preceded by the letter 'L' except in cases where it is clearly distinguishable from the other indications on the label.
Article 4
When the foodstuffs are prepackaged, the indication referred to in Article 1 (1) and, where appropriate, the letter 'L' shall appear on the prepackaging or on a label attached thereto.
When the foodstuffs are not prepackaged, the indication referred to in Article 1 (1) and, where appropriate, the letter 'L' shall appear on the packaging or on the container or, failing that, on the relevant commercial documents.
It shall in all cases appear in such a way as to be easily visible, clearly legible and indelible.
Article 5
When the date of minimum durability or 'use by' date appears on the label, the indication referred to in Article 1 (1) need not appear on the foodstuff, provided that the date consists at least of the uncoded indication of the day and the month in that order.
Article 6
This Directive shall apply without prejudice to the indications laid down by specific Community provisions.
The Commission shall publish and keep up to date a list of the provisions in question.
Article 7
Member States shall, where necessary, amend their laws, regulations or administrative provisions so as to:
- authorize trade in products complying with this Directive by not later than 20 June 1990,
- prohibit trade in products not complying with this Directive with effect from 20 June 1991, however, trade in products placed on the market or labelled before that date and not conforming with this Directive may continue until stocks run out.
They shall forthwith inform the Commission thereof.
Article 8
This Directive is addressed to the Member States.
Done at Luxembourg, 14 June 1989.
For the Council
The President
P. SOLBES
(1) OJ No C 310, 20. 11. 1987, p. 2.
(2) OJ No C 167, 27. 6. 1988, p. 425, and OJ No C 120, 16. 5. 1989.
(3) OJ No C 95, 11. 4. 1988, p. 1.
(4) OJ No L 33, 8. 2. 1979, p. 1.
(5) See page 17 of this Official Journal.
