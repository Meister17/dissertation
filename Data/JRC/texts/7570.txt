COUNCIL REGULATION (EEC) N° 2009/86of 24 June 1986concerning the conclusion of the Cooperation Agreement between the European Economic Community, of the one part, and the countries parties to the General Treaty on Central American Economic Integration (Costa Rica, El Salvador, Guatemala, Honduras and Nicaragua) and Panama, of the other part
THE COUNCIL OF THE EUROPEAN COMMUNITIES,Having regard to the Treaty establishing the European Economic Community, and in particular Articles 113 and 235 thereof,Having regard to the proposal from the Commission,Having regard to the opinion of the European Parliament,Whereas the Community should approve, for the attainment of its ends in the sphere of external economic relations, the Cooperation Agreement with the countries parties to the General Treaty on Central American Economic Integration and Panama;Whereas certain forms of economic cooperation provided for by the Agreement exceed the powers of action provided for by the Treaty in the common commercial policy,
HAS ADOPTED THIS REGULATION:
Article 1
The Cooperation Agreement between the European Economic Community, of the one part, and the countries
parties to the General Treaty on Central American Economic Integration (Costa Rica, El Salvador, Guatemala, Honduras and Nicaragua) and Panama, of the other part, is hereby approved on behalf of the Community.The text of the Agreement is attached to this Regulation.
Article 2
The President of the Council shall give the notification provided for in Article 11 of the Agreement (1).
Article 3
The Community shall be represented within the Joint Cooperation Committee provided for in Article 7 of the Agreement, by the Commission assisted by representatives from the Member States.
Article 4
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Luxembourg, 24 June 1986.For the Council
The President
G. BRAKS
(1) The date of entry into force of the Agreement will be published in the Official Journal of the European Communities by the General Secretariat of the Council.
