Decision of the European Central Bank
of 9 December 2005
on the approval of the volume of coin issuance in 2006
(ECB/2005/14)
(2005/918/EC)
THE GOVERNING COUNCIL OF THE EUROPEAN CENTRAL BANK,
Having regard to the Treaty establishing the European Community, and in particular to Article 106(2) thereof,
Whereas:
(1) The European Central Bank (ECB) has the exclusive right from 1 January 1999 to approve the volume of coins issued by the Member States that have adopted the euro (the participating Member States).
(2) The participating Member States have submitted to the ECB for approval their estimates of the volume of euro coins to be issued in 2006, supplemented by explanatory notes on the forecasting methodology,
HAS DECIDED AS FOLLOWS:
Article 1
Approval of the volume of euro coins to be issued in 2006
The ECB hereby approves the volume of coins to be issued by the participating Member States in 2006 as described in the following table:
(EUR million) |
| Issuance of coins intended for circulation and issuance of collector coins (not intended for circulation) in 2006 |
Belgium | 145,5 |
Germany | 500,0 |
Greece | 71,4 |
Spain | 625,0 |
France | 362,0 |
Ireland | 76,0 |
Italy | 772,4 |
Luxembourg | 45,0 |
Netherlands | 60,0 |
Austria | 177,0 |
Portugal | 150,0 |
Finland | 60,0 |
Article 2
Final provision
This Decision is addressed to the participating Member States.
Done at Frankfurt am Main, 9 December 2005.
The President of the ECB
Jean-Claude Trichet
--------------------------------------------------
