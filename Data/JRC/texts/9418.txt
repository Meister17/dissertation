Order of the President of the Court of 15 February 2006 — Commission of the European Communities
v Italian Republic
(Case C-218/05) [1]
(2006/C 154/35)
Language of the case: Italian
The President of the Court has ordered that the case be removed from the register.
[1] OJ C 182, 23.07.2005.
--------------------------------------------------
