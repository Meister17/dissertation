Commission Decision
of 26 August 2005
amending Decision 89/471/EEC authorising methods for grading pig carcases in Germany
(notified under document number C(2005) 3238)
(Only the German text is authentic)
(2005/628/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 3220/84 of 13 November 1984 determining the Community scale for grading pig carcases [1], and in particular Article 5(2) thereof,
Whereas:
(1) By Commission Decision 89/471/EEC [2], the use of several methods for grading pig carcases in Germany was authorised.
(2) The German Government has requested the Commission to authorise the application of a new apparatus as a reference system for assessing the lean meat content of carcases in the framework of the existing grading methods and has submitted the details required in Article 3 of Commission Regulation (EEC) No 2967/85 of 24 October 1985 laying down detailed rules for the application of the Community scale for grading pig carcases [3].
(3) The examination of this request has revealed that the conditions for authorising the new apparatus are fulfilled. The use of the apparatus "Ultrasound-Scanner SSD 256" as a reference system for assessing the lean meat content can thus be replaced by the use of the apparatus "Ultrasonic Scanner GE Logiq 200pro".
(4) Decision 89/471/EEC should therefore be amended accordingly.
(5) The measures provided for in this Decision are in accordance with the opinion of the Management Committee for Pigmeat,
HAS ADOPTED THIS DECISION:
Article 1
Decision 89/471/EEC is hereby amended as follows:
1. In Article 1, paragraph 1 is replaced by the following:
"1. As a method for grading of pig carcases the assessment method for lean meat based on the use of the apparatus "Ultrasonic Scanner GE Logiq 200pro", details of which are given in Part 1 of the Annex, is hereby authorised."
2. Article 4a is deleted.
3. In the Annex, Part 1 is amended as follows:
(a) the title is replaced by the following:
"PART 1
Ultrasonic Scanner GE Logiq 200pro"
;
(b) paragraph 1 is replaced by the following:
"1. The "Ultrasonic Scanner GE Logiq 200pro" is a two-dimensional ultrasonic scanner with digital image processing. The system is operated with a linear 3.5 MHz probe that allows a sonar penetration down to about 20 cm depending on the display window chosen. The scanning width of the probe is 9,4 cm, which corresponds to two or three ribs of the carcase.
The assessment method provided for in paragraph 2 is to be used as a method for the grading of pig carcases established on the basis of the values of the measurements obtained by the apparatus "Ultrasonic Scanner GE Logiq 200pro".
Every apparatus used in the abattoir must be calibrated and must give values of the measurements equivalent to those of the "Ultrasonic Scanner GE Logiq 200pro" ".
Article 2
This Decision is addressed to the Federal Republic of Germany.
Done at Brussels, 26 August 2005.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 301, 20.11.1984, p. 1. Regulation as last amended by Regulation (EC) No 3513/93 (OJ L 320, 22.12.1993, p. 5).
[2] OJ L 233, 10.8.1989, p. 30. Decision as last amended by Decision 97/546/EC (OJ L 224, 14.8.1997, p. 20).
[3] OJ L 285, 25.10.1985, p. 39. Regulation as amended by Regulation (EC) No 3127/94 (OJ L 330, 21.12.1994, p. 43).
--------------------------------------------------
