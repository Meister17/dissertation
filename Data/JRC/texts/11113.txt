[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 29.8.2006
COM(2006) 469 final
2006/0160 (ACC)
Proposal for a
COUNCIL DECISION
regarding the position to be taken by the Community within the International Tropical Timber Council on the extension of the International Agreement on Tropical Timber, 1994
(presented by the Commission)
EXPLANATORY MEMORANDUM
The International Agreement on Tropical Timber of 1994 entered into force on 1 January 1996. It is due to expire on 31 December 2006, after two periods of extension.
A successor Agreement to the International Tropical Timber Agreement of 1994 was successfully negotiated and concluded within UNCTAD in January 2006.
Under the provisions of Article 46 of the International Tropical Timber Agreement of 1994, a proposal was made at the 40th session of the International Tropical Timber Council held from 29 May to 3 June 2006 in Mérida (Mexico) to extend the Agreement until the entry into force of the successor Agreement of 2006.
Under the legal basis, the proposed decision does not affect the European Community’s contribution to the Administrative budget of the International Tropical Timber Organisation (ITTO), which is still zero.
The purpose of this proposal is to authorise the European Community to vote in favour of extending the Agreement.
The Commission therefore suggests that the Council adopts the attached decision.
2006/0160 (ACC)
Proposal for a
COUNCIL DECISION
regarding the position to be taken by the Community within the International Tropical Timber Council on the extension of the International Agreement on Tropical Timber, 1994
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 133 in conjunction with Article 300(2) thereof,
Having regard to the proposal from the Commission[1],
Whereas:
(1) The International Agreement on Tropical Timber of 1994 has been signed and provisionally applied by the Community by Council Decision 96/493/EC[2].
(2) The successor Agreement to the International Tropical Timber Agreement of 1994 was successfully concluded within UNCTAD in January 2006.
(3) The International Tropical Timber Agreement of 1994 remains in force until 31 December 2006 unless, in accordance with the provisions of Article 46(3), it is extended beyond that date by decision of the International Tropical Timber Council until such time as the successor Agreement enters into force.
(4) The extension of that Agreement is in the interest of the Community.
(5) The European Community’s position in the International Tropical Timber Council should be determined,
HAS DECIDED AS FOLLOWS:
Sole Article
The European Community’s position within the International Tropical Timber Council shall be to vote in favour of extending of the International Agreement on Tropical Timber, 1994, until the provisional or definitive entry into force of the successor Agreement of 2006.
Done at Brussels,
For the Council
The President
[1] OJ Cxxx p.xx
[2] OJ L 208, of 17.08.1996, p.1
