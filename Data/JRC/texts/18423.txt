*****
COMMISSION REGULATION (EEC) No 3439/83
of 5 December 1983
laying down special conditions for the export of certain cheeses to Australia
THE COMMISSION OF THE EUROPEAN
COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 804/68 of 27 June 1968 on the common organization of the market in milk and milk products (1), as last amended by Regulation (EEC) No 1600/83 (2), and in particular the first subparagraph of Article 17 (4) thereof,
Whereas the Commission has reached agreement with the Government of Australia that countervailing duties will not be imposed on certain cheeses of Community origin imported into Australia and that refunds on exports of these cheeses to Australia will be reduced;
Whereas provision should be made for a system of certificates to accompany the said cheeses so that the competent authorities in the country of destination can speed up customs clearance; whereas steps should be taken to ensure that a higher refund than that provided for in the case of exports to Australia is not granted;
Whereas the Management Committee for Milk and Milk Products has not delivered an opinion within the time limit set by its chairman,
HAS ADOPTED THIS REGULATION:
Article 1
At the request of an exporter, a certificate corresponding to the specimen shown in Annex II shall be issued for exports to Australia of the cheeses in Annex I.
Article 2
1. Certificates shall be issued by the competent authority, hereinafter referred to as the 'issuing agency', designated by each Member State. These agencies shall keep copies of certificates issued.
2. An issuing agency shall number each certificate. Copies shall bear the same number as the original.
Article 3
In respect of products falling within subheading 04.04 E I b) 2 of the Common Customs Tariff, box 7 on the certificate shall contain the words 'water content, by weight, in non-fatty matter exceeding 47 % but not exceeding 62 %'.
Article 4
Certificates shall be valid only for the quantity indicated thereon. However, a quantity differing by not more than 5 % from that indicated on a certificate shall be considered as covered.
Article 5
1. The original and one copy of a certificate shall be presented for endorsement to the customs office which has received the export declaration relating to the cheeses covered by the certificate. Certificates and copies must be so presented within 60 days from the date on which the certificate is issued.
2. The customs office referred to in paragraph 1 shall ensorse the original and the copy of a certificate only if they are presented within the period referred to in paragraph 1 and if the country of destination given on the export declaration and on the document used to obtain a refund is the same as that for which the certificate has been issued.
3. After endorsement, originals and copies of certificates shall be returned to the party concerned.
4. Where a check is requested a posteriori by the competent authorities in the country of destination, the issuing agency shall inform them as soon as possible of the result.
Article 6
1. Without prejudice to Article 20 of Commission Regulation (EEC) No 2730/79 (3), a copy of the certificate of which a specimen is shown in Annex II, duly endorsed in the box provided for the purpose by the customs authorities in the country of destination, shall be considered as proof within the meaning of paragraph 3 of the said Article.
2. No export refund exceeding the rate laid down for exports of cheeses to Australia may be granted where the document used to obtain the refund when the cheese is cleared by customs for export gives Australia as the country of destination.
Article 7
1. The form, of which a specimen is shown in Annex II, shall be made out in one original and at least two copies.
2. Member States shall print the forms provided for in this Regulation or have them printed. The size of the forms shall be 210 × 297 millimetres.
3. The Member States from which the cheese is exported may require that certificates used wihin their territory be drawn up in one of their official languages in addition to containing the text in English.
Article 8
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
It shall apply from 1 January 1984.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 5 December 1983.
For the Commission
Poul DALSAGER
Member of the Commission
(1) OJ No L 148, 28. 6. 1968, p. 13.
(2) OJ No L 163, 22. 6. 1983, p. 56.
(3) OJ No L 317, 12. 12. 1979, p. 1.
ANNEX I
List of cheeses for which a certificate, of which a specimen is shown in Annex II, may be issued
1.2 // // // CCT heading No // Description // // // ex 04.04 C // Blue-veined cheese, excluding Roquefort // 04.04 E I b) 1 // Cheddar // ex 04.04 E I b) 2 // Other cheeses of a water content, calculated by weight, of the non-fatty matter exceeding 47 % but not exceeding 62 %, except for: // // - Kefalotiri, Kefalograviera and Kasseri, made exclusively from sheep's and/or goats' milk // // - Asiago, Caciocavallo, Montasio, Provolone, Ragusano, Butterkaese, Esrom, Italico, Kernhem, Saint-Nectaire, Saint-Paulin, Taleggio, Ricotta, Feta // // - cheeses of a fat content by weight in dry matter of less than 19 % and a dry matter content of 32 % or more by weight // // - cheeses of a fat content by weight in dry matter of 19 % or more and less than 39 % and a water content, calculated by weight, in the non-fatty matter of 62 % or less // //
