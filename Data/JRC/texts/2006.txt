Action brought on 1 August 2005 — Balabanis and Le Dour v Commission
Parties
Applicant(s): Panagiotis Balabanis (Brussels, Belgium), Olivier Le Dour (Brussels, Belgium) (represented by: X. Martin, S. Orlandi, A. Coolen, J.-N Louis, E. Marchal, lawyers)
Defendant(s): Commission of the European Communities
Form of order sought
The applicant(s) claim(s) that the Court should:
- annul the Commission's decision not to take account of the applicants' merits for the 2004 promotion exercise and the decisions not to promote them to a higher grade in their category;
- order the defendant to pay the costs.
Pleas in law and main arguments
In this case the applicants, who were appointed probationary officials on 16 March 2002 and who, in accordance with the new wording of Article 45 of the Staff Regulations, completed the minimum of two years in their grade as from 16 March 2004, thus contest the Appointing Authority's refusal to consider them as eligible for the 2004 promotion exercise and to promote them to a higher grade in their category for the same exercise.
In support of their claims the applicants assert that Article 45 of the Staff Regulations has been infringed. In accordance with that new provision the probationary period must be taken into account for the purpose of calculating the minimum seniority in grade.
--------------------------------------------------
