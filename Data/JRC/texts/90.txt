*****
COMMISSION REGULATION (EEC) No 1584/89
of 7 June 1989
concerning the classification of certain goods in the combined nomenclature
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 2658/87 (1) on the tariff and statistical nomenclature and on the Common Customs Tariff, as last amended by Regulation (EEC) No 1495/89 (2), and in particular Article 9 thereof,
Whereas in order to ensure uniform application of the combined nomenclature annexed to Regulation (EEC) No 2658/87, it is necessary to adopt measures concerning the classification of the goods referred to in the Annex to this Regulation;
Whereas Regulation (EEC) No 2658/87 has set down the general rules for the interpretation of the combined nomenclature and these rules also apply to any other nomenclature which is wholly or partly based on it or which adds any additional subdivisions to it and which is established by specific Community provisions, with a view to the application of tariff or other measures relating to trade in goods;
Whereas, pursuant to the said general rules, the goods described in column 1 of the table annexed to this Regulation must be classified under the appropriate CN codes indicated in column 2, by virtue of the reasons set out in column 3;
Whereas the Nomenclature Committee has not delivered an opinion within the time limit set by its chairman,
HAS ADOPTED THIS REGULATION:
Article 1
The goods described in column 1 of the annexed table are now classified within the combined nomenclature under the appropriate CN codes indicated in column 2 of the said table.
Article 2
This Regulation shall enter into force on the 21st day after its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 7 June 1989.
For the Commission
Christiane SCRIVENER
Member of the Commission
(1) OJ No L 256, 7. 9. 1987, p. 1.
(2) OJ No L 148, 1. 6. 1989, p. 1.
ANNEX
1.2.3 // // // // Description of the goods // CN code classification // Reasons // // // // (1) // (2) // (3) // // // // 1. Set of garments for women or girls put up for retail sale, consisting of: - a lightweight knitted garment (55 % man-made fibres and 45 % cotton) designed to be worn next to the body, which falls well short of the waist, covers the chest and the upper part of the back, has no sleeves and has a low-cut neckline at the front. This garment has an elastic band (2,5 cm in width) at the base. (See photo 431 A in the Appendix (*)) // Upper garment: 6114 30 00 // Classification is determined by the provisions of general rules 1 and 6 for the interpretation of the combined nomenclature, by note 13 to Section XI, note 3 (b) to Chapter 61, the additional note to Chapter 61, as well as by the texts of CN codes 6108 and 6108 22 00, 6110 and 6110 30 99 and 6114 and 6114 30 00 The upper garment cannot be regarded as a body-supporting garment of CN code 6212 as it is not designed to perform this function // - briefs of the same fabric, composition and colour as the upper garment. (See photo 431 B in the Appendix (*)) // Briefs: 6108 22 00 // // 2. Lightweight, knitted, sleeveless garment (100 % cotton) for women or girls and designed to be worn next to the body, falling well short of the waist, covering the chest and with a low-cut neckline at the front and back. This garment has two darts at the front of approximately 5 cm extending from the base which is slightly gathered and has an elastic band (approximately 1,5 cm wide). (See photo 432 in the Appendix (*)) // 6114 20 00 // The classification is determined by the provisions of general rules 1 and 6 for the interpretation of the combined nomenclature, by the additional note to Chapter 61 as well as by the texts of CN codes 6110 and 6110 20 99, 6114 and 6114 20 00 The characteristics of this garment are not sufficient to classify it under CN code 6212 given that it does not have a supporting function // // //
(*) The photographs are merely of an illustrative nature.
Appendix
