Publication of decisions by Member States to grant or revoke operating licenses pursuant to Article 13(4) of Council Regulation (EEC) No 2407/92 on licensing of air carriers [1] [2]
(2006/C 135/05)
(Text with EEA relevance)
FINLAND
Operating licences revoked
Category B: Operating licences including the restriction of Article 5(7)(a) of Regulation (EEC) No 2407/92
Name of air carrier | Address of air carrier | Permitted to carry | Decision effective since |
Utin Lento Oy | Utin lentoasema Lentoportintie 83 FIN-45410 Utti | passengers, mail, cargo | 27.4.2006 |
[1] OJ L 240, 24.8.1992, p. 1.
[2] Communicated to the European Commission before 31.8.2005.
--------------------------------------------------
