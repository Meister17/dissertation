[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 13.7.2006
COM(2006) 380 final
COMMUNICATION FROM THE COMMISSION TO THE COUNCIL, THE EUROPEAN PARLIAMENT, THE EUROPEAN ECONOMIC AND SOCIAL COMMITTEE AND THE COMMITTEE OF THE REGIONS
Mid-Term Re view of the Programme for the Promotion of Short Sea Shipping (COM(2003) 155 final) {SEC(2006) 922}{SEC(2006) 923}
1. BACKGROUND
As pointed out by the mid-term review of the 2001 White Paper[1], Short Sea Shipping plays a key role in ensuring sustainable mobility and also contributes to meeting other objectives, like alleviating congestion and environmental pressure. Short Sea Shipping is an integral part of the logistics chain in Europe’s transport system as advocated by the recent Commission Communication on freight logistics[2].
The Commission’s Communication on Short Sea Shipping in 1999[3] analysed obstacles that hinder the development of the mode and advocated a door-to-door approach with one-stop shops.
In June 2002, the European Union Transport Ministers held an informal meeting in Gijón (Spain) dedicated to Short Sea Shipping. Following this meeting, the Commission prepared a Programme for the Promotion of Short Sea Shipping[4].
In 2004, the Commission presented a further Communication on Short Sea Shipping[5] reporting on progress since 1999. This was followed by an informal meeting of the European Union Transport Ministers in July 2004 in Amsterdam.
In January 2006, a Ministerial conference in Ljubljana (Slovenia) prepared conclusions on Motorways of the Sea.
Also the European Parliament has, on several occasions, confirmed its dedication to promoting Short Sea Shipping. Its latest Resolution[6] on the subject dates from April 2005.
The European Short Sea Shipping policy is fully in line with the Lisbon agenda.
2. SHORT SEA SHIPPING IS GROWING
Short Sea Shipping has maintained its position as the only mode of transport able to challenge the fast growth of road transport. Between 1995 and 2004, the tonne-kilometre performance of Short Sea Shipping in the EU-25 grew by 32 %, while road performance grew by 35 %. Short Sea Shipping performs 39 % of all tonne-kilometres in the EU-25 while the share of road is 44 %. The corresponding shares for the EU-15 are 42 % for Short Sea Shipping and 44 % for road.
Available data show that the fastest growing segment of Short Sea Shipping has continued to be containerised cargo with an average yearly growth of 8,8 % since 2000.
[pic]
Figure: Tonne-kilometre growth 1995-2004 in per cent for road, Short Sea Shipping (SSS), inland waterway transport (IWT) and rail (index 1995 = 100)[7].
3. OVERCOMING OBSTACLES TO THE DEVELOPMENT OF SHORT SEA SHIPPING
A number of obstacles still hinder Short Sea Shipping from developing faster:
- It has not yet reached full integration in the multimodal door-to-door supply chain;
- It involves complex administrative procedures;
- It requires higher port efficiency and good hinterland accessibility.
The 2003 Programme for the Promotion of Short Sea Shipping set out 14 actions with the objective to improve the efficiency of the mode and overcome obstacles to its development. These were divided into legislative, technical and operational actions. This mid-term review evaluates the progress achieved on these actions to date and advocates a way forward.[8]
3.1. Legislative Actions
3.1.1. Directive [9] on certain reporting formalities for ships (IMO-FAL[10])
Transposition of the Directive into national legislation is almost complete.
The Commission has made information on the FAL forms publicly available, and has actively advocated to non-EU countries the example that the EU has now set in accepting internationally agreed IMO FAL forms in their standards format and layout.
The 32nd session of the IMO FAL Committee in July 2005 adopted some technical amendments to the Convention.
Action No. 1 in the Promotion Programme is almost complete. Follow-up and monitoring will continue. A new benchmark needs to be set for bringing the annexes to the Directive in line with the IMO measures adopted at the 32nd session of the FAL Committee – deadline: 2007 (actors: the Commission and Member States). |
3.1.2. Marco Polo
The subvention programme Marco Polo[11] became operational in 2003. In the first selection rounds, approximately half of the accepted projects involved Short Sea Shipping (mainly starting up new lines or expanding existing ones).
In July 2004, the Commission made a proposal to extend Marco Polo[12]. This proposal, Marco Polo II would, from 2007 onwards, continue the actions eligible under the current Marco Polo programme but have a considerably larger budget.
The new programme identifies Motorways of the Sea (see chapter 3.1.4) as a specific new action. This action should decrease road traffic over time on a given corridor by shifting goods from road to Short Sea Shipping operating on Motorways of the Sea.
Action No. 2 in the Promotion Programme is mid-way to completion. Work will continue to ensure the utilisation of Marco Polo to the benefit of Short Sea Shipping until 2010 and beyond. |
3.1.3. Intermodal loading units
The final adoption of the Commission proposal for a Directive on Intermodal Loading Units[13] is still pending.
Action No. 3 in the Promotion Programme has not advanced. |
3.1.4. Motorways of the Sea
Motorways of the Sea are an important instrument for promoting Short Sea Shipping.
The revision of the Community Guidelines on the development of the trans-European transport network (TEN-T)[14] in April 2004 contains a priority project with four Motorways of the Sea areas. It also draws a parallel between Motorways of the Sea and Marco Polo by advancing certain criteria, such as avoidance of distortions of competition and the continued viability of the project after the period of Community funding.
In February 2005, the Commission developed a Vademecum[15] facilitating the practical application of the legal framework.
Three actions in support of regional strategic master plans for Motorways of the Sea were accepted for TEN-T financing under the 2005 call for proposals - one is for the Baltic Sea, one for the European west coast, and one for the Mediterranean.
Implementing Motorways of the Sea requires partnership and co-operation . This is vital to accomplish the concentration of freight flows that is imperative for Motorways of the Sea to become viable. Motorways of the Sea are proposed by at least two Member States, they should involve both the public and private sectors, and should include short-sea links.
Motorways of the Sea are also about quality . As well as requiring quality infrastructure and superstructure in ports and hinterlands connections, this quality approach should also extend to administrative procedures. The approach could also include tracking and tracing, single windows, and adequate procedures in ports to handle the required high short-sea frequency.
In the light of the conclusions of the ministerial conference in January 2006 and the new TEN-T financial perspective, it is now essential that the Member States give the necessary priority to Motorways of the Sea projects.
Action No. 4 in the Promotion Programme, defining the concept of Motorways of the Sea, has been fully completed. Therefore, a new benchmark needs to be set for making the first Motorways of the Sea operational - deadline: 2010 (actors: the Commission, Member States and industry). The Commission will also examine whether the concept of Motorways of the Sea can be extended beyond the TEN-T to a quality label for logistics excellence in Short Sea Shipping – deadline: 2007. |
3.1.5. The environmental performance of Short Sea Shipping
Maritime transport has higher energy-efficiency than other modes of transport and is, in general, less harmful to the environment. Increased use of Short Sea Shipping would be in line with the Union’s environmental policies and CO2 targets.
Nevertheless, environmental improvements are needed in shipping, in particular in the areas of SOx, NOx and particulates. These are urgent for maintaining the “clean” image of shipping in a situation where road transport is becoming cleaner. However, as pointed out by the Thematic Network for Short Sea Shipping (REALISE)[16], when taking into account a broader scope of external costs, such as noise, accidents and congestion, Short Sea Shipping can still maintain its lead to road transport in relation to external socio-environmental effects.
In July 2005 the European Parliament and the Council took a significant step towards cleaner shipping. They adopted a Directive[17] that will enforce the maximum sulphur limit of 1,5 % for marine fuels used in the Baltic Sea (from August 2006), the North Sea and the English Channel (foreseen for autumn 2007)[18]. The same limit will apply to all passenger vessels on regular services between EU ports from August 2006.
Action No. 5 in the Promotion Programme is mid-way to completion. Work will continue to back up the Commission’s wider environmental strategy towards even “greener” shipping, including actions in the field of research and technological development. |
3.2. Technical Actions
3.2.1. Guide to Customs Procedures for Short Sea Shipping
As indicated in the 2003 Promotion Programme, the Commission carried out European-wide consultations on the Guide to Customs Procedures for Short Sea Shipping[19]. Responses to the consultations seemed to focus on divergent national, regional and local applications of EU Customs rules, and on simplified Customs procedures. The first issue of EU Customs rules is addressed in chapter 3.2.3 hereafter. In response to the latter issue of simplified procedures, the Commission presented, in 2004, a Working Document[20] on “Authorised Regular Shipping Service” which is a service authorised by the Customs to carry Community goods between two Member States with the minimum of formalities.
Furthermore, the Commission has periodically updated the original Guide (latest update in January 2004). Further updates will be considered when the current Commission proposals on modernising the Customs Code have been adopted (see chapter 3.2.3).
Action No. 6 in the Promotion Programme has been completed for the time being. Work to identify and address specific issues will continue under Actions 7 and 8 (see chapters 3.2.2 and 3.2.3). |
3.2.2. Identification and elimination of obstacles to making Short Sea Shipping more successful than it is today
Finding solutions to identified bottlenecks in door-to-door Short Sea Shipping has maintained a strong tail wind and keeps on producing tangible results. Continuous co-operation with the Member States and industry has been essential to the exercise. The initial list of 161 bottlenecks established in 2000 has now been reduced to 35[21]. Work will continue to tackle the remaining bottlenecks. The exercise was re-launched in April 2005 in order to have an update of the situation and encompass the enlarged EU.
A number of solved bottlenecks were presented in the 2004 Communication on Short Sea Shipping. Further to those, the following bottlenecks, for example, have found a solution:
Electronic format of the Customs T2 (or equivalent) document is now accepted by the Port of Goole (UK). It is no longer necessary to make the physical journey of some 70 miles to the Customs office and back to present a hard copy. |
In Spanish ports Customs officials no longer require hard copies of the “declaración sumaria’ (equivalent of cargo manifest) but accept it in electronic format. |
In Greece, cargo manifests are no longer required to be translated into Greek, except when they are being used as a summary declaration. |
Furthermore, the Commission feels that it is now more vital than ever to focus efforts on the port sector so as to enhance the role of ports as nodal points in the logistics chain.
Action No. 7 in the Promotion Programme is more than half-way to completion. Work will continue to solve identified obstacles and find out whether further obstacles exist also encompassing the new Member States, including obstacles in the port sector. |
3.2.3. Approximation of national applications and computerisation of Community Customs procedures
As a first step towards e-Customs, the New Computerised Transit System (NCTS) has been operational since mid-2003. In this system electronic messages replace the earlier paper procedure relating to the transport of goods under the single administrative document (SAD).
In November 2005, the Commission prepared a new package of measures[22] intended to simplify and streamline Customs procedures and to put in place a system that would create a paperless environment for Customs and trade, including security aspects. This package consists of proposals for a modernized Customs Code and e-Customs. Electronic declarations and messages would become the rule and paper-based declarations an exception.
Future developments, such as long-range identification and tracking (LRIT) systems using satellite communications could constitute a further step towards an environment where both the ship’s journey and goods could be reliably and securely tracked all the way along, thereby decreasing the need for individual controls.
Furthermore, two contact groups[23] of Customs offices work towards increasing practical co-operation and co-ordination between the Customs offices of major EU ports. These Groups address differing national, regional or local applications of Community Customs rules, set standards, and aim to achieve an equivalent application of controls.
Action No. 8 in the Promotion Programme is more than half-way to completion. Work will continue in the Customs contact groups to harmonise national, regional and local applications. |
3.2.4. Research and Technological Development
The Thematic Network of Short Sea Shipping, REALISE[24] finalised its work at the end of 2005. It focused on three integrated studies relating to statistics, environmental issues, and the price of Short Sea Shipping (see chapters 3.1.5, 3.3.4 and 3.3.5).
Furthermore, other research actions are taking place with relevance to Short Sea Shipping. These relate, inter alia , to lowering ship emissions, new ships types, engines, Motorways of the Sea, and port handling equipment.
Action No. 9 in the Promotion Programme is more than half-way to completion. Work will continue to follow up the results under the 5th Framework Programme, secure good results under the 6th and carry out effective dissemination. Short Sea Shipping, as part of the waterborne platform, also needs to be a priority under the 7th Framework Programme. |
3.3. Operational Actions
3.3.1. One-stop administrative shops
The package of Customs measures mentioned above in chapter 3.2.3 contains a framework for the information provided by economic operators to be shared between Customs authorities and other agencies operating at the border, such as border guards, veterinary and environmental authorities. The economic operators would need to give the information only once ('single window') and the goods would be controlled by those authorities at the same time and at the same place ('one stop administrative shop').
Action No. 10 in the Promotion Programme is more than half-way to completion. Work will continue to promote one-stop administrative shops (‘single windows’). |
3.3.2. Short Sea Shipping Focal Points
Short Sea Shipping Focal Points are representatives of national maritime administrations and responsible for Short Sea Shipping in their administrations. The Commission has continued to highlight their vital role in short-sea policy and has organised regular meetings with them to exchange information, discuss Motorways of the Sea, and solve obstacles to the development of Short Sea Shipping (see chapter 3.2.2). Most of the new Member States and acceding and candidate countries have been rapidly incorporated in the work of the Focal Points.
Action No. 11 in the Promotion Programme is more than half-way to completion. Work will continue with regular meetings of the Focal Points and to secure the flow of information and achievement of results, including Motorways of the Sea. |
3.3.3. Shortsea Promotion Centres
There are currently 21 Shortsea Promotion Centres (SPCs)[25] operating in Europe. These Centres are driven by business interests and offer neutral, impartial advice on the use of Short Sea Shipping to meet the needs of transport users. They are essentially independent from specific interest groups and work in line with the European promotion policy. The national Centres are networked in the European Shortsea Network (ESN)[26] which offers a common, virtual tool for European promotion. The Commission strongly supports these Centres, their work and their networking and expects this support to be matched at national level.
Recently one task that has become more prominent in the operation of these SPCs is the identification and solution of bottlenecks in Short Sea Shipping (see chapter 3.2.2).
The Commission is currently examining ways to promote a wider concept of multimodality based on the existing structures. One feasible option on the table is to extend the scope of short-sea promotion towards multimodality in inland logistics chains.
Action No. 12 in the Promotion Programme is more than half-way to completion. Work will continue to ensure good functioning of and guidance to the SPCs. Extension of the geographical scope of the SPCs will continue. Efforts to secure at least medium-term financial security for the SPCs will also continue. A new benchmark needs to be set to examine the feasibility of extending the scope of SPCs to encompass the promotion of inland multimodality and related logistics within 3 years (actors: SPCs, industry, the Member States and Commission). |
3.3.4. The image of Short Sea Shipping
Based on information received from the maritime industries and through the European Shortsea Network, efforts to improve the overall image of Short Sea Shipping have been successful. Accordingly, the general image of the mode seems to have reached that of a modern and efficient means of transport in co-modal chains.
However, full integration of Short Sea Shipping in logistics chains still remains to be improved. Consequently, efforts on promoting the image of the mode should now focus on this targeted segment that can also help Motorways of the Sea. Shippers, cargo owners, forwarders and hauliers should continue to be important targets for promotion and so should attracting young people to the profession.
The Commission feels that exchanging best practice (success stories) is an essential way of promoting Short Sea Shipping. For five years now the Commission has been collecting short-sea success stories, cross-checking them with the European Shortsea Network, and making them regularly available on the Internet[27].
The Thematic Network for Short Sea Shipping (REALISE) examined whether door-to-door transport chains involving Short Sea Shipping are normally more or less expensive than other door-to-door chains (such as unimodal road or road/rail). Based on evidence collected under the study, an absolute conclusion could not be reached. Instead, it could be noted that “Short Sea Shipping within multi-modal chains is cheaper – on many European corridors and segments – than unimodal transport solutions”[28]. Nevertheless, the study pointed out that price competitivity alone might not be a sufficient condition to encourage a substantial shift from road to Short Sea Shipping. Other conditions include the port and port hinterland functions and services (including timing and reliability) that affect the modal choice.
A further study on comparative benchmarking of performance across modes[29] came up with a first set of comparative indicators (e.g. transport costs, external costs, total time, and flexibility). The results indicate that it would be up to the transport user to make the choice between lower costs or higher speed. Short Sea Shipping over a longer distance will normally win on costs but lose to the truck on speed and flexibility. A further decisive factor for modal choice, reliability, can be met by both Short Sea Shipping and road.
Action No. 13 in the Promotion Programme is more than half-way to completion. Efforts will now concentrate on improving the integration of Short Sea Shipping in the multimodal logistics supply chain, including the related aspects of image. |
3.3.5. Statistical information
The Council Directive on Maritime Statistics[30] that came into full effect in 2000 will gradually become the main source of short-sea data when it will be able to provide sufficient data series, in tonne-kilometres, to analyse trends.
The Commission has been working on a matrix that would allow coherent comparisons between modes by converting the tonne-based short-sea data into tonne-kilometres used in other modes. A first tool is already being tested in Eurostat. The Thematic Network for Short Sea Shipping (REALISE) contributed to refining the data results.
Action No. 14 in the Promotion Programme is almost complete. The Commission will continue to work towards a single, reliable statistical source for tonne-kilometre data on Short Sea Shipping. The currently available conversion matrix will need to be further refined. |
4. IN CONCLUSION
The Programme for the Promotion of Short Sea Shipping has shown its merits, strengthened the position of the mode in co-modality, and is more than half-way to completion . The Actions in the Programme seem to have addressed the main problem areas facing the mode.
Three action sheets, as they were presented in the 2003 Promotion Programme, have been almost or fully completed: IMO FAL, Motorways of the Sea, and Short-sea Customs Guide. New targets with new deadlines have been set for the first two. The third one has been merged with other ongoing actions.
In certain cases, there is a need to target the action more precisely than earlier (integrating Short Sea Shipping tighter in the logistics supply chain) or add a new target (extending the scope of Short Sea Promotion Centres to cover inland supply chains).
Separate efforts will also need to continue in the ports sector to make Short Sea Shipping even more efficient and competitive in the logistics chain than it is today.
At this point in time the 14 actions introduced in the Promotion Programme seem to have been the right ones and work on all of them, whether ongoing, new or re-targeted, should continue with amplified efforts in co-operation with the Member States, industry and European Shortsea Network.
[1] Keep Europe Moving – sustainable mobility for our continent, COM(2006) 314 final.
[2] Freight Logistics in Europe – key to sustainable mobility, COM(2006) 336 final.
[3] The Development of Short Sea Shipping in Europe: A Dynamic Alternative in a Sustainable Transport Chain - Second Two-yearly Progress Report, COM(1999) 317 final.
[4] Programme for the Promotion of Short Sea Shipping, COM(2003) 155 final.
[5] Communication on Short Sea Shipping, COM(2004) 453 final.
[6] 2004/2161(INI).
[7] Source: Eurostat, DG for Energy and Transport, and 75 member ports of the European Sea Ports Organisation (ESPO). A list of these ports is available in chapter 1.2.4.14 of the annexed Staff Working Document. The Commission is grateful to these ports and to ESPO for co-ordinating the exercise.
The dotted part of the line for road reflects the significant changes that have taken place in the collection methodology for road data in certain Member States for 2003/2004.
[8] The Commission thanks the Short Sea Shipping Focal Points, maritime industries and Shortsea Promotion Centres for their contributions to this paper.
More detailed explanation of the degree to which the actions have been completed can be found in appendix 2 of the annexed Working Document.
[9] Directive 2002/6/EC of the European Parliament and of the Council of 18 February 2002 on reporting formalities for ships arriving in and/or departing from ports of the Member States of the Community, OJ L 67, 9.3.2002, p. 31.
[10] International Maritime Organisation’s Facilitation Forms.
[11] Regulation (EC) No 1382/2003 of the European Parliament and of the Council of 22 July 2003 on the granting of Community financial assistance to improve the environmental performance of the freight transport system (Marco Polo Programme), OJ L 196, 2.8.2003, p. 1.
[12] Proposal for a Regulation of the European Parliament and of the Council establishing the second “Marco Polo” programme for the granting of Community financial assistance to improve the environmental performance of the freight transport system (“Marco Polo II”), COM(2004) 478 final.
[13] Proposal for a Directive of the European Parliament and of the Council on Intermodal Loading Units, COM(2003) 155 final, as amended by COM(2004) 361 final.
[14] Decision No 884/2004/EC of the European Parliament and of the Council of 29 April 2004 amending Decision No 1692/96/EC on Community guidelines for the development of the trans-European transport network, OJ L 167, 30.4.2004, p. 1.
[15] See www.ec.europa.eu/comm/transport/intermodality/motorways_sea/projects_en.htm .
[16] Deliverable 3.3: Environmental Transport Options.
[17] Directive 2005/33/EC of the European Parliament and of the Council of 6 July 2005 amending Directive 1999/32/EC as regards the sulphur content of marine fuels, OJ L 191, 22.7.2005, p. 59.
[18] Cf. “SOx Emission Control Areas” established under Annex VI to MARPOL 73/78.
[19] Guide to Customs Procedures for Short Sea Shipping, SEC(2002) 632.
[20] Simplified Customs Procedures in Short Sea Shipping: ‘Authorised Regular Shipping Service’, SEC(2004) 333.
[21] See: www.ec.europa.eu/comm/transport/maritime/sss/bottlenecks/index_en.htm .
[22] Proposal for a Regulation of the European Parliament and of the Council laying down the Community Customs Code (Modernized Customs Code), COM(2005) 608 final, and Proposal for a Decision of the European Parliament and of the Council on a paperless environment for customs and trade, COM(2005) 609 final.
[23] RALFH dealing with major northern EU ports and ODYSSUD dealing with major southern EU ports.
[24] See www.realise-sss.org .
[25] Belgium, Bulgaria, Croatia, Cyprus, Denmark, Finland, France, Germany, Greece, Ireland, Italy, Lithuania, Malta, the Netherlands, Norway, Poland, Portugal, Spain, Sweden, Turkey and the UK.
[26] See www.shortsea.info .
[27] See www.ec.europa.eu/comm/transport/maritime/sss/policy_succes_en.htm .
[28] Deliverable 4.3: Multi-modal Meta Model Report.
[29] Comparative Benchmarking of Performance for Freight Transport across the Modes from the Perspective of Transport Users, ISL, Bremen, January/March 2006.
[30] Council Directive 95/64/EC of 8 December 1995 on statistical returns in respect of carriage of goods and passengers by sea, OJ L 320, 30.12.1995, p. 25, as implemented.
