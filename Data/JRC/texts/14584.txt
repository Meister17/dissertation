Commission Decision
of 22 June 2006
amending Decision 2004/452/EC concerning the list of bodies whose researchers may access confidential data for scientific purposes
(notified under document number C(2006) 2411)
(Text with EEA relevance)
(2006/429/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 322/97 of 17 February 1997 on Community Statistics [1], and in particular Article 20(1) thereof,
Whereas:
(1) Commission Regulation (EC) No 831/2002 of 17 May 2002 implementing Council Regulation (EC) No 322/97 on Community statistics, concerning access to confidential data for scientific purposes [2] aims at establishing, for the purpose of enabling statistical conclusions to be drawn for scientific purposes, the conditions under which access to confidential data transmitted to the Community authority may be granted and the rules of cooperation between the Community and national authorities in order to facilitate such access.
(2) Commission Decision 2004/452/EC of 29 April 2004 laying down a list of bodies whose researchers may access confidential data for scientific purposes [3] has laid down a list of bodies whose researchers may access confidential data for scientific purposes.
(3) The Department of Political Science of the Baruch College of the City University of New York (New York State, United States of America), the German Central Bank, the Employment Analysis Unit of the Directorate-General of Employment, Social Affairs and Equal Opportunities of the European Commission, the World Bank and the University of Tel Aviv (Israel) have to be regarded as a bodies fulfilling the required conditions and therefore have to be added to the list of agencies, organisations and institutions referred to in Article 3(1)(c) of Regulation (EC) No 831/2002.
(4) The measures provided for in this Decision are in accordance with the opinion of the Committee on Statistical Confidentiality,
HAS ADOPTED THIS DECISION:
Article 1
The Annex to Decision 2004/452/EC is replaced by the text in annex to this Decision.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 22 June 2006.
For the Commission
Joaquín Almunia
Member of the Commission
[1] OJ L 52, 22.2.1997, p. 1. Regulation as amended by Regulation (EC) No 1882/2003 of the European Parliament and of the Council (OJ L 284, 31.10.2003, p. 1).
[2] OJ L 133, 18.5.2002, p. 7.
[3] OJ L 156, 30.4.2004, p. 1, as corrected by OJ L 202, 7.6.2004, p. 1. Decision as last amended by Decision 2005/746/EC (OJ L 280, 25.10.2005, p. 16).
--------------------------------------------------
ANNEX
"ANNEX
Bodies whose researchers may access confidential data for scientific purposes
European Central Bank
Spanish Central Bank
Italian Central Bank
University of Cornell (New York State, United States of America)
Department of Political Science, Baruch College, City University of New York (New York State, United States of America)
German Central Bank
Unit Employment Analysis, Directorate-General of Employment, Social Affairs and Equality Opportunities of the European Commission
University of Tel Aviv (Israel)
World Bank"
--------------------------------------------------
