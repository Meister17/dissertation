COMMISSION DECISION of 25 July 1995 amending Council Decision 79/542/EEC and Commission Decisions 92/260/EEC, 93/195/EEC and 93/197/EEC with regard to the animal health conditions for the temporary admission, re-entry and imports into the Community of registered horses from Syria (Text with EEA relevance) (95/323/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 90/426/EEC of 26 June 1990 on animal health conditions governing the movement and imports from third countries of equidae (1), as last amended by the Act of Accession of Austria, Finland and Sweden, and in particular Articles 12, 13, 15, 16 and 19 (ii) thereof,
Whereas by Council Decision 79/542/EEC (2), as last amended by the Act of Accession of Austria, Finland and Sweden, a list of third countries from which Member States authorize imports of bovine animals, swine, equidae, sheep and goats, fresh meat and meat products has been established;
Whereas the health conditions and veterinary certification for the temporary admission and imports of registered horses are laid down respectively in Commission Decisions 92/260/EEC (3) and 93/197/EEC (4), both as last amended by the Act of Accession of Austria, Finland and Sweden, and for the re-entry of registered horses after temporary export in Commission Decision 93/195/EEC (5), as last amended by Decision 95/99/EC (6);
Whereas following a Commission veterinary inspection mission to Syria the animal health situation appears to be under the satisfactory control of well structured and organized veterinary services;
Whereas Syria has been free from African horse sickness for more than two years and systematic vaccination against this disease has not been carried out during the last twelve months, from glanders and dourine for more than six months and Venezuelan equine encephalomyelitis and vesicular stomatitis have never occurred;
Whereas the veterinary authorities of Syria have guaranteed to notify within 24 hours by telefax, telegram or telex to the Commission and the Member States the confirmation of any infectious or contagious disease in equidae mentioned in Annex A of Council Directive 90/426/EEC and any change in the vaccination or import policy in respect of equidae;
Whereas the animal health conditions and veterinary certification must be adopted according to the animal health situation of the third country concerned; whereas the present case relates only to registered horses;
Whereas Council Decision 79/542/EEC and Commission Decisions 92/260/EEC, 93/195/EEC and 93/197/EEC must be amended accordingly;
Whereas the measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
In Part 2 of the Annex to Council Decision 79/542/EEC, special column for equidae, the following line is inserted in accordance with the alphabetical order of the ISO code:
>TABLE>
Article 2
Commission Decision 92/260/EEC is amended as follows:
1. 'Syria` is added in alphabetical order to the list of third countries in Group E of Annex I.
2. 'Syria` is added in alphabetical order to the list of third countries in the title of the health certificate set out in Annex II (E).
Article 3
Commission Decision 93/195/EEC is amended as follows:
1. 'Syria (2)` is added in alphabetical order to the list of third countries in Group E of Annex I.
2. 'Syria` is added in alphabetical order to the list of third countries under 'Group E` in the title of the health certificate set out in Annex II.
Article 4
Commission Decision 93/197/EEC is amended as follows:
1. 'Syria (2)` is added in alphabetical order to the list of third countries in Group E of Annex I.
2. 'Syria` is added in alphabetical order to the list of third countries in the first half sentence of the title relating to registered horses of the health certificate set out in Annex II E.
Article 5
This Decision is addressed to the Member States.
Done at Brussels, 25 July 1995.
For the Commission Franz FISCHLER Member of the Commission
