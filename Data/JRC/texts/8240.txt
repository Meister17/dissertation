COMMISSION DIRECTIVE 1999/61/EC
of 18 June 1999
amending the Annexes to Council Directives 79/373/EEC and 96/25/EC
(Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 79/373/EEC of 2 April 1979 on the marketing of compound feedingstuffs(1), as last amended by Directive 98/87/EC(2), and in particular Article 10(e) thereof,
Having regard to Council Directive 96/25/EC of 29 April 1996 on the circulation of feed materials, amending Directives 70/524/EEC, 74/63/EEC, 82/471/EEC and 93/74/EEC and repealing Directive 77/101/EEC(3), as amended by Commission Directive 98/67/EC(4), and in particular Article 11(b) thereof,
(1) Whereas Commission Decision 94/381/EC of 27 June 1994 concerning certain protection measures with regard to bovine spongiform encephalopathy and the feeding of mammalian derived protein(5), as last amended by Decision 1999/129/EC(6), bans the feeding to ruminants of protein obtained from mammalian tissue while exempting some products as they are considered not to present a health risk;
(2) Whereas Decision 1999/129/EC extends the list of exempted products to the "hydrolysed proteins with a molecular weight below 10000 daltons derived from animal hides and skins", produced under certain conditions;
(3) Whereas, for practical reasons and for the sake of legal consistency, Commission Decision 1999/420/EC(7) amends accordingly Commission Decision 91/516/EEC(8) establishing a list of ingredients whose use is prohibited in compound feedingstuffs;
(4) Whereas Directives 96/25/EC and 79/373/EEC lay down respectively general and specific rules for the labelling of feed materials and compound feedingstuffs; whereas, to prevent the users of feedingstuffs containing protein derived from certain tissue of mammals from feeding them to ruminants through ignorance of current feedingstuffs and veterinary rules, these Directives foresee an appropriate labelling of such feedingstuffs calling the attention to the prohibition on their use in ruminant feed and including also the list of exempted products; whereas this list should therefore be amended accordingly;
(5) Whereas the measures provided for in this Directive are in accordance with the opinion of the Standing Committee on Feedingstuffs,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Point 7.1, second paragraph, third indent in Part A of the Annex to Directive 79/373/EEC is replaced by the following text: "- hydrolysed proteins with a molecular weight below 10000 daltons which have been:
(i) derived from hides and skins obtained from animals which have been slaughtered in a slaughterhouse and have undergone an ante mortem inspection by an official veterinarian in accordance with Chapter VI of Annex I of Directive 64/433/EEC and passed fit, as a result of such inspection, for slaughter for the purpose of that Directive;
and
(ii) produced by a production process which involves appropriate measures to minimise contamination of hides and skins, preparation of the hides and skins by brining, liming and intensive washing followed by exposure of the material to a pH of &gt; 11 for &gt; 3 hours at temperature &gt; 80 °C and followed by heat treatment at &gt; 140 °C for 30 minutes at &gt; 3,6 bar or a by an equivalent production process approved by the Commission after consultation of the appropriate Scientific Committee;
and
(iii) come from establishments which carry out an own checks program (HACCP);"
Article 2
Point 1, second paragraph, third indent in Part A chapter VIII of the Annex to Directive 96/25/EC is replaced by the following text: "- hydrolysed proteins with a molecular weight below 10000 daltons which have been:
(i) derived from hides and skins obtained from animals which have been slaughtered in a slaughterhouse and have undergone an ante mortem inspection by an official veterinarian in accordance with Chapter VI of Annex I of Directive 64/433/EEC and passed fit, as a result of such inspection, for slaughter for the purpose of that Directive;
and
(ii) produced by a production process which involves appropriate measures to minimise contamination of hides and skins, preparation of the hides and skins by brining, liming and intensive washing followed by exposure of the material to a pH of &gt; 11 for &gt; 3 hours at temperature &gt; 80 °C and followed by heat treatment at &gt; 140 °C for 30 minutes at &gt; 3,6 bar or a by an equivalent production process approved by the Commission after consultation of the appropriate Scientific Committee;
and
(iii) come from establishments which carry out an own checks program (HACCP);"
Article 3
1. Member States shall bring into force not later than 31 October 1999 the laws, regulations and administrative provisions necessary to comply with the provisions of this Directive. They shall immediately inform the Commission thereof.
When Member States adopt these provisions, they shall contain a reference to this Directive or shall be accompanied by such reference on the occasion of their official publication. The methods of making such reference shall be laid down by the Member States.
2. Member States shall communicate to the Commission the text of the main provisions of domestic law which they adopt in the field governed by this Directive.
Article 4
This Directive shall enter into force on the third day following its publication in the Official Journal of the European Communities.
Article 5
This Directive is addressed to the Member States.
Done at Brussels, 18 June 1999.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ L 86, 6.4.1979, p. 30.
(2) OJ L 318, 27.11.1998, p. 43.
(3) OJ L 125, 23.5.1996, p. 35.
(4) OJ L 261, 24.9.1998, p. 10.
(5) OJ L 172, 7.7.1994, p. 23.
(6) OJ L 41, 16.2.1999, p. 14.
(7) See page 69 of this Official Journal.
(8) OJ L 281, 9.10.1991, p. 23.
