[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 28.07.2005
COM(2005) 340 final
Proposal for a
COUNCIL DECISION
authorising the placing on the market of rye bread with added phytosterols/phytostanols as novel foods or novel food ingredients under Regulation (EC) No 258/97 of the European Parliament and of the Council
(PRESENTED BY THE COMMISSION)
EXPLANATORY MEMORANDUM
On 24 September 2001 Pharmaconsult Oy Ltd. (formerly MultiBene Health Oy Ltd.) submitted, under Article 4 of Regulation (EC) No 258/97 concerning novel foods and novel food ingredients, a request to the competent authorities of Finland for the placing on the market of phytosterols.
The initial assessment report from Finland concluded that the phytosterols are safe for human consumption. However, other Member States raised reasoned objections to their placing on the market as novel foods or novel food ingredients. Therefore an authorisation Decision via comitology procedure was required in accordance with Article 7 (1) of Regulation (EC) No 258/97.
In the view of the Member States’ objections the Commission requested an opinion from the former Scientific Committee on Food (SCF), which was delivered on 4 April 2003. The SCF, in its opinion on the application from MultiBene (now Pharmaconsult Oy Ltd.) for approval of plant sterol-enriched foods, came to the conclusion that the addition of phytosterols is safe, provided that the daily consumption does not exceed 3g.
Furthermore, the SCF, in its opinion “General view on the longterm effects of the intake of elevated levels of phytosterols from multiple dietary sources, with particular attention to the effects on β-carotene” of 26 September 2002 indicated that there was no evidence of additional benefits at intakes higher than 3 g/day but that high intakes might induce undesirable effects and that it was therefore prudent to avoid plant sterol intakes exceeding 3 g/day.
Against this background, a draft Commission Decision authorising the placing on the market of rye bread with added phytosterols/phytostanols as novel foods or novel food ingredients was submitted to the Standing Committee on the Food Chain and Animal Health, on 30 April 2004, for vote. Six Member States voted in favour, six Member States voted against and three Member States abstained.
The Committee failed to deliver, by qualified majority, an opinion on the draft submitted by the Commission. Consequently, pursuant to Article 13 (4) b) of Regulation (EC) No 258/97 and in accordance with Article 5 of Council Decision 1999/468/EC, the Commission must now submit to the Council a proposal relating to the measures to be taken, the Council having three months in which to act by a qualified majority, and inform the European Parliament, which may consider appropriate to take a position in accordance with Article 8 of the above Decision.
Proposal for a
COUNCIL DECISION
authorising the placing on the market of rye bread with added phytosterols/phytostanols as novel foods or novel food ingredients under Regulation (EC) No 258/97 of the European Parliament and of the Council
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community,
Having regard to Regulation (EC) No 258/97 of the European Parliament and of the Council of 27 January 1997 concerning novel foods and novel food ingredients[1], and in particular Article 7 thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) On 24 September 2001 Pharmaconsult Oy Ltd. (formerly MultiBene Health Oy Ltd.) made a request to the competent authorities of Finland for placing phytosterols on the market;
(2) On 17 January 2002 the competent authorities of Finland issued their initial assessment report;
(3) In their initial assessment report, Finland’s competent food assessment body came to the conclusion that the phytosterols/stanols are safe for human consumption;
(4) The Commission forwarded the initial assessment report to all Member States on 5 March 2002;
(5) Within the 60 day period laid down in Article 6 (4) of the Regulation, reasoned objections to the marketing of the product were raised in accordance with that provision;
(6) The Scientific Committee on Food (SCF) in its opinion “General view on the long-term effects of the intake of elevated levels of phytosterols from multiple dietary sources, with particular attention to the effects on (-carotene” of 26 September 2002 indicated that there was no evidence of additional benefits at intakes higher than 3 g/day and that high intakes might induce undesirable effects and that it was therefore prudent to avoid plant sterol intakes exceeding 3 g/day;
(7) Furthermore, the SCF, in its opinion on an application from MultiBene for approval of plant sterol-enriched foods of 4 April 2003, reiterated its concerns about cumulative intakes from a wide range of foods with added phytosterols. However, at the same time the SCF confirmed that the addition of phytosterols to a wide range of bakery products was safe;
(8) In order to meet the concerns on cumulative intakes of phytosterols/phytostanols from different products Pharmaconsult Oy consequently agreed to reduce the original application on bakery products exclusively to rye bread;
(9) Commission Regulation (EC) N° 608/2004[2] concerning the labelling of foods and food ingredients with added phytosterols, phytosterol esters, phytostanols and/or phytostanol esters ensures that consumers receive the information necessary in order to avoid excessive intake of additional phytosterols;
(10) The Standing Committee on the Food Chain and Animal Health has not given a favourable opinion.
HAS ADOPTED THIS DECISION:
Article 1
Foods and food ingredients as described in Annex 1 with added phytosterols/phytostanols as specified in Annex 2, hereinafter called the products, may be placed on the market in the Community.
Article 2
The products shall be presented in such a manner that they can be easily divided into portions that contain either maximum 3 g (in case of 1 portion per day) or maximum 1 g (in case of three portions per day) of added phytosterols/phytostanols.
Article 3
This Decision is addressed to Pharmaconsult Oy, Riippakoivunkuja 5, FIN - 02130 Espoo.
Done at Brussels, [...]
For the Council
The President
ANNEX 1
Products referred to in Article 1
Rye bread that contains ≥ 50% (wholemeal rye flour, whole or cracked rye kernels and rye flakes); ≤ 30 % wheat; ≤ 5 % added sugar but no fat added.
ANNEX 2
Specifications of phytosterols and phytostanols for the addition to foods and food ingredients
Definition:
Phytosterols and phytostanols are sterols and stanols that are extracted from plants and may be presented as free sterols and stanols or esterified with food grade fatty acids.
Composition (with GC-FID or equivalent method):
( 80% (-sitosterol
( 15% (-sitostanol
( 40% campesterol
( 5% campestanol
( 30% stigmasterol
( 3% brassicasterol
( 3% other sterols/stanols
Contamination/Purity (GC-FID or equivalent method)
Phytosterols and phytostanols extracted from sources other than vegetable oil suitable for food have to be free of contaminants, best ensured by a purity of more than 99% ofthe phytosterol/phytostanol ingredient.
[1] OJ L 43, 14.2.1997, p. 1.
[2] OJ L 97, 1.4.2004, p. 44.
