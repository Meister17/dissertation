Appeal brought on 17 March 2006 by Arizona Chemical BV, Eastman Belgium BVBA, Cray Valley Iberica, SA against the judgment of the Court of First Instance (Third Chamber) delivered on 14 December 2005 in Case T-369/03: Arizona Chemical B.V, Eastman Belgium BVBA, Resinall Europe BVBA, Cray Valley Iberica S.A v Commission of the European Communities, supported by Finland
Parties
Appellants: Arizona Chemical BV, Eastman Belgium BVBA, Cray Valley Iberica, SA (represented by: K. Van Maldegem and C. Mereu, avocats)
Other parties to the proceedings: Commission of the European Communities, Finland, Resinall Europe BVBA
Form of order sought
The applicants claim that the Court should:
- declare the present appeal admissible and well-founded;
- set aside the order of the Court of First Instance of 14 December 2005 in Case T-369/03;
- declare the Applicants' requests in Case T-369/03 admissible;
- rule on the merits or, in the alternative, refer the case to the Court of First Instance to rule on the merits; and
- order the Commission of the European Communities to bear all costs and expenses of these proceedings.
Pleas in law and main arguments
The applicants submit that the judgements of the Court of First Instance should be set aside for the following reasons:
1) Inconsistency in reasoning and misapplication of the legal test for admissibility applicable to the addressee of a binding act producing legal effects.
The applicants submit that the Court of First Instance (hereinafter the CFI) erred in law where it did not base its assessment on the fact that the contested decision is a binding act producing legal effects bringing about a distinct change in the applicants' legal position.
2) Misinterpretation of the regulatory framework applicable to the assessment of the applicants' data under directive 67/548/EEC.
The applicants submit that the CFI erred in law where it considered that the assessment of the applicants' data and the Commission's final decision on the relevance of these data as the basis for declassification is not an administrative process that is subject to challenge.
3) Misinterpretation of the regulatory framework and of the applicants' related rights under directive 67/548/EEC.
The applicants submit that te CFI erred in law when it concluded that the applicants could not bring a challenge on the grounds that the applicants were seeking to challenge a measure of general application.
4) Violation of the applicants' right to effective judicial protection.
The applicants submit tha the CFI erred in law when it considered that the applicants may challenge the contested decision at national level.
5) Error in law in concluding that the applicants' action is time-barred.
The applicants' submit that the action for damages is not time-barred because the starting point for damages is, at the earliest, the 1999 decision of the Commission not to declassify, and, at the latest, the contested decision.
--------------------------------------------------
