Commission Regulation (EC) No 1543/2006
of 12 October 2006
amending Regulation (EC) No 474/2006 establishing the Community list of air carriers which are subject to an operating ban within the Community referred to in Chapter II of Regulation (EC) No 2111/2005 of the European Parliament and of the Council and as amended by Regulation (EC) No 910/2006
(Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Regulation (EC) No 2111/2005 of the European Parliament and the Council of 14 December 2005 on the establishment of a Community list of air carriers subject to an operating ban within the Community and on informing air transport passengers of the identity of the operating air carrier, and repealing Article 9 of Directive 2004/36/CE [1] (hereinafter referred to as "the basic Regulation"), and in particular Article 4 thereof,
Whereas:
(1) The Commission adopted Regulation (EC) No 474/2006 of 22 March 2006 establishing the Community list of air carriers which are subject to an operating ban within the Community referred to in Chapter II of Regulation (EC) No 2111/2005 of the European Parliament and of the Council [2].
(2) The Commission adopted Regulation (EC) No 910/2006 of 20 June 2006 amending Regulation (EC) No 474/2006 of 22 March 2006 establishing the Community list of air carriers which are subject to an operating ban within the Community referred to in Chapter II of Regulation (EC) No 2111/2005 of the European Parliament and of the Council [3].
(3) In accordance with Article 4(2) of the basic Regulation and Article 2 of Commission Regulation (EC) No 473/2006 of 22 March 2006 laying down implementing rules for the Community list of air carriers which are subject to an operating ban within the Community referred to in Chapter II of Regulation (EC) No 2111/2005 of the European Parliament and of the Council [4], a Member State requested to update the Community list.
(4) In accordance with Article 4(3) of the basic Regulation, Member States communicated to the Commission information that is relevant in the context of updating the Community list. On this basis, the Commission should decide to update the Community list on its own initiative or at the request of Member States.
(5) In accordance with Article 7 of the basic Regulation and Article 4 of Regulation (EC) No 473/2006, the Commission informed all air carriers concerned either directly or, when this was not practicable, through the authorities responsible for their regulatory oversight, indicating the essential facts and considerations which would form the basis for a decision to impose on them an operating ban within the Community or to modify the conditions of an operating ban imposed on an air carrier which is included in the Community list.
(6) In accordance with Article 7 of the basic Regulation and Article 4 of Regulation (EC) No 473/2006, opportunity was given by the Commission to the air carriers concerned to consult the documents provided by Member States, to submit written comments and to make an oral presentation to the Commission within 10 working days and to the Air Safety Committee [5].
(7) In accordance with Article 3 of Regulation (EC) No 473/2006, the authorities with responsibility for regulatory oversight over the air carriers concerned have been consulted by the Commission as well as, in specific cases, by some Member States.
Dairo Air Services and DAS Air Cargo
(8) There is evidence that the operator DAS Air Cargo (DAZ) certified in Kenya is a subsidiary of Dairo Air Services (DSR) certified in Uganda. The two carriers operate the same aircraft. Therefore, any measure decided with regard to DSR should equally be applicable to DAZ.
(9) There is verified evidence of serious safety deficiencies on the part of Dairo Air Services. These deficiencies have been identified by the Netherlands, the United Kingdom, Belgium, France, Germany and Spain, during ramp inspections performed under the SAFA programme [6]; the repetition of these inspection findings indicates systemic safety deficiencies. Despite cooperation with Member States and individual remedial actions taken by the Ugandan authorities and by Dairo Air Services, the repetition of these findings indicates systemic safety deficiencies.
(10) The UK Civil Aviation Authority performed an inspection on Dairo Air Services and Das Air Cargo, which revealed that between 21 April and 25 July 2006 aircraft operated by the two air carriers were being maintained by a maintenance organisation without a proper approval, thus constituting a serious safety deficiency.
(11) DSR demonstrated a lack of transparency and adequate and timely communication in response to an enquiry by the civil aviation authority of the Netherlands regarding the safety aspect of its operation, as demonstrated by the absence of an adequate and timely reply to the correspondence sent by this Member State.
(12) On the basis of the common criteria, it is assessed that Dairo Air Services and DAS Air Cargo do not fully meet the relevant safety standards, and therefore should be included in Annex A.
Air carriers from the Kyrgyz Republic
(13) Following the invitation of the Kyrgyz Republic civil aviation authority, a team of European experts conducted a fact-finding mission to the Kyrgyz Republic from 10 to 15 September 2006. Its report shows that the Kyrgyz civil aviation authority has revealed an insufficient ability to implement and enforce the relevant safety standards in accordance with their obligations under the Chicago convention.
(14) In addition, a majority of carriers visited by the European experts although holders of an Air Operators Certificate (AOC) issued by Kyrgyz Republic, did not have their principle place of business in Kyrgyz Republic, contrary to the requirements of Annex 6 to the Chicago Convention.
(15) On the basis of the common criteria, it is assessed that all air carriers certified in Kyrgyz Republic do not meet the relevant safety standards and therefore they should be subject to an operating ban and included in Annex A.
(16) The authorities of Kyrgyz Republic have provided the Commission with evidence of the withdrawal of the Air Operator's Certificates of the following two air carriers: Phoenix Aviation and Star Jet. Since these two carriers certified in Kyrgyz Republic have consequently ceased their activities, they should not be included in Annex A.
Air carriers from the Democratic Republic of Congo
(17) The authorities of the Democratic Republic of Congo have provided the Commission with information indicating that they released an AOC to the following air carriers: Air Beni, Air Infini, Bel Glob Airlines, Bravo Air Congo, Gomair, Katanga Airways, Sun Air Services, Zaabu International. Since these new air carriers are certified by the authorities of the Democratic Republic of Congo which have shown a lack of ability to carry out adequate safety oversight, they should be included in Annex A.
(18) The authorities of the Democratic Republic of Congo have provided the Commission with evidence of the withdrawal of the Air Operator's Certificates of the following air carriers: African Business and Transportations, Air Charter Services, Air Plan International, Air Transport Service, ATO – Air Transport Office, Congo Air, Dahla Airlines, DAS Airlines, Espace Aviation Services, Funtshi Aviation Service, GR Aviation, JETAIR – Jet Aero Services, Kinshasa Airways, Okapi Airways, Scibe Airlift, Shabair, Trans Service Airlift, Waltair Aviation, Zaire Aero Service (ZAS). Since these carriers certified in the Democratic Republic of Congo have consequently ceased their activities, they should be withdrawn from Annex A.
Air carriers from Liberia
(19) The authorities of Liberia have provided the Commission with evidence of the withdrawal of the Air Operator's Certificates of the following air carriers: Air Cargo Plus, Air Cess (Liberia), Air Liberia, Atlantic Aviation Services, Bridge Airlines, Excel Air Services, International Air Services, Jet Cargo-Liberia, Liberia Airways, Liberian World Airlines, Lonestar Airways, Midair Limited, Occidental Airlines, Occidental Airlines (Liberia), Santa Cruise Imperial Airlines, Satgur Air Transport, Simon Air, Sosoliso Airlines, Trans-African Airways, Transway Air Services, United Africa Airlines (Liberia). Since these carriers certified in Liberia have consequently ceased their activities, they should be withdrawn from Annex A.
Air carriers from Sierra Leone
(20) The authorities of Sierra Leone have provided the Commission with evidence of the withdrawal of the Air Operator's Certificates of the following air carriers: Aerolift, Afrik Air Links, Air Leone, Air Salone, Air Sultan Limited, Air Universal, Central Airways Limited, First Line Air, Inter Tropic Airlines, Mountain Air Company, Orange Air Services, Pan African Air Services, Sierra National Airlines, Sky Aviation, Star Air, Transport Africa, Trans Atlantic Airlines, West Coast Airways. Since these carriers certified in Sierra Leone have consequently ceased their activities, they should be withdrawn from Annex A.
Air carriers from Swaziland
(21) The authorities of Swaziland have provided the Commission with evidence of the withdrawal of the Air Operator's Certificates of the following air carriers: African International Airways, Air Swazi Cargo, East Western Airways, Galaxy Avion, Interflight, Northeast Airlines, Ocean Air, Skygate International, Swazi Air Charter, Volga Atlantic Airlines. Since these carriers certified in Swaziland have consequently ceased their activities, they should be withdrawn from Annex A.
(22) The authorities of Swaziland and South Africa have provided sufficient evidence that the Air Operator's Certificate issued to African International Airway’s under the aegis of the CAA of Swaziland has been withdrawn, and that the air carrier is now operating under a new Air Operator's Certificate issued by the CAA of South Africa which therefore has the responsibility for its safety oversight. Therefore, on the basis of the common criteria, and without prejudice to verification of effective compliance with the relevant safety standards through adequate ramp inspections, it is assessed that African International Airways should be withdrawn from Annex A.
Air Service Comores
(23) In reply to an inquiry by the civil aviation authority of France, Air Service Comores indicated that an action plan had been established in order to correct the safety deficiencies identified during ramp inspections. However, there is still no evidence of the implementation of an appropriate action plan for all operations of Air Service Comores.
(24) The authorities of Comoros with responsibility for regulatory oversight of Air Service Comores have provided the Civil Aviation Authorities of France with sufficient information about the safety of operations with respect to the specific aircraft LET 410 UVP with registration mark D6-CAM.
(25) Therefore, on the basis of the common criteria, it is assessed that Air Service Comores meets the relevant safety standards only for flights operated with the aircraft LET 410 UVP with registration mark D6-CAM. Consequently Air Service Comores should be subject to operational restrictions and should be moved from Annex A to Annex B.
Ariana Afghan Airlines
(26) Ariana Afghan Airlines submitted a request to be withdrawn from the Community list, provided some documentation in support of this request and showed strong disposition towards cooperation with the Commission and Member States. However, since the full implementation of an adequate corrective action plan by the carrier is not completed, the Commission considers that Ariana Afghan Airlines should be retained in the Community list.
(27) Ariana Afghan Airlines provided information indicating that it has ceased operations with the aircraft Airbus A-310 registered in France with marks F-GYYY, which has been sold.
(28) Therefore, the specific conditions applicable to the Community ban to Ariana Afghan Airlines have changed. The air carrier should be subject to a ban to all its operations and therefore remain included in Annex A.
Air Koryo
(29) Documentation submitted by Air Koryo and the Civil Aviation Authorities of the Democratic People’s Republic of Korea (DPRK) indicates that the carrier has embarked upon a corrective action plan with the intention of aligning itself fully with the relevant safety standards in due time.
(30) Furthermore, the Civil Aviation Authorities of the Democratic People’s Republic of Korea (DPRK) has stated that at present, Air Koryo is not permitted to operate any flights to European destinations unless the carrier equips itself with new aircraft which meet the relevant international safety standards.
(31) On the basis of the common criteria, it is assessed that Air Koryo still does not meet fully the relevant safety standards and therefore should be retained within Annex A.
Phuket Air
(32) Following the invitation of the air carrier, a team of European experts conducted a fact-finding mission to Phuket Air in Bangkok, Thailand between 11 to 15 September 2006. The report from this mission shows that while significant progress has been made by the carrier following its inclusion in the Community list, substantial safety deficiencies still remain to be rectified.
(33) Whilst acknowledging the effort made by the carrier towards achieving the level of progress noted in the report, as well as the strong disposition towards cooperation shown both by the carrier and the Thai Department of Civil Aviation, a decision to withdraw Phuket Air from the EC list is still considered to be premature pending the receipt and review of satisfactory evidence confirming the full implementation of the corrective action plan which the carrier is still in the process of completing.
(34) On the basis of the common criteria, it is assessed that Phuket Air still does not meet fully the relevant safety standards and therefore should be retained within Annex A.
A Jet Aviation/Helios Airways
(35) The air carrier formerly known as Helios Airways now operates as A Jet Aviation. Indeed the Air Operator Certificate of Helios Airways has been subject to a variation consisting in a change of name to A Jet Aviation [7].
(36) An investigation conducted by the European Aviation Safety Agency (EASA) under Article 45 of Regulation (EC) No 1592/2002 of the European Parliament and of the Council [8] and by the Joint Aviation Authorities (JAA) during three joint visits which took place between October 2005 and August 2006 [9] identified series safety deficiencies related to the operations of A Jet Aviation/Helios Airways.
(37) Following consultations with EASA, JAA and the Commission, the civil aviation authorities of Cyprus with responsibility for regulatory oversight of that carrier have provided evidence of the adoption of provisional measures to correct these safety deficiencies identified.
(38) In view of the above considerations, the Commission considers that, at this stage, A Jet Aviation/Helios Airways should not be included in the Community list. However the situation of this carrier and the exercise of oversight responsibilities by the Cyprus civil aviation authorities will be closely monitored by the Commission with the assistance of EASA and JAA in the coming months.
Johnsons Air
(39) Following deficiencies identified by various Member States, these Member States and the Commission entered into consultations with Johnsons Air and the civil aviation authorities of Ghana with responsibility for regulatory oversight of that carrier.
(40) Johnsons Air has provided evidence of an action plan intended to correct the safety deficiencies identified. Furthermore, the competent authorities of Ghana should submit within strict deadlines its oversight programme for the operations conducted by Johnsons Air outside Ghana.
(41) In view of the above considerations, the Commission considers that, at this stage, Johnsons Air should not be included in the Community list. Without prejudice to further verification of effective compliance with the relevant safety standards through adequate ramp inspections, the Commission intends to review within three months the situation of Johnsons Air on the basis of the oversight programme due to be submitted by the civil aviation authorities of Ghana.
Pakistan International Airlines
(42) Following serious safety deficiencies identified by various Member States indicating systemic safety problems, these Member States and the Commission entered into consultations with Pakistan International Airlines and the civil aviation authorities of Pakistan with responsibility for regulatory oversight of that carrier.
(43) The Commission has asked Pakistan International Airlines to provide evidence of an adequate remedial action plan intended to address its systemic safety deficiencies within strict deadlines. Furthermore, the competent authorities of Pakistan have announced the establishment of an action plan to reinforce their surveillance activities on the carrier which must be urgently submitted to the Commission.
(44) Pending the submission of the above mentioned plans within the indicated deadlines and the formal endorsement of such plan by the Pakistani authorities, the Commission considers that, at this stage, Pakistan International Airlines should not be included in the Community list. However the Commission will take appropriate action, if necessary under Article 5(1) of the basic Regulation, in the event that the above mentioned plans are not delivered in due time or are judged insufficient. In addition, the Member States intend to ensure further verification of effective compliance with the relevant safety standards through systematic ramp inspections on this carrier.
Pulkovo
(45) Following deficiencies identified by various Member States, the Commission entered into consultations with the authorities of Russia with responsibility for regulatory oversight of that carrier and heard the carrier concerned.
(46) Pulkovo has provided evidence of an action plan intended to correct its systemic safety deficiencies within specific deadlines and to further improve their organisation with a view to managing safety effectively. The action plan has been formally endorsed by the competent authorities of Russia. Furthermore, the competent authorities of Russia have submitted an action plan to reinforce their surveillance activities on the carrier.
(47) In view of the above considerations, the Commission considers that, at this stage, Pulkovo should not be included in the Community list. Without prejudice to further verification of effective compliance with the relevant safety standards including through ramp inspections, the Commission intends to review the situation of Pulkovo or the carrier resulting from its announced future merger with another Russian carrier and of the authorities with responsibility for regulatory oversight of this air carrier within three months, with the assistance of the European Aviation Safety Agency and the authorities of any interested Member State. Both the carrier and the competent authorities of Russia have accepted this procedure.
General considerations concerning the other carriers included in the list
(48) No evidence of the full implementation of appropriate remedial actions by the other carriers included in the list updated on 20 June 2006 and by the authorities with responsibility for regulatory oversight of these air carriers has been communicated to the Commission so far in spite of specific requests submitted by the latter. Therefore, on the basis of the common criteria, it is assessed that these air carriers should continue to be subject to an operating ban.
(49) The measures provided for in this Regulation are in accordance with the opinion of the Air Safety Committee,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 474/2006, as modified by Regulation (EC) No 910/2006, is amended as follows:
1. Annex A of the Regulation is replaced by the Annex A to this Regulation.
2. Annex B of the Regulation is replaced by the Annex B to this Regulation.
Article 2
This Regulation shall enter into force on the day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 12 October 2006.
For the Commission
Jacques Barrot
Vice-President
[1] OJ L 344, 27.12.2005, p. 15.
[2] OJ L 84, 23.3.2006, p. 14.
[3] OJ L 168, 21.6.2006, p. 16.
[4] OJ L 84, 23.3.2006, p. 8.
[5] Established by Article 12 of Council Regulation (EEC) No 3922/91 of 16 December 1991 on the harmonization of technical requirements and administrative procedures in the field of civil aviation (OJ L 373, 31.12.1991, p. 4).
[6] CAA-NL-2000-47, CAA-NL-2003-50, CAA-NL-2004-13, CAA-NL-2004-39, CAA-NL-2004-132, CAA-NL-2004-150, CAA-NL-2005-8, CAA-NL-2005-65, CAA-NL-2005-141, CAA-NL-2005-159, CAA-NL-2005-161, CAA-NL-2005-200, CAA-NL-2005-205, CAA-NL-2005-220, CAA-NL-2005-225, CAA-NL-2006-1, CAA-NL-2006-11, CAA-NL-2006-53, CAA-NL-2006-54, CAA-NL-2006-55, CAA-NL-2006-56, CAA-NL-2006-57, CAA-UK-2005-24, CAA-UK-2006-97, CAA-UK-2006-117, DGAC-E-2005-268, LBA/D-2005-511, LBA/D-2006-483, BCAA-2000-1, BCAA-2006-38, DGAC/F-2003-397.
[7] Initially, Helios Airways intended to set up a new legal entity called A Jet and transfer all its assets to the new company. A Jet would operate using the procedures, aircraft, facilities, personnel and management structure already accepted by the Department of Civil Aviation for Helios. Consequently, a full AOC issue process was started. Nevertheless, Helios changed its name on the Registry of Companies to A Jet. The AOC and other applicable approval documents have been amended to reflect the new name.
[8] OJ L 240, 7.9.2002, p. 1. Regulation as last amended by Commission Regulation (EC) No 1701/2003 (OJ L 243, 27.9.2003, p. 5).
[9] A JAA-EASA Joint Standardisation visit in Cyprus had been performed in October 2005. A first follow-up visit was organised from 22 to 24 May 2006, to review how the actions undertaken by the DCA did address the findings raised. Because of the importance of the findings made in the latter visit and because some actions were not performed or closed yet, a second follow-up visit was organised from 7 to 9 August 2006. For the specific issues raised in the area of operational requirements (JAR-OPS and JAR-FCL), the JAA have carried out a visit on 6 July 2006; also, the competent authorities of Cyprus assisted by the UKCAA have carried out an inspection visit on 12 to 15 September 2006.
--------------------------------------------------
ANNEX A
LIST OF AIR CARRIERS OF WHICH ALL OPERATIONS ARE SUBJECT TO A BAN WITHIN THE COMMUNITY [1]
Name of the legal entity of the air carrier as indicated on its AOC (and its trading name, if different) | Air Operator Certificate (AOC) Number or Operating Licence Number | ICAO airline designation number | State of the Operator |
Air Koryo | Unknown | KOR | Democratic People's Republic of Korea (DPRK) |
Ariana Afghan Airlines | 009 | AFG | Afghanistan |
BGB Air | AK-0194-04 | POI | Kazakhstan |
Blue Wing Airlines | SRSH-01/2002 | BWI | Surinam |
Dairo Air Services | 005 | DSR | Uganda |
DAS Air Cargo | Unknown | DAZ | Kenya |
GST Aero Air Company | AK-020304 | BMK | Kazakhstan |
Phuket Airlines | 07/2544 | VAP | Thailand |
Silverback Cargo Freighters | Unknown | VRB | Rwanda |
All air carriers certified by the authorities with responsibility for regulatory oversight of Democratic Republic of Congo (RDC), with the exception of Hewa Bora Airways [2], including | — | — | Democratic Republic of Congo (RDC) |
Africa One | 409/CAB/MIN/TC/017/2005 | CFR | Democratic Republic of Congo (RDC) |
African Company Airlines | 409/CAB/MIN/TC/009/2005 | FPY | Democratic Republic of Congo (RDC) |
Aigle Aviation | 409/CAB/MIN/TC/0042/2006 | Unknown | Democratic Republic of Congo (RDC) |
Air Beni | 409/CAB/MIN/TC/0019/2005 | Unknown | Democratic Republic of Congo (RDC) |
Air Boyoma | 409/CAB/MIN/TC/0049/2006 | Unknown | Democratic Republic of Congo (RDC) |
Air Infini | 409/CAB/MIN/TC/006/2006 | Unknown | Democratic Republic of Congo (RDC) |
Air Kasai | 409/CAB/MIN/TC/010/2005 | Unknown | Democratic Republic of Congo (RDC) |
Air Navette | 409/CAB/MIN/TC/015/2005 | Unknown | Democratic Republic of Congo (RDC) |
Air Tropiques SPRL | 409/CAB/MIN/TC/007/2005 | Unknown | Democratic Republic of Congo (RDC) |
Bel Glob Airlines | 409/CAB/MIN/TC/0073/2006 | Unknown | Democratic Republic of Congo (RDC) |
Blue Airlines | 409/CAB/MIN/TC/038/2005 | BUL | Democratic Republic of Congo (RDC) |
Bravo Air Congo | 409/CAB/MIN/TC/0090/2006 | Unknown | Democratic Republic of Congo (RDC) |
Business Aviation SPRL | 409/CAB/MIN/TC/012/2005 | Unknown | Democratic Republic of Congo (RDC) |
Butembo Airlines | 409/CAB/MIN/TC/0056/2006 | Unknown | Democratic Republic of Congo (RDC) |
Cargo Bull Aviation | 409/CAB/MIN/TC/032/2005 | Unknown | Democratic Republic of Congo (RDC) |
Central Air Express | 409/CAB/MIN/TC/011/2005 | CAX | Democratic Republic of Congo (RDC) |
Cetraca Aviation Service | 409/CAB/MIN/TC/037/2005 | CER | Democratic Republic of Congo (RDC) |
CHC Stellavia | 409/CAB/MIN/TC/0050/2006 | Unknown | Democratic Republic of Congo (RDC) |
Comair | 409/CAB/MIN/TC/0057/2006 | Unknown | Democratic Republic of Congo (RDC) |
Compagnie Africaine d’Aviation (CAA) | 409/CAB/MIN/TC/016/2005 | Unknown | Democratic Republic of Congo (RDC) |
CO-ZA Airways | 409/CAB/MIN/TC/0053/2006 | Unknown | Democratic Republic of Congo (RDC) |
Doren Air Congo | 409/CAB/MIN/TC/0054/2006 | Unknown | Democratic Republic of Congo (RDC) |
Enterprise World Airways | 409/CAB/MIN/TC/031/2005 | EWS | Democratic Republic of Congo (RDC) |
Filair | 409/CAB/MIN/TC/014/2005 | Unknown | Democratic Republic of Congo (RDC) |
Free Airlines | 409/CAB/MIN/TC/0047/2006 | Unknown | Democratic Republic of Congo (RDC) |
Galaxy Incorporation | 409/CAB/MIN/TC/0078/2006 | Unknown | Democratic Republic of Congo (RDC) |
Global Airways | 409/CAB/MIN/TC/029/2005 | BSP | Democratic Republic of Congo (RDC) |
Goma Express | 409/CAB/MIN/TC/0051/2006 | Unknown | Democratic Republic of Congo (RDC) |
Gomair | 409/CAB/MIN/TC/0023/2005 | Unknown | Democratic Republic of Congo (RDC) |
Great Lake Business Company | 409/CAB/MIN/TC/0048/2006 | Unknown | Democratic Republic of Congo (RDC) |
ITAB – International Trans Air Business | 409/CAB/MIN/TC/0022/2005 | Unknown | Democratic Republic of Congo (RDC) |
Katanga Airways | 409/CAB/MIN/TC/0088/2006 | Unknown | Democratic Republic of Congo (RDC) |
Kivu Air | 409/CAB/MIN/TC/0044/2006 | Unknown | Democratic Republic of Congo (RDC) |
Lignes Aériennes Congolaises | Ministerial signature (ordonnance 78/205) | LCG | Democratic Republic of Congo (RDC) |
Malu Aviation | 409/CAB/MIN/TC/013/2005 | Unknown | Democratic Republic of Congo (RDC) |
Malila Airlift | 409/CAB/MIN/TC/008/2005 | MLC | Democratic Republic of Congo (RDC) |
Mango Airlines | 409/CAB/MIN/TC/0045/2006 | Unknown | Democratic Republic of Congo (RDC) |
Rwakabika "Bushi Express" | 409/CAB/MIN/TC/0052/2006 | Unknown | Democratic Republic of Congo (RDC) |
Safari Logistics SPRL | 409/CAB/MIN/TC/0076/2006 | Unknown | Democratic Republic of Congo (RDC) |
Services Air | 409/CAB/MIN/TC/0033/2005 | Unknown | Democratic Republic of Congo (RDC) |
Sun Air Services | 409/CAB/MIN/TC/0077/2006 | Unknown | Democratic Republic of Congo (RDC) |
Tembo Air Services | 409/CAB/MIN/TC/0089/2006 | Unknown | Democratic Republic of Congo (RDC) |
Thom’s Airways | 409/CAB/MIN/TC/030/2005 | Unknown | Democratic Republic of Congo (RDC) |
TMK Air Commuter | 409/CAB/MIN/TC/020/2005 | Unknown | Democratic Republic of Congo (RDC) |
Tracep | 409/CAB/MIN/TC/0055/2006 | Unknown | Democratic Republic of Congo (RDC) |
Trans Air Cargo Service | 409/CAB/MIN/TC/035/2005 | Unknown | Democratic Republic of Congo (RDC) |
Transports Aériens Congolais (TRACO) | 409/CAB/MIN/TC/034/2005 | Unknown | Democratic Republic of Congo (RDC) |
Uhuru Airlines | 409/CAB/MIN/TC/039/2005 | Unknown | Democratic Republic of Congo (RDC) |
Virunga Air Charter | 409/CAB/MIN/TC/018/2005 | Unknown | Democratic Republic of Congo (RDC) |
Wimbi dira Airways | 409/CAB/MIN/TC/005/2005 | WDA | Democratic Republic of Congo (RDC) |
Zaabu International | 409/CAB/MIN/TC/0046/2006 | Unknown | Democratic Republic of Congo (RDC) |
All air carriers certified by the authorities with responsibility for regulatory oversight of Equatorial Guinea, including | — | — | Equatorial Guinea |
Air Bas | Unknown | RBS | Equatorial Guinea |
Air Consul SA | Unknown | RCS | Equatorial Guinea |
Air Maken | Unknown | AKE | Equatorial Guinea |
Air Services Guinea Ecuatorial | Unknown | SVG | Equatorial Guinea |
Aviage | Unknown | VGG | Equatorial Guinea |
Avirex Guinée Équatoriale | Unknown | AXG | Equatorial Guinea |
Cargo Plus Aviation | Unknown | CGP | Equatorial Guinea |
Cess | Unknown | CSS | Equatorial Guinea |
Cet Aviation | Unknown | CVN | Equatorial Guinea |
COAGE – Compagnie Aeree De Guinee Equatorial | Unknown | COG | Equatorial Guinea |
Compania Aerea Lineas Ecuatoguineanas de Aviacion S.A. (LEASA) | Unknown | LAS | Equatorial Guinea |
Ducor World Airlines | Unknown | DWA | Equatorial Guinea |
Ecuato Guineana de Aviacion | Unknown | ECV | Equatorial Guinea |
Ecuatorial Express Airlines | Unknown | EEB | Equatorial Guinea |
Ecuatorial Cargo | Unknown | EQC | Equatorial Guinea |
Equatair | Unknown | EQR | Equatorial Guinea |
Equatorial Airlines SA | Unknown | EQT | Equatorial Guinea |
Euroguineana de Aviacion | Unknown | EUG | Equatorial Guinea |
Federal Air GE Airlines | Unknown | FGE | Equatorial Guinea |
GEASA — Guinea Ecuatorial Airlines SA | Unknown | GEA | Equatorial Guinea |
GETRA — Guinea Ecuatorial de Transportes Aereos | Unknown | GET | Equatorial Guinea |
Guinea Cargo | Unknown | GNC | Equatorial Guinea |
Jetline Inc. | Unknown | JLE | Equatorial Guinea |
Kng Transavia Cargo | Unknown | VCG | Equatorial Guinea |
Litoral Airlines, Compania, (Colair) | Unknown | CLO | Equatorial Guinea |
Lotus International Air | Unknown | LUS | Equatorial Guinea |
Nagesa, Compania Aerea | Unknown | NGS | Equatorial Guinea |
Presidencia de la Republica de Guinea Ecuatorial | Unknown | ONM | Equatorial Guinea |
Prompt Air GE SA | Unknown | POM | Equatorial Guinea |
Skimaster Guinea Ecuatorial | Unknown | KIM | Equatorial Guinea |
Skymasters | Unknown | SYM | Equatorial Guinea |
Southern Gateway | Unknown | SGE | Equatorial Guinea |
Space Cargo Inc. | Unknown | SGO | Equatorial Guinea |
Trans Africa Airways G.E.S.A. | Unknown | TFR | Equatorial Guinea |
Unifly | Unknown | UFL | Equatorial Guinea |
UTAGE — Union de Transport Aereo de Guinea Ecuatorial | Unknown | UTG | Equatorial Guinea |
Victoria Air | Unknown | VIT | Equatorial Guinea |
All Air Carriers Certified By The Authorities With Responsibility For Regulatory Oversight Of The Kyrgyz Republic, including | — | — | Kyrgyz Republic |
Anikay Air | 16 | AKF | Kyrgyz Republic |
Asia Alpha | 31 | SAL | Kyrgyz Republic |
Avia Traffic Company | 23 | AVJ | Kyrgyz Republic |
Bistair-Fez Bishkek | 08 | BSC | Kyrgyz Republic |
Botir Avia | 10 | BTR | Kyrgyz Republic |
British Gulf International Airlines Fez | 18 | BGK | Kyrgyz Republic |
Click Airways | 11 | CGK | Kyrgyz Republic |
Country International Airlines | 19 | CIK | Kyrgyz Republic |
Dames | 20 | DAM | Kyrgyz Republic |
Fab — Air | 29 | FBA | Kyrgyz Republic |
Galaxy Air | 12 | GAL | Kyrgyz Republic |
Golden Rule Airlines | 22 | GRS | Kyrgyz Republic |
Intal Avia | 27 | INL | Kyrgyz Republic |
Itek Air | 04 | IKA | Kyrgyz Republic |
Kyrgyz Airways | 06 | KGZ | Kyrgyz Republic |
Kyrgyz General Aviation | 24 | KGB | Kyrgyz Republic |
Kyrgyz Trans Avia | 31 | KTC | Kyrgyz Republic |
Kyrgyzstan Altyn | 03 | LYN | Kyrgyz Republic |
Kyrgyzstan Airlines | 01 | KGA | Kyrgyz Republic |
Max Avia | 33 | MAI | Kyrgyz Republic |
OHS Avia | 09 | OSH | Kyrgyz Republic |
Reem Air | 07 | REK | Kyrgyz Republic |
Sky Gate International Aviation | 14 | SGD | Kyrgyz Republic |
Sky Way | 21 | SAB | Kyrgyz Republic |
Sun Light | 25 | SUH | Kyrgyz Republic |
Tenir Airlines | 26 | TEB | Kyrgyz Republic |
Trast Aero | 05 | TSJ | Kyrgyz Republic |
All Air Carriers Certified By The Authorities With Responsibility For Regulatory Oversight Of Liberia, including | — | — | Liberia |
Weasua Air Transport Co., Ltd | Unknown | WTC | Liberia |
All Air Carriers Certified By The Authorities With Responsibility For Regulatory Oversight Of Sierra Leone, including | — | — | Sierra Leone |
Air Rum Ltd | Unknown | RUM | Sierra Leone |
Bellview Airlines (S/L) Ltd | Unknown | BVU | Sierra Leone |
Destiny Air Services Ltd | Unknown | DTY | Sierra Leone |
Heavylift Cargo | Unknown | Unknown | Sierra Leone |
Orange Air Sierra Leone Ltd | Unknown | ORJ | Sierra Leone |
Paramount Airlines Ltd | Unknown | PRR | Sierra Leone |
Seven Four Eight Air Services Ltd | Unknown | SVT | Sierra Leone |
Teebah Airways | Unknown | Unknown | Sierra Leone |
All Air Carriers Certified By The Authorities With Responsibility For Regulatory Oversight Of Swaziland, including | — | — | Swaziland |
Aero Africa (Pty) Ltd | Unknown | RFC | Swaziland |
Jet Africa Swaziland | Unknown | OSW | Swaziland |
Royal Swazi National Airways Corporation | Unknown | RSN | Swaziland |
Scan Air Charter Ltd | Unknown | Unknown | Swaziland |
Swazi Express Airways | Unknown | SWX | Swaziland |
Swaziland Airlink | Unknown | SZL | Swaziland |
[1] Air carriers listed in Annex A could be permitted to exercise traffic rights by using wet-leased aircraft of an air carrier which is not subject to an operating ban, provided that the relevant safety standards are complied with.
[2] Hewa Bora Airways is allowed to use the specific aircraft mentioned in Annex B for its current operations within the European Community.
--------------------------------------------------
ANNEX B
LIST OF AIR CARRIERS OF WHICH OPERATIONS ARE SUBJECT TO OPERATIONAL RESTRICTIONS WITHIN THE COMMUNITY [1]
Name of the legal entity of the air carrier as indicated on its AOC (and its trading name, if different) | Air Operator Certificate (AOC) Number | ICAO airline designation number | State of the Operator | Aircraft type | Registration mark(s) and, when available, construction serial number(s) | State of registry |
Air Bangladesh | 17 | BGD | Bangladesh | B747-269B | S2-ADT | Bangladesh |
Air Service Comores | 06-819/TA-15/DGACM | KMD | Comoros | All fleet with the exception of: LET 410 UVP | All fleet with the exception of: D6-CAM (851336) | Comoros |
Air West Co. Ltd | 004/A | AWZ | Sudan | All fleet with the exception of: IL-76 | All fleet with the exception of: ST-EWX (construction serial No 1013409282) | Sudan |
Hewa Bora Airways (HBA) [2] | 416/dac/tc/sec/087/2005 | ALX | Democratic Republic of Congo (RDC) | All fleet with the exception of: L-1011 | All fleet with the exception of : 9Q-CHC (construction serial No 193H-1209) | Democratic Republic of Congo (RDC) |
[1] Air carriers listed in Annex B could be permitted to exercise traffic rights by using wet-leased aircraft of an air carrier which is not subject to an operating ban, provided that the relevant safety standards are complied with.
[2] Hewa Bora Airways is only allowed to use the specific aircraft mentioned for its current operations within the European Community.
--------------------------------------------------
