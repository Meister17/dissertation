Commission Regulation (EC) No 301/2006
of 20 February 2006
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 21 February 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 20 February 2006.
For the Commission
J. L. Demarty
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 386/2005 (OJ L 62, 9.3.2005, p. 3).
--------------------------------------------------
ANNEX
to Commission Regulation of 20 February 2006 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code [1] | Standard import value |
07020000 | 052 | 79,6 |
204 | 48,3 |
212 | 95,5 |
624 | 111,0 |
999 | 83,6 |
07070005 | 052 | 148,8 |
204 | 89,9 |
628 | 131,0 |
999 | 123,2 |
07091000 | 220 | 79,4 |
624 | 95,8 |
999 | 87,6 |
07099070 | 052 | 107,7 |
204 | 53,5 |
999 | 80,6 |
08051020 | 052 | 54,3 |
204 | 52,1 |
212 | 41,8 |
220 | 45,3 |
624 | 62,3 |
999 | 51,2 |
08052010 | 204 | 100,5 |
999 | 100,5 |
08052030, 08052050, 08052070, 08052090 | 052 | 63,7 |
204 | 99,4 |
220 | 80,3 |
464 | 141,8 |
624 | 77,1 |
662 | 58,9 |
999 | 86,9 |
08055010 | 052 | 45,8 |
220 | 68,7 |
999 | 57,3 |
08081080 | 400 | 121,9 |
404 | 104,0 |
528 | 112,1 |
720 | 84,1 |
999 | 105,5 |
08082050 | 388 | 77,2 |
400 | 77,0 |
512 | 80,6 |
528 | 79,4 |
720 | 68,0 |
999 | 76,4 |
[1] Country nomenclature as fixed by Commission Regulation (EC) No 750/2005 (OJ L 126, 19.5.2005, p. 12). Code "999" stands for "of other origin".
--------------------------------------------------
