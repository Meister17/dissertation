Action brought on 28 June 2006 — PowderJect Research v OHIM (POWDERMED)
Parties
Applicant: PowderJect Research Ltd (Oxford, United Kingdom) (represented by: A. Bryson, Barrister, P. Brownlow, Solicitor)
Defendant: Office for Harmonisation in the Internal Market (Trade Marks and Designs)
Form of order sought
- Annul the decision of the Second Board of Appeal of the Office for Harmonisation in the Internal Market (Trade Marks and Designs) of 12 April 2006, relating to the applicant's Community trade mark application No 4202743 for POWERMED in classes 5, 10 and 42;
- award costs in favour of the applicant.
Pleas in law and main arguments
Community trade mark concerned: The word mark "POWDERMED" for goods and services in classes 5, 10 and 42 — application No 4202743
Decision of the examiner: Refusal of the application
Decision of the Board of Appeal: Dismissal of the appeal
Pleas in law: Infringement of Article 7(1)(b) and (c) of Council Regulation No 40/94. The Board of Appeal erred in treating the trade mark as being composed of two English words in common usage and it gave no or insufficient consideration to the structure of the trade mark applied for by not taking into account that the trade mark would at first sight suggest the past tense of a verb by reason of its closing letters "…ed" and that it would not be immediately apparent to the relevant public that the trade mark could be dissected into "powder" and "med".
--------------------------------------------------
