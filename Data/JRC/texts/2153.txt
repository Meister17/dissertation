State aid
(Articles 87 to 89 of the Treaty establishing the European Community)
Commission notice pursuant to Article 88(2) of the EC Treaty to other Member States and interested parties
State aid C 10/2000 (ex NN 112/99 and N 141/99)
Aid for STAMAG Stahl- und Maschinenbau AG, Saxony — Germany
(2005/C 270/14)
(Text with EEA relevance)
By the following letter dated 14 December 2000 the Commission informed Germany of its decision to close the procedure under Article 88(2) of the EC Treaty.
TEXT OF LETTER
"1. By letter dated 24 February 1999, registered as received on 26 February under number N 141/99, Germany notified the Commission of aid for STAMAG Stahl- und Maschinenbau AG pursuant to Article 88(3) of the EC Treaty.
2. The Commission had approved restructuring aid for the company back in 1997 [1]. The aid notified in 1999 was seen as an amendment to the original restructuring plan.
3. The Commission requested further information on 25 March 1999. The deadline for replying was extended on two occasions, until 7 May and 5 June 1999. The information requested was submitted by letters dated 7 and 21 June and 8, 12 and 13 July 1999. The case was discussed at a meeting with representatives of the Federal Government on 20 July 1999. Further details were provided on 2 and 26 August 1999.
4. By letter dated 19 August 1999, registered as received on 27 August, the Commission was informed of the payment of part of the notified aid package and of additional aid measures. The case was therefore re-registered as non-notified aid under number NN 112/1999. Additional information was submitted on 7, 12 and 26 October and 12 November 1999. On 27 December 1999 Germany informed the Commission that STAMAG had filed for bankruptcy on 10 December 1999 and withdrew the notification.
5. Since, on the basis of the information available, some of the aid measures had been implemented, the Commission decided to initiate the formal investigation procedure. The Commission decision was published in the Official Journal of the European Communities [2].
6. The Commission invited interested parties to submit their comments on the aid. It received comments from the United Kingdom through its Permanent Representation to the EU. It forwarded them to Germany, which was given the opportunity to react.
7. Germany's comments were received on 27 July 2000. In that letter Germany explained that, in the end, no new aid had been paid out and that the aid approved by the Commission in 1997 had been registered as part of the bankrupt estate.
8. The Commission notes that, under Article 8 of Council Regulation (EC) No 659/1999 [3], the Member State concerned may withdraw the notification in due time before the Commission has taken a decision on the aid. In cases where it has initiated the formal investigation procedure, the Commission is required to close that procedure.
9. Consequently, the Commission has decided to close the formal investigation procedure under Article 88(3) of the EC Treaty and notes that the relevant aid was never paid out and that Germany has withdrawn the notification."
[1] OJ C 58, 24.2.1998.
[2] OJ C 110, 15.4.2000.
[3] Council Regulation (EC) No 659/1999 of 22 March 1999 laying down detailed rules for the application of Article 93 of the EC Treaty (OJ L 83, 27.3.1999, p. 1).
--------------------------------------------------
