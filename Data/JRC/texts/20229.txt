COMMISSION REGULATION (EC) No 785/95 of 6 April 1995 laying down detailed rules for the application of Council Regulation (EC) No 603/95 on the common organization of the market in dried fodder
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 603/95 of 21 February 1995 on the common organization of the market in dried fodder (1), and in particular Article 18 thereof,
Whereas, in order to ensure efficient application of the system of aid for dried fodder, certain concepts should be defined;
Whereas, in order to avoid any risk of double payment, it is appropriate to exclude all of the products benefitting from the aid of arable crops listed in Annex I of Council Regulation (EEC) No 1765/92 (2), as last amended by the Act of Accession of Austria, Finland and Sweden, with the exception of sweet lupins until the flowering stage, from the scope of the aid for dried fodder;
Whereas, having regard to the criteria laid down in Article 8 of Regulation (EC) No 603/95, the minimum quality for the products in question, expressed in terms of moisture and protein content, should be laid down; whereas, taking account of commercial practices, the moisture content should be differentiated according to certain production processes;
Whereas Article 12 of Regulation (EC) No 603/95 lays down that Member States must carry out checks to verify that all undertakings and buyers of fodder for drying satisfy the conditions set out in that Regulation; whereas, in order to facilitate such checks and to ensure fulfilment of the conditions of eligibility for aid, processing undertakings and buyers of fodder for drying should be subject to an approval procedure; whereas, to the same end, the details to be included in aid applications, stock records and delivery declarations of processing undertakings should be specified; whereas the other supporting documents to be supplied should be specified;
Whereas, in order to ensure uniform application of the system of aid, the payment procedures should be defined;
Whereas, in order to facilitate the marketing of fodder for processing and to enable the competent authorities to carry out the checks necessary for verifying entitlement to aid, contracts between processing undertakings and farmers should be concluded before delivery of the raw materials and lodged with the competent authorities before a certain date to enable them to know the expected volume of production; whereas, to that end, contracts must be in writing, indicate the date of conclusion, the period of validity, the names and addresses of the contracting parties and the nature of the products to be processed and identify the agricultural parcel on which the fodder to be processed was grown;
Whereas, where there are no such contracts, delivery declarations, subject to the conditions applicable to contracts, must be drawn up by the processing undertaking;
Whereas, in order to verify that the quantities of raw materials delivered to undertakings tally with the quantities of dried fodder leaving them, the said undertakings should weigh all fodder for processing and determine its moisture content;
Whereas, in order to permit effective verification of the right to aid, in view of the different levels of aid provided for artificially dried fodder and sun-dried fodder, it is essential that processing undertakings manufacture and stock the different products in separate premises;
Whereas rigorous checks must be carried out to ensure fulfilment of the quality requirements for dried fodder based on regular sampling of finished products leaving undertakings; whereas, where those products are mixed with other materials, samples must be taken before mixing;
Whereas Regulation (EC) No 603/95 provides for a series of checks to be carried out at each stage of the production process, inter alia linked to the integrated verification and management system; whereas, therefore, checks on the identification of the agricultural parcels concerned should be coordinated with the checks carried out under that system;
Whereas, in order to ensure efficient management of the market for dried fodder, the Commission must receive certain information on a regular basis;
Whereas, in order to ensure fulfilment of the conditions laid down in the relevant rules, in particular regarding eligibility for aid, penalties should be laid down to prevent any infringements;
Whereas Commission Regulation (EEC) No 1528/78 (1), as last amended by Regulation (EEC) No 1069/93 (2), and Commission Regulation (EEC) No 2743/78 (3) are replaced by this Regulation; whereas they should therefore be repealed;
Whereas Regulation (EC) No 603/95 applies as from 1 April 1995, the date on which the marketing year 1995/96 begins; whereas this Regulation should therefore apply as from the same date;
Whereas the Joint Management Committee for Cereals, Oils and Fats and Dried Fodder has not delivered an opinion within the time limit set by its chairman,
HAS ADOPTED THIS REGULATION:
Article 1
This Regulation lays down the detailed rules for the application of the common organization of the market in dried fodder set up by Regulation (EC) No 603/95.
Article 2
For the purposes of this Regulation:
1. 'dried fodder` means the products referred to in Article 1 of Regulation (EC) No 603/95, drawing a distinction between:
(a) 'dehydrated fodder` which means the products referred to in the first and third indents of Article 1 (a) of Regulation (EC) No 603/95, artificially heat-dried, with the exception of all products listed in Annex I of Regulation (EEC) No 1765/92 and their forage products, with the exception of sweet lupin until the flowering stage;
(b) 'sun-dried fodder` which means the products referred to in the second and fourth indents of Article 1 (a) of Regulation (EC) No 603/95, otherwise dried and ground;
(c) 'protein concentrates` which means the products referred to in the first indent of Article 1 (b) of Regulation (EC) No 603/95;
(d) 'dehydrated products` which means the products as referred to in the second indent of Article 1 (b) of Regulation (EC) No 603/95;
2. 'processing undertaking` means any dried fodder processing undertaking referred to in Article 6 of Regulation (EC) No 603/95, duly approved by the Member State in which it is located, where:
(a) either fresh fodder is dehydrated by means of a drier which satisfies the following requirements:
- air temperature at the entry point not less than 93 °C,
- residence time of the fodder being dehydrated not exceeding three hours,
- in case of drying in layers of fodder, bed depth of each layer no deeper than one metre;
(b) or sun-dried fodder is milled;
(c) or protein concentrates are manufactured;
3. 'purchaser of fodder for drying and for grinding` means a natural or legal person referred to in the third indent of Article 9 (c) of Regulation (EC) No 603/95, duly approved by the Member State where he is established, who purchases fresh fodder from producers in order to deliver it to processing undertakings;
4. 'lot` means a specific quantity of fodder of uniform quality as regards composition, moisture content and protein content, leaving a processing undertaking at the same time.
Article 3
1. For the purposes of this Regulation, in order to obtain entitlement to the aid referred to in Article 3 of Regulation (EC) No 603/95, the products referred to in Article 2 (1) shall be considered to have left the processing undertaking where:
(a) they leave in the unaltered state:
- the precincts of the processing undertaking,
- where the dried fodder cannot be stored in those precincts, any storage place outside them offering adequate safeguards for the purposes of checking the fodder stored and which has been approved in advance by the competent authority,
- in the case of mobile drying equipment, the equipment in which the dehydration is carried out and, if the dehydrated fodder is stored by the person having dried it, any storage place which satisfies the conditions of the second indent; or (b) they leave the precincts or any storage place referred to at (a) having been mixed within the processing undertaking with raw materials, other than those specified in Article 1 of Regulation (EC) No 603/95 and those used for binding, with a view to the manufacture of compound feed, and, at the time they leave the processing undertaking, are of sound and fair merchantable quality meeting the requirements for being placed on the market as feedingstuffs and have the following characteristics:
(i) maximum moisture content:
- 12 % for sun-dried fodder, dehydrated fodder having undergone a grinding procedure, protein concentrates and dehydrated products,
- 14 % for other dehydrated fodder;
(ii) minimum crude protein content in comparison to dry matter:
- 15 % for dehydrated fodder, sun-dried fodder and dehydrated products,
- 45 % for protein concentrates.
2. Dried fodder which has left a processing undertaking may not be readmitted within the precincts of the same or any other undertaking or any storage place referred to in paragraph 1 (a).
However, during the 1995/96 marketing year, dried fodder leaving an undertaking may be admitted to another undertaking on the condition that that operation takes place under the supervision of, and under the conditions fixed by, the competent authority of the Member States concerned.
Article 4
1. For the purposes of approval referred to in Article 2 (2), the processing undertaking:
(a) must possess the technical equipment needed to carry out the operations referred to in Article 2 (2) (a), (b) or (c);
(b) must comply with:
- the conditions laid down in Regulation (EC) No 603/95,
- the conditions laid down in this Regulation.
The processing undertaking shall have its approval withdrawn for a period to be specified by the competent authority, commensurate with the seriousness of the infringements found, if at least one of the conditions referred to at (a) and (b) of the first subparagraph is no longer met.
2. For the purposes of approval as referred to in Article 2 (3), a purchaser of fodder for drying and grinding must:
- lodge with the competent authority, not later than the date provided for in Article 8 (5), the contracts concluded with producers together with a list of all the agricultural parcels concerned,
- keep a register of the products in question, showing at least the daily purchases and sales of each product and, in respect of each lot, the quantity involved, a reference to the contract with the producer who delivered the product and, where applicable, the processing undertaking for which the product is intended,
- keep his financial accounts available for inspection by the competent authority,
- facilitate the verification procedures.
A purchaser of fodder for drying and grinding shall have his approval withdrawn for a period to be specified by the competent authority, commensurate with the seriousness of the infringements found, if at least one of the conditions referred to in the first subparagraph is no longer met.
Article 5
1. In order to qualify for the aid referred to in Article 3 of Regulation (EC) No 603/95, processing undertakings must submit an aid application within 45 days following the month during which the dried fodder left the undertaking.
However, no aid applications for a marketing year may be submitted after 15 April following the end of the marketing year in question.
Except in cases of force majeure, late submission of an application shall result in a reduction of 1 % per working day of the amount of aid covered by the application to which the undertaking would have been entitled if it had submitted its application in time. Applications submitted more than 20 days late shall not be accepted.
2. Applications for aid shall contain at least the following:
- the name, forename, address and signature of the applicant,
- particulars of the quantities in respect of which application for aid is made, broken down by lot,
- the date at which each quantity left the undertaking,
- an indication that samples were taken from each lot, in accordance with Article 11 (3), at the time it left the undertaking, together with all the information needed to identify the samples.
3. The aid to be granted for mixtures containing both dried fodder and raw materials other than those referred to in Article 1 of Regulation (EC) No 603/95 and for dried fodder containing binding matter shall be calculated on a pro rata basis for the quantities of dried fodder contained in those products.
Article 6
1. The advance payment provided for in Article 6 (1) of Regulation (EC) No 603/95 shall be paid once the competent authority has ascertained entitlement to the aid for the quantities covered by the aid application and not later than 90 days after the date on which the application was submitted. The advance shall be paid to the processing undertaking for the dried fodder which left it during a given month.
2. The balance provided for in Article 6 (3) of Regulation (EC) No 603/95 shall be paid within 60 days of the date of publication by the Commission in the Official Journal of the European Communities of the amount of the balance, on the basis of the communications received from the Member States indicating the total quantity of dried fodder qualifying for aid during the marketing year in question.
Article 7
The operative event for the agricultural conversion rate applicable for the purposes of Regulation (EC) No 603/95 shall occur on the date on which the dried fodder leaves the processing undertaking.
Article 8
1. In addition to the information specified in Article 11 of Regulation (EC) No 603/95, each contract as provided for in Article 9 (c) of that Regulation shall include, in particular:
(a) the names, forenames and addresses of the contracting parties;
(b) the date on which it is concluded;
(c) the term for which it is concluded;
(d) the type or types of crop to be processed and the foreseeable quantity involved;
(e) identification of the parcel or parcels on which the fodder to be processed was grown, in accordance with the parcel identification system provided for in the integrated management and verification system; and (f) where a processing undertaking is executing a processing contract as provided for in Article 11 (2) of Regulation (EC) No 603/95 concluded with an independent producer or with one or more of its own members, the contract shall also indicate:
- the finished product to be delivered,
- the costs to be paid by the producer.
2. Where an undertaking processes its own production or, in the case of a group, that of its members, a delivery declaration shall be drawn up giving at least the following information:
- the date of delivery or, where appropriate, an indicative date if delivery is to take place after the date on which the declaration is submitted to the competent authority,
- the quantity of fodder received or to be received,
- the type or types of crop to be processed,
- where applicable, the name and address of the member of the group making the delivery, and - identification of the parcel or parcels on which the fodder to be processed was grown, in accordance with the parcel identification system provided for in the integrated management and verification system.
3. Where an undertaking obtains supplies from an approval purchaser, a delivery declaration shall be drawn up giving at least the following information:
- the identity of the approved purchaser,
- the date of delivery or, where appropriate, an indicative date if delivery is to take place after the date on which the declaration is submitted to the competent authority,
- the quantity of fodder received or to be received, broken down according to the producers with whom contracts were concluded by the approved purchaser, specifying the reference of the contracts,
- the type or types of crop to be processed,
- identification of the parcel or parcels on which the fodder to be processed was grown, in accordance with the parcel identification system provided for in the integrated management and verification system.
4. The contracts referred to in paragraph 1 shall be concluded in writing at least 15 days before the date of delivery and not later than 31 July following the beginning of the marketing year in question.
5. Not later than 31 August following the beginning of the marketing year in question, processing undertakings shall submit to the competent authority a copy of the contracts referred to in paragraph 1 and a copy of the delivery declarations referred to in paragraphs 2 and 3, with a list of the parcels concerned.
Except in cases of force majeure, late submission of the abovementioned documents shall result in a reduction of 1 % per working day of the total amount of aid to which the undertaking would be entitled if it had submitted them in time. Where the delay exceeds 20 days, undertakings shall lose their entitlement to the aid.
Article 9
Processing undertakings shall determine, for fodder to be dehydrated and, where applicable, for sun-dried fodder delivered to them for processing, the quantities delivered, measured by systematic weighing. However, this provision shall not apply where the fodder to be dehydrated is processed in a mobile dehydration unit. In such cases only, the delivered quantities may be estimated on the basis of the areas sown.
The average moisture content of the quantities of fodder to be dehydrated shall be measured by comparing the quantities used and the quantities of finished product obtained.
Article 10
Where a processing undertaking carries out the manufacture, on the one hand, of dehydrated fodder and/or protein concentrates and, on the other hand, of sun-dried fodder:
- the dehydrated fodder must be manufactured in premises or places separate from those where sun-dried fodder is manufactured,
- products obtained from the two manufacturing operations must be stored in different places,
- it shall be prohibited to mix within the undertaking a product belonging to one of the groups with a product belonging to the other group.
Article 11
The sampling and determination of the weight of dried fodder referred to in Article 12 (2) of Regulation (EC) No 603/95 shall be carried out by the processing undertaking when the dried fodder leaves the undertaking.
However, where the dried fodder is mixed, on the premises of the processing undertaking, with raw materials other than those referred to in Article 1 of Regulation (EC) No 603/95, sampling shall be carried out before mixing takes place.
2. The competent authority may require each processing undertaking to notify it at least two working days in advance each time dried fodder leaves the undertaking or is mixed, specifying the dates and quantities, to enable the latter to carry out the necessary checks.
The competent authority shall regularly take samples relating to at least 5 % of the volume of dried fodder leaving the undertaking and at least 5 % of the volume of dried fodder mixed on its premises with raw materials other than those referred to in Article 1 of Regulation (EC) No 603/95 each marketing year.
3. The determination of moisture and crude protein content provided for in Article 3 shall be carried out by taking a sample for every 100 tonnes (maximum) of each lot of dried fodder leaving the processing undertaking or mixed on its premises with raw materials other than those referred to in Article 1 of Regulation (EC) No 603/95 in accordance with the method laid down in the Community provisions establishing Community methods of analysis for the official control of feedingstuffs (1).
Where several lots of the same quality with regard to composition, moisture content and protein content, together weighing 100 tonnes or less, leave the undertaking or are mixed on its premises, a sample shall be taken from each lot. However, the analysis shall be carried out on a representative mixture of these samples.
Article 12
1. In addition to the details referred to in Article 9 (a) of Regulation (EC) No 603/95, the stock accounts of processing undertakings shall include at least details of:
- the species referred to in Article 1 of Regulation (EC) No 603/95 of fodder to be dehydrated and, where applicable, sun-dried fodder entering the undertaking,
- the moisture content of fodder to be dehydrated,
- the references of the contract and/or delivery declaration provided for in Article 8,
- the dates on which the dried fodder left the undertaking, specifying the quantities which left on each date,
- the stock of dried fodder at the end of each marketing year.
2. Processing undertakings shall keep separate stock accounts for dehydrated fodder, sundried fodder, protein concentrates and dehydrated products.
3. Where a processing undertaking also dries or processes products other than fodder referred to in Article 2 (1), it shall keep separate accounts in respect of such other drying or processing activities.
Article 13
The documentary evidence to be made available by processing undertakings to the competent authority at the latter's request shall include:
(a) in the case of all processing undertakings:
- details whereby the production capacity of the plant may be determined,
- details of the fuel stocks held in the plant at the beginning and end of production,
- invoices for the purchase of fuel and the meter readings for electricity consumption during the production period,
- details of the number of hours the driers and, in the case of sundried fodder, the crushers were in operation;
(b) in the case of processing undertakings which sell their own output, the invoices in respect of their sales of dried fodder, with details in particular of:
- the quantity and composition of the fodder sold,
- the name and address of the purchaser;
(c) in the case of undertakings processing fodder produced by their members and delivering them dried fodder, the delivery orders or any other accounting document acceptable to the competent authority, with details in particular of:
- the quantity and composition of the processed fodder delivered,
- the names of the persons to whom such fodder was delivered;
(d) in the case of undertakings processing for farmers fodder supplied by and delivered back to those farmers, invoices in respect of their processing charges, with details in particular of:
- the quantity and composition of dried fodder produced,
- the names of the farmers concerned.
Article 14
1. The competent authorities shall carry out cross-checks of the agricultural parcels referred to in the contracts and/or declarations and those declared by the producers in their area aid applications in order to prevent any aid being granted without justification.
2. The competent authorities shall inspect the stock records of all approved undertakings. They shall also carry out random checks on the financial supporting documents relating to the transactions carried out by these undertakings. Each undertaking must be inspected for this purpose at least once every marketing year.
However, in the case of newly-approved undertakings, the inspection shall cover all applications submitted during the first year of operation.
3. The competent authorities shall:
- regularly check in particular the accounts of approved undertakings,
- undertake regular additional checks on suppliers of raw materials and on operators to whom dried fodder has been supplied.
The competent authorities may also make unannounced checks of the same type as those referred to above.
4. Applications subjected to on-the-spot checking shall be selected by the competent authority on the basis of a risk analysis and an element of representativeness of the aid applications submitted.
The risk analysis shall take account, in particular, of:
- the amount of aid involved,
- changes from the previous year,
- the findings of checks made in past years,
- other factors to be defined by the Member State.
Article 15
Member States shall inform the Commission:
(a) - at the beginning of each quarter, of the quantities of dried fodder in respect of which applications for the aid referred to in Article 3 of Regulation (EC) No 603/95 were lodged during the previous quarter, such particulars to be broken down by the months in which the quantities concerned left the undertaking,
- not later than 31 May each year, of the quantities of dried fodder in respect of which entitlement to the aid was recognized during the previous marketing year.
The particulars provided shall distinguish between the products referred to respectively in Article 2 (1) (a), (b), (c) and (d) and will be used by the Commission to determine whether the maximum guaranteed quantity has been complied with;
(b) not later than 31 December each year, of the areas and quantities in respect of which the contracts and declarations referred to in Article 8 have been submitted. The particulars shall be broken down by species referred to in Article 1 of Regulation (EC) No 603/95 and shall distinguish between the cases provided for in Article 8 (1), (2) and (3);
(c) not later than 30 April of each year, of the estimated quantities of stocks of dried fodder held in the processing undertakings at 31 March of that year;
(d) not later than 1 May 1995, of the measures adopted to implement Regulation (EC) No 603/95 and this Regulation.
Article 16
Where inspections reveal that the quantity of dried fodder indicated in one or more aid applications is greater than that which actually left the processing undertaking, the amount of aid to be granted shall be calculated on the basis of the quantity which actually left the undertaking, reduced by twice the difference found.
If the difference found exceeds 20 % of the quantity which actually left the undertaking, no aid shall be granted.
However, in the case of a false indication of quantities in one or more applications made intentionally or as a result of serious recklessness:
- the processing undertaking in question shall be excluded from the aid scheme concerned as regards the application or applications in question, and - in the case of a false indication of quantities in one or more applications intentionally made, the said undertaking shall be excluded from the aid scheme for the following marketing year, in respect of a quantity equal to that for which the aid application(s) in question was or were rejected.
Article 17
Regulation (EEC) No 1528/78 and (EEC) No 2743/78 are hereby repealed. However, those provisions concerning the management of the aid scheme in force during the 1994/95 marketing year shall continue to apply until the accounts for that marketing year have been finally cleared.
Article 18
This Regulation shall enter into force of the day of its publication in the Official Journal of the European Communities.
It shall apply from 1 April 1995. However, the following provisions shall apply from 1 April 1996:
(a) Article 4 (1) providing for the approval of processing undertakings;
(b) the first sentence of Article 9 providing for the determination by systematic weighing, in the case of undertakings which have no weighing apparatus;
(c) Article 14 (1) providing for cross-checks. However, in so far as one or more elements of the integrated system are in operation before 1 January 1996, Member States shall use that or those elements in their management and verification activities and, in particular, to the extent possible, to carry out the cross-checks.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 6 April 1995.
For the Commission Franz FISCHLER Member of the Commission
(1) (a) Sampling: First Commission Directive (76/371/EEC) (OJ No L 102, 15. 4. 1976, p. 1); (b) measurement of moisture: Second Commission Directive (71/393/EEC) (OJ No L 279, 20. 12. 1971, p. 7); (c) measurement of crude protein: Third Commission Directive (72/199/EEC) (OJ No L 123, 29. 5. 1972, p. 6).
(2) OJ No L 181, 1. 7. 1992, p. 12.
(1) OJ No L 179, 1. 7. 1978, p. 10.
(2) OJ No L 108, 1. 5. 1993, p. 114.
(3) OJ No L 330, 25. 11. 1978, p. 19.
(1)
