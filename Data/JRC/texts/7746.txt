*****
COMMISSION REGULATION (EEC) No 2498/87
of 18 August 1987
amending Regulation (EEC) No 2730/81 establishing a list of agencies in non-member importing countries entitled to issue invitations to tender in the milk and milk products sector
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 804/68 of 27 June 1968 on the Common organization of the market in milk and milk products (1), as last amended by Regulation (EEC) No 773/87 (2), and in particular Articles 13 (3) and 17 (4) thereof,
Whereas Commission Regulation (EEC) No 2730/81 (3), as last amended by Regulation (EEC) No 768/87 (4), established a list of agencies in non-member importing countries entitled to issue invitations to tender in the milk and milk products sector;
Whereas, in the light of the most recent information available to the Commission on the trade practices followed by the importing countries concerned and the official nature of the agencies in question, the Regulation (EEC) No 2730/81 should be amended;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Milk and Milk Products,
HAS ADOPTED THIS REGULATION:
Article 1
In the Annex to Regulation (EEC) No 2730/81, the list of issuing organizations shall be modified with regard to India, Japan and Venezuela as follows:
1.2 // Importing country // Issuing organization // 'India // India Dairy Corporation Derpen R.C. Dutt Berode 390 005 India // // National Dairy Development Board (NDDB) Anand/Gujarat India // Japan // Livestock Industry Promotion Corporation (LIPC) Azadubai Building 2-1 Azadubai Mato Ku Tokyo // // Nikon Shiryo Kogyokai Kyodoukumiai 2-2-1 Azabudai Minatoku Tokyo // Venezuela // ASOLEP Asociacion de Productores de Leche en Polvo Colinas de la California Esq. Avdas. San Francisco y Palmarito, Edificio Indulac Caracas 1010/A // // Comandancia General de la Marina Base aéreal la Carlota, Hangar la Marina Caracas // // INDULAC Colinas de California Esq. Avdas San Francisco y Palmarito Edificio Indulac, Apartado 1546 Caracas 10107-A // // Instituto Nacional de Nutrición Operativa Avenida Barait esq. El Carmen Edificio Sede Central Caracas'.
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 18 August 1987.
For the Commission
Frans ANDRIESSEN
Vice-President
(1) OJ No L 148, 28. 6. 1968, p. 13.
(2) OJ No L 78, 20. 3. 1987, p. 1.
(3) JO No L 272, 26. 9. 1981, p. 25.
(4) OJ No L 77, 19. 3. 1987, p. 34.
