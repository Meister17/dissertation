Council Decision of 29 April 2004 on the conclusion of the Framework Agreement between the European Community and the European Space Agency (2004/578/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 170, in conjunction with the first sentence of the first subparagraph of Article 300(2) and the first subparagraph of Article 300(3) thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament(1),
Whereas:
(1) The Commission has negotiated on behalf of the Community an Agreement with the European Space Agency.
(2) The Agreement was signed on behalf of the Community on 25 November 2003 subject to its possible conclusion at a later date.
(3) This Agreement should be approved,
HAS DECIDED AS FOLLOWS:
Article 1
The Agreement between the European Community and the European Space Agency is hereby approved on behalf of the Community.
The text of the Agreement is attached to this Decision.
Article 2
The President of the Council is hereby authorised to designate the person empowered to deposit on behalf of the Community the act of approval, as provided for in Article 12(1) of the Framework Agreement, in order to express the consent of the Community to be bound.
Done at Luxembourg, 29 April 2004 .
For the Council
The President
M. Mc Dowell
(1) Opinion delivered on 20 April 2004 (not yet published in the Official Journal).
