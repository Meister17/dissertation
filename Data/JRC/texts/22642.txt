COUNCIL DECISION of 24 June 1982 on the conclusion of the Convention on the conservation of migratory species of wild animals (82/461/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 235 thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament (1),
Having regard to the opinion of the Economic and Social Committee (2),
Whereas a programme of action of the European Communities on the environment was adopted by the Declaration of 22 November 1973 (3), and supplemented by the resolution of 17 May 1977 (4) ; whereas the aim of an environment policy in the Community, as defined in these acts, is to improve the quality of life and to protect the environment;
Whereas the Council has adopted Directive 79/409/EEC on the conservation of wild birds (5);
Whereas the Community took part in the negotiations for the conclusion of the Convention on the conservation of migratory species of wild animals;
Whereas conclusion by the Community of the Convention is necessary to allow the Community to negotiate and to conclude the regional agreements provided for by that Convention to the extent that such agreements fall within the exclusive powers vested in the Community by Directive 79/409/EEC;
Whereas the conclusion of the Convention by the Community implies no extension of the exclusive powers of the Community, without prejudice to any legal acts which the Community may adopt at a later date,
Whereas, in view of the special natural situation of Greenland and of the living conditions of its population, Greenland should be excluded from the scope of the Convention,
HAS DECIDED AS FOLLOWS:
Article 1
The Convention on the conservation of migratory species of wild animals is hereby approved on behalf of the European Economic Community.
The text of the Convention is annexed to this Decision.
Article 2
The President of the Council shall deposit the instrument of accession provided for by Article XVII of the Convention for the territories to which the Treaty establishing the European Economic Community applies, under the conditions laid down in that Treaty, with the exception of Greenland.
Done at Luxembourg, 24 June 1982.
For the Council
The President
F. AERTS (1) OJ No C 327, 14.12.1981, p. 95. (2) OJ No C 300, 18.11.1980, p. 15. (3) OJ No C 112, 20.12.1973, p. 1. (4) OJ No C 139, 13.6.1977, p. 1. (5) OJ No L 103, 25.4.1979, p. 1.
CONVENTION ON THE CONSERVATION OF MIGRATORY SPECIES OF WILD ANIMALS
THE CONTRACTING PARTIES,
RECOGNIZING that wild animals in their innumerable forms are an irreplaceable part of the earth's natural system which must be conserved for the good of mankind;
AWARE that each generation of man holds the resources of the earth for future generations and has an obligation to ensure that this legacy is conserved and, where utilized, is used wisely;
CONSCIOUS of the ever-growing value of wild animals from environmental, ecological, genetic, scientific, aesthetic, recreational, cultural, educational, social and economic points of view;
CONCERNED particularly with those species of wild animals that migrate across or outside national jurisdictional boundaries;
RECOGNIZING that the States are and must be the protectors of the migratory species of wild animals that live within or pass through their national jurisdictional boundaries;
CONVINCED that conservation and effective management of migratory species of wild animals require the concerted action of all States within the national jurisdictional boundaries of which such species spend any part of their life cycle;
RECALLING Recommendation 32 of the Action Plan adopted by the United Nations Conference on the Human Environment (Stockholm, 1972) and noted with satisfaction by the Twenty-seventh Session of the General Assembly of the United Nations,
HAVE AGREED AS FOLLOWS:
Article I
Interpretation
1. For the purpose of this Convention: (a) "migratory species" means the entire population or any geographically separate part of the population of any species or lower taxon of wild animals, a significant proportion of whose menbers cyclically and predictably cross one or more national jurisdictional boundaries;
(b) "conservation status of a migratory species" means the sum of the influences acting on the migratory species that may affect its long-term distribution and abundance;
(c) "conservation status" will be taken as "favourable" when: 1. population dynamics data indicate that the migratory species is maintaining itself on a long-term basis as a viable component of its ecosystems;
2. the range of the migratory species is neither currently being reduced, nor is likely to be reduced, on a long-term basis;
3. there is, and will be in the foreseeable future, sufficient habitat to maintain the population of the migratory species on a long-term basis ; and
4. the distribution and abundance of the migratory species approach historic coverage and levels to the extent that potentially suitable ecosystems exist and to the extent consistent with wise wildlife management;
(d) "conservation status" will be taken as "unfavourable" if any of the conditions set out in subparagraph (c) of this paragraph is not met;
(e) "endangered" in relation to a particular migratory species means that the migratory species is in danger of extinction throughout all or a significant portion of its range;
(f) "range" means all the areas of land or water that a migratory species inhabits, stays in temporarily, crosses or overflies at any time on its normal migration route;
(g) "habitat" means any area in the range of a migratory species which contains suitable living conditions for that species;
(h) "Range State" in relation to a particular migratory species means any State (and where appropriate any other party referred to under subparagraph (k) of this paragraph) that exercises jurisdiction over any part of the range of that migratory species, or a State, flag vessels of which are engaged outside national jurisdictional limits in taking that migratory species;
(i) "taking" means taking, hunting, fishing, capturing, harassing, deliberate killing, or attempting to engage in any such conduct;
(j) "Agreement" means an international agreement relating to the conservation of one or more migratory species as provided for in Articles IV and V of this Convention ; and
(k) "Party" means a State or any regional economic integration organization constituted by sovereign States which has competence in respect of the negotiation, conclusion and application of international agreements in matters covered by this Convention for which this Convention is in force.
2. In matters within their competence, the regional economic integration organizations which are Parties to this Convention shall in their own name exercise the rights and fulfil the responsibilities which this Convention attributes to their member States. In such cases the member States of these organizations shall not be entitled to exercise such rights individually.
3. Where this Convention provides for a decision to be taken by either a two-thirds majority or a unanimous decision of "the Parties present and voting" this shall mean "the Parties present and casting an affirmative or negative vote". Those abstaining from voting shall not be counted amongst "the Parties present and voting" in determining the majority.
Article II
Fundamental principles
1. The Parties acknowledge the importance of migratory species being conserved and of Range States agreeing to take action to this end whenever possible and appropriate, paying special attention to migratory species the conservation status of which is unfavourable, and taking individually or in cooperation appropriate and necessary steps to conserve such species and their habitat.
2. The Parties acknowledge the need to take action to avoid any migratory species becoming endangered.
3. In particular, the Parties: (a) should promote, cooperate in our support research relating to migratory species;
(b) shall endeavour to provide immediate protection for migratory species included in Appendix I ; and
(c) shall endeavour to conclude Agreements covering the conservation and management of migratory species included in Appendix II.
Article III
Endangered migratory species : Appendix I
1. Appendix I shall list migratory species which are endangered.
2. A migratory species may be listed in Appendix I provided that reliable evidence, including the best scientific evidence available, indicates that the species is endangered.
3. A migratory species may be removed from Appendix I when the Conference of the Parties determines that: (a) reliable evidence, including the best scientific evidence available, indicates that the species is no longer endangered, and
(b) the species is not likely to become endangered again because of loss of protection due to its removal from Appendix I.
4. Parties that are Range States of a migratory species listed in Appendix I shall endeavour: (a) to conserve and, where feasible and appropriate, restore those habitats of the species which are of importance in removing the species from danger of extinction;
(b) to prevent, remove, compensate for or minimize, as appropriate, the adverse effects of activities or obstacles that seriously impede or prevent the migration of the species ; and
(c) to the extent feasible and appropriate, to prevent, reduce or control factors that are endangering or are likely to further endanger the species, including strictly controlling the introduction of, or controlling or eliminating already introduced, exotic species.
5. Parties that are Range States of a migratory species listed in Appendix I shall prohibit the taking of animals belonging to such species. Exceptions may be made to this prohibition only if: (a) the taking is for scientific purposes;
(b) the taking is for the purpose of enhancing the propagation or survival of the affected species;
(c) the taking is to accommodate the needs of traditional subsistence users of such species ; or
(d) extraordinary circumstances so require :
provided that such exceptions are precise as to content and limited in space and time. Such taking should not operate to the disadvantage of the species.
6. The Conference of the Parties may recommend to the Parties that are Range States of a migratory species listed in Appendix I that they take further measures considered appropriate to benefit the species.
7. The Parties shall as soon as possible inform the Secretariat of any exceptions made pursuant to paragraph 5 of this Article.
Article IV
Migratory species to be the subject of Agreements : Appendix II
1. Appendix II shall list migratory species which have an unfavourable conservation status and which require international agreements for their conservation and management, as well as those which have a conservation status which would significantly benefit from the international cooperation that could be achieved by an international agreement.
2. If the circumstances so warrant, a migratory species may be listed both in Appendix I and Appendix II.
3. Parties that are Range States of migratory species listed in Appendix II shall endeavour to conclude Agreements where these would benefit the species and should give priority to those species in an unfavourable conservation status.
4. Parties are encouraged to take action with a view to concluding agreements for any population or any geographically separate part of the population of any species or lower taxon of wild animals, members of which periodically cross one or more national jurisdictional boundaries.
5. The Secretariat shall be provided with a copy of each Agreement concluded pursuant to the provision of this Article.
Article V
Guidelines for Agreements
1. The object of each Agreement shall be to restore the migratory species concerned to a favourable conservation status or to maintain it in such a status. Each Agreement should deal with those aspects of the conservation and management of the migratory species concerned which serve to achieve that object.
2. Each Agreement should cover the whole of the range of the migratory species concerned and should be open to accession by all Range States of that species, whether or not they are Parties to this Convention.
3. An Agreement should, wherever possible, deal with more than one migratory species.
4. Each Agreement should: (a) identify the migratory species covered;
(b) describe the range and migration route of the migratory species;
(c) provide for each Party to designate its national authority concerned with the implementation of the Agreement;
(d) establish, if necessary, appropriate machinery to assist in carrying out the aims of the Agreement, to monitor its effectiveness, and to prepare reports for the Conference of the Parties;
(e) provide for procedures for the settlement of disputes between Parties to the Agreement ; and
(f) at a minimum, prohibit, in relation to a migratory species of the Order Cetacea, any taking that is not permitted for that migratory species under any other multilateral agreement and provide for accession to the Agreement by States that are not Range States of that migratory species.
5. Where appropriate and feasible, each Agreement should provide for, but not be limited to: (a) periodic review of the conservation status of the migratory species concerned and the identification of the factors which may be harmful to that status;
(b) coordinated conservation and management plans;
(c) research into the ecology and population dynamics of the migratory species concerned, with special regard to migration;
(d) the exchange of information on the migratory species concerned, special regard being paid to the exchange of the results of research and of relevant statistics;
(e) conservation and, where required and feasible, restoration of the habitats of importance in maintaining a favourable conservation status, and protection of such habitats from disturbances, including strict control of the introduction of, or control of already introduced, exotic species detrimental to the migratory species;
(f) maintenance of a network of suitable habitats appropriately disposed in relation to the migration routes;
(g) where it appears desirable, the provision of new habitats favourable to the migratory species or reintroduction of the migratory species into favourable habitats;
(h) elimination of, to the maximum extent possible, or compensation for activities and obstacles which hinder or impede migration;
(i) prevention, reduction or control of the release into the habitat of the migratory species of substances harmful to that migratory species;
(j) measures based on sound ecological principles to control and manage the taking of the migratory species;
(k) procedures for coordinating action to suppress illegal taking;
(l) exchange of information on substantial threats to the migratory species;
(m) emergency procedures whereby conservation action would be considerably and rapidly strengthened when the conservation status of the migratory species is seriously affected ; and
(n) making the general public aware of the contents and aims of the Agreement.
Article VI
Range States
1. A list of the Range States of migratory species listed in Appendices I and II shall be kept up to date by the Secretariat using information it has received from the Parties.
2. The Parties shall keep the Secretariat informed as to which of the migratory species listed in Appendices I and II they consider they are Range States ; including provision of information on their flag vessels engaged outside national jurisdictional limits in taking the migratory species concerned and where possible future plans in respect of such taking.
3. The Parties which are Range States for migratory species listed in Appendix I or Appendix II should inform the Conference of the Parties through the Secretariat, at least six months prior to each ordinary meeting of the Conference, on measures that they are taking to implement the provisions of this Convention for these species.
Article VII
The Conference of the Parties
1. The Conference of the Parties shall be the decision-making organ of this Convention.
2. The Secretariat shall call a meeting of the Conference of the Parties not later than two years after the entry into force of this Convention.
3. Thereafter the Secretariat shall convene ordinary meetings of the Conference of the Parties at intervals of not more than three years, unless the Conference decides otherwise, and extraordinary meetings at any time on the written request of at least one-third of the Parties.
4. The Conference of the Parties shall establish and keep under review the financial regulations of this Convention. The Conference of the Parties shall, at each of its ordinary meetings, adopt the budget for the next financial period. Each Party shall contribute to this budget according to a scale to be agreed upon by the Conference. Financial regulations, including the provisions on the budget and the scale of contributions as well as their modifications, shall be adopted by unanimous vote of the Parties present and voting.
5. At each of its meetings the Conference of the Parties shall review the implementation of this Convention and may in particular: (a) review and assess the conservation status of migratory species;
(b) review the progress made toward the conservation of migratory species, especially those listed in Appendices I and II;
(c) make such provision and provide such guidance as may be necessary to enable the Scientific Council and the Secretariat to carry out their duties;
(d) receive and consider any reports presented by the Scientific Council, the Secretariat, any Party or any standing body established pursuant to an Agreement;
(e) make recommendations to the Parties for improving the conservation status of migratory species and review the progress being made under Agreements;
(f) in those cases where an Agreement has not been concluded, make recommendations for the convening of meetings of the Parties that are Range States of a migratory species or group of migratory species to discuss measures to improve the conservation status of the species;
(g) make recommendations to the Parties for improving the effectiveness of this Convention ; and
(h) decide on any additional measure that should be taken to implement the objects of this Convention.
6. Each meeting of the Conference of the Parties should determine the time and venue of the next meeting.
7. Any meeting of the Conference of the Parties shall determine and adopt rules of procedure for that meeting. Decisions at a meeting of the Conference of the Parties shall require a two-thirds majority of the Parties present and voting, except where otherwise provided for by this Convention.
8. The United Nations, its Specialized Agencies, the International Atomic Energy Agency, as well as any State not a party to this Convention and, for each Agreement, the body designated by the parties to that Agreement may be represented by observers at meetings of the Conference of the Parties.
9. Any agency or body technically qualified in protection, conservation and management of migratory species, in the following categories, which has informed the Secretariat of its desire to be represented at meetings of the Conference of the Parties by observers, shall be admitted unless at least one-third of the Parties present object: (a) international agencies or bodies, either governmental or non-governmental, and national governmental agencies and bodies ; and
(b) national non-governmental agencies or bodies which have been approved for this purpose by the State in which they are located.
Once admitted, these observers shall have the right to participate but not to vote.
Article VIII
The Scientific Council
1. At its first meeting, the Conference of the Parties shall establish a Scientific Council to provide advice on scientific matters.
2. Any Party may appoint a qualified expert as a member of the Scientific Council. In addition, the Scientific Council shall include as members qualified experts selected and appointed by the Conference of the Parties ; the number of these experts, the criteria for their selection and the terms of their appointments shall be as determined by the Conference of the Parties.
3. The Scientific Council shall meet at the request of the Secretariat as required by the Conference of the Parties.
4. Subject to the approval of the Conference of the Parties, the Scientific Council shall establish its own rules of procedure.
5. The Conference of the Parties shall determine the functions of the Scientific Council, which may include: (a) providing scientific advice to the Conference of the Parties, to the Secretariat, and, if approved by the Conference of the Parties, to any body set up under this Convention or an Agreement or to any Party;
(b) recommending research and the coordination of research on migratory species, evaluating the results of such research in order to ascertain the conservation status of migratory species and reporting to the Conference of the Parties on such status and measures for its improvement;
(c) making recommendations to the Conference of the Parties as to the migratory species to be included in Appendices I or II, together with an indication of the range of such migratory species;
(d) making recommendations to the Conference of the Parties as to specific conservation and management measures to be included in Agreements on migratory species ; and
(e) recommending to the Conference of the Parties solutions to problems relating to the scientific aspects of the implementation of this Convention, in particular with regard to the habitats of migratory species.
Article IX
The Secretariat
1. For the purposes of this Convention, a Secretariat shall be established.
2. Upon entry into force of this Convention, the Secretariat is provided by the Executive Director of the United Nations Environment Programme. To the extent and in the manner he considers appropriate, he may be assisted by suitable inter-governmental or non-governmental, international or national agencies and bodies technically qualified in protection, conservation and management of wild animals.
3. If the United Nations Environment Programme is no longer able to provide the Secretariat, the Conference of the Parties shall make alternative arrangements for the Secretariat.
4. The functions of the Secretariat shall be: (a) to arrange for and service meetings: (i) of the Conference of the Parties, and
(ii) the Scientific Council;
(b) to maintain liaison with and promote liaison between the Parties, the standing bodies set up under Agreements and other international organizations concerned with migratory species;
(c) to obtain from any appropriate source reports and other information which will further the objects and implementation of this Convention and to arrange for the appropriate dissemination of such information;
(d) to invite the attention of the Conference of the Parties to any matter pertaining to the objectives of this Convention;
(e) to prepare for the Conference of the Parties reports on the work of the Secretariat and on the implementation of this Convention;
(f) to maintain and publish a list of Range States of all migratory species included in Appendices I and II;
(g) to promote, under the direction of the Conference of the Parties, the conclusion of Agreements;
(h) to maintain and make available to the Parties a list of Agreements and, if so required by the Conference of the Parties, to provide any information on such Agreements;
(i) to maintain and publish a list of the recommendations made by the Conference of the Parties pursuant to subparagraphs (e), (f) and (g) of paragraph 5 of Article VII or of decisions made pursuant to subparagraph (h) of that paragraph;
(j) to provide for the general public information concerning this Convention and its objectives ; and
(k) to perform any other function entrusted to it under this Convention or by the Conference of the Parties.
Article X
Amendment of the Convention
1. This Convention may be amended at any ordinary or extraordinary meeting of the Conference of the Parties.
2. Proposals for amendment may be made by any Party.
3. The text of any proposed amendment and the reasons for it shall be communicated to the Secretariat at least one hundred and fifty days before the meeting at which it is to be considered and shall promptly be communicated by the Secretariat to all Parties. Any comments on the text by the Parties shall be communicated to the Secretariat not less than sixty days before the meeting begins. The Secretariat shall, immediately after the last day for submission of comments, communicate to the Parties all comments submitted by that day.
4. Amendments shall be adopted by a two-thirds majority of Parties present and voting.
5. An amendment adopted shall enter into force for all Parties which have accepted it on the first day of the third month following the date on which two-thirds of the Parties have deposited an instrument of acceptance with the Depositary. For each Party which deposits an instrument of acceptance after the date on which two-thirds of the Parties have deposited an instrument on acceptance, the amendment shall enter into force for that Party on the first day of the third month following the deposit of its instrument of acceptance.
Article XI
Amendment of the Appendices
1. Appendices I and II may be amended at any ordinary or extraordinary meeting of the Conference of the Parties.
2. Proposals for amendment may be made by any Party.
3. The text of any proposed amendment and the reasons for it, based on the best scientific evidence available, shall be communicated to the Secretariat at least one hundred and fifty days before the meeting and shall promptly be communicated by the Secretariat to all Parties. Any comments on the text by the Parties shall be communicated to the Secretariat not less than sixty days before the meeting begins. The Secretariat shall, immediately after the last day for submission of comments, communicate to the Parties all comments submitted by that day.
4. Amendments shall be adopted by a two-thirds majority of Parties present and voting.
5. An amendment to the Appendices shall enter into force for all Parties ninety days after the meeting of the Conference of the Parties at which it was adopted, except for those Parties which make a reservation in accordance with paragraph 6 of this Article.
6. During the period of ninety days provided for in paragraph 5 of this Article, any Party may by notification in writing to the Depositary make a reservation with respect to the amendment. A reservation to an amendment may be withdrawn by written notification to the Depositary and thereupon the amendment shall enter into force for that Party ninety days after the reservation is withdrawn.
Article XII
Effect on international conventions and other legislation
1. Nothing in this Convention shall prejudice the codification and development of the law of the sea by the United Nations Conference on the Law of the Sea convened pursuant to Resolution 2750 C (XXV) of the General Assembly of the United Nations nor the present or future claims and legal views of any State concerning the law of the sea and the nature and extent of coastal and flag State jurisdiction.
2. The provisions of this Convention shall in no way affect the rights or obligations of any Party deriving from any existing treaty, convention or agreement.
3. The provisions of this Convention shall in no way affect the right of Parties to adopt stricter domestic measures concerning the conservation of migratory species listed in Appendices I and II or to adopt domestic measures concerning the conservation of species not listed in Appendices I and II.
Article XIII
Settlement of disputes
1. Any dispute which may arise between two or more Parties with respect to the interpretation or application of the provisions of this Convention shall be subject to negotiation between the Parties involved in the dispute.
2. If the dispute cannot be resolved in accordance with paragraph 1 of this Article, the Parties may, by mutual consent, submit the dispute to arbitration, in particular that of the Permanent Court of Arbitration at The Hague, and the Parties submitting the dispute shall be bound by the arbitral decision.
Article XIV
Reservations
1. The provisions of this Convention shall not be subject to general reservations. Specific reservations may be entered in accordance with the provisions of this Article and Article XI.
2. Any State or any regional economic integration organization may, on depositing its instrument of ratification, acceptance, approval or accession, enter a specific reservation with regard to the presence on either Appendix I or Appendix II or both, of any migratory species and shall then not be regarded as a Party in regard to the subject of that reservation until ninety days after the Depositary has transmitted to the Parties notification that such reservation has been withdrawn.
Article XV
Signature
This Convention shall be open for signature at Bonn for all States and any regional economic integration organization until the twenty-second day of June, 1980.
Article XVI
Ratification, acceptance, approval
This Convention shall be subject to ratification, acceptance or approval. Instruments of ratification, acceptance or approval shall be deposited with the Government of the Federal Republic of Germany, which shall be the Depositary.
Article XVII
Accession
After the twenty-second day of June 1980 this Convention shall be open for accession by all non-signatory States and any regional economic integration organization. Instruments of accession shall be deposited with the Depositary.
Article XVIII
Entry into force
1. This Convention shall enter into force on the first day of the third month following the date of deposit of the fifteenth instrument of ratification, acceptance, approval or accession with the Depositary.
2. For each State or each regional economic integration organization which ratifies, accepts or approves this Convention or accedes thereto after the deposit of the fifteenth instrument of ratification, acceptance, approval or accession, this Convention shall enter into force on the first day of the third month following the deposit by such State or such organization of this instrument of ratification, acceptance, approval or accession.
Article XIX
Denunciation
Any Party may denounce this Convention by written notification to the Depositary at any time. The denunciation shall take effect twelve months after the Depositary has received the notification.
Article XX
Depositary
1. The original of this Convention, in the English, French, German, Russian and Spanish languages, each version being equally authentic, shall be deposited with the Depositary. The Depositary shall transmit certified copies of each of these versions to all States and all regional economic integration organizations that have signed the Convention or deposited instruments of accession to it.
2. The Depositary shall, after consultation with the governments concerned, prepare official versions of the text of this Convention in the Arabic and Chinese languages.
3. The Depositary shall inform all signatory and acceding States and all signatory and acceding regional economic integration organizations and the Secretariat of signatures, deposit of instruments of ratification, acceptance, approval or accession, entry into force of this Convention, amendments thereto, specific reservations and notifications of denunciation.
4. As soon as this Convention enters into force, a certified copy thereof shall be transmitted by the Depositary to the Secretariat of the United Nations for registration and publication in accordance with Article 102 of the Charter of the United Nations.
IN WITNESS WHEREOF the undersigned, being duly authorized to that effect, have signed this Convention.
DONE at Bonn, 23 June 1979.
APPENDIX I
INTERPRETATION
1. Migratory species included in this Appendix are referred to: (a) by the name of the species or subspecies ; or
(b) as being all of the migratory species included in a higher taxon or designated part thereof.
2. Other references to taxa higher than species are for the purposes of information or classification only.
3. The abbreviation "(s.I.)" is to used denote that the scientific name is used in its extended meaning.
4. The symbol (-) followed by a number placed against the name of a taxon indicates the exclusion from that taxon of designated geographically separate populations as follows:
- 101 Peruvian populations.
5. The symbol (+) followed by a number placed against the name of a species denotes that only designated geographically separate populations of that species are included in this Appendix, as follows:
+ 201 Northwest African populations
+ 202 African populations
+ 203 Upper Amazon populations.
6. An asterisk (*) placed against the name of a species indicates that the species or a separate population of that species or a higher taxon which includes that species, is included in Appendix II.
>PIC FILE= "T0021843"> >PIC FILE= "T0021844">
APPENDIX II
INTERPRETATION
1. Migratory species included in this Appendix are referred to: (a) by the name of the species or subspecies ; or
(b) as being all of the migratory species included in a higher taxon or designated part thereof.
Unless otherwise indicated, where reference is made to a taxon higher than species, it is understood that all the migratory species within that taxon could significantly benefit from the conclusion of Agreements.
2. The abbreviation "spp." following the name of a family or genus is used to denote all migratory species within that family or genus.
3. Other references to taxa higher than species are for purposes of information or classification only.
4. The abbreviation "(s.I.)" is used to denote that the scientific name is used in its extended meaning.
5. The symbol (+) followed by a number placed against the name of a species or higher taxon denotes that only designated geographically separate populations of that taxon are included in this Appendix as follows:
+ 201 Asian populations.
6. As asterisk (*) placed against the name of a species or higher taxon indicates that the species or a separate population of that species or one or more species included in that higher taxon, are included in Appendix I.
>PIC FILE= "T0021845"> >PIC FILE= "T0021846">
