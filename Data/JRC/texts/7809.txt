COMMISSION REGULATION (EEC) N° 4005/87
of 23 December 1987
amending Regulation (EEC) N° 845/72 laying down special measures to encourage silkworm rearing
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) N° 2658/87 of 23 July 1987 on the tariff and statistical nomenclature and on the Common Customs Tariff (1), as amended by Regulation (EEC) N° 3985/87 (2), and in particular Article 15 thereof,
Whereas Council Regulation (EEC) N° 2658/87 establishes, with effect from 1 January 1988, a combined goods nomenclature based on the Harmonized System which will meet the requirements both of the Common Customs Tariff and the nomenclature of goods for the external trade statistics of the Community;
Whereas, as a consequence, it is necessary to express the descriptions of goods and tariff heading numbers which appear in Council Regulation (EEC) N° 845/72 (3),
according to the terms of the combined nomenclature; whereas these adaptations do not call for any amendment of substance,
HAS ADOPTED THIS REGULATION:
Article 1
Article 1 (1) of Regulation (EEC) N° 845/72 is replaced by the following:
'1. In order to encourage action by trade and joint trade organizations to facilitate the adjustment of supply to market requirements, Community measures to improve quality may be taken in respect of silkworms falling within subheading 0106 00 99 of the combined nomenclature and in respect of silkworm eggs falling within subheading 0511 99 90 of the combined nomenclature.'
Article 2
This Regulation shall enter into force on 1 January 1988.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 23 December 1987.
For the Commission
Frans ANDRIESSEN
Vice-President
SPA:L377UMBE24.96
FF: 5UEN; SETUP: 01; Hoehe: 421 mm; 48 Zeilen; 2016 Zeichen;
Bediener: JUTT Pr.: B;
Kunde: ................................
(1) OJ N° L 256, 7. 9. 1987, p. 1.
(2) OJ N° L 376, 31. 12. 1987, p. 1.
(3) OJ N° L 100, 27. 4. 1972, p. 1.
