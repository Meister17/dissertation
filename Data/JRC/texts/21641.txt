COUNCIL REGULATION (EEC) No 2434/92 of 27 July 1992 amending Part II of Regulation (EEC) No 1612/68 on freedom of movement for workers within the Community
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 49 thereof,
Having regard to the proposal from the Commission (1),
In cooperation with the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas freedom of movement for workers within the Community constitutes a fundamental right established by the Treaty;
Whereas to give effect to freedom of movement for workers who are nationals of Member States it is necessary to strengthen the mechanism for clearing vacancies and applications for employment provided for in Regulation (EEC) No 1612/68 (4);
Whereas the principle of non-discrimination between Community workers implies the recognition, in fact and in law, of the right of all nationals of the Member States to enjoy the same priority on the labour market as that enjoyed by nationals in each Member State; whereas this equality of priority should apply under the mechanism for clearing vacancies and applications for employment;
Whereas it is important to ensure the greatest possible transparency of the Community labour market, especially when determining vacancies and applications for employment which are the subject of Community clearance,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EEC) No 1612/68 is hereby amended as follows:
1. does not concern the English text;
2. in Article 14:
- in paragraph 1 the words 'by region and by branch of activity' shall be deleted,
- paragraph 2 shall be replaced by the following:
'2. The Commission, taking the utmost account of the opinion of the Technical Committee, shall determine the manner in which the information referred to in paragraph 1 is to be drawn up.',
- in the first sentence of paragraph 3 the words 'in agreement with the Technical Committee' shall be replaced by 'taking the utmost account of the opinion of the Technical Committee',
- does not concern the English text';
3. Article 15 shall be replaced by the following:
'Article 15
1. The specialist service of each Member State shall regularly send to the specialist services of the other Member States and to the European Coordination Office:
(a) details of vacancies which could be filled by nationals of other Member States;
(b) details of vacancies addressed to non-Member States;
(c) details of applications for employment by those who have formally expressed a wish to work in another Member State;
(d) information, by region and by branch of activity, on applicants who have declared themselves actually willing to accept employment in another country.
The specialist service of each Member State shall forward this information to the appropriate employment services and agencies as soon as possible.
2. The details of vacancies and applications referred to in paragraph 1 shall be circulated according to a uniform system to be established by the European Coordination Office in collaboration with the Technical Committee.
If necessary, the European Coordination Office may adapt this system in collaboration with the Technical Committee.';
4. Article 16 shall be replaced by the following:
'Article 16
1. Any vacancy within the meaning of Article 15 communicated to the employment services of a Member State shall be notified to and processed by the competent employment services of the other Member States concerned.
Such services shall forward to the services of the first Member State the details of suitable applications.
2. The applications for employment referred to in Article 15 (1) (c) shall be responded to by the relevant services of the Member States within a reasonable period, not exceeding one month.
3. The employment services shall grant workers who are nationals of the Member States the same priority as the relevant measures grant to nationals vis-à-vis workers from non-Member States.';
5. in Article 17 (1):
- does not concern the English text,
- the word 'returns' in subparagraph (a) (i) shall be replaced by 'details',
- subparagraph (b) shall be replaced by the following:
'(b) the services territorially responsible for the border regions of two or more Member States shall regularly exchange data relating to vacancies and applications for employment in their area and, acting in accordance with their arrangements with the other employment services of their countries, shall directly bring together and clear vacancies and applications for employment.
If necessary, the services territorially responsible for border regions shall also set up cooperation and service structures to provide:
- users with as much practical information as possible on the various aspects of mobility, and
- management and labour, social services (in particular public, private or those of public interest) and all institutions concerned, with a framework of coordinated measures relating to mobility;';
6. in Article 19:
- paragraph 1 shall be replaced by the following:
'1. On the basis of a report from the Commission drawn up from information supplied by the Member States, the latter and the Commission shall at least once a year analyse jointly the results of Community arrangements regarding vacancies and applications.',
- the following shall be added:
'3. Every two years the Commission shall submit a report to the European Parliament, the Council and the Economic and Social Committee on the implementation of Part II of this Regulation, summarizing the information required and the data obtained from the studies and research carried out and highlighting any useful points with regard to developments on the Community's labour market.';
7. Article 20 shall be deleted;
8. the Annex shall be deleted.
Article 2
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 27 July 1992.
For the Council
The President
N. LAMONT
(1) OJ No C 254, 28. 9. 1991, p. 9 and OJ No C 107, 28. 4. 1992, p. 10.(2) OJ No C 94, 13. 4. 1992, p. 202 and Decision of 8 July 1992 (not yet published in the Official Journal).(3) OJ No C 40, 17. 2. 1992, p. 1.(4) OJ No L 257, 19. 10. 1968, p. 2. Last amended by Regulation (EEC) No 312/76 (OJ No L 39, 14. 2. 1976, p. 2.
