Commission Regulation (EC) No 493/2006
of 27 March 2006
laying down transitional measures within the framework of the reform of the common organisation of the markets in the sugar sector, and amending Regulations (EC) No 1265/2001 and (EC) No 314/2002
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 318/2006 of 20 February 2006 on the common organisation of the markets in the sugar sector [1], and in particular Article 44 thereof,
Having regard to Council Regulation (EC) No 1260/2001 of 19 June 2001 on the common organisation of the markets in the sugar sector [2], and in particular Articles 7(5), 15(8) and 16(5) thereof,
Whereas:
(1) The necessary measures should be taken to facilitate the transition in the sugar sector from the rules provided for in Regulation (EC) No 1260/2001 to the new regime established by Regulation (EC) No 318/2006.
(2) Following the deletion of the obligation to export provided for in Article 13 of Regulation (EC) No 1260/2001, measures should be laid down to manage the quantities of sugar resulting from the disappearance of this obligation and of the C sugar arrangements from 1 July 2006. These measures should comply with the Community’s international obligations.
(3) To improve management of the quantities of sugar produced in excess of the quota attributable to the 2005/2006 marketing year, undertakings should be allowed to carry forward some of these quantities to the 2006/2007 marketing year. To this end, it should be laid down that the carry forward in question is subject to the application of Commission Regulation (EEC) No 65/82 of 13 January 1982 laying down detailed rules for carrying forward sugar to the following marketing year [3], while allowing a certain degree of flexibility on the decision to carry forward in order to facilitate the transition between the existing regime and the new regime.
(4) The quantity of non-quota sugar in the 2005/2006 marketing year, which may be neither carried over nor exported, should be considered to be non-quota sugar in the 2006/2007 marketing year in order to allow its disposal through the uses provided for in respect of this sugar by Regulation (EC) No 318/2006, as well as, taking into account the exceptional conditions of the transition between those marketing years, its use in animal feed.
(5) For control purposes and, where applicable, the application of penalties, the proportion of C sugar production in the 2005/2006 marketing year not carried over and not considered to be in excess of the quota in the 2006/2007 marketing year should continue to be subject to the application of Commission Regulation (EEC) No 2670/81 of 14 September 1981 laying down detailed implementing rules in respect of sugar production in excess of the quota [4].
(6) In order to improve the market balance in the Community without creating new stocks of sugar in the 2006/2007 marketing year, provision should be made for a transitional measure to reduce eligible production under quota in respect of that marketing year. A threshold should be fixed above which the production under quota of each undertaking is considered withdrawn within the meaning of Article 19 of Regulation (EC) No 318/2006 or, at the request of the undertaking, as production in excess of the quota within the meaning of Article 12 of that Regulation. In view of the transition between the two regimes, this threshold should be obtained by a combination, in equal parts, of the method laid down in Article 10 of Regulation (EC) No 1260/2001 and that laid down in Article 19 of Regulation (EC) No 318/2006 and take into account the special efforts made by some Member States within the framework of the restructuring fund set up by Council Regulation (EC) No 320/2006 of 20 February 2006 establishing a temporary scheme for the restructuring of the sugar industry in the Community and amending Regulation (EC) No 1290/2005 on the financing of the common agricultural policy [5].
(7) In order to comply with the marketing conditions for the 2005/2006 marketing year, it should be laid down that the aid for disposal and additional aid for sugar produced in certain regions of the Community in the 2005/2006 marketing year and, within the limits of the quantities set by Commission Regulation (EC) No 180/2006 of 1 February 2006 [6], the refining aid for certain preferential sugars imported and refined in the 2005/2006 delivery period may continue to be paid beyond 30 June 2006. To this end, Commission Regulation (EC) No 1554/2001 of 30 July 2001 laying down detailed rules for the application of Council Regulation (EC) No 1260/2001 as regards marketing sugar produced in the French overseas departments and equalising the price conditions with preferential raw sugar [7] and Commission Regulation (EC) No 1646/2001 of 13 August 2001 laying down detailed implementing rules for the grant of adjustment aid to the preferential raw sugar refining industry and adjusting both the adjustment aid and additional basic aid for the sugar refining industry [8] should continue to apply to the granting of these aids. For the sugar concerned the refining of preferential sugars by certain refineries should continue to be limited and the control on presumed maximum supply needs should be maintained, and provision should be made for the continued application of Commission Regulation (EC) No 1460/2003 of 18 August 2003 setting for the 2003/2004 to 2005/2006 marketing years rules of application for Council Regulation (EC) No 1260/2001 as regards the presumed maximum raw sugar supply needs of refineries [9].
(8) To carry out the calculation, fixing and collecting of production levies in the 2005/2006 marketing year, certain provisions of Commission Regulation (EC) No 314/2002 of 20 February 2002 laying down detailed rules for the application of the quota system in the sugar sector [10] and of Commission Regulation (EC) No 779/96 of 29 April 1996 laying down detailed rules for the application of Council Regulation (EEC) No 1785/81 as regards communications in the sugar sector [11] should continue to apply beyond 30 June 2006. The levies are calculated on the basis of statistical data which are regularly updated. As it is the last time that levies are to be fixed for the entire period between the 2001/2002 marketing year and the 2005/2006 marketing year, without a subsequent possibility, as in previous years, to adjust the calculations on the basis of updated figures, the calculation should be deferred and the levies should be fixed on 15 February 2007 to guarantee the reliability of the calculations and the relevance of the statistical data used.
(9) In order to ensure the supply to the chemical industry in the context of the transition between the existing regime and the new regime introduced on 1 July 2006, certain provisions of Commission Regulation (EC) No 1265/2001 of 27 June 2001 laying down detailed rules for the application of Council Regulation (EC) No 1260/2001 as regards granting the production refund on certain sugar products used in the chemical industry [12] should continue to apply beyond 30 June 2006 to the refund certificates issued before that date. Since the new regime allows the use by the chemical industry of non-quota sugar, the period of validity of the export certificates should be reduced and the granting of the refund should be limited to the production under quota in the 2005/2006 marketing year.
(10) Under Article 1(2) of Regulation (EC) No 318/2006, the period covered by the marketing year begins on 1 October and ends on 30 September of the following year. However, the 2005/2006 marketing year, as laid down by Regulation (EC) No 1260/2001, ends on 30 June 2006. The 2006/2007 marketing year therefore begins on 1 July 2006 and ends on 30 September 2007, and thus extends over 15 months. For the 2006/2007 marketing year, therefore, provision should be made for an increase in the quotas and the traditional refining requirements which previously corresponded to a 12-month period and which will, after this marketing year, again apply to a 12-month period, by taking the extra three months into account so as to ensure an allocation which corresponds to that of the preceding and subsequent marketing years. These transitional quotas should cover sugar production from the start of the 2006/2007 marketing year, from sugar beet sown before 1 January 2006.
(11) Regulations (EC) No 314/2002 and (EC) No 1265/2001 should therefore be amended accordingly.
(12) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Sugar,
HAS ADOPTED THIS REGULATION:
CHAPTER I
TRANSITIONAL MEASURES
Article 1
Carrying forward of quotas
1. Notwithstanding Article 14 of Regulation (EC) No 1260/2001, and within the limits laid down in the second subparagraph of Article 2(1) of Regulation (EEC) No 65/82, each undertaking may decide by 31 October 2006 on the quantity of C sugar production attributable to the 2005/2006 marketing year that it is to carry forward to the 2006/2007 marketing year, or amend its decision made before the entry into force of this Regulation to carry forward a certain quantity.
2. Undertakings which make a decision to carry forward as referred to in paragraph 1, or which amend their decision, shall:
(a) inform the Member State concerned, before 31 October 2006, of the quantity of sugar carried forward;
(b) undertake to store the quantity carried forward until 31 October 2006.
3. Regulation (EEC) No 65/82 shall apply to B and C sugar from the 2005/2006 marketing year carried forward to the 2006/2007 marketing year.
4. Member States shall inform the Commission, not later than 30 November 2006 and for each undertaking, of the quantity of B and C sugar carried forward from the 2005/2006 marketing year to the 2006/2007 marketing year.
Article 2
C sugar
1. Without prejudice to decisions to carry forward taken in accordance with Article 1 of this Regulation, and to exports under licences issued in accordance with Article 4 of Commission Regulation (EC) No 1464/95 [13], C sugar from the 2005/2006 marketing year shall be considered to be non-quota sugar, as referred to in Article 12 of Regulation (EC) No 318/2006, produced in respect of the 2006/2007 marketing year.
2. Notwithstanding Article 15 of Regulation (EC) No 318/2006, the levy shall not be charged on the quantities of C sugar referred to in paragraph 1 of this Article which are used in animal feed under the same control conditions as those laid down by the Commission for the industrial sugar referred to in Article 13(2) of Regulation (EC) No 318/2006.
3. Regulation (EEC) No 2670/81 shall apply to C sugar production in the 2005/2006 marketing year, with the exception of the sugar carried forward or considered to be non-quota sugar in the 2006/2007 marketing year as referred to in paragraph 1 of this Article.
The minimum price for A sugar for the 2005/2006 marketing year shall apply to the beet equivalent to the quantity of sugar referred to in Article 3 of Regulation (EEC) No 2670/81.
Article 3
Preventive withdrawal
1. For each undertaking, the share of the production of sugar, isoglucose or inulin syrup in the 2006/2007 marketing year which is produced under the quotas listed in Annex III to Regulation (EC) No 318/2006 and which exceeds the threshold established in accordance with paragraph 2 of this Article shall be considered withdrawn within the meaning of Article 19 of that Regulation or, at the request of the undertaking concerned before 31 January 2007, shall be considered fully or partially to be produced in excess of the quota within the meaning of Article 12 of that Regulation.
2. For each undertaking, the threshold referred to in paragraph 1 shall be established by multiplying the quota allocated to the undertaking under Article 7(2) of Regulation (EC) No 318/2006 by the sum of the following coefficients:
(a) the coefficient fixed for the Member State concerned in Annex I to this Regulation;
(b) the coefficient obtained by dividing the sum of the quotas renounced in the 2006/2007 marketing year in the Member State concerned under Article 3 of Regulation (EC) No 320/2006 by the sum of the quotas fixed for that Member State in Annex III to Regulation (EC) No 318/2006. The Commission shall fix this coefficient not later than 15 October 2006.
However, where the sum of the coefficients exceeds 1,0000, the threshold shall be equal to the quota referred to in paragraph 1.
3. The minimum price applicable to the quantity of beet equivalent to the sugar production withdrawn in accordance with paragraph 1 shall be that of the 2007/2008 marketing year.
4. The obligation referred to in Article 6(5) of Regulation (EC) No 318/2006 shall concern the quantity of beet equivalent to the threshold referred to in paragraph 1 of this Article.
5. Before 1 July 2006, Member States shall send the Commission an estimate of the quantities of sugar, isoglucose and inulin syrup to be considered withdrawn under this Article.
Article 4
Aid for sugar produced in the French overseas departments
1. Aid for disposal and additional aid shall be granted to sugar produced under quota in the 2005/2006 marketing year in the French overseas departments, refined and/or transported between 1 July 2006 and 31 October 2006.
They shall apply to the quantities of sugar concerned, in replacement for the aids referred to in Articles 7(4) and 38(3) and (4) of Regulation (EC) No 1260/2001.
The aid for disposal shall concern:
- refining in refineries in the European regions of the Community of the sugars produced in the French overseas departments, in particular on the basis of their output,
- the transport of sugar produced in the French overseas departments to the European regions of the Community and, where appropriate, its storage in these departments.
2. Regulations (EC) No 1554/2001 and (EC) No 1646/2001 shall apply to the sugar produced under quota in the 2005/2006 marketing year for the aid for disposal and the additional aid referred to in paragraph 1 of this Article.
3. For the purposes of this Article, "refinery" means a production unit whose sole activity consists in refining either raw sugar or syrups produced prior to the crystallising stage.
Article 5
Refining aid
1. Adjustment aid shall be granted to the industry refining preferential raw cane sugar imported under Protocol 3 on ACP sugar attached to Annex IV to the ACP-EC Partnership Agreement signed in Cotonou on 23 June 2000 [14], and refined in the 2005/2006 delivery period between 1 July 2006 and 30 September 2006.
This aid shall be paid to refineries. It shall apply to the quantities referred to in Regulation (EC) No 180/2006 and not refined by 1 July 2006, as a replacement for the aid referred to in Article 38(1), (2) and (4) of Regulation (EC) No 1260/2001.
2. Regulation (EC) No 1646/2001 shall apply to the preferential sugar refined in the 2005/2006 delivery period.
3. Except in cases of force majeure, where the presumed maximum supply needs for a Member State as laid down in Article 39(2) of Regulation (EC) No 1260/2001 are exceeded in the course of the 2005/2006 marketing year, a quantity corresponding to the excess shall be subject to the payment of an amount corresponding to the full rate of import duty in force for the marketing year in question, increased by EUR 115,40 per tonne of white sugar equivalent.
4. Regulation (EC) No 1460/2003 shall apply to control and, where appropriate, the consequences of exceeding the presumed maximum supply needs for the refining industries as referred to in paragraph 3 of this Article.
5. For the purposes of this Article, "refinery" means a production unit whose sole activity consists in refining either raw sugar or syrups produced prior to the crystallising stage.
Article 6
Levies
Regulation (EC) No 314/2002, as amended by this Regulation, shall apply to the fixing and collecting of production levies in the 2005/2006 marketing year, including the corrections relating to the calculation of the levies in the 2001/2002, 2002/2003, 2003/2004 and 2004/2005 marketing years as laid down in Article 15(2) of Regulation (EC) No 1260/2001.
Article 7
Production refunds
Articles 1, 2, 3, 11, 14, 15, 17 18, 19, 20 and 21 of Regulation (EC) No 1265/2001, as amended by this Regulation, shall apply to the refund certificates issued until 30 June 2006.
Article 8
Notifications
Regulation (EC) No 779/96 shall apply until 30 September 2006.
Article 9
Transitional quotas
1. For the 2006/2007 marketing year, a transitional sugar quota of 497780 tonnes shall be allocated to the Member States in accordance with the breakdown in part A of Annex II.
The quota referred to in the first subparagraph shall be reserved for sugar produced from beet sown before 1 January 2006. The minimum price of this beet, within the meaning of Article 5 of Regulation (EC) No 318/2006, shall be EUR 47,67 per tonne.
2. For the 2006/2007 marketing year, a transitional isoglucose quota of 126921 tonnes of dry matter shall be allocated to the Member States in accordance with the breakdown in part B of Annex II.
3. For the 2006/2007 marketing year, a transitional inulin syrup quota of 80180 tonnes of dry matter, expressed as white sugar/isoglucose equivalent, shall be allocated to the Member States in accordance with the breakdown in part C of Annex II.
4. The transitional quotas laid down in paragraphs 1, 2 and 3:
(a) shall not be subject to the payment of the temporary restructuring amount provided for in Article 11(2) of Regulation (EC) No 320/2006;
(b) may not benefit from the payment of the aid provided for in Regulation (EC) No 320/2006.
5. The Member States shall allocate transitional quotas, on the basis of objective criteria and in a manner ensuring equal treatment of producers and to avoid market and competition distortion, to undertakings producing sugar, isoglucose and inulin syrup established in their territory and approved in accordance with Article 17 of Regulation (EC) No 318/2006.
6. The Member States shall introduce a control system and shall take all the necessary steps to verify the production of the products referred to in paragraphs 1, 2 and 3, and in particular that the sugar corresponds to sugar beet sown before 1 January 2006.
The Member States shall communicate to the Commission, not later than 15 July 2006, the breakdown by undertaking of the transitional quotas allocated under this Article.
The Member States shall notify the Commission, not later than 31 December 2006, of the control measures taken and the results thereof.
Article 10
Traditional refining requirements
For the 2006/2007 marketing year, the traditional refining requirements referred to in Article 29(1) of Regulation (EC) No 318/2006 shall be increased by the quantities laid down in Annex III.
CHAPTER II
AMENDMENTS OF REGULATIONS (EC) NO 1265/2001 AND (EC) NO 314/2002
Article 11
Amendment of Regulation (EC) No 1265/2001
Regulation (EC) No 1265/2001 is hereby amended as follows:
1. The following paragraph is added to Article 11:
"5. At the request of the interested party, the competent authority of the Member State shall cancel refund certificates which have not been fully used and have not passed their final date of validity. The relevant security shall be released for the amount unused.
Member States shall inform the Commission at the end of each month of the quantity of refund certificates cancelled in the course of the previous month, broken down by month of issue of the refund certificate."
2. The following paragraph is added to Article 14:
"3. The refund certificate shall be valid only for the basic products referred to in Article 1 which are produced under quota in the 2005/2006 marketing year or previous marketing years."
3. The following sentence is added to Article 15:
"However, the refund certificates shall no longer be valid after 31 August 2006."
4. The following paragraph is added to Article 17:
"3. Member States shall take the necessary additional steps to ensure the proper application of the provisions of Article 14(3)."
Article 12
Amendment of Regulation (EC) No 314/2002
Regulation (EC) No 314/2002 is hereby amended as follows:
1. Article 4a(5) is deleted;
2. The following sentence is added to the third subparagraph of Article 4c(1):
"The notification for the 2005/2006 marketing year shall be made before 1 December 2006."
3. Article 8 is amended as follows:
(a) The following subparagraph is added to paragraph 1:
"In respect of the 2005/2006 marketing year, the amounts and the coefficients referred to in points (a) and (b) of the first subparagraph shall be fixed before 15 February 2007.";
(b) paragraph 2 is amended as follows:
(i) the following sentence is added to the first subparagraph:
"For the 2005/2006 marketing year, these balances shall be determined before 28 February 2007.";
(ii) the following sentence is added to the second subparagraph:
"For the 2005/2006 marketing year, this payment shall be made before 15 April 2007."
CHAPTER III
FINAL PROVISION
Article 13
Entry into force
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Union.
It shall apply from 1 July 2006. However, Articles 1, 3, 11(3) and 12(1) shall apply from the date of its entry into force.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 27 March 2006.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 58, 28.2.2006, p. 1.
[2] OJ L 178, 30.6.2001, p. 1. Regulation as last amended by Commission Regulation (EC) No 39/2004 (OJ L 6, 10.1.2004, p. 16).
[3] OJ L 9, 14.1.1982, p. 14. Regulation as last amended by Regulation (EC) No 2223/2000 (OJ L 253, 7.10.2000, p. 15).
[4] OJ L 262, 16.9.1981, p. 14. Regulation as last amended by Regulation (EC) No 95/2002 (OJ L 17, 19.1.2002, p. 37).
[5] OJ L 58, 28.2.2006, p. 42.
[6] OJ L 29, 2.2.2006, p. 28.
[7] OJ L 205, 31.7.2001, p. 18. Regulation as amended by Regulation (EC) No 1442/2002 (OJ L 212, 8.8.2002, p. 5).
[8] OJ L 219, 14.8.2001, p. 14. Regulation as amended by Regulation (EC) No 1164/2002 (OJ L 170, 29.6.2002, p. 48).
[9] OJ L 208, 19.8.2003, p. 12.
[10] OJ L 50, 21.2.2002, p. 40. Regulation as last amended by Regulation (EC) No 1665/2005 (OJ L 268, 13.10.2005, p. 3).
[11] OJ L 106, 30.4.1996, p. 9. Regulation as last amended by Regulation (EC) No 1159/2003 (OJ L 162, 1.7.2003, p. 25).
[12] OJ L 178, 30.6.2001, p. 63.
[13] OJ L 144, 28.6.1995, p. 14. Regulation as last amended by Regulation (EC) No 96/2004 (OJ L 15, 22.1.2004, p. 3).
[14] OJ L 317, 15.12.2000, p. 3.
--------------------------------------------------
ANNEX I
Coefficients referred to in Article 3(2)(a)
Member State | Coefficients |
Belgium | 0,8558 |
Czech Republic | 0,9043 |
Denmark | 0,8395 |
Germany | 0,8370 |
Greece | 0,8829 |
Spain | 0,8993 |
France (mainland) | 0,8393 |
France (French overseas departments) | 0,8827 |
Ireland | 0,8845 |
Italy | 0,8621 |
Latvia | 0,9136 |
Lithuania | 0,9141 |
Hungary | 0,9061 |
Netherlands | 0,8475 |
Austria | 0,8522 |
Poland | 0,8960 |
Portugal (mainland) | 0,8852 |
Portugal (Azores) | 0,8845 |
Slovenia | 0,8844 |
Slovakia | 0,8833 |
Finland | 0,8841 |
Sweden | 0,8845 |
United Kingdom | 0,8834 |
--------------------------------------------------
ANNEX II
Part A: Transitional quotas for sugar referred to in Article 9(1)
Member State | Transitional quota for sugar 2006/2007 (tonnes of white sugar) |
Spain | 324000 |
Italy | 121187 |
Portugal | 52593 |
Total | 497780 |
Part B: Transitional quotas for isoglucose referred to in Article 9(2)
Member State | Transitional quota isoglucose 2006/2007 (in tonnes of dry matter) |
Belgium | 17898 |
Germany | 8847 |
Greece | 3223 |
Spain | 20645 |
France | 4962 |
Italy | 5076 |
Hungary | 34407 |
Netherlands | 2275 |
Poland | 6695 |
Portugal | 2479 |
Slovakia | 10637 |
Finland | 2968 |
United Kingdom | 6809 |
Total | 126921 |
Part C: Transitional quotas for inulin syrup referred to in Article 9(3)
Member State | Transitional quota inulin syrup 2006/2007 (in tonnes of dry matter sugar/isoglucose equivalent) |
Belgium | 53812 |
France | 6130 |
Netherlands | 20238 |
Total | 80180 |
--------------------------------------------------
ANNEX III
Transitional traditional refining requirements referred to in Article 10
Member State | Transitional traditional refining requirements 2006/2007 (tonnes of white sugar) |
France | 74157 |
Portugal | 72908 |
Slovenia | 4896 |
Finland | 14981 |
United Kingdom | 282145 |
Total | 479087 |
--------------------------------------------------
