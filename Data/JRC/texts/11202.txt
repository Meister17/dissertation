Commission Decision
of 8 November 2006
approving certain national programmes for the control of salmonella in breeding flocks of Gallus gallus
(notified under document number C(2006) 5281)
(Text with EEA relevance)
(2006/759/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Regulation (EC) No 2160/2003 of the European Parliament and of the Council of 17 November 2003 on the control of salmonella and other specified food-borne zoonotic agents [1] and, in particular Article 6(2) thereof,
Whereas:
(1) The purpose of Regulation (EC) No 2160/2003 is to ensure that proper and effective measures are taken to detect and control salmonella and other zoonotic agents at all relevant stages of production, processing and distribution, particularly at the level of primary production, in order to reduce their prevalence and the risk they pose to public health.
(2) A Community target was established for the reduction of the prevalence of all salmonella serotypes with public health significance in breeding flocks of Gallus gallus at the level of primary production by Commission Regulation (EC) No 1003/2005 of 30 June 2005 implementing Regulation (EC) No 2160/2003 as regards a Community target for the reduction of the prevalence of certain salmonella serotypes in breeding flocks of Gallus gallus and amending Regulation (EC) No 2160/2003 [2].
(3) In order to achieve the Community target Member States are to establish national programmes for the control of salmonella in breeding flocks of Gallus gallus and submit them to the Commission in accordance with Regulation (EC) No 2160/2003.
(4) Certain Member States have submitted their national programmes for the control of salmonella in breeding flocks of Gallus gallus.
(5) Those programmes were found to comply with relevant Community veterinary legislation and in particular with Regulation (EC) No 2160/2003.
(6) The national control programmes should therefore be approved.
(7) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
The national programmes for the control of salmonella in breeding flocks of Gallus gallus submitted by the Member States listed in the Annex are approved.
Article 2
This Decision shall apply from 1 January 2007.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 8 November 2006.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 325, 12.12.2003, p. 1. Regulation as amended by Regulation (EC) No 1003/2005 (OJ L 170, 1.7.2005, p. 12).
[2] OJ L 170, 1.7.2005, p. 12. Regulation as amended by Regulation (EC) No 1168/2006 (OJ L 211, 1.8.2006, p. 4).
--------------------------------------------------
ANNEX
Belgium
Czech Republic
Denmark
Germany
Estonia
Greece
Spain
France
Ireland
Italy
Cyprus
Latvia
Lithuania
Hungary
Netherlands
Austria
Poland
Portugal
Slovenia
Slovakia
Finland
Sweden
United Kingdom
--------------------------------------------------
