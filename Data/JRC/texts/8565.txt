DIRECTIVE 1999/42/EC OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL
of 7 June 1999
establishing a mechanism for the recognition of qualifications in respect of the professional activities covered by the Directives on liberalisation and transitional measures and supplementing the general systems for the recognition of qualifications
THE EUROPEAN PARLIAMENT AND
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Articles 40 and 47(1), the first and third sentences of Article 47(2), and Article 55 thereof,
Having regard to the proposal from the Commission(1),
Having regard to the opinion delivered by the Economic and Social Committee(2),
Acting in accordance with the procedure laid down in Article 251 of the Treaty(3) and in the light of the joint text adopted on 22 April 1999,
(1) Whereas, under the Treaty, all discriminatory treatment on grounds of nationality with regard to establishment and provision of services is prohibited as from the end of the transitional period; whereas, therefore, certain provisions of the Directives applying in this area have become redundant for the purposes of applying the rule of national treatment, since that rule is enshrined in the Treaty itself and has direct effect;
(2) Whereas, however, certain of the Directives' provisions designed to facilitate the effective exercise of the right of establishment and freedom to provide services should be retained, particularly where they usefully prescribe how obligations under the Treaty are to be discharged;
(3) Whereas, in order to facilitate the exercise of freedom of establishment and freedom to provide services in respect of a number of activities, Directives introducing transitional measures have been adopted pending mutual recognition of qualifications; whereas those Directives accept the fact that the activity in question has been pursued for a reasonable and sufficiently recent period of time in the Member State from which the national comes as a sufficient qualification for taking up the activities in question in Member States which have rules governing the taking up of such activities;
(4) Whereas the main provisions of the said Directives should be replaced in line with the conclusions of the European Council held in Edinburgh on 11 and 12 December 1992 regarding subsidiarity, simplification of Community legislation and, in particular, the reconsideration by the Commission of the relatively old directives dealing with professional qualifications; whereas the Directives in question should therefore be repealed;
(5) Whereas Council Directive 89/48/EEC of 21 December 1988 on a general system for the recognition of higher-education diplomas awarded on completion of professional education and training of at least three years' duration(4) and Council Directive 92/51/EEC of 18 June 1992 on a second general system for the recognition of professional education and training to supplement Directive 89/48/EEC(5) do not apply to certain professional activities covered by the Directives applying to this subject-matter (Part One of Annex A to this Directive); whereas recognition machinery in respect of qualifications should, therefore, be introduced for those professional activities not covered by Directives 89/48/EEC and 92/51/EEC; whereas the professional activities listed in Part Two of Annex A to this Directive fall for the most part within the scope of Directive 92/51/EEC as far as the recognition of diplomas is concerned;
(6) Whereas a proposal has been transmitted to the Council with a view to amending Directives 89/48/EEC and 92/51/EEC in respect of the proof of financial standing and the proof of an insurance against financial risks that a host Member State may require of the beneficiary; whereas the Council intends to deal with this proposal at a later stage;
(7) Whereas a proposal has been transmitted to the Council with a view to facilitating the free movement of specialised nurses who do not have any of the qualifications listed in Article 3 of Directive 77/452/EEC(6); whereas the Council intends to deal with this proposal at a later stage;
(8) Whereas this Directive should require regular reports to be drawn up on its implementation;
(9) Whereas this Directive without prejudice to the application of Articles 39(4) and 45 of the Treaty,
HAVE ADOPTED THIS DIRECTIVE:
TITLE I
Scope
Article 1
1. Member States shall adopt the measures defined in this Directive in respect of establishment or provision of services in their territories by natural persons and companies or firms covered by Title I of the General Programmes for the abolition of restrictions on freedom to supply services(7) and on freedom of establishment(8) (hereinafter called "beneficiaries") who wish to pursue the activities listed in Annex A.
2. This Directive shall apply to the activities listed in Annex A which nationals of Member States wish to pursue in a host Member State in a self-employed or employed capacity.
Article 2
Member States in which the taking-up or pursuit of any activity referred to in Annex A is subject to possession of certain qualifications shall ensure that any beneficiaries who apply therefor be provided, before they establish themselves or before they begin to provide services, with information as to the rules governing the occupation which they propose to pursue.
TITLE II
Recognition of formal qualifications awarded by another Member State
Article 3
1. Without prejudice to Article 4, a Member State may not, on the grounds of inadequate qualifications, refuse to permit a national of another Member State to take up or pursue any of the activities listed in Part One of Annex A on the same conditions as apply to its own nationals, without having first compared the knowledge and skills certified by the diplomas, certificates or other evidence of formal qualifications obtained by the beneficiary with a view to pursuing the same activity elsewhere in the Community with those required under its own national rules. Where the comparative examination shows that the knowledge and skills certified by a diploma, certificate or other evidence of formal qualifications awarded by another Member State correspond to those required by the national rules, the host Member State cannot refuse the holder the right to pursue the activity in question. Where, however, the comparative examination shows a substantial difference, the host Member State shall give the beneficiary the opportunity to demonstrate that he has acquired the knowledge and skills which were lacking. In this case, the host Member State shall give the applicant the right to choose between an adaptation period and an aptitude test by analogy with Directives 89/48/EEC and 92/51/EEC.
By way of derogation from this rule, the host Member State may require an adaptation period or an aptitude test if the migrant envisages exercising professional activities in a self-employed capacity or as a manager of an undertaking which are covered by Part One of Annex A and which require the knowledge and the application of the specific national rules in force, provided that knowledge and application of those rules are required by the competent authorities of the host Member State for access to such activities by its own nationals.
Member States shall endeavour to take into consideration the beneficiary's preference as between those alternatives.
2. Applications for recognition within the meaning of paragraph 1 shall be examined within the shortest possible time, and the competent authority in the host Member State shall state its reasons when giving a decision, which shall be taken no later than four months from the date on which the application and comprehensive supporting documentation were submitted. There shall be a right to appeal under national law against a decision or against the absence of such decision.
TITLE III
Recognition of professional qualifications on the basis of professional experience acquired in another Member State
Article 4
Where, in a Member State, the taking-up or pursuit of any activity listed in Annex A is subject to possession of general, commercial or professional knowledge and ability, that Member State shall accept as sufficient evidence of such knowledge and ability the fact that the activity in question has been pursued in another Member State. Where the activity is mentioned in Part One of Annex A, it must have been pursued:
1. in the case of the activities in List I:
(a) for six consecutive years in either a self-employed capacity or as a manager of an undertaking; or
(b) for three consecutive years in a self-employed capacity or as a manager of an undertaking where the beneficiary proves that he has received at least three years' prior training for the activity in question, attested by a nationally recognised certificate or regarded by a competent professional or trade body as fully satisfying its requirements; or
(c) for three consecutive years in a self-employed capacity where the beneficiary proves that he has pursued the activity in question for at least five years in an employed capacity; or
(d) for five consecutive years in a managerial capacity of which at least three years were spent in technical posts with responsibility for one or more departments of the undertaking where the beneficiary proves that he has received at least three years' prior training for the activity in question, attested by a nationally recognised certificate or regarded by a competent professional or trade body as fully satisfying its requirements.
In the cases referred to in (a) and (c), pursuit of the activity shall not have ceased more than ten years before the date on which the application under Article 8 is made;
2. in the case of the activities in List II:
(a) for six consecutive years in either a self-employed capacity or as a manager of an undertaking; or
(b) - for three consecutive years in a self-employed capacity or as a manager of an undertaking where the beneficiary proves that he has received at least three years' prior training for the activity in question, attested by a nationally recognised certificate or regarded by a competent professional or trade body as fully satisfying its requirements, or
- for four consecutive years in a self-employed capacity or as a manager of an undertaking where the beneficiary proves that he has received at least two years' prior training for the activity in question, attested by a nationally recognised certificate or regarded by a competent professional or trade body as fully satisfying its requirements, or
(c) for three consecutive years in a self-employed capacity or as a manager of an undertaking where the beneficiary proves that he has pursued the activity in question for at least five years in an employed capacity; or
(d) - for five consecutive years in an employed capacity where the beneficiary proves that he has received at least three years' prior training for the activity in question, attested by a nationally recognised certificate or regarded by a competent professional or trade body as fully satisfying its requirements, or
- for six consecutive years in an employed capacity where the beneficiary proves that he has received at least two years' prior training for the activity in question, attested by a nationally recognised certificate or regarded by a competent professional or trade body as fully satisfying its requirements.
In the cases referred to in (a) and (c), pursuit of the activity shall not have ceased more than ten years before the date on which the application provided for in Article 8 is made;
3. in the case of the activities in List III:
(a) for six consecutive years in either a self-employed capacity or as a manager of an undertaking; or
(b) for three consecutive years in a self-employed capacity or as a manager of an undertaking, where the beneficiary proves that he has received at least three years' prior training for the activity in question, attested by a nationally recognised certificate or regarded by a competent professional or trade body as fully satisfying its requirements; or
(c) for three consecutive years in a self-employed capacity where the beneficiary proves that he has pursued the activity in question for at least five years in an employed capacity.
In the cases referred to in (a) and (c), pursuit of the activity shall not have ceased more than ten years before the date on which the application provided for in Article 8 is made;
4. in the case of the activities in List IV:
(a) for five consecutive years in either a self-employed capacity or as a manager of an undertaking; or
(b) for two consecutive years in a self-employed capacity or as a manager of an undertaking where the beneficiary proves that he has received at least three years' prior training for the activity in question, attested by a nationally recognised certificate or regarded by a competent professional or trade body as fully satisfying its requirements; or
(c) for three consecutive years in a self-employed capacity or as a manager of an undertaking where the beneficiary proves that he has received at least two years' prior training for the activity in question, attested by a nationally recognised certificate or regarded by a competent professional or trade body as fully satisfying its requirements; or
(d) for two consecutive years in a self-employed capacity or as a manager of an undertaking where the beneficiary proves that he has pursued the activity in question for at least three years in an employed capacity; or
(e) for three consecutive years in an employed capacity where the beneficiary proves that he has received at least two years' previous training for the activity in question, attested by a nationally recognised certificate or regarded by a competent professional or trade body as fully satisfying its requirements;
5. in the case of the activities in List V(a) and (b):
(a) for three years in a self-employed capacity or as a manager of an undertaking, provided that pursuit of the activity in question did not cease more than two years before the date on which the application provided for in Article 8 is made;
(b) for three years in a self-employed capacity or as a manager of an undertaking, provided that pursuit of the activity in question did not cease more than two years before the date on which the application provided for in Article 8 is made, unless the host Member State permits its nationals to interrupt their pursuit of that activity for a longer period; or
6. in the case of the activities in List VI:
(a) for three consecutive years in either a self-employed capacity or as a manager of an undertaking; or
(b) for two consecutive years in a self-employed capacity or as a manager of an undertaking where the beneficiary proves that he has received prior training for the activity in question, attested by a nationally recognised certificate or regarded by a competent professional or trade body as fully satisfying its requirements; or
(c) for two consecutive years in a self-employed capacity or as a manager of an undertaking where the beneficiary proves that he has pursued the activity in question for at least three years in an employed capacity; or
(d) for three consecutive years in an employed capacity where the beneficiary proves that he has received previous training for the activity in question, attested by a nationally recognised certificate or regarded by a competent professional or trade body as fully satisfying its requirements.
In the cases referred to in (a) and (c), pursuit of the activity shall not have ceased more than 10 years before the date on which the application provided for in Article 8 is made.
Article 5
Where a beneficiary holds a nationally recognised certificate obtained in a Member State attesting to knowledge of and ability in the activity in question equivalent to at least two or three years, as appropriate, of professional training, that certificate may be treated by the host Member State in the same way as a certificate attesting to training of the duration required by Article 4(1)(b) and (d), (2)(b) and (d), (3)(b) and (4)(b), (c) and (e).
Article 6
Where the duration of the training of the beneficiary is at least two years and less than three years, the requirements of Article 4 shall be satisfied if the duration of professional experience in a self-employed capacity or as a manager specified in Article 4(1)(b) and (d), (2)(b), first indent, (3)(b) and (4)(b) or in an employed capacity specified in Article 4(2)(d), first indent is extended in the same proportion to cover the difference in the duration of the training.
Article 7
A person shall be regarded as having pursued an activity as a manager of an undertaking within the meaning of Article 4 if he has pursued such an activity in an undertaking in the occupational field in question:
(a) as a manager of an undertaking or a manager of a branch of an undertaking; or
(b) as a deputy to the proprietor or the manager of an undertaking where that post involves responsibility equivalent to that of the proprietor or manager represented; or
(c) in a managerial post with duties of a commercial and/or technical nature and with responsibility for one or more departments of the undertaking.
Article 8
Proof that the conditions laid down in Article 4 are satisfied shall be established by a certificate concerning the nature and duration of the activity issued by the competent authority or body in the home Member State or in the Member State from where the beneficiary comes which the beneficiary must submit in support of his application for authorisation to pursue the activity or activities in question in the host Member State.
TITLE IV
Recognition of other professional qualifications obtained in another Member State
Article 9
1. Where a host Member State requires its own nationals wishing to take up any activity referred to in Article 1(2) to furnish proof of good character and proof that they are not and have not previously been declared bankrupt, or proof of either of these, it shall accept as sufficient evidence, in respect of nationals of other Member States, the production of an extract from the "judicial record" or, failing this, of an equivalent document issued by a competent judicial or administrative authority in the home Member State or in the Member State from where the beneficiary comes showing that these requirements are satisfied.
2. Where a host Member State imposes on its own nationals wishing to take up any activity referred to in Article 1(2) certain requirements as to good character and requires them to prove that they are not and have not previously been declared bankrupt and have not previously been the subject of professional or administrative disciplinary measures (for example, withdrawal of the right to hold certain offices, suspension from practice or striking-off), but proof cannot be obtained from the document referred to in paragraph 1 of this Article, it shall accept as sufficient evidence in respect of nationals of other Member States a certificate issued by a competent judicial or administrative authority in the home Member State or in the Member State from where the beneficiary comes attesting that the requirements are satisfied. Such certificate shall relate to the specific facts regarded as relevant by the host Member State.
3. Where the home Member State or the Member State from where the beneficiary comes does not issue the documents referred to in paragraphs 1 and 2, such documents shall be replaced by a declaration on oath - or, in those Member States where there is no provision for such declaration on oath, by a solemn declaration - made by the person concerned before a competent judicial or administrative authority or, where appropriate, a notary in that Member State; such authority or notary shall issue a certificate attesting the authenticity of the declaration on oath or solemn declaration. The declaration of no previous bankruptcy may also be made before a competent professional or trade body in that Member State.
4. Where a host Member State requires proof of financial standing, it shall regard certificates issued by banks in the home Member State or in the Member State from where the beneficiary comes as equivalent to those issued in its own territory.
5. Where a host Member State requires its own nationals wishing to take up or pursue any activity referred to in Article 1(2) to furnish proof that they are insured against the financial risks arising from their professional liability, it shall accept certificates issued by the insurance undertakings of other Member States as equivalent to those issued in its own territory. Such certificates shall state that the insurer has complied with the laws and regulations in force in the host Member State regarding the terms and extent of cover.
6. At the time of their production, the documents referred to in paragraphs 1, 2, 3 and 5 may not date from more than three months after their date of issue.
TITLE V
Procedural provisions
Article 10
1. Member States shall designate, within the period stipulated in Article 14, the authorities and bodies responsible for issuing the certificates referred to in Articles 8 and 9(1), (2) and (3) and shall communicate this information forthwith to the other Member States and to the Commission.
2. Each Member State may nominate a coordinator for the activities of the authorities and bodies referred to in paragraph 1 to the coordinating group set up under Article 9(2) of Directive 89/48/EEC. The tasks of the coordinating group shall also be as follows:
- facilitating the implementation of this Directive;
- collecting all useful information for its application in the Member States and especially gathering and comparing information on the different professional qualifications in the areas of activity falling within the scope of this Directive.
TITLE VI
Final provisions
Article 11
1. The Directives listed in Annex B are hereby repealed.
2. References to the repealed Directives shall be construed as references to this Directive.
Article 12
As from 1 January 2001, Member States shall communicate to the Commission every two years a report on the application of the system introduced.
In addition to general remarks, that report shall contain a statistical summary of the decisions taken and a description of the main problems arising from the application of this Directive.
Article 13
Not later than five years after the date referred to in Article 14, the Commission shall report to the European Parliament and the Council on the state of application of this Directive, and in particular of Article 5, in the Member States.
After undertaking all the necessary hearings, especially of the coordinators, the Commission shall submit its conclusions regarding any changes to the existing arrangement. If necessary, the Commission shall also submit proposals for improving the existing arrangements with the aim of facilitating free movement of persons, the right of establishment and freedom to provide services.
Article 14
1. Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive before 31 July 2001. They shall immediately inform the Commission thereof.
When Member States adopt these measures, they shall contain a reference to this Directive or shall be accompanied by such reference on the occasion of their official publication. The methods of making such a reference shall be laid down by the Member States.
2. Member States shall communicate to the Commission the text of the main provisions of national law which they adopt in the field covered by this Directive.
Article 15
This Directive shall enter into force on the day of its publication in the Official Journal of the European Communities.
Article 16
This Directive is addressed to the Member States.
Done at Luxembourg, 7 June 1999.
For the European Parliament
The President
J. M. GIL-ROBLES
For the Council
The President
E. BULMAHN
(1) OJ C 115, 19.4.1996, p. 16 and
OJ C 264, 30.8.1997, p. 5.
(2) OJ C 295, 7.10.1996, p. 43.
(3) Opinion of the European Parliament of 20 February 1997 (OJ C 85, 17.3.1997, p. 114), Common Position of the Council of 29 June 1998 (OJ C 262, 19.8.1998, p. 12), Decision of the European Parliament of 8 October 1998 (OJ C 328, 26.10.1998, p. 156). Decision of the European Parliament of 7 May 1999 and Council Decision of 11 May 1999.
(4) OJ L 19, 24.1.1989, p. 16.
(5) OJ L 209, 24.7.1992, p. 25. Directive as last amended by Commission Directive 97/38/EC (OJ L 184, 12.7.1997, p. 31).
(6) Council Directive 77/452/EEC of 27 June 1977 concerning the mutual recognition of diplomas, certificates and other evidence of the formal qualifications of nurses responsible for general care, including measures to facilitate the effective exercise of this right of establishment and freedom to provide services (OJ L 176, 15.7.1977, p. 1). Directive as last amended by Directive 90/658/EC (OJ L 353, 17.12.1990, p. 73).
(7) OJ 2, 15.1.1962, p. 32/62.
(8) OJ 2, 15.1.1962, p. 36/62.
ANNEX A
PART ONE
Activities related to categories of professional experience
List I
Major Groups covered by Directive 64/427/EEC, as amended by Directive 69/77/EEC, and by Directives 68/366/EEC, 75/368/EEC and 75/369/EEC
1
Directive 64/427/EEC
(liberalisation Directive: 64/429/EEC)
NICE Nomenclature (corresponding to ISIC Major Groups 23-40)
>TABLE>
2
Directive 68/366/EEC
(liberalisation Directive 68/365/EEC)
NICE Nomenclature
>TABLE>
3
Directive 75/368/EEC (activities listed in Article 5(1 ))
ISIC Nomenclature
>TABLE>
4
Directive 75/369/EEC (Article 6: where the activity is regarded as being of an industrial or small-craft nature)
ISIC Nomenclature
The following itinerant activities:
(a) the buying and selling of goods:
- by itinerant tradesmen, hawkers or pedlars (ex ISIC Group 612);
- in covered markets other than from permanently fixed installations and in open-air markets;
(b) activities covered by transitional measures already adopted that expressly exclude or do not mention the pursuit of such activities on an itinerant basis.
List II
Directive 82/470/EEC (Article 6(3))
Groups 718 and 720 of the ISIC Nomenclature
The activities comprise in particular:
- organizing, offering for sale and selling, outright or on commission, single or collective items (transport, board, lodging, excursions, etc.) for a journey or stay, whatever the reasons for travelling (Article 2(B)(a)).
List III
Directive 82/489/EEC
>TABLE>
List IV
Directive 82/470/EEC (Article 6(1))
Groups 718 and 720 of the ISIC Nomenclature:
The activities comprise in particular:
- acting as an intermediary between contractors for various methods of transport and persons who dispatch or receive goods, and carrying out related activities:
(aa) by concluding contracts with transport contractors, on behalf of principals;
(bb) by choosing the method of transport, the firm and the route considered most profitable for the principal;
(cc) by arranging the technical aspects of the transport operation (e.g. packing required for transportation); by carrying out various operations incidental to transport (e.g. ensuring ice supplies for refrigerated wagons);
(dd) by completing the formalities connected with the transport such as the drafting of way bills; by assembling and dispersing shipments;
(ee) by coordinating the various stages of transportation, by ensuring transit, reshipment, transshipment and other termination operations;
(ff) by arranging both freight and carriers and means of transport for persons dispatching goods or receiving them;
- assessing transport costs and checking the detailed accounts;
- taking certain temporary or permanent measures in the name of and on behalf of a shipowner or sea transport carrier (with the port authorities, ship's chandlers, etc.)
(The activities listed under Article 2(A)(a), (b) and (d)).
List V
Directives (64/222/EEC) and (70/523/EEC)
(a)
See Article 4(5)(a) of this Directive
Directive 64/222/EEC
(liberalisation Directive 64/224/EEC)
1. professional activities of an intermediary who is empowered and instructed by one or more persons to negotiate or enter into commercial transactions in the name of and on behalf of those persons;
2. professional activities of an intermediary who, while not being permanently so instructed, brings together persons wishing to contract directly with one another or arranges their commercial transactions or assists in the completion thereof;
3. professional activities of an intermediary who enters into commercial transactions in his own name on behalf of others;
4. professional activities of an intermediary who carries out wholesale selling by auction on behalf of others;
5. professional activities of an intermediary who goes from door to door seeking orders;
6. provision of services, by way of professional activities, by an intermediary in the employment of one or more commercial, industrial or small craft undertakings.
(b)
See Article 4(5)(b) of this Directive
Directive 70/523/EEC
Activities of self-employed persons in the wholesale coal trade and activities of intermediaries in the coal trade (ex Group 6112, ISIC Nomenclature)
List VI
Directives 68/364/EEC, 68/368/EEC, 75/368/EEC, 75/369/EEC, 82/470/EEC
1
Directive 68/364/EEC
(liberalisation Directive 68/363/EEC)
>TABLE>
>TABLE>
2
Directive 68/368/EEC
(liberalisation Directive 68/367/EEC)
ISIC Nomenclature
ISIC ex Major Group 85:
1. Restaurants, cafes, taverns and other drinking and eating places (ISIC Group 852)
2. Hotels, rooming houses, camps and other lodging places (ISIC Group 853)
3
Directive 75/368/EEC (Article 7)
All the activities listed in the Annex to Directive 75/368/EEC, except those referred to in Article 5 of that Directive (List I, No 3 of this Annex).
>TABLE>
4
Directive 75/369/EEC (Article 5)
The following itinerant activities:
(a) the buying and selling of goods:
- by itinerant tradesmen, hawkers or pedlars (ex ISIC Group 612);
- in covered markets other than from permanently fixed installations and in open-air markets;
(b) activities covered by transitional measures already adopted that expressly exclude or do not mention the pursuit of such activities on an itinerant basis.
5
Directive 82/470/EEC (Article 6(2))
(Activities listed in Article 2(A)(c) and (e), (B)(b), (C) and (D))
These activities comprise in particular:
- hiring railway cars or wagons for transporting persons or goods;
- acting as an intermediary in the sale, purchase or hiring of ships;
- arranging, negotiating and concluding contracts for the transport of emigrants;
- receiving all objects and goods deposited, on behalf of the depositor, whether under customs control or not, in warehouses, general stores, furniture depots, coldstores, silos, etc.
- supplying the depositor with a receipt for the object or goods deposited;
- providing pens, feed and sales rings for livestock being temporarily accommodated while awaiting sale or while in transit to or from the market;
- carrying out inspection or technical valuation of motor vehicles;
- measuring, weighing and gauging goods.
PART TWO
Activities other than those covered in Part One
1
Directives 63/261/EEC, 63/262/EEC, 65/1/EEC, 67/530/EEC, 67/531/EEC, 67/532/EEC, 68/192/EEC, 68/415/EEC and 71/18/EEC
ISIC Nomenclature
>TABLE>
In particular:
(a) general agriculture including the growing of field crops and viticulture; growing of fruits, nuts, seeds, vegetables, flowers, both in the open and under glass;
(b) raising of livestock, poultry, rabbits, fur-bearing or other animals, bees; the production of meat, milk, wool, skins and fur, eggs, honey;
(c) agricultural, animal husbandry and horticultural services on a fee or contract basis.
2
Directive 63/607/EEC
(Films)
3
Directive 64/223/EEC
ISIC Nomenclature
>TABLE>
4
Directive 64/428/EEC
NICE Nomenclature
>TABLE>
5
Directive 65/264/EEC
(cinema)
6
Directive 66/162/EEC
ISIC Nomenclature
>TABLE>
7
Directive 67/43/EEC
ISIC Nomenclature
>TABLE>
8
Directive 67/654/EEC
ISIC Nomenclature
>TABLE>
9
Directives 68/369/EEC and 70/451/EEC
ISIC Nomenclature
>TABLE>
10
Directive 69/82/EEC
ISIC Nomenclature
>TABLE>
11
Directive 70/522/EEC
ISIC Nomenclature
>TABLE>
ANNEX B
REPEALED DIRECTIVES
PART ONE: LIBERALISATION DIRECTIVES
>TABLE>
PART TWO: DIRECTIVES PROVIDING FOR TRANSITIONAL MEASURES
>TABLE>
