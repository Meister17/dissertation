Judgment of the Court of First Instance of 1 February 2006 — Elisabetta Dami v OHIM
(Joined Cases T-466/04 and T-467/04) [1]
Parties
Applicant: Elisabetta Dami (Milan, Italy) (represented by: P. Beduschi and S. Giudici, lawyers)
Defendant: Office for Harmonisation in the Internal Market (Trade Marks and Designs) (represented by: A. Folliard-Monguiral, Agent)
Other party to the proceedings before the Board of Appeal of OHIM: The Stilton Cheese Makers Association (Surbiton, Surrey, United Kingdom)
Action
Two actions against the decisions of the Second Board of Appeal of OHIM of 20 September 2004 (Cases R 973/2002-2 and R 982/2002-2) concerning opposition proceedings between Mrs Elisabetta Dami and The Stilton Cheese Makers Association,
Operative part of the judgment
The Court:
1. annuls the decisions of the Second Board of Appeal of the Office for Harmonisation in the Internal Market (Trade Marks and Designs) (OHIM) of 20 September 2004 (Cases R 973/2002-2 and R 982/2002-2);
2. orders OHIM to pay the costs.
[1] OJ C 69, 19.3.2005.
--------------------------------------------------
