COUNCIL DECISION
of 22 June 1995
laying down the rules for the microbiological testing by sampling of fresh beef and veal and pigmeat intended for Finland and Sweden
(95/409/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 64/433/EEC of 26 June 1964 on health conditions for the production and marketing of fresh meat (1), and in particular Article 5 (3) (a) and (b) thereof,
Whereas the Commission has approved the operational programmes submitted by Finland and Sweden regarding salmonella controls; whereas those programmes comprise specific measures for fresh beef and veal and pigmeat;
Whereas the implementation of microbiological tests by an establishment constitutes one of the additional guarantees to be given to Finland and Sweden and provides equivalent guarantees to those obtained under the Finnish and Swedish operational programmes, as recognized by the relevant Commission decisions;
Whereas Finland and Sweden must apply to consignments from third countries import requirements at least as stringent as those laid down in this Decision;
Whereas, as far as the sampling methods to be applied are concerned, it is appropriate to make a distinction between carcases and half-carcases, on the one hand, and quarters, cuts and smaller pieces on the other;
Whereas it is appropriate to take into account international methods for the microbiological examination of samples;
Whereas microbiological tests are not to be required for fresh meat from an establishment which is subject to a programme recognized as equivalent to that implemented by Finland or Sweden;
Whereas the provisions of this Decision are without prejudice to any provisions which might be adopted in implementation of Article 5 (2) of Directive 64/433/EEC,
HAS ADOPTED THIS DECISION:
Article 1
Pursuant to Article 5 (3) (a) of Directive 64/433/EEC, consignments of fresh beef and veal and pigmeat intended for Finland and Sweden shall be subject to the rules laid down in Articles 2, 3 and 4.
Article 2
Fresh beef and veal and pigmeat intended for Finland and Sweden shall be subjected to microbiological testing for salmonella, as provided for in Article 5 (3) (a) of Directive 64/433/EEC, by sampling in the establishment of origin of such meat. These microbiological tests shall be carried out as laid down in the Annex.
Article 3
Fresh beef and veal and pigmeat from an establishment subject to a programme recognized, in accordance with the procedure laid down in Article 16 of Directive 64/433/EEC, as equivalent to that implemented by Finland or Sweden, shall not be subjected to the microbiological tests laid down in this Decision.
Article 4
The Council, acting on a Commission proposal drawn up in the light of a report established on the basis of the results of the operational programmes implemented by Finland and Sweden and the experience gained in applying this Decision, shall review this Decision before 1 July 1998.
Article 5
This Decision shall apply from 1 July 1995.
Article 6
This Decision is addressed to the Member States.
Done at Brussels, 22 June 1995.
For the Council
The President
Ph. VASSEUR
(1) OJ No 121, 29. 7. 1964, p. 2012/64. Directive as last amended by Decision 95/1/EC, Euratom, ECSC (OJ No L 1, 1. 1. 1995, p. 1).
ANNEX
SECTION A SAMPLING METHOD
1. Carcases, half-carcases and quarters obtained from the slaughterhouse of origin ('swab technique')
Samples using the surface swabbing method are taken at sites likely to be contaminated. Surface swabbing includes opening sides of the carcases and cut surfaces. In addition beef carcases are swabbed in at least three areas (leg, flank and neck), pork carcases however are swabbed in at least two areas (leg and brisket).
To avoid cross-contamination, samples are taken without handling the meat and by using sterile swabs and templates.
The sampling areas (20 cm × 20 cm) described in the first paragraph are swabbed with two sterile cotton swabs. The first swab is moistened with sterile peptone water and is rubbed firmly several times across the sampling area. The same sampling area is rubbed with the second swab used dry. Subsequently swabs are put into 100 ml of buffered peptone water.
Each sample is duly marked and identified.
2. Quarter cuts obtained from an establishment other than the slaughterhouse of origin of the carcase, cuts and smaller pieces ('destructive method')
Pieces of tissue are obtained by punching a sterile cork borer into the meat surface or cutting a slice of tissue of approximately 25 cm² with sterile instruments. The samples are aseptically transferred into a sample container or plastic dilution bag and then homogenized (peristaltic stomacher or rotary blender (homogenizer)). Samples of frozen meat remain frozen during transport to the laboratory. Samples from chilled meat are not frozen but kept refrigerated. Separate samples of the same consignment may be pooled.
Each sample is duly marked and identified.
SECTION B NUMBER OF SAMPLES TO BE TAKEN
1. Carcases, half-carcases, half-carcases cut into a maximum of three pieces and quarters referred to under A (1)
The number of carcases or half-carcases (units) in a consigment of which separate random samples are to be taken is a follows:
>TABLE>
2. Quarters, cuts and smaller pieces as referred to under A (2)
The number of packing units in the consignment of which separate random samples are to be taken is as follows:
>TABLE>
Depending on the weight of the packing units, the number of packing units to be sampled can be reduced using the following multiplication factors:
>TABLE>
SECTION C MICROBIOLOGICAL METHOD FOR THE EXAMINATION OF THE SAMPLES
Microbiological testing of the samples for salmonella must be carried out according to the standard method of the International Organization for Standardization ISO 6579:1993. However, methods offering equivalent guarantees may be authorized on a case-by-case basis by the Council, acting on a proposal from the Commission.
