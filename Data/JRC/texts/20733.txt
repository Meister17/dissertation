REGULATION (EEC) No 1037/72 OF THE COUNCIL of 18 May 1972 laying down general rules for granting and financing aid for hop producers
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community;
Having regard to Council Regulation (EEC) No 1696/711 of 26 July 1971 on the common organization of the market in hops, and in particular Article 13 (3) thereof;
Having regard to Council Regulation (EEC) No 729/702 of 21 April 1970 on the financing of the common agricultural policy, and in particular Article 3 (2) thereof;
Having regard to the proposal from the Commission;
Whereas Article 12 of Regulation (EEC) No 1696/71 provides for the granting of aid for hops produced in the Community ; whereas it is therefore necessary to lay down the general rules of application provided for in Article 13 of that Regulation;
Whereas aid is necessary for administrative reasons to grant aid, in each Member State, solely for areas planted to hops in the territory of that State;
Whereas aid is only granted in respect of registered areas planted to hops ; whereas, consequently, provision should be made for the introduction by the Member States, of a system of declarations and registration of such areas;
Whereas the system of aid cannot work properly without checks ensuring that the aid is only granted for areas which are planted to hops and which are harvested;
Whereas it should be laid down that areas for which aid is requested must be planted to hops in accordance with normal practice;
Whereas this aid entails expenditure ; whereas Article 17 of Regulation (EEC) No 1696/71 states that regulations on the financing of the common agricultural policy are applicable to this aid ; whereas, consequently, it should be specified that this aid is to be financed pursuant to Article 3 of Regulation (EEC) No 729/70;
HAS ADOPTED THIS REGULATION:
Article 1
The general rules laid down in the following Articles shall be applicable to aid for hop producers which can be granted under Article 12 of Regulation (EEC) No 1696/71.
Article 2
1. Each Member State shall grant aid solely for areas planted to hops in its territory.
2. The aid shall be granted, on application by the producer, under conditions which ensure equality of treatment for the beneficiaries irrespective of the place of their establishment in the Community.
Article 3
1. Member States shall introduce a system of declarations and registration of areas planted to hops.
2. Member States shall arrange for the verification of: (a) the accuracy of declarations of areas planted to hops which are submitted by the producers,
(b) the areas harvested.
1 OJ No L 175, 4.8.1971, p. 1. 2 OJ No L 94, 28.4.1970, p. 13.
Producer groups shall not be appointed to carry out this verification.
Article 4
Areas planted to hops for which aid is requested must have a normal density of hop plants suitable for production.
Article 5
1. The aid provided for in Article 12 of Regulation (EEC) No 1696/71 is an intervention within the meaning of Article 3 (1) of Regulation (EEC) No 729/70.
2. Expenditure on the aid referred to in paragraph 1 shall be equal to amounts paid under Article 13 of Regulation (EEC) No 1696/71 and provisions pursuant thereto.
Article 6
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 18 May 1972.
For the Council
The President
M. MART
