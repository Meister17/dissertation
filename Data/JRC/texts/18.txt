*****
COUNCIL DIRECTIVE
of 21 June 1989
amending Directive 80/779/EEC on air quality limit values and guide values for sulphur dioxide and suspended particulates
(89/427/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 130s thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas the European Communities' 1973 (4), 1977 (5), 1983 (6) and 1987 (7) action programmes on the environment put the accent on harmonization of the action taken to protect the environment and on the need to reduce the concentrations of the principal pollutants in the air to levels considered acceptable for protecting sensitive ecosystems;
Whereas Council Directive 80/779/EEC (8), as last amended by the 1985 Act of Accession, offers a choice between two methods of sampling and analysis and two sets of associated limit values;
Whereas Article 10 (4) of the abovementioned Directive requires the submission between July 1987 and July 1988, of proposals on this parallel application of two different sets of measurement methods and limit values;
Whereas these proposals must take account of the results of the parallel measurements referred to in Article 10 (3) and of the need to avoid discriminatory provisions;
Whereas the results of the parallel measurements have shown that the limit values set out in Annex I and Annex IV to this Directive do not correspond as regards their stringency;
Whereas some Member States apply the limit values in Annex I and others those in Annex IV;
Whereas this has led to the use of different methods of sampling which are difficult to compare;
Whereas it is essential to harmonize the measurement methods and whereas, therefore, reference methods or technical specifications for the analysis and sampling of sulphur dioxide and suspended particulates in the air must be defined and finalized;
Whereas the Member States have taken measures to observe the limit values in the exempted zones as soon as possible and by 1 April 1993 at the latest;
Whereas these measures are based on one or other of the two measurement methods and associated values laid down by Directive 80/779/EEC;
Whereas the dual approach for measuring suspended particulates in the air causes discrimination between Member States;
Whereas a two-stage review is needed to draft proposals to avoid this dual approach without calling into question completion of the measures already taken by the Member States to observe the limit values;
Whereas these amendments must be taken into account in the obligations imposed by Article 3 of the said Directive on Member States applying Annex IV thereof,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Directive 80/779/EEC is hereby amended as follows:
1. Article 10 (1), (3) and (4) is replaced by the following:
'1. For the purposes of applying this Directive, Member States shall use either the reference methods of sampling and analysis referred to in Annex III for sulphur dioxide and for suspended particulates measured by the black smoke method or in Annex IV for suspended particulates measured by the gravimetric method, or any other method of sampling and analysis in respect of which they demonstrate to the Commission at regular intervals:
- either that it ensures satisfactory correlation of results with those obtained using the reference method,
- or that measurements taken in parallel with the reference method at a series of representative stations chosen in accordance with the requirements laid down in Article 6 show that there is a reasonably stable relationship between the results obtained using that method and those obtained using the reference method.'
'3. By way of derogation from Article 3, Member States which decide to avail themselves of the provisions of paragraph 2 shall:
- inform the Commission, before 1 January 1991, of any zones where they consider that there is a likelihood that the concentrations of sulphur dioxide and suspended particulates in the air might exceed the limit values set out in Annex IV after 1 January 1991,
- forward to the Commission, as from 1 April 1991, plans for the progressive improvement of air quality in those zones. These plans, drawn up on the basis of relevant information on the nature, origin and development of the pollution, shall describe in particular the measures taken or to be taken and the procedures implemented or to be implemented by the Member States concerned. These measures and procedures must bring the concentrations of sulphur dioxide and suspended particulates in the air in these zones to values below or equal to the limit values set out in Annex IV as soon as possible and by 1 April 1993 at the latest.
4. To overcome the disadvantages of the dual approach currently laid down in Annexes I and IV, which are not completely equivalent, by 31 December 1992 the Commission shall submit to the Council a proposal for a general review of this Directive. The proposal shall take account of the experience acquired from the studies referred to in paragraph 5 and of the results of subsequent measurements taken using technical specifications or reference methods for determining suspended particulates and sulphur dioxide. These technical specifications or reference methods must be finalized by the Commission, in agreement with the Member States, by 31 December 1990.
This proposal shall cover any other aspects in need of review in the light of scientific knowledge and of the experience acquired over the period of application of this Directive. It shall take account of, inter alia, aspects connected with the design of the networks for measuring air pollution and with the installation of measuring equipment as well as the quality and comparability of the measurements taken.'
2. Annex IV shall be amended in accordance with the Annex to this Directive.
Article 2
1. Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive within 18 months of its notification (1). They shall forthwith inform the Commission thereof.
2. Member States shall see to it that they communicate to the Commission the texts of the provisions of national law which they adopt in the field covered by this Directive.
Article 3
This Directive is addressed to the Member States.
Done at Luxembourg, 21 June 1989.
For the Council
The President
C. ARANZADI
(1) OJ No C 254, 30. 9. 1988, p. 6.
(2) OJ No C 96, 17. 4. 1989, p. 189.
(3) OJ No C 56, 6. 3. 1989, p. 6.
(4) OJ No C 112, 20. 12. 1973, p. 1.
(5) OJ No C 139, 13. 6. 1977, p. 1.
(6) OJ No C 46, 17. 2. 1983, p. 1.
(7) OJ No C 328, 7. 12. 1987, p. 1.
(8) OJ No L 229, 30. 8. 1980, p. 30.
(1) This Directive was notified to Member States on 11 July 1989.
ANNEX
Annex IV to Directive 80/779/EEC:
1. Table A shall is hereby replaced by the following:
'TABLE A
Limit values for sulphur dioxide expressed in mg/m3, and associated values for suspended particulates (measured by the gravimetric method) expressed in mg/m3
1.2.3 // // // // Reference period // Limit values for sulphur dioxide // Associated value for suspended particulates // // // // Year // 80 (median of daily mean values taken throughout the year) // 150 (median of daily mean values taken throughout the year) // // 120 (median of daily mean values taken throughout the year) // 150 (median of daily mean values taken throughout the year) // // // // Winter 1. 10. - 31. 3. // 130 (median of daily mean values taken throughout the winter) // 200 (median of daily mean values taken throughout the winter) // // 180 (median of daily mean values taken throughout the winter) // 200 (median of daily mean values taken throughout the winter) // // // // Year (made up of units of measuring periods of 24 hours) // 250 (1) (98th percentile of all daily mean values taken throughout the year) // 350 (98th percentile of all daily mean values taken throughout the year) // // 350 (1) (98th percentile of all daily mean values taken throughout the year) // 350 (98th percentile of all daily mean values taken throughout the year) // // //
(1) Member States must take all appropriate steps to ensure that this value is not exceeded for more than three consecutive days. Moreover, Member States must endeavour to prevent and to reduce any such instances in which this value has been exceeded.'
2. The whole of the text of the first indent of paragraph (i) is hereby replaced by the following:
'- Method of sampling:
The reference method in Annex III A.'
