Council Decision
of 20 December 2005
on the conclusion of an Agreement in the form of an Exchange of Letters between the European Community and the Hashemite Kingdom of Jordan concerning reciprocal liberalisation measures and amending the EC-Jordan Association Agreement as well as replacing Annexes I, II, III and IV and Protocols 1 and 2 to that Agreement
(2006/67/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 133, in conjunction with the first sentence of the first subparagraph of Article 300(2), thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) Article 15 of the Euro-Mediterranean Agreement establishing an Association between the European Communities and their Member States, of the one part, and the Hashemite Kingdom of Jordan, of the other part [1] ("the Association Agreement") in force since 1 May 2002, states that the Community and Jordan shall gradually implement greater liberalisation of their reciprocal trade in agricultural products. Point (c) of Article 10(1) states that the provisions applicable to agricultural products shall apply mutatis mutandis to the agricultural component of processed agricultural products. Article 17(1) provides that, from 1 January 2002, the Community and Jordan shall assess the situation with a view to determining the liberalisation measures to be applied by the Community and Jordan with effect from 1 January 2003, in accordance with the objective of greater trade liberalisation in agricultural products.
(2) By Recommendation No 1/2005 [2], the EU-Jordan Association Council endorsed an Action Plan of the European Neighbourhood Policy that includes a specific provision for further liberalisation of trade in agricultural and processed agricultural products.
(3) The Commission has, on behalf of the Community, negotiated an Agreement in the form of an Exchange of Letters with a view to inserting new Articles 11a and 14a and replacing Article 17(1) of the Association Agreement as well as to replacing Annexes I, II, III and IV and Protocols Nos 1 and 2 to that Agreement.
(4) The Agreement in the form of an Exchange of Letters, initialled on 23 June 2005, should be approved.
(5) The measures necessary for the implementation of this Decision should be adopted in accordance with Council Decision 1999/468/EC of 28 June 1999 laying down the procedures for the exercise of implementing powers conferred on the Commission [3],
HAS DECIDED AS FOLLOWS:
Article 1
The Agreement in the form of an Exchange of Letters between the European Community and the Hashemite Kingdom of Jordan concerning reciprocal liberalisation measures and amending the EC-Jordan Association Agreement as well as replacing Annexes I, II, III and IV and Protocols Nos 1 and 2 to that Agreement, is hereby approved on behalf of the Community.
The text of the Agreement in the form of an Exchange of Letters is attached to this Decision.
Article 2
The measures necessary for the implementation of Protocols Nos 1 and 2 shall be adopted in accordance with the procedure referred to in Article 3(2).
Article 3
1. The Commission shall be assisted by the Management Committee for Sugar established by Article 42 of Regulation (EC) No 1260/2001 [4] or, where appropriate, by the committees established by the corresponding provisions of other regulations on the common organisation of markets or by the Customs Code Committee established by Article 248a of Regulation (EEC) No 2913/92 [5] (hereinafter referred to as "the Committee").
2. Where reference is made to this paragraph, Articles 4 and 7 of Decision 1999/468/EC shall apply.
The period laid down in Article 4(3) of Decision 1999/468/EC shall be set at one month.
3. The Committee shall adopt its Rules of Procedure.
Article 4
The President of the Council is hereby authorised to designate the person(s) empowered to sign the Agreement in the form of an Exchange of Letters in order to bind the Community.
Article 5
This Decision shall be published in the Official Journal of the European Union.
Done at Brussels, 20 December 2005.
For the Council
The President
M. Beckett
[1] OJ L 129, 15.5.2002, p. 3.
[2] OJ L 228, 3.9.2005, p. 10.
[3] OJ L 184, 17.7.1999, p. 23.
[4] OJ L 178, 30.6.2001, p. 1. Regulation as last amended by Commission Regulation (EC) No 39/2004 (OJ L 6, 10.1.2004, p. 16).
[5] OJ L 302, 19.10.1992, p. 1. Regulation as last amended by Regulation (EC) No 648/2005 of the European Parliament and of the Council (OJ L 117, 4.5.2005, p. 13).
--------------------------------------------------
