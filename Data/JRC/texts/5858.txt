Adoption of two reference documents for the purpose of Council Directive 96/61/EC concerning integrated pollution prevention and control [1]
(2005/C 107/05)
On 15 April 2005, the Commission adopted the complete texts of the reference documents on:
- best available techniques in smitheries and foundries,
- best available techniques in slaughterhouses and animal by-products industries.
These documents are available on the Internet site http://eippcb.jrc.es.
[1] OJ L 257 of 10.10.1996, p. 26.
--------------------------------------------------
