Council Joint Action 2006/468/CFSP
of 5 July 2006
renewing and revising the mandate of the Special Representative of the European Union for Sudan
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty on European Union and, in particular Articles 14, 18(5) and 23(2) thereof,
Whereas
(1) On 18 July 2005 the Council adopted Joint Action 2005/556/CFSP appointing a Special Representative of the European Union for Sudan [1].
(2) The European Union has been actively involved at diplomatic and political level since the beginnings of the international efforts to contain and resolve the Darfur crisis.
(3) The Union wishes to strengthen its political role in a crisis with a multitude of local, regional and international actors and to maintain coherence between the Union's assistance to the crisis management in Darfur, led by the African Union (AU), on the one hand, and overall political relations with Sudan, including implementation of the Comprehensive Peace Agreement (CPA) between the Government of Sudan and the Sudan Peoples Liberation Movement/Army (SPLM/A), on the other.
(4) On 5 May 2006, the Darfur Peace Agreement (DPA) was concluded in Abuja by the Government of Sudan and the Sudan Liberation Movement/Army (SLM/A). The Union will work for the full and rapid implementation of the DPA as a precondition for lasting peace and security and an end to the suffering of millions of the people in Darfur. The functions of the Special Representative of the European Union (EUSR) should take full account of the role of the EU as regards the implementation of the DPA, including in relation to the Darfur-Darfur Dialogue and Consultation process.
(5) The Union has provided a significant amount of assistance to the AU mission in the Darfur region of Sudan (AMIS) in terms of planning and management support, funding and logistics.
(6) The AU has stated the need to increase significantly the strength of AMIS in light of the additional tasks to be performed by the Mission with respect to the implementation of the DPA, implying the enhancement of AMIS in terms of additional military and civilian police personnel, logistics and overall capacity. On 15 May 2006 the Council agreed to extend the European Union civilian-military supporting action to the African Union mission in the Darfur region of Sudan. Commensurate political engagement with the AU and the Government of Sudan, and specific coordination capacity, therefore continues to be required.
(7) On 31 March 2005 the UN Security Council adopted Resolution 1593(2005) on the report of the International Commission of Inquiry on violations of international humanitarian law and human rights law in Darfur.
(8) The establishment of a permanent presence in Khartoum would allow for a strengthening of the contacts of the EUSR with the Government of Sudan, the Sudanese political parties, the AMIS Mission Headquarters, the United Nations and its agencies, and diplomatic missions, as well as a closer monitoring of, and participation in, the activities of the Assessment and Evaluation Committee and related working groups or commissions. It would also make it possible to follow more closely the situation in Eastern Sudan as well as to maintain regular contacts with the Government of South Sudan and the SPLM.
(9) The mandate of the EUSR for Sudan should therefore be revised and renewed, and its duration aligned with the mandates of other EUSRs. Consequently, Joint Action 2005/556/CFSP should be repealed.
(10) The EUSR will implement his mandate in the context of a situation which may deteriorate and could harm the objectives of the CFSP as set out in Article 11 of the Treaty,
HAS ADOPTED THIS JOINT ACTION:
Article 1
The mandate of Mr Pekka HAAVISTO as European Union Special Representative (EUSR) for Sudan shall be renewed until 28 February 2007.
Article 2
The mandate of the EUSR shall be based on the policy objectives of the European Union in Sudan, notably as regards:
(a) efforts, as part of the international community and in support of the African Union (AU) and the UN, to assist the Sudanese parties, the AU and the UN to implement the Darfur Peace Agreement (DPA) as well as to facilitate the implementation of the Comprehensive Peace Agreement (CPA) and to promote South-South dialogue, with due regard to the regional ramifications of these issues and to the principle of African ownership; and
(b) ensuring maximum effectiveness and visibility of the Union's contribution to the AU mission in the Darfur region of Sudan (AMIS).
Article 3
1. In order to achieve the policy objectives the EUSR's mandate shall be to:
(a) liaise with the AU, the Government of Sudan, the Darfur armed movements and other Sudanese parties as well as non-governmental organisations and maintain close collaboration with the UN and other relevant international actors, with the aim of pursuing the Union's policy objectives;
(b) represent the Union at the Darfur-Darfur dialogue, at high-level meetings of the Joint Commission, as well as other relevant meetings as requested;
(c) represent the Union, whenever possible, at the CPA and DPA Assessment and Evaluation Commissions;
(d) follow developments regarding talks between the Government of the Sudan and the Eastern Front, and represent the Union at such talks, if requested by the parties and the mediation;
(e) ensure coherence between the Union's contribution to crisis management in Darfur and the overall political relationship of the Union with Sudan;
(f) with regard to human rights, including the rights of children and women, and the fight against impunity in Sudan, follow the situation and maintain regular contacts with the Sudanese authorities, the AU and the UN, in particular with the Office of the High Commissioner for Human Rights, the human rights observers active in the region and the Office of the Prosecutor of the International Criminal Court.
2. For the purpose of the fulfilment of his mandate, the EUSR shall, inter alia:
(a) maintain an overview of all activities of the Union;
(b) ensure coordination and coherence of the Union's contributions to AMIS;
(c) support the political process and activities relating to the implementation of the CPA and the DPA; and
(d) follow up and report on compliance by the Sudanese parties with the relevant UN Security Council Resolutions, notably 1556(2004), 1564(2004), 1591(2005), 1593(2005), 1672(2006) and 1679(2006).
Article 4
1. The EUSR shall be responsible for the implementation of the mandate acting under the authority and operational direction of the Secretary-General/High Representative (SG/HR). The EUSR shall be accountable to the Commission for all expenditure.
2. The Political and Security Committee (PSC) shall maintain a privileged link with the EUSR and shall be the primary point of contact with the Council. The PSC shall provide the EUSR with strategic guidance and political input within the framework of the mandate.
3. The EUSR shall regularly report to the PSC on the situation in Darfur, in particular on the implementation of the Darfur Peace Agreement and of the Union's assistance to AMIS, as well as on the situation in Sudan as a whole.
Article 5
1. The financial reference amount intended to cover the expenditure related to the mandate of the EUSR in the period from 18 July 2006 until 28 February 2007 shall be EUR 1030000.
2. The expenditure financed by the amount stipulated in paragraph 1 shall be managed in accordance with the procedures and rules applicable to the budget of the European Union with the exception that any pre-financing shall not remain the property of the Community.
3. The management of the expenditure shall be subject to a contract between the EUSR and the Commission. Expenditure shall be eligible as from 18 July 2006.
4. The Presidency, the Commission, and/or Member States, as appropriate, shall provide logistical support in the region.
Article 6
1. Within the limits of his mandate and the corresponding financial means made available, the EUSR shall be responsible for constituting his team in consultation with the Presidency, assisted by the Secretary-General/High Representative, and in full association with the Commission. The EUSR shall inform the Presidency and the Commission of the final composition of his team.
2. Member States and institutions of the European Union may propose the secondment of staff to work with the EUSR. The remuneration of personnel who might be seconded by a Member State or an institution of the European Union to the EUSR shall be covered by the Member State or the institution of the European Union concerned respectively.
3. All A-type posts which are not covered by secondment shall be advertised as appropriate by the General Secretariat of the Council and notified to Member States and institutions of the European Union in order to recruit the bestqualified applicants.
4. The privileges, immunities and further guarantees necessary for the completion and smooth functioning of the mission of the EUSR and the members of his staff shall be defined with the parties. Member States and the Commission shall grant all necessary support to such effect.
Article 7
1. In the coordination of the Union's contributions to AMIS, the EUSR shall be assisted by the ad hoc Coordination Cell (EUSR Office) established in Addis Ababa, acting under his authority, as referred to in Article 5(2) of Joint Action 2005/557/CFSP of 18 July 2005 on the European Union civilian-military supporting action to the African Union mission in the Darfur region of Sudan [2].
2. The EUSR Office in Addis Ababa shall comprise a political advisor, a senior military advisor and a police advisor.
3. The police and military advisors in the EUSR Office shall act as advisors to the EUSR respectively regarding the police and military components of the Union's supporting action referred to in paragraph 1. In that capacity, they shall report to the EUSR.
4. The police and military advisors shall not receive instructions from the EUSR regarding the management of expenditure in relation respectively to the police and military components of the Union's supporting action referred to in paragraph 1. The EUSR shall bear no responsibility in this respect.
5. An Office of the EUSR shall be established in Khartoum, comprising a Political Advisor and the necessary administrative and logistic support staff. The Office in Khartoum shall draw on the technical expertise of the EUSR Office in Addis Ababa regarding military and police matters, whenever required.
Article 8
As a rule, the EUSR shall report in person to the SG/HR and to the PSC and may report also to the relevant Working Group. Regular written reports shall be circulated to the SG/HR, the Council and the Commission. The EUSR may report to the General Affairs and External relations Council on the recommendation of the SG/HR and the PSC.
Article 9
To ensure the consistency of the external action of the European Union, the activities of the EUSR shall be coordinated with those of the SG/HR, the Presidency and the Commission. The EUSR shall provide regular briefings to Member States' missions and Commission delegations. In the field, close liaison shall be maintained with the Presidency, the Commission and Heads of Mission who shall make best efforts to assist the EUSR in the implementation of the mandate. The EUSR shall also liaise with other international and regional actors in the field.
Article 10
The implementation of this Joint Action and its consistency with other contributions from the Union to the region shall be kept under regular review. The EUSR shall present to the SG/HR, Council and Commission a comprehensive mandate implementation report by mid-November 2006. This report shall form a basis for evaluation of this Joint Action in the relevant Working Groups and by the PSC. In the context of overall priorities for deployment, the SG/HR shall make recommendations to the PSC concerning the Council's decision on renewal, amendment or termination of the mandate.
Article 11
This Joint Action shall enter into force on the day of its adoption. It shall apply as from 18 July 2006.
Joint Action 2005/556/CFSP is hereby repealed with effect from 18 July 2006.
Article 12
This Joint Action shall be published in the Official Journal of the European Union.
Done at Brussels, 5 July 2006.
For the Council
The President
P. Lehtomäki
[1] OJ L 188, 20.7.2005, p. 43.
[2] OJ L 188, 20.7.2005, p. 46.
--------------------------------------------------
