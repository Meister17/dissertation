Commission Decision
of 29 August 2003
laying down the transitional measures to be applied by Hungary with regard to veterinary checks on products of animal origin from Romania
(notified under document number C(2003) 3074)
(Text with EEA relevance)
(2003/630/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 97/78/EC of 18 December 1997 laying down the principles governing the organisation of veterinary checks on products entering the Community from third countries(1), as amended by Annex II(6)(B)(1)(53)(b) to the Act of Accession of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia, and in particular Article 21 thereof,
Having regard to the Act concerning the conditions of accession of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia to the European Union, and to the adjustments to the Treaties on which the European Union is founded, and in particular Article 42 thereof,
Whereas:
(1) Hungary has been granted a transitional period of three years for certain aspects of the veterinary checks regime concerning the standards for the facilities required at the border with Romania, for the checks on products of animal origin.
(2) This provision is limited only to the requirements for facilities, and all other aspects of the veterinary checks procedures should be carried out according to EU requirements.
(3) Provisions should therefore be made to identify the border inspection post where products of animal origin coming from Romania could be checked at the border with Hungary, and to provide appropriate derogation to the requirements applicable to the inspection facilities for products of animal origin at this border inspection post.
(4) The derogation to the separation rule applicable to low throughput border inspection posts provided for in Article 4(5) of Commission Decision 2001/812/EC(2) should apply irrespective of the maximum number of consignments set by this derogation.
(5) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
Products of animal origin coming by road transport from Romania must enter the territory of Hungary through the border inspection post listed in the Annex.
Article 2
Article 4(5) of Decision 2001/812/EC shall apply to the border inspection post listed in the Annex, without limit on the number of consignments passing through that border inspection post.
Article 3
This Decision shall take effect subject to and on the date of entry into force of the Act of Accession.
Article 4
This Decision is applicable until 30 April 2007.
Article 5
This Decision is addressed to the Member States.
Done at Brussels, 29 August 2003.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 24, 30.1.1998, p. 9.
(2) OJ L 306, 23.11.2001, p. 28.
ANNEX
Border inspection posts on the Hungarian-Romanian border
Nagylak
