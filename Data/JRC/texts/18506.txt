REGULATION (EEC) No 2988/74 OF THE COUNCIL of 26 November 1974 concerning limitation periods in proceedings and the enforcement of sanctions under the rules of the European Economic Community relating to transport and competition
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Articles 75, 79 and 87 thereof;
Having regard to the proposal from the Commission;
Having regard to the Opinion of the European Parliament (1);
Having regard to the Opinion of the Economic and Social Committee (2);
Whereas under the rules of the European Economic Community relating to transport and competition the Commission has the power to impose fines, penalties and periodic penalty payments on undertakings or associations of undertakings which infringe Community law relating to information or investigation, or to the prohibition on discrimination, restrictive practices and abuse of dominant position ; whereas those rules make no provision for any limitation period;
Whereas it is necessary in the interests of legal certainty that the principle of limitation be introduced and that implementing rules be laid down ; whereas, for the matter to be covered fully, it is necessary that provision for limitation be made not only as regards the power to impose fines or penalties, but also as regards the power to enforce decisions, imposing fines, penalties or periodic penalty payments ; whereas such provisions should specify the length of limitation periods, the date on which time starts to run and the events which have the effect of interrupting or suspending the limitation period ; whereas in this respect the interests of undertakings and associations of undertakings on the one hand, and the requirements imposed by administrative practice, on the other hand, should be taken into account;
Whereas this Regulation must apply to the relevant provisions of Regulation No 11 concerning the abolition of discrimination in transport rates and conditions, in implementation of Article 79 (3) of the Treaty (3) establishing the European Economic Community, of Regulation No 17 (4) : first Regulation implementing Articles 85 and 86 of the Treaty, and of Council Regulation (EEC) No 1017/68 (5) of 19 July 1968 applying rules of competition to transport by rail, road and inland waterway ; whereas it must also apply to the relevant provisions of future regulations in the fields of European Economic Community law relating to transport and competition,
HAS ADOPTED THIS REGULATION:
Article 1
Limitation periods in proceedings
1. The power of the Commission to impose fines or penalties for infringements of the rules of the European Economic Community relating to transport or competition shall be subject to the following limitation periods: (a) three years in the case of infringements of provisions concerning applications or notifications of undertakings or associations of undertakings, requests for information, or the carrying out of investigations;
(b) five years in the case of all other infringements.
2. Time shall begin to run upon the day on which the infringement is committed. However, in the case of continuing or repeated infringements, time shall begin to run on the day on which the infringement ceases. (1)OJ No C 129, 11.12.1972, p. 10. (2)OJ No C 89, 23.8.1972, p. 21. (3)OJ No 52, 16.8.1960, p. 1121/60. (4)OJ No 13, 21.2.1962, p. 204/62. (5)OJ No L 175, 23.7.1968, p. 1.
Article 2
Interruption of the limitation period in proceedings
1. Any action taken by the Commission, or by any Member State, acting at the request of the Commission, for the purpose of the preliminary investigation or proceedings in respect of an infringement shall interrupt the limitation period in proceedings. The limitation period shall be interrupted with effect from the date on which the action is notified to at least one undertaking or association or undertakings which have participated in the infringement.
Actions which interrupt the running of the period shall include in particular the following: (a) written requests for information by the Commission, or by the competent authority of a Member State acting at the request of the Commission ; or a Commission decision requiring the requested information;
(b) written authorizations to carry out investigations issued to their officials by the Commission or by the competent authority of any Member State at the request of the Commission ; or a Commission decision ordering an investigation;
(c) the commencement of proceedings by the Commission;
(d) notification of the Commission's statement of objections.
2. The interruption of the limitation period shall apply for all the undertakings or associations of undertakings which have participated in the infringement.
3. Each interruption shall start time running afresh. However, the limitation period shall expire at the latest on the day on which a period equal to twice the limitation period has elapsed without the Commission having imposed a fine or a penalty ; that period shall be extended by the time during which limitation is suspended pursuant to Article 3.
Article 3
Suspension of the limitation period in proceedings
The limitation period in proceedings shall be suspended for as long as the decision of the Commission is the subject of proceedings pending before the Court of Justice of the European Communities.
Article 4
Limitation period for the enforcement of sanctions
1. The power of the Commission to enforce decisions imposing fines, penalties or periodic payments for infringements of the rules of the European Economic Community relating to transport or competition shall be subject to a limitation period of five years.
2. Time shall begin to run on the day on which the decision becomes final.
Article 5
Interruption of the limitation period for the enforcement of sanctions
1. The limitation period for the enforcement of sanctions shall be interrupted: (a) by notification of a decision varying the original amount of the fine, penalty or periodic penalty payments or refusing an application for variation;
(b) by any action of the Commission, or of a Member State at the request of the Commission, for the purpose of enforcing payments of a fine, penalty or periodic penalty payment.
2. Each interruption shall start time running afresh.
Article 6
Suspension of the limitation period for the enforcement of sanctions
The limitation period for the enforcement of sanctions shall be suspended for so long as: (a) time to pay is allowed ; or
(b) enforcement of payment is suspended pursuant to a decision of the Court of Justice of the European Communities.
Article 7
Application to transitional cases
This Regulation shall also apply in respect of infringements committed before it enters into force.
Article 8
Entry into force
This Regulation shall enter into force on 1 January 1975.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 26 November 1974.
For the Council
The President
J. LECANUET
