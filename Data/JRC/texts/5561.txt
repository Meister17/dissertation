Commission Decision
of 27 October 2005
laying down rules for the procurement of food aid by NGOs authorised by the Commission to purchase and mobilise products to be supplied under Council Regulation (EC) No 1292/96 and repealing its Decision of 3 September 1998
(2005/769/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European community,
Having regard to Council Regulation (EC) No 1292/96 of 27 June 1996 on food-aid policy and food-aid management and special operations in support of food security [1], and in particular Article 19(1) thereof,
Whereas:
(1) Article 3(1) of Commission Regulation (EC) No 2519/97 of 16 December 1997 laying down general rules for the mobilisation of products to be supplied under Council Regulation (EC) No 1292/96 as Community food aid [2], allows the Commission to authorise international and non-governmental organisations which are beneficiaries of Community aid to purchase and mobilise the products for use as aid supplies themselves, provided that the Commission establishes the rules and procedures which shall then apply.
(2) Article 164 of Commission Regulation (EC, Euratom) No 2342/2002 of 23 December 2002 laying down detailed rules for the implementation of Council Regulation (EC, Euratom) No 1605/2002 on the Financial Regulation applicable to the general budget of the European Communities [3] (the implementing rules), provides that where the implementation of an action for which a Community grant may be received involves procurement, the grant agreement concluded for that purpose should include the procurement rules with which the beneficiary must comply.
(3) Article 120 of Council Regulation (EC, Euratom) No 1605/2002 of 25 June 2002 on the Financial Regulation applicable to the general budget of the European Communities [4] (Financial Regulation) subjects the award of procurement contracts by the beneficiary of a grant to the principles set out in the Financial Regulation and its Implementing Rules.
(4) The procurement rules to be followed by the bodies identified in part 2 of the Annex to Regulation (EC) No 1292/96 for the implementation of food aid policy are already laid down in the contribution agreements concluded by the Commission with those international organisations for that purpose; for non-governmental organisations (NGOs), the procurement rules and other conditions which are necessary for the mobilisation of food aid and for respecting the financial principles laid down in the Financial Regulation and its implementing rules should be based in particular on those established by Regulation (EC) No 2519/97, adapted as necessary to take into account the financial management situation.
(5) The procurement rules should apply where the Commission authorises NGOs to purchase and mobilise food aid in the framework of the contracts to be signed for implementing the annual food aid work programme, without prejudice to the discretion for the Commission’s authorising officer to include in such contracts additional requirements for sound financial management purposes. Commission Decision of 3 September 1998 should therefore be repealed.
(6) Pursuant to Article 29 of Regulation (EC) No 1292/96, the Food Aid and Food Security Committee has been informed of the present measure,
HAS DECIDED AS FOLLOWS:
Article 1
The rules for the procurement of food aid by non-governmental organisations authorised by the Commission to purchase and mobilise products to be supplied under Regulation (EC) No 1292/96 are laid down in the Annex to this Decision. Those rules shall form an integral part of the contracts and conventions concluded by the Commission for that purpose.
Article 2
The Commission Decision of 3 September 1998 authorising certain organisations receiving Community food aid to purchase certain products for supply as Community food aid themselves is repealed.
Article 3
This Decision takes effect on the date of its publication.
Done at Brussels, 27 October 2005.
For the Commission
Louis Michel
Member of the Commission
[1] OJ L 166, 5.7.1996, p. 1. Regulation as last amended by Regulation (EC) No 1882/2003 of the European Parliament and of the Council (OJ L 284, 31.10.2003, p. 1).
[2] OJ L 346, 17.12.1997, p. 23.
[3] OJ L 357, 31.12.2002, p. 1.
[4] OJ L 248, 16.9.2002, p. 1.
--------------------------------------------------
ANNEX
The non-governmental organisation beneficiary of Community aid (hereinafter the NGO) shall apply the following rules for the mobilisation of products to be supplied under Regulation (EC) No 1292/96 as Community food aid, without prejudice to any additional financial management requirements included in the contract concluded with the beneficiary for implementing food aid policy.
I. GENERAL PRINCIPLES
This Annex shall apply to goods to be supplied "free at destination".
II. PLACE OF PURCHASE OF THE GOODS
Depending on the conditions laid down for a particular supply, the product to be supplied shall be purchased in the European Community, or a developing country listed in the Annex to Regulation (EC) No 1292/96, belonging if possible to the same geographical region. To the extent possible, priority should be given to purchases in the country of operation or a neighbouring country.
In exceptional circumstances and in accordance with the procedures laid down in Article 11(2) of Regulation (EC) No 1292/96, products may be purchased on the market of a country other than those listed in the Annex to Regulation (EC) No 1292/96.
The NGO shall ensure that the products to be supplied as food aid can be freely imported in the beneficiary country and will not be subject to any import duty or tax having equivalent effect.
III. CHARACTERISTICS OF THE PRODUCTS
The products shall as much as possible match the nutritional habits of the beneficiary population.
The characteristics of the products to be mobilised as food aid shall be consistent with the requirements laid down in the Communication from the Commission relating to the characteristics of products to be supplied as Community food aid [1].
Furthermore, packaging shall be consistent with the requirements laid down in the Communication from the Commission relating to the packaging of products to be supplied as Community food aid [2].
IV. NATIONALITY RULES
Participation in the tender procedures provided for in the framework of the mobilisation of products to be supplied as food aid shall be open on equal terms to any natural or legal person from the European Community or from a developing country listed in the Annex to Regulation (EC) No 1292/96.
The tenderer must be legally registered and able to show proof of it on request.
V. GROUNDS FOR EXCLUSION FROM PARTICIPATION IN PROCUREMENT PROCEDURES AND FROM AWARD OF CONTRACTS
1. Grounds for exclusion from participation in procurement procedures
Tenderers are excluded from participation in a procurement procedure if:
(a) they are bankrupt or being wound up, are having their affairs administered by the courts, have entered into an arrangement with creditors, have suspended business activities, are the subject of proceedings concerning those matters, or are in any analogous situation arising from a similar procedure provided for in national legislation or regulations;
(b) they have been convicted of an offence concerning their professional conduct by a judgment which has the force of res judicata;
(c) they have been guilty of grave professional misconduct proven by any means which the beneficiary of the grant can justify;
(d) they have not fulfilled obligations relating to the payment of social security contributions or the payment of taxes in accordance with the legal provisions of the country in which they are established or with those of the country of the grant beneficiary or those of the country where the contract is to be performed;
(e) they have been the subject of a judgment which has the force of res judicata for fraud, corruption, involvement in a criminal organisation or any other illegal activity detrimental to the Communities’ financial interests;
(f) following another procurement procedure or grant award procedure financed by the Community budget, they have been declared to be in serious breach of contract for failure to comply with their contractual obligations.
Tenderers must certify that they are not in one of the situations listed above.
2. Exclusion from award of contracts
Contracts may not be awarded to tenderers who, during the procurement procedure:
(a) are subject to a conflict of interest;
(b) are guilty of misrepresentation in supplying the information required by the beneficiary of the grant as a condition of participation in the contract procedure or fail to supply this information.
VI. AWARD PROCEDURES
1. General provisions
The NGO shall launch an international open invitation to tender for supply contracts with a value of EUR 150000 or more. In the case of an international open invitation to tender, the NGO shall publish a tender notice in all appropriate media, in particular on the NGO’s web site, in the international press and the national press of the country in which the Action is being carried out, or in other specialist periodicals.
Supply contracts for a value of EUR 30000 or more but less than EUR 150000 shall be awarded by means of an open tender procedure published locally. In the case of a local open tender procedure, the tender notice shall be published in all appropriate media but only in the country in which the Action is being carried out. It must however provide other eligible suppliers with the same opportunities as local firms.
Supply contracts with a value of less than EUR 30000 must be awarded by means of a competitive negotiated procedure without publication, in which the NGO consults at least three suppliers of its choice and negotiates the terms of the contract with one or more of them.
Supply contracts with a value of less than EUR 5000 may be awarded on the basis of a single tender.
The time-limits for receipt of tenders and requests to participate must be long enough to allow interested parties a reasonable and appropriate period to prepare and submit their tenders.
2. Negotiated procedure
The beneficiary may use the negotiated procedure on the basis of a single tender in the following cases:
(a) where, for reasons of extreme urgency brought about by events which the beneficiary could not have foreseen and which can in no way be attributed to him, the time-limit for the procedures referred to in section VI.1 above cannot be kept. The circumstances invoked to justify extreme urgency must in no way be attributable to the beneficiary.
Actions carried out in crisis situations identified by the Commission are considered to satisfy the test of extreme urgency. The Commission will inform the beneficiary if a crisis situation exists and when it comes to an end;
(b) for additional deliveries by the original supplier intended either as a partial replacement of normal supplies or installations or as the extension of existing supplies or installations, where a change of supplier would oblige the beneficiary to acquire equipment having different technical characteristics which would result in either incompatibility or disproportionate technical difficulties in operation and maintenance;
(c) where the tender procedure has been unsuccessful, that is where no qualitatively and/or financially worthwhile tender has been received. In such cases, after cancelling the tender procedure, the beneficiary may negotiate with one or more tenderers of its choice, from among those that took part in the tender procedure, provided that the initial terms of the tender procedure are not substantially altered;
(d) where the contract concerned be awarded to bodies with a de jure or de facto monopoly, duly substantiated in the Commission’s award decision;
(e) a direct agreement contract may be undertaken where warranted by the particular conditions of a supply and, in particular, in the case of an experimental supply.
3. Obligations for the submission of a tender
The NGO shall specify in the tender notice the form and the deadline according to which the tenderer’s bid must be made.
All requests to participate and tenders declared as satisfying the requirements must be evaluated and ranked by an evaluation committee on the basis of the exclusion, selection and award criteria announced in advance. This committee must have an odd number of members, at least three, with all the technical and administrative capacities necessary to give an informed opinion on the tenders.
One single tender may be submitted for each lot. It shall be valid only if it relates to a complete lot. Where a lot is subdivided into part lots, the tender shall be established as an average thereof. Where the invitation to tender relates to the supply of more than one lot, a separate tender shall be submitted per lot. The tenderer is not obliged to present a tender for all the lots.
Tenders shall provide:
- the tenderer’s name and address,
- the reference numbers of the invitation to tender, lot and action,
- the net weight of the lot or the specific monetary amount to which the tender relates,
- the proposed price per net metric tonne of product at which the tenderer undertakes to carry out the supply in accordance with the conditions laid down;
or
- where the invitation to tender is for a contract to supply a maximum quantity of a given product for a specific monetary amount, the net quantity of products offered,
- the transport costs for the specified delivery stage,
- the delivery deadline.
The tender shall be valid only if it is accompanied by evidence that a tendering guarantee has been lodged. The amount of the tendering guarantee, expressed in the currency of the payment, and the period of validity, shall be laid down in the tender notice. The guarantee shall represent minimum 1 % of the total amount of the bid, and the period of validity shall be at least one month.
The guarantee shall be lodged in favour of the NGO in the form of a security from a credit establishment recognised by a Member State or accepted by the NGO. The guarantee shall be irrevocable and capable of being called at first request.
In case of mobilisation in the country which is itself the beneficiary of the food aid, the NGO may define in the tender notice other conditions for the guarantee taking account of the customs of the country.
The guarantee shall be released:
- by a letter or a fax by the NGO where the tender has not been accepted or has been rejected, or where no contract has been awarded,
- where the tenderer, designated as the supplier, has lodged the delivery guarantee.
The guarantee shall be forfeited if the supplier fails to provide the delivery guarantee within a reasonable deadline following the award of the contract and also if the tenderer withdraws his tender after it has been received.
A tender which is not submitted in accordance with these provisions or contains reservations or conditions other than those laid down for in the invitation to tender shall be rejected.
No tender may be changed or withdrawn after it has been received.
The award shall be made to the tenderer who submitted the lowest tender respecting all the conditions of the invitation to tender, in particular the characteristics of the products to be mobilised. Where the lowest tender is presented simultaneously by a number of tenderers, the contract shall be awarded by the drawing of lots.
When the contract is awarded, both the supplier and the unsuccessful tenderers shall be duly notified by letter or fax.
The NGO may decide not to award the contract on the expiry of the first or of the second dead-line, in particular where the tenders submitted are outside the range of normal market prices. The NGO shall not be required to give reasons for its decision. Tenderers shall be informed of the decision not to award the contract by written notice, within three working days.
VII. OBLIGATIONS OF THE SUPPLIER AND CONDITIONS OF SUPPLY
The NGO shall specify in the tender notice the conditions relevant to the responsibilities of the supplier under the present rules, and the supplier shall perform his obligations in accordance with all conditions laid down in the tender notice as well as those arising from his tender.
The supplier shall arrange transport at his own expense by the route most appropriate having regard to the approved deadline, from the port of shipment or loading quay indicated in his tender to the final place of destination specified in the tender notice.
However, at the supplier’s written request the NGO may authorise the port of shipment or the loading quay to be changed, provided any costs this entails are borne by the supplier.
The supplier shall take out a maritime insurance policy or claim cover under a general policy. The insurance shall be for at least the tender amount and shall cover all risks associated with carriage and any other supply-related activity by the supplier up to the stage of delivery specified. It shall also cover all costs of sorting, withdrawal or destruction of damaged goods, repacking and analysis of goods where an average does not preclude their acceptance by the beneficiary.
The goods may not be delivered in split consignments on more than one vessel, unless the NGO so agrees. In that case, the NGO shall require the supplier to bear the additional checking costs.
Where appropriate, the tender notice may specify a date before which any delivery will be considered premature.
The supply shall be complete when all the goods have actually been delivered "free at destination". The supplier shall bear all the costs until the goods are made available at the warehouse of destination.
The supplier shall bear all risks, including loss or deterioration, to which the goods may be subject until completion of the supply and recording of that fact by the monitor in the final certificate of conformity (see point VIII).
The supplier shall notify the beneficiary and the monitor promptly in writing of the means of transport used, the loading dates, the expected date of arrival at destination, and any incident occurring while the goods are in transit.
The supplier shall carry out the formalities relating to the export license and customs clearance, bearing the related costs and charges.
In order to ensure that he meets his obligations, the supplier shall lodge a delivery guarantee within a reasonable deadline following the notification of the award of the contract. That guarantee, expressed in the currency of the payment, shall represent 5 to 10 % of the total amount of the tender. The period of validity shall end one month after the date of the final delivery. It shall be lodged in the same way as the tendering guarantee.
The delivery guarantee shall be released in full by a letter or a fax by the NGO when the supplier:
- has carried out the supply in compliance with all his obligations, or
- has been released from his obligations;
or
- has not carried out the supply for reasons of force majeure recognised by the NGO.
VIII. MONITORING
As soon as the contract has been awarded, the NGO shall inform the supplier of the agency which will be responsible for verifying and certifying the quality, quantity, packing and marking of the goods to be delivered in respect of each supply, issuing the certificate of conformity or the certificate of delivery, and generally coordinating all stages of the supply operation (hereinafter referred to as the monitoring agency).
After the notification of the award of the contract, the supplier shall inform in writing the monitoring agency of the name and address of the manufacturer, packer or stockholder of the goods to be delivered, and the approximate date of manufacture or packaging, as well as of the name of his representative at the place of delivery.
The monitoring agency shall carry out at least two checks, based on terms of reference complying with international monitoring standards, as follows:
(a) a provisional check shall be carried out when the goods are loaded or at the factory. The final check shall be carried out at the delivery stage specified;
(b) when the provisional check is complete, the monitoring agency shall issue a provisional certificate of conformity to the supplier, subject to reservation if necessary. The monitoring agency shall state whether any reservation is such as to render the goods unacceptable at the delivery stage;
(c) when the final check is complete, the monitoring agency shall issue a final certificate of conformity to the supplier specifying in particular the date of completion of the supply and the net quantity supplied; such certificate shall be subject to reservations if necessary;
(d) where the monitoring agency issues a reasoned "notice of reservation", it shall notify the supplier and the NGO in writing as soon as possible. If the supplier wishes to dispute the findings with the monitoring agency and the NGO, he shall do so within two working days of dispatch of this notice.
The costs of the checks referred to above shall be borne by the NGO. The supplier shall bear any financial consequences in the event of qualitative shortcomings or late presentation of the goods for checking.
If the supplier or the beneficiary objects to the findings of a check, the monitoring agency, after authorisation of the NGO, shall arrange for a review inspection involving, according to the nature of the objection, a review sampling, review analysis, and/or a reweighing or rechecking of the packaging. The review inspection shall be carried out by an agency or laboratory designated by agreement between the supplier, the final beneficiary and the monitoring agency.
The costs of this review inspection shall be borne by the losing party.
If the final certificate of conformity is not issued after the checks or review inspection has been carried out, the supplier shall be obliged to replace the goods.
The replacement and related checks’ costs shall be borne by the supplier.
The monitoring agency shall issue written invitations to the representatives of the supplier and of the final beneficiary to be present at the checking operations, in particular for the taking of samples to be used for analyses. The taking of samples shall be carried out in accordance with professional practice. When sampling is undertaken, the monitoring agency shall take two additional samples which shall be kept under seal at the NGO’s disposal for the purpose of any further check or in the event of objections being raised by the beneficiary or supplier.
The cost of the goods taken as samples shall be borne by the supplier.
Recipient of the goods shall issue a taking-over certificate to the supplier without delay after the goods have been supplied "free at destination" and the supplier has provided the beneficiary with the original of the final certificate of conformity, and with a pro forma invoice establishing both the value of the goods and their transfer to the beneficiary free of charge.
For goods supplied in bulk, a tolerance of 3 % by weight (excluding the weight of samples) below the quantity requested is applicable. For goods supplied in packing, this tolerance is limited to 1 %. Where the tolerances are exceeded, the NGO may require the supplier to make an additional delivery on the same financial terms as the initial delivery.
IX. TERMS OF PAYMENT
The sum to be paid by the NGO to the supplier shall not exceed the amount of the tender plus any costs, less any reduction provided for below.
Where the quality, the packaging or the marking of the goods is found at the delivery stage not to correspond to the specifications, without being such as to have prevented the issuance of a taking over certificate, the NGO, in calculating the sum to be paid, may apply reductions.
Except in cases of force majeure, the delivery guarantee shall be partially forfeit on a cumulative basis in the following cases:
- 10 % of the value of the quantities not delivered, without prejudice to the tolerances referred to in point 8 above,
- 0,1 % of the value of the quantities supplied after the deadline, per day of delay,
- where appropriate, and only if this is specified in the tender notice, 0,1 % per day where the goods are delivered prematurely.
The amount of the guarantees to be forfeited shall be deducted from the final amount to be paid. The guaranties shall then be released simultaneously in full.
The NGO may repay to the supplier, at his written request, certain additional costs, such as warehousing or insurance actually paid by the supplier, but excluding any administrative costs, which the NGO shall assess on the basis of appropriate supporting documents, provided a taking-over certificate or delivery certificate has been issued without reservations relating to the nature of the costs claimed, and in the event of:
- an extension of the delivery period at the recipient's request, or
- a delay exceeding 30 days between the date of delivery and the issue of the taking-over certificate, or the issue of the final certificate of conformity.
Additional costs shall not be accepted if they exceed:
- EUR 1 per tonne of bulk goods and EUR 2 per tonne of processed goods per week in the case of warehousing costs,
- 0,75 % a year of the value of the goods in the case of insurance costs.
The sum to be paid shall be payable at the supplier’s request submitted in duplicate. A request for payment of the full amount of the tender or balance thereof shall be accompanied by the following documents:
- an invoice for the sum claimed,
- the original of the taking-over certificate,
- a copy signed and certified by the supplier as conforming to the original of the final certificate of conformity.
When 50 % of the total quantity laid down in the tender notice has been delivered, the supplier may present a request for advance payment accompanied by an invoice for the sum claimed and a copy of the provisional certificate of conformity.
All requests for payment of the full amount of the tender or balance thereof shall be presented to the NGO after issuance of the taking-over certificate. All payments shall be made within 60 days of the receipt by the NGO of a complete and accurate request for payment. Unjustified delays shall attract post-maturity interest at the monthly rate applied by the European Central Bank.
X. FINAL PROVISION
It shall be for the NGO to decide whether the supplier’s failure to supply the goods or to fulfil one of his obligations may be due to force majeure. Costs resulting from a case of force majeure recognised by the NGO shall be borne by the latter.
[1] OJ C 312, 31.10.2000, p. 1.
[2] OJ C 267, 13.9.1996, p. 1.
--------------------------------------------------
