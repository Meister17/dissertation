COMMISSION REGULATION (EC) No 547/1999 of 12 March 1999 amending Regulation (EC) No 2802/95 concerning the classification of certain goods in the Combined Nomenclature
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2658/87 of 23 July 1987 on the tariff and statistical nomenclature and on the Common Customs Tariff (1), as last amended by Commission Regulation (EC) No 2261/98 (2), and in particular Article 9 thereof,
Whereas in order to ensure uniform application of the Combined Nomenclature annexed to the said Regulation, it is necessary to adopt measures concerning the classification of the goods referred to in the Annex to this Regulation;
Whereas Regulation (EEC) No 2658/87 has set down the general rules for the interpretation of the Combined Nomenclature and those rules also apply to any other nomenclature which is wholly or partly based on it or which adds any additional subdivision to it and which is established by specific Community provisions, with a view to the application of tariff and other measures relating to trade in goods;
Whereas the Annex to Commission Regulation (EC) No 2802/95 of 4 December 1995 concerning the classification of certain goods in the Combined Nomenclature (3) classified product No 1 as a beverage without taking account of its specific therapeutic and prophylactic properties in the treatment of anaemia caused by iron deficiencies; whereas it is necessary to amend the classification of this product which must be considered as a medicament;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Customs Code Committee,
HAS ADOPTED THIS REGULATION:
Article 1
The classification of product No 1 in the Annex to Regulation (EC) No 2802/95 shall be replaced by that in the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the 21st day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 12 March 1999.
For the Commission
Mario MONTI
Member of the Commission
(1) OJ L 256, 7. 9. 1987, p. 1.
(2) OJ L 292, 30. 10. 1998, p. 1.
(3) OJ L 291, 6. 12. 1995, p. 5.
ANNEX
>TABLE>
