COMMISSION REGULATION (EEC) No 964/91 of 18 April 1991 concerning the classification of certain goods in the combined nomenclature
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 2658/87 of 23 July 1987 on the tariff and statistical nomenclature and on the Common Customs Tariff (1), as last amended by Regulation (EEC) No 315/91 (2), and in particular Article 9,
Whereas in order to ensure uniform application of the combined nomenclature annexed to the said Regulation, it is necessary to adopt measures concerning the classification of the goods referred to in the Annex to this Regulation;
Whereas Regulation (EEC) No 2658/87 has set down the general rules for the interpretation of the combined nomenclature and these rules also apply to any other nomenclature which is wholly or partly based on it or which adds any additional subdivisions to it and which is established by specific Community provisions, with a view to the application of tariff or other measures relating to trade in goods;
Whereas, pursuant to the said general rules, the goods described in column 1 of the table annexed to the present Regulation must be classified under the appropriate CN codes indicated in column 2, by virtue of the reasons set out in column 3;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Nomenclature Committee,
HAS ADOPTED THIS REGULATION: Article 1
The goods described in column 1 of the annexed table are now classified within the combined nomenclature under the appropriate CN codes indicated in column 2 of the said table. Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
It shall apply to product No 1 of the Annex from 30 April to 31 December 1991 and to product No 2 of the Annex from 1 August 1991. This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 18 April 1991. For the Commission
Christiane SCRIVENER
Member of the Commission (1) OJ No L 256, 7. 9. 1987, p. 1. (2) OJ No L 37, 9. 2. 1991, p. 24.
ANNEX
Description of the goods Classification CN code Reasons (1) (2) (3) 1. Product, in the form of powder with the following principal analytical results (percentage by weight):
- milk fat 5,8
- whey proteins 74,3
(78,9 by dry weight)
- lactose 6,0
- ash 2,9 0404 90 33 Classification is determined by the provisions of general rules 1 and 6 for the interpretation of the combined nomenclature and the texts of CN codes 0404, 0404 90 and 0404 90 33.
Because of its protein lactose and milk fat content, which differs considerably from that of whey powder, the product is excluded from code CN 0404 10. 2. Mushrooms of the species Agaricus completely simmered. The musrooms are immersed in water, to which salt and/or citric acid and/or SO2 may have been added, and have been chilled.
These mushrooms, which are not put up in packs for retail sale, are used, generally, as a starting material in the preservation industry. 2003 10 10 Classification is determined by the provisions of general rules 1 and 6 for the interpretation of the combined nomenclature, as well as by Additional Note 1 to Chapter 20 and by the texts of CN codes 2003, 2003 10 and 2003 10 10.
