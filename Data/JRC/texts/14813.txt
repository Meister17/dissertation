Removal from the register of Case C-63/05 [1]
(2006/C 60/68)
(Language of the case: Greek)
By order of 14 December 2005, the President of the Court of Justice of the European Communities has ordered the removal from the register of Case C-63/05: Commission of the European Communities
v Hellenic Republic.
[1] OJ C 82, 02. 04. 2005.
--------------------------------------------------
