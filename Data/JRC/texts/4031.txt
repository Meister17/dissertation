Commission Decision
of 23 December 2005
amending Decision 2003/135/EC as regards the extension of the plans for the eradication of classical swine fever in feral pigs and the emergency vaccination of feral pigs against classical swine fever in certain areas of the Federal States of North Rhine-Westfalia and Rhineland-Palatinate (Germany)
(notified under document number C(2005) 5621)
(Only the German and French texts are authentic)
(2005/950/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 2001/89/EC of 23 October 2001 on Community measures for the control of classical swine fever [1], and in particular Articles 16(1) and 20(2) thereof,
Whereas:
(1) Commission Decision 2003/135/EC of 27 February 2003 on the approval of the plans for the eradication of classical swine fever and the emergency vaccination of feral pigs against classical swine fever in Germany, in the Federal States of Lower Saxony, North Rhine-Westphalia, Rhineland-Palatinate and Saarland [2] was adopted as one of a number of measures to combat classical swine fever.
(2) The disease was successfully eradicated in the federal state of North Rhine-Westphalia and the approved eradication plan adopted for certain areas of this Federal State was lifted by Commission Decision 2005/58/EC of 26 January 2005 amending Decision 2003/135/EC as regards the termination of the eradication and vaccination plans in the Federal States of Lower-Saxony and North Rhine-Westfalia and the eradication plan in the Federal States of Saarland (Germany) [3].
(3) The German authorities have informed the Commission on the re-occurrence during October 2005 of the disease in feral pigs in certain areas of North Rhine-Westfalia and have amended the plans for the eradication of classical swine fever and the emergency vaccination of feral pigs against classical swine fever accordingly and notified it to the Commission.
(4) In the light of the epidemiological information, the eradication plan in Germany should be extended to areas in the district of Euskirchen in North Rhine-Westphalia and the districts of Ahrweiler and Daun in Rhineland-Palatinate. In addition, the emergency vaccination plan for feral pigs against classical swine fever should be amended to cover those areas.
(5) Decision 2003/135/EC should therefore be amended accordingly.
(6) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
The Annex to Decision 2003/135/EC is replaced by the text in the Annex to this Decision.
Article 2
This Decision is addressed to the Federal Republic of Germany and the French Republic.
Done at Brussels, 23 December 2005.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 316, 1.12.2001, p. 5. Directive as amended by the 2003 Act of Accession.
[2] OJ L 53, 28.2.2003, p. 47. Decision as last amended by Decision 2005/236/EC (OJ L 72, 18.3.2005, p. 44).
[3] OJ L 24, 27.1.2005, p. 45.
--------------------------------------------------
ANNEX
"
"ANNEX
1. AREAS WHERE ERADICATION PLANS ARE IN PLACE:
A. Rhineland-Palatinate:
(a) the Kreise: Bad Dürkheim, Donnersbergkreis and Südliche Weinstraße;
(b) the cities of: Speyer, Landau, Neustadt an der Weinstraße, Pirmasens and Kaiserslautern;
(c) in the Kreis Alzey-Worms: the localities Stein-Bockenheim, Wonsheim, Siefersheim, Wöllstein, Gumbsheim, Eckelsheim, Wendelsheim, Nieder-Wiesen, Nack, Erbes-Büdesheim, Flonheim, Bornheim, Lonsheim, Bermershein vor der Höhe, Albig, Bechenheim, Offenheim, Mauchenheim, Freimersheim, Wahlheim, Kettenheim, Esselborn, Dintesheim, Flomborn, Eppelsheim, Ober-Flörsheim, Hangen-Weisheim, Gundersheim, Bermersheim, Gundheim, Framersheim, Gau-Heppenheim, Monsheim and Alzey;
(d) in the Kreis Bad Kreuznach: the localities Becherbach, Reiffelbach, Schmittweiler, Callbach, Meisenheim, Breitenheim, Rehborn, Lettweiler, Abtweiler, Raumbach, Bad Sobernheim, Odernheim a. Glan, Staudernheim, Oberhausen a. d. Nahe, Duchroth, Hallgarten, Feilbingert, Hochstätten, Niederhausen, Norheim, Bad Münster a. Stein-Ebernburg, Altenbamberg, Traisen, Fürfeld, Tiefenthal, Neu-Bamberg, Frei-Laubersheim, Hackenheim, Volxheim, Pleitersheim, Pfaffen-Schwabenheim, Biebelsheim, Guldental, Bretzenheim, Langenlonsheim, Laubenheim, Dorsheim, Rümmelsheim, Windesheim, Stromberg, Waldlaubersheim, Warmsroth, Schweppenhausen, Eckenroth, Roth, Boos, Hüffelsheim, Schloßböckelheim, Rüdesheim, Weinsheim, Oberstreit, Waldböckelheim, Mandel, Hargesheim, Roxheim, Gutenberg and Bad Kreuznach;
(e) in the Kreis Germersheim: the municipalities Lingenfeld, Bellheim and Germersheim;
(f) in the Kreis Kaiserslautern: the municipalities Weilerbach, Otterbach, Otterberg, Enkenbach-Alsenborn, Hochspeyer, Kaiserslautern-Süd, Landstuhl and Bruchmühlbach-Miesau the localities Ramstein-Miesenbach, Hütschenhausen, Steinwenden and Kottweiler-Schwanden;
(g) in the Kreis Kusel: the localities Odenbach, Adenbach, Cronenberg, Ginsweiler, Hohenöllen, Lohnweiler, Heinzenhausen, Nussbach, Reipoltskirchen, Hefersweiler, Relsberg, Einöllen, Oberweiler-Tiefenbach, Wolfstein, Kreimbach-Kaulbach, Rutsweiler a.d. Lauter, Rothselberg, Jettenbach and Bosenbach;
(h) in the Rhein-Pfalz-Kreis: the municipalities Dudenhofen, Waldsee, Böhl-Iggelheim, Schifferstadt, Römerberg and Altrip;
(i) in the Kreis Südwestpfalz: the municipalities Waldfischbach-Burgalben, Rodalben, Hauenstein, Dahner-Felsenland, Pirmasens-Land and Thaleischweiler-Fröschen, the localities Schmitshausen, Herschberg, Schauerberg, Weselberg, Obernheim-Kirchenarnbach, Hettenhausen, Saalstadt, Wallhalben and Knopp-Labach;
(j) in the Kreis Ahrweiler: the municipalities Adenau und Ahrweiler;
(k) in the Kreis Daun: the munipalities Nohn und Üxheim.
B. North Rhine-Westfalia:
- in the Kreis Euskirchen: the city Bad Münstereifel, the municipality Blankenheim (localities Lindweiler, Lommersdorf and Rohr), the city Euskirchen (localities Billig, Euenheim, Flamersheim, Kirchheim, Kreuzweingarten, Niederkastenholz, Rheder, Schweinheim, Stotzheim and Wißkirchen), the city Mechernich (localities Antweiler, Harzheim, Holzheim, Lessenich, Rissdorf, Wachendorf and Weiler am Berge), the municipality Nettersheim (localities Bouderath, Buir, Egelgau, Frohngau, Holzmühlheim, Pesch, Roderath and Tondorf).
2. AREAS WHERE THE EMERGENCY VACCINATION IS APPLIED:
A. Rhineland-Palatinate:
(a) the Kreise: Bad Dürkheim, Donnersbergkreis and Südliche Weinstraße;
(b) the cities: Speyer, Landau, Neustadt an der Weinstraße, Pirmasens and Kaiserslautern;
(c) in the Kreis Alzey-Worms: the localities Stein-Bockenheim, Wonsheim, Siefersheim, Wöllstein, Gumbsheim, Eckelsheim, Wendelsheim, Nieder-Wiesen, Nack, Erbes-Büdesheim, Flonheim, Bornheim, Lonsheim, Bermersheim vor der Höhe, Albig, Bechenheim, Offenheim, Mauchenheim, Freimersheim, Wahlheim, Kettenheim, Esselborn, Dintesheim, Flomborn, Eppelsheim, Ober-Flörsheim, Hangen-Weisheim, Gundersheim, Bermersheim, Gundheim, Framersheim, Gau-Heppenheim, Monsheim and Alzey;
(d) in the Kreis Bad Kreuznach: the localities Becherbach, Reiffelbach, Schmittweiler, Callbach, Meisenheim, Breitenheim, Rehborn, Lettweiler, Abtweiler, Raumbach, Bad Sobernheim, Odernheim a. Glan, Staudernheim, Oberhausen a. d. Nahe, Duchroth, Hallgarten, Feilbingert, Hochstätten, Niederhausen, Norheim, Bad Münster a. Stein-Ebernburg, Altenbamberg, Traisen, Fürfeld, Tiefenthal, Neu-Bamberg, Frei-Laubersheim, Hackenheim, Volxheim, Pleitersheim, Pfaffen-Schwabenheim, Biebelsheim, Guldental, Bretzenheim, Langenlonsheim, Laubenheim, Dorsheim, Rümmelsheim, Windesheim, Stromberg, Waldlaubersheim, Warmsroth, Schweppenhausen, Eckenroth, Roth, Boos, Hüffelsheim, Schloßböckelheim, Rüdesheim, Weinsheim, Oberstreit, Waldböckelheim, Mandel, Hargesheim, Roxheim, Gutenberg and Bad Kreuznach;
(e) in the Kreis Germersheim: the municipalities Lingenfeld, Bellheim and Germersheim;
(f) in the Kreis Kaiserslautern: the municipalities Weilerbach, Otterbach, Otterberg, Enkenbach-Alsenborn, Hochspeyer, Kaiserslautern-Süd, Landstuhl and Bruchmühlbach-Miesau, the localities Ramstein-Miesenbach, Hütschenhausen, Steinwenden and Kottweiler-Schwanden;
(g) in the Kreis Kusel: the localities Odenbach, Adenbach, Cronenberg, Ginsweiler, Hohenöllen, Lohnweiler, Heinzenhausen, Nussbach, Reipoltskirchen, Hefersweiler, Relsberg, Einöllen, Oberweiler-Tiefenbach, Wolfstein, Kreimbach-Kaulbach, Rutsweiler a.d. Lauter, Rothselberg, Jettenbach and Bosenbach;
(h) in the Rhein-Pfalz-Kreis: the municipalities Dudenhofen, Waldsee, Böhl-Iggelheim, Schifferstadt, Römerberg and Altrip;
(i) in the Kreis Südwestpfalz: the municipalities Waldfischbach-Burgalben, Rodalben, Hauenstein, Dahner-Felsenland, Pirmasens-Land and Thaleischweiler-Fröschen, the localities Schmitshausen, Herschberg, Schauerberg, Weselberg, Obernheim-Kirchenarnbach, Hettenhausen, Saalstadt, Wallhalben and Knopp-Labach;
(j) in the Kreis Ahrweiler: the municipalities Adenau and Ahrweiler;
(k) in the Kreis Daun: the munipalities Nohn and Üxheim.
B. North Rhine-Westfalia
- in the Kreis Euskirchen: the city Bad Münstereifel, the municipality Blankenheim (localities Lindweiler, Lommersdorf and Rohr), the city Euskirchen (localities Billig, Euenheim, Flamersheim, Kirchheim, Kreuzweingarten, Niederkastenholz, Rheder, Schweinheim, Stotzheim and Wißkirchen), the city Mechernich (localities Antweiler, Harzheim, Holzheim, Lessenich, Rissdorf, Wachendorf and Weiler am Berge), the municipality Nettersheim (localities Bouderath, Buir, Egelgau, Frohngau, Holzmühlheim, Pesch, Roderath and Tondorf)."
"
--------------------------------------------------
