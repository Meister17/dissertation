Administrative Commission of the European Communities on social security for migrant workers
Rates for conversion of currencies pursuant to Council Regulation (EEC) No 574/72
(2006/C 29/12)
Article 107(1), (2), (3) and (4) of Regulation (EEC) No 574/72
Reference period: January 2006
Application period: April, May and June 2006
| EUR | CZK | DKK | EEK | CYP | LVL | LTL | HUF | MTL | PLN | SIT | SKK | SEK | GBP | NOK | ISK | CHF |
1 EUR = | 1 | 28,7220 | 7,46125 | 15,6466 | 0,573759 | 0,696045 | 3,45280 | 250,706 | 0,429300 | 3,82010 | 239,487 | 37,4918 | 9,31108 | 0,685984 | 8,03664 | 74,5818 | 1,54942 |
1 CZK = | 0,0348165 | 1 | 0,259775 | 0,544760 | 0,0199763 | 0,0242339 | 0,120214 | 8,72872 | 0,0149467 | 0,133003 | 8,33810 | 1,30533 | 0,324179 | 0,0238836 | 0,279808 | 2,59668 | 0,0539455 |
1 DKK = | 0,134026 | 3,84949 | 1 | 2,09705 | 0,0768985 | 0,093288 | 0,462764 | 33,6011 | 0,0575373 | 0,511992 | 32,0974 | 5,02487 | 1,24793 | 0,0919396 | 1,07712 | 9,99589 | 0,207663 |
1 EEK = | 0,0639116 | 1,83567 | 0,476861 | 1 | 0,0366699 | 0,0444854 | 0,220674 | 16,0231 | 0,0274373 | 0,244149 | 15,3060 | 2,39616 | 0,595087 | 0,0438424 | 0,513635 | 4,76665 | 0,0990262 |
1 CYP = | 1,74289 | 50,0593 | 13,0042 | 27,2703 | 1 | 1,21313 | 6,01786 | 436,954 | 0,748223 | 6,65802 | 417,400 | 65,3442 | 16,2282 | 1,19560 | 14,0070 | 129,988 | 2,70048 |
1 LVL = | 1,43669 | 41,2645 | 10,7195 | 22,4793 | 0,824313 | 1 | 4,96060 | 360,187 | 0,616770 | 5,48829 | 344,068 | 53,8640 | 13,3771 | 0,985545 | 11,5461 | 107,151 | 2,22604 |
1 LTL = | 0,289620 | 8,31847 | 2,16093 | 4,53157 | 0,166172 | 0,201589 | 1 | 72,6096 | 0,124334 | 1,10638 | 69,3602 | 10,8584 | 2,69668 | 0,198675 | 2,32757 | 21,6004 | 0,448744 |
1 HUF = | 0,00398873 | 0,114564 | 0,0297609 | 0,0624101 | 0,00228857 | 0,00277634 | 0,0137723 | 1 | 0,00171236 | 0,0152373 | 0,955248 | 0,149545 | 0,0371394 | 0,00273621 | 0,0320560 | 0,297487 | 0,00618023 |
1 MTL = | 2,32937 | 66,9043 | 17,3800 | 36,4468 | 1,33650 | 1,62135 | 8,04286 | 583,989 | 1 | 8,89844 | 557,854 | 87,3324 | 21,6890 | 1,59791 | 18,7203 | 173,729 | 3,60918 |
1 PLN = | 0,261773 | 7,51865 | 1,95316 | 4,09586 | 0,150195 | 0,182206 | 0,903851 | 65,6282 | 0,112379 | 1 | 62,6912 | 9,81436 | 2,43739 | 0,179572 | 2,10378 | 19,5235 | 0,405597 |
1 SIT = | 0,0041756 | 0,119931 | 0,0311552 | 0,0653339 | 0,00239579 | 0,0029064 | 0,0144175 | 1,04685 | 0,00179258 | 0,0159512 | 1 | 0,156551 | 0,0388793 | 0,00286439 | 0,0335577 | 0,311423 | 0,00646976 |
1 SKK = | 0,0266725 | 0,766087 | 0,19901 | 0,417334 | 0,0153036 | 0,0185653 | 0,0920948 | 6,68696 | 0,0114505 | 0,101892 | 6,38771 | 1 | 0,24835 | 0,0182969 | 0,214357 | 1,98928 | 0,041327 |
1 SEK = | 0,107399 | 3,08471 | 0,80133 | 1,68043 | 0,0616211 | 0,0747545 | 0,370827 | 26,9256 | 0,0461064 | 0,410275 | 25,7206 | 4,02658 | 1 | 0,0736739 | 0,863126 | 8,01001 | 0,166406 |
1 GBP = | 1,45776 | 41,8698 | 10,8767 | 22,8090 | 0,836403 | 1,014670 | 5,03335 | 365,470 | 0,625816 | 5,56879 | 349,114 | 54,6541 | 13,5733 | 1 | 11,7155 | 108,722 | 2,25869 |
1 NOK = | 0,124430 | 3,57388 | 0,928405 | 1,94691 | 0,0713929 | 0,0866091 | 0,429632 | 31,1954 | 0,0534179 | 0,475336 | 29,7994 | 4,66511 | 1,15858 | 0,0853571 | 1 | 9,28023 | 0,192795 |
1 ISK = | 0,0134081 | 0,385107 | 0,100041 | 0,209791 | 0,00769302 | 0,00933264 | 0,0462955 | 3,36149 | 0,00575609 | 0,0512203 | 3,21106 | 0,502694 | 0,124844 | 0,00919774 | 0,107756 | 1 | 0,0207748 |
1 CHF = | 0,645402 | 18,5372 | 4,81550 | 10,0983 | 0,370305 | 0,449229 | 2,22844 | 161,806 | 0,277071 | 2,46550 | 154,565 | 24,1973 | 6,00939 | 0,442735 | 5,18686 | 48,1352 | 1 |
1. Regulation (EEC) No 574/72 determines that the rate for the conversion into a currency of amounts denominated in another currency shall be the rate calculated by the Commission and based on the monthly average, during the reference period specified in paragraph 2, of reference rates of exchange of currencies published by the European Central Bank.
2. The reference period shall be:
- the month of January for rates of conversion applicable from 1 April following,
- the month of April for rates of conversion applicable from 1 July following,
- the month of July for rates of conversion applicable from 1 October following,
- the month of October for rates of conversion applicable from 1 January following.
The rates for the conversion of currencies shall be published in the second Official Journal of the European Union ("C" series) of the months of February, May, August and November.
--------------------------------------------------
