[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 21.12.2005
COM(2005) 682 final
2005/0266 (ACC)
Proposal for a
COUNCIL DECISION
on the conclusion of an Agreement in the form of an exchange of letters between the European Community and the Republic of Chile concerning amendments to the Agreement on Trade in Spirit Drinks and Aromatised Drinks annexed to the Association Agreement between the European Community and its Member States, of the one part, and the Republic of Chile, of the other part
(presented by the Commission)
EXPLANATORY MEMORANDUM
The Agreement on Trade in Spirit Drinks and Aromatised Drinks annexed to the Association Agreement between the European Community and the Republic of Chile (hereinafter referred to as “Annex VI”) was signed in Brussels on 18 November 2002 and entered into force on 1 February 2003.
The Joint Committee set up by Article 17 of the Annex VI is responsible for the administration of the agreement and its respective appendices. This Joint Committee bears in particular the responsibility for making recommendation on Annex VI, in particular by taking into account developments in the acquis communautaire and laws and regulations of Chile, and by addressing issues that remained unsolved during the original negotiations. The Joint Committee has agreed on the need to amend not only the appendices but also the text of the agreement in order to update it.
Following the mandate given by the Council to the Commission on 24 November 2005, the relevant negotiations on various (rather technical) amendments to the Annex VI have been successfully concluded which now should be adopted by the Council. To that effect, the Commission submits to the Council a proposal for the conclusion of the Agreement in the form of exchange of letters.
2005/0266 (ACC)
Proposal for a
COUNCIL DECISION
on the conclusion of an Agreement in the form of an exchange of letters between the European Community and the Republic of Chile concerning amendments to the Agreement on Trade in Spirit Drinks and Aromatised Drinks annexed to the Association Agreement between the European Community and its Member States, of the one part, and the Republic of Chile, of the other part
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 133, in conjunction with the first sentence of the first subparagraph of Article 300(2) thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) The Agreement establishing an Association between the European Community and its Member States, of the one part, and the Republic of Chile, of the other part, was signed on 18 November 2002, and entered into force on 1 March 2005[1].
(2) On 24 November 2005 the Council authorised the Commission to enter into negotiations with the Republic of Chile to amend the Agreement on Trade in Spirit Drinks and Aromatised Drinks annexed to the Association Agreement between the European Community, and its Member States, of the one part, and the Republic of Chile, of the other part[2] (hereinafter referred to as ‘Annex VI’).
(3) These negotiations have been successfully concluded and the draft Agreement in the form of exchange of letters amending Annex VI should be approved,
HAS DECIDED AS FOLLOWS:
Article 1
The Agreement in the form of an exchange of letters between the European Community and the Republic of Chile concerning amendments to the Agreement on Trade in Spirit Drinks and Aromatised Drinks annexed to the Association Agreement between the European Community and its Member States, of the one part, and the Republic of Chile, of the other part, is hereby approved on behalf of the Community.
The text of the Agreement is annexed to this Decision.
Article 2
The Commissioner for Agriculture and Rural Development is hereby empowered to sign the exchange of letters in order to legally bind the Community.
Done at Brussels,
For the Council
The President
ANNEX
AGREEMENT IN THE FORM OF EXCHANGE OF LETTERS between the European Community and the Republic of Chile concerning amendments to the Agreement on Trade in Spirit Drinks and Aromatised Drinks annexed to the Association Agreement between the European Community and its Member States, of the one part, and the Republic of Chile, of the other part
Letter No 1 Letter from the European Community
Brussels, ………. 2005
Sir,
I have the honour to refer to meetings of the Joint Committee established in accordance with Article 17 of Annex VI to the Association Agreement (the Agreement on Trade in Spirit Drinks and Aromatised Drinks). The Joint Committee has recommended that modifications should be made to the Agreement on Trade in Spirit Drinks and Aromatised Drinks (hereinafter referred to as ‘Annex VI’), in order to take account of legislative developments since its adoption.
During the recent Joint Committee meeting held in Madrid on 13–14 June 2005 there was agreement on the need to amend not only the appendices but also the text of the agreement in order to update it. I have therefore the honour to propose that Annex VI be amended as indicated in the Appendix attached hereto, with effect as of xxxx (probably to be the date of signature).
I should be obliged if you would confirm that your Government is in agreement with the content of this letter.
Please accept, Sir, the assurance of my highest consideration.
On behalf of the European Community
Letter No 2 Letter from Chile
Santiago de Chile/Brussels, ………. 2005
Madam,
I have the honour to acknowledge receipt of your letter of today’s date which reads as follows:
“I have the honour to refer to meetings of the Joint Committee established in accordance with Article 17 of Annex VI to the Association Agreement (the Agreement on Trade in Spirit Drinks and Aromatised Drinks). The Joint Committee has recommended that modifications should be made to the Agreement on Trade in Spirit Drinks and Aromatised Drinks (hereinafter referred to as ‘Annex VI’), in order to take account of legislative developments since its adoption.
During the recent Joint Committee meeting held in Madrid on 13–14 June 2005 there was agreement on the need to amend not only the appendices but also the text of the agreement in order to update it. I have therefore the honour to propose that Annex VI be amended as indicated in the Appendix attached hereto, with effect as of xxxx (probably to be the date of signature).
I should be obliged if you would confirm that your Government is in agreement with the content of this letter.”
I have the honour to inform you that the Republic of Chile is in agreement with the content of this letter.
Please accept, Madam, the assurance of my highest consideration.
On behalf of the Republic of Chile
“APPENDIX
Annex VI is hereby amended as follows:
(1) In Article 5, paragraph 2 is replaced by the following:
“2. The names referred to in Article 6 shall be reserved exclusively for the products originating in the Party to which they apply.”
(2) Article 7 is amended as follows:
(a) paragraph 2 is replaced by the following:
“2. On the basis of the Chilean trademark register as established on 10 June 2002, the trademarks listed in Appendix II A shall be cancelled within 12 years for use on the internal market and five years for export from the date of entry into force of this Agreement.”
(b) after paragraph 2, the following paragraph 2a is inserted:
“2a. On the basis of the Chilean trademark register as established on 10 June 2002, trademarks listed in Appendix II B. are allowed under the conditions laid down in this Appendix, exclusively for use on the internal market, and shall be cancelled within 12 years from the date of entry into force of this Agreement.”
(3) Article 8 is amended as follows:
(a) paragraph 1 is replaced by the following:
“1. The Parties are not aware, on the basis of the Chilean trademark register as established on 10 June 2002, of any trademarks other than those listed in Article 7(2) and (2a) which are identical with, or similar to, or contain the protected designation referred to in Article 6.
(b) paragraph 2 is replaced by the following:
“2. Pursuant to paragraph 1, the Parties shall not deny the right to use a trademark contained in the Chilean trademark register on 10 June 2002, other than those referred to in Article 7(2) and (2a), on the basis that such a trademark is identical with, or similar to, or contains a protected designation listed in Appendix I.”
(4) Article 17(3) is replaced by the following:
“3. In particular, the Joint Committee may make recommendations in furtherance of the objectives of this Agreement. It shall be conducted in accordance with the Rules of Procedure for the Special Committees.” ”
FINANCIAL STATEMENT |
1. | BUDGET HEADING: 05 02 09 | APPROPRIATIONS: EUR 1 227,84 Mio |
2. | TITLE: Council Decision on the conclusion of an Agreement in the form of an exchange of letters between the European Community and the Republic of Chile concerning amendments to the Agreement on Trade in Spirit Drinks and Aromatised Drinks annexed to the Association Agreement between the European Community and its Member States, of the one part, and the Republic of Chile, of the other part |
3. | LEGAL BASIS: Art. 133 of the EC Treaty |
4. | AIMS: Under this EU/Chile Association Council Decision, Chile will eliminate imports duties on wines, spirit drinks and aromatised drinks originating in the Community |
5. | FINANCIAL IMPLICATIONS | 12 MONTH PERIOD (EUR million) | CURRENT FINANCIAL YEAR 2005 (EUR million) | FOLLOWING FINANCIAL YEAR 2006 (EUR million) |
5.0 | EXPENDITURE – CHARGED TO THE EC BUDGET (REFUNDS/INTERVENTIONS) – NATIONAL AUTHORITIES – OTHER | – | – | – |
5.1 | REVENUE – OWN RESOURCES OF THE EC (LEVIES/CUSTOMS DUTIES) – NATIONAL | – | – | – |
2007 | 2008 | 2009 | 2010 |
5.0.1 | ESTIMATED EXPENDITURE | – | – | – | – |
5.1.1 | ESTIMATED REVENUE | – | – | – | – |
5.2 | METHOD OF CALCULATION: – |
6.0 | CAN THE PROJECT BE FINANCED FROM APPROPRIATIONS ENTERED IN THE RELEVANT CHAPTER OF THE CURRENT BUDGET? | YES NO |
6.1 | CAN THE PROJECT BE FINANCED BY TRANSFER BETWEEN CHAPTERS OF THE CURRENT BUDGET? | YES NO |
6.2 | WILL A SUPPLEMENTARY BUDGET BE NECESSARY? | YES NO |
6.3 | WILL APPROPRIATIONS NEED TO BE ENTERED IN FUTURE BUDGETS? | YES NO |
OBSERVATIONS: It is not expected that the trade volume will change significantly as a result of this measure. It can be assumed that there will be no implication with the Community budget. |
[1] OJ L 84, 2.4.2005, p. 21.
[2] OJ L 352, 20.12.2005, p. 1193.
