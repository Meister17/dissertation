Opinion of the Committee of the Regions on the "Communication from the Commission to the Council, the European Parliament, the European Economic and Social Committee and the Committee of the Regions on the promotion of cooperative societies in Europe"
(2004/C 318/05)
THE COMMITTEE OF THE REGIONS,
Having regard to the Communication from the Commission to the Council, the European Parliament, the European Economic and Social Committee and the Committee of the Regions on the promotion of cooperative societies in Europe (COM(2004) 18 final);
Having regard to the decision of the European Commission of 23 February 2004 to consult it on this subject, under the first paragraph of Article 265 of the Treaty establishing the European Community;
Having regard to the decision of its President of 19 June 2003 to instruct its Commission for Economic and Social Policy to draw up an opinion on this subject;
Having regard to its draft opinion (CdR 97/2004 rev. 1) adopted on 30 April 2004 by the Commission for Economic and Social Policy (rapporteur: Ms Irma Pellinen, Chairman of Haukipudas Municipal Council and Member of the Regional Council of North East Bothnia (FIN/PES)),
adopted the following opinion at its 55th Plenary Session, held on 16 and 17 June 2004 (session of 16 June 2004).
1. The Committee of the Regions' views
The Committee of the Regions
1.1 welcomes the Commission Communication on the promotion of cooperative societies in Europe, which aims to foster greater use of cooperatives across Europe by raising their profile and increasing understanding of their characteristics. Better awareness of how cooperatives work, further development of cooperative legislation, the maintenance and improvement of cooperatives' position and input all contribute to the achievement of Community objectives;
1.2 endorses the Commission's views about cooperatives as a vehicle for SMEs to undertake joint activities and improve their market position and as a means of providing high quality services to customer-members, especially "proximity services" such as health, social protection and welfare. Workers' cooperatives can make a positive contribution to building a knowledge-based society;
1.3 stresses the importance of the Commission's aim to find ways of increasing the willingness and ability of SMEs to use the cooperative form of enterprise as a vehicle for establishing joint business activities or groupings of businesses;
1.4 acknowledges the importance of organising a structured exchange of information, identification of best practices and publicising cooperative projects involving SMEs;
1.5 supports efforts to examine ways of collecting more accurate statistical data on cooperatives;
1.6 highlights the importance of participation by cooperatives in programmes for education, training, lifelong learning and e-learning and of encouraging universities and secondary school to organise cooperative management training;
1.7 considers it to be of the utmost importance to identify and disseminate good practices in the area of public provision of business support services for cooperatives;
1.8 underlines the importance of financing for cooperatives. It is necessary to assess the possibility of including a specific reference to cooperatives in financial instruments managed by the European Investment Fund, particularly the funds which form part of the Multi-Annual Programme for Enterprise and Entrepreneurship and in particular for Small and Medium-sized Enterprises. Efforts should also be made to ensure that cooperatives have full access to Community programmes and information about these programmes;
1.9 supports the Commission's intention to study policies, good practices and regulations concerning social cooperative enterprises;
1.10 encourages the Commission to hold discussions with Member States on the application of the Regulation and Directive on a Statute for a European Cooperative Society (SCE);
1.11 supports the Commission's work with Member States to improve legislation on cooperative societies;
1.12 endorses efforts to draft "model laws" for cooperative legislation;
1.13 feels it is important in the context of reviewing the SCE Regulation to consider the possibility of simplifying the Regulation by suggesting the adoption of common Europe-wide rules, where possible;
1.14 underlines the important contribution which cooperatives make to agricultural development in the new Member States by supporting cooperation between farms in purchasing, marketing of products and improving production;
2. The Committee of the Regions' recommendations
The Committee of the Regions
2.1 urges the Commission and the Member States to make a rapid assessment of the scope for implementing and supporting initiatives by stakeholders aimed at raising awareness among public authorities and private economic operators about the possibility of using the cooperative form as a means of establishing a business or a grouping of small enterprises;
2.2 believes it important that the Commission explore the possibility of organising a structured exchange of information and experiences and identifying good practices in the area of doing business through cooperatives. The Commission should also examine with Member States and stakeholders the need for benchmarking of national policies and practices;
2.3 encourages experimentation with satellite techniques and other sampling methods for the collection of necessary statistical data on cooperatives in Europe;
2.4 considers it important that programmes for education, training, lifelong learning and e-learning take account of participation by cooperatives, particularly in international programmes and specialist networks that lead to the development of best practices in innovative areas;
2.5 highlights the importance of identifying and disseminating good practices in the public provision of business support services;
2.6 considers it important to quickly examine the possibility of including a specific reference to cooperatives in the financial instruments managed by the European Investment Fund which form part of the Multi-Annual Programme for Enterprise and Entrepreneurship and in particular for Small and Medium-sized Enterprises and to ensure that cooperatives are eligible to take part in Community programmes and that they have full access to information about these programmes;
2.7 encourages the Commission to study policies, good practices and regulations concerning social cooperative enterprises and the social and job-creation impact of cooperatives in Europe more widely and to report its findings to the Community institutions;
2.8 recommends that the Commission urgently launch discussions with officials in Member States who are responsible for implementation of Europe-wide rules on cooperatives. The discussions should focus, above all, on issues where national measures are required or national laws are applicable;
2.9 stresses the need to work actively with public authorities, especially in the new Member States, to ensure that cooperative legislation is improved. In addition, the Commission must require Member States to inform the Commission and each other of their intentions to amend cooperative legislation at the drafting stage and before adopting new legislation;
2.10 underlines the importance of the Commission's request to national and European stakeholder organisations to draft "model laws" and the Commission's willingness to assist them in the drafting;
2.11 recommends that the Commission actively monitor application of the SCE Regulation and compile a register of areas where amendments are needed so that five years after the entry into force of the Regulation it can be simplified and reinforced, where necessary;
2.12 welcomes the fact that the Commission will seek to ensure that the contribution of cooperatives and other social economy enterprises to agricultural development is exploited through Community initiatives;
2.13 stresses that the Commission and Member State governments should examine the possibility of granting tax concessions to cooperatives on the basis of their usefulness in regional or social terms and of whether they essentially observe the main founding principles of cooperatives: democracy, the solidarity of members, both workers and consumers, and their direct management of entrepreneurial activities;
2.14 draws attention to the opportunities which cooperatives offer in terms of developing the local economy and new innovative forms of employment at local and regional level;
2.15 urges the Commission and Member States to actively promote the establishment of co-ops (particularly marketing co-ops) in the new forms of agriculture and rural activities, such as organic farming, aquaculture, etc. Actions might include training, facilitation, legal advice, identification of participants, etc.;
2.16 urges municipalities and regions to take better account of the cooperative form of enterprise in their industrial policy;
2.17 recalls that cooperatives offer new opportunities for improving employment, service provision and economic regeneration in remote areas which suffer from lack of capital.
Brussels, 16 June 2004.
The President
of the Committee of the Regions
Peter Straub
--------------------------------------------------
