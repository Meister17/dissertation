COMMISSION REGULATION (EEC) No 4159/87 of 29 December 1987 amending Regulations (EEC) No 121/65, No 564/68, No 998/68, No 2260/69 and No 1570/71 on the non-fixing of additional amounts for imports of certain pigmeat products as a consequence of the introduction of the combined nomenclature
THE COMMISSION OF THE EUROPEAN COMMUNITIES, Having regard to the Treaty establishing the European Economic Community, Having regard to Council Regulation (EEC) No 2658/87 of 23 July 1987 on the tariff and statistical nomenclature and on the Common Customs Tariff (1), as amended by Regulation (EEC) No 3985/87 (2), and in particular the second subparagraph of Article 15 (1) thereof; Having regard to Council Regulation (EEC) No 2759/75 of 29 October 1975 on the common organization of the market in pigmeat (3), as last amended by Regulation (EEC) No 3906/87 (4) and in particular Article 13 (2) thereof; Whereas, with effect from 1 January 1988, Regulation (EEC) No 2658/87 establishes, on the basis of the Harmonized System, a combined goods nomenclature which will meet the requirements both of the Common Customs Tariff and of the external trade statistics of the Community; Whereas certain Regulations on the non-fixing of additional amounts for imports of certain pigmeat products must be adapted to take account of the use of the new combined nomenclature based on the Harmonized System; Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Pigmeat, HAS ADOPTED THIS REGULATION:
Article 1
Article 1 of Commission Regulation No 121/65/EEC of 16 September 1965, exempting imports of pigs from Austria from the levy of additional amounts (5) is hereby replaced by the following:'Article 1The levies fixed in accordance with Article 8 of Council Regulation (EEC) No 2759/75 (*) shall not be increased by an additional amount in respect of imports of the following products originating in and coming from Austria: >TABLE>
(*) OJ No L 282, 1. 11. 1975, p. 1.'
Article 2
Article 1 of Commission Regulation (EEC) No 564/68 of 24 April 1968, on the non-fixing of additional amounts for imports of live swine and pig carcases from Poland (6) is hereby replaced by the following: 'Article 1The levies fixed in accordance with Article 8 of Council Regulation (EEC) No 2759/75 (*) shall not be increased by an additional amount in respect of imports of the following products originating in and coming from Poland: >TABLE>
(*) OJ No L 282, 1. 11. 1975, p. 1.'
Article 3
Article 1 of Commission Regulation (EEC) No 998/68 of 18 July 1968 on the non-fixing of additional amounts for imports of pig carcases and of certain cuts of pigmeat from Hungary (1), as amended by Regulation (EEC) No 328/83 (2), is hereby replaced by the following: 'Article 1The levies fixed in accordance with Article 8 of Council Regulation (EEC) No 2759/75 (*) shall not be increased by an additional amount in respect of imports of the following products originating in and coming from Hungary: >TABLE>
>TABLE>
(*) OJ No L 282, 1. 11. 1975, p. 1.'
Article 4
Article 1 of Commission Regulation (EEC) No 2260/69 of 13 November 1969 on the non-fixing of additional amounts for imports of pig carcases and of certain cuts of pigmeat from Romania (3), as amended by Regulation (EEC) No 328/83 is hereby replaced by the following: 'Article 1The levies fixed in accordance with Article 8 of Council Regulation (EEC) No 2759/75 (*) shall not be increased by an additional amount in respect of imports of the following products originating in and coming from Romania: >TABLE>
>TABLE>
(*) OJ No L 282, 1. 11. 1975, p. 1.'
Article 5
Article 1 of Commission Regulation (EEC) No 1570/71 of 22 July 1971, on the non-fixing of additional amounts for imports of pig carcases and of certain cuts of pigmeat from Bulgaria (1), as amended by Regulation (EEC) No 328/83 is hereby replaced by the following: 'Article 1The levies fixed in accordance with Article 8 of Council Regulation (EEC) No 2759/75 (*) shall not be increased by an additional amount in respect of imports of the following products originating in and coming from Bulgaria: >TABLE>
>TABLE>
(*) OJ No L 282, 1. 11. 1975, p. 1.'
Article 6
This Regulation shall enter into force on 1 January 1988.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 29 December 1987. For the Commission Frans ANDRIESSEN Vice-President
(1) OJ No L 256, 7. 9. 1987, p. 1.
(2) OJ No L 376, 31. 12. 1987, p. 1.
(3) OJ No L 282, 1. 11. 1975, p. 1.
(4) OJ No L 370, 30. 12. 1987, p. 11.
(5) OJ No 155, 18. 9. 1965, p. 2560/65.
(6) OJ No L 107, 8. 5. 1968, p. 6.
(1) OJ No L 170, 19. 7. 1968, p. 14.
(2) OJ No L 38, 10. 2. 1983, p. 12.
(3) OJ No L 286, 14. 11. 1969, p. 22.
(1) OJ No L 165, 23. 7. 1971, p. 23.
