Proposal for COUNCIL DECISION on the signing, on behalf of the European Community, and provisional application of the Agreement in the form of an exchange of letters concerning the provisional application of the Protocol setting out the fishing opportunities and financial contribution provided for in the Agreement between the European Economic Community and the Islamic Federal Republic of the Comoros on fishing off the Comoros for the period from 28 February 2004 to 31 December 2004
(presented by the Commission)
EXPLANATORY MEMORANDUM
The Protocol to the Fisheries Agreement between the European Community and the Islamic Federal Republic of the Comoros is due to expire on 27 February 2004.
The Commission has undertaken to carry out detailed evaluations and impact analyses before renewing any protocols and after their expiry. In the case of the Protocol to the EC-Comoros Fisheries Agreement, such an evaluation - which can take from six to eight months - could not be completed in time.
Therefore, the two parties have decided to extend the expired protocol for the period from 28 February to 31 December 2004. The extension, in the form of an exchange of letters, was initialled by the two parties on 3 February 2004, in order to set out the technical and financial conditions governing the fishing activities of EC vessels in Comorian waters for the above period.
The Commission accordingly proposes that the Council adopt a decision applying the Agreement in the form of an exchange of letters on a provisional basis, pending its definitive entry into force.
A proposal for a Council Regulation on the conclusion of the Agreement in the form of an exchange of letters extending the period of validity of the Protocol is the subject of a separate procedure.
Proposal for COUNCIL DECISION on the signing, on behalf of the European Community, and provisional application of the Agreement in the form of an exchange of letters concerning the provisional application of the Protocol setting out the fishing opportunities and financial contribution provided for in the Agreement between the European Economic Community and the Islamic Federal Republic of the Comoros on fishing off the Comoros for the period from 28 February 2004 to 31 December 2004
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 37 in conjunction with Article 300(2) thereof,
Having regard to the proposal from the Commission [1],
[1] OJ C [...], [...], p. [...].
Whereas:
(1) Under the terms of the Agreement between the European Economic Community and the Islamic Federal Republic of the Comoros on fishing off the Comoros [2], the contracting parties are to enter into negotiations, before the period of validity of the Protocol to the Agreement expires, to determine by mutual agreement the contents of the Protocol for the period that follows and, where applicable, the amendments or additions to be made to the Annex thereto.
[2] OJ L 137, 2.6.1988, p. 19.
(2) The two parties have decided that, pending negotiations on amendments to the Protocol, the period of validity of the current Protocol approved by Regulation (EC) No 1439/2001 [3] should be extended by one year by means of an agreement in the form of an exchange of letters.
[3] OJ L 193, 17.7.2001, p. 1.
(3) The exchange of letters provides Community fishermen with fishing opportunities from 28 February 2004 to 31 December 2004 in waters over which the Comoros has sovereignty or jurisdiction.
(4) To avoid any interruption in the fishing activities of Community vessels it is essential that the extension should come into effect as soon as possible. The Agreement in the form of an exchange of letters should therefore be signed, subject to its definitive conclusion by the Council.
(5) The method of allocating the fishing opportunities among Member States on the basis of the Protocol that has expired should be confirmed,
HAS DECIDED AS FOLLOWS:
Article 1
The signature of the Agreement in the form of an exchange of letters concerning the extension of the Protocol setting out the fishing opportunities and financial contribution provided for in the Agreement between the European Economic Community and the Islamic Federal Republic of the Comoros on fishing off the Comoros for the period from 28 February 2004 to 31 December 2004 is hereby approved on behalf of the Community, subject to its definitive conclusion by the Council.
The text of the Agreement in the form of an exchange of letters is attached to this Decision.
Article 2
The President of the Council is hereby authorised to designate the persons empowered to sign the Agreement in the form of an exchange of letters on behalf of the Community subject to its conclusion.
Article 3
The Agreement in the form of an exchange of letters shall be provisionally applied by the Community from 28 February 2004.
Article 4
The fishing opportunities fixed in Article 1 of the Protocol shall be allocated among the Member States as follows:
(a) tuna seiners:
Spain: 18 vessels
France: 21 vessels
Italy: 1 vessel
(b) surface longliners:
Spain: 20 vessels
Portugal : 5 vessels
If licence applications from these Member States do not cover all the fishing opportunities fixed by the Protocol, the Commission may take into consideration licence applications from any other Member State.
Article 5
The Member States whose vessels fish under this Agreement shall notify the Commission of the quantities of each stock caught within the Comorian fishing zone in accordance with Commission Regulation (EC) No 500/2001 [4].
[4] OJ L 73, 15.3.2001, p. 8.
Done at
For the Council
The President
AGREEMENT IN THE FORM OF AN EXCHANGE OF LETTERS
concerning the extension of the Protocol setting out the fishing opportunities and financial compensation provided for in the agreement between the European Economic Community and the Islamic Federal Republic of the Comoros on fishing off the Comoros for the period 28 February 2004 to 31 December 2004
A. Letter from the Community
Sirs,
I have the honour to confirm that we agree to the following interim arrangements for the extension of the Protocol currently in force (28 February 2001 to 27 February 2004) setting out the fishing opportunities and financial contribution provided for in the Agreement between the European Economic Community and the Islamic Federal Republic of the Comoros on fishing off the Comoros, pending the negotiations on the amendments to be made to the Protocol to the Fisheries Agreement:
1. From 28 February 2004 to 31 December 2004 the arrangements applicable over the last three years will continue in operation. The Community's financial contribution under the interim arrangements will correspond pro rata temporis to the amount provided for in Article 2 of the Protocol currently in force, i.e. EUR 291 875. This financial contribution will be paid no later than 1 December 2004. The relevant conditions for payment of the amount provided for in Article 3 of the Protocol will also apply.
2. During the interim period, fishing licences will be granted within the limits applicable under Article 1 of the Protocol currently in force, against payment of fees or advances equal to those set out at point 1 of the Annex to the Protocol.
I should be obliged if you would acknowledge receipt of this letter and confirm that you are in agreement with its contents.
Please accept, Sirs, the assurance of my highest consideration.
On behalf of the Council of the European Union
B. Letter from the Government of the Union of the Comoros
Sirs,
I have the honour to acknowledge receipt of your letter of today's date, worded as follows:
"I have the honour to confirm that we agree to the following interim arrangements for the extension of the Protocol currently in force (28 February 2001 to 27 February 2004) setting out the fishing opportunities and financial contribution provided for in the Agreement between the European Economic Community and the Islamic Federal Republic of the Comoros on fishing off the Comoros, pending the negotiations on the amendments to be made to the Protocol to the Fisheries Agreement:
1. From 28 February 2004 to 31 December 2004 the arrangements applicable over the last three years will continue in operation. The Community's financial contribution under the interim arrangements will correspond pro rata temporis to the amount provided for in Article 2 of the Protocol currently in force, i.e. EUR 291 875. This financial contribution will be paid no later than 1 December 2004. The relevant conditions for payment of the amount provided for in Article 3 of the Protocol will also apply.
2. During the interim period, fishing licences will be granted within the limits applicable under Article 1 of the Protocol currently in force, against payment of fees or advances equal to those set out at point 1 of the Annex to the Protocol.
I should be obliged if you would acknowledge receipt of this letter and confirm that you are in agreement with its contents."
I have the honour to confirm that the above is acceptable to the Government of the Union of the Comoros and that your letter and this letter constitute an agreement in accordance with your proposal.
Please accept, Sirs, the assurance of my highest consideration.
For the Government of the Union of the Comoros
