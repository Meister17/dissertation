Prior notification of a concentration
(Case COMP/M.4273 — Sungrebe/Severstal/Arcelor)
(2006/C 144/06)
(Text with EEA relevance)
1. On 8 June 2006, the Commission received a notification of a proposed concentration pursuant to Article 4 of Council Regulation (EC) No 139/2004 [1] by which the undertaking Arcelor S.A. ("Arcelor", Luxembourg) acquires within the meaning of Article 3(1)(b) of the Council Regulation control of the undertakings OAO Severstal ("Severstal", Russia) and Lucchini SpA ("Lucchini", Italy) and some mining assets by way of purchase of shares and of a proposed concentration pursuant to Article 4 of the Council Regulation by which the undertaking Sungrebe Investments Limited ("Sungrebe", British Virgin Islands) acquires within the meaning of Article 3(1)(b) of the Council Regulation control of the whole of the undertaking Arcelor by way of purchase of shares.
2. The business activities of the undertakings concerned are:
- for Arcelor: manufacture and distribution of steel,
- for Severstal: manufacture and distribution of steel,
- for Lucchini: manufacture and distribution of steel,
- for Sungrebe: investment holding.
3. The Commission will assess whether the notified transactions fall within the scope of Regulation (EC) No 139/2004 and reserves the final decision on this point.
4. The Commission invites interested third parties to submit their possible observations on the proposed operation to the Commission.
Observations must reach the Commission not later than 10 days following the date of this publication. Observations can be sent to the Commission by fax (fax No (32-2) 296 43 01 or 296 72 44) or by post, under reference number COMP/M.4273 — Sungrebe/Arcelor/Severstal, to the following address:
European Commission
Directorate-General for Competition,
Merger Registry
J-70
B-1049 Bruxelles/Brussel
[1] OJ L 24, 29.1.2004, p. 1.
--------------------------------------------------
