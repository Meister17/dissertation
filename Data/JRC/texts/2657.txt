Commission Decision
of 12 July 2005
amending Decision 97/296/EC drawing up the list of third countries from which the import of fishery products is authorised for human consumption, with respect to Algeria, the Bahamas and Grenada
(notified under document number C(2005) 2551)
(Text with EEA relevance)
(2005/501/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Decision 95/408/EC of 22 June 1995 on the conditions for drawing up, for an interim period, provisional lists of third country establishments from which Member States are authorised to import certain products of animal origin, fishery products or live bivalve molluscs [1], and in particular Article 2(2) thereof,
Whereas:
(1) Commission Decision 97/296/EC of 22 April 1997 drawing up the list of third countries from which the import of fishery products is authorised for human consumption [2], lists the countries and territories from which import of fishery products for human consumption is authorised. Part I of the Annex to that Decision lists the countries and territories covered by a specific Decision under Council Directive 91/493/EEC of 22 July 1991 laying down the health conditions for the production and the placing on the market of fishery products [3], and part II of that Annex lists the countries and territories meeting the conditions set out in Article 2(2) of Decision 95/408/EC.
(2) Commission Decisions 2005/498/EC [4], 2005/499/EC [5] and 2005/500/EC [6], set specific import conditions for fishery products from Algeria, the Bahamas and Grenada. Those countries should therefore be included in the list in part I of the Annex to Decision 97/296/EC.
(3) In the interest of clarity, the lists concerned should be replaced in their entirety.
(4) Decision 97/296/EC should therefore be amended accordingly.
(5) This Decision should apply from the same day as Decisions 2005/498/EC, 2005/499/EC and 2005/500/EC as regards the import of fishery products from Algeria, the Bahamas and Grenada.
(6) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
The Annex to Decision 97/296/EC is replaced by the text in the Annex to this Decision.
Article 2
This Decision shall apply from 28 August 2005.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 12 July 2005.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 243, 11.10.1995, p. 17. Decision as last amended by Directive 2004/41/EC of the European Parliament and of the Council (OJ L 157, 30.4.2004, p. 33) (corrected version in OJ L 195, 2.6.2004, p. 12).
[2] OJ L 122, 14.5.1997, p. 21. Decision as last amended by Decision 2005/219/EC (OJ L 69, 16.3.2005, p. 55).
[3] OJ L 268, 24.9.1991, p. 15. Directive as last amended by Regulation (EC) No 806/2003 (OJ L 122, 16.5.2003, p. 1).
[4] See page 92 of this Official Journal.
[5] See page 99 of this Official Journal.
[6] See page 104 of this Official Journal.
--------------------------------------------------
ANNEX
--------------------------------------------------
