Order of the Court of First Instance of 14 June 2006 — Italy
v Commission
(Case T-110/05) [1]
Parties
Applicant: Italian Republic (represented by: G. Aiello, acting as Agent)
Defendant: Commission of the European Communities (represented by: C. Cattabriga and L. Visaggio, acting as Agents)
Re:
Annulment of Commission Regulation (EC) No 2102/2004 of 9 December 2004 on certain exceptional market support measures for eggs in Italy (OJ 2004 L 365, p. 10), in so far as it fails to provide exceptional market support measures in the poultrymeat sector in accordance with Article 14 of Regulation (EEC) No 2777/75 of the Council of 29 October 1975 on the common organisation of the market in poultrymeat (OJ 1975 L 282, p. 77).
Operative part of the order
1. The action is dismissed as inadmissible.
2. The Italian Republic is ordered to pay the costs.
[1] OJ C 115 of 14. 5.2005.
--------------------------------------------------
