Report
on the annual accounts of the European Environment Agency for the financial year 2004 together with the Agency's replies
(2005/C 332/06)
CONTENTS
1-2 INTRODUCTION
3-6 THE COURT'S OPINION
7-10 OBSERVATIONS
Tables 1 to 4
The Agency's replies
INTRODUCTION
1. The European Environment Agency (hereinafter called the Agency) was established by Council Regulation (EEC) No 1210/90 of 7 May 1990 [1]. It is responsible for setting up an observation network that provides the Commission, the Member States and, more generally, the public with reliable information on the state of the environment. The information should, in particular, enable the European Union and the Member States to take action to safeguard the environment and assess the effectiveness of such action. Table 1 summarises the Agency's competences and activities on the basis of information it has supplied.
2. For information, the annual accounts drawn up by the Agency for the financial year 2004 are summarised in Tables 2, 3 and 4.
THE COURT'S OPINION
3. The Court's opinion is addressed to the European Parliament and the Council in accordance with Article 185(2) of Council Regulation (EC, Euratom) No 1605/2002 [2]; it was drawn up following an examination of the Agency's accounts, as required by Article 248 of the Treaty establishing the European Community.
4. The Agency's accounts for the financial year ended 31 December 2004 [3] were drawn up by its Executive Director, pursuant to Article 13 of Regulation (EEC) No 1210/90, and sent to the Court, which is required to give its opinion on their reliability and on the legality and regularity of the underlying transactions.
5. The Court conducted an audit in accordance with its policies and standards, which are based on international auditing standards that have been adapted to the Community context. The audit was planned and performed to obtain reasonable assurance that the accounts are reliable and the underlying transactions are legal and regular.
6. The Court has obtained a reasonable basis for the opinion expressed below.
Reliability of the accounts
The Agency's accounts for the financial year ended 31 December 2004 are, in all material respects, reliable.
Legality and regularity of the underlying transactions
The transactions underlying the Agency's annual accounts, taken as a whole, are legal and regular.
The observations which follow do not call the Court's opinion into question.
OBSERVATIONS
7. At the end of 2004, all the available appropriations in the budget, i.e. 427189 euro, were transferred to two budget headings under Title III of the budget (operating expenditure) and on the date of the transfer were committed for two operations to be carried out in 2005 [4]. Such a practice is not in accordance with the principle of annuality [5]. Furthermore, with regard to the transfers made from Title I (staff expenditure), they contravene the provisions which prohibit the carrying over of appropriations relating to staff expenditure.
8. The Agency does not conduct a full annual check on the assets to be entered in its inventory. The audit of fixed assets brought to light omissions from the inventory, which, in particular, make it impossible to establish where some assets are located.
9. The analysis of amendments to various contracts (total value: 107000 euro) highlighted several shortcomings: amendments which were not justified for one of the reasons stipulated in the financial regulation [6], the use of a framework contract no longer in force [7], and the value provided for by an amendment was excessive compared with the value stipulated in the initial contract [8].
10. The agreement on the location of the Agency's headquarters and the protocol on the privileges and immunities of the European Communities provide that it shall be exempt from all national, regional and municipal taxes. In November 2004, the Agency was obliged to pay approximately 906000 euro in taxes for the period from 2000 to 2004. Applying the accounting principle of prudence [9], the Agency has not recorded this sum in its balance sheet as recoverable; an explanatory note concerning this payment appears in the revenue and expenditure account. In this instance, the Court recommends that the Agency should obtain reimbursement of all unwarranted payments.
This report was adopted by the Court of Auditors in Luxembourg at its meeting of 5 October 2005.
For the Court of Auditors
Hubert Weber
President
[1] OJ L 120, 11.5.1990.
[2] OJ L 248, 16.9.2002, p. 1.
[3] These accounts were drawn up on 30 June 2005 and received by the Court on 28 September 2005.
[4] Commitments EEA 52051 and EEA 52049.
[5] Article 6 of the Agency's financial regulation.
[6] Contracts Nos 2601/B/2005 (EEA 51948 — 15195 euro) and 3473/B/2004 ((EEA 51971 — 29800 euro).
[7] Contract No 2240/B/2004 (EEA 51995 — 23301 euro).
[8] Contracts Nos 3473/B/2004 (EEA 51921 — 49372 euro) and 3476/B/2004 (EEA 51976 — 38906 euro).
[9] Article 78 of the Agency's financial regulation.
--------------------------------------------------
Table 1
European Environment Agency (Copenhagen)
Source: Information supplied by the Agency.
Areas of Community competence deriving from the Treaty | Competences of the Agency as defined in Council Regulation (EEC) No 1210/90 of 7 May 1990 | Governance | Resources made available to the Agency (2003 data) | Products and services supplied in 2004 |
Environment policy Community policy on the environment shall aim at a high level of protection taking into account the diversity of situations in the various regions of the Community. It shall be based on the precautionary principle and on the principles that preventive action should be taken, that environmental damage should as a priority be rectified at source and that the polluter should pay. (…) In preparing its policy (…), the Community shall take account of available scientific and technical data (…). (Article 174 of the Treaty) | Objectives Setting up of a European environment information and observation network to provide the Community and the Member States with reliable information so that they are able to: (a)take the requisite action to protect the environment;(b)evaluate its implementation;(c)ensure that the public is well-informed about the state of the environment. | Tasks To supply the Community and the Member States with the requisite information;to record and evaluate data on the state of the environment and to report on its quality;to ensure that environmental data are comparable at European level;to promote the incorporation of European environmental data into international programmes;to publish a report every five years on the state of, changes in and outlook for the environment;to encourage the development of environmental forecasting techniques and methods of assessing the cost of damage caused to the environment, and the exchange of information on damage-prevention technology. | 1. Management Board Consisting of: one representative per Member State,two representatives of the Commission,two scientists appointed by the European Parliament. Tasks To adopt the work programme and ensure it is implemented. 2. Executive Director Appointed by the Management Board on a proposal from the Commission. 3. Advisory Forum Consisting of one representative per Member State, it advises the Executive Director. 4. Scientific Committee Consisting of qualified figures in the field of the environment appointed by the Management Board. 5. External audit European Court of Auditors. 6. Discharge Authority Parliament on a recommendation by the Council. | Final budget 33,6 million euro (27,5 million euro) Community subsidy: 81 % (77 %). Staff numbers at 31 December 2004 Number of posts in the establishment plan: 115 (111) posts Posts occupied: 102 (95) + 15 (16) other posts (auxiliary contracts, seconded national experts, local and employment-agency staff). Total staff numbers: 115 (111) assigned to the following tasks: operational: 72 (69);administrative: 42 (41);mixed: 1 (1). | Support work and provision of indicators for the Synthesis report on sustainable development;indicators concerning the state of the environment;analysis and estimation of greenhouse gas emissions (1990-2020);finalisation of the Kiev report (state of the environment in candidate Central and Eastern European countries);three seminars organised under the Presidency of the Council;provision of environmental indicators in specific fields (transport sector in candidate countries, Danube region — Black Sea);assistance in harmonising data;management of the EIONET network (European Environment Information and Observation Network). |
Table 2
European Environment Agency — Implementation of the budget for the financial year 2004
NB: Variations in totals are due to the effects of rounding.
Source: Information supplied by the Agency — This table summarises the data provided by the Agency in its annual accounts.
(1000 euro) |
Revenue | Expenditure |
Origin of revenue | Revenue entered in the final budget for the financial year | Revenue collected | Allocation of expenditure | Appropriations under the final budget | Appropriations carried over from the previous financial year | Available appropriations (2004 budget and financial year 2003) |
entered | committed | paid | carried over | cancelled | entered | committed | paid | cancelled | available | committed | paid | carried over | cancelled |
Community subsidies | 27200 | 27200 | Title I Staff | 12955 | 12908 | 12449 | 501 | 5 | 315 | 315 | 190 | 124 | 13270 | 13223 | 12639 | 501 | 129 |
Other subsidies | 6412 | 3455 | Title II Administration | 3845 | 3845 | 3451 | 394 | 0 | 395 | 395 | 319 | 76 | 4240 | 4240 | 3770 | 394 | 76 |
Other revenue | 0 | 128 | Title III Operating activities | 16812 | 14425 | 9548 | 7264 | 1 | 7008 | 7008 | 6701 | 307 | 23820 | 21433 | 16249 | 7264 | 308 |
Total | 33612 | 30783 | Total | 33612 | 31178 | 25448 | 8159 | 6 | 7718 | 7718 | 7210 | 507 | 41330 | 38896 | 32658 | 8159 | 513 |
Table 3
European Environment Agency — Revenue and expenditure account for the financial years 2004 and 2003
NB: Variations in totals are due to the effects of rounding.
Source: Information supplied by the Agency — This table summarises the data provided by the Agency in its annual accounts.
(1000 euro) |
| 2004 | 2003 |
Revenue
EC subsidy | 28658 | 28723 |
Other subsidies | 1998 | 1080 |
Bank interest | 128 | 89 |
Total revenue (A) | 30784 | 29891 |
Expenditure
Total expenditure for Title I
Payments | 12447 | 11123 |
Payments — Assigned revenue | 2 | |
Appropriations carried over | 435 | 315 |
Appropriations carried over — Assigned revenue | 66 | |
Total expenditure for Title II
Payments | 3451 | 2447 |
Appropriations carried over | 394 | 395 |
Total expenditure for Title III
Payments | 9534 | 5997 |
Payments — Assigned revenue | 14 | |
Appropriations carried over | 4845 | 7008 |
Appropriations carried over — Assigned revenue | 2419 | |
Total expenditure (B) | 33606 | 27284 |
Net result for the year (A – B) | –2822 | 2607 |
Appropriations carried over and cancelled | 507 | 295 |
Refunds carried over and cancelled | 0 | 36 |
Balance carried over from the previous year | –4190 | –7427 |
Assigned revenue cancelled | 98 | 322 |
Exchange-rate differences | 3 | –4 |
Regularisation regarding previous years | 43 | –18 |
Balance carried forward | –6360 | –4190 |
Table 4
European Environment Agency — Balance sheet at 31 December 2004 and 31 December 2003
NB: Variations in totals are due to the effects of rounding.
Source: Information supplied by the Agency — This table summarises the data provided by the Agency in its annual accounts.
(1000 euro) |
Assets | 2004 | 2003 | Liabilities | 2004 | 2003 |
Fixed assets | | | Fixed capital | | |
Buildings (deposit) | 432 | 425 | Own capital | 1838 | 1265 |
Furniture | 483 | 541 | Revenue and Expenditure balance | –6360 | –4190 |
IT equipment | 891 | 266 | | | |
Subtotal | 1806 | 1232 | Subtotal | –4522 | –2925 |
Stock (office supplies) | 32 | 33 | Current liabilities | | |
| | | General cash carried forward | 0 | 1856 |
| | | New Member countries | 0 | 1066 |
Current assets | | | Earmarked projects | 0 | 1269 |
Sums to be recovered | | | | | |
Commission | 0 | 1856 | Automatic carry-overs | 5674 | 7717 |
PHARE contribution | 0 | 1066 | Automatic carry-overs (assigned revenue) | 2708 | 1135 |
Other subsidies to be received | 0 | 1269 | Carry-over of sums to be reused (appropriations for the year) | 93 | 212 |
VAT recoverable | 889 | 232 | Carry-over of sums to be reused (appropriations for the previous year) | 197 | 0 |
Sundry debtors | 109 | 127 | Social security and payroll | 142 | 417 |
Recovery orders | 42 | 65 | Recovery orders | 38 | 65 |
Prepayments | 271 | 0 | | | |
Subtotal | 1310 | 4614 | Subtotal | 8852 | 13737 |
Bank accounts and cash | | | | | |
Bank accounts | 1142 | 4892 | | | |
Imprest accounts | 39 | 40 | | | |
Total | 4329 | 10811 | Total | 4329 | 10811 |
--------------------------------------------------
THE AGENCY'S REPLIES
7. Indeed two commitments were made towards the end of the year and after a final 2004 budget transfer. The resources were allocated to projects that had been defined initially in the 2004 management plan, but start of the projects was delayed due to uncertainties on budget requirements for ongoing recruitments. We are continuing to improve our budget management and execution and hence to better conform with the principle of annuality.
8. The observation is correct and the result of lacking resources in a period of considerable rebuilding and refurbishing of the EEA facilities. Annual control will get due attention from 2005 onwards.
9. We recognise these insufficiencies and will avoid such in future. We will in particular ensure that, for future contracts, any amendments are strictly limited to less than 50 % of the initial contract amount (rather than indicative tender amount) and that framework contracts are less than four years old before using them.
10. The Agency is strenuously pursuing reimbursement of taxes inappropriately charged by the City of Copenhagen for 2000 to 2004. We are in dialogue with the city and the Danish Protocol Office and have informed DG Environment and the Secretary-General. The Commission Legal Services are considering legal proceedings against Denmark.
--------------------------------------------------
