Decision of the EEA Joint Committee No 58/2006
of 2 June 2006
amending Annex VI (Social security) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex VI to the Agreement was amended by Decision of the EEA Joint Committee No 118/2005 of 30 September 2005 [1].
(2) Decision of the Administrative Commission of the European Communities on Social Security for Migrant Workers No 203 of 26 May 2005 amending Decision No 170 of 11 June 1998 concerning the compilation of the lists provided for in Articles 94(4) and 95(4) of Council Regulation (EEC) No 574/72 [2] is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
The following indent shall be added in point 3.51 (Decision No 170) of Annex VI to the Agreement:
- "— 32005 D 0965: Decision No 203 of 26 May 2005 (OJ L 349, 31.12.2005, p. 27)."
Article 2
The texts of Decision No 203 in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 3 June 2006, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [3].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 2 June 2006.
For the EEA Joint Committee
The President
R. Wright
[1] OJ L 339, 22.12.2005, p. 22.
[2] OJ L 349, 31.12.2005, p. 27.
[3] No constitutional requirements indicated.
--------------------------------------------------
