P6_TC1-COD(2004)0098
Position of the European Parliament adopted at first reading on 23 February 2005 with a view to the adoption of European Parliament and Council Directive 2005/.../EC on the recognition of seafarers' certificates issued by the Member States and amending Directive 2001/25/EC
THE EUROPEAN PARLIAMENT AND THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 80(2) thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Economic and Social Committee [1],
Having regard to the opinion of the Committee of the Regions [2],
Acting in accordance with the procedure laid down in Article 251 of the Treaty [3],
Whereas:
(1) In its conclusions of 5 June 2003 on improving the image of Community shipping and attracting young people to the seafaring profession, the Council highlighted the necessity of fostering the professional mobility of seafarers within the European Union, with particular emphasis on recognition procedures for seafarers' certificates of competency, while ensuring thorough compliance with the requirements of the International Maritime Organisation Convention on Standards of Training, Certification and Watchkeeping for Seafarers, 1978 as amended from time to time (STCW Convention).
(2) Maritime transport is an intensively and rapidly developing sector of a particularly international character. Accordingly, in view of the increasing shortage of Community seafarers, the balance between supply and demand in personnel can be maintained more efficiently at the Community rather than the national level. It is therefore essential that the common transport policy in the field of maritime transport be extended to facilitate the movement of seafarers within the Community.
(3) As regards seafarers' qualifications, the Community has laid down minimum maritime education, training and certification requirements by way of Directive 2001/25/EC of the European Parliament and of the Council of 4 April 2001 on the minimum level of training of seafarers [4]. That Directive incorporates into Community law the international training, certification and watchkeeping standards laid down by the STCW Convention.
(4) Directive 2001/25/EC provides that seafarers must hold a certificate of competency issued and endorsed by the competent authority of a Member State in accordance with that Directive and entitling the lawful holder thereof to serve on a ship in the capacity and perform the functions involved at the level of responsibility specified therein.
(5) Under Article 18(1) and (2) of Directive 2001/25/EC mutual recognition among Member States of certificates held by seafarers whether or not nationals of Member States are subject to the provisions of Directives 89/48/EEC [5] and 92/51/EEC [6] setting up, respectively a first and a second general system for the recognition of professional education and training. Those Directives do not provide for the automatic recognition of formal qualifications of seafarers, as seafarers may be subject to compensation measures.
(6) Each Member State should recognise any certificate and other evidence of formal qualifications issued by another Member State in accordance with Directive 2001/25/EC. Therefore, each Member State should permit a seafarer having acquired his/her certificate of competency in another Member State, satisfying the requirements of that Directive, to take up or to pursue the maritime profession for which he/she is qualified, without any prerequisites other than those imposed on its own nationals.
(7) Since this Directive is aimed at facilitating the mutual recognition of certificates, it does not regulate the conditions concerning access to employment.
(8) The STCW Convention specifies language requirements for seafarers. These requirements should be introduced into Community law to ensure effective communication on board ships and facilitate the free movement of seafarers within the Community.
(9) Today, the proliferation of certificates of competency of seafarers obtained by fraud poses a serious danger to safety at sea and the protection of the marine environment. In most cases, holders of fraudulent certificates of competency do not meet the minimum certification requirements of the STCW Convention. These seafarers may easily be involved in maritime accidents.
(10) Member States should therefore take and enforce specific measures to prevent and penalise fraudulent practices associated with certificates of competency as well as pursue their efforts within the International Maritime Organisation to achieve strict and enforceable agreements on the world-wide combating of such practices. The Committee on Safe Seas and the Prevention of Pollution from Ships is an appropriate forum for exchanging information, experience and best practices in this respect.
(11) Regulation (EC) No 1406/2002 [7] established a European Maritime Safety Agency (the Agency), for the purpose of ensuring a high, uniform and effective level of maritime safety and prevention of pollution by ships. One of the tasks assigned to the Agency is to assist the Commission in the performance of any task assigned to it by Community legislation applicable to training, certification and watchkeeping of ships' crews.
(12) The Agency should therefore assist the Commission in verifying that Member States comply with the requirements laid down in this Directive and Directive 2001/25/EC.
(13) The mutual recognition among Member States of certificates held by seafarers whether or not nationals of Member States should no longer be subject to the provisions of Directives 89/48/EEC and 92/51/EEC, but should be governed by the provisions of this Directive.
(14) The Council should, in accordance with paragraph 34 of the Interinstitutional Agreement on better law-making [8], encourage Member States to draw up, for themselves and in the interest of the Community, their own tables, which will, as far as possible, illustrate the correlation between this Directive and their transposition measures and to make those tables public.
(15) Directive 2001/25/EC should therefore be amended accordingly,
HAVE ADOPTED THIS DIRECTIVE:
Article 1
Scope
This Directive shall apply to seafarers who are:
(a) nationals of a Member State;
(b) non-nationals who hold a certificate issued by a Member State.
Article 2
Definitions
For the purposes of this Directive the following definitions shall apply:
(a) "seafarer" means a person who is trained and who is certificated by a Member State at least in accordance with the requirements laid down in Annex I to Directive 2001/25/EC;
(b) "certificate" means a valid document within the meaning of Article 4 of Directive 2001/25/EC;
(c) "appropriate certificate" means a certificate as defined in Article 1 point 27) of Directive 2001/25/EC;
(d) "endorsement" means a valid document issued by the competent authority of a Member State in accordance with Article 5(2) and (6) of Directive 2001/25/EC;
(e) "recognition" means the acceptance by the competent authorities of a host Member State of a certificate or appropriate certificate issued by another Member State;
(f) "host Member State" means any Member State in which a seafarer seeks recognition of his/her appropriate certificate(s) or other certificate(s);
(g) "STCW Convention" means the International Convention on Standards of Training, Certification and Watchkeeping for Seafarers, 1978, as amended from time to time;
(h) "STCW Code" means the Seafarers' Training, Certification and Watchkeeping Code, as adopted by Resolution 2 of the 1995 STCW Conference of Parties, as amended from time to time;
(i) "the Agency" means the European Maritime Safety Agency, established by Regulation (EC) No 1406/2002.
Article 3
Recognition of certificates
1. Every Member State shall recognise appropriate certificates or other certificates issued by another Member State in accordance with the requirements laid down in Directive 2001/25/EC.
2. The recognition of appropriate certificates shall be limited to the capacities, functions and levels of competency prescribed therein and be accompanied by an endorsement attesting such recognition.
3. Member States shall ensure the right to appeal against any refusal to endorse a valid certificate, or the absence of any response, in accordance with national legislation and procedures.
4. Notwithstanding paragraph 3, the competent authorities of a host Member State may impose further limitations on capacities, functions and levels of competence relating to near-coastal voyages, as referred to in Article 7 of Directive 2001/25/EC, or alternative certificates issued under Regulation VII/1 of Annex I to Directive 2001/25/EC.
5. A host Member State shall ensure that seafarers who present for recognition certificates for functions at the management level have an appropriate knowledge of the maritime legislation of that Member State relevant to the functions they are permitted to perform.
Article 4
Amendments to Directive 2001/25/EC
Directive 2001/25/EC shall be amended as follows:
1) Article 4 is replaced by:
"Article 4
Certificate
A certificate shall be any valid document, by whatever name it may be known, issued by or under the authority of the competent authority of a Member State in accordance with Article 5 and with the requirements laid down in Annex I."
2) The following Article 7a is inserted:
"Article 7a
Prevention of fraud and other unlawful practices
1. Member States shall take and enforce the appropriate measures to prevent fraud and other unlawful practices involving the certification process or certificates issued and endorsed by their competent authorities, and shall provide for sanctions that are effective, proportionate and dissuasive.
2. Member States shall designate the national authorities competent to detect and combat fraudulent practices and exchange information with other countries' competent authorities concerning the certification of seafarers.
Member States shall forthwith inform the other Member States and the Commission thereof.
Member States shall also forthwith inform thereof any third countries with which they have entered into an undertaking in accordance with Regulation I/10, paragraph 1.2 of the STCW Convention.
3. At the request of a host Member State, the competent authorities of another Member State shall provide written confirmation or denial of the authenticity of seafarers' certificates, corresponding endorsements or any other documentary evidence of training issued in that other Member State'"
3) Article 18(1) and (2) are deleted with effect from [...] [9];
4) The following Article 21a is inserted:
"Article 21a
Regular Monitoring of compliance
Without prejudice to the powers of the Commission under Article 226 of the Treaty, the Commission, assisted by the Agency, shall verify on a regular basis and at least every five years that Member States comply with the minimum requirements laid down by this Directive;"
5) The following Article 21b is inserted:
"Article 21b
Compliance report
No later than [...] [10] the Commission must submit to the European Parliament and the Council an evaluation report drawn up on the basis of the information obtained pursuant to Article 21a. In this report the Commission will analyse the Member States' compliance with this Directive and, where necessary, make proposals for additional measures"
6) The following paragraph 1a is inserted in Annex I, Chapter I:
"1a. Member States shall ensure that seafarers possess adequate language proficiency, as defined in Sections A-II/1, A-III/1, A-IV/2 and A-II/4 of the STCW Code so as to enable them to perform their specific duties on a vessel flying the flag of a host Member State."
Article 5
Transposition
1. Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive by [...] [11] at the latest. They shall forthwith communicate to the Commission the text of those provisions.
When Member States adopt those provisions, they shall contain a reference to this Directive or be accompanied by such a reference on the occasion of their official publication. Member States shall determine how such reference is to be made.
2. Member States shall communicate to the Commission the text of the provisions of national law which they adopt in the field covered by this Directive.
Article 6
Entry into force
This Directive shall enter into force on the twentieth day following that of its publication in the Official Journal of the European Union.
Article 7
Addressees
This Directive is addressed to the Member States.
Done at ..., on ...
For the European Parliament
The President
For the Council
The President
[1] OJ C ...
[2] OJ C ...
[3] Position of the European Parliament of 23 February 2005.
[4] OJ L 136, 18.5.2001, p. 17. Directive as last amended by Directive 2003/103/EC (OJ L 326, 13.12.2003, p. 28).
[5] Council Directive 89/48/EEC of 21 December 1988 on a general system for the reconition of higher-education diplomas awarded on completion of professional education and training of at least three years' duration (OJ L 19, 24.1.1989, p. 16). Directive as amended by Directive 2001/19/EC of the European Parliament and of the Council (OJ L 206, 31.7.2001, p. 1).
[6] Council Directive 92/51/EEC of 18 June 1992 on a second general system for the recognition of professional education and training to supplement Directive 89/48/EEC (OJ L 209, 24.7.1992, p. 25). Directive as last amended by Commission Decision 2004/108/EC (OJ L 32, 5.2.2004, p. 15).
[7] Regulation (EC) No 1406/2002 of the European Parliament and of the Council of 27 June 2002 establishing a European Maritime Safety Agency (OJ L 208, 5.8.2002, p. 1). Regulation as last amended by Regulation (EC) No 724/2004 (OJ L 129, 29.4.2004, p. 1).
[8] OJ C 321, 31.12.2003, p. 1.
[9] 24 months after the entry into force of this Directive.
[10] 5 years from the date of entry into force of this Directive.
[11] 24 months after the date of entry into force of this Directive.
--------------------------------------------------
