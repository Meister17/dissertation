Authorisation for State aid pursuant to Articles 87 and 88 of the EC Treaty
Cases where the Commission raises no objections
(2006/C 241/12)
(Text with EEA relevance)
Date of adoption : 6.9.2005
Member States : Germany
Aid No : NN 71/05
Title : HSH Nordbank
Aid intensity or amount : Measure not constituting aid
Duration : Unlimited
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
--------------------------------------------------
