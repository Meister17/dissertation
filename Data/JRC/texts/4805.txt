Commission Decision
of 8 March 2005
repealing Decision 2004/440/EC adopting a transitional measure in favour of certain establishments in the milk sector in Slovakia
(notified under document number C(2005) 519)
(Text with EEA relevance)
(2005/198/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to the Act of Accession of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia, and in particular Article 42 thereof,
Whereas:
(1) Commission Decision 2004/440/EC of 29 April 2004 adopting a transitional measure in favour of certain establishments in the milk sector in Slovakia [1] has granted a transitional period, until 30 October 2004, to one establishment in the milk sector in Slovakia in view to fully comply with the structural requirements laid down by Community legislation.
(2) Following an official declaration from the Slovak competent authority, the milk establishment has completed its upgrading process and is now in full compliance with Community legislation.
(3) Decision 2004/440/EC should therefore be repealed.
(4) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
Decision 2004/440/EC is repealed.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 8 March 2005.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 154, 30.4.2004, p. 97, corrected version (OJ L 189, 27.5.2004, p. 79).
--------------------------------------------------
