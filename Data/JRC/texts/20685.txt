COUNCIL DECISION of 10 March 1994 concerning the amendments to the reservations entered by the Community in respect of some provisions of certain Annexes to the international Convention on the simplification and harmonization of customs procedures (94/167/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 113 thereof,
Having regard to the proposal from the Commission,
Whereas, by Decision 75/199/EEC (1), the Community became a Contracting Party to the international Convention on the simplification and harmonization of customs procedures;
Whereas, by the said Decision and Decisions 77/415/EEC (2), 78/528/EEC (3), 80/391/EEC (4), 85/204/EEC (5), 86/103/EEC (6), 87/593/EEC (7), 87/594/EEC (8), 88/355/EEC (9) and 88/356/EEC (10), the Community has accepted 18 Annexes to the said Convention;
Whereas, pursuant to Article 5 (1) of the said Convention, the Community has entered reservations on some 'Standards and Recommended Practices' defined in these Annexes in order to cover the special requirements of the customs union;
Whereas, pursuant to Article 5 (2) of the said Convention, the Community is required to review, once every three years at least, the Standards and Recommended Practices in respect of which it has entered reservations; whereas Article 5 (1) of the Convention allows Contracting Parties to enter reservations even after an Annex has been accepted,
HAS DECIDED AS FOLLOWS:
Article 1
The reservations entered by the Community in respect of Annexes A.1, A.2, B.1, B.2, B.3, C.1, D.1, D.2, E.1, E.3, E.4, E.5, E.6, E.8, F.1, F.2, F.3 and F.6 of the international Convention on the simplification and harmonization of customs procedures shall be amended as a result of the review the outcome of which is set out in the Annex to this Decision.
Article 2
The President of the Council shall designate the person authorized to notify the Secretary-General of the Customs Cooperation Council of the outcome of the review referred to in Article 1.
Done at Brussels, 10 March 1994.
For the Council
The President
Y. PAPANTONIOU
(1) OJ No L 100, 21. 4. 1975, p. 1.
(2) OJ No L 166, 4. 7. 1977, p. 1.
(3) OJ No L 160, 17. 6. 1978, p. 13.
(4) OJ No L 100, 17. 4. 1980, p. 27.
(5) OJ No L 87, 27. 3. 1985, p. 8.
(6) OJ No L 88, 3. 4. 1986, p. 42.
(7) OJ No L 362, 22. 12. 1987, p. 1.
(8) OJ No L 362, 22. 12. 1987, p. 8.
(9) OJ No L 161, 28. 6. 1988, p. 3.
(10) OJ No L 161, 28. 6. 1988, p. 12.
ANNEX
Outcome of the review, conducted by the Community in accordance with the requirements of Article 5 (2) of the international Convention on the simplification and harmonization of customs procedures, of the reservations entered by the Community in respect of the Annexes to the said Convention 1. Annex A.1
concerning customs formalities prior to the lodgement of the goods declaration
1.1. The reservation in respect of Standard 11 shall be lifted.
1.2. The reservation in respect of Standard 21 shall be maintained.
2. Annex A.2
concerning the temporary storage of goods
2.1. The general reservation shall be maintained.
2.2. A reservation worded as follows shall be entered in respect of Recommended Practice 10:
'Community legislation provides that the customs authority may require the person holding the goods to lodge a security with a view to ensuring payment of any customs debt which may arise.'
2.3. The wording of the reservation entered in respect of Recommended Practice 13 shall be replaced by:
'Community legislation authorizes goods in temporary storage to be subjectg only of such forms of handling as are designed to ensure their preservation in an unaltered state, without modifying their appearance or the technical characteristics.'
3. Annex B.1
concerning clearance for home use
3.1. The wording of the second paragraph of the general reservation shall be replaced by:
'Community legislation distinguishes between releasing goods for free circulation and releasing them for home use. To obtain the release of goods for free circulation simply requires customs duty to be paid whereas for goods to be released for home use, various other provisions, e.g. on taxation, also have to be applied.
It should also be pointed out that release for free circulation and release for home use in the Community generally occur at the same time and in the territory of the same Member State.'
3.2. The reservations in respect of Recommended Practices 19 and 52 shall be maintained.
3.3. The first sentence of the wording of the reservation in respect of Standard 28 shall be replaced by the following:
'The Community also applies the provisions of this Standard if declarations are incomplete.'
4. Annex B.2
concerning relief from import duties and taxes in respect of goods declared for home use
4.1. The general reservation and the reservations in respect of Recommended Practices 20, 27, 32 and 33 and of Standard 34 shall be maintained.
4.2. The wording of the reservation in respect of Standard 3 shall be replaced by:
'Community legislation only grants the exemption to consignments of negligible value if they are dispatched directly from a third country to consignee in the Community.'
4.3. In the last sentence of the wording of the reservation in respect of Recommended Practice 10, 'is accompanied' shall be replaced by 'may be accompanied.'
4.4. The wording of the reservation in respect of Recommended Practice 16 shall be replaced by:
'The exemption is granted to the substances concerned if they are to be used exclusively for non-commercial, medical or scientific purposes.'
4.5. A second paragraph worded as follows shall be added to the wording of the reservation in respect of Recommended Practice 18:
'Member States have the option of excluding dual-use vehicles which are used for commercial or professional purposes from the relief granted on value added tax.'
4.6. The wording of the reservation in respect of Recommended Practice 9 shall be replaced by:
'Relief from import duties and taxes on goods covered by Standard 17 may be made subject to the condition that any customs duties or taxes to which the goods are normally liable in their country of departure or origin have actually been levied. The period for which the beneficiary must continue to own or possess the goods after importing them is set at 12 months.'
4.7. The wording of the reservation in respect of Standard 21 shall be replaced by:
'Member States have the option of making relief from value added tax on trousseaux and movable property belonging to the person transferring his or her residence subject to the condition that any customs duties and/or taxes to which the goods are normally liable in their country of departure or origin have actually been levied.
The exemption . . . (remainder unchanged).'
4.8. The wording of the first indent of the reservation in respect of Recommended Practice 23 shall be replaced by the following:
'- not earlier than two months before the date fixed for the wedding. In this case, the relief shall be subject to the lodging of an appropriate security for the customs duties whereas this security is optional as far as tax exemption is concerned, and . . .'.
4.9. The wording of the second paragraph and the table in the reservation in respect of Standard 28 shall be replaced by:
'In addition to the quantitative restrictions on tobacco, alcohol and alcoholic beverages, Community legislation lay down, in respect of the products listed below, the maximum tax or duty and tax allowances and the maximum quantities shown opposite each product:
Exemption from:
taxes
(a) coffee: 500 grams
or
coffee-extracts and essences: 200 grams
(b) tea: 100 grams
or
tea extracts and essences: 40 grams
duties and taxes:
(c) perfumes: 50 grams
or
toilet waters: 0,25 litre.'
4.10. The beginning of the first sentence of the reservation in respect of Recommended Practice 29 shall be replaced by the following:
'The goods referred to in the Recommended Practice are admitted free of import duties and taxes . . .'.
4.11. The wording of the reservation in respect of Recommended Practice 35 shall be replaced by:
'Community legislation provides for the relief referred to in this Recommended Practice subject to the materials in question not normally being reusable. Relief from duty on these materials is granted subject to the further condition that their value is included in the tax base for the goods carried.'
5. Annex B.3
concerning reimportation in the same State
5.1. The general reservation and the reservations in respect of Recommended Practices 8 and 24 shall be maintained.
5.2. The reservation in respect of Recommended Practice 11 shall be lifted.
6. Annex C.1
concerning outright exportation
6.1. The reservations (general reservation and the reservations in respect of Recommended Practice 10 and Standard 21) shall be maintained.
7. Annex D.1
concerning rules of origin
7.1. The reservations in respect of Standard 7 and Recommended Practice 10 shall be maintained.
7.2. In the second paragraph of the wording of the reservation in respect of Standard 8, 'CCC Nomenclature' shall be replaced by 'Nomenclature of the Harmonized System.'
8. Annex D.2
concerning documentary evidence of origin
8.1. The reservations in respect of Recommended Practice 3, 10 and 12 shall be maintained.
9. Annex E.1
concerning customs transit
9.1. The wording of the general reservation shall be replaced by:
'Although the Member States of the Community constitute a single territory, each Member State has the option to set up simplified transit procedures applicable in certain circumstances to goods which are not intended to move through the territory of another Member State.
Member States also have the option to establish amongst themselves, via bilateral or multilateral arrangements, simplified procedures which comply with criteria established as necessary and are applicable to certain traffic or to specific enterprises.'
10. Annex E.3
concerning customs warehouses
10.1. The general reservation shall be maintained.
10.2. The following reservation shall be entered in respect of Recommended Practice 9:
'Community legislation requires "appropriate customs surveillance" to be ensured within the warehousing procedure. Independently of the surveillance methods used, Member States have the option of requiring a security.'
10.3. The following reservation shall be entered in respect of Recommended Practice 11:
'Normally the Community applies the provisions of this Recommended Practice but it reserves the right not to do so in exceptional circumstances.'
10.4. The following reservation shall be entered in respect of Recommended Practice 13:
'This Recommended Practice does not apply to value added tax.
The Member States of the Community are free to decide whether or not to allow the repayment of customs duties.'
10.5. The following reservation shall be entered in respect of Recommended Practice 15:
'Whether or not the goods in question are to be exported subsequently, Community legislation does not provide for the storage in customs warehouses of goods that are liable to, or have borne, internal duties or taxers.'
10.6. The wording of the reservation in respect of Standard 18 shall be replaced by the following:
'In the Community, import goods may undergo the usual forms of handling to ensure their preservation or marketable quality or prepare them for distribution or resale.
In certain circumstances, goods governed by the common agricultural policy may be handled only in those ways specifically provided for in their case.'
10.7. The wording of the reservation in respect of Standard 19 shall be replaced by:
'Community legislation on customs warehouses does not fix the maximum duration of storage. Nevertheless, in exceptional cases the period may be less than one year.'
11. Annex E.4
concerning drawback
11.1. The reservations (general reservation and the reservations in respect of Standard 5) shall be maintained.
12. Annex E.5
concerning temporary admission subject to re-exportation in the same State
12.1. The general reservation and the reservations in respect of Standard 14 and Recommended Practices 33 and 37 shall be maintained.
12.2. A reservation worded as follows shall be entered in respect of Standard 4:
'Community legislation does not grant temporary admission for goods which, for the purposes of discharging the temporary admission or inward processing procedures, have been entered under the customs warehouse or free zone procedures prior to being re-exported (free zones have been created in Denmark, Germany, Greece, Spain, Ireland, Italy, Portugal and the United Kingdom).'
12.3. A reservation worded as follows shall be entered in respect of Recommended Practice 5:
'Normally the Community applies the provisions of this Recommended Practice, but it reserves the right not to do so in exceptional circumstances.'
12.4. The wording of the reservation in respect of Standard 23 shall be replaced by:
'In the Member States where free zones exist (Denmark, Germany, Greece, Spain, Ireland, Italy, Portugal and United Kingdom) this Standard is applicable only if the goods are introduced into the zones with a view to subsequent exportation from the customs territory of the Communities.'
12.5. Four reservations, one each on packings, commercial vehicles, containers and pallets, shall be entered in respect of Recommended Practice 36. They shall be worded as follows:
Reservation on packings
'Community legislation authorizes temporary admission on the basis of an oral declaration accompanied by an inventory, for packings bearing indelible, non-detachable marks and imported full by a person established outside the customs territory of the Community. Written declarations are required for all other types of packings entered under the temporary admission arrangements.
Where re-exportation is not in doubt because of commercial usage, Community legislation also allows temporary admission without a security for packings imported empty and bearing indelible, non-detachable marks. Provision is also made for granting exemptions from the requirement to provide a security in respect of packings imported full and subject to a verbal declaration accompanied by an inventory, unless an express request to the contrary is made to the competent authorities.'
Reservation on commercial road vehicles
'Community legislation provides that, where there is a serious risk of failure to comply with the obligation to re-export a commercial road vehicle, the temporary admission procedure applies subject to the production of a document of a type laid down in an international Convention or the lodging of a declaration; the customs authority has the option of requiring a security when the declaration is lodged.'
Reservation on containers
'For all containers, whether or not approved for carrying goods under customs seal, Community legislation authorizes temporary admission without formalities from the time of entry into Community customs territory, provided that the containers bear durably applied markings in an appropriate, clearly visible position identifying the owner or operator and giving the marks and identification numbers used by the owner or operator, the tare weight of the container (except in the case of swap bodies used in combined road-rail transport) and the country of registration of the container (except in the case of air transport).
Containers not fulfulling these conditions may be granted temporary admission to Community customs territory once a written application has been submitted, an authorization issued and, where there are well-founded or serious doubts regarding the obligation to re-export, a list has been produced and/or a security constituted.'
Reservation on pallets
'As long as pallets are identifiable, Community legislation authorizes their temporary admission without formalities from the time of entry into Community customs territory. Where they are not identifiable, a written application has to be submitted and an authorization issued.
In both cases a written declaration and, where necessary, a security may be required if the customs consider that there is a serious risk of failure to comply with the obligation to re-export.'
12.6. The beginning of the second sentence of the reservation in respect of Recommended Practice 38 shall be replaced by the following:
'Temporary admission with partial relief from import duties is not applied to consumable products and goods whose. . .'.
13. Annex E.6
concerning temporary admission for inward processing
13.1. The general reservation and the reservation in respect of Standard 19 shall be maintained.
13.2. The wording of the reservation in respect of Recommended Practice 5 shall be replaced by:
'Normally the Community applies the provisions of this Recommended Practice, but it reserves the right not to do so in exceptional circumstances.'
13.3. A reservation worded as follows shall be entered in respect of Recommended Practice 16:
'Where the customs authority requires a security, the type and amount of security shall be fixed by the customs authority of each Member State.'
13.4. The wording of the reservation in respect of Standard 34 shall be replaced by:
'In Member States where free zones exist (Denmark, Germany, Greece, Spain, Ireland, Italy, Portugal and the United Kingdom) this Standard shall be applicable when the compensatory products enter the zones with a view to their subsequent exportation from the customs territory of the Community.'
13.5. The wording of the reservation in respect of Recommended Practice 39 shall be replaced by:
'Community legislation does not provide for any limitation similar to that mentioned in this Recommended Practice. If a customs debt relating to the compensating products or to the goods in their unaltered state occurs, it gives rise to the payment of compensatory interest on the amount of the import duties due.'
14. Annex E.8
concerning temporary exportation for outward processing
14.1. The general reservation and the reservation in respect of Standard 20 shall be maintained.
14.2. The wording of the reservation in respect of Recommended Practice 3 shall be replaced by:
'Normally the Community applies the provisions of this Recommended Practice, but it reserves the right not to do so in exceptional circumstances.'
15. Annex F.1
concerning free zones
15.1. The wording of the general reservation shall be replaced by:
'Community legislation allows Member States the option of constituting certain areas of Community customs territory as free zones. So far free zones of this type have been set up by Denmark, Germany, Greece, Spain, Ireland, Italy, Portugal and the United Kingdom.
In addition Community legislation provides for a particular type of free zone called a 'free warehouse', to which exactly the same rules apply as those governing the free zones. So far, free warhouses have been set up in Spain, France, Italy, the Netherlands and Portugal.
Moreover, Community legislation covers only part of the procedures dealt with in this Annex. In respect of areas not covered by Community legislation, the Member States shall enter their own reservations if necessary.'
15.2. The wording of the reservation in respect of Standard 21 shall be replaced by:
'Community legislation does not lay down a maximum storage period. Nevertheless, in exceptional cases, the period may be less than one year.'
16. Annex F.2
concerning processing of goods for home use
16.1. The reservations (general reservation and the reservations in respect of Recommended Practice 7) shall be maintained.
17. Annex F.3
concerning customs facilities applicable to travellers
17.1 The general reservation and the reservations in respect of Standards 21, 38 and 44 and Recommended Practice 45 shall be maintained.
17.2. A reservation worded as follows shall be entered in respect of Recommended Practice 18:
'Community legislation does not provide for a system of flat-rate assessment as far as value added tax and excise duties are concerned.'
17.3. A reservation worded as follows shall be entered in respect of Recommended Practice 31:
'Community legislation provides that when there is a serious risk of failure to comply with the obligation to re-export the means of transport for private use, the temporary admission procedure applies subject to the production of a document of a type laid down in an international Convention or the lodging of a declaration; the customs authority has the option of requiring a security when the declaration is lodged.'
18. Annex F.6
concerning the repayment of import duties and taxes
18.1. The reservations (general reservation and the reservations in respect of Standard 7) shall be maintained.
