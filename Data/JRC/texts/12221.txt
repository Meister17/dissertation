Commission Regulation (EC) No 1461/2006
of 29 September 2006
amending Council Regulation (EC) No 2580/2001 on specific restrictive measures directed against certain persons and entities with a view to combating terrorism
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 2580/2001 of 27 December 2001 on specific restrictive measures directed against certain persons and entities with a view to combating terrorism [1], and in particular Article 7 thereof,
Whereas:
(1) The Annex to Regulation (EC) No 2580/2001 lists the competent authorities to whom information and requests concerning the measures imposed by that Regulation should be sent.
(2) The Czech Republic, Estonia and Greece requested that the information concerning their competent authorities be amended. The address of the Commission should also be amended,
HAS ADOPTED THIS REGULATION:
Article 1
The Annex to Regulation (EC) No 2580/2001 is hereby amended as set out in the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 29 September 2006.
For the Commission
Eneko Landáburu
Director-General for External Relations
[1] OJ L 344, 28.12.2001, p. 70. Regulation as last amended by Commission Regulation (EC) No 1957/2005 (OJ L 314, 30.11.2005, p. 16).
--------------------------------------------------
ANNEX
The Annex to Regulation (EC) No 2580/2001 is amended as follows:
(1) The address details under the heading "Czech Republic" shall be replaced with:
"Ministerstvo financí/Ministry of Finance
Finanční analytický útvar/Financial Analytical Unit
PO BOX 675
Jindřišská 14
111 21 Praha 1
Tel.: (420-2) 570 44 501
Fax: (420-2) 570 44 502
E-mail: fau@mfcr.cz".
(2) The address details under the heading "Estonia" shall be replaced with:
"Välisministeerium
Islandi väljak 1
15049 Tallinn
Tel: (+372) 6 377 100
Fax: (+372) 6 377 199
Finantsinspektsioon
Sakala 4
15030 Tallinn
Tel: (+372) 66 80 500
Fax: (+372) 66 80 501".
(3) The address details under the heading "Greece" shall be replaced with:
"Ministry of National Economy
General Directorate of Economic Policy
5 Nikis str.
GR-105 63 Athens
Tel. (30-210) 333 27 81-2
Fax (30-210) 333 28 10
Yπουργείο Εθνικής Οικονομίας
Γενική Διεύθυνση Οικονομικής Πολιτικής
Νίκης 5
GR-105 63 Αθήνα
Τηλ.: (30-210) 333 27 81-2
Φαξ: (30-210) 333 28 10".
(4) The address details under the heading "European Community" shall be replaced with:
"Commission of the European Communities
Directorate-General for External Relations
Directorate A. Crisis Platform and Policy Coordination in CFSP
Unit A.2. Crisis Management and Conflict Prevention
CHAR 12/45
B-1049 Brussels
Tel. (32-2) 295 55 85, 299 11 76
Fax (32-2) 299 08 73
E-mail: relex-sanctions@ec.europa.eu".
--------------------------------------------------
