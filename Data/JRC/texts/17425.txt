Commission Regulation (EC) No 794/2004 of 21 april 2004 implementing Council Regulation (EC) No 659/1999 laying down detailed rules for the application of Article 93 of the EC Treaty
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 659/1999 of 22 March 1999 laying down detailed rules for the application of Article 93 of the EC Treaty(1), and in particular Article 27 thereof,
After consulting the Advisory Committee on State Aid,
Whereas:
(1) In order to facilitate the preparation of State aid notifications by Member States, and their assessment by the Commission, it is desirable to establish a compulsory notification form. That form should be as comprehensive as possible.
(2) The standard notification form as well as the summary information sheet and the supplementary information sheets should cover all existing guidelines and frameworks in the state aid field. They should be subject to modification or replacement in accordance with the further development of those texts.
(3) Provision should be made for a simplified system of notification for certain alterations to existing aid. Such simplified arrangements should only be accepted if the Commission has been regularly informed on the implementation of the existing aid concerned.
(4) In the interests of legal certainty it is appropriate to make it clear that small increases of up to 20 % of the original budget of an aid scheme, in particular to take account of the effects of inflation, should not need to be notified to the Commission as they are unlikely to affect the Commission's original assessment of the compatibility of the scheme, provided that the other conditions of the aid scheme remain unchanged.
(5) Article 21 of Regulation (EC) No 659/1999 requires Member States to submit annual reports to the Commission on all existing aid schemes or individual aid granted outside an approved aid scheme in respect of which no specific reporting obligations have been imposed in a conditional decision.
(6) For the Commission to be able to discharge its responsibilities for the monitoring of aid, it needs to receive accurate information from Member States about the types and amounts of aid being granted by them under existing aid schemes. It is possible to simplify and improve the arrangements for the reporting of State aid to the Commission which are currently described in the joint procedure for reporting and notification under the EC Treaty and under the World Trade Organisation (WTO) Agreement set out in the Commission's letter to Member States of 2 August 1995 . The part of that joint procedure relating to Member States reporting obligations for subsidy notifications under Article 25 of the WTO Agreement on Subsidies and Countervailing measures and under Article XVI of GATT 1994, adopted on 21 July 1995 is not covered by this Regulation.
(7) The information required in the annual reports is intended to enable the Commission to monitor overall aid levels and to form a general view of the effects of different types of aid on competition. To this end, the Commission may also request Member States to provide, on an ad hoc basis, additional data for selected topics. The choice of subject matter should be discussed in advance with Member States.
(8) The annual reporting exercise does not cover the information, which may be necessary in order to verify that particular aid measures respect Community law. The Commission should therefore retain the right to seek undertakings from Member States, or to attach to decisions conditions requiring the provision of additional information.
(9) It should be specified that time-limits for the purposes of Regulation (EC) No 659/1999 should be calculated in accordance with Regulation (EEC, Euratom) No 1182/71 of the Council of 3 June 1971 determining the rules applicable to periods, dates and time limits(2), as supplemented by the specific rules set out in this Regulation. In particular, it is necessary to identify the events, which determine the starting point for time-limits applicable in State aid procedures. The rules set out in this Regulation should apply to pre-existing time-limits which will continue to run after the entry into force of this Regulation.
(10) The purpose of recovery is to re-establish the situation existing before aid was unlawfully granted. To ensure equal treatment, the advantage should be measured objectively from the moment when the aid is available to the beneficiary undertaking, independently of the outcome of any commercial decisions subsequently made by that undertaking.
(11) In accordance with general financial practice it is appropriate to fix the recovery interest rate as an annual percentage rate.
(12) The volume and frequency of transactions between banks results in an interest rate that is consistently measurable and statistically significant, and should therefore form the basis of the recovery interest rate. The inter-bank swap rate should, however, be adjusted in order to reflect general levels of increased commercial risk outside the banking sector. On the basis of the information on inter-bank swap rates the Commission should establish a single recovery interest rate for each Member State. In the interest of legal certainty and equal treatment, it is appropriate to fix the precise method by which the interest rate should be calculated, and to provide for the publication of the recovery interest rate applicable at any given moment, as well as relevant previously applicable rates.
(13) A State aid grant may be deemed to reduce a beneficiary undertaking's medium-term financing requirements. For these purposes, and in line with general financial practice, the medium-term may be defined as five years. The recovery interest rate should therefore correspond to an annual percentage rate fixed for five years.
(14) Given the objective of restoring the situation existing before the aid was unlawfully granted, and in accordance with general financial practice, the recovery interest rate to be fixed by the Commission should be annually compounded. For the same reasons, the recovery interest rate applicable in the first year of the recovery period should be applied for the first five years of the recovery period, and the recovery interest rate applicable in the sixth year of the recovery period for the following five years.
(15) This Regulation should apply to recovery decisions notified after the date of entry into force of this Regulation,
HAS ADOPTED THIS REGULATION:
CHAPTER I
SUBJECT MATTER AND SCOPE
Article 1
Subject matter and scope
1.This Regulation sets out detailed provisions concerning the form, content and other details of notifications and annual reports referred to in Regulation (EC) No 659/1999. It also sets out provisions for the calculation of time limits in all procedures concerning State aid and of the interest rate for the recovery of unlawful aid.
2.This Regulation shall apply to aid in all sectors.
CHAPTER II
NOTIFICATIONS
Article 2
Notification forms
Without prejudice to Member States' obligations to notify state aids in the coal sector under Commission Decision 2002/871/CE(3), notifications of new aid pursuant to Article 2(1) of Regulation (EC) No 659/1999, other than those referred to in Article 4(2), shall be made on the notification form set out in Part I of Annex I to this Regulation.
Supplementary information needed for the assessment of the measure in accordance with regulations, guidelines, frameworks and other texts applicable to State aid shall be provided on the supplementary information sheets set out in Part III of Annex I.
Whenever the relevant guidelines or frameworks are modified or replaced, the Commission shall adapt the corresponding forms and information sheets.
Article 3
Transmission of notifications
1.The notification shall be transmitted to the Commission by the Permanent Representative of the Member State concerned. It shall be addressed to the Secretary - General of the Commission.
If the Member State intends to avail itself of a specific procedure laid down in any regulations, guidelines, frameworks and other texts applicable to State aid, a copy of the notification shall be addressed to the Director-General responsible. The Secretary - General and the Directors - General may designate contact points for the receipt of notifications.
2.All subsequent correspondence shall be addressed to the Director - General responsible or to the contact point designated by the Director - General.
3.The Commission shall address its correspondence to the Permanent Representative of the Member State concerned, or to any other address designated by that Member State.
4.Until 31 December 2005 notifications shall be transmitted by the Member State on paper. Whenever possible an electronic copy of the notification shall also be transmitted.
With effect from 1 January 2006 notifications shall be transmitted electronically, unless otherwise agreed by the Commission and the notifying Member State.
All correspondence in connection with a notification which has been submitted after 1 January 2006 shall be transmitted electronically.
5.The date of transmission by fax to the number designated by the receiving party shall be considered to be the date of transmission on paper, if the signed original is received no later than ten days thereafter.
6.By 30 September 2005 at the latest, after consulting Member States, the Commission shall publish in the Official Journal of the European Union details of the arrangements for the electronic transmission of notifications, including addresses together with any necessary arrangements for the protection of confidential information.
Article 4
Simplified notification procedure for certain alterations to existing aid
1.For the purposes of Article 1(c) of Regulation (EC) No 659/1999, an alteration to existing aid shall mean any change, other than modifications of a purely formal or administrative nature which cannot affect the evaluation of the compatibility of the aid measure with the common market. However an increase in the original budget of an existing aid scheme by up to 20 % shall not be considered an alteration to existing aid.
2.The following alterations to existing aid shall be notified on the simplified notification form set out in Annex II:
(a) increases in the budget of an authorised aid scheme exceeding 20 %;
(b) prolongation of an existing authorised aid scheme by up to six years, with or without an increase in the budget;
(c) tightening of the criteria for the application of an authorised aid scheme, a reduction of aid intensity or a reduction of eligible expenses;
The Commission shall use its best endeavours to take a decision on any aid notified on the simplified notification form within a period of one month.
3.The simplified notification procedure shall not be used to notify alterations to aid schemes in respect of which Member States have not submitted annual reports in accordance with Article 5, 6, and 7, unless the annual reports for the years in which the aid has been granted are submitted at the same time as the notification.
CHAPTER III
ANNUAL REPORTS
Article 5
Form and content of annual reports
1.Without prejudice to the second and third subparagraphs of this Article and to any additional specific reporting requirements laid down in a conditional decision adopted pursuant to Article 7(4) of Regulation (EC) No 659/1999, or to the observance of any undertakings provided by the Member State concerned in connection with a decision to approve aid, Member States shall compile the annual reports on existing aid schemes referred to in Article 21(1) of Regulation (EC) No 659/1999 in respect of each whole or part calendar year during which the scheme applies in accordance with the standardised reporting format set out in Annex IIIA.
Annex IIIB sets out the format for annual reports on existing aid schemes relating to the production, processing and marketing of agricultural products listed in Annex I of the Treaty.
Annex IIIC sets out the format for annual reports on existing aid schemes for state aid relating to the production, processing or marketing of fisheries products listed in Annex I of the Treaty.
2.The Commission may ask Member States to provide additional data for selected topics, to be discussed in advance with Member States.
Article 6
Transmission and publication of annual reports
1.Each Member State shall transmit its annual reports to the Commission in electronic form no later than 30 June of the year following the year to which the report relates.
In justified cases Member States may submit estimates, provided that the actual figures are transmitted at the very latest with the following year's data.
2.Each year the Commission shall publish a State aid synopsis containing a synthesis of the information contained in the annual reports submitted during the previous year.
Article 7
Status of annual reports
The transmission of annual reports shall not be considered to constitute compliance with the obligation to notify aid measures before they are put into effect pursuant to Article 88(3) of the Treaty, nor shall such transmission in any way prejudice the outcome of an investigation into allegedly unlawful aid in accordance with the procedure laid down in Chapter III of Regulation (EC) No 659/1999.
CHAPTER IV
TIME-LIMITS
Article 8
Calculation of time-limits
1.Time-limits provided for in Regulation (EC) No 659/1999 and in this Regulation or fixed by the Commission pursuant to Article 88 of the Treaty shall be calculated in accordance with Regulation (EEC, Euratom) No 1182/71, and the specific rules set out in paragraphs 2 to 5 of this Article. In case of conflict, the provisions of this regulation shall prevail.
2.Time limits shall be specified in months or in working days.
3.With regard to time-limits for action by the Commission, the receipt of the notification or subsequent correspondence in accordance with Article 3(1) and Article 3(2) of this Regulation shall be the relevant event for the purpose of Article 3(1) of Regulation (EEC, Euratom) No 1182/71.
As far as notifications transmitted after 31 December 2005 , and correspondence relating to them are concerned, the receipt of the electronic notification or communication at the relevant address published in the Official Journal of the European Union shall be the relevant event.
4.With regard to time-limits for action by Member States, the receipt of the relevant notification or correspondence from the Commission in accordance with Art. 3(3) of this Regulation shall be the relevant event for the purposes of Article 3(1) of Regulation (EEC, Euratom) No 1182/71.
5.With regard to the time-limit for the submission of comments following initiation of the formal investigation procedure referred to in Art. 6(1) of Regulation (EC) No 659/1999 by third parties and those Member States which are not directly concerned by the procedure, the publication of the notice of initiation in the Official Journal of the European Union shall be the relevant event for the purposes of Article 3(1) of Regulation (EEC, Euratom) No 1182/71.
6.Any request for the extension of a time-limit shall be duly substantiated, and shall be submitted in writing to the address designated by the party fixing the time-limit at least two working days before expiry.
CHAPTER V
INTEREST RATE FOR THE RECOVERY OF UNLAWFUL AID
Article 9
Method for fixing the interest rate
1.Unless otherwise provided for in a specific decision the interest rate to be used for recovering State aid granted in breach of Article 88(3) of the Treaty shall be an annual percentage rate fixed for each calendar year.
It shall be calculated on the basis of the average of the five-year inter-bank swap rates for September, October and November of the previous year, plus 75 basis points. In duly justified cases, the Commission may increase the rate by more than 75 basis points in respect of one or more Member States.
2.If the latest three-month average of the five-year inter-bank swap rates available, plus 75 basis points, differs by more than 15 % from the State aid recovery interest rate in force, the Commission shall recalculate the latter.
The new rate shall apply from the first day of the month following the recalculation by the Commission. The Commission shall inform Member States by letter of the recalculation and the date from which it applies.
3.The interest rate shall be fixed for each Member State individually, or for two or more Member States together.
4.In the absence of reliable or equivalent data or in exceptional circumstances the Commission may, in close co-operation with the Member State(s) concerned, fix a State aid recovery interest rate, for one or more Member States, on the basis of a different method and on the basis of the information available to it.
Article 10
Publication
The Commission shall publish current and relevant historical State aid recovery interest rates in the Official Journal of the European Union and for information on the Internet.
Article 11
Method for applying interest
1.The interest rate to be applied shall be the rate applicable on the date on which unlawful aid was first put at the disposal of the beneficiary.
2.The interest rate shall be applied on a compound basis until the date of the recovery of the aid. The interest accruing in the previous year shall be subject to interest in each subsequent year.
3.The interest rate referred to in paragraph 1 shall be applied throughout the whole period until the date of recovery. However, if more than five years have elapsed between the date on which the unlawful aid was first put at the disposal of the beneficiary and the date of the recovery of the aid, the interest rate shall be recalculated at five yearly intervals, taking as a basis the rate in force at the time of recalculation.
CHAPTER VI
FINAL PROVISIONS
Article 12
Review
The Commission shall in consultation with the Member States, review the application of this Regulation within four years after its entry into force.
Article 13
Entry into force
This Regulation shall enter into force on the twentieth day following that of its publication in the Official Journal of the European Union .
Chapter II shall apply only to those notifications transmitted to the Commission more than five months after the entry into force of this Regulation.
Chapter III shall apply to annual reports covering aid granted from 1 January 2003 onwards.
Chapter IV shall apply to any time limit, which has been fixed but which has not yet expired on the date of entry into force of this Regulation.
Articles 9 and 11 shall apply in relation to any recovery decision notified after the date of entry into force of this Regulation.
This Regulation shall be binding in its entirety and be directly applicable in all Member States.
Done at Brussels, 21 april 2004 .
For the Commission
Mario Monti
Member of the Commission
(1) OJ L 83, 27.3.1999, p. 1. Regulation as amended by the 2003 Act of Accession.
(2) OJ L 124, 8.6.1971, p. 1.
(3) OJ L 300, 5.11.2002, p. 42.
ANNEX I
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
>REFERENCE TO A GRAPHIC>
ANNEX II
>REFERENCE TO A GRAPHIC>
ANNEX III A
STANDARDISED REPORTING FORMAT FOR EXISTING STATE AID
(This format covers all sectors except agriculture)
With a view to simplifying, streamlining and improving the overall reporting system for State aid, the existing Standardised Reporting Procedure shall be replaced by an annual updating exercise. The Commission shall send a pre-formatted spreadsheet, containing detailed information on all existing aid schemes and individual aid, to the Member States by 1 March each year. Member States shall return the spreadsheet in an electronic format to the Commission by 30 June of the year in question. This will enable the Commission to publish State aid data in year t for the reporting period t-1(1).
The bulk of the information in the pre-formatted spreadsheet shall be pre-completed by the Commission on the basis of data provided at the time of approval of the aid. Member States shall be required to check and, where necessary, modify the details for each scheme or individual aid, and to add the annual expenditure for the latest year (t-1). In addition, Member States shall indicate which schemes have expired or for which all payments have stopped and whether or not a scheme is co-financed by Community Funds.
Information such as the objective of the aid, the sector to which the aid is directed, etc shall refer to the time at which the aid is approved and not to the final beneficiaries of the aid. For example, the primary objective of a scheme which, at the time the aid is approved, is exclusively earmarked for small and medium-sized enterprises shall be aid for small and medium-sized enterprises. However, another scheme for which all aid is ultimately awarded to small and medium-sized enterprises shall not be regarded as such if, at the time the aid is approved, the scheme is open to all enterprises.
The following parameters shall be included in the spreadsheet. Parameters 1-3 and 6-12 shall be pre-completed by the Commission and checked by the Member States. Parameters 4, 5 and 13 shall be completed by the Member States.
1. Title
2. Aid number
3. All previous aid numbers (e.g., following the renewal of a scheme)
4. Expiry
Member States should indicate those schemes which have expired or for which all payments have stopped.
5. Co-financing
Although Community funding itself is excluded, total State aid for each Member State shall include aid measures that are co-financed by Community funding. In order to identify which schemes are co-financed and estimate how much such aid represents in relation to overall State aid, Member States are required to indicate whether or not the scheme is co-financed and if so the percentage of aid that is co-financed. If this is not possible, an estimate of the total amount of aid that is co-financed shall be provided.
6. Sector
The sectoral classification shall be based largely on NACE(2) at the [three-digit level].
7. Primary objective
8. Secondary objective
A secondary objective is one for which, in addition to the primary objective, the aid (or a distinct part of it) was exclusively earmarked at the time the aid was approved. For example, a scheme for which the primary objective is research and development may have as a secondary objective small and medium-sized enterprises (SMEs) if the aid is earmarked exclusively for SMEs. Another scheme for which the primary objective is SMEs may have as secondary objectives training and employment if, at the time the aid was approved, the aid is earmarked for x% training and y% employment.
9. Region(s)
Aid may, at the time of approval, be exclusively earmarked for a specific region or group of regions. Where appropriate, a distinction should be made between the Article 87(3)a regions and the Article 87(3)c regions. If the aid is earmarked for one particular region, this should be specified at NUTS(3) level II.
10. Category of aid instrument(s)
A distinction shall be made between six categories (Grant, Tax reduction/exemption, Equity participation, Soft loan, Tax deferral, Guarantee).
11. Description of aid instrument in national language
12. Type of aid
A distinction shall be made between three categories: Scheme, Individual application of a scheme, Individual aid awarded outside of a scheme (ad hoc aid).
13. Expenditure
As a general rule, figures should be expressed in terms of actual expenditure (or actual revenue foregone in the case of tax expenditure). Where payments are not available, commitments or budget appropriations shall be provided and flagged accordingly. Separate figures shall be provided for each aid instrument within a scheme or individual aid (e.g. grant, soft loans, etc.) Figures shall be expressed in the national currency in application at the time of the reporting period. Expenditure shall be provided for t-1, t-2, t-3, t-4, t-5.
(1) t is the year in which the data are requested.
(2) NACE Rev.1.1 is the Statistical classification of economic activities in the European Community.
(3) NUTS is the nomenclature of territorial units for statistical purposes in the Community.
ANNEX III B
STANDARDISED REPORTING FORMAT FOR EXISTING STATE AID
(This format covers the agricultural sector)
With a view to simplifying, streamlining and improving the overall reporting system for State aid, the existing Standardised Reporting Procedure shall be replaced by an annual updating exercise. The Commission shall send a pre-formatted spreadsheet, containing detailed information on all existing aid schemes and individual aid, to the Member States by 1 March each year. Member States shall return the spreadsheet in an electronic format to the Commission by 30 June of the year in question. This will enable the Commission to publish State aid data in year t for the reporting period t-1(1).
The bulk of the information in the pre-formatted spreadsheet shall be pre-completed by the Commission on the basis of data provided at the time of approval of the aid. Member States shall be required to check and, where necessary, modify the details for each scheme or individual aid, and to add the annual expenditure for the latest year (t-1). In addition, Member States shall indicate which schemes have expired or for which all payments have stopped and whether or not a scheme is co-financed by Community Funds.
Information such as the objective of the aid, the sector to which the aid is directed, etc shall refer to the time at which the aid is approved and not to the final beneficiaries of the aid. For example, the primary objective of a scheme which, at the time the aid is approved, is exclusively earmarked for small and medium-sized enterprises shall be aid for small and medium-sized enterprises. However, another scheme for which all aid is ultimately awarded to small and medium-sized enterprises shall not be regarded as such if, at the time the aid is approved, the scheme is open to all enterprises.
The following parameters shall be included in the spreadsheet. Parameters 1-3 and 6-12 shall be pre-completed by the Commission and checked by the Member States. Parameters 4, 5, 13 and 14 shall be completed by the Member States.
1. Title
2. Aid number
3. All previous aid numbers (e.g., following the renewal of a scheme).
4. Expiry
Member States should indicate those schemes which have expired or for which all payments have stopped
5. Co-financing
Although Community funding itself is excluded, total State aid for each Member State shall include aid measures that are co-financed by Community funding. In order to identify which schemes are co-financed and estimate how much such aid represents in relation to overall State aid, Member States are required to indicate whether or not the scheme is co-financed and if so the percentage of aid that is co-financed. If this is not possible, an estimate of the total amount of aid that is co-financed shall be provided.
6. Sector
The sectoral classification shall be based largely on NACE(2) at the [three-digit level].
7. Primary objective
8. Secondary objective
A secondary objective is one for which, in addition to the primary objective, the aid (or a distinct part of it) was exclusively earmarked at the time the aid was approved. For example, a scheme for which the primary objective is research and development may have as a secondary objective small and medium-sized enterprises (SMEs) if the aid is earmarked exclusively for SMEs. Another scheme for which the primary objective is SMEs may have as secondary objectives training and employment aid if, at the time the aid was approved the aid is earmarked for x% training and y% employment.
9. Region(s)
Aid may, at the time of approval, be exclusively earmarked for a specific region or group of regions. Where appropriate, a distinction should be made between Objective 1 regions and less-favoured areas.
10. Category of aid instrument(s)
A distinction shall be made between six categories (Grant, Tax reduction/exemption, Equity participation, Soft loan, Tax deferral, Guarantee).
11. Description of aid instrument in national language
12. Type of aid
A distinction shall be made between three categories: Scheme, Individual application of a scheme, Individual aid awarded outside of a scheme (ad hoc aid).
13. Expenditure
As a general rule, figures should be expressed in terms of actual expenditure (or actual revenue foregone in the case of tax expenditure). Where payments are not available, commitments or budget appropriations shall be provided and flagged accordingly. Separate figures shall be provided for each aid instrument within a scheme or individual aid (e.g. grant, soft loans, etc.) Figures shall be expressed in the national currency in application at the time of the reporting period. Expenditure shall be provided for t-1, t-2, t-3, t-4, t-5.
14. Aid intensity and beneficiaries
Member States should indicate:
- the effective aid intensity of the support actually granted per type of aid and of region
- the number of beneficiaries
- the average amount of aid per beneficiary.
(1) t is the year in which the data are requested
(2) NACE Rev.1.1 is the Statistical classification of economic activities in the European Community.
ANNEX III C
INFORMATION TO BE CONTAINED IN THE ANNUAL REPORT TO BE PROVIDED TO THE COMMISSION
The reports shall be provided in computerised form. They shall contain the following information:
1.Title of aid scheme, Commission aid number and reference of the Commission decision
2.Expenditure. The figures have to be expressed in euros or, if applicable, national currency. In the case of tax expenditure, annual tax losses have to be reported. If precise figures are not available, such losses may be estimated. For the year under review indicate separately for each aid instrument within the scheme (e.g. grant, soft loan, guarantee, etc.):
2.1.amounts committed, (estimated) tax losses or other revenue forgone, data on guarantees, etc. for new assisted projects. In the case of guarantee schemes, the total amount of new guarantees handed out should be provided;
2.2.actual payments, (estimated) tax losses or other revenue forgone, data on guarantees, etc. for new and current projects. In the case of guarantee schemes, the following should be provided: total amount of outstanding guarantees, premium income, recoveries, indemnities paid out, operating result of the scheme under the year under review;
2.3.number of assisted projects and/or enterprises;
2.4.estimated overall amount of:
- aid granted for the permanent withdrawal of fishing vessels through their transfer to third countries;
- aid granted for the temporary cessation of fishing activities;
- aid granted for the renewal of fishing vessels;
- aid granted for modernisation of fishing vessels;
- aid granted for the purchase of used vessels;
- aid granted for socio-economic measures;
- aid granted to make good damage caused by natural disasters or exceptional occurences;
- aid granted to outermost regions;
- aid granted through parafiscal charges;
2.5.regional breakdown of amounts under point 2.1. by regions defined as Objective 1 regions and other areas;
3.Other information and remarks.
