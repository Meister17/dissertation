*****
COUNCIL DIRECTIVE
of 21 March 1989
amending Directives 72/462/EEC and 77/99/EEC to take account of the introduction of public health and animal health rules which are to govern imports of meat products from third countries
(89/227/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 43 thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas the Council has made arrangements concerning intra-Community trade in meat products as regards health requirements by its Directive 77/99/EEC (4), as last amended by Directive 88/658/EEC (5);
Whereas the Council has made arrangements concerning intra-Community trade in meat products as regards animal health requirements by Directive 80/215/EEC (6), as last amended by Directive 88/660/EEC (7);
Whereas there is a need for Community arrangements applicable to imports of meat products from third countries;
Whereas, pending the entry into force of Community public health rules on trade in poultrymeat and game, poultrymeat and game products should be excluded from the scope of this Directive;
Whereas in this connection the public health and animal health conditions subject to which the Member States authorize importation of meat products should be laid down;
Whereas Directive 72/462/EEC (8), as last amended by Directive 88/289/EEC (9), laid down the relevant conditions applicable to imports of fresh meat from certain third countries or parts thereof; whereas the same criteria may be applied to imports of meat products;
Whereas, generally speaking, the public health and animal health rules applicable to imports from third countries must be at least equivalent to those laid down in Directives 77/99/EEC and 80/215/EEC;
Whereas it should be laid down that fresh meat intended for the manufacture of meat products must come from approved establishments; whereas such establishments must satisfy the conditions laid down in Directive 72/462/EEC;
Whereas it should be laid down that meat products must come from approved establishments; whereas those establishments must satisfy the conditions laid down in Directive 77/99/EEC;
Whereas, in order to check that the provisions of this Directive are observed by the exporting third country, it is necessary to apply to them the inspection rules laid down in Directive 72/462/EEC and in particular the system of inspection on the spot by Community veterinary experts and the inspections on arrival in the territory of the Community;
Whereas the production of an animal health certificate and a public health certificate, drawn up by an official veterinarian in the exporting third country is the most appropriate means of ensuring that a consignment of meat products may be admitted for importation,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Directive 72/462/EEC is amended as follows:
1. The title is replaced by the following:
'Council Directive of 12 December 1972 on health and veterinary inspection problems upon importation of bovine animals and swine and fresh meat or meat products from third countries'.
2. Articles 1 to 4 are replaced by the following:
'Article 1
1. This Directive shall apply to imports from third countries of:
- domestic bovine animals and swine for breeding, production or slaughter,
- fresh meat from domestic animals of the following species: bovine animals (including buffaloes), swine, sheep and goats and from domestic solipeds,
- for the purposes of Article 3, fresh meat of cloven-hoofed wild animals and wild solipeds, in so far as this concerns permitted importation from certain third countries of origin,
- meat products produced from fresh meat as defined in the second indent, with the exception of fresh meat referred to in Article 5 of Directive 64/433/EEC and in the corresponding provisions of Article 20 of Directive 72/462/EEC.
2. This Directive shall not apply to:
(a) animals intended exclusively for grazing or draught purposes, on a temporary basis, in the vicinity of the Community frontiers;
(b) meat and meat products other than those referred to in (e) forming part of travellers' personal luggage and intended for their personal consumption, in so far as the amount or quantity transported does not exceed one kilogram per person and provided that they come from a third country or part thereof appearing on the list drawn up in accordance with Article 3 and from which importation is not prohibited under Article 28;
(c) meat and meat products other than those referred to in (e) sent as small packages to private persons, provided that such meat and meat products are not imported by way of trade, that the quantity does not exceed one kilogram and that they come from a third country or part thereof appearing on the list drawn up in accordance with Article 3 and from which importation is not prohibited under Article 28;
(d) meat and meat products for consumption by the crew and passengers on board means of transport operating internationally.
Where such meat and meat products or their kitchen waste are unloaded, they must be destroyed. It is not, however, necessary to destroy meat or meat products when they are transferred, directly or after being placed provisionally under customs supervision, from one means of transport to another;
(e) where quantities of no more than one kilogram are involved, meat products having undergone heat treatment in a hermetically sealed container to a Fo value of 3,00 or more:
(i) forming part of travellers' personal luggage and intended for their personal consumption;
(ii) sent as small packages to private persons, provided that such meat products are not imported by way of trade.
Article 2
For the purposes of this Directive, the definitions given in Articles 2 of Directives 64/432/EEC, 64/433/EEC and in Council Directive 72/461/EEC of 12 December 1972 on health and veterinary inspection problems upon importation of bovine animals and swine and fresh meat from third countries (1), all these Directives, as last amended by Directive 87/489/EEC (2) and by Council Directive 77/99/EEC of 21 December 1976 on health problems affecting intra-Community trade in meat products (3), as last amended by Directive 88/658/EEC (4), shall apply as necessary.
However, the definitions of poultrymeat appearing in Article 1 of Directive 71/118/EEC shall not apply for the purposes of this Directive.
In addition,
(a) "official veterinarian" means the veterinarian designated by the competent central authority of a Member State or a third country;
(b) "country of destination" means the Member State to which animals, fresh meat or meat products are sent from a third country;
(c) "third country" means a country in which Directives 64/432/EEC, 64/433/EEC and 77/99/EEC do not apply; (d) "importation" means the introduction into the territory of the Community of animals, fresh meat or meat products from third countries;
(e) "holding" means an officially supervised agricultural, industrial or commercial undertaking situated in the territory of a third country, in which animals for breeding, production or slaughter are regularly kept or bred;
(f) "epizootic-free area" means an area in which, according to official findings, the animals have not suffered from any of the contagious or infectious diseases on the list drawn up in accordance with the procedure laid down in Article 29, for a period and within a radius defined in accordance with the same procedure.
(1) OJ No L 302, 31. 12. 1972, p. 24.
(2) OJ No L 280, 31. 10. 1987, p. 28.
(3) OJ No L 26, 31. 1. 1977, p. 85.
(4) OJ No L 382, 31. 12. 1988, p. 15.
Article 3
1. The Council, acting on a proposal from the Commission, shall draw up a list of the countries or parts thereof from which the Member States shall authorize importation:
- of domestic bovine animals and swine for breeding, production or slaughter,
- of fresh meat from domestic animals of the following species: bovine animals (including buffaloes), swine, sheep and goats, or domestic solipeds and meat products manufactured from or with the said meat,
- of fresh meat of cloven-hoofed wild animals and wild solipeds,
taking into account the health situation in those countries or parts thereof.
This list may be supplemented or amended according to the procedure laid down in Article 30 in particular as regards the drawing-up of the heading concerning meat products, with a reference, where appropriate, to the animal species and, in the case provided for under Article 21a (2), to the required treatment.
2. In deciding, in the case of both bovine animals and swine and fresh meat and meat products, whether a third country or part thereof may appear on the list referred to in paragraph 1, particular account shall be taken of:
(a) the state of health of the livestock, other domestic animals and wildlife in the third country, particular attention being paid to exotic animal diseases and of the environmental health situation in that country, which might endanger public and animal health in the Member States;
(b) the regularity and rapidity of the information supplied by the third country relating to the existence of infectious or contagious animal diseases in its territory, in particular those diseases mentioned in Lists A and B of the International Office of Epizootic Diseases;
(c) the country's rules on animal disease prevention and control;
(d) the structure of the veterinary services in the country and their powers;
(e) the organization and implementation of measures to prevent and control infectious or contagious animal diseases;
(f) that country's legislation on the use of substances, in particular legislation concerning the prohibition or authorization of substances, their distribution, release on to the market and their rules covering administration and inspection.
3. For the purpose of deciding, in the case of meat products, whether a country or part thereof can appear on the list referred to in paragraph 1, account shall be taken of, inter alia, the guarantees provided by the third country in regard to public health and animal health.
4. The list referred to in paragraph 1 and all amendments thereto shall be published in the Official Journal of the European Communities.
Article 4
1. In accordance with the procedure laid down in Article 29, one or more lists shall be drawn up of establishments from which Member States may authorize importation of fresh meat or meat products. In accordance with the detailed implementing rules to be established by the Commission under the procedure laid down in Article 30, the list or lists may be amended or supplemented by the Commission in line with the result of the inspections provided for in Article 5, of which it has previously informed the Member States.
In the event of difficulties, the matter shall be referred to the Committee in accordance with the procedure laid down in Article 29.
Before 1 January 1990, the Council shall review these provisions on the basis of a Commission report.
2. In deciding whether a slaughterhouse, a cutting plant or an establishment engaged in the production of meat products or a cold store situated outside a slaughterhouse or cutting plant or production plant may appear on one of the lists referred to in paragraph 1, particular account shall be taken of:
(a) the guarantees which the third country can offer with regard to compliance with this Directive;
(b) the third country's regulations with regard to the administration to animals for slaughter of any substance which might affect the wholesomeness of the meat and/or meat products; (c) as regards fresh meat, compliance in each particular case with this Directive and with Annex I to Directive 64/433/EEC.
However, derogations shall be permitted, in accordance with the procedure laid down in Article 29 of this Directive, from the second, third and fourth indents of paragraph 13 (c), and paragraphs 24 and 41 (c) of Annex I of Directive 64/433/EEC where the third country concerned provides similar guarantees; in that case, health conditions no less strict than those imposed in that Annex shall be imposed on a case-by-case basis in accordance with that procedure;
(d) as regards meat products, compliance in each particular case with the provisions of this Directive and with the relevant provisions of Annexes A and B to Directive 77/99/EEC;
(e) the organization of the meat inspection service or services of the third country, the powers of this service or these services and the supervision to which it or they is or are subject.
3. A slaughterhouse, a cutting plant, establishment engaged in the production of meat products or a cold store situated outside a slaughterhouse, cutting plant or production plant may not appear on the list or lists provided for in paragraph 1 unless it is situated in one of the third countries or parts thereof on the list referred to in Article 3 (1) and if it has, in addition, been officially approved for exports to the Community by the competent authorities of the third country. Such approval shall be subject to observance of the following requirements:
(a) compliance with the relevant provisions of Annex I to Directive 64/433/EEC or respectively Annexes A and B to Directive 77/99/EEC;
(b) constant supervision by an official veterinarian of the third country.
4. The list or lists referred to in paragraph 1 and all amendments thereto shall be published in the Official Journal of the European Communities.'
3. The following subparagraph is added to Article 19:
'The first subparagraph shall apply mutatis mutandis to meat products.'
4. The following Chapter IV is inserted after Article 21:
'CHAPTER IV
Imports of meat products
Article 21a
1. Without prejudice to paragraph 2, the meat products must have been prepared wholly or partly from fresh meat:
- satisfying the requirements of Articles 14 and 15, and any specific animal health conditions laid down pursuant to Article 16, or
- originating in a Member State, provided such fresh meat:
(i) satisfies the requirements of Articles 3 and 4 of Directive 80/215/EEC without prejudice to the requirements of Articles 7 and 10 of that Directive,
(ii) has been sent, under veterinary control, to the processing establishment either directly or following prior storage in an approved cold-storage plant,
(iii) has, before processing, undergone inspection by an official veterinarian to ensure that such fresh meat is still fit to undergo processing in accordance with Directive 77/99/EEC.
2. However, Member States may not refuse imports of meat products from a third country or a part thereof which appear under the ''Meat products" heading on the list drawn up in accordance with Article 3, but from which imports of fresh meat are not authorized or are no longer authorized, provided that the products in question meet the following requirements:
(i) they must come from an establishment which, having already satisfied the general conditions for approval, has been granted special approval for this type of product;
(ii) they must have been obtained from or with fresh meat as defined in paragraph 1 or from meat coming from the producing country which must:
- satisfy certain requirements of health policy to be drawn up in each individual case, on the basis of the health situation of the producing country under the procedure set out in Article 30,
- come from a slaughterhouse that is specifically approved for the delivery of meat to the establishment referred to under (i),
- bear a special mark to be specified under the procedure laid down in Article 29;
(iii) they must have undergone heat treatment in a hermetically sealed container to a Fo value of 3,00 or more.
However, under the procedure laid down in Article 29, other types of treatment may be allowed on the basis of the animal health situation prevailing in the exporting country.
(1) OJ No C 286, 25. 10. 1984, p. 5.
(2) OJ No C 175, 15. 7. 1985, p. 301.
(3) OJ No C 87, 9. 4. 1985, p. 6.
(4) OJ No L 26, 31. 1. 1977, p. 85.
(5) OJ No L 382, 31. 12. 1988, p. 15.
(6) OJ No L 47, 21. 2. 1980, p. 4.
(7) OJ No L 382, 31. 12. 1988, p. 35.
(8) OJ No L 302, 31. 12. 1972, p. 28.
(9) OJ No L 124, 18. 5. 1988, p. 31.
Article 21b
In addition to the requirements set out in Article 21a, meat products coming from third countries may be imported into the Community only if they satisfy the following requirements:
1. they must have been obtained in an establishment appearing under the ''Meat products" heading on the list drawn up pursuant to Article 4;
2. they must have come from an establishment meeting the relevant requirements of Annexes A and B to Directive 77/99/EEC;
3. they must have been obtained in conditions of hygiene satisfying the requirements of Chapter II and points 23 and 25 of Chapter III of Annex A to Directive 77/99/EEC;
4. they must have been obtained wholly from:
(a) fresh meat:
(i) from an establishment appearing on one of the lists drawn up pursuant to Directive 64/433/EEC or this Directive;
(iii) satisfying requirements of Articles 17 and 18 of this Directive, and, in addition, meeting the conditions laid down in points 23 and 25 of Chapter III of Annex A to Directive 77/99/EEC;
(b) where Article 21a (2) applies, from meat satisfying the specific requirements fixed for the producing country in question;
(c) meat products obtained in an establishment appearing either on the list drawn up pursuant to Article 4 or on one of the lists referred to in Article 7 of Directive 77/99/EEC;
5. they must meet the general requirements laid down by Directive 77/99/EEC, and in particular:
(a) they must have undergone one of the treatments defined in Article 2 (d) of Directive 77/99/EEC;
(b) they must have undergone inspection by an official veterinarian in accordance with Chapter IV of Annex A to Directive 77/99/EEC and, where hermetically sealed, inspection pursuant to requirements to be established in compliance with Chapter II of Annex B to Directive 77/99/EEC.
In carrying out such inspection, the official veterinarian may be aided by assistants reporting to him. Such assistants must:
(i) be appointed by the central competent authority of the exporting country in accordance with the provisions in force;
(ii) have received appropriate training;
(iii) have a legal status ensuring that they are independent of those in charge of the establishments;
(iv) have no power of decision concerning the final result of the inspection;
(c) in the event of wrapping and packaging, they must have been wrapped and packaged in accordance with Chapter V of Annex A to Directive 77/99/EEC;
(d) they must bear a public health stamp which meets the marking conditions laid down in Chapter VI of Annex A to Directive 77/99/EEC, except for the initials and sets of initials for Member States as specified in point 39 (a), which are to be replaced by the name of the third country of origin, accompanied by the veterinary authorization number of the establishment of origin;
(e) they must be stored and transported to the Community under satisfactory conditions of hygiene in accordance with Chapter VIII of Annex A to Directive 77/99/EEC and handled under satisfactory conditions of hygiene; in the case of meat products referred to in Article 4 of that Directive, the producer must, for the purposes of inspection, mark visibly and clearly on the packaging of the product the temperature at which the product must be transported and stored and the period for which it can be stored in that condition;
6. they must not have been subjected to ionizing radiation.'
5. Articles 22 to 26 are replaced by the following:
'CHAPTER V
Requirements applicable to both meat and meat products
Article 22
1. Member States shall not authorize fresh meat or meat products to be imported without presentation of an animal health certificate and a public health certificate drawn up by an official veterinarian of the exporting third country.
These certificates must:
(a) be drawn up in at least one of the official languages of the country of destination and one of those of the Member State in which the import inspections provided for in Articles 23 and 24 are carried out;
(b) accompany the fresh meat or meat products in the original;
(c) consist of a single sheet of paper;
(d) be made out for a single consignee. The animal health certificate must certify that the fresh meat or meat products comply with the animal health requirements laid down in this Directive and with those laid down pursuant to it with respect to the importation of fresh meat or meat products from the third country.
2. The health certificate must correspond to a model established in accordance with the procedure laid down in Article 29.
It may be decided in accordance with the same procedure and case by case that this animal health certificate and the public health certificate shall constitute a single sheet.
3. The public health certificate must correspond, in presentation and content for fresh meat to the specimen appearing in Annex A and for meat products to the specimen appearing in Annex C, and be issued on the day on which the fresh meat or meat products are loaded with a view to dispatch to the country of destination.
Article 23
1. The Member States shall ensure that, upon arrival in the geographical territory of the Community, fresh meat or meat products are subjected without delay to an animal health inspection carried out by the competent authority, whatever the procedure under which they were declared.
The detailed rules necessary to ensure that the inspection referred to in this paragraph is carried out in a uniform manner shall be adopted in accordance with the procedure laid down in Article 29.
2. Without prejudice to paragraph 3, the Member States shall ensure that importation is prohibited if this inspection reveals that:
- the meat or meat products do not come from the territory of a third country, or from a part thereof, included on the list drawn up in accordance with Article 3 (1),
- the meat or meat products come from the territory of a third country, or from a part thereof, from which imports are prohibited in accordance with Articles 14 and 28, but without prejudice to Article 21a (2),
- the animal health certificate which accompanies the meat or meat products does not comply with the conditions laid down pursuant to Article 22 (1) and (2).
3. The Member States shall authorize fresh meat or meat products from one third country to be transported to another provided that:
(a) the party concerned supplies proof that the first third country towards which the meat or meat products are being sent, after transit through Community territory, undertakes under no circumstances to reject or to send back to the Community the meat or meat products the importation or transit of which it has authorized;
(b) such transport has been previously authorized by the competent authorities of the Member State in the territory of which the animal health inspection provided for in paragraph 1 is carried out;
(c) such transport is carried out, without the goods being unloaded on Community territory, under the supervison of the competent authorities in vehicles or containers sealed by the competent authorities; the only handling authorized during this transport shall be that carried out respectively at the point of entry into or exit from Community territory for direct transhipment from a ship or aircraft to any other means of transport or vice versa.
4. All expenditure incurred pursuant to this Article shall be chargeable to the consignor, the consignee or their representative without compensation by the State.
Article 24
1. The Member States shall ensure that each consignment of fresh meat or meat products undergoes a public health inspection before being released for consumption on the geographical territory of the Community, and an animal health inspection, carried out by an official veterinarian.
The Member States shall ensure that importers are obliged to give at least two working days' notice to the local service responsible for the import inspection at the post where the freh meat or meat products are to be submitted for inspection, specifying the quantity and nature of the meat or the meat products and the time from which the inspection may be carried out.
2. The public health inspection provided for in paragraph 1 shall be carried out by random sampling in the case of imports covered by Articles 17 (1), 18 (1) and (2) and Articles 21a and 21b. The purpose of this inspection shall be in particular to verify, in accordance with paragraphe 3:
(a) the public health certificate, the compliance of the fresh meat or meat products with the stipulations on that certificate, the health marking; (b) the state of preservation, the presence of dirt or pathogenic agents;
(c) the presence of residues of substances referred to in Article 20;
(d) whether, with regard to fresh meat, slaughter and cutting or, with regard to meat products, the production have been carried out in establishments approved for that purpose;
(e) the conditions of transport.
3. There shall be adopted, in accordance with the procedure laid down in Article 29, the implementing rules necessary to ensure that the inspections referred to in paragraph 1 are carried out in a uniform way, particularly as regards the application of Article 20, and more particularly the methods of analysis and sampling intervals and standards.
4. The Member States shall prohibit the marketing of fresh meat or meat products if the inspections provided for in paragraph 1 have shown that:
- the fresh meat or meat products are not suitable for human consumption,
- the conditions laid down in this Directive and Annex I to Directive 64/433/EEC or Annexes A and B to Directive 77/99/EEC have not been fulfilled,
- one of the certificates referred to in Article 22 which accompany each consignment does not comply with the conditions laid down in the said Article.
5. If the fresh meat or meat products cannot be imported they must be returned, unless this is contrary to animal or public health considerations.
If it is impossible to return the meat or meat products, they must be destroyed in the territory of the Member State in which the inspections have taken place.
By way of derogation from this provision and if the importer or if his representative so requests, the Member State carrying out the animal health and public health inspections may authorize its entry for uses other than human consumption, provided that there is no danger for humans or for animals, that the meat or meat products are from a third country included on the list drawn up in accordance with Article 3 (1) and that importation is not prohibited under Article 28. Such meat or meat products may not leave the territory of that Member State, which must verify the final destination of the meat or meat products.
6. In all cases, after the inspections referred to in paragraph 1, the certificates must be endorsed so as to indicate clearly the use authorized for the meat or meat products.
Article 25
The fresh meat or meat products of each consignment authorized for circulation in the Community by a Member State on the basis of the inspections referred to in Article 24 (1) must, when forwarded to the country of destination, be accompanied by a certificate corresponding, in presentation and content, to the specimen given in Annex B.
This certificate must:
(a) be drawn up by the competent official veterinarian at the inspection post or at the place of storage;
(b) be issued on the day of loading for dispatch of the fresh meat or meat products to the country of destination;
(c) be drawn up in at least one of the official languages of the country of destination;
(d) accompany the consignment of fresh meat or meat products in the original.
Article 26
All expenditure incurred as a result of the application of Articles 24 and 25, and in particular the costs of inspection of the fresh meat or meat products, storage costs and the cost of destroying such meat or meat products shall be chargeable to the consignor, the consignee or their representative without compensation by the State.'
6. The present Chapter IV becomes Chapter VI.
7. In Article 27 (1) first subparagraph, point (b) is replaced by the following:
'(b) the inspection posts for importation of fresh meat or meat products.'
8. Article 28 (2) is replaced by the following:
'2. Without prejudice to Article 14 and Article 21a (2), if an infectious or contagious animal disease breaks out or spreads in a third country included on the list drawn up in accordance with Article 3 (1) and the disease can be carried by fresh meat or meat products and is likely to endanger public health or the health of the livestock in one of the Member States or if any other reason connected with animal health justifies it, the Member State concerned shall prohibit the importation of that meat or meat products whether imported directly or indirectly through another Member State, either from the whole of the third country or from part of its territory.' 9. Article 32a is replaced by the following:
'Article 32a
1. This Directive shall not apply to imports from third countries of fresh meat referred to in the third indent of Article 1 (1) or meat products thereof until the entry into force of the Commission Decision(s) adopted, in accordance with the procedure set out in Article 29, for the purpose of making the necessary adjustments to the list referred to in Article 3.
2. National laws on public health shall continue to apply to imports of fresh meat or meat products, referred to in paragraph 1, from third countries until the entry into force of Community rules on this subject.'
10. Annex B is replaced by the Annex appended hereto.
11. Annex C is replaced by the Annex appended hereto.
Article 2
Pending adoption of the harmonization of the animal health rules applicable to fresh poultrymeat and to game, imports of poultry products and game products shall continue to be covered by the existing national rules, subject to the general provisions of the Treaty.
Article 3
Article 17 (1) of Directive 77/99/EEC is replaced by the following:
'1. Pending the implementation of Community provisions concerning imports of poultrymeat products from third countries, Member States shall apply to such imports provisions which shall not be more favourable than those governing intra-Community trade.'
Article 4
Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive not later than 30 June 1990.
They shall forthwith inform the Commission thereof.
Article 5
This Directive is addressed to the Member States.
Done at Brussels, 21 March 1989.
For the Council
The President
C. ROMERO HERRERA
ANNEX
'ANNEX B
SPECIMEN
IMPORT INSPECTION CERTIFICATE FOR FRESH MEAT/MEAT PRODUCTS (1) IMPORTED FROM THIRD COUNTRIES
Member State in which the import inspection was carried out:
Inspection post:
Type of meat/meat products (1):
Packaging:
Number of carcases (2):
Number of half-carcases (2):
Number of quarters (2) or packages:
Net weight:
Third country of origin:
In the case of meat products:
Products imported pursuant to Article 14 / Article 21a (2) (1) of Directive 72/462/EEC:
I, the undersigned, official veterinarian, certify that the meat/meat products (1) described in the present certificate was/were inspected at the time of consignment forward.
1.2 // // // (Place and date) // (Official veterinarian)
(1) Delete as appropriate.
(2) Only for fresh meat.'
'ANNEX C
SPECIMEN
PUBLIC HEALTH CERTIFICATE
for meat products (1) intended for
(Name of EEC Member State)
No: (2)
Exporting country:
Ministry:
Department:
Ref:
(Optional)
I. Identification of meat products
Meat products of:
(Animal species)
Nature of pieces:
Nature of packaging:
Number of pieces or packages:
Required storage and transport temperature (3):
Storage period (3):
Net weight:
II. Origin of meat products
Address(es) and veterinary approval number(s) of the approved establishment(s):
III. Destination of meat products
The meat products will be sent from:
(Place of loading)
to:
(Country and place of destination)
by the following means of transport (4):
Nome and address of consignor:
Nome and address of consignee: IV. Health attestation
I, the undersigned, official veterinarian, certify that:
(a) - the meat products described above,
the label affixed to the packaged of meat products described above,
bear(s) a mark to the effect that the meat products come wholly from fresh meat from animals slaughtered in slaughterhouses approved for exporting to the country of destination or, where Article 21a (2) of Directive 72/462/EEC is applied, from animals slaughtered in a slaughterhouse specially for the delivery of meat for the treatment laid down in the said paragraph (5);
(b) the meat products have as such been passed as fit for human consumption following a veterinary inspection carried out in accordance with the requirements of Directive 72/462/EEC;
(c) the meat products have been obtained from pigmeat which has/has not been subject to an examination for trichinosis and in the latter case has undergone cold treatment (5);
(d) the means of transport and the loading conditions of meat products of this consignment meet the hygiene requirements laid down in respect of export to the country of destination;
(e) the meat products have been obtained from meat which satisfies the requirement of Chapter III of this Directive and those of Article 3 to Directive 77/99/EEC have been obtained under the derogation provided for in Article 21a (2) of Directive 72/462/EEC (5).
Done at , on
(Signature of the official veterinarian)
(1) Meat products within the meaning of Directive 77/99/EEC.
(2) Optional.
(3) To be completed where indicated in accordance with Article 4 of Directive 77/99/EEC.
(4) For railway wagons or goods vehicles the registration number should be given, for aircraft the flight number and for ships the name.
(5) Delete as appropriate.'
