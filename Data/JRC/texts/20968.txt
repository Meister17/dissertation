Commission Decision
of 25 November 2002
amending Decision 2001/556/EC, with respect to Canada for gelatine intended for human consumption
(notified under document number C(2002) 4540)
(Text with EEA relevance)
(2002/926/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Decision 95/408/EC of 22 June 1995 on the conditions for drawing up, for an interim period, provisional lists of third country establishments from which Member States are authorised to import certain products of animal origin, fishery products or live bivalve molluscs(1), as last amended by Decision 2001/4/EC(2), and in particular Article 2(4) thereof,
Whereas:
(1) Provisional lists of establishments in third countries producing gelatine intended for human consumption have been drawn up by Commission Decision 2001/556/EC of 11 July 2001 drawing up provisional lists of third country establishments from which Member States authorise imports of gelatine intended for human consumption(3).
(2) Canada has provided the name of an establishment producing gelatine intended for human consumption for which the competent authorities certify that the establishments are in accordance with the Community rules.
(3) Provisional listing of this establishment can thus be drawn up for Canada. Decision 2001/556/EC should therefore be amended accordingly.
(4) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
In the Annex to Decision 2001/556/EC, the following row is added:
"País: CANADÁ/Land: CANADA/Land: KANADA/Χώρα: ΚΑΝΑΔΑΣ/Country: CANADA/Pays: CANADA/Paese: CANADA/Land: CANADA/País: CANADÁ/Maa: KANADA/Land: KANADA"
Article 2
This Decision shall apply as from the third day following its publication in the Official Journal of the European Communities.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 25 November 2002.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 243, 11.10.1995, p. 17.
(2) OJ L 2, 5.1.2001, p. 21.
(3) OJ L 200, 25.7.2001, p. 23.
