Commission Regulation (EC) No 1823/2006
of 12 December 2006
amending for the 73rd time Council Regulation (EC) No 881/2002 imposing certain specific restrictive measures directed against certain persons and entities associated with Usama bin Laden, the Al-Qaida network and the Taliban, and repealing Council Regulation (EC) No 467/2001
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 881/2002 imposing certain specific restrictive measures directed against certain persons and entities associated with Usama bin Laden, the Al-Qaida network and the Taliban, and repealing Council Regulation (EC) No 467/2001 prohibiting the export of certain goods and services to Afghanistan, strengthening the flight ban and extending the freeze of funds and other financial resources in respect of the Taliban of Afghanistan [1], and in particular Article 7(1), first indent, thereof,
Whereas:
(1) Annex I to Regulation (EC) No 881/2002 lists the persons, groups and entities covered by the freezing of funds and economic resources under that Regulation.
(2) On 5 and 7 December 2006, the Sanctions Committee of the United Nations Security Council decided to amend the list of persons, groups and entities to whom the freezing of funds and economic resources should apply. Annex I should therefore be amended accordingly.
(3) In order to ensure that the measures provided for in this Regulation are effective, this Regulation must enter into force immediately,
HAS ADOPTED THIS REGULATION:
Article 1
Annex I to Regulation (EC) No 881/2002 is hereby amended as set out in the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 12 December 2006.
For the Commission
Eneko Landáburu
Director-General for External Relations
[1] OJ L 139, 29.5.2002, p. 9. Regulation as last amended by Commission Regulation (EC) No 1685/2006 (OJ L 314, 15.11.2006, p. 24).
--------------------------------------------------
ANNEX
Annex I to Regulation (EC) No 881/2002 is amended as follows:
1. The following entries shall be added under the heading "Natural persons":
(a) "Najmuddin Faraj Ahmad (alias (a) Mullah Krekar, (b) Fateh Najm Eddine Farraj, (c) Faraj Ahmad Najmuddin). Address: Heimdalsgate 36-V, 0578 Oslo, Norway. Date of birth: (a) 7.7.1956, (b) 17.6.1963. Place of birth: Olaqloo Sharbajer, Al-Sulaymaniyah Governorate, Iraq. Nationality: Iraqi."
(b) "Mohamed Moumou (alias (a) Mohamed Mumu, (b) Abu Shrayda, (c) Abu Amina, (d) Abu Abdallah, (e) Abou Abderrahman). Address: (a) Storvretsvagen 92, 7 TR. C/O Drioua, 142 31 Skogas, Sweden, (b) Jungfruns Gata 413; Postal address Box 3027, 13603 Haninge, Sweden, (c) Dobelnsgatan 97, 7 TR C/O Lamrabet, 113 52 Stockholm, Sweden, (d) Trodheimsgatan 6, 164 32 Kista, Sweden. Date of birth: (a) 30.7.1965, (b) 30.9.1965. Place of birth: Fez, Morocco. Nationality: (a) Moroccan, (b) Swedish. Passport No: 9817619 (Swedish passport, expires on 14.12.2009)."
2. The entry "Ghuma Abd’rabbah (alias (a) Ghunia Abdurabba, (b) Ghoma Abdrabba, (c) Abd’rabbah, (d) Abu Jamil). Address: Birmingham, United Kingdom. Date of birth: 2.9.1957. Place of birth: Benghazi, Libya. Nationality: British." under the heading "Natural persons" shall be replaced by:
"Ghuma Abd’rabbah (alias (a) Ghunia Abdurabba, (b) Ghoma Abdrabba, (c) Abd’rabbah, (d) Abu Jamil, (e) Ghunia Abdrabba). Address: Birmingham, United Kingdom. Date of birth: 2.9.1957. Place of birth: Benghazi, Libya. Nationality: British."
--------------------------------------------------
