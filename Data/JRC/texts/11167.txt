Commission Regulation (EC) No 89/2006
of 19 January 2006
amending Regulation (EC) No 2295/2003 with regard to the terms that may be used when marketing eggs where the access of hens to open-air runs is restricted
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 1907/90 of 26 June 1990 on certain marketing standards for eggs [1], and in particular Article 7(1)(d) and Article 10(3) thereof,
Whereas:
(1) Commission Regulation (EC) No 2295/2003 [2] lays down detailed rules for implementing Regulation (EEC) No 1907/90.
(2) Commission Directive 2002/4/EC of 30 January 2002 on the registration of establishments keeping laying hens, covered by Council Directive 1999/74/EC [3] lays down minimum standards for the protection of laying hens.
(3) In order to safeguard consumers against statements which might otherwise be made with the fraudulent intent of obtaining prices higher than those prevailing for eggs of hens raised in batteries or standard grade eggs, Regulation (EC) No 2295/2003 lays down minimum husbandry criteria to be respected by farmers claiming to use particular rearing methods. Only the terms listed in Annex II to that Regulation may be used and Annex III thereto lists the requirements that must be satisfied in order to be able to use those terms.
(4) Among the specific requirements that must be satisfied in order to market eggs using the term "free-range eggs", access to open-air runs is essential.
(5) Restrictions, including veterinary restrictions, adopted under Community law to protect public and animal health, may restrict the access of poultry to open-air runs.
(6) Where producers can no longer satisfy all the rearing conditions laid down in Annex III to Regulation (EC) No 2295/2003, they must, in the interests of consumers, cease to use the compulsory labelling relating to the rearing method.
(7) In order to take account of the possible economic consequences of such temporary restrictions, since the whole of the sector needs a reasonable adjustment period, especially as regards labelling, and provided that product quality is not substantially affected, provision should be made for a transitional period during which producers can continue to use the labelling relating to the "free-range" method of rearing.
(8) A derogation from access to open-air runs is explicitly provided for in the first indent of point 1(a) of Annex III to Regulation (EC) No 2295/2003 in the event of "temporary restrictions imposed by veterinary authorities".
(9) Since the duration of temporary restrictions during which producers may continue to use the term "free-range", although hens no longer have access to open-air runs, is not specified, a time-limit should be laid down in order to protect the interests of consumers.
(10) Regulation (EC) No 2295/2003 should therefore be amended accordingly.
(11) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Poultrymeat and Eggs,
HAS ADOPTED THIS REGULATION:
Article 1
The first indent of point 1(a) of Annex III to Regulation (EC) No 2295/2003 is hereby replaced by the following:
- "— hens have continuous daytime access to open-air runs; any derogation from this requirement granted because of restrictions, including veterinary restrictions adopted under Community law, must not exceed 12 weeks."
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 19 January 2006.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 173, 6.7.1990, p. 5. Regulation as last amended by Regulation (EC) No 1039/2005 (OJ L 172, 5.7.2005, p. 1).
[2] OJ L 340, 24.12.2003, p. 16. Regulation as last amended by Regulation (EC) No 1515/2004 (OJ L 278, 27.8.2004, p. 7).
[3] OJ L 30, 31.1.2002, p. 44. Directive as amended by the 2003 Act of Accession.
--------------------------------------------------
