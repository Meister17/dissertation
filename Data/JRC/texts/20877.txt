Council Framework Decision
of 13 June 2002
on joint investigation teams
(2002/465/JHA)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty on the European Union, and in particular Article 34(2)(b) thereof,
Having regard to the initiative of the Kingdom of Belgium, the French Republic, the Kingdom of Spain and the United Kingdom(1),
Having regard to the opinion of the European Parliament(2),
Whereas:
(1) One of the Union's objectives is to provide citizens with a high level of safety within an area of freedom, security and justice and this objective is to be achieved by preventing and combating crime through closer cooperation between police forces, customs authorities and other competent authorities in the Member States, while respecting the principles of human rights and fundamental freedoms and the rule of law on which the Union is founded and which are common to the Member States.
(2) The European Council held in Tampere on 15 and 16 October 1999 called for joint investigation teams as foreseen in the Treaty to be set up without delay, as a first step, to combat trafficking in drugs and human beings as well as terrorism.
(3) Provision has been made in Article 13 of the Convention established by the Council in accordance with Article 34 of the Treaty on European Union on Mutual Assistance in Criminal Matters between the Member States of the European Union(3) for the establishment and operation of joint investigation teams.
(4) The Council urges that all measures be taken to ensure that this Convention is ratified as soon as possible, and in any event in the course of 2002.
(5) The Council recognises that it is important to respond quickly to the European Council's call for the setting up of joint investigative teams without delay.
(6) The Council considers that for the purpose of combating international crime as effectively as possible, it is appropriate that at this stage a specific legally binding instrument on joint investigation teams should be adopted at the level of the Union which should apply to joint investigations into trafficking in drugs and human beings as well as terrorism.
(7) The Council considers that such teams should be set up, as a matter of priority, to combat offences committed by terrorists.
(8) The Member States that set up a team should decide on its composition, purpose and duration.
(9) The Member States setting up a team should have the possibility to decide, where possible and in accordance with applicable law, to let persons not representing the competent authorities of Member States take part in the activities of the team, and that such persons may include representatives of, for example, Europol, the Commission (OLAF) or representatives of authorities of non Member States, and in particular representatives of law enforcement authorities of the United States. In such cases the agreement setting up the team should specify issues relating to possible liability for such representatives.
(10) A joint investigating team should operate in the territory of a Member State in conformity with the law applicable to that Member State.
(11) This Framework Decision should be without prejudice to any other existing provisions or arrangements on the setting up or operation of joint investigation teams,
HAS ADOPTED THIS FRAMEWORK DECISION:
Article 1
Joint investigation teams
1. By mutual agreement, the competent authorities of two or more Member States may set up a joint investigation team for a specific purpose and a limited period, which may be extended by mutual consent, to carry out criminal investigations in one or more of the Member States setting up the team. The composition of the team shall be set out in the agreement.
A joint investigation team may, in particular, be set up where:
(a) a Member State's investigations into criminal offences require difficult and demanding investigations having links with other Member States;
(b) a number of Member States are conducting investigations into criminal offences in which the circumstances of the case necessitate coordinated, concerted action in the Member States involved.
A request for the setting up of a joint investigation team may be made by any of the Member States concerned. The team shall be set up in one of the Member States in which the investigations are expected to be carried out.
2. In addition to the information referred to in the relevant provisions of Article 14 of the European Convention on Mutual Assistance in Criminal Matters and Article 37 of the Benelux Treaty of 27 June 1962, as amended by the Protocol of 11 May 1974, requests for the setting up of a joint investigation team shall include proposals for the composition of the team.
3. A joint investigation team shall operate in the territory of the Member States setting up the team under the following general conditions:
(a) The leader of the team shall be a representative of the competent authority participating in criminal investigations from the Member State in which the team operates. The leader of the team shall act within the limits of his or her competence under national law.
(b) The team shall carry out its operations in accordance with the law of the Member State in which it operates. The members of the team shall carry out their tasks under the leadership of the person referred to in subparagraph (a), taking into account the conditions set by their own authorities in the agreement on setting up the team.
(c) The Member State in which the team operates shall make the necessary organisational arrangements for it to do so.
4. In this Framework Decision, members of the joint investigation team from Member States other than the Member State in which the team operates are referred to as being "seconded" to the team.
5. Seconded members of the joint investigation team shall be entitled to be present when investigative measures are taken in the Member State of operation. However, the leader of the team may, for particular reasons, in accordance with the law of the Member State where the team operates, decide otherwise.
6. Seconded members of the joint investigation team may, in accordance with the law of the Member State where the team operates, be entrusted by the leader of the team with the task of taking certain investigative measures where this has been approved by the competent authorities of the Member State of operation and the seconding Member State.
7. Where the joint investigation team needs investigative measures to be taken in one of the Member States setting up the team, members seconded to the team by that Member State may request their own competent authorities to take those measures. Those measures shall be considered in that Member State under the conditions which would apply if they were requested in a national investigation.
8. Where the joint investigation team needs assistance from a Member State other than those which have set up the team, or from a third State, the request for assistance may be made by the competent authorities of the State of operations to the competent authorities of the other State concerned in accordance with the relevant instruments or arrangements.
9. A member of the joint investigation team may, in accordance with his or her national law and within the limits of his or her competence, provide the team with information available in the Member State which has seconded him or her for the purpose of the criminal investigations conducted by the team.
10. Information lawfully obtained by a member or seconded member while part of a joint investigation team which is not otherwise available to the competent authorities of the Member States concerned may be used for the following purposes:
(a) for the purposes for which the team has been set up;
(b) subject to the prior consent of the Member State where the information became available, for detecting, investigating and prosecuting other criminal offences. Such consent may be withheld only in cases where such use would endanger criminal investigations in the Member State concerned or in respect of which that Member State could refuse mutual assistance;
(c) for preventing an immediate and serious threat to public security, and without prejudice to subparagraph (b) if subsequently a criminal investigation is opened;
(d) for other purposes to the extent that this is agreed between Member States setting up the team.
11. This Framework Decision shall be without prejudice to any other existing provisions or arrangements on the setting up or operation of joint investigation teams.
12. To the extent that the laws of the Member States concerned or the provisions of any legal instrument applicable between them permit, arrangements may be agreed for persons other than representatives of the competent authorities of the Member States setting up the joint investigation team to take part in the activities of the team. Such persons may, for example, include officials of bodies set up pursuant to the Treaty. The rights conferred upon the members or seconded members of the team by virtue of this Framework Decision shall not apply to these persons unless the agreement expressly states otherwise.
Article 2
Criminal liability regarding officials
During the operations referred to in Article 1, officials from a Member State other than the Member State of operation shall be regarded as officials of the Member State of operation with respect to offences committed against them or by them.
Article 3
Civil liability regarding officials
1. Where, in accordance with Article 1, officials of a Member State are operating in another Member State, the first Member State shall be liable for any damage caused by them during their operations, in accordance with the law of the Member State in whose territory they are operating.
2. The Member State in whose territory the damage referred to in paragraph 1 was caused shall make good such damage under the conditions applicable to damage caused by its own officials.
3. The Member State whose officials have caused damage to any person in the territory of another Member State shall reimburse the latter in full any sums it has paid to the victims or persons entitled on their behalf.
4. Without prejudice to the exercise of its rights vis-à-vis third parties and with the exception of paragraph 3, each Member State shall refrain, in the case provided for in paragraph 1, from requesting reimbursement of damages it has sustained from another Member State.
Article 4
Implementation
1. Member States shall take the necessary measures to comply with the provisions of this Framework Decision by 1 January 2003.
2. Member States shall transmit to the General Secretariat of the Council and the Commission the text of any provisions transposing into their national law the obligations imposed on them under this Framework Decision. On the basis of this and other information, the Commission shall, by 1 July 2004, submit a report to the Council on the operation of this Framework Decision. The Council shall assess the extent to which the Member States have complied with this Framework Decision.
Article 5
Entry into force
This Framework Decision shall enter into force on the date of its publication in the Official Journal. It shall cease to have effect when the Convention on Mutual Assistance in Criminal Matters between Member States of the European Union has entered into force in all Member States.
Done at Luxembourg, 13 June 2002.
For the Council
The President
M. Rajoy Brey
(1) OJ C 295, 20.10.2001, p. 9.
(2) Opinion delivered on 13 November 2001 (not yet published in the Official Journal).
(3) OJ C 197, 12.7.2000, p. 1.
