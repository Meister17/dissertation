COMMISSION REGULATION (EC) No 590/1999 of 18 March 1999 supplementing the Annex to Regulation (EC) No 1107/96 on the registration of geographical indications and designations of origin under the procedure laid down in Article 17 of Regulation (EEC) No 2081/92
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2081/92 of 14 July 1992 on the protection of geographical indications and designations of origin for agricultural products and foodstuffs (1), as last amended by Commission Regulation (EC) No 1068/97 (2), and in particular Article 17(2) thereof,
Whereas, for certain names notified by the Member States under Article 17 of Regulation (EEC) No 2081/92, additional information was requested in order to ensure that they complied with Articles 2 and 4 of that Regulation; whereas that additional information shows that the names comply with the said Articles; whereas they should therefore be registered and added to the Annex to Commission Regulation (EC) No 1107/96 (3), as last amended by Regulation (EC) No 83/1999 (4);
Whereas, following the accession of three new Member States, the six-month period provided for in Article 17 of Regulation (EEC) No 2081/92 is to begin on the date of their accession; whereas some of the names notified by those Member States comply with Articles 2 and 4 of that Regulation and should therefore be registered;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Regulatory Committee on geographical indications and designations of origin,
HAS ADOPTED THIS REGULATION:
Article 1
The names in the Annex to this Regulation are hereby added to the Annex to Regulation (EC) No 1107/96.
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 18 March 1999.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ L 208, 24. 7. 1992, p. 1.
(2) OJ L 156, 13. 6. 1997, p. 10.
(3) OJ L 148, 21. 6. 1996, p. 1.
(4) OJ L 8, 14. 1. 1999, p. 17.
ANNEX
A. PRODUCTS INTENDED FOR HUMAN CONSUMPTION LISTED IN ANNEX II TO THE TREATY
Fresh meat (and offal)
ITALY
- Cotechino Modena (PGI)
- Zampone Modena (PGI)
Fruits, vegetables, cereals
GERMANY
- Spreewälder Gurken (PGI)
- Spreewälder Meerrettich (PGI)
