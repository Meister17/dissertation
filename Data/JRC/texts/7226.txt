Authorisation for State aid pursuant to Articles 87 and 88 of the EC Treaty
Cases where the Commission raises no objections
(2005/C 240/04)
(Text with EEA relevance)
Date of adoption of the decision : 20.4.2005
Member State : France
Aid No : E10/2005
Title : France licence fee
Objective : Financing of the French public service broadcasting France Télévision
Legal basis : Loi no 49-1032 du 30 juillet 1949
Budget : the amount varies on an annual basis
Duration : The above statuses constitute the basis for the ongoing financing of the public service broadcasting entrusted to France Télévision
Other information : Commission decision declaring that the aids granted to France Télévision constitute existing State aids within the meaning of Article 1(b) Procedural Regulation (EC) No 659/1999. Following the Commission request, the French authorities committed to modify some aspects of French public service broadcasting financing scheme so as to ensure its current compatibility with the common market.
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 30.3.2005
Member State : Luxembourg
Aid No : N 205 C/2004 and N 205 D/2004
Title : Framework law on aid to small and medium-sized enterprises
Objective : Environmental protection; promotion of R&D activities
Legal basis : loi portant création d'"un cadre général des régimes d'aides en faveur du secteur des classes moyennes"
Budget : EUR 1 million (environment) and EUR 500000 (R&D)
Aid intensity or amount - up to 40 % excluding bonus for environmental aid
- 100 % + 15 % for aid to decontaminate polluted sites
- 25 %, 50 % and 75 % for research aid according to the stage of research, excluding bonus
Duration : 6 years
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 20.4.2004
Member State : United Kingdom
Aid No : N 206/2003
Title : Waterborne Freight Grant (WFG)
Objective : To encourage modal shift of freight from road to water by granting aid to new/existing coastal, short sea or inland waterway services provided that they avoid lorry journeys and that they generate environmental benefits within the UK
Legal basis : Transport Act 2000, section 272; Transport (Scotland) Act 2001, section 71
Budget : A total budget of GBP 60 million for the Freight Facilities Grant Scheme (FFG) and for the Waterborne Freight Grant (WFG) will be provided.
Duration : 6 years
Other information : The scheme is complementary to the existing Freight Facilities Grant (FFG) scheme [1]
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 20.4.2004
Member State : France
Aid No : N 312/2003
Title : Reform of the method of financing Industrial Technical Centres (Centres Techniques Industriels — CTI) and Professional Centres for Technical Development (Centres Professionnels de Développement Technique — CPDE)
Objective : To promote mechanical engineering products, consumer goods and building materials (Consumer goods, mechanical engineering products, building materials)
Legal basis : loi 2001-692 du 1er août 2001 et loi de finance annuelle
Duration : From the date of Commission approval until 31.12.2004
Other information : The beneficiaries are CTIs and CPDEs in the consumer goods, mechanical engineering and building materials sectors
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 3.3.2005
Member State : Germany (Land Saxony)
Aid No : N 548/04
Title : Aid to the Development of new or novel products and methods in the Free State of Saxony. — Prolongation of the Scheme NN 32/98
Objective : R&D
Legal basis :
Förderrichtlinie des Sächsischen Staatsministeriums für Wirtschaft und Arbeit über die Gewährung von Zuwendungen für Projekte zur Entwicklung neuer oder neuartiger Produkte und Verfahren im Freistaat Sachsen (Einzelbetriebliche Projektförderung) vom 7. Februar 2001;
Vorläufige Sächsische Haushaltsordnung (SäHo) vom 19. Dezember 1990;
Vorläufige Verwaltungsvorschrift zu §§ 23, 44 SäHo
Budget : EUR 225 million
Intensity or amount : 50 % for industrial research; 25 % for pre-competitive development; plus 10 % SME-bonus (where applicable); plus 10 % bonus for effective cooperation between public R&D institutes and firms
Duration : 1.1.2005 — 31.12.2009
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 21.6.2005
Member States : Italy
Aid No : N 582/04
Title : Aid to biofuels/tax benefit
Objective : Environmental protection
Legal basis : Art. 21 par. 6 del Decreto Legislativo 25 ottobre 1995 n. 504; legge del 30.12.2004 n. 311 (finanziaria 2005)
Aid intensity or amount : EUR 561267000
Duration : Until 30.6.2010
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 14.7.2004
Member State : United Kingdom (Wales)
Aid No : N 597/2003
Title : Wood Energy Business Scheme
Objective : The objective of the scheme is to encourage the use of wood as a renewable source of energy in Wales. It funds projects that help the emergence of a wood fuel market in Wales.
Legal basis : 1967 Forestry Act
Budget : GBP 6 million (EUR 9 million) for the whole duration of the scheme.
Aid intensity or amount : Up to 40 % of eligible costs, with a bonus of 5 % in regions covered by Article 87(3)(c) and 10 % in regions covered by Article 87(3)(a). A further 10 % may be added for SMEs.
Duration : 3 years (until September 2007)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 3.5.2005
Member State : Belgium (Flemish region)
Aid No : N 608/2004
Title : Combined Heat and Power certificates
Objective : Environmental protection (Energy)
Legal basis : Besluit van de Vlaamse regering van 5 maart 2004 houdende de openbaredienstverplichting ter bevordering van de elektriciteitsopwekking in kwalitatieve warmtekrachtinstallaties
Intensity or amount : The measure does not constitute aid.
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
[1] N 649/2001 — UK — Freight Facilities Grant; Commission decision of 20.12.2001.
--------------------------------------------------
