Commission Regulation (EC) No 394/2005
of 8 March 2005
amending Regulation (EC) No 795/2004 laying down detailed rules for the implementation of the single payment scheme provided for in Council Regulation (EC) No 1782/2003 establishing common rules for direct support schemes under the common agricultural policy and establishing certain support schemes for farmers, and derogating from Regulation (EC) No 1782/2003
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1782/2003 of 29 September 2003 establishing common rules for direct support schemes under the common agricultural policy and establishing certain support schemes for farmers and amending Regulations (EEC) No 2019/93, (EC) No 1452/2001, (EC) No 1453/2001, (EC) No 1454/2001, (EC) No 1868/94, (EC) No 1251/1999, (EC) No 1254/1999, (EC) No 1673/2000, (EEC) No 2358/71 and (EC) No 2529/2001 [1], and in particular Article 145(c) and (d) and (q) thereof,
Whereas:
(1) Commission Regulation (EC) No 795/2004 [2] introduces the implementing rules for the single payment scheme that will apply from 2005. The administrative and operational implementation of the scheme which has started, on that basis, at national level, has shown the need of further detailed rules on some aspects of the scheme and to clarify and adapt certain aspects of the existing rules.
(2) In particular, it is appropriate to specify the application of the definition of multiannual crops in relation to the eligibility conditions for land to be put on set-aside and in relation to the energy crops aid scheme referred to in Article 88 of Regulation (EC) No 1782/2003.
(3) For administrative reasons, in order to limit the creation of fractions of payment entitlements to the extent necessary, Article 3(4) of Regulation (EC) No 795/2004 provides that in case of transfer, before splitting an existing entitlement, all existing fractions should be used. It is appropriate to specify that that provision should refer to existing fractions of entitlements of the same nature, such as normal entitlements, set-aside entitlements, entitlements accompanied by an authorisation in accordance with Article 60 of Regulation (EC) No 1782/2003.
(4) According to Article 7(6) of Regulation (EC) No 795/2004, farmers who leased or sold hectares shall not benefit from the mechanism provided for in that Article. As far as the aim of that mechanism is not undermined by the fact of buying and selling or leasing an equivalent number of hectares, it should be provided that the mechanism should also apply in those cases.
(5) Article 10 of Regulation (EC) No 795/2004 provides for the reversion to the national reserve of a part of the reference amount in particular situations. For administrative reasons it is appropriate to allow Member States to apply such reduction above a ceiling to be defined.
(6) Article 21(4) of Regulation (EC) No 795/2004 assimilates lease to purchase of land for investment purpose. Investments in production capacity made under the form of a lease should also be taken into account.
(7) Article 22(2) of Regulation (EC) No 795/2004 aims to take into account the cases of farmers who find themselves in special situations deriving from the fact of having bought land that was under a lease during the reference period. It is appropriate to specify the scope of application of the provision by giving a definition of the condition of the lease to be taken into account.
(8) The second subparagraph of Article 46(2) of Regulation (EC) No 1782/2003 provides that, except in case of force majeure or exceptional circumstances, a farmer may transfer his payment entitlements without land only after he has used, within the meaning of Article 44 of that Regulation, at least 80 % of his payment entitlements during at least one calendar year or, after he has given up voluntarily to the national reserve all the payment entitlements he has not used in the first year of application of the single payment scheme. It is appropriate to specify the conditions of application of that provision.
(9) Regulation (EC) No 795/2004 should therefore be amended accordingly.
(10) From 1 January 2006, Article 51 of Regulation (EC) No 1782/2003, as amended by Council Regulation (EC) No 864/2004 [3], authorises Member States to allow secondary crops to be cultivated on the eligible hectares during a period of maximum three months starting each year on 15 August. Taking into account that until 31 December 2004 such practices are compatible with the existing rules on direct payments and that farmers could again be authorised under that new provision from 1 January 2006, the fact of interrupting such practices for one year would result in severe economical problems for farmers used to such practices and would also provoke practical and specific problems in terms of eligibility of the land. Therefore, in order to guarantee the continuity of the measure and to allow farmers, in Member States where such decision would be taken, to take their sowing decisions in time, it is necessary and duly justified to provide, by way of derogation from Regulation (EC) No 1782/2003, for the application in 2005 of that option.
(11) Due to the fact that the single payment scheme is applicable starting from 1 January 2005, it is appropriate that this Regulation applies retroactively from that date.
(12) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Direct Payments,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 795/2004 is amended as follows:
1. in Article 2, points (c) and (d) are replaced by the following:
"c) "permanent crops" shall mean non-rotational crops other than permanent pasture that occupy the land for five years or longer and yield repeated harvests including nurseries as defined in point (G/5) of Annex I to Commission Decision 2000/115/EC [4], with the exception of multiannual crops and the nurseries of such multiannual crops.
For the purpose of Article 54(2) of Regulation (EC) No 1782/2003 and in case of areas which are also subject to an application for the aid for energy crops provided for in Article 88 of that Regulation, short rotation coppice (CN code ex06029041) Miscanthus sinensis (CN code ex06029051) Phalaris arundicea (reed canary grass) (CN code ex12149090) shall be considered as multiannual crops. However, for the purpose of Article 44(2) of Regulation (EC) No 1782/2003, shall be considered as eligible hectares:
- the areas planted with those crops between 30 April 2004 and 10 March 2005,
- the areas planted with these crops before 30 April 2004 and leased or acquired, between 30 April 2004 and 10 March 2005 in view of applying for the single payment scheme.
d) "multiannual crops" shall mean crops of the following products:
CN code | |
07091000 | Artichokes |
07092000 | Asparagus |
07099090 | Rhubarb |
081020 | Raspberries, blackberries, mulberries and loganberries |
081030 | Black-, white- or redcurrants and gooseberries |
081040 | Cranberries, bilberries and other fruits of the genus Vaccinium |
2. in Article 3, paragraph 4 is replaced by the following:
"4. Paragraphs 2 and 3 shall apply only if the farmer still needs to declare or transfer a payment entitlement or a fraction of a payment entitlement with a fraction of a hectare after having declared or transferred existing payment entitlements or fractions of payment entitlements of the same nature.";
3. in Article 7, paragraph 6 is replaced by the following:
"6. For the purpose of paragraphs 1, 2, 3 and 4, hectares transferred by sale or by lease, and not replaced by a corresponding number of hectares, shall be included in the number of hectares which the farmer declares.";
4. in Article 10 the following paragraph 6 is added:
"6. Member States may fix a ceiling above which paragraph 1 shall apply.";
5. in Article 21, paragraph 4 is replaced by the following:
"4. Long term lease of six and more years started by 15 May 2004 at the latest shall be considered as a purchase of land or investment in production capacity for the application of paragraph 1.";
6. in Article 22(2), the following subparagraph is added:
"For the purposes of application of this paragraph, "land under a lease" shall mean land which was, at the time of, or after the purchase under a lease which has never been renewed except when the renewal was imposed by a legal obligation."
7. in Article 25, the following paragraph is added:
"4. For the application of the second subparagraph of Article 46(2) of Regulation (EC) No 1782/2003, the percentage of the payment entitlements the farmer has used shall be calculated on the number of payment entitlements allocated to him in the first year of application of the single payment scheme, with the exception of payment entitlements sold with land, and must be used during one calendar year.";
8. Article 47 is replaced by the following:
"Article 47
Overrun of the ceilings
Where the sum of the amounts to be paid under each of the schemes provided for in Articles 66 to 71 of Regulation (EC) No 1782/2003 exceeds the ceiling fixed in accordance with Article 64(2) of that Regulation, the amount to be paid shall be reduced proportionately in the year concerned."
Article 2
By way of derogation from Article 51 of Regulation (EC) No 1782/2003, Member States may decide for the year 2005 to apply the second subparagraph of Article 51(b) of that Regulation as amended by Regulation (EC) No 864/2004.
Article 3
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Union.
It shall apply from 1 January 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 8 March 2005.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 270, 21.10.2003, p. 1. Regulation last amended by Regulation (EC) No 118/2005 (OJ L 24, 27.1.2005, p. 15).
[2] OJ L 141, 30.4.2004, p. 1. Regulation amended by Regulation (EC) No 1974/2004 (OJ L 345, 20.11.2004, p. 85).
[3] OJ L 161, 30.4.2004, p. 48.
[4] OJ L 38, 12.2.2000, p. 1."
--------------------------------------------------
