COUNCIL REGULATION (EEC) No 1661/80 of 27 June 1980 on the safeguard measures provided for in the Cooperation Agreement and the Interim Agreement concerning trade and commercial cooperation between the European Economic Community and the Socialist Federative Republic of Yugoslavia
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Articles 43 and 113 thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament (1),
Whereas a Cooperation Agreement between the European Economic Community and the Socialist Federative Republic of Yugoslavia, hereinafter referred to as "the Cooperation Agreement", was signed on 2 April 1980 and an Interim Agreement concerning trade and commercial cooperation, hereinafter referred to as "the Interim Agreement", was concluded on 30 May 1980;
Whereas for the purpose of implementing the safeguard clauses and precautionary measures provided for in Articles 35 to 38 and Article 55 of the Cooperation Agreement and in Articles 22 to 25 and Article 36 of the Interim Agreement, detailed rules should be laid down for the application of Community Regulations, in particular Council Regulation (EEC) No 926/79 of 8 May 1979 on common rules for imports (2) and Council Regulation (EEC) No 3017/79 of 20 December 1979 on protection against dumped or subsidized imports from countries not members of the European Community (3),
HAS ADOPTED THIS REGULATION:
Article 1
In the case of dumping, or subsidies liable to warrant the application by the Community of the measures provided for in Article 35 of the Cooperation Agreement and Article 22 of the Interim Agreement, the introduction of anti-dumping or countervailing duties shall be decided upon in accordance with the procedure and detailed rules laid down in Regulation (EEC) No 3017/79.
Article 2
In cases liable to warrant the application by the Community of the measures provided for in Articles 36 and 55 of the Cooperation Agreement and Articles 23 and 36 of the Interim Agreement, appropriate safeguard measures may, on the conditions laid down in those Articles, be adopted by the Council in accordance with the procedure and detailed rules laid down in Regulation (EEC) No 926/79 and in particular Article 13 thereof.
In urgent cases and subject to the conditions laid down in Article 36 of the Cooperation Agreement and Article 23 of the Interim Agreement: - the Commission may adopt the appropriate safeguard measures in accordance with the procedure and detailed rules laid down in Regulation (EEC) No 926/79, and in particular Article 12 thereof,
- any Member State may take interim safeguard measures in conformity with the procedure described in Article 14 of Regulation (EEC) No 926/79.
Article 3
1. This Regulation shall not preclude the application of Regulations on the common organization of agricultural markets or of Community or national administrative provisions resulting therefrom or of the special Regulations adopted under Article 235 of the Treaty for processed agricultural products ; this Regulation shall apply in addition to such measures. (1)Opinion delivered on 20 June 1980 (not yet published in the Official Journal). (2)OJ No L 131, 29.5.1979, p. 15. (3)OJ No L 339, 31.12.1979, p. 1.
2. The second indent of Article 2 (2) shall not apply as such to products covered by such Regulations.
Article 4
The Commission shall notify the Cooperation Council and the Joint Commission as laid down in Article 38 of the Cooperation Agreement and Article 25 of the Interim Agreement.
Article 5
This Regulation shall enter into force on 1 July 1980.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 27 June 1980.
For the Council
The President
A. SARTI
