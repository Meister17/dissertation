Decision No 4/2006 of the Community/Switzerland Air Transport Committee
of 27 October 2006
amending the Annex to the Agreement between the European Community and the Swiss Confederation on Air Transport
(2006/786/EC)
THE COMMUNITY/SWITZERLAND AIR TRANSPORT COMMITTEE,
Having regard to the Agreement between the European Community and the Swiss Confederation on Air Transport [1], hereinafter referred to as "the Agreement", and in particular Article 23(4) thereof,
HAS DECIDED AS FOLLOWS:
Article 1
1. The following shall be added to point 4 (Air Safety) of the Annex to the Agreement, after the reference to Commission Regulation (EC) No 1701/2003:
"No 1702/2003
Commission Regulation (EC) No 1702/2003 of 24 September 2003 laying down implementing rules for the airworthiness and environmental certification of aircraft and related products, parts and appliances, as well as for the certification of design and production organisations."
The text of the Regulation shall, for the purposes of the Agreement, be read with the following adaptation.
Article 2 shall be amended as follows:
In paragraphs 3, 4, 6, 8, 10, 11, 13 and 14 the date " 28 September 2003" shall be replaced by the wording "the date of entry into force of the Decision of the Community/Switzerland Air Transport Committee which incorporates Regulation (EC) No 1592/2002 into the Annex to the Agreement".
2. The following shall be added to point 4 (Air Safety) of the Annex to the Agreement, after the inclusion referred to in Article 1(1) of the present Decision:
"No 2042/2003
Commission Regulation (EC) No 2042/2003 of 20 November 2003 on the continuing airworthiness of aircraft and aeronautical products, parts and appliances, and on the approval of organisations and personnel involved in these tasks."
3. The following shall be added to point 4 (Air Safety) of the Annex to the Agreement, after the inclusion referred to in Article 1(2) of the present Decision:
"No 381/2005
Commission Regulation (EC) No 381/2005 of 7 March 2005 amending Regulation (EC) No 1702/2003 laying down implementing rules for the airworthiness and environmental certification of aircraft and related products, parts and appliances, as well as for the certification of design and production organizations."
4. The following shall be added to point 4 (Air Safety) of the Annex to the Agreement, after the inclusion referred to in Article 1(3) of the present Decision:
"No 488/2005
Commission Regulation (EC) No 488/2005 of 21 March 2005 on the fees and charges levied by the European Aviation Safety Agency."
Article 2
This Decision shall be published in the Official Journal of the European Union, and the Official Compendium of Swiss Federal Law. It shall enter into force on the first day of the second month following its adoption.
Done at Brussels, 27 October 2006.
For the Joint Committee
The Head of the Community Delegation
Daniel Calleja Crespo
The Head of the Swiss Delegation
Raymond Cron
[1] OJ L 114, 30.4.2002, p. 73.
--------------------------------------------------
