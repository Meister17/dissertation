Commission Decision
of 8 February 2005
on the continuation in the year 2005 of Community comparative trials and tests on propagating material of Hosta Tratt. under Council Directive 98/56/EC started in 2004
(2005/116/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 98/56/EC of 20 July 1998 on the marketing of propagating material of ornamental plants [1],
Having regard to Commission Decision 2003/865/EC of 11 December 2003 setting out the arrangements for Community comparative trials and tests on propagating material of Pelargonium l’Hérit. and Hosta Tratt., Euphorbia pulcherrima Willd. ex Klotzsch and Rosa L. under Council Directive 98/56/EC [2], and in particular Article 3 thereof,
Whereas:
(1) Decision 2003/865/EC sets out the arrangements for the comparative trials and tests to be carried out under Directive 98/56/EC as regards Hosta Tratt. for 2004 and 2005.
(2) Tests and trials carried out in 2004 should be continued in 2005,
HAS DECIDED AS FOLLOWS:
Sole Article
Community comparative trials and tests which began in 2004 on propagating material of Hosta Tratt. shall be continued in 2005 in accordance with Decision 2003/865/EC.
Done at Brussels, 8 February 2005.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 226, 13.8.1998, p. 16. Directive as last amended by Directive 2003/61/EC (OJ L 165, 3.7.2003, p. 23).
[2] OJ L 325, 12.12.2003, p. 62.
--------------------------------------------------
