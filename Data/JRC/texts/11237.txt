Decision of the EEA Joint Committee
No 86/2006
of 7 July 2006
amending Annex IX (Financial services) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex IX to the Agreement was amended by Decision of the EEA Joint Committee No 60/2006 of 2 June 2006 [1].
(2) Directive 2005/14/EC of the European Parliament and of the Council of 11 May 2005 amending Council Directives 72/166/EEC, 84/5/EEC, 88/357/EEC and 90/232/EEC and Directive 2000/26/EC of the European Parliament and of the Council relating to insurance against civil liability in respect of the use of motor vehicles [2] is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
Annex IX to the Agreement shall be amended as follows:
1. the following indent shall be added in points 7 (Second Council Directive 88/357/EEC), 8 (Council Directive 72/166/EEC) and 9 (Second Council Directive 84/5/EEC):
- "32005 L 0014: Directive 2005/14/EC of the European Parliament and of the Council of 11 May 2005 (OJ L 149, 11.6.2005, p. 14).";
2. the following shall be added in point 10 (Third Council Directive 90/232/EEC):
", as amended by:
- 32005 L 0014: Directive 2005/14/EC of the European Parliament and of the Council of 11 May 2005 (OJ L 149, 11.6.2005, p. 14).";
3. the following shall be added in point 10a (Directive 2000/26/EC of the European Parliament and of the Council):
", as amended by:
- 32005 L 0014: Directive 2005/14/EC of the European Parliament and of the Council of 11 May 2005 (OJ L 149, 11.6.2005, p. 14).
The provisions of the Directive shall, for the purposes of this Agreement, be read with the following adaptation:
Article 4(8) shall read as follows:
"The appointment of a claims representative shall not in itself constitute the opening of a branch within the meaning of Article 1(b) of Directive 92/49/EEC and the claims representative shall not be considered an establishment within the meaning of Article 2(c) of Directive 88/357/EEC.""
Article 2
The text of Directive 2005/14/EC in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 8 July 2006, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [3].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 7 July 2006.
For the EEA Joint Committee
The President
Oda Helen Sletnes
[1] OJ L 245, 7.9.2006, p. 7.
[2] OJ L 149, 11.6.2005, p. 14.
[3] Constitutional requirements indicated.
--------------------------------------------------
