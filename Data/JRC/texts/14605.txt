Publication of an application pursuant to Article 6(2) of Council Regulation (EC) No 510/2006 on the protection of geographical indications and designations of origin for agricultural products and foodstuffs
(2006/C 127/08)
This publication confers the right to object to the application pursuant to Article 7 of Council Regulation (EC) No 510/2006. Statements of objection must reach the Commission within six months from the date of this publication.
SUMMARY
COUNCIL REGULATION (EC) No 510/2006
Application for registration according to Article 5 and Article 17(2)
"EKSTRA DEVIŠKO OLJČNO OLJE SLOVENSKE ISTRE"
EC No: SI/0420/ 29.10.2004
PDO ( X ) PGI ( )
This summary has been drawn up for information only. For full details, interested parties are invited to consult the full version of the product specification obtainable from the national authorities indicated in section 1 or from the European Commission [1].
1. Responsible department in the Member State
Name: | Ministrstvo za kmetijstvo, gozdarstvo in prehrano |
Address: | Dunajska cesta 58, SLO-1000 Ljubljana |
Tel.: | (386-1) 478 90 00 |
Fax: | (386-1) 478 90 55 |
e-mail: | varnahrana.mkgp@gov.si |
2. Group
Name: | DOSI — Društvo Oljkarjev Slovenske Istre |
Address: | p.p. 55, SLO-6310 Izola |
Tel.: | — |
Fax: | — |
e-mail: | — |
Composition: | Producers/processors ( X ) Other ( ) |
3. Type of product
Class 1.5. Oil
4. Specification
(summary of requirements under Article 4(2))
Name : "Ekstra deviško oljčno olje Slovenske Istre"
Description Chemical composition:
The distinguishing features of "Ekstra deviško oljčno olje Slovenske Istre" are its high oleic acid content (≥ 72), its low linoleic acid content (≤ 8,0) and a high antioxidant content. Its peroxide number, expressed in mmol O2/kg, is ≤ 7.
Organoleptic description:
The oil has a characteristic fruity aroma reminiscent of olives and other fruits. It must have no organoleptic defects and its overall organoleptic rating must be 6,5.
Geographical area - in the north by the border between Slovenia and Italy;
- in the west by the sea;
- in the south by the border between Slovenia and Croatia;
- in the east by the edge of the Karst region.
Proof of origin :
The area of production is defined at point 4.3. above. The manner in which the oil is produced must also meet criteria designed to guarantee the top quality and special characteristics of "Ekstra deviško oljčno olje Slovenske Istre".
All producers must be registered within the demarcated region and enrolled in the list of producers. Processing must also take place at registered olive oil mills in the same region. During storage all containers must bear the words "geografsko poreklo" (geographical origin) and have a serial number. All information on the series must also be shown on the label. Bottling is permitted only in registered plants in the demarcated region.
Method of production - the oils must be obtained from the fruit at a temperature of less than 27° C;
- nothing may be added during processing, except water;
- the olives must be processed within 48 hours of picking.
- growing of olives (supervision of quantities and varieties);
- picking of olives (by hand) at optimum ripeness;
- storage of olives (for no more than 48 hours);
- processing (at registered olive mills) with controls on hygiene, temperature and yield;
- storage in properly labelled containers at an appropriate temperature (12 to 20° C);
- sampling (quality control);
- bottling and labelling in registered bottling plants;
- storage of bottled oil.
Link Authenticity:
Slovenian Istria enjoys a very favourable geographical situation for olive-growing, as it is here that the Mediterranean — with its characteristic climate — reaches its most northerly point, making for an excellent relationship between yield and biophenol content. "Ekstra deviško oljčno olje Slovenske Istre" therefore has a high content of antioxidants and biophenols and achieves top quality. Another contributing factor is the special nature of the soil: Slovenian Istria consists mainly of carbonate flysch covered by Eutric Cambisol soils (the degree of base saturation is over 50 %), the type in which olive trees thrive best.
History:
Olive-oil production in Slovenian Istria is a tradition stretching far back into the past. The Greek historian Pausanius (180-115 BC) mentions Istrian olive oil in his Description of Greece (10.32.19). References to oil production in the region are also preserved in many sources from 1201 to the 17th century and right up to the present day (see detailed list in the application). These show how the production of olive oil in Slovenian Istria has developed through the centuries. Traditional methods and knowledge acquired down the centuries still contribute today to the high quality of the region's olive oil.
Name: | INSPECT d.o.o |
Bureau Veritas Company |
Address: | Linhartova 49a, SLO-1000 Ljubljana |
Tel.: | (386-1) 475 76 70 |
Fax: | (386-1) 474 76 02 |
e-mail: | inspect@bureauveritas.com |
Inspection body Labelling :
"Ekstra deviško oljčno olje Slovenske Istre" is labelled in accordance with Commission Regulation No 1019/2002 on marketing standards for olive oil.
"Ekstra deviško oljčno olje Slovenske Istre" can be bottled only in Slovenian Istria. The designation "Ekstra deviško oljčno olje Slovenske Istre" and the indication "prvo hladno prešanje" [first cold pressing] or "hladno stiskanje" [cold pressing] must be visible on the label.
To ensure traceability, the association will issue labels with serial numbers in accordance with the register of producers of olive oil of designated origin.
National requirements : Rules on the designation of geographical origin of "Ekstra deviško oljčno olje Slovenske Istre" (Official Gazette of the Republic of Slovenia, No 47/04).
[1] European Commission, Directorate-General for Agriculture and Rural Development, Agricultural Product Quality Policy, B-1049 Brussels.
--------------------------------------------------
