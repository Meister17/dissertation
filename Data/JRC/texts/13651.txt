Action brought on 6 January 2006 — Frankin and Others v Commission
Parties
Applicants: Jacques Frankin (Sorée, Belgium) and Others (represented by: G. Bounéou and F. Frabetti, lawyers)
Defendant: Commission of the European Communities
Form of order sought
The applicants claim that the Court should:
- annul the explicit decision of 10 June 2005 whereby the Commission refused to afford the applicants assistance under Article 24 of the Staff Regulations;
- order the Commission to make good all the loss thereby sustained by the applicants;
- order the defendant to pay the costs.
Pleas in law and main arguments
The applicants, who are all officials or other servants of the Commission, had sought to have their pension entitlement acquired in Belgium transferred to the Community system, in accordance with the provisions of a Belgian law enacted in 1991. In 2003 Belgium enacted a new law, which, in the applicants' submission, provides more favourable for new transfers of that type. As the applicants had already transferred their rights, however, they were unable to take advantage of the provisions of the Law of 2003.
The applicants therefore submitted a request seeking to obtain the assistance provided for in Article 24 of the Staff Regulations. The Commission, which had no intention of assisting its officials and temporary servants to secure those transfers, rejected their request by decision of 10 June 2005.
By their action, the applicants contest that decision, which they treat as a refusal to afford assistance, in breach of Article 24 of the Staff Regulations. In addition to that article, they rely in support of their claims on a breach of the duty to have regard to the welfare of the staff, of the principle of non-discrimination, of the prohibition of unfair process, of the obligation to state reasons, of legitimate expections and of the rule patere legem quam ipse fecisti and on a misuse of powers.
--------------------------------------------------
