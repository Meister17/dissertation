COMMISSION DIRECTIVE of 29 May 1991 on establishing indicative limit values by implementing Council Directive 80/1107/EEC on the protection of workers from the risks related to exposure to chemical, physical and biological agents at work (91/322/EEC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Directive 80/1107/EEC of 27 November 1980 on the protection of workers from the risks related to exposure to chemical, physical and biological agents at work (1), as last amended by Directive 88/642/EEC (2), and in particular the first subparagraph of Article 8 (4) thereof,
Having regard to the opinion of the Advisory Committee on Safety, Hygiene and Health Protection at Work,
Whereas the third subparagraph of Article 8 (4) of Directive 80/1107/EEC states that indicative limit values shall reflect expert evaluations based on scientific data;
Whereas the aim of fixing these values is the harmonization of conditions in this area, while maintaining the improvements made;
Whereas the Directive constitutes a practical step towards the achievement of the social dimension of the internal market;
Whereas occupational exposure limit values should be regarded as an important part of the overall approach to ensuring the protection of the health of workers at the workplace;
Whereas an initial list of occupational exposure limit values can be established for agents for which similar values exist in the Member States, giving priority to agents which are found at places of work and are likely to have an effect on the health of workers; whereas this list can be based on existing scientific data as far as the effects on health are concerned, although for certain agents these data are very limited;
Whereas in addition it may be necessary to establish occupational exposure limit values for shorter periods taking into account the effects arising from short term exposure;
Whereas a reference method covering, inter alia, assessment of exposure and measuring strategy for occupational exposure limit values is contained in Directive 80/1107/EEC;
Whereas, in view of the importance of obtaining reliable measurements of exposure in relation to occupational exposure limit values, it may be necessary in the future to establish appropriate reference methods;
Whereas occupational exposure limit values need to be kept under review and will need to be revised if new scientific data indicate that they are no longer valid;
Whereas, for some agents it will be necessary in the future to consider all absorption pathways, including the possibility of penetration through the skin, in order to ensure the best possible level of protection;
Whereas the measures laid down in this Directive are in conformity with the opinion of the Committee set up pursuant to Article 9 of Directive 80/1107/EEC,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Indicative limit values, of which Member States shall take account, inter alia, when establishing the limit values referred to in Article 4 (4) (b) of Directive 80/1107/EEC are listed in the Annex.
Article 2
1. Member States shall bring into force the provisions necessary to comply with this Directive by 31 December 1993. They shall immediately inform the Commission thereof.
When Member States adopt these provisions, these shall contain a reference to this Directive or shall be accompanied by such reference at the time of their official publication. The procedure for such reference shall be adopted by Member States.
2. Member States shall communicate to the Commission the provisions of national law which they adopt in the field governed by this Directive.
Article 3
This Directive is addressed to the Member States.
Done at Brussels, 29 May 1991. For the Commission
Vasso PAPANDREOU
Member of the Commission
(1) OJ No L 327, 3. 12. 1980, p. 8. (2) OJ No L 356, 24. 12. 1988, p. 74.
ANNEX
INDICATIVE LIMIT VALUES FOR OCCUPATIONAL EXPOSURE
Einecs (1) CAS (2) Name of agent Limit values (3) mg/m3 (4) ppm (5) 2 001 933 54-11-5 Nicotine (6) 0,5 - 2 005 791 64-18-6 Formic acid 9 5 2 005 807 64-19-7 Acetic acid 25 10 2 006 596 67-56-1 Methanol 260 200 2 008 352 75-05-8 Acetonitrile 70 40 2 018 659 88-89-1 Picric acid (6) 0,1 - 2 020 495 91-20-3 Naphtalene 50 10 2 027 160 98-95-3 Nitrobenzene 5 1 2 035 852 108-46-3 Resorcinol (6) 45 10 2 037 163 109-89-7 Diethylamine 30 10 2 038 099 110-86-1 Pyridine (6) 15 5 2 046 969 124-38-9 Carbon dioxide 9 000 5 000 2 056 343 144-62-7 Oxalic acid (6) 1 - 2 069 923 420-04-2 Cyanamide (6) 2 - 2 151 373 1305-62-0 Calcium dihydroxide (6) 5 - 2 152 361 1314-56-3 Disphosphorus pentaoxide (6) 1 - 2 152 424 1314-80-3 Disphosphorus pentasulphide (6) 1 - 2 152 932 1319-77-3 Cresols (all isomers) (6) 22 5 2 311 161 7440-06-4 Platinum (metallic) (6) 1 - 2 314 843 7580-67-8 Lithium hydride (6) 0,025 - 2 317 781 7726-95-6 Bromine (6) 0,7 0,1 2 330 603 10026-13-8 Phosphorus pentachloride (6) 1 - 2 332 710 10102-43-9 Nitrogen monoxide 30 25 8003-34-7 Pyrethrum 5 - Barium (soluble compounds as Ba) (6) 0,5 - Silver (soluble compounds as Ag) (6) 0,01 - Tin (inorganic compounds as Sn) (6) 2 -
(1) Einecs: European Inventory of Existing Chemical Substances.
(2) CAS: Chemical Abstract Service Number.
(3) Measured or calculated in relation to a reference period of eight hours.
(4) Mg/m3 = milligrams per cubic metre of air at 20 °C and 101,3 KPa (760 mm mercury pressure).
(5) Ppm = parts per million by volume in air (ml/m3).
(6) Existing scientific data on health effects appear to be particularly limited.
