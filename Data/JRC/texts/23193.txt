COUNCIL DECISION of 23 March 1998 concerning the approval, on behalf of the Community, of PARCOM Decision 96/1 on the phasing-out of the use of hexachloroethane in the non-ferrous metal industry (98/241/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 100a, in conjunction with Article 228(2), first sentence, and (3), first subparagraph,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Whereas by Council Decision of 3 March 1975 the Community concluded the Convention on the Prevention of Marine Pollution from Land-Based Sources (Paris Convention) (3) thus becoming a Contracting Party to this Convention;
Whereas the executive body of the Paris Convention (PARCOM - Paris Commission) may adopt measures in the area of pollution prevention and that it adopted PARCOM Decision 96/1 on the phasing-out of the use of hexachloroethane in the non-ferrous metal industry;
Whereas the Commission participated in the adoption of PARCOM Decision 96/1 on the basis of the authorisation granted by the Council and within the negotiating directives issued accordingly;
Whereas hexachloroethane appears on the list of dangerous substances in Annex I to Council Directive 76/769/EEC of 27 July 1976 on the approximation of the laws, regulations and administrative provisions of the Member States relating to restrictions on the marketing and use of certain dangerous substances and preparations (4);
Whereas the provisions of PARCOM Decision 96/1 are in line with Directive 76/769/EEC;
Whereas it is therefore desirable that the Community approve PARCOM Decision 96/1,
HAS DECIDED AS FOLLOWS:
Sole Article
1. PARCOM Decision 96/1 on the phasing-out of the use of hexachloroethane in the non-ferrous metal industry is hereby approved on behalf of the Community.
The text of the said Decision is attached to this Decision.
2. The Commission is hereby authorised to notify this approval to the Paris Commission.
Done at Brussels, 23 March 1998.
For the Council
The President
M. MEACHER
(1) OJ C 364, 2. 12. 1997, p. 13.
(2) OJ C 80, 16. 3. 1998.
(3) OJ L 194, 25. 7. 1975, p. 5.
(4) OJ L 262, 27. 9. 1976, p. 201. Directive as last amended by Commission Directive 97/64/EC (OJ L 315, 19. 11. 1997, p. 13).
