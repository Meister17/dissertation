European Agency for Safety and Health at Work — Publication of the final accounts for the financial year 2004
(2005/C 269/01)
The complete version of the final accounts may be found at the following address:
http://agency.osha.eu.int
Table 1
Implementation of the budget for the financial year 2004
NB: Differences in totals are due to the effects of rounding.
(million euro) |
Revenue | Expenditure |
Source of revenue | Revenue entered in the final budget for the financial year | Revenue collected | Expenditure allocation | Appropriations in the final budget | Appropriations carried over from the previous financial year | Available appropriations (2004 budget and financial year 2003) |
entered | committed | paid | carried over | cancelled | outstanding commitments | paid | cancelled | appropriations | committed | paid | carried over | cancelled |
Community subsidies | 10,6 | 9,5 | Title I Staff | 4,3 | 3,7 | 3,4 | 0,1 | 0,9 | 0,1 | 0,1 | 0,0 | 4,4 | 3,8 | 3,4 | 0,1 | 0,9 |
Own revenue | 0,0 | 0,0 | Title II Administration | 1,4 | 1,2 | 1,0 | 0,2 | 0,2 | 0,2 | 0,2 | 0,0 | 1,6 | 1,4 | 1,1 | 0,2 | 0,2 |
Other subsidies | 0,2 | 0,1 | Title III Operating activities | 5,0 | 5,0 | 2,4 | 2,5 | 0,1 | 5,9 | 5,0 | 0,8 | 10,9 | 10,9 | 7,4 | 2,5 | 0,9 |
Other revenue | 0,0 | 0,1 | Phare expenditure | | | | | | 0,5 | 0,4 | 0,1 | 0,5 | 0,5 | 0,4 | | 0,1 |
Phare revenue | 0,0 | 0,1 | | | | | | | | | | | | | | |
Total | 10,8 | 9,8 | Total | 10,8 | 10,0 | 6,8 | 2,9 | 1,2 | 6,6 | 5,6 | 1,0 | 17,4 | 16,6 | 12,4 | 2,9 | 2,2 |
Table 2
Revenue and expenditure account for the financial years 2004 and 2003
NB: Differences in totals are due to the effects of rounding.
(1000 euro) |
| 2004 | 2003 |
Revenue
Community subsidies | 9542 | 11641 |
Other subsidies | 66 | 66 |
Other revenue | 111 | 157 |
Phare revenue | 121 | 824 |
Total revenue (a) | 9840 | 12688 |
Expenditure
Staff — Title I of the budget
Payments | 3379 | 3245 |
Appropriations carried over | 60 | 87 |
Administration — Title II of the budget
Payments | 966 | 1146 |
Appropriations carried over | 248 | 186 |
Operating activities — Title III of the budget
Payments | 2426 | 2559 |
Appropriations carried over | 2549 | 5859 |
Phare expenditure
Payments | 0 | 548 |
Appropriations carried over | 0 | 502 |
Total expenditure (b) | 9628 | 14131 |
Outturn for the financial year (c = a - b) | 212 | – 1443 |
Balance carried over from the previous financial year | – 1987 | – 1108 |
Appropriations carried over from the previous financial year and cancelled | 887 | 766 |
Sums to be re-used carried over from the previous financial year and not used | 0 | 1 |
RO (PhareII) | 144 | 0 |
Payments against commitments cancelled in 2002 | 0 | – 191 |
Exchange-rate differences | 0 | 4 |
Phare amount to be refunded to the Commission | – 39 | 0 |
Adjustment entries | 3 | – 16 |
Outturn for the financial year before economic adjustments (d) | – 779 | – 1987 |
Budget revenue to be recovered | 0 | 850 |
Other revenue to be recovered | 0 | 3 |
Acquisitions of fixed assets | 58 | 207 |
Depreciation | – 175 | – 186 |
Stock | – 6 | 0 |
Scrapping of fixed assets | – 91 | 0 |
Depreciation | 88 | 0 |
Miscellaneous expenditure | – 34 | – 1 |
Economic adjustments (e) | – 161 | 873 |
Balance for the financial year (d + e) | – 940 | – 1113 |
Table 3
Balance sheet at 31 December 2004 and 31 December 2003
NB: Differences in totals are due to the effects of rounding.
(1000 euros) |
Assets | 2004 | 2003 | Liabilities | 2004 | 2003 |
Fixed assets | | | Own capital | | |
Computer software | 66 | 95 | Own capital | 431 | 431 |
IT equipment | 90 | 136 | Balance for the financial year | – 67 | – 1113 |
Fittings and furniture | 169 | 215 | Subtotal | 365 | – 683 |
Transportation equipment | 0 | 0 | Medium and long-term liabilities | | |
Subtotal | 325 | 445 | Liabilities against assigned revenue | 0 | 0 |
Medium and long-term assets | | | Subtotal | 0 | 0 |
Community subsidies | 0 | 0 | Current liabilities | | |
Subtotal | 0 | 0 | Non-automatic carry-overs | 0 | 135 |
Stocks | | | Automatic carry-overs | 2857 | 6498 |
Office supplies | 0 | 6 | Commission | 145 | 282 |
Subtotal | 0 | 6 | Sundry accounts payable | 67 | 128 |
Current assets | | | Payroll deductions | 0 | 73 |
Community subsidies | 809 | 1035 | Liabilities against assigned revenue | 0 | 0 |
Sundry accounts receivable | 19 | 62 | Subtotal | 3069 | 7117 |
Recoverable VAT | 0 | 0 | Suspense accounts | | |
Subtotal | 828 | 1097 | Recovery orders | 0 | 0 |
Cash accounts | | | Revenue for re-use | 0 | 11 |
Bank and cash | 2305 | 4889 | Subtotal | 0 | 11 |
Imprest accounts | 3 | 1 | Provisions for risks and charges | | |
Subtotal | 2308 | 4890 | Cases pending | 34 | 0 |
Suspense accounts | | | Subtotal | 34 | 0 |
Advances | 6 | 6 | | | |
Subtotal | 6 | 6 | | | |
Total | 3468 | 6445 | Total | 3468 | 6445 |
[1] This sum includes payments made from the appropriations to be reused in 2004 (18573 euro).
[2] Calculation according to the principles of Article 15 of Council Regulation (EC, Euratom) No 1150/2000 of 22 May 2000 (OJ L 130, 31.5.2000, p. 8).
--------------------------------------------------
