COUNCIL DIRECTIVE of 24 July 1979 amending Directives 66/401/EEC, 66/402/EEC, 70/458/EEC and 70/457/EEC on the marketing of fodder plant seed, cereal seed and vegetable seed and on the common catalogue of varieties of agricultural plant species (79/692/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Articles 43 and 100 thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas, for the reasons given below, certain Directives on the marketing of seeds and propogating material should be amended;
Whereas, in the case of fodder plant seed, certain conditions concerning seed testing should be relaxed for those regions of the Community in which very favourable ecological conditions ensure compliance with the relevant Community standards laid down;
Whereas, in the case of seed of rye used for fodder purposes, a temporary relaxation of the Community standards applicable to rye seed should be permitted in order to overcome the temporary difficulties which exist in certain Community regions for these products;
Whereas, in the case of cereal seed, recently experimented methods for reducing the effects of attack by certain harmful organisms appear to justify the acceptance of mixtures of seed of different varieties of cereals, provided that precautions are taken to guarantee that the quality of the seed or of the final product is not affected;
Whereas, in order to improve the functioning of the systems of the common catalogue of varieties of agricultural plant species, certain provisions relating to the acceptance of varieties at national level, to the names of varieties, and to the exchange of information between the Member States should be adapted to internationally established rules and the rules concerning the free marketing, as regards variety, of seeds belonging to the varieties accepted in the various Member States should be strengthened, in the case of certain species at least;
Whereas the provisions on the marketing of vegetable seed and the provisions on the common catalogue of varieties of agricultural plant species provide that, after 1 July 1977, the equivalence of measures taken in third countries concerning varieties can no longer be (1)OJ No C 174, 21.7.1978, p. 8. (2)OJ No C 239, 9.10.1978, p. 54. (3)OJ No C 114, 7.4.1979, p. 26.
established at national level ; whereas certain equivalence decisions taken at Community level only took effect as from 1 July 1978 ; whereas the period provided for the establishment of equivalence at national level should therefore be extended by one year in order to cover traditional trade patterns which were maintained after 1 July 1977 pending Community decisions,
HAS ADOPTED THIS DIRECTIVE:
Article 1
The following paragraph shall be added to Article 2 of Council Directive 66/401/EEC of 14 June 1966 on the marketing of fodder plant seed (1), as last amended by Directive 79/641/EEC (2):
"1c. Member States may be authorized, in accordance with the procedure laid down in Article 21, not to apply in respect of the production of a given Member State the condition contained in Annex II, section I, point 2, paragraph B (1) for one or more of the species concerned, if it can be assumed from ecological conditions and previous experience that the standards laid down in Annex II, section I, point 2, column 13 of the table are being complied with."
Article 2
Council Directive 66/402/EEC of 14 June 1966 on the marketing of cereal seed (3), as last amended by Directive 79/641/EEC is hereby amended as follows: 1. The following paragraph shall be added to Article 2:
"1c. Until 30 June 1982, Member States may be authorized, in accordance with the procedure laid down in Article 21, to permit the marketing of seed of specific varieties of rye, chiefly intended for use as fodder, which do not satisfy the conditions laid down: - in Annex II as regards germination,
- and in Annex II (2), table A, column 6, as regards basic seed."
2. The following first paragraph shall be added to Article 13:
"1. Member States may permit seed of a species of cereal to be marketed in the form of specific mixtures of seeds of various varieties provided that scientific or technical knowledge indicates that these mixtures are such as to be particularly effective against the propagation of certain harmful organisms and provided also that the components of the mixture comply, before mixing, with the marketing rules applicable to them."
3. In Article 13, paragraph 1 shall become paragraph 2 and paragraph 2 shall become paragraph 3.
4. The following number shall be added to Annex IV, A (b):
"8. "Marketing permitted exclusively in ..." (Member State concerned)."
Article 3
Council Directive 70/457/EEC of 29 September 1970 on the common catalogue of varieties of agricultural plant species (4), as last amended by Directive 78/55/EEC (5), is hereby amended as follows: 1. Article 5 (1) shall be replaced by the following:
"1. A variety shall be regarded as distinct if, whatever the origin, artificial or natural, of the initial variation from which it has resulted, it is clearly distinguishable on one or more important characteristics from any other variety known in the Community.
The characteristics of a variety must be capable of precise recognition and precise definition.
A variety known in the Community shall be any variety which, at the time when the application for the acceptance of the variety to be assessed is duly made, is: - either listed in the common catalogue of varieties of agricultural plant species or the catalogue of varieties of vegetable species,
- or, without being listed in one of those catalogues, has been accepted or submitted for acceptance in the Member State in question or in another Member State, either for certification and marketing, or for certification for other countries,
unless the aforementioned conditions are no longer fulfilled in all the Member States concerned before the decision on the application for acceptance of the variety to be assessed is taken." (1)OJ No 125, 11.7.1966, p. 2298/66. (2)OJ No L 183, 19.7.1979, p. 13. (3)OJ No 125, 11.7.1966, p. 2309/66. (4)OJ No L 225, 12.10.1970, p. 1. (5)OJ No L 16, 20.1.1978, p. 23.
2. The following shall be added to Article 7 (1):
"In order to establish distinctness, the growing trials shall include at least the available comparable varieties which are varieties known in the Community within the meaning of Article 5 (1). For the purpose of applying Article 9, other available comparable varieties shall be included."
3. Article 9 (3) shall be replaced by the following:
"3. In taking into account the information available, Member States shall also ensure that a variety which is not clearly distinguishable: - from a variety previously accepted in the Member State in question or in another Member State, or
- from another variety which has been assessed with regard to distinctness, stability and uniformity in accordance with rules corresponding to those of this Directive, without, however, being a variety known in the Community within the meaning of Article 5 (1),
bears the name of that variety. This provision shall not apply if this name is likely to mislead or cause confusion concerning the variety in question, or if, pursuant to all the provisions of the Member State concerned governing the names of varieties, other facts prevent its utilization, or if the rights of third parties impede the free use of that name in connection with the variety in question."
4. Paragraph 3 of Article 9 shall become paragraph 4.
5. Article 10 (1) shall be replaced by the following:
"1. Any application or withdrawal of an application for acceptance of a variety, any entry in a catalogue of varieties as well as any amendment thereto shall be notified forthwith to the other Member States and the Commission."
6. The following Article shall be added after Article 12:
"Article 12a
1. Member States shall ensure that any doubts which arise after the acceptance of a variety concerning the appraisal of its distinctness or of its name at the time of acceptance are clarified.
2. Where, after acceptance of a variety, it is established that the condition concerning distinctness within the meaning of Article 5 was not fulfilled at the time of acceptance, acceptance shall be replaced by another decision or, where appropriate, a revocation, which conforms with this Directive.
By this other decision, the variety shall, with effect from the date of its initial acceptance, no longer be regarded as a variety known in the Community within the meaning of Article 5 (1).
3. Where, after acceptance of a variety, it is established that its name within the meaning of Article 9 was not acceptable when the variety was accepted, the name shall be adapted in such a way that it conforms with this Directive. Member States may permit the previous name to be used temporarily as an additional name. The detailed arrangements in accordance with which the previous name may be used as an additional name may be laid down in accordance with the procedure provided for in Article 23."
7. The following shall be added to Article 15 (2):
"Only applications submitted four months before the end of the abovementioned period shall be taken into consideration."
8. The following shall be added to Article 15 (5):
"In the case of Beta vulgaris L. and Solanum tuberosum L. this condition shall be considered fulfilled on 31 December each year in respect of varieties for which the communications provided for in Article 10 (1) and (2) were made four months before that date, unless a Member State informs the Commission or makes a declaration to the Standing Committee on Seeds and Propagating Material to the effect that it does not intend to dispense with making an application pursuant to paragraph 2. Acting on a proposal from the Commission, the Council shall decide whether these provisions will also be applied to other species."
9. In Article 21 (2), the words "30 June 1977" shall be replaced by "30 June 1978".
Article 4
In Article 32 (2) of Council Directive 70/458/EEC of 29 September 1970 on the marketing of vegetable seed (1), as last amended by Directive 79/641/EEC, the words "30 June 1977" shall be replaced by "30 June 1978".
Article 5
Member States shall bring into force the laws, regulations and administrative provisions necessary to comply: (1)OJ No L 225, 12.10.1970, p. 7. - with Articles 1 and 2 with effect from 1 January 1980,
- with Articles 3 (9) and 4 with effect from 1 July 1977,
- with the other provisions of this Directive on 1 July 1982.
Article 6
This Directive is addressed to the Member States.
Done at Brussels, 24 July 1979.
For the Council
The President
J. GIBBONS
