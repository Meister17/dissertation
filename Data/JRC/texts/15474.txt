Action brought on 7 November 2005 — Tesoka v European Foundation for the Improvement of Living and Working Conditions
Parties
Applicant: Sabrina Tesoka (Overijse, Belgium) (represented by: J.-L. Fagnart, lawyer)
Defendant: European Foundation for the Improvement of Living and Working Conditions
Form of order sought
The applicant claims that the Court should:
- Annul the explicit decision of rejection of 14 October 2005
- Rule that the applicant may receive all the allowances and benefits which she can claim by reason of her resignation of 2 August 2005 pursuant to the second subparagraph of Article 17(2) of Regulation No 1860/76 amended by Article 8 of Regulation (EC) No 1111/2005 of 24 June 2005;
- Order the defendant to pay the applicant fair compensation of EUR 35000, together with interest for late payment at the rate of 7 % from 2 August 2005;
- Order the defendant to pay the costs.
Pleas in law and main arguments
The applicant, a member of the defendant's staff since 2001, resigned on 2 August 2005 in order to receive the pecuniary benefits set out under Regulation No 1111/2005 for staff members who had resigned before 4 August 2005. In her action, the applicant argues that the defendant rejected her application for indemnities to which she was entitled, and for the documents which she needed to receive social protection in her country of residence, and claims annulment of that decision of refusal and compensation for the damage allegedly suffered.
In support of her action, she alleges infringement of the second subparagraph of Article 17(2) of Regulation No 1860/76 as amended by Article 8 of Regulation (EC) No 1111/2005, infringement of Article 28a of the Conditions of Employment of Other Servants of the European Communities, of the provisions of Commission Regulation 91/88 of 13 January 1988, and of her legitimate expectations.
--------------------------------------------------
