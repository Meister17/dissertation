COMMISSION REGULATION (EEC) No 3899/92
of 23 December 1992
amending Regulation (EEC) No 3510/82 fixing the conversion factors applicable to tuna
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 3759/92 of 17 December 1992 on the common organization of the market in fishery and aquaculture products (1), and in particular Article 17 (6) thereof,
Whereas Commission Regulation (EEC) No 3510/82 (2), as last amended by Regulation (EEC) No 3971/89 (3), fixes the conversion factors applicable to the different species, sizes and presentations of tuna;
Whereas the developments on the tuna market since the fixing of the conversion factors have brought about a change in the commercial value ratio between the products concerned and the pilot product; whereas this state of affairs calls for the conversion factors in question and the products for which they are specifically intended to be reviewed;
Whereas, therefore, the Annex to Regulation (EEC) No 3510/92 should be amended;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Fishery Products,
HAS ADOPTED THIS REGULATION:
Article 1
The Annex to Regulation (EEC) No 3510/82 is replaced by the Annex to this Regulation.
Article 2
This Regulation shall enter into force on 1 January 1993.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 23 December 1992. For the Commission
Manuel MARÍN
Vice-President
ANNEX
<(BLK0)BLK2 ID="I">
I. Conversion factors applicable to the different species of tuna
>TABLE>
II. Conversion factors applicable to each of the species referred to in section I depending on the presentation >TABLE>
</(BLK0)BLK2>
