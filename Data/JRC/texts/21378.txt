Commission Decision
of 24 June 2002
amending Decision 1999/120/EC drawing up provisional lists of third country establishments from which the Member States authorise imports of animal casings, with respect to Ukraine
(notified under document number C(2002) 2226)
(Text with EEA relevance)
(2002/483/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Decision 95/408/EC of 22 June 1995 on the conditions for drawing up, for an interim period, provisional lists of third country establishments from which Member States are authorised to import certain products of animal origin, fishery products or live bivalve molluscs(1), as last amended by Decision 2001/4/EC(2), and in particular Article 2(4) thereof,
Whereas:
(1) Provisional lists of establishments in third countries producing animal casings have been drawn up by Commission Decision 1999/120/EC(3), as last amended by Decision 2000/80/EC(4).
(2) Ukraine has provided the name of an establishment producing animal casings for which the competent authorities certify that the establishment is in accordance with the Community rules.
(3) A provisional listing of this establishment can thus be drawn up for Ukraine. Decision 1999/120/EC should therefore be amended accordingly.
(4) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
In the Annex to Decision 1999/120/EC, the following row is added for Ukraine:
País: Ucrania/Land: Ukraine/Land: Ukraine/Χώρα: Ουκρανία/Country: Ukraine/Pays: Ukraine/Paese: Ucraina/Land: Oekraïne/País: Ucrânia/Maa: Ukraina/Land: Ukraina
>TABLE>
Article 2
This Decision shall apply as from the 20th day following that of its publication in the Official Journal of the European Communities.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 24 June 2002.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 243, 11.10.1995, p. 17.
(2) OJ L 2, 5.1.2001, p. 21.
(3) OJ L 36, 10.2.1999, p. 21.
(4) OJ L 30, 4.2.2000, p. 41.
