Brussels, 17.12.2004
COM(2004) 812 final
2004/0283 (ACC)
Proposal for a
COUNCIL DECISION
on the signing and the provisional application of a bilateral agreement between the European Community and the Republic of Belarus on trade in textile products
(presented by the Commission)
EXPLANATORY MEMORANDUM
In accordance with the Council negotiating directives of 11 October 2004, the Commission has negotiated an agreement on the renewal for one year of the existing bilateral Agreement between the European Community and the Republic of Belarus on trade in textile products. This agreement in the form of an exchange of letters will be initialled on xx December 2004.
The agreement provides for an extension of the present textiles agreement until 31 December 2005 and is in compliance with the overall EU policy approach vis-à-vis Belarus. In view of the political situation prevailing in Belarus, the proposed Agreement is restrictive. In 2005 Belarus would be, together with North Korea, the only country subject to textile quotas in the EU.
The quantitative limits have been adjusted to take account of the normal yearly growth rates provided for in the current agreement. Adjustments for some categories have been made as requested by some Member States to take partially into account the Belarussian demand.
It is to be noted that in absence of an agreement, Belarus would be free to increase its tariffs or introduce other import restrictions.
The Council is invited to approve on behalf of the Community this proposal for a Council Decision on the provisional application of the Agreement in the form of an Exchange of Letters amending the existing Agreement between the European Community and the Republic of Belarus on trade in textile and clothing products pending the formal conclusion of that Agreement on behalf of the Community.
2004/0283 (ACC)
Proposal for a
COUNCIL DECISION
on the signing and the provisional application of a bilateral agreement between the European Community and the Republic of Belarus on trade in textile products
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 133 in conjunction with Article 300, paragraph 2, first sentence thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) The Commission has negotiated on behalf of the Community a bilateral agreement to extend for one year the existing bilateral agreement and protocols on trade in textile products with the Republic of Belarus, with quantitative limits adjusted to take into account annual growth rates and Belarussian demand for some catagories;
(2) Subject to its possible conclusion at a later date, the Agreement should be signed on behalf of the Community;
(3) This bilateral agreement should be applied on a provisional basis as of 1st January 2005, pending the completion of procedures required for its conclusion, subject to the reciprocal provisional application by the Republic of Belarus.
HAS DECIDED AS FOLLOWS:
Article 1
Subject to a possible conclusion at a later date, the President of the Council is hereby authorised to designate the persons empowered to sign on behalf of the European Community the Agreement on trade in textile products between the European Community and Belarus.
Article 2
The Agreement in the form of an Exchange of Letters amending the Agreement between the European Community and the Republic of Belarus on trade in textile products shall be applied on a provisional basis, pending its formal conclusion and subject to reciprocal provisional application of the Agreement by the Republic of Belarus[1], from 1st January 2005.
The text of the Agreement is annexed to this Decision.
Article 3
1. If Belarus fails to fulfil its obligations under paragraph 2.5 of the 1999 Agreement, the quota for 2005 will be reduced to the levels applicable in 2004[2].
2. The decision to implement paragraph 1 shall be taken in accordance with the procedures referred to in Article 17 of Council Regulation (EEC) No 3030/93 of 12 October 1993 on common rules for imports of certain textile products from third countries.
Article 4
This Decision shall be published in the Official Journal of the European Union . It shall enter into force the day after its publication in the Official Journal.
Done at Brussels,
For the Council
The President
ANNEX
AGREEMENT in the form of an Exchange of Letters between the European Community and the Republic of Belarus amending the Agreement between the European Community and the Republic of Belarus on trade in textile products initialled in Brussels on 1 April 1993, as last amended by an Agreement in the form of an Exchange of Letters initialled on 23 December 2003
Letter from the Council of the European Union
Sir,
1. I have the honour to refer to the Agreement between the European Community and the Republic of Belarus on trade in textile products initialled on 1 April 1993, as last amended and extended by the Agreement in the form of an Exchange of Letters initialled on 23 December 2003 (hereafter referred to as the 'Agreement').
2. In view of the expiry of the Agreement on 31 December 2004 and in accordance with Article 19 (1) of the Agreement, the European Community and the Republic of Belarus agree to extend the duration of the Agreement, for a further period of one year, subject to the following amendments and conditions:
2.1. The text of Article 19, paragraph 1, second and third sentences, of the Agreement shall be replaced by the following:
'It shall be applicable until 31 December 2005.'
2.2. Annex II which sets out the quantitative restrictions for exports from the Republic of Belarus to the European Community is replaced by Appendix 1 to this letter.
2.3. The Annex to Protocol C which sets out the quantitative restrictions for exports from the Republic of Belarus to the European Community after OPT operations in the Republic of Belarus is replaced for the period of 1 January 2005 to 31 December 2005 by Appendix 2 to this letter.
2.4. Imports into Belarus of textile and clothing products of European Community origin shall be subject in 2005 to custom duties not exceeding those provided for 2004 in Appendix 4 of the Agreement in the form of an Exchange of Letters between the European Community and the Republic of Belarus initialled on 11 November 1999.
In the case of non-application of these rates the Community will have the right to reintroduce for the period of the agreement remaining unexpired on a pro rata basis the levels for quantitative restrictions applicable for 2004 as specified in the Exchange of Letters initialled on 23 December 2003.
3. Should the Republic of Belarus become a Member of the World Trade Organisation (WTO) before the date of the expiry of the Agreement, the Agreements and rules of the WTO shall be applied from the date of the Republic of Belarus’ accession to the WTO.
4. I should be obliged if you could kindly confirm the acceptance of your Government of the foregoing. Should this be the case, this Agreement in the form of an Exchange of Letters shall enter into force on the first day of the month following the day on which the Parties have notified each other that the legal procedures necessary to this end have been completed. In the meantime, it shall be applied provisionally from 1 January 2005 on the condition of reciprocity.
Please accept, Sir, the assurance of my highest consideration.
For the Council of the European Union
Appendix 1
Annex II
+++++ TABLE +++++
T pieces: thousand of pieces
Appendix 2
Annex to Protocol C
+++++ TABLE +++++
Letter from the Government of the Republic of Belarus
Sir,
I have the honour to acknowledge receipt of your letter of ….. which reads as follows:
“ Sir,
1. I have the honour to refer to the Agreement between the European Community and the Republic of Belarus on trade in textile products initialled on 1 April 1993, as last amended and extended by the Agreement in the form of an Exchange of Letters initialled on 23 December 2003 (hereafter referred to as the 'Agreement').
2. In view of the expiry of the Agreement on 31 December 2004 and in accordance with Article 19 (1) of the Agreement, the European Community and the Republic of Belarus agree to extend the duration of the Agreement, for a further period of one year, subject to the following amendments and conditions:
2.1. The text of Article 19, paragraph 1, second and third sentences, of the Agreement shall be replaced by the following:'It shall be applicable until 31 December 2005.'
2.2. Annex II which sets out the quantitative restrictions for exports from the Republic of Belarus to the European Community is replaced by Appendix 1 to this letter.
2.3. The Annex to Protocol C which sets out the quantitative restrictions for exports from the Republic of Belarus to the European Community after OPT operations in the Republic of Belarus is replaced for the period of 1 January 2005 to 31 December 2005 by Appendix 2 to this letter.
2.4. Imports into Belarus of textile and clothing products of European Community origin shall be subject in 2005 to custom duties not exceeding those provided for 2004 in Appendix 4 of the Agreement in the form of an Exchange of Letters between the European Community and the Republic of Belarus initialled on 11 November 1999.In the case of non-application of these rates the Community will have the right to reintroduce for the period of the agreement remaining unexpired on a pro rata basis the levels for quantitative restrictions applicable for 2004 as specified in the Exchange of Letters initialled on 23 December 2003.
3. Should the Republic of Belarus become a Member of the World Trade Organisation (WTO) before the date of the expiry of the Agreement, the Agreements and rules of the WTO shall be applied from the date of the Republic of Belarus’ accession to the WTO.
4. I should be obliged if you could kindly confirm the acceptance of your Government of the foregoing. Should this be the case, this Agreement in the form of an Exchange of Letters shall enter into force on the first day of the month following the day on which the Parties have notified each other that the legal procedures necessary to this end have been completed. In the meantime, it shall be applied provisionally from 1 January 2005 on the condition of reciprocity.
Please accept, Sir, the assurance of my highest consideration.”
I have the honour to confirm that my Government is in agreement with the content of your letter.
Please accept, Sir, the assurance of my highest consideration.
For the Government of the Republic of Belarus
[1] The date from which provisional application will become effective will be published in the Official Journal of the European Union, C series.
[2] OJ L 275, 8.11.1993, p. 1. Regulation last amended by Council Regulation (EC) No 138/2003 of 21 January 2003.
