Commission Decision
of 11 May 2005
on the renewal of the mandate of the European Group on Ethics in Science and New Technologies
(Only the French text is authentic)
(2005/383/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Whereas:
(1) In November 1991, the European Commission decided to incorporate ethics into the decision-making process for Community research and technological development policies by setting up the Group of Advisers on the Ethical Implications of Biotechnology (GAEIB).
(2) The Commission decided on 16 December 1997 to replace the GAEIB by the European Group on Ethics in Science and New Technologies (EGE) extending the Group’s mandate to cover all areas of the application of science and technology.
(3) The EGE’s mandate was renewed by the Commission Decision of 26 March 2001 for a four-year period and its remit was slightly modified to improve the Group’s working methods.
(4) The EGE requires new working methods in order to respond to more rapid science and technology developments in a timely manner and requires new competences in order to address a greater range of science and technology applications.
(5) The Communication from the Commission on the collection and use of expert advice by the Commission: principles and guidelines (COM(2002) 713), states that "open calls may be particularly appropriate when dealing with sensitive issues and when groups are liable to stand for a reasonable period of time".
(6) The current EGE remit comes to an end on 25 March 2005 and the following decision shall therefore replace the remit annexed to the Communication to the Commission of 26 March 2001 (C(2001) 691),
HAS DECIDED AS FOLLOWS:
Article 1
The Commission hereby decides to renew the mandate of the European Group on Ethics in Science and New Technologies (EGE) for a four-year period.
Article 2
Mission
The task of the EGE shall be to advise the Commission on ethical questions relating to sciences and new technologies, either at the request of the Commission or on its own initiative. The Parliament and the Council may draw the Commission’s attention to questions which they consider to be of major ethical importance. The Commission shall, when seeking the opinion of the EGE, set a time limit within which such an opinion shall be given.
Article 3
Composition — Nomination — Appointment
1. The EGE members are appointed by the President of the Commission.
2. The following rules will apply:
- Members are nominated ad personam. Members serve in a personal capacity and are asked to advise the Commission independently from any outside influence. The EGE shall be independent, pluralist and multidisciplinary.
- The EGE shall have up to 15 members.
- Each member of the EGE shall be appointed for a term of four years. Such appointment may be renewable for a maximum of two further terms.
- Members who are no longer capable of contributing efficiently to the work of the group, or who resign, may be replaced by an alternative member, in accordance with Article 3(1), on the basis of a reserve list, for the remaining duration of their mandate.
- The identification and selection of the EGE members will be made on the basis of an open call for expressions of interest. Additional applications received through other channels will also be taken into consideration in the selection procedure.
- The list of EGE members shall be published by the Commission in the Official Journal of the European Union.
Article 4
Functioning
1. The EGE members shall elect a chairperson and a vice-chairperson from among its members for the duration of the term of office.
2. The EGE work programme shall be agreed by the President of the Commission (including ethical reviews suggested by the EGE under their right of self initiative — see Article 2). The Bureau of European Policy Advisers (BEPA) of the Commission, acting in close cooperation with the EGE’s chairperson, shall be responsible for organising the work of the EGE and its Secretariat.
3. The EGE’s working sessions shall be private. Outside these working sessions the EGE may discuss its work with concerned Commission departments and may invite representatives of NGOs or representative organisations when appropriate for an exchange of views. The agenda for the EGE meetings will be distributed to relevant Commission services.
4. The EGE will normally meet at the Commission’s seat according to the modalities and the calendar fixed by the Commission. The EGE should meet at least six times during a 12-month period involving around 12 working days a year. Members are expected to attend a minimum of four meetings a year.
5. For the purposes of preparing its opinions and within the limits of the available resources for this action, the EGE:
- may invite experts having a specific competence, to guide and inform the work of the EGE if this is deemed useful and/or necessary,
- may initiate studies in order to collect all necessary scientific and technical information,
- may set up working groups to consider specific issues,
- will organize a public round table in order to promote dialogue and improve transparency for each opinion that it produces,
- will establish close links with the Commission departments involved in the topic the Group is working on,
- may establish closer links with representatives of the various ethics bodies in the European Union and in the applicant countries.
6. Every opinion shall be published immediately after its adoption. Where an opinion is not adopted unanimously, it shall include any dissenting point of view. Where there is an operational requirement for advice to be given more quickly on a particular subject, short statements will be produced, to be followed if necessary by a fuller analysis, while ensuring that transparency is respected as for any other opinion. EGE opinions always refer to the state of the art of the technology at the time the opinion is issued. The EGE may decide to update opinions if it deems it necessary.
7. The EGE shall adopt its own Rules of Procedure.
8. A report on the EGE’s activities shall be produced under the responsibility of the chairperson before the end of its term of office. The report shall be published.
Article 5
Meeting expenses
Travel and subsistence expenses for the meetings of the EGE shall be covered by the Commission according to Commission rules.
Article 6
Entry into force
The present decision will be published in the Official Journal of the European Union and shall enter into force on the day of the nomination of the new EGE members.
Done at Brussels, 11 May 2005.
For the Commission
José Manuel Barroso
The President
--------------------------------------------------
