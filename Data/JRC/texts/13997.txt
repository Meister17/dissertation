Order of the President of the Court of First Instance of 25 April 2006 — Componenta
v Commission
(Case T-455/05 R)
Parties
Applicant: Componenta Oyj (Helsinki, Finland) (represented by: M. Savola, lawyer)
Defendant: Commission of the European Communities (represented by: C. Giolito and M. Huttunen, Agents)
Intervener in support of the applicant: Republic of Finland (represented by: E. Bygglin, Agent)
Re:
Application for suspension of operation of Commission Decision C(2005) 3871 final of 20 October 2005 on State aid C 37/2004 (ex NN 51/2004) granted by the Republic of Finland to Componenta Ojy
Operative part of the order
1. The application for interim measures is dismissed.
2. The costs are reserved.
--------------------------------------------------
