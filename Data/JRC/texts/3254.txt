[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 19.05.2005
SEC(2005) 645 final
.
Draft
DECISION OF THE EEA JOINT COMMITTEE
amending Annex IV (Energy) to the EEA Agreement
.
- Draft common position of the Community -(presented by the Commission)
EXPLANATORY MEMORANDUM
1. In order to ensure the requisite legal security and homogeneity of the Internal Market, the EEA Joint Committee is to integrate all the relevant Community legislation into the EEA Agreement as soon as possible after its adoption.
2. The EEA Joint Committee should therefore adopt the attached decision to amend Annex IV to the EEA Agreement by adding new Community acquis in the field of energy. The decision concerns the following directive:
- 32001 L 0077 : Directive 2001/77/EC of the European Parliament and of the Council of 27 September 2001on the promotion of electricity produced from renewable energy sources in the internal electricity market (OJ L 283, 27.10.2001, p. 33).
3. In the draft Joint Committee Decision, reference values for the contribution of electricity produced from renewable energy sources to gross electricity consumption by 2010 are introduced for Norway and Iceland, as well as certain conditions related to these figures. The draft decision also contains a derogation for Liechtenstein, not least because the country imports about 75% of its electricity from Switzerland.
4. Article 1(3)(a) of Council Regulation (EC) No 2894/94 concerning the arrangements for implementing the EEA Agreement envisages that the Council establishes the Community position for decisions extending Community legislation with substantial changes.
5. The draft Decision of the EEA Joint Committee is submitted for the approval of the Council. The Commission aims at putting forward the position of the Community in the EEA Joint Committee as soon as possible after the adoption of the Council.
Draft
DECISION OF THE EEA JOINT COMMITTEE
amending Annex IV (Energy) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as ‘the Agreement’, and in particular Article 98 thereof,
Whereas:
(1) Annex IV to the Agreement was amended by Decision of the EEA Joint Committee No ... of …[1].
(2) Directive 2001/77/EC of the European Parliament and of the Council of 27 September 2001 on the promotion of electricity produced from renewable energy sources in the internal electricity market[2] is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
The following point shall be inserted after point 18 (Regulation (EC) No 2422/2001 of the European Parliament and of the Council) in Annex IV to the Agreement:
‘19. 32001 L 0077 : Directive 2001/77/EC of the European Parliament and of the Council of 27 September 2001 on the promotion of electricity produced from renewable energy sources in the internal electricity market (OJ L 283, 27.10.2001, p. 33).
The provisions of the Directive shall, for the purposes of the present Agreement, be read with the following adaptations:
(a) This Directive shall not apply to Liechtenstein.
(b) in Article 3(2), the date “27 October 2002" and in Articles 3(3), 5(1) and 6(2), the date “27 October 2003” shall read “six months after the entry into force of Decision of the EEA Joint Committee No … of …”;
(c) in Article 3(2), second indent, the word “Community” shall be replaced by “EFTA States”;
(d) paragraph 8 of Protocol 1 shall not apply to Article 3 (4), second indent;
(e) in Article 4(1) the words “Articles 87 and 88 of the Treaty” shall read “Articles 61 and 62 of the EEA Agreement”. The words “Articles 6 and 174 of the Treaty” shall read “Article 73 of the EEA Agreement”;
(f) in the Annex, the following shall be added:
Iceland | 5,58 | 99,90 | 99,50 |
Norway | 110,95 | 96,30 | 90,00 |
(g) The figure for Iceland is contingent upon there being no changes in interconnectivity with other electricity systems. Furthermore, due to the importance of climatic factors on the production of electricity from hydro resources, the demand for electricity and breakdowns in transmission the figure for 2010 should be calculated on a long-range model based on hydrologic and climatic conditions.
(h) The ability of Norway to reach its target of 90 percent is contingent upon electricity consumption not increasing by more than 1 percent annually. This corresponds with approximately 6 - 7 TWh new production capacity of electricity from renewable energy sources being introduced from 1997 to 2010.
The considerable variations in Norwegian hydropower production might make it necessary for Norway to take into account the average hydropower production potential in the reports on the fulfilment of the indicative target.’
Article 2
The text of Directive 2001/77/EC in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union , shall be authentic.
Article 3
This Decision shall enter into force on , provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee*.
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union .
Done at Brussels,
For the EEA Joint Committee
The President
The Secretaries to the EEA Joint Committee
[1] OJ C […], […], p. […].
[2] OJ L 283, 27.10.2001, p. 33.
* [No constitutional requirements indicated.] [Constitutional requirements indicated.]
