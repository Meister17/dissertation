COMMISSION DECISION of 26 November 1997 concerning certain protective measures with regard to certain fishery products originating in China and amending Decision 97/368/EC (Text with EEA relevance) (97/805/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 90/675/EEC of 10 December 1990 laying down the principles governing the organization of veterinary checks on products entering the Community from third countries (1), as last amended by Directive 96/43/EC (2), and in particular Article 19,
Whereas, upon importation of fishery products originating in several processing establishments in China, the presence of Vibrio parahaemolyticus has been detected;
Whereas the presence of Vibrio parahaemolyticus on food is a result of bad hygienic practices before and/or after processing of food and it presents a potential risk for human health;
Whereas imports of products from the establishments concerned in China must not therefore be further allowed;
Whereas Community inspections in China and the results of checks at the Community border inspection posts have shown that potential health risks with regard to the production and processing of fishery products exist;
Whereas Commission Decision 97/368/EC (3), as amended by Decision 97/620/EC (4), concerning certain protective measures with regard to certain fishery products originating in China, provides for a ban on importation of fresh fishery products originating in China and for a requirement that frozen or processed fishery products originating in China must be systematically submitted to a microbiological examination;
Whereas Decision 97/368/EC should be reviewed before 28 February 1998 and on the ground of the current findings it should be necessary to extend the measures provided in this Decision until 30 June 1998;
Whereas the measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
This Decision shall apply to fishery products, fresh, frozen or processed, originating in China.
Article 2
Member States shall ban the imports of fishery products, in all forms, originating in the following establishments in China: Pingyang freezing multiple-producing factory - No 60 Jiangkou Road Aojiang Pingyang, Zhejiang (plant Code No 3300/02097) and Wenzhou Hongdali aquatic products processing Co. Ltda Yanting cold storage plant Yanting, Cangnan - Zhejiang (plant Code No 3300/02083).
Article 3
In Article 6 of Decision 97/368/EC the date '28 February 1998` shall be replaced by '30 June 1998`.
Article 4
The Member States shall amend the measures they apply in respect of imports from China to bring them into line with this Decision. They shall immediately inform the Commission thereof.
Article 5
All expenditure incurred by the application of this Decision shall be chargeable to the consignor, the consignee or their agent.
Article 6
This Decision is addressed to the Member States.
Done at Brussels, 26 November 1997.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ L 373, 31. 12. 1990, p. 1.
(2) OJ L 162, 1. 7. 1996, p. 1.
(3) OJ L 156, 13. 6. 1997, p. 57.
(4) OJ L 254, 17. 9. 1997, p. 17.
