Decision of the EEA Joint Committee
No 123/2006
of 22 September 2006
amending Annex XIII (Transport) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex XIII to the Agreement was amended by Decision of the EEA Joint Committee No 90/2006 of 7 July 2006 [1].
(2) Directive 2006/23/EC of the European Parliament and of the Council of 5 April 2006 on a Community air traffic controller licence [2] is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
The following point shall be inserted after point 66y (Commission Regulation (EC) No 2150/2005) of Annex XIII to the Agreement:
"66z. 32006 L 0023: Directive 2006/23/EC of the European Parliament and of the Council of 5 April 2006 on a Community air traffic controller licence (OJ L 114, 27.4.2006, p. 22)."
Article 2
The texts of Directive 2006/23/EC in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 23 September 2006, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [3].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 22 September 2006.
For the EEA Joint Committee
The President
Oda Helen Sletnes
[1] OJ L 289, 19.10.2006, p. 29.
[2] OJ L 114, 27.4.2006, p. 22.
[3] No constitutional requirements indicated.
--------------------------------------------------
