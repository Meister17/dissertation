COMMISSION REGULATION (EEC) No 1740/78 of 25 July 1978 amending Regulation (EEC) No 1579/74 on the procedure for calculating the import levy on products processed from cereals and from rice and for the advance fixing of this levy for these products and for compound feedingstuffs manufactured from cereals
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 2727/75 of 29 October 1975 on the common organization of the market in cereals (1), as last amended by Regulation (EEC) No 1254/78 (2), and in particular Article 24 thereof,
Whereas in view of the need to have available more up to date information on imports of certain products, it is necessary to provide for more frequent returns by Member States ; whereas Regulation (EEC) No 1579/74 of 24 June 1974 (3) should therefore be amended accordingly;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Cereals,
HAS ADOPTED THIS REGULATION:
Article 1
In Article 5 of Regulation (EEC) No 1579/74, the following paragraph is hereby added:
"However, in respect of products falling under subheadings 07.06 A, 23.02 A I and 23.02 A II of the Common Customs Tariff, these particulars shall be communicated to the Commission daily."
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 25 July 1978.
For the Commission
Finn GUNDELACH
Vice-President (1)OJ No L 281, 1.11.1975, p. 1. (2)OJ No L 156, 14.6.1978, p. 1. (3)OJ No L 168, 25.6.1974, p. 7.
