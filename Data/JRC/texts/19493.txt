Commission Regulation (EC) No 2314/2003
of 29 December 2003
opening a standing invitation to tender for the resale on the Community market of rye held by the German intervention agency
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 1766/92 of 30 June 1992 on the common organisation of the market in cereals(1), and in particular Article 5 thereof,
Whereas:
(1) Commission Regulation (EEC) No 2131/93 of 28 July 1993 laying down the procedure and conditions for the sale of cereals held by intervention agencies(2) provides in particular that cereals held by intervention agencies are to be sold by tendering procedure at prices preventing market disturbance.
(2) Germany still has intervention stocks of rye.
(3) Because of the difficult weather conditions in much of the Community, cereal production has been significantly reduced in the 2003/04 marketing year. This situation has resulted in high prices locally, causing particular difficulties for livestock holdings and the feedingstuffs industry, which are finding it hard to obtain supplies at competitive prices.
(4) It is therefore appropriate to make stocks of rye held by the German intervention agency available on the internal market. The period of presentation of the offers for the last partial invitation to tender under Regulation (EC) No 1510/2003(3) having expired on 18 December 2003, it is advisable to open a new standing invitation to tender.
(5) To take account of the situation on the Community market, provision should be made for the Commission to manage this invitation to tender. In addition, provision must be made for an award coefficient for tenders offering the minimum selling price.
(6) When the German intervention agency notifies the Commission, the tenderers should remain anonymous.
(7) With a view to modernising management, the information required by the Commission should be sent by electronic mail.
(8) The Management Committee for Cereals has not issued an opinion by the time limit laid down by its Chairman,
HAS ADOPTED THIS REGULATION:
Article 1
The German intervention agency shall open a standing invitation to tender for the sale on the Community market of 1139000 tonnes of rye held by it.
Article 2
The sale provided for in Article 1 shall take place in accordance with Regulation (EEC) No 2131/93.
However, notwithstanding that Regulation:
(a) tenders shall be drawn up on the basis of the actual quality of the lot to which they apply;
(b) the minimum selling price shall be set at a level which does not disturb the cereals market.
Article 3
Notwithstanding Article 13(4) of Regulation (EEC) No 2131/93 the tender security is set at EUR 10 per tonne.
Article 4
1. The closing date for the submission of tenders for the first partial invitation to tender shall be 8 January 2004 at 09.00 (Brussels time).
The closing dates for the submission of tenders for subsequent partial invitations to tender shall be each Thursday at 09.00 (Brussels time), with the exception of 8 April and 20 May 2004.
The closing date for the submission of tenders for the last partial tendering procedure shall be 27 May 2004 at 09.00 (Brussels time).
2. Tenders shall be lodged with the German intervention agency: Bundesanstalt für Landwirtschaft und Ernährung (BLE) Adickesallee 40 D - 60322 Frankfurt am Main Fax: (49-691) 56 49 62
Article 5
Within two hours of the expiry of the time limit for the submission of tenders, the German intervention agency shall notify the Commission of tenders received. They must be sent by electronic mail in accordance with the form set out in the Annex.
Article 6
In accordance with the procedure laid down in Article 23 of Regulation (EEC) No 1766/92, the Commission shall set the minimum sale price or decide not to accept the tenders. If tenders are submitted for the same lot and for a total quantity larger than that available, a separate price may be fixed for each lot.
Where tenders offer the minimum selling price, the Commission may fix an award coefficient for the quantities offered at the same time as it sets the minimum selling price.
Article 7
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 29 December 2003.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 181, 1.7.1992, p. 21. Regulation as last amended by Regulation (EC) No 1104/2003 (OJ L 158, 27.6.2003, p. 1).
(2) OJ L 191, 31.7.1993, p. 76. Regulation as last amended by Regulation (EC) No 1630/2000 (OJ L 187, 26.7.2000, p. 24).
(3) OJ L 217, 29.8.2003, p. 11. Regulation as last amended by Regulation (EC) No 2110/2003 (OJ L 317, 2.12.2003, p. 3).
ANNEX
Standing invitation to tender for the resale of 1139000 tonnes of rye held by the German intervention agency
Regulation (EC) No 2314/2003
>PIC FILE= "L_2003342EN.003302.TIF">
