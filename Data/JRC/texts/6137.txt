Council Decision 2005/191/CFSP
of 18 October 2004
concerning the conclusion of agreements between the European Union and the Republic of Iceland, the Kingdom of Norway and Romania establishing a framework for the participation of the Republic of Iceland, the Kingdom of Norway and Romania in the European Union crisis-management operations
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty on European Union, and in particular Article 24 thereof,
Having regard to the recommendation from the Presidency,
Whereas:
(1) Conditions regarding the participation of third States in EU crisis-management operations should be laid down in an Agreement establishing a framework for such possible future participation, rather than defining these conditions on a case-by-case basis for each operation concerned.
(2) Following authorisation by the Council on 23 February 2004, the Presidency, assisted by the Secretary-General/High Representative, negotiated Agreements between the European Union and the Republic of Iceland, the Kingdom of Norway and Romania establishing a framework for the participation of the Republic of Iceland, the Kingdom of Norway and Romania in the European Union crisis-management operations.
(3) The Agreements should be approved,
HAS DECIDED AS FOLLOWS:
Article 1
The Agreements between the European Union and the Republic of Iceland, the Kingdom of Norway and Romania establishing a framework for the participation of the Republic of Iceland, the Kingdom of Norway and Romania in the European Union crisis-management operations are hereby approved on behalf of the European Union.
The texts of the Agreements are attached to this Decision.
Article 2
The President of the Council is hereby authorised to designate the person(s) empowered to sign the Agreements in order to bind the European Union.
Article 3
This Decision shall take effect on the day of its adoption.
Article 4
This Decision shall be published in the Official Journal of the European Union.
Done at Luxembourg, 18 October 2004.
For the Council
The President
C. Veerman
--------------------------------------------------
