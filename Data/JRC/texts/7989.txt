*****
COMMISSION REGULATION ( EEC ) No 3444/90
of 27 November 1990
laying down detailed rules for granting private storage aid for pigmeat
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation ( EEC ) No 2759/75 of 29 October 1975 on the common organization of the market in pigmeat ( 1 ), as last amended by Regulation ( EEC ) No 1249/89 ( 2 ), and in particular Articles 5(4 ) and 7(2 ) and the second subparagraph of Article 22 thereof,
Having regard to Council Regulation ( EEC ) No 1676/85 of 11 June 1985 on the rates of exchange to be applied in agriculture ( 3 ), as last amended by Regulation ( EEC ) No 2205/90 ( 4 ), and in particular Articles 5(3 ) and 12 thereof,
Whereas detailed rules for granting private storage aid for pigmeat must be adopted in addition to the general rules laid down by Council Regulation ( EEC ) No 2763/75 ( 5 );
Whereas, if it is to achieve its purpose, such aid should be granted only to natural or legal persons established in the Community whose activities and experience in the sector offer sufficient certainty that storage will be effected in a satisfactory manner and who have adequate cold storage capacity within the Community;
Whereas, for the same reason, aid should be granted only for the storage of products in frozen condition, of sound and fair merchantable quality and of Community origin as defined by Commission Regulation ( EEC ) No 964/71 ( 6 ), with a level of radioactivity not exceeding the maximum levels permitted under Council Regulation ( EEC ) No 737/90 of 22 March 1990 on the conditions governing imports of agricultural products originating in third countries following the accident at the Chernobyl nuclear power station ( 7 );
Whereas the market situation and its future development could make it opportune to invite the contracting party to designate his stocks for export from the moment of placing in storage and that it is appropriate under this hypothesis to determine the conditions under which the meat which is the object of a storage contract could be simultaneously placed under the regime as referred to in Article 5 of Council Regulation ( EEC ) No 565/80 of 4 March 1980 on the advance payment of export refunds in respect of agricultural products ( 8 ), as amended by Regulation ( EEC ) No 2026/83 ( 9 ), in order to benefit from advance payment of export refunds;
Whereas, to make the scheme more effective, contracts must relate to a certain minimum quantity, differentiated by product, as appropriate and the obligations to be fulfilled by the contracting party, in particular those enabling the intervention agency to make an effective inspection of storage conditions, must be specified;
Whereas the amount of the security designed to ensure compliance with the contractual obligations should be fixed at a percentage of the amount of the aid;
Whereas, pursuant to Commission Regulation ( EEC ) No 2220/85 of 22 July 1985 laying down common detailed rules for the application of the system of securities for agricultural products ( 10 ), as last amended by Regulation ( EEC ) No 3745/89 ( 11 ), the primary requirements to be met for the release of the security should be defined; whereas storing the contracted quantity for the agreed period constitutes one of the primary requirements for the granting of private storage aid for pigmeat; whereas, to take account of commercial practice and for practical reasons, a certain tolerance in respect of the said quantity should be permitted;
Whereas a measure of proportionality should apply in the release of the security and the granting of aid where certain requirements relating to the quantities to be stored are not met;
Whereas, in order to improve the efficiency of the scheme, the contracting party should be enabled to receive an advance payment of aid subject to a security
and rules should be laid down regarding the submission of applications for aid, the supporting documents to be produced and the time limit for payment;
Whereas, pursuant to Article 5 of Regulation ( EEC ) No 1676/85, it should be specified that, in the case of private storage aid, the operative event to determine the amount of the security and the aid in national currency is the day of conclusion of the storage contract or the final day for submission of tenders in response to an invitation to tender;
Whereas previous experience with other schemes for private storage of agricultural products has shown the need to specify to what extent Council Regulation ( EEC, Euratom ) No 1182/71 ( 1 ) applies to the determination of periods, dates and time limits referred to under such schemes and to define precisely the dates when contractual storage begins and ends;
Whereas, in particular, Article 3(4 ) of Regulation ( EEC, Euratom ) No 1182/71 specifies that where the last day of a period is a public holiday, Sunday or Saturday, the period should end on the expiry of the last hour of the following working day; whereas the application of this provision to storage contracts may not be in the interest of storers and may even result in inequality of treatment; whereas, therefore, a derogation should be made with regard to the determination of the last day of storage under contract;
Whereas provision should be made for a measure of proportionality in the granting of aid where the storage period is not fully observed; whereas provision should also be made for the storage period to be shortened where meat removed from storage is intended for export; whereas proof that the meat has been exported must be supplied, as in the case of refunds, in accordance with Commission Regulation ( EEC ) No 3665/87 of 27 November 1987 laying down common detailed rules for the application of the system of export refunds on agricultural products ( 2 ), as last amended by Regulation ( EEC ) No 1615/90 ( 3 );
Whereas Article 4 ( 1 ) of Regulation ( EEC ) No 2763/75 provides that the amount of aid for private storage may be determined by means of a tendering procedure; whereas Articles 4 and 5 of the same Regulation contain rules to be observed in connection with this procedure; whereas, however, more detailed rules are necessary for that purpose;
Whereas the purpose of the tendering procedure is to determine the amount of the aid; whereas, in the selection of successful tenderers, preference should be given to those most advantageous for the Community; whereas, therefore, a maximum amount of aid may be set for which tenders would be accepted; whereas, if no tender is acceptable, none need be accepted;
Whereas provision should be made for a system of checks to ensure that aid is not granted unduly; whereas for this purpose it is appropriate that the Member States make checks appropriate to the various stages of storage;
Whereas there is reason to prevent and to sanction against irregularities and fraud; whereas for this purpose, it is appropriate to exclude in the case of false declaration the contracting party from the granting of aids for private storage for the calendar year following that of the constitution of a false declaration;
Whereas, to give the Commission an overall view of the effect of the private storage scheme, the Member States should supply it with the necessary information;
Whereas Commission Regulation ( EEC ) No 1092/80 of 2 May 1980 laying down detailed rules for granting private storage aid for pigmeat ( 4 ), as last amended by Regulation ( EEC ) No 3498/88 ( 5 ), has been amended substantially; whereas, now that further amendments are to be made, the relevant legislation should be entirely recast; whereas, however, the new provisions should apply only to contracts concluded after the entry into force of this Regulation;
Whereas the Management
Committee for Pigmeat has not delivered an opinion within the time limit set by its chairman,
HAS ADOPTED THIS REGULATION :
Article 1
The grant of private storage aid, as provided for in Article 3 of Regulation ( EEC ) No 2759/75, shall be subject to the conditions set out in this Regulation .
TITLE I
GENERAL PROVISIONS
Article 2
1 . Contracts for the private storage of pigmeat shall be concluded between the intervention agencies of the Member States and natural or legal persons, hereafter referred to as the "contracting party ':
_ carrying on business in the meat and livestock sectors since at least twelve months and officially registered in a Member State,
and
_ having suitable storage facilities at their disposal within the Community .
2 . Private storage aid may be granted only for fresh meat of sound and fair merchantable quality coming from animals raised in the Community since at least the last two months and slaughtered not more than ten days before the date on which the products are placed in storage as referred to in Article 4 ( 2 ).
3 . No meat with a radioactive content exceeding the maximum levels permitted under Community regulations may be the subject of a storage contract . The levels applicable to products of Community origin shall be as fixed in Article 3 of Regulation ( EEC ) No 737/90 . The level of radioactive contamination of the product shall be checked only if the situation so requires and for the necessary period . Where necessary, the duration and scope of the checks shall be determined in accordance with the procedure laid down in Article 24 of Regulation ( EEC ) No 2759/75 .
4 . Contracts may not relate to less than a minimum quantity to be determined for each product .
5 . The meat must be placed in storage in fresh state and stored in frozen state .
Article 3
1 . Contract applications or tenders, and contracts shall relate to only one of the products for which aid may be granted .
2 . Contract applications or tenders shall not be acceptable unless they include the particulars referred to in paragraph 3 ( a ), ( b ), ( d ) and ( e ), and proof has been furnished that a security has been provided .
3 . Contracts shall include the following particulars :
( a ) a declaration by which the contracting party engages himself to place in storage and to stock products which fulfill the conditions as referred to in Article 2 ( 2 ) and ( 3 );
( b ) the description and the quantity of the product to be stored;
( c ) the time limit for placing in storage, referred to in Article 4 ( 3 ), of the total quantity referred to under ( b );
( d ) the duration of storage;
( e ) the amount of the aid per unit of weight;
( f ) the amount of the security;
( g ) a provision enabling the storage period to be shortened or extended uner the conditions laid down in Community regulations .
4 . Contracts shall impose at least the following obligations on the contracting party :
( a ) to place the agreed quantity of product in storage within the time limits laid down in Article 4 and to store it at his own risk and expense in conditions ensuring the maintenance of the characteristics of the products referred to in Article 2 ( 2 ) for the contractual period, without altering, substituting or transferring to another warehouse the stored products; however, in exceptional cases and on duly motivated request the intervention agency may authorize a relocation of the stored products;
( b ) to advise the intervention agency with which he has concluded the contract, in due time before the entry into storage of each individual lot, within the meaning of the second subparagraph of Article 4 ( 1 ), of the date and place of storage as well as the nature and quantity of the product to be stored; the intervention agency may require that this information is given at least two working days before the placing in storage of each individual lot;
( c ) to send to the intervention agency the documents relating to the operations for placing in storage not later than 1 month after the date referred to in Article 4 ( 3 );
( d ) to store the products in accordance with the requirements for identification referred to in Article 13 ( 4 );
( e ) to permit the agency to check at any time that all the obligations laid d
in the contract are being observed .
Article 4
1 . Placing in storage must be completed not later than 28 days after the date of conclusion of the contract .
The products may be placed in storage in individual lots, each lot representing the quantity placed in storage on a given day by contract and by warehouse .
2 . Placing in storage begins, for each individual lot of the contractual quantity, on the day on which it comes under the control of the intervention agency .
( 1 ) OJ No L 282, 1 . 11 . 1975, p . 1 .
( 2 ) OJ No L 129, 11 . 5 . 1989, p . 12 .
( 3 ) OJ No L 164, 29 . 6 . 1985, p . 1 .
( 4 ) OJ No L 201, 31 . 7 . 1990, p . 9 .
( 5 ) OJ No L 282, 1 . 11 . 1975, p . 19 .
( 6 ) OJ No L 104, 11 . 5 . 1971, p . 12 .
( 7 ) OJ No L 82, 29 . 3 . 1990, p . 1 .
( 8 ) OJ No L 62, 7 . 3 . 1980, p . 5 .
( 9 ) OJ No L 199, 22 . 7 . 1983, p . 12 .
( 10 ) OJ No L 205, 3 . 8 . 1985, p . 5 .
( 11 ) OJ No L 364, 14 . 12 . 1989, p . 54 .
( 1 ) OJ No L 124, 8 . 6 . 1971, p . 1 .
( 2 ) OJ No L 351, 14 . 12 . 1987, p . 1 .
( 3 ) OJ No L 152, 16 . 6 . 1990, p . 33 .
( 4 ) OJ No L 114, 3 . 5 . 1980, p . 22 .
( 5 ) OJ No L 306, 11 . 11 . 1988, p . 32 .
That day shall be the day on which the net weight of the fresh or chilled product is determined,
_ at the place of storage, where the meat is frozen on the premises,
_ at the place of freezing, where the meat is frozen in suitable facilities outside the place of storage .
However, in the case of boneless products placed in storage, weighing may be done at the place of boning .
The determination of weights of products to be placed in storage shall not take place before the conclusion of a contract .
3 . Placing in storage ends on the day on which the last lot of the contractual quantity is placed in storage .
That day shall be the day on which all the products under contract have entered the place of final storage, whether fresh or frozen .
4 . When the products placed in storage are put under the regime laid down in Article 5 ( 1 ) of Regulation ( EEC ) No 565/80 :
_ notwithstanding Article 28 ( 5 ) of Regulation ( EEC ) No 3665/87, the time limit provided for by that provision is increased to cover the duration of the maximum period under contractual storage, increased by one month;
_ Member States may require that the placing in storage and the placing under the regime as referred to in Article 5 ( 1 ) of Regulation ( EEC ) No 565/80 shall commence simultaneously . In this case when a contract for private storage is concluded for a quantity which consists of several lots which are placed in storage on different dates, each of the said lots may be the subject of a separate payment declaration . The payment declaration as referred to in Article 25 of Regulation ( EEC ) No 3665/87 shall be submitted for each lot on the day of its entry in storage .
Article 5
1 . The amount of the security referred to in Article 3 ( 2 ) shall not exceed 30 % of the amount of aid applied for .
2 . The primary requirements within the meaning of Article 20 ( 2 ) of Regulation ( EEC ) No 2220/85 shall be :
_ not to withdraw a contract application or a tender;
_ to keep in storage at least 90 % of the contractual quantity for the contractual storage period, at the contracting party's own risk and under the conditions referred to in Article 3 ( 4 ) ( a ), and
_ where Article 9 ( 4 ) applies, to export the meat in accordance with one of the three possibilities listed there .
3 . Subject to Article 9 ( 4 ) of this Regulation, Article 27 ( 1 ) of Regulation ( EEC ) No 2220/85 shall not apply .
4 . Securities shall be released immediately where contract applications or tenders are not accepted .
5 . Where the time limit for placing in storage as referred to in Article 4 ( 1 ) is exceeded by 10 days, the contract shall be annulled and the security shall be forfeit in accordance with Article 23 of Regulation ( EEC ) No 2220/85 .
Article 6
1 . The aid shall be fixed per unit of weight and shall relate to the weight determined in accordance with Article 4 ( 2 ).
2 . Subject to paragraph 3 hereafter and Article 9 ( 4 ), contracting parties shall be entitled to aid if the primary requirements referred to in Article 5 ( 2 ) are met .
3 . Aid shall be paid at most for the contractual quantity provided .
If the quantity actually stored during the contractual storage period is less than the contractual quantity and :
( a ) not less than 90 % of that quantity, the aid shall be reduced proportionately;
( b ) less than 90 % but not less than 80 % of that quantity, the aid for the quantity actually stored shall be reduced by half;
( c ) less than 80 % of that quantity, the aid shall not be paid .
4 . After three months of storage under contract, a single advance payment may be made, at the contracting party's request, provided that he lodges a security equal to the
advance payment plus 20 %.
The advance payment shall not exceed the amount of aid corresponding to a storage period of three months . Where products under contract are exported in accordance with Article 9 ( 4 ) prior to the advance payment, the actual storage period for those products shall be taken into account when calculating the amount of advance payment .
Article 7
1 . Except in cases of force majeure the application for payment of the aid and the supporting documents must be lodged with the competent authority within six months following the end of the maximum period of contractual storage . Where the supporting documents could not be produced within the stipulated time limit although the contracting party acted promptly to obtain them on time, additional time limits, which may not exceed a total of six months, may be granted for their production . Where Article 9 ( 4 ) is applied, the necessary
proof must be produced within the time limits specified in Article 47 ( 2 ), ( 4 ), ( 6 ) and ( 7 ) of Regulation ( EEC ) No 3665/87 .
2 . Subject to the cases of force majeure referred to in Article 10 and in cases where an inquiry was opened into entitlement to the aid, the aid shall be paid by the competent authority as soon as possible and not later than three months from the day of deposit of an application for payment, with the required supporting documents, by the contracting party .
Article 8
The conversion rate to be applied to the amounts of aid and securities shall be the agricultural conversion rate in force, in cases of aid fixed at a flat rate in advance, on the day a contract is concluded or, in the case of aid granted by tender, on the last day for the submission of tenders .
Article 9
1 . The periods, dates and time limits referred to in this Regulation shall be determined in accordance with Regulation ( EEC, Euratom ) No 1182/71 . However, Article 3 ( 4 ) of that Regulation shall not apply to the determination of the storage period as referred to in Article 3 ( 3 ) ( d ) of this Regulation or as amended under Article 3 ( 3 ) ( g ) or under paragraph 4 of this Article .
2 . The first day of the contractual storage period shall be the first day following that on which placing in storage was completed .
3 . Removal from storage may commence on the day following the last day of the contractual storage period .
4 . On the expiry of a storage period of two months, the contracting party may remove from storage all or part of the quantity of products under a given contract, subject to a minimum of 5 tonnes per contracting party and per warehouse or, if less than this quantity is available, the total quantity under contract in a warehouse, provided that, within 60 days following their removal from storage, the products have :
_ left the Community's customs territory without further processing,
_ reached their destination without further processing, in the cases referred to in Article 34 ( 1 ) of Regulation ( EEC ) No 3665/87,
or
_ been placed without further processing in a victualling warehouse approved pursuant to Article 38 of Regulation ( EEC ) No 3665/87 .
The contractual storage period shall end for each individual lot intended for export on the day before
_ the day of removal from storage,
or
_ the day of acceptance of the export declaration, where a product has not been moved .
The amount of aid shall be reduced in proportion to the reduction in the storage period according to the amounts fixed in accordance with Article 3 of Regulation ( EEC ) No 2763/75 .
For the purposes of this paragraph, proof of export shall be furnished in accordance with Article 4 of Regulation ( EEC ) No 3665/87 .
5 . Where paragraphes 3 and 4 of this Article are applied, the contracting party shall advise the intervention agency in good time before the intended commencement of removal from storage; the intervention agency may require that this information is given at least two working days before that date .
Where the obligation to notify the intervention agency is not complied with but where sufficient evidence has been furnished, within 30 days following removal from the warehouse, to the satisfaction of the competent authority as to the date of removal from storage and the quantities concerned :
_ the aid shall be granted, without prejudice to Article 6 ( 3 ),
and
_ 15 % of the security shall be declared forfeit in respect of the quantity concerned .
For all other cases of non-compliance with this requirement :
_ no aid shall be paid in respect of the contract concerned,
and
_ the whole of the security shall be declared forfeit in respect of the contract concerned .
6 . Subject to the
cases of force majeure referred to in Article 10, where the contracting party fails to respect the end of the contractual storage period or the two-month time limit referred to in paragraph 4 for the totality of the quantity stored, each calendar day of non-compliance shall entail a reduction of 10 % in the amount of aid for the contract in question .
Article 10
In cases of force majeure where the performance of the contractual obligations of a contracting party are affected, the competent authority of the Member State concerned shall decide on the measures which it deems necessary having regard to the circumstances invoked . That authority shall inform the Commission of each case of force majeure and of the action taken in respect thereof .
TITLE II
SPECIAL PROVISIONS
Article 11
Where the aid if fixed at a flat rate in advance :
( a ) the contract application must be lodged with the competent intervention agency in accordance with Article 3 ( 1 ) and ( 2 );
( b ) the intervention agency concerned must, within five working days following the day on which a contract application is lodged with the agency, advise the applicant of the decision concerning that application by registered letter, by telex, by telefax or against written acknowledgement .
Where the application is accepted, the contract shall be deemed to have been concluded on the day of departure of the notification of the decision referred to in the first subparagraph under ( b ). The intervention agency shall specify the date referred to in Article 3 ( 3 ) ( c ) accordingly .
Article 12
1 . Where the aid is granted by tender :
( a ) the Commission shall announce in the Official Journal of the European Communities the opening of a procedure to tender, specifying in particular the products to be stored, the time limit ( date and hour ) for the submission of tenders and the minimum quantities in respect of which a tender may by submitted;
( b ) tenders must be made in ecus and submitted to the intervention agency concerned in accordance with Article 3 ( 1 ) and ( 2 );
( c ) tenders shall be examined in private session by the appropriate agencies of the Member States; persons present at the examination shall be sworn to secrecy;
( d ) tenders submitted must be forwarded anonymously to the Commission by way of the Member States, to arrive not later than the second working day following the final date for submission as specified in the invitation;
( e ) where no tenders are submitted, Member States shall inform the Commission of this within the time limit as specified under ( d );
( f ) on the basis of the tenders received, the Commission shall decide in accordance with the procedure laid down in Article 24 of Regulation ( EEC ) No 2759/75, either to fix a maximum amount of aid, taking account of the conditions laid down in Article 4 ( 2 ) of Regulation ( EEC ) No 2763/75, or to make no award;
( g ) where a maximum amount of aid is fixed, tenders not exceeding this amount shall be accepted .
2 . Within five working days following the day on which the Member States are notified of the Commission's decision, the intervention agency concerned shall inform all tenderers of the decision taken by registered letter, by telex, by telefax or against written acknowledgement .
Where a tender is accepted, the contract shall be deemed to have been concluded on the date of departure of the notification of the intervention agency to the tenderer as referred to in the first subparagraph . The intervention agency shall specify the date referred to in Article 3 ( 3 ) ( c ) accordingly .
TITLE III
CHECKING AND SANCTIONS
Article 13
1 . Member States shall ensure that the conditions giving rise to entitlement to aid are fulfilled . For this purpose they shall designate the national authority to be responsible for checking storage operations .
2 . The contracting party shall make available to the authority charged with the checking operations of all documentation, for each contract, permitting in particular the following information on the products placed in private storage to be verified :
( a ) the ownership at the time of placing in storage;
( b ) the date of placing in storage;
( c ) the weight and the number of boxes or pieces otherwise packaged;
( d ) the presence of products in the warehouse;
( e ) the calculated date of the end of the minimum contractual storage period and, where Article 9 ( 4 ) or ( 6 ) is applied, completed by the actual date of removal .
3 . The contracting party or, where applicable, the operator of the warehouse, shall keep stock accounts available at the warehouse
covering, by contract number :
( a ) the identification of the products placed in private storage;
( b ) the date of placing in storage and the calculated date of the end of the minimum contractual storage period, completed by the actual date of removal from storage;
( c ) the number of half-carcases, boxes or other pieces stored individually, a description of the products and the weight of each pallet or the other pieces stored individually, registered, where applicable, by individual lots;
( d ) the location of the products in the warehouse .
4 . Products stored must be easily identifiable and must be identified individually by contract . Each pallet and, where applicable, each piece individually stored must be marked so that the contract number, the description of the product and the weight are shown . The date of placing in storage must be shown on each individual lot placed in storage on a given day .
When the products are placed in storage, the authority charged with the checking operations shall verify the identification referred to in the first subparagraph and may seal the products in storage .
5 . The authority charged with the checking operations shall undertake :
( a ) for each contract, a check on the compliance with all the obligations laid down in Article 3 ( 4 );
( b ) an obligatory check to ensure that the products are present in the warehouse during the final week of the contractual storage period;
( c ) _ either the sealing of all the products stored under a contract in accordance with the second sub-paragraph of paragraph 4
or
_ an unannounced sample check to ensure that the products are present in the warehouse . The sample taken must be representative and must correspond to at least 10 % of the overall quantity placed in storage in each Member State under a private storage aid measure . Such checks shall include, in addition to an examination of the accounts referred to in paragraph 3, a physical check of the weight and type of the products and their identification . Such physical checks must relate to at least 5 % of the quantity subject to the unannounced check .
The sealing or handling costs of the check are borne by the contracting party .
6 . Checks pursuant to paragraph 5 must be the subject of a report stating :
_ the date of the check,
_ its duration
and
_ the operations conducted .
The report on the check must be signed by the official responsible and countersigned by the contracting party or, where applicable, by the operator of the warehouse and must be included in the payment file .
7 . In the case of significant irregularities affecting at least 5 % of the quantities of products covered by a single contract subject to the checks, the verification shall be extended to a larger sample to be determined by the authority responsible for the checks .
Member States shall notify such cases to the Commission within four weeks .
Article 14
In the case where it is established and verified by the responsible authority charged with checking operations that the declaration as referred to in Article 3 ( 3 ) ( a ) is a false declaration made either deliberately or through serious negligence, the contracting party in question shall be excluded from the private storage aid regime until the end of the calendar year following that of this finding .
TITLE IV
FINAL PROVISIONS
Article 15
1 . Member States shall inform the Commission of all provisions adopted in application of this Regulation .
2 . Member States shall notify the Commission by telex or by telefax :
( a ) before Thursday of each week and broken down by storage period, of the products and quantities which have been the subject of contract applications, and of the products and quantities for which contracts have been concluded during the preceding week, giving a summary of the products and quantities for which contracts have been concluded;
( b ) every month, of the products and total quantities placed in storage;
( c ) every month, of the products and total quantities actually in storage and of the products and total quantities in respect of which the contractual storage period has ended;
( d ) every month, if the storage period has been shortened or extended in accordance with Article 3 ( 3 ) ( g ) or reduced in accordance with Article 9 ( 4 ) or ( 6 ), of the products and quantities in respect of which the storage period has been revised and of the original and revised months for removal from storage .
3 . The application of the measures provided for in this Regulation shall be subject to regular examination in accordance with the procedure laid down in Article 25 of Regulation ( EEC ) N
2759/75 .
Article 16
1 . Regulation ( EEC ) No 1092/80 is hereby repealed .
2 . References to the Regulation repealed by paragraph 1 shall be construed as references to this Regulation .
References to Articles of Regulation ( EEC ) No 1092/80 are to be read in accordance with the correlation table given in the Annex .
Article 17
This Regulation shall enter into force on the day following that of its publication in the Official Journal of the European Communities .
It shall apply to contracts concluded as from that date .
This Regulation shall be binding in its entirety and directly applicable in all Member States .
Done at Brussels, 27 November 1990 .
For the Commission
Ray MAC SHARRY
Member of the Commission
ANNEX
CORRELATION
1.2Regulation ( EEC ) No 1092/80
This Regulation // //
1.2Article 1 Article 2 Article 3 Article 4 _ Article 5 Article 6 _ Article 7 Article 8 Article 9 Article 10 Article 11 _ _ Article 12 Article 13 Article 14
Article 1 Article 2 Article 3 ( 3 ) and ( 4 ) Article 3, ( 1 ) and ( 2 ) Article 4 Article 5 Article 6 Article 7 Article 8 Article 9 Article 10 Article 11 Article 12 Article 13 Article 14 Article 15 Article 16 Article 17
