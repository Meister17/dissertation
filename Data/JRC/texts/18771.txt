COUNCIL REGULATION (EEC) No 2019/93 of 19 July 1993 introducing specific measures for the smaller Aegean islands concerning certain agricultural products
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Articles 42 and 43 thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas the European Council, meeting in Rhodes on 2 and 3 December 1988, acknowledged the specific socio-economic problems experienced by certain island regions in the Community; whereas measures should be implemented in order to respond to these specific problems;
Whereas the exceptional geographical situation of the Aegean islands in relation to the sources of supply of food and agricultural products essential for everyday consumption or agricultural production on the Aegean islands imposes costs which constitute a severe handicap to the sectors concerned; whereas this natural handicap can be offset by the introduction of specific supply arrangements for essential basic products;
Whereas the quantities of products benefiting from the specific supply arrangements must be determined within the framework of periodic forecast supply balances which may be adjusted during the year on the basis of the essential requirements of the local market and taking account of local production; whereas, in view of the measures adopted to encourage the development of local production, these arrangements should be applied in the fruit and vegetable sector over a period of five years and on a reducing basis;
Whereas the arrangements in question are intended to reduce production costs and consumer prices; whereas their actual impact should therefore be monitored;
Whereas, to avoid any deflection of trade, products covered by the specific supply arrangements may not be redispatched to other parts of the Community or re-exported to third countries;
Whereas a suitable and effective management and control system should be established for the implementation of the arrangements;
Whereas the specific conditions of agriculture on the Aegean islands require special attention; whereas measures are necessary both for stockfarming and animal products and for crop products;
Whereas, to promote the development of traditional stockfarming on the islands, supplementary premiums should be granted for the fattening of male bovine animals and for the maintenance of suckler cows and private storage aid should be granted for cheeses traditionally manufactured on the islands;
Whereas measures should be taken in the fruit and vegetables and flower-growing sector to support and increase production and improve farm productivity and product quality;
Whereas measures should also be taken to support the production of potatoes for human consumption and of seed potatoes;
Whereas, in order to help support traditional viticulture on this islands, aid should be granted for vines producing quality wines psr which meet the requirements laid down in Community rules and form part of a quality improvement programme;
Whereas, in order to support and encourage quality improvements in local production of quality liqueur wines psr, aid should be granted to offset storage costs for the ageing of such products;
Whereas, in order to help support traditional olive-growing on the islands, to maintain production potential and to preserve the countryside and the natural environment, a per-hectare aid should be granted, provided that olive groves are kept in a way that guarantees regular production;
Whereas bee-keeping is a sector linked to maintaining the important and fragile flora of the Aegean islands while at the same time providing the inhabitants with extra income; whereas, therefore, that traditional activity should be supported through the grant of an aid helping to reduce the high production costs; whereas that aid should be granted as part of the initiatives to improve the marketing conditions for honey to be carried out by associations of producers; whereas, in anticipation of such associations being set up, a limited amount of aid should be allocated to all honey producers for a restricted period;
Whereas farms on the Aegean islands present major structural weaknesses from which specific difficulties derive; whereas a number of derogations are therefore necessary form the rules which restrict or prohibit the grant of certain forms of structural aid;
Whereas certain structural measures essential for the development of agriculture on the Aegean islands in question are financed under Community support frameworks to promote the development and structural adjustment of regions whose development is lagging behind (objective 1) pursuant to Articles 130a and 130c of the Treaty;
Whereas the problems experienced by the Aegean islands are accentuated by the lack of size of the islands; whereas, in order to direct the priorities and guarantee the effectiveness of the measures envisaged, these should only be applied to islands with populations not exceeding 100 000 inhabitants, known as the 'smaller islands`,
HAS ADOPTED THIS REGULATION:
Article 1
This Regulation lays down specific measures to remedy, in respect of certain agricultural products and means of agricultural production, the difficulties caused by the remote and insular nature of the smaller Aegean islands, hereafter referred to as the 'smaller islands`.
For the purposes of this Regulation 'smaller islands` are any islands in the Aegean Sea the permanent population of which does not exceed 100 000 inhabitants.
TITLE I
Specific supply arrangements
Article 2
For each calendar year, forecast supply balances shall be established for the agricultural products necessary for human consumption and for the basic means required for agricultural production listed in the Annex hereto. These balances may be revised during the year on the basis of trends in the smaller islands' requirements.
Article 3
1. Under the arrangements referred to in this Title, aids shall be granted for the supply to the smaller islands of the products listed in the Annex hereto, taking particular account of the specific needs of those islands and, where the products are food, the precise quality requirements and quantitative needs. The supply arrangements shall be implemented in such a way as not to obstruct the potential development of local products.
2. The aid shall be fixed as a lump sum for each group of islands on the basis of marketing costs calculated from the ports of mainland Greece from which normal supplies are dispatched.
Aid for fruit and vegetables shall be paid for a period of five years as from 1993. The aid for 1994, 1995, 1996 and 1997 shall be fixed at 80 %, 60 %, 40 % and 20 % respectively of the amount applicable in 1993.
90 % of the amount of the aid shall be financed by the Community and 10 % by the Member State in question.
3. A condition for benefiting from the arrangements shall be that the advantages are actually passed on to the end user.
4. The products covered by the specific supply arrangements provided for in this Title may not be re-exported to third countries or redispatched to the rest of the Community.
5. Products covered by the specific supply arrangements and products processed therefrom shall not be eligible for refunds on exportation from the smaller islands.
Article 4
Detailed rules for the application of this Title shall be adopted according to the procedure laid down in Article 23 of Council Regulation (EEC) No 1766/92 of 30 June 1992 on the common organization of the market in cereals (4) or the corresponding Articles of the regulations on the common organization of the markets in the sectors concerned.
They shall cover in particular:
- determination of the quantities of products covered by the supply arrangements,
- the amounts of the aid,
- the provisions to ensure effective monitoring and that the advantages are actually passed on to the end user.
TITLE II
Measures to support local products
Article 5
The aids provided for under this Title shall be granted for the support of traditional activities, improvement of the quality of and development of local products in accordance with the requirements of the markets on the islands in question and for the revitalization of certain agricultural activities which are a traditional and natural vocation on the islands in question.
Article 6
1. The following aids shall be granted in the stockfarming sector:
- fattening aid for male bovine animals, which shall represent a supplement of ECU 40 per head to the special premium provided for in Article 4b of Council Regulation (EEC) No 805/68 of 27 June 1968 on the common organization of the market in beef an veal (5); the supplement may be granted in respect of an animal of a minimum weight to be determined in accordance with the procedure provided for in paragraph 3,
- a supplement to the premium for maintaining suckler cows provided for in Article 4d of Regulation (EEC) No 805/68, which shall be paid to beef and veal producers; the amount of this supplement shall be ECU 40 per suckler cow held by the producer on the day on which the application is submitted up to a maximum of 40 head per holding.
2. Aid shall be granted for the private storage of locally manufactured cheeses:
- Feta, at least two months old,
- Graviera, at least three months old,
- Ladotyri, at least three months old,
- Kefalograviera, at least three months old.
The amount of the aid shall be fixed in accordance with the procedure referred to in paragraph 3.
3. The Commission shall adopt detailed rules for the application of this Article, including control provisions, in accordance with the procedure laid down in Article 30 of Council Regulation (EEC) No 804/68 of 27 June 1968 on the common organization of the market in milk and milk products (6) or that laid down in Article 27 of Regulation (EEC) No 805/68, as the case may be.
Article 7
1. Aid per hectare shall be granted to producers and producers groups and organizations recognized pursuant to Article 13 of Council Regulation (EEC) No 1035/72 of 18 May 1972 on the common organization of the market in fruit and vegetables (7) and Council Regulation (EEC) No 1360/78 of 19 June 1978 on producer groups and associations thereof (8) respectively that undertake a programme of initiatives, approved by the competent authorities, with a view to the development and/or diversification of production and/or the improvement of the quality of the fruit, vegetables and flowers listed in Chapters 6, 7 and 8 of the combined nomenclature. The eligible initiatives shall seek in particular to develop production and product quality, particularly through varietal conversion and crop improvements. These initiatives shall form an integral part of programmes conducted over at least three years.
The aid shall be granted for programmes covering a minimum area of 0,3 hectares.
2. The amount of Community aid shall be a maximum of ECU 500/ha if the Member State provides official financing of at least ECU 300/ha and the contribution of the individual producer or group amounts to at least ECU 200/ha. If the contributions of the Member State and/or the producers are less than the amounts specified, the Community aid shall be reduced proportionately.
The aid shall be paid each year of execution of the programme, for a period not exceeding three years.
3. The aid shall be increased by ECU 100/ha where the programme of initiatives is submitted and carried out by a producer group or organization and where, for its implementation, recourse to technical assistance is envisaged. The additional aid shall be granted in respect of programmes involving a minimum area of 2 ha.
4. This Article shall not apply to the production of potatoes for human consumption falling within CN codes 0701 90 51, 0701 90 59 and 0701 90 90 or the production of seed potatoes falling within CN code 0701 10 00, nor to the production of tomatoes falling within CN code 0702.
5. Detailed rules for the application of this Article, including control provisions, shall be adopted in accordance with the procedure laid down in Article 33 of Regulation (EEC) No 1035/72.
Article 8
1. Aid per hectare shall be granted annually for the cultivation of potatoes for human consumption falling within CN codes 0701 90 51, 0701 90 59 and 0701 90 90 and for the cultivation of seed potatoes falling within CN code 0701 10 00.
The aid shall be paid in respect of areas cultivated and harvested up to a maximum of 3 200 ha per year.
2. The amount of annual aid shall be ECU 500/ha.
3. Detailed rules for the application of this Article, including control provisions, shall be adopted in accordance with the procedure laid down in Article 11 of Council Regulation (EEC) No 2358/71 of 26 October 1971 on the common organization of the market in seeds (9).
Article 9
1. A flat-rate aid per hectare shall be granted for the continued cultivation of vines for the production of quality wines psr in traditional wine-growing zones.
Aid shall be paid in respect of areas:
(a) planted with varieties included in the list of vine varieties suitable for the production of each of the quality wines psr produced and belonging to the recommended or authorized categories referred to in Article 13 of Council Regulation (EEC) No 822/87 of 16 March 1987 on the common organization of the market in wine (10);
(b) and on which the yield per hectare is below a maximum limit fixed by the Member State, expressed in quantities of grapes, grape must or wine, subject to the conditions of Article 11 of Council Regulation (EEC) No 823/87 of 16 March 1987 laying down special provisions relating to quality wines produced in specified regions (11).
2. The aid shall amount to ECU 400/ha. From the 1997/98 wine year, the aid shall be granted exclusively to producer groups or organizations initiating a measure to improve the quality of the wines produced in accordance with a programme approved by the competent authorities; the programme shall include in particular the means for improving vinification, storage and distribution conditions.
3. Articles 32, 34, 38, 39, 41, 42 and 46 of Regulation (EEC) No 822/87 and Council Regulation (EEC) No 1442/88 of 24 May 1988 on the granting, for the 1988/89 to 1995/96 wine years, of permanent abandonment premiums in respect of wine-growing areas (12) shall not apply to areas or to products obtained from areas in respect of which the aid referred to in paragraph 1 is paid.
4. Detailed rules for the application of this Article shall be adopted, as necessary, in accordance with the procedure laid down in Article 83 of Regulation (EEC) No 822/87. They shall concern in particular the conditions for applying the programme referred to in paragraph 2 and the control provisions.
Article 10
1. Aid shall be granted for the ageing of locally produced traditional quality liqueur wines involving an ageing process of not less than two years. The aid shall be paid during the second year of ageing for up to a maximum quantity of 40 000 hl per year.
The amount of aid shall be set at ECU 0,02/hl per day.
2. Detailed rules for the application of this Article shall be adopted, as necessary, in accordance with the procedure laid down in Article 83 of Regulation (EEC) No 822/87.
Article 11
1. A flat-rate annual aid per hectare shall be granted for the maintenance of olive groves in traditional olive-growing zones, provided that the olive groves are kept and maintained in good production conditions.
The amount of aid shall be ECU 120/ha.
2. Detailed rules for the application of this Article shall be adopted in accordance with the procedure laid down in Article 38 of Council Regulation No 136/66/EEC of 22 September 1966 on the establishment of a common organization in oils and fats (13). In particular they shall fix the conditions for the application of the aid arrangements referred to in paragraph 1, the conditions for proper maintenance of olive groves and the control provisions.
Article 12
1. Aid shall be granted for the production of quality honey specific to the Aegean islands, containing a large proportion of thyme honey.
The aid shall be paid to groups of honey producers, recognized in accordance with Regulation (EEC) No 1360/78 on the basis of the number of registered production hives, which undertake to implement annual programmes of initiatives designed to improve the conditions under which quality honey is marketed and promoted.
The amount of the aid shall be fixed at ECU 10 per registered production hive per year.
2. For a transitional period of a maximum of two years with a view to the establishment and recognition of the groups referred to in paragraph 1, the aid shall be paid to any bee-keeper keeping at least 10 hives in production.
For this particular case, the amount of the aid shall be fixed at ECU 7 per registered production hive.
3. The aid referred to in paragraphs 1 and 2 shall be granted for a maximum of 50 000 and 100 000 hives per year respectively.
4. Detailed rules for the application of this Article, including control provisions, where necessary, shall be adopted in accordance with the procedure laid down in Article 17 of Council Regulation (EEC) No 2771/75 of 29 October 1975 on the common organization of the market in eggs (14).
TITLE III
Derogations applicable to structural measures
Article 13
1. Notwithstanding Articles 5, 6, 7, 10 and 19 of Council Regulation (EEC) No 2328/91 of 15 July 1991 on improving the efficiency of agricultural structures (15), investment aid for agricultural holdings on the Aegean islands in question shall be granted on the following conditions:
(a) notwithstanding Article 5 (1) (a), the investment aid scheme provided for in Articles 5 to 9 of the abovementioned Regulation may be applied on the Aegean islands in question to farmers who, while they do not practise farming as their main occupation, derive at least 25 % of their total income from farming carried out on the holding and whose holding does not require the equivalent of more than one MWU, provided that the envisaged investments do not exceed ECU 25 000. With the exception of local specialities all the food production must be restricted to local consumption;
(b) the authorization concerning the keeping of simplified accounts provided for in Article 5 (1) (d) shall apply;
(c) pig production on family holdings shall not be subject to the conditions laid down in Article 6 (4); however, the condition referred to in the last subparagraph of that paragraph that, upon completion of the plan, at least the equivalent of 35 % of the quantity of feed consumed by pigs can be produced on the holding shall be replaced by a figure of 10 %;
(d) with regard to eggs and poultry production, the prohibition referred to in Article 6 (6) shall not apply to family farms;
(e) notwithstanding Article 7 (2) (a) and (b), the maximum value of the investment aid shall be 55 % whether the investment is in fixed assets or not.
The provisions in (c), (d) and (e) above shall apply only insofar as livestock production is undertaken in a manner compatible with animal welfare and environmental protection requirements and provided that production is for the domestic market on the islands in question.
2. With regard to the installation of young farmers, the condition provided for in the last indent of Article 10 (1) of Regulation (EEC) No 2328/91 shall not apply.
3. Notwithstanding Article 19 (1) (a) of Regulation (EEC) No 2328/91, in the case of cattle farming, the amount of the compensatory allowance referred to in Article 17 of that Regulation may be raised to ECU 180,5 per LU subject to a maximum amount of ECU 3 540 per holding for 1993, to be updated by the procedure laid down in Article 29 of Regulation (EEC) No 4253/88 (16).
4. Notwithstanding Article 19 (1) (b) (iii), the compensatory allowance referred to in Article 17 of Regulation (EEC) No 2328/91 may be granted on the islands for all crops, provided that they are cultivated in a manner compatible with environmental protection requirements and subject to a maximum amount of ECU 3 540 per holding for 1993, to be updated by the procedure laid down in Article 29 of Regulation (EEC) No 4253/88.
In addition, cows whose milk is intended for the domestic market of the region may be taken into consideration for the calculation of the compensatory allowance in all areas of the region specified in Article 3 (4) and (5) of Council Directive 75/268/EEC of 28 April 1975 on mountain and hill farming in certain less-favoured areas (17), up to a maximum of 20 livestock units.
5. The Commission, in accordance with the procedure laid down in Article 29 of Council Regulation (EEC) No 4253/88:
(a) shall adopt the conditions for applying this Article;
(b) may decide, for the smaller islands on a reasoned request by the competent authorities, to derogate from the second indent of Article 17 (3) of that Regulation with a view to authorizing an increase in the rate of Community funding above the limit provided for investments in certain sectors of the processing and marketing of agricultural products intended to improve the standard of living of the inhabitants.
TITLE IV
Final provisions
Article 14
The measures provided for in Titles I and II of this Regulation shall constitute intervention measures designed to stabilize the agricultural markets within the meaning of Article 3 (1) of Council Regulation (EEC) No 729/70 of 21 April 1970 on the financing of the common agricultural policy (18).
Article 15
1. The Commission shall submit to the European Parliament and the Council an annual report on the implementation of the measures provided for in this Regulation, accompanied where appropriate by proposals concerning any adjustment measures which may prove necessary in order to achieve the objectives of this Regulation.
2. At the end of the third year of application of the specific supply arrangements, the Commission shall submit to the European Parliament and the Council a general report on the economic situation of the Aegean islands, showing the impact of the measures taken pursuant to this Regulation.
In the light of the report's conclusions, the Commission shall submit, wherever this proves necessary, appropriate adjustments, including, should the occasion arise, a reduction in the level of certain aid and/or the setting of temporary limits.
Article 16
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 19 July 1993.
For the Council
The President
A. BOURGEOIS
(1) OJ No C 56, 26. 2. 1993, p. 21.
(2) Opinion delivered on 25 June 1993 (not yet published in the Official Journal).
(3) Opinion delivered on 26 May 1993 (not yet published in the Official Journal).
(4) OJ No L 181, 1. 7. 1992, p. 21.
(5) OJ No L 148, 28. 6. 1968, p. 24. Regulation as last amended by Regulation (EEC) No 747/93 (OJ No L 77, 31. 3. 1993, p. 15).
(6) OJ No L 148, 28. 6. 1968, p. 13. Regulation as last amended by Regulation (EEC) No 2071/92 (OJ No L 215, 30. 7. 1992, p. 64).
(7) OJ No L 118, 20. 5. 1972, p. 1. Regulation as last amended by Regulation (EEC) No 746/93 (OJ No L 77, 31. 3. 1993, p. 14).
(8) OJ No L 166, 23. 6. 1978, p. 1. Regulation as last amended by Regulation (EEC) No 746/93 (OJ No L 77, 31. 3. 1993, p. 14).
(9) OJ No L 246, 5. 11. 1971, p. 1. Regulation as last amended by Regulation (EEC) No 3695/92 (OJ No L 374, 22. 12. 1992, p. 40).
(10) OJ No L 84, 27. 3. 1987, p. 1. Regulation as last amended by Regulation (EEC) No 1566/93 (OJ No L 154, 25. 6. 1993, p. 39).
(11) OJ No L 84, 27. 3. 1987, p. 59. Regulation as last amended by Regulation (EEC) No 3896/91 (OJ No L 368, 31. 12. 1991, p. 3).
(12) OJ No L 132, 28. 5. 1988, p. 3. Regulation as last amended by Regulation (EEC) No 833/92 (OJ No L 88, 3. 4. 1992, p. 16).
(13) OJ No 172, 30. 9. 1966, p. 3025/66. Regulation as last amended by Regulation (EEC) No 2046/92 (OJ No L 215, 30. 7. 1992, p. 1).
(14) OJ No L 282, 1. 11. 1975, p. 49. Regulation as last amended by Regulation (EEC) No 1235/89 (OJ No L 128, 11. 5. 1989, p. 29).
(15) OJ No L 218, 6. 8. 1991, p. 1. Regulation as last amended by Regulation (EEC) No 870/93 (OJ No L 91, 15. 4. 1993, p. 10).
(16) Council Regulation (EEC) No 4253/88 of 19 December 1988, laying down provisions for implementing Regulation (EEC) No 2052/88 as regards coordination of the activities of the different structural funds between themselves and with the operations of the European Investment Bank and the other existing financial instruments (OJ No L 374, 31. 12. 1988, p. 1).
(17) OJ No L 128, 19. 5. 1975, p. 1. Directive as last amended by Regulation (EEC) No 797/85 (OJ No L 93, 30. 3. 1985, p. 1).
(18) OJ No L 94, 28. 4. 1970, p. 13. Regulation as last amended by Regulation (EEC) No 2048/88 (OJ No L 185, 15. 7. 1988, p. 1).
ANNEX
List of products covered by the specific supply arrangements under Title I for the smaller Aegean islands
>TABLE>
