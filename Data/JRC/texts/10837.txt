Reference for a preliminary ruling from Tribunal des Affaires de Sécurité Sociale de Paris made on 22 February 2006 — Philippe Derouin v Union pour le recouvrement des cotisations de sécurité sociale et d'allocations familiales de Paris — Région Parisienne
Referring court
Tribunal des Affaires de Sécurité Sociale de Paris
Date lodged: 22 February 2006
Parties to the main proceedings
Applicant: Philippe Derouin.
Defendant: Union pour le recouvrement des cotisations de sécurité sociale et d'allocations familiales de Paris — Région Parisienne (Urssaf).
Questions referred
Is Regulation 1408/71 of 14 June 1971 [1] to be interpreted as precluding a convention, such as the United Kingdom/France Taxation Convention of 22 May 1968, from providing that income received in the United Kingdom by workers resident in France and covered by social insurance in that State is excluded from the basis on which the "contribution sociale généralisée" (CSG — general social contribution) and the "contribution pour le remboursement de la dette sociale" (CRDS — social debt repayment contribution) levied in France are assessed?
[1] Regulation (EEC) No 1408/71 of the Council of 14 June 1971 on the application of social security schemes to employed persons and their families moving within the Community (OJ 1971 L 149, p. 2)
--------------------------------------------------
