[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 30.9.2005
COM(2005) 460 final
2005/0195 (CNS)
Proposal for a
COUNCIL DECISION
amending Decision 2000/24/EC in order to include the Maldives in the list of countries covered, following the Indian Ocean tsunamis of December 2004
(presented by the Commission)
EXPLANATORY MEMORANDUM
CONTEXT OF THE PROPOSAL |
110 | Grounds for and objectives of the proposal Council Decision 2000/24/EC, as amended, establishes a global Community guarantee for the European Investment Bank (EIB) in respect of all payments not received by it but due in respect of credits opened for investment projects in certain regions. The Decision contains a list of countries covered which does not include the Maldives, a country severely affected by the Indian Ocean tsunamis of December 2004. |
120 | General context The tsunamis in the Indian Ocean region on 26 December 2004 devastated certain coastal areas of surrounding countries, in particular India, Indonesia, the Maldives and Sri Lanka. The Commission and the EIB swiftly started exploring the possibility for the EIB to set up a long term lending facility to help finance the reconstruction efforts. At its meeting of 18th January, ECOFIN "expressed its satisfaction at the initiative concerning the EIB, and called on the EIB and the Commission to continue work on preparing its contribution". Following work undertaken on needs assessment by International Financial Institutions, joint missions to Indonesia and Sri Lanka by EIB and Commission services, and taking into account the immense and unexpected amounts of funds provided by public and private international donors, it has become clear that most of the infrastructure reconstruction needs of the countries concerned will be met from available grants and highly concessional loan financing. However EIB have identified a small number of potential projects in some of the affected countries (Indonesia, Sri Lanka, the Maldives), and have agreed to prioritise the remaining margin within the existing Asia and Latin America (ALA) lending mandate, established by Council Decision 2000/24/EC as amended, towards lending to these countries. The Commission would provide grant support alongside EIB loans to jointly identified projects in these countries, as and when necessary. The existing ALA lending mandate covers Indonesia and Sri Lanka (amongst other countries), but does not include the Maldives. This proposal would add the Maldives to the countries covered by the Decision, thereby allowing the EIB to lend to the Maldives under the coverage of a Community guarantee. |
139 | Existing provisions in the area of the proposal There are no existing provisions in the area of the proposal. |
141 | Consistency with other policies and objectives of the Union Not applicable. |
CONSULTATION OF INTERESTED PARTIES AND IMPACT ASSESSMENT |
Consultation of interested parties |
211 | Consultation methods, main sectors targeted and general profile of respondents The European Investment Bank has been involved in the preparation of this legislative proposal. |
212 | Summary of responses and how they have been taken into account The comments of the EIB have been taken into account. |
Collection and use of expertise |
229 | There was no need for external expertise. |
LEGAL ELEMENTS OF THE PROPOSAL |
305 | Summary of the proposed action The proposal would amend the existing Council Decision (2000/24/CE as amended) by adding the Maldives to the list of countries covered by the Decision. |
310 | Legal basis The proposal is based on Article 181a of the Treaty establishing the European Community. |
329 | Subsidiarity principle The proposal amends an existing legal instrument, which provides for a guarantee at Community level. The subsidiarity principle therefore does not apply. |
Proportionality principle The proposal complies with the proportionality principle for the following reason(s). |
331 | The proposal provides for a simple amendment to an existing legal instrument, rather than the creation of a new legal instrument. |
332 | The Community guarantee for EIB external actions is administrated through existing systems in the Commission and EIB. |
Choice of instruments |
341 | Proposed instruments: Other (Council Decision). |
342 | Other means would not be adequate for the following reason(s). The Community guarantee for EIB external actions is established by Council Decision. |
BUDGETARY IMPLICATION |
409 | The proposal has no implication for the Community budget. |
1. 2005/0195 (CNS)
Proposal for a
COUNCIL DECISION
amending Decision 2000/24/EC in order to include the Maldives in the list of countries covered, following the Indian Ocean tsunamis of December 2004
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 181a thereof,
Having regard to the proposal from the Commission[1],
Having regard to the opinion of the European Parliament[2],
Whereas:
(1) The tsunamis in the Indian Ocean region on 26 December 2004 caused widespread damage to certain coastal regions of surrounding countries.
(2) The European Investment Bank (EIB) has identified potential projects in the most seriously affected countries, including the Maldives, which could benefit entities which have suffered direct or indirect losses as a result of the tsunamis.
(3) Council Decision 2000/24/EC of 22 December 1999 granting a Community guarantee to the European Investment Bank against losses under loans for projects outside the Community (Central and Eastern Europe, Mediterranean countries, Latin America and Asia and the Republic of South Africa)[3] does not include the Maldives in the list of countries covered.
(4) Council Decision 2000/24/EC should therefore be amended to include the Maldives,
HAS DECIDED AS FOLLOWS:
Article 1
Article 1(2) of Decision 2000/24/EC is amended as follows:
- in the fourth indent the words “the Maldives” are inserted between the words “Malaysia” and “Mongolia”.
Article 2
This Decision shall take effect on the day of its adoption.
Done at Brussels,
For the Council
The President
[1] OJ C , , p. .
[2] OJ C , , p. .
[3] OJ L9, 13.1.2000, p.24. Decision as last amended by Decision 2005/47/EC (OJ L 21, 25.1.2005, p.9).
