COUNCIL DIRECTIVE of 26 June 1991 on animal health conditions governing intra-Community trade in and imports from third countries of fresh poultrymeat (91/494/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 43 thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas poultrymeat is included in the list of products in Annex II to the Treaty; whereas the breeding and rearing of poultry is included in the farming sector and constitutes a source of income for part of the farming population;
Whereas disparities between Member States should be removed by laying down rules regarding the animal health aspects of intra-Community trade in fresh poultrymeat in order to ensure the rational development of that sector and improve productivity by encouraging intra-Community trade, with a view to the completion of the internal market;
Whereas, in particular, to improve information concerning the state of health of the poultry from which fresh meat for consignment to another Member State comes, it should be stipulated that the poultry must either have been reared on the territory of the Community or have been imported from third countries in accordance with Chapter III of Council Directive 90/539/EEC of 15 October 1990 on animal health conditions governing intra-Community trade and imports from third countries of poultry and hatching eggs (4);
Whereas, in order to prevent it from propagating epizootic diseases, fresh meat coming from a holding or area which, in accordance with Community rules, has been placed under health restrictions or from an area infected by avian influenza or Newcastle disease should be excluded from intraCommunity trade;
Whereas care should be taken that fresh poultrymeat that does not comply with Community rules should not be given
the health mark provided for in Council Directive 71/118/EEC of 15 February 1971 on health problems affecting trade in fresh poultrymeat (5), as last amended by Directive 90/484/EEC (6); whereas, such meat may, however, be put to other uses provided that it has undergone treatment designed to destroy the germs of diseases, in which case it will bear a special mark to that effect;
Whereas, in respect of the organization of and the follow-up to the checks being carried out by the Member State of destination and the safeguard measures to be implemented, reference should be made to the general rules laid down in Council Directive 89/662/EEC of 11 December 1989 concerning veterinary checks in intra-Community trade with a view to the completion of the internal market (7);
Whereas provision should be made so that inspections may be carried out by the Commission;
Whereas, in the interests of the harmonious development of intra-Community trade; Community arrangements should be defined in respect of imports from third countries;
Whereas defining the said arrangements requires in particular that a list be drawn up of third countries or parts of third countries from which fresh poultrymeat may be imported and that a certificate be produced;
Whereas the Commission's veterinary experts should be instructed to carry out checks in third countries in order to determine whether the Community rules are complied with;
Whereas the rules and general principles governing checks on fresh poultrymeat will be determined later as part of the measures to be taken to bring about the internal market;
Whereas Directive 90/539/EEC should be amended in order to take into account the contents of this Directive, with a view in particular to ensure parallelism with regard to the date on which Member States must comply with the new health rules;
Whereas the provisions of this Directive will have to be reviewed in the context of the completion of the internal market;
Whereas provision should be made for a procedure establishing close cooperation between the Commission and the Member States within the Standing Veterinary Committee;
Whereas it is advisable to allow time to introduce harmonized rules concerning Newcastle disease,
HAS ADOPTED THIS DIRECTIVE:
CHAPTER I General provisions
Article 1
This Directive lays down animal health requirements governing intra-Community trade in and imports from third countries of fresh poultrymeat.
Article 2
For the purposes of this Directive, the definitions, and especially those for poultry, set out in Article 2 of Directive 90/539/EEC shall also apply.
Moreover:
(a) 'meat' means any parts of poultry which are fit for human consumption;
(b) 'fresh meat' means any meat, including meat vacuum-wrapped or wrapped in a controlled atmosphere, which has not undergone any treatment other than cold treatment to ensure its preservation.
CHAPTER II Rules governing intra-community trade
Article 3
A. In order to be traded within the Community fresh meat must have been obtained from poultry which:
1. has been held in Community territory since hatching or has been imported from third countries in accordance with the requirements of Chapter III of Directive 90/539/EEC.
Until 31 December 1992 poultrymeat intended for Member States or regions of Member States the status of which has been recognized in accordance with Article 12 (2) of that Directive must come from poultry which has not been vaccinated against Newcastle disease using an attenuated live vaccine during the 30 days preceding slaughter.
Acting before 1 January 1992 by a qualified majority on a proposal from the Commission based on a report on the risks of transmission of Newcastle disease, the Council shall adopt the rules applicable as from 1 January 1993;
2. comes from a holding:
- which has not been placed under animal health restrictions in connection with a poultry disease,
- which is not situated in an area which has been declared an avian influenza or Newcastle disease infection area;
3. during transport to the slaughterhouse did not come into contact with poultry suffering from avian influenza or Newcastle disease; such transport through an area which has been declared an avian influenza or Newcastle disease infection area shall be prohibited unless major road or rail links are used;
4. comes from slaughterhouses in which, at the time of slaughter, no case of avian influenza or Newcastle disease had been recorded.
Any fresh meat which is suspected of having been contaminated at the slaughterhouse, cutting plant or storage depot or in the course of transport must be excluded from trade;
5. is marked in accordance with Articles 4 and 5;
6. is accompanied by the health certificate provided for in Annex IV to Directive 71/118/EEC, amended in accordance with the Annex to this Directive.
B.
Exempted from this chapter are national controls governing meat:
- contained in the personal luggage of travellers and intended for their personal consumption,
- in small consignments to private individuals, provided that the said consignments are not of a commercial nature,
- for consumption by the crew and passengers on board means of transport operating internationally.
Article 4
Fresh poultrymeat covered by this Directive shall be given the health mark specified in Article 3 (1) A (e) of Directive 71/118/EEC, provided that it meets the requirements laid down in Article 3 (A) of this Directive and that it comes from poultry slaughtered in accordance with the hygiene requirements laid down in Directive 71/118/EEC.
Article 5
1. Notwithstanding Article 4, and insofar as it is not intended for intra-Community trade in fresh meat, fresh poultrymeat which does not satisfy the requirements laid down in Article 3 (A) (2) (3) and the first subparagraph
of (4) may carry a mark in accordance with Article 3 (1) A (e) of Directive 71/118/EEC, provided that this mark is immediately:
(a) either overstamped in such a way that the health mark defined in point 44.1 (a) and (b) of Chapter X of
Annex I to Directive 71/118/EEC is covered by a diagonal cross consisting of two straight lines crossing at right angles, with the point of intersection in the centre of the stamp and the information thereon remaining legible;
(b) or replaced by a single special mark consisting of the health mark defined in point 44 (a) and (b) of Chapter X of Annex I to Directive 71/118/EEC, overstamped in accordance with point (a) of this paragraph.
The provisions of point 43 of Chapter X of Annex I to Directive 71/118/EEC shall apply mutatis mutandis to the keeping and use of marking instruments.
2. The meat referred to in paragraph 1 must be obtained, cut, transported and stored separately from, or not at the same time as, meat intended for intra-Community trade in fresh meat and must be used in such a way as to avoid it being introduced into meat products intended for intraCommunity trade unless they have undergone the treatment specified in Article 4 (1) of Directive 80/215/EEC (8) as last amended by Directive 89/662/EEC.
Article 6
The rules laid down in Directive 89/662/EEC shall apply, in particular as regards the organization of and the follow-up to the checks carried out by the Member State of destination and the safeguard measures to be applied.
Article 7
Commission veterinary experts may, to the extent necessary to ensure uniform application of this Directive, and in conjunction with the competent national authorities, carry out on-the spot inspections. The Commission shall inform the Member States of the outcome of such inspections.
Member States in whose territory an inspection is carried out shall give the experts all the assistance necessary for the performance of their tasks.
General rules for the application of this Article shall be adopted in accordance with the procedure laid down in Article 18. The rules to be followed for the inspections provided for in this Article shall be adopted in accordance with the same procedure.
CHAPTER III Rules applicable to imports from third countries
Article 8
1. Fresh poultrymeat imported into the Community must satisfy the requirements laid down in Articles 9 to 12.
2. However, this Chapter shall not apply to:
(a) poultrymeat forming part of travellers' personal luggage and intended for their personal consumption, provided that the quantity transported does not exceed one kilogram per person and that it comes from a third country or part thereof appearing on the list drawn up in accordance with Article 9 and from which importation is not prohibited under Article 14;
(b)
poultrymeat sent as small consignments to private individuals, provided that such meat is not imported by way of trade, that the quantity does not exceed one kilogram and that it comes from a third country or part thereof appearing on the list drawn up in accordance with Article 9 and from which importation is not prohibited under Article 14;
(c)
meat for consumption by the crew and passengers on board means of transport operating internationally.
Where such meat or the resulting kitchen waste is unloaded, it must be destroyed. It is not, however, necessary to destroy meat when it is transferred, directly or after being placed provisionally under customs supervision, from one means of transport to another.
Article 9
1. Fresh poultrymeat must come from a third country or part of a third country appearing on a list drawn up by the Commission in accordance with the procedure laid down in Article 18. The list may be supplemented or amended in accordance with the procedure laid down in Article 17.
2. In deciding whether a third country or part thereof may be included in the list referred to in paragraph 1, particular account shall be taken of:
(a) the state of health of the poultry, other domestic animals and wildlife in the third country, particular attention being paid to exotic animal diseases, and the environmental health situation in that country, where either are liable to endanger public and animal health in the Member States;
(b)
the regularity and rapidity of the supply of information by the third country relating to the existence of contagious animal diseases in its territory, in particular the diseases appearing on lists A and B of the International Office of Epizootics;
(c)
that country's rules on animal disease prevention and control;
(d)
the structure of the veterinary services in the country and their powers;
(e)
the organization and implementation of measures to prevent and control contagious animal diseases;
(f)
that country's legislation on the use of banned substances, in particular legislation concerning the prohibition or authorization of substances, their distribution, placing on the market and the rules on administering and controlling them;
(g)
the guarantees which the third country can give with regard to rules laid down by this Directive.
3. The list referred to in paragraph 1 and any amendments thereto shall be published in the Official Journal of the European Communities.
Article 10
1. Fresh poultrymeat must come from third countries free from avian influenza and Newcastle disease.
2. The general criteria for classifying third countries in terms of the diseases referred to in paragraph 1 shall be adopted in accordance with the procedure laid down in Article 17. These criteria must in no case be more favourable than those adopted for the Member States pursuant to Directive 90/539/EEC.
3. The Commission may, in accordance with the procedure laid down in Article 18, decide that paragraph 1 shall apply only to a part of the territory of a third country.
Article 11
1. Fresh poultrymeat must:
(a) satisfy animal health requirements adopted in accordance with the procedure laid down in Article 17. These requirements may differ according to the species of bird;
(b)
come from flocks which, prior to consignment, have, without interruption, been held in the third country or part thereof for a period to be defined in accordance with the procedure laid down in Article 17.
2. The animal health conditions shall be determined on the basis of the rules laid down in Chapter II and the corresponding Annexes to Directive 90/539/EEC. In accordance with the procedure laid down in Article 18, derogations may be granted on a case-by-case basis, if the third country concerned offers similar animal health guarantees which are at least of an equivalent standard.
Article 12
1. Fresh poultrymeat must be accompanied by a certificate drawn up by an official veterinarian of the exporting third country.
The certificate must:
(a) be issued on the day of loading for consignment to the country of destination;
(b)
be drawn up in the official language or languages of the country of shipment, of the country of destination, and in one of the official languages of the country in which the import checks are to be carried out;
(c)
accompany the consignment in the original;
(d)
attest that the fresh meat satisfies the requirements of this Directive and those adopted pursuant to this Directive with regard to importation from third countries;
(e)
consist of a single sheet of paper;
(f)
be made out for a single consignee.
2. The certificate must conform with a model established in accordance with the procedure laid down in Article 18.
Article 13
On-the-spot inspections shall be carried out by veterinary experts of the Member States and the Commission to ensure that all the provisions of this Directive are effectively applied.
Member States' experts responsible for these inspections shall be designated by the Commission on proposals from the Member States.
The inspections shall be carried out on behalf of the Community, which shall bear the expenditure incurred.
The frequency of the inspections and the inspection procedure shall be determined in accordance with the procedure laid down in Article 18.
Article 14
1. The Commission may, in accordance with the procedure laid down in Article 17, decide that imports from a third country or part thereof are to be confined to fresh poultrymeat of particular species.
2. The Commission may decide, in accordance with the procedure laid down in Article 17, to apply animal health restrictions, as required, after importation.
Article 15
The rules and general principles applicable during inspections in third countries or during inspections of
imported poultrymeat from third countries and the safeguard measures to be implemented shall be those set out in Directive 90/675/EEC (9).
Article 16
1. Pending the implementation of Community health rules concerning imports of poultrymeat from third countries, Member States shall apply to such imports provisions which shall not be more favourable than
those governing intra-Community trade in accordance
with Directive 71/118/EEC and shall make trade in
poultrymeat subject to the requirements laid down in the second subparagraph of Article 6 (1) (b) of Directive 89/662/EEC.
2. To ensure uniform application of these provisions, inspections may be carried out on-the-spot in third countries by veterinary experts of the Member States and the Commission.
Member States' experts responsible for these inspections shall be designated by the Commission on a proposal from the Member States.
The inspections shall be carried out on behalf of the Community, which shall bear the expenditure incurred.
However, Member States shall be entitled to continue to make inspections under national arrangements of any third country establishments which have not been inspected under the Community procedure.
A list of establishments meeting the conditions referred to Annex 1 to Directive 71/118/EEC shall be drawn up in accordance with the procedure laid down in Article 18.
3. The health certificate which accompanies products on import, and the form and nature of the health mark which the products shall bear, shall correspond to a model to be determined in accordance with the procedure laid down in Article 18.
CHAPTER IV Common provisions
Article 17
Where the procedure laid down in this Article is to be used, the Standing Veterinary Committee (hereinafter referred to as 'the Committee'), set up by Decision 68/361/EEC ($), shall discuss the matter in accordance with the rules laid down in Article 12 of Directive 71/118/EEC.
Article 18
Where the procedure laid down in this Article is to be
used, the Committee shall discuss the matter in accordance
with the rules laid down in Article 12 a of Directive 71/118/EEC.
Article 19
1. Annex A to Directive 89/662/EEC shall be supplemented by the following text:
'- Council Directive 91/494/EEC of 26 June 1991, on animal health conditions governing intra-Community trade in and imports from third countries of fresh poultrymeat (OJ No L 268, 24. 9. 1991, p. 35).'
2. Directive 90/539/EEC shall be amended as follows:
(a) In Article 12 (2), first subparagraph, the phrase 'at the latest six months before the date on which the Member States must conform to this Directive' shall be deleted.
(b)
In Article 36, the date '1 January 1992' shall be replaced by '1 May 1992'.
Article 20
In connection with the proposals for completing the internal market the Council shall review the provisions of this Directive before 31 December 1992, acting by a qualified majority on a proposal from the Commission.
Article 21
Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive no later than 1 May 1992. They shall forthwith inform the Commission thereof.
When Member States adopt these measures, they shall contain a reference to this Directive or shall be accompanied by such reference on the occasion of their official publication. The methods of making such a reference shall be laid down by the Member States.
Article 22
This Directive is addressed to the Member States.
Done at Luxembourg, 26 June 1991.
For the Council
The President
R. STEICHEN
(1) OJ No C 327, 30. 12. 1989, p. 72.(2) OJ No C 183, 15. 7. 1991.(3) OJ No C 124, 21. 5. 1990, p. 12.(4) OJ No L 303, 31. 10. 1990, p. 6.(5) OJ No L 55, 8. 3. 1971, p. 23.(6) OJ No L 267, 29. 9. 1990, p. 45.(7) OJ No L 395, 31. 12. 1989, p. 13.(8) OJ No L 47, 21. 2. 1980, p. 4.(9) OJ No L 373, 31. 12. 1990, p. 1.(10) OJ No L 255, 18. 10. 1968, p. 23.
ANNEX
Amendments to be made to the health certificate set out in annex IV to Directive 71/118/EEC
1. The title should be supplemented as follows:
'ANIMAL-HEALTH AND HEALTH CERTIFICATE'
2. Point IV should be replaced by the following:
'IV. Attestation
I, the undersigned, official veterinarian, certify that:
(a) the poultrymeat described above (;) satisfies the requirements of Directive 90/494/EEC on animal health conditions governing intra-Community trade in and imports from third countries of fresh poultrymeat and also the requirements of the second subparagraph of Article 3 (1) of that Directive, if such meat is destined for a Member State or region of a Member State that is recognised as being free of Newcastle disease;
(b)
- the poultrymeat described above (%),
- the packaging of the meat described above (%),
bear a mark proving that:
- the meat comes from animals slaughtered in approved slaughterhouses (%),
- the meat was cut in approved cutting premises (%);
(c)
this meat has been passed as fit for human consumption following a veterinary inspection carried out in accordance with Council Directive 71/118/EEC of 15 February 1971 on health problems affecting trade in fresh poultrymeat;
(d)
the transport vehicles or containers and the loading conditions of this consignment meet the hygiene requirements laid down in Directive 71/118/EEC.'
3. Footnote 1 should be replaced by the following:
'(;) fresh poultrymeat: fresh meat from the following species: live domestic hens, turkeys, guinea fowls, ducks, geese, quail, pigeons, pheasants and partridges which have not been treated to ensure their preservation'.
