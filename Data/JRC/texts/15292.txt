Judgment of the Court
(Fourth Chamber)
of 15 December 2005
in Case C-96/05: Commission of the European Communities v Hellenic Republic [1]
In Case C-96/05 Commission of the European Communities (Agents: G. Braun and G. Zavvos) v Hellenic Republic (Agent: N. Dafniou) — ACTION for a declaration of failure to fulfil obligations pursuant to Article 226 EC, brought on 21 February 2005 — the Court (Fourth Chamber), composed of K. Schiemann, President of the Chamber, M. Ilešič (Rapporteur) and E. Levits, Judges; P. Léger, Advocate General; R. Grass, Registrar, has given a judgment on 15 December 2005, in which it:
1. Declares that, by failing to adopt within the period prescribed the laws, regulations and administrative provisions necessary to comply with Directive 2001/65/EC of the European Parliament and of the Council of 27 September 2001 amending Directives 78/660/EEC, 83/349/EEC and 86/635/EEC as regards the valuation rules for the annual and consolidated accounts of certain types of companies as well as of banks and other financial institutions, the Hellenic Republic has failed to fulfil its obligations under that directive.
2. Orders the Hellenic Republic to pay the costs.
[1] OJ C 93, 16.04.2005.
--------------------------------------------------
