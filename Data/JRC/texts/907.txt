Commission Regulation (EC) No 150/2001
of 25 January 2001
laying down detailed rules for the application of Council Regulation (EC) No 104/2000 as regards the penalties to be applied to producer organisations in the fisheries sector for irregularity of the intervention mechanism and amending Regulation (EC) No 142/98
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 104/2000 of 17 December 1999 on the common organisation of the market in fishery and aquaculture products(1), and in particular Articles 21(8), 23(5), 24(8), 25(6), and 27(6) thereof,
Whereas:
(1) Regulation (EC) No 104/2000, which repealed Council Regulation (EEC) No 3759/92(2) with effect from 1 January 2001, provides for a number of intervention mechanisms available to producer organisations. Penalties for irregularities to these intervention mechanisms need to be established by the Community in order to deter fraud and to ensure the fair treatment of producer organisations between Member States.
(2) Council Regulation (EC, Euratom) No 2988/95(3) provides for a framework for homogenous checks and administrative measures and penalties concerning irregularities with regard to Community law.
(3) The types of irregularity targeted by this Regulation should be defined.
(4) The penalties applied should be proportionate to the irregularity and should be based on objective and verifiable criteria. In order to ensure that the degree of responsibility is taken into account, intentional irregularities or those caused by serious negligence should incur more severe penalties. For irregularities of limited financial consequence, producer organisations should not be penalised by complete withdrawal of entitlement to aid but merely by a proportionate reduction.
(5) The Commission should be informed of the irregularities of the intervention mechanisms in order to ensure that the correct amounts are reimbursed to the Community budget.
(6) Since the penalties provided for in Commission Regulation (EC) No 142/98 of 21 January 1998 laying down detailed rules for granting the compensatory allowance for tuna intended for the processing industry(4) have been incorporated into this Regulation, it is appropriate to amend Regulation (EC) No 142/98 accordingly.
(7) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Fishery Products,
HAS ADOPTED THIS REGULATION:
Article 1
This Regulation establishes the penalties for irregularities to Articles 21, 23, 24, 25 and 27 of Regulation (EC) No 104/2000 setting out the intervention mechanisms of financial compensation for withdrawals, carry-over aid, independent withdrawals and carry-over, private storage aid and the tuna compensatory allowance (hereinafter referred to as "the intervention mechanisms").
Article 2
For the purposes of this Regulation, the following definitions shall apply:
(a) the "economic operator" mentioned in Article 1(2) of Regulation (EC, Euratom) No 2988/95 means a producer organisation or one of its members;
(b) "intervention price" means one of the following prices according to the intervention mechanism used:
(i) the withdrawal price fixed in accordance with Article 20 of Regulation (EC) No 104/2000;
(ii) the Community selling price fixed in accordance with Articles 22 and 25 of Regulation (EC) No 104/2000;
(iii) the autonomous withdrawal price fixed in accordance with Article 24 of Regulation (EC) No 104/2000;
(iv) the triggering threshold set out in Article 27 of Regulation (EC) No 104/2000.
Article 3
1. Where an irregularity involves amounts of less than 5 % of the annual aid received by the organisation for that mechanism, the Member State shall retain an amount up to 20 % of the intervention price applicable to the quantities of product concerned depending on the seriousness in financial terms of the infringement.
2. Where an irregularity involves amounts of between 5 % and 10 % of the annual aid received by the organisation for that mechanism, the Member State shall retain an amount between 30 % and 50 % of the intervention price applicable to the quantities of product concerned depending on the seriousness in financial terms of the infringement.
3. Where an irregularity involves amounts more than 10 % of the annual aid received by the organisation for that mechanism, the Member State shall retain an amount between 60 % and 80 % of the intervention price applicable to the quantities of product concerned depending on the seriousness in financial terms of the infringement.
4. In case of an intentional irregularity or those caused by serious negligence as established by the Member State, the Member State shall retain all the aid for which the producer organisation is eligible under that mechanism for the fishing year concerned. In the case where an intentional irregularity is established, the Member State shall not grant the aid under that mechanism for the following year.
5. The amounts retained and other sanctions provided for in this Article shall not be regarded as criminal penalties.
Article 4
1. The amount to be retained in accordance with Article 3 shall be either refunded to the Member State, taken from the security left by the producer organisation or deducted from the aid to be received for the next fishing year.
2. The amounts retained by or refunded to the Member State shall be credited to the European Agricultural Guidance and Guarantee Fund.
3. Member States shall inform the Commission each month of the cases where they have applied the provisions of Article 3.
Article 5
Article 9 of Regulation (EC) No 142/98 is deleted.
Article 6
This Regulation shall enter into force on the seventh day following that of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 25 January 2001.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 17, 21.1.2000, p. 22.
(2) OJ L 388, 31.12.1992, p. 1.
(3) OJ L 312, 23.12.1995, p. 1.
(4) OJ L 17, 22.1.1998, p. 8.
