Information relating to the entry into force of the Agreement between the European Community and the Principality of Monaco providing for measures equivalent to those laid down in Council Directive 2003/48/EC on taxation of savings income in the form of interest payments
(2005/C 137/01)
The Agreement between the European Community and the Principality of Monaco providing for measures equivalent to those laid down in Council Directive 2003/48/EC on taxation of savings income in the form of interest payments will enter into force on 1 July 2005, the procedures provided for in Article 16 of the Agreement having been completed on 31 May 2005.
--------------------------------------------------
