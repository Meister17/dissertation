[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 13.10.2006
COM(2006) 606 final
2006/0193 (COD)
Proposal for a
REGULATION OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL
amending Regulation EC No…/…on the addition of vitamins and minerals and of certain other substances to foods
(presented by the Commission)
EXPLANATORY MEMORANDUM
CONTEXT OF THE PROPOSAL |
( Grounds for and objectives of the proposal The proposal aims to amend Regulation (EC) No…/… of the European Parliament and the Council on the addition of vitamins and minerals and of certain other substances to foods, to align it with Council Decision 2006/512/EC amending Decision 1999/468/EC laying down the procedures for the exercise of implementing powers conferred to the Commission. |
( General context Council Decision 1999/468/EC of 28 June 1999 laying down procedures for the exercise of implementing powers conferred on the Commission has been amended by Council Decision 2006/512/EC of 17 July 2006. This decision established a new Comitology procedure: the "regulatory procedure with scrutiny". Article 2 paragraph 2 of Decision 1999/468/EC, as amended, reads : " Where a basic instrument, adopted in accordance with the procedure referred to in Article 251 of the Treaty, provides for the adoption of measures of general scope designed to amend non-essential elements of that instrument, inter alia by deleting some of those elements or by supplementing the instrument by the addition of new non-essential elements, those measures shall be adopted in accordance with the regulatory procedure with scrutiny." The new rules have become applicable at the date of the entry into force of the amending Decision (23 July 2006). The present proposal aims to amend Regulation (EC) No…/… of the European Parliament and the Council on the addition of vitamins and minerals and of certain other substances to foods, to align it with Council Decision 2006/512/EC amending Decision 1999/468/EC laying down the procedures for the exercise of implementing powers conferred to the Commission. Due to the urgency of the matter it will be necessary that the adoption of this proposal by the Parliament and Council takes place by December 2006. |
LEGAL ELEMENTS OF THE PROPOSAL |
( Summary of the proposed action The present proposal aims to introduce in the Regulation (EC) No…/… of the European Parliament and the Council on the addition of vitamins and minerals and of certain other substances to foods reference to the new Regulatory procedure with scrutiny in all cases where the Commission is empowered to adopt quasi-legislative measures within the meaning of Article 2 of Decision 1999/468/EC laying down the procedures for the exercise of implementing powers conferred to the Commission, as amended by Council Decision 2006/512/EC. |
( Legal basis Article 95 of the EC Treaty |
( Subsidiarity principle The proposal falls under the exclusive competence of the Community. The subsidiarity principle therefore does not apply. |
( Proportionality principle The proposal complies with the proportionality principle for the following reason. |
The proposal is limited to the amendments strictly necessary to align the Regulation to the new Comitology Decision. |
( Choice of instruments |
Proposed instruments: Regulation. |
Other means would not be adequate for the following reason. The proposal concerns amendments to an existing Regulation. |
BUDGETARY IMPLICATION |
The proposal has no implication for the Community budget. |
1. 2006/0193 (COD)
Proposal for a
REGULATION OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL
amending Regulation EC No…/…on the addition of vitamins and minerals and of certain other substances to foods
THE EUROPEAN PARLIAMENT AND THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 95 thereof,
Having regard to the proposal from the Commission[1],
Having regard to the opinion of the European Economic and Social Committee[2],
Having regard to the opinion of the Committee of the Regions[3],
Acting in accordance with the procedure laid down in Article 251 of the Treaty[4],
Whereas:
(1) Regulation (EC) No…/… of the European Parliament and of the Council[5] provides that the regulatory procedure established by Council Decision 1999/468/EC of 28 June 1999, laying down the procedures for the exercise of implementing powers conferred on the Commission[6], is to be applied for the adoption of implementing measures concerning that Regulation.
(2) Decision 1999/468/EC has been amended by Decision 2006/512/EC, which introduced a regulatory procedure with scrutiny to be used for the adoption of implementing measures of general scope which seek to amend non-essential elements of a basic instrument adopted in accordance with the procedure referred to in Article 251 of the Treaty including by deleting some of those elements or by supplementing the instrument by the addition of new non-essential elements.
(3) The regulatory procedure with scrutiny should therefore be followed for measures of general scope designed to amend non-essential elements of Regulation (EC) No…/….
(4) On the grounds of efficiency the normal time-limits laid down by the regulatory procedure with scrutiny should be curtailed in certain cases.
(5) Where, on imperative grounds of urgency, the time-limits laid down in the regulatory procedure with scrutiny cannot be complied with, the Commission should be authorized to apply in certain cases the urgency procedure provided for in Article 5a(6) of Decision 1999/468/EC.
(6) Regulation (EC)…/… should therefore be amended accordingly,
HAVE ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No…/…is amended as follows:
(1) Article 3 (3), first subparagraph, is amended as follows:
(a) "Article 14(2)" is replaced by "Article 14 (4)"
(b) The following second sentence is added:
"On imperative grounds of urgency, the Commission may apply the procedure referred to in Article 14 (5)".
(2) In Article 4, second paragraph, Article 5(1), Article 6(1),(2) and (6) and Article 7(1), "Article 14(2)" is replaced by "Article 14(3)"
(3) Article 8(2) is amended as follows:
(a) "Article 14(2)" is replaced by "Article 14(3)".
(b) The following second sentence is added:
"On imperative grounds of urgency, the Commission may apply the procedure referred to in Article 14 (5)".
(4) Article 8(5) is amended as follows:
(a) "Article 14(2)" is replaced by "Article 14(3)".
(b) The following second sentence is added:
"On imperative grounds of urgency, the Commission may apply the procedure referred to in Article 14 (5)".
(5) Article 14 is replaced by the following:
"Article 14 Committee procedure
1. The Commission shall be assisted by the Standing Committee on the Food Chain and Animal Health established by Article 58(1) of Regulation (EC) No 178/2002, hereinafter referred to as "the Committee".
2. Where reference is made to this paragraph, Articles 5 and 7 of Decision 1999/468/EC shall apply, having regard to the provisions of Article 8 thereof.
The period laid down in Article 5(6) of Decision 1999/468/EC shall be set at three months.
3. Where reference is made to this paragraph, Article 5a (1) to (4) and Article 7 of Decision 1999/468/EC shall apply, having regard to the provisions of Article 8 thereof.
4. Where reference is made to this paragraph, Article 5 a (1) to (4) and (5) (b) and Article 7 of Decision 1999/468/EC shall apply, having regard to the provisions of Article 8 thereof.
The periods laid down in Article 5a (3)(c) and (4) (b) and (e) of Decision 1999/468/EC shall be set at two months, one month and two months respectively.
5. Where reference is made to this paragraph, Article 5a (1), (2) and (6) and Article 7 of Decision 1999/468/EC shall apply, having regard to the provisions of Article 8 thereof."
Article 2
This Regulation shall enter into force on the twentieth days following that of its publication in the Official Journal of the European Union .
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels,
For the European Parliament For the Council
The President The President
[1] OJ C , , p. .
[2] OJ C , , p. .
[3] OJ C , , p. .
[4] OJ C , , p. .
[5] OJ L , , p. .
[6] OJ L 184, 17.7.1999, p. 23. Decision as amended by Decision 2006/512/EC (OJ L 200, 22.7.2006, p. 11).
