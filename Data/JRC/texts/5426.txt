P6_TA(2005)0055
Elections in Moldova
European Parliament resolution on the parliamentary elections in Moldova
The European Parliament,
- having regard to its previous resolutions on the situation in Moldova, in particular its resolution of 18 December 2003 [1],
- having regard to the Partnership and Cooperation Agreement between Moldova and the EU which was signed on 28 November 1994 and entered into force on 1 July 1998,
- having regard to the Communication from the Commission of 12 May 2004 on the European Neighbourhood Policy (COM(2004)0373),
- having regard to the Final Statement and Recommendations of the EU-Moldova Parliamentary Cooperation Committee of 11 June 2003,
- having regard to the Action Plan for Moldova, which was submitted to the EU-Moldova Cooperation Council, meeting on 22 February 2005, for approval,
- having regard to the declaration by the Presidency on behalf of the European Union of 9 February 2005 concerning the forthcoming parliamentary elections in Moldova,
- having regard to the Needs Assessment Mission Report drawn up by the OSCE's Office for Democratic Institutions and Human Rights (OSCE/ODIHR) for the parliamentary elections in Moldova,
- having regard to Rule 103(4) of its Rules of Procedure,
A. whereas the European Neighbourhood Policy recognises Moldova's European aspirations and the importance of Moldova as a country with deep historical, cultural and economic links to the Member States,
B. whereas within the framework of this European Neighbourhood Policy an Action Plan has been prepared, which includes proposals aimed at encouraging the political and institutional reforms which will enable Moldova to integrate progressively in EU policies and programmes,
C. whereas a genuine and balanced partnership can only be developed on the basis of shared common values with regard, in particular, to democracy, the rule of law and respect for human and civil rights,
D. whereas Moldovan President Vladimir Voronin declared on 11 December 2004 that his country has "no choice" but to move without further delay towards integration with Europe,
E. whereas parliamentary elections will take place in Moldova on 6 March 2005,
F. whereas these elections must be considered a serious test for the consolidation of democracy in Moldova and for its commitment to shared values,
G. whereas the Joint Recommendations by OSCE/ODIHR and the Council of Europe's Venice Commission on possible improvements to election legislation and electoral administration have not been fully implemented so far,
H. whereas serious concern has been expressed about government control of the public media and pressure on private media and about a general lack of access to the media; whereas concern has also been expressed about abuse of administrative resources for the benefit of incumbent candidates,
I. whereas uncertainties still exist with regard to the electoral lists, in particular with regard to voting possibilities for expatriates and students,
J. whereas Moldova's government has accused the authorities of the eastern breakaway region of Transnistria of stoking tensions ahead of the March elections by mobilising reserve officers and concentrating armed units,
K. stressing that the democratic legitimacy of the new government will enhance the chances of moving forward towards a comprehensive settlement of the Transnistria conflict which respects the sovereignty and territorial integrity of Moldova,
1. Underlines the importance of a further strengthened relationship between the EU and Moldova — inter alia through the urgent appointment of a European Union Special Representative to Moldova and the opening of the Commission's Delegation to Moldova — and confirms the need to work together to contribute to increased stability, security and prosperity on the European continent and to prevent the emergence of new dividing lines;
2. Expresses its strong and continuing support for the efforts of the Moldovan people to establish a fully functioning democracy, the rule of law and respect for human rights in Moldova;
3. Stresses that the forthcoming elections, and in particular compliance with international democratic standards during the electoral process, are of the greatest importance for the further development of relations between Moldova and the European Union;
4. Urges the Moldovan authorities to take all necessary steps to ensure that the parliamentary elections are free and fair, both on the day of the elections and during the election campaign, and thus honour Moldova's international commitments, as laid down by the Council of Europe and the OSCE;
5. Calls, in particular, on the Moldovan authorities to ensure that the elections are held in a transparent fashion on the basis of unbiased, pluralist media coverage of the campaign and even-handedness by the State administration towards all candidates, parties and their supporters;
6. Appeals to the Moldovan authorities to guarantee the right to vote to all citizens of the country, including students, those working outside the country and those whose identity cards expire immediately prior to the elections;
7. Welcomes the Moldovan authorities' invitation to international observers to be present at the parliamentary elections, but urges them, at the same time, to offer the same opportunity to representatives of Moldovan civil society;
8. Urges the Member States, in particular Italy and Portugal, where most Moldovan expatriates live, to find ways to allow Moldovan embassies to open polling stations in places other than embassies, thus facilitating access to voting for the largest possible number of Moldovan immigrants;
9. Calls upon Moldova and its breakaway region of Transnistria to restart settlement talks, urges the mediators to redouble their efforts to assist in the process and reaffirms its continuing strong commitment to supporting settlement of the conflict, drawing on all instruments at its disposal in close consultation with the OSCE;
10. Welcomes in this context the unanimous Council decision of 26 August 2004 [2] to impose restrictions such as visa bans until 27 February 2005 on 17 Transnistrian leaders held responsible for preventing a political settlement of the conflict;
11. Recalls the Council proposal for joint crisis management, and stresses that Russia carries a great responsibility for the situation in the region, maintaining as it does about 2500 troops in Transnistria, which, according to the decision taken at the 1999 OSCE summit in Istanbul, should have been withdrawn by the end of 2002;
12. Instructs its President to forward this resolution to forward this resolution to the Council, the Commission, the Secretary General of the Council of Europe, the Secretary General of the OSCE, the Director of OSCE/ODIHR, and the governments and parliaments of Moldova, Romania, Russia, Ukraine and the USA.
[1] OJ C 91 E, 15.4.2004, p. 692.
[2] OJ L 279, 28.8.2004, p. 47.
--------------------------------------------------
