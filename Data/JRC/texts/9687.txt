Action brought on 30 December 2005 — Tegometall International v OHIM
Parties
Applicant: Tegometall International AG (Lengwil, Switzerland) (represented by: H. Timmann, lawyer)
Defendant: Office for Harmonisation in the Internal Market (Trade Marks and Designs)
Other party to the proceedings before the Board of Appeal of OHIM: Wupperman AG (Leverkusen, Germany)
Form of order sought
The applicant claims that the Court should:
- alter the decision of the Second Board of Appeal of the Office for Harmonisation in the Internal Market of 21 October 2005 in Case R 1063/2004-2, served on 2 November 2005, as amended by Corrigendum of 16 November 2005, served on 23 November 2005, and dismiss the application for declaration of invalidity of Community trade mark No 1227743 of 23 July 2003;
- in the alternative, annul that decision and refer the case back to the Second Board of Appeal for further deliberation;
- order the applicant in the invalidity proceedings to pay the costs of the proceedings before the Cancellation Division and the Board of Appeal and of the present action.
Pleas in law and main arguments
Registered Community trade mark in respect of which a declaration of invalidity has been sought: The word mark TEK for goods in Classes 6 and 20 (shelves and parts of shelves, in particular hanging baskets for shelves, all the aforesaid goods of metal or not of wood) — Community trade mark No 1227743
Proprietor of the Community trade mark: The applicant
Applicant for the declaration of invalidity: Wupperman AG
Decision of the Cancellation Division: Dismissal of the application for declaration of invalidity
Decision of the Board of Appeal: Annulment of the decision of the Cancellation Division
Pleas in law: Infringement of Article 7(1)(b) and (c) of Council Regulation No 40/94, since the facts found by the Board of Appeal do not justify cancellation of the mark. In addition, the Board of Appeal has infringed the applicant's right to a fair hearing.
--------------------------------------------------
