Publication of an application pursuant to Article 6(2) of Council Regulation (EC) No 510/2006 on the protection of geographical indications and designations of origin for agricultural products and foodstuffs
(2006/C 234/02)
This publication confers the right to object to the application pursuant to Article 7 of Council Regulation (EC) No 510/2006. Statements of objection must reach the Commission within six months from the date of this publication.
SUMMARY
COUNCIL REGULATION (EC) No 510/2006
Application for registration in accordance with Article 5 and Article 17(2)
"BANON"
EC No: FR/PDO/005/0290/ 07.05.2003
PDO ( X ) PGI ( )
This summary has been drawn up for information only. For full details, interested parties are invited to consult the full version of the product specification obtainable from the national authorities indicated in section 1 or from the European Commission [1].
1. Responsible department in the Member State:
Name: | Institut National des Appellations d'Origine |
Address: | 51, rue d'Anjou F-75008 Paris |
Tel: | (33-1) 53 89 80 00 |
Fax: | (33-1) 42 25 57 97 |
E-mail: | info@inao.gouv.fr |
2. Group:
Name: | Syndicat Interprofessionnel de Défense et de Promotion du Banon |
Address: | MRE — Route de la Durance F-04100 Manosque |
Tel: | (33-4) 92 87 47 55 |
Fax: | (33-4) 92 72 73 13 |
E-mail: | frecapvincent@wanadoo.fr |
Composition: | producteurs/transformateurs ( X ) autres ( ) |
3. Type of product:
Class 1-3 — Cheeses
4. Product specification (summary of requirements under Article 4(2))
Name : "Banon"
Description :
Banon is a soft cheese manufactured from raw, full-fat goat's milk. It is produced through rapid coagulation (using rennet). The ripening cheese is entirely wrapped in brown chestnut leaves and tied up with six to twelve strands of natural raffia forming a radial pattern.
After ripening for a minimum of fifteen days, ten of which in its leaf wrapping, Banon is smooth, creamy, velvety and soft. The rind is a creamy-yellow colour under the leaves. With the leaves, the cheese is 75 to 85 mm in diameter and 20 to 30 mm in height. The net weight without leaves after the maturing period is 90 to 110 grams.
The cheese contains at least 40 grams of dry matter per 100 grams of cheese and 40 grams of fats per 100 grams of cheese when it is completely dried out.
Geographical area Département des Alpes-de-Haute-Provence (04):
- Canton of BANON: Banon, L'Hospitalet, Montsalier, Redortiers, Revest-des-Brousses, Revest-du-Bion, La-Rochegiron, Saumane, Simiane-la-Rotonde
- Canton of DIGNE: Digne-les-Bains
- Canton of DIGNE Est: Marcoux
- Canton of DIGNE Ouest: Aiglun, Barras, Le-Castellard-Melan, Le-Chaffaut-St-Jurson, Champtercier, Hautes-Duyes, Mallemoisson, Mirabeau, Thoard
- Canton of FORCALQUIER: Dauphin, Forcalquier, Limans, Mane, Niozelles, Pierrerue, Saint-Maime, Saint-Michel-l'Observatoire, Sigonce; Part of the municipality of: Villeneuve
- Canton of LA-JAVIE: Archail, Beaujeu, Le-Brusquet, Draix, La-Javie
- Canton of MANOSQUE: Part of the municipality of: Manosque
- Canton of MANOSQUE Nord: Saint-Martin-les-Eaux; Part of the municipality of: Volx
- Canton of MANOSQUE Sud: Montfuron, Pierrevert
- Canton of LES-MEES: Le-Castellet, Entrevennes, Malijai, Puimichel; Part of the municipalities of: Les-Mees, Oraison
- Canton of MEZEL: Beynes, Bras-d'Asse, Chateauredon, Estoublon, Mezel, Saint-Jeannet, Saint-Julien-d'Asse
- Canton of LA-MOTTE-DU-CAIRE: Chateaufort, Clamensane, La-Motte-du-Caire, Nibles, Valernes
- Canton of MOUSTIERS-SAINTE-MARIE: Moustiers-Sainte-Marie, St-Jurs
- Canton of NOYERS-SUR-JABRON: Bevons, Chateauneuf-Miravail, Curel, Noyers-sur-Jabron Les-Omergues, Saint-Vincent-sur-Jabron, Valbelle
- Canton of PEYRUIS: Part of the municipalities of: La-Brillanne, Ganagobie, Lurs, Peyruis
- Canton of REILLANNE: Aubenas-les-Alpes, Cereste, Montjustin, Oppedette, Reillanne, Sainte-Croix-A-Lauze, Vacheres, Villemus
- Canton of RIEZ: Allemagne-en-Provence, Esparron-de-Verdon, Montagnac-Montpezat, Puimoisson, Quinson, Riez, Roumoules, Sainte-Croix-de-Verdon, Saint-Laurent-du-Verdon
- Canton of SAINT-ETIENNE-LES-ORGUES: Cruis, Fontienne, Lardiers, Mallefougasse-Auges, Montlaux, Ongles, Revest-Saint-Martin, Saint-Etienne-Les-Orgues
- Canton of SISTERON: Entrepierres, Sisteron
- Canton of VALENSOLE: Brunet, Saint-Martin-de-Brômes; Part of the municipalities of: Valensole, Greoux-les-Bains
- Canton of VOLONNE: Aubignosc, Chateauneuf-Val-Saint-Donat, L'Escale, Peipin, Salignac, Sourribes, Volonne; Part of the municipalities of: Chateau-Arnoux-Saint-Auban, Montfort
Département des Hautes-Alpes (05)
- Canton of LARAGNE-MONTEGLIN: Eyguians, Laragne-Monteglin
- Canton of ORPIERRE: Etoile-Saint-Cyrice, Lagrand, Nossage-et-Benevent, Orpierre, Sainte-Colombe, Saleon, Trescleoux
- Canton of RIBIERS: Barret-Le-Bas, Chateauneuf-de-Chabre, Eourres, Saint-Pierre-Avez, Salerans
- Canton of ROSANS: Bruis, Chanousse, Montjay, Moydans, Ribeyret, Rosans, Saint-Andre-de-Rosans, Sainte-Marie, Sorbiers
- Canton of SERRES: Le-Bersac, L'Epine, Mereuil, Montclus, Montmorin, Montrond, La-Piarre, Saint-Genis, Serres, Sigottier
Département du Vaucluse (84)
- Canton of APT: Auribeau, Castellet, Gignac, Lagarde-d'Apt, Saignon, Saint-Martin-de-Castillon, Viens
- Canton of BONNIEUX: Buoux, Sivergues
- Canton of SAULT: Aurel, Monieux, Saint-Christol, Saint-Trinit, Sault
Département de la Drôme (26)
- Canton of BUIS-LES-BARONNIES: Rioms, La-Rochette-du-Buis, Saint-Auban-sur-l'Ouvèze
- Canton of SEDERON: Aulan, Ballons, Barret-de-Lioure, Eygalayes, Ferrassieres, Izon-la-Bruisse, Laborel, Lachau, Mevouillon, Montauban-sur-l'Ouveze, Montbrun-les-Bains, Montfroc, Montguers, Reilhanette, Sederon, Vers-sur-Meouge, Villebois-les-Pins, Villefranche-le-Château
Proof of origin :
Every milk producer, processing plant and maturing plant fills in a "declaration of aptitude" registered with the INAO which allows the INAO to identify all operators involved. All operators must keep their registers and any other documents required for checking the origin, quality and production conditions of the milk and cheese at the INAO's disposal. A label must be affixed to each cheese in order to guarantee its traceability.
As part of the checks carried out on the specified features of the designation of origin, an analytical and organoleptic test is conducted to ensure that the products submitted for examination are of high quality and possess the requisite typical characteristics.
Method of production :
The milk used to produce Banon must come exclusively from goats from the landrace breeds Provençale, Rove and Alpine and from crosses of those breeds. Moreover, the average yield per herd is restricted to 850 kg of milk per lactating goat per year.
The goats feed essentially by grazing on pasture or rough grazing. As soon as the weather and vegetation permit, the goats are put out to graze in these areas. They must graze for at least 210 days per year and pasture must constitute most of their forage for at least four months of the year.
On the farm, the forage area actually reserved for the herd must be equal to at least 1 ha of grassland for every 8 goats and 1 ha of rough grazing for every 2 goats. Rearing without a grazing area or outside exercise area is prohibited.
The input of feed (dry fodder and supplements) supplied in the trough is limited on an annual and daily basis, and purchases of fodder from outside the area are also similarly restricted.
The milk must be collected from the farm every day, and stored at a temperature of 8 °C prior to collection.
It is used in its raw, whole state, without adjustment of protein and fat content.
All physical or chemical processing is prohibited, apart from filtering in order to eliminate macroscopic impurities, cooling to an above-zero temperature for preservation purposes and heating of the milk to a maximum of 35 °C before renneting.
Except for the addition of rennet, lactic cultures, ripening cultures or salt (sodium chloride), no substance may be added to or removed from the milk.
The milk is renneted at a temperature of between 29 °C and 35 °C. In the case of farmhouse cheeses, renneting is carried out no more than 18 hours after the oldest batch of milk is drawn. In the case of industrially-produced cheeses, it is carried out no more than 4 hours after the milk collected is last drawn.
Coagulation occurs no more than 2 hours after renneting. The curds are moulded into cheese-sieves by hand. The cheese is turned over at least twice during the first 12 hours. Draining is carried out at a temperature of at least 20 °C. The cheese is turned out between 24 and 48 hours after moulding. It is then salted.
Ripening takes place in two stages: first, the cheese is ripened for 5 to 10 days at a temperature of at least 8oC before wrapping in chestnut leaves, then it is ripened for at least 10 days at a temperature of between 8 and 14 °C after wrapping. The humidity level must be above 80 %.
The cheeses may be soaked in eau-de-vie made from wine or from grape marc before being placed in chestnut leaves.
This two-fold ripening process, which lasts for at least 15 days after renneting, allows the aromatic properties of the cheese to develop.
Processing may not be deferred for either the curds or the cheese.
Link :
Banon cheese is named after the municipality of BANON, which is situated in the Department of Alpes-de-Haute-Provence.
Banon cheese is produced in an area where the soil is not very fertile and vegetation is sparse and very specific (scrubland) on account of the substratum and the Provençal climate prevailing at medium altitudes, i.e. dry, sunny, frequently fairly cool during winter. Goats like the difficult terrain, which provides them with their main source of food.
A pastoral production system has been generated by this specific environment and has permitted its development. As the ground has little agronomic potential man has sought to make the most of the sparse surrounding natural resources.
Thus, in addition to pigs and poultry, each family owned some goats, "the poor man's cow", which were used to produce fresh milk and cheeses. Originally produced for own consumption, surplus cheeses then started to be sold on local markets, in particular at BANON, a crossroads between major communications routes and a venue for fairs and markets. The Banon cheese became fairly quickly renowned, as is shown from numerous texts dating in particular from XIX century.
After the Second World War herding goats became specialised and the cheese was produced for sale, with the surplus for the family's own consumption.
Banon has two original features in that it is produced through rapid coagulation using rennet and is wrapped in brown chestnut leaves.
Rennet has been used in the production of Banon since time immemorial. This technology has always existed in this area, as evidenced by the large holes in the cheese-sieves, which demonstrate that the curds have been produced with rennet. Banon is one of the few goat cheeses still employing this technique.
This is necessary because of the weather conditions (high temperature and dry ambient climate which prevent the milk being cooled and kept at a low temperature allowing lactic bacteria to work), which requires the milk to coagulate swiftly with the aid of a large quantity of rennet in order to eliminate any risk of the milk quickly turning sour.
Wrapping is also a traditional technique associated with this cheese. It both preserves the cheese (by insulating it from the air) and helps it ripen (development of organoleptic properties). Chestnut leaves were preferred because they are tough and contain tannin.
Thus Banon production helped provide food year-round, in particular in the long winter months when the goats did not give milk.
The production criteria are defined in such a way as to preserve the specific features and traditional practices of the area, and enable them to be reflected in the product. The goats' feeding habits are particularly adapted to their environment and give them specific characteristics. The goats obtain much of their food by grazing.
Name: | Institut National des Appellations d'Origine |
Address: | 51, rue d'Anjou F-75008 Paris |
Tel: | (33-1) 53 89 80 00 |
Fax: | (33-1) 42 25 57 97 |
E-mail: | info@inao.gouv.fr |
Name: | D.G.C.C.R.F. |
Address: | 59, Bd V. Auriol F-75703 Paris CEDEX 13 |
Inspection body Labelling :
Each Banon cheese with a registered designation of origin is marketed under an individual label showing the designation of origin.
The designation is marked in characters at least as large as any other characters on the label, together with the indication "Appellation d'origine contrôlée" (registered designation of origin) or "AOC".
Cheeses qualifying for the registered designation of origin must bear the logo comprising the acronym "INAO", the words "Appellation d'Origine Contrôlée" and the designation.
National requirements : Decree on the "Banon" registered designation of origin.
[1] European Commission, Directorate-General for Agriculture and Rural Development, Agricultural Product Quality Policy, B-1049 Brussels.
--------------------------------------------------
