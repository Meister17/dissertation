Commission Regulation (EC) No 1802/2005
of 3 November 2005
amending Regulation (EC) No 2771/1999 laying down detailed rules for the application of Council Regulation (EC) No 1255/1999 as regards intervention on the market in butter and cream
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1255/1999 of 17 May 1999 on the common organisation of the market in milk and milk products [1], and in particular Article 10 thereof,
Whereas:
(1) Article 21 of Commission Regulation (EC) No 2771/1999 [2], lays down that intervention butter placed on sale must have entered into storage before 1 January 2003.
(2) Given the situation on the butter market and the quantities of butter in intervention storage it is appropriate that butter in storage before 1 January 2004 should be available for sale.
(3) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Milk and Milk Products,
HAS ADOPTED THIS REGULATION:
Article 1
In Article 21 of Regulation (EC) No 2771/1999, the date " 1 January 2003" is replaced by the date " 1 January 2004".
Article 2
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 3 November 2005.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 160, 26.6.1999, p. 48. Regulation as last amended by Commission Regulation (EC) No 186/2004 (OJ L 29, 3.2.2004, p. 6).
[2] OJ L 333, 24.12.1999, p. 11. Regulation as last amended by Regulation (EC) No 1008/2005 (OJ L 170, 1.7.2005, p. 30).
--------------------------------------------------
