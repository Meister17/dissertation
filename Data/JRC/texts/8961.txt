REGULATION (EEC) No 949/75 OF THE COMMISSION of 11 April 1975 amending Regulation (EEC) No 315/68 as regards the quality standards for lilium bulbs
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community;
Having regard to Council Regulation (EEC) No 234/68 (1) of 27 February 1968 on the establishment of a common organization of the market in live trees and other plants, bulbs, roots and the like, cut flowers and ornamental foliage, and in particular Article 4 thereof;
Whereas Regulation (EEC) No 315/68 (2) of 12 March 1968, as last amended by Regulation (EEC) No 1793/73 (3), laid down quality standards for flowering bulbs, corms and tubers;
Whereas experience has shown that present production techniques and the species, varieties and hybrids of lilium bulbs now marketed make it necessary to modify the botanical descriptions and minimum sizes specified in respect of those products ; whereas the relevant standards should therefore be adjusted accordingly;
Whereas the measures provided for in this Regulation are in accordance with the Opinion of the Management Committee for Live Plants,
HAS ADOPTED THIS REGULATION:
Article 1
In the table appearing in Section III of the Annex to Regulation (EEC) No 315/68, the entries relating to lilium bulbs are replaced by the text of the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 11 April 1975.
For the Commission
P.J. LARDINOIS
Member of the Commission (1)OJ No L 55, 2.3.1968, p. 1. (2)OJ No L 71, 21.3.1968, p. 1. (3)OJ No L 181, 4.7.1973, p. 12.
ANNEX
>PIC FILE= "T0007743">
