Communication from the French Government concerning Directive 94/22/EC of the European Parliament and of the Council of 30 May 1994 on the conditions for granting and using authorisations for the prospection, exploration and production of hydrocarbons [1]
(Notice regarding applications for exclusive licences to prospect for liquid and gaseous hydrocarbons, designated the "Etampes licence" and the "Malesherbes licence")
(2005/C 331/05)
(Text with EEA relevance)
By request of 17 January 2005, Géopétrol SA, a company with registered offices at 9, rue Nicolas Copernic Z.I du Coudray 93150, Le Blanc-Mesnil Cedex (France), applied for an exclusive four-year licence, designated the "Etampes licence", to prospect for liquid and gaseous hydrocarbons in an area of approximately 588 km2 in the departments of Essonne, Loiret and Seine-et-Marne.
The perimeter of the area covered by this licence is made up of the meridian and parallel arcs successively joining the vertices defined below by their geographical coordinates, the original meridian being that of Paris.
VERTICES | LONGITUDE | LATITUDE |
A | 0.30° W | 53.90° N |
B | 0.02° W | 53.90° N |
C | 0.02° W | 53.83° N |
D | 0.00° | 53.83° N |
E | 0.00° | 53.80° N |
F | 0.30° E | 53.80° N |
G | 0.30° E | 53.70° N |
H | 0.30° W | 53.70° N |
By request of 12 April 2005, Madison Energy France SNC, a company with registered offices at 13/15 boulevard de la Madeleine, 75001 Paris, applied for an exclusive four-year licence, designated the "Malesherbes Licence", to prospect for liquid and gaseous hydrocarbons in an area of approximately 266,7 km2 in the departments of Essonne, Loiret and Seine-et-Marne.
The perimeter of the area covered by this licence is made up of the meridian and parallel arcs successively joining the vertices defined below by their geographical coordinates, the original meridian being that of Paris.
VERTICES | LONGITUDE | LATITUDE |
A | 0.00° | 53.80° N |
B | 0.20° E | 53.80° N |
C | 0.20° E | 53.60° N |
D | 0.00° | 53.60° N |
These two applications include a common area whose perimeter is made up of the meridian and parallel arcs successively joining the vertices defined below by their geographical coordinates, the original meridian being that of Paris.
VERTICES | LONGITUDE | LATITUDE |
A | 0.00° | 53.80° N |
B | 0.20° E | 53.80° N |
C | 0.20° E | 53.70° N |
D | 0.00° | 53.70° N |
Submission of applications
The initial applicants and competing applicants must prove that they comply with the requirements for obtaining the licence, defined in Articles 3, 4 and 5 of Decree 95-427 of 19 April 1995, as amended, concerning mining rights.
Interested companies may, within ninety days of the publication of this notice, submit a competing application in accordance with the procedure summarised in the "Notice regarding the granting of mining rights for hydrocarbons in France" published in Official Journal of the European Communities C 374 of 30 December 1994, page 11, and established by Decree 95-427 of 19 April 1995, as amended, concerning mining rights (Journal officiel de la République française, 22 April 1995).
Competing applications must be sent to the Minister responsible for mines at the address below. Decisions on the perimeters applied for in the initial applications and competing applications will be taken as follows:
- For the perimeter defined in the application for the Etampes licence, including the common area, within two years of the date on which the French authorities received the application, i.e. by 21 January 2007 at the latest.
- For the perimeter defined in the application for the Malesherbes licence, excluding the common area if this has already been granted with the Etampes licence, within two years of the date on which the French authorities received the application, i.e. by 13 April 2007 at the latest.
Conditions and requirements regarding performance of the activity and cessation thereof
Applicants are referred to Articles 79 and 79.1 of the mining code and to Decree No 95-696 of 9 May 1995, as amended, on the start of mining operations and the mining regulations (Journal officiel de la République française, 11 May 1995).
Further information can be obtained from the Ministry of Economic Affairs, Finance and Industry (Directorate-General for Energy and Raw Materials, Directorate for Energy and Mineral Resources, Bureau of Mining Legislation), 61, Boulevard Vincent Auriol, Télédoc 133, F-75703 Paris Cedex 13 (tel. (33) 144 97 23 02, fax (33) 144 97 05 70).
The abovementioned laws and regulations can be consulted at http://www.legifrance.gouv.fr.
[1] OJ L 164, 30.6.1994, p. 3.
--------------------------------------------------
