Action brought on 18 July 2006 — FMC v Commission
Parties
Applicant: FMC Corporation (Philadelphia, USA) (represented by: C. Stanbrook, Q.C., and Y. Virvilis, lawyer)
Defendant: Commission of the European Communities
Form of order sought
- Annul Commission Decision C(2006) 1766 final of 3 May 2006, in so far as it applies to FMC Corporation; and
- in the alternative reduce the fine imposed on FMC Corporation; and
- order the Commission to bear the costs of these present proceedings.
Pleas in law and main arguments
The applicant seeks partial annulment of the Commission's Decision C(2006) 1766 final of 3 May 2006 in Case COMP/F/38.620 — Hydrogen Peroxide and Perborate, by which the Commission found that the applicant had infringed Article 81 EC and Article 53 of the Agreement on the European Economic Area by participating in a cartel, which consisted mainly of exchanges between competitors of information on prices and sales volumes, agreements on prices, agreements on reduction of production capacity in the EEA and monitoring of the anti-competitive arrangements.
The applicant invokes two pleas in law in support of its application and contends in general that it is not liable for the infringements of its subsidiary Foret as it did not exercise a decisive influence over the subsidiary.
Firstly, the applicant claims that the contested decision is inadequately reasoned.
Secondly, the applicant submits that the contested decision is flawed both in law and in fact as:
a) the Commission's conclusions were based on a misconstruction of the evidence, on wrongful discrimination in giving different weight to different sources of oral evidence, and generally on a manifest error of assessment;
b) the Commission used the wrong legal test of control for the purposes of determining the applicant's responsibility for the infringement of Foret;
c) the Commission used evidence which did not relate to the period of the alleged infringement; and
d) the Commission used evidence which it had not notified to the applicant as forming the basis of the case against the company, thereby denying the applicant the opportunity of exercising its rights of defence.
--------------------------------------------------
