Decision No 206
of 15 December 2005
concerning the methods of operation and the composition of the Audit Board of the Administrative Commission on Social Security for Migrant Workers
(2006/352/EC)
THE ADMINISTRATIVE COMMISSION ON SOCIAL SECURITY FOR MIGRANT WORKERS,
Having regard to Article 101(3) of Council Regulation (EEC) No 574/72 of 21 March 1972 laying down the procedure for implementing Regulation (EEC) No 1408/71, under which the Administrative Commission shall determine the methods of operation and the composition of the Audit Board,
Having regard to Decisions No 86 of 24 September 1973 and No 159 of 3 October 1995 concerning the methods of operation and the composition of the Audit Board,
Whereas the enlargement of the European Union on 1 May 2004 justifies a revision of the previous decisions on the methods of operation and composition of the Audit Board,
HAS DECIDED AS FOLLOWS:
1. Decisions No 86 and No 159 are deleted and the text concerning the methods of operation and the composition of the Audit Board of the Administrative Commission on Social Security for Migrant Workers reproduced in those decisions are replaced by the text annexed to this decision.
2. This decision shall apply from the first day of the month following its publication in the Official Journal of the European Union.
The Chair of the Administrative Commission
Anna Hudzieczek
--------------------------------------------------
ANNEX
Methods of operation and the composition of the Audit Board of the Administrative Commission on Social Security for Migrant Workers
1. The Audit Board provided for in Article 101(3) of Regulation (EEC) No 574/72 shall, when carrying out its functions as laid down in Article 102(1) and Article 113(3) of Regulation 574/72, operate under the authority of the Administrative Commission on Social Security for Migrant Workers, from which it shall receive directives.
Within this framework the Audit Board shall present a long term work programme to the Administrative Commission for approval. The Audit Board shall once a year submit to the Administrative Commission a progress report on the work programme.
2. The Audit Board shall, in principle, reach its decisions from documentary evidence. It can request from the competent authorities any information or enquiries it deems necessary for the investigation of the matters submitted for its examination. Where necessary, subject to the prior approval of the chairman of the Administrative Commission, the Audit Board may delegate a member of the Secretariat or certain members of the Audit Board to carry out, on the spot, any investigation required for the pursuit of its work. The chairman of the Administrative Commission shall notify the representative on the Administrative Commission of the Member State concerned that this investigation is being made.
The Audit Board shall be assisted by an independent expert with professional training and experience in matters concerning the functions of the Audit Board, in particular as regards its tasks under Articles 94, 95 and 101 of Regulation 574/72.
3. The Audit Board shall be composed of two representatives of each of the Member States of the European Union appointed by the competent authorities of those States.
Any member of the Audit Board unable to attend may be replaced by a deputy appointed for that purpose by the competent authorities.
4. Decisions shall be taken by majority, each Member State having only one vote.
The opinions of the Audit Board must indicate whether they were reached unanimously or by majority. They must, where appropriate, set out the conclusions or reservations of the minority.
Whenever an opinion is not reached unanimously, the Audit Board shall submit it to the Administrative Commission together with a report containing in particular a statement of and the reasons for the opposing views.
It shall also appoint a rapporteur responsible for supplying the Administrative Commission with all the information the latter deems appropriate in order to enable it to settle the dispute in question.
The rapporteur shall not be selected from the representatives of countries involved in the dispute.
5. The representative of the European Commission or his alternate on the Administrative Commission shall act in a consultative capacity within the Audit Board.
6. The office of chairman of the Audit Board shall be held by a member belonging to the Member State whose representative on the Administrative Commission holds the office of chairman of that Commission.
The chairman of the Audit Board may, in conjunction with the Secretariat, take all steps required to solve without delay all problems within the competence of the Audit Board.
As a rule, the chairman of the Audit Board shall chair meetings of working parties set up to examine problems for which the Audit Board is competent; if, however, he is incapacitated or if certain specific problems are being examined, the chairman may be represented by another person designated by him.
7. The Secretariat of the Administrative Commission shall prepare and organise the meetings of the Audit Board and draw up the minutes thereof. It shall carry out the work required for the functioning of the Audit Board. The agenda, date and duration of the Audit Board meetings shall be agreed with the chairman.
8. The agenda shall be forwarded by the Secretariat of the Administrative Commission to the members of the Audit Board and the members of the Administrative Commission not less than 20 days before the start of each meeting.
The Secretariat of the Administrative Commission shall make available within the same time-limit the documents relating to the meeting.
9. In so far as is necessary, the rules of the Administrative Commission shall apply to the Audit Board.
--------------------------------------------------
