Commission Regulation (EC) No 431/2005
of 16 March 2005
derogating for 2005 from Regulation (EC) No 1518/2003 regarding the date of issue of export licences in the pigmeat sector
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2759/75 of 29 October 1975 on the common organisation of the market in pigmeat [1], and in particular Article 8(2) thereof,
Whereas:
(1) Article 3(3) of Commission Regulation (EC) No 1518/2003 of 28 August 2003 laying down detailed rules for implementing the system of export licences in the pigmeat sector [2] provides that export licences are to be issued on the Wednesday following the week in which the licence applications have been lodged, provided that no special measures have since been taken by the Commission.
(2) In view of the public holidays in 2005 and the irregular publication of the Official Journal of the European Union during those holidays, the period between the submission of applications and the day on which the licences are to be issued will be too brief to guarantee proper administration of the market and so should be extended.
(3) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Pigmeat,
HAS ADOPTED THIS REGULATION:
Article 1
Notwithstanding Article 3(3) of Regulation (EC) No 1518/2003, licences shall be delivered for 2005 on the dates set out in the Annex to this Regulation.
This derogation shall apply provided that none of the special measures referred to in Article 3(4) of Regulation (EC) No 1518/2003 have been taken before the dates of issue in question.
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 16 March 2005.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 282, 1.11.1975, p. 1. Regulation as last amended by Regulation (EC) No 1365/2000 (OJ L 156, 29.6.2000, p. 5).
[2] OJ L 217, 29.8.2003, p. 35. Regulation as last amended by Regulation (EC) No 1361/2004 (OJ L 253, 29.7.2004, p. 9).
--------------------------------------------------
ANNEX
Period for submission of licence applications | Dates of issue |
21 to 25 March 2005 | 31 March 2005 |
2 to 6 May 2005 | 12 May 2005 |
9 to 13 May 2005 | 19 May 2005 |
8 to 12 August 2005 | 18 August 2005 |
24 to 28 October 2005 | 4 November 2005 |
19 to 23 December 2005 | 29 December 2005 |
--------------------------------------------------
