Judgment of the Court (Grand Chamber) of 6 September 2006 — Portuguese Republic v Commission of the European Communities
(Case C-88/03) [1]
Parties
Applicant: Portuguese Republic (represented by: L. Fernandes, Agent, and J. da Cruz Vilaça and L. Romão, advogados)
Defendant: Commission of the European Communities (represented by: V. Di Bucci and F. de Sousa Fialho, Agents)
Interveners in support of the applicant: Kingdom of Spain (Agent: N. Díaz Abad), United Kingdom of Great Britain and Northern Ireland (R. Caudwell, Agent, and D. Anderson QC)
Re:
Annulment of Commission Decision C(2002) 4487 final in so far as it considers State aid incompatible with the common market the reduction of the rate of income tax for natural and legal persons having their tax residence on the Azores and pursuing certain financial activities (Section J, codes 65, 66 and 67 and Section K, code 74, of the Statistical Classification of Economic Activities in the European Community — NACE REV1.1)
Operative part of the judgment
The Court:
1. Dismisses the action;
2. Orders the Portuguese Republic to pay the costs;
3. Orders the United Kingdom of Great Britain and Northern Ireland and the Kingdom of Spain to bear their own costs.
[1] OJ C 112, 10.5.2003.
--------------------------------------------------
