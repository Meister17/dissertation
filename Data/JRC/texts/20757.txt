REGULATION (EEC) No 1686/72 OF THE COMMISSION of 2 August 1972 on certain detailed rules for aid for seed
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community;
Having regard to Council Regulation (EEC) No 2358/711 of 26 October 1972 on the common organization of the market in seeds, and in particular Articles 3 (5) and 9 thereof;
Whereas Council Regulation (EEC) No 1674/722 of 2 August 1972 laid down general rules for granting and financing aid for seed ; whereas it is for the Commission to lay down the relevant detailed rules;
Whereas, to ensure the proper functioning of the aid system, growing contracts and growing declarations should be registered sufficiently early for the required supervision to be carried out;
Whereas the seed establishment or breeder may require a period in which to treat, pack and have certified the seeds supplied by the grower ; whereas aid can therefore only be granted to the latter several months after the harvest but whereas a time limit should nonetheless be fixed;
Whereas applications for aid lodged by seed growers must give at least the information necessary for supervisory purposes;
Whereas the necessary measures should be laid down for cases where a seed establishment or a breeder from one Member State grows seed in another Member State;
Whereas the measures provided for in this Regulation are in accordance with the Opinion of the Management Committee for Seeds;
HAS ADOPTED THIS REGULATION:
Article 1
Article 3 of Regulation (EEC) No 2358/71 shall be implemented in accordance with the following detailed rules.
Article 2
Member States may fix annually the time limits for the registration of growing contracts and declarations provided for in Article 5 of Regulation (EEC) No 1674/72.
Article 3
1. Aid shall be granted to the seed grower in response to an application to be submitted after the harvest and before a date fixed by the Member State in question for each species or group of varieties.
2. The Member State shall pay the amount of aid to the grower within the two months following the lodging of the application and at the latest on 30 June of the year following the year of the harvest.
Article 4
The application for aid shall include at least: - the name and address of the applicant;
- the quantities of certified seed produced, expressed in quintals to one decimal point;
- the registration number of the growing contract or growing declaration.
The application shall be accompanied by a supporting document to show that the quantities of seed referred to have been officially certified. 1 OJ No L 246, 5.11.1971, p. 1.
2 OJ No L 177, 4.8.1972, p. 1.
Article 5
A seed establishment or a breeder growing seed or having seed grown in a Member State other than that in which approval or registration took place must supply that other Member State, on request, with all the information required for checking entitlement to aid.
Article 6
Each Member State shall notify the Commission of the provisions and measures adopted for the application of the aid system introduced by Article 3 of Regulation (EEC) No 2358/71.
Article 7
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Communities.
It shall apply from 1 July 1972.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 2 August 1972.
For the Commission
The President
S.L. MANSHOLT
