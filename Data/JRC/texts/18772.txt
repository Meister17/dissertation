COMMISSION REGULATION (EEC) No 2090/93 of 27 July 1993 amending Regulation (EEC) No 563/82 laying down detailed rules for the application of Regulation (EEC) No 1208/81 for establishing the market prices of adult bovine animals on the basis of the Community scale for the classification of carcases
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 805/68 of 27 June 1968 on the common organization of the market in beef and veal (1), as last amended by Regulation (EEC) No 125/93 (2),
Having regard to Council Regulation (EEC) No 1208/81 of 28 April 1981 determining the Community scale for the classification of carcases of adult bovine animals (3), as last amended by Regulation (EEC) No 1026/91 (4), and in particular Article 2 (2) thereof,
Whereas Article 1 (3) of Commission Regulation (EEC) No 563/82 (5), as last amended by Regulation (EEC) No 3402/85 (6), fixes the corrective factors applicable to carcase weights when carcase presentation differs from the reference presentation; whereas, for certain degrees of fat cover, the corrective factors relating to the removal of external fat should be adjusted in order to bring them more into line with practice; whereas, also, the methods for calculating such corrective factors should be specified according to whether they are identical in all Member States or differ from one slaugterhouse to another;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Beef and Veal,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EEC) No 563/92 is hereby amended as follows:
1. The following paragraph is added to Article 1:
'4. Where the adjustments referred to in the previous paragraph are the same throughout the territory of a Member State they shall be calculated on a national basis; where such adjustments vary from one slaughterhouse to another, they shall be calculated individually.'
2. In the Annex, the corrective factors laid down for the removal of external fat of fat classes 3 and 4 are replaced by the percentage increases +2 and +3 respectively.
Article 2
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Communities.
It shall apply from 1 September 1993.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 27 July 1993.
For the Commission
René STEICHEN
Member of the Commission
(1) OJ No L 148, 28. 6. 1968, p. 24.
(2) OJ No L 18, 27. 1. 1993, p. 1.
(3) OJ No L 123, 7. 5. 1981, p. 3.
(4) OJ No L 106, 26. 4. 1991, p. 2.
(5) OJ No L 67, 11. 3. 1982, p. 23.
(6) OJ No L 322, 3. 12. 1985, p. 14.
