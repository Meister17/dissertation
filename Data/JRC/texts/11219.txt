Order of the President of the Court of 20 June 2006 — Kingdom of Spain
v Council of the European Union
(Case C-139/05) [1]
(2006/C 224/62)
Language of the case: Spanish
The President of the Court has ordered that the case be removed from the register.
[1] OJ C 115, 14.5.2005.
--------------------------------------------------
