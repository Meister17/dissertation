P6_TA(2004)0070
EC-San Marino Agreement (taxation of savings income) *
European Parliament legislative resolution on the proposal for a Council decision on the conclusion of the Agreement between the European Community and the Republic of San Marino providing for measures equivalent to those laid down in Council Directive 2003/48/EC of 3 June 2003 on taxation of savings income in the form of interest payments (COM(2004)0643 — C6-0172/2004 — 2004/0241(CNS))
(Consultation procedure)
The European Parliament,
- having regard to the proposal for a Council decision (COM(2004)0643) [1],
- having regard to the draft Agreement between the European Community and the Republic of San Marino providing for measures equivalent to those laid down in Council Directive 2003/48/EC on taxation of savings income in the form of interest payments,
- having regard to Article 94, Article 300(2), first subparagraph, and Article 300(4) of the EC Treaty,
- having regard to Article 300(3), first subparagraph, of the EC Treaty, pursuant to which the Council consulted Parliament (C6-0172/2004),
- having regard to Rules 51 and 83(7) of its Rules of Procedure,
- having regard to the report of the Committee on Economic and Monetary Affairs (A6-0039/2004),
1. Approves the conclusion of the agreement;
2. Instructs its President to forward its position to the Council and Commission, and the governments and parliaments of the Member States and of the Republic of San Marino.
[1] Not yet published in OJ.
--------------------------------------------------
