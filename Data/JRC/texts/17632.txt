Brussels, 23.12.2004
COM(2004) 830 final
2004/0284 (COD)
Proposal for a
REGULATION OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL
amending Council Regulation (EEC) No 1408/71 on the application of social security schemes to employed persons, to self-employed persons and to members of their families moving within the Community and Council Regulation (EEC) No 574/72 laying down the procedure for implementing Regulation (EEC) No 1408/71
(presented by the Commission)
EXPLANATORY MEMORANDUM
1. INTRODUCTION
Regulations (EEC) Nos 1408/71 and 574/72 were updated by Regulation (EC) No 118/97[1] and most recently amended by Regulation (EC) No 631/2004[2] of the European Parliament and of the Council.
This proposal intends to update these Community Regulations to reflect changes in national legislation, in particular of the new Member States since the end of accession negotiations. It also intends to complete the simplification of procedures on receiving medical care abroad introduced by Regulation (EC) No 631/2004[3] by extending some of those modifications to the identical procedures regarding benefits for accidents at work and occupational diseases.
2. COMMENTS ON THE ARTICLES
Article 1
Amendments of Regulation (EEC) N° 1408/71
Annexes I, II IIa, III, IV and VI to Regulation (EEC) No 1408/71 shall be amended in accordance with the Annex to this Regulation.
Article 2
Amendments of Regulation (EEC) No 574/72
Deletion of Article 60 (5) and (6)
These are provisions regarding benefits for accidents at work and occupational diseases which are identical to Articles 17 (6) and (7) on sickness benefits in kind which have been abrogated by Regulation (EC) No 631/2004. In the same interest of simplification, the identical provisions on benefits for accidents at work and occupational diseases should also be deleted.
Paragraph (5) of Article 60, which obliges the institution of the place of residence, in the event of hospitalisation, to inform the competent institution of the date of admission to hospital and the probable duration of hospitalisation, should be deleted. In practice the hospitalisation notification is of little use and is increasingly rarely provided.
Also in the interests of simplification, paragraph (6), which obliges the institution of the place of residence to notify the competent institution in the event of granting any major benefits in kind, should be deleted.
Amendment of Article 62
This amendment extends the deletion of Article 20 and the amendment of Article 21 regarding sickness benefits in kind by Regulation 631/2004 to the parallel provision of Article 62 on accidents at work and occupational diseases: Article 62 (1) to (5) which cover only persons employed in international transport is no longer necessary following the introduction of the European Health Insurance Card and the alignment of rights to sickness benefits by Regulation 631/2004 to all insured persons. Article 62 (6) and (7) states that the individual must submit to the institution of the place of stay a certified statement stating the he/she is entitled to benefits in kind. As the identical obligation for sickness benefits has been replaced by the possibility to have direct access to medical care without having to contact the institution of the place of stay before, the same should apply for benefits of accidents at work and occupational diseases.
Amendment of Article 63 (2)
Article 63 (2) will need to be amended to reflect the deletion of paragraphs (5) and (6) of Article 60.
Amendment of Article 66 (1)
Article 66 (1) will need to be amended to reflect the deletion Article 20 by Regulation 631/2004.
Article 3
Entry into force
As the provisions of bilateral agreements listed in Annex III involving new Member States continue to apply and are not replaced by Regulation 1408/71 as of its application in the new Member States, these amendments need to apply from the date of accession 1 May 2004.
3. COMMENTS ON THE ANNEX
1. Amendment of Annex I section I
Annex I section I defines the meaning of the term "member of the family" where national legislation does not enable members of the family to be distinguished from other persons.
The wording under the heading "V. SLOVAKIA" should be adapted to reflect the adoption of the new Act No 600/2003 Coll. on child allowance and the amendment to the Act No 461/2003 Coll. on social insurance.
2. Amendment of Annex II section I
Annex II section I lists the special schemes for self-employed persons which are excluded from the scope of the Regulation pursuant to the fourth subparagraph of Article 1 (j).
Changes in the relevant French legislation have moved the initiative for establishing some of the schemes currently listed under heading "H. FRANCE" from the groups concerned to the national legislator. As a consequence, they no longer fulfil the condition for an exclusion from Regulation 1408/71, as required by the fourth subparagraph of Article 1 (j), but are within the normal scope of the Regulation. The reference under heading "H. FRANCE" must be amended accordingly.
3. Amendments to Annex II section II
Annex II section II lists the special childbirth or adoption allowances which are excluded from the scope of the Regulation, including its rules on exporting benefits.
The Estonian legislature grants a lump-sum family benefit which intends to compensate the one-off expenses related to the adoption of a child. It meets the same objectives as the adoption allowances cited in Article 1 u) i) of Regulation (EEC) No 1408/71 and referred to in Annex II section II. The reference under the heading "E. ESTONIA" should be amended to take this into account.
Latvian law has been amended to introduce a new child adoption allowance which consists of a lump-sum payment for adopting a child who has been placed in institutional care before adoption. The reference under the heading "L. LATVIA" should be amended to take this into account.
As a consequence of the ruling of the Court of Justice of the European Communities in the case Leclere C-43/99, the Luxembourg childbirth allowances should no longer be considered to be special childbirth allowances excluded from the scope of the Regulation but rather as exportable family benefits. They should therefore be deleted from the heading "N. LUXEMBOURG".
Polish law has been amended to introduce a one-time additional benefit to family allowances, intended to cover the additional cost related to childbirth. The reference under the heading "S. POLAND" should be amended to take this into account.
4. Amendments to Annex IIa
Annex IIa contains special non-contributory benefits granted to the persons concerned exclusively in the territory of the Member State in which they reside, under the terms of Article 10a of Regulation (EEC) No 1408/71.
German law has been amended to introduce a basic provision for jobseekers, replacing the current unemployment benefit and, in part, the subsistence allowance under social assistance. This "unemployment benefit II" is a mixed benefit containing elements of social security and social assistance and is non-contributory as it is financed by the government, thus fulfilling the criteria to be considered a special non-contributory benefit. It should be referred to under the heading "D. GERMANY". However, for persons who had previously received contributory unemployment benefits, the unemployment benefit II comprises, for the first two years, a supplement which is a percentage of the difference between the previous – contributory - unemployment benefit and the unemployment benefit II. With this supplement, the benefit is – indirectly – linked to previous income. Therefore, the reference under the heading "D. GERMANY" does not refer to unemployment benefit II in case of eligibility for this supplement.
Latvian law has been amended to substitute the previous Law on Social Assistance by the Law on State Social Benefits for the purpose of clarity. As it does not have any effects on the nature of the two benefits currently listed in Annex IIa, the reference under the heading "L. LATVIA" should be amended to reflect this change in the national legal base.
In Poland, the legal base to grant the social pension currently included in Annex IIa has been amended without changing the nature of the benefit. The reference under the heading "S. POLAND" should be amended to take this into account.
The Slovak legislature has abolished the Act on Social Security which had accrued the entitlement to special non-contributory benefits listed in Annex IIa and has replaced it by a new Act. No new entitlements are arising under the new Act, but to apply to entitlements already arisen under the old Act, the reference under the heading "V. SLOVAKIA" should be amended to reflect this change in national legislation.
5. Amendments to Annex III
Listed in Annex III are provisions of existing bilateral agreements which were in force prior to the application of the Regulation in the Member State concerned. Part A lists the provisions of bilateral agreements which continue to apply, despite the fact that the provisions of bilateral agreements are generally replaced by Regulation (EEC) 1408/71, and Part B lists those provisions of bilateral agreements which do not apply to all persons to whom the Regulation applies.
Those modifying entries made to Annex III Part A and B by the Accession Treaty which indicate either that no bilateral agreements exist, or that a bilateral agreement exists but that none of its provisions fulfil the conditions to be listed in either Part, should be deleted. They are not necessary for the application of Regulation 1408/71 and make reading Annex III difficult. Deleting these entries coincides with the proposed modifications to Annex III Part A and B by Commission proposal COM (2003) 468 final regarding Regulation 1408/71 prior to accession. All remaining entries should be renumbered.
To enter provisions to Annex III Part A can only be justified in two cases: where the provisions in question produce favourable effects for the beneficiaries of the agreement concerned, thus reflecting the case law of the Court; or where the provisions in question respond to specific, exceptional circumstances, usually historical, and the effects are limited in time by the exhaustion of potential rights for persons concerned by the specific situation in question.
The bilateral agreements between the Czech Republic and Germany, between the Czech Republic and Cyprus, between the Czech Republic and Luxembourg and between the Czech Republic and Slovakia, as well as between Germany and Slovakia, Luxembourg and Slovakia, and Austria and Slovakia contain provisions which fulfil these conditions. Therefore, the respective headings should be amended.
Entries in Annex III Part B should be limited to exceptional, objective situations, justifying derogation from Article 3 (1) of the Regulation and Articles 12, 39 and 42 of the Treaty.
Provisions of bilateral agreement between the Czech Republic and Cyprus, and between Austria and Slovakia fulfil these requirements.
As these provisions of bilateral agreements which applied at the time of accession shall continue to apply because they are included in Annex III, it is necessary that the reference has retroactive effect as of the date of accession.
6. Amendment of Annex IV
Annex IV, part A, contains the list of those "legislations referred to in Article 37 (1) of the Regulation under which the amount of invalidity benefits is independent of the length of periods of insurance".
In the Czech Republic, the level of invalidity pensions depends on the length of insurance periods (type "B"-system), with the exception of the full disability pension for persons whose complete disability arose before reaching 18 years of age and who were not insured for the required period. This benefit is provided under the pension insurance system but without any insurance deductions and thus should be included under the heading "B. CZECH REPUBLIC".
Annex IV, part C lists the "cases referred to in Article 46 (1) (b) of the Regulation where the calculation of benefits in accordance with Article 46 (2) of the Regulation may be waived" as the double pension calculation will never lead to a higher result.
Under Czech legislation, survivors' benefits can be calculated not only on the basis of the invalidity or old-age pensions which the deceased person actually received, but also on the basis of the invalidity or old-age pension to which the deceased person would have been entitled at the time of death. In this case, the "pro rata" calculation according to Article 46 (2) of the Regulation regarding the old-age pension may be higher than under national calculation and makes a double calculation necessary. The current entry under the heading "B. CZECH REPUBLIC" needs to be amended.
Under Estonian legislation, pensions based on insurance periods acquired until 31 December 1998 consist of two parts, one of which is a basic lump-sum amount. In such cases, the national calculation of a pension will always lead at least to the same amount as a calculation according to Article 46 (2) of the Regulation. Therefore, the heading "E. ESTONIA" should be amended accordingly.
Annex IV part D lists the benefits and agreements referred to in the special provisions of Article 46b (2) of the Regulation regarding the overlapping of benefits of the same kind in two or more Member States.
As Slovak legislation has been amended, abolishing the institute of "partial invalidity pension", the current entry in Annex IV, part D, under point 2) g) should be amended accordingly.
7. Amendments to Annex VI
Annex VI sets out particular methods for applying the legislation of certain Member States.
Point 4 (b) under the heading "Q. THE NETHERLANDS" should be amended to make clear that the decisive factor to determine whether a benefit should be calculated under the Dutch invalidity insurance for employed persons, or under the Dutch invalidity insurance for self-employed persons, is whether the person concerned was, at the time the incapacity for work occurred, engaged in work as an employed or self-employed person.
The entry under Point 7 of the heading "Q. THE NETHERLANDS", regarding the determination of the applicable social security legislation, should be amended to reflect that the Dutch Supreme Court has extended the qualification of certain persons as employed or self-employed person to other group of persons. This concerns persons who pursue an activity in the Netherlands for which they are, under national law, at the same time affiliated to an employed and a self-employed scheme.
4. APPLICATION IN THE COUNTRIES OF THE EUROPEAN ECONOMIC AREA AND IN THE SWISS CONFEDERATION
The free movement of persons is one of the objectives and principles of the Agreement on the European Economic Area (EEA) which came into force on 1 January 1994[4]. In Chapter 1 of the third part, concerning the free movement of persons, services and capital, Articles 28, 29 and 30 deal with the free movement of employed and self-employed workers. Article 29, more specifically, restates the principles listed in Article 42 of the EC Treaty in respect of social security for persons moving within the Community. Consequently this proposal for a Regulation must, if it is adopted, apply in the EEA member countries.
The Agreement between the European Community and its Member States, of the one part, and the Swiss confederation, of the other part, on the free movement of persons, which came into force on 1 June 2002[5], contains an Article 8 which restates the principles listed in Article 42 of the EC Treaty in respect of social security for persons moving within the Community. Consequently this proposal for a Regulation must, if it is adopted, apply in the Swiss Confederation.
2004/0284 (COD)
Proposal for a
REGULATION OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL
amending Council Regulation (EEC) No 1408/71 on the application of social security schemes to employed persons, to self-employed persons and to members of their families moving within the Community and Council Regulation (EEC) No 574/72 laying down the procedure for implementing Regulation (EEC) No 1408/71 (Text with EEA relevance)
THE EUROPEAN PARLIAMENT AND THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Articles 42 and 308 thereof,
Having regard to the proposal from the Commission[6],
Having regard to the opinion of the European Economic and Social Committee[7],
Acting in accordance with the procedure laid down in Article 251 of the Treaty[8],
Whereas:
With the entry into force of Regulation (EC) No 631/2004 of the European Parliament and of the Council[9], access to sickness benefits in kind during a temporary stay in another Member State no longer requires additional formalities other than presentation of a document. These extra requirements, specifically the obligation to submit systematically and in advance a certified statement to the institution of the place of stay certifying entitlement to benefits in kind and completing extra formalities on arrival in the territory of the other Member State, was required formally, but was not applied in practice. The same formal obligation still exists for obtaining benefits in respect of accidents at work and occupational diseases during a stay in a different Member State and also appears to be needlessly restrictive and of a nature to hamper the free movement of the persons concerned. It is therefore appropriate to extend the simplified procedures to the provisions on benefits in respect of accidents at work and occupational diseases contained in Council Regulation (EEC) No 1408/71[10] and Council Regulation (EEC) No 574/72[11].
In order to take account of changes in the legislation of certain Member States, in particular in the new Member States since the end of accession negotiations, the Annexes to Regulation (EEC) No 1408/71 need to be adapted. Certain changes also need to be made to Regulation (EEC) No 574/72.
Regulation (EEC) No 1408/71 and Regulation (EEC) No 574/72 should therefore be amended accordingly.
In order to ensure legal certainty and to protect the legitimate expectations of persons affected, it is necessary to provide that certain provisions modifying Annex III of Regulation 1408/71 shall take effect retrospectively from 1 May 2004.
The Treaty does not provide powers other than those under Article 308 to take appropriate measures within the field of social security for persons other than employed persons.
HAVE ADOPTED THIS REGULATION:
Article 1
Annexes I, II, IIa, III, IV and VI to Regulation (EEC) No 1408/71 are amended in accordance with the Annex to this Regulation.
Article 2
Regulation (EEC) No 574/72 is amended as follows:
(1) In Article 60, paragraphs 5 and 6are deleted.
(2) Article 62 is replaced by the following:
"Article 62
Benefits in kind in the case of a stay in a Member State other than the competent State
(1) In order to receive benefits in kind under Article 55 (1) (a) (i) of the Regulation, an employed or self-employed person shall submit to the care provider a document issued by the competent institution certifying that he is entitled to benefits in kind. That document shall be drawn up in accordance with Article 2. If the person concerned is not able to submit that document, he shall contact the institution of the place of stay which shall request from the competent institution a certified statement testifying that the person concerned is entitled to benefits in kind.A document issued by the competent institution for entitlement to benefits in accordance with Article 55 (1) (a) (i) of the Regulation, in each individual case concerned, shall have the same effect with regard to the care provider as national evidence of the entitlements of the persons insured with the institution of the place of stay."
(2) Article 60 (9) of the implementing Regulation shall apply by analogy."
(3) Article 63 (2) is replaced by the following:
"2. Article 60 (9) of the implementing Regulation shall apply by analogy."
(4) In Article 66 (1), the words "in Articles 20 and 21" are replaced by the words "in Article 21".
Article 3
This Regulation shall enter into force on the day following that of its publication in the Official Journal of the European Union .
Point 5 (a) (ii) to (ix) and point 5 (b) (ii) and (iv) of the Annex shall apply from 1 May 2004.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels,
For the European Parliament For the Council
The President The President
ANNEX
The annexes to Regulation (EEC) No 1408/71 are amended as follows:
In Annex I section II, the section "V. SLOVAKIA" is replaced by the following:
"V. SLOVAKIA
For the purpose of determining entitlement to benefits in kind pursuant to the provisions of Chapter 1 of title III of the Regulation, "member of the family" means a spouse and/or a dependent child as defined by the Act on child allowances".
In Annex II section I, the section "H. FRANCE" is replaced by the following:
“H. FRANCE
1. Supplementary benefit schemes for self-employed persons in craft-trade, industrial or commercial occupations or the liberal professions, supplementary old-age insurance schemes for self-employed persons in the liberal professions, supplementary insurance schemes for self-employed persons in the liberal professions covering invalidity or death, and supplementary old-age benefit schemes for contracted medical practitioners and auxiliaries, as referred to respectively in Articles L.615-20, L.644-1, L.644-2, L.645-1 and L.723-14 of the Social Security Code.
2. Supplementary sickness and maternity insurance schemes for self-employed workers in agriculture, as referred to in Article L.727-1 of the Rural Code.”
Annex II section II is amended as follows:
(a) Section "E. ESTONIA" is replaced by the following:
"E. ESTONIA
(a) Childbirth allowance
(b) Adoption allowance"
(b) Section "L. LATVIA" is replaced by the following:
"L. LATVIA
(a) Childbirth grant
(b) Child adoption allowance"
(c) Section "N. LUXEMBOURG" is replaced by the following:
"N. LUXEMBOURG
None"
(d) Section "S. POLAND" is replaced by the following:
"S. POLAND
Childbirth supplement (Act of 28 November 2003 on family benefits)".
Annex IIa is amended as follows:
(a) Under the heading "D. GERMANY", the word "none" shall be replaced by:
"Benefits to cover subsistence costs under the basic provision for jobseekers unless, with respect to these benefits, the eligibility requirements for a temporary supplement following receipt of unemployment benefit (Article 24 (1) of Book II of the Social Code) are fulfilled”
(b) Section "L. LATVIA" is replaced by the following:
"L. LATVIA
(a) State Social Security Benefit (Law on State Social Benefits of 1 January 2003)
(b) Allowance for the compensation of transportation expenses for disabled persons with restricted mobility (Law on State Social Benefits of 1 January 2003)"
(c) Section "S. POLAND" is replaced by the following:
"S. POLAND
Social pension (Act of 27 June 2003 on social pension)"
(d) Section "V. SLOVAKIA" is replaced by the following:
"V. SLOVAKIA
Adjustment of pensions as the sole source of income with their entitlement arisen according to the Act No 100/1998 Coll."
Annex III is amended as follows:
(a) Part A is amended as follows:
(i) The following points are deleted:
Points 1, 4, 10, 11, 12, 14, 15, 18, 20, 21, 25, 27, 28, 29, 30, 31, 32, 34, 35, 37, 38, 39, 41, 42, 43, 45, 46, 47, 49, 55, 56, 57, 59, 60, 63, 65, 66, 70, 76, 77, 78, 81, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 115, 116, 117, 119, 120, 123, 125, 126, 133, 134, 135, 137, 138, 141, 143, 144, 150, 151, 152, 154, 155, 158, 160, 161, 166, 167, 168, 170, 171, 174, 176, 177, 181, 182, 183, 185, 186, 189, 192, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 239, 241, 246, 247, 249, 250, 252, 253, 254, 255, 256, 257, 258, 259, 260, 261, 262, 263, 264, 266, 268, 269, 280, 281, 282, 283, 284, 285, 286, 287, 291, 292, 293, 294, 295, 296, 297.
(ii) The following numberings are changed:
of the heading BELGIUM-GERMANY from '3' to '1',
of the heading BELGIUM-FRANCE from '7' to '2,
of the heading CZECH REPUBLIC-GERMANY from '26' to '3',
of the heading CZECH REPUBLIC-CYPRUS from '33' to '4',
of the heading CZECH REPUBLIC-LUXEMBOURG from '36' to '5',
of the heading CZECH REPUBLIC-AUSTRIA from '40' to '6',
of the heading CZECH REPUBLIC-SLOVAKIA from '44' to '7',
of the heading DENMARK-FINLAND from '67' to '8',
of the heading DENMARK-SWEDEN from '68' to '9',
of the heading GERMANY-GREECE from '71' to '10',
of the heading GERMANY-SPAIN from '72' to '11',
of the heading GERMANY-FRANCE from '73' to '12',
of the heading GERMANY-LUXEMBOURG from '79' to '13',
of the heading GERMANY-HUNGARY from '80' to '14',
of the heading GERMANY-NETHERLANDS from '82' to '15',
of the heading GERMANY-AUSTRIA from '83' to '16',
of the heading GERMANY-POLAND from '84' to '17',
of the heading GERMANY-SLOVENIA from '86' to '18',
of the heading GERMANY-SLOVAKIA from '87' to '19',
of the heading GERMANY-SWEDEN from '89' to '20',
of the heading GERMANY-UNITED KINGDOM from '90' to '21',
of the heading SPAIN-PORTUGAL from '142' to '22',
of the heading FRANCE-ITALY from '149' to '23',
of the heading IRELAND-UNITED KINGDOM from '180' to '24',
of the heading ITALY-SLOVENIA from '191' to '25',
of the heading LUXEMBOURG-SLOVAKIA from '242' to '26',
of the heading HUNGARY-AUSTRIA from '248' to '27',
of the heading HUNGARY-SLOVENIA from '251' to '28',
of the heading NETHERLANDS-PORTUGAL from '267' to '29',
of the heading AUSTRIA-POLAND from '273' to '30',
of the heading AUSTRIA-SLOVENIA from '275' to '31',
of the heading AUSTRIA-SLOVAKIA from '276' to '32',
of the heading PORTUGAL-UNITED KINGDOM from '290' to '33' and
of the heading FINLAND-SWEDEN from '298' to '34'.
(iii) Under the heading "3. CZECH REPUBLIC–GERMANY", the words "no convention" are replaced by the following: "Article 39 (1) (b) and (c) of the Agreement on Social Security of 27 July 2001;Point 14 of the Final Protocol to the Agreement on Social Security of 27 July 2001"
(iv) Under the heading "4. CZECH REPUBLIC-CYPRUS", the word "none" is replaced by the following:
"Article 32 (4) of the Agreement on Social Security of 19 January 1999"
(v) Under the heading "5. CZECH REPUBLIC–LUXEMBOURG", the word "none" is replaced by the following:
"Article 52 point 8 of the Convention of 17 November 2000"
(vi) Section "7. CZECH REPUBLIC–SLOVAKIA" is replaced by the following:"7. CZECH REPUBLIC–SLOVAKIAArticles 12, 20 and 33 of the Convention on social security of 29 October 1992"
(vii) Under the heading "19. GERMANY-SLOVAKIA ", the words "no convention" shall be replaced by the following:"Article 29 (1) points 2 and 3 of the Agreement of 12 September 2002Paragraph 9 of the Final Protocol to the Agreement of 12 September 2002"
(viii) Under the heading "26. LUXEMBOURG-SLOVAKIA" the words "no convention" are replaced by the following:
"Article 50 paragraph 5 of the Treaty on social security of 23 May 2002"
(ix) Under the heading "32. AUSTRIA-SLOVAKIA", the words "no convention" are replaced by the following:"Article 34 (3) of the Agreement of 21 December 2001 on social security".
(b) Part B is amended as follows:
(i) The following points are deleted:
Points 1, 4, 10, 11, 12, 14, 15, 18, 20, 21, 25, 26, 27, 28, 29, 30, 31, 32, 34, 35, 36, 37, 38, 29, 41, 42, 43, 44, 45, 46, 47, 49, 55, 56, 57, 59, 60, 63, 65, 66, 70, 76, 77, 78, 81, 84, 87, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 115, 116, 117, 119, 120, 123, 125, 126, 133, 134, 135, 137, 138, 141, 143, 144, 150, 151, 152, 154, 155, 158, 160, 161, 166, 167, 168, 170, 171 174, 176, 177, 181, 182, 183, 185, 186, 189, 192, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 239, 241, 242, 246, 247, 249, 250, 252, 253, 254, 255, 256, 257, 258, 259, 260, 261, 262, 263, 264, 266, 268, 269, 280, 281, 282, 283, 284, 285, 286, 287, 291, 292, 293, 294, 295, 296, 297.
(ii) The following numberings are changed:
of the heading CZECH REPUBLIC-CYPRUS from '33' to '1',
of the heading CZECH REPUBLIC-AUSTRIA from '40' to '2',
of the heading GERMANY-HUNGARY from '80' to '3',
of the heading GERMANY-SLOVENIA from '86' to '4',
of the heading ITALY-SLOVENIA from '191' to '5',
of the heading HUNGARY-AUSTRIA from '248' to '6',
of the heading HUNGARY-SLOVENIA from '251' to '7',
of the heading AUSTRIA-POLAND from '273' to '8',
of the heading AUSTRIA-SLOVENIA from '275' to '9' and
of the heading AUSTRIA-SLOVAKIA from '276' to '10'.
(iii) Under the heading "1. CZECH REPUBLIC-CYPRUS", the word "none" is replaced by the following:
"Article 32 (4) of the Agreement on Social security of 19 January 1999"
(iv) Under the heading "10. AUSTRIA-SLOVAKIA ", the words "No convention" are replaced by the following:"Article 34 (3) of the Agreement of 21 December 2001 on social security".
Annex IV is amended as follows:
(a) In Section A, under the heading "B. CZECH REPUBLIC", the word "None" is replaced by the following:
"Full disability pension for persons whose complete disability arose before reaching eighteen years of age and who were not insured for the required period (Section 42 of the Pension Insurance Act No 155/1995 Coll.)".
(b) Section C is amended as follows:
(i) Section "B. CZECH REPUBLIC" is replaced by the following:"B. CZECH REPUBLICInvalidity (full and partial) and survivors' (widows', widowers' and orphans') pensions in case they are not derived from the old age pension to which the deceased would be entitled at the time of his death".
(ii) Under the heading "E. ESTONIA", the word "None" is replaced by the following:
"All applications for invalidity, old age and survivors' pensions for which
- periods of insurance in Estonia have been completed up to December 31, 1998
- person social tax of the applicant paid according to the Estonian legislation is at least average social tax for the relevant year of insurance".
(c) In Section D, point 2) (g) is replaced by the following:
"(g) Slovak invalidity pension and survivors' pension derived thereof"
Annex VI is amended as follows:
(a) Under the heading "Q. THE NETHERLANDS", point 4) (b) is replaced by the following:
"b) If, pursuant to subparagraph (a), the person concerned is entitled to a Dutch invalidity benefit, the calculation of benefits as referred to in Article 46(2) of the Regulation shall be carried out:
(i) in accordance with the provisions laid down in the WAO if, prior to the occurrence of the incapacity for work, the person concerned was last engaged in work as an employed person within the meaning of Article 1(a) of the Regulation;
(ii) in accordance with the provisions laid down in the Invalidity Insurance (Self-Employed Persons) Act (WAZ) if, prior to the occurrence of the incapacity for work, the person concerned was last engaged in work in a capacity other than that of an employed person within the meaning of Article 1(a) of the Regulation."
(b) Under the heading "Q. THE NETHERLANDS", point 7) is replaced by the following:
“7. For the purposes of applying Title II of the Regulation, a person regarded as an employed person within the meaning of the 1964 Wage Tax Act and who is insured on this basis for national insurance, is considered to be pursuing activities in paid employment”.
[1] OJ L 28, 30.1.1997
[2] OJ L 100, 6.4.2004
[3] OJ L 100, 6.4.2004
[4] OJ L 1, 3.1.1994, as amended by Decision of the EEA Joint Committee No 7/94 of 21.3.1994 (OJ L 160, 28.6.1994)
[5] OJ L 114, 30.4.2002
[6] OJ C […] […], p.[…]
[7] OJ C […] […], p.[…]
[8] OJ C […] […], p.[…]
[9] OJ L 100, 6.4.2004, p. 1.
[10] OJ L 149 of 5.7.1971, Regulation updated by Regulation (EC) No 118/97 (OJ L 28, 30.1.1997, p. 1), last amended by the Regulation (EC) No 631/2004 of the European Parliament and of the Council (OJ L 100, 6.4.2004) and repealed with effect from the date of entry into force of the Implementing Regulation by Regulation (EC) No 883/2004 of the European Parliament and of the Council (OJ L 166, 30.4.2004)
[11] OJ L 74 of 27.3.1972, Regulation updated by Regulation (EC) No 118/97 (OJ L 28, 30.1.1997, p. 1) and last amended by Regulation (EC) No 631/2004 of the European Parliament and of the Council (OJ L 100, 6.4.2004)
