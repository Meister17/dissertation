Council Decision
of 15 November 2004
concluding the consultation procedure with the Togolese Republic under Article 96 of the Cotonou Agreement
(2004/793/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community,
Having regard to the ACP-EC Partnership Agreement signed in Cotonou on 23 June 2000 ("the Cotonou Agreement") [1], and in particular Article 96 thereof,
Having regard to the Internal Agreement on measures to be taken and procedures to be followed for the implementation of the ACP-EC Partnership Agreement [2], and in particular Article 3 thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) The Government of the Togolese Republic has violated the rules defined by the essential elements referred to in Article 9 of the Cotonou Agreement.
(2) On 14 April 2004, pursuant to Article 96 of that Agreement, consultations started with the ACP countries and the Togolese Republic during which the Togolese authorities gave specific undertakings designed to remedy problems identified by the European Union, to be implemented during a period of intensive dialogue lasting three months.
(3) At the conclusion of this period some substantive initiatives have been taken in respect of some of the undertakings referred to above and some undertakings have been met. Nevertheless, several important measures concerning essential elements of the Cotonou Agreement have still to be taken,
HAS DECIDED AS FOLLOWS:
Article 1
The consultations opened with the Togolese Republic under Article 96(2)(a) of the Cotonou Agreement are hereby terminated.
Article 2
The measures set out in the draft letter in the Annex are hereby adopted as appropriate measures within the meaning of Article 96(2)(c) of the Cotonou Agreement.
Article 3
The decision of 14 December 1998 addressing a letter of the Council and the Commission to the Togolese Republic is hereby repealed.
Article 4
This decision shall enter into force on the day it is adopted. It shall be published in the Official Journal of the European Union.
It shall be valid for 24 months from the date of its adoption by Council. It shall be regularly reviewed at least every six months.
Done at Brussels, 15 November 2004.
For the Council
The President
M. van der Hoeven
--------------------------------------------------
[1] OJ L 317, 15.12.2000, p. 3.
[2] OJ L 317, 15.12.2000, p. 376.
--------------------------------------------------
+++++ ANNEX 1 +++++</br>
