Judgment of the Court of First Instance of 19 October 2005 — Cofradía de pescadores de "San Pedro" de Bermeo and Others v Council
(Case T-415/03) [1]
Parties
Applicant(s): Cofradía de pescadores de "San Pedro" de Bermeo (Bermeo, Spain) (and the other applicants whose names appear in the Annex to the judgment, represented by: E. Garayar Gutiérrez, G. Martínez-Villaseñor, A. García Castillo and M. Troncoso Ferrer, lawyers)
Defendant(s): Council of the European Union (represented by: M. Balta and F. Florindo Gijón, Agents)
Intervener(s) in support of the defendant(s): Commission of the European Communities (represented by: initially T. van Rijn and S. Pardo Quintillán, and subsequently T. van Rijn and F. Jimeno Fernández, Agents) and French Republic (represented by: G. de Bergues and A. Colomb, Agents)
Application for
damages to compensate for the loss allegedly suffered by the applicants following the Council's authorisation of the transfer to the French Republic of part of the quota for anchovy allocated to the Portuguese Republic.
Operative part of the judgment
The Court:
1) Dismisses the application;
2) Orders the applicants to bear their own costs and those of the Council;
3) Orders the French Republic and the Commission to bear their own costs.
[1] OJ C 47, 21.2.2004.
--------------------------------------------------
