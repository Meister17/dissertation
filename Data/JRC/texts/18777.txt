COMMISSION REGULATION (EEC) No 2866/93 of 20 October 1993 amending Regulation (EEC) No 1756/93 fixing the operative events for the agricultural conversion rate applicable to milk and milk products
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 3813/92 of 28 December 1992 on the unit of account and the conversion rates to be applied for the purposes of the common agricultural policy (1), and in particular Article 6 (2) thereof,
Whereas Commission Regulation (EEC) No 1756/93 (2) accidentally failed to repeal the last sentence of the last subparagraph of Article 1 (5) of Commission Regulation (EEC) No 625/78 of 30 March 1978 on detailed rules of application for public storage of skimmed-milk powder (3), as last amended by Regulation (EEC) No 2270/91 (4), and the last part of the sentence in the third subparagraph of Article 24 (4) of Commission Regulation (EEC) No 685/69 of 14 April 1969 on detailed rules of application for intervention on the market in butter and cream (5), as last amended by Regulation (EEC) No 1756/93 (6); whereas those omissions should therefore be rectified;
Whereas Regulation (EEC) No 1756/93 seeks to fix precisely the agricultural conversion rate to be applied for all amounts fixed in ecus in the milk and milk products sector; whereas that Regulation should therefore be amended so as to fix the operative events for the amounts specified in Article 3 (3) (a) and (b) and in Article 8 (2) (c) of Commission Regulation (EEC) No 1107/68 of 27 July 1968 on detailed rules of application for intervention on the market in Grana Pardano and Parmigiano-Reggiano cheeses (7), as last amended by Regulation (EEC) No 2441/93 (8);
Whereas the operative event for the agricultural conversion rate to be used for the conversion into national currency of the sum referred to in Article 4 (1) of Commission Regulation (EEC) No 2742/90 of 26 September 1990 laying down detailed rules for the application of Council Regulation (EEC) No 2204/90 (9), as last amended by Regulation (EEC) No 2146/92 (10), referred to in item 5 of Part D of the Annex to Regulation (EEC) No 1756/93 is the date of payment of the amount in question; whereas the aim of that provision is to penalize the unauthorized use of caseines and caseinates; whereas that aim should be considered to have been achieved at the time the infringement is discovered; whereas the operative event should therefore be amended appropriately;
Whereas the Management Committee for Milk and Milk Products has not delivered an opinion within the time limit set by its chairman,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EEC) No 1756/93 is amended as follows:
1. in Article 3 (1), the following indents are added:
'- the last sentence of the last subparagraph of Article 1 (5) of Regulation (EEC) No 625/78,
- the last part of the sentence in the third subparagraph of Article 24 (4) of Regulation (EEC) No 685/69.';
2. the following is added to item 1 of Part D of the Annex:
"" ID="01">D. Storage costs referred to in points (a) and (b) of the second subparagraph of Article 3 (3)> ID="02">Agricultural conversion rate applicable on the date of taking over within the meaning of the first subparagraph of Article 3 (3)"> ID="01">E. Offer price accepted under the tendering procesures referred to in Article 8 (2) (c)> ID="02">Agricultural conversion rate applicable on the date of payment' ">
3. in item 5 of Part D of the Annex, 'Agricultural conversion rate applicable on the date of payment' is replaced by 'Agricultural conversion rate applicable on the first day of the month in which the infringement is discovered'.
Article 2
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Communities.
However, point 1 of Article 1 shall apply from 1 July 1993.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 20 October 1993.
For the Commission
René STEICHEN
Member of the Commission
(1) OJ No L 387, 31. 12. 1992, p. 1.
(2) OJ No L 161, 2. 7. 1993, p. 48.
(3) OJ No L 84, 31. 3. 1978, p. 19.
(4) OJ No L 208, 30. 7. 1991, p. 35.
(5) OJ No L 90, 15. 4. 1969, p. 12.
(6) OJ No L 161, 2. 7. 1993, p. 48.
(7) OJ No L 184, 29. 7. 1968, p. 29.
(8) OJ No L 224, 3. 9. 1993, p. 5.
(9) OJ No L 264, 27. 9. 1990, p. 20.
(10) OJ No L 214, 30. 7. 1992, p. 23.
