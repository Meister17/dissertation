Council Decision
of 20 September 2005
concerning the conclusion of a Protocol to the Euro-Mediterranean Agreement establishing an Association between the European Communities and their Member States, of the one part, and the Hashemite Kingdom of Jordan, of the other part, to take account of the accession of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia, and the Slovak Republic to the European Union
(2005/735/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 310 in conjunction with the second sentence of Article 300(2), first subparagraph, and the second subparagraph of Article 300(3) thereof,
Having regard to the 2003 Act of Accession, and in particular Article 6(2) thereof,
Having regard to the proposal from the Commission,
Having regard to the assent of the European Parliament [1],
Whereas:
(1) The Protocol to the Euro-Mediterranean Agreement establishing an Association between the European Communities and their Member States, of the one part, and the Hashemite Kingdom of Jordan, of the other part, was signed on behalf of the European Community and its Member States on 31 May 2005.
(2) The Protocol should be approved,
HAS DECIDED AS FOLLOWS:
Sole Article
The Protocol to the Euro-Mediterranean Agreement establishing an Association between the European Communities and their Member States, of the one part, and the Hashemite Kingdom of Jordan, of the other part, to take account of the accession of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia, and the Slovak Republic to the European Union, is hereby approved on behalf of the European Community and its Member States.
The text of the Protocol is attached to this Decision [2].
Done at Brussels, 20 September 2005.
For the Council
The President
M. Beckett
[1] Assent delivered on 6 September 2005 (not yet published in the Official Journal).
[2] See page 3 of this Official Journal.
--------------------------------------------------
