Commission Decision
of 5 December 2001
amending its rules of procedure
(notified under document number C(2001) 3714)
(2001/937/EC, ECSC, Euratom)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community, and in particular Article 218(2) thereof,
Having regard to the Treaty establishing the European Coal and Steel Community, and in particular Article 16 thereof,
Having regard to the Treaty establishing the European Atomic Energy Community, and in particular Article 131 thereof,
Having regard to the Treaty on European Union, and in particular Article 28(1) and Article 41(1) thereof,
HAS DECIDED AS FOLLOWS:
Article 1
The detailed rules for the application of Regulation (EC) No 1049/2001 of the European Parliament and of the Council of 30 May 2001 regarding public access to European Parliament, Council and Commission documents(1), which are set out in the Annex to this Decision, are annexed to the Commission's Rules of Procedure.
Article 2
Commission Decision 94/90/ECSC, EC, Euratom(2) is repealed.
Article 3
The Decision shall enter into force on the day of its publication in the Official Journal of the European Communities.
Done at Brussels, 5 December 2001.
For the Commission
The President
Romano Prodi
(1) OJ L 145, 31.5.2001, p. 43.
(2) OJ L 46, 18.2.1994, p. 58.
ANNEX
Detailed rules for the application of Regulation (EC) No 1049/2001 of the European Parliament and of the Council regarding public access to European Parliament, Council and Commission documents
Whereas:
(1) In accordance with Article 255(2) of the EC Treaty, the European Parliament and the Council adopted Regulation (EC) No 1049/2001 regarding public access to European Parliament, Council and Commission documents.(1)
(2) In accordance with Article 255(3) of the Treaty, Article 18 of the Regulation, which lays down general principles and limits for the exercise of the right of access to documents, provides that each institution is to adapt its Rules of Procedure to the provisions of the Regulation,
Article 1
Beneficiaries
Citizens of the Union and natural or legal persons residing or having their registered office in a Member State shall exercise their right of access to Commission documents under Article 255(1) of the Treaty and Article 2(1) of Regulation (EC) No 1049/2001 in accordance with these detailed rules. This right of access concerns documents held by the Commission, that is to say, documents drawn up or received by it and in its possession.
Pursuant to Article 2(2) of Regulation (EC) No 1049/2001, citizens of third countries not residing in a Member State and legal persons not having their registered in one of the Member States shall enjoy the right of access to Commission documents on the same terms as the beneficiaries referred to in Article 255(1) of the Treaty.
However, pursuant to Article 195(1) of the Treaty, they shall not have the option of laying a complaint before the European Ombudsman. But if the Commission wholly or partly refuses them access to a document after a confirmatory application, they may bring an action before the Court of First Instance of the European Communities in accordance with the fourth paragraph of Article 230 of the Treaty.
Article 2
Access applications
All applications for access to a document shall be sent by mail, fax or e-mail to the Secretariat-General of the Commission or to the relevant Directorate-General or department. The addresses to which applications are to be sent shall be published in the practical guide referred to in Article 8 of these Rules.
The Commission shall answer initial and confirmatory access applications within fifteen working days from the date of registration of the application. In the case of complex or bulky applications, the deadline may be extended by fifteen working days. Reasons must be given for any extension of the deadline and it must be notified to the applicant beforehand.
If an application is imprecise, as referred to in Article 6(2) of Regulation (EC) No 1049/2001, the Commission shall invite the applicant to provide additional information making it possible to identify the documents requested; the deadline for reply shall run only from the time when the Commission has this information.
Any decision which is even partly negative shall state the reason for the refusal based on one of the exceptions listed in Article 4 of Regulation (EC) No 1049/2001 and shall inform the applicant of the remedies available to him.
Article 3
Treatment of initial applications
Without prejudice to Article 9 of these Rules, as soon as the application is registered, an acknowledgement of receipt shall be sent to the applicant, unless the answer can be sent by return post.
The acknowledgement of receipt and the answer shall be sent in writing, where appropriate, by electronic means.
The applicant shall be informed of the response to his application either by the Director-General or the head of department concerned, or by a Director designated for this purpose in the Secretariat-General or by a Director designated in the OLAF where the application concerns documents concerning OLAF activities referred to in Article 2(1) and (2) of Commission Decision 1999/352/EC, ECSC, Euratom(2) establishing OLAF, or by a member of staff they have designated for this purpose.
Any answer which is even partly negative shall inform the applicant of his right to submit, within fifteen working days from receipt of the answer, a confirmatory application to the Secretary-General of the Commission or to the Director of OLAF where the confirmatory application concerns documents concerning OLAF activities referred to in Article 2(1) and (2) of Decision 1999/352/EC, ECSC, Euratom.
Article 4
Treatment of confirmatory applications
In accordance with Article 14 of the Commission's Rules of Procedure, the power to take decisions on confirmatory applications is delegated to the Secretary-General. However, where the confirmatory application concerns documents concerning OLAF activities referred to in Article 2(1) and (2) of Decision 1999/352/EC, ECSC, Euratom, the decision-making power is delegated to the Director of OLAF.
The Directorate-General or department shall assist the Secretariat-General in the preparation of the decision.
The decision shall be taken by the Secretary-General or by the Director of OLAF after agreement of the Legal Service.
The decision shall be notified to the applicant in writing, where appropriate by electronic means, and inform him of his right to bring an action before the Court of First Instance or to lodge a complaint with the European Ombudsman.
Article 5
Consultations
1. Where the Commission receives an application for access to a document which it holds but which originates from a third party, the Directorate-General or department holding the document shall check whether one of the exceptions provided for by Article 4 of Regulation (EC) No 1049/2001 applies. If the document requested is classified under the Commission's security rules, Article 6 of these Rules shall apply.
2. If, after that examination, the Directorate-General or department holding the document considers that access to it must be refused under one of the exceptions provided for by Article 4 of Regulation (EC) No 1049/2001, the negative answer shall be sent to the applicant without consultation of the third-party author.
3. The Directorate-General or department holding the document shall grant the application without consulting the third-party author where:
(a) the document requested has already been disclosed either by its author or under the Regulation or similar provisions;
(b) the disclosure, or partial disclosure, of its contents would not obviously affect one of the interests referred to in Article 4 of Regulation (EC) No 1049/2001.
4. In all the other cases, the third-party author shall be consulted. In particular, if the application for access concerns a document originating from a Member State, the Directorate-General or department holding the document shall consult the originating authority where:
(a) the document was forwarded to the Commission before the date from which Regulation (EC) No 1049/2001 applies;
(b) the Member State has asked the Commission not to disclose the document without its prior agreement, in accordance with Article 4(5) of Regulation (EC) No 1049/2001.
5. The third-party author consulted shall have a deadline for reply which shall be no shorter than five working days but must enable the Commission to abide by its own deadlines for reply. In the absence of an answer within the prescribed period, or if the third party is untraceable or not identifiable, the Commission shall decide in accordance with the rules on exceptions in Article 4 of Regulation (EC) No 1049/2001, taking into account the legitimate interests of the third party on the basis of the information at its disposal.
6. If the Commission intends to give access to a document against the explicit opinion of the author, it shall inform the author of its intention to disclose the document after a ten-working day period and shall draw his attention to the remedies available to him to oppose disclosure.
7. Where a Member State receives an application for access to a document originating from the Commission, it may, for the purposes of consultation, contact the Secretariat-General, which shall be responsible for determining the Directorate-General or department responsible for the document within the Commission. The issuing Directorate-General or department of the document reply to the application after consulting the Secretariat-General.
Article 6
Treatment of applications for access to classified documents
Where an application for access concerns a sensitive document as defined in Article 9(1) of Regulation (EC) No 1049/2001, or another document classified under the Commission's security rules, it shall be handled by officials entitled to acquaint themselves with the document.
Reasons shall be given on the basis of the exceptions listed in Article 4 of Regulation (EC) No 1049/2001 for any decision refusing access to all or part of a classified document. If it proves that access to the requested document cannot be refused on the basis of these exceptions, the official handling the application shall ensure that the document is declassified before sending it to the applicant.
The agreement of the originating authority shall be required if access is to be given to a sensitive document.
Article 7
Exercise of the right of access
Documents shall be sent by mail, fax or, if available, by e-mail, depending on the application. If documents are voluminous or difficult to handle, the applicant may be invited to consult the documents on the spot. This consultation shall be free.
If the document has been published, the answer shall consist of the publication references and/or the place where the document is available and where appropriate of its web address on the EUROPA site.
If the volume of the documents requested exceeds twenty pages, the applicant may be charged a fee of EUR 0,10 per page plus carriage costs. The charges for other media shall be decided case by case but shall not exceed a reasonable amount.
Article 8
Measures facilitating access to the documents
1. The coverage of the register provided for by Article 11 of Regulation (EC) No 1049/2001 shall be extended gradually. It shall be announced on the EUROPA homepage.
The register shall contain the title of the document (in the languages in which it is available), its serial number and other useful references, an indication of its author and the date of its creation or adoption.
A help page (in all official languages) shall inform the public how the document can be obtained. If the document is published, there shall be a link to the full text.
2. The Commission shall draw up a practical guide to inform the public of their rights under Regulation (EC) No 1049/2001. The guide shall be distributed in all official languages on the EUROPA site and in booklet form.
Article 9
Documents directly accessible to the public
1. This Article applies only to documents drawn up or received after the date from which Regulation (EC) No 1049/2001 applies.
2. The following documents shall be automatically provided on request and, as far as possible, made directly accessible by electronic means:
(a) agendas for Commission meetings;
(b) ordinary minutes of Commission meetings, after approval;
(c) documents adopted by the Commission for publication in the Official Journal of the European Communities;
(d) documents originating from third parties which have already been disclosed by their author or with his consent;
(e) documents already disclosed following a previous application.
3. If it is clear that none of the exceptions provided for in Article 4 of Regulation (EC) No 1049/2001 is applicable to them, the following documents may be made available, as far as possible by electronic means, provided they do not reflect opinions or individual positions:
(a) after the adoption of a proposal for an act of the Council or of the European Parliament and of the Council, preparatory documents for that proposal that were submitted to the College during the adoption process;
(b) after the adoption of an act by the Commission under the implementing powers conferred on it, preparatory documents for that act submitted to the College during the adoption process;
(c) after the adoption by the Commission of an act under its own powers, or of a communication, report or working document, preparatory documents for that document submitted to the College during the adoption process.
Article 10
Internal organisation
The Directors-General and heads of department shall have the power to decide on the action to be taken on initial applications. To this end, they shall designate an official to consider access applications and coordinate the response of his Directorate-General or department.
Answers to initial applications shall be sent to the Secretariat-General for information.
Confirmatory applications shall be sent for information to the Directorate-General or department which answered the initial application.
The Secretariat-General shall ensure coordination and uniform implementation of these rules by Commission Directorates-General and departments. To this end, it shall provide all necessary advice and guidelines.
(1) OJ L 145, 31.5.2001, p. 43.
(2) OJ L 136, 31.5.1999, p. 20.
