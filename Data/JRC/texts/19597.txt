Commission Regulation (EC) No 224/2003
of 5 February 2003
determining the aid referred to in Council Regulation (EC) No 1255/1999 for the private storage of butter and cream and derogating from Article 29 of Regulation (EC) No 2771/1999 laying down detailed rules for the application of Regulation (EC) No 1255/1999 as regards intervention on the market in butter and cream
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1255/1999 of 17 May 1999 on the common organisation of the market in milk and milk products(1), as last amended by Commission Regulation (EC) No 509/2002(2), and in particular Article 10 thereof,
Whereas:
(1) Article 34(2) of Commission Regulation (EC) No 2771/1999(3), as last amended by Regulation (EC) No 1614/2001(4), stipulates that, without prejudice to Article 38 of that Regulation, the amount of the aid referred to in Article 6(3) of Regulation (EC) No 1255/1999 for private storage is to be fixed each year. To this end, account should be taken of the fixed, daily and financial costs of storage, and of the movements in the European Central Bank's interest rate in the case of the financial costs.
(2) Article 29(1) of Regulation (EC) No 2771/1999 stipulates the period in which entry into store must take place. The current situation on the butter market justifies bringing the entry date of 15 March for butter and cream storage operations in 2003 forward to 1 March, as an exceptional measure.
(3) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Milk and Milk Products,
HAS ADOPTED THIS REGULATION:
Article 1
The aid referred to in Article 6(3) of Regulation (EC) No 1255/1999 shall be calculated per tonne of butter or butter equivalent for contracts concluded in 2003 on the following basis:
(a) EUR 24 for the fixed costs,
(b) EUR 0,35 for the costs of cold storage for each day of contractual storage, and
(c) an amount per day of contractual storage, calculated on the basis of 91 % of the intervention price for butter in force on the day the contractual storage begins and on the basis of an annual interest rate of 2,75 %.
Article 2
Article 29(1) of Regulation (EC) No 2771/1999 notwithstanding, entry into store in 2003 may take place from 1 March.
Article 3
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 5 February 2003.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 160, 26.6.1999, p. 48.
(2) OJ L 79, 22.3.2002, p. 15.
(3) OJ L 333, 24.12.1999, p. 11.
(4) OJ L 214, 8.8.2001, p. 20.
