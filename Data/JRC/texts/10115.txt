Commission Regulation (EC) No 734/2006
of 16 May 2006
laying down detailed rules for the application of Council Regulation (EC) No 1255/1999 as regards the granting of private storage aid for certain cheeses in the 2006/2007 storage period
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1255/1999 of 17 May 1999 on the common organisation of the market in milk and milk products [1], and in particular Article 10 thereof,
Whereas:
(1) Under Article 9 of Regulation (EC) No 1255/1999, private storage aid may be granted for long-keeping cheeses and for cheeses which are manufactured from sheep’s and/or goat’s milk and require at least six months for maturing, if for those cheeses price developments and the stock situation indicate a serious imbalance of the market which may be eliminated or reduced by seasonal storage.
(2) The seasonal nature of the production of certain long-keeping cheeses and Pecorino Romano, Kefalotyri and Kasseri cheese is aggravated by the fact that the seasonality of consumption is the inverse of the seasonality of production. The fragmented production of such cheeses further aggravates the consequences of that seasonality. Therefore, provision should be made for recourse to seasonal storage in respect of a quantity corresponding to the difference between summer and winter production.
(3) The types of cheeses eligible for aid and the maximum quantities which may qualify for it should be laid down, as well as the duration of the contracts in relation to the real requirements of the market and the keeping qualities of the cheeses in question.
(4) It is necessary to specify the terms of the storage contract and the essential measures to enable the cheese covered by a contract to be identified and subjected to checks. The amount of aid should be fixed with reference to storage costs and the balance to be maintained between cheeses qualifying for the aid and other cheeses marketed. In view of these elements, and of the available resources, the amounts per day of storage for the warehousing costs should be reduced. The amount for the financial costs should be calculated on the basis of an interest rate of 2,5 %. As for the fixed costs, the aid measure should no longer compensate for other than daily storage costs and financial costs, as storage does not give raise to supplementary fixed costs, being part of the normal manufacturing process of those cheeses.
(5) Detailed rules should also be laid down regarding documentation, accounting and the frequency and nature of checks. In this connection, it should be laid down that the Member States may charge the costs of checks fully or in part to the contractor.
(6) It should be clarified that only whole cheeses are eligible for the private storage aid.
(7) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Milk and Milk Products,
HAS ADOPTED THIS REGULATION:
Article 1
Subject matter
This Regulation lays down the detailed rules for granting Community aid for private storage of certain cheeses (hereinafter referred to as aid) pursuant to Article 9 of Regulation (EC) No 1255/1999 during the 2006/2007 storage period.
Article 2
Definitions
For the purpose of this Regulation:
(a) "storage lot" means a quantity of cheese weighing at least two tonnes, of the same type and taken into storage in a single storage depot on a single day;
(b) "day of commencement of contractual storage" means the day following that of entry into storage;
(c) "last day of contractual storage" means the day before that of removal from storage;
(d) "storage period" means the period during which the cheese can be covered by the private storage scheme, as specified for each type of cheese in the Annex.
Article 3
Cheeses eligible for aid
1. Aid shall be granted in respect of certain long-keeping cheeses, Pecorino Romano, Kefalotyri and Kasseri cheese under the terms laid down in the Annex. Only whole cheeses are eligible.
2. The cheeses must have been manufactured in the Community and satisfy the following conditions:
(a) be indelibly marked with an indication of the undertaking in which they were manufactured and of the day and month of manufacture; the above details may be in code form;
(b) have undergone quality tests which establish their classification after maturing in the categories laid down in the Annex.
Article 4
Storage contract
1. Contracts relating to the private storage of cheese shall be concluded between the intervention agency of the Member State on whose territory the cheese is stored and natural or legal persons, hereinafter called "contractors".
2. Storage contracts shall be drawn up in writing on the basis of an application to draw up a contract.
Applications must reach intervention agencies within no more than 30 days of the date of entry into storage and may relate only to lots of cheese which have been fully taken into storage. The intervention agencies shall register the date on which each application is received.
If the application reaches the intervention agency within 10 working days following the deadline, the storage contract may still be concluded but the aid shall be reduced by 30 %.
3. Storage contracts shall be concluded for one or more storage lots and shall include, in particular, provisions concerning:
(a) the quantity of cheese to which the contract applies;
(b) the dates relating to the execution of the contract;
(c) the amount of aid;
(d) the identity of the storage depots.
4. Storage contracts shall be concluded within no more than 30 days of the date of registration of the application to draw up a contract.
5. Inspection measures, particularly those referred to in Article 7, shall be the subject of specifications drawn up by the intervention agency. The storage contract shall refer to those specifications.
Article 5
Entry into and removal from storage
1. The periods of entry into and removal from storage shall be as laid down in the Annex.
2. Removal from storage shall be in whole storage lots.
3. Where, at the end of the first 60 days of contractual storage, the deterioration in the quality of the cheese is greater than is normal in store, contractors may be authorised, once per storage lot, to replace the defective quantity, at their own expense.
If checks during storage or on removal from storage reveal defective quantities, no aid may be paid for those quantities. In addition, the part of the lot which is still eligible for aid may not be less than two tonnes.
The second subparagraph shall apply where part of a lot is removed before the start of the period of removal from storage referred to in paragraph 1 or before expiry of the minimum storage period referred to in Article 8(2).
4. For the purpose of calculating the aid in the case referred to in the first subparagraph of paragraph 3, the first day of contractual storage shall be the day of commencement of contractual storage.
Article 6
Storage conditions
1. The Member State shall ensure that all the conditions granting entitlement to payment of the aid are fulfilled.
2. The contractor or, at the request of the Member State or with its authorisation, the person responsible for the storage depot shall make available to the competent authority responsible for inspection any documentation permitting verification of the following particulars of products placed in private storage:
(a) ownership at the time of entry in storage;
(b) the origin and the date of manufacture of the cheeses;
(c) the date of entry in storage;
(d) presence in the store and the address of the store;
(e) the date of removal from storage.
3. The contractor or, where applicable, the person responsible for the storage depot shall keep stock records available at the depot for each contract, covering:
(a) the identification, by storage lot number, of the products placed in private storage;
(b) the dates of entry into and removal from storage;
(c) the number of cheeses and their weight by storage lot;
(d) the location of the products in the store.
4. Products stored must be easily identifiable, easily accessible and identified individually by contract. A special mark shall be affixed to stored cheeses.
Article 7
Checks
1. On entry into storage the competent agency shall conduct checks, in particular to ensure that products stored are eligible for the aid and to prevent any possibility of substitution of products during storage under contract.
2. The competent agency shall make an unannounced check, by sampling, to ensure that the products are present in the storage depot. The sample concerned must be representative and must correspond to at least 10 % of the overall quantity under contract for a private storage aid measure.
Such checks must include, in addition to an examination of the accounts referred to in Article 6(3), a physical check of the weight and type of products and their identification. Such physical checks must relate to at least 5 % of the quantity subjected to the unannounced check.
3. At the end of the contractual storage period, the competent agency shall check to see that the products are present. However, where the products are still in storage after expiry of the maximum contractual storage period, this check may be made when the products are removed from storage.
For the purposes of the check referred to in the first subparagraph, the contractor shall notify the competent authority, indicating the storage lots concerned, at least five working days before the expiry of the contractual storage period or the start of the removal operations, where these take place during or after the contractual storage period.
The Member State may accept a shorter time-limit than the five working days specified in the second subparagraph.
4. A report shall be drawn up on the checks carried out pursuant to paragraphs 1, 2 and 3, specifying:
(a) the date of the check;
(b) its duration;
(c) the operations carried out.
The report must be signed by the inspector responsible and countersigned by the contractor or, as the case may be, the person responsible for the storage depot, and must be included in the payment dossier.
5. In the case of irregularities affecting at least 5 % of the quantities of products checked, the check shall be extended to a larger sample to be determined by the competent authority.
The Member States shall notify such cases to the Commission within four weeks.
6. Member States may provide that the costs of checks are to be fully or in part charged to the contractor.
Article 8
Storage aid
1. The aid shall be as follows:
(a) EUR 0,10 per tonne per day of storage under contract for the warehousing costs;
(b) for the financial costs per day of contractual storage:
(i) EUR 0,28 per tonne for long-keeping cheeses;
(ii) EUR 0,35 per tonne for Pecorino Romano;
(iii) EUR 0,49 per tonne for Kefalotyri and Kasseri.
2. No aid shall be granted in respect of storage under contract for less than 60 days. The maximum aid payable shall not exceed an amount corresponding to 180 days storage under contract.
Where the contractor fails to comply with the time-limit referred to in the second or, as the case may be, third subparagraph of Article 7(3), the aid shall be reduced by 15 % and shall be paid only in respect of the period for which the contractor supplies satisfactory proof to the competent agency that the cheeses have remained in contractual storage.
3. The aid shall be paid on application by the contractor, at the end of the contractual storage period, within 120 days of receipt of the application, provided that the checks referred to in Article 7(3) have been carried out and that the conditions for entitlement to the aid have been met.
However, if it has been necessary to commence an administrative inquiry into entitlement to the aid, payment shall not be made until entitlement has been recognised.
Article 9
Entry into force
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 16 May 2006.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 160, 26.6.1999, p. 48. Regulation last amended by Regulation (EC) No 1913/2005 (OJ L 307, 25.11.2005, p. 2).
--------------------------------------------------
ANNEX
Categories of cheeses | Quantities eligible for aid | Minimum age for cheeses | Period of entry into storage | Period of removal from storage |
French long-keeping cheeses: protected designation of origin Beaufort and Comté cheeses"Label Rouge" Emmental grand cruclass A or B Emmental and Gruyère cheeses | 16000 t | 10 days | From 1 June to 30 September 2006 | From 1 October 2006 to 31 March 2007 |
German long-keeping cheeses: "Markenkäse" or "Klasse fein" Emmentaler/Bergkäse | 1000 t | 10 days | From 1 June to 30 September 2006 | From 1 October 2006 to 31 March 2007 |
Irish long-keeping cheeses: "Irish long-keeping cheese. Emmental, special grade" | 900 t | 10 days | From 1 June to 30 September 2006 | From 1 October 2006 to 31 March 2007 |
Austrian long-keeping cheeses: "1.Güteklasse Emmentaler/Bergkäse/Alpkäse" | 1700 t | 10 days | From 1 June to 30 September 2006 | From 1 October 2006 to 31 March 2007 |
Finnish long-keeping cheeses: "I luokka" | 1700 t | 10 days | From 1 June to 30 September 2006 | From 1 October 2006 to 31 March 2007 |
Swedish long-keeping cheeses: "Västerbotten/Prästost/Svecia/Grevé" | 1700 t | 10 days | From 1 June to 30 September 2006 | From 1 October 2006 to 31 March 2007 |
Polish long-keeping cheeses: "Podlaski/Piwny/Ementalski/Ser Corregio/Bursztyn/Wielkopolski" | 3000 t | 10 days | From 1 June to 30 September 2006 | From 1 October 2006 to 31 March 2007 |
Slovenian long-keeping cheeses: "Ementalec/Zbrinc" | 200 t | 10 days | From 1 June to 30 September 2006 | From 1 October 2006 to 31 March 2007 |
Lithuanian long-keeping cheeses: "Goja/Džiugas" | 700 t | 10 days | From 1 June to 30 September 2006 | From 1 October 2006 to 31 March 2007 |
Latvian long-keeping cheeses: "Rigamond, Ementāles tipa un Ekstra klases siers" | 500 t | 10 days | From 1 June to 30 September 2006 | From 1 October 2006 to 31 March 2007 |
Hungarian long-keeping cheeses: "Hajdú" | 300 t | 10 days | From 1 June to 30 September 2006 | From 1 October 2006 to 31 March 2007 |
Pecorino Romano | 19000 t | 90 days and produced after 1 October 2005 | From 1 June to 31 December 2006 | Before 31 March 2007 |
Kefalotyri and Kasseri made from sheep’s or goat’s milk or a mixture of the two | 2500 t | 90 days and produced after 30 November 2005 | From 1 June to 30 November 2006 | Before 31 March 2007 |
--------------------------------------------------
