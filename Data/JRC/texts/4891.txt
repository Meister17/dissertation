Commission Directive 2005/61/EC
of 30 September 2005
implementing Directive 2002/98/EC of the European Parliament and of the Council as regards traceability requirements and notification of serious adverse reactions and events
(Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Directive 2002/98/EC of the European Parliament and of the Council of 27 January 2003 setting standards of quality and safety for the collection, testing, processing, storage and distribution of human blood and blood components and amending Directive 2001/83/EC [1], and in particular points (a) and (i) of the second paragraph of Article 29 thereof,
Whereas:
(1) Directive 2002/98/EC lays down standards of quality and safety for the collection and testing of human blood and blood components, whatever their intended purpose, and for their processing, storage and distribution when intended for transfusion so as to ensure a high level of human health protection.
(2) In order to prevent the transmission of diseases by blood and blood components and to ensure an equivalent level of quality and safety, Directive 2002/98/EC calls for the establishment of specific technical requirements dealing with traceability, a Community procedure for notifying serious adverse reactions and events and the notification format.
(3) Notification of suspected serious adverse reactions or serious adverse events should be submitted to the competent authority as soon as known. This Directive therefore establishes the notification format defining the minimum data needed, without prejudice to the faculty of Member States to maintain or introduce in their territory more stringent protective measures which comply with the provisions of the Treaty as provided under Article 4(2) of Directive 2002/98/EC.
(4) This Directive lays down those technical requirements, which take account of Council Recommendation 98/463/EC of 29 June 1998 on the suitability of blood and plasma donors and the screening of donated blood in the European Community [2], Directive 2001/83/EC of the European Parliament and of the Council of 6 November 2001 on the Community code relating to medicinal products for human use [3], Commission Directive 2004/33/EC of 22 March 2004 implementing Directive 2002/98/EC of the European Parliament and of the Council as regards certain technical requirements for blood and blood components [4], and certain recommendations of the Council of Europe.
(5) Accordingly, blood and blood components imported from third countries, including those used as starting material or raw material for the manufacture of medicinal products derived from human blood and human plasma, intended for distribution in the Community, should meet equivalent Community standards and specifications relating to traceability and serious adverse reaction and serious adverse event notification requirements as set out in this Directive.
(6) It is necessary to determine common definitions for technical terminology in order to ensure the consistent implementation of Directive 2002/98/EC.
(7) The measures provided for in this Directive are in accordance with the opinion of the Committee set up by Directive 2002/98/EC,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Definitions
For the purposes of this Directive, the following definitions shall apply:
(a) "traceability" means the ability to trace each individual unit of blood or blood component derived thereof from the donor to its final destination, whether this is a recipient, a manufacturer of medicinal products or disposal, and vice versa;
(b) "reporting establishment" means the blood establishment, the hospital blood bank or facilities where the transfusion takes place that reports serious adverse reactions and/or serious adverse events to the competent authority;
(c) "recipient" means someone who has been transfused with blood or blood components;
(d) "issue" means the provision of blood or blood components by a blood establishment or a hospital blood bank for transfusion to a recipient;
(e) "imputability" means the likelihood that a serious adverse reaction in a recipient can be attributed to the blood or blood component transfused or that a serious adverse reaction in a donor can be attributed to the donation process;
(f) "facilities" means hospitals, clinics, manufacturers, and bio-medical research institutions to which blood or blood components may be delivered.
Article 2
Traceability
1. Member States shall ensure the traceability of blood and blood components through accurate identification procedures, record maintenance and an appropriate labelling system.
2. Member States shall ensure that the traceability system in place in the blood establishment enables the tracing of blood components to their location and processing stage.
3. Member States shall ensure that every blood establishment has a system in place to uniquely identify each donor, each blood unit collected and each blood component prepared, whatever its intended purpose, and the facilities to which a given blood component has been delivered.
4. Member States shall ensure that all facilities have a system in place to record each blood unit or blood component received, whether or not locally processed, and the final destination of that received unit, whether transfused, discarded or returned to the distributing blood establishment.
5. Member States shall ensure that every blood establishment has a unique identifier that enables it to be precisely linked to each unit of blood that it has collected and to each blood component that it has prepared.
Article 3
Verification procedure for issuing blood or blood components
Member States shall ensure that the blood establishment, when it issues units of blood or blood components for transfusion, or the hospital blood bank has in place a procedure to verify that each unit issued has been transfused to the intended recipient or if not transfused to verify its subsequent disposition.
Article 4
Record of data on traceability
Member States shall ensure that blood establishments, hospital blood banks, or facilities retain the data set out in Annex I for at least 30 years in an appropriate and readable storage medium in order to ensure traceability.
Article 5
Notification of serious adverse reactions
1. Member States shall ensure that those facilities where transfusion occurs have procedures in place to retain the record of transfusions and to notify blood establishments without delay of any serious adverse reactions observed in recipients during or after transfusion which may be attributable to the quality or safety of blood and blood components.
2. Member States shall ensure that reporting establishments have procedures in place to communicate to the competent authority as soon as known all relevant information about suspected serious adverse reactions. The notification formats set out in Part A and Part C of Annex II shall be used.
3. Member States shall ensure that reporting establishments:
(a) notify to the competent authority all relevant information about serious adverse reactions of imputability level 2 or 3, as referred to in Part B of Annex II, attributable to the quality and safety of blood and blood components;
(b) notify the competent authority of any case of transmission of infectious agents by blood and blood components as soon as known;
(c) describe the actions taken with respect to other implicated blood components that have been distributed for transfusion or for use as plasma for fractionation;
(d) evaluate suspected serious adverse reactions according to the imputability levels set out in Part B of Annex II;
(e) complete the serious adverse reaction notification, upon conclusion of the investigation, using the format set out in Part C of Annex II;
(f) submit a complete report on serious adverse reactions to the competent authority on an annual basis using the format set out in Part D of Annex II.
Article 6
Notification of serious adverse events
1. Member States shall ensure that blood establishments and hospital blood banks have procedures in place to retain the record of any serious adverse events which may affect the quality or safety of blood and blood components.
2. Member States shall ensure that reporting establishments have procedures in place to communicate to the competent authority as soon as known, using the notification format set out in Part A of Annex III, all relevant information about serious adverse events which may put in danger donors or recipients other than those directly involved in the event concerned.
3. Member States shall ensure that reporting establishments:
(a) evaluate serious adverse events to identify preventable causes within the process;
(b) complete the serious adverse event notification, upon conclusion of the investigation, using the format set out in Part B of Annex III;
(c) submit a complete report on serious adverse events to the competent authority on an annual basis using the format set out in Part C of Annex III.
Article 7
Requirements for imported blood and blood components
1. Member States shall ensure that for imports of blood and blood components from third countries blood establishments have a system of traceability in place equivalent to that provided for in Article 2(2) to (5).
2. Member States shall ensure that for imports of blood and blood components from third countries blood establishments have a system of notification in place equivalent to that provided for in Articles 5 and 6.
Article 8
Annual reports
Member States shall submit to the Commission an annual report, by 30 June of the following year, on the notification of serious adverse reactions and events received by the competent authority using the formats in Part D of Annex II and Part C of Annex III.
Article 9
Communication of information between competent authorities
Member States shall ensure that their competent authorities communicate to each other such information as is appropriate with regard to serious adverse reactions and events in order to guarantee that blood or blood components known or suspected to be defective are withdrawn from use and discarded.
Article 10
Transposition
1. Without prejudice to Article 7 of Directive 2002/98/EC, Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive by 31 August 2006 at the latest. They shall forthwith communicate to the Commission the text of those provisions and a correlation table between those provisions and this Directive.
When Member States adopt those provisions, they shall contain a reference to this Directive or be accompanied by such a reference on the occasion of their official publication. Member States shall determine how such reference is to be made.
2. Member States shall communicate to the Commission the text of the main provisions of national law which they adopt in the field covered by this Directive.
Article 11
Entry into force
This Directive shall enter into force on the 20th day following that of its publication in the Official Journal of the European Union.
Article 12
Addressees
This Directive is addressed to the Member States.
Done at Brussels, 30 September 2005.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 33, 8.2.2003, p. 30.
[2] OJ L 203, 21.7.1998, p. 14.
[3] OJ L 311, 28.11.2001, p. 67. Directive as last amended by Directive 2004/27/EC (OJ L 136, 30.4.2004, p. 34).
[4] OJ L 91, 30.3.2004, p. 25.
--------------------------------------------------
ANNEX I
Record of data on traceability as provided for in Article 4
BY BLOOD ESTABLISHMENTS
1. Blood establishment identification
2. Blood donor identification
3. Blood unit identification
4. Individual blood component identification
5. Date of collection (year/month/day)
6. Facilities to which blood units or blood components are distributed, or subsequent disposition.
BY FACILITIES
1. Blood component supplier identification
2. Issued blood component identification
3. Transfused recipient identification
4. For blood units not transfused, confirmation of subsequent disposition
5. Date of transfusion or disposition (year/month/day)
6. Lot number of the component, if relevant.
--------------------------------------------------
ANNEX II
NOTIFICATION OF SERIOUS ADVERSE REACTIONS
PART A
Rapid notification format for suspected serious adverse reactions
Reporting establishment
Report identification
Reporting date (year/month/day)
Date of transfusion (year/month/day)
Age and sex of recipient
Date of serious adverse reaction (year/month/day)
Serious adverse reaction is related to
- Whole blood
- Red blood cells
- Platelets
- Plasma
- Other (specify)
Type of serious adverse reaction(s)
- Immunological haemolysis due to ABO incompatibility
- Immunological haemolysis due to other allo-antibody
- Non-immunological haemolysis
- Transfusion-transmitted bacterial infection
- Anaphylaxis/hypersensitivity
- Transfusion related acute lung injury
- Transfusion-transmitted viral infection (HBV)
- Transfusion-transmitted viral infection (HCV)
- Transfusion-transmitted viral infection (HIV-1/2)
- Transfusion-transmitted viral infection, Other (specify)
- Transfusion-transmitted parasitical infection (Malaria)
- Transfusion-transmitted parasitical infection, Other (specify)
- Post-transfusion purpura
- Graft versus host disease
- Other serious reaction(s) (specify)
Imputability level (NA, 0-3)
PART B
Serious adverse reactions — imputability levels
Imputability levels to assess serious adverse reactions.
Imputability level | Explanation |
NA | Not assessable | When there is insufficient data for imputability assessment. |
0 | Excluded | When there is conclusive evidence beyond reasonable doubt for attributing the adverse reaction to alternative causes. |
Unlikely | When the evidence is clearly in favour of attributing the adverse reaction to causes other than the blood or blood components. |
1 | Possible | When the evidence is indeterminate for attributing adverse reaction either to the blood or blood component or to alternative causes. |
2 | Likely, Probable | When the evidence is clearly in favour of attributing the adverse reaction to the blood or blood component. |
3 | Certain | When there is conclusive evidence beyond reasonable doubt for attributing the adverse reaction to the blood or blood component. |
PART C
Confirmation format for serious adverse reactions
Reporting establishment
Report identification
Confirmation date (year/month/day)
Date of serious adverse reaction (year/month/day)
Confirmation of serious adverse reaction (Yes/No)
Imputability level (NA, 0-3)
Change of type of serious adverse reaction (Yes/No)
If Yes, specify
Clinical outcome (if known)
- Complete recovery
- Minor sequelae
- Serious sequelae
- Death
PART D
Annual notification format for serious adverse reactions
Reporting establishment
Reporting period
This Table refers to [ ] Whole blood[ ] Red blood cells[ ] Platelets[ ] Plasma[ ] Other (use separate table for each component) | Number of units issued (total number of units issued with a given number of blood components) |
Number of recipients transfused (total number of recipients transfused with a given number of blood components) (if available) |
Number of units transfused (the total number of blood components (units) transfused over the reporting period) (if available) |
| Total number reported | Number of serious adverse reactions with imputability level 0 to 3 after confirmation (see Annex IIA) |
| Number of deaths |
| not assessable | Level 0 | Level 1 | Level 2 | Level 3 |
Immunological Haemolysis | Due to ABO incompatibility | Total | | | | | |
Deaths | | | | | |
Due to other allo-antibody | Total | | | | | |
Deaths | | | | | |
Non-immunological haemolysis | Total | | | | | |
Deaths | | | | | |
Transfusion-transmitted bacterial infection | Total | | | | | |
Deaths | | | | | |
Anaphylaxis/hypersensitivity | Total | | | | | |
Deaths | | | | | |
Transfusion related acute lung injury | Total | | | | | |
Deaths | | | | | |
Transfusion-transmitted viral Infection | HBV | Total | | | | | |
Deaths | | | | | |
HCV | Total | | | | | |
Deaths | | | | | |
HIV-1/2 | Total | | | | | |
Deaths | | | | | |
Other (specify) | Total | | | | | |
Deaths | | | | | |
Transfusion-transmitted parasitical infection | Malaria | Total | | | | | |
Deaths | | | | | |
Other (specify) | Total | | | | | |
Deaths | | | | | |
Post-transfusion purpura | Total | | | | | |
Deaths | | | | | |
Graft versus host disease | Total | | | | | |
Deaths | | | | | |
Other serious reactions (specify) | Total | | | | | |
Deaths | | | | | |
--------------------------------------------------
ANNEX III
NOTIFICATION OF SERIOUS ADVERSE EVENTS
PART A
Rapid Notification Format for Serious Adverse Events
Reporting establishment
Report identification
Reporting date (year/month/day)
Date of serious adverse event (year/month/day)
Serious adverse event, which may affect quality and safety of blood component due to a deviation in: | Specification |
Product defect | Equipment failure | Human error | Other (specify) |
Whole blood collection | | | | |
Apheresis collection | | | | |
Testing of donations | | | | |
Processing | | | | |
Storage | | | | |
Distribution | | | | |
Materials | | | | |
Others (specify) | | | | |
PART B
Confirmation Format for Serious Adverse Events
Reporting establishment
Report identification
Confirmation date (year/month/day)
Date of serious adverse event (year/month/day)
Root cause analysis (details)
Corrective measures taken (details)
PART C
Annual Notification Format for Serious Adverse Events
Reporting establishment
Reporting period
1 January-31 December (year)
Total number of blood and blood components processed:
Serious adverse event, affecting quality and safety of blood component due to a deviation in: | Total number | Specification |
Product defect | Equipment failure | Human error | Other (specify) |
Whole blood collection | | | | | |
Apheresis collection | | | | | |
Testing of donations | | | | | |
Processing | | | | | |
Storage | | | | | |
Distribution | | | | | |
Materials | | | | | |
Others (specify) | | | | | |
--------------------------------------------------
