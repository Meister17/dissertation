COMMISSION REGULATION (EEC) N° 3944/92 of 30 December 1992 Introducing a double checking system for monitoring certain textile products originating in Bangladesh
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community;
Having regard to Council Regulation (EEC) N° 4136/86 of 22 December 1986 on common rule for imports of certain textile products originating in third countries (1), as last amended by Commission Regulation (EEC) N° 1539/92 (2), and in particular Article 13 thereof;
Whereas Article 13 of Regulation (EEC) N° 4136/86 provides for the possibility of consultations in case of fraud or circumvention of the provisions of agreements on textiles concluded between the European Economic Community and supplier third countries;
Whereas there is sufficient evidence to believe that large scale fraud and circumvention is taking place of the provisions of the agreement with Bangladesh especially as regards the determination of origin of textile products;
Whereas after consultations with the Bangladeshi authorities an agreement was initialled in the form of an Exchange of Letters establishing a double checking system for monitoring the shipments of textile goods of categories 4, 6 and 8 without, however, negotiating at this stage quantitative limits;
Whereas on 30 December 1992 the Council approved on behalf of the Community the Agreement between Bangladesh and the European Economic Community in the form of an Exchange of Letters on the introduction of a double-checking system for monitoring all exports of the above mentioned categories;
Whereas Annex VI to Regulation (EEC) 4136/86 lays down the provisions concerning the implementation of a double-checking system as well as the form and production of export licences;
Whereas in order to specify that the export documents to be issued by the Bangladeshi authorities do not refer to products submitted to restricitions, it is necessary to modify, as appropriate, the export documents;
Whereas the introduction of the double-checking system should not prevent the importation into the Community of shipments of products for the categories 4, 6 and 8 which were shipped from Bangladesh before the date of entry into force of this Regulation;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Textile Committee.
HAS ADOPTED THIS REGULATION:
Article 1
Without prejudice to the provision of Article 2, imports into the Community of the category of products originating in Bangladesh and specified in the Annex 1 hereto shall be subject, until 31 December 1992, to the double-checking system described in Annex VI to Regulation (EEC) N° 4136/86.
Article 2
Products referred to in Article 1 shipped from Bangladesh to the Community before the date of entry into force of this Regulation and not yet released for free circulation, shall be so released subject to the presentation of a bill of lading or other transport document proving that shipment actually took place before that date.
Article 3
The export licence form attached to Annex VI to Regulation 4136/86 is replaced for the purposes of this Regulation by the form specified in the Annex II hereto.
Article 4
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 30 December 1992.
For the Commission Frans ANDRIESSEN Vice-President
(1) OJ N° L 387, 31. 12. 1986, p. 42.
(2) OJ N° L 163, 17. 06. 1992, p. 9.
ANNEX I
>TABLE>
ANEXO II - BILAG II - ANHANG II - ÐÁÑÁÑÔÇÌÁ II - ANNEX II - ANNEXE II - ALLEGATO II - BIJLAGE II - ANEXO II
>REFERENCE TO A FILM>
