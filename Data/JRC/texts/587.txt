Commission Regulation (EC) No 2706/2000
of 11 December 2000
amending Regulation (EC) No 1455/1999 laying down the marketing standard for sweet peppers
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 2200/96 of 28 October 1996 on the common organisation of the market in fruit and vegetables(1), as last amended by Regulation (EC) No 1257/1999(2), and in particular Article 2(2) thereof,
Whereas:
(1) Commission Regulation (EC) No 1455/1999(3) lays down the marketing standard for sweet peppers.
(2) It is not possible in practice to establish a distinction between types of sweet pepper. It should therefore be specified that all sweet peppers are varieties (cultivars) grown from Capsicum annuum L. var. annuum, and the derogation with respect to the minimum size for Capsicum annuum L. var. longum, also known as "peperoncini", should be deleted.
(3) Sales of small elongated sweet peppers (pointed) are growing. The minimum size applicable to this type of pepper should therefore be lowered.
(4) Where sweet peppers are presented in a mixture of colours, uniformity of origin is not required. It is therefore necessary to provide, where appropriate, for indication of the different countries of origin.
(5) Sales of miniature peppers have grown in recent years. Special provisions concerning sizing should therefore be laid down for those products, which are below the minimum size, along with the corresponding provisions concerning labelling and presentation.
(6) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Fresh Fruit and Vegetables,
HAS ADOPTED THIS REGULATION:
Article 1
The Annex to Regulation (EC) No 1455/1999 is amended as follows:
1. The first paragraph of Title I (Definition of produce) is replaced by the following:
"This standard applies to sweet peppers of varieties (cultivars) grown from Capsicum annuum L. var. annuum, to be supplied fresh to the consumer, sweet peppers for industrial processing being excluded."
2. The first indent of the third paragraph of Title III (Provisions concerning sizing) is replaced by the following:
"- elongated sweet peppers (pointed): 20 mm."
3. The fifth paragraph of Title III (Provisions concerning sizing) is replaced by the following with the associated footnote:
"The size requirements shall not apply to miniature produce(4)."
4. The following paragraph is inserted after the fourth paragraph of point A (Uniformity) of Title V (Provisions concerning presentation):
"Miniature sweet peppers must be reasonably uniform in size. They may be mixed with other miniature products of a different type and origin."
5. The third indent of point B (Nature of produce) of Title VI (Provisions concerning marking) is deleted.
6. The first indent of point C (Origin of produce) of Title VI (Provisions concerning marking) is replaced by the following:
"- Country or, where appropriate, countries of origin and, optionally, district where grown or national, regional or local place name."
7. The following indent is inserted after the second indent in point D (Commercial specifications) of Title VI (Provisions concerning marking):
"- Mini peppers, baby peppers, or other appropriate term for miniature produce. Where several types of miniature produce are mixed in the same package, all products and their respective origins must be mentioned."
Article 2
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Communities.
It shall apply from the first day of the third month following that of its entry into force.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 11 December 2000.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 297, 21.11.1996, p. 1.
(2) OJ L 160, 26.6.1999, p. 80.
(3) OJ L 167, 2.7.1999, p. 22.
(4) Miniature product means a variety or cultivar of sweet pepper, obtained by plant breeding and/or special cultivation techniques, excluding sweet peppers of non-miniature varieties which have not fully developed or are of inadequate size. All other requirements of the standard must be met.
