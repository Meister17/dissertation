COUNCIL REGULATION (EC) No 2443/96 of 17 December 1996 providing for additional measures for direct support of producers' incomes or for the beef and veal sector
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Articles 42 and 43 thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament (1),
Whereas, in the light of the continuing serious difficulties in the beef and veal sector resulting from consumer concerns in relation to bovine spongiform encephalopathy (BSE) and further to those measures which were taken to support producers' incomes by Council Regulation (EC) No 1357/96 (2), additional measures for the direct support of producers' incomes or for the beef and veal sector are necessary;
Whereas the amount of aid available to each Member State for direct support of producers' incomes or for the beef and veal sector will be distributed according to a key which takes into account, in particular, the size of the beef herd in each Member State; whereas it is appropriate that Member States should distribute the total amount of finance available on the basis of objective criteria, while avoiding any market distortion;
Whereas, for budgetary reasons, the Community will finance the expenditure incurred by Member States in relation to the direct support of producers' incomes or for the beef and veal sector only where such payments are made by a certain deadline,
HAS ADOPTED THIS REGULATION:
Article 1
Member States shall use the amounts set out in the Annex to make additional payments, according to objective criteria, to support producers' incomes or the beef and veal sector in their territory, provided that these payments do not cause distortion of competition.
Article 2
1. The measures introduced by Article 1 of this Regulation shall be deemed to be intervention intended to stabilize agricultural markets within the meaning of Article 3 (1) of Council Regulation (EEC) No 729/70 of 21 April 1970 on the financing of the common agricultural policy (3).
2. The Community shall finance the expenditure incurred by Member States in relation to the payments referred to in Article 1 only where such payments are made by them by 15 October 1997 at the latest.
Article 3
The conversion rate to be applied shall be the agricultural rate valid on 1 December 1996.
Article 4
Any detailed rules necessary for the application of this Regulation shall be adopted in accordance with the procedure set out in Article 27 of Council Regulation (EEC) No 805/68 of 27 June 1968 on the common organization of the market in beef and veal (4).
Article 5
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Communities.
It shall not be applicable before the day the general budget of the European Communities for the financial year 1997 has been declared as finally adopted.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 17 December 1996.
For the Council
The President
I. YATES
(1) Opinion delivered on 13 December 1996 (not yet published in the Official Journal).
(2) OJ No L 175, 13. 7. 1996, p. 9.
(3) OJ No L 94, 28. 4. 1970, p. 13. Regulation as last amended by Regulation (EC) No 1287/95 (OJ No L 125, 8. 6. 1995, p. 1).
(4) OJ No L 148, 28. 6. 1968, p. 24. Regulation as last amended by Regulation (EC) No 2222/96 (OJ No L 296, 21. 11. 1996, p. 50).
ANNEX
Amounts referred to in Article 1
>TABLE>
