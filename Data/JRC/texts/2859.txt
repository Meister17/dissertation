Commission Regulation (EC) No 46/2005
of 13 January 2005
derogating from Regulation (EC) No 1282/2001 as regards the final date for submitting harvest and production declarations for the 2004/05 wine year
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1493/1999 of 17 May 1999 on the common organisation of the market in wine [1], and in particular Article 73 thereof,
Whereas:
(1) Commission Regulation (EC) No 1282/2001 [2] requires wine growers to submit harvest and production declarations no later than 10 December, with a view to knowing the volume of Community wine production in good time.
(2) The central computer system of one Member State suffered a technical problem a few days before that deadline. The producers no longer have access to the system and so cannot submit their declarations by the deadline.
(3) To resolve the problem, which is not the fault of the producers, and pending the normal relaunch of that central computer system, those producers should be granted an extension for the submission of harvest and production declarations.
(4) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Wine,
HAS ADOPTED THIS REGULATION:
Article 1
Notwithstanding Article 11(1) of Regulation (EC) No 1282/2001, the declarations referred to in Articles 2 and 4 of that Regulation may be submitted for the 2004/05 wine year up to 15 January 2005.
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Union.
It shall apply from 10 December 2004.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 13 January 2005.
For the Commission
Mariann Fischer Boel
Member of the Commission
--------------------------------------------------
[1] OJ L 179, 14.7.1999, p. 1. Regulation as last amended by Commission Regulation (EC) No 1795/2003 (OJ L 262, 14.10.2003, p. 13).
[2] OJ L 176, 29.6.2001, p. 14.
