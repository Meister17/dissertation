Authorisation for State aid pursuant to Articles 87 and 88 of the EC Treaty
Cases where the Commission raises no objections
(2006/C 1/04)
Date of adoption of the decision : 28.10.2005
Member State : Spain
Aid No : N 296/2005
Title : Aid to improve production of table grapes
Objective : Structural improvement in table-grape production, consisting in uprooting and replacing table-grape plants with recommended varieties which will enhance quality and modernise production structures
Legal basis : Orden por la que se establecen las bases reguladoras para la concesión de ayudas destinadas al saneamiento de la producción de uva de mesa
Budget : EUR 12,150 million
Aid intensity or amount : The maximum rate of the aid is 20 %, irrespective of the area, and 22,5 % for young farmers during the first five years after they set up. This aid may be combined with other aid granted by the Autonomous Community on whose territory the beneficiary's holding is located, on condition that it does not exceed the ceilings of 50 % in less-favoured areas and 40 % elsewhere. Those maximum rates may be raised by 5 % for investments carried out by young farmers during the first five years after they set up
Duration : Payment of the aid is scheduled from 2005 until 2009 (inclusive)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 18.11.2005
Member State : Italy
Aid No : N 307/2003
Title : Regional Law No. 10 of 1 April 2005 "Aid for agricultural and agri-food cooperatives"
Objective : The measure provides for the following types of aid: Start-up aid for producer organisations, investment aid, aid for technical support, aid for quality assurance systems, aid for promotion and advertising of agricultural products
Legal basis : Legge Regionale 1 aprile 2005, n. 10 "Interventi a favore della cooperazione agricola e agroalimentare nella regione Molise"
Budget : EUR 100000 for 2005
Aid intensity or amount - Start-up aid for producer organisations: 100 % aid, degressive over 5 years
- Investment aid: 40/50 % aid, extra 5 % for young farmers
- Aid for quality assurance systems: 100 %
- Technical support: 100 % aid
- Promotion: 100 %
- Advertising: 50 %
Duration : Unlimited
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 22.7.2004
Member State : Italy (Calabria)
Aid No : N 313/2003
Title : Aiuti a favore degli investimenti nel settore agricolo ed agroalimentare
Objective : Aid for investments in undertakings processing and marketing agricultural products and aid for investments in the relocation of buildings in the public interest
Legal basis : Delibera della Giunta regionale n. 449 del 17 giugno 2003 concernente le disposizioni d'applicazione dell'articolo 2, comma 2, lettera b) della Legge regionale 8 luglio 2002, n. 24
Budget : EUR 2000000 (estimated budget) for the first year of application
Aid intensity or amount : Varies according to the measures, as set out in the letter to the Member State
Duration : Not specified.
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 18.10.2005
Member State : Italy (Friuli-Venezia Giulia region)
Aid No : N 327/2005
Title : Draft Regional Law No 125 "Integrated agricultural and rural development services system"
Objective : To promote the system for disseminating agricultural knowledge in order to foster the balanced development of rural areas, boost production value-added and improve the competitiveness of agricultural holdings
Legal basis : Disegno di legge: "Sistema integrato dei servizi di sviluppo agricolo e rurale (SISSAR)"
Budget : An amount of EUR 883500 is planned for 2006. Subsequent amounts will be laid down in the Region's annual and triennial Finance Acts
Aid intensity or amount (a) basic research: up to 100 % of eligible expenditure;
(b) industrial research: up to 100 % of eligible expenditure;
(c) precompetitive development activity: up to 100 % of eligible expenditure, up to a limit of EUR 100000 per beneficiary
(a) EUR 9000 for professional training and information;
(b) EUR 30000 (up to EUR 60000 if the beneficiary is a cooperative) for consultancy and specialist assistance services
Duration : 2006-12 (6 years)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 11.1.2005
Member State : France
Aid No : N 332/2004
Title : Restructuring of cider apple orchards
Objective : To adapt cider-apple orchards to consumer requirements through investment aid and aid to reduce production capacities.
Budget : EUR 2497500
Aid intensity or amount : Variable
Duration : 4 years
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 19.1.2005
Member State : Denmark
Aid No : N 343/04
Title : Tax on mineral phosphorous in feed phosphates
Objective : The county council land tax is reduced from 4,3 per mille to 3,8 per mille for land used for primary production, i.e. land used for farms, horticultural establishments, nurseries and fruit orchards, as a compensation for the introduction of a new tax on phosphorous in feed
Legal basis : Lov af 28. april 2004 om afgift på mineralsk fosfor i foderfosfater
Budget : An estimated maximum of DKK 99000 (approx. EUR 13300) per year
Aid intensity or amount : The ceiling creates an average tax reduction of approx. DKK 700 (about EUR 95) per holding per year
Duration : Unlimited
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 28.12.2004
Member State : Italy (Sicily)
Aid No : N 347/2004
Title : Law 185/1992: Assistance in agricultural areas having suffered damage
Objective : To compensate for damage to farming structures as a result of bad weather
Legal basis : Articolo 3, paragrafo 2, lettera c) della legge 185/1992. Deliberazione della Giunta siciliana n. 81 del 12 marzo 2004 e deliberazione della Giunta siciliana n. 93 del 19 marzo 2004
Budget : EUR 26121277
Aid intensity or amount : 100 % of the damage.
Duration : Measure applying a scheme approved by the Commission
Other details : Measures applying the scheme approved by the Commission under State aid C 12/B/95 (Commission Decision 2004/307/EC, 16.12.2003, OJ L 99/2004).
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 20.7.2005
Member State : Italy (Friuli-Venezia Giulia)
Aid No : N 373/2004
Title : Assistance in agricultural areas which have suffered damage (hail storm of 20 June 2004, province of Trieste)
Objective : To compensate farmers for damage to agricultural production and farming structures as a result of bad weather
Legal basis : Decreto legislativo n. 102/2004
Budget : See the approved scheme (NN 54/A/04)
Aid intensity or amount : 80 % of the damage
Duration : Measure applying an aid scheme approved by the Commission
Other details : Measure applying the scheme approved by the Commission under State aid NN 54/A/2004 (Commission letter C(2005)1622 final, dated 7 June 2005).
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 17.11.2005
Member State : Germany
Aid No : N 383/05
Title : Setting up a computerised information system for horticulture; Modification of aid No N 233/01
Objective : To obtain a sustainable improvement of the knowledge transfer and increased research efficiency in the horticulture sector
Legal basis : Zuwendungsbescheid und Änderungsbescheid auf der Grundlage der Bundeshaushaltsordnung
Budget : New budget: EUR 246951
Aid intensity or amount : Average aid intensity of 58,3 % in 2006, 45,3 % in 2007 and 33,3 % in 2008
Duration : 1.4.2006 — 30.6.2008
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 21.9.2005
Member State : Denmark
Aid No : N 391/04
Title : Aid for the preservation of endangered species
Objective : The purpose of the aid is to support the preservation of endangered breeds and make it possible for the general public to watch those breeds. The endangered breeds that are eligible for aid are chosen by the Danish Committee for the Management of Farm Animal Genetic Resources
Legal basis :
§ 24 i lov nr. 432 af 9. juni 2004 om husdyrbrug
Bekendtgørelse om tilskud til bevaring af husdyrgenetiske ressourcer
Budget : DKK 2945000 (about EUR 396000) per year
Aid intensity or amount : Up to 100 %
Duration : Until 1 November 2007
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
--------------------------------------------------
