Prior notification of a concentration
(Case COMP/M.4162 — Merck/Schering)
(2006/C 72/04)
(Text with EEA relevance)
1. On 16 March 2006, the Commission received a notification of a proposed concentration pursuant to Article 4 of Council Regulation (EC) No 139/2004 [1] by which the undertaking Merck KgaA ("Merck", Germany) acquires within the meaning of Article 3(1)(b) of the Council Regulation control of the undertaking Schering Aktiengesellschaft ("Schering", Germany) by way of tender offer.
2. The business activities of the undertakings concerned are:
- for undertaking Merck: a global research-based pharmaceutical and chemical company;
- for undertaking Schering:a global research-based pharmaceutical company.
3. On preliminary examination, the Commission finds that the notified transaction could fall within the scope of Regulation (EC) No 139/2004. However, the final decision on this point is reserved.
4. The Commission invites interested third parties to submit their possible observations on the proposed operation to the Commission.
Observations must reach the Commission not later than 10 days following the date of this publication. Observations can be sent to the Commission by fax (No (32-2) 296 43 01 or 296 72 44) or by post, under reference number COMP/M.4162 — Merck/Schering, to the following address:
European Commission
Competition DG
Merger Registry
J-70
B-1049 Brussels
[1] OJ L 24, 29.1.2004, p. 1.
--------------------------------------------------
