COUNCIL REGULATION (EC) No 322/97 of 17 February 1997 on Community Statistics
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 213 thereof,
Having regard to the draft Regulation submitted by the Commission (1),
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Having regard to the opinion of the European Monetary Institute (4),
(1) Whereas, for the performance of the various tasks entrusted to it by the provisions of the Treaty in order to ensure the proper functioning and development of the common market, the Commission needs to collect any relevant information;
(2) Whereas, in particular, for the formulation, application, monitoring and assessment of the policies laid down in the Treaty, the Community must be able to base its decisions on statistics which are up-to-date, reliable, pertinent and comparable between the Member States;
(3) Whereas, to ensure the feasibility, coherence and comparability of Community statistics, collaboration and coordination must be reinforced between those authorities which contribute to the production of such information at both national and Community level; whereas the provisions of this Regulation contribute to the development of a Community statistical system;
(4) Whereas the said authorities must show the greatest impartiality and professionalism in the production of statistics, respecting the same principles of behaviour and professional ethics;
(5) Whereas on 14 April 1994 the UN Statistical Commission adopted the Fundamental Principles of Official Statistics;
(6) Whereas, in order to prepare and carry out priority Community statistical actions, statistical programmes must be implemented, taking into account available resources both at national and Community level;
(7) Whereas the establishment of the Community statistical programme to be adopted by the Council and the annual work programmes to be adopted by the Commission require particularly close collaboration within the Statistical Programme Committee, established by Decision 89/382/EEC, Euratom (5);
(8) Whereas this Regulation aims to establish a legislative framework for the production of Community statistics; whereas provision should be made for planning the production of the Community statistics which will be specified by individual statistical actions;
(9) Whereas this Regulation defines the responsibility of the national authorities and the Community authority for the production of Community statistics in compliance with the principle of subsidiarity as defined in Article 3b of the Treaty;
(10) Whereas, in preparing statistical programmes, the Committees instituted by the Council in their given statistical fields must carry out the functions with which they have been entrusted;
(11) Whereas the methods and conditions of the implementation of the Community Statistical Programme by individual statistical actions have to be defined;
(12) Whereas dissemination is part of the production process of Community statistics;
(13) Whereas it is important to protect the confidential information which the national and Community statistical authorities must collect for the production of Community statistics, in order to gain and maintain the confidence of the parties responsible for providing this information; whereas the confidentiality of statistical information must satisfy the same set of principles in all the Member States;
(14) Whereas, for this purpose, it is necessary to establish a common concept of confidential data to be used in relation to the production of Community statistics;
(15) Whereas this concept must take into account that data taken from sources available to the public are considered confidential by some national authorities, according to national legislation;
(16) Whereas the specific rules of processing data in the framework of the Community Statistical Programme will not affect Directive 95/46/EC of the European Parliament and of the Council of 24 October 1995 on the protection of individuals with regard to the processing of personal data and on the free movement of such data (6);
(17) Whereas the Treaty has conferred certain statistical responsibilities on the European Monetary Institute, which it shall exercise without seeking or taking instructions from Community institutions or bodies, from any Government of a Member State or from any other body; whereas it is important to ensure appropriate coordination between the relevant tasks of the authorities at national and Community level which contribute to the production of Community statistics on the one hand, and the functions of the European Monetary Institute on the other;
(18) Whereas national central banks, at the latest at the date of the establishment of the European System of Central Banks, should be independent of Community institutions or bodies, of any government of a Member State or of any other body; whereas, in stage II of Economic and Monetary Union, Member States should start and complete the process to ensure the independence of national central banks;
(19) Whereas the Commission has consulted the Statistical Programme Committee, the Committee on Monetary, Financial and Balance of Payments Statistics, established by Decision 91/115/EEC (7), and the European Advisory Committee on Statistical Information in the Economic and Social Spheres, set up by Decision 91/116/EEC (8),
HAS ADOPTED THIS REGULATION:
CHAPTER I
General provisions
Article 1
The purpose of this Regulation is to establish a legislative framework for the systematic and programmed production of Community statistics with a view to the formulation, application, monitoring and assessment of the policies of the Community.
The national authorities at national level and the Community authority at Community level shall be responsible for the production of Community statistics in compliance with the principle of subsidiarity.
To guarantee comparability of results, Community statistics shall be produced on the basis of uniform standards and, in specific, duly justified cases, of harmonized methods.
Article 2
For the purposes of this Regulation:
- 'Community statistics` shall mean quantitative, aggregated and representative information taken from the collection and systematic processing of data, produced by the national authorities and the Community authority in the framework of implementation of the Community statistical programme in accordance with Article 3 (2),
- 'production of statistics` shall mean the process encompassing all the activities necessary for the collection, storage, processing, compilation, analysis and dissemination of the statistical information,
- 'national authorities` shall mean national statistical institutes and other bodies responsible in each Member State for producing Community statistics,
- 'Community authority` shall mean the Commission department responsible for carrying out the tasks devolving on the Commission as regards the production of Community statistics (Eurostat).
CHAPTER II
The Community statistical programme and its implementation
Article 3
1. In accordance with the relevant provisions of the Treaty, the Council shall adopt a Community statistical programme which defines the approaches, the main fields and the objectives of the actions envisaged for a period not exceeding five years.
The Community statistical programme shall constitute the framework for the production of all Community statistics. If necessary, it may be updated.
The Commission shall prepare a report on the implementation of the programme at the end of the period covered by the programme.
The Commission shall submit the guidelines for establishing the Community statistical programme for prior examination by the Statistical Programme Committee and, within the framework of their respective powers, by the European Advisory Committee on Statistical Information in the Economic and Social Spheres and the Committee on Monetary, Financial and Balance of Payments Statistics.
2. The Community statistical programme referred to in paragraph 1 shall be implemented by individual statistical actions. These actions shall be either:
(a) decided on by the Council in accordance with the appropriate Treaty provisions; or
(b) decided on by the Commission under the conditions laid down in Article 6 and in accordance with the procedure laid down in Article 19; or
(c) decided on by means of agreement between the national authorities and the Community authority within their respective spheres of competence.
3. Each year, before the end of May, the Commission shall submit for examination by the Statistical Programme Committee its work programme for the following year. This programme shall indicate in particular:
- the actions which the Commission considers to have priority, bearing in mind both national and Community financial constraints,
- the procedures and any legal instruments envisaged by the Commission for the implementation of the programme.
The Commission will take the utmost account of the comments of the Statistical Programme Committee. The Commission will take the action it considers most appropriate.
Article 4
The Commission shall indicate in its initiatives on the individual statistical actions referred to in Article 3 (2) (a) and (b):
- the reasons justifying the action envisaged, notably in the light of the aims of the Community policy concerned,
- the precise objectives for the action and an evaluation of the expected results,
- the procedures for implementing the action, its length and the role of the national authorities and the Community authority,
- the role of the relevant specialist committees,
- the means by which burdens on respondents will be minimized,
- a cost-effectiveness analysis which takes into account the financing costs of the action both for the Community and for the Member States,
- the international statistical recommendations to be observed in the fields covered.
Article 5
The acts adopted by the Council or the Commission in the cases referred to in Article 3 (2) (a) and (b) must define the elements necessary to obtain the quality and comparability level required in Community statistics.
Article 6
The Commission may decide on an individual statistical action as provided for in Article 3 (2) (b) when it meets all the following conditions:
- the action must not last for more than one year,
- the data to be gathered must be already available or accessible within the national authorities responsible, or, in exceptional cases, data which can be gathered directly,
- any additional costs incurred at national level as a result of the action must be borne by the Commission.
Article 7
When Community statistics result from an agreement between the national authorities and the Community authority, as referred to in Article 3 (2) (c), no obligation shall arise for the respondents, unless such an obligation is laid down in national legislation.
Article 8
The implementation of individual statistical actions shall come under the responsibility of national authorities unless otherwise stated in a Council legal act. If national authorities do not accomplish this task, the individual statistical actions may be carried out by the Community authority with the explicit agreement of the national authority concerned.
Article 9
To guarantee the coherence necessary to produce statistics meeting their respective information requirements, the Commission shall cooperate closely with the European Monetary Institute, taking due account of the principles defined in Article 10. The Committee on Monetary, Financial and Balance of Payments Statistics shall take part, within the limits of its competence, in this process of cooperation.
Although the European Monetary Institute and the national central banks do not participate in the production of Community statistics, by analogy with Article 3 (2) (c), following an agreement between a national central bank and the Community authority within their respective spheres of competence and without prejudice to national arrangements between the national central bank and the national authority, data produced by the central bank may be used, directly or indirectly, by national authorities and the Community authority for the production of Community statistics.
CHAPTER III
Principles
Article 10
In order to ensure the best possible quality in both deontological and professional aspects, Community statistics shall be governed by the principles of impartiality, reliability, relevance, cost-effectiveness, statistical confidentiality and transparency.
The principles referred to in the first subparagraph are defined as follows:
'impartiality` is an objective and independent manner of producing Community statistics, free from any pressure from political or other interest groups, particularly as regards the selection of techniques, definitions and methodologies best suited to the attainment of the objectives as set out. It implies the availability of statistics, with a minimum delay, to all users (Community institutions, governments, social and economic operators, academic circles and the public in general);
'reliability` is the characteristic of Community statistics to reflect as faithfully as possible the reality which they are designed to represent. It implies that scientific criteria are used for the selection of sources, methods and procedures. Any information on the coverage, methodology, procedures and sources will also improve data reliability;
'relevance` shall mean that the production of Community statistics is a function of clearly defined requirements determined by the Community objectives. These requirements determine the fields, timeliness and scale of statistics, which should keep abreast of new demographic, economic, social and environmental developments at all times. Data collection should be limited to what is necessary for attaining the desired results. The production of Community statistics which has ceased to be of interest for Community objectives should be abandoned;
'cost-effectiveness` shall mean the optimum use of all available resources and the minimization of the burden on respondents. The amount of work and the costs which the production of statistics requires should be in proportion to the importance of the results/benefits sought;
'statistical confidentiality` shall mean the protection of data related to single statistical units which are obtained directly for statistical purposes or indirectly from administrative or other sources against any breach of the right to confidentiality. It implies the prevention of non-statistical utilization of the data obtained and unlawful disclosure;
'transparency` shall mean the right of respondents to have information on the legal basis, the purposes for which the data are required and the protective measures adopted. The authorities responsible for collecting Community statistics shall take every step to supply such information.
CHAPTER IV
Dissemination
Article 11
1. Dissemination shall mean the activity of making Community statistics accessible to users.
2. Dissemination shall be carried out in such a way that access to Community statistics is rendered simple and impartial throughout the Community.
3. The dissemination of Community statistics shall be carried out by the Community authority and national authorities within their respective spheres of competence.
Article 12
Statistical results at Community level shall be disseminated with the same periodicity as the transmission to the Community authority of the results available at national level. As far as possible and without compromising quality at Community level, the dissemination will take place before the next transmission of national results to the Community authority is due.
CHAPTER V
Statistical confidentiality
Article 13
1. Data used by the national authorities and the Community authority for the production of Community statistics shall be considered confidential when they allow statistical units to be identified, either directly or indirectly, thereby disclosing individual information.
To determine whether a statistical unit is identifiable, account shall be taken of all the means that might reasonably be used by a third party to identify the said statistical unit.
2. By derogation from paragraph 1, data taken from sources which are available to the public and remain available to the public at the national authorities according to national legislation, shall not be considered confidential.
Article 14
Transmission between national authorities and between national authorities and the Community authority of confidential data which do not permit direct identification may take place to the extent that this transmission is necessary for the production of specific Community statistics. Any further transmission must be explicitly authorized by the national authority that collected the data.
Article 15
Confidential data obtained exclusively for the production of Community statistics shall be used by national authorities and by the Community authority exclusively for statistical purposes unless the respondents have unambiguously given their consent to the use for any other purposes.
Article 16
1. In order to reduce the burden on respondents, and subject to paragraph 2, the national authorities and the Community authority shall have access to administrative data sources, each in the fields of activity of their own public administrations, to the extent that these data are necessary for the production of Community statistics.
2. The practical arrangements and the limits and conditions for achieving effective access shall be determined where necessary by each Member State and the Commission in their respective spheres of competence.
3. The use of confidential data obtained from administrative or other sources by the national authorities or by the Community authority for the production of Community statistics does not affect the use of the data for the purposes for which they were originally collected.
Article 17
1. Access for scientific purposes to confidential data obtained for Community statistics may be granted by the national authority responsible for the production of these data if the standard of protection prevailing in the country of origin and, if applicable, in the country of use, is ensured according to the measures laid down in Article 18.
2. Access for scientific purposes to confidential data transmitted to the Community authority in accordance with Article 14 may be granted by that authority, if the national authority which provided the data requested has given its explicit approval for such use.
Article 18
1. Necessary regulatory, administrative, technical and organizational measures shall be taken at national and Community level to ensure the physical and logical protection of confidential data and to ensure that no unlawful disclosure and non-statistical use shall occur when Community statistics are disseminated.
2. Officials and other servants of the national authorities or of the Community authority having access to data subject to Community legislation which imposes statistical confidentiality shall be subject to compliance with such confidentiality, even after the cessation of their functions.
CHAPTER VI
Final provisions
Article 19
1. In the case referred to in Article 3 (2) (b), the Commission shall be assisted by the Statistical Programme Committee.
2. The representative of the Commission shall submit to the Committee a draft of the measures to be taken. The Committee shall deliver its opinion on the draft within a time limit which the Chairman may lay down according to the urgency of the matter. The opinion shall be delivered by the majority laid down in Article 148 (2) of the Treaty in the case of decisions which the Council is required to adopt on a proposal from the Commission. The votes of the representatives of the Member States within the Committee shall be weighted in the manner set out in that Article. The Chairman shall not vote.
3. (a) The Commission shall adopt the measures envisaged if they are in accordance with the opinion of the Committee.
(b) If the measures envisaged are not in accordance with the opinion of the Committee, or if no opinion is delivered, the Commission shall, without delay, submit to the Council a proposal relating to the measures to be taken. The Council shall act by a qualified majority.
If, on the expiry of a period of three months from the date of referral, the Council has not acted, the proposed measures shall be adopted by the Commission.
Article 20
1. For the adoption of the measures necessary for the implementation of Chapter V, particularly those designed to ensure that all the national authorities and the Community authority apply the same principles and minimum standards for avoiding disclosure of confidential Community statistical data and the conditions governing access for scientific purposes, in accordance with Article 17 (2), to confidential data held by the Community authority, the Commission shall be assisted by the Committee on Statistical Confidentiality set up by Article 7 of Council Regulation (Euratom, EEC) No 1588/90 of 11 June 1990 on the transmission of data subject to statistical confidentiality to the Statistical Office of the European Communities (9).
2. The representative of the Commission shall submit to the Committee a draft of the measures to be taken. The Committee shall deliver its opinion on the draft within a time limit which the Chairman may lay down according to the urgency of the matter. The opinion shall be delivered by the majority laid down in Article 148 (2) of the Treaty in the case of decisions which the Council is required to adopt on a proposal from the Commission. The votes of the representatives of the Member States within the Committee shall be weighted in the manner set out in that Article. The Chairman shall not vote.
3. (a) The Commission shall adopt measures which shall apply immediately.
(b) However, if these measures are not in accordance with the opinion of the Committee, they shall be communicated by the Commission to the Council forthwith.
In that event, the Commission shall defer application of the measures which it has decided for a period of three months from the date of referral to the Council.
The Council, acting by a qualified majority, may take a different decision within the time limit referred to in the previous subparagraph.
Article 21
1. This Regulation shall apply without prejudice to Directive 95/46/EC.
2. Point 1 of Article 2 of Regulation (Euratom, EEC) No 1588/90 shall be replaced by the following:
'1. confidential statistical data: data defined in Article 13 of Council Regulation (EC) No 322/97 of 17 February 1997 on Community Statistics (*);
(*) OJ No L 52, 22. 2. 1997, p. 1.`
Article 22
Statistics produced on the basis of existing Community legal acts shall be considered as Community statistics regardless of the decision-making procedures by which they are governed.
Statistics produced, or to be produced, by the national authorities and the Community authority pursuant to the framework programme for priority actions in the field of statistical information 1993 to 1997 provided for by Decision 93/464/EEC (10) shall be regarded as Community statistics.
Article 23
This Regulation shall enter into force on the twentieth day following that of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 17 February 1997.
For the Council
The President
G. ZALM
(1) OJ No C 106, 14. 4. 1994, p. 22.
(2) OJ No C 109, 1. 5. 1995, p. 321.
(3) OJ No C 195, 18. 7. 1994, p. 1.
(4) Opinion delivered on 7 February 1995.
(5) OJ No L 181, 28. 6. 1989, p. 47.
(6) OJ No L 281, 23. 11. 1995, p. 31.
(7) OJ No L 59, 6. 3. 1991, p. 19.
(8) OJ No L 59, 6. 3. 1991, p. 21.
(9) OJ No L 151, 15. 6. 1990, p. 1.
(10) OJ No L 219, 28. 8. 1993, p. 1.
