Extension and amendment of public service obligations imposed in respect of 10 scheduled air services on routes within Greece in accordance with Council Regulation (EEC) No 2408/92
(2004/C 312/08)
(Text with EEA relevance)
1. The Greek Government has decided to extend and revise from 1 May 2005 the public service obligations imposed in respect of 10 scheduled air services on routes within Greece, pursuant to Article 4(1)(a) of Council Regulation (EEC) No 2408/92 of 23 July 1992 on access for Community air carriers to intra-Community air routes, which were published in the Official Journal of the European Communities C 239 of 25 August 2001 and C 280 of 4 October 2001 (the latter concerning the postponement of their entry into force).
2. The amendments to the public service obligations are as follows:
A. MINIMUM FREQUENCY OF FLIGHTS AND MINIMUM NUMBER OF SEATS AVAILABLE EACH WEEK ON EACH ROUTE:
Athens — Astipalaia:
- Four return flights a week and a total of 60 seats a week in each direction throughout the winter.
- Five return flights a week and a total of 75 seats a week in each direction throughout the summer.
Athens — Ikaria:
- Four return flights a week and a total of 140 seats a week in each direction throughout the winter.
- Six return flights a week and a total of 210 seats a week in each direction throughout the summer.
Athens — Leros:
- Six return flights a week and a total of 132 seats a week in each direction throughout the winter.
- Eight return flights a week and a total of 176 seats a week in each direction throughout the summer.
Athens — Milos:
- Seven return flights a week and a total of 210 seats a week in each direction throughout the winter.
- Eight return flights a week and a total of 240 seats a week in each direction throughout the summer.
Athens — Skiros:
- Two return flights a week and a total of 20 seats a week in each direction throughout the winter.
- Three return flights a week and a total of 30 seats a week in each direction throughout the summer.
Thessaloniki — Samos:
- Two return flights a week and a total of 30 seats a week in each direction throughout the winter.
- Three return flights a week and a total of 45 seats a week in each direction throughout the summer.
Thessaloniki — Khios:
- Two return flights a week and a total of 30 seats a week in each direction throughout the winter.
- Three return flights a week and a total of 45 seats a week in each direction throughout the summer.
Limnos — Mitilini — Khios — Samos — Rodos
- Limnos — Mitilini — Rodos:
Two return flights a week and a total of 20 seats a week in each direction throughout the year.
- Limnos — Mitilini — Khios — Rodos:
One return flight a week and a total of 20 seats a week in each direction throughout the year.
- Limnos — Mitilini — Samos — Rodos:
One return flight a week and a total of 20 seats a week in each direction throughout the year.
- Limnos — Mitilini — Khios — Samos — Rodos:
One return flight a week and a total of 20 seats a week in each direction throughout the year.
Rodos — Karpathos — Kasos
- Four return flights a week and a total of 200 seats a week in each direction throughout the winter.
- Six return flights a week and a total of 300 seats a week in each direction throughout the summer.
- Four return flights a week and a total of 200 seats a week in each direction throughout the winter.
- Six return flights a week and a total of 300 seats a week in each direction throughout the summer.
Rodos — Kastelorizo:
- Four return flights a week and a total of 80 seats a week in each direction throughout the winter.
- Six return flights a week and a total of 120 seats a week in each direction throughout the summer.
The winter and summer periods are as determined by IATA.
If the average occupancy rate of all the flights on one route has exceeded 75 % during the previous period, the minimum weekly frequency or the minimum number of seats provided each week may be increased in proportion to the increase recorded. This increase shall be notified to the carrier operating the service by registered letter six months before it applies, and shall not come into effect until it has been published by the European Commission in the Official Journal of the European Union.
Where the aircraft used do not have the capacity required to cover the minimum weekly seat availability (laid down in paragraph 2(a)), the flight frequencies may be increased accordingly.
If flights are cancelled because of weather conditions, they must be operated on the days immediately afterwards in order to fulfil the weekly requirement, taking into account the minimum number of seats to be provided pursuant to paragraph 2(a).
B. RATES
The reference price of the one-way economy fare may not exceed the following rates:
+++++ TABLE +++++
The above amounts may be increased in the event of an unforeseen increase in the costs of operating the service outside the control of the carrier. This increase shall be notified to the carrier operating the service but shall not come into effect until it has been published by the European Commission in the Official Journal of the European Union.
C. MINIMUM FREQUENCY OF FLIGHTS
In accordance with Article 4(1)(c) of Regulation (EEC) No 2408/92, a carrier intending to operate scheduled services on these routes must give a guarantee that it will operate the route for at least 12 months without interruption.
The number of flights cancelled for reasons for which the carrier is responsible may not exceed 2 % of the total annual number of flights, except in cases of force majeure.
Should the air carrier intend to cease operation of any of the aforementioned routes, it must notify the Civil Aviation Authority, Directorate for Air Operations, Section for Bilateral Air Agreements, at least six months before services are interrupted.
3. Useful information
The operation of flights on the above routes by a Community carrier without complying with the corresponding public services obligations may result in the imposition of administrative and/or other penalties.
As regards the types of aircraft to be used, air carriers are asked to consult Aeronautical Information Publications of Greece (AIP Greece) as regards technical and business data and airport procedures.
As regards timetables, arrivals and departures of aircraft must be planned within the operating times of airports as determined by decision of the Minister for Transport and Communications.
If no air carrier has declared to the Civil Aviation Authority, Directorate for Air Operations, an intention to operate scheduled services from 1 May 2005 on one or more of the abovementioned routes without financial compensation, Greece has decided, under the procedure set out in Article 4(1)(d) of Regulation (EEC) No 2408/92, to limit access to each or several of the abovementioned routes to only one carrier for three years and to grant the right to operate those services from 1 May 2005 following an invitation to tender.
These public service obligations replace those published in the Official Journal of the European Communities C 239 of 25 August 2001.
--------------------------------------------------
