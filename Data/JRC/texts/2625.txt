Statement of revenue and expenditure of the Translation Centre for the bodies of the European Union for the financial year 2005 — Amending Budget 1
(2005/894/EC)
STATEMENT OF REVENUE
Title Chapter | Heading | Financial year 2005 | AB 2005 | Total 2005 |
1
PAYMENTS FROM THE AGENCIES, OFFICES, INSTITUTIONS AND BODIES
1 0 | PAYMENTS FROM THE AGENCIES AND BODIES | 25885500 | –3215000 | 22670500 |
| Title 1 — Total | 25885500 | –3215000 | 22670500 |
2
SUBSIDY FROM THE COMMISSION
2 0 | SUBSIDY FROM THE COMMISSION | p.m. | p.m. | p.m. |
| Title 2 — Total | p.m. | p.m. | p.m. |
3
INTERINSTITUTIONAL COOPERATION
3 0 | INTERINSTITUTIONAL COOPERATION | 2338500 | –843000 | 1495500 |
| Title 3 — Total | 2338500 | –843000 | 1495500 |
4
OTHER REVENUE
4 0 | OTHER REVENUE | 200000 | 100000 | 300000 |
| Title 4 — Total | 200000 | 100000 | 300000 |
5
SURPLUS CARRIED OVER FROM THE PREVIOUS FINANCIAL YEAR
5 0 | SURPLUS CARRIED OVER FROM THE PREVIOUS FINANCIAL YEAR | p.m. | 3496684 | 3496684 |
| Title 5 — Total | p.m. | 3496684 | 3496684 |
| GRAND TOTAL | 28424000 | –461316 | 27962684 |
TITLE 1
PAYMENTS FROM THE AGENCIES, OFFICES, INSTITUTIONS AND BODIES
CHAPTER 1 0 — PAYMENTS FROM THE AGENCIES AND BODIES
Article Item | Heading | Financial year 2005 | AB 2005 | Total 2005 |
CHAPTER 1 0
1 0 0
Payments from the agencies and bodies
1 0 0 0 | European Environment Agency | 600000 | –315000 | 285000 |
1 0 0 1 | European Training Foundation | 357000 | | 357000 |
1 0 0 2 | European Monitoring Centre for Drugs and Drug Addiction | 805000 | –750000 | 55000 |
1 0 0 3 | European Medicines Agency | 900000 | | 900000 |
1 0 0 4 | European Agency for Safety and Health at Work | 793000 | 200000 | 993000 |
1 0 0 5 | Office for Harmonization in the Internal Market | 2497000 | –900000 | 1597000 |
1 0 0 6 | OHIM trade marks and designs | 15422000 | | 15422000 |
1 0 0 7 | Community Plant Variety Office | 150000 | –50000 | 100000 |
1 0 0 8 | Europol | 1700000 | –450000 | 1250000 |
1 0 0 9 | European Foundation for the Improvement of Living and Working Conditions | 610000 | | 610000 |
| Article 1 0 0 — Total | 23834000 | –2265000 | 21569000 |
1 0 1
1 0 1 0 | Cedefop | 80500 | –50000 | 30500 |
1 0 1 2 | European Monitoring Centre on Racism and Xenophobia | 330000 | | 330000 |
1 0 1 3 | European Agency for Reconstruction | p.m. | 20000 | 20000 |
1 0 1 4 | European Investment Bank | p.m. | | p.m. |
1 0 1 5 | European Food Safety Authority | 620000 | –220000 | 400000 |
1 0 1 6 | Eurojust | 80500 | –70000 | 10500 |
1 0 1 7 | European Maritime Safety Agency | 80500 | | 80500 |
1 0 1 8 | European Aviation Safety Agency | 800000 | –600000 | 200000 |
1 0 1 9 | European Railway Safety Agency | p.m. | | p.m. |
| Article 1 0 1 — Total | 1991500 | –920000 | 1071500 |
1 0 2
1 0 2 0 | European Network and Information Security Agency | 60000 | –30000 | 30000 |
1 0 2 1 | European Centre for Disease Prevention and Control | p.m. | | p.m. |
1 0 2 2 | European Agency for the Management of Operational Cooperation at the External Borders | p.m. | | p.m. |
| Article 1 0 2 — Total | 60000 | –30000 | 30000 |
| CHAPTER 1 0 — TOTAL | 25885500 | –3215000 | 22670500 |
| Title 1 — Total | 25885500 | –3215000 | 22670500 |
CHAPTER 1 0 —PAYMENTS FROM THE AGENCIES AND BODIES
1 0 0Payments from the agencies and bodies
Remarks
Article 10(2)(b) of Council Regulation (EC) No 2965/94, as amended by Council Regulation (EC) No 1645/2003.
1 0 0 0European Environment Agency
Financial year 2005 | AB 2005 | Total 2005 |
600000 | –315000 | 285000 |
1 0 0 1European Training Foundation
Financial year 2005 | AB 2005 | Total 2005 |
357000 | | 357000 |
1 0 0 2European Monitoring Centre for Drugs and Drug Addiction
Financial year 2005 | AB 2005 | Total 2005 |
805000 | –750000 | 55000 |
1 0 0 3European Medicines Agency
Financial year 2005 | AB 2005 | Total 2005 |
900000 | | 900000 |
1 0 0 4European Agency for Safety and Health at Work
Financial year 2005 | AB 2005 | Total 2005 |
793000 | 200000 | 993000 |
1 0 0 5Office for Harmonization in the Internal Market
Financial year 2005 | AB 2005 | Total 2005 |
2497000 | –900000 | 1597000 |
1 0 0 6OHIM trade marks and designs
Financial year 2005 | AB 2005 | Total 2005 |
15422000 | | 15422000 |
1 0 0 7Community Plant Variety Office
Financial year 2005 | AB 2005 | Total 2005 |
150000 | –50000 | 100000 |
1 0 0 8Europol
Financial year 2005 | AB 2005 | Total 2005 |
1700000 | –450000 | 1250000 |
1 0 0 9European Foundation for the Improvement of Living and Working Conditions
Financial year 2005 | AB 2005 | Total 2005 |
610000 | | 610000 |
1 0 1
1 0 1 0Cedefop
Financial year 2005 | AB 2005 | Total 2005 |
80500 | –50000 | 30500 |
1 0 1 2European Monitoring Centre on Racism and Xenophobia
Financial year 2005 | AB 2005 | Total 2005 |
330000 | | 330000 |
1 0 1 3European Agency for Reconstruction
Financial year 2005 | AB 2005 | Total 2005 |
p.m. | 20000 | 20000 |
1 0 1 4European Investment Bank
Financial year 2005 | AB 2005 | Total 2005 |
p.m. | | p.m. |
1 0 1 5European Food Safety Authority
Financial year 2005 | AB 2005 | Total 2005 |
620000 | –220000 | 400000 |
1 0 1 6Eurojust
Financial year 2005 | AB 2005 | Total 2005 |
80500 | –70000 | 10500 |
1 0 1 7European Maritime Safety Agency
Financial year 2005 | AB 2005 | Total 2005 |
80500 | | 80500 |
1 0 1 8European Aviation Safety Agency
Financial year 2005 | AB 2005 | Total 2005 |
800000 | –600000 | 200000 |
1 0 1 9European Railway Safety Agency
Financial year 2005 | AB 2005 | Total 2005 |
p.m. | | p.m. |
1 0 2
1 0 2 0European Network and Information Security Agency
Financial year 2005 | AB 2005 | Total 2005 |
60000 | –30000 | 30000 |
1 0 2 1European Centre for Disease Prevention and Control
Financial year 2005 | AB 2005 | Total 2005 |
p.m. | | p.m. |
1 0 2 2European Agency for the Management of Operational Cooperation at the External Borders
Financial year 2005 | AB 2005 | Total 2005 |
p.m. | | p.m. |
TITLE 2
SUBSIDY FROM THE COMMISSION
CHAPTER 2 0 — SUBSIDY FROM THE COMMISSION
Article Item | Heading | Financial year 2005 | AB 2005 | Total 2005 |
CHAPTER 2 0
2 0 0 | Subsidy from the Commission | p.m. | | p.m. |
| CHAPTER 2 0 — TOTAL | p.m. | | p.m. |
| Title 2 — Total | p.m. | | p.m. |
CHAPTER 2 0 —SUBSIDY FROM THE COMMISSION
2 0 0Subsidy from the Commission
Financial year 2005 | AB 2005 | Total 2005 |
p.m. | | p.m. |
Remarks
Article 10(2)(c) of Council Regulation (EC) No 2965/94, as amended by Council Regulation (EC) No 1645/2003.
TITLE 3
INTERINSTITUTIONAL COOPERATION
CHAPTER 3 0 — INTERINSTITUTIONAL COOPERATION
Article Item | Heading | Financial year 2005 | AB 2005 | Total 2005 |
CHAPTER 3 0
3 0 0
Interinstitutional cooperation
3 0 0 0 | Commission — DGs | 1192000 | –850000 | 342000 |
3 0 0 1 | Commission — DGT | 262500 | | 262500 |
3 0 0 2 | Management of interinstitutional projects | 698000 | | 698000 |
3 0 0 3 | European Parliament | p.m. | | p.m. |
3 0 0 4 | Council of the European Union | p.m. | | p.m. |
3 0 0 5 | European Court of Auditors | 145000 | 45000 | 190000 |
3 0 0 6 | Committee of the Regions of the European Union | p.m. | | p.m. |
3 0 0 7 | Economic and Social Committee of the European Communities | p.m. | | p.m. |
3 0 0 8 | Court of Justice of the European Communities | p.m. | | p.m. |
3 0 0 9 | Contribution to Community programmes | p.m. | | p.m. |
| Article 3 0 0 — Total | 2297500 | –805000 | 1492500 |
3 0 1
European Central Bank
3 0 1 0 | European Central Bank | 41000 | –38000 | 3000 |
3 0 1 1 | European Ombudsman | | | p.m. |
| Article 3 0 1 — Total | 41000 | –38000 | 3000 |
| CHAPTER 3 0 — TOTAL | 2338500 | –843000 | 1495500 |
| Title 3 — Total | 2338500 | –843000 | 1495500 |
CHAPTER 3 0 —INTERINSTITUTIONAL COOPERATION
3 0 0Interinstitutional cooperation
Remarks
Article 10(2)(b) of Council Regulation (EC) No 2965/94, as amended by Council Regulation (EC) No 1645/2003.
3 0 0 0Commission — DGs
Financial year 2005 | AB 2005 | Total 2005 |
1192000 | –850000 | 342000 |
3 0 0 1Commission — DGT
Financial year 2005 | AB 2005 | Total 2005 |
262500 | | 262500 |
3 0 0 2Management of interinstitutional projects
Financial year 2005 | AB 2005 | Total 2005 |
698000 | | 698000 |
3 0 0 3European Parliament
Financial year 2005 | AB 2005 | Total 2005 |
p.m. | | p.m. |
3 0 0 4Council of the European Union
Financial year 2005 | AB 2005 | Total 2005 |
p.m. | | p.m. |
3 0 0 5European Court of Auditors
Financial year 2005 | AB 2005 | Total 2005 |
145000 | 45000 | 190000 |
3 0 0 6Committee of the Regions of the European Union
Financial year 2005 | AB 2005 | Total 2005 |
p.m. | | p.m. |
3 0 0 7Economic and Social Committee of the European Communities
Financial year 2005 | AB 2005 | Total 2005 |
p.m. | | p.m. |
3 0 0 8Court of Justice of the European Communities
Financial year 2005 | AB 2005 | Total 2005 |
p.m. | | p.m. |
3 0 0 9Contribution to Community programmes
Financial year 2005 | AB 2005 | Total 2005 |
p.m. | | p.m. |
Remarks
Revenue deriving from the Translation Centre's participation in Community programmes.
3 0 1European Central Bank
3 0 1 0European Central Bank
Financial year 2005 | AB 2005 | Total 2005 |
41000 | –38000 | 3000 |
3 0 1 1European Ombudsman
Financial year 2005 | AB 2005 | Total 2005 |
| | p.m. |
TITLE 4
OTHER REVENUE
CHAPTER 4 0 — OTHER REVENUE
Article Item | Heading | Financial year 2005 | AB 2005 | Total 2005 |
CHAPTER 4 0
4 0 0
Bank interest
4 0 0 0 | Bank interest | 200000 | 100000 | 300000 |
| Article 4 0 0 — Total | 200000 | 100000 | 300000 |
4 0 1
Miscellaneous repayments
4 0 1 0 | Miscellaneous repayments | p.m. | | p.m. |
| Article 4 0 1 — Total | p.m. | | p.m. |
| CHAPTER 4 0 — TOTAL | 200000 | 100000 | 300000 |
| Title 4 — Total | 200000 | 100000 | 300000 |
CHAPTER 4 0 —OTHER REVENUE
4 0 0Bank interest
Remarks
This article covers interest which the Centre will receive on its bank account.
4 0 0 0Bank interest
Financial year 2005 | AB 2005 | Total 2005 |
200000 | 100000 | 300000 |
4 0 1Miscellaneous repayments
Remarks
This article covers miscellaneous repayments (private telephone calls, etc.).
4 0 1 0Miscellaneous repayments
Financial year 2005 | AB 2005 | Total 2005 |
p.m. | | p.m. |
TITLE 5
SURPLUS CARRIED OVER FROM THE PREVIOUS FINANCIAL YEAR
CHAPTER 5 0 — SURPLUS CARRIED OVER FROM THE PREVIOUS FINANCIAL YEAR
Article Item | Heading | Financial year 2005 | AB 2005 | Total 2005 |
CHAPTER 5 0
5 0 0
Surplus carried over from the previous financial year
5 0 0 0 | Surplus carried over from the previous financial year | p.m. | 3496684 | 3496684 |
| Article 5 0 0 — Total | p.m. | 3496684 | 3496684 |
| CHAPTER 5 0 — TOTAL | p.m. | 3496684 | 3496684 |
| Title 5 — Total | p.m. | 3496684 | 3496684 |
| GRAND TOTAL | 28424000 | –461316 | 27962684 |
CHAPTER 5 0 —SURPLUS CARRIED OVER FROM THE PREVIOUS FINANCIAL YEAR
5 0 0Surplus carried over from the previous financial year
5 0 0 0Surplus carried over from the previous financial year
Financial year 2005 | AB 2005 | Total 2005 |
p.m. | 3496684 | 3496684 |
Remarks
Surplus carried forward from the previous financial year. Article 16 of the Financial Regulation of the Centre of 22 December 2003.
STATEMENT OF EXPENDITURE
Title Chapter | Heading | Financial year 2005 | AB 2005 | Total 2005 |
1
STAFF
1 1 | STAFF IN ACTIVE EMPLOYMENT | 14691200 | –461316 | 14229884 |
| Title 1 — Total | 15078700 | –461316 | 14617384 |
| GRAND TOTAL | 28424000 | –461316 | 27962684 |
TITLE 1
STAFF
CHAPTER 1 1 — STAFF IN ACTIVE EMPLOYMENT
Article Item | Heading | Financial year 2005 | AB 2005 | Total 2005 |
CHAPTER 1 1
1 1 0
Staff in active employment
1 1 0 0 | Basic salaries | 10600000 | –336000 | 10264000 |
1 1 0 2 | Expatriation and foreign-residence allowances | 1700000 | –125316 | 1574684 |
| Article 1 1 0 — Total | 13105000 | –461316 | 12643684 |
| CHAPTER 1 1 — TOTAL | 14691200 | –461316 | 14229884 |
| Title 1 — Total | 15078700 | –461316 | 14617384 |
| GRAND TOTAL | 28424000 | –461316 | 27962684 |
CHAPTER 1 1 —STAFF IN ACTIVE EMPLOYMENT
1 1 0Staff in active employment
1 1 0 0Basic salaries
Financial year 2005 | AB 2005 | Total 2005 |
10600000 | –336000 | 10264000 |
Remarks
Staff Regulations of officials and Conditions of employment of other servants of the European Communities, in particular Articles 62 and 66 thereof.
This appropriation is intended to cover basic salaries of officials and temporary staff.
1 1 0 2Expatriation and foreign-residence allowances
Financial year 2005 | AB 2005 | Total 2005 |
1700000 | –125316 | 1574684 |
Remarks
Staff Regulations of officials of the European Communities, in particular Articles 62 and 69 thereof and Article 4 of Annex VII thereto.
This appropriation is intended to cover the expatriation and foreign-residence allowances for officials and temporary staff.
--------------------------------------------------
Category and career | Permanent posts | Temporary posts |
2005 | 2004 | 2003 | 2005 | 2004 | 2003 |
A*16 | — | — | — | — | — | — |
A*15 | — | — | — | 1 | 1 | 1 |
A*14 | 1 | 1 | 1 | — | — | — |
A*13 | — | — | — | — | — | — |
A*12 | 2 | 1 | — | 4 | 4 | 4 |
A*11 | 9 | 7 | 3 | 8 | 9 | 7 |
A*10 | 7 | 1 | 1 | 30 | 32 | 22 |
A*9 | — | — | — | 7 | 2 | — |
A*8 | 4 | 8 | 2 | 9 | 14 | 26 |
A*7 | — | — | — | 16 | 18 | 1 |
A*6 | — | — | | — | — | — |
A*5 | — | — | — | 2 | 2 | — |
Sub-total A | 23 | 18 | 7 | 77 | 82 | 61 |
B*11 | — | — | — | — | — | — |
B*10 | — | — | — | — | — | — |
B*9 | — | — | — | — | — | — |
B*8 | 1 | — | — | 2 | 2 | 1 |
B*7 | 4 | 4 | 2 | 3 | 3 | 7 |
B*6 | — | — | 1 | 9 | 8 | 6 |
B*5 | 3 | 3 | — | 7 | 9 | 5 |
B*4 | — | — | — | 1 | 1 | — |
B*3 | — | — | — | 1 | 1 | — |
Sub-total B | 8 | 7 | 3 | 23 | 24 | 19 |
C*7 | — | — | — | — | — | — |
C*6 | — | — | — | 1 | 1 | — |
C*5 | — | — | — | 3 | 2 | 2 |
C*4 | 2 | 1 | — | 7 | 7 | 6 |
C*3 | 2 | — | — | 19 | 20 | 15 |
C*2 | 1 | 2 | — | 15 | 17 | 15 |
C*1 | — | — | — | — | — | — |
Sub-total C | 5 | 3 | — | 45 | 47 | 38 |
D*5 | — | — | — | — | — | — |
D*4 | — | — | — | — | — | 1 |
D*3 | — | — | — | — | — | 2 |
D*2 | — | — | — | — | — | 1 |
Sub-total D | — | — | — | — | — | 4 |
Total | 36 | 28 | 10 | 145 | 153 | 122 |
--------------------------------------------------
