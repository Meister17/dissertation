Commission Decision
of 23 December 2003
amending Decision 2003/467/EC as regards the declaration that certain provinces of Italy are free of bovine brucellosis and enzootic bovine leukosis
(notified under document number C(2003) 5063)
(Text with EEA relevance)
(2004/63/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 64/432/EEC of 26 June 1964 on health problems affecting intra-Community trade in bovine animals and swine(1), and in particular Annex A(II)(7) and Annex D(I)(E) thereto,
Whereas:
(1) The lists of regions of Member States declared free of bovine brucellosis and enzootic bovine leukosis are set out in Commission Decision 2003/467/EC of 25 June 2003 establishing the official tuberculosis, brucellosis and enzootic-bovine-leukosis free status of certain Member States and regions of Member States as regards bovine herds(2).
(2) Italy submitted to the Commission documentation demonstrating compliance with the appropriate conditions provided for in Directive 64/432/EEC as regards the provinces of Cremona, Lodi and Pavia in the Region of Lombardia in order that those provinces may be declared officially free of brucellosis as regards bovine herds.
(3) Italy also submitted to the Commission documentation demonstrating compliance with the appropriate conditions provided for in Directive 64/432/EEC as regards the provinces of Milano, Lodi and Cremona in the Region of Lombardia and the provinces of Arezzo, Firenze, Grossetto, Livorno, Lucca, Pisa, Pistoia, Prato, and Siena in the Region of Toscana, in order that those provinces may be declared officially free of enzootic bovine leukosis as regards bovine herds.
(4) Following evaluation of the documentation submitted by Italy, the provinces of Cremona, Lodi and Pavia in the Region of Lombardia should be declared officially free of bovine brucellosis and the provinces of Milano, Lodi and Cremona in the Region of Lombardia and the provinces of Arezzo, Firenze, Grossetto, Livorno, Lucca, Pisa, Pistoia, Prato, and Siena in the Region of Toscana should be declared officially free of enzootic bovine leukosis.
(5) Decision 2003/467/EC should therefore be amended accordingly.
(6) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
Annexes II and III to Decision 2003/467/EC are amended in accordance with the Annex to this Decision.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 23 December 2003.
For the Commission
David Byrne
Member of the Commission
(1) OJ 121, 29.7.1964, p.1977/64. Directive as last amended by the Act of Accession of 2003.
(2) OJ L 156, 25.6.2003, p. 74.
ANNEX
Annexes II and III to Decision 2003/467/EC are amended as follows:
1. In Annex II, Chapter 2 is replaced by the following:
"CHAPTER 2 OFFICIALLY BRUCELLOSIS-FREE REGIONS OF MEMBER STATES
In Italy:
- Region Emilia-Romagna: Provinces of Bologna, Ferrara, Forli-Cesena, Modena, Parma, Piacenza, Ravenna, Reggio Emilia, Rimini
- Region Lombardia: Provinces of Bergamo, Como, Cremona, Lecco, Lodi, Mantova, Pavia, Sondrio, Varese
- Region Marche: Province of Ascoli Piceno
- Region Sardinia: Provinces of Cagliari, Nuoro, Oristano, Sassari
- Region Trentino-Alto Aldige: Provinces of Bolzano, Trento
In Portugal:
- Autonomous Region of Azores: Islands of Pico, Graciosa, Flores, Corvo
In the United Kingdom:
- Great Britain: England, Scotland, Wales".
2. In Annex III, Chapter 2 is replaced by the following:
"CHAPTER 2 OFFICIALLY ENZOOTIC-BOVINE-LEUKOSIS-FREE REGIONS OF MEMBER STATES
In Italy:
- Region Emilia-Romagna: Provinces of Bologna, Ferrara, Forli-Cesena, Modena, Parma, Piacenza, Ravenna, Reggio Emilia, Rimini
- Region Lombardia: Provinces of Bergamo, Brescia, Como, Cremona, Lecco, Lodi, Mantova, Milano, Sondrio, Varese
- Region Marche: Province of Ascoli Piceno
- Region Toscana: Provinces of Arezzo, Firenze, Grossetto, Livorno, Lucca, Pisa, Pistoia, Prato, Siena
- Region Trentino-Alto Aldige: Provinces of Bolzano, Trento
- Region Val d'Aosta: Province of Aosta"
