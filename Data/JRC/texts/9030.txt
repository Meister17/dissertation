Decision of the EEA Joint Committee No 67/2006
of 2 June 2006
amending Annex XIII (Transport) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex XIII to the Agreement was amended by Decision of the EEA Joint Committee No 35/2006 of 10 March 2006 [1].
(2) Regulation (EC) No 549/2004 of the European Parliament and of the Council of 10 March 2004 laying down the framework for the creation of the single European sky [2] is to be incorporated into the Agreement.
(3) Regulation (EC) No 550/2004 of the European Parliament and of the Council of 10 March 2004 on the provision of air navigation services in the single European sky [3] is to be incorporated into the Agreement.
(4) Regulation (EC) No 551/2004 of the European Parliament and of the Council of 10 March 2004 on the organisation and use of the airspace in the single European sky [4] is to be incorporated into the Agreement.
(5) Regulation (EC) No 552/2004 of the European Parliament and of the Council of 10 March 2004 on the interoperability of the European Air Traffic Management network [5] is to be incorporated into the Agreement.
(6) This Decision is not to apply to Liechtenstein.
(7) The EFTA States take note of and associate themselves with the Statement by the EC Member States on military issues related to the Single European Sky [6],
HAS DECIDED AS FOLLOWS:
Article 1
The following points shall be inserted after point 66s (Commission Regulation (EC) No 488/2005) of Annex XIII to the Agreement:
"66t. 32004 R 0549: Regulation (EC) No 549/2004 of the European Parliament and of the Council of 10 March 2004 laying down the framework for the creation of the single European sky (OJ L 96, 31.3.2004, p. 1).
The provisions of the Regulation shall, for the purposes of this Agreement, be read with the following adaptations:
(a) The following paragraph shall be added in Article 5:
"5. The EFTA States shall participate fully in the committee established pursuant to paragraph 1, except for the right to vote."
(b) This Regulation shall not apply to Liechtenstein.
66u. 32004 R 0550: Regulation (EC) No 550/2004 of the European Parliament and of the Council of 10 March 2004 on the provision of air navigation services in the single European sky (OJ L 96, 31.3.2004, p. 10).
The provisions of the Regulation shall, for the purposes of this Agreement, be read with the following adaptations:
(a) With regard to Iceland the last sentence of Article 14 shall read as follows:
"This scheme shall be consistent with Article 15 of the 1944 Chicago Convention on International Civil Aviation and with Eurocontrol's charging system for en route charges or with Joint Financing Agreements administered by ICAO for the North-Atlantic region."
(b) With regard to Iceland the following shall be added at the end of the first sentence of Article 15(2)(b):
"or North-Atlantic Region."
(c) Where the EFTA Surveillance Authority, in accordance with Article 16(3), addresses a decision to the EFTA States, any EFTA State may refer the decision to the EFTA Standing Committee within one month. The EFTA Standing Committee may take a different decision within a period of one month.
(d) This Regulation shall not apply to Liechtenstein.
66v. 32004 R 0551: Regulation (EC) No 551/2004 of the European Parliament and of the Council of 10 March 2004 on the organisation and use of the airspace in the single European sky (OJ L 96, 31.3.2004, p. 20).
The provisions of the Regulation shall, for the purposes of this Agreement, be read with the following adaptations:
(a) The following paragraph shall be added in Article 2:
"Where EC Member States on the one side and EFTA States on the other side are concerned, the EC Commission and the EFTA Surveillance Authority shall consult each other and exchange information when preparing their respective decisions in accordance with this Article."
(b) The following shall be added in Article 5(4):
"If a mutual agreement is to be concluded between one or more EC Member States on the one side and one or more EFTA States on the other side, they shall only act after having consulted interested parties, including the Commission, the EFTA Surveillance Authority and the other EC Member States and EFTA States."
(c) This Regulation shall not apply to Liechtenstein.
66w. 32004 R 0552: Regulation (EC) No 552/2004 of the European Parliament and of the Council of 10 March 2004 on the interoperability of the European Air Traffic Management network (OJ L 96, 31.3.2004, p. 26).
The provisions of the Regulation shall, for the purposes of this Agreement, be read with the following adaptation:
This Regulation shall not apply to Liechtenstein."
Article 2
The texts of Regulations (EC) Nos 549/2004, 550/2004, 551/2004 and 552/2004 in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 3 June 2006, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [7].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 2 June 2006.
For the EEA Joint Committee
The President
R. Wright
[1] OJ L 147, 1.6.2006, p. 53.
[2] OJ L 96, 31.3.2004, p. 1.
[3] OJ L 96, 31.3.2004, p. 10.
[4] OJ L 96, 31.3.2004, p. 20.
[5] OJ L 96, 31.3.2004, p. 26.
[6] OJ L 96, 31.3.2004, p. 9.
[7] Constitutional requirements indicated.
--------------------------------------------------
Joint Statement by the Contracting Parties
to Decision No 67/2006 incorporating Regulations (EC) Nos 549/2004, 550/2004, 551/2004 and 552/2004 of the European Parliament and of the Council into the EEA Agreement
With regard to Article 6 of Regulation (EC) No 549/2004, the Contracting Parties recognise that stakeholders from the EFTA States are eligible to be involved in the activities of the "Industry Consultation Body" on the same basis as stakeholders from the EU Member States.
With regard to Article 11 of Regulation (EC) No 549/2004, the Contracting Parties recognise the importance of exchanging information in accordance with, and without prejudice to, paragraph 5 of Protocol 1 to the EEA Agreement, and of the Commission taking note of the performance review regarding the EFTA States.
With respect to the Joint Financing Agreement to which Iceland is a party, the Contracting Parties agree that such a scheme is consistent with Article 14 of Regulation (EC) No 550/2004.
--------------------------------------------------
