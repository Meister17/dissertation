Notice of initiation of an expiry review of the countervailing measures applicable to imports of certain polyethylene terephthalate originating in, inter alia, India
(2005/C 304/03)
Following the publication of a notice of impending expiry [1] of the anti-subsidy measures in force on imports of certain polyethylene terephthalate originating in, inter alia, India, (country concerned), the Commission has received a request for review pursuant to Article 18 of Council Regulation (EC) No 2026/97 on protection against subsidised imports from countries not members of the European Community (the basic Regulation) [2], as last amended by Council Regulation (EC) No 461/2004 [3].
1. Request for review
The request was lodged on 30 August 2005 by the Polyethylene Terephthalate (PET) Committee of PlasticsEurope (the applicant) on behalf of producers representing a major proportion, in this case more than 90 %, of the total Community production of certain polyethylene terephthalate.
2. Product
The product under review is polyethylene terephthalate with a coefficient of viscosity of 78 ml/g or higher, according to DIN (Deutsche Industrienorm) 53728 originating in India (the product concerned), currently classifiable within CN code 39076020. This CN code is given only for information.
3. Existing measures
The measures currently in force are a definitive countervailing duty imposed by Council Regulation (EC) No 2603/2000 [4], as last amended by Council Regulation (EC) No 1645/2005 [5] and undertakings accepted by Commission Decision No 2000/745/EC [6] as last amended by Commission Decision No 2005/697/EC [7].
4. Grounds for the review
The applicants have provided evidence that the expiry of the measures would lead to a continuation or recurrence of subsidisation and injury.
It is alleged that the producers of the product concerned have benefited and will continue to benefit from a number of subsidies granted by the Government of India. These alleged subsidies consist of schemes of benefits to industries located in special economic zones/export oriented units; the advance licenses; the duty entitlement passbook scheme; an income tax exemption; the export promotion capital goods scheme; export credits; the package scheme of incentives of the Government of Maharashtra; the Gujarat sales tax incentive scheme; the Gujarat electricity duty exemption scheme and the West Bengal incentive scheme.
The total subsidy is estimated to be significant.
It is alleged that the above schemes are subsidies since they involve a financial contribution from the Government of India or other regional governments and confer a benefit to the recipients, i.e. to exporters/producers of certain polyethylene terephthalate. They are alleged to be contingent upon export performance and therefore specific and countervailable or to be otherwise specific and countervailable.
The applicant further alleges the likelihood of injurious subsidisation. In this respect the applicant presents evidence that, should measures be allowed to lapse, the current import level of the product concerned is likely to increase due to the existence of unused capacity and the recent investments in production capacity in the country concerned.
In addition, the applicant alleges that the removal of injury is mainly due to the existence of measures and that any recurrence of substantial subsidised imports from the country concerned would likely lead to a recurrence of further injury of the Community industry should measures be allowed to lapse.
5. Procedure
Having determined, after consulting the Advisory Committee, that sufficient evidence exists to justify the initiation of an expiry review, the Commission hereby initiates a review in accordance with Article 18 of the basic Regulation.
5.1. Procedure for the determination of likelihood of subsidization and injury
The investigation will determine whether the expiry of the measures would be likely, or unlikely, to lead to a continuation or recurrence of subsidization and injury.
(a) Sampling
In view of the apparent number of parties involved in this proceeding, the Commission may decide to apply sampling, in accordance with Article 27 of the basic Regulation.
(i) Sampling for investigation of subsidisation in India
In order to enable the Commission to decide whether sampling is necessary and, if so, to select a sample, all exporters producers, or representatives acting on their behalf, are hereby requested to make themselves known by contacting the Commission and providing the following information on their company or companies within the time limit set in point 6(b)(i) and in the formats indicated in point 7:
- name, address, e-mail address, telephone, and fax number and contact person,
- the turnover in local currency and the sales volume in tonnes of the product concerned sold for export to the Community and exports to other countries (separately) during the period 1 October 2004 until 30 September 2005
- the turnover in local currency and the sales volume in tonnes of the product concerned sold on the domestic market during the period 1 October 2004 until 30 September 2005,
- whether the company intends to claim an individual subsidy rate (individual subsidy rates can only be claimed by producers [8]),
- the precise activities of the company with regard to the production of the product concerned and the production volume in tonnes of the product concerned, the production capacity and the investments in production capacity during the period 1 October 2004 until 30 September 2005,
- the names and the precise activities of all related companies [9] involved in the production and/or selling (export and/or domestic) of the product concerned,
- an indication whether the company is recognised as an export oriented unit,
- an indication whether the company is located in a special economic zone,
- any other relevant information that would assist the Commission in the selection of the sample,
- by providing the above information, the company agrees to its possible inclusion in the sample. If the company is chosen to be part of the sample, this will imply replying to a questionnaire and accepting an on-the-spot investigation of its response. If the company indicates that it does not agree to its possible inclusion in the sample, it will be deemed to not have cooperated in the investigation. The consequences of non-cooperation are set out in point 8 below.
In order to obtain the information it deems necessary for the selection of the sample of exporters/producers, the Commission will, in addition, contact the authorities of the exporting country, and any known associations of exporters/producers.
(ii) Sampling for importers
In order to enable the Commission to decide whether sampling is necessary and, if so, to select a sample, all importers, or representatives acting on their behalf, are hereby requested to make themselves known to the Commission and to provide the following information on their company or companies within the time limit set in point 6(b)(i) and in the formats indicated in point 7:
- name, address, e-mail address, telephone and fax number and contact person,
- the total turnover in EUR of the company during the period 1 October 2004 until 30 September 2005,
- the total number of employees,
- the precise activities of the company with regard to the product concerned,
- the volume in tonnes and value in EUR of imports into and resales made in the Community market during the period 1 October 2004 until 30 September 2005 of the imported product concerned originating in India,
- the names and the precise activities of all related companies [10] involved in the production and/or selling of the product concerned,
- any other relevant information that would assist the Commission in the selection of the sample,
- by providing the above information, the company agrees to its possible inclusion in the sample. If the company is chosen to be part of the sample, this will imply replying to a questionnaire and accepting an on-the-spot investigation of its response. If the company indicates that it does not agree to its possible inclusion in the sample, it will be deemed to not have cooperated in the investigation. The consequences of non-cooperation are set out in point 8 below.
In order to obtain the information it deems necessary for the selection of the sample of importers, the Commission will, in addition, contact any known associations of importers.
(iii) Sampling for Community producers
In view of the large number of Community producers supporting the request, the Commission intends to investigate injury to the Community industry by applying sampling.
In order to enable the Commission to select a sample, all Community producers are hereby requested to provide the following information on their company or companies within the time limit set in point 6(b)(i):
- name, address, e-mail address, telephone and fax number and contact person,
- the total turnover in EUR of the company during the period 1 October 2004 until 30 September 2005,
- the precise activities of the company with regard to the production of the product concerned and the volume in tonnes of the product concerned during the period 1 October 2004 until 30 September 2005,
- the value in EUR of sales of the product concerned made in the Community market during the period 1 October 2004 until 30 September 2005,
- the volume in tonnes of sales of the product concerned made in the Community market during the period 1 October 2004 until 30 September 2005,
- the volume in tonnes of the production of the product concerned during the period 1 October 2004 until 30 September 2005,
- the names and the precise activities of all related companies [11] involved in the production and/or selling of the product concerned,
- any other relevant information that would assist the Commission in the selection of the sample,
- by providing the above information, the company agrees to its possible inclusion in the sample. If the company is chosen to be part of the sample, this will imply replying to a questionnaire and accepting an on-the-spot investigation of its response. If the company indicates that it does not agree to its possible inclusion in the sample, it will be deemed to not have cooperated in the investigation. The consequences of non-cooperation are set out in point 8 below.
(iv) Final selection of the samples
All interested parties wishing to submit any relevant information regarding the selection of the samples must do so within the time limit set in point 6(b)(ii).
The Commission intends to make the final selection of the samples after having consulted the parties concerned that have expressed their willingness to be included in the sample.
Companies included in the samples must reply to a questionnaire within the time limit set in point 6(b)(iii) and must cooperate within the framework of the investigation.
If sufficient cooperation is not forthcoming, the Commission may base its findings, in accordance with Articles 27(4) and 28 of the basic Regulation, on the facts available. A finding based on facts available may be less advantageous to the party concerned, as explained in point 8.
(b) Questionnaires
In order to obtain the information it deems necessary for its investigation, the Commission will send questionnaires to the sampled Community industry and to any association of producers in the Community, to the sampled exporters/producers in India, to any association of exporters/producers, to the sampled importers, to any association of importers named in the request or which cooperated in the investigation leading to the measures subject to the present review and to the authorities of the exporting country concerned.
(c) Collection of information and holding of hearings
All interested parties are hereby invited to make their views known, submit information other than questionnaire replies and to provide supporting evidence. This information and supporting evidence must reach the Commission within the time limit set in point 6(a)(ii).
Furthermore, the Commission may hear interested parties, provided that they make a request showing that there are particular reasons why they should be heard. This request must be made within the time limit set in point 6(a)(iii).
5.2. Procedure for the assessment of Community interest
In accordance with Article 31 of the basic Regulation and in the event that the continuation or recurrence of subsidization and injury is confirmed, a determination will be made as to whether to maintain or repeal the anti-subsidy measures would not be against the Community interest. For this reason the Community industry, importers, their representative associations, representative users and representative consumer organisations, provided that they prove that there is an objective link between their activity and the product concerned, may, within the general time limits set in point 6(a)(ii), make themselves known and provide the Commission with information. The parties which have acted in conformity with the previous sentence may request a hearing, setting the particular reasons why they should be heard, within the time limit set in point 6(a)(iii). It should be noted that any information submitted pursuant to Article 31 will only be taken into account if supported by factual evidence at the time of submission.
6. Time limits
(a) General time limits
(i) For parties to request a questionnaire
All interested parties who did not cooperate in the investigation leading to the measures subject to the present review should request a questionnaire as soon as possible, but not later than 15 days after the publication of this notice in the Official Journal of the European Union.
(ii) For parties to make themselves known, to submit questionnaire replies and any other information
All interested parties, if their representations are to be taken into account during the investigation, must make themselves known by contacting the Commission, present their views and submit questionnaire replies or any other information within 40 days of the date of publication of this notice in the Official Journal of the European Union, unless otherwise specified. Attention is drawn to the fact that the exercise of most procedural rights set out in the basic Regulation depends on the party's making itself known within the aforementioned period.
Companies selected in a sample must submit questionnaire replies within the time limit specified in point 6(b)(iii).
(iii) Hearings
All interested parties may also apply to be heard by the Commission within the same 40-day time limit.
(b) Specific time limit in respect of sampling
(i) The information specified in paragraph 5.1(a)(i), 5.1(a)(ii) and 5.1(a)(iii) should reach the Commission within 15 days of the date of publication of this notice in the Official Journal of the European Union, given that the Commission intends to consult parties concerned that have expressed their willingness to be included in the sample on its final selection within a period of 21 days of the publication of this notice in the Official Journal of the European Union.
(ii) All other information relevant for the selection of the sample as referred to in 5.1(a)(iv) must reach the Commission within a period of 21 days of the publication of this notice in the Official Journal of the European Union.
(iii) The questionnaire replies from sampled parties must reach the Commission within 37 days from the date of the notification of their inclusion in the sample.
7. Written submissions, questionnaire replies and correspondence
All submissions and requests made by interested parties must be made in writing (not in electronic format, unless otherwise specified) and must indicate the name, address, e-mail address, telephone and fax number of the interested party. All written submissions, including the information requested in this notice, questionnaire replies and correspondence provided by interested parties on a confidential basis shall be labelled as Limited [12] and, in accordance with Article 29(2) of the basic Regulation, shall be accompanied by a non-confidential version, which will be labelled FOR INSPECTION BY INTERESTED PARTIES.
Commission address for correspondence:
European Commission
Directorate General for Trade
Directorate B
Office: J-79 5/16
BE-1049 Brussels
Fax (32-2) 295 65 05
8. Non-cooperation
In cases in which any interested party refuses access to or does not provide the necessary information within the time limits, or significantly impedes the investigation, provisional or final findings, affirmative or negative, may be made in accordance with Article 28 of the basic Regulation, on the basis of the facts available.
Where it is found that any interested party has supplied false or misleading information, the information shall be disregarded and use may be made of the facts available. If an interested party does not cooperate or cooperates only partially and findings are therefore based on facts available in accordance with Article 28 of the basic Regulation, the result may be less favourable to that party than if it had cooperated.
9. Schedule of the investigation
The investigation will be concluded, according to Article 22(1) of the basic Regulation within 15 months of the date of the publication of this notice in the Official Journal of the European Union.
[1] OJ C 52, 2.3.2005, p. 2.
[2] OJ L 288, 21.10.97, p.1.
[3] OJ L 77, 13.3.2004, p.12.
[4] OJ L 301, 30.11.2000, p. 1.
[5] OJ L 266, 11.10.2005, p. 1.
[6] OJ L 301, 30.11.2000, p. 88.
[7] OJ L 266, 11.10.2005, p. 62.
[8] Individual margins may be claimed pursuant to Article 27(3) of the basic Regulation for companies not included in the sample.
[9] For guidance on the meaning of related companies, please refer to Article 143 of Commission Regulation (EEC) No 2454/93 as amended, laying down provisions for the implementation of Council Regulation (EEC) No 2913/92 establishing the Community Customs Code (OJ L 253, 11.10.1993, p.1).
[10] For guidance on the meaning of related companies, please refer to Article 143 of Commission Regulation (EEC) No 2454/93, as amended, laying down provisions for the implementation of Council Regulation (EEC) No 2913/92 establishing the Community Customs Code (OJ L 253, 11.10.1993, p.1).
[11] For guidance on the meaning of related companies, please refer to Article 143 of Commission Regulation (EEC) No 2454/93, as amended, laying down provisions for the implementation of Council Regulation (EEC) No 2913/92 establishing the Community Customs Code (OJ L 253, 11.10.1993, p.1).
[12] This means that the document is for internal use only. It is protected pursuant to Article 4 of Regulation (EC) No 1049/2001 of the European Parliament and of the Council regarding public access to European Parliament, Council and Commission documents (OJ L 145, 31.5.2001, p. 43). It is a confidential document pursuant to Article 29 of the basic Regulation and Article 12 of the WTO Agreement on Subsidies and Countervailing Measures.
--------------------------------------------------
