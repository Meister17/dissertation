Commission Regulation (EC) No 2461/2000
of 8 November 2000
amending Regulation (EC) No 2138/97 delimiting the homogenous olive oil production zones
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation No 136/66/EEC of 22 September 1966 on the common organisation of the market in oils and fats(1), as last amended by Regulation (EC) No 2702/1999(2),
Having regard to Council Regulation (EEC) No 2261/84 of 17 July 1984 laying down general rules on the granting of aid for the production of olive oil and of aid to olive oil producer organisations(3), as last amended by Regulation (EC) No 1639/98(4), and in particular Article 19 thereof,
Whereas:
(1) Article 18 of Regulation (EEC) No 2261/84 lays down that the olive yields and oil yields are to be fixed by homogenous production zones, on the basis of the figures supplied by producer Member States.
(2) The production zones are designated in the Annex to Commission Regulation (EC) No 2138/97(5), as last amended by Regulation (EC) No 2224/2000(6). For administrative and structural reasons, amendments should be made to the homogenous production zones in Greece for the 1999/2000 marketing year.
(3) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Oils and Fats,
HAS ADOPTED THIS REGULATION:
Article 1
Point C the Annex to Regulation (EC) No 2138/97 is amended as follows:
in the nomos "Ηρακλείου":
- the communes of "Κασσάνων", "Νιπηδητού" and "Παναγιάς" are deleted from zone 3,
- the communes of "Αφρατίου", "Εμπάρου", "Καραβάδου", "Μάρθας", "Μηλιαράδου" and "Ξενιάκου" are deleted from zone 4,
- a new zone known as "zone 10", is created, comprising the communes of "Αφρατίου", "Εμπάρου", "Καραβάδου", "Κασσάνων", "Μάρθας", "Μηλιαράδου", "Νιπηδητού", "Ξενιάκου" and "Παναγιάς"
- the commune of "Προφήτη Ηλία" is moved from zone 9 to zone 2.
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 8 November 2000.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ 172, 30.9.1966, p. 3025/66.
(2) OJ L 327, 21.12.1999, p. 7.
(3) OJ L 208, 3.8.1984, p. 3.
(4) OJ L 210, 28.7.1998, p. 38.
(5) OJ L 297, 31.10.1997, p. 3.
(6) OJ L 253, 7.10.2000, p. 16.
