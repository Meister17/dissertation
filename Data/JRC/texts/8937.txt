REGULATION (EEC) No 1365/75 OF THE COUNCIL of 26 May 1975 on the creation of a European Foundation for the improvement of living and working conditions
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, in particular Article 235 thereof;
Having regard to the proposal from the Commission;
Having regard to the Opinion of the European Parliament (1);
Having regard to the Opinion of the Economic and Social Committee (2);
Whereas the problems presented by the improvement of living and working conditions in modern society are increasingly numerous and complex ; whereas it is important that appropriate Community action should be built up on an inter-disciplinary scientific basis and at the same time that employers and workers should be associated in the action undertaken;
Whereas the Community is not yet in a position to undertake analyses, studies and research systematically and scientifically;
Whereas the programme of action of the European Communities on the environment (3) lays down that the Community institutions should set up a body capable of scanning those elements which, through their combined effects, affect living and working conditions, and of carrying out a long-term forward study of those factors which may endanger the conditions of existence and those which are capable of improving them;
Whereas the Council resolution of 21 January 1974 (4) concerning a social action programme, lays down inter alia an action programme for workers, aimed at humanizing living and working conditions;
Whereas the establishment of a Foundation is necessary if Community objectives for the improvement of living and working conditions are to be attained;
Whereas the powers required for the creation of such a Foundation are not provided for in the Treaty;
Whereas the Foundation will be set up within the framework of the European Communities and will function in accordance with Community law ; whereas the conditions under which certain general provisions will apply should be defined,
HAS ADOPTED THIS REGULATION:
Article 1
A European Foundation for the improvement of living and working conditions, hereinafter called "the Foundation", is hereby established.
Article 2
1. The aim of the Foundation shall be to contribute to the planning and establishment of better living and working conditions through action designed to increase and disseminate knowledge likely to assist this development.
2. With this aim in view, the tasks of the Foundation shall be to develop and to pursue ideas on the medium - and long-term improvement of living and working conditions in the light of practical experience and to identify factors leading to change. The Foundation shall take the relevant Community policies into account when carrying out its tasks. It shall advise the Community institutions on foreseeable objectives and guidelines by forwarding in particular scientific information and technical data. (1)OJ No C 76, 3.7.1974, p. 33. (2)OJ No C 109, 19.9.1974, p. 37. (3)OJ No C 112, 20.12.1973, p. 3. (4)OJ No C 13, 12.2.1974, p. 1.
3. As regards the improvement of living and working conditions, it shall deal more specifically with the following issues, determining the priorities to be observed: - man at work,
- organization of work and particularly job design,
- problems peculiar to certain categories of workers,
- long-terms aspects of improvement of the environment,
- distribution of human activities in space and in time.
Article 3
1. In order to achieve its aim, the Foundation shall foster the exchange of information and experience in these fields and shall, where appropriate, set up a system of information and documentation. It may, for example: (a) facilitate contact between universities, study and research institutes, economic and social administrations and organizations and encourage concerted action;
(b) set up working groups;
(c) conclude study contracts, participate in studies, promote and provide assistance for pilot projects and, where required, itself carry out certain studies;
(d) organize courses, conferences and seminars.
2. The Foundation shall cooperate as closely as possible with specialized institutes, foundations and bodies in the Member States or at international level.
Article 4
1. The Foundation shall be non-profit making. It shall enjoy in all the Member States the most extensive legal capacity accorded to legal persons.
2. The seat of the Foundation shall be in Ireland.
Article 5
The Foundation shall comprise: - an Administrative Board,
- a director and deputy director,
- a Committee of Experts.
Article 6
1. The Administrative Board shall consist of 30 members, of whom: (a) nine members shall represent the Governments of the Member States,
(b) nine members shall represent the employers' organizations,
(c) nine members shall represent the employees' organizations,
(d) three members shall represent the Commission.
2. The members referred to in (a), (b) and (c) shall be appointed by the Council on the basis of one member for each Member State and for each of the abovementioned categories. The Council shall at the same time appoint under the same conditions an alternate to attend meetings of the Administrative Board only in the absence of the member. The Commission shall appoint the members and alternates who are to represent it.
3. The term of office of members of the Administrative Board shall be three years. It shall be renewable. Upon the expiry of their term of office or in the event of their resignation, members shall remain in office until their appointments are renewed or until they are replaced.
4. The Administrative Board shall elect its chairman and three deputy chairmen from among its members, to serve for a period of one year.
5. The Chairman shall convene the Administrative Board at least twice a year and at the request of at least one-third of its members.
6. Decisions by the Administrative Board shall be taken by an absolute majority of its members.
Article 7
1. The Administrative Board shall administer the Foundation whose guidelines it shall lay down after consultation with the Committee of Experts. On the basis of a draft submitted by the director, the Administrative Board shall, in agreement with the Commission, adopt the programme of work.
2. It shall adopt its rules of procedure which shall enter into force after being approved by the Council, following consultation with the Commission.
3. It shall decide whether to accept any legacy, donation or subsidy from sources other than the Community.
Article 8
1. The director and deputy director of the Foundation shall be appointed by the Commission from a list of candidates submitted by the Administrative Board.
2. The director and the deputy director shall be chosen on the grounds of their competence and their independence shall be beyond doubt.
3. The director and the deputy director shall be appointed for a maximum period of five years. Their term of office shall be renewable.
Article 9
1. The director shall direct the Foundation and shall implement the decisions of the Administrative Board. He shall be the legal representative of the Foundation.
2. Under the provisions relating to staff, the director shall have authority over the staff of the Foundation. He shall be responsible for their recruitment and dismissal and for stipulating the qualifications required of them.
3. The director shall prepare the activities of the Administrative Board. The director, or the deputy director, or both shall attend the meetings of this board.
4. The director shall be accountable to the Administrative Board for the running of the Foundation.
Article 10
1. The Committee of Experts shall consist of 12 members appointed by the Council on a proposal from the Commission and selected from among scientific and other circles concerned in the Foundation's activity.
2. The Commission will take into account when drawing up its proposal: - the need to maintain a balance between the two complementary aspects of the Foundation - i.e. living conditions and working conditions;
- the need for the best possible scientific and technical advice;
- the need for at least one national from each Member State to be appointed.
3. The term of office of the members of the Committee of Experts shall be three years and shall be renewable.
Article 11
1. The task of the Committee of Experts shall be to deliver opinions to the other organs of the Foundation in all fields falling within the latter's competence, either at the request of the director or on its own initiative. All its opinions, including that given to the director for drawing up the programme of work (referred to in Article 12), shall be communicated simultaneously to the director and the Administrative Board.
2. The Committee shall select a chairman from among its members and draw up its rules of procedure.
3. The committee shall be convened by its chairman, by agreement with the director. Meetings shall be held at least twice a year and when requested by at least seven of its members.
Article 12
1. The director shall draw up an annual programme of work before 1 July each year on the basis of the guidelines referred to in Article 7. The annual programme shall be part of a four-year rolling programme. The projects in the annual programme shall be accompanied by an estimate of the necessary expenditure.
When drawing up the programme, the director shall take account of the opinions of the Committee of Experts, as well as of those of the Community institutions and the Economic and Social Committee.
To this end, and in order to avoid any duplication of work, the Community institutions and the Economic and Social Committee shall inform the Foundation of their requirements and as far as possible of any relevant work and studies which they are carrying out themselves.
2. The director shall forward the programme of work to the Administrative Board for approval.
Article 13
1. Not later than 31 March of each year, the director shall prepare a general report on the activities, financial situation and future guidelines of the Foundation and shall submit it to the Administrative Board.
2. After its adoption by the Administrative Board, the director shall forward the general report to the Community institutions and to the Economic and Social Committee.
Article 14
The Administrative Board shall draw up a balanced statement of all revenue and expenditure for each financial year, which shall be the same as the calendar year.
Article 15
1. The Administrative Board shall, by 31 March each year at the latest, send the Commission an estimate of revenue and expenditure. This estimate, which shall include an establishment plan, shall be forwarded by the Commission to the Council with the preliminary draft budget of the European Communities.
2. The budget of the European Communities shall each year, under a specific heading, include a subsidy for the Foundation.
The procedure in force for the transfer of appropriations from one chapter to another shall apply to the appropriation for this subsidy.
The Budget Authority shall draw up the establishment plan of the Foundation.
3. The Administrative Board shall adopt the estimate of revenue and expenditure before the beginning of the financial year, adjusting it to the subsidy granted by the Budget Authority. The estimate thus adopted shall be forwarded by the Commission to the Budget Authority.
Article 16
1. The financial provisions applying to the Foundation shall be adopted under Article 209 of the Treaty.
2. The Administrative Board shall by 31 March at the latest, send the accounts of all the revenue and expenditure of the Foundation for the preceding financial year to the Commission and to the Audit Board. The latter shall examine them in accordance with the second paragraph of Article 206 of the Treaty.
3. The Commission shall submit the accounts and the report of the Audit Board, together with its own comments, to the Council and to the European Parliament by 31 October at the latest. The Council and the European Parliament shall give a discharge to the Administrative Board of the Foundation under the procedure laid down in the fourth paragraph of Article 206 at the Treaty.
4. The financial controller of the Commission shall be responsible for checking the commitment and payment of all expenditure and the recording and recovery of all revenue of the Foundation.
Article 17
The provisions governing the staff of the Foundation shall be adopted by the Council, acting on a proposal from the Commission.
Article 18
Members of the Administrative Board, the director, the deputy director, the staff and all other persons participating in the activities of the Foundation shall be required, even after their duties have ceased, not to disclose information of the kind covered by the obligation of professional secrecy.
Article 19
The rules governing the languages of the European Communities shall apply to the Foundation.
Article 20
The Protocol on the privileges and immunities of the European Communities shall apply to the Foundation.
Article 21
1. The contractual liability of the Foundation shall be governed by the law applicable to the contract in question.
The Court of Justice of the European Communities shall have jurisdiction to give judgment pursuant to any arbitration clause contained in a contract concluded by the Foundation.
2. In the case of non-contractual liability, the Foundation shall, in accordance with the general principles common to the laws of the Member States, make good any damage caused by the Foundation or its servants in the performance of their duties.
The Court of Justice of the European Communities shall have jurisdiction in disputes relating to compensation for any such damage.
3. The personal liability of servants towards the Foundation shall be governed by the relevant provisions applying to the staff of the Foundation.
Article 22
Member States, members of the Administrative Board and third parties directly and personally involved may refer to the Commission any act of the Foundation, whether express or implied, for the Commission to examine the legality of that act.
Referral shall be made to the Commission within 15 days of the day on which the party concerned first became aware of the act in question.
The Commission shall take a decision within one month. If no decision has been taken within this period, the case shall be deemed to have been dismissed.
Article 23
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Luxembourg, 26 May 1975.
For the Council
The President
M.A. CLINTON
