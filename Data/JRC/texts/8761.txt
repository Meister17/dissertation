COUNCIL REGULATION (ECSC, EEC, EURATOM) No 4045/88 of 19 December 1988 laying down the emoluments of the President, Members and Registrar of the Court of First Instance of the European Communities
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to Council Decision 88/591/ECSC, EEC, Euratom of 24 October 1988 establishing a Court of First Instance of the European Communities (1), and in particular Article 2 (5) thereof,
Having regard to the Treaty establishing a Single Council and a Single Commission of the European Communities, and in particle Article 6 thereof,
Whereas, by the abovementioned Decision, the Council attached to the Court of Justice a Court of First Instance charged with exercising important judicial functions;
Whereas it is necessary to determine the salaries, pensions and any payment to be made instead of remuneration of the President, members and the Registrar of the Court of First Instance;
Whereas it is accordingly necessary to amend Council Regulation No 422/67/EEC, No 5/67/Euratom of 25 July 1967 determining the emoluments of the President and members of the Commission and of the President, Judges, Advocates-General and Registrar of the Court of Justice (2), as last amended by Regulation (Euratom, ECSC, EEC) No 3875/87 (3),
HAS ADOPTED THIS REGULATION:
Sole Article Regulation No 422/67/EEC, No 5/67/Euratom is hereby amended as follows:
1. The following words shall be added to the title:
´and of the President, Members and Registrar of the Court of First Instance'.
2. The following Article shall be inserted:
´Article 21a 1. Subject to paragraph 2 and 3, the provisions of this Regulation concerning the President, Members and the Registrar of the Court of Justice shall apply to the President, Members and Registrar of the Court of First Instance.
2. The basic monthly salary of the President, Members and Registrar of the Court of First Instance shall be equal to the amount resulting from application of the following percentages to the basic salary of an official of the European Communities on the last step of grade A 1:
President: 112,5 %,
Members: 104 %,
Registrar: 95 %.
3. The monthly entertainment allowance referred to in Article 4 (3) shall amount to:
President: Bfrs 21 015,
Members: Bfrs 19 170,
Registrar: Bfrs 16 299.
Presiding Judges of the Chambers of the Court shall in addition receive, during their term of office, a special duty allowance of Bfrs 25 573 per month.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 19 December 1988.
For the Council The President Th. PANGALOS (1) OJ No L 319, 25. 11. 1988, p. 1.
(2) OJ No 187, 8. 8. 1967, p. 1.
(3) OJ No L 363, 23. 12. 1987, p. 66.
