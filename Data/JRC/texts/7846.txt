COUNCIL DECISION
of 28 September 1987
concerning the conclusion of an Additional Protocol to the Cooperation Agreement between the European Economic Community and the Lebanese Republic
(87/513/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 238 thereof,
Having regard to the recommendation from the Commission,
Having regard to the assent of the European Parliament (1),
Whereas the Additional Protocol to the Cooperation Agreement between the European Economic Community and the Lebanese Republic (2), signed in Brussels on 3 May 1977, should be approved,
HAS DECIDED AS FOLLOWS:
Article 1
The Additional Protocol to the Cooperation Agreement between the European Economic Community and the
Lebanese Republic is hereby approved on behalf of the Community.
The text of the Protocol is attached to this Decision.
Article 2
The President of the Council shall give the notification provided for in Article 5 of the Protocol (3).
Article 3
This Decision shall take effect on the day following its publication in the Official Journal of the European Communities.
Done at Brussels, 28 September 1987.
For the Council
The President
B. HAARDER
EWG:L555UMBE17.95
FF: 5UEN; SETUP: 01; Hoehe: 451 mm; 42 Zeilen; 1593 Zeichen;
Bediener: PUPA Pr.: C;
Kunde: ................................
(1) Assent delivered on 16 September 1987 (not yet published in the Official Journal).
(2) OJ N° L 267, 27. 9. 1978, p. 2.
(3) The date of entry into force of the Protocol will be published in the Official Journal of the European Communities by the General Secretariat of the Council.
