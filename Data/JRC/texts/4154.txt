Council Regulation (EC) No 779/2005
of 23 May 2005
terminating the partial interim review of the anti-dumping measures applicable to imports of silicon carbide originating in Ukraine
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 384/96 of 22 December 1995 on protection against dumped imports from countries not members of the European Community [1] ("basic Regulation"), and in particular Article 11(3) thereof,
Having regard to the proposal submitted by the Commission after consulting the Advisory Committee,
Whereas:
A. PROCEDURE
1. Measures in force
(1) By Regulation (EC) No 821/94 [2], following an expiry review, the Council imposed a definitive anti-dumping duty on imports of silicon carbide originating inter alia in Ukraine ("the measures"). By Regulation (EC) No 1100/2000 [3], following an expiry review, requested by the European Chemical Industry Council ("CEFIC") the Council maintained the measures at their original level. By Regulation (EC) No 991/2004 [4] the Council amended Regulation (EC) No 1100/2000 as a consequence of the enlargement of the European Union by the accession of 10 new Member States on 1 May 2004 (the "EU 10") in order to provide, in the event of an undertaking being accepted by the Commission, for the possibility to exempt imports to the Community made under the terms of such undertaking from the anti-dumping duties imposed by Regulation (EC) No 1100/2000. By Decisions 2004/498/EC [5] and 2004/782/EC [6], the Commission accepted the undertakings offered by the Ukrainian exporting producer Open Joint Stock Company "Zaporozhsky Abrasivny Combinat" ("ZAC").
(2) The actual rate of the duty applicable to the net, free-at-Community-frontier price, before duty, is 24 % for imports of silicon carbide originating in Ukraine.
2. Current investigation
(3) The Commission received a request lodged by ZAC ("the applicant") for a partial interim review pursuant to Article 11(3) of the basic Regulation.
(4) The request was based on the prima facie evidence, provided by the applicant, that the circumstances on the basis of which measures were established have changed and that these changes are of lasting nature. The applicant alleged, inter alia, that the circumstances with regard to market economy status ("MES") had changed significantly. In particular, the applicant argued that it now fulfilled the requirements to be granted MES pursuant to Article 2(7)(b) of the basic Regulation. Furthermore, the applicant provided evidence showing that a comparison of normal value based on its own cost/domestic prices and its export prices to the USA as a third country market comparable to the EU, would lead to a reduction of dumping significantly below the level of the current measure. Accordingly, the applicant alleged that the continued imposition of the measure at its current level was no longer necessary to offset dumping.
(5) The Commission, after consulting the Advisory Committee, initiated on 7 January 2004 by notice published in the Official Journal of the European Union [7] a partial interim review limited in scope to the examination of dumping and MES as far as ZAC is concerned.
(6) The Commission sent a questionnaire and a claim form for MES pursuant to Article 2(7) of the basic Regulation to the applicant.
(7) The Commission sought and verified all the information it deemed necessary for the determination of dumping and MES. A verification visit was carried out at the premises of the applicant.
(8) The investigation of dumping covered the period from 1 January 2003 to 31 December 2003 (hereinafter referred to as "investigation period" or "IP").
3. Parties concerned by the investigation
(9) The Commission officially advised the exporting producer, the representatives of the exporting country and the Community producers of the initiation of the review. Interested parties were given the opportunity to make their views known in writing, to submit information and to provide supporting evidence and to request a hearing within the time limit set out in the notice of initiation. All interested parties who so requested and showed that there were reasons why they should be heard were granted a hearing.
(10) In this regard, the following interested parties made their views known:
(a) Community producers Association:
- European Chemical Industry Council (CEFIC)
(b) Community producer:
- Best-Business, Kunštát na Moravě, Czech Republic
(c) Exporting producer:
- Zaporozhsky Abrasivny Combinat, Zaporozhye, Ukraine
(d) Producers in analogue countries:
- Volzhsky Abrasive, Volshsky, Volgograd Region, Russia
- Saint-Gobain Materiais Cerâmicos Ltda, Barbacena, Brazil.
B. PRODUCT CONCERNED
(11) The product concerned by this proceeding is silicon carbide, falling within CN code 28492000 (hereinafter referred to as "silicon carbide" or the "product concerned"). No evidence was found suggesting that circumstances with regard to the product concerned had significantly changed since the imposition of the measures.
C. RESULT OF THE INVESTIGATION
1. Preliminary remark
(12) In accordance with Article 11(3) of the basic Regulation, the purpose of this type of review is to determine the need for the continued imposition of measures at their current level. In carrying out a partial interim review the Commission may, inter alia, consider whether the circumstances with regard to dumping have changed significantly. The Commission investigated all claims put forward by the applicant and the circumstances which could have changed significantly since the imposition of the measures: MES individual treatment ("IT"), the choice of an analogue country and the export prices of the applicant.
2. Market Economy Status (MES)
(13) The applicant requested MES pursuant to Article 2(7)(b) of the basic Regulation and submitted the claim form for market economy status within the deadline set out in the notice of initiation.
(14) Pursuant to Article 2(7)(b) of the basic Regulation, in anti-dumping investigations concerning imports originating in Ukraine, normal value shall be determined in accordance with paragraphs 1 to 6 of the said Article for those producers which were found to meet all five criteria laid down in Article 2(7)(c) of the basic Regulation.
(15) The investigation revealed that not all criteria were met by the applicant:
Article 2(7)(c) indent 1 | Article 2(7)(c) indent 2 | Article 2(7)(c) indent 3 | Article 2(7)(c) indent 4 | Article 2(7)(c) indent 5 |
Not met | Not met | Not met | Met | Met |
(16) The investigation showed that ZAC was in the process of privatization supervised by the Ukrainian State. In the framework of the privatisation, ZAC's majority shareholder and private investor concluded a contract with a state organisation. Until the end of the IP, ZAC had several obligations imposed by the contract, in particular concerning its workforce and activities. The fulfilment of these obligations was subject to yearly State inspections and failure to fulfil these obligations was subject to sanctions. It was found that the conditions imposed in the contract go beyond what a private investor under normal market economy conditions would accept. Therefore, it is concluded that the company decisions of ZAC regarding labour, output and sales were not made in response to market signals reflecting supply and demand. Rather the decisions were taken with significant State interference in this regard.
(17) Further it was also found that the accounts and the audit of the accounts were not reliable. Indeed, ZAC could modify key data in the accounting program (dates and values for a closed accounting period) and it has not been possible to track certain financial operations in the accounts of ZAC. These serious drawbacks were not reported in the audit report. Therefore it is concluded, that ZAC does not have one clear set of basic accounting records which are independently audited in line with international accounting standards and which are applied for all purposes.
(18) Finally it was found, that by placing defence objects of military nature belonging to the State on the balance sheet and by depreciation of these objects, the patrimonial state, production costs and financial situation of ZAC are subject to significant distortions carried over from the former non-market economy system. Also, the production costs are distorted through the acceptance by ZAC of an interest free loan granted by an investor during the process of privatization.
(19) On this basis it was concluded that not all criteria set out in Article 2(7)(c) of the basic Regulation were met and that market economy conditions do not prevail for the applicant.
(20) The Commission informed the applicant and the Community industry in detail of the abovementioned determinations and granted them the possibility to comment. The Community industry supported the Commission’s determinations. The comments submitted by the applicant were not such as to warrant any change in the MES determination.
3. Individual treatment (IT)
(21) Further to Article 2(7)(a) of the basic Regulation, a country-wide duty is established for countries falling under Article 2(7), except in those cases where companies are able to demonstrate on the basis of properly substantiated claims, that all criteria laid down in Article 9(5) of the basic Regulation are met.
(22) The applicant also claimed IT leading to the establishment of a specific individual anti-dumping duty in the event it was not granted MES. However, the investigation did not point towards the existence of other producers of the product concerned in Ukraine but showed that the applicant is the only known producer of the product concerned in Ukraine. In such a case it is considered that the question of IT does not arise because the specific individual dumping margin would be identical to the country-wide dumping margin.
4. Analogue country
(23) According to Article 2(7) of the basic Regulation, for non-market-economy countries and, to the extent that MES could not be granted, for countries in transition, normal value shall be determined on the basis of the price or constructed value in an analogue country. The applicant claimed that the analogue country used in the original investigation, Brazil, was not appropriate and that in the current interim review, Russia should be chosen as the most, if not the only appropriate analogue country for establishing normal value for Ukraine.
(24) The arguments put forward by the applicant in favour of Russia were the facts that allegedly (i) Russia’s access to raw materials, energy resources and other major inputs, the technology used in production and the scale of production are comparable to Ukraine (ii) Russia's domestic sales are representative, as the total domestic sales volume exceed 5 % of the total export sales volume of Ukraine (iii) the competitive situation in Russia is comparable to Ukraine.
(25) The Commission considered the proposal of the applicant. It was considered first of all that exports of the product concerned originating in Russia were found to be dumped in the original investigation. Such a situation alone implies already an anomaly in the relationship between normal value and export price and puts into question the suitability of Russia as an analogue country. Notwithstanding this observation and following the explicit request of the applicant, the Commission services invited the Russian exporting producer to cooperate in this proceeding. However, the Russian company has not cooperated.
(26) For these reasons it was found that Russia could not be chosen as an appropriate analogue country for establishing normal value for Ukraine. Furthermore, no evidence was found suggesting that circumstances with regard to the analogue country in the original investigation had changed in favour of the applicant.
5. Export price
(27) According to Article 2(8) of the basic Regulation, the export price shall be the price actually paid or payable for the product when sold for export from the exporting country to the Community. In cases where there is no export price, the export price may be constructed according to Article 2(9) of the basic Regulation on the basis of the price at which the imported products are first resold to an independent buyer, or, if the products are not resold to an independent buyer, or are not resold in the condition in which they were imported, on any reasonable basis.
(28) The applicant claimed a change of circumstances with regard to its export prices and argued that in the absence of representative exports to the Community, export prices to a substitute non-EU market comparable to the Community should be used as a reasonable basis to establish a dumping margin. To this end, the applicant proposed the USA or EU10 as a reference country.
(29) The Commission considered the proposals of the applicant, as indeed, in very exceptional circumstances it could be envisaged to use export prices to third countries as a basis for comparison with normal value. However, in this case it was found that the export quantities of the applicant to the USA during the IP were not even representative, so that the question whether it was appropriate to use the export prices to the USA did not arise. Hence the claim to base the dumping calculation on export prices to the USA was rejected. Furthermore, no evidence was found suggesting that the isolated use of export prices to EU10 would be in favour of the applicant. Finally, it is confirmed that no representative sales were found to exist during the IP to the Community.
6. Conclusion
(30) Given the above, MES could not be granted to the applicant. The question of IT does not arise in this case. In addition, all other examined claims regarding the choice of an analogue country and the export prices of the applicant put forward by the applicant were rejected. On this basis, it is considered that the circumstances with regard to dumping have not changed significantly compared to the situation prevailing during the investigation period used in the investigation which led to the imposition of the measures. Therefore, it is concluded that the partial interim review of the anti-dumping measures applicable to imports into the Community of silicon carbide originating in Ukraine should be terminated without amending or repealing the measures in force,
HAS ADOPTED THIS REGULATION:
Article 1
1. The partial interim review of the anti-dumping duty on imports of silicon carbide originating in Ukraine is hereby terminated.
2. The definitive anti-dumping duty imposed by Regulation (EC) No 1100/2000 shall be maintained.
Article 2
This Regulation shall enter into force on the day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 23 May 2005.
For the Council
The President
J. Asselborn
[1] OJ L 56, 6.3.1996, p. 1. Regulation as last amended by Regulation (EC) No 461/2004 (OJ L 77, 13.3.2004, p. 12).
[2] OJ L 94, 13.4.1994, p. 21. Regulation as amended by Regulation (EC) No 1786/97 (OJ L 254, 17.9.1997, p. 6).
[3] OJ L 125, 26.5.2000, p. 3.
[4] OJ L 182, 19.5.2004, p. 18.
[5] OJ L 183, 20.5.2004, p. 88.
[6] OJ L 344, 20.11.2004, p. 37.
[7] OJ C 3, 7.1.2004, p. 4.
--------------------------------------------------
