Amendment by France of the public service obligations imposed in respect of scheduled air services between Rennes and Mulhouse
(2006/C 52/07)
(Text with EEA relevance)
1. France has decided, pursuant to Article 4(1)(a) of Council Regulation (EEC) No 2408/92 of 23 July 1992 on access for Community air carriers to intra-Community air routes, to amend the public service obligations imposed in respect of scheduled air services between the airports of Rennes (Saint-Jacques) and Basel-Mulhouse. These obligations replace those published in Official Journal of the European Communities C 76 of 27 March 2002.
2. The new public service obligations for scheduled air services between the airports of Rennes (Saint-Jacques) and Basel-Mulhouse are as follows
Minimum frequency
The minimum level of service provided must be two round trips per day, morning and evening, excluding public holidays, from Monday to Friday, for 220 days a year.
The services must be operated without a stopover between Rennes (Saint-Jacques) and Basel-Mulhouse.
Type of aircraft used and capacity provided
The service must be operated using a pressurised aircraft with a capacity of at least eighteen seats.
Timetables
Timetables during the working week must be such as to enable business passengers to make the round trip within the day and to spend at least seven hours at their destination, whether Basel-Mulhouse or Rennes (Saint-Jacques).
Flight bookings
Seats on these flights must be marketed using at least one computerised booking system.
Continuity of service
Except in cases of force majeure, the number of flights cancelled for reasons directly attributable to the carrier must not exceed 3 % of the number of flights scheduled in any one year. Moreover, the carrier must give six months' notice before discontinuing the services.
Community carriers are hereby informed that the operation of air services without regard to the abovementioned public service obligations may result in administrative and/or criminal penalties.
--------------------------------------------------
