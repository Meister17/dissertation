New national side of euro circulation coins
(2006/C 20/10)
+++++ TIFF +++++
Euro circulation coins have legal tender status throughout the euro area. The Commission publishes all new euro coin designs [1] with a view to informing anyone required to handle coins in the course of their work and the public at large. In accordance with the Council conclusions of 8 December 2003 [2], Member States and countries that have concluded a Monetary Agreement with the Community providing for the issuance of euro circulation coins are allowed to issue certain quantities of commemorative euro circulation coins on condition that not more than one new coin design is issued per country per year and that only the 2 EUR denomination is used. These coins have the technical features of normal euro circulation coins, but bear a commemorative design on the obverse national side.
Issuing State : Luxembourg
Subject of commemoration : 25th birthday of the heir to the throne, The Grand-Duke Guillaume
Factual description of the design : The coin depicts on the left-hand side of the inner part the effigy of His Royal Highness, The Grand-Duke Henri looking to the right, superimposed on the effigy of the hereditary Grand-Duke Guillaume, on the right-hand side of the inner part. The date 2006 appears below both effigies, flanked by the letter "S" and the logo of the Mint. The word "LËTZEBUERG" appears above the effigies along the upper edge of the inner part of the coin. The 12 stars surround the design on the outer ring of the coin.
Issue volume : 1.1 million coins
Approximate issue period : January 2006
Edge lettering : 2 **, repeated six times, alternately upright and inverted
[1] See OJ C 373, 28.12.2001, p. 1-30 for a reference to all national sides that were issued in 2002.
[2] See conclusions of the General Affairs Council of 8 December 2003 on changes in the design of national sides of euro coins. See also Commission Recommendation of 29 September 2003 on a common practice for changes to the design of national obverse sides of euro circulation coins (OJ L 264, 15.10.2003, p. 38-39).
--------------------------------------------------
