Council Regulation (EC) No 1645/2003
of 18 June 2003
amending Regulation (EC) No 2965/94 setting up a Translation Centre for the bodies of the European Union
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 308 thereof,
Having regard to the proposal from the Commission(1),
Having regard to the opinion of the European Parliament(2),
Having regard to the opinion of the Court of Auditors(3),
Whereas:
(1) Certain provisions of Council Regulation (EC) No 2965/94 of 28 November 1994 setting up a Translation Centre for bodies of the European Union(4) should be brought into line with Council Regulation (EC, Euratom) No 1605/2002 of 25 June 2002 on the Financial Regulation applicable to the general budget of the European Communities(5) (hereinafter referred to as "the general Financial Regulation"), and in particular Article 185 thereof.
(2) Article 10 of Regulation (EC) No 2965/94 should be amended to clarify the arrangements for financing the Centre.
(3) The general principles and limits governing right of access to documents provided for in Article 255 of the Treaty have been laid down by Regulation (EC) No 1049/2001 of the European Parliament and of the Council of 30 May 2001 regarding public access to European Parliament, Council and Commission documents(6).
(4) When Regulation (EC) No 1049/2001 was adopted, the three institutions agreed in a joint declaration that the agencies and similar bodies should implement rules conforming to those of that Regulation.
(5) Appropriate provisions should therefore be included in Regulation (EC) No 2695/94 to make Regulation (EC) No 1049/2001 applicable to the Translation Centre for the bodies of the European Union, as should a provision on a right of appeal against a refusal of access to documents.
(6) Regulation (EC) No 2965/94 should therefore be amended accordingly,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 2965/94 is hereby amended as follows:
1. Article 8(3) shall be replaced by the following:
"3. The Management Board shall adopt the annual report on the Centre's activities and transmit it by 15 June at the latest to the European Parliament, the Council, the Commission, the Court of Auditors and the bodies referred to in Article 2.
4. The Centre shall forward annually to the budgetary authority any information relevant to the outcome of the evaluation procedures.";
2. Article 10(2):
(a) point (b) shall be replaced by the following:
"(b) The Centre's revenue shall comprise payments made by the bodies for which the Centre works and by the institutions and organs with which collaboration has been agreed in return for work performed by it, including interinstitutional activities, and a Community subsidy.";
(b) point (c) shall be deleted;
3. Article 13 shall be replaced by the following:
"Article 13
1. Estimates of all the revenue and expenditure of the Centre shall be prepared for each financial year, corresponding to the calendar year, and shall be shown in the budget of the Centre, which shall include an establishment plan.
2. The revenue and expenditure shown in the budget of the Centre shall be in balance.
3. Each year the Management Board, on the basis of a draft drawn up by the Director, shall produce an estimate of revenue and expenditure for the Centre for the following financial year. This estimate, which shall include a draft establishment plan, shall be forwarded by the Management Board to the Commission by 31 March at the latest.
4. The estimate shall be forwarded by the Commission to the European Parliament and to the Council (hereinafter referred to as the 'budgetary authority') together with the preliminary draft general budget of the European Union.
5. On the basis of the estimate, the Commission shall enter in the preliminary draft general budget of the European Union the estimates it deems necessary for the establishment plan and the amount of the subsidy to be charged to the general budget, which it shall place before the budgetary authority in accordance with Article 272 of the Treaty.
6. The budgetary authority shall authorise the appropriations for the subsidy to the Centre.
The budgetary authority shall adopt the establishment plan for the Centre.
7. The budget of the Centre shall be adopted by the Management Board. It shall become final following final adoption of the general budget of the European Union. Where appropriate, it shall be adjusted accordingly.
8. The Management Board shall, as soon as possible, notify the budgetary authority of its intention to implement any project which may have significant financial implications for the funding of the budget, in particular any projects relating to property such as the rental or purchase of buildings. It shall inform the Commission thereof.
Where a branch of the budgetary authority has notified its intention to deliver an opinion, it shall forward its opinion to the Management Board within a period of six weeks from the date of notification of the project.";
4. Article 14(2)(3) and (4) shall be replaced by the following:
"2. By 1 March at the latest following each financial year, the Centre's accounting officer shall communicate the provisional accounts to the Commission's accounting officer together with a report on the budgetary and financial management for that financial year. The Commission's accounting officer shall consolidate the provisional accounts of the institutions and decentralised bodies in accordance with Article 128 of the general Financial Regulation.
3. By 31 March at the latest following each financial year, the Commission's accounting officer shall forward the Centre's provisional accounts to the Court of Auditors, together with a report on the budgetary and financial management for that financial year. The report on the budgetary and financial management for the financial year shall also be forwarded to the European Parliament and to the Council.
4. On receipt of the Court of Auditors' observations on the Centre's provisional accounts, pursuant to Article 129 of the general Financial Regulation, the Director shall draw up the Centre's final accounts under his own responsibility and submit them to the Management Board for an opinion.
5. The Management Board shall deliver an opinion on the Centre's final accounts.
6. The Director shall, by 1 July at the latest following each financial year, forward the final accounts to the European Parliament, the Council and the Court of Auditors, together with the Management Board's opinion.
7. The final accounts shall be published.
8. The Director shall send the Court of Auditors a reply to its observations by 30 September at the latest. He shall also send this reply to the Management Board.
9. The Director shall submit to the European Parliament, at the latter's request, any information required for the smooth application of the discharge procedure for the financial year in question, as laid down in Article 146(3) of the general Financial Regulation.
10. The European Parliament, on a recommendation from the Council acting by a qualified majority, shall, before 30 April of year N + 2, give a discharge to the Director in respect of the implementation of the budget for year N.";
5. Article 15 shall be replaced by the following:
"Article 15
The financial rules applicable to the Centre shall be adopted by the Management Board after the Commission has been consulted. They may not depart from Commission Regulation (EC, Euratom) No 2343/2002 of 19 November 2002 on the framework Financial Regulation for the bodies referred to in Article 185 of Council Regulation (EC, Euratom) No 1605/2002 on the Financial Regulation applicable to the general budget of the European Communities(7) unless specifically required for the Centre's operation and with the Commission's prior consent."
6. an Article 18a shall be inserted:
"Article 18a
1. Regulation (EC) No 1049/2001 of the European Parliament and of the Council of 30 May 2001 regarding public access to European Parliament, Council and Commission documents(8) shall apply to documents held by the Centre.
2. The Management Board shall adopt the practical arrangements for implementing Regulation (EC) No 1049/2001 within six months of entry into force of Council Regulation (EC) No 1645/2003 of 18 June 2003 amending Regulation (EC) No 2965/94 setting up a Translation Centre for bodies of the European Union(9).
3. Decisions taken by the Centre pursuant to Article 8 of Regulation (EC) No 1049/2001 may give rise to the lodging of a complaint to the Ombudsman or form the subject of an action before the Court of Justice, under the conditions laid down in Articles 195 and 230 of the Treaty respectively."
Article 2
This Regulation shall enter into force on the first day of the month following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Luxembourg, 18 June 2003.
For the Council
The President
G. Drys
(1) OJ C 331 E, 31.12.2002, p. 50.
(2) Opinion delivered on 27.3.2003 (not yet published in the Official Journal).
(3) OJ C 285, 21.11.2002, p. 4.
(4) OJ L 314, 7.12.1994, p. 1. Regulation as amended by Regulation (EC) No 2610/95 (OJ L 268, 10.10.1995, p. 1).
(5) OJ L 248, 16.9.2002, p. 1; corrigendum in OJ L 25, 30.1.2003, p. 43.
(6) OJ L 145, 31.5.2001, p. 43.
(7) OJ L 357, 31.12.2002, p. 72, with Corrigendum in OJ L 2, 7.1.2003, p. 39.
(8) OJ L 145, 31.5.2001, p. 43.
(9) OJ L 245, 29.9.2003, p. 13.
