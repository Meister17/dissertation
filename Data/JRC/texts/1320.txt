Council Decision
of 22 December 2000
on the signing and the provisional application of the Agreement on trade in textile products between the European Community and the Republic of Croatia initialled in Brussels on 8 November 2000
(2001/55/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community and in particular Article 133 thereof, in conjunction with the first sentence of the first subparagraph of Article 300 (2) thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) The Commission has negotiated on behalf of the Community an Agreement on trade in textile products with the Republic of Croatia.
(2) The Agreement was initialled on 8 November 2000.
(3) Subject to its possible conclusion at a later date, the Agreement should be signed on behalf of the Community.
(4) It is appropriate to apply this Agreement on a provisional basis as from 1 January 2001 pending the completion of the relevant procedures for its formal conclusion, subject to reciprocity.
HAS DECIDED AS FOLLOWS:
Article 1
Subject to a possible conclusion at a later date, the President of the Council is hereby authorised to designate the persons empowered to sign on behalf of the Community the Agreement on trade in textile products between the European Community and the Republic of Croatia.
Article 2
Subject to reciprocity, the Agreement referred to in Article 1 shall be applied on a provisional basis as from 1 January 2001 pending the completion of the procedures for its conclusion.
The text of the Agreement is attached to this Decision.
Done at Brussels, 22 December 2000.
For the Council
C. Pierret
The President
