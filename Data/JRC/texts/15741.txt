Extract of a reorganisation measure decided under Article 3, paragraphs 1 and 2 of Directive 2001/24/EC of the European Parliament and of the Council of 4 April 2001 on the reorganisation and winding-up of credit institutions
(2006/C 207/04)
Urgency measure under article 71, paragraph 2, of the Law of 1992, concerning the supervision of the credit sector
(Wet toezicht kredietwezen 1992)
By decree of 5 July 2006 of the Court in Almelo, at the request of the Netherlands Central Bank on 1 May 2006, the urgency measures that had been decreed on 18 November 2005 for the duration of 18 months by the Court in Almelo under article 71, paragraph 2, of the Law of 1992 concerning the supervision of the credit sector, are extended to 10 October 2006. The extension of the urgency measures concerns:
A/b Financiën B.V.
Iepenweg Zuid 4
7611 KZ Aadorp
The Netherlands
Registered at the Chamber of Commerce under No 06043696
Registered in Almelo
By decree of 5 July 2006 of the Court in Zwolle-Lelystad, at the request of the Netherlands Central Bank on 1 May 2006, the urgency measures that had been decreed on 18 November 2005 for the duration of 18 months by the Court in Zwolle-Lelystad under article 71, paragraph 2, of the Law of 1992 concerning the supervision of the credit sector, are extended to 10 October 2006. The extension of the urgency measures concerns:
Belba B.V.
Iepenweg Zuid 4
7611 KZ Aadorp
The Netherlands
Registered at the Chamber of Commerce under No 06083372
Registered in Schuinesloot
Administrator: Daniëls Huisman Advocaten
Mr J.A.D.M. Daniëls
Postbus 31
7601 JR Almelo
The Netherlands
--------------------------------------------------
