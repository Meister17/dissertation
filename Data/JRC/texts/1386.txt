Agreement
between the European Community and the Government of Canada renewing a cooperation programme in higher education and training
THE EUROPEAN COMMUNITY,
of the one part, and
THE GOVERNMENT OF CANADA,
of the other part,
hereinafter collectively referred to as "the Parties",
NOTING that the Transatlantic Declaration adopted by the European Community and its Member States and the Government of Canada on 22 November 1990 makes specific reference to strengthening mutual cooperation in various fields which directly affect the present and future well-being of their citizens, such as exchanges and joint projects in education and culture, including academic and youth exchanges;
NOTING that the Joint Declaration on EU-Canada relations adopted on 17 December 1996 remarks that in order to renew their ties based on shared cultures and values, the Parties will encourage contacts between their citizens at every level, especially among their youth; and that the Joint Action Plan attached to the Declaration encourages the Parties to further strengthen their cooperation through the Agreement on higher education and training;
CONSIDERING that the adoption and the implementation of the 1995 Agreement on higher education and training materialises the commitment of the Transatlantic Declaration and that the experience of its implementation has been highly positive to both Parties;
ACKNOWLEDGING the crucial contribution of higher education and training to the development of human resources capable of participating in the global knowledge-based economy;
RECOGNISING that cooperation in higher education and training should complement other relevant cooperation initiatives between the European Community and Canada;
ACKNOWLEDGING the importance of taking into account the work done in the field of higher education and training by international organisations active in these fields such as the OECD, Unesco and the Council of Europe;
RECOGNISING that the Parties have a common interest in cooperation in higher education and training, as part of the wider cooperation that exists between the European Community and Canada;
EXPECTING to obtain mutual benefit from cooperative activities in higher education and training;
RECOGNISING the need to widen access to the activities supported under this Agreement, in particular those activities in the training sector;
DESIRING to renew the basis for the continuing conduct of cooperative activities in higher education and training,
HAVE AGREED AS FOLLOWS:
Article 1
Purpose
This Agreement renews the cooperation programme in higher education and training between the European Community and Canada, established in 1995.
Article 2
Definitions
For the purpose of this Agreement:
1. "higher education institution" means any establishment according to the applicable laws or practices which offers qualifications or diplomas at higher education level, whatever such establishment may be called;
2. "training institution" means any type of public, semi-public or private body, which, irrespective of the designation given to it, in accordance with the applicable laws and practices, designs or undertakes vocational education or training, further vocational training, refresher vocational training or retraining contributing to qualifications recognised by the competent authorities;
3. "students" means all those persons following learning or training courses or programmes which are run by higher education or training institutions as defined in this Article, and which are recognised or financially supported by the competent authorities.
Article 3
Objectives
The objectives of the Cooperation Programme shall be to:
1. promote closer understanding between the peoples of the European Community and Canada, including broader knowledge of their languages, cultures and institutions;
2. improve the quality of human resource development in both the European Community and Canada, including the acquisition of skills required to meet the challenges of the global knowledge-based economy;
3. encourage an innovative and sustainable range of student-centred higher education and training cooperative activities between the different regions in the European Community and in Canada that have a durable impact;
4. improve the quality of transatlantic student mobility by promoting transparency, mutual recognition of qualifications and periods of study and training, and where appropriate, portability of credits;
5. encourage the exchange of expertise in e-learning and open and distance education and their effective use by project consortia to broaden Programme impact;
6. form or enhance partnerships among higher education and training institutions, professional associations, public authorities, private sector and other associations as appropriate in both the European Community and Canada;
7. reinforce a European Community and a Canadian value-added dimension to Transatlantic cooperation in higher education and training;
8. complement bilateral programmes between the Member States of the European Community and Canada as well as other European Community and Canadian programmes and initiatives.
Article 4
Principles
Cooperation under this Agreement shall be conducted on the basis of the following principles:
1. full respect for the responsibilities of the Member States of the European Community and the Provinces and Territories of Canada and the autonomy of the higher education and training institutions;
2. overall balance of benefits from activities undertaken through this Agreement;
3. effective provision of seed-funding for a diverse range of innovative projects, that build new structures and links, that have a multiplying effect through consistent and effective dissemination of results, that are sustainable over the longer term without on-going Cooperation Programme support, and where student mobility is involved, provide mutual recognition of periods of study and training and, where appropriate, portability of credits;
4. broad participation across the different Member States of the European Community and the Provinces and Territories of Canada;
5. recognition of the full cultural, social and economic diversity of the European Community and Canada; and
6. selection of projects on a competitive and transparent basis, taking account of the foregoing principles.
Article 5
Programme actions
The Cooperation Programme shall be pursued by means of the actions described in the Annex, which forms an integral part of this Agreement.
Article 6
Joint Committee
1. A Joint Committee is hereby established. It shall comprise representatives of each Party.
2. The functions of the Joint Committee shall be to:
(a) review the cooperative activities envisaged under this Agreement;
(b) provide a report at least biennially to the Parties on the level, status and effectiveness of cooperative activities undertaken under this Agreement.
3. The Joint Committee shall meet at least every second year, with such meetings being held alternately in the European Community and Canada. Other meetings may be held as mutually determined.
4. Minutes shall be agreed by those persons selected from each side to jointly chair the meeting, and shall, together with the biennial report, be made available to the joint Cooperation Committee established under the 1976 Framework Agreement for commercial and economic cooperation between the European Community and Canada and appropriate Ministers of each Party.
Article 7
Monitoring and evaluation
The Cooperation Programme shall be monitored and evaluated as appropriate on a cooperative basis. This shall permit, as necessary, the reorientation of the Cooperation Programme in the light of any needs or opportunities becoming apparent in the course of its operation.
Article 8
Funding
1. Cooperative activities shall be subject to the availability of funds and to the applicable laws and regulations, policies and programmes of the European Community and Canada. Financing will be on the basis of an overall matching of funds between the Parties.
2. Each Party shall provide funds for the direct benefit of: for the European Community, citizens of one of the European Community Member States or persons recognised by a Member State as having official status as permanent residents; for Canada, its own citizens and permanent residents as defined in the Immigration Act.
3. Costs incurred by or on behalf of the Joint Committee shall be met by the Party to whom the members are responsible. Costs, other than those of travel and subsistence, which are directly associated with meetings of the Joint Committee, shall be met by the host Party.
Article 9
Entry of personnel
Each Party shall take all reasonable steps and use its best efforts to facilitate entry to and exit from its territory of personnel, students, material and equipment of the other Party engaged in or used in cooperative activities under this Agreement in accordance with laws and regulations of each Party.
Article 10
Other agreements
1. This Agreement is without prejudice to cooperation which may be taken pursuant to other agreements between the Parties.
2. This Agreement is without prejudice to existing or future bilateral agreements between individual Member States of the European Community and Canada in the fields covered herein.
Article 11
Territorial application of this Agreement
This Agreement shall apply, on the one hand, to the territories in which the Treaty establishing the European Community is applied and under the conditions laid down in that Treaty and, on the other hand, to the territory of Canada.
Article 12
Final clauses
1. This Agreement shall enter into force on the first day of the month following the date on which the Parties shall have notified each other in writing that their legal requirements for the entry into force of this Agreement have been fulfilled. The Agreement shall enter into force on the first day of the month following the later notification.
2. This Agreement shall be in force for a period of five years, following which it may be renewed by agreement of the Parties.
3. This Agreement may be amended or extended by agreement of the Parties. Amendments or extensions shall be in writing and shall enter into force on the first day of the month following the date on which the Parties shall have notified each other in writing that their legal requirements for the entry into force of the Agreement providing for the amendment or extension in question have been fulfilled.
4. This Agreement may be terminated at any time by either Party upon twelve months written notice. The expiration or termination of this Agreement shall not affect the validity or duration of any arrangements made under it or the obligations established pursuant to the Annex to this Agreement.
Article 13
Authentic texts
This Agreement is drawn up in duplicate in the Danish, Dutch, English, Finnish, French, German, Greek, Italian, Portuguese, Spanish and Swedish languages, each of these texts being equally authentic.
EN FE DE LO CUAL, los abajo firmantes suscriben el presente Acuerdo./TIL BEKRÆFTELSE HERAF har undertegnede befuldmægtigede underskrevet denne aftale./ZU URKUND DESSEN haben die Unterzeichneten dieses Abkommen unterschrieben./ΕΙΣ ΠΙΣΤΩΣΗ ΤΩΝ ΑΝΩΤΕΡΩ, οι υπογράφοντες πληρεξούσιοι έθεσαν την υπογραφή τους κάτω από την παρούσα συμφωνία./IN WITNESS WHEREOF the undersigned, have signed this Agreement./EN FOI DE QUOI, les soussignés ont apposé leur signature au bas du présent accord./IN FEDE DI CHE i sottoscritti hanno firmato il presente accordo./TEN BLIJKE WAARVAN de ondergetekenden hun handtekening onder deze overeenkomst hebben geplaatst./EM FÉ DO QUE os abaixo assinados apuseram as suas assinaturas no presente Acordo./TÄMÄN VAKUUDEKSI jäljempänä mainitut ovat allekirjottaneet tämän sopimuksen./TILL BEVIS HÄRPÅ har undertecknade befullmäktigade undertecknat detta avtal.
Hecho en Ottawa, el /diecinueve de diciembre del año dos mil./Udfærdiget i Ottawa den /nittende december to tusind./Geschehen zu Ottawa am /neunzehnten Dezember zweitausend./Έγινε στην Οτάβα, στις /δέκα εννέα Δεκεμβρίου δύο χιλιάδες./Done at Ottawa on the /nineteenth day of December in the year two thousand./Fait à Ottawa, le /dix-neuf décembre deux mille./Fatto a Ottawa addì /diciannove dicembre duemila./
Gedaan te Ottawa, de /negentiende december tweeduizend./Feito em Otava, em /dezanove de Dezembro de dois mil./Tehty Ottawassa /yhdeksäntenätoista päivänä joulukuuta vuonna kaksituhatta./Som skedde i Ottawa den /nittonde december tjugohundra.
Por la Comunidad Europea/For Det Europæiske Fællesskab/Für die Europäische Gemeinschaft/Για την Ευρωπαϊκή Κοινότητα/For the European Community/Pour la Communauté européenne/Per la Comunità europea/Voor de Europese Gemeenschap/Pela Comunidade Europeia/Euroopan yhteisön puolesta/På Europeiska gemenskapens vägnar
>PIC FILE= "L_2001071EN.002001.EPS">
Por el Gobierno de Canadá/For Canadas regering/Für die Regierung Kanadas/Για την Κυβέρνηση του Καναδά/For the Government of Canada/Pour le gouvernement du Canada/Per il governo del Canada/Voor de regering van Canada/Pelo Governo do Canadá/Kanadan hallituksen puolesta/På Kanadas regerings vägnar
>PIC FILE= "L_2001071EN.002002.EPS">
ANNEX
ACTIONS
ACTION 1
Joint EC/Canada consortia projects
1. The Parties will provide support to higher education institutions and training institutions which form joint EC/Canada consortia for the purpose of undertaking joint projects in the area of higher education and training. The European Community will provide support for the use of the European Community consortia partners, Canada will provide support for Canadian consortia partners.
2. Each joint consortium must involve at least three active partners on each side from at least three different Member States of the European Community and from at least two different Provinces or Territories of Canada.
3. Each joint consortium should as a rule involve transatlantic mobility of students, with a goal of parity in the flows in each direction, and foresee adequate language and cultural preparation.
4. Financial support may be awarded to joint consortia projects for innovative activities with objectives which can be accomplished within a time-scale of up to a maximum of three years. Preparatory or project development activities may be supported for a period of up to one year.
5. The eligible subject areas for joint EC/Canada consortia cooperation shall be agreed by the Joint Committee as established by Article 6.
6. Activities eligible for support may include:
- preparatory or project development activities,
- development of organisational frameworks for student mobility, including work placements, which provide adequate language preparation and full recognition by the partner institutions,
- structured exchanges of students, teachers, trainers, administrators, human resource managers, vocational training programme planners and managers, trainers and occupational guidance specialists in either higher education institutions or vocational training organisations,
- joint development of innovative curricula including the development of teaching materials, methods and modules,
- joint development of new methodologies in higher education and training including the use of information and communication technologies, e-learning, open and distance learning,
- short intensive programmes of a minimum of three weeks,
- teaching assignments forming an integral part of the curriculum in a partner institution,
- other innovative projects, which aim to improve the quality of transatlantic cooperation in higher education and training and meet one or more of the objectives specified in Article 3 of this Agreement.
ACTION 2
Complementary activities
The Parties may support a limited number of complementary activities in accordance with the objectives of the Agreement, including exchanges of experience or other forms of joint action in the fields of education and training.
PROGRAMME ADMINISTRATION
1. Each Party may provide financial support for the activities provided for under this Programme.
2. Administration of the Actions shall be implemented by the competent officials of each Party. These tasks will comprise:
- deciding the rules and procedures for the presentation of proposals including the preparation of a common set of guidelines for applicants,
- establishing the timetable for publication of calls for proposals, submission and selection of proposals,
- providing information on the programme and its implementation,
- appointing academic advisors and experts, including for independent appraisal of proposals,
- recommending to the appropriate authorities of each Party which projects to finance,
- financial management,
- a cooperative approach to programme monitoring and evaluation.
TECHNICAL SUPPORT MEASURES
Under the Cooperation Programme, funds will be made available for purchasing of services to ensure optimal programme implementation; in particular the Parties may organise seminars, colloquia or other meetings of experts, conduct evaluations, produce publications or disseminate programme-related information.
