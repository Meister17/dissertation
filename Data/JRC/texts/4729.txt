Decision of the EEA Joint Committee No 70/2005
of 29 April 2005
amending Annex XXII (Company law) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex XXII to the Agreement was amended by Decision of the EEA Joint Committee No 176/2004 of 3 December 2004 [1].
(2) Directive 2004/25/EC of the European Parliament and of the Council of 21 April 2004 on takeover bids [2] is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
The following point shall be inserted after point 10c (Council Regulation (EC) No 1435/2003) of Annex XXII to the Agreement:
"10d. 32004 L 0025: Directive 2004/25/EC of the European Parliament and of the Council of 21 April 2004 on takeover bids (OJ L 142, 30.4.2004, p. 12)."
Article 2
The text of Directive 2004/25/EC in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 30 April 2005, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [3].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 29 April 2005.
For the EEA Joint Committee
The President
Richard Wright
[1] OJ L 133, 26.5.2005, p. 31.
[2] OJ L 142, 30.4.2004, p. 12.
[3] Constitutional requirements indicated.
--------------------------------------------------
