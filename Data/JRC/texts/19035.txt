Commission Decision
of 23 June 2003
establishing the official tuberculosis, brucellosis, and enzootic-bovine-leukosis-free status of certain Member States and regions of Member States as regards bovine herds
(notified under document number C(2003) 1925)
(Text with EEA relevance)
(2003/467/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 64/432/EEC of 26 June 1964 on health problems affecting intra-Community trade in bovine animals and swine(1), as last amended by Commission Regulation (EC) No 1226/2002(2), and in particular Annex A(I)(4), Annex A(II)(7) and Annex D(I)(E) thereto,
Whereas:
(1) Directive 64/432/EEC provides that Member States or parts or regions thereof may be declared officially free of tuberculosis, brucellosis and enzootic bovine leukosis as regards bovine herds subject to compliance with certain conditions set out in that Directive.
(2) Commission Decision 1999/467/EC(3), as last amended by Decision 2001/26/EC(4), established the official tuberculosis-free status of certain Member States and regions of Member States as regards bovine herds.
(3) Commission Decision 1999/466/EC(5), as last amended by Decision 2003/164/EEC(6), established the official brucellosis-free status of certain Member States and regions of Member States as regards bovine herds.
(4) Commission Decision 1999/465/EC(7), as last amended by Decision 2003/177/EEC(8), established the official enzootic-bovine-leukosis-free status of certain Member States and regions of Member States.
(5) Belgium as regards the territory of that Member State, and Italy as regards the provinces of Ascoli Piceno, Bergamo, Lecco and Sondrio submitted to the Commission documentation demonstrating compliance with all the conditions provided for in Directive 64/432/EEC, in order that the territory of Belgium and those regions of Italy may be declared officially free of tuberculosis as regards bovine herds.
(6) Belgium as regards the territory of that Member State, and Italy as regards the region of Sardinia and the provinces of Ascoli Piceno, Bergamo, Como, Lecco, Mantova, Sondrio, Trento and Varese submitted to the Commission documentation demonstrating compliance with all the conditions provided for in Directive 64/432/EEC, in order that the territory of Belgium and those regions of Italy may be declared officially free of brucellosis as regards bovine herds.
(7) Italy submitted to the Commission documentation demonstrating compliance with all the conditions provided for in Directive 64/432/EEC, as regards the provinces of Ascoli Piceno, Bergamo, Brescia, Como, Lecco, Mantova, Sondrio and Varese in order that those regions may be declared officially free of enzootic bovine leukosis.
(8) In the interests of clarity, the lists of Member States and regions of Member States declared officially free of tuberculosis, brucellosis and enzootic bovine leukosis, as regards bovine herds, should be set out in the same act. Accordingly, Decisions 1999/467/EEC, 1999/466/EEC and 1999/465/EEC should be repealed.
(9) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
Officially tuberculosis-free Member States and Regions of Member States
1. The Member States listed in Chapter 1 of Annex I are declared officially free of tuberculosis as regards bovine herds.
2. The regions of the Member States listed in Chapter 2 of Annex 1 are declared officially free of tuberculosis as regards bovine herds.
Article 2
Officially brucellosis-free Member States and Regions of Member States
1. The Member States listed in Chapter 1 of Annex II are declared officially free of brucellosis as regards bovine herds.
2. The regions of the Member States listed in Chapter 2 of Annex II are declared officially free of brucellosis as regards bovine herds.
Article 3
Officially enzootic-bovine-leukosis-free Member States and Regions of Member States
1. The Member States listed in Chapter 1 of Annex III are declared officially free of enzootic bovine leukosis.
2. The regions of the Member States listed in Chapter 2 of Annex III are declared officially free of enzootic bovine leukosis.
Article 4
Repeals
Decisions 1999/465/EC, 1999/466/EC and 1999/467/EC are repealed.
Article 5
Addressees
This Decision is addressed to the Member States.
Done at Brussels, 23 June 2003.
For the Commission
David Byrne
Member of the Commission
(1) OJ 121, 29.7.1964, p.1977/64.
(2) OJ L 179, 9.7.2002, p. 13.
(3) OJ L 181, 16.7.1999, p. 36.
(4) OJ L 6, 11.1.2001, p. 18.
(5) OJ L 181, 16.7.1999, p. 34.
(6) OJ L 66, 11.3.2003, p. 49.
(7) OJ L 181, 16.7.1999, p. 32.
(8) OJ L 70, 14.3.2003, p. 50.
ANNEX I
CHAPTER 1 Officially tuberculosis-free Member States
Belgium
Denmark
Germany
France
Luxembourg
Netherlands
Austria
Finland
Sweden
CHAPTER 2 Officially tuberculosis-free regions of Member States
In Italy:
- Lombardia Region: Provinces of Bergamo, Lecco, Sondrio
- Marche Region: Province of Ascoli Piceno
- Trentino-Alto Aldige Region: Provinces of Bolzano, Trento
ANNEX II
CHAPTER 1 Officially brucellosis-free Member States
Belgium
Denmark
Germany
Luxembourg
Netherlands
Austria
Finland
Sweden
CHAPTER 2 Officially brucellosis-free regions of Member States
In Italy:
- Lombardia Region: Provinces of Bergamo, Como, Lecco, Mantova, Sondrio, Varese
- Marche Region: Province of Ascoli Piceno
- Trentino-Alto Aldige Region: Provinces of Bolzano, Trento
- Emilia-Romagna Region: Provinces of Bologna, Ferrara, Forli-Cesena, Modena, Parma, Piacenza, Ravenna, Reggio Emilia, Rimini
- Sardinia Region: Provinces of Cagliari, Nuoro, Oristana, Sassari
In Portugal:
- Autonomous Region of the Azores: Islands of Pico, Graciosa, Flores, Corvo
In the United Kingdom:
- Great Britain: England, Scotland, Wales
ANNEX III
CHAPTER 1 Officially enzootic-bovine-leukosis-free Member States
Belgium
Denmark
Germany
Spain
France
Ireland
Luxembourg
Netherlands
Austria
Finland
Sweden
United Kingdom
CHAPTER 2 Officially enzootic-bovine-leukosis-free regions of Member States
In Italy:
- Lombardia Region: Provinces of Bergamo, Brescia, Como, Lecco, Mantova, Sondrio, Varese
- Marche Region: Province of Ascoli Piceno
- Trentino-Alto Aldige Region: Provinces of Bolzano, Trento
- Emilia-Romagna Region: Provinces of Bologna, Ferrara, Forli-Cesena, Modena, Parma, Piacenza, Ravenna, Reggio Emilia, Rimini
- Val d'Aosta Region: Province of Aosta
