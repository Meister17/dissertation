Judgment of the Court
(Third Chamber)
of 20 October 2005
in Case C-264/03: Commission of the European Communities
v French Republic [1]
In Case C-264/03: ACTION under Article 226 EC for failure to fulfil obligations, brought on 17 June 2003, Commission of the European Communities (Agents: B. Stromsky, K. Wiedner and F. Simonetti) v French Republic (Agents: G. de Bergues and D. Petrausch — the Court (Third Chamber), composed of A. Rosas (Rapporteur), President of the Chamber, J. Malenovský, J.-P. Puissochet, A. Borg Barthet and U. Lõhmus, Judges; Advocate General: M. Poiares Maduro, Registrar: K. Sztranc, Administrator, gave a judgment on 20 October 2005, in which it:
[1] OJ C 200 of 23.08.2003.
--------------------------------------------------
