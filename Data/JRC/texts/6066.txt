Decision no 200
of 15 December 2004
concerning the methods of operation and the composition of the Technical Commission on Data Processing of the Administrative Commission on Social Security for Migrant Workers
(Text with EEA relevance)
(2005/324/EC)
THE ADMINISTRATIVE COMMISSION ON SOCIAL SECURITY FOR MIGRANT WORKERS,
Having regard to Article 81(d) of Council Regulation (EEC) No 1408/71 of 14 June 1971 on the application of social security schemes to employed persons and their families moving within the Community [1], under which the Administrative Commission shall foster and develop cooperation between Member States by modernising procedures for the exchange of information, in particular by adapting the information flow between institutions for the purpose of telematic exchange, taking account of the development of data processing in each Member State, with a view to expediting the award of benefits;
Having regard to Article 117c(1) of Council Regulation (EEC) No 574/72 fixing the procedure for implementing Regulation (EEC) No 1408/71 of 14 June 1971 on the application of social security schemes to employed persons and their families moving within the Community [2], under which the Administrative Commission shall set up and determine the methods of operation and composition of a Technical Commission, which shall, pursuant to Articles 117, 117a and 117b, deliver reports and a reasoned opinion before decisions are taken;
HAS DECIDED AS FOLLOWS:
Article 1
1. The Administrative Commission on Social Security for Migrant Workers sets up the Technical Commission on Data Processing provided for in Article 117c(1) of Regulation (EEC) No 574/72. It shall be called "the Technical Commission".
2. The Technical Commission shall have the functions laid down in Article 117c(2) of Regulation (EEC) No 574/72.
3. The mandate with regard to the specific tasks of the Technical Commission shall be set by the Administrative Commission on Social Security for Migrant Workers, who may modify these tasks as required.
Article 2
The Technical Commission shall adopt its reports and reasoned opinions where necessary on the basis of technical documents and studies. It can request from national administrations any information it deems necessary for appropriate accomplishment of its tasks.
Article 3
1. The Technical Commission shall be composed of two members from each Member State, one of whom shall be nominated as the standing member, with the other designated as his deputy. The nominations from each Member State shall be forwarded to the Secretary-General of the Administrative Commission by the government representative for the Member State on the Administrative Commission.
2. Reports and reasoned opinions shall be adopted by simple majority of all members of the Technical Commission, each Member State having a single vote that shall be cast by the standing member or in his absence by his deputy. The reports or reasoned opinions of the Technical Commission must indicate whether they were reached unanimously or by simple majority. They must, should there be a minority, set out conclusions or reservations of the minority.
3. A representative of the Commission of the European Communities or a person designated by him shall act in a consultative capacity within the Technical Commission.
Article 4
The office of Chairman of the Technical Commission shall be held each half-year by either the standing member or another designated official belonging to the State whose representative on the Administrative Commission holds the office of Chairman of that Commission for the same period. The Chairman of the Technical Commission shall report on the activities of the Technical Commission as required by the Chairman of the Administrative Commission.
Article 5
The Technical Commission may set up ad hoc working groups to consider specific issues. The Technical Commission shall describe the tasks to be taken forward by such working groups, the timetable for completion of those tasks and the financial implications of its action in the work programme mentioned in Article 7.
Article 6
The Secretariat of the Administrative Commission shall prepare and organise the meetings of the Technical Commission and draw up the minutes thereof.
Article 7
The Technical Commission shall submit a detailed work programme to the Administrative Commission for its approval. The Technical Commission shall also report each year, to the Administrative Commission, on its activities and achievements in relation to the work programme and with any proposals for amending it.
Article 8
Any proposed action of the Technical Commission involving expenses to be borne by the Commission of the European Communities is subject to the approval of the representative of that institution.
Article 9
The languages of the Technical Commission shall be the same as those recognised as official languages of the Community institutions in accordance with the Article 290 of the Treaty establishing the European Community.
Article 10
The supplementary rules laid down in the attached Annex shall also apply to the Technical Commission.
Article 11
This Decision shall replace the Decision No 169 of the Administrative Commission [3].
Article 12
This decision shall be published in the Official Journal of the European Union.
It shall apply from 1 March 2005.
The Chairman of the Administrative Commission
C.-J. van den Berg
[1] OJ L 149, 5.7.1971, p. 2. Regulation as last amended by Regulation (EC) No 631/2004 of the European Parliament and of the Council (OJ L 100, 6.4.2004, p. 1).
[2] OJ L 74, 27.3.1972, p. 1. Regulation as last amended by Regulation (EC) No 631/2004.
[3] OJ L 195, 11.7.1998, p. 46.
--------------------------------------------------
ANNEX
SUPPLEMENTARY RULES OF THE TECHNICAL COMMISSION
1. Attendance at meetings
(a) If the Chairman in office is prevented from attending a meeting of the Technical Commission, his deputy shall act as chairman.
(b) Members may be accompanied at the meetings of the Technical Commission by one or more additional experts where this is necessary because of the nature of the subjects to be dealt with. Each delegation may, as a rule, consist of not more than four persons.
(c) The representative of the Commission of the European Communities or a member of the Secretariat or any other person designated by the Secretary-General of the Administrative Commission shall attend all meetings of the Technical Commission or its adhoc working groups. Those meetings may furthermore be attended, where this is relevant to the question to be dealt with, by a representative of other departments of the Commission of the European Communities.
2. Voting
(a) When a standing member of the Technical Commission holds the office of Chairman, his deputy shall vote in his place.
(b) Any member present when a vote is taken who abstains from voting shall be invited by the chairman to state his reasons for abstaining.
(c) When the majority of members present abstain, the proposal put to the vote shall be considered as not having been taken into consideration.
3. Agenda
(a) The provisional agenda of each meeting of the Technical Commission shall be drawn up by the Secretariat in consultation with the chairman of the Technical Commission. Before proposing to include an item in the agenda, the Secretariat may, where this appears necessary, ask the delegations concerned to make their views on this question known in writing.
(b) The provisional agenda shall, in principle, comprise items for which a request submitted by a member or by the representative of the Commission of the European Communities and, where appropriate, notes relating to it have been received by the Secretariat at least 20 working days before the beginning of the meeting.
(c) The provisional agenda shall be sent at least 10 working days before the beginning of each meeting to the members of the Technical Commission, the representative of the Commission of the European Communities, and any other person expected to be attending the meeting. The documents relating to the items on the agenda shall be sent to them as soon as they are available.
(d) At the beginning of each meeting the Technical Commission shall approve the agenda of the meeting. A unanimous vote of the Technical Commission is required for the inclusion in the agenda of any items other than those appearing on the provisional agenda. Except in cases of urgency, members of the Technical Commission may reserve their final position until the following meeting with regard to items appearing on the provisional agenda for which they have not received the relevant documents in their own language five working days before the beginning of the meeting.
4. Ad hoc working groups
(a) Ad hoc working groups shall be presided over by an expert designated by the Chairman of the Technical Commission in consultation with the representative of the Commission of the European Communities or, failing this, by an expert representing the State whose representative on the Administrative Commission holds the office of Chairman of that Commission.
(b) The chairman of the ad hoc working group shall be summoned to the meeting of the Technical Commission in the course of which the report of that ad hoc working group is discussed.
5. Administrative matters
(a) The Chairman of the Technical Commission may give the Secretariat any instructions for meetings to be held and for the performance of activities that are within the scope of the functions of the Technical Commission.
(b) The Technical Commission shall be convened by a letter of convocation sent to the members and the representative of the Commission of the European Communities 10 working days before the meeting by the Secretariat in consultation with the Chairman of the Technical Commission.
(c) Minutes are to be recorded for each meeting and are, in principle, to be approved at the following meeting.
--------------------------------------------------
