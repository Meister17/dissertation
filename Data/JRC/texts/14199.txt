Commission Regulation (EC) No 1817/2006
of 11 December 2006
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 12 December 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 11 December 2006.
For the Commission
Jean-Luc Demarty
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 386/2005 (OJ L 62, 9.3.2005, p. 3).
--------------------------------------------------
ANNEX
to Commission Regulation of 11 December 2006 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code [1] | Standard import value |
07020000 | 052 | 96,0 |
204 | 48,6 |
999 | 72,3 |
07070005 | 052 | 147,2 |
204 | 67,3 |
628 | 167,7 |
999 | 127,4 |
07099070 | 052 | 153,2 |
204 | 58,4 |
999 | 105,8 |
08051020 | 052 | 58,8 |
388 | 46,7 |
508 | 15,3 |
528 | 26,3 |
999 | 36,8 |
08052010 | 052 | 63,5 |
204 | 58,9 |
999 | 61,2 |
08052030, 08052050, 08052070, 08052090 | 052 | 66,6 |
999 | 66,6 |
08055010 | 052 | 51,6 |
528 | 35,6 |
999 | 43,6 |
08081080 | 400 | 88,1 |
720 | 80,3 |
999 | 84,2 |
08082050 | 052 | 134,0 |
400 | 113,0 |
528 | 106,5 |
720 | 78,4 |
999 | 108,0 |
[1] Country nomenclature as fixed by Commission Regulation (EC) No 750/2005 (OJ L 126, 19.5.2005, p. 12). Code "999" stands for "of other origin".
--------------------------------------------------
