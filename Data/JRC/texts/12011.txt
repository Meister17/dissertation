Commission Decision
of 27 June 2006
amending Decision 2006/148/EC on introducing preventive vaccination against highly pathogenic avian influenza H5N1 and related provisions for movements in France
(notified under document number C(2006) 2875)
(Only the French text is authentic)
(2006/438/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 2005/94/EC of 20 December 2005 on Community measures for the control of avian influenza and repealing Directive 92/40/EEC [1], and in particular Article 57(2) thereof,
Whereas:
(1) By Commission Decision 2006/148/EC of 24 February 2006 on introducing preventive vaccination against highly pathogenic avian influenza H5N1 and related provisions for movements in France [2], the plan for preventive vaccination against highly pathogenic avian influenza H5N1, submitted by France to the Commission on 21 February 2006 (the preventive vaccination plan), has been approved and certain measures to be applied in France have been laid down where preventive vaccination is carried out.
(2) According to the preventive vaccination plan, France has undertaken the vaccination of ducks and geese against highly pathogenic avian influenza H5N1, which is considered as a pilot project since there is limited experience with preventive vaccination in these species.
(3) According to the preventive vaccination plan, such as approved by Decision 2006/148/EC, vaccination had to be completed by 1 April 2006.
(4) On 20 April 2006 France presented its first comprehensive report on the use of vaccination. Furthermore, France has requested a prolongation of the use of preventive vaccination under the same conditions until 30 June 2006 in order to gain further experience and epidemiological insight and has correspondigly submitted an amendment to the preventive vaccination plan.
(5) On the basis of the information contained in the report presented by France, the Commission is of the view that further field experience is needed with respect to the use of vaccination against the spread of highly pathogenic avian influenza H5N1 in ducks and geese. Therefore it is appropriate to approve the prolongation of preventive vaccination submitted by France until 30 June 2006.
(6) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
The first subparagraph of Article 2(1) of Decision 2006/148/EC shall be replaced by the following:
"1. The plan for preventive vaccination, providing for vaccination until 30 June 2006, against highly pathogenic avian influenza H5N1, submitted by France to the Commission on 21 February 2006, and its amendment of 20 April 2006, is approved (the preventive vaccination plan)."
Article 2
Addressee
This Decision is addressed to the French Republic.
Done at Brussels, 27 June 2006.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 10, 14.1.2006, p. 16.
[2] OJ L 55, 25.2.2006, p. 51.
--------------------------------------------------
