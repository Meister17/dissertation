Judgment of the Court of First Instance of 12 July 2006 — Hassan v Council and Commission
(Case T-49/04) [1]
Parties
Applicant: Faraj Hassan (Brixton, United Kingdom) (represented by: E. Grieves, Barrister, and H. Miller, Solicitor)
Defendants: Council of the European Union (represented by: S. Marquardt and E. Finnegan, Agents) and Commission of the European Communities (represented by: J. Enegren and C. Brown, Agents)
Re:
Application, first, for annulment of Council Regulation (EC) No 881/2002 of 27 May 2002 imposing certain specific restrictive measures directed against certain persons and entities associated with Usama bin Laden, the Al-Qaeda network and the Taliban, and repealing Council Regulation (EC) No 467/2001 prohibiting the export of certain goods and services to Afghanistan, strengthening the flight ban and extending the freeze of funds and other financial resources in respect of the Taliban of Afghanistan (OJ 2002 L 139, p. 9), as amended by Commission Regulation (EC) No 2049/2003 of 20 November 2003 amending Regulation No 881/2002 for the 25th time (OJ 2003 L 303, p. 20), and, second, for damages
Operative part of the judgment
The Court:
1. Dismisses the action;
2. Orders the applicant to pay the costs.
[1] OJ C 94, 17.4.2004.
--------------------------------------------------
