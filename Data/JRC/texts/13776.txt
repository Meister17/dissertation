Action brought on 8 May 2006 — Kerstens v Commission
Parties
Applicant: Petrus J.F. Kerstens (Overijse, Belgium) (represented by: C. Mourato, lawyer)
Defendant: Commission of the European Communities
Form of order sought
The applicant claims that the Tribunal should:
- Annul the Appointing Authority's decision of 11 July 2005 adopting the applicant's Career Development Report (CDR) for the year 2004;
- Annul the Appointing Authority's express decision of 6 February 2006 rejecting the applicant's complaint No R/769/05;
- Order the Commission of the European Communities to pay the costs.
Pleas in law and main arguments
The applicant, a Commission official, challenges the merit points and markings contained in his CDR for the year 2004. He pleads infringement of the appraisal procedure rules and of the General Provisions Implementing Article 43 of the Staff Regulations, a manifest error of assessment and infringement of Article 43 of the Staff Regulations. The applicant reserves, lastly, the right to expound a third plea in law alleging misuse of powers.
--------------------------------------------------
