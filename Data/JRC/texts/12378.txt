Commission Decision
of 28 July 2006
concerning certain protection measures in relation to highly pathogenic avian influenza in South Africa
(notified under document number C(2006) 3350)
(Text with EEA relevance)
(2006/532/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/496/EEC of 15 July 1991 laying down the principles governing the organisation of veterinary checks on animals entering the Community from third countries and amending Directives 89/662/EEC, 90/425/EEC and 90/675/EEC [1], and in particular Article 18(1) and (6) thereof,
Having regard to Council Directive 97/78/EC of 18 December 1997 laying down the principles governing the organisation of veterinary checks on products entering the Community from third countries [2], and in particular Article 22(1) and (5) thereof,
Whereas:
(1) Avian influenza is an infectious viral disease in poultry and other birds, causing mortality and disturbances which can quickly take epizootic proportions liable to present a serious threat to animal and public health and to reduce sharply the profitability of poultry farming. There is a risk that the disease agent might be spread through international trade in live poultry and other birds or their products.
(2) On 29 June 2006, South Africa confirmed an outbreak of highly pathogenic avian influenza in a ratite farm in the Western Cape Province.
(3) The avian influenza virus strain detected during that outbreak is of subtype H5N2 and therefore different from the strain currently causing the epidemic in Asia, Northern Africa and Europe. Current knowledge suggests that the risk for public health in relation to that subtype is inferior to the risk from the strain circulating in Asia, which is an H5N1 virus subtype.
(4) Under current Community legislation, South Africa is only authorised to export to the Community live ratites and their hatching eggs and fresh meat and meat preparations and products containing meat of those species.
(5) In view of the animal health risk of the introduction of highly pathogenic avian influenza into the Community, it is appropriate as an immediate measure to suspend imports of live ratites and hatching eggs of these species from South Africa.
(6) In addition, it is appropriate to suspend imports into the Community from South Africa of fresh meat of ratites and meat preparations and meat products consisting of or containing meat of those species. However, taking into account the fact that the disease was introduced on the affected farms in mid-June, it is appropriate to provide for a derogation for fresh meat and meat preparations and meat products consisting of or containing meat from such species slaughtered before 1 May 2006 subject to certain conditions.
(7) South Africa has applied strict disease control measures and has sent further information on the disease situation to the Commission which justify limiting the suspension of imports to the affected part of the territory of South Africa.
(8) Commission Decision 2005/432/EC of 3 June 2005 laying down the animal and public health conditions and model certificates for imports of meat products for human consumption from third countries and repealing Decisions 97/41/EC, 97/221/EC and 97/222/EC [3] lays down the list of third countries from which Member States may authorise the importation of certain meat products and establishes treatment regimes considered effective in inactivating the pathogens of certain animal diseases. In order to prevent the risk of disease transmission via such products, appropriate treatment must be applied depending on the health status of the country of origin and the species the product is obtained from. It is therefore appropriate that imports of meat products and meat preparations consisting of or containing meat of ratites originating in South Africa and subjected to an appropriate treatment provided for in that Decision should continue to be authorised.
(9) As soon as South Africa has communicated further information on the disease situation concerning highly pathogenic avian influenza and the control measures taken in that respect, the measures taken at Community level in relation to the recent outbreak in South Africa should be reviewed. Accordingly, this Decision should only apply until 31 October 2006.
(10) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
Member States shall suspend imports, from that part of the territory of South Africa referred to in the Annex to this Decision, of:
(a) live ratites and hatching eggs of ratites;
(b) fresh meat of ratites;
(c) meat products and meat preparations consisting of or containing meat of ratites.
Article 2
1. By way of derogation from points (b) and (c) of Article 1, Member States shall authorise the importation of fresh meat, meat products and meat preparations referred to in those points which have been obtained from birds slaughtered before 1 May 2006.
2. In the veterinary certificates accompanying consignments of the meat, meat products and meat preparations referred to in paragraph 1 the following words shall be included:
"Fresh ratite meat/meat products consisting of or containing meat of ratites/meat preparations consisting of or containing meat of ratites [4] obtained from birds slaughtered before 1 May 2006 in accordance with Article 2(1) of Commission Decision 2006/532/EC.
3. By way of derogation from point (c) of Article 1, Member States shall authorise the importation of meat products and meat preparations consisting of or containing meat of ratites provided that the meat has undergone at least one of the specific treatments referred to under points B, C or D in Part 4 of Annex II to Decision 2005/432/EC.
Article 3
The Member States shall immediately take the necessary measures to comply with this Decision and publish those measures. They shall immediately inform the Commission thereof.
Article 4
This Decision shall apply until 31 October 2006.
Article 5
This Decision is addressed to the Member States.
Done at Brussels, 28 July 2006.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 268, 24.9.1991, p. 56. Directive as last amended by the 2003 Act of Accession.
[2] OJ L 24, 30.1.1998, p. 9. Directive as last amended by Regulation (EC) No 882/2004 of the European Parliament and of the Council (OJ L 165, 30.4.2004, p. 1, corrected by OJ L 191, 28.5.2004, p. 1).
[3] OJ L 151, 14.6.2005, p. 3. Decision as amended by Decision 2006/330/EC (OJ L 121, 6.5.2006, p. 43).
[4] Delete as appropriate."
--------------------------------------------------
ANNEX
Part of the territory of South Africa referred to in Article 1
ISO country code | Name of country | Part of territory |
ZA | South Africa | The districts of Riversdale and Mossel Bay in the Western Cape Province |
--------------------------------------------------
