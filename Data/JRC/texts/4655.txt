Notice to importers
Imports from Israel into the Community
(2005/C 20/02)
By an earlier Notice to Importers published on 23 November 2001 in Official Journal of the European Communities C 328 (page 6), operators presenting documentary evidence of origin with a view to securing preferential treatment for products originating from Israeli settlements in the West Bank, Gaza Strip, East Jerusalem and the Golan Heights, were informed that putting the products in free circulation could give rise to a customs debt.
According to the Community, products coming from places brought under Israeli Administration since 1967 are not entitled to benefit from preferential tariff treatment under the EU-Israel Association Agreement [1].
Operators are informed that the EU and Israel have arrived to an arrangement for the implementation of Protocol 4 to the Agreement. As a result, all movement certificates EUR.1 and invoice declarations made out in Israel will bear, as from 1 February 2005 the name of the city, village or industrial zone where production conferring originating status has taken place.
Operators presenting preferential proofs of origin under the EU-Israel Association Agreement are informed that the preferential treatment will be refused to the goods for which the proof of origin indicates that the production conferring originating status has taken place in a city, village or industrial zone which is brought under Israeli Administration since 1967.
This notice replaces the November 2001 notice from 1 February 2005.
--------------------------------------------------
[1] OJ L 147 of 21.6. 2000, p. 3.
