Notice inviting applications for authorisation to extract hydrocarbons in block P8 of the Dutch continental shelf
(2005/C 191/07)
The Minister for Economic Affairs of the Kingdom of the Netherlands hereby gives notice that an application has been received for authorisation to extract hydrocarbons in block P8 as marked on the map attached as Annex 3 to the Mining Regulation (Government Gazette 2002, No 245).
With reference to Article 3(2) of Directive 94/22/EC of the European Parliament and of the Council of 30 May 1994 on the conditions for granting and using authorisations for the prospection, exploration and production of hydrocarbons and the publication required by Article 15 of the Mining Act (Bulletin of Acts and Decrees 2002, No 542), the Minister for Economic Affairs hereby invites interested parties to submit an application for authorisation to extract hydrocarbons in block P8 of the Dutch continental shelf.
Applications may be submitted during the 13 weeks following the publication of this invitation in the Official Journal of the European Union and must be sent to the Minister for Economic Affairs, for the attention of the Director of Energy Production, Prinses Beatrixlaan 5, The Hague, Netherlands, and marked "personal". Applications submitted following the expiry of this period will not be considered.
A decision on applications will be taken not later than twelve months after the expiry of the period.
Further information can be obtained from the following telephone number: (31 70) 379 66 94.
--------------------------------------------------
