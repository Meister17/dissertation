Information communicated by Member States regarding State aid granted under Commission Regulation (EC) No 68/2001 of 12 January 2001 on the application of Articles 87 and 88 of the EC Treaty to training aid
(2006/C 177/04)
(Text with EEA relevance)
Aid number | XT 28/01 |
Member State | Italy |
Region | Abruzzi |
Title of aid scheme | Continuing training — measures to promote training plans |
Legal basis | Art. 118 L. statale 23.12.2000 n. 388 |
Annual expenditure planned under the scheme | Regional Council decision No 376 of 15 May 2001 for EUR 1881223.38 and decision DL 2/357 of 20 December 2002 approving further lists of beneficiaries — "training aid" Annex Bbis to decision DL 2/357 of 20 December 2002 |
Maximum aid intensity | SMEs: 70 % (general training)35 % (specific training)Large firms: 50 % (general training) |
Date of implementation | Date of call for applications issued under Regional Council decision No 376/01 and subsequent implementing decision DL 2/357 of 20 December 2002 |
Duration of scheme | Duration of training measure limited to 12 months ending on 31 December 2003 |
Objective of aid | Training of employees as specified in detail in the operational plan submitted for assessment. The Region will check courses in progress to verify actual implementation of the general or specific training as described in the approved individual plans |
Economic sector(s) concerned | All manufacturing industries Other services |
Name and address of granting authority | Name: Ente Regione Abruzzo — Giunta regionale Direzione Politiche attive del lavoro, della formazione e dell'istruzione Servizio implementazione programmi e progetti |
Address: Via Raffaello n. 137 I-65126 Pescara |
Aid number | XT 25/03 |
Member State | Spain |
Region | Galicia |
Title of aid scheme or name of company receiving individual aid | Training of women to develop occupational skills relating to priority sectors and activities in Galicia |
Legal basis | Resolución de 30 de diciembre de 2002 por la que se establecen las bases reguladoras a las ayudas que regirán las líneas de actuación y estímulo a la formación de mujeres en aquellas profesiones ligadas a sectores y actividades considerados prioritarios en Galicia y se procede a su convocatoria. (Diario Oficial de Galicia no 5 de 9.1.2003) |
Annual expenditure planned or overall amount of individual aid granted to the company | 2003 EUR 21580002004 EUR 25896002005 EUR 31075202006 EUR 3729024 |
Maximum aid intensity | The State aid governed by this Order is granted, in principle, for general training. The maximum intensity is that established in Article 4 of Regulation (EC) No 68/2001 since Galicia qualifies for regional aid under Article 87(3)(a) of the EC Treaty |
Date of implementation | June-November 2003 |
Duration of scheme or individual aid award | Until 31.12.2006 |
Objective of aid | Vocational training programmes for women organised by local authorities, associations or companies.Coordination of vocational training activities with vocational guidance activities and plans for the promotion of the social economy and businesses.Programmes to help women enter the labour market by improving their vocational skills in jobs for which there is demand in the market.One-off training projects that add value to the above activities |
Economic sectors concerned | All sectors |
Name and address of the granting authority | Name: Servicio Gallego de Promoción de la igualdad del Hombre y de la Mujer |
Address: Plaza de Europa, 15 A 2o Área Central-Polígono de Fontiñas E-15781 Santiago de Compostela Tel: (981) 54 53 60 E-mail: sgpihm@xunta.es |
Aid No | XT 60/04 |
Member State | Belgium |
Region | Flanders |
Title of aid scheme or name of company receiving individual aid | Interbrew NV Vaartstraat 94 B-3000 Leuven |
Legal basis | Besluit van de Vlaamse regering van 11.6.2004 |
Annual expenditure planned or overall amount of aid granted to the company | Aid scheme | Annual overall amount | |
Loans guaranteed | |
Individual aid | Overall aid amount | EUR 0,9 million |
Loans guaranteed | |
Maximum aid intensity | In conformity with Article 4(2)-(6) of the Regulation | Yes | |
Date of implementation | 11.6.2004 |
Duration of scheme or individual aid | Until 31.12.2006 |
Objective of aid | General training | Yes |
Specific training | Yes |
Economic sectors concerned | All sectors eligible for training aid | No |
Limited to specific sectors | "Ad hoc" case |
—Agriculture | |
—Fisheries and aquaculture | |
—Coalmining | |
—All manufacturing | |
or | |
Steel | |
Shipbuilding | |
Synthetic fibres | |
Motor vehicles | |
Other manufacturing | Brewing |
—All services | |
or | |
Maritime transport services | |
Other transport services | |
Financial services | |
Other services | |
Name and address of the granting authority | Name: Ministerie van de Vlaamse Gemeenschap Administratie Economie Afdeling Economisch Ondersteuningsbeleid |
Address: Markiesstraat 1 B-1000 Brussel |
Large individual aid grants | In conformity with Article 5 of the Regulation. The measure excludes awards of aid or requires prior notification to the Commission of awards of aid, if the amount of aid granted to one enterprise for a single training project exceeds EUR 1 million | Yes | |
--------------------------------------------------
