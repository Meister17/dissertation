Prior notification of a concentration
(Case COMP/M.4420 — Crédit Agricole/Fiat Auto/FAFS)
(2006/C 271/06)
(Text with EEA relevance)
1. On 27 October 2006, the Commission received a notification of a proposed concentration pursuant to Article 4 of Council Regulation (EC) No 139/2004 [1] by which the undertakings Crédit Agricole SA ("Crédit Agricole", France) and Fiat Auto S.p.A. ("Fiat", Italy) belonging to the Fiat Group acquire within the meaning of Article 3(1)(b) of the Council Regulation joint control of the undertaking Fidis Retail S.p.A. ("Fidis", Italy) by way of purchase of shares. Subsequently, certain subsidiaries of the Fiat Group will be contributed to the joint venture which will be renamed as Fiat Auto Financial Services S.p.A ("FAFS", Italy).
2. The business activities of the undertakings concerned are:
- for Crédit Agricole: banking and financial services,
- for Fiat: manufacturing of cars,
- for FAFS: car purchase financing for consumers and concessionaires, commercial vehicle long-term renting and fleet management services.
3. On preliminary examination, the Commission finds that the notified transaction could fall within the scope of Regulation (EC) No 139/2004. However, the final decision on this point is reserved.
4. The Commission invites interested third parties to submit their possible observations on the proposed operation to the Commission.
Observations must reach the Commission not later than 10 days following the date of this publication. Observations can be sent to the Commission by fax (fax No (32-2) 296 43 01 or 296 72 44) or by post, under reference number COMP/M.4420 — Crédit Agricole/Fiat Auto/FAFS, to the following address:
European Commission
Directorate-General for Competition
Merger Registry
J-70
B-1049 Bruxelles/Brussel
[1] OJ L 24, 29.1.2004, p. 1.
--------------------------------------------------
