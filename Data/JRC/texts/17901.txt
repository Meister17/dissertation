Commission Decision
of 7 May 2004
on the state aid granted by Germany to Fairchild Dornier GmbH (Dornier)
(notified under document number C(2004) 1621)
(Only the German version is authentic)
(Text with EEA relevance)
(2004/820/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community, and in particular the first subparagraph of Article 88(2) thereof,
Having regard to the Agreement on the European Economic Area, and in particular Article 62(1)(a) thereof,
Having called on interested parties to submit their comments pursuant to the provisions cited above [1] and having regard to their comments,
Whereas:
1. PROCEDURE
(1) On 19 July 2002 the Commission authorised rescue aid [2] for Fairchild Dornier GmbH ("Dornier"). The aid consisted of a guarantee with a three-month maturity. On 6 August 2002 Germany notified the Commission that it intended to extend the guarantee and communicated to it additional measures in favour of Dornier.
(2) On 5 February 2003 a formal investigation procedure was initiated into the extension of the guarantee and the additional measures [3]. Germany’s reply to the initiation of the investigation procedure was received on 2 April 2003, and the last information supplied by Germany on 3 December 2003 No comments were submitted by third parties during the investigation procedure.
2. DESCRIPTION OF THE AID
2.1. Dornier
(3) From 1996 the German aircraft manufacturer Dornier belonged to the American company Fairchild Aerospace. With some 3600 employees, Dornier manufactured aircraft and aircraft parts in Oberpfaffenhofen-Wessling, Bavaria. The plant facilities and business premises in the United States were sold off. In March 2002 Dornier filed for bankruptcy.
(4) Bankruptcy proceedings were opened on 1 July 2002. At the same time the employees were divided into an active group and a passive group, the plan being to dismiss the latter, which accounted for about half the workforce. The employees in the passive group ceased working and were included in a social plan, which was partly financed by a state corporation. On 20 December 2002 the bankruptcy trustee decided to liquidate the undertaking and sell the assets separately.
(5) There were two separate asset deals: aircraft manufacture and customer services were sold to AvCraft Aerospace GmbH and AvCraft International Ltd, while the manufacture of Airbus parts and aviation services were transferred to Ruag Holding (Switzerland). According to Germany, this was done in an open, transparent procedure.
2.2. Financial measures
Extension of the guarantee
(6) On 19 July 2002 the Commission authorised a 50 % deficiency guarantee by the Federal Government and Bavaria for a loan of USD 90 million. The guarantee was authorised as rescue aid for the requested period of three months. The period began when the authorisation was granted and would have ended on 20 September 2002.
(7) On 6 August 2002 Germany applied for an extension of the guarantee until 20 December 2002, i.e. for a further three months, so that Dornier could carry on operating during the search for a financial partner. The terms of the guarantee remained unchanged. The guarantee related to the same loan, which had not been fully utilised. It was formally terminated on 20 December 2002. The extension until that date is the subject of this Decision.
Social measures
(8) In the second notification of 6 August 2002 the Commission was informed that the Federal Labour Office (Bundesanstalt für Arbeit) had agreed to cover some EUR 12,6 million of a social plan costing a total of EUR 20,6 million for the 1800 employees faced with dismissal. The remaining EUR 8 million were financed by the undertaking. The Federal Labour Office measure is also the subject of this Decision.
(9) According to Germany, the measures were not used to pay wages or redundancy money, but to finance the following costs: individual support for employees, identifying their strengths and weaknesses, setting goals, training, measures to promote mobility, decentralisation, setting up a job agency, etc. The group of employees covered by the social plan ceased working.
3. CONCLUSION
(10) The guarantee was terminated in December 2002 after a total period of six months. The social plan drawn up for employees in the passive group also ended in December 2002. Dornier was then liquidated, and its assets were transferred to different investors. The beneficiary of the measures no longer exists therefore. Consequently, and since according to Germany the liquidation proceedings were carried out openly and transparently and the assets sold at market prices, an assessment of the measures would not be necessary.
(11) The formal investigation under Article 88(2) of the EC Treaty of the measures described was thus rendered unnecessary,
HAS ADOPTED THIS DECISION:
Article 1
The formal investigation procedure under Article 88(2) of the EC Treaty initiated with regard to Fairchild Dornier GmbH on 5 February 2003 is terminated.
Article 2
This Decision is addressed to the Federal Republic of Germany.
Done at Brussels, 7 May 2004.
For the Commission
Mario Monti
Member of the Commission
--------------------------------------------------
[1] OJ C 67, 20.3.2003, p. 2.
[2] OJ C 239, 4.10.2002, p. 2.
[3] See footnote 2.
