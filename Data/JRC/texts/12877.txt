Decision of the EEA Joint Committee
No 14/2006
of 27 January 2006
amending Annex XX (Environment) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex XX to the Agreement was amended by Decision of the EEA Joint Committee No 107/2005 of 8 July 2005 [1].
(2) Commission Decision 2005/293/EC of 1 April 2005 laying down detailed rules on the monitoring of the reuse/recovery and reuse/recycling targets set out in Directive 2000/53/EC of the European Parliament and of the Council on end-of-life vehicles [2] is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
The following point shall be inserted after point 32ec (Commission Decision 2003/138/EC) of Annex XX to the Agreement:
"32ed. 32005 D 0293: Commission Decision 2005/293/EC of 1 April 2005 laying down detailed rules on the monitoring of the reuse/recovery and reuse/recycling targets set out in Directive 2000/53/EC of the European Parliament and of the Council on end-of-life vehicles (OJ L 94, 13.4.2005, p. 30)."
Article 2
The texts of Decision 2005/293/EC in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 28 January 2006, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [3].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 27 January 2006.
For the EEA Joint Committee
The President
R. Wright
[1] OJ L 306, 24.11.2005, p. 45.
[2] OJ L 94, 13.4.2005, p. 30.
[3] No constitutional requirements indicated.
--------------------------------------------------
