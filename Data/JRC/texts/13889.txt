Prior notification of a concentration
(Case COMP/M.4473 — MLCP/CIR/Oakwood)
Candidate case for simplified procedure
(2006/C 288/10)
(Text with EEA relevance)
1. On 17 November 2006, the Commission received a notification of a proposed concentration pursuant to Article 4 of Council Regulation (EC) No 139/2004 [1] by which the undertakings Merrill Lynch Credit Products, LLP ("MLCP", USA), part of the Merrill Lynch & Co, Inc. group of companies and CIR Compagnie Industriali Riunite S.p.A, ("CIR", Italy), controlled by Compagnia Finanziaria De Benedetti, acquire joint control within the meaning of Article 3(1)(b) of the Council Regulation of the whole of an existing fund, Oakwood Financial Fund L.P. ("Oakwood", Cayman Islands) by way of purchase of shares.
2. The business activities of the undertakings concerned are:
- for MLCP: investment banking and other financial services;
- for CIR: newspaper and magazine publishing activities; radio, digital television, advertising and internet sectors; automotive components; gas and electricity; private healthcare; and non-performing loans;
- for Oakwood: origination and sales/acquisition of non-conforming residential mortgages, non-conforming hire purchase contracts for motor vehicles, salary and pension secured loans and small ticket equipment leasing to local authorities and SMEs.
3. On preliminary examination, the Commission finds that the notified transaction could fall within the scope of Regulation (EC) No 139/2004. However, the final decision on this point is reserved. Pursuant to the Commission Notice on a simplified procedure for treatment of certain concentrations under Council Regulation (EC) No 139/2004 [2] it should be noted that this case is a candidate for treatment under the procedure set out in the Notice.
4. The Commission invites interested third parties to submit their possible observations on the proposed operation to the Commission.
Observations must reach the Commission not later than 10 days following the date of this publication. Observations can be sent to the Commission by fax (fax No (32-2) 296 43 01 or 296 72 44) or by post, under reference number COMP/M.4473 — MLCP/CIR/Oakwood, to the following address:
European Commission
Directorate-General for Competition
Merger Registry
J-70
B-1049 Bruxelles/Brussel
[1] OJ L 24, 29.1.2004, p. 1.
[2] OJ C 56, 5.3.2005, p. 32.
--------------------------------------------------
