COMMISSION DECISION of 10 February 1981 amending Decision 80/755/EEC authorizing the indelible printing of prescribed information on packages of cereal seed (81/109/EEC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Directive 66/402/EEC of 14 June 1966 on the marketing of cereal seed (1), as last amended by Directive 79/692/EEC (2), and in particular the last sentence of Article 10 (1) (a) thereof,
Whereas, cereal seed may not normally be placed on the market unless the packages are labelled with an official label in accordance with the provisions laid down in Directive 66/402/EEC;
Whereas under those provisions, the indelible printing of the prescribed information on the package itself, on the basis of the model laid down for the label, may be authorized;
Whereas such authorization should be granted, under certain conditions which ensure that responsibility rests with the certification authority;
Whereas the Commission has already granted such an authorization under Decision 80/755/EEC (3);
Whereas, in the course of applying the abovementioned Decision, it has become apparent that the same should be amended as regards the prescribed information to be printed or stamped on packages when samples are taken;
Whereas the measures provided for in this Decision are in accordance with the opinion of the Standing Committee on Seeds and Propagating Material for Agriculture, Horticulture and Forestry,
HAS ADOPTED THIS DECISION:
Article 1
Article 1 (2) (c) of Decision 80/755/EEC is hereby amended as follows: - "Annex IV, (A) (a) (3.3a) and (6)" is replaced by"Annex IV, (A) (a) (3) and (3a)",
- in the German text "nicht vor" is replaced by "bei".
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 10 February 1981.
For the Commission
Poul DALSAGER
Member of the Commission (1) OJ No 125, 11.7.1966, p. 2309/66. (2) OJ No L 205, 13.8.1979, p. 1. (3) OJ No L 207, 9.8.1980, p. 37.
