Commission Regulation (EC) No 379/2005
of 4 March 2005
amending Regulation (EC) No 1168/1999 laying down marketing standards for plums
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 2200/96 of 28 October 1996 on the common organisation of the market in fruit and vegetables [1], and in particular Article 2(2) thereof,
Whereas:
(1) Commission Regulation (EC) No 537/2004 of 23 March 2004 adapting several regulations concerning the market of fresh fruit and vegetables by reason of the accession of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia to the European Union [2] added several varieties to the non-exhaustive list of large-fruited varieties of Prunus domestica by replacing the Appendix to the Annex to Commission Regulation (EC) No 1168/1999 [3]. However, the new Appendix does not contain the non-exhaustive list of large-fruited varieties of Prunus salicina it included before the amendment, following the recommendation of the United Nations Economic Commission for Europe to distinguish between varieties of Prunus domestica and those of Prunus salicina. In the interest of transparency on the world market, that list should be re-established.
(2) Regulation (EC) No 1168/1999 should therefore be amended accordingly.
(3) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Fresh Fruit and Vegetables,
HAS ADOPTED THIS REGULATION:
Article 1
The Appendix to the Annex to Regulation (EC) No 1168/1999 is amended in accordance with the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the twentieth day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 4 March 2005.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 297, 21.11.1996, p. 1. Regulation as last amended by Commission Regulation (EC) No 47/2003 (OJ L 7, 11.1.2003, p. 64).
[2] OJ L 86, 24.3.2004, p. 9.
[3] OJ L 141, 4.6.1999, p. 5. Regulation as last amended by Regulation (EC) No 907/2004 (OJ L 163, 30.4.2004, p. 50).
--------------------------------------------------
ANNEX
The Appendix to the Annex to Regulation (EC) No 1168/1999 is amended as follows:
1. The title of the table is replaced by the following:
"1. Non-exhaustive list of large-fruited varieties of Prunus domestica"
2. The following text is added:
"2. Non-exhaustive list of large-fruited varieties of Prunus salicina
Variety Cultivar and/or trade name | Synonyms |
Allo | |
Andy’s Pride | |
Angeleno | |
Autumn Giant | |
Autumn Pride | |
Beaut Sun | |
Beauty | Beaty |
Bella di Barbiano | |
Black Amber | |
Black Beaut | |
Black Gold | |
Black Rosa | |
Black Royal | |
Black Star | |
Black Sun | |
Burbank | |
Burmosa | |
Calita | |
Casselman | Kesselman |
Catalina | |
Celebration | |
Centenaria | |
Del Rey Sun | |
Delbarazur | |
Dólar | |
Eclipse | |
Eldorado | |
Eric Sun | |
Flavor King | |
Formosa | |
Fortune | |
Friar | |
Frontier | |
Gavearli | |
Gaviota | |
Globe Sun | |
Goccia d'Oro | |
Golden Japan | Shiro |
Golden King | |
Golden Kiss | |
Golden Plum | |
Goldsweet 4 | |
Grand Rosa | |
Green Sun | |
Hackman | |
Harry Pickstone | |
Howard Sun | |
Kelsey | |
Lady Red | |
Lady West | |
Laetitia | |
Laroda | |
Larry Ann | Larry Anne, Tegan Blue, Freedom |
Late Red | |
Late Santa Rosa | |
Linda Rosa | |
Mariposa | Improved Satsuma, Satsuma Improved |
Methley | |
Midnight Sun | |
Morettini 355 | Cœur de Lion |
Narrabeen | |
Newyorker | |
Nubiana | |
Obilnaja | |
October Sun | |
Original Sun | |
Oro Miel | |
Ozark Premier | Premier |
Pink Delight | |
Pioneer | |
Queen Ann | |
Queen Rosa | |
Red Beaut | |
Red Rosa | |
Red Sweet | |
Redgold | |
Redroy | |
Reubennel | Ruby Nel |
Royal Black | |
Royal Diamond | |
Royal Garnet | |
Royal Star | |
Roysum | |
Ruby Blood | |
Ruby Red | |
Sangue di Drago | |
Santa Rosa | |
Sapphire | |
Satsuma | |
Simka | |
Sir Prize | Akihime |
Songold | |
Southern Belle | |
Southern Pride | |
Souvenir | |
Souvenir II | |
Spring Beaut | |
Starking Delicious | |
Stirling | |
Suplumeleven | |
Suplumthirteen | |
Suplumtwelve | |
Susy | |
TC Sun | |
Teak Gold | |
Top Black | |
Tracy Sun | |
Wickson | |
Yakima | |
Yellow Sun | |
Zanzi Sun" | |
--------------------------------------------------
