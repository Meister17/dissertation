[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 14.11.2006
COM(2006) 706 final
2005/0223(COD)
COMMUNICATION FROM THE COMMISSION TO THE EUROPEAN PARLIAMENT pursuant to the second subparagraph of Article 251 (2) of the EC Treaty concerning the
common position of the Council on the adoption of a Regulation of the European Parliament and of the Council on the submission of data on landings of fishery products in Member States.
2005/0223(COD)
COMMUNICATION FROM THE COMMISSION TO THE EUROPEAN PARLIAMENT pursuant to the second subparagraph of Article 251 (2) of the EC Treaty concerning the
common position of the Council on the adoption of a Regulation of the European Parliament and of the Council on the submission of data on landings of fishery products in Member States. (Text with EEA relevance)
1. BACKGROUND
Proposal submitted to the European Parliament and to the Council (COM(2005)566 –– 2005/0223(COD)) | 11 November 2005 |
Opinion of the European Parliament (first reading) : (A6-0169/2006) | 15 June 2006 |
Adoption of the common position by the Council : | 13 November 2006 |
2. AIM OF THE COMMISSION PROPOSAL
The current Community legislation based on Council Regulation (EEC) No 1382/91 requires the Member States to submit to the Commission monthly data on the quantity and value of landings of fishery products in their ports. An analysis of the methods of collecting and compiling the data by the national authorities and of the uses made of such data by the Commission's services have revealed that improvements can be made which would reduce the workload on the Member States and improve the usefulness of the data. The proposed Regulation provides for the submission by the Member States of harmonised data on the quantity and value of landings on their territories to permit analyses of the market for fishery products and more general economic analyses of the situation in fisheries.
The Commission proposal is intended to replace the Council Regulation (EEC) No 1382/91.
3. COMMENTS ON THE COMMON POSITION
3.1. General
At first reading, the European Parliament approved the Commission's proposal subject to 21 amendments. The great majority of these amendments was of an editorial or minor technical nature that did not affect the substance of the proposal.
3.2. Decisions on the European Parliament's amendments after first reading
The Commission has agreed to the amendments proposed by the Parliament. The European Parliament's resolution adopts a positive approach to this proposal with the great majority of the 21 amendments being in the nature of technical and editorial clarifications. The exception is the requirement for the Commission to report to Parliament and Council once every three years on the implementation of the Regulation (amendment 7, new Article 8b). It is viewed by the Commission as improving the transparency of the operation. In this light, all 21 amendments proposed by the European Parliament are acceptable to the Commission. The Council has endorsed these amendments within its common position.
3.3. New provisions introduced by the Council and the Commission's position.
On 17 July 2006, the Council adopted Decision 2006/512/EC amending Council Decision 1999/468/EC of 28 June 1999 laying down the procedures for the exercise of implementing powers conferred on the Commission and introducing a new procedure named "regulatory procedure with scrutiny" (Article 5a).
The new Committee procedure has to be followed to adopt measures of general scope which seek to amend non-essential elements of a basic instrument adopted in accordance with the procedure referred to in Article 251 of the Treaty, inter alia by deleting some of those elements or by supplementing the instrument by the addition of new non-essential elements.
New provisions introduced by the Council refer to the implementing powers conferred to the Commission when the new procedure has to be apply.
In particular, powers are conferred on the Commission to adapt the Annexes to the Regulation. These measures of general scope designed to amend non-essential elements of this Regulation should be adopted in accordance with the regulatory procedure with scrutiny laid down to in Article 5a of Council Decision 1999/468/EC.
The Commission accepts these new provisions.
4. Conclusion
Under these conditions and for the reasons set out above, the Commission hereby expresses a favourable opinion on the common position of the Council adopted unanimously.
