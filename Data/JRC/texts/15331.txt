Euro exchange rates [1]
26 September 2006
(2006/C 232/01)
| Currency | Exchange rate |
USD | US dollar | 1,2695 |
JPY | Japanese yen | 147,83 |
DKK | Danish krone | 7,4591 |
GBP | Pound sterling | 0,66950 |
SEK | Swedish krona | 9,2895 |
CHF | Swiss franc | 1,5776 |
ISK | Iceland króna | 89,05 |
NOK | Norwegian krone | 8,3135 |
BGN | Bulgarian lev | 1,9558 |
CYP | Cyprus pound | 0,5767 |
CZK | Czech koruna | 28,444 |
EEK | Estonian kroon | 15,6466 |
HUF | Hungarian forint | 273,08 |
LTL | Lithuanian litas | 3,4528 |
LVL | Latvian lats | 0,6960 |
MTL | Maltese lira | 0,4293 |
PLN | Polish zloty | 3,9583 |
RON | Romanian leu | 3,5422 |
SIT | Slovenian tolar | 239,59 |
SKK | Slovak koruna | 37,400 |
TRY | Turkish lira | 1,9430 |
AUD | Australian dollar | 1,6859 |
CAD | Canadian dollar | 1,4149 |
HKD | Hong Kong dollar | 9,8852 |
NZD | New Zealand dollar | 1,8974 |
SGD | Singapore dollar | 2,0159 |
KRW | South Korean won | 1198,60 |
ZAR | South African rand | 9,7550 |
CNY | Chinese yuan renminbi | 10,0424 |
HRK | Croatian kuna | 7,4115 |
IDR | Indonesian rupiah | 11684,48 |
MYR | Malaysian ringgit | 4,673 |
PHP | Philippine peso | 63,869 |
RUB | Russian rouble | 33,9970 |
THB | Thai baht | 47,606 |
[1] Source: reference exchange rate published by the ECB.
--------------------------------------------------
