Commission Decision
of 12 March 2001
requiring Member States temporarily to take additional measures against the dissemination of Bursaphelenchus xylophilus (Steiner et Buhrer) Nickle et al. (the pinewood nematode) as regards areas in Portugal, other than those in which it is known not to occur
(notified under document number C(2001) 692)
(2001/218/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 2000/29/EC of 8 May 2000 on protective measures against the introduction into the Community of organisms harmful to plants or plant products and against their spread within the Community(1), and in particular Article 16(3) thereof,
Whereas:
(1) Where a Member State considers that there is an imminent danger of the introduction into its territory of Bursaphelenchus xylophilus (Steiner et Buhrer) Nickle et al., (the pine wood nematode (PWN)), from another Member State, it may temporarily take any additional measures necessary to protect itself from that danger.
(2) Portugal informed the other Member States and the Commission on 25 June 1999 that some samples of pine trees originating in its territory were identified as infested by PWN. Complementary reports supplied by Portugal indicated that more samples of pine trees showed infestation by it.
(3) Sweden, on the basis of the abovementioned information adopted on 29 September 1999, certain additional measures including a special heat treatment and the use of a plant passport, for all wood leaving Portugal, with a view to strengthening protection against the introduction of PWN from Portugal.
(4) It has not yet been possible either to identify the source of contamination although elements indicate that packaging material is the most likely pathway.
(5) The Commission, by Decision 2000/58/EC(2) authorised Member States temporarily to take additional measures against the dissemination of PWN, as regards areas in Portugal, other than those in which it is known not to occur.
(6) From an assessment by the Food and Veterinary Office (FVO) in May and October 2000 and additional information supplied by Portugal, it appears that the phytosanitary situation has improved as a result of the application of an eradication programme. However trees showing symptoms of infestation by PWN were still found during surveys of the area where it was previously known to occur.
(7) In official surveys carried out by the other Member States on wood, isolated bark and plants of Abies Mill., Cedrus Trew, Larix Mill., Picea A. Dietr., Pinus L., Pseudotsuga Carr. and Tsuga Carr., originating in their country, none of the samples taken and analysed tested positive for the presence of the PWN.
(8) It is therefore necessary for Portugal to continue to take specific measures. It may also be necessary for the other Member States to continue to adopt additional measures to protect themselves.
(9) The above measures should refer to movements of wood, isolated bark and host plants within demarcated areas in Portugal and from such areas into other areas of Portugal and into the other Member States.
(10) It is also necessary that Portugal continues to take measures to control the spread of PWN with the aim of eradication.
(11) The effect of the emergency measures will be assessed continually during 2001 and 2002, in particular on the basis of information to be provided by Portugal and the other Member States. If it becomes apparent that the emergency measures referred to in the present Decision are not sufficient to prevent the spread of PWN or have not been complied with, more stringent or alternative measures should be envisaged.
(12) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on Plant Health,
HAS ADOPTED THIS DECISION:
Article 1
In this Decision:
- "the pine wood nematode (PWN)" means Bursaphelenchus xylophilus (Steiner et Buhrer) Nickle et al.,
- "susceptible wood and bark" means wood and isolated bark of conifers (Coniferales), except that of Thuja L.;
- "susceptible plants" means plants (other than fruit and seeds) of Abies Mill., Cedrus Trew, Larix Mill., Picea A. Dietr., Pinus L., Pseudotsuga Carr. and Tsuga Carr.
Article 2
Until 28 February 2002 Portugal shall ensure that the conditions laid down in the Annex to this Decision are met in relation to susceptible wood, bark and plants, which are to be moved within or from demarcated areas in Portugal and defined as in accordance with Article 5, either to other areas in Portugal or to other Member States.
The conditions specified in part 1 of the Annex to this Decision, shall apply only to consignments leaving the demarcated areas in Portugal after 28 February 2001.
Article 3
Member States of destination other than Portugal:
(a) may subject consignments of susceptible wood, and bark and of susceptible plants coming from demarcated areas in Portugal and moved into their territory to testing for the presence of PWN;
(b) may take further appropriate steps to carry out official monitoring in respect of such consignments, to ascertain whether they comply with the relevant conditions specified in the Annex to this Decision.
Article 4
Member States shall conduct official surveys for PWN, on susceptible wood and bark and susceptible plants originating in their country, to determine whether there is any evidence of infestation by PWN.
Without prejudice to the provisions of Article 16(1) of Directive 2000/29/EC, where the results of the surveys provided for in the first paragraph indicate the occurrence of the PWN in areas where it was previously unknown, they shall be notified to the other Member States and to the Commission by 15 November 2001.
Article 5
Portugal shall establish areas in which PWN is known not to occur, and demarcate areas (hereinafter called demarcated areas) comprised of a part in which the PWN is known to occur and a part designated as buffer zone of not less than 20 km with surrounding that part, taking into account the results of the surveys referred to in Article 4.
The Commission shall compile a list of areas in which PWN is known not to occur and convey such a list to the Standing Committee on Plant Health and to the Member States. Any areas in Portugal not comprised in the above compiled list, shall be deemed to be demarcated areas.
The list of the areas referred to in the first sentence of the second paragraph shall be adjusted by the Commission according to the results of the survey referred to in the second paragraph of Article 4 and to the findings notified under Article 16(1) of Directive 2000/29/EC.
Article 6
This Decision shall be reviewed by 15 December 2001 at the latest.
Article 7
Decision 2000/58/EC is hereby repealed with effect from the date of coming into force of this Decision.
Article 8
This Decision is addressed to the Member States.
Done at Brussels, 12 March 2001.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 169, 10.7.2000, p. 1.
(2) OJ L 21, 26.1.2000, p. 36.
ANNEX
For the purpose of Article 2, the following conditions shall be complied with.
1. Without prejudice to the provisions referred to in point 2 in the case of movements from demarcated areas into areas in Portugal, other than demarcated areas or into other Member States of:
(a) susceptible plants shall be accompanied by a plant passport prepared and issued in accordance with the provisions of Commission Directive 92/105/EEC(1), after:
- the plants have been officially inspected and found free from signs or symptoms of PWN and
- no symptoms of PWN have been observed at the place of production or in its immediate vicinity since the beginning of the last complete cycle of vegetation;
(b) susceptible wood and isolated bark, other than wood in the form of:
- chips, particles, wood waste or scrap obtained in whole or part from these conifers,
- packing cases, crates or drums,
- pallets, box pallets or other load boards,
- dunnage, spacers and bearers,
but including that which has not kept its natural round surface, shall be accompanied by the plant passport referred to in point 1(a), after the wood or the isolated bark has undergone an appropriate heat-treatment to achieve a minimum wood-core temperature of 56 °C for 30 minutes in order to ensure freedom from live PWNs;
(c) susceptible wood, in the form of chips, particles, wood waste or scrap obtained in whole or part from these conifers shall be accompanied by the said plant passport after having undergone an appropriate fumigation-treatment in order to ensure freedom from live PWNs;
(d) susceptible wood, in the form of dunnage, spacers and bearers, including that which has not kept its natural round surface shall:
- be stripped of its bark,
- be free from grub holes which are larger than 3 mm across,
- have a moisture content expressed as a percentage of dry matter of less than 20 % achieved at time of manufacture;
(e) susceptible wood, in the form of packing cases, boxes, crates, drums and similar packings, pallets, box pallets and other load boards, pallet collars, whether or not actually in use in the transport of objects of all kinds shall undergo either an appropriate heat treatment to achieve a minimum wood-core temperature of 56 °C for 30 minutes, pressure (impregnated) treatment, or fumigation in order to ensure freedom from live PWNs and either display an officially approved treatment marking enabling the identification of where and by whom the treatment has been carried out or be accompanied by the said plant passport attesting to the measures carried out.
2. In cases of movements within demarcated areas of Portugal:
(a) susceptible plants:
- grown in places of production where no symptoms of PWN have been observed, or in its immediate vicinity since the beginning of the last complete cycle of vegetation and found free from signs or symptoms of PWN during official inspections, shall be accompanied by the said plant passport when moved from the place of production,
- grown in places of production where symptoms of the pine wood nematode have been observed, or in its immediate vicinity, since the beginning of the last complete cycle of vegetation or identified as infested by the pine wood nematode shall not be moved from the place of production and shall be destroyed by burning,
- grown in places, such as forests, public or private gardens, which are either identified as infested by the pine wood nematode, or showing any symptoms of poor health or situated in salvage areas, shall:
- if identified during the period from 1 November to 1 April, be felled within that period, or
- if identified during the period from 2 April to 31 October, be felled immediately and,
- if located in the part of demarcated areas designated as buffer zones in accordance with the provisions of Article 5, tested for the presence of PWN. If the presence is confirmed, the delimitation of the demarcated areas shall be changed accordingly;
(b) during the period from 1 November to 1 April, susceptible wood in the form of roundwood or sawnwood, with or without bark, including that which has not kept its natural round surface:
(i) obtained from trees identified as infested by PWN, or situated in salvage areas, or showing any symptoms of poor health, shall before 2 April either be:
- destroyed by burning under official control at appropriate places, or
- moved under official control to either:
- a processing plant to be chipped and utilised within this plant, or
- an industrial plant for use as fuel wood within this plant, or
- a processing plant, where the wood shall either be:
- heat treated in such a way that a minimum wood-core temperature of 56 °C for 30 minutes has been achieved, or
- chipped and fumigated in order to ensure freedom from live PWNs;
(ii) obtained from trees other than those referred to in subparagraph (i) shall either be:
officially tested for the presence of PWN and of Monochamus spp.; if the presence is confirmed the wood shall be subjected to the provisions referred to in (i); if the presence is refuted, the wood may be moved under official control to a processing plant for further use as construction timber, or by way of derogation moved into areas in Portugal, other than demarcated areas, under official control to approved processing plants notified to the Commission, where the wood, within the period between 1 November and 1 April, shall either be:
- heat treated in such a way that a minimum wood-core temperature of 56 °C for 30 minutes has been achieved. Further movement of this heat-treated wood is allowed when the wood is accompanied by the said plant passport, or
- chipped and fumigated in order to ensure freedom from live PWNs. Further movement of this fumigated wood is allowed when it is accompanied by the said plant passport, or
- chipped and used for industrial purposes within this plant, or
- moved under official control to a plant, where the wood shall either be:
- heat treated in such a way that a minimum wood core temperature of 56 °C for 30 minutes has been achieved, or
- chipped and fumigated in order to ensure freedom from live pine wood nematodes, or
- chipped and used for industrial purposes;
(c) during the period from 2 April to 31 October, susceptible wood in the form of roundwood or sawnwood, with or without bark, including that which has not kept its natural round surface:
(i) obtained from trees identified as infested by PWN, or situated in salvage areas, or showing any symptoms of poor health, shall either be:
- immediately destroyed by burning under official control at appropriate places, or
- immediately stripped of bark at appropriate places outside the forest before being moved under official control to storage places where the wood is treated with an appropriate insecticide or which have appropriate and approved wet storage facilities, available at least during the above period, with a view to a further movement to an industrial plant:
- to be immediately chipped and used for industrial purposes, or
- for immediate use as fuel within this plant, or
- to be immediately heatreated in such a way that a minimum wood-core temperature of 56 °C for 30 minutes has been achieved, or
- to be immediately chipped and fumigated in order to ensure freedom from live PWNs;
(ii) obtained from trees other than those referred to in subparagraph (i) shall be immediately stripped of bark at the place of felling or in the immediate vicinity and either be:
- officially tested for the presence of PWN and of Monochamus spp.; if the presence is confirmed the wood shall be subjected to the provisions referred to in (i); if the presence is refuted, the wood may be moved under official control to a processing plant for further use as construction timber, or
- moved under official control to a plant where the wood shall either be:
- chipped and used for industrial purposes, or
- heat-treated in such a way that a minimum wood-core temperature of 56 °C for 30 minutes has been achieved, or
- chipped and fumigated in order to ensure freedom from live PWNs;
(d) susceptible bark shall be;
- destroyed by burning or used as fuel at an industrial processing plant, or
- heat-treated in such a way that a minimum temperature of 56 °C for 30 minutes has been achieved throughout the bark, or
- fumigated in order to ensure freedom from live PWNs;
(e) susceptible wood in the form of waste produced at the time of felling, shall be burned at appropriate places under official control:
- during the period from 1 November to 1 April, within that period, or
- during the period from 2 April to 31 October, immediately;
(f) susceptible wood, in the form of waste produced during wood processing, shall either be immediately burned at appropriate places under official control, used as fuel wood at the processing plant or fumigated in order to ensure freedom from live PWNs;
(g) susceptible wood, in the form of packing cases, boxes, crates, drums and similar packings, pallets, box pallets and other load boards, pallet collars, dunnage, spacers and bearers, including that which has not kept its natural round surface, shall:
- be stripped of its bark,
- be free from grub holes which are larger than 3 mm across,
- have a moisture content expressed as a percentage of dry matter of less than 20 % achieved at time of manufacture.
(1) OJ L 4, 8.1.1993, p. 22.
