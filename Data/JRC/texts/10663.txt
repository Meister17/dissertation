Notice concerning a request in accordance with Article 30 of Directive 2004/17/EC — Extension of the period
Request made on behalf of a Member State
(2006/C 72/05)
On 20 February 2006 the Commission received a request in accordance with Article 30(4) of Directive 2004/17/EC of the European Parliament and of the Council of 31 March 2004 coordinating the procurement procedures of entities operating in the water, energy, transport and postal services sectors [1].
This request, which comes from the Republic of Finland, concerns the production (including combined heat and power) and sale of electricity in that country. The request was published in Official Journal of the European Union C 59 of 11.3.2006. The initial period expires on 21 May 2006.
Given that the Commission departments need to obtain and examine further information and in compliance with the provisions laid down in the third sentence of Article 30(6), the period within which the Commission must take a decision on this request is extended by one month.
The final period will therefore expire on 22 June 2006.
[1] OJ L 134, 30.4.2004, p. 1.
--------------------------------------------------
