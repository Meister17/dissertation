Council Decision
of 8 December 2003
extending the effects of Decision 2003/861/EC concerning analysis and cooperation with regard to counterfeit euro coins to those Member States which have not adopted the euro as their single currency
(2003/862/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 308 thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament(1),
Whereas:
(1) In adopting Decision of 2003/861/EC concerning analysis and cooperation with regard to counterfeit euro coins the Council provided that it is to have effect in those Member States which have adopted the euro as their single currency.
(2) It is important that the euro should enjoy the same level of protection in those Member States which have not adopted it and the necessary provisions should be taken to that end,
HAS ADOPTED THIS DECISION:
Article 1
Council Decision 2003/861/EC concerning analysis and cooperation with regard to counterfeit euro coins shall be extended to those Member States which have not adopted the euro as their single currency.
Article 2
This Decision is addressed to those Member States which have not adopted the euro as their single currency.
Done at Brussels, 8 December 2003.
For the Council
The President
F. Frattini
(1) Opinion delivered on 18 November 2003 (not yet published in the Official Journal).
