Commission Regulation (EC) No 2153/2002
of 3 December 2002
amending Regulation (EC) No 1599/97 laying down detailed rules for the application of the system of minimum import prices for certain soft fruits originating in Bulgaria, Hungary, Poland, Romania, Slovakia, the Czech Republic, Estonia, Latvia and Lithuania
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 2290/2000 of 9 October 2000 establishing certain concessions in the form of Community tariff quotas for certain agricultural products and providing for an adjustment, as an autonomous and transitional measure, of certain agricultural concessions provided for in the Europe Agreement with Bulgaria(1), and in particular Article 1(3) thereof, and the corresponding provisions of the Council Regulations and Decisions relating to the Baltic states and the other central and eastern European countries concerned,
Whereas:
(1) At the close of the recent trade negotiations between the Community, on the one hand, and Latvia and Lithuania on the other, the system of minimum import prices for imports into the Community of certain soft fruits originating in these third countries for processing were amended. The new provisions of this system are set out in the appendix to Annex C(b) to Council Regulation (EC) No 1361/2002(2) for Lithuania and the appendix to Annex C(b) to Council Regulation (EC) No 1362/2002(3) for Latvia.
(2) For reasons of clarity and legal certainty, the minimum import prices currently set out in the Annex to Commission Regulation (EC) No 1599/97(4), as last amended by Regulation (EC) No 538/2000(5), should no longer be shown in that Annex. It is, however, necessary to indicate the Community legislation under which these prices are fixed. Depending on the third country in question, this may be a Council regulation establishing concessions in the form of Community tariff quotas for certain agricultural products and providing for an adjustment, as an autonomous and transitional measure, of certain agricultural concessions provided for in the Europe Agreement with the country concerned, or the relevant provisions of the Europe Agreement with that country.
(3) Regulation (EC) No 1599/97 should therefore be amended accordingly.
(4) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Products Processed from Fruit and Vegetables,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 1599/97 is hereby amended as follows:
1. The second sentence of Article 1 is replaced by the following:"Each declaration may cover only goods of one and the same origin falling within a single combined nomenclature code and, in the case of frozen products, a single Taric code as shown in Annex I."
2. Article 3 is replaced by the following:
"Article 3
1. For each lot and each origin concerned, during the completion of the customs import formalities with a view to release for free circulation, the competent authorities shall make a comparison of the value shown in the customs declaration and the minimum import price shown, for the product in question, in the Community legislation, as referred to in Annex II, applicable to the imports in question.
2. Where the value shown in the customs declaration is below the applicable minimum price referred to in paragraph 1, a countervailing charge shall be levied equal to the difference between that value and the minimum price."
3. Article 5(1) is replaced by the following:
"1. For the products listed in Annex I hereto, Member States shall communicate to the Commission the quantities put into free circulation and their values, broken down by origin and CN code and, for frozen products, by Taric code."
4. The Annex is replaced by the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 3 December 2002.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 262, 17.10.2000, p. 1.
(2) OJ L 198, 27.7.2002, p. 1.
(3) OJ L 198, 27.7.2002, p. 13.
(4) OJ L 216, 8.8.1997, p. 63.
(5) OJ L 65, 14.3.2000, p. 11.
ANNEX
"ANNEX I
List of products subject to the system of minimum import prices
>TABLE>
ANNEX II
Community legislation referred to in Article 3(1)
>TABLE>"
