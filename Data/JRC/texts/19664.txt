Commission Regulation (EC) No 1466/2003
of 19 August 2003
laying down the marketing standard for artichokes and amending Regulation (EC) No 963/98
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 2200/96 of 28 October 1996 on the common organisation of the market in fruit and vegetables(1), as last amended by Commission Regulation (EC) No 47/2003(2), and in particular Article 2(2) and Article 3(3)(c) thereof,
Whereas:
(1) Artichokes are among the products listed in Annex I to Regulation (EC) No 2200/96 for which standards must be adopted. Commission Regulation (EC) No 963/98 of 7 May 1998 laying down marketing standards applicable to cauliflowers and artichokes(3), as last amended by Regulation (EC) No 46/2003(4), should be amended with respect to the definition of "Poivrade" and "Bouquet" type artichokes.
(2) In the interest of clarity, the standards applicable to artichokes should be laid down in a separate Regulation and Regulation (EC) No 963/98 should be amended accordingly. To that end, and in the interest of preserving transparency on the world market, account should be taken of the standard for artichokes recommended by the Working Party on Standardisation of Perishable Produce and Quality Development of the United Nations Economic Commission for Europe (UN/ECE).
(3) Application of the new standards should remove products of unsatisfactory quality from the market, bring production into line with consumer requirements and facilitate trade based on fair competition, thereby helping to improve profitability.
(4) The standards are applicable at all marketing stages. Long-distance transport, storage over a certain period and the various processes the products undergo may cause some degree of deterioration owing to the biological development of the products or their perishable nature. Account should be taken of such deterioration when applying the standard at the marketing stages following dispatch. As products in the "Extra" class have to be particularly carefully sorted and packaged, only lack of freshness and turgidity should be taken into account in their case.
(5) Certain varieties of artichoke produced in the Italian regions of Sicilia, Puglia, Sardegna, Campania, Lazio and Toscana are traditionally sold in the region of production in bunches surrounded by leaves and with stems longer than 10 centimetres. At the request of Italy, this marketing practice was authorised by Commission Regulation (EC) No 448/97 of 7 March 1997 derogating, for certain regions in Italy, from the trading standards set to artichokes(5). In order to clarify and simplify Community rules, that derogation should be integrated into this Regulation and Regulation No 448/97 should be repealed.
(6) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Fresh Fruit and Vegetables,
HAS ADOPTED THIS REGULATION:
Article 1
The marketing standard for artichokes falling within CN code 0709 10 00 shall be as set out in the Annex.
The standard shall apply at all marketing stages under the conditions laid down in Regulation (EC) No 2200/96.
However, at stages following dispatch, the products may show in relation to the provisions of the standard:
(a) a slight lack of freshness and turgidity,
(b) for products graded in classes other than the "Extra" class, slight deterioration due to their development and their tendency to perish.
Article 2
1. By way of derogation from the Annex, artichokes produced in the Italian regions of Sicilia, Puglia, Sardegna, Campania, Lazio and Toscana may be sold by the retail trade within these regions in bunches surrounded by leaves and with a stem longer than 10 cm.
2. For the purposes of applying paragraph 1, each consignment shall carry, in addition to the other required information, the following indication on the document or notice referred to in Article 5(2) of Regulation (EC) No 2200/96:
"Destinato alla vendita al dettaglio unicamente in... (region of production)"
Article 3
Regulation (EC) No 963/98 is amended as follows:
1. In the title, the words "and artichokes" are deleted.
2. Article 1(1) is replaced by the following:
"1. The marketing standard for cauliflowers falling within CN code 0704 10 shall be as set out in the Annex."
3. Annex II is deleted.
4. In Annex I, the words "Annex I" are replaced by the word "Annex".
Article 4
Regulation (EC) No 448/97 is repealed.
Article 5
This Regulation shall enter into force on the 20th day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 19 August 2003.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 297, 21.11.1996, p. 1.
(2) OJ L 7, 11.1.2003, p. 64.
(3) OJ L 135, 8.5.1998, p. 18.
(4) OJ L 7, 11.1.2003, p. 61.
(5) OJ L 68, 8.3.1997, p. 17.
ANNEX
STANDARD FOR ARTICHOKES
I. DEFINITION OF PRODUCE
This standard applies to artichoke heads of varieties (cultivars) grown from Cynara scolymus L. to be supplied fresh to the consumer, artichokes for industrial processing being excluded.
The names "Poivrade" and "Bouquet" refer to young cone-shaped artichokes of the violet type.
II. PROVISIONS CONCERNING QUALITY
The purpose of the standard is to define the quality requirements for artichokes, after preparation and packaging.
A. Minimum requirements
In all classes, subject to the special provisions for each class and the tolerances allowed, the artichokes must be:
- intact,
- sound; produce affected by rotting or deterioration such as to make it unfit for consumption is excluded,
- clean, practically free from any visible foreign matter,
- fresh in appearance, and in particular showing no sign of withering,
- practically free from pests,
- practically free from damage caused by pests,
- free of abnormal external moisture,
- free of any foreign smell and/or taste.
The stems must be cut off cleanly and must not be longer than 10 cm. This latter provision is not applicable to artichokes packed in bunches, that is: made up of a certain number of heads fastened together around the stems, or to artichokes of the variety "Spinoso".
The development and condition of the artichokes must be such as to enable them:
- to withstand transport and handling, and
- to arrive in satisfactory condition at the place of destination.
B. Classification
Artichokes are classified in three classes defined below:
(i) "Extra" Class
Artichokes in this class must be of superior quality. They must be characteristic of the variety and/or commercial type. The central bracts must be well closed, in accordance with the variety.
They must be free from defects with the exception of very slight superficial defects of the skin of the bracts provided these do not affect the general appearance of the produce, its quality, keeping quality and presentation in the package.
The ducts in the base must show no incipient woodiness.
(ii) Class I
Artichokes in this class must be of good quality. They must be characteristic of the variety and/or commercial type. The central bracts must be well closed, in accordance with the variety.
The following slight defects, however, may be allowed provided these do not affect the general appearance of the produce, its quality, keeping quality and presentation in the package:
- a slight defect in shape,
- a slight deterioration due to frost (cracks),
- very slight bruising.
The ducts in the base must show no incipient woodiness.
(iii) Class II
This class includes artichokes which do not qualify for inclusion in the higher classes, but satisfy the minimum requirements specified above. They may be slightly open.
The following defects may be allowed provided the artichokes retain their essential characteristics as regards the quality, the keeping quality and presentation:
- defects in shape,
- deterioration due to frost ("nipped" artichokes),
- slight bruising,
- slight staining on the outer bracts,
- incipient woodiness of the ducts in the base.
III. PROVISIONS CONCERNING SIZING
Size is determined by the maximum diameter of the equatorial section of the head.
The minimum diameter is fixed at 6 cm.
The scale given below is compulsory for the "Extra" Class and Class I and optional for Class II:
- diameter of 13 cm and over,
- diameter from 11 cm up to but excluding 13 cm,
- diameter from 9 cm up to but excluding 11 cm,
- diameter from 7,5 cm up to but excluding 9 cm,
- diameter from 6 cm up to but excluding 7,5 cm.
In addition, a diameter from 3,5 cm up to but excluding 6 cm is allowed for artichokes of the "Poivrade" type and the "Bouquet" type.
IV. PROVISIONS CONCERNING TOLERANCES
Tolerances in respect of quality and size shall be allowed in each package for produce not satisfying the requirements of the class indicated.
A. Quality tolerances
(i) "Extra" Class
5 % by number of artichokes not satisfying the requirements of the class, but meeting those of Class I or, exceptionally, coming within the tolerances of that class.
(ii) Class I
10 % by number of artichokes not satisfying the requirements of the class, but meeting those of Class II or, exceptionally, coming within the tolerances of that class.
(iii) Class II
10 % by number of artichokes satisfying neither the requirements of the class, nor the minimum requirements, with the exception of produce affected by rotting or any other deterioration rendering it unfit for consumption.
B. Size tolerances
For all classes (if sized): 10 % by number of artichokes not satisfying the requirements as regards sizing and the size indicated, but conforming to the size immediately above and/or below that specified, with a minimum of 5 cm in diameter for artichokes classified in the smallest size (6 to 7,5 cm).
No size tolerance is allowed for artichokes of the "Poivrade" type or "Bouquet" type.
V. PROVISIONS CONCERNING PRESENTATION
A. Uniformity
The contents of each package must be uniform and contain only artichokes of the same origin, variety or commercial type, quality and size (if sized).
The visible part of the contents of the package must be representative of the entire contents.
Notwithstanding the preceding provisions in this point, products covered by this Regulation may be mixed, in sales packages of a net weight of three kilograms or less, with different types of fresh fruit and vegetables on the conditions laid down by Commission Regulation (EC) No 48/2003(1).
B. Packaging
The artichokes must be packed in such a way as to protect the produce properly.
The materials used inside the package must be new, clean and of a quality such as to avoid causing any external or internal damage to the produce. The use of materials, particularly paper or stamps, bearing trade specifications is allowed provided the printing or labelling has been done with non-toxic ink or glue.
Packages must be free from all foreign matter.
VI. PROVISIONS CONCERNING MARKING
Each package must bear the following particulars in letters grouped on the same side, legibly and indelibly marked, and visible from the outside:
A. Identification
Packer and/or dispatcher: name and address or officially issued or accepted code mark. However, in the case where a code mark is used, the reference "Packer and/or dispatcher (or equivalent abbreviations)" must be indicated in close proximity to the code mark.
B. Nature of produce
- "Artichokes", if the contents are not visible from the outside,
- Name of the variety for the "Extra" Class,
- "Poivrade" or "Bouquet", where appropriate,
- "Spinoso", where appropriate.
C. Origin of produce
Country of origin and, optionally, district where grown or national, regional or local place name.
D. Commercial specifications
- Class,
- Number of heads,
- Size (if sized) expressed in minimum and maximum diameters of the heads.
E. Official control mark (optional)
(1) OJ L 7, 11.1.2003, p. 65.
