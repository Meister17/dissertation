*****
COMMISSION REGULATION (EEC) No 84/83
of 14 January 1983
amending Regulation (EEC) No 1391/78 laying down amended rules for the application of the system of premiums for the non-marketing of milk and milk products and for the conversion of dairy herds
THE COMMISSION OF THE EUROPEAN
COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 1078/77 of 17 May 1977 introducing a system of premiums for the non-marketing of milk and milk products and for the conversion of dairy herds (1), as last amended by Regulation (EEC) No 1365/80 (2), and in particular Article 7 thereof,
Whereas Article 12 (4) of Commission Regulation (EEC) No 1391/78 (3), as last amended by Regulation (EEC) No 2735/80 (4), makes provision for a reduction of the premium in the event of failure to fulfil the undertaking referred to in Article 3 (2) (c) of Regulation (EEC) No 1078/77 during the fourth year of the conversion period; whereas experience has shown that it is necessary to extend this provision to the entire conversion period;
Whereas it is also necessary to specify the powers of the competent authorities in the event of notification not being given or being given late;
Whereas experience in applying Commission Regulations (EEC) No 1307/77 (5) and (EEC) No 1391/78 has shown that it is necessary to relax the provisions relating to the presentation of the original of the identity card in the event of the animal being exported to another Member State;
Whereas the Management Committee for Milk and Milk Products has not delivered an opinion within the time limit set by its chairman,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EEC) No 1391/78 is hereby amended as follows:
1. The following subparagraph is added to Article 5 (3):
'Where such notification is not given or is given late, the competent authority shall check whether the producer has complied with the condition laid down in the last subparagraph of Article 2 (2) and in the second subparagraph of Article 3 (2) of Regulation (EEC) No 1078/77.'
2. The following paragraph 6 is added to Article 8:
'6. If, three months after transfer, the identity card cannot be submitted or cannot be submitted duly completed, the Member State may, notwithstanding paragraph 4, accept any other document presenting equivalent guarantees as proof of slaughter, death or export.'
3. The following subparagraph is inserted after the first subparagraph of Article 9 (4):
'Where such notification is not given or is given late, the competent authority shall check whether the transferee is to take over the obligations under the premium scheme.'
4. Article 12 (4) is replaced by the following:
'4. Notwithstanding Article 9 (1), in a case which cannot be deemed a case of force majeure and in which the beneficiary of the conversion premium no longer fulfils, during the conversion period, the undertaking referred to in Article 3 (2) (c) of Regulation (EEC) No 1078/77, the amount of the premium to be recovered or, if the balance has still not been paid to him, the amount to be withheld for each year in respect of which the aforementioned undertaking was not fulfilled, shall be equal to 25 % of the total amount of the premium to which he would have been entitled, this percentage being reduced for each year in proportion to the difference between the number of livestock units kept and the required number of livestock units.'
Article 2
Article 1 (1) and (3) and, at the request of the party concerned, Article 1 (2) and (4) shall apply to aid applications submitted from 1 July 1977 either pursuant to Article 1 of Regulation (EEC) No 1307/77 or pursuant to Article 4 (1) of Regulation (EEC) No 1391/78.
Article 3
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 14 January 1983.
For the Commission
Poul DALSAGER
Member of the Commission
(1) OJ No L 131, 26. 5. 1977, p. 1.
(2) OJ No L 140, 5. 6. 1980, p. 18.
(3) OJ No L 167, 24. 6. 1978, p. 45.
(4) OJ No L 283, 28. 10. 1980, p. 5.
(5) OJ No L 150, 18. 6. 1977, p. 24.
