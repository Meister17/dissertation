COMMISSION REGULATION (EC) No 2680/1999
of 17 December 1999
approving a system of identification for bulls intended for cultural and sporting events
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 820/97 of 21 April 1997 establishing a system for the identification and registration of bovine animals and the labelling of beef and beef products(1), and in particular Article 4(1) thereof,
Having regard to requests submitted by Spain, Portugal and France,
(1) Whereas Spain, Portugal and France have submitted requests regarding the identification of bulls intended for cultural and sporting events, due to difficulties linked to tradition;
(2) Whereas it is justified to take into account those requests, provided that the system to be foreseen offers equivalent guarantees to the ones foreseen by Regulation (EC) No 820/97; whereas the special provisions to be made for bulls intended for cultural and sporting events should be limited only to eartags; whereas the other elements of the identification system as provided for by Article 3 of Regulation (EC) No 820/97 should be implemented according to the current Community legislation;
(3) Whereas for bulls intended for cultural and sporting events the competent authority should choose one of the following means of marking: (a) two plastic eartags, (b) one or two metallic eartags together with brand marking, or (c) one plastic eartag together with brand marking; whereas in any case the two eartags foreseen by the current Community legislation should be attached to those animals or should acompany them when they are subject to intra-Community trade;
(4) Whereas the eartags may be removed from animals destined to such events prior to movement to the place where such an event is organised or at weaning; whereas, when both eartags are removed at weaning, the animals should be brand-marked at the same time;
(5) Whereas appropriate links among all the identification elements used should be established in order to guarantee coherence and accuracy;
(6) Whereas as bulls intended for cultural and sporting events may be reared in other Member States too, these special provisions should apply to all Member States,
HAS ADOPTED THIS REGULATION:
Article 1
The present Regulation refers to bulls intended for cultural and sporting events registered to the herd-books of the following organisations:
(a) - "Asociación nacional de ganaderías de Lidia",
- "Asociación de ganaderos de Lidia unidos",
- "Agrupación española de reses bravas",
- "Unión de criadores de toros de Lidia",
regarding the breed raza bovina de Lidia, for animals born in Spain;
(b) "Associação de Criadores de Toiros de Lide regarding the breed Brava for animals born in Portugal";
(c) "Association des éleveurs Français de taureaux de combat" regarding the breed Brave or de combat and "Association des éleveurs de la Raço di biou" regarding the breed Camargue or Raço di biou, for animals born in France (including any crossbreeds of these).
Article 2
1. The animals defined in Article 1 shall be subject to the special provisions set out in paragraphs 2 to 5.
2. The competent authority shall choose one of the following means of marking the animals:
- two plastic eartags;
- one or two metallic eartags together with brand marking;
- one plastic eartag together with brand marking;
3. The competent authority may remove the eartags from animals destined to such events prior to movement to the place where such an event is organised or at weaning. When both eartags are removed at weaning, the animals shall be brand-marked at the same time.
4. In any case, the keeper of such animals shall be in possession of the two eartags complying with the current Community legislation. When such animals are subject to intra-Community trade, those two eartags shall be attached to them or shall accompany them during any movement.
5. Appropriate links among all the identification elements used shall be established in order to guarantee coherence and accuracy.
Article 3
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 17 December 1999.
For the Commission
David BYRNE
Member of the Commission
(1) OJ L 117, 7.5.1997, p. 1.
