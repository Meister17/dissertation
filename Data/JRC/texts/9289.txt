Commission Regulation (EC) No 557/2006
of 5 April 2006
fixing the A1 and B export refunds for fruit and vegetables (tomatoes, oranges, lemons and apples)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 2200/96 of 28 October 1996 on the common organisation of the market in fruit and vegetables [1], and in particular the third subparagraph of Article 35(3),
Whereas:
(1) Commission Regulation (EC) No 1961/2001 [2] lays down the detailed rules of application for export refunds on fruit and vegetables.
(2) Article 35(1) of Regulation (EC) No 2200/96 provides that, to the extent necessary for economically significant exports, the products exported by the Community may be covered by export refunds, within the limits resulting from agreements concluded in accordance with Article 300 of the Treaty.
(3) Under Article 35(2) of Regulation (EC) No 2200/96, care must be taken to ensure that the trade flows previously brought about by the refund scheme are not disrupted. For this reason and because exports of fruit and vegetables are seasonal in nature, the quantities scheduled for each product should be fixed, based on the agricultural product nomenclature for export refunds established by Commission Regulation (EEC) No 3846/87 [3]. These quantities must be allocated taking account of the perishability of the products concerned.
(4) Article 35(4) of Regulation (EC) No 2200/96 provides that refunds must be fixed in the light of the existing situation or outlook for fruit and vegetable prices on the Community market and supplies available on the one hand, and prices on the international market on the other hand. Account must also be taken of the transport and marketing costs and of the economic aspect of the exports planned.
(5) In accordance with Article 35(5) of Regulation (EC) No 2200/96, prices on the Community market are to be established in the light of the most favourable prices from the export standpoint.
(6) The international trade situation or the special requirements of certain markets may call for the refund on a given product to vary according to its destination.
(7) Tomatoes, oranges, lemons and apples of classes Extra, I and II of the common quality standards can currently be exported in economically significant quantities.
(8) In order to ensure the best use of available resources and in view of the structure of Community exports, it is appropriate to fix the A1 and B export refunds.
(9) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for fresh Fruit and Vegetables,
HAS ADOPTED THIS REGULATION:
Article 1
1. For system A1, the refund rates, the refund application period and the scheduled quantities for the products concerned are fixed in the Annex hereto. For system B, the indicative refund rates, the licence application period and the scheduled quantities for the products concerned are fixed in the Annex hereto.
2. The licences issued in respect of food aid as referred to in Article 16 of Commission Regulation (EC) No 1291/2000 [4] shall not count against the eligible quantities in the Annex hereto.
Article 2
This Regulation shall enter into force on 9 May 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 5 April 2006.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 297, 21.11.1996, p. 1. Regulation as last amended by Commission Regulation (EC) No 47/2003 (OJ L 7, 11.1.2003, p. 64).
[2] OJ L 268, 9.10.2001, p. 8. Regulation as last amended by Regulation (EC) No 386/2005 (OJ L 62, 9.3.2005, p. 3).
[3] OJ L 366, 24.12.1987, p. 1. Regulation as last amended by Regulation (EC) No 2091/2005 (OJ L 343, 24.12.2005, p. 1).
[4] OJ L 152, 24.6.2000, p. 1. Regulation as last amended by Regulation (EC) No 1856/2005 (OJ L 297, 15.11.2005, p. 7).
--------------------------------------------------
ANNEX
to the Commission Regulation of 5 April 2006 fixing the export refunds on fruit and vegetables (tomatoes, oranges, lemons and apples)
Product code [1] | Destination [2] | System A1 Refund application period 9.5.2006 to 23.6.2006 | System B Licence application period 16.5.2006 to 30.6.2006 |
Refund amount (EUR/t net weight) | Scheduled quantiy (t) | Indicative refund amount (EUR/t net weight) | Scheduled quantity (t) |
0702 00 00 9100 | F08 | 30 | | 30 | 12476 |
0805 10 20 9100 | A00 | 39 | | 39 | 19378 |
0805 50 10 9100 | A00 | 60 | | 60 | 3333 |
0808 10 80 9100 | F09 | 33 | | 33 | 38080 |
[1] The product codes are set out in Commission Regulation (EEC) No 3846/87 (OJ L 366, 24.12.1987, p. 1), as amended.
[2]
- The following destinations:Norway, Iceland, Greenland, Faeroe Islands, Romania, Albania, Bosnia and Herzegovina, Croatia, former Yugoslav Republic of Macedonia, Serbia and Montenegro (including Kosovo, as defined in UN Security Council Resolution 1244 of 10 June 1999), Armenia, Azerbaijan, Belarus, Georgia, Kazakhstan, Kyrgyzstan, Moldova, Russia, Tajikistan, Turkmenistan, Uzbekistan, Ukraine, Saudi Arabia, Bahrain, Qatar, Oman, United Arab Emirates (Abu Dhabi, Dubai, Sharjah, Ajman, Umm al Qalwain, Ras al Khaimah, Fujairah), Kuwait, Yemen, Syria, Iran, Jordan, Bolivia, Brazil, Venezuela, Peru, Panama, Ecuador and Colombia,African countries and territories except for South Africa,destinations referred to in Article 36 of Commission Regulation (EC) No 800/1999 (OJ L 102, 17.4.1999, p. 11).
--------------------------------------------------
