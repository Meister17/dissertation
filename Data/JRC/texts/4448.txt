Decision of the EEA Joint Committee
No 153/2004
of 29 October 2004
amending Annex XIII (Transport) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex XIII to the Agreement was amended by Decision of the EEA Joint Committee No 129/2004 of 24 September 2004 [1].
(2) Regulation (EC) No 789/2004 of the European Parliament and of the Council of 21 April 2004 on the transfer of cargo and passenger ships between registers within the Community and repealing Council Regulation (EEC) No 613/91 [2], is to be incorporated into the Agreement,
(3) Regulation (EC) No 789/2004 repeals Council Regulation (EEC) No 613/91 [3], which is incorporated into the Agreement and which is consequently to be deleted from the Agreement.
HAS DECIDED AS FOLLOWS:
Article 1
Annex XIII to the Agreement shall be amended as follows:
1. The following point shall be inserted after point 56p (Regulation (EC) No 782/2003 of the European Parliament and of the Council) of Annex XIII to the Agreement:
"56q. 32004 R 0789: Regulation (EC) No 789/2004 of the European Parliament and of the Council of 21 April 2004 on the transfer of cargo and passenger ships between registers within the Community and repealing Council Regulation (EEC) No 613/91 (OJ L 138, 30.4.2004, p. 19)."
2. The text of point 56 (Council Regulation (EEC) No 613/91) shall be deleted.
Article 2
The texts of Regulation (EC) No 789/2004 in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 30 October 2004, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [4].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 29 October 2004.
For the EEA Joint Committee
The President
Kjartan Jóhannsson
[1] OJ L 64, 10.3.2005, p. 55.
[2] OJ L 138, 30.4.2004, p. 19.
[3] OJ L 68, 15.3.1991, p. 1.
[4] No constitutional requirements indicated.
--------------------------------------------------
