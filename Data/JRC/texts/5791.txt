Final adoption
of amending budget No 3 of the European Union for the financial year 2005
(2005/699/EC, Euratom)
THE PRESIDENT OF THE EUROPEAN PARLIAMENT,
Having regard to the Treaty establishing the European Community, and in particular Article 272(4), penultimate subparagraph, thereof,
Having regard to the Treaty establishing the European Atomic Energy Community, and in particular Article 177 thereof,
Having regard to Council Regulation (EC, Euratom) No 1605/2002 of 25 June 2002 on the Financial Regulation applicable to the general budget of the European Communities [1], and in particular Articles 37 and 38 thereof,
Having regard to the general budget of the European Union for the financial year 2005 finally adopted on 16 December 2004 [2],
Having regard to the Interinstitutional Agreement of 6 May 1999 between the European Parliament, the Council and the Commission on budgetary discipline and improvement of the budgetary procedure [3],
Having regard to the decision of the European Parliament and the Council of 7 September 2005 on the mobilisation of the flexibility instrument according to point 24 of the abovementioned Interinstitutional Agreement of 6 May 1999 in favour of the rehabilitation and reconstruction assistance of the Tsunami-affected countries,
Having regard to Preliminary draft amending budget No 3/2005 of the European Union for the financial year 2005 presented by the Commission on 27 April 2005,
Having regard to Draft amending budget No 4/2005 of the European Union for the financial year 2005 established by the Council on 15 July 2005,
Having regard to Rule 69 of and Annex IV to the Rules of Procedure of the European Parliament,
Having regard to the resolution adopted by the European Parliament on 7 September 2005,
The procedure laid down in Article 272 of the Treaty establishing the European Community and Article 177 of the Treaty establishing the European Atomic Energy Community having thus been completed,
DECLARES:
Sole article
Amending budget No 3 of the European Union for the financial year 2005 has been finally adopted.
Done at Strasbourg, 7 September 2005.
The President
J. Borrell Fontelles
[1] OJ L 248, 16.9.2002, p. 1.
[2] OJ L 60, 8.3.2005, p. 1.
[3] OJ C 172, 18.6.1999, p. 1. Agreement amended by Decision 2003/429/EC of the European Parliament and of the Council (OJ L 147,14.6.2003, p. 25).
--------------------------------------------------
FINAL ADOPTION OF AMENDING BUDGET No 3 OF THE EUROPEAN UNION FOR THE 2005 FINANCIAL YEAR
CONTENTS
— Expenditure
— Title 05: Agriculture and rural development
— Title 19: External relations
--------------------------------------------------
SECTION III
COMMISSION
EXPENDITURE
Title | Heading | Appropriations 2005 | Amending budget No. 3 | New amount |
Commitments | Payments | Commitments | Payments | Commitments | Payments |
01 | ECONOMIC AND FINANCIAL AFFAIRS | 452732509 | 462854009 | | | 452732509 | 462854009 |
02 | ENTERPRISE | 393303419 | 399288419 | | | 393303419 | 399288419 |
03 | COMPETITION | 88839252 | 88839252 | | | 88839252 | 88839252 |
04 | EMPLOYMENT AND SOCIAL AFFAIRS | 11577354556 | 9058458825 | | | 11577354556 | 9058458825 |
05 | AGRICULTURE AND RURAL DEVELOPMENT | 53722123633 | 52484803811 | | | 53722123633 | 52484803811 |
06 | ENERGY AND TRANSPORT | 1413397334 | 1346158134 | | | 1413397334 | 1346158134 |
07 | ENVIRONMENT | 322320776 | 319290776 | | | 322320776 | 319290776 |
08 | RESEARCH | 3299731056 | 2525607306 | | | 3299731056 | 2525607306 |
09 | INFORMATION SOCIETY | 1335651319 | 1181111319 | | | 1335651319 | 1181111319 |
10 | DIRECT RESEARCH | 366422464 | 348310914 | | | 366422464 | 348310914 |
11 | FISHERIES | 1029744589 | 927155514 | | | 1029744589 | 927155514 |
12 | INTERNAL MARKET | 73349263 | 72749263 | | | 73349263 | 72749263 |
13 | REGIONAL POLICY | 27103707247 | 20916865535 | | | 27103707247 | 20916865535 |
14 | TAXATION AND CUSTOMS UNION | 119785688 | 114301688 | | | 119785688 | 114301688 |
15 | EDUCATION AND CULTURE | 941251284 | 869019404 | | | 941251284 | 869019404 |
16 | PRESS AND COMMUNICATION | 185012786 | 176001686 | | | 185012786 | 176001686 |
17 | HEALTH AND CONSUMER PROTECTION | 513511715 | 516164510 | | | 513511715 | 516164510 |
18 | AREA OF FREEDOM, SECURITY AND JUSTICE | 578452580 | 566255804 | | | 578452580 | 566255804 |
19 | EXTERNAL RELATIONS | 3061836673 | 3281150276 | 15000000 | | 3076836673 | 3281150276 |
20 | TRADE | 76234391 | 77254391 | | | 76234391 | 77254391 |
21 | DEVELOPMENT AND RELATIONS WITH AFRICAN, CARIBBEAN AND PACIFIC (ACP) STATES | 1235215936 | 1315772436 | | | 1235215936 | 1315772436 |
22 | ENLARGEMENT | 1853819158 | 2681549158 | | | 1853819158 | 2681549158 |
23 | HUMANITARIAN AID | 513098157 | 515460657 | | | 513098157 | 515460657 |
24 | FIGHT AGAINST FRAUD | 61395038 | 58235038 | | | 61395038 | 58235038 |
25 | COMMISSION'S POLICY COORDINATION AND LEGAL ADVICE | 209126692 | 207311692 | | | 209126692 | 207311692 |
26 | COMMISSION'S ADMINISTRATION | 647663022 | 647663022 | | | 647663022 | 647663022 |
27 | BUDGET | 1385620356 | 1385620356 | | | 1385620356 | 1385620356 |
28 | AUDIT | 10602470 | 10602470 | | | 10602470 | 10602470 |
29 | STATISTICS | 131296575 | 126078575 | | | 131296575 | 126078575 |
30 | PENSIONS | 899771000 | 899771000 | | | 899771000 | 899771000 |
31 | RESERVES | 557192789 | 325722789 | | | 557192789 | 325722789 |
| Expenditure D — Total | 114159563727 | 103905428029 | 15000000 | | 114174563727 | 103905428029 |
TITLE 05
AGRICULTURE AND RURAL DEVELOPMENT
Overall objectives
The objectives of the common agricultural policy (CAP) derive directly from the Treaty and are, in particular, to stabilise markets, ensure a fair standard of living in the farming community and guarantee security of supplies.
The CAP has been reformed a number of times since it was first introduced, most recently with the agreement signed in June 2003 in Luxembourg. The key aim of that reform, as part of Agenda 2000, was to shift the slant of the farm economy towards the market in order to make the agricultural sector more competitive. Reflecting the multipurpose application of agricultural activity, the CAP must also be fully compatible with sustainable development, in particular by promoting environmentally friendly production methods and the effective use of resources. Rural development, the second pillar of the CAP, aims at enhancing the competitiveness of rural areas and preserving the environment and rural heritage in order to secure the future of rural areas and promote job maintenance and creation.
In 2005, the priorities for 2004 will be pursued, in particular as regards the implementation of the reforms adopted in 2003 and 2004, the operational application of the acquis in the new member countries and the World Trade Organisation (WTO) negotiations. These priorities are in addition to the regular activities of CAP.
Title Chapter | Heading | Appropriations 2005 | Amending budget No. 3 | New amount |
Commitments | Payments | Commitments | Payments | Commitments | Payments |
05 01 | ADMINISTRATIVE EXPENDITURE OF AGRICULTURE POLICY AREA | 149945773 | 149945773 | | | 149945773 | 149945773 |
05 02 | PLANT PRODUCTS | 29134620000 | 29134620000 | | | 29134620000 | 29134620000 |
05 03 | ANIMAL PRODUCTS | 13683780000 | 13683780000 | | | 13683780000 | 13683780000 |
05 04 | RURAL DEVELOPMENT | 10771477860 | 9194072038 | | | 10771477860 | 9194072038 |
05 05 | SPECIAL ACCESSION PROGRAMME FOR AGRICULTURE AND RURAL DEVELOPMENT (SAPARD) | 248800000 | 577500000 | | | 248800000 | 577500000 |
05 06 | EXTERNAL RELATIONS | 5270000 | 5270000 | | | 5270000 | 5270000 |
05 07 | AUDIT OF AGRICULTURAL EXPENDITURE | –374085000 | –362785000 | | | –374085000 | –362785000 |
05 08 | POLICY STRATEGY AND COORDINATION OF AGRICULTURE POLICY AREA | 102315000 | 102401000 | | | 102315000 | 102401000 |
05 49 | EXPENDITURE ON ADMINISTRATIVE MANAGEMENT OF PROGRAMMES COMMITTED IN ACCORDANCE WITH THE FORMER FINANCIAL REGULATION | — | p.m. | | | — | p.m. |
| Title 05 — Total | 53722123633 | 52484803811 | | | 53722123633 | 52484803811 |
CHAPTER 05 08 —POLICY STRATEGY AND COORDINATION OF "AGRICULTURE" POLICY AREA
Title Chapter Article Item | Heading | FP | Appropriations 2005 | Amending budget No. 3 | New amount |
Commitments | Payments | Commitments | Payments | Commitments | Payments |
05 08
POLICY STRATEGY AND COORDINATION OF AGRICULTURE POLICY AREA
05 08 01 | Farm accountancy data network (FADN) | 3 | 14000000 | 14000000 | | | 14000000 | 14000000 |
| Article 05 08 01 — Subtotal | | 14000000 | 14000000 | | | 14000000 | 14000000 |
05 08 02 | Surveys on the structure of agricultural holdings | 3 | 14400000 | 15500000 | | | 14400000 | 15500000 |
| Article 05 08 02 — Subtotal | | 14400000 | 15500000 | | | 14400000 | 15500000 |
05 08 03 | Restructuring of systems for agricultural surveys | 3 | 7615000 | 6601000 | | | 7615000 | 6601000 |
| Article 05 08 03 — Subtotal | | 7615000 | 6601000 | | | 7615000 | 6601000 |
05 08 04
Promotion measures — Payments by Member States
05 08 04 01 | Promotion measures — Payments by Member States | 1.1 | 48500000 | 48500000 | | | 48500000 | 48500000 |
| Article 05 08 04 — Subtotal | | 48500000 | 48500000 | | | 48500000 | 48500000 |
05 08 05
Promotion measures — Direct payments by the European Community
05 08 05 01 | Promotion measures — Direct payments by the European Community | 1.1 | 11000000 | 11000000 | | | 11000000 | 11000000 |
| Article 05 08 05 — Subtotal | | 11000000 | 11000000 | | | 11000000 | 11000000 |
05 08 06 | Enhancing public awareness of the common agricultural policy | 1.1 | 6500000 | 6500000 | | | 6500000 | 6500000 |
| Article 05 08 06 — Subtotal | | 6500000 | 6500000 | | | 6500000 | 6500000 |
05 08 07 | Completion of earlier measures in the field of information | 3 | p.m. | p.m. | | | p.m. | p.m. |
| Article 05 08 07 — Subtotal | | p.m. | p.m. | | | p.m. | p.m. |
05 08 08 | External impact of Directive 2000/36/EC | 3 | 300000 | 300000 | | | 300000 | 300000 |
| Article 05 08 08 — Subtotal | | 300000 | 300000 | | | 300000 | 300000 |
05 08 99 | Other | 1.1 | p.m. | p.m. | | | p.m. | p.m. |
| Article 05 08 99 — Subtotal | | p.m. | p.m. | | | p.m. | p.m. |
| Chapter 05 08 — Total | | 102315000 | 102401000 | | | 102315000 | 102401000 |
05 08 03Restructuring of systems for agricultural surveys
Appropriations 2005 | Amending budget No. 3 | New amount |
Commitments | Payments | Commitments | Payments | Commitments | Payments |
7615000 | 6601000 | | | 7615000 | 6601000 |
The likely schedule of payments vis-à-vis commitments is as follows:
Commitments | Payments |
2004 | 2005 | 2006 | 2007 | Subsequent years and others |
Pre-2004 commitments still outstanding | 4156497 | 637697 | | | | |
Commitment appropriations made available again and/or carried over from 2003 | | | | | | |
Appropriations 2004 | 100504810 | 3046000 | 997800 | | | |
Appropriations 2005 | 7615000 | 2917303 | 3915000 | 782697 | | |
Total | 112276307 | 6601000 | 4912800 | 782697 | | |
Remarks
This appropriation is intended to cover:
- expenditure on the improvement of systems of agricultural statistics in the Community,
- subsidies, contractual expenditure and expenditure involving repayments for services connected with the carrying-out of enquiries and statistical or economic studies in the fields of agriculture, agrienvironment and rural development,
- subsidies, contractual expenditure and expenditure involving repayments for services connected with the purchasing and the consulting of databases,
- subsidies, contractual expenditure and expenditure involving repayments for services connected with agricultural sector modelling and short- and medium-term forecasts of market and agricultural structure trends, and with the dissemination of results,
- subsidies, contractual expenditure and expenditure involving repayment for services connected with the carrying out of economic analysis and development of indicators in the field of agriculture policy,
- subsidies, contractual expenditure and expenditure involving repayments for services connected with the implementation of operations for applying remote sensing, area sampling and agrometeorological models to agricultural statistics.
Legal basis
Preparatory actions within the meaning of Article 49(2) of Council Regulation (EC, Euratom) No 1605/2002 of 25 June 2002 on the Financial Regulation applicable to the general budget of the European Communities (OJ L 248, 16.9.2002, p. 1).
Tasks arising from the prerogatives of the Commission at the institutional level, under the terms of Article 49 (2) c), from the regulation (EC, Euratom) no 1605/2002 of the Council at its meeting on 25 June 2002 on financial regulation applicable to the general budget of the European Communities (OJ L 248, 16.9.2002, p. 1).
Council Decision 96/411/EC of 25 June 1996 on improving Community agricultural statistics (OJ L 162, 1.7.1996, p. 14), as last amended by Decision No 787/2004/EC of the European Parliament and of the Council (OJ L 138, 30.4.2004, p. 12).
Decision No 1445/2000/EC of the European Parliament and of the Council of 22 May 2000 on the application of aerial-survey and remote-sensing techniques to the agricultural statistics for 1999 to 2003 (OJ L 163, 4.7.2000, p. 1), as last amended by Decision No 786/2004/EC (OJ L 138, 30.4.2004, p. 7).
TITLE 19
EXTERNAL RELATIONS
Overall objectives
The policy area External Relations seeks to support the objectives of the EU external policy by means of cooperation, development aid, conflict prevention and human rights programmes and projects. These objectives include, alongside development cooperation, promotion of the EU's identity on the international stage, notably through implementation of the common foreign and security policy.
Title Chapter | Heading | Appropriations 2005 | Amending budget No. 3 | New amount |
Commitments | Payments | Commitments | Payments | Commitments | Payments |
19 01 | ADMINISTRATIVE EXPENDITURE OF EXTERNAL RELATIONS POLICY AREA | 384013173 | 384013173 | | | 384013173 | 384013173 |
19 02 | MULTILATERAL RELATIONS AND GENERAL EXTERNAL RELATIONS MATTERS | 98573500 | 88048500 | | | 98573500 | 88048500 |
19 03 | COMMON FOREIGN AND SECURITY POLICY | 62200000 | 53600000 | | | 62200000 | 53600000 |
19 04 | EUROPEAN INITIATIVE FOR DEMOCRACY AND HUMAN RIGHTS (EIDHR) | 1767000 | 139717000 | | | 1767000 | 139717000 |
19 05 | RELATIONS WITH NON-EU OECD COUNTRIES | 16000000 | 17000000 | | | 16000000 | 17000000 |
19 06 | RELATIONS WITH THE EASTERN EUROPE, THE CAUCASUS AND CENTRAL ASIAN REPUBLICS | 483580000 | 563650000 | | | 483580000 | 563650000 |
19 08 | RELATIONS WITH THE MIDDLE EAST AND SOUTHERN MEDITERRANEAN | 1047673000 | 921298353 | | | 1047673000 | 921298353 |
19 09 | RELATIONS WITH LATIN AMERICA | 310625000 | 442050000 | | | 310625000 | 442050000 |
19 10 | RELATIONS WITH ASIA | 634000000 | 623150000 | 15000000 | | 649000000 | 623150000 |
19 11 | POLICY STRATEGY AND COORDINATION FOR EXTERNAL RELATIONS POLICY AREA | 23405000 | 20430000 | | | 23405000 | 20430000 |
19 49 | EXPENDITURE ON ADMINISTRATIVE MANAGEMENT OF PROGRAMMES COMMITTED IN ACCORDANCE WITH THE FORMER FINANCIAL REGULATION | — | 28193250 | | | — | 28193250 |
| Title 19 — Total | 3061836673 | 3281150276 | 15000000 | | 3076836673 | 3281150276 |
CHAPTER 19 10 —RELATIONS WITH ASIA
Title Chapter Article Item | Heading | FP | Appropriations 2005 | Amending budget No. 3 | New amount |
Commitments | Payments | Commitments | Payments | Commitments | Payments |
19 10
RELATIONS WITH ASIA
19 10 01 | Financial and technical cooperation with Asian developing countries | 4 | 327000000 | 320000000 | | | 327000000 | 320000000 |
| Article 19 10 01 — Subtotal | | 327000000 | 320000000 | | | 327000000 | 320000000 |
19 10 02 | Political, economic and cultural cooperation with Asian developing countries | 4 | 98000000 | 126500000 | | | 98000000 | 126500000 |
| Article 19 10 02 — Subtotal | | 98000000 | 126500000 | | | 98000000 | 126500000 |
19 10 03 | Aid to uprooted people in Asian countries | 4 | 26000000 | 10350000 | | | 26000000 | 10350000 |
| Article 19 10 03 — Subtotal | | 26000000 | 10350000 | | | 26000000 | 10350000 |
19 10 04 | Rehabilitation and reconstruction operations in developing countries in Asia | 4 | p.m. | 2300000 | 15000000 | | 15000000 | 2300000 |
| Article 19 10 04 — Subtotal | | p.m. | 2300000 | 15000000 | | 15000000 | 2300000 |
19 10 06 | Aid for the rehabilitation and reconstruction of Afghanistan | 4 | 183000000 | 164000000 | | | 183000000 | 164000000 |
| Article 19 10 06 — Subtotal | | 183000000 | 164000000 | | | 183000000 | 164000000 |
| Chapter 19 10 — Total | | 634000000 | 623150000 | 15000000 | | 649000000 | 623150000 |
Remarks
The purpose of development cooperation under this heading is primarily its contribution to achieving the millennium development goals (MDGs), particularly Goal 1: to halve the proportion of people whose income is less than USD 1 per day and to halve the proportion of people who suffer from hunger before 2015. The MDGs give an overall benchmark for this.
For OECD Development Assistance Committee (DAC) list 1 countries, a benchmark of 35 % of annual commitments is allocated to social infrastructure, mainly education and health, but also including macroeconomic assistance with social sector conditionality. In this context the contribution from the EU budget should be seen as part of overall donor support to the social sector in any given country. A degree of flexibility should be the norm. A benchmark of 20 % of total annual commitments will be allocated to activities in the sector of basic health and basic education, including sectoral budget support to health and education ministries where it is to serve basic health and basic education.
Before July each year, the Commission must submit to the European Parliament and to the Council an annual report on development cooperation. This report must also contain information on how the work of the Commission has contributed to the 35 % target for social infrastructure and in achieving the MDGs. The report should also contain information on the state of donor coordination between the Member States and information on how budget support has contributed to the MDGs. Budget support will depend on prior demonstration of sufficient institutional capacity and compliance with detailed criteria for the custody and use of funds in the recipient country. The criteria must be stated in the annual report, and compliance therewith must be assessed in the report.
After the report has been presented the three institutions will enter into a dialogue on the results and on how further progress can be made towards achieving the objectives.
The cooperation agreements with the Asian developing countries lay down a human rights’ clause, failure to comply with which means that the agreements will be suspended. The Commission is called on to be scrupulous in making sure that that clause is complied with by beneficiaries when any project is funded. This appropriation also covers the protection and promotion of children's rights and the integration of children's rights into programming, including in the country strategy papers, national indicative programmes and mid-term reviews.
19 10 04Rehabilitation and reconstruction operations in developing countries in Asia
Appropriations 2005 | Amending budget No. 3 | New amount |
Commitments | Payments | Commitments | Payments | Commitments | Payments |
p.m. | 2300000 | 15000000 | | 15000000 | 2300000 |
The likely schedule of payments vis-à-vis commitments is as follows:
Commitments | Payments |
2004 | 2005 | 2006 | 2007 | Subsequent years and others |
Pre-2004 commitments still outstanding | 16066901 | 6500000 | 1300000 | 1000000 | 1000000 | 6266901 |
Commitment appropriations made available again and/or carried over from 2003 | | | | | | |
Appropriations 2004 | 4125000 | 2000000 | 1000000 | 1000000 | 125000 | |
Appropriations 2005 | 15000000 | | | | | |
Total | 35191901 | 8500000 | 2300000 | 2000000 | 1125000 | 6266901 |
Remarks
This appropriation is intended to cover measures to initiate the return to a normal life of people in developing countries in the aftermath of critical situations arising out of natural disasters, violent conflicts or other crises. In particular, it will cover the post Tsunami reconstruction aid.
Operations covered will include:
- the relaunch of production on a lasting basis,
- the physical and operational rehabilitation of basic infrastructure, including mine clearance,
- social reintegration, in particular of refugees, displaced persons and demobilised troops,
- the restoration of the institutional capacities needed in the rehabilitation period, especially at local level,
- assisting with the needs of children, particularly rehabilitation of children affected by war, including child soldiers,
- supporting disabled people and disabled people's organisations to ensure that their particular needs are met in the context of rehabilitation operations,
- ensuring that the needs of women, children and older people are taken into account during disaster relief and reconstruction operations,
- raising awareness of the dangers posed by natural disasters and of measures designed to avoid them or to avoid or reduce their consequences.
In particular, operations may cover programmes and projects implemented by non-governmental development organisations and other civil society players who are encouraged to participate and, in turn, promote the participation of the beneficiary population at every level of the decision-making and implementation process.
Legal basis
Council Regulation (EEC) No 443/92 of 25 February 1992 on financial and technical assistance to, and economic cooperation with, the developing countries in Asia and Latin America (OJ L 52, 27.2.1992, p. 1).
Council Regulation (EC) No 2258/96 of 22 November 1996 on rehabilitation and reconstruction operations in developing countries (OJ L 306, 28.11.1996, p. 1).
[1] The ensuing payments appropriations will be requested by a transfer.
--------------------------------------------------
