COUNCIL REGULATION (EC) No 847/96 of 6 May 1996 introducing additional conditions for year-to-year management of TACs and quotas
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 43 thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Whereas, in addition to the provisions listed in Article 4 (2) of Council Regulation (EEC) No 3760/92 of 20 December 1992 establishing a Community system for fisheries and aquaculture (3), it is necessary to lay down conditions for the pursuit of exploitation activities which would improve the mechanisms at present available by the introduction of the appropriate year-to-year flexibility in the management of total allowable catches (TACs) and quotas which, within certain limits, is compatible with conservation policies;
Whereas, pursuant to Article 8 of Regulation (EEC) No 3760/92, it is for the Council to establish the fishing availabilities to be allocated to Member States and to determine the conditions for adjusting these availabilities from one year to the next;
Whereas stocks subject to precautionary or analytical TACs need to be defined;
Whereas permitted landings from a stock, for the purpose of this Regulation, need to be defined;
Whereas, under certain conditions, precautionary TACs and quotas for certain stocks may be revised upwards during the year with negligible danger of undermining the principle of rational and responsible exploitation of marine resources;
Whereas it is appropriate to encourage Member States to transfer part of their quotas of stocks subject to analytical TAC from one year to another within certain limits;
Whereas other stocks subject to either an analytical or a precautionary TAC may be known to be in a state of exploitation such that any increase in the TAC is undesirable;
Whereas overfishing of quotas should be penalized; whereas this can be achieved by imposing appropriate reductions in the following year's quota on the Member States responsible for the overfishing; whereas, in accordance with Article 23 of Council Regulation (EEC) No 2847/93 of 12 October 1993 establishing a control system applicable to the common fisheries policy (4), the Council is to adopt rules by which the Commission may operate deductions from the quotas when overfishing has taken place, taking into account the degree of the overfishing, any cases of overfishing in the previous year and the biological status of the resources concerned,
HAS ADOPTED THIS REGULATION:
Article 1
1. Precautionary TACs shall apply to stocks for which no scientifically-based evaluation of fishing possibilities is available specifically for the year in which the TACs are to be set; analytical TACs shall apply otherwise.
2. For the purpose of this Regulation, permitted landings from a stock shall consist, for a given Member State, of the quota allocated by the Council on the basis of Article 8 (4) of Regulation (EEC) No 3760/92, as modified by:
- the exchanges made on the basis of Article 9 of Regulation (EEC) No 3760/92,
- the compensation envisaged by Article 21 (4) of Regulation (EEC) No 2847/93,
- the quantities withheld on the basis of Article 4 (2) of this Regulation, and
- the deductions envisaged by Article 5 of this Regulation.
Article 2
When fixing TACs in accordance with Article 8 of Regulation (EEC) No 3760/92, the Council shall decide:
- which stocks are subject to a precautionary TAC and which stocks are subject to an analytical TAC, on the basis of scientific advice available on the stocks,
- the stocks to which Articles 3 or 4 shall not apply, on the basis of the biological status of the stocks and of commitments reached with third countries,
- the stocks to which the deductions envisaged in Article 5 (2) shall apply, on the basis of their biological status.
Article 3
1. When more than 75 % of a precautionary TAC has been utilized before 31 October of the year of its application, a Member State with a quota for the stock for which that TAC has been fixed may request an increase in the TAC. Such a request, accompanied by relevant supporting biological information and an indication of the magnitude of the revision, shall be addressed to the Commission. The Commission, within 20 working days, shall examine all the elements of the request with a view to presenting to the Council a proposal for an amendment of the Regulation fixing TACs and quotas if it is found justified. The Member State shall be informed of the results of the examination.
2. Member States may take catches up to 5 % in excess of permitted landings. However, these catches shall be considered as exceeding permitted landings as regards the deductions envisaged in Article 5.
3. When more than 75 % of a quota for a stock subject to a precautionary TAC has been utilized before 31 October of the year of its application, the Member State to which such a quota has been allocated may request the Commission's permission to land additional quantities of fish of the same stock indicating the additional quantity required, this quantity not to exceed 10 % of the appropriate quota. The Commission shall decide on such requests within 20 workings days in accordance with the procedure laid down in Article 36 of Regulation (EEC) No 2847/93. The additional quantity granted under this procedure shall be considered as exceeding permitted landings for the purposes of the deductions envisaged in Article 5 of this Regulation.
Article 4
1. Article 3 (2) and (3) shall apply to stocks subject to an analytical TAC.
2. For stocks subject to analytical TAC, except those referred to in Article 5 (2), a Member State to which a relevant quota has been allocated may ask the Commission, before 31 October of the year of application of the quota, to withhold a maximum of 10 % of its quota to be transferred to the following year.
The Commission, in accordance with the procedure laid down in Article 36 of Regulation (EEC) No 2847/93, shall add to the relevant quota the quantity withheld.
Article 5
1. Except for the stocks referred to in paragraph 2, all landings in excess of the respective permitted landings shall be deducted from the quotas of the same stock in the following year.
2. For the stocks referred to in the third indent of Article 2, overfishing of permitted landings shall lead to deduction from the corresponding quota in the following year according to the following table:
>TABLE>
However, a deduction equal to the overfishing × 1,00 shall apply in all cases of overfishing relative to permitted landings equal to, or less than, 100 tonnes.
An additional 3 % of the quantity fished in excess of permitted landings shall be deducted for each successive year in which permitted landings are overfished by more than 10 %.
3. Deductions shall be without prejudice to Article 21 (4) of Regulation (EEC) No 2847/93.
Article 6
This Regulation shall enter into force on 1 January 1997.
However, Article 5 shall apply from 1 January 1998.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 6 May 1996.
For the Council
The President
G. LOMBARDI
(1) OJ No C 382, 31. 12. 1994, p. 4.
(2) OJ No C 249, 25. 9. 1995, p. 84.
(3) OJ No L 389, 31. 12. 1992, p. 1. Regulation as last amended by the 1994 Act of Accession.
(4) OJ No L 261, 20. 10. 1993, p. 1. Regulation as last amended by Regulation (EC) No 2870/95 (OJ No L 301, 14. 12. 1995, p. 1).
