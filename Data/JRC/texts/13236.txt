Commission Decision
of 27 October 2006
not to publish the reference of standard EN 13683:2003 "Garden equipment — Integrally powered shredders/chippers — Safety" in accordance with Directive 98/37/EC of the European Parliament and of the Council
(notified under document number C(2006) 5060)
(Text with EEA relevance)
(2006/732/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Directive 98/37/EC of the European Parliament and of the Council of 22 June 1998 on the approximation of the laws of the Member States relating to machinery [1], and in particular Article 6(1) thereof,
Having regard to the opinion of the Standing Committee established by Article 5 of Directive 98/34/EC of the European Parliament and of the Council of 22 June 1998 laying down a procedure for the provision of information in the field of technical standards and regulations and of rules on Information Society services [2],
Whereas:
(1) Where a national standard transposing a harmonised standard, the reference of which has been published in the Official Journal of the European Union, covers one or more essential health and safety requirements set out in Annex I to Directive 98/37/EC, the machine built in accordance with this standard is presumed to meet the essential requirements concerned.
(2) Pursuant to Article 6(1) of Directive 98/37/EC, Germany lodged a formal objection in respect of standard EN 13683:2003, adopted by the European Committee for Standardisation (CEN) on 8 September 2003, the reference of which has not yet been published in the Official Journal of the European Union.
(3) Having examined standard EN 13683:2003, the Commission has established that it fails to meet essential health and safety requirements 1.1.2.(c) (principles of safety integration), 1.3.8 (choice of protection against risks related to moving parts) and 1.4.1 (general requirements for guards and protection devices) of Annex I to Directive 98/37/EC. First, the specifications set out in clauses 5.2.1.1 and 5.2.1.2 of the standard on preventing access to the cutting blade from the feed side and from above are not sufficient to avoid contact with the blade. It is possible to remove jammed cuttings whilst the appliance is running and for the hand to come into contact with the cutting blade in doing so. Second, the specifications set out in clause 5.2.2 of the standard on preventing access to the cutting blade from the discharge chute from below are not sufficient. The dimensions of the discharge chute are such that an adult hand could come into contact with the cutting blade.
(4) The references of standard EN 13683:2003 should therefore not be published in the Official Journal of the European Union,
HAS ADOPTED THIS DECISION:
Article 1
The references of standard EN 13683:2003 "Garden equipment — Integrally powered shredders/chippers — Safety" shall not be published in the Official Journal of the European Union.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 27 October 2006.
For the Commission
Günter Verheugen
Vice-President
[1] OJ L 207, 23.7.1998, p. 1. Directive as amended by Directive 98/79/EC (OJ L 331, 7.12.1998, p. 1).
[2] OJ L 204, 21.7.1998, p. 37. Directive as last amended by the 2003 Act of Accession.
--------------------------------------------------
