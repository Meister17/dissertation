Commission Decision
of 22 August 2002
amending Decision 2002/80/EC imposing special conditions on the import of figs, hazelnuts and pistachios and certain products derived thereof originating in or consigned from Turkey
(notified under document number C(2002) 3109)
(Text with EEA relevance)
(2002/679/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 93/43/EEC of 14 June 1993 on the hygiene of foodstuffs(1), and in particular Article 10(1) thereof,
Whereas:
(1) Article 2 of Commission Decision 2002/80/EC(2), as amended by Decision 2002/233/EC(3), provides for a review of that decision before 1 July 2002, in order to assess whether the special conditions set out in that Decision provide a sufficient level of protection of public health within the Community, and whether there is a continuing need for the special conditions.
(2) The results of random sampling and analysis of consignments of dried figs, hazelnuts and pistachios originating in or consigned from Turkey demonstrate that there is a continuing need for the special conditions set out in Decision 2002/80/EC in order to provide a sufficient level of protection of public health within the Community.
(3) Fresh figs are not known to be contaminated by aflatoxins and it is therefore appropriate to exclude fresh figs from the scope of Decision 2002/80/EC. Fig and hazelnut pastes have been found to be contaminated by aflatoxins and it is therefore appropriate to include fig and hazelnut pastes within the scope of that Decision.
(4) In order to ensure that the random sampling and analysis of consignments of dried figs, hazelnuts and pistachios originating in or consigned from Turkey are performed in a harmonised manner throughout the Community, it is appropriate to fix an approximate frequency for the random sampling and analysis, as well to specify the sampling method to apply for hazelnuts, including vacuum packs.
(5) It is necessary to update the list of points of entry for Belgium, Germany, France, Ireland, Austria and Sweden through which the products concerned by Decision 2002/80/EC may be imported.
(6) Decision 2002/80/EC should therefore be amended accordingly.
(7) The Standing Committee on the Food Chain and Animal Health has been consulted,
HAS ADOPTED THIS DECISION:
Article 1
Decision 2002/80/EC is amended as follows:
1. Article 1 is amended as follows:
(a) Paragraph 1 is replaced by the following: "1. Member States shall not import products falling in any of the following categories, originating in or consigned from Turkey, which are intended for human consumption or to be used as an ingredient in foodstuffs, unless the consignment is accompanied by the results of official sampling and analysis, and by the health certificate set out in Annex 1 completed, signed and verified by a representative of the General Directorate of Protection and Control of the Ministry of Agriculture and Rural Affairs of the Republic of Turkey:
- dried figs falling within CN code 0804 20 90,
- hazelnuts (Corylus sp) in shell or shelled falling within CN code 0802 21 00 or 0802 22 00,
- pistachios falling within CN code 0802 50 00,
- mixtures of nuts or dried fruits falling within CN code 0813 50 and containing figs, hazelnuts or pistachios,
- fig paste and hazelnut paste falling within CN code 2007 99 98,
- hazelnuts, figs and pistachios, prepared or preserved, including mixtures falling within CN code 2008 19."
(b) The following paragraph is added: "6. The random sampling and analysis referred to in paragraph 5 shall be carried out on approximately 10 % of the consignments of products for each category of the products referred to in paragraph 1.
Any consignment to be subjected to sampling and analysis, shall be detained before release onto the market from the point of entry into the Community for a maximum of 10 working days. In this event, the competent authorities in the Member States shall issue an accompanying official document establishing that the consignment has been subjected to official sampling and analysis and indicating the result of the analysis.
For hazelnuts the sampling shall be performed according to the sampling procedure set out in point 5.2 of Annex I to Commission Directive 98/53/EC(4). In the case of hazelnuts traded in vacuum packs, for lots equal or more than 15 tonnes at least 25 incremental samples resulting in a 30 kg aggregate sample have to be taken and for lots less than 15 tonnes, 25 % of the incremental samples to be taken according to Directive 98/53 have to be taken."
2. Article 2 is replaced by the following: "Article 2
This Decision shall be kept under review in the light of information and guarantees provided by the competent authorities of Turkey and on the basis of the results of the tests carried out by Member States.
This Decision shall be reviewed by 31 December 2002 at the latest, in order to assess whether the special conditions, referred to in Article 1, provide a sufficient level of protection of public health within the Community. The review shall also assess whether there is a continuing need for the special conditions."
3. Annex II is replaced by the text in the Annex to this Decision.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 22 August 2002.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 175, 19.7.1993, p. 1.
(2) OJ L 34, 5.2.2002, p. 26.
(3) OJ L 78, 21.3.2002, p. 14.
(4) OJ L 201, 17.7.1998, p. 93.
ANNEX
"ANNEX ΙΙ
List of points of entry through which figs, hazelnuts and pistachios and products derived thereof originating in or consigned from Turkey may be imported into the Community
>TABLE>"
