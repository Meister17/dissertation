*****
COMMISSION REGULATION (EEC) No 2041/83
of 22 July 1983
amending for the sixth time Regulation (EEC) No 2960/77 on detailed rules for the sale of olive oil held by intervention agencies
THE COMMISSION OF THE EUROPEAN
COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation No 136/66/EEC of 22 September 1966 on the establishment of the common organization of the market in oils and fats (1), as last amended by Regulation (EEC) No 1413/82 (2), and in particular Article 12 (4) thereof,
Whereas Article 13 (1) of Commission Regulation (EEC) No 2960/77 (3), as last amended by Regulation (EEC) No 1852/82 (4), provides that where olive oil held by the intervention agencies is sold it shall be withdrawn, depending on the quantity awarded, within a period of 40 or 60 days following notification of the result of the tender;
Whereas experience has shown that for quantities in excess of 3 000 tonnes operators are encountering real difficulties in observing the time limits, in particular where the oil is distributed between several intervention centres; whereas the time limit for withdrawing the oil in such a case should be extended under certain conditions in order to enable intervention sales to be conducted properly;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Oils and Fats,
HAS ADOPTED THIS REGULATION:
Article 1
Article 13 (1) of Regulation (EEC) No 2960/77 is hereby replaced by the following:
'1. The purchaser shall withdraw the entire lot awarded. Withdrawal may begin as soon as the provisional amount referred to in Article 12 (1) has been paid and, in the case of sale for export, the security referred to in Article 12 (3) has been lodged.
Withdrawal shall be completed not later than the 40th day following receipt of the information referred to in Article 10.
However, where the party concerned has been awarded a quantity
- in excess of 1 000 tonnes but not exceeding 3 000 tonnes, withdrawal shall be completed not later than the 60th day following receipt of the information,
- in excess of 3 000 tonnes, withdrawal shall be completed not later than the 90th day following receipt of the information, provided that a quantity of at least 3 000 tonnes shall be withdrawn not later than 60 days following receipt of the information.'
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 22 July 1983.
For the Commission
Poul DALSAGER
Member of the Commission
(1) OJ No 172, 30. 9. 1966, p. 3025/66.
(2) OJ No L 162, 12. 6. 1982, p. 6.
(3) OJ No L 348, 30. 12. 1977, p. 46.
(4) OJ No L 203, 10. 7. 1982, p. 17.
