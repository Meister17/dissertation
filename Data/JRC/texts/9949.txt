Agreement
in the form of an Exchange of Letters between the European Community and the Republic of Chile concerning amendments to the Agreement on trade in spirit drinks and aromatised drinks annexed to the Agreement establishing an association between the European Community and its member states, of the one part, and the Republic of Chile, of the other part
A. Letter from the Community
Brussels,
Sir,
I have the honour to refer to meetings of the Joint Committee established in accordance with Article 17 of Annex VI to the Association Agreement (the Agreement on Trade in Spirit Drinks and Aromatised Drinks). The Joint Committee has recommended that modifications should be made to the Agreement on Trade in Spirit Drinks and Aromatised Drinks (hereinafter referred to as "Annex VI"), in order to take account of legislative developments since its adoption.
During the recent Joint Committee meeting held in Madrid on 13- 14 June 2005 there was agreement on the need to amend not only the appendices but also the text of the Agreement in order to update it. I have therefore the honour to propose that Annex VI be amended as indicated in the Appendix attached hereto, with effect as of the date of signature.
I should be obliged if you would confirm that your Government is in agreement with the content of this letter.
Please accept, Sir, the assurance of my highest consideration.
On behalf of the European Community
Appendix
Annex VI is hereby amended as follows:
1. in Article 5, paragraph 2 is replaced by the following:
"2. The names referred to in Article 6 shall be reserved exclusively for the products originating in the Party to which they apply.";
2. Article 7 is amended as follows:
(a) paragraph 2 is replaced by the following:
"2. On the basis of the Chilean trademark register as established on 10 June 2002, the trademarks listed in Appendix II A shall be cancelled within 12 years for use on the internal market and five years for export from the date of entry into force of this Agreement.";
(b) after paragraph 2, the following paragraph is inserted:
"2a. On the basis of the Chilean trademark register as established on 10 June 2002, trademarks listed in Appendix II B. are allowed under the conditions laid down in this Appendix, exclusively for use on the internal market, and shall be cancelled within 12 years from the date of entry into force of this Agreement.";
3. Article 8 is amended as follows:
(a) paragraph 1 is replaced by the following:
"1. The Parties are not aware, on the basis of the Chilean trademark register as established on 10 June 2002, of any trademarks other than those listed in Article 7(2) and (2a) which are identical with, or similar to, or contain the protected designation referred to in Article 6.";
(b) paragraph 2 is replaced by the following:
"2. Pursuant to paragraph 1, the Parties shall not deny the right to use a trademark contained in the Chilean trademark register on 10 June 2002, other than those referred to in Article 7(2) and (2a), on the basis that such a trademark is identical with, or similar to, or contains a protected designation listed in Appendix I.";
4. Article 17(3) is replaced by the following:
"3. In particular, the Joint Committee may make recommendations in furtherance of the objectives of this Agreement. It shall be conducted in accordance with the Rules of Procedure for the Special Committees.".
B. Letter from the Republic of Chile
Santiago de Chile/Brussels,
Madam,
I have the honour to acknowledge receipt of your letter of today's date which reads as follows:
"I have the honour to refer to meetings of the Joint Committee established in accordance with Article 17 of Annex VI to the Association Agreement (the Agreement on Trade in Spirit Drinks and Aromatised Drinks). The Joint Committee has recommended that modifications should be made to the Agreement on Trade in Spirit Drinks and Aromatised Drinks (hereinafter referred to as "Annex VI"), in order to take account of legislative developments since its adoption.
During the recent Joint Committee meeting held in Madrid on 13- 14 June 2005 there was agreement on the need to amend not only the appendices but also the text of the Agreement in order to update it. I have therefore the honour to propose that Annex VI be amended as indicated in the Appendix attached hereto, with effect as of the date of signature.
I should be obliged if you would confirm that your Government is in agreement with the content of this letter.".
I have the honour to inform you that the Republic of Chile is in agreement with the content of this letter.
Please accept, Madam, the assurance of my highest consideration.
For the Republic of Chile
--------------------------------------------------
