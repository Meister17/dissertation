Council Decision
of 10 November 2000
implementing Common Position 2000/696/CFSP concerning the maintenance of specific restrictive measures against Mr Milosevic and persons associated with him
(2000/697/CFSP)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty on European Union and, in particular, Article 23(2) thereof,
Having regard to Common Position 2000/696/CFSP on the maintenance of specific restrictive measures against Mr Milosevic and persons associated with him(1), and in particular Article 1(2) thereof,
Having regard to Council Common Position 2000/599/CFSP of 9 October 2000 on support to a democratic Federal Republic of Yugoslavia and the immediate lifting of certain restrictive measures(2),
Whereas:
(1) In order to implement Article 1(2) of Common Position 2000/599/CFSP, the Council has to adopt a list of persons reported for non-admission in the Member States.
(2) Council Decision 1999/319/CFSP of 10 May 1999 implementing Common Position 1999/318/CFSP concerning additional restrictive measures(3) against the Federal Republic of Yugoslavia should be repealed,
HAS DECIDED AS FOLLOWS:
Article 1
The persons to which the obligation of non-admission referred to in Article 1 of Common Position 2000/696/CFSP applies are the following:
>TABLE>
Article 2
Decision 1999/319/CFSP is hereby repealed.
Article 3
This Decision shall take effect on the date of its adoption.
Article 4
This Decision shall be published in the Official Journal.
Done at Brussels, 10 November 2000.
For the Council
The President
C. Josselin
(1) See page 1 of this Official Journal.
(2) OJ L 261, 14.10.2000, p. 1.
(3) OJ L 123, 13.5.1999, p. 1.
