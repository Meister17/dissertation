[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 21.11.2006
COM(2006) 702 final
Proposal for a
COUNCIL REGULATION
amending the Annex to Regulation (EC) No 2042/2000 imposing a definitive anti-dumping duty on imports of television camera systems originating in Japan
(presented by the Commission)
EXPLANATORY MEMORANDUM
CONTEXT OF THE PROPOSAL |
110 | Grounds for and objectives of the proposal This proposal concerns the application of Council Regulation (EC) No 384/96 of 22 December 1995 on protection against dumped imports from countries not members of the European Community, as last amended by Council Regulation (EC) No 2117/2005 ("the basic Regulation") and aims at including television camera systems in an Annex to Regulation (EC) No 2042/2000. This annex contains a list of television camera systems which, although falling in the product description, cannot be regarded as broadcast camera systems and therefore have to be exempted from the anti-dumping duty. |
120 | General context This proposal is made in the context of the implementation of the basic Regulation and is the result of an investigation which was carried out in line with the substantive and procedural requirements laid out in the basic Regulation. |
139 | Existing provisions in the area of the proposal There are no existing provisions in the area of the proposal. |
141 | Consistency with other policies and objectives of the Union Not applicable. |
CONSULTATION OF INTERESTED PARTIES AND IMPACT ASSESSMENT |
Consultation of interested parties |
219 | Interested parties concerned by the proceeding have already had the possibility to defend their interests during the investigation, in line with the provisions of the basic Regulation. |
Collection and use of expertise |
229 | There was no need for external expertise. |
230 | Impact assessment This proposal is the result of the implementation of the basic Regulation. The basic Regulation does not foresee a general impact assessment but contains an exhaustive list of conditions that have to be assessed. |
LEGAL ELEMENTS OF THE PROPOSAL |
305 | Summary of the proposed action The Commission received a request from a Japanese exporting producer of television camera systems to add a model of professional camera systems to the Annex to Council Regulation (EC) No 2042/2000 and thus exempt it from the application of the duty. This model was considered as a professional camera system which should be exempted from the application of the duty. It is therefore proposed to exempt this model exported by Hitachi under Article 1(3)(e) of Regulation (EC) No 2042/2000 and consequently add it to the Annex. It is therefore proposed that the Council adopt the attached proposal for a Regulation. |
310 | Legal basis Council Regulation (EC) No 384/96 of 22 December 1995 on protection against dumped imports from countries not members of the European Community, as last amended by Council Regulation (EC) No 2117/2005 of 21 December 2005. |
329 | Subsidiarity principle The proposal falls under the exclusive competence of the Community. The subsidiarity principle therefore does not apply. |
Proportionality principle The proposal complies with the proportionality principle for the following reason(s). |
331 | The form of action is described in the above-mentioned basic Regulation and leaves no scope for national decision. |
332 | Indication of how financial and administrative burden falling upon the Community, national governments, regional and local authorities, economic operators and citizens is minimized and proportionate to the objective of the proposal is not applicable. |
Choice of instruments |
341 | Proposed instruments: regulation. |
342 | Other means would not be adequate for the following reason(s). The above-mentioned basic Regulation does not foresee alternative options. |
BUDGETARY IMPLICATION |
409 | The proposal has no implication for the Community budget. |
1. Proposal for a
COUNCIL REGULATION
amending the Annex to Regulation (EC) No 2042/2000 imposing a definitive anti-dumping duty on imports of television camera systems originating in Japan
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 384/96 of 22 December 1995 on protection against dumped imports from countries not members of the European Community[1], (’the basic Regulation’),
Having regard to the proposal submitted by the Commission after consulting the Advisory Committee,
Whereas:
A. PREVIOUS PROCEDURES
(1) The Council, by Regulation (EC) No 1015/94[2], imposed a definitive anti-dumping duty on imports of television camera systems (’TCS’) originating in Japan.
(2) In September 2000, the Council, by Regulation (EC) No 2042/2000[3] confirmed the definitive anti-dumping duties imposed by Regulation (EC) No 1015/94 (as subsequently amended) pursuant to Article 11(2) of the basic Regulation.
(3) In Article 1(3)(e) of Regulation (EC) No 2042/2000, the Council specifically excluded from the scope of the anti-dumping duty camera systems listed in the Annex to that Regulation (’the Annex’), representing high-end professional camera systems technically falling within the product definition under Article 1(2) of that Regulation, but which cannot be regarded as television camera systems.
(4) The Commission, by notice of 29 September 2005[4], initiated a review, pursuant to Article 11(2) of the basic Regulation, of Regulation (EC) No 2042/2000 which imposed the existing anti-dumping measures on imports of TCS originating in Japan.
(5) The Commission, by notice of 18 May 2006, initiated an anti-dumping proceeding concerning imports of certain camera systems originating in Japan pursuant to Article 5 of the basic Regulation. Given that the product scope of this proceeding includes the products subject to measures by Regulation (EC) No 2042/2000, the Commission also initiated by this notice of 18 May 2006, a review, under Article 11(3) of the basic Regulation, of the existing measures. The investigations mentioned in recitals (4) and (5) above are still ongoing.
B. INVESTIGATION CONCERNING NEW MODELS OF PROFESSIONAL CAMERA SYSTEMS
1. Procedure
(6) One Japanese exporting producer, Hitachi Denshi (Europa) GmbH (‘Hitachi’) informed the Commission that it intended to introduce a new model of professional camera systems into the Community market and requested the Commission to add this new model of professional camera systems to the Annex of Council Regulation (EC) No 2042/2000 and thus exempt it from the scope of the anti-dumping duties.
(7) The Commission informed the Community industry accordingly and commenced an investigation limited to the determination of whether the product under consideration falls within the scope of the anti-dumping duties and whether the operative part of Council Regulation (EC) No 2042/2000 should be amended accordingly.
2. Model under investigation
(8) The request for exemption was received for the following model of camera systems, supplied with the relevant technical information:
Hitachi:
- Camera head V-35W.
This model was presented as a successor model of the already exempted V-35 camera head.
3. Findings
(9) Camera head V-35W fall within the product description of Article 1(2)(a) of Council Regulation (EC) No 2042/2000. However, as with its predecessor model, it is mainly used for professional applications and it is not sold with the corresponding triax system or adaptor on the EC market.
(10) Therefore, it was found that it qualifies as a professional camera system within the meaning of Article 1(3)(e) of Regulation (EC) No 2042/2000. As a result, it should be excluded from the scope of the existing anti-dumping measures and added to the Annex of Regulation (EC) No 2042/2000.
(11) In accordance with the established Community institutions' practice, the new model should be exempted from the anti-dumping duty from the date of receipt by the Commission services of the relevant request for exemption. Therefore, all imports of the following camera model imported on or after 11th April 2006 should be exempted from the anti-dumping duty from this date:
Hitachi:
- Camera head V-35W
4. Information of the interested parties and conclusions
(12) The Commission informed the Community industry and the exporting producer of TCS concerned of its findings and provided them with an opportunity to present their views. None of the parties objected to the Commission's findings.
(13) In view of the above, it is proposed to amend the Annex of Council Regulation (EC) No 2042/2000 accordingly.
HAS ADOPTED THIS REGULATION:
Article 1
The Annex of Council Regulation (EC) No 2042/2000 shall be replaced by the Annex hereto.
Article 2
1. This Regulation shall enter into force on the day following that of its publication in the Official Journal of the European Union .
2. This Regulation shall apply to imports of the following model produced and exported to the Community by the following exporting producer:
Hitachi from 11th April 2006
- Camera head V-35W.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels,
For the Council
The President
ANNEX
List of professional camera systems not qualified as television camera systems (broadcast camera systems), which are exempted from the measures
Company name | Camera heads | Viewfinder | Camera control unit | Operational control unit | Master control unit | Camera adapters |
Sony | DXC-M7PK DXC-M7P DXC-M7PH DXC-M7PK/1 DXC-M7P/1 DXC-M7PH/1 DXC-327PK DXC-327PL DXC-327PH DXC-327APK DXC-327APL DXC-327AH DXC-537PK DXC-537PL DXC-537PH DXC-537APK DXC-537APL DXC-537APH EVW-537PK EVW-327PK DXC-637P DXC-637PK | DXF-3000CE DXF-325CE DXF-501CE DXF-M3CE DXF-M7CE DXF-40CE DXF-40ACE DXF-50CE DXF-601CE DXF-40BCE DXF-50BCE DXF-701CE DXF-WSCE(1) DXF-801CE(1) HDVF-C30W | CCU-M3P CCU-M5P CCU-M7P CUU-M5AP(1) | RM-M7G RM-M7E(1) | __ | CA-325P CA-325AP CA-325B CA-327P CA-537P CA-511 CA-512P CA-513 VCT-U14(1) |
DXC-637PL DXC-637PH PVW-637PK PVW-637PL DXC-D30PF DXC-D30PK DXC-D30PL DXC-D30PH DSR-130PF DSR-130PK DSR-130PL PVW-D30PF PVW-D30PK PVW-D30PL DXC-327BPF DXC-327BPK DXC-327BPL DXC-327BPH DXC-D30WSP(1) DXC-D35PH(1) DXC-D35PL(1) DXC-D35PK(1) DXC-D35WSPL(1) DSR-135PL(1) |
Ikegami | HC-340 HC-300 HC-230 HC-240 HC-210 HC-390 LK-33 HDL-30MA HDL-37 HC-400(1) HC-400W(1) HDL-37E HDL-10 HDL-40 HC-500(1) HC-500W(1) | VF15-21/22 VF-4523 VF15-39 VF15-46(1) VF5040(1) VF5040W(1) | MA-200/230 MA-200A(1) MA-400(1) CCU-37 CCU-10 | RCU-240 RCU-390(1) RCU-400(1) RCU-240A | __ | CA-340 CA-300 CA-230 CA-390 CA-400(1) CA-450(1) |
Hitachi | SK-H5 SK-H501 DK-7700 DK-7700SX HV-C10 HV-C11 HV-C10F Z-ONE (L) Z-ONE (H) Z-ONE Z-ONE A (L) Z-ONE A (H) Z-ONE A (F) Z-ONE A Z-ONE B (L) Z-ONE B (H) Z-ONE B (F) Z-ONE B Z-ONE B (M) Z-ONE B (R) FP-C10 (B) FP-C10 (C) FP-C10 (D) FP-C10 (G) FP-C10 (L) FP-C10 (R) FP- C10 (S) FP-C10 (V) FP-C10 (F) FP-C10 FP-C10 A FP-C10 A (A) FP-C10 A (B) FP-C10 A (C) FP-C10 A (D) FP-C10 A (F) FP-C10 A (G) FP-C10 A (H) FP-C10 A (L) FP-C10 A (R) FP-C10 A (S) FP-C10 A (T) FP-C10 A (V) FP-C10 A (W) Z-ONE C (M) Z-ONE C (R) Z-ONE C (F) Z-ONE C HV-C20 HV-C20M Z-ONE-D Z-ONE-D (A) Z-ONE-D (B) Z-ONE-D (C) Z-ONE.DA(1) V-21(1) V-21W(1) V-35(1) DK-H31(1) V-35W(1) | GM-5 (A) GM-5-R2 (A) GM-5-R2 GM-50 GM-8A(1) GM-9(1) GM-51(1) | RU-C1 (B) RU-C1 (D) RU-C1 RU-C1-S5 RU-C10 (B) RU-C10 (C) RC-C1 RC-C10 RU-C10 RU-Z1 (B) RU-Z1 (C) RU-Z1 RC-C11 RU-Z2 RC-Z1 RC-Z11 RC-Z2 RC-Z21 RC-Z2A(1) RC-Z21A(1) RU-Z3(1) RC-Z3(1) RU-Z35(1) RU-3300N(1) | __ | __ | CA-Z1 CA-Z2 CA-Z1SJ CA-Z1SP CA-Z1M CA-Z1M2 CA-Z1HB CA-C10 CA-C10SP CA-C10SJA CA-C10M CA-C10B CA-Z1A(1) CA-Z31(1) CA-Z32(1) CA-ZD1(1) CA-Z35(1) EA-Z35(1) |
Matsushita | WV-F700 WV-F700A WV-F700SHE WV-F700ASHE WV-F700BHE WV-F700ABHE WV-F700MHE WV-F350 WV-F350HE WV-F350E WV-F350AE WV-F350DE WV-F350ADE WV-F500HE (*) WV-F-565HE AW-F575HE AW-E600 AW-E800 AW-E800A AW-E650 AW-E655 AW-E750 AW-E860L AK-HC910L AK-HC1500G | WV-VF65BE WV-VF40E WV-VF39E WV-VF65BE (*) WV-VF40E (*) WV-VF42E WV-VF65B AW-VF80 | WV-RC700/B WV-RC700/G WV-RC700A/B WV-RC700A/G WV-RC36/B WV-RC36/G WV-RC37/B WV-RC37/G WV-CB700E WV-CB700AE WV-CB700E (*) WV-CB700AE (*) WV-RC700/B (*) WV-RC700/G (*) WV-RC700A/B (*) WV-RC700A/G (*) WV-RC550/G WV-RC550/B WV-RC700A WV-CB700A WV-RC550 WV-CB550 AW-RP501 AW-RP505 AK-HRP900 AK-HRP150 | __ | __ | WV-AD700SE WV-AD700ASE WV-AD700ME WV-AD250E WV-AD500E (*) AW-AD500AE AW-AD700BSE |
JVC | KY-35E KY-27ECH KY-19ECH KY-17FITECH KY-17BECH KY-F30FITE KY-F30BE KY-F560E KY-27CECH KH-100U KY-D29ECH KY-D29WECH(1) | VF-P315E VF-P550E VF-P10E VP-P115E VF-P400E VP-P550BE VF-P116E VF-P116WE(1) VF-P550WE(1) | RM-P350EG RM-P200EG RM-P300EG RM-LP80E RM-LP821E RM-LP35U RM-LP37U RM-P270EG RM-P210E | __ | __ | KA-35E KA-B35U KA-M35U KA-P35U KA-27E KA-20E KA-P27U KA-P20U KA-B27E KA-B20E KA-M20E KA-M27E |
Olympus | MAJ-387N MAJ-387I | OTV-SX 2 OTV-S5 OTV-S6 |
Camera OTV-SX |
(*)Also called master set up unit (MSU) or master control panel (MCP). (1) Models exempted under the condition that the corresponding triax system or triax-adapter is not sold on the EC market. |
[1] OJ L 56, 6.3.1996, p.1. Regulation as last amended by Regulation (EC) No 2117/2005 (OJ L L 340, 23.12.2005, p. 17).
[2] OJ L 111, 30.4.1994, p. 106. Regulation as last amended by Council Regulation (EC) No 1754/2004 (OJ L 313, 12.10.2004, p. 1).
[3] OJ L 244, 29.9.2000, p. 38. Regulation as last amended by Council Regulation (EC) No 1454/2005 (OJ L 231, 8.9.2005, p. 1).
[4] OJ C 239, 29.9.2005, p. 9.
