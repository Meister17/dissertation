Authorisation for State aid pursuant to Articles 87 and 88 of the EC Treaty
Cases where the Commission raises no objections
(2005/C 316/10)
Date of adoption of the decision : 14.7.2004
Member State : United Kingdom (England)
Aid No : N 233/2004
Title : The Woodland Grant Scheme (England) (1988) — Restocking and Regeneration Grant
Objective : To enable woodland owners and lessees to undertake woodland restocking through investing in the replacement of woodland (i.e. replanting or regeneration)
Legal basis : The Forestry Act 1979
Budget : GBP 1,5 million per year
Aid intensity or amount : Activities will be supported at a maximum rate of 80 % of eligible costs
Duration : 1 year, starting from July 2004
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 30.11.2004
Member State : Germany
Aid No : N 243/2004
Title : Promoting the use of biodegradable lubricants
Objective : The existing aid programme N 651/2002 would be prolonged by two years. The aid promotes the initial equipment with biogenic lubricants and hydraulic fluids and the corresponding conversion of machinery and plant, so as to give companies an incentive to use these environmentally friendly materials. Increasing the market share of these products is intended to improve economic efficiency and promote product development
Legal basis : Richtlinien zur Förderung von Projekten zum Schwerpunkt Einsatz von biologisch schnell abbaubaren Schmierstoffen und Hydraulikflüssigkeiten auf Basis nachwachsender Rohstoffe
Budget : The annual budget for the aid scheme is EUR 7 million in 2005 and 5 million in 2006
Aid intensity or amount : 60 % of the eligible costs
Duration : Until 31.12.2006
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 22.12.2004
Member State : Italy (Abruzzi)
Aid No : N 261/2004
Title : Subsidised farm credit
Legal basis : Legge regionale 14 settembre 1994, n. 62 e Legge regionale 9 marzo 2004, n. 11
Objective : Regional Law No 11 of 9 March 2004 amends Regional Law No 62 of 14 September 1994 on subsidised farm credit
Budget : The regional authorities are allocating total funding of EUR 900000 for 2004
Aid intensity or amount :
Investment aid for the first purchase of livestock, materials and equipment: 50 % in less-favoured areas and 40 % in other areas.
1,10 % of the applicable interest rate
Duration : Five years, until 31 December 2009
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 18.6.2004
Member State : United Kingdom
Aid No : N 295/2003
Title : Bio-Energy Infrastructure Scheme
Objective : The objective is to support the use of renewable sources for generating electricity and heat
Legal basis : The scheme has been initiated by the UK Government as a voluntary proposal and will be established under section 153(4) of the Environmental Protection Act 1990
Budget : A total budget of GBP 3,5 million (EUR 5,1 million) is available
Aid intensity or amount : Up to 100 %
Duration : Five years, with grant aid for the first three years only
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 6.7.2004
Member State : United Kingdom
Aid No : N 498/03
Title : Industry Forum Adaptation Scheme: Cereals Scheme
Objective :
As part of the approval of N 470/99 (the Industry Forum Adaptation Scheme — hereinafter IFAS), the UK authorities undertook to apply the relevant Community Guidelines for the agricultural sector, and to notify the Commission of individual aids made in the agricultural sector. The aim of the IFAS is to promote a permanent improvement in business competitiveness by the transfer of best practice within sectors. The aim of this scheme is improved competitiveness in the cereals sector
By transferring best practice to the cereals supply chain and by improving skills of businesses active in the cereals supply chain it is the intention to improve supply chain integration and efficiency. The means to achieve this is through a targeted programme that will identify and address weaknesses in the supply chain
Legal basis : L Sections 7 and 8 of the Industrial Development Act 1982
Budget - 2003/04: GBP 200000 (approx. EUR 297000)
- 2004/05: GBP 458000 (approx. EUR 680000)
- 2005/06: GBP 549000 (approx. EUR 815000)
- 2006/07: GBP 203000 (approx. EUR 301000)
Aid intensity or amount : 50 %, with private funding making up the balance. HGCA will administer the entire project costs. No single beneficiary will receive more than EUR 100000 over 3 years
Duration : 3 years from the date of approval
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 11.6.2004
Member State : Austria
Aid No : N 499/03
Title : BSE-related aid measures in 2003
Objective : Financing measures related to BSE and disposal of animal waste and of fallen stock Aid would be paid for the costs incurred in 2003 of statutory rapid BSE- and TSE-tests on slaughter animals (bovine animals and small ruminants), transport and disposal of fallen stock within the meaning of the TSE-Guidelines or, alternatively, towards the costs of insurance premiums covering such costs and the destruction of specified risk material pursuant to the TSE-Guidelines as well as of meat and bone meal having no further commercial use
Legal basis : Sonderrichtlinie des Bundesministers für Land- und Forstwirtschaft, Umwelt und Wasserwirtschaft (BMLFUW) und der Bundesministerin für Gesundheit und Frauen (BMGF) zur Finanzierung von Maßnahmen im Zusammenhang mit der TSE- und BSE-Vorsorge, Falltieren und Schlachtabfällen
Budget : National funding of EUR 6,8 million for BSE/TSE–tests and EUR 10,9 million for other measures
Aid intensity or amount : Variable according to submeasure
Duration : One-off measure
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 14.5.2004
Member State : Italy (Autonomous Province of Trento)
Aid No : N 531/03
Title : Aid to compensate farmers for losses incurred as a result of unfavourable weather conditions (decision No 2724 of 17 October 2003)
Objective : To compensate farmers whose hay production was damaged as a result of unfavourable weather conditions
Legal basis : Deliberazione della Giunta Regionale n. 2724 del 17 ottobre 2003: Aiuti destinati ad indennizzare gli agricoltori delle perdite causate da avverse condizioni atmosferiche; criteri e modalità per l'attuazione degli interventi per la siccità dell'annata 2003
Budget : EUR 10000000
Aid intensity or amount : Maximum of 80 % of the damage
Duration : Limited to the duration of examination of applications
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 10.5.2004
Member State : Italy
Aid No : N 536/2003
Title : Regional intervention for the development of Confidi in the agricultural sector
Objective : Providing subsidiary guarantees to farmers or cooperatives and providing consultancy services to all the farmers operating in the Region, through the Confidi
Legal basis : Legge Regionale n. 13 del 25.7.2003 Interventi per lo sviluppo dei Confidi nel settore agricolo
Budget : EUR 1032913,80 for guarantees and EUR 516456,90 for technical assistance for the first year
Aid intensity or amount : The cash grant equivalent of the aid for subsidiary guarantees is calculated as the difference between market loan rates and the actual loan stipulated with the bank, following the existence of the guarantee, deduced the amount paid by the beneficiary (premium and costs related to the granting of the guarantee). For technical assistance aid is limited to EUR 100000 per beneficiary any three year period. The aid intensity is maximum 70 % of eligible expenses
Duration : 5 years
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 19.4.2004
Member State : Italy (Calabria)
Aid No : N 559/03
Title : Determining the criteria and arrangements for compensation in the fruit sector following the frost in certain municipalities of Calabria between 7 and 9 April 2003
Objective : To compensate growers whose production of stone and citrus fruit was damaged as a result of unfavourable weather
Legal basis : Deliberazione della Giunta regionale del 28 ottobre 2003, n. 824: Determinazione dei criteri e delle modalità di erogazione d'indennizzi in favore del settore frutticolo a seguito delle gelate verificatesi in alcuni comuni della Calabria dal 7 al 9 aprile 2003
Budget : EUR 1600000
Aid intensity or amount : Varying depending on the damage (determined on the basis of a formula)
Duration : 1 year.
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 12.8.2004
Member State (Region) : France
Aid No : N 731/2002 — corrigendum
Title : Aid for sheep products with official quality mark
Objective : To pay part of the inspection costs for sheep products with an official quality mark
Budget : EUR 1630000 per year
Aid intensity or amount : The rate of aid is 100 % of eligible expenditure in the first year, 83 % in the second, 67 % in the third, 50 % in the fourth, 33 % in the fifth and 17 % in the sixth year
Duration : Six years
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
--------------------------------------------------
