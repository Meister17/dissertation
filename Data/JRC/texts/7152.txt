P6_TA(2005)0022
EC-Bulgaria Europe Agreement ***
European Parliament legislative resolution on the proposal for a Council and Commission Decision on the conclusion of the Additional Protocol to the Europe Agreement establishing an association between the European Communities and their Member States, of the one part, and the Republic of Bulgaria, of the other part, to take account of the accession of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia, and the Slovak Republic to the European Union (13163/2004 — C6-0207/2004 — 2004/0815(AVC))
(Assent procedure)
The European Parliament,
- having regard to the proposal for a Council and Commission Decision (13163/2004) [1],
- having regard to the request for assent submitted by the Council pursuant to Article 300(3), second subparagraph, in conjunction with Articles 300(2), first subparagraph, second sentence and 310 of the EC Treaty (C6-0207/2004),
- having regard to Rules 75, 83(7) and 43(1) of its Rules of Procedure,
- having regard to the recommendation of the Committee on Foreign Affairs (A6-0010/2005),
1. Gives its assent to the conclusion of the Additional Protocol to the Europe Agreement with the Republic of Bulgaria;
2. Instructs its President to forward its position to the Council, the Commission and the governments and parliaments of the Member States and the Republic of Bulgaria.
[1] Not yet published in OJ.
--------------------------------------------------
