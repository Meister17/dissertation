COUNCIL DECISION of 12 July 1982 concluding the Agreement on the International Carriage of Passengers by Road by means of Occasional Coach and Bus Services (ASOR) (82/505/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 75 thereof,
Having regard to the proposal from the Commission [1],
[1] OJ No C 31, 8.2.1982, p. 1.
Having regard to the opinion of the European Parliament [2],
[2] OJ No C 182, 19.7.1982, p. 27.
Having regard to the opinion of the Economic and Social Committee [3],
[3] Opinion delivered on 26 May 1982 (not yet)
Whereas the Agreement on the International Carriage of Passengers by Road by means of Occasional Coach and Bus Services (ASOR) was negotiated between the Commission on behalf of the European Economic Community and Austria, Spain, Finland, Norway, Portugal, Sweden, Switzerland and Turkey in accordance with the negotiating directives adopted by the Council;
Whereas the ASOR will help to facilitate the operation of occasional services and will promote tourism in Western Europe,
HAS DECIDED AS FOLLOWS:
Article 1
The Agreement on the International Carriage of Passengers by Road by means of Occasional Coach and Bus Services (ASOR) together with the declarations annexed to the Final Act are hereby approved on behalf of the European Economic Community.
The texts referred to in the first paragraph are attached to this Decision.
Article 2
The President of the Council shall deposit the acts provided for in Article 18 of the Agreement [4].
[4] The date of entry into force of the Agreement will be published in the Official Journal of the European Communities by the General Secretariat of the Council.
Done at Brussels, 12 July 1982.
For the Council
The President
