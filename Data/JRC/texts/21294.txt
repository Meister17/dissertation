Commission Decision
of 20 June 2002
laying down specific conditions for imports of fishery products from the Republic of Bulgaria
(notified under document number C(2002) 2195)
(Text with EEA relevance)
(2002/472/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/493/EEC of 22 July 1991 laying down the health conditions for the production and the placing on the market of fishery products(1), as last amended by Directive 97/79/EC(2), and in particular Article 11(1) thereof,
Whereas:
(1) An inspection has been carried out on behalf of the Commission in the Republic of Bulgaria to verify the conditions under which fishery products are produced, stored and dispatched to the Community.
(2) The requirements in the legislation of Bulgaria on health inspection and monitoring of fishery products may be considered equivalent to those laid down in Directive 91/493/EEC.
(3) In particular, the "National Veterinary Service (NVS) of the Ministry of Agriculture and Forest" is capable of effectively verifying the implementation of the legislation in force.
(4) It is appropriate to lay down detailed rules concerning the health certificate which must, under Directive 91/493/EEC, accompany consignments of fishery products imported into the Community from Bulgaria. In particular those rules must specify the definition of a model certificate, the minimum requirements regarding the language or languages in which it must be drafted and the status of the person empowered to sign it.
(5) The mark which must be affixed to packages of fishery products should give the name of the third country and the approval/registration number of the establishment, factory vessel, cold store or freezer vessel of origin, except for certain frozen products.
(6) It is also necessary to draw up a list of approved establishments, factory vessels, or cold stores, and a list of freezer vessels equipped in accordance with the requirements of Council Directive 92/48/EEC(3) must also be drawn up. These lists must be drawn up on the basis of a communication from the NVS to the Commission. It is therefore for the NVS to ensure compliance with the provisions laid down to that end in Article 11(4) of Directive 91/493/EEC.
(7) The NVS has provided official assurances regarding compliance with the rules set out in Chapter V of the Annex to Directive 91/493/EEC with regard to the control of fishery products, and regarding the fulfilment of hygienic requirements equivalent to those laid down by that Directive.
(8) However the inspection visit to Bulgaria has revealed certain deficiencies on the veterinary supervision as regards identification of deficiencies in fishery establishments and in the completion of the export health certificate for fishery products and live fish. Therefore, the addition of new establishments or vessels to the list of establishments and vessels from which the imports from Bulgaria are authorised, will require a new inspection visit on the spot by the Commission experts.
(9) Taking into account the results of the inspection visit, the reduction in the frequency of physical checks, laid down by Commission Decision 94/360/EC of 20 May 1994 on the reduced frequency of physical checks of consignments of certain products to be implemented from third countries, under Council Directive 90/675/EEC(4), shall not be applied to fishery products imported from Bulgaria.
(10) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
The "National Veterinary Service (NVS) of the Ministry of Agriculture and Forest" shall be the competent authority in Bulgaria for verifying and certifying compliance of fishery products with the requirements of Directive 91/493/EEC.
Article 2
1. Fishery products imported into the Community from Bulgaria shall meet the conditions set out in paragraphs 2, 3 and 4.
2. Each consignment shall be accompanied by a numbered original health certificate, duly completed, signed, dated and comprising a single sheet in accordance with the model in Annex I.
3. The products shall come from approved establishments, factory vessels or cold stores or from registered freezer vessels listed in Annex II.
4. Except in the case of frozen fishery products in bulk and intended for the manufacture of preserved foods, all packages shall bear the word "BULGARIA" and the approval/registration number of the establishment, factory vessel, cold store or freezer vessel of origin in indelible letters.
Article 3
1. The certificate referred to in Article 2(2) shall be drawn up in at least one official language of the Member State in which the checks are carried out.
2. The certificate shall bear the name, capacity and signature of the representative of the NVS and the latter's official stamp in a colour different from that of other endorsements.
Article 4
Member States, when importing fishery products from Bulgaria, shall not apply to these products the reduction in the frequency of physical checks provided by Decision 94/360/EC.
Article 5
1. Annex II shall only be modified following the results of an inspection visit on the spot.
2. By derogation from paragraph 1, Annex II may be modified following the procedure laid down by Decision 95/408/EC to change the name or to delete establishments and vessels included in the list of this Annex.
Article 6
This Decision shall apply from 24 June 2002.
Article 7
This Decision is addressed to the Member States.
Done at Brussels, 20 June 2002.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 268, 24.9.1991, p. 15.
(2) OJ L 24, 30.1.1998, p. 31.
(3) OJ L 187, 7.7.1992, p. 41.
(4) OJ L 158, 25.6.1994, p. 41.
ANNEX I
>PIC FILE= "L_2002163EN.002602.TIF">
>PIC FILE= "L_2002163EN.002701.TIF">
ANNEX II
LIST OF ESTABLISHMENTS AND VESSELS
>TABLE>
PP: Etablissement/Processing Plant
