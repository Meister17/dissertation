Commission Regulation (EC) No 1925/2000
of 11 September 2000
establishing the operative events for the exchange rates to be applied when calculating certain amounts provided for by the mechanisms of Council Regulation (EC) No 104/2000 on the common organisation of the market in fishery and aquaculture products
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 2799/98 of 15 December 1998 establishing agrimonetary arrangements for the euro(1), and in particular Article 3(2) thereof,
Whereas:
(1) All the specific definitions of operative events for the exchange rates for calculations provided for by the mechanisms of Council Regulation (EC) No 104/2000 of 17 December 1999 on the common organisation of the market in fishery and aquaculture products(2) should be collated in a single Regulation.
(2) Regulation (EC) No 104/2000 has made a number of changes to the aids available to producers' organisations, in particular the introduction of the operational programmes and amendments to the private storage aid mechanisms. The previous implementing regulation, Commission Regulation (EC) No 3516/93(3), as amended by Regulation (EC) No 963/1999(4), does not have provisions on exchange rates to be applied for these new mechanisms. For these reasons, Regulation (EC) No 3516/93 should be replaced by this Regulation.
(3) The revised agrimonetary arrangements outlined in Regulation (EC) No 2799/98 support the need to replace Regulation (EC) No 3516/93 since references to the agrimonetary conversion rate are no longer applicable.
(4) The operative events for financial compensation and carry-over aid should be consistent with the operative events for withdrawal prices and other amounts involved in their calculation.
(5) The possibility for producers' organisations to use a margin of tolerance in the application of the withdrawal price and other prices in the common organisation of the markets in fishery and aquaculture products is constrained by the necessity of informing the Member State the level of the modified price at least two days before it is due to become applicable. Producers' organisations must therefore know the exchange rate for these prices in advance of the period for which it is to be applicable in order that they can meet this obligation to inform the competent authorities in advance. The operative event must be set before the period of its application.
(6) An operative event should be introduced for the financial compensation for the operational programmes. This should be the first day of the fishing year so that the producers' organisation can know the level of aid in advance.
(7) An operative event should be established for the rate of exchange to be applied to the different prices notified to the Commission in the context of the common organisation of the market. This operative event must correspond to a single day of the period for which the price is calculated. Since practical use is made of this information a posteriori, the operative event should be established as the last day of the period for which the price is calculated.
(8) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Fishery Products,
HAS ADOPTED THIS REGULATION:
Article 1
This Regulation establishes the operative events for the exchange rates to be used for the mechanisms laid down in Regulation (EC) No 104/2000.
The exchange rate to be used shall be the rate most recently fixed by the European Central Bank (ECB) prior to the operative event as set out in Article 1 of Commission Regulation (EC) No 2808/98(5).
Article 2
For the fisheries sector, the operative event for the exchange rate for the withdrawal price and for the amounts linked to that price, which are listed in Annex I, shall be the 22nd day of the month before the operation took place.
Article 3
The operative event for the exchange rate applicable to the financial compensation provided for in Article 21 of Regulation (EC) No 104/2000 shall be the 22nd day of the month before the operation took place.
Article 4
The operative event for the exchange rate applicable to the carry-over aid provided for in Article 23 of Regulation (EC) No 104/2000, to the flat-rate aid provided for in Article 24(4) of the same Regulation and to the private storage aid provided for in Article 25 of the same Regulation shall be the 22nd day of the month before the stored products were withdrawn.
Article 5
The operative event for the exchange rate applicable to the financial compensation for operational programmes as provided for in Article 10(2) of Regulation (EC) No 104/2000 shall be the first day of the fishing year for which the operational programme was established.
Article 6
The operative event for the exchange rate applicable to the compensatory allowance for tuna for canning provided for in Article 27(1) of Regulation (EC) No 104/2000 shall be the second day of the month the product is delivered.
Article 7
In all cases where an advance may be granted in respect of measure provided for in Regulation (EC) No 104/2000, the operative event for the exchange rate shall be the event corresponding to the measure taken, as set out in Articles 2 to 6.
Article 8
The exchange rate applicable to average market prices notified under Commission Regulation (EEC) No 2210/93(6) shall be the exchange rate in force on the last day of the period for which the price is calculated.
Article 9
Regulation (EC) No 3516/93 is hereby repealed.
References to the repealed Regulation (EC) No 3516/93 shall be construed as referring to this Regulation and should be read in accordance with the correlation table in Annex II.
Article 10
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Communities.
It shall be applicable from 1 January 2001.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 11 September 2000.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 349, 24.12.1998, p. 1.
(2) OJ L 17, 21.1.2000, p. 22.
(3) OJ L 320, 22.12.1993, p. 10.
(4) OJ L 119, 7.5.1999, p. 26.
(5) OJ L 349, 24.12.1998, p. 36.
(6) OJ L 197, 6.8.1993, p. 8.
ANNEX I
1. Community withdrawal price provided for in Article 20(1) of Regulation (EC) No 104/2000.
2. Community selling price provided for in Article 22 of Regulation (EC) No 104/2000.
3. Standard value to be deducted from financial compensation as provided for in Article 21(5) of Regulation (EC) No 104/2000.
4. Unit amount of Community carry-over aid provided for in Article 23 of Regulation (EC) No 104/2000.
5. Unit amount of flat-rate aid for autonomous withdrawal provided for in Article 24(4) of Regulation (EC) No 104/2000.
6. Community selling price for private storage aid provided for in Article 25(1) of Regulation (EC) No 104/2000.
ANNEX II
>TABLE>
