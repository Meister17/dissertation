Commission Regulation (EC) No 1051/2005
of 5 July 2005
amending Regulation (EC) No 1622/1999 laying down detailed rules for applying Council Regulation (EC) No 2201/96 as regards the scheme for the storage of unprocessed dried grapes and unprocessed dried figs
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 2201/96 of 28 October on the common organisation of the markets in processed fruit and vegetable products [1], and in particular Article 9(8) thereof,
Whereas:
(1) Article 9 of Regulation (EC) No 2201/96 introduces storage arrangements for unprocessed dried grapes and unprocessed dried figs during the final two months of the marketing years of those products. The arrangements consist of a system for approving the storage agencies and paying them storage aid and financial compensation. Commission Regulation (EC) No 1622/1999 [2] establishes the conditions which the storage agencies must meet in order to be approved, in particular as regards the steps they must take to ensure that the products are stored properly.
(2) The second indent of the second subparagraph of Article 2(2) of Regulation (EC) No 1622/1999 establishes transitional measures for unprocessed dried figs applying until the end of the 2003/04 marketing year.
(3) Weather conditions during part of the 2004/05 marketing year were poor and fruit was unable to develop satisfactorily. As a result, some producers chose to harvest their fruit at a size slightly smaller than the standard but meeting the other quality conditions required for human consumption. In order to prevent those quantities being diverted to the production of fig paste, which would mean a substantial financial loss, it is necessary to extend the abovementioned transitional measures and provide for an additional minimum size for the 2004/05 marketing year.
(4) Article 4 of Regulation (EC) No 1622/1999 provides for the products held by storage agencies to be sold by tender procedure. The intended use for unprocessed dried figs is a specific industrial use to be specified in the tender notice by the competent authority. In view of the lack of economic interest engendered by that approach no tenders were submitted so the competent authorities should be given the opportunity to expand the range of possible uses of the products to be sold from storage by adding direct animal feed and use in composting and biodegrading processes.
(5) It is necessary to establish the physical and documentary check procedures for these new uses both at the time of entry into storage and at the time of removal.
(6) Regulation (EC) No 1622/1999 should therefore be amended accordingly.
(7) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Products Processed from Fruit and Vegetables,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 1622/1999 is hereby amended as follows:
1. In Article 2, paragraph 2 is replaced by the following:
"2. The products shall be delivered to the storage agencies in stackable plastic crates. However, on a transitional basis, unprocessed dried grapes may be delivered in suitable containers until the end of the 2001/02 marketing year, and unprocessed dried figs until the end of the 2004/05 marketing year.
The delivered products must:
- in the case of unprocessed dried grapes, meet the minimum requirements laid down in the Annex to Regulation (EC) No 1621/1999,
- in the case of unprocessed dried figs, meet the minimum requirements laid down in Annex II to Commission Regulation (EC) No 1573/1999 [3] and have a minimum size of 180 fruits per kg up to the end of the 2004/05 marketing year and 150 fruits per kg for subsequent marketing years.
2. Article 4 is amended as follows:
(a) In paragraph 1, point (a) is replaced by the following:
"(a) unprocessed dried figs shall be sold for a specific industrial use or a use as referred to in paragraph 3, to be specified in the notice of invitation to tender;"
(b) The following paragraph 3 is added:
"3. After notifying the Commission of the justified reasons why the uses provided for in paragraph 2 were not possible, Member States may authorise the storage agencies to permit the following uses for unprocessed dried figs:
(a) distribution to animals;
(b) use in composting and biodegrading processes respecting the environment, in particular water quality and the countryside."
3. Article 10 is amended as follows:
(a) In paragraph 1, the following subparagraph is added:
"In the event of the uses referred to in Article 4(3), the checks provided for in points (b) and (c) of the first subparagraph of this paragraph shall cover 100 % of each consignment removed from storage during the marketing year. After those checks the products removed from storage shall be denatured under the conditions laid down by the Member State and in the presence of the competent authorities."
(b) Paragraph 2 is replaced by the following:
"2. The competent authority shall withdraw approval where any of the conditions for approval are no longer met. No storage aid or financial compensation shall be paid in respect of the marketing year in progress and amounts already paid shall be reimbursed, plus interest for the time elapsing between payment and reimbursement.
The rate of interest shall be that applied by the European Central Bank to its transactions in euro, as published in the "C" series of the Official Journal of the European Union and in force on the date of the wrong payment, plus three percentage points."
Article 2
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 5 July 2005.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 297, 21.11.1996, p. 29. Regulation as last amended by Regulation (EC) No 386/2004 (OJ L 64, 2.3.2004, p. 25).
[2] OJ L 192, 24.7.1999, p. 33.
[3] OJ L 187, 20.7.1999, p. 27."
--------------------------------------------------
