[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 26.10.2005
SEC(2005) 1379 final
AMENDING LETTER N° 2 TO THE PRELIMINARY DRAFT BUDGET FOR 2006
GENERAL STATEMENT OF REVENUESTATEMENT OF REVENUE AND EXPENDITURE BY SECTIONSection III – Commission
AMENDING LETTER N° 2 TO THE PRELIMINARY DRAFT BUDGET FOR 2006
GENERAL STATEMENT OF REVENUESTATEMENT OF REVENUE AND EXPENDITURE BY SECTIONSection III – Commission
Having regard to:
- the Treaty establishing the European Community, and in particular Article 272 thereof,
- the Treaty establishing the European Atomic Energy Community, and in particular Article 177 thereof,
- the Council Regulation (EC, Euratom) No 1605/2002 of 25 June 2002 on the Financial Regulation applicable to the general budget of the European Communities[1], and in particular Article 34 thereof,
the European Commission hereby presents to the budgetary authority the amending letter No 2 to the preliminary draft budget for 2006 for the reasons set out in the explanatory memorandum.
TABLE OF CONTENTS
1. Introduction 4
2. Heading 1: Agriculture 5
2.1. Subheading 1a: CAP expenditure except for rural development 7
2.1.1. The market organisations and related expenditure 7
2.1.2. Euro-dollar exchange rate 7
2.2. Modification of the remarks of budget line 17 04 02 12
2.3. Changes in the nomenclature 13
SUMMARY TABLE BY HEADING OF THE FINANCIAL PERSPECTIVE 14
GENERAL STATEMENT OF REVENUE
STATEMENT OF REVENUE AND EXPENDITURE BY SECTION
The general statement of revenue and the statement of revenue and expenditure by section are forwarded separately via the SEI-BUD system. An English version of the general statement of revenue and of the statement of revenue and expenditure by section is attached as a budgetary annex by way of example.
EXPLANATORY MEMORANDUM
1. INTRODUCTION
The Interinstitutional Agreement on budgetary discipline and improvement of the budgetary procedure signed by the European Parliament, the Council and the Commission on 6 May 1999[2] provides that, "if it considers it necessary, the Commission may present to the budgetary authority an ad hoc letter of amendment to update the figures underlying the estimate of agricultural expenditure in the preliminary draft budget and/or to correct, on the basis of the most recent information available concerning fisheries agreements in force on 1 January of the financial year concerned, the breakdown between the appropriations entered in the operational items for international fisheries agreements and those entered in reserve." This letter of amendment must be sent to the budgetary authority by the end of October.
The Commission presents herewith this ad hoc amending letter (AL) to the preliminary draft budget 2006, containing a careful, line by line update on the estimated needs for agricultural expenditure. In addition to changing market factors, the AL also incorporates legislative decisions adopted in the agricultural sector since the PDB was drawn up, as well as any proposals, which are expected to have effect during the coming budget year.
The AL is based, in the same way as the PDB itself, on the needs of the Community as a whole. As far as the market measures are concerned, no breakdown of the appropriations between Member States is available. In addition, it must be stressed that these appropriations are to be understood as a forecast and not as an objective of expenditure. The actual expenditure will depend, in particular, on real market conditions, on the actual euro-dollar exchange rate, and on the rhythm of the payments by Member States. Since this is compulsory expenditure, whatever the amount a Member State is obliged to pay in accordance with the regulations will be reimbursed in full.
The euro-dollar rate used, in accordance with the Council Regulation on Budgetary Discipline, is based on the average rate observed between 1 July and 30 September 2005. It comes to 1,22 (EUR 1=USD 1,22) and results in a decrease in needs of about EUR 169 million compared to the PDB, in particular for cereals, sugar and cotton.
Overall needs for subheading 1a (CAP expenditure excluding rural development) are estimated at EUR 43 280 million, EUR 362 million lower than in the PDB, leaving a margin of EUR 1 567 million below the ceiling of the Financial Perspectives.
This decrease results mainly from the chapter 05 02 “Interventions on agricultural markets” (- EUR 350 million). For plant products, lower needs are foreseen for cereals (- EUR 136 million), for rice (- EUR 22 million), sugar (- EUR 122 million) and textile plants (- EUR 28 million). These are offset by a forecast increase for fruits and vegetables (+ EUR 110 million) and wine (+ EUR 165 million). For animal products there are lower needs for milk and milk products (- EUR 285 million) in addition to forecast lower spending for beef and veal (- EUR 80 million). Increased needs are also foreseen for food programmes (+ EUR 48 million). The appropriations for decoupled direct aids remain unchanged.
For rural development (subheading 1b), the PDB estimates for commitment and payment appropriations remain unchanged at EUR 7 771 million and EUR 7 711 million respectively. For the 15 “old” Member States, the level of commitment appropriations is set at the ceiling of the Financial Perspective. For the 10 new Member States, the commitment appropriations proposed correspond to the amount agreed in Copenhagen.
2. HEADING 1: AGRICULTURE
The purpose of the AL n°2/2006 is to ensure that the agricultural budget is based on the most up-to-date economic data and legislative framework. By the month of October, the Commission has at its disposal a first indication of the level of production (harvests) for 2005, which is the basis for any reliable estimate of the budgetary needs for 2006.
As in the past, the Commission has carefully reviewed all its estimates of agricultural expenditure line by line. Besides taking into account market factors, AL n°2/2006 also incorporates any legislative decisions adopted in the agricultural sector since the PDB was drawn up, as well as proposals.
The following table sums up the budgetary forecasts for the various chapters of the EAGGF Guarantee Section, and shows the margins under the Financial Perspectives.
Letter of amendment No 2/2006
Heading 1
PDB 2006 | AL 2006 | Difference |
1€ = 1,31 $ | 1€ = 1,22 $ |
(a) | (b) | (c)=(a)-(b) |
05 01 | Administrative expenditure of policy area 'Agriculture' |
05 01 04 01 | Monitoring and prevention – Direct payments by EC | 3,5 | 3,5 | 0 |
05 01 04 06 | Pilot project on quality promotion | p.m. | p.m. | p.m. |
05 01 04 07 | Pilot project on security fund in the sector of fruits and vegetables | 0,5 | 0,5 |
Total | 3,5 | 4,0 | 0,5 |
05 02 | Interventions in agricultural markets |
05 02 01 | Cereals | 836 | 700 | -136 |
05 02 02 | Rice | 26 | 4,0 | -22 |
05 02 03 | Refunds on non-Annex 1 products | 415 | 415 | 0 |
05 02 04 | Food programmes | 220 | 268 | 48 |
05 02 05 | Sugar | 1.498 | 1.376 | -122 |
05 02 06 | Olive oil | 35 | 35 | 0 |
05 02 07 | Textile plants | 997 | 969 | -28 |
05 02 08 | Fruit and vegetables | 1.434 | 1.544 | 110 |
05 02 09 | Products of the wine-growing sector | 1.329 | 1.494 | 165 |
05 02 10 | Promotion | 52 | 52 | 0 |
05 02 11 | Other plant products/measures | 297,6 | 297,6 | 0 |
05 02 12 | Milk and milk products | 1.143 | 858 | -285 |
05 02 13 | Beef and veal | 468 | 388 | -80 |
05 02 14 | Sheepmeat and goatmeat | p.m. | p.m. | 0 |
05 02 15 | Pigmeat, eggs and poultry, bee-keeping and others | 164 | 164 | 0 |
05 02 99 | Recoveries | -56 | -56 | 0 |
Total | 8.859 | 8.509 | -350 |
05 03 | Direct aids |
05 03 01 | Decoupled direct aids | 16.375 | 16.375 | 0 |
05 03 02 | Other direct aids | 18.118 | 18.106 | -12 |
05 03 03 | Additional amounts of aid | 347 | 347 | 0 |
05 03 04 | Ancillary direct aids (reliquats, small producers, agrimonetary aids, etc.) | p.m. | p.m. | 0 |
05 03 99 | Recoveries | -11 | -11 | 0 |
Total | 34.829 | 34.817 | -12 |
05 04 | Rural Development |
05 04 01 | Rural Development in EAGGF-Guarantee | 5.675 | 5.675 | 0 |
05 04 04 | Transitional instrument in EAGGF-Guarantee for EUR-10 | 2.096 | 2.096 | 0 |
Total | 7.771 | 7.771 | 0 |
Audit of Agricultural Expenditure |
05 07 01 01 | Monitoring and preventive measures — Payments by MS | p.m. | p.m. | 0 |
05 07 01 02 | Monitoring and preventive measures — Direct payments by EC | 9,4 | 9,4 | 0 |
05 07 01 06 | Accounting clearance of previous years' accounts (sub-heading 1a) | -70 | -70 | 0 |
05 07 01 07 | Conformity clearance of previous years' accounts (sub-heading 1a) | -330 | -330 | 0 |
05 07 01 08 | Accounting clearance of previous years' accounts (sub-heading 1b) | p.m. | p.m. | 0 |
05 07 01 09 | Conformity clearance of previous years' accounts (sub-heading 1b) | p.m. | p.m. | 0 |
05 07 02 | Settlement of disputes | p.m. | p.m. | 0 |
Total | -391 | -391 | 0 |
Policy strategy and coordination |
05 08 06 | Enhancing public awareness of the common agricultural policy | 6,5 | 6,5 | 0 |
Total | 6,5 | 6,5 | 0 |
Other Policy Areas |
11 02 | Fisheries markets | 33,2 | 33,2 | 0 |
17 01 04 01 | Plant-health measures - administration | 0,1 | 0,1 | 0 |
17 01 04 04 | Pilot study: risk financing model for livestock epidemics | p.m. | p.m. | 0 |
17 01 04 05 | Feed and food safety and related activities - Administration | 8,0 | 8,0 | 0 |
17 03 02 | Community tobacco fund - Direct payments by EC | 14,6 | 14,6 | 0 |
17 04 01 | Animal disease eradication and monitoring programmes | 209,5 | 209,5 | 0 |
17 04 02 | Other veterinary/animal welfare/public health measures | 10 | 10 | 0 |
17 04 03 | Emergency fund (animal health/food safety) | 48 | 48 | 0 |
17 04 04 | Plant-health measures | 2,5 | 2,5 | 0 |
17 04 05 | Other measures | p.m. | p.m. | 0 |
17 04 07 | Control animal feed and food | 8,5 | 8,5 | 0 |
Total | 334 | 334 | 0 |
Sub-Heading 1a – Agricultural expenditure-excl. rural development |
Sub-total | 43.641 | 43.280 | -362 |
Sub-ceiling | 44.847 | 44.847 | 0 |
Margin | -1.206 | -1.567 | -362 |
Sub-Heading 1b - Rural development and accompanying measures |
Sub-total | 7.771 | 7.771 | 0 |
Sub-ceiling | 7.771 | 7.771 | 0 |
Margin | 0 | 0 |
Total Appropriations Heading 1 | 51.412 | 51.051 | -362 |
2.1. Subheading 1a: CAP expenditure except for rural development
2.1.1. The market organisations and related expenditure
In the AL n°2/2006, the total appropriations for subheading 1a (CAP expenditure excluding rural development) are lower than estimated in the PDB (- EUR 362 million). This saving is the result of a combination of increases and reductions across the different sectors.
Most significant is the reduction in needs for milk and milk products. This is largely due to the favourable market conditions triggering lower expenses for the different market measures. Overall, the savings compared to the PDB 2006 in this sector are estimated at EUR 285 million. A reduction in credits of EUR 122 million is also entered for the sugar sector mainly due to some revenues which will arise from the sale of sugar out of Community stocks and the changed euro-dollar rate. The sugar in intervention was already depreciated in 2005 so the budgetary cost per tonne for export is less than that necessary for exporting from the free market. This sugar, bought during the 2004-2005 campaign, has led to additional spending in budget 2005. The decrease in appropriations for cereals (- EUR 136 million) is mainly due to the changed euro-dollar rate and a lower than forecasted cereals harvest. Furthermore, lower expenditure (- EUR 80 million) is anticipated for the beef and veal sector where strong internal demand has reduced the quantities to be exported with refunds. A decrease in aid for cotton results in savings for textile plants of EUR 28 million.
In contrast, there are increases in a number of sectors. The wine sector is absorbing EUR 165 million more appropriations than initially foreseen because of additional crisis distillation and measures for the permanent abandonment of areas under vines. Also, the increasing needs for the operational funds for producer organisations are responsible for higher expenses for the fruit and vegetable sector, where credits rise by EUR 110 million. Finally, for food programmes increased participation of Member States results in EUR 48 million more in budgetary needs.
The Commission has also taken into account some of the opinions expressed by the budgetary authority during its first reading. In order to incorporate these wishes into the AL the Commission proposes the following modifications:
- EUR 500 000 will be made available under 05 01 04 07 Pilot project to conduct a feasibility study on introducing a security fund in the fruit and vegetables sector.
- EUR 300 000 will be made available under 05 04 03 01 to finance a study of the means to combat the dying back of forests in the European Union (under Heading 3 of the Financial Perspective).
2.1.2. Euro-dollar exchange rate
In accordance with Article 8(1) of Council Regulation (EC) n°2040/2000 on budgetary discipline, the euro-dollar rate to be used for the letter of amendment is the average rate over the most recent three-month period. In the case of this letter of amendment, this means the period between 1 July and 30 September 2005. The average exchange rate recorded is EUR 1 = USD 1,22 which is lower than the rate used for the preliminary draft (EUR 1 = USD 1,31). This change in the rate has the effect of reducing needs by about EUR 169 million (- EUR 94 million for market measures for cereals, - EUR 55 million for sugar, - EUR 2 million for rice and – EUR 18 million for cotton).
The comments below explain the main differences between the appropriations of the PDB and those of the AL.
05 01 04 07 – Pilot project on security fund in the sector of fruits and vegetables (appropriations + EUR 0,5 million ) (NEW LINE)
appropriations in preliminary draft budget: none
appropriations after letter of amendment : EUR 0,5 million
The purpose of this pilot project is, in particular, to study the feasibility of:
- introducing a Community system for forecasting fruit and vegetable production by means of market monitoring centres, at both Community and national level, tasked with anticipating crises in order to speed up response capabilities;
- introducing a security fund, managed by producer organisations and complementing the present withdrawal scheme, to be used in the event of a crisis (large-scale price collapse, climatic disasters, etc.)
05 02 01 – Cereals (appropriations – EUR 136 million)
appropriations in preliminary draft budget: EUR 836 million
appropriations after letter of amendment: EUR 700 million
The decrease in needs for market measures for cereals is a result of two distinct effects: approximately EUR 94 million can be attributed to the ‘euro-dollar’ effect. The remaining EUR 42 million of savings is due to a ‘market effect’. These downward adjustments are made for intervention storage of cereals (- EUR 72 million) and for export refunds for cereals (- EUR 65 million). Needs for intervention for starch increase by EUR 1 million.
Following the latest forecasts, cereals production in EU-25 for 2005/6 is expected to be approximately 11 million tonnes lower than foreseen in the PDB. Despite the lower than forecast cereals harvest, purchases into intervention are estimated at a higher level and sales at a lower level than in the PDB. Still, economies are made on intervention storage of cereals due to a downward estimation of transport and depreciation costs. The table below illustrates the present situation as compared to that of the PDB.
PDB (million t) | AL (million t) |
Opening Stocks Purchases Sales Closing Stocks Average Stocks | 15.0 5.5 10.8 9.7 14.2 | 14.5 6.5 6.1 14.9 15.2 |
As to export refunds , whilst world dollar prices for most cereals are estimated to remain rather stable, the lower euro-dollar exchange rate has contributed to decreased refund rates in EUR/tonne. Also the reduced export level enters into the equation. So , for instance, export refunds for barley decrease (- EUR 39 million) because less exports will be supported at a lower refund rate.
05 02 02 – Rice ( appropriations - EUR 22 million)
appropriations in preliminary draft budget: EUR 26 million
appropriations after letter of amendment: EUR 4 million
Improved market prospects for rice brought about a decrease in budgetary needs for rice reflected, for instance, in changing patterns in intervention. As the table below illustrates, a lower opening stock, no forecasted purchases and increased sales out of intervention lead to a lower level of closing stocks than foreseen in the PDB. Hence the budgetary needs decrease by EUR 15 million.
PDB (tonnes) | AL (tonnes) |
Opening Stocks Purchases Sales Closing Stocks Average Stocks | 472 000 75 000 150 000 397 000 440 858 | 302 000 0 200 000 102 000 202 000 |
Due to a further reduction of the refund rate in EUR/tonne paid for a smaller quantity of rice exported, the needs for export refunds on rice are expected to decrease by EUR 7 million compared to the PDB.
05 02 04 – Food programmes (appropriations + EUR 48 million)
appropriations in preliminary draft budget: EUR 220 million
appropriations after letter of amendment: EUR 268 million
Given a much higher uptake in 2005, the inclusion of new Member States in the programme and following requests from old Member States with a good record of execution, the needs for programmes for deprived persons are increased by EUR 48 million.
05 02 05 – Sugar (appropriations - EUR 122 million)
appropriations in preliminary draft budget: EUR 1 498 million
appropriations aft er letter of amendment: EUR 1 376 million
The decrease in appropriations for the sugar sector is a result of mixture of events affecting the market situation. The Commission expects that 800 thousand tonnes of sugar will be put into intervention in addition to the 700 000 tonnes already in stock at 1 October 2005. This will lead to a net increase in the line 05 02 05 99 (Other measures) of EUR 271 million.
However, it is foreseen that most of the sugar in stock will be sold by the Commission, mainly through refunded exports (1 000 000 tonnes). Therefore, in order to respect the ceiling for exports set by the WTO, the operators will be allowed to export with refunds only a further 210 000 tonnes (in comparison with the hypothesis made in the PDB of 1,1 million tonnes). This fact, coupled with the lower euro-dollar exchange, leads to a saving of EUR 412 million for the line 05 02 05 01 (Export refunds).
05 02 07 – Textile plants (appropriations – EUR 28 million)
appropriations in preliminary draft budget: EUR 997 million
appropriations after letter of amendment: EUR 969 million
The decrease in appropriations for this budget line is due to reduced needs for the aid of cotton mainly as a result of a lower dollar exchange rate which influences the level of aid paid for cotton.
The overshoot of the Total Guaranteed Quantity for cotton in the three producing countries (Greece, Spain and Portugal) will be lower than envisaged at the time of the PDB. This causes a downward adjustment of the penalty-percentage applied, and thus a higher amount in aid per tonne. However, the higher world market price for cotton decreases the needs for cotton aid, an effect which is reinforced by the more favourable euro-dollar rate. This results in savings for the budget of EUR 28 million.
05 02 08 – Fruit and vegetables (appropriations + EUR 110 million)
appropriations in preliminary draft budget: EUR 1 434 million
appropriations after letter of amendment: EUR 1 544 million
Forecast expenditure for fruit and vegetables is higher vis-à-vis the PDB due to increasing needs for producer organisation and withdraws which are not sufficiently counterbalanced by savings for aids for processed fruits.
The appropriations for compensation for withdrawals have slightly increased (EUR 9 million). Due to the availability of the latest figures for the current campaign, the estimated needs for the 2006 budget have been slightly modified.
The appropriations for the producer organisations show a big increase (EUR 171 million) because of the availability of the Member States' forecastson their operational plans for 2005. Overall the 2005 plans could be 54% higher than the 2004 plans.
The appropriations for processed tomatoes show a decrease in the needs of EUR 14 million. This is due to slight decrease of the foreseen quantities to be processed.
The appropriations for processed citrus fruits have slightly decreased (EUR 13 million), in particular, because of a further reduction in the aid (overshoot of the ceiling for oranges).
05 02 09 – Products of the wine-growing sector (appropriations + EUR 165 million)
appropriations in preliminary draft budget: EUR 1 329 million
appropriations after letter of amendment: EUR 1 494 million
Due to a large harvest in the campaign 2004/05, wine production has substantially increased, leading to a surplus situation at the end of the marketing year. Crisis distillation in several Member States has already been decided for 8 million hl at the end of the 2005 budget year. However, for budgetary reasons, it has been decided to postpone all the related payments to 2006. Furthermore, it is estimated that 4 million hl will need to be distilled in 2006, leading in total to 12 million hl. Compared to the 8 million hl forecast in the PDB, this leads to EUR 114 million more. Updated figures for storage of alcohol, as a result of the new estimates for distillation, explain an additional need of EUR 5 million.
Despite a lower production expected at EU level for the campaign 2005/06, the particular situation in certain wine-growing regions will require reinforced measures for the permanent abandonment of areas under vines. The additional burden on the 2006 budget is estimated at EUR 46 million.
05 02 12 – Milk and milk products (appropriations – EUR 285 million)
appropriations in preliminary draft budget: EUR 1 143 million
appropriations after letter of amendment: EUR 858 million
Overall, expenditure on milk and milk products is expected to be EUR 285 million lower compared to the PDB. This is largely due to the favourable market conditions triggering lower expenses for the different market measures.
Forecasts for subsidised exports have been revised downwards, and lower levels of export refunds can be assumed for most dairy products; this results in savings of EUR 202 million.
Updated stock figures , combined with revised assumptions on storage costs, lead to a small additional amount of EUR 5 million for skimmed-milk powder. This is more than compensated for by savings of EUR 26 million for butter, resulting in net savings of EUR 21 million for storage measures.
Further savings of EUR 63 million are the consequence of revised assumptions on internal disposal measures , both for skimmed milk (- EUR 52 million) and butterfat (- EUR 11 million). The decrease for skimmed milk is explained by lower quantities of skimmed milk powder used as feed for calves. On the other hand, more skimmed milk is expected to be used for the production of casein with, however, a lower aid level. The saving for butterfat disposal measures is the consequence of lower quantities entering into the different schemes thanks to the good market prospects for the dairy sector.
Finally , a small amount of EUR 1million has been added to the budget item 05 02 12 99 'Other measures (milk and milk products)' to cover some outstanding payments related to earlier measures in the sector, in particular to compensate several producers of milk and milk products temporarily restricted in carrying-out their trade ('SLOM producers').
05 02 13 – Beef and veal (appropriations - EUR 80 million)
appropriations in preliminary draft budget: EUR 468 million
appropriations after letter of amendment : EUR 388 million
The beef and veal sector has well recovered from the different crises in the past. Strong demand combined with a more stable production, has lead to a situation in which the EU has become net importer. The savings compared to the PDB proposals concern the two budget items financing export refunds and are the result of fewer quantities to be exported with refunds and lower refund rates. For meat exports (05 02 13 01), savings are estimated at EUR 60 million and for live animals (05 02 13 04) at EUR 17 million.
An additional small saving of EUR 3 million comes from budget item 05 02 13 99 for which some outstanding payments related to earlier BSE measures were expected. However, since there were no payments so far in 2005, the Commission now proposes a ‘p.m.’ for this budget item.
05 03 02 – Other direct aids (appropriations – EUR 12,1 million)
appropriations in preliminary draft budget: EUR 18 118 million
appropriations af ter letter of amendment: EUR 18 106 million
The appropriations for the olive oil production aid (05 03 02 21) have been increased by EUR 92 million due to the updating of the eligible production levels. The AL requirements are based on the latest estimates of production sent by Member States for the campaign 2004/2005. In particular, production for Greece changes from 0,434 to 0,481 million tonnes, for Spain from 0,984 to 1,118 million tonnes and for Italy 0,769 to 0,952 million tonnes. (See Commission Regulation fixing the estimated production of olive oil and the unit amount of the production aid that may be paid in advance for the 2004/2005 marketing year). Also tobacco premiums will cost EUR 1 million more than foreseen in the PDB.
Following the latest communications received from Member States, reductions in the appropriations have been introduced for the specific quality premium for durum wheat (- EUR 20 million) , the protein crop premium (- EUR 10,5 million) and aid for energy crops (- EUR 23,3 million).
The appropriations for banana compensatory aid (05 03 02 30) show a saving of EUR 51 million because of the change in the foreseeable amount of the compensatory aid for the production 2005, due to the high prices, and because of the modified assumption for the 2005 and 2006 production. The 2005 production falls from 780 000 tonnes to 720 000 tonnes; the 2006 production from 780 000 tonnes to 740 000 tonnes.
05 04 03 01 – Forestry (Outside the EAGGF) (appropriations + EUR 0,3 million)
appropriations in preliminary draft budget: p.m
appropriations after letter of amendment: 300 000
The additional money will be used to finance:
- a detailed study of the main causes of the dying back of forest in the European Union, with particular reference to fires and atmospheric pollution, and of possible ways of reducing their occurrence;
- an analysis of the impact of the fire-prevention measures introduced as part of rural development policy in the Member States and of how to make the best possible use of available resources, mainly by improving coordination between Member States and between regions.
2.2. Modification of the remarks of budget line 17 04 02
Regulation (EC) No. 178/2002 of the European Parliament and of the Council of 28 January 2002 (feed and food law), and in particular Article 50 thereof, envisages the establishment of a rapid alert system for the notification of a direct or indirect human health risk deriving from food and from feeding stuffs.
The 2006 PDB, and in particular budget line 17 04 02, makes reference to the aforesaid Regulation. However the reference must be limited to the implementation of article 50.
It is therefore proposed to modify the budgetary comment of this line, adding the reference to art. 50 of Regulation (EC) No. 178/2002 .
2.3. Changes in the nomenclature
Line | Action | Credits 2006 EUR |
05 01 04 07 Pilot project on security fund in the sector of fruits an vegetables | New line | 500 000 |
SUMMARY TABLE BY HEADING OF THE FINANCIAL PERSPECTIVE
Financial perspective Heading/subheading | 2006 Financial perspective | APB 2006 (including AL n°1/2006) | AL 2/2006 | APB 2006 + AL 1 and 2/2006 |
|CA |PA |CA |PA |CA |PA |CA |PA | |1. AGRICULTURE | | | | | | | | | |- Agricultural expenditure |45 502 000 000 | |43 641 320 000 |43 641 320 000 |-361 600 000 |-361 600 000 |43 279 720 000 |43 279 720 000 | |- Rural development and accompanying measures |7 116 000 000 | |7 771 000 000 |7 711 300 000 | | |7 771 000 000 |7 711 300 000 | |Total |52 618 000 000 | |51 412 320 000 |51 352 620 000 | | |51 050 720 000 |50 991 020 000 | | Margin | | |1 205 680 000 | | | |1 567 280 000 | | |2. STRUCTURAL OPERATIONS | | | | | | | | | |- Structural funds |38 523 000 000 | |38 522 922 880 |32 134 099 237 | | |38 522 922 880 |32 134 099 237 | |- Cohesion fund |6 094 000 000 | |6 032 082 110 |3 505 500 000 | | |6 032 082 110 |3 505 500 000 | |Total |44 617 000 000 | |44 555 004 990 |35 639 599 237 | | |44 555 004 990 |35 639 599 237 | | Margin | | |61 995 010 | | | |61 995 010 | | |3. INTERNAL POLICIES |9 385 000 000 | |9 218 359 185 |8 836 227 649 |+300 000 | |9 218 659 185 |8 836 227 649 | | Margin | | |166 640 815 | | | |166 340 815 | | |4. EXTERNAL ACTION |5 269 000 000 | |5 432 500 000 |5 378 395 920 | | |5 432 500 000 |5 378 395 920 | | Margin | | |-163 500 000 | | | |-163 500 000 | | |5. ADMINISTRATION |6 708 000 000 | |6 697 756 487 |6 697 756 487 | | |6 697 756 487 |6 697 756 487 | | Margin | | |10 243 513 | | | |10 243 513 | | |6. RESERVES | | | | | | | | | |- Guarantee reserve |229 000 000 | |229 000 000 |229 000 000 | | |229 000 000 |229 000 000 | |- Emergency aid reserve |229 000 000 | |229 000 000 |229 000 000 | | |229 000 000 |229 000 000 | |Total |458 000 000 | | 458 000 000 |458 000 000 | | | 458 000 000 |458 000 000 | | Margin | | |0 | | | |0 | | |7. PREACCESSION STRATEGY |3 566 000 000 | |2 480 600 000 |3 152 150 000 | | |2 480 600 000 |3 152 150 000 | | Margin | | | 1 085 400 000 | | | | 1 085 400 000 | | |8. COMPENSATIONS |1 074 000 000 | |1 073 500 332 |1 073 500 332 | | |1 073 500 332 |1 073 500 332 | | Margin | | | 499 668 | | | | 499 668 | | | TOTAL |123 695 000 000 |119 292 000 000 |121 328 040 994 |112 588 249 625 | -361 300 000 |-361 600 000 |120 966 740 994 |112 226 649 625 | | Margin | | |2 366 959 006 |6 703 750 375 | | |2 728 259 006 |7 065 350 375 | |
[1] OJ L 248, 16.9.2002, p. 1.
[2] OJ C 172, 18.6.1999, p.1
