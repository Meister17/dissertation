Authorisation for State aid pursuant to Articles 87 and 88 of the EC Treaty
Cases where the Commission raises no objections
(2006/C 267/02)
Date of adoption of the decision : 18.9.2006
Member State : Spain
Aid No : N 323/05
Title : Aid to promote cooperatives in Castille-Leon
Objective : Direct grants for agricultural cooperatives to promote and develop the agricultural cooperative movement in the Autonomous Community of Castille-Leon
Legal basis : Orden AYG/…/2005, de … de …, por la que se establece un regimen de ayudas para promover y fomentar el movimiento cooperativo agrario en la Comunidad Autonoma de Castilla y León
Budget : EUR 342400 for the 2005 financial year
Aid intensity or amount : Various
Duration : 31 December 2006
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
--------------------------------------------------
