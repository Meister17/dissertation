*****
COMMISSION DIRECTIVE
of 7 November 1990
amending Annex II to Council Directive 66/402/EEC on the marketing of cereal seed
( 90/623/EEC )
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Directive 66/402/EEC of 14 June 1966 on the marketing of cereal seed ( 1 ), as last amended by Commission Directive 89/2/EEC ( 2 ),
Whereas, according to present scientific and technical knowledge, it appears that certain varieties of oat ( Avena sativa ) of the 'naked oat' type have a potential value as fodder;
Whereas, however, it is difficult to produce seed of these varieties with a germination capacity equal to that normally achieved by seed of the oat varieties;
Whereas by Directive 88/506/EEC ( 3 ) the Commission declared that, in the light of the development of scientific and technical knowledge it was appropriate to reduce, for varieties of oats of the "naked oat' type, the minimum germination capacity of 85 % of pure seed laid down for oats in Annex II to Directive 66/402/EEC to 75 %;
Whereas that reduction applied only until 30 June 1990 so that further technical data about those varieties could be collected and assessed;
Whereas further technical data has shown that it is appropriate for the reduction to continue for a further limited period so that yet further technical data about these varieties can be collected and assessed;
Whereas the measures provided for in this Directive are in accordance with the opinion of the Standing Committee on Seeds and Propagating Material for Agriculture, Horticulture and Forestry,
HAS ADOPTED THIS DIRECTIVE :
Article 1
In section ( 2 ) ( B ) ( d ) of Annex II to Directive 66/402/EEC, "30 June 1990' is replaced by "31 December 1992 '.
Article 2
This Directive is addressed to the Member States .
Done at Brussels, 7 November 1990 .
For the Commission
Ray MAC SHARRY
Member of the Commission
( 1 ) OJ No 125, 11 . 7 . 1966, p . 2309/66 .
( 2 ) OJ No L 5, 7 . 1 . 1989, p . 31 .
( 3 ) OJ No L 274, 6 . 10 . 1988, p . 44 .
