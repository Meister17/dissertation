Political and Security Committee decision EUJUST LEX/1/2005
of 8 March 2005
concerning the appointment of the Head of Mission of the EU Integrated Rule of Law Mission for Iraq, EUJUST LEX
(2005/232/CFSP)
THE POLITICAL AND SECURITY COMMITTEE,
Having regard to the Treaty on European Union and in particular the third paragraph of Article 25 thereof,
Having regard to Council Joint Action 2005/190/CFSP of 7 March 2005 on the EU Integrated Rule of Law Mission for Iraq, EUJUST LEX [1] and in particular Article 9(1) thereof,
Whereas:
(1) Article 9(1) of Joint Action 2005/190/CFSP provides that the Council authorises the Political and Security Committee to take the relevant decisions in accordance with Article 25 of the Treaty, including the decision to appoint, upon a proposal by the Secretary-General/High Representative, a Head of Mission.
(2) The Secretary-General/High Representative has proposed the appointment of Stephen WHITE,
HAS DECIDED AS FOLLOWS:
Article 1
Stephen WHITE is hereby appointed Head of Mission of the EU Integrated Rule of Law Mission for Iraq, EUJUST LEX, from the day the mission is launched. Until that date, he shall act as head of the planning team.
Article 2
This Decision shall take effect on the day of its adoption.
It shall apply until 30 June 2006.
Done at Brussels, 8 March 2005.
For the Political and Security Committee
The President
P. Duhr
[1] OJ L 62, 9.3.2005, p. 37.
--------------------------------------------------
