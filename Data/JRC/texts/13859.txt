Authorisation for State aid pursuant to Articles 87 and 88 of the EC Treaty
Cases where the Commission raises no objections
(2006/C 144/02)
Date of adoption of the decision : 27.4.2006
Member State : France
Aid No : N 165/2005
Title : Temporary aid granted for livestock production in the Overseas Departments in 2005 in addition to Community support
Objective : To supplement the Community support provided for in 2005 in order to renew for that year the aid per head of livestock as it existed previously (operating aid)
Legal basis : Nationale: les articles L 621-1 et suivants du code rural
Budget : EUR 1,8 million
Aid intensity or amount : "de minimis"
Duration : 2005
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption of the decision : 12.4.2006
Member State : Italy (Apulia)
Aid No : N 180/2006
Title : Assistance in farming areas affected by natural disasters (whirlwind of 22 February 2005 in the province of Brindisi).
Objective : To compensate for damage to agricultural production and farming structures as a result of bad weather
Legal basis : Decreto legislativo n. 102/2004
Budget : See the approved scheme (NN 54/A/04)
Aid intensity or amount : Up to 100 %
Duration : Until the final payment is made
Other details : Measure applying the scheme approved by the Commission under State aid NN 54/A/2004 (Commission letter C(2005)1622 final, dated 7 June 2005)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption of the decision : 26.4.2006
Member State : United Kingdom (Wales)
Aid No : N 183/2006
Title : Extension of the National Scrapie Plan for Great Britain (Wales)
Objective : Prolongation of the National Scrapie Plan for Great Britain only for Wales for 2 years with an increase of the budget and tightening of criteria
Legal basis : Non-statutory
Budget : Increase of the budget: GBP 6 million (EUR 8,72 million)
Aid intensity or amount : Up to 100 %
Duration : Prolongation for 2 years until 31 March 2008
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption of the decision : 27.4.2006
Member State : Italy (Sicily)
Aid No : N 197/2006
Title : Assistance in farming areas affected by natural disasters (persistent rain and whirlwinds from 22 October until 13 December 2005 in the provinces of Catania, Enna and Ragusa)
Objective : Compensation for damage to agricultural production and farms as a result of bad weather (persistent rain and whirlwinds from 22 October until 13 December 2005 in the provinces of Catania, Enna and Ragusa)
Legal basis : Decreto legislativo 102/2004: "Nuova disciplina del Fondo di solidarietà nazionale"
Budget : To be financed from the budget approved under Aid No NN 54/A/04
Aid intensity or amount : Up to 100 %
Duration : Measure applying an aid scheme approved by the Commission
Other details : Measure applying the scheme approved by the Commission under State aid NN 54/A/2004 (Commission letter C(2005)1622 final, dated 7 June 2005)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption of the decision : 26.4.2006
Member State : Spain (Murcia)
Aid No : N 206/2006
Title : Aid for training and technology transfer in the agri-food sector
Objective : Modernisation of farms, bringing production more closely into line with demand and diversification of activities by means of training for farmers and technology transfer, using the prospective synergies between these two instruments
Legal basis : "Orden de la Consejería de Agricultura y Agua, por la que se hace pública la convocatoria en el año 2006 de las líneas de ayuda para Programas de colaboración para la formación y transferencia tecnológica del sector agroalimentario y del medio rural"
Budget : EUR 923000
Aid intensity or amount : The maximum rate of aid for training is 100 % for educational costs and 75 % for the cost of attending the courses. The rate of aid for technology transfer is 70 % of the eligible costs. (NB: for investment in infrastructure under the technology transfer programmes, which have no productive purpose but are intended for technical trials for subsequent use by all farmers, the rate of aid is limited to 40 % of the cost to beneficiaries of the individual technology transfer programme, and will not be more than 15 % of the overall budget for the aid scheme)
Duration : One year (2006)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption of the decision : 26.4.2006
Member State : Germany
Aid No : N 277/2003
Title : Transfer of nature protection land
Objective : Nature protection land of the federal government (Bund) in the Laender Mecklenburg-Western Pomerania, Brandenburg, Saxony-Anhalt, Saxony, Thuringia and Lower Saxony of up to 32,000 hectares are to be transferred free of charge to these Laender or nature protection foundations and associations. The German authorities have given assurances that nature protection restrictions are respected on a permanent basis
Legal basis : Ausgleichsleistungsgesetz
Budget : Not determined
Aid intensity or amount : No aid
Duration : Unlimited
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption of the decision : 24.3.2006
Member State : Germany (Baden-Württemberg)
Aid No : N 349/2004
Title : Activities for the preservation of biotopes and the countryside
Objective : Care measures on surfaces important for nature protection as well as the payment of a compensation for giving-up or relocating of facilities due to nature protection reasons
Legal basis : "Landschaftspflegerichtlinie des Landes Baden-Württemberg vom 5. Dezember 2001"
Budget :
Preservation of the countryside and biotope: approx. EUR 6 million per year
Remuneration for giving-up or relocating of a facility: approx. EUR 0,2 million per year
Aid intensity or amount : Up to 100 %
Duration : Unlimited
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption of the decision : 24.3.2006
Member State : Slovak Republic
Aid No : N 407/05
Title : Restructuring aid to Roľnícka spoločnosť a.s. Bottovo
Objective : Restructuring of the company.
Legal basis :
Zákon č. 328/1991 Zb. o konkurze a vyrovnaní v znení neskorších predpisov.
Zákon č. 511/1992 Zb. o správe daní a poplatkov a o zmenách v sústave územných finančných orgánov v znení neskorších predpisov
Budget : SKK 4996690
Aid intensity or amount : Non-aid
Duration : one-off
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption of the decision : 24.3.2006
Member State : Denmark
Aid No : N 416/05
Title : Aid for consultancy services in the potato sector
Objective : Improvement of cultivation, handling and treatment of potatoes. The aid amounting to DKK 70000 would be granted to Dansk Landsbrugsrådgivning, Landcentret for the costs of a project, in which 6 persons will be trained as specialised potato cultivation consultants. The potato cultivation consultants will, after the training, provide consultancy services to potato growers in Denmark on cultivation, handling and treatment of potatoes. All potato growers have access to these services. The Danish authorities have confirmed that the potato growers are the sole beneficiaries of the aid
Legal basis : Bekendtgørelse nr. 973 af 4. december 2003 om produktionsafgift på kartofler)
Budget : Ca DKK 70000 (about EUR 9383)
Aid intensity or amount : DKK 30 per hour of consultancy
Duration : One-off
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption of the decision : 12.4.2006
Member State : The Netherlands
Aid No : N 438/2005
Title : A change in the parafiscal levies for the fruit and vegetables sector
Objective : To finance sales promotion for onions and mushrooms, various promotion measures for fruit and vegetables, research on horticultural products and support for quality control. The present measure concerns a change in financing in the sense that parafiscal charges for vegetables and strawberry growers will no longer be based on revenue, but on hectares grown with these crops. In addition, the maximum budget for this measure will be increased to EUR 20 million per year
Legal basis : Verordening van het Productschap voor Tuinbouw bestemmingsheffing vollegrondsgroenten 2005
Budget : The maximum budget for this measure will be EUR 20 million per year, although budgets for the foreseeable future will remain below this level
Duration : Indefinite
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
--------------------------------------------------
