[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 20.10.2005
COM(2005) 468 final
2005/0198 (CNS)
Proposal for a
COUNCIL DECISION
authorizing the conclusion, on behalf of the European Community, of a Memorandum of Understanding between the European Community and the Swiss Federal Council on a contribution by the Swiss Confederation towards reducing economic and social disparities in the enlarged European Union, and authorizing certain Member States to conclude individually agreements with the Swiss Confederation on the implementation of the Memorandum
(presented by the Commission)
EXPLANATORY MEMORANDUM
The Council authorised the Commission in April 2003 to negotiate with Switzerland on a financial contribution to reducing economic and social disparities in the enlarged EU within the framework of the package of agreements to be negotiated with Switzerland.
At the EU-CH summit in May 2004, Switzerland pledged to contribute with CHF 1 billion over 5 years. This was accepted by Council as part of an overall compromise leading to the conclusion of a large negotiation round which covered nine sector agreements.
Negotiations on the technical details of this specific contribution were held between November 2004 and May 2005. The result of these negotiations is the Memorandum of Understanding in the annex, which sets out the framework for the allocation of the Swiss contribution and the sectors to be prioritised. The following aspects are noteworthy:
Form of the agreement
The conclusion of a binding agreement with the European Community proved to be impossible as it would not have been accepted in the Swiss ratification process. Therefore it has been necessary to accept instead a Memorandum of Understanding with the Federal Council which will be complemented by binding agreements with each beneficiary state. The memorandum contains all necessary elements and stipulates that individual bilateral agreements between beneficiary states and Switzerland will be in conformity with its content.
Number of beneficiaries
Switzerland has made clear that the financial contribution is limited to making payments which cover only the ten new Member States. The Swiss authorities have argued further that the Swiss Parliament would never accept to change the legal basis of the budget appropriation to include EU 13. In the light of the current political situation in Switzerland this seems credible. The Swiss also refer to the EU’s agreement with Norway as a precedent (the bilateral Norwegian contribution is allocated only to the 10 new Member States). If the Community continues therefore to insist on the inclusion of all beneficiaries of EU cohesion policy, Switzerland will refuse to sign a Memorandum with the EC and negotiate instead bilaterally with the new Member States. That would clearly not be in the interest of the EU as it is intended that the Memorandum will be the basis for a future Swiss contribution on the occasion of the next enlargement and for a prolongation after five years. Therefore the proposed Memorandum is the only realistic option.
The Commission negotiators have endeavoured to ensure that the Memorandum provides for the possibility of joint projects together with other EU Member States participating in the EU’s economic and social cohesion policy, thereby making it clear that this policy is not limited to the ten beneficiaries mentioned in the Memorandum. Such joint projects may be cofinanced by Community instruments in the fields of cohesion and interregional cooperation.
Distribution key
The distribution of the Swiss contribution on beneficiaries is based on the existing key of the cohesion fund, which had been agreed by all member states. Malta has requested a revision of this key, but this is not achievable within the context of this exercise. The current distribution key was also used in the EU’s bilateral agreement with Norway.
Legal basis
It is proposed that the Memorandum be concluded on the basis of Articles 159, paragraph 3, and 300 of the Treaty, which provide that Council shall act by qualified majority. The Memorandum will be concluded with its signature.
Cohesion policy
It should be noted that the Memorandum of Understanding will be concluded in the context of external relations. It is not meant to have any impact on the principles, including the criteria and methods for the allocation of resources, of the cohesion policy of the European Union itself, which has the objective to promote its overall harmonious development.
2005/0198 (CNS)
Proposal for a
COUNCIL DECISION
authorizing the conclusion, on behalf of the European Community, of a Memorandum of Understanding between the European Community and the Swiss Federal Council on a contribution by the Swiss Confederation towards reducing economic and social disparities in the enlarged European Union, and authorizing certain Member States to conclude individually agreements with the Swiss Confederation on the implementation of the Memorandum
THE COUNCIL OF THE EUROPEAN UNION
Having regard to the Treaty establishing the European Community, and in particular Article 159, paragraph 3, in conjunction with the first sentence of the first subparagraph of Article 300 (2) and the first subparagraph of Article 300 (3) thereof,
Having regard to the proposal from the Commission,[1]
Having regard to the opinion of the European Parliament,[2]
Whereas:
(1) The Council authorised the Commission in April 2003 to negotiate on behalf of the Community with the Swiss Confederation an agreement on a financial contribution of Switzerland to reducing economic and social disparities in the enlarged European Union.
(2) Negotiations on the technical details of this specific contribution were held between November 2004 and May 2005. The result of these negotiations is the memorandum of understanding in the annex, which sets out the framework for the allocation of the Swiss contribution and the sectors to be prioritised, without prejudice to the principles, including the criteria and methods for the allocation of resources, of the cohesion policy of the Community.
HAS DECIDED AS FOLLOWS:
Article 1
The Memorandum of Understanding between the European Community and the Swiss Federal Council on a contribution by the Swiss Confederation towards reducing economic and social disparities in the enlarged European Union is hereby approved on behalf of the European Community.
The text of the Memorandum of Understanding is attached to this Decision.
Article 2
The President of the Council is hereby authorized to designate the person empowered to sign on behalf of the European Community the Memorandum of Understanding between the European Community and the Swiss Federal Council on a contribution by the Swiss Confederation towards reducing economic and social disparities in the enlarged European Union.
Article 3
The Member States which are listed in the Memorandum of Understanding as beneficiaries of the Swiss financial contribution are hereby authorised to conclude individually agreements with the Swiss Confederation on the implementation of the memorandum. Prior to the signature of such an agreement, the Member States shall coordinate their positions within the Council.
Article 4
This Decision shall be published in the Official Journal of the European Union .
Done at Brussels,
For the Council
The President
ANNEX I
Memorandum of Understanding between the European Community and the Swiss Federal Council on a contribution by the Swiss Confederation towards reducing economic and social disparities in the enlarged European Union
the European Community, hereinafter referred to as ‘the Community’, and
The Swiss Federal Council, hereinafter referred to as the Federal Council
Considering the close links between the European Union and Switzerland based on numerous sectoral agreements, particularly in economic, scientific and cultural areas,
Considering that enlargement of the European Union contributes towards safeguarding peace, freedom, stability and prosperity in Europe, and the Federal Council is determined to demonstrate the support of the Swiss Confederation in that respect,
Considering the European Community’s request to the Swiss Confederation to contribute towards reducing economic and social disparities within the European Union,
Considering the European Union’s efforts on external assistance and pre-accession aid and economic and social cohesion,
Considering the Swiss Confederation’s support to the countries of central and eastern Europe,
Desiring to give shape to the political will of the Federal Council to contribute to reducing economic and social disparities within the European Union through this memorandum,
have drawn up the following guidelines:
1. The Swiss contribution
The Federal Council shall negotiate with the Member States listed in point 2 agreements on the arrangements for a Swiss contribution of one billion Swiss francs over a period of five years, starting from the approval of the funds by the Swiss Parliament, towards the reduction of economic and social disparities within the enlarged European Union.
2. Breakdown of the Swiss contribution
Beneficiary State | Percentage of the total contribution |
Czech Republic | 11.0% |
Estonia | 4.0% |
Cyprus | 0.6% |
Latvia | 6.0% |
Lithuania | 7.1% |
Hungary | 13.1% |
Malta | 0.3% |
Poland | 49.0% |
Slovenia | 2.2% |
Slovakia | 6.7% |
Regional and national projects and programmes, and projects and programmes that involve several beneficiary States, may be financed by this contribution.
3. Review
The situation shall be reviewed 2 years and 4 years after the implementation of the Swiss contribution begins, in order to reallocate any funds that have not been committed to high-priority projects and programmes in the beneficiary States.
4. Funding guidelines and areas
Security, stability and support for reforms :
- Strengthening local management capacity at regional and communal level;
- Measures for securing borders;
- Improvements in the management of immigration and asylum issues;
- Access to information systems on security law and developing the legal aspects of security;
- Modernisation of the judiciary;
- Strengthening institutions and capacity for combating corruption and organised crime;
- Nuclear safety;
- Prevention and management of natural disasters;
- Regional development initiatives in peripheral or disadvantaged regions.
Environment and infrastructure :
- Sanitation and modernisation of basic infrastructure (energy efficiency, drinking water, waste water, waste disposal, public transport);
- Improvements in environmental conditions, reduction of harmful emissions, development and enforcement of ecological monitoring standards and norms;
- Toxic waste disposal, and revitalisation of contaminated industrial sites;
- Regional, town and country planning of land use, infrastructure, the environment, etc.;
- Cross-border environmental initiatives like “Environment for Europe”;
- Biodiversity and nature protection.
Promotion of the private sector :
- Development of the private sector and promotion of exports, with the emphasis on small and medium-sized enterprises (SMEs);
- Improving access to capital, support to SMEs in the management field;
- Promotion of certified organic agricultural products;
- Promotion of standards, norms and conformity assessment in industrial and agricultural production; promotion of industrial production that conforms to the principle of sustainable development from the social, environmental, and eco-efficiency point of view;
- Improved regulation of the financial sector, and strengthening of financial markets and institutions;
- Protection of intellectual property.
Human and social development :
- Building public administration capacity at central, regional and municipal levels, to bring it up to EU standards;
- Technical and vocational training;
- Research and development (scientific exchange programmes, grants, partnerships, cooperation in applied research, etc.);
- Health (modernisation of hospitals, reform of health insurance systems, preventive measures, etc.);
- Twinning of towns and communes;
- Support for international development initiatives.
Funding areas shall be selected and ranked according to the needs of each beneficiary State.
5. Information and coordination
The Federal Council and the European Commission shall communicate regularly as regards the implementation of the Swiss contribution, whenever the need arises, including in policy matters. They shall coordinate their activities to ensure the compatibility of the projects and programmes proposed, in order to reduce economic and social disparities. To that end, the European Commission shall inform the Federal Council of its assessment of the compatibility between the projects and programmes proposed and the Community’s objectives, and of any relevant changes in EU cohesion policies, which shall be duly taken into account. They shall also coordinate with other institutions and donors that finance the same projects and programmes.
Where appropriate, projects and programmes may be carried out in cooperation with other EU Member States participating in the EU’s economic and social cohesion policy. These projects and programmes may be cofinanced by Community instruments in the fields of cohesion and interregional cooperation.
6. Selection of projects and programmes
The Federal Council shall select projects and programmes in agreement with the beneficiary States, taking account of their requests, needs and actual take-up capacity.
7. Implementation of projects and programmes
1. Projects and programmes shall be implemented as appropriate on a basis that is bilateral, multi-bilateral (involving cofinancing with other institutions or donors), or multilateral (through a fiduciary system).
2. The Swiss contributions shall be in the form of gifts or concessional financial facilities. They will not be repayable to Switzerland.
3. Entities that benefit from the Swiss contribution will in principle be minority finance partners on the projects or programmes. The Swiss contribution, in the form of grants, may not exceed 60% of the cost of the project, except in the case of projects receiving additional financing in the form of budget allocations from national, regional or local authorities, in which case the Swiss contribution may not exceed 85% of the total cost. The rules for cofinancing shall be respected. Technical assistance projects, institution-building programmes, and projects and programmes implemented by non-governmental organisations may be fully financed by the Swiss contribution.
4. Rules regarding public procurement and State aid shall be respected. Contributions towards projects or programmes may not be tied.
5. The management costs of the Swiss government shall be covered by the amounts referred to in point 1. These include staffing and administrative costs, mission expenses for officials and consultants, and the cost of financial controls and evaluation.
8. Implementation of the Swiss contribution
The Federal Council shall propose that the Swiss Parliament approve funding amounting to one billion Swiss francs for the implementation of the Swiss contribution, to start in 2006. The agreements referred to in paragraph 1 must be in conformity with the guidelines laid down in this Memorandum. A general description of the content of the framework agreements between Switzerland and the beneficiary States is attached in annex. These agreements are to be approved by the contracting parties in accordance with their own procedures.
Done at … on … in duplicate in the Czech, Danish, Dutch, English, Estonian, Finnish, French, German, Greek, Hungarian, Italian, Latvian, Lithuanian, Polish, Portuguese, Slovenian, Slovak, Spanish and Swedish languages.
For the Federal Council: … For the European Community:
ANNEX II
General description of the content of the framework agreements between Switzerland and the new Member States of the EU (hereinafter referred to as Partner States)
Each of the ten framework agreements shall contain the general principles of the cooperation between Switzerland and the Partner State in question. Project agreements, which will cover technical points concerning the project, shall be concluded on the basis of these framework agreements.
The standard content of a framework agreement can be summarised as follows:
1. A reference to the Memorandum of Understanding;
2. The aims of the cooperation between Switzerland and the Partner State;
3. The start and completion dates of the cooperation between Switzerland and the Member State;
4. The total amount of the Swiss contribution, and the review process between Switzerland and the EC;
5. The conceptual framework (priority funding areas, principles, partners) for the cooperation between Switzerland and the Partner State;
6. Procedures and criteria for selection (including communication and cooperation with the European Commission), approval and implementation of the projects and programmes;
7. Forms of financing for projects and programmes;
8. Principles concerning the use of the funds that are made available, contribution ceilings, management costs (maximum 5%), payment, audit and administration arrangements;
9. Competences and coordination regarding the implementation of the framework agreement.
LEGISLATIVE FINANCIAL STATEMENT
Policy area(s): Regional Policy Activity: Cohesion policy |
TITLE OF ACTION: MEMORANDUM OF UNDERSTANDING BETWEEN THE EUROPEAN COMMUNITY AND THE SWISS FEDERAL COUNCIL ON A CONTRIBUTION BY THE SWISS CONFEDERATION TOWARDS REDUCING ECONOMIC AND SOCIAL DISPARITIES IN THE ENLARGED EUROPEAN UNION |
1. BUDGET LINE(S) + HEADING(S)
2. OVERALL FIGURES
2.1. Total allocation for action (Part B): € million for commitment
NOT APPLICABLE
2.2. Period of application:
Start: The Memorandum will (probably) enter into force in 2005 (depending on date of signature)
Expiry: The memorandum has no expiry date, but will become obsolete after the end of the Swiss financial contribution
2.3. Overall multiannual estimate of expenditure:
NOT APPLICABLE
(a) Schedule of commitment appropriations/payment appropriations (financial intervention) (see point 6.1.1)
€ million ( to three decimal places)
Year [n] | [n+1] | [n+2] | [n+3] | [n+4] | [n+5 and subs. Years] | Total |
Commitments |
Payments |
(b) Technical and administrative assistance and support expenditure (see point 6.1.2)
Commitments |
Payments |
Subtotal a+b |
Commitments |
Payments |
(c) Overall financial impact of human resources and other administrative expenditure (see points 7.2 and 7.3)
Commitments/ payments |
TOTAL a+b+c |
Commitments |
Payments |
2.4. Compatibility with financial programming and financial perspective
[x] Proposal is compatible with existing financial programming.
Proposal will entail reprogramming of the relevant heading in the financial perspective.
Proposal may require application of the provisions of the Interinstitutional Agreement.
2.5. Financial impact on revenue: [3]
[x] Proposal has no financial implications (involves technical aspects regarding implementation of a measure)
OR
Proposal has financial impact – the effect on revenue is as follows (indicative):
(€ million to one decimal place)
Prior to action [Year n-1] | Situation following action |
5. DESCRIPTION AND GROUNDS
not applicable
5.1. Need for Community intervention
5.1.1. Objectives pursued
N/A
5.1.2. Measures taken in connection with ex ante evaluation
N/A
5.1.3. Measures taken following ex post evaluation
N/A
5.2. Action envisaged and budget intervention arrangements
N/A
5.3. Methods of implementation
N/A
6. FINANCIAL IMPACT
N/A
6.1. Total financial impact on Part B - (over the entire programming period)
(The method of calculating the total amounts set out in the table below must be explained by the breakdown in Table 6.2. )
6.1.1. Financial intervention
Commitments (in € million to three decimal places)
Breakdown | [Year n] | [n+1] | [n+2] | [n+3] | [n+4] | [n+5 and subs. Years] | Total |
Action 1 |
Action 2 |
etc. |
TOTAL |
6.1.2. Technical and administrative assistance, support expenditure and IT expenditure (commitment appropriations) |
[Year n] | [n+1] | [n+2] | [n+3] | [n+4] | [n+5 and subs. years] | Total |
1) Technical and administrative assistance |
a) Technical assistance offices |
b) Other technical and administrative assistance: - intra muros: - extra muros: of which for construction and maintenance of computerised management systems |
Subtotal 1 |
2) Support expenditure |
a) Studies |
b) Meetings of experts |
c) Information and publications |
Subtotal 2 |
TOTAL |
6.2. Calculation of costs by measure envisaged in Part B (over the entire programming period)
(Where there is more than one action, give sufficient detail of the specific measures to be taken for each one to allow the volume and costs of the outputs to be estimated.)
Commitments (in € million to three decimal places)
Breakdown | Type of outputs (projects, files ) | Number of outputs (total for years 1…n) | Average unit cost | Total cost (total for years 1…n) |
1 | 2 | 3 | 4=(2X3) |
Action 1 - Measure 1 - Measure 2 Action 2 - Measure 1 - Measure 2 - Measure 3 etc. |
TOTAL COST |
If necessary explain the method of calculation
7. IMPACT ON STAFF AND ADMINISTRATIVE EXPENDITURE
N/A
7.1. Impact on human resources
Types of post | Staff to be assigned to management of the action using existing and/or additional resources | Total | Description of tasks deriving from the action |
Number of permanent posts | Number of temporary posts |
Officials or temporary staff | A B C | If necessary, a fuller description of the tasks may be annexed. |
Other human resources |
Total |
7.2. Overall financial impact of human resources
Type of human resources | Amount (€) | Method of calculation * |
Officials Temporary staff |
Other human resources (specify budget line) |
Total |
The amounts are total expenditure for twelve months.
7.3. Other administrative expenditure deriving from the action
Budget line (number and heading) | Amount € | Method of calculation |
Overall allocation (Title A7) A0701 – Missions A07030 – Meetings A07031 – Compulsory committees 1 A07032 – Non-compulsory committees 1 A07040 – Conferences A0705 – Studies and consultations Other expenditure (specify) |
Information systems (A-5001/A-4300) |
Other expenditure - Part A (specify) |
Total |
The amounts are total expenditure for twelve months.
1 Specify the type of committee and the group to which it belongs.
I. Annual total (7.2 + 7.3) II. Duration of action III. Total cost of action (I x II) | € years € |
8. FOLLOW-UP AND EVALUATION
N/A
8.1. Follow-up arrangements
N/A
8.2. Arrangements and schedule for the planned evaluation
N/A
9. ANTI-FRAUD MEASURES
N/A
[1] OJ C , , p. .
[2] OJ C , , p. .
[3] For further information, see separate explanatory note.
