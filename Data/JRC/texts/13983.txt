Commission Regulation (EC) No 1599/2006
of 26 October 2006
fixing the export refunds on milk and milk products
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1255/1999 of 17 May 1999 on the common organisation of the market in milk and milk products [1], and in particular Article 31(3) thereof,
Whereas:
(1) Article 31(1) of Regulation (EC) No 1255/1999 provides that the difference between prices on the world market for the products listed in Article 1 of that Regulation and prices for those products on the Community market may be covered by an export refund.
(2) Given the present situation on the market in milk and milk products, export refunds should therefore be fixed in accordance with the rules and certain criteria provided for in Article 31 of Regulation (EC) No 1255/1999.
(3) The second subparagraph of Article 31(3) of Regulation (EC) No 1255/1999 provides that the world market situation or the specific requirements of certain markets may make it necessary to vary the refund according to destination.
(4) In accordance with the Memorandum of Understanding between the European Community and the Dominican Republic on import protection for milk powder in the Dominican Republic [2] approved by Council Decision 98/486/EC [3], a certain amount of Community milk products exported to the Dominican Republic can benefit from reduced customs duties. For this reason, export refunds granted to products exported under this scheme should be reduced by a certain percentage.
(5) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Milk and Milk Products,
HAS ADOPTED THIS REGULATION:
Article 1
Export refunds as provided for in Article 31 of Regulation (EC) No 1255/1999 shall be granted on the products and for the amounts set out in the Annex to this Regulation subject to the conditions provided for in Article 1(4) of Commission Regulation (EC) No 174/1999 [4].
Article 2
This Regulation shall enter into force on 27 October 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 26 October 2006.
For the Commission
Jean-Luc Demarty
Director-General for Agriculture and Rural Development
[1] OJ L 160, 26.6.1999, p. 48. Regulation as last amended by Regulation (EC) No 1913/2005 (OJ L 307, 25.11.2005, p. 2).
[2] OJ L 218, 6.8.1998, p. 46.
[3] OJ L 218, 6.8.1998, p. 45.
[4] OJ L 20, 27.1.1999, p. 8.
--------------------------------------------------
ANNEX
Export refunds on milk and milk products applicable from 27 October 2006
The destinations are defined as follows:
L02 : Andorra and Gibraltar.
L20 : All destinations except L02, Ceuta, Melilla, Holy See (Vatican City State), the United States of America, Bulgaria, Romania and the areas of the Republic of Cyprus in which the Government of the Republic of Cyprus does not exercise effective control.
L04 : Albania, Bosnia and Herzegovina, Kosovo, Serbia, Montenegro and the former Yugoslav Republic of Macedonia.
L40 : All destinations except L02, L04, Ceuta, Melilla, Iceland, Liechtenstein, Norway, Switzerland, Holy See (Vatican City State), the United States of America, Bulgaria, Romania, Croatia, Turkey, Australia, Canada, New Zealand and the areas of the Republic of Cyprus in which the Government of the Republic of Cyprus does not exercise effective control.
Product code | Destination | Unit of measurement | Refunds |
0401 30 31 9100 | L02 | EUR/100 kg | 13,02 |
L20 | EUR/100 kg | 18,61 |
0401 30 31 9400 | L02 | EUR/100 kg | 20,34 |
L20 | EUR/100 kg | 29,07 |
0401 30 31 9700 | L02 | EUR/100 kg | 22,45 |
L20 | EUR/100 kg | 32,06 |
0401 30 39 9100 | L02 | EUR/100 kg | 13,02 |
L20 | EUR/100 kg | 18,61 |
0401 30 39 9400 | L02 | EUR/100 kg | 20,34 |
L20 | EUR/100 kg | 29,07 |
0401 30 39 9700 | L02 | EUR/100 kg | 22,45 |
L20 | EUR/100 kg | 32,06 |
0401 30 91 9100 | L02 | EUR/100 kg | 25,57 |
L20 | EUR/100 kg | 36,54 |
0401 30 99 9100 | L02 | EUR/100 kg | 25,57 |
L20 | EUR/100 kg | 36,54 |
0401 30 99 9500 | L02 | EUR/100 kg | 37,59 |
L20 | EUR/100 kg | 53,70 |
0402 10 11 9000 | L02 | EUR/100 kg | — |
L20 [1] | EUR/100 kg | — |
0402 10 19 9000 | L02 | EUR/100 kg | — |
L20 [1] | EUR/100 kg | — |
0402 10 99 9000 | L02 | EUR/100 kg | — |
L20 | EUR/100 kg | — |
0402 21 11 9200 | L02 | EUR/100 kg | — |
L20 | EUR/100 kg | — |
0402 21 11 9300 | L02 | EUR/100 kg | 36,43 |
L20 | EUR/100 kg | 46,74 |
0402 21 11 9500 | L02 | EUR/100 kg | 38,01 |
L20 | EUR/100 kg | 48,79 |
0402 21 11 9900 | L02 | EUR/100 kg | 40,50 |
L20 [1] | EUR/100 kg | 52,00 |
0402 21 17 9000 | L02 | EUR/100 kg | — |
L20 | EUR/100 kg | — |
0402 21 19 9300 | L02 | EUR/100 kg | 36,43 |
L20 | EUR/100 kg | 46,74 |
0402 21 19 9500 | L02 | EUR/100 kg | 38,01 |
L20 | EUR/100 kg | 48,79 |
0402 21 19 9900 | L02 | EUR/100 kg | 40,50 |
L20 [1] | EUR/100 kg | 52,00 |
0402 21 91 9100 | L02 | EUR/100 kg | 40,76 |
L20 | EUR/100 kg | 52,31 |
0402 21 91 9200 | L02 | EUR/100 kg | 40,99 |
L20 [1] | EUR/100 kg | 52,64 |
0402 21 91 9350 | L02 | EUR/100 kg | 41,44 |
L20 | EUR/100 kg | 53,17 |
0402 21 99 9100 | L02 | EUR/100 kg | 40,76 |
L20 | EUR/100 kg | 52,31 |
0402 21 99 9200 | L02 | EUR/100 kg | 40,99 |
L20 [1] | EUR/100 kg | 52,64 |
0402 21 99 9300 | L02 | EUR/100 kg | 41,44 |
L20 | EUR/100 kg | 53,17 |
0402 21 99 9400 | L02 | EUR/100 kg | 43,71 |
L20 | EUR/100 kg | 56,12 |
0402 21 99 9500 | L02 | EUR/100 kg | 44,51 |
L20 | EUR/100 kg | 57,14 |
0402 21 99 9600 | L02 | EUR/100 kg | 47,67 |
L20 | EUR/100 kg | 61,18 |
0402 21 99 9700 | L02 | EUR/100 kg | 49,42 |
L20 | EUR/100 kg | 63,47 |
0402 29 15 9200 | L02 | EUR/100 kg | — |
L20 | EUR/100 kg | — |
0402 29 15 9300 | L02 | EUR/100 kg | 36,43 |
L20 | EUR/100 kg | 46,74 |
0402 29 15 9500 | L02 | EUR/100 kg | 38,01 |
L20 | EUR/100 kg | 48,79 |
0402 29 19 9300 | L02 | EUR/100 kg | 36,43 |
L20 | EUR/100 kg | 46,74 |
0402 29 19 9500 | L02 | EUR/100 kg | 38,01 |
L20 | EUR/100 kg | 48,79 |
0402 29 19 9900 | L02 | EUR/100 kg | 40,50 |
L20 | EUR/100 kg | 52,00 |
0402 29 99 9100 | L02 | EUR/100 kg | 40,76 |
L20 | EUR/100 kg | 52,31 |
0402 29 99 9500 | L02 | EUR/100 kg | 43,71 |
L20 | EUR/100 kg | 56,12 |
0402 91 11 9370 | L02 | EUR/100 kg | 4,13 |
L20 | EUR/100 kg | 5,90 |
0402 91 19 9370 | L02 | EUR/100 kg | 4,13 |
L20 | EUR/100 kg | 5,90 |
0402 91 31 9300 | L02 | EUR/100 kg | 4,88 |
L20 | EUR/100 kg | 6,97 |
0402 91 39 9300 | L02 | EUR/100 kg | 4,88 |
L20 | EUR/100 kg | 6,97 |
0402 91 99 9000 | L02 | EUR/100 kg | 15,71 |
L20 | EUR/100 kg | 22,46 |
0402 99 11 9350 | L02 | EUR/100 kg | 10,55 |
L20 | EUR/100 kg | 15,08 |
0402 99 19 9350 | L02 | EUR/100 kg | 10,55 |
L20 | EUR/100 kg | 15,08 |
0402 99 31 9300 | L02 | EUR/100 kg | 9,40 |
L20 | EUR/100 kg | 13,44 |
0403 90 11 9000 | L02 | EUR/100 kg | — |
L20 | EUR/100 kg | — |
0403 90 13 9200 | L02 | EUR/100 kg | — |
L20 | EUR/100 kg | — |
0403 90 13 9300 | L02 | EUR/100 kg | 36,09 |
L20 | EUR/100 kg | 46,33 |
0403 90 13 9500 | L02 | EUR/100 kg | 37,68 |
L20 | EUR/100 kg | 48,36 |
0403 90 13 9900 | L02 | EUR/100 kg | 40,16 |
L20 | EUR/100 kg | 51,53 |
0403 90 33 9400 | L02 | EUR/100 kg | 36,09 |
L20 | EUR/100 kg | 46,33 |
0403 90 59 9310 | L02 | EUR/100 kg | 13,02 |
L20 | EUR/100 kg | 18,61 |
0403 90 59 9340 | L02 | EUR/100 kg | 19,06 |
L20 | EUR/100 kg | 27,22 |
0403 90 59 9370 | L02 | EUR/100 kg | 19,06 |
L20 | EUR/100 kg | 27,22 |
0404 90 21 9120 | L02 | EUR/100 kg | — |
L20 | EUR/100 kg | — |
0404 90 21 9160 | L02 | EUR/100 kg | — |
L20 | EUR/100 kg | — |
0404 90 23 9120 | L02 | EUR/100 kg | — |
L20 | EUR/100 kg | — |
0404 90 23 9130 | L02 | EUR/100 kg | 36,43 |
L20 | EUR/100 kg | 46,74 |
0404 90 23 9140 | L02 | EUR/100 kg | 38,01 |
L20 | EUR/100 kg | 48,79 |
0404 90 23 9150 | L02 | EUR/100 kg | 40,50 |
L20 | EUR/100 kg | 52,00 |
0404 90 81 9100 | L02 | EUR/100 kg | — |
L20 | EUR/100 kg | — |
0404 90 83 9110 | L02 | EUR/100 kg | — |
L20 | EUR/100 kg | — |
0404 90 83 9130 | L02 | EUR/100 kg | 36,43 |
L20 | EUR/100 kg | 46,74 |
0404 90 83 9150 | L02 | EUR/100 kg | 38,01 |
L20 | EUR/100 kg | 48,79 |
0404 90 83 9170 | L02 | EUR/100 kg | 40,50 |
L20 | EUR/100 kg | 52,00 |
0405 10 11 9500 | L02 | EUR/100 kg | 72,00 |
L20 | EUR/100 kg | 97,08 |
0405 10 11 9700 | L02 | EUR/100 kg | 73,79 |
L20 | EUR/100 kg | 99,50 |
0405 10 19 9500 | L02 | EUR/100 kg | 72,00 |
L20 | EUR/100 kg | 97,08 |
0405 10 19 9700 | L02 | EUR/100 kg | 73,79 |
L20 | EUR/100 kg | 99,50 |
0405 10 30 9100 | L02 | EUR/100 kg | 72,00 |
L20 | EUR/100 kg | 97,08 |
0405 10 30 9300 | L02 | EUR/100 kg | 73,79 |
L20 | EUR/100 kg | 99,50 |
0405 10 30 9700 | L02 | EUR/100 kg | 73,79 |
L20 | EUR/100 kg | 99,50 |
0405 10 50 9500 | L02 | EUR/100 kg | 72,00 |
L20 | EUR/100 kg | 97,08 |
0405 10 50 9700 | L02 | EUR/100 kg | 73,79 |
L20 | EUR/100 kg | 99,50 |
0405 10 90 9000 | L02 | EUR/100 kg | 76,50 |
L20 | EUR/100 kg | 103,15 |
0405 20 90 9500 | L02 | EUR/100 kg | 67,51 |
L20 | EUR/100 kg | 91,01 |
0405 20 90 9700 | L02 | EUR/100 kg | 70,20 |
L20 | EUR/100 kg | 94,64 |
0405 90 10 9000 | L02 | EUR/100 kg | 92,11 |
L20 | EUR/100 kg | 124,18 |
0405 90 90 9000 | L02 | EUR/100 kg | 73,66 |
L20 | EUR/100 kg | 99,32 |
0406 10 20 9640 | L04 | EUR/100 kg | 26,04 |
L40 | EUR/100 kg | 32,55 |
0406 10 20 9650 | L04 | EUR/100 kg | 21,71 |
L40 | EUR/100 kg | 27,13 |
0406 10 20 9830 | L04 | EUR/100 kg | 8,06 |
L40 | EUR/100 kg | 10,06 |
0406 10 20 9850 | L04 | EUR/100 kg | 9,76 |
L40 | EUR/100 kg | 12,20 |
0406 20 90 9913 | L04 | EUR/100 kg | 19,33 |
L40 | EUR/100 kg | 24,15 |
0406 20 90 9915 | L04 | EUR/100 kg | 26,24 |
L40 | EUR/100 kg | 32,80 |
0406 20 90 9917 | L04 | EUR/100 kg | 27,89 |
L40 | EUR/100 kg | 34,85 |
0406 20 90 9919 | L04 | EUR/100 kg | 31,15 |
L40 | EUR/100 kg | 38,95 |
0406 30 31 9730 | L04 | EUR/100 kg | 3,47 |
L40 | EUR/100 kg | 8,15 |
0406 30 31 9930 | L04 | EUR/100 kg | 3,47 |
L40 | EUR/100 kg | 8,15 |
0406 30 31 9950 | L04 | EUR/100 kg | 5,05 |
L40 | EUR/100 kg | 11,85 |
0406 30 39 9500 | L04 | EUR/100 kg | 3,47 |
L40 | EUR/100 kg | 8,15 |
0406 30 39 9700 | L04 | EUR/100 kg | 5,05 |
L40 | EUR/100 kg | 11,85 |
0406 30 39 9930 | L04 | EUR/100 kg | 5,05 |
L40 | EUR/100 kg | 11,85 |
0406 30 39 9950 | L04 | EUR/100 kg | 5,72 |
L40 | EUR/100 kg | 13,40 |
0406 40 50 9000 | L04 | EUR/100 kg | 30,62 |
L40 | EUR/100 kg | 38,27 |
0406 40 90 9000 | L04 | EUR/100 kg | 31,45 |
L40 | EUR/100 kg | 39,31 |
0406 90 13 9000 | L04 | EUR/100 kg | 34,85 |
L40 | EUR/100 kg | 49,89 |
0406 90 15 9100 | L04 | EUR/100 kg | 36,03 |
L40 | EUR/100 kg | 51,56 |
0406 90 17 9100 | L04 | EUR/100 kg | 36,03 |
L40 | EUR/100 kg | 51,56 |
0406 90 21 9900 | L04 | EUR/100 kg | 35,02 |
L40 | EUR/100 kg | 50,00 |
0406 90 23 9900 | L04 | EUR/100 kg | 31,39 |
L40 | EUR/100 kg | 45,14 |
0406 90 25 9900 | L04 | EUR/100 kg | 30,79 |
L40 | EUR/100 kg | 44,07 |
0406 90 27 9900 | L04 | EUR/100 kg | 27,88 |
L40 | EUR/100 kg | 39,92 |
0406 90 31 9119 | L04 | EUR/100 kg | 25,78 |
L40 | EUR/100 kg | 36,95 |
0406 90 33 9119 | L04 | EUR/100 kg | 25,78 |
L40 | EUR/100 kg | 36,95 |
0406 90 35 9190 | L04 | EUR/100 kg | 36,71 |
L40 | EUR/100 kg | 52,80 |
0406 90 35 9990 | L04 | EUR/100 kg | 36,71 |
L40 | EUR/100 kg | 52,80 |
0406 90 37 9000 | L04 | EUR/100 kg | 34,85 |
L40 | EUR/100 kg | 49,89 |
0406 90 61 9000 | L04 | EUR/100 kg | 39,68 |
L40 | EUR/100 kg | 57,42 |
0406 90 63 9100 | L04 | EUR/100 kg | 39,09 |
L40 | EUR/100 kg | 56,38 |
0406 90 63 9900 | L04 | EUR/100 kg | 37,57 |
L40 | EUR/100 kg | 54,45 |
0406 90 69 9910 | L04 | EUR/100 kg | 38,13 |
L40 | EUR/100 kg | 55,25 |
0406 90 73 9900 | L04 | EUR/100 kg | 32,08 |
L40 | EUR/100 kg | 45,96 |
0406 90 75 9900 | L04 | EUR/100 kg | 32,72 |
L40 | EUR/100 kg | 47,05 |
0406 90 76 9300 | L04 | EUR/100 kg | 29,05 |
L40 | EUR/100 kg | 41,58 |
0406 90 76 9400 | L04 | EUR/100 kg | 32,53 |
L40 | EUR/100 kg | 46,57 |
0406 90 76 9500 | L04 | EUR/100 kg | 30,13 |
L40 | EUR/100 kg | 42,76 |
0406 90 78 9100 | L04 | EUR/100 kg | 31,86 |
L40 | EUR/100 kg | 46,55 |
0406 90 78 9300 | L04 | EUR/100 kg | 31,56 |
L40 | EUR/100 kg | 45,08 |
0406 90 79 9900 | L04 | EUR/100 kg | 26,06 |
L40 | EUR/100 kg | 37,47 |
0406 90 81 9900 | L04 | EUR/100 kg | 32,53 |
L40 | EUR/100 kg | 46,57 |
0406 90 85 9930 | L04 | EUR/100 kg | 35,66 |
L40 | EUR/100 kg | 51,34 |
0406 90 85 9970 | L04 | EUR/100 kg | 32,72 |
L40 | EUR/100 kg | 47,05 |
0406 90 86 9200 | L04 | EUR/100 kg | 31,63 |
L40 | EUR/100 kg | 46,89 |
0406 90 86 9400 | L04 | EUR/100 kg | 33,89 |
L40 | EUR/100 kg | 49,55 |
0406 90 86 9900 | L04 | EUR/100 kg | 35,66 |
L40 | EUR/100 kg | 51,34 |
0406 90 87 9300 | L04 | EUR/100 kg | 29,45 |
L40 | EUR/100 kg | 43,52 |
0406 90 87 9400 | L04 | EUR/100 kg | 30,07 |
L40 | EUR/100 kg | 43,95 |
0406 90 87 9951 | L04 | EUR/100 kg | 31,95 |
L40 | EUR/100 kg | 45,74 |
0406 90 87 9971 | L04 | EUR/100 kg | 31,95 |
L40 | EUR/100 kg | 45,74 |
0406 90 87 9973 | L04 | EUR/100 kg | 31,37 |
L40 | EUR/100 kg | 44,91 |
0406 90 87 9974 | L04 | EUR/100 kg | 33,61 |
L40 | EUR/100 kg | 47,89 |
0406 90 87 9975 | L04 | EUR/100 kg | 33,32 |
L40 | EUR/100 kg | 47,09 |
0406 90 87 9979 | L04 | EUR/100 kg | 31,39 |
L40 | EUR/100 kg | 45,14 |
0406 90 88 9300 | L04 | EUR/100 kg | 26,01 |
L40 | EUR/100 kg | 38,30 |
0406 90 88 9500 | L04 | EUR/100 kg | 26,82 |
L40 | EUR/100 kg | 38,32 |
[1] As for the relevant products intended for exports to Dominican Republic under the quota 2006/2007 referred to in the Decision 98/486/EC, and complying with the conditions laid down in Article 20a of Regulation (EC) No 174/1999, the following rates should apply:
--------------------------------------------------
