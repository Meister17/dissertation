Prior notification of a concentration
(Case COMP/M.4021 — CRH/Baubedarf Lehrmann/Dieter Niederste-Hollenberg/Bauking)
Candidate case for simplified procedure
(2005/C 278/07)
(Text with EEA relevance)
1. On 28 October 2005, the Commission received a notification of a proposed concentration pursuant to Article 4 of Council Regulation (EC) No 139/2004 [1] by which the undertakings CRH Deutschland GmbH controlled by CRH plc ("CRH", Ireland), Baubedarf Lehrmann Holding GmbH & Co. KG ("Baubedarf Lehrmann", Germany) and Mr. Dieter Niederste-Hollenberg (Germany) acquire within the meaning of Article 3(1)(b) of the Council Regulation joint control of the undertaking Bauking AG ("Bauking", Germany) by way of purchase of shares.
2. The business activities of the undertakings concerned are:
- for undertaking CRH: production and distribution of building materials;
- for undertaking Baubedarf Lehrmann: real estate property;
- for undertaking Dieter Niederste-Hollenberg: real estate property;
- for undertaking Bauking: Do-it-yourself outlets, builders merchant.
3. On preliminary examination, the Commission finds that the notified transaction could fall within the scope of Regulation (EC) No 139/2004. However, the final decision on this point is reserved. Pursuant to the Commission Notice on a simplified procedure for treatment of certain concentrations under Council Regulation (EC) No 139/2004 [2] it should be noted that this case is a candidate for treatment under the procedure set out in the Notice.
4. The Commission invites interested third parties to submit their possible observations on the proposed operation to the Commission.
Observations must reach the Commission not later than 10 days following the date of this publication. Observations can be sent to the Commission by fax (No (32-2) 296 43 01 or 296 72 44) or by post, under reference number COMP/M.4021 — CRH/Baubedarf Lehrmann/Dieter Niederste-Hollenberg/Bauking, to the following address:
European Commission
Directorate-General for Competition
Merger Registry
J-70
BE-1049 Brussels
[1] OJ L 24, 29.1.2004, p. 1.
[2] OJ C 56, 5.3.2005, p. 32.
--------------------------------------------------
