COUNCIL REGULATION (EEC) No 1764/92 of 29 June 1992 amending the arrangements for the import into the Community of certain agricultural products originating in Algeria, Cyprus, Egypt, Israel, Jordan, Lebanon, Malta, Morocco, Syria and Tunisia
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 113 thereof,
Having regard to the proposal from the Commission,
Whereas, in the general context of a new Mediterranean policy, in order to strengthen the links and increase cooperation with the countries of the region, the Council and the Commission adopted a Resolution on trade with Mediterranean non-member countries at the Council meeting on 18 and 19 December 1990;
Whereas that Resolution provides in particular for the implementation of measures to promote agricultural exports from those countries to the Community and whereas the detailed arrangements for the application of such measures must therefore be defined;
Whereas, in order to do this, it is necessary to amend the arrangements for imports into the Community under the provisions of the Protocols to the Association or Cooperation Agreements with Algeria, Cyprus, Egypt, Israel, Jordan, Lebanon, Malta, Moroco, Syria and Tunisia,
HAS ADOPTED THIS REGULATION:
Article 1
1. The customs duties applicable on 31 December 1991 in the Community as constituted on 31 December 1985 to products listed in Annex II to the Treaty, which originate in the Mediterranean non-member countries in question and for which tariff dismantling is to extend beyond 1 January 1993 pursuant to the Protocols to the Association or Cooperation Agreements shown in Annex I hereto, shall be abolished in two equal stages, from 1 January 1992 and from 1 January 1993.
2. Paragraph 1 shall apply within the limits, if any, of the tariff quotas and timetables laid down in the Protocols referred to in that paragraph and shall take account of any special provisions laid down therein.
3. When, as a result of the application of paragraph 1, customs duties reach a level of 2 % or less, they shall be suspended in full.
This measure shall apply mutatis mutandis to specific customs duties which do not exceed 2 % ad valorem.
Article 2
1. The tariff quotas and reference quantities laid down, for products listed in Annex II to the Treaty, in the Protocols referred to in Article 1 shall be increased, in four equal stages of 5 % each year from 1992 to 1995 within the limits of the timetables referred to in Article 1 (2).
This increase shall be 3 % for tariff quotas for products listed in Annex II to this Regulation.
2. The increase in tariff quotas shall be applied to products originating in Cyprus only if an increase is not already provided for in the Protocol concluded between the Community and the Republic of Cyprus referred to in Annex I to this Regulation.
Article 3
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Communities.
It shall apply from 1 June 1992.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Luxembourg, 29 June 1992.
For the Council
The President
Jorge BRACA DE MACEDO
ANNEX I
List of protocols referred to in Article 1
- Additional Protocol to the Cooperation Agreement between the European Economic Community and the People's Democratic Republic of Algeria (OJ No L 297, 21. 10. 1987, p. 1);
- Protocol laying down the conditions and procedures for the implementation of the second stage of the Agreement establishing an Association between the European Economic Community and the Republic of Cyprus and adapting certain provisions of the Agreement (OJ No L 393, 31. 12. 1987, p. 1);
- Additional Protocol to the Cooperation Agreement between the European Economic Community and the Arab Republic of Egypt (OJ No L 297, 21. 10. 1987, p. 10);
- Fourth Additional Protocol to the Agreement between the European Economic Community and the State of Israel (OJ No L 327, 30. 11. 1988, p. 35);
- Additional Protocol to the Cooperation Agreement between the European Economic Community and the Hashemite Kingdom of Jordan (OJ No L 297, 21. 10. 1987, p. 18);
- Additional Protocol to the Cooperation Agreement between the European Economic Community and the Lebanese Republic (OJ No L 297, 21. 10. 1987, p. 28);
- Supplementary Protocol to the Agreement establishing an Association between the European Economic Community and Malta (OJ No L 81, 23. 3. 1989, p. 1);
- Additional Protocol to the Cooperation Agreement between the European Economic Community and the Kingdom of Morocco (OJ No L 224, 13. 8. 1988, p. 17);
- Additional Protocol to the Cooperation Agreement between the European Economic Community and the Syrian Arab Republic (OJ No L 327, 30. 11. 1988, p. 57);
- Additional Protocol to the Cooperation Agreement between the European Economic Community and the Republic of Tunisia (OJ No L 297, 21. 10. 1987, p. 35).
ANNEX II
List of products referred to in Article 2 (1) for which the annual increase in tariff quotas laid down in the protocols shall be limited to 3 %
>TABLE>
