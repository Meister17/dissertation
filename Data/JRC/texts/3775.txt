Judgment of the Court
(Third Chamber)
of 13 October 2005
in Case C-379/04: reference for a preliminary ruling from the Landgericht Würzburg, Richard Dahms GmbH
v Fränkischer Weinbauverband eV [1]
In Case C-379/04: reference for a preliminary ruling under Article 234 EC from the Landgericht Würzburg (Germany), made by decision of 23 August 2004, received at the Court on 3 September 2004, in the proceedings between Richard Dahms GmbH and Fränkischer Weinbauverband eV, — the Court (Third Chamber), composed of A. Rosas, President of the Chamber, A. La Pergola, A. Borg Barthet, U. Lõhmus (Rapporteur) and A. Ó Caoimh, Judges; L.A. Geelhoed, Advocate General; R. Grass, Registrar, gave a judgment on 13 October 2005, the operative part of which is as follows:
[1] OJ C 262 of 23.10.2004.
--------------------------------------------------
