Commission Regulation (EC) No 2163/2005
of 22 December 2005
providing for the rejection of applications for export licences for beef and veal products
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1254/1999 of 17 May 1999 on the common organisation of the market in beef and veal [1],
Having regard to Commission Regulation (EC) No 1445/95 of 26 June 1995 on rules of application for import and export licences in the beef and veal sector and repealing Regulation (EEC) No 2377/80 [2], and in particular points (b) and (c) of the first subparagraph of Article 10(2) and Article 10(2a) thereof,
Whereas:
(1) On 20 December 2005, the Commission announced its intention of amending Regulation (EC) No 2000/2005 of 7 December 2005 fixing the export refunds on beef and veal [3] to abolish the payment of refunds for exports of adult male bovine animals for slaughter to Egypt and Lebanon.
(2) In the days following that announcement, applications were received for licences for the export to the abovementioned countries of numbers of adult male bovine animals for slaughter exceeding those normally exported. These applications must be considered to be speculative in view of the amendment to Regulation (EC) No 2000/2005.
(3) The applications for export licences that have not yet been issued should therefore be rejected and the lodging of applications for export licences shall be suspended for five working days from the date of entry into force of this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
Applications for export licences with advance fixing of the refund for the export of adult male bovine animals for slaughter (CN code 0102 90 71 9000) to Egypt and the Lebanon lodged during the four working days preceding the entry into force of this Regulation shall be rejected.
The lodging of applications for export licences for these animals shall be suspended for five working days following the entry into force of this Regulation.
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 22 December 2005.
For the Commission
J. M. Silva Rodríguez
Director-General for Agriculture and Rural Development
[1] OJ L 160, 26.6.1999, p. 21. Regulation as last amended by Regulation (EC) No 1913/2005 (OJ L 307, 25.11.2005, p. 2).
[2] OJ L 143, 27.6.1995, p. 35. Regulation as last amended by Regulation (EC) No 1118/2004 (OJ L 217, 17.6.2004, p. 10).
[3] OJ L 320, 8.12.2005, p. 46.
--------------------------------------------------
