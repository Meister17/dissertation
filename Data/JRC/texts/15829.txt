Judgment of the Court of First Instance of 1 February 2006 — Rodrigues Carvalhais v OHIM
(Case T-206/04) [1]
Parties
Applicant: Fernando Rodrigues Carvalhais (Almada, Portugal) (represented by: P. Graça initially, then J. Lopes, lawyers)
Defendant: Office for Harmonisation in the Internal Market (Trade Marks and Designs) (represented by: J. Novais Gonçalves, Agent)
Other party to the proceedings before the Board of Appeal of OHIM, intervening before the Court of First Instance: Profilpas Snc (Cadoneghe, Italy) (represented by: J.L. Revenga Santos initially, then J.M. Monravá, lawyers)
Action
brought against the decision of the First Board of Appeal of OHIM of 18 March 2004 (Case R 408/2003-1), concerning opposition proceedings between M. Fernando Rodrigues Carvalhais and Profilpas Snc
Operative part of the judgment
The Court:
1. Dismisses the action.
2. Orders the applicant to pay the costs.
[1] OJ C 217 of 28.8.2004.
--------------------------------------------------
