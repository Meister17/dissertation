Publication of an application for registration pursuant to Article 6(2) of Regulation (EEC) No 2081/92 on the protection of geographical indications and designations of origin
(2005/C 126/07)
This publication confers the right to object to the application pursuant to Articles 7 and 12d of the abovementioned Regulation. Any objection to this application must be submitted via the competent authority in a Member State, in a WTO member country or in a third country recognized in accordance with Article 12(3) within a time limit of six months from the date of this publication. The arguments for publication are set out below, in particular under 4.6, and are considered to justify the application within the meaning of Regulation (EEC) No 2081/92.
SUMMARY
COUNCIL REGULATION (EEC) No 2081/92
"SALAME CREMONA"
EC No: IT/00265/ 27.12.2002
PDO ( ) PGI ( X )
This summary has been drawn up for information purposes only. For full details, in particular the producers of the PDO or PGI concerned, please consult the complete version of the product specification obtainable at national level or from the European Commission [1].
1. Responsible department in the Member State:
Name: | Ministero delle Politiche Agricole e Forestali |
Address: | Via XX Settembre n. 20 — 00187 Roma |
Tel.: | (06) 481 99 68 |
Fax: | (06) 42 01 31 26 |
E-mail: | qualita@politiche agricole.it |
2. Group:
2.1.Name: | Consorzio tutela del Salame Cremona |
2.2.Address: | Via Lanaioli n. 1 — 26100 Cremona |
Tel.-fax: | (0372) 59 82 51 |
2.3.Composition: | producer/processor (X) other ( ) |
3. Type of product:
Meat preparations — Class 1.2.
4. Specification:
(Summary of requirements under Article 4(2)):
Name : "Salame Cremona"
Description Physical and morphological features
A weight of not less than 500 gr after maturing
Diameter of not less than 65 mm when prepared
Length of not less than 150 mm when prepared
Chemical and physico-chemical features
Total protein content: | at least 20,0 % |
Collagen: protein ratio: | maximum 0,10 |
Water: protein ratio: | maximum 2,00 |
Fat: protein ratio: | maximum 2,00 |
pH: | 5,20 or higher |
Microbiological features
Mesophilic total colony units: > 1 × 107 per gram, with a prevalence of lactic acid bacteria and coccus.
Organoleptic features
Appearance: cylindrical, with irregular features.
Consistency: should be compact and tender
Appearance on slicing: slices are compact and homogenous, with typical binding of fragments of muscle and fat, so that the edges are not clearly visible (so-called "smelmato" appearance). There is no visible gristle.
Colour: deep red
Smell: typical spicy fragrance.
Geographical area : The area where Salame Cremona is manufactured comprises the regions of Lombardy, Emilia Romagna, Piedmont and Veneto.
Proof of origin :
The main historical documents, which provide clear and specific evidence of the origin of the product and its link with the region, date back to 1231 and are preserved in the Cremona State Archive. They confirm trade between the territory of Cremona and neighbouring states in pork and meat products. A series of Renaissance documents in the "Litterarum" and "Fragmentorum" of the state Archive prove conclusively not only the presence but also the importance of salame in the area identified in the specification. Reports drafted when Bishop Cesare Speciano (1599-1606) visited the convents mention in the section on "day-to-day life" that "on days when meat is eaten" a certain quantity of salame was also distributed. Even today, the traditional presence of Salame Cremona is gaining in importance in the main agricultural and food fairs of Lombardy and the Po Valley.
Socio-economic references demonstrate the presence of a large number of producers engaged in the processing of pork in the plain of the Po, an industry which spread thanks to its integration with the dairy and cheese industry and cereal growing (especially maize).
Method of production :
The production process may be summed up as follows: the raw materials for the product must come from pigs born, raised and slaughtered in one of the following regions: Friuli-Venezia Giulia, Veneto, Lombardy, Piedmont, Emilia-Romagna, Umbria, Tuscany, Marche, Abruzzi, Lazio and Molise. They may be of the traditional Large White Italian and Landrace Italian breeds, as improved by the Italian herd book, or progeny of sows of those breeds; progeny of sows of the Duroc Italian breed, as improved by the Italian herd book; progeny of sows of other breeds or cross-bred sows provided they come from selection or crossing schemes whose purposes are not incompatible with those of the Italian herd book for the production of heavy pigs.
The pigs are sent for slaughter between the end of their ninth month and their 15th month. The average weight of each individual sent for slaughter must lie between 144 and 176 kg.
The pork used for mincing consists of muscle tissue from carcases and pieces of striated muscle and fat.
Ingredients: Salt, spices, peppercorns and roughly milled pepper, garlic crushed and worked into the mixture.
The following may also be used: still white or red wine, sugar and/or dextrose and/or fructose and/or lactose, starter ferment, sodium and/or potassium nitrite, ascorbic acid and its sodium salt.
Mechanically recovered meat is prohibited.
Preparation: The pieces of muscle and fat are carefully trimmed, with removal of the larger bits of connective tissue, soft fat, lymph nodes and nerves. The meat is ground in mincers using blades with 6 mm holes. During mincing, the meat must be at a temperature higher than 0 °C. The meat is salted during slaughter, the other ingredients and flavourings are added to the minced meat. The ingredients are mixed in a vacuum machine or at atmospheric pressure for a long period. Salame Cremona is packed into natural porcine, bovine, equine or ovine casings of an initial diameter of 65 mm or more. The sausage is manually or mechanically fastened with string. The product may be kept in storage chambers for up to one day at a temperature between 2 °C and 10 °C. The product is warm-dried (temperature between 15 °C and 25 °C).
Maturing takes place in premises where there is adequate air circulation at a temperature between 11 °C and 16 °C for a period of at least five weeks.
The period of maturing varies, according to the initial diameter of the casing
Salame Cremona may be released for consumption by the piece, in vacuum packs or in protective atmosphere packs, whole or in thick or thin slices. Producer who wish to use the PGI must respect the rules deposited with the European Union in every particular.
Production, packaging and slicing must be carried out under the supervision of the control body referred to in Article 7 of the specification, exclusively in the production area defined in Article 3 of the specification, in order to permit checks and ensure traceability and not to degrade the product's quality features.
Link :
The production of salame is very closely linked with local pig breeding establishments, which go back to Roman times. Salame Cremona is also closely connected with its environment, resulting from the growth, first in the Cremona area and next in the whole plain of the Po, of pig-breeding in connection with cheese-making and maize-growing.
For some time past, in addition to the excellent synergy between dairy cattle farming and pig farming, along with cereal growing, the climate (which is prone to fog and not too windy) has been favourable to the product, enabling maturing salame to acquire its special features of tenderness, unctuosity and spicy aroma. The production area has soils typical of alluvial regions and has been used for pig raising for many centuries, first using the so-called "family system" and then on an increasingly professional scale. The areas of production all have climates prone to fog, are very humid and have little wind, which favours the maturing of sausage products such as Salame Cremona. The part of the production area near the Po valley is exclusively rolling, crossed by rivers and canals and marked by crop products, particularly grassland and maize.
Throughout the production area autumns and winters are fairly harsh, and very wet and foggy, while springs are mild and rainy and summers have quite high temperatures and frequent short, and very often heavy, rainy periods. Pigs have always been present on the Po plain and in adjacent valleys since they are the animals best adapted to making most profitable use of the available maize and by-products of milk processing.
The area of production has always proved ideal both for the provision of the raw materials (maize and curds) and for the processing and storage of pigmeat, particularly Salame Cremona, which needs a very humid and relatively windless climate if it is to achieve top quality.
However, all this would not have been enough to produce the quality necessary for Salame Cremona without the human factor: over time, in the production area, highly specific techniques of preparation and maturing of salame have been perfected.
Even today, Salame Cremona is produced according to processes that fully respect tradition, and fit in well with the new technologies introduced into processing practices.
The environmental factor (the climate) and the human factor (the highly specialised technical capacity of the workers producing Salame Cremona), are still nowadays basic and irreplaceable in the specificity and reputation of the product.
Salame Cremona is a very well-known product with a long-established reputation, as is clear from its traditional presence in agricultural and food fairs of the Po valley, and its wide distribution on the major national and international markets.
This is confirmed by the appearance of Salame Cremona on lists of the main food products with designation of Italian origin, in annexes to bilateral agreements between Italy and other European countries from the 1950s to the 1970s (Germany, France, Austria, Spain) on the protection of geographical designations of origin.
Name: | Istituto Parma Qualità — I.P.Q. |
Address: | Via Roma, 82/c — 43013 Langhirano (PR) |
Inspection body Labelling :
The label must bear, in clear, indelible letters larger than those of any other words on the label, the words Salame Cremona, and the words Indicazione Geografica Protetta and/or IGP, (protected geographical indication — PGI) which must also be translated into the language of the country where the product is marketed.
It is prohibited to add any other description not expressly provided for.
However, references to the name of a person or company, or a private mark, are permitted, as long as they do not constitute praise of the product and are not such as to mislead the consumer.
The label must also bear the Community symbol referred to in Article 1 of Commission Regulation (EC) No 1276/98
National requirements : —
[1] European Commission, Directorate-General for Agriculture, Agricultural product quality policy, B-1049 Brussels.
--------------------------------------------------
