Judgment of the Court of First Instance of 22 November 2005 — Vanhellemont v Commission
(Case T-396/03) [1]
Parties
Applicant(s): Joseph Vanhellemont (Merchtem, Belgium) (represented by: L. Vogel, lawyer)
Defendant(s): Commission of the European Communities (represented by: J. Currall and H. Kraemer, Agents)
Application for
First, annulment of the decision of the Commission of 26 August 2003 rejecting the complaint made by the applicant seeking the organisation of a recount of votes cast in the elections of December 2002 for the Brussels local staff committee of the Commission, and also, as far as necessary, the annulment of the decisions against which that complaint was directed and, second, a request for damages
Operative part of the judgment
The Court:
1) Annuls the decision of the Commission not to intervene following the applicant's challenges of 23 December 2002 seeking the organisation of a further recount of the votes cast in the elections of December 2002 for the Brussels local staff committee;
2) Rejects the remainder of the application;
3) Orders the Commission to pay the costs.
[1] OJ C 47, 21.2.2004.
--------------------------------------------------
