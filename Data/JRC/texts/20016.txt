COMMISSION DECISION of 26 June 1995 amending Decision 95/90/EC laying down specific conditions for importing fishery and aquaculture products from Albania (Text with EEA relevance) (95/235/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/493/EEC of 22 July 1991 laying down the health conditions for the production and the placing on the market of fishery products (1), as last amended by the Act of Accession of Austria, Finland and Sweden, and in particular Article 11 (5) thereof,
Whereas the list of establishments approved by Albania for importing fishery and aquaculture products into the Community has been drawn up in Commission Decision 95/90/EC (2); whereas this list may be amended following the communication of a new list by the competent authority in Albania;
Whereas the competent authority in Albania has communicated a new list adding two establishments, and modifying the data of two establishments;
Whereas it is necessary to amend the list of approved establishments accordingly;
Whereas the measures provided for in this Decision have been drawn up in accordance with the procedure laid down by Commission Decision 90/13/EEC (3),
HAS ADOPTED THIS DECISION:
Article 1
Annex B to Decision 95/90/EC is replaced by the Annex to this Decision.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 26 June 1995.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ No L 268, 24. 9. 1991, p. 15.
(2) OJ No L 70, 30. 3. 1995, p. 27.
(3) OJ No L 8, 11. 1. 1990, p. 70.
ANNEX
'ANNEX B
>TABLE>
