COUNCIL DECISION of 3 May 1989 accepting on behalf of the Community the recommendation of 5 June 1962 of the Customs Cooperation Council concerning the customs treatment of registered baggage carried by rail as amended on 21 June 1988 (89/339/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 113 thereof,
Having regard to the proposal from the Commission,
Whereas on 21 June 1988 the Customs Cooperation Council adopted amendments to the recommendation of 5 June 1962, introducing a new specimen customs declaration form for registered baggage and permitting customs or economic unions to accept the recommendation;
Whereas the new specimen customs declaration form for registered baggage was approved by the Customs Cooperation Council on a proposal from the Community; whereas that specimen form corresponds to the draft submitted by the Community and is designed to update the content of the customs declaration for registered baggage and to facilitate the introduction of that document by countries which hitherto were unable to use it;
Whereas the amendment is of fundamental interest to the Community which should therefore adopt the recommendation of 5 June 1962, as amended,
HAS DECIDED AS FOLLOWS:
Article 1
The recommendation of 5 June 1962 of the Customs Cooperation Council concerning the customs treatment of registered baggage carried by rail, as amended on 21 June 1988, is hereby accepted on behalf of the Community with immediate effect.
The text of the recommendation is attached to this Decision.
Article 2
The President of the Council is hereby authorized to designate the person empowered to notify the Secretary-General of the Customs Cooperation Council of the acceptance by the Community of the recommendation referred to in Article 1.
Done at Brussels, 3 May 1989.
For the Council
The President
P. SOLBES
RECOMMENDATION OF THE CUSTOMS COOPERATION COUNCIL of 5 June 1962 concerning the customs treatment of registered baggage carried by rail (amended on 21 June 1988)
THE CUSTOMS COOPERATION COUNCIL,
CONSIDERING that the efforts made by the railway authorities to ensure efficiency in the international transport of registered baggage deserve to be supported,
DESIRING, to that end, to facilitate the expeditious handling of such baggage by simplifying customs formalities,
CONSIDERING that, in so far as possible, passengers should not be required to report in person to the customs authorities of the countries of departure and destination for the clearance of their registered baggage, and should be able to claim their baggage immediately on arrival at their destination,
RECOMMENDS that Members of the Council and customs or economic unions, apply the following provisions with regard to registered baggage:
1. When having their baggage registered by the railway authorities, passengers shall have the possibility of making a declaration on the appended form in order to expedite customs formalities;
2. The form shall be printed either on the back of the consignment note made out by the railway authorities or on a separate sheet to be glued to the consignment note; it shall be printed in the language (or one of the official languages) of the country of departure, but the passenger
shall be given the opportunity to obtain a translation in another language;
3. The declaration shall be presented by the railway authorities to the customs authorities of the countries of departure and destination where so required;
4. The written declaration shall be regarded as being in substitution for, and shall have the same effect as, the declaration normally required from passengers;
5. The customs authorities shall, so far as they deem it possible, waive the examination of the contents of baggage covered by a written declaration;
6. Where the customs authorities waive examination of the contents of baggage, it shall be released immediately to the railway authorities for forwarding to destination;
7. The customs authorities remain free to adopt any control measures they deem necessary in order to prevent abuses,
NOTWITHSTANDING the provisions of paragraph 1 above, the declaration form may be adapted where appropriate by agreement between customs administrations,
THE COUNCIL invites Members and customs or economic unions which accept the present recommendation to notify the Secretary-General of the Council and specify the date
and the terms of its implementation. The Secretary-General will communicate this information to Members' customs administrations, to customs or economic unions and to the International Union of Railways.
Annex
CUSTOMS DECLARATION FOR REGISTERED BAGGAGE 1. I HEREBY DECLARE
(a) that the baggage referred to below contains only articles of personal use normally used when travelling, such as clothing, household linen, toiletries, books and sports equipment, and that these articles are not being imported for commercial purposes;
(b) that the baggage does not contain:
- foodstuffs, tobacco, alcoholic beverages, anethol, firearms, sidearms, ammunition, explosives, drugs, live animals, plants, radio transmitters or transmitter-receivers, currency, species and products obtained from species protected under the Washington Convention of 3 March 1973 on International Trade in Endangered Species of Wild Flora and Fauna; articles forbidden by the laws of the country of destination on the protection of public decency and morality,
- goods intended for distribution free of charge or otherwise or for professional or commercial purposes,
- goods bought or received by myself outside the customs territory of my country and not yet declared to the customs authorities of my country of normal residence (this restriction applies only when returning to the country of normal residence).
2. I HEREBY AUTHORIZE the railway authorities to carry out all customs formalities.
3. I KNOW that making a false statement renders me liable to prosecution and seizure of my goods.
Country of destination: .
Place of destination: .
Number of items Number of persons accompanying the passenger
Number of items
Number of persons accompanying the passenger
IN BLOCK LETTERS
SURNAME:
OTHER NAMES:
.
Normal residence
Normal residence:
Street: .
No: .
Town: .
Country: .
Date-stamp of
departure station
Signature of passenger:
.
Consignment Note No
