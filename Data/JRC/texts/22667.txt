*****
COUNCIL REGULATION (ECSC, EEC, EURATOM) No 2152/82
of 28 July 1982
amending Regulation (Euratom, ECSC, EEC) No 549/69 determining the categories of officials and other servants of the European Communities to whom the provisions of Article 12, the second paragraph of Article 13 and Article 14 of the Protocol on the Privileges and Immunities of the Communities apply
THE COUNCIL OF THE EUROPEAN
COMMUNITIES,
Having regard to the Treaty establishing a single Council and a single Commission of the European Communities, and in particular the first paragraph of Article 28 thereof,
Having regard to the Protocol on the Privileges and Immunities of the European Communities, and in particular Articles 16 and 22 thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament (1),
Whereas Regulation (Euratom, ECSC, EEC) No 549/69 (2), as last amended by Regulation (ECSC, EEC Euratom) No 1545/73 (3), should be amended in order to take account of Council Regulation (ECSC, EEC, Euratom) No 2150/82 of 28 July 1982 introducing special and temporary measures to terminate the service of officials of the European Communities consequent upon the accession of the Hellenic Republic (4),
HAS ADOPTED THIS REGULATION:
Article 1
The following subparagraph (h) shall be added to Article 2 of Regulation (Euratom, ECSC, EEC) No 549/69:
'(h) those entitled to the allowance provided for in the event of termination of service under Article 2 of Regulation (ECSC, EEC, Euratom) No 2150/82.'
Article 2
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Communities.
It shall apply from the time when Regulation (ECSC, EEC, Euratom) No 2150/82 enters into force.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 28 July 1982.
For the Council
The President
O. MOELLER
(1) OJ No C 182, 19. 7. 1982, p. 127.
(2) OJ No L 74, 27. 3. 1969, p. 1.
(3) OJ No L 155, 11. 6. 1973, p. 7.
(4) See page 1 of this Official Journal.
