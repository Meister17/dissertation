Commission Decision
of 19 April 2002
recognising in principle the completeness of the dossiers submitted for detailed examination in view of the possible inclusion of clothianidin and Pseudozyma flocculosa in Annex I to Council Directive 91/414/EEC concerning the placing of plant-protection products on the market
(notified under document number C(2002) 1434)
(Text with EEA relevance)
(2002/305/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/414/EEC of 15 July 1991 concerning the placing of plant-protection products on the market(1), as last amended by Commission Directive 2002/18/EC(2), and in particular Article 6(3) thereof,
Whereas:
(1) Directive 91/414/EEC provides for the development of a Community list of active substances authorised for incorporation in plant protection products.
(2) A dossier for the active substance clothianidin was submitted by Takeda Chemical Industries Ltd, United Kingdom, to the authorities of Belgium on 26 September 2001 with an application to obtain its inclusion in Annex I to Directive 91/414/EEC. A similar application was submitted by Maasmond-Westland, the Netherlands, on 6 March 2001 to the authorities of the Netherlands concerning the microbial active substance Pseudozyma flocculosa.
(3) The authorities of Belgium and the Netherlands have indicated to the Commission that, on preliminary examination, the dossiers for the active substances concerned appear to satisfy the data and information requirements of Annex II to Directive 91/414/EEC. The dossiers submitted appear also to satisfy the data and information requirements of Annex III to Directive 91/414/EEC in respect of one plant protection product containing the active substance concerned. In accordance with Article 6(2) of Directive 91/414/EEC, the dossiers were subsequently forwarded by the respective applicants to the Commission and other Member States, and were referred to the Standing Committee on the Food Chain and Animal Health.
(4) By this Decision it should be formally confirmed at Community level that the dossiers are considered as satisfying in principle the data and information requirements provided for in Annex II and, for at least one plant protection product containing the active substance concerned, the requirements of Annex III to Directive 91/414/EEC.
(5) This Decision should not prejudice the right of the Commission to request the applicant to submit further data or information to the Member State designated as Rapporteur in respect of a given substance in order to clarify certain points in the dossier.
(6) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
The dossiers concerning the active substances identified in the Annex to this Decision, which were submitted to the Commission and the Member States with a view to obtaining the inclusion of those substances in Annex I to Directive 91/414/EEC, satisfy in principle the data and information requirements set out in Annex II to Directive 91/414/EEC.
The dossiers also satisfy the data and information requirements set out in Annex III to Directive 91/414/EEC in respect of one plant protection product containing the active substance, taking into account the uses proposed.
Article 2
The rapporteur Member States shall pursue the detailed examination for the dossiers concerned and shall report the conclusions of their examinations accompanied by any recommendations on the inclusion or non-inclusion of the active substance concerned in Annex I of Directive 91/414/EEC and any conditions related thereto to the Commission as soon as possible and at the latest within a period of one year from the date of publication of this Decision in the Official Journal of the European Communities.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 19 April 2002.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 230, 19.8.1991, p. 1.
(2) OJ L 55, 26.2.2002, p. 29.
ANNEX
ACTIVE SUBSTANCES CONCERNED BY THIS DECISION
>TABLE>
