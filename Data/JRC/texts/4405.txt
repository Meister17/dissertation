Commission Regulation (EC) No 2176/2005
of 23 December 2005
fixing the Community withdrawal and selling prices for the fishery products listed in Annex I to Council Regulation (EC) No 104/2000 for the 2006 fishing year
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 104/2000 of 17 December 1999 on the common organisation of the markets in fishery and aquaculture products [1], and in particular Article 20(3) and Article 22 thereof,
Whereas:
(1) Regulation (EC) No 104/2000 provides that the Community withdrawal and selling prices for each of the products listed in Annex I thereto are to be fixed on the basis of the freshness, size or weight and presentation of the product by applying the conversion factor for the product category concerned to an amount not more than 90 % of the relevant guide price.
(2) The withdrawal prices may be multiplied by adjustment factors in landing areas which are very distant from the main centres of consumption in the Community. The guide prices for the 2006 fishing year were fixed for all the products concerned by Council Regulation (EC) No …/… [2].
(3) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Fishery Products,
HAS ADOPTED THIS REGULATION:
Article 1
The conversion factors used for calculating the Community withdrawal and selling prices, as referred to in Article 20 and 22 of Regulation (EC) No 104/2000, for the 2006 fishing year for the products listed in Annex I to that Regulation, are set out in Annex I to this Regulation.
Article 2
The Community withdrawal and selling prices applicable for the 2006 fishing year and the products to which they relate are set out in Annex II.
Article 3
The withdrawal prices applicable for the 2006 fishing year in landing areas which are very distant from the main centres of consumption in the Community and the products to which those prices relate are set out in Annex III.
Article 4
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Union.
It shall apply from 1 January 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 23 December 2005.
For the Commission
Joe Borg
Member of the Commission
[1] OJ L 17, 21.1.2000, p. 22. Regulation as amended by the 2003 Act of Accession.
[2] Not yet published in the Official Journal.
--------------------------------------------------
ANNEX I
Conversion factors for the products listed in points A, B and C of Annex I to Regulation (EC) No 104/2000
Species | Size | Conversion factors |
Gutted fish, with head | Whole fish |
Extra, A | Extra, A |
Herring of the species Clupea harengus | 1 | 0,00 | 0,47 |
2 | 0,00 | 0,72 |
3 | 0,00 | 0,68 |
4a | 0,00 | 0,43 |
4b | 0,00 | 0,43 |
4c | 0,00 | 0,90 |
5 | 0,00 | 0,80 |
6 | 0,00 | 0,40 |
7a | 0,00 | 0,40 |
7b | 0,00 | 0,36 |
8 | 0,00 | 0,30 |
Sardines of the species Sardina pilchardus | 1 | 0,00 | 0,51 |
2 | 0,00 | 0,64 |
3 | 0,00 | 0,72 |
4 | 0,00 | 0,47 |
Dogfish Squalus acanthias | 1 | 0,60 | 0,60 |
2 | 0,51 | 0,51 |
3 | 0,28 | 0,28 |
Dogfish Scyliorhinus spp. | 1 | 0,64 | 0,60 |
2 | 0,64 | 0,56 |
3 | 0,44 | 0,36 |
Redfish Sebastes spp. | 1 | 0,00 | 0,81 |
2 | 0,00 | 0,81 |
3 | 0,00 | 0,68 |
Cod of the species Gadus morhua | 1 | 0,72 | 0,52 |
2 | 0,72 | 0,52 |
3 | 0,68 | 0,40 |
4 | 0,54 | 0,30 |
5 | 0,38 | 0,22 |
Coalfish Pollachius virens | 1 | 0,72 | 0,56 |
2 | 0,72 | 0,56 |
3 | 0,71 | 0,55 |
4 | 0,61 | 0,30 |
Haddock Melanogrammus aeglefinus | 1 | 0,72 | 0,56 |
2 | 0,72 | 0,56 |
3 | 0,62 | 0,43 |
4 | 0,52 | 0,36 |
Whiting Merlangius merlangus | 1 | 0,66 | 0,50 |
2 | 0,64 | 0,48 |
3 | 0,60 | 0,44 |
4 | 0,41 | 0,30 |
Ling Molva spp. | 1 | 0,68 | 0,56 |
2 | 0,66 | 0,54 |
3 | 0,60 | 0,48 |
Mackerel of the species Scomber scombrus | 1 | 0,00 | 0,72 |
2 | 0,00 | 0,71 |
3 | 0,00 | 0,69 |
Species | Size | Conversion factors |
Gutted fish, with head | Whole fish |
Extra, A | Extra, A |
Spanish mackerel of the species Scomber japonicus | 1 | 0,00 | 0,77 |
2 | 0,00 | 0,77 |
3 | 0,00 | 0,63 |
4 | 0,00 | 0,47 |
Anchovies Engraulis spp. | 1 | 0,00 | 0,68 |
2 | 0,00 | 0,72 |
3 | 0,00 | 0,60 |
4 | 0,00 | 0,25 |
Plaice Pleuronectes platessa | 1 | 0,75 | 0,41 |
2 | 0,75 | 0,41 |
3 | 0,72 | 0,41 |
4 | 0,52 | 0,34 |
Hake of the species Merluccius merluccius | 1 | 0,90 | 0,71 |
2 | 0,68 | 0,53 |
3 | 0,68 | 0,52 |
4 | 0,56 | 0,43 |
5 | 0,52 | 0,41 |
Megrims Lepidorhombus spp. | 1 | 0,68 | 0,64 |
2 | 0,60 | 0,56 |
3 | 0,54 | 0,49 |
4 | 0,34 | 0,29 |
Dab Limanda limanda | 1 | 0,71 | 0,58 |
2 | 0,54 | 0,42 |
Flounder Platichthys flesus | 1 | 0,66 | 0,58 |
2 | 0,50 | 0,42 |
Albacore or long finned tuna Thunnus alalunga | 1 | 0,90 | 0,81 |
2 | 0,90 | 0,77 |
Cuttlefish (Sepia officinalis and Rossia macrosoma) | 1 | 0,00 | 0,64 |
2 | 0,00 | 0,64 |
3 | 0,00 | 0,40 |
Species | Size | Conversion factors | |
Whole fish Gutted fish, with head | Fish without head |
Extra, A | Extra, A |
Monkfish Lophius spp. | 1 | 0,61 | 0,77 | |
2 | 0,78 | 0,72 | |
3 | 0,78 | 0,68 | |
4 | 0,65 | 0,60 | |
5 | 0,36 | 0,43 | |
| | All presentations | |
Extra, A |
Shrimps of the species Crangon crangon | 1 | 0,59 | |
2 | 0,27 | |
| | Cooked in water | Fresh or chilled | |
Extra, A | Extra, A |
Deep-water prawns Pandalus borealis | 1 | 0,77 | 0,68 | |
2 | 0,27 | — | |
| | Whole | |
Edible crabs Cancer pagurus | 1 | 0,72 | |
2 | 0,54 | |
| | Whole | Tails |
E' | Extra, A | Extra, A |
Norway lobster Nephrops norvegicus | 1 | 0,86 | 0,86 | 0,81 |
2 | 0,86 | 0,59 | 0,68 |
3 | 0,77 | 0,59 | 0,50 |
4 | 0,50 | 0,41 | 0,41 |
| | Gutted fish, with head | Whole fish | |
Extra, A | Extra, A |
Sole (Solea spp.) | 1 | 0,75 | 0,58 | |
2 | 0,75 | 0,58 | |
3 | 0,71 | 0,54 | |
4 | 0,58 | 0,42 | |
5 | 0,50 | 0,33 | |
--------------------------------------------------
ANNEX II
Withdrawal and selling prices in the Community of the products listed in points A, B and C of Annex I to Regulation (EC) No 104/2000
Species | Size | Withdrawal price (EUR/tonne) |
Gutted fish, with head | Whole fish |
Extra, A | Extra, A |
Herring of the species Clupea harengus | 1 | 0 | 125 |
2 | 0 | 191 |
3 | 0 | 180 |
4a | 0 | 114 |
4b | 0 | 114 |
4c | 0 | 239 |
5 | 0 | 212 |
6 | 0 | 106 |
7a | 0 | 106 |
7b | 0 | 95 |
8 | 0 | 80 |
Sardines of the species Sardina pilchardus | 1 | 0 | 292 |
2 | 0 | 366 |
3 | 0 | 412 |
4 | 0 | 269 |
Dogfish Squalus acanthias | 1 | 647 | 647 |
2 | 550 | 550 |
3 | 302 | 302 |
Dogfish Scyliorhinus spp. | 1 | 488 | 458 |
2 | 488 | 427 |
3 | 336 | 275 |
Redfish Sebastes spp. | 1 | 0 | 920 |
2 | 0 | 920 |
3 | 0 | 772 |
Cod of the species Gadus morhua | 1 | 1180 | 852 |
2 | 1180 | 852 |
3 | 1115 | 656 |
4 | 885 | 492 |
5 | 623 | 361 |
Coalfish Pollachius virens | 1 | 538 | 418 |
2 | 538 | 418 |
3 | 530 | 411 |
4 | 456 | 224 |
Haddock Melanogrammus aeglefinus | 1 | 719 | 559 |
2 | 719 | 559 |
3 | 619 | 429 |
4 | 519 | 359 |
Species | Size | Withdrawal price (EUR/tonne) |
Gutted fish, with head | Whole fish |
Extra, A | Extra, A |
Whiting Merlangius merlangus | 1 | 618 | 469 |
2 | 600 | 450 |
3 | 562 | 412 |
4 | 384 | 281 |
Ling Molva spp. | 1 | 813 | 670 |
2 | 789 | 646 |
3 | 718 | 574 |
Mackerel of the species Scomber scombrus | 1 | 0 | 233 |
2 | 0 | 229 |
3 | 0 | 223 |
Spanish mackerel of the species Scomber japonicus | 1 | 0 | 226 |
2 | 0 | 226 |
3 | 0 | 185 |
4 | 0 | 138 |
Anchovies Engraulis spp. | 1 | 0 | 889 |
2 | 0 | 942 |
3 | 0 | 785 |
4 | 0 | 327 |
PlaicePleuronectes platessa
— 1 January to 30 April 2006 | 1 | 806 | 440 |
2 | 806 | 440 |
3 | 773 | 440 |
4 | 558 | 365 |
— 1 May to 31 December 2006 | 1 | 1113 | 608 |
2 | 1113 | 608 |
3 | 1068 | 608 |
4 | 772 | 505 |
Hake of the species Merluccius merluccius | 1 | 3308 | 2609 |
2 | 2499 | 1948 |
3 | 2499 | 1911 |
4 | 2058 | 1580 |
5 | 1911 | 1507 |
Megrims Lepidorhombus spp. | 1 | 1694 | 1594 |
2 | 1495 | 1395 |
3 | 1345 | 1221 |
4 | 847 | 722 |
Dab Limanda limanda | 1 | 626 | 511 |
2 | 476 | 370 |
Species | Size | Withdrawal price (EUR/tonne) |
Gutted fish, with head | Whole fish |
Extra, A | Extra, A |
Flounder Platichtys flesus | 1 | 343 | 301 |
2 | 260 | 218 |
Albacore or long-finned tuna Thunnus alalunga | 1 | 2229 | 1798 |
2 | 2229 | 1709 |
Cuttlefish Sepia officinalis and Rossia macrosoma | 1 | 0 | 1037 |
2 | 0 | 1037 |
3 | 0 | 648 |
| | Whole fish Gutted fish, with head | Fish without head |
Extra, A | Extra, A |
Monkfish Lophius spp. | 1 | 1749 | 4565 |
2 | 2236 | 4268 |
3 | 2236 | 4031 |
4 | 1864 | 3557 |
5 | 1032 | 2549 |
| | All presentations |
Extra, A |
Shrimps of the species Crangon crangon | 1 | 1432 |
2 | 655 |
| | Cooked in water | Fresh or chilled |
Extra, A | Extra, A |
Deep-water prawns Pandalus borealis | 1 | 4911 | 1087 |
2 | 1722 | — |
Species | Size | Selling prices (EUR/tonne) | |
Whole | |
Edible crabs Cancer pagurus | 1 | 1246 | | |
2 | 935 | | |
| | Whole | Tails |
E' | Extra, A | Extra, A |
Norway lobster Nephrops norvegicus | 1 | 4590 | 4590 | 3432 |
2 | 4590 | 3149 | 2881 |
3 | 4109 | 3149 | 2119 |
4 | 2669 | 2188 | 1737 |
| | Gutted fish, with head | Whole fish | |
Extra, A | Extra, A |
Sole Solea spp. | 1 | 5009 | 3874 | |
2 | 5009 | 3874 | |
3 | 4742 | 3607 | |
4 | 3874 | 2805 | |
5 | 3340 | 2204 | |
--------------------------------------------------
ANNEX III
Withdrawal prices in landing areas which are very distant from the main centres of consumption
Species | Landing area | Conversion factor | Size | Withdrawal price (in EUR/tonne) |
Gutted fish, with head | Whole fish |
Extra, A | Extra, A |
Herring of the species Clupea harengus | Coastal regions and islands of Ireland | 0,90 | 1 | 0 | 112 |
2 | 0 | 172 |
3 | 0 | 162 |
4a | 0 | 103 |
Coastal regions of eastern England from Berwick to Dover Coastal regions of Scotland from Portpatrick to Eyemouth and the islands located west and north of those regions Coastal regions of County Down (Northern Ireland) | 0,90 | 1 | 0 | 112 |
2 | 0 | 172 |
3 | 0 | 162 |
4a | 0 | 103 |
Mackerel of the species Scomber scombrus | Coastal regions and islands of Ireland | 0,96 | 1 | 0 | 223 |
2 | 0 | 220 |
3 | 0 | 214 |
Coastal regions and islands of Cornwall and Devon in the United Kingdom | 0,95 | 1 | 0 | 221 |
2 | 0 | 218 |
3 | 0 | 212 |
Hake of the species Merluccius merluccius | Coastal regions from Troon (in South-western Scotland) to Wick (in north-eastern Scotland) and the Islands located west and north of those regions | 0,75 | 1 | 2481 | 1957 |
2 | 1874 | 1461 |
3 | 1874 | 1433 |
4 | 1544 | 1185 |
5 | 1433 | 1130 |
Albacore or Long-finned tuna Thunnus alalunga | Islands of the Azores and Madeira | 0,48 | 1 | 1070 | 863 |
2 | 1070 | 821 |
Sardines of the species Sardina pilchardus | Canary Islands | 0,48 | 1 | 0 | 140 |
2 | 0 | 176 |
3 | 0 | 198 |
4 | 0 | 129 |
Coastal regions and islands of Cornwall and Devon in the United Kingdom | 0,74 | 1 | 0 | 216 |
2 | 0 | 271 |
3 | 0 | 305 |
4 | 0 | 199 |
Atlantic coastal regions of Portugal | 0,93 | 2 | 0 | 340 |
0,81 | 3 | 0 | 334 |
--------------------------------------------------
