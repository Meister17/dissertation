P6_TA(2004)0091
Adaptation of Directive 77/388/EEC on VAT following enlargement *
European Parliament legislative resolution on the proposal for a Council directive amending Directive 77/388/EEC by reason of the accession of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia (8754/2004 — COM(2004)0295 — C6-0074/2004 — 2004/0810(CNS))
(Consultation procedure)
The European Parliament,
- having regard to the Commission proposal to the Council (8754/2004 — COM(2004)0295) [1],
- having regard to Article 93 of the EC Treaty, pursuant to which the Council consulted Parliament (C6-0074/2004),
- having regard to Rule 51 of its Rules of Procedure,
- having regard to the report of the Committee on Economic and Monetary Affairs (A6-0058/2004),
A. reaffirming its support for the experimental scheme of reduced rates of value added tax for labour-intensive services, as expressed in its positions of 20 November 2002 [2], of 4 December 2003 [3] and of 15 January 2004 [4],
1. Approves the Commission proposal;
2. Calls on the Council to notify Parliament if it intends to depart from the text approved by Parliament;
3. Asks the Council to consult Parliament again if it intends to amend the Commission proposal substantially;
4. Instructs its President to forward its position to the Council and Commission.
[1] Not yet published in OJ.
[2] OJ C 25 E, 29.1.2004, p. 177.
[3] OJ C 89 E, 14.4.2004, p. 138.
[4] OJ C 92 E, 16.4.2004, p. 382.
--------------------------------------------------
