Judgment of the Court
(Fifth Chamber)
of 6 October 2005
in Case C-429/04: Commission of the European Communities v Kingdom of Belgium [1]
In Case C-429/04: action for failure to fulfil obligations under Article 226 EC brought on the 6 October 2004, Commission of the European Communities (Agents: K. Simonsson and W. Wils)
v Kingdom of Belgium (Agents: D. Haven and M. Wimmer), the Court (Fifth Chamber), composed of R. Silva de Lapuerta, President of the Chamber, C Gulmann and G. Arestis (Rapporteur), Judges, Advocate General: C. Stix-Hackl, Registrar: R. Grass, has given a judgment on the 6 October 2005 in which it:
1. Declares that, by failing to adopt the laws, regulations and administrative measures necessary to comply with Directive 2001/96/EC of the European Parliament and of the Council of 4 December 2001 establishing harmonised requirements and procedures for the safe loading and unloading of bulk carriers, the Kingdom of Belgium has failed to fulfil its obligations under this directive.
2. Orders the Kingdom of Belgium to pay the costs.
[1] OJ C 284, 20.11.2004
--------------------------------------------------
