Commission Regulation (EC) No 2273/2002
of 19 December 2002
laying down detailed rules for the application of Council Regulation (EC) No 1254/1999 as regards the survey of prices of certain bovine animals on representative Community markets
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1254/1999 of 17 May 1999 on the common organisation of the market in beef and veal(1), as last amended by Commission Regulation (EC) No 2345/2001(2), and in particular Article 41 thereof,
Whereas:
(1) Commission Regulation (EC) No 2705/98 of 14 December 1998 on the determination of prices of adult bovine animals on representative Community markets and the survey of prices of certain other cattle in the Community(3), as amended by Regulation (EC) No 1156/2000(4), lays down provisions on the recording of prices on the representative market or markets of each Member State for the various categories of bovine animals in order to determine the movement of prices on the market. In view of recent market developments, and in particular the fact that fewer live adult bovine animals are marketed in the Member States and that Member States therefore no longer have representative markets for these animals, the communication to the Commission of prices of adult bovine animals is no longer considered necessary in order to monitor developments in the beef market. Nevertheless, Member States may communicate such prices recorded on their representative markets.
(2) To monitor the Community market for categories of bovine animals other than adult bovines, provision should be made for a survey of prices for male calves between eight days and four weeks, for male store cattle and for slaughter calves. Detailed rules on the information to be provided for the price survey for each of these bovine animal categories should be established.
(3) The price recorded on the representative markets of the Community may be taken as the average of the prices of the bovine animals in question on the representative market or markets of each Member State. This average should be weighted in accordance with the coefficients expressing the relative size of the cattle population of each Member State for each category marketed in a reference period.
(4) The representative market or markets for each Member State should be selected on the basis of experience in recent years. Where there is more than one representative market in a Member State account should be taken of the arithmetic average of the quotations recorded on these markets.
(5) Member States may, because of veterinary or health protection regulations, find it necessary to take measures which affect quotations. In such circumstances, it is not always suitable when recording market prices to take into account quotations which do not reflect the normal trend of the market. Consequently, certain criteria should be laid down allowing the Commission to take account of that situation.
(6) Provision should be made for the communication to the Commission of the weekly prices, through electronic means of transmission to be agreed upon by the Commission.
(7) Regulation (EC) No 2705/98 should accordingly be repealed.
(8) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Beef and Veal,
HAS ADOPTED THIS REGULATION:
Article 1
1. The average Community price, per head, of male calves between eight days and four weeks old shall be the average of the prices of those bovine animals, weighted by the coefficients laid down in Annex I, Part A, recorded on the main markets of the Member States representative of this type of production. The coefficients are established using the number of dairy cows recorded in the Community.
2. The prices of the bovine animals referred to in paragraph 1 recorded on the representative market or markets of each Member State concerned shall be the average, weighted by coefficients reflecting the relative proportion of each breed or quality, of the prices recorded exclusive of VAT for these animals during a seven-day period in that Member State at the same wholesale stage. The weighting coefficients are laid down in Annex I, Parts B to H.
3. The Member States concerned shall communicate to the Commission not later than midday on Thursday each week the quotations for the bovine animals referred to in paragraph 1 recorded on their respective markets during the seven-day period preceding the day on which the information is communicated.
Article 2
1. The average Community price, per kilogram of live weight, of male store cattle aged on average between 6 and 12 months and weighing 300 kilograms or less, shall be the average of the prices of the bovine animals referred to above, weighted by the coefficients laid down in Annex II, Part A, recorded on the main markets of the Member States representative of this type of production. The coefficients are established using the number of suckler cows recorded in the Community.
2. The prices of the bovine animals referred to in paragraph 1 recorded on the representative market or markets of each Member State concerned shall be the average, weighted by coefficients reflecting the relative proportion of each breed or quality, of the prices recorded exclusive of VAT for these animals during a seven-day period in that Member State at the same wholesale stage. The weighting coefficients are laid down in Annex II, Parts B to F.
3. The Member States concerned shall communicate to the Commission not later than midday on Thursday each week the quotations for the bovine animals referred to in paragraph 1 recorded on their respective markets during the seven-day period preceding the day on which the information is communicated.
Article 3
1. The average Community price, per 100 kilograms of carcase weight, of slaughter calves obtained principally using milk or milk preparations and slaughtered at around six months of age, shall be the average of the prices of the bovine animals referred to above, weighted by the coefficients laid down in Annex III, Part A, recorded on the main markets of the Member States representative of this type of production. The coefficients are established using the net production (slaughterings) of calves in the Community.
2. The prices of the bovine animals referred to in paragraph 1 recorded in the quotation centre or centres of the Member States concerned shall be the average, where appropriate weighted by coefficients reflecting the relative proportion of each quality, of the prices recorded exclusive of VAT for these animals during a seven-day period at the slaughterhouse entry stage. The weighting coefficients are laid down in Annex III, Parts B to E.
3. The Member States concerned shall communicate to the Commission not later than midday on Thursday each week the quotations for the carcases of the bovine animals referred to in paragraph 1, recorded in their respective quotation centres during the seven-day period preceding the day on which the information is communicated.
Article 4
Where a Member State takes measures for veterinary or health protection reasons which affect the normal movement in quotations recorded on its markets, the Commission may authorise it either to disregard the quotations recorded on the market or markets in question, or to use the last quotations recorded on the market or markets in question before those measures were put into effect.
Article 5
Member States shall use electronic means of transmission, to be agreed upon by the Commission, for the communications referred to in Article 1(3), Article 2(3) and Article 3(3) by 30 June 2003 at the latest.
Article 6
Where Member States have representative markets for adult bovine animals on their territory, they may communicate the price of adult bovine animals as follows:
- the price of adult bovine animals on the representative market or markets shall be the average, weighted by coefficients reflecting the relative size of each category and quality, of the prices recorded for the categories and qualities of adult bovine animals and the meat thereof during a seven-day period prior to the day of the communication in that Member State at the same wholesale stage,
- in Member States with several representative markets, the price of each category shall be the arithmetic mean of the quotations recorded on each of these markets. Where markets are held more than once during the seven-day period referred to in the first indent above, the price of each category shall be the arithmetic mean of the quotations recorded each market day on the same physical market. If, in the course of any given week, the price is not recorded on a particular market for a particular category, the price for that category in the Member State shall be the arithmetical mean of the other markets.
Article 7
Regulation (EC) No 2705/98 shall be repealed from 1 January 2003.
Article 8
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Communities.
It shall apply to prices recorded from the week beginning 1 January 2003.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 19 December 2002.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 160, 26.6.1999, p. 21.
(2) OJ L 315, 1.12.2001, p. 29.
(3) OJ L 340, 16.12.1998, p. 3.
(4) OJ L 130, 31.5.2000, p. 23.
ANNEX I
Survey of prices of male calves between eight days and four weeks old
A. WEIGHTING COEFFICIENTS
>TABLE>
B. GERMANY
1. Representative markets
In the absence of public markets, prices are recorded by the official departments of the chambers of agriculture, and farmers' cooperatives and unions.
2. Qualities and coefficients
>TABLE>
C. SPAIN
1. Representative markets
Torrelavega (Cantabria), Santiago de Compostela (Galicia), Pola de Siero (Asturias), León (Castilla y León)
2. Qualities and coefficients
>TABLE>
D. FRANCE
1. Representative markets
Rethel, Dijon, Rabastens, Lezay, Lyon, Agen, Le Cateau, Sancoins, Château-Gonthier, Saint Étienne
2. Qualities and coefficients
>TABLE>
E. IRELAND
1. Representative markets
Bandon, Blessington
2. Qualities and coefficients
>TABLE>
F. ITALY
1. Representative markets
(a) Modena, Vicenza
(b) Prices recorded on import markets
2. Qualities and coefficients
>TABLE>
G. NETHERLANDS
1. Representative markets
Leeuwarden, Purmerend, Utrecht
2. Qualities and coefficients
>TABLE>
H. UNITED KINGDOM
1. Representative markets
Approximately 35 markets (England and Wales)
2. Qualities and coefficients
>TABLE>
ANNEX II
Survey of prices of light store cattle 6 to 12 months old with a live weight of 300 kg or less
A. WEIGHTING COEFFICIENTS
>TABLE>
B. SPAIN
1. Representative markets
Salamanca (Castilla y León)
Talavera (Castilla-La Mancha)
2. Qualities and coefficients
>TABLE>
C. FRANCE
1. Representative markets
Limoges, Clermont-Ferrand, Dijon
2. Qualities and coefficients
>TABLE>
D. IRELAND
1. Representative markets
Bandon, Blessington, Kilkenny
2. Qualities and coefficients
>TABLE>
E. ITALY
1. Representative markets
(a) Modena, Parma, Montichiari
(b) Prices recorded on import markets
2. Qualities and coefficients
>TABLE>
F. UNITED KINGDOM
1. Representative markets
Approximately 35 markets (England and Wales)
2. Qualities and coefficients
>TABLE>
Annex III
Survey of prices of veal calves slaughtered when about six months old
A. WEIGHTING COEFFICIENTS
>TABLE>
B. BELGIUM
1. Quotation centres (slaughterhouses)
Provinces of Antwerp and Limburg
2. Qualities
Veaux blancs, conformation classes E, U and R
C. FRANCE
1. Quotation centres
Commissions paritaires des régions Sud-Ouest, Centre, Centre-Est/Est, Nord/Nord-Ouest, Ouest
2. Qualities
Veaux blancs, all conformation classes E, U, R and O
D. ITALY
1. Quotation centres (slaughterhouses)
Modena
2. Qualities
Carne bianca
E. NETHERLANDS
1. Quotation centres (slaughterhouses)
Apeldoorn, Nieuwerkerk a/d Ijssel, Den Bosch, Aalten, Leeuwarden
2. Qualities and coefficients
>TABLE>
