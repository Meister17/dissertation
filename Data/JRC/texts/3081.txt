Uniform application of the Combined Nomenclature (CN)
(Classification of goods)
(2005/C 129/06)
Explanatory notes adopted in accordance with the procedure defined in Article 10(1) of Council Regulation (EEC) No 2658/87 of 23 July 1987 on the tariff and statistical nomenclature and on the Common Customs Tariff [1], as last amended by Council Regulation (EC) No 493/2005 [2]
The "explanatory notes to the Combined Nomenclature of the European Communities" [3] shall be amended as follows:
On page 318 the following texts are to be inserted:
"8514 Industrial or laboratory electric furnaces and ovens (including those functioning by induction or dielectric loss); other industrial or laboratory equipment for the heat treatment of materials by induction or dielectric loss
85142080 Dielectric furnaces and ovens
Microwave ovens of a type designed to be used in restaurants, canteens, etc., differ from domestic appliances of heading 8516 in their power output and oven capacity. Such ovens having a power output of more than 1000 W or an oven capacity of more than 34 litres are considered to be for industrial use. For microwave ovens combined in a single housing with a grill or another type of oven, the aforementioned power output applies only to the microwave. The classification of such a combination is not influenced by the oven capacity criterion.
Microwave ovens having a power output of not more than 1000 W and an oven capacity of not more than 34 litres are considered to be for domestic use (heading 8516)."
and
"85165000 Microwave ovens
See the Explanatory Notes to subheading 85142080."
On page 333 the following point 4 is to be inserted:
85489090 Other
"4. Elements of ferrite or other ceramics (for example, those used in circulators for ultra high frequency transmission apparatus or as a high frequency filter in electrical cables), which are electrical parts, equally suitable for use in machines or apparatus falling within different headings of this chapter."
[1] OJ L 256, 7.9.1987, p. 1.
[2] OJ L 82, 31.3.2005, p. 1.
[3] OJ C 256, 23.10.2002, p. 1.
--------------------------------------------------
