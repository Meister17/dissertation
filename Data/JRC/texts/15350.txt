Judgment of the Court (Grand Chamber) of 12 September 2006 (reference for a preliminary ruling from the Østre Landsret (Denmark)) — Laserdisken ApS v Kulturministeriet
(Case C-479/04) [1]
Referring court
Østre Landsret (Denmark)
Parties to the main proceedings
Applicant: Laserdisken ApS
Defendant: Kulturministeriet
Re:
Reference for a preliminary ruling — Østre Landsret — Validity and interpretation of Article 4(2) of Directive 2001/29/EC of the European Parliament and of the Council of 22 May 2001 on the harmonisation of certain aspects of copyright and related rights in the information society (OJ 2001 L 167, p. 10) — Exhaustion of the rightholder's rights only as of the first sale or first other transfer of ownership in the Community by the rightholder or with his consent — Imports from third countries of DVDs of cinematographic works.
Operative part of the judgment
1. Consideration of the first question does not reveal any information such as to affect the validity of Article 4(2) of Directive 2001/29/EC of the European Parliament and of the Council of 22 May 2001 on the harmonisation of certain aspects of copyright and related rights in the information society.
2. Article 4(2) of Directive 2001/29 is to be interpreted as precluding national rules providing for exhaustion of the distribution right in respect of the original or copies of a work placed on the market outside the European Community by the rightholder or with his consent.
[1] OJ C 31, 05.02.2005.
--------------------------------------------------
