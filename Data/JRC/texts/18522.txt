REGULATION (EEC) No 1709/74 OF THE COMMISSION of 2 July 1974 on the classification of goods under subheading 20.06 B I of the Common Customs Tariff
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community;
Having regard to Council Regulation (EEC) No 97/69 (1) of 16 January 1969 on measures to be taken for uniform application of the nomenclature of the Common Customs Tariff, as last amended by the Act (2) annexed to the Treaty on the accession of new Member States to the European Economic Community and the European Atomic Energy Community, signed at Brussels on 22 January 1972, and in particular Article 3 thereof;
Whereas provision must be made to ensure uniform application of the nomenclature of the Common Customs Tariff as regards classification of cherries which are provisionally preserved in a mixture of water and ethyl alcohol for use, inter alia, in the manufacture of chocolate products;
Whereas heading No 08.11 of the Common Customs Tariff annexed to Council Regulation (EEC) No 950/68 (3) of 28 June 1968, as last amended by Council Regulation (EEC) No 1615/74 (4) of 25 June 1974, covers "fruit provisionally preserved (for example, by sulphur dioxide gas, in brine, in sulphur water or in other preservative solutions), but unsuitable in that state for immediate consumption";
Whereas the "Explanatory Notes" to the Brussels Nomenclature make it clear that the abovementioned heading No 08.11 applies to fruit which has been treated solely to ensure its provisional preservation during transport or storage prior- to use, provided that it remains unsuitable for immediate consumption in that state ; whereas, consequently, this heading excludes fruit which has been treated in a way which does not make it unsuitable for immediate consumption;
Whereas cherries put up in a mixture of water and ethyl alcohol, with an alcoholic strength sufficient for preservation of the fruit for a limited period, are not thereby rendered unsuitable for immediate consumption ; whereas cherries thus treated cannot fall within heading No 08.11;
Whereas, on the other hand, subheading 2006 B I of the Common Customs Tariff applies to fruit otherwise prepared or preserved, containing added spirit, suitable for immediate consumption;
Whereas it follows that the cherries referred to above must fall within subheading 20.06 B I of the Common Customs Tariff;
Whereas the measures provided for in this Regulation are in accordance with the Opinion of the Committee on Common Customs Tariff Nomenclature,
HAS ADOPTED THIS REGULATION:
Article 1
Cherries put up in a mixture of water and ethyl alcohol shall be classified as fruit suitable for immediate consumption in the following subheading of the Common Customs Tariff:
20.06 Fruit otherwise prepared or preserved, whether or not containing added sugar or spirit:
B. Other:
I. Containing added spirit
Article 2
This Regulation shall enter into force on the twentyfirst day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 2 July 1974.
For the Commission
The President
François-Xavier ORTOLI (1)OJ No L 14, 21.1.1969, p. 1. (2)OJ No L 73, 27.3.1972, p. 14. (3)OJ No L 172, 22.7.1968, p. 1. (4)OJ No L 174, 28.6.1974, p. 4.
