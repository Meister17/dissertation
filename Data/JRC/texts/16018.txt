Publication of decisions by Member States to grant or revoke operating licenses pursuant to Article 13(4) of Council Regulation (EEC) No 2407/92 on licensing of air carriers [1] [2]
(2006/C 68/05)
(Text with EEA relevance)
AUSTRIA
Operating licences revoked
Category B: Operating licences including the restriction of Article 5(7)(a) of Regulation (EEC) No 2407/92
Name of air carrier | Address of air carrier | Permitted to carry | Decision effective since |
Zenith Airways GmbH | Promenadenweg 8 A-2522 Oberwaltersdorf | passengers, mail, cargo | 16.2.2006 |
[1] OJ L 240, 24.8.1992, p. 1.
[2] Communicated to the European Commission before 31.8.2005.
--------------------------------------------------
