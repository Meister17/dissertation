Removal from the register of Case C-163/05 [1]
(2006/C 60/72)
(Language of the case: Portuguese)
By order of 10 November 2005, the President of the Court of Justice of the European Communities has ordered the removal from the register of Case C-163/05: Commission of the European Communities
v Portuguese Republic.
[1] OJ C 132 of 28.05.2005.
--------------------------------------------------
