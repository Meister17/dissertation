[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 24.10.2006
COM(2006) 613 final
2006/0201 (ACC)
Proposal for a
COUNCIL DECISION
on the conclusion of a Protocol to the Partnership and Cooperation Agreement between the European Communities and their Member States, of the one part, and the Republic of Azerbaijan, of the other part, extending the provisions of the Partnership and Cooperation Agreement to bilateral trade in textiles, taking account of the expiry of the bilateral textiles agreement
(presented by the Commission)
EXPLANATORY MEMORANDUM
The Partnership and Cooperation Agreement between the European Communities and their Member States, of the one part, and the Republic of Azerbaijan, of the other part, (hereinafter referred to as “the PCA”) entered into force on 1 July 1999.
This agreement contains a provision according to which its trade provisions – namely, most-favoured nation (MFN) trade conditions and abolition of quantitative restrictions – do not apply to trade in textile products, which was regulated by a separate bilateral agreement.
This bilateral agreement, in force since 1 October 1993, lapsed on 31 December 2004. On 13 March 2006 the Council authorised the Commission to enter into negotiations with the Republic of Azerbaijan to amend the PCA, so as to ensure that the principles which apply to trade in other goods are also extended formally to trade in textile products.
These negotiations have been successfully concluded and appropriate amendments to the PCA in the form of a Protocol amending the PCA should be adopted by the Council. To that effect, the Commission submits to the Council a proposal for the conclusion of the Protocol.
2006/0201 (ACC)
Proposal for a
COUNCIL DECISION
on the conclusion of a Protocol to the Partnership and Cooperation Agreement between the European Communities and their Member States, of the one part, and the Republic of Azerbaijan, of the other part, extending the provisions of the Partnership and Cooperation Agreement to bilateral trade in textiles, taking account of the expiry of the bilateral textiles agreement
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 133, in conjunction with the first sentence of the first subparagraph of Article 300(2) thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) On 13 March 2006 the Council authorised the Commission to enter into negotiations with the Republic of Azerbaijan to amend the Partnership and Cooperation Agreement between the European Communities and their Member States, of the one part, and the Republic of Azerbaijan, of the other part, (hereinafter referred to as “the PCA”), so as to ensure that the principles which apply to trade in other goods are also extended formally to trade in textile products.
(2) These negotiations have been successfully concluded and the Protocol amending the PCA should be concluded by the European Community.
HAS DECIDED AS FOLLOWS:
Article 1
1. The Protocol amending the Partnership and Cooperation Agreement between the European Communities and their Member States, of the one part, and the Republic of Azerbaijan, of the other part, (hereinafter referred to as “the PCA”), extending the provisions of the Partnership and Cooperation Agreement to bilateral trade in textiles, taking account of the expiry of the bilateral textiles agreement (hereinafter referred to as “the Protocol”), is hereby approved.
2. The text of the Protocol is attached to this Decision.
Article 2
The President of the Council is hereby authorised to designate the person(s) empowered to sign the Protocol in order to express the consent to be bound thereby.
Done at Brussels,
For the Council
The President
PROTOCOL
on the conclusion of a Protocol to the Partnership and Cooperation Agreement between the European Communities and their Member States, of the one part, and the Republic of Azerbaijan, of the other part, extending the provisions of the Partnership and Cooperation Agreement to bilateral trade in textiles, taking account of the expiry of the bilateral textiles agreement
THE EUROPEAN COMMUNITY, hereinafter referred to as “the Community”,
of the one part, and
THE REPUBLIC OF AZERBAIJAN,
of the other part,
Whereas:
1. The Partnership and Cooperation Agreement between the European Communities and their Member States, of the one part, and the Republic of Azerbaijan, of the other part, (hereinafter referred to as “the PCA”) entered into force on 1 July 1999.
2. Negotiations have taken place to ensure that the principles in the PCA which apply to trade in other goods are also extended formally to trade in textile products.
3. Appropriate amendments to the PCA should be adopted.
HAVE AGREED AS FOLLOWS:
Article 1
The PCA is hereby amended as follows:
(1) In Article 12 the reference to Article 17 is deleted.
(2) Article 17 is deleted.
Article 2
This Protocol shall form an integral part of the PCA.
Article 3
This Protocol shall enter into force on the first day of the month following the date of signature.
Article 4
This Protocol is drawn up in duplicate in each of the official languages of the contracting parties, each of these texts being equally authentic.
Done at Brussels,
For the European Community For the Republic of Azerbaijan
