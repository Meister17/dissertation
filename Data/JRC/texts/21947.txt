COUNCIL REGULATION (EEC) No 2112/78 of 25 July 1978 concerning the conclusion of the Customs Convention on the international transport of goods under cover of TIR carnets (TIR Convention) of 14 November 1975 at Geneva
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to the recommendation from the Commission,
Whereas the conclusion of the Customs Convention on the international transport of goods under cover of TIR carnets (TIR Convention) of 14 November 1975 at Geneva introduces new provisions into the system of international transport of goods by road vehicles ; whereas the nature of the provisions is such as to contribute to the harmonious development of international trade in accordance with the objectives of the European Economic Community;
Whereas it is therefore appropriate that the TIR Convention should be approved on behalf of the Community,
HAS ADOPTED THIS REGULATION:
Article 1
The Customs Convention on the international transport of goods under cover of TIR carnets (TIR Convention) of 14 November 1975 at Geneva is hereby approved on behalf of the European Economic Community.
The text of the Convention and the Annexes thereto is annexed to this Regulation.
Article 2
The President of the Council is hereby authorized to deposit the instrument of ratification on behalf of the Community in accordance with Article 52 (1) (b) of the Convention (1).
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 25 July 1978.
For the Council
The President
K. von DOHNANYI (1)The date of entry into force of the Convention will be published in the Official Journal of the European Communities by the General Secretariat of the Council.
