Commission Regulation (EC) No 902/2005
of 15 June 2005
laying down the reduction coefficient to be applied under the Community tariff quota for barley provided for by Regulation (EC) No 2305/2003
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1784/2003 of 29 September 2003 on the common organisation of the market in cereals [1],
Having regard to Commission Regulation (EC) No 2305/2003 of 29 December 2003 opening and providing for the administration of a Community tariff quota for barley from third countries [2], and in particular Article 3(3) thereof,
Whereas:
(1) Regulation (EC) No 2305/2003 opened an annual tariff quota for the import of 300000 t of barley falling within CN code 100300.
(2) The quantities applied for on 13 June 2005 in accordance with Article 3(1) of Regulation (EC) No 2305/2003 exceed the quantities available. The extent to which licences may be issued should therefore be determined and a reduction coefficient laid down to be applied to the quantities applied for,
HAS ADOPTED THIS REGULATION:
Article 1
Each application for import licences for the tariff quota for barley lodged and forwarded to the Commission on 13 June 2005 in accordance with Article 3(1) and (2) of Regulation (EC) No 2305/2003 shall be accepted at a rate of 0 % of the quantity applied for.
Article 2
This Regulation shall enter into force on 16 June 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 15 June 2005.
For the Commission
J. M. Silva Rodríguez
Director-General for Agriculture and Rural Development
[1] OJ L 270, 21.10.2003, p. 78.
[2] OJ L 342, 30.12.2003, p. 7.
--------------------------------------------------
