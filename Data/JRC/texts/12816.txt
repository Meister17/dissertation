Commission Regulation (EC) No 899/2006
of 19 June 2006
amending Regulation (EC) No 2133/2001 opening and providing for the administration of certain Community tariff quotas and tariff ceilings in the cereals sector as regards the opening of a Community tariff quota for certain dog or cat food falling within CN code 230910
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1784/2003 of 29 September 2003 on the common organisation of the market in cereals [1], and in particular Article 12(1) thereof,
Having regard to Council Decision 2006/333/EC of 20 March 2006 on the conclusion of an Agreement in the form of an Exchange of Letters between the European Community and the United States of America pursuant to Article XXIV:6 and Article XXVIII of the General Agreement on Tariffs and Trade (GATT) 1994 relating to the modification of concessions in the schedules of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic in the course of their accession to the European Union [2], and in particular Article 2 thereof,
Whereas:
(1) The agreement between the Community and the United States of America [3] approved by Decision 2006/333/EC provides, for each calendar year from 2006 onwards, for a tariff quota for the import of certain dog or cat food falling within CN code 230910 at a customs duty of 7 % ad valorem.
(2) Commission Regulation (EEC) No 2454/93 of 2 July 1993 laying down provisions for the implementation of Council Regulation (EEC) No 2913/92 establishing the Community Customs Code [4] brings together the management rules applicable to tariff quotas to be used in the chronological order of the dates of the customs declarations and the monitoring of imports under a preferential regime. To harmonise the management of this new tariff quota with similar quotas, its management should be incorporated into the relevant provision.
(3) Commission Regulation (EC) No 2133/2001 [5] should therefore be amended accordingly.
(4) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Cereals,
HAS ADOPTED THIS REGULATION:
Article 1
The following line is hereby added to Annex II to Regulation (EC) No 2133/2001:
Order No | CN code | Description | Quota volume in net weight (tonnes) | Tariff quota duty | Origin |
"09.0089 | 23091013 23091015 23091019 23091033 23091039 23091051 23091053 23091059 23091070 | Dog or cat food, put up for retail sale | 2058 | 7 % ad valorem | All third countries (erga omnes)" |
Article 2
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 19 June 2006.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 270, 21.10.2003, p. 78. Regulation as amended by Commission Regulation (EC) No 1154/2005 (OJ L 187, 19.7.2005, p. 11).
[2] OJ L 124, 11.5.2006, p. 13.
[3] OJ L 124, 11.5.2006, p. 15.
[4] OJ L 253, 11.10.1993, p. 1. Regulation as last amended by Regulation (EC) No 402/2006 (OJ L 70, 9.3.2006, p. 35).
[5] OJ L 287, 31.10.2001, p. 12. Regulation as amended by Regulation (EC) No 777/2004 (OJ L 123, 27.4.2004, p. 50).
--------------------------------------------------
