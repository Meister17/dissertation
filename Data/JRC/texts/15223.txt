Commission Regulation (EC) No 1474/2006
of 5 October 2006
amending Regulations (EC) No 2771/1999 and (EC) No 1898/2005 as regards the entry into storage of intervention butter put on sale
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1255/1999 of 17 May 1999 on the common organisation of the market in milk and milk products [1], and in particular Article 10 thereof,
Whereas:
(1) Article 21 of Commission Regulation (EC) No 2771/1999 of 16 December 1999 laying down detailed rules for the application of Council Regulation (EC) No 1255/1999 as regards intervention on the market in butter and cream [2] lays down that intervention butter placed on sale must have entered into storage before 1 January 2005.
(2) Article 1(a) of Commission Regulation (EC) No 1898/2005 of 9 November 2005 laying down detailed rules for implementing Council Regulation (EC) No 1255/1999 as regards measures for the disposal of cream, butter and concentrated butter on the Community market [3] lays down that intervention butter bought in under Article 6(2) of Regulation (EC) No 1255/1999 to be sold at reduced prices must have been taken into storage before 1 January 2005.
(3) Given the situation on the butter market and the quantities of butter in intervention storage it is appropriate that butter in storage before 1 January 2006 should be available for sale.
(4) Regulations (EC) No 2771/1999 and (EC) No 1898/2005 should therefore be amended accordingly.
(5) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Milk and Milk Products,
HAS ADOPTED THIS REGULATION:
Article 1
In Article 21 of Regulation (EC) No 2771/1999, the date " 1 January 2005" is replaced by the date " 1 January 2006".
Article 2
In Article 1(a) of Regulation (EC) No 1898/2005, the date " 1 January 2005" is replaced by the date " 1 January 2006".
Article 3
This Regulation shall enter into force on the day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 5 October 2006.
For the Commission
Jean-Luc Demarty
Director-General for Agriculture and Rural Development
[1] OJ L 160, 26.6.1999, p. 48. Regulation as last amended by Regulation (EC) No 1913/2005 (OJ L 307, 25.11.2005, p. 2).
[2] OJ L 333, 24.12.1999, p. 11. Regulation as last amended by Regulation (EC) No 1226/2006 (OJ L 222, 15.8.2006, p. 3).
[3] OJ L 308, 25.11.2005, p. 1. Regulation as amended by Regulation (EC) No 1417/2006 (OJ L 267, 27.9.2006, p. 34).
--------------------------------------------------
