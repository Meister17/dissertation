Information communicated by Member States regarding State aid granted under Commission Regulation (EC) No 70/2001 of 12 January 2001, as amended by Commission Regulation (EC) No 364/2004 of 25 February 2004, on the application of Articles 87 and 88 of the EC Treaty to State aid to small and medium-sized enterprises
(2006/C 58/02)
(text with EEA relevance)
Aid No | XS 78/04 |
Member State | Netherlands |
Region | Gelderland |
Title of aid scheme or name of company receiving individual aid | Machinefabriek Kersten Beheer B.V. |
Legal basis | Gemeente Brummen, raadsbesluit d.d. 27 maart 2003 |
Annual expenditure planned or overall amount of aid granted to the company | Aid scheme | Annual overall amount | |
Loans guaranteed | |
Individual aid | Overall aid amount | EUR 0,29 million |
Loans guaranteed | |
Maximum aid intensity | In conformity with Articles 4(2)-(6) and 5 of the Regulation | Yes | |
Date of implementation | 10.8.2004 |
Duration of scheme or individual aid award | Until 1.7.2005 |
Objective of aid | Aid to SMEs | Yes | |
Economic sectors concerned | Limited to specific sectors | |
Steel | Yes |
Name and address of the granting authority | Name: Gemeente Brummen | |
Address: Postbus 5 6970 AA Brummen Netherlands | |
Large individual aid grants | In conformity with Article 6 of the Regulation | Yes |
Aid No | XS 4/05 |
Member State | Slovak Republic |
Region | Bratislava autonomous region. Within the region, only the territory of three districts: (Malacky, Pezinok and Senec) and the territory of five boroughs of the Slovak capital, Bratislava: (Vajnory, Záhorská Bystrica, Jarovce, Rusovce and Čunovo). According to the regional state aid map for the Slovak Republic, the assisted area falls under Article 87(3)(c) of the EC Treaty. |
Title of aid scheme or name of company receiving individual aid | Aid scheme for SMEs under SPD 2 (state aid scheme) — Appendix 1 |
Legal basis | Zákon NR SR č. 503/2001 Z. z. o podpore regionálneho rozvoja Jednotný programový dokument NUTS II Bratislava cieľ 2 na obdobie rokov 2004 – 2006 Nariadenie Komisie (ES) č. 70/2001 z 12. januára 2001 o použití článkov 87 a 88 Zmluvy o založení ES na štátnu pomoc malým a stredným podnikom, publikované v Úradnom vestníku Európskeho spoločenstva (Ú. v. ES L 10 z 13. januára 2001) v znení nariadenia Komisie (ES) č. 364/2004 z 25. februára 2004 |
Annual expenditure planned or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount 2004 Note: the amount not utilised in 2004 is included in the amount for 2005 | |
Annual overall amount 2005 | EUR 8, 75 million |
Annual overall amount 2006 | EUR 4,522 million |
Total for 2004 — 2006 | EUR 13,272 million |
Loans guaranteed | |
Individual aid | Overall aid amount | |
Loans guaranteed | |
Maximum aid intensity | In conformity with Articles 4(2)-(6) and 5 of the Regulation | Yes For investment projects: 30 % gross (which represents 20 % net + 10 percentage points gross as a bonus for SMEs) of total eligible expenditure; For first-time participation in a trade fair, exhibition or trade mission and for advisory and consulting services received: 50 % of total eligible expenditure. |
Date of implementation | 27.5.2005 |
Duration of scheme or individual aid award | Until 31.12.2006 |
Objective of aid | Aid to SMEs | Yes | |
Economic sectors concerned | All sectors eligible for aid to SMEs, except for ore-mining, coal-mining, steel manufacture, shipbuilding, manufacture of synthetic fibres, healthcare and transport, and the scheme is inapplicable in accordance with Article 1(2)(a), (b) and (c) of Commission Regulation (EC) No 70/2001 of 12 January 2001. | Yes |
Limited to specific sectors | Yes |
—All manufacturing | Yes |
Other manufacturing | Yes |
Other services Advisory and consulting; First-time participation in a trade fair, exhibition or trade mission. | Yes |
Name and address of the granting authority | Name: The Ministry of Construction and Regional Development SR |
Address: The Ministry of Construction and Regional Development SR Prievozská 2/B SK-816 44 Bratislava |
Large individual aid grants | In conformity with Article 6 of the Regulation | Yes |
Aid No | XS 49/05 |
Member State | Cyprus |
Title of aid scheme or name of company receiving an individual aid | Programme for access to research infrastructure abroad |
Legal basis | Απόφαση του Διοικητικού Συμβουλίου του Ιδρύματος Προώθησης Έρευνας με ημερομηνία 2 Σεπτεμβρίου 2004 |
Annual expenditure planned or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount | EUR 0,46 million |
Loans guaranteed | |
Individual aid | Overall aid amount | |
Loans guaranteed | |
Maximum aid intensity | In conformity with Articles 4(2)-(6) and 5 of the Regulation | Yes 75 % for basic research,up to 75 % for industrial research andup to 50 % for pre-competitive research. | |
Date of implementation | 24 February 2005 |
Duration of scheme or individual aid award | Until 31 October 2005 |
Objective of aid | Aid to SMEs | Yes | |
Economic sectors concerned | All sectors eligible for aid to SMEs | Yes |
Name and address of the granting authority | Name: Ίδρυμα Προώθησης Έρευνας |
Address: Απελλή και Νιρβάνα Άγιοι Ομολογητές, Λευκωσία Τ.Κ.2342, 1683 Κύπρος |
Large in ividual aid grants | In conformity with Article 6 of the Regulation | | maximum amount grantable is EUR 0,15 million |
Aid No | XS 59/05 |
Member State | Italy |
Region | Lazio |
Title of aid scheme or name of company receiving individual aid | Meccano Aeronautica SpA, Via Nettuno n. 288, Cisterna di Latina (LT), Italy. |
Legal basis | Art. 17 della Legge Finanziaria Regionale per l'esercizio 2002 del 16 aprile 2002 n. 8, pubblicata nel Bollettino Ufficiale Lazio del 20 aprile 2002, n. 11 S.O. n. 8 |
Annual expenditure planned or overall amount of individual aid granted to the company | The assistance given to Meccano Aeronautica SpA amounts to EUR 3450000. This figure consists of (a)EUR 3200000 in aid towards investment in tangible assets within the meaning of Article 4 of Regulation (EC) No 70/2001;(b)EUR 250000 in aid towards services provided by outside consultants within the meaning of Article 5 of Regulation (EC) No 70/2001.The assistance will be paid out on the basis of a six-monthly check on the costs actually borne by Meccano Aeronautica SpA for staff (a) and services (b). Full payment is to be made no later than 31 December 2009. |
Maximum aid intensity | The maximum intensity will be within the ceilings laid down in the aid map approved by the Commission for Italy for the period 2000-2006, which was published in OJ L 105, 20.4.2003, over and above the rates provided for in Article 4(3)(a) of Regulation (EC) No 70/2001. |
Date of implementation | The contract granting the aid is to be concluded on 15 March 2005. It provides that the aid is to be granted only in respect of eligible costs actually borne, and only provided regular checks establish that the recipient is meeting the other obligations it has undertaken, in particular that the aid will not be combined with aid under any other scheme, that the investment will be no less than 75% of the assistance (a), that the services (b) will not be of a routine nature, and that the increase in jobs will be maintained for at least five years. For the performance of the other obligations there are properly secured guarantees. |
Duration of scheme or individual aid award | Full payment is to be made no later than 31 December 2009. |
Objective of aid | The aid is intended primarily as aid to an SME. It also pursues the broader objective of reindustrialisation and reuse of the industrial plant of Goodyear Italia SpA in order to provide new jobs for the former Goodyear employees who have been dismissed or are in redeployment in a disadvantaged area covered by Article 87(3)(c) of the EC Treaty. |
Economic sectors concerned | Limited to specific sectors: manufacturing |
Name and address of the granting authority | Name: Regione Lazio |
Address: Via Cristoforo Colombo 212 I-00147 Rome |
--------------------------------------------------
