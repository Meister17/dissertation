COUNCIL DIRECTIVE 95/60/EC of 27 November 1995 on fiscal marking of gas oils and kerosene
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 99 thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas the Community measures envisaged by this Directive are not only necessary but also indispensable for the attainment of the objectives of the internal market; whereas these objectives cannot be achieved by Member States individually; whereas furthermore their attainment at Community level is already provided for in Directive 92/81/EEC (4), and in particular Article 9 thereof; whereas this Directive conforms with the principle of subsidiarity;
Whereas Directive 92/82/EEC (5) lays down provisions in respect of the minimum rates of excise duty applicable to certain mineral oils and in particular to the different categories of gas oil and kerosene;
Whereas the proper functioning of the internal market now requires that common rules be established for fiscal marking of gas oil and kerosene which have not borne duty at the full rate applicable to such mineral oils used as propellant;
Whereas certain Member States should be allowed to derogate from the measures laid down in this Directive because of special national circumstances;
Whereas Directive 92/12/EEC (6) lays down provisions on the general arrangements for products subject to excise duty and in particular Article 24 thereof provides for the establishment of an Excise Committee which may examine matters concerning the application of Community provisions on excise duties;
Whereas it is appropriate that certain technical matters relating to the specification of products to be used for fiscal marking of gas oil and kerosene be dealt with under the provisions of the said Article,
HAS ADOPTED THIS DIRECTIVE:
Article 1
1. Without prejudice to national provisions on fiscal marking, Member States shall apply a fiscal marker in accordance with the provisions of this Directive to:
- all gas oil falling within CN code 2710 00 69 which has been released for consumption within the meaning of Article 6 of Directive 92/12/EEC and has been exempt from, or subject to, excise duty at a rate other than that laid down in Article 5 (1) of Directive 92/82/EEC;
- kerosene falling within CN code 2710 00 55 which has been released for consumption within the meaning of Article 6 of Directive 92/12/EEC and has been exempt from, or subject to, excise duty at a rate other than that laid down in Article 8 (1) of Directive 92/82/EEC.
2. Member States may allow exceptions to the application of the fiscal marker provided for in paragraph 1 on grounds of public health or safety or for other technical reasons, provided they take appropriate fiscal supervision measures.
Moreover, Ireland may decide not to use or allow use of this marker in accordance with Article 21 (4) of Directive 92/12/EEC. In such a situation Ireland shall inform the Commission, which shall inform the other Member States.
Article 2
1. The marker shall consist of a well-defined combination of chemical additives to be added under fiscal supervision before the mineral oils concerned are released for consumption.
However,
- in the case of direct deliveries from another Member State under a tax suspension arrangements outside a tax warehouse, Member States may require the marker to be added before the product leaves the tax warehouse of despatch;
- Member States which adopted this measure before 1 January 1996 may, in certain exceptional cases or situations, allow markers to be added after the mineral oils in question are released for consumption under fiscal supervision. Any Member State which applies such a measure shall so inform the Commission. The Commission shall inform the other Member States of the measure. In the latter case, Member States may reimburse the excise duty paid when the product was released for consumption;
- Denmark may, provided that the goods remain subject to fiscal control, delay the addition of the marker until the moment of final retail sale, at the latest.
2. The marker to be used shall be established in accordance with the procedure laid down in Article 24 of Directive 92/12/EEC.
Article 3
Member States shall take the necessary steps to ensure that improper use of the marked products is avoided and, in particular, that the mineral oils in question cannot be used for combustion in the engine of a road-going motor vehicle or kept in its fuel tank unless such use is permitted in specific cases determined by the competent authorities of the Member States.
Member States shall provide that the use of the mineral oils in question in the cases mentioned in the first subparagraph is to be considered as an offence under the national law of the Member State concerned. Each Member State shall take the measures required to give full effect to all the provisions of this Directive and shall, in particular, determine the penalties to be imposed in the event of failure to comply with the said measures; such penalties shall be commensurate with their purpose and shall have adequate deterrent effect.
Article 4
Member States may add a national marker or colour in addition to the marker provided for in Article 1 (1).
No person shall add to the mineral oils concerned any marker or colour other than those provided for in Community law or in the national law of the Member State concerned.
Article 5
1. Member States shall bring into force the provisions necessary to comply with this Directive on the entry into force of the provisions adopted in accordance with the procedure provided for in Article 2. They shall forthwith inform the Commission thereof.
When Member States adopt these provisions, they shall contain a reference to this Directive or be accompanied by such reference on the occasion of their official publication. The methods of making such reference shall be laid down by the Member State.
2. Member States shall communicate to the Commission the text of the provisions of national law which they adopt in the field governed by this Directive.
Article 6
This Directive is addressed to the Member States.
Done at Brussels, 27 November 1995.
For the Council The President P. SOLBES MIRA
