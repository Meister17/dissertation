COMMISSION REGULATION (EC) No 2194/96 of 15 November 1996 amending Regulation (EEC) No 120/89 laying down common detailed rules for the application of export levies and charges on agricultural products
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 1766/92 of 30 June 1992 on the common organization of the market in cereals (1), as last amended by Commission Regulation (EC) No 923/96 (2), and in particular Articles 9 (2), 13 (11) and 16 (2) thereof, and the corresponding provisions of the other regulations on the common organization of the markets in agricultural products,
Whereas in certain circumstances an export levy or charge is applied to all export transactions and physical exits from the customs territory of the Community of agricultural products fulfilling the conditions laid down in Article 9 (2) and 10 (1) of the Treaty;
Whereas Article 30 (1) (b) (i) of Commission Regulation (EEC) No 3719/88 (3), as last amended by Regulation (EC) No 2137/95 (4), lays down the requirement that products for which an export licence has been presented must leave the customs territory of the Community within 60 days from the day of acceptance of the export declaration;
Whereas Article 32 (1) of Commission Regulation (EEC) No 3665/87 (5), as last amended by Regulation (EC) No 1384/95 (6), stipulates that within 60 days of the date on which goods cease to be subject to the arrangements provided for in Article 4 or 5 of Council Regulation (EEC) No 565/80 (7), as amended by Regulation (EC) No 2026/83 (8), those goods must leave the customs territory of the Community in their original state;
Whereas, where an export levy or charge is fixed after the date of acceptance of an export declaration for agricultural products, those products are not subject to payment of that levy or charge if they leave the customs territory of the Community within the 60-day time limit laid down in Article 30 (1) (b) (i) of Regulation (EEC) No 3719/88; whereas, likewise, where the agricultural products are subject to one of the arrangements referred to in Articles 4 and 5 of Regulation (EEC) No 565/80, the export levies and charges do not apply if those products are exported within the time limits determined in accordance with Article 32 (1) of Regulation (EEC) No 3665/87;
Whereas Article 211 of Council Regulation (EEC) No 2913/92 (9), as last amended by the Act of Accession of Austria, Finland and Sweden, stipulates that failure to comply with the conditions under which goods were allowed to leave the customs territory of the Community with total or partial relief from export duties incurs a customs debt on exportation; whereas that customs debt is incurred at the moment the goods or agricultural products leave the customs territory of the Community; whereas the declarant is the debtor;
Whereas Article 251 of Commission Regulation (EEC) No 2454/93 of 2 July 1993 laying down provisions for the implementation of Council Regulation (EEC) No 2913/92, establishing the Community Customs Code (10), as last amended by Regulation (EC) No 1676/96 (11), stipulates that export declarations are to be invalidated where the goods have been declared for export but have not left the customs territory of the Community within the time limit laid down; whereas, however, that provision cannot be applied when the export is carried out;
Whereas payment of a refund may arise from a monthly increase or a positive correction to a refund rate fixed at zero;
Whereas certain amendments should be made to Commission Regulation (EEC) No 120/89 (12), as amended by Regulation (EEC) No 1431/93 (13), in order to provide for the approach to be taken in view of the above factors;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the relevant management committees,
HAS ADOPTED THIS REGULATION:
Article 1
The following Article 4a is hereby added to Commission Regulation (EEC) No 120/89:
'Article 4a
1. Where Article 4 does not apply and where no refund is granted on the products, the declarant shall incur a debt within the meaning of Article 211 of Council Regulation (EEC) No 2913/92 (*) if the products leave the customs territory of the Community after the 60-day time limit laid down in Article 32 (1) of Regulation (EEC) No 3665/87 or Article 30 (1) (b) (i) of Regulation (EEC) No 3719/88 at the rate in force pursuant to the second subparagraph of Article 4 (1) above, but on the basis of the nature, characteristics and quantity of the exported products given in the export declaration as initially accepted.
For the purposes of this paragraph, the final subparagraph of Article 251 (2) (a) of Commission Regulation (EEC) No 2454/93 (**) shall not apply.
2. The customs debt referred to in paragraph 1 shall be deemed to be incurred at the place where the export declaration is accepted.
From the day on which an export levy applies to products as referred to in paragraph 1, the customs office of exit from the Community's customs territory shall inform the customs office where the export formalities were carried out of the date the products in question actually left the Community's customs territory by returning the control copy T5 or a photocopy of the control copy T5 or by sending a notification specially drawn up for the purpose.
The document sent to the customs office where the export formalities were carried out is completed by the customs office of exit to include the following statement:
Aplicación del artículo 4 bis del Reglamento (CEE) n° 120/89
Anvendelse af artikel 4a i forordning (EØF) nr. 120/89
Anwendung von Artikel 4a der Verordnung (EWG) Nr. 120/89
ÅöáñìïãÞ ôïõ Üñèñïõ 4á ôïõ êáíïíéóìïý (ÅÏÊ) áñéè. 120/89
Application of Article 4a of Regulation (EEC) No 120/89
Application de l'article 4 bis du règlement (CEE) n° 120/89
Applicazione dell'articolo 4 bis del regolamento (CEE) n. 120/89
Toepassing van artikel 4 bis van Verordening (EEG) nr. 120/89
Aplicação do artigo 4ºA do Regulamento (CEE) nº 120/89
Asetuksen (ETY) N:o 120/89 4 a artiklan soveltaminen
I enlighet med artikel 4a i förordning (EEG) nr 120/89.
3. If the customs office where the export formalities were carried out is not responsible for the charging of export levies it will inform the responsible national authority.
(*) OJ No L 302, 19. 10. 1992, p. 1.
(**) OJ No L 253, 11. 10. 1993, p. 1.`
Article 2
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 15 November 1996.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ No L 181, 1. 7. 1992, p. 21.
(2) OJ No L 126, 24. 5. 1996, p. 37.
(3) OJ No L 331, 2. 12. 1988, p. 1.
(4) OJ No L 214, 8. 9. 1995, p. 21.
(5) OJ No L 351, 14. 12. 1987, p. 1.
(6) OJ No L 134, 20. 6. 1995, p. 14.
(7) OJ No L 62, 4. 3. 1980, p. 5.
(8) OJ No L 199, 22. 7. 1983, p. 12.
(9) OJ No L 302, 19. 10. 1992, p. 1.
(10) OJ No L 253, 11. 10. 1993, p. 1.
(11) OJ No L 218, 28. 8. 1996, p. 1.
(12) OJ No L 16, 20. 1. 1989, p. 19.
(13) OJ No L 140, 11. 6. 1993, p. 27.
