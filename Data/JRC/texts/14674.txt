Information communicated by Member States regarding State aid granted under Commission Regulation (EC) No 70/2001 of 12 January 2001 on the application of Articles 87 and 88 of the EC Treaty to State aid to small and medium-sized enterprises
(2006/C 132/05)
(Text with EEA relevance)
Aid No | XS 114/05 |
Member State | Spain |
Region | Basque Country |
Title of aid scheme or name of company receiving individual aid | Aid for research to analyse and identify needs (diagnostics) |
Legal basis | Orden de 26 de abril de 2005, de la Consejera de Educación, Universidades e Investigación, por la que se convocan ayudas económicas para la realización de Estudios de Análisis y Detección de Necesidades (Diagnósticos) que se desarrollen por parte de las empresas de la Comunidad Autónoma del País Vasco en el ejercicio 2005 (Boletín Oficial del País Vasco de 29/04/2005) |
Annual expenditure planned or overall amount of aid granted to the company | Aid scheme | Annual overall amount | EUR 0,2 million |
Loans guaranteed | |
Individual aid | Overall aid amount | |
Loans guaranteed | |
Maximum aid intensity | In conformity with Articles 4(2)-(6) and 5 of the Regulation | Yes | |
Date of implementation | Since 29.4.2005 |
Duration of scheme or individual aid award | Until 10.5.2006 |
Objective of aid | Aid to SMEs | Yes | |
Economic sectors concerned | All sectors eligible for aid to SMEs | Yes |
Name and address of the granting authority | Name: Hobetuz Fundación Vasca para la Formación Profesional Continua |
Address: Gran Vía 35 — 6o E-48009 Bilbao |
Large individual aid grants | In conformity with Article 6 of the Regulation | Yes | |
Aid No | XS 119/04 |
Member State | Netherlands |
Region | Entire country |
Title of aid scheme or name of company receiving individual aid | Product Technology Application Centre |
Legal basis | Kaderwet EZ-subsidies |
Annual expenditure planned or overall amount of aid granted to the company | Aid scheme | Annual overall amount | EUR 1 million |
Loans guaranteed | |
Individual aid | Overall aid amount | |
Loans guaranteed | |
Maximum aid intensity | In conformity with Articles 4(2)-(6) and 5 of the Regulation | Yes | |
Date of implementation | 1.11.2004 |
Duration of scheme or individual aid award | Until 31.12.2009 |
Objective of aid | Aid to SMEs | Yes | |
Economic sectors concerned | All sectors eligible for aid to SMEs | Yes |
Limited to specific sectors | Yes |
—All manufacturing | |
Or | |
Steel | |
Shipbuilding | |
Synthetic fibres | |
Motor vehicles | |
Other manufacturing | |
Name and address of the granting authority | Name: Ministerie van Economische Zaken |
Address: Bezuidenhoutseweg 30 Postbus 20101 2500 EC Den Haag Nederland |
Large individual aid grants | In conformity with Article 6 of the Regulation | Yes | |
Aid No | XS 123/05 |
Member State | United Kingdom |
Region | Scotland (including the Highlands and Islands of Scotland) |
Title of aid scheme or name of company receiving individual aid | Scottish Enterprise Business Advisory Service Scheme for SMEs |
Legal basis | Enterprise and New Towns (Scotland) Act 1990 |
Annual expenditure planned under the scheme or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount | GBP 100 million |
Loans guaranteed | |
Individual aid | Overall aid amount | |
Loans guaranteed | |
Maximum aid intensity | In conformity with Articles 4(2)-(6) and 5 of the Regulation | Yes | |
Date of implementation | 1 June 2005 |
Duration of scheme or individual aid award | Until 31 December 2006 |
Objective of aid | Aid to SMEs | Yes | |
Economic sectors concerned | All sectors eligible for aid to SMEs | Yes |
Name and address of the granting authority | Name: Scottish Enterprise |
Address: 150 Broomielaw Atlantic Quay Glasgow G2 8LU United Kingdom |
Large individual aid grants | In conformity with Article 6 of the Regulation | Yes | |
Aid No | XS 128/05 |
Member State | United Kingdom |
Region | West Wales & The Valleys Objective 1 Region |
Title of aid scheme or name of company receiving individual aid | LAS Waste Ltd |
Legal basis | Council Regulation (EC) No 1260/99 The Structural Funds (National Assembly for Wales) Regulations 2000 (no/906/2000) The Structural Funds (National Assembly for Wales) Designation 2000 |
Annual expenditure planned under the scheme or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount | |
Loans guaranteed | |
Individual aid | Overall aid amount | GBP 780000 |
Loans guaranteed | |
Maximum aid intensity | In conformity with Articles 4(2)-(6) and 5 of the Regulation | Yes | |
Date of implementation | From 16.6.2005 |
Duration of scheme or individual aid award | Until 31.12.2006 NB As noted above, the grant was committed prior to 31 December 2006. Payments against this commitment will, potentially (in line with N+2), continue until 30 June 2008 |
Objective of aid | Aid to SMEs | Yes | |
Economic sectors concerned | Limited to specific sectors | Yes |
—Other services (Waste segregation and materials recycling) | Yes |
Name and address of the granting authority | Name: National Assembly for Wales |
Address: C/o Welsh European Funding Office Cwm Cynon Business Park Mountain Ash CF45 4ER United Kingdom |
Large individual aid grants | In conformity with Article 6 of the Regulation | Yes | |
Aid No | XS 134/05 |
Member State | Poland |
Region | Entire country |
Title of aid scheme or name of company receiving individual aid | Aid to consultancy services for companies |
Legal basis | Art. 31 ustawy z dnia 20 kwietnia 2004 r. o Narodowym Planie Rozwoju (Dz.U. nr 116, poz. 1206). Rozporządzenie Ministra Gospodarki i Pracy z dnia 17 grudnia 2004 r. w sprawie udzielania pomocy na wspieranie inwestycji i doradztwa w przedsiębiorstwach (Dz.U. z 2004 r., nr 267, poz. 2652). Rozporządzenie weszło w życie 17 grudnia 2004 r. |
Annual expenditure planned or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount | Forecast expenditure under the scheme is as follows: 2005:EUR 376630:EUR 282473 from the ERDF,EUR 94157 from the state budget.2006:EUR 376631:EUR 282473 from the ERDF,EUR 94158 from the state budget.Total expenditure until the end of the scheme ( 31 December 2006) is estimated at EUR 753261. The scheme will be implemented within the framework of: Measure 3.4 — Microenterprises |
Loans guaranteed | |
Individual aid | Overall aid amount | |
Loans guaranteed | |
Maximum aid intensity | In conformity with Articles 4(2)-(6) and 5 of the Regulation | Yes | |
Date of implementation | 17.12.2004 |
Duration of scheme or individual aid award | Up to 31.12.2006 |
Objective of aid | Aid to consultancy services for SMEs | Yes | |
Economic sectors concerned | All sectors eligible for aid to SMEs with the exception of companies operating in the synthetic fibres, shipping, shipbuilding, iron and steel or coalmining sectors | Yes |
Name and address of the granting authority | Name: Marshall of the Province/Authorised representative of the competent implementing body in the light of the project's location |
Address: 16 marshall's offices/ implementing bodies throughout Poland |
Large individual aid grants | In conformity with Article 6 of the Regulation | Yes | |
Aid No | XS 135/05 |
Member State | Poland |
Region | Entire country |
Title of aid scheme or name of company receiving individual aid | Investment aid to companies |
Legal basis | Art. 31 ustawy z dnia 20 kwietnia 2004 r. o Narodowym Planie Rozwoju (Dz.U. nr 116, poz. 1206). Rozporządzenie Ministra Gospodarki i Pracy z dnia 17 grudnia 2004 r. w sprawie udzielania pomocy na wspieranie inwestycji i doradztwa w przedsiębiorstwach (Dz.U. z 2004 r., nr 267, poz. 2652). Rozporządzenie weszło w życie 17 grudnia 2004 r. |
Annual expenditure planned or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount | Forecast expenditure under the scheme is as follows: 2005:EUR 37286475:EUR 27964856 from the ERDF,EUR 9321619 from the state budget.2006:EUR 37286475:EUR 27964856 from the ERDF,EUR 9321619 from the state budget.Total expenditure until the end of the scheme ( 31 December 2006) is estimated at EUR 74572950. The scheme will be implemented within the framework of: Measure 3.4 — Microenterprises |
Loans guaranteed | |
Individual aid | Overall aid amount | |
Loans guaranteed | |
Maximum aid intensity | In conformity with Articles 4(2)-(6) and 5 of the Regulation | Yes | |
Date of implementation | 17.12.2004 |
Duration of scheme or individual aid award | Up to 31.12.2006 |
Objective of aid | Investment aid to SMEs | Yes | |
Economic sectors concerned | All sectors eligible for aid to SMEs With the exception of companies operating in the synthetic fibres, shipping, shipbuilding, iron and steel or coalmining sectors | Yes |
Name and address of the granting authority | Name: Marshall of the Province/Authorised representative of the competent implementing body in the light of the project's location |
Address: 16 marshall's offices/ implementing bodies throughout Poland |
Large individual aid grants | In conformity with Article 6 of the Regulation | Yes | |
Aid No | XS 141/05 |
Member State | Belgium |
Region | Flanders |
Title of aid scheme or name of company receiving individual aid | Guarantee scheme for SMEs |
Legal basis | Het decreet van 6 februari 2004 betreffende een waarborgregeling voor kleine en middelgrote ondernemingen |
Annual expenditure planned or overall amount of aid granted to the company | Aid scheme | Annual overall amount | |
Loans guaranteed | EUR 150 million |
Individual aid | Overall aid amount | |
Loans guaranteed | |
Maximum aid intensity | In conformity with Articles 4(2)-(6) and 5 of the Regulation | Yes | |
Date of implementation | 1.7.2005 |
Duration of scheme or individual aid award | Until 31.12.2006 |
Objective of aid | Aid to SMEs | Yes | |
Economic sectors concerned | All sectors eligible for aid to SMEs | Yes |
Name and address of the granting authority | Name: Flemish Region Participatiemaatschappij Vlaanderen N
V Waarborgbeheer |
Address: Hooikaai 55 B-1000 Brussels |
Large individual aid grants | In conformity with Article 6 of the Regulation The measure excludes awards of aid or requires prior notification to the Commission of awards of aid, (a)if the total eligible costs are at least EUR 25 million andthe gross aid intensity is at least 50 %,in areas which qualify for regional aid, the net aid intensity is at least 50 %; or(b)if the total gross aid amount is at least EUR 15 million | Yes | |
--------------------------------------------------
