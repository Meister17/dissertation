Commission Recommendation
of 27 May 2005
concerning authentication of euro coins and handling of euro coins unfit for circulation
(notified under document number C(2005) 1540)
(2005/504/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community, and in particular Article 211 thereof,
Whereas:
(1) Council Regulation (EC) No 1338/2001 of 28 June 2001 laying down measures necessary for the protection of the euro against counterfeiting [1] requires that credit institutions and any other institutions engaged in the sorting and distribution to the public of notes and coins as a professional activity, including establishments whose activity consists in exchanging notes and coins of different currencies, such as bureaux de change, shall be obliged to withdraw from circulation all euro notes and coins received by them which they know or have sufficient reason to believe to be counterfeit and hand them over to the competent national authorities.
(2) There is no common method ensuring that counterfeit coins are detected and withdrawn from circulation. This entails the risk that counterfeit coins and other euro coin-like objects fraudulently or erroneously put into circulation be further used, thus potentially causing confusion and prejudice to the public.
(3) In the process of authentication of coins through automated sorting a number of objects are rejected including, among others, counterfeit coins and genuine euro coins which are unfit for circulation. Genuine unfit coins are also presented to the competent authorities by companies and individuals.
(4) There are no common rules for national authorities to handle and reimburse such genuine unfit coins, which results in differing treatment among euro area countries and causes distortions in the reimbursement of such euro coins.
(5) In order to assist the implementation of Article 6 of Regulation (EC) No 1338/2001, it is desirable to provide for a process by which the circulating euro coins are authenticated and counterfeits, euro coin-like objects and euro coins unfit for circulation are removed from the cash cycle.
(6) In order to create a level playing field with regard to genuine euro coins unfit for circulation, it is desirable to establish guidelines for handling and reimbursement or replacement of such coins,
HEREBY RECOMMENDS:
PART I
DEFINITIONS AND RECOMMENDED PRACTICES
Article 1
Definitions
For the purpose of this Recommendation, the following definitions shall apply:
(a) "authentication of euro coins" means the process of verifying the authenticity of euro coins through automated electromechanical sorting or manually. During that process counterfeits are rejected, as well as genuine euro coins unfit for circulation, foreign coins similar to the euro and other metallic objects such as medals and tokens similar to euro coins;
(b) "euro coins unfit for circulation" mean genuine euro circulation coins which are defective or whose technical parameters and identification characteristics have been altered notably by relatively long circulation or by accident (dimensions, weight, colour, corrosion, edge damages), as well as deliberately altered coins, other than counterfeits. Regarding, in particular, the technical specifications, for the purposes of this document, coins are unfit if one of the dimensions is different from the one specified for the respective euro coin by at least 0.30 millimetres and/or the weight by at least 5 %.
Article 2
Recommended practices
Member States should carry out or supervise the authentication of the euro coins circulating on their territory in line with the provisions of Part II of this Recommendation.
Member States should adopt common rules for handling, reimbursement or replacement of unfit euro coins in line with the provisions of Part III of this Recommendation.
PART II
AUTHENTICATION OF EURO COINS
Article 3
Authentication and testing requirements
The following euro coin denominations should be authenticated as a minimum: EUR 2, EUR 1, 50 cent.
Authentication should be carried out centrally and/or within the coin handling process. To that effect, Member States should be in contact with the credit institutions and other professional cash handlers concerned, with regard to the implementation of the authentication procedures provided for in Articles 4 to 6 of this Recommendation.
The quantity of euro coins to be authenticated each year in each of the Member States should amount to at least 10 % of the total net volume of the relevant denominations issued by that Member State until the end of the previous year. To ensure the authentication of the relevant quantity of euro coins, in line with Articles 4 to 6, Member States should carry out checks to the appropriate number of credit institutions and other professional cash handlers.
Article 4
The tests for controlling the sorting machines
The control of the functioning of sorting machines should be carried out by means of a detection test, preceded, if applicable, by a check for sorting, as follows:
1. The purpose of the sorting test is to check that the machine is capable of correctly sorting all the euro coin denominations. This is applicable in the absence of national rules governing the sorting capacity.
The sorting test requires at least one hundred genuine euro coins of each of the denominations to be tested. These coins of all denominations are mixed together and passed through the machine three times.
The acceptance ratio should be at least 98 % in each of the trials. In cases where the genuine euro coins used for the sorting and detection tests are at the border of euro specified tolerance ranges, a lower acceptance ratio may be considered.
Any genuine coins rejected by a machine should be re-tested. After three consecutive trials, all genuine coins should be accepted.
2. The purpose of the detection test is to check that the machine is capable of rejecting euro coin-like objects that are not in conformity with the specifications of the euro coins, in particular counterfeit coins.
The detection test requires samples of counterfeit coins for all denominations concerned, with a definition also covering materials used for producing coin-like objects and non-euro coins. For that purpose, a number of representative families should be used, coming from the stocks deposited with the Coin National Analysis Centres (CNACs) or the European Technical and Scientific Centre (ETSC). The ETSC, in collaboration with the CNACs defines and updates these families.
These counterfeits are mixed with an appropriate number of genuine coins, defined in collaboration with the ETSC and passed through the machine three times. All counterfeits should be rejected on each of the trials.
3. In the respect of national rules, the tests provided for in this Article should be performed at least once a year on each of the sorting machines at the sites where the authentication takes place, at the establishments selected in line with Article 3.
Article 5
Possibilities for additional testing of the sorting machines at a CNAC or the ETSC
With a view to enabling manufacturers of sorting machines to obtain the indications necessary for the initial adjustment of their equipment, tests may be carried out at certain CNACs, the ETSC or, following bilateral agreement, on the premises of the manufacturer. These tests should be carried out on the basis of modalities and conditions of confidentiality defined in collaboration with the ETSC.
Following the testing at a CNAC or the ETSC, a summary report is issued for the attention of the entity concerned and copied to the ETSC. The report is kept for at least three years and may be used for comparison purposes.
The report should, as a minimum, include the identification of the machine tested, the test results and global assessment, the precise contents of the sample batches used for the tests, the acceptance criteria, the date and signature of the authorised person.
The CNACs and the ETSC maintain a register of the performance of sorting machines tested on their premises. A consolidated list of sorting machines having successfully undergone the tests provided for in the second paragraph of this Article can be consulted at the CNACs or the ETSC, on an indicative basis.
Article 6
Auditing and reporting
Member States should monitor the capacity of the institutions selected in line with Article 3 to authenticate euro coins on the basis, as a minimum, of the following elements:
- existence of a written policy and organization in the sorting centre for detecting counterfeits, unfit euro coins and euro coin-like objects,
- appointment of trained human resources to fulfil the policy,
- adequacy of technical resources and existence of the manufacturer’s initial report indicating the level of performance of sorting machines,
- existence of a written maintenance plan intended to keep sorting machines at their initial performance level,
- existence of minimum written procedures defining the various processes for sorting euro coins and handing over counterfeits and suspect coins to competent national authorities in a short timeframe,
- volume of coins authenticated.
On the occasion of the checks provided for in Article 3, a light audit may, in the respect of national rules, be performed and reported, based on the above elements.
Each Member State should report annually to the ETSC on the activity of authentication, including the controls and audits, as well as the volume of authenticated euro coins and ratio of each category of rejected objects against the amount of each sorted denomination in circulation.
PART III
HANDLING EURO COINS UNFIT FOR CIRCULATION
Article 7
Reimbursement or replacement of unfit euro coins
Each Member State should provide for reimbursement or, if appropriate, replacement of euro coins unfit for circulation, whose denomination should be identified, irrespective of national side, for companies and individuals established in that Member State or outside the euro area. Member States should ensure similar conditions for reimbursement of unfit coins with a view to allowing submissions irrespective of the country where they were removed from circulation, based on the evaluation provided for in Article 12.
Member States may decide to refuse reimbursement of deliberately altered genuine euro coins if this is in conflict with national practice or tradition (disrespect of king’s effigy, disrespect of issuing authority etc.).
Article 8
Handling fees
A handling fee should, in principle, be imposed for reimbursement or replacement of unfit coins. The fee should be uniform throughout the euro area and should amount to 5 % of the nominal value of the submission.
A quantity of up to one kilogramme of unfit coins per denomination may be exempted from the fee mentioned in the first paragraph each year for one submitting entity.
An additional fee of 15 % of the nominal value of each bag/box may be charged in case a bag/box contains counterfeits or shows anomalies, such as wrongly sorted coins, non-euro coins or euro coins with a non-identifiable denomination or other discrepancies, when these anomalies are at a proportion requiring a more detailed examination, in line with Article 10.
Member States may provide for general exemptions from handling fees in cases where submitting entities cooperate closely and regularly with the authorities in withdrawing from circulation unfit euro coins.
Transport and related costs should be borne by the submitting entity.
Counterfeit coins delivered to the authorities should not be subject to handling or other fees.
Article 9
Packaging of unfit euro coins
The submitting entity should sort the coins per denomination in standardised bags or boxes in line with the standards applicable in the individual Member States to whom the request is addressed. Failure to meet these standards may lead to refusal of the submission.
In the absence of national packaging standards, the bags or boxes should comprise:
- 500 coins for the denominations of EUR 2 or EUR 1,
- 1000 coins for the denominations of EUR 0,50, EUR 0,20 and EUR 0,10,
- 2000 coins for the denominations of EUR 0,05, EUR 0,02 and EUR 0,01.
Each bag/box should bear clear indications of the submitting entity, the value and the denomination contained, the weight, date of packaging and bag/box number. The submitting entity should provide a packaging list with an overview of the bags/boxes submitted.
In case the total quantity of unfit euro coins is smaller than the above standards, unfit euro coins should be sorted by denomination and may be submitted in non-standard packaging.
Article 10
Checks by Member States
1. Member States should check the unfit coins submitted for:
- the quantity declared for each of the bags/boxes,
- authenticity, with a view to ensuring that there are no counterfeits,
- visual appearance, to determine that they are in line with the provisions of Article 7.
2. Checks for the quantity of coins submitted should be performed by weighing each bag/box. A tolerance range of – 2 % and + 1 % should be applied to the nominal weight of the standard packaging referred to in the second paragraph of Article 9. An equivalent control should be applied when national packaging conventions are different from the above standards. Additionally, each bag/box should be checked for visible anomalies.
In the event that the weight of the bag/box is outside the tolerance range, the entire quantity of the concerned bag/box will need to be processed.
3. Checks for authenticity and visual appearance may be conducted through sampling. As a minimum, a representative sample of 10 % of the quantities submitted should be checked for the denominations of EUR 2, EUR 1, EUR 0,50, EUR 0,20 and EUR 0,10.
4. Checks for authenticity should be performed on the samples mentioned in paragraph 3 by means of the following procedures:
(i) in the case of mechanical/automatic sorting, the machines should be adjusted according to the procedures provided for in Part II of this Recommendation;
(ii) in other cases, the criteria of the CNACs are applied.
In the event that one counterfeit is detected, the entire quantity in the bag/box will need to be authenticated.
5. Checks for visual appearance should be performed on the samples mentioned in paragraph 3 with a view to determining whether a bag/box shows anomalies, such as wrongly sorted coins, non-euro coins or euro coins with a non-identifiable denomination. In case such anomalies occur at a proportion greater than 1 %, the relevant bag/box should be re-checked and the proportion of non-reimbursable coins determined by means of one of the following methods:
(i) the entire quantity of coins in each concerned bag/box is examined manually, in a way defined by the competent national authorities;
(ii) a 10 % sample from the bag/box, additional to the one provided for in paragraph 3, is checked for visual appearance. The combined proportion of non-reimbursable coins counted in the two samples is then extended to the entire content of the bag/box.
Article 11
Flow of information and communication
Member States should report annually to the Commission and the Economic and Financial Committee (EFC) the unfit euro coins reimbursed or replaced. The information should include the quantity of such coins and their denomination. Moreover, the Commission will prepare regular reports for the EFC.
Member States should ensure that information concerning the services designated for reimbursement or replacement and specific modalities, such as packaging standards and fees, be made available on the appropriate web sites and through the appropriate publications.
PART IV
FINAL PROVISIONS
Article 12
Evaluation
Three years after the publication of this Recommendation, the present rules will be evaluated in the light of the experience gained, inter alia, as regards the harmonisation of the conditions for reimbursement or replacement of unfit coins provided for in Article 7, the appropriateness of abrogating exemptions from handling fees provided for in Article 8, the possibility of providing for a compensation mechanism among Member States for the unfit coins reimbursed and the potential need for legislation.
Article 13
Addressees
This recommendation is addressed to the participating Member States as defined in Article 1 of Council Regulation (EC) No 974/98 [2].
Done at Brussels, 27 May 2005.
For the Commission
Siim Kallas
Vice-President
[1] OJ L 181, 4.7.2001, p. 6.
[2] OJ L 139, 11.5.1998, p. 1.
--------------------------------------------------
