Commission Decision
of 6 September 2006
on certain protection measures in relation to intra-Community trade in poultry intended for restocking of wild game supplies
(notified under document number C(2006) 3940)
(Text with EEA relevance)
(2006/605/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 90/425/EEC of 26 June 1990 concerning veterinary and zootechnical checks applicable in intra-Community trade in certain live animals and products with a view to the completion of the internal market [1], and in particular Article 10(4) thereof,
Having regard to Council Directive 2005/94/EC of 20 December 2005 on Community measures for the control of avian influenza and repealing Directive 92/40/EEC [2], and in particular Article 3 thereof,
Whereas:
(1) Council Directive 90/539/EEC of 15 October 1990 on animal health conditions governing intra-Community trade in, and imports from third countries of, poultry and hatching eggs [3] lays down animal health conditions governing intra-Community trade and imports from third countries of poultry, including rules for intra-Community trade and imports of poultry intended for restocking wild game supplies.
(2) Commission Decision 2005/734/EC of 19 October 2005 laying down biosecurity measures to reduce the risk of transmission of highly pathogenic avian influenza caused by influenza virus A subtype H5N1 from birds living in the wild to poultry and other captive birds and providing for an early detection system in areas at particular risk [4] provides that Member States are to define on their territory areas at a particular risk for the introduction and occurrence of highly pathogenic avian influenza caused by influenza A virus of subtype H5N1 based on certain risk factors.
(3) Poultry intended for restocking wild game supplies comprise different species of farmed feathered game including waterfowl. Such poultry are bred in captivity and then released into the wild in order to be hunted and serve as a source of wild feathered game meat.
(4) Farming concerning poultry intended for restocking wild game supplies often involves contact with wild birds and might therefore pose an increased risk for the spread of avian influenza in particular when dispatched to other Member States or third countries.
(5) Experiences with outbreaks of highly pathogenic avian influenza of subtype H5N1 and other avian influenza strains of H5 and H7 subtypes have shown that that category of poultry is particularly at risk and that additional measures should be taken to reduce such risks.
(6) Accordingly it is appropriate that Member States draw up guidelines for good biosecurity practices for that type of poultry production, detailing and complementing the measures provided for in Decision 2005/734/EC in particular as regards holdings from which poultry are dispatched to other Member States or third countries.
(7) Directive 2005/94/EC sets out certain preventive measures relating to the surveillance and early detection of avian influenza. That Directive requires the implementation of surveillance programmes for avian influenza in poultry holdings. The guidelines for good biosecurity practices, additional biosecurity measures and testing prior to the dispatch of poultry provided for in this Decision should give further guarantees for trade and exports in live poultry and reduce the risk of spreading the disease.
(8) Laboratory investigations should be carried out in accordance with the procedures laid down in Commission Decision 2006/437/EC of 31 August 2006 approving a Diagnostic Manual for avian influenza as provided for in Council Directive 2005/94/EC.
(9) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
Subject matter and scope
This Decision provides for:
(a) biosecurity measures to be applied on holdings keeping poultry intended for restocking supplies of wild game; and
(b) surveillance measures to be applied when productive poultry intended for restocking supplies of wild game, are dispatched to other Member States or third countries.
Article 2
Definitions
For the purpose of this Decision, the following definitions shall apply:
(a) "poultry" means fowl, turkeys, guinea fowl, ducks, geese, quails, pigeons, pheasants and partridges and ratites (Ratitae) reared or kept in captivity for breeding, the production of meat or eggs for consumption, or for re-stocking supplies of game;
(b) "productive poultry" means poultry 72 hours old or more, reared for the production of meat and/or eggs for consumption or for restocking supplies of game;
(c) "wild game" means wild birds that are hunted for human consumption.
Article 3
Guidelines for good biosecurity practices
Member States, in collaboration with producers keeping poultry for restocking supplies of wild game, shall develop guidelines for good biosecurity practices for such holdings taking into account the biosecurity measures of Decision 2005/734/EC ("guidelines for good biosecurity practices").
Article 4
Conditions for dispatch of poultry for restocking supplies of wild game
1. Member States shall ensure that the dispatch to other Member States or third countries of productive poultry intended for restocking wild game supplies is only authorised if the holding of dispatch was:
(a) subjected to an inspection by the official veterinarian confirming that the holding complies with the guidelines for good biosecurity practices; and
(b) during the two-month period preceding the date of dispatch of the poultry,
(i) either included in the official surveillance programme for avian influenza as provided for in Article 4 of Directive 2005/94/EC;
or
(ii) subjected to a serological investigation, with negative results for the avian influenza virus subtypes H5 and H7, in each case on samples taken at random from the flock of origin from which the consignment is to be drawn, as follows:
- 50 samples in case of ducks or geese, or
- 20 samples in case of other poultry;
2. Member States shall ensure that the dispatch to other Member States or third countries of productive poultry intended for restocking wild game supplies and which is less than one month old, is only authorised if:
(a) the holding of dispatch complied with the conditions of paragraph 1; and
(b) a virological investigation for avian influenza is carried out either by virus isolation or PCR on 20 cloacal swabs and 20 tracheal or oropharyngeal swabs from the poultry to be dispatched, during the one-week period preceding the date of dispatch.
3. Member States shall ensure that before dispatching the productive poultry referred to in paragraphs 1 and 2 of this Article, the health examination of the flock of origin required by Article 10a(1)(c) of Directive 90/539/EEC is carried out during the 24 hours preceding the time of dispatch of the consignment.
4. Member State shall ensure that the laboratory tests provided for in paragraphs 1(b) and paragraph 2 of this Article are carried out in accordance with the Diagnostic Manual established in accordance with Article 50(1) of Directive 2005/94/EC.
Article 5
Certification
Member States shall ensure that the health certificates provided for in Article 17 of Directive 90/539/EEC accompanying consignments of poultry intended for restocking wild game supplies dispatched to other Member States must be completed by the following:
"This consignment complies with the animal health conditions laid down in Commission Decision 2006/605/EC."
Article 6
Compliance measures
Member States shall immediately take the necessary measures to comply with this Decision and publish those measures. They shall immediately inform the Commission thereof.
Article 7
Addressee
This Decision is addressed to the Member States.
Done at Brussels, 6 September 2006.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 224, 18.8.1990, p. 29. Directive as last amended by Directive 2002/33/EC of the European Parliament and of the Council (OJ L 315, 19.11.2002, p. 14).
[2] OJ L 10, 14.1.2006, p. 16.
[3] OJ L 303, 31.10.1990, p. 6. Directive as last amended by the 2003 Act of Accession.
[4] OJ L 274, 20.10.2005, p. 105. Decision as last amended by Decision 2006/405/EC (OJ L 158, 10.6.2006, p. 14).
--------------------------------------------------
