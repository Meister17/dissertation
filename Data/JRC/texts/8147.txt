DECISION OF THE EEA JOINT COMMITTEE
No 68/98
of 4 July 1998
amending Annex VI (Social security) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as adjusted by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas Annex VI to the Agreement was amended by Decision of the EEA Joint Committee No 24/98 of 27 March 1998(1);
Whereas Decision No 165 of 30 June 1997 on the forms necessary for the application of Council Regulations (EEC) No 1408/71 and (EEC) No 574/72 (E 128 and E 128B)(2), adopted by the Administrative Commission of the European Communities on Social Security for Migrant Workers, is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
The following point shall be inserted after point 3.48 (Decision No 164) in Annex VI to the Agreement: "3.49 397 D 0823: Decision No 165 of 30 June 1997 on the forms necessary for the application of Council Regulations (EEC) No 1408/71 and (EEC) No 574/72 (E 128 and E 128B) (OJ L 341, 12.12.1997, p. 61)."
Article 2
The texts of Decision No 165 in the Icelandic and Norwegian languages, which are annexed to the respective language versions of this Decision, are authentic.
Article 3
This Decision shall enter into force on 5 July 1998, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee.
Article 4
This Decision shall be published in the EEA section of, and in the EEA supplement to, the Official Journal of the European Communities.
Done at Brussels, 4 July 1998.
For the EEA Joint Committee
The President
F. BARBASO
(1) OJ L 310, 19.11.1998, p. 4.
(2) OJ L 341, 12.12.1997, p. 61.
