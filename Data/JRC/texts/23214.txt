COMMISSION DECISION of 6 October 1998 laying down special conditions governing imports of live bivalve molluscs, echinoderms, tunicates and marine gastropods originating in Tunisia (notified under document number C(1998) 2952) (Text with EEA relevance) (98/569/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/492/EEC of 15 July 1991 laying down the health conditions for the production and the placing on the market of live bivalve molluscs (1), as last amended by the Directive 97/79/EC (2), and in particular Article 9 thereof,
Whereas a Commission expert has conducted an inspection visit to Tunisia to verify the conditions under which live bivalve molluscs, echinoderms, tunicates and marine gastropods are produced, stored and dispatched to the Community;
Whereas the provisions of legislation of Tunisia makes the 'Direction Générale de la Sante Animale (DGSA) du Ministère de l'Agriculture` responsible for inspecting the health of live bivalve molluscs, echinoderms, tunicates and marine gastropods and for monitoring the hygiene and sanitary conditions of production; whereas the same legislation empowers DGSA to authorise or prohibit the harvesting of bivalve molluscs, echinoderms, tunicates and marine gastropods from certain zones;
Whereas the DGSA and its laboratories are capable of effectively verifying the application of the laws in force in Tunisia;
Whereas the competent Tunisia authorities have undertaken to communicate regularly and quickly to the Commission data on the presence of plankton containing toxins in the harvesting areas;
Whereas the competent Tunisia authorities have provided official assurances regarding compliance with the requirements specified in Chapter V of the Annex to Directive 91/492/EEC and with requirements equivalent to those prescribed in that Directive for the classification of producing and relaying zones, approval of dispatch centres and public health control and production monitoring; whereas in particular any possible change in harvesting zones will be communicated to the Community;
Whereas Tunisia is eligible for inclusion in the list of third countries fulfilling the conditions of equivalence referred to in Article 9(3)(a) of Directive 91/492/EEC;
Whereas the procedure for obtaining the health certificate referred to in Article 9(3)(b)(i) of Directive 91/492/EEC must include the definition of a model certificate, the minimum requirements regarding the language(s) in which it must be drafted and the grade of the person empowered to sign it and the health mark to be affixed to packaging;
Whereas, pursuant to Article 9(3)(b)(ii) of Directive 91/492/EEC, the production areas from which bivalve molluscs, echinoderms, tunicates and marine gastropods may be harvested and exported to the Community must be designated;
Whereas, pursuant to Article 9(3)(c) of Directive 91/492/EEC, a list of the establishments from which the import of bivalve molluscs, echinoderms, tunicates and marine gastropods is authorised should be established; whereas this list must be drawn up on the basis of a communication from the DGSA to the Commission; whereas it is therefore for the DGSA to ensure compliance with the provisions laid down to that end in Article 9(3)(c) of Directive 91/492/EEC;
Whereas the special import conditions apply without prejudice to decisions taken pursuant to Council Directive 91/67/EEC of 28 January 1991 concerning the animal health conditions governing the placing on the market of aquaculture and products (3), as last amended by Directive 97/79/EC;
Whereas the measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
The 'Direction Générale de la Sante Animale (DGSA) du Ministère de l'Agriculture` shall be the competent authority in Tunisia for verifying and certifying compliance of live bivalve molluscs, echinoderms, tunicates and marine gastropods with the requirements of Directive 91/492/EEC.
Article 2
Live bivalve molluscs, echinoderms, tunicates and marine gastropods originating in Tunisia and intended for human consumption, must meet the following conditions:
1. each consignment must be accompanied by a numbered original health certificate, duly completed, signed, dated and comprising a single sheet in accordance with the model in Annex A hereto;
2. consignments must originate in the authorised production areas listed in Annex B hereto;
3. they must be packed in sealed packages by an approved dispatch centre included in the list in Annex C hereto;
4. each package must bear an indelible health mark containing at least the following information:
- country of dispatch: TUNISIA,
- the species (common and scientific names),
- the identification of the production area and the dispatch centre by their approval number,
- the date of packing, comprising at least the day and month.
Article 3
1. Certificates as referred to in Article 2(1) must be drawn up in at least one official language of the Member State where the checks are carried out.
2. Certificates must bear the name, capacity and signature of the representative of the DGSA and the latter's official stamp in a colour different from that of other endorsements.
Article 4
This Decision is addressed to the Member States.
Done at Brussels, 6 October 1998.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ L 268, 24. 9. 1991, p. 1.
(2) OJ L 24, 30. 1. 1998, p. 31.
(3) OJ L 46, 19. 2. 1991, p. 1.
ANNEX A
HEALTH CERTIFICATE covering live bivalve molluscs (1), echinoderms (1), tunicates (1), marine gastropods (1) originating in Tunisia and intended for human consumption in the European Community
>START OF GRAPHIC>
Reference No:
Country of dispatch:
TUNISIA
Competent authority:
'Direction générale de la santé animale (DGSA) du ministère de l'agriculture`
I. Details identifying the fishery products
- Species (scientific name):
- Code number (where available):
- Type of packaging:
- Number of packages:
- Net weight:
- Analysis report number (where available):
II. Origin of products
- Authorised production area:
- Name and official approval number of dispatch centre:
III. Destination of products
The products are dispatched
from:
(place of dispatch)
to:
(country and place of destination)
by the following means of transport:
Name and address of dispatcher:
Name of consignee and address at place of destination:
(1) Delete where inapplicable.
IV. Health attestation
- The official inspector hereby certifies that the live products specified above:
1. were harvested, where necessary relayed, and transported in accordance with the health rules laid down in Chapters I, II and III of the Annex to Directive 91/492/EEC;
2. were handled, where necessary purified, and packaged in compliance with the requirements laid down in Chapter IV of the Annex to Directive 91/492/EEC;
3. have undergone health controls in accordance with Chapter VI of the Annex to Directive 91/492/EEC;
4. are in compliance with Chapters V, VII, VIII, IX, and X of the Annex to Directive 91/492/EEC and therefore fit for immediate human consumption.
- The undersigned official inspector hereby declares that he is aware of the provisions of the Directive 91/492/EEC and Decision 98/569/EC.
Done at,
(Place)
on
(Date)
Official
Stamp (1)
Signature of official inspector (1)
(Name in capital letters, capacity and qualifications of person signing)
(1) The colour of the stamp and signature must be different from that of the other particulars in the certificate.
>END OF GRAPHIC>
ANNEX B
>TABLE>
ANNEX C
>TABLE>
