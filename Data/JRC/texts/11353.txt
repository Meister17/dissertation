Guideline of the European Central Bank
of 14 July 2006
on certain preparations for the euro cash changeover and on frontloading and sub-frontloading of euro banknotes and coins outside the euro area
(ECB/2006/9)
(2006/525/EC)
THE GOVERNING COUNCIL OF THE EUROPEAN CENTRAL BANK,
Having regard to the Treaty establishing the European Community, and in particular to Article 106(1) thereof,
Having regard to Article 16 and Article 26.4 of the Statute of the European System of Central Banks and of the European Central Bank,
Whereas:
(1) Article 10 of Council Regulation (EC) No 974/98 of 3 May 1998 on the introduction of the euro [1] states that "with effect from the respective cash changeover dates, the ECB and the central banks of the participating Member States shall put into circulation banknotes denominated in euro in the participating Member States".
(2) To permit a smooth introduction of the euro in future participating Member States, a legal framework to enable the national central banks (NCBs) of these Member States to borrow euro banknotes and coins from the Eurosystem for the purpose of frontloading and sub-frontloading them prior to the cash changeover should be established, taking into account different possible national cash changeover scenarios.
(3) Frontloading of euro banknotes and coins to eligible counterparties, and sub-frontloading to professional third parties, would contribute to a smooth cash changeover, alleviate the logistical burden of adopting the euro and contribute to reducing the costs associated with dual currency circulation.
(4) Frontloading and sub-frontloading of euro banknotes and coins should not amount to putting such euro banknotes and coins into circulation as they will not have the status of legal tender in the future participating Member States before the cash changeover date; consequently the contractual arrangements for loans of euro banknotes and coins should contain obligations to impose certain restrictions on eligible counterparties and professional third parties in order to avoid this happening.
(5) Frontloading to eligible counterparties and sub-frontloading to professional third parties may only take place if statutory provisions in the future participating Member States provide sufficient protection, or contractual arrangements are established between the parties involved, in relation to: (i) loans of euro banknotes and coins for frontloading; (ii) frontloading; and (iii) sub-frontloading.
(6) This Guideline should: (i) set out the rules to be applied for the contractual framework and conditions for frontloading and sub-frontloading; (ii) lay down the accounting and financial reporting requirements to be observed in relation to frontloading and sub-frontloading; and (iii) provide for appropriate arrangements to insure frontloaded and sub-frontloaded euro banknotes and coins.
(7) While the primary competence for establishing the regime for the issue of euro coins lies with the participating Member States, future Eurosystem NCBs play an essential role in the distribution of euro coins. Therefore, the provisions of this Guideline which relate to euro coins should be viewed as recommendations to be applied by the NCBs within the framework for the issue of euro coins to be set up by the competent national authorities of the future participating Member States.
(8) Providing the future Eurosystem NCBs with euro banknotes and coins for the purpose of frontloading entails certain financial risks. To cover these risks, the future Eurosystem NCBs should commit to repay the euro banknotes borrowed from the Eurosystem from the future banknote production requirements allocated to them. Furthermore, frontloading should only be permitted if eligible counterparties provide the relevant future Eurosystem NCB with sufficient eligible collateral.
(9) The Eurosystem NCB delivering euro banknotes and coins for the purpose of frontloading and the future Eurosystem NCBs should conclude specific contractual arrangements to adhere to the rules and procedures laid down in this Guideline.
(10) Unless national statutory provisions in force in the future participating Member States ensure that equivalent rules and procedures apply, the conditions laid down in this Guideline for frontloading and subsequent sub-frontloading must be incorporated in contractual arrangements between the future Eurosystem NCBs, eligible counterparties and professional third parties.
(11) The ECB, as coordinator of frontloading, should be informed in advance of requests for frontloading, and the future Eurosystem NCBs should inform the ECB of any decisions to frontload,
HAS ADOPTED THIS GUIDELINE:
CHAPTER I
GENERAL PROVISIONS
Article 1
Definitions
For the purposes of this Guideline:
- "frontloading" means the physical delivery of euro banknotes and coins by a future Eurosystem NCB to eligible counterparties in the territory of a future participating Member State during the frontloading/sub-frontloading period;
- "frontloading/sub-frontloading period" means the period during which frontloading and sub-frontloading take place, which shall commence no sooner than four months prior to the cash changeover date and end at 00.00 (local time) on the cash changeover date;
- "sub-frontloading" means the delivery of frontloaded euro banknotes and coins by an eligible counterparty to professional third parties in the territory of a future participating Member State during the frontloading/sub-frontloading period;
- "euro area" means the territory of the participating Member States;
- "cash changeover date" means the date on which euro banknotes and coins become legal tender in a given future participating Member State;
- "participating Member State" means a Member State that has adopted the euro;
- "future participating Member State" means a non-participating Member State that has fulfilled the conditions set for the adoption of the euro and in relation to which a decision on the abrogation of the derogation (pursuant to Article 122(2) of the Treaty) has been taken;
- "non-participating Member State" means a Member State that has not adopted the euro;
- "Eurosystem" means the NCBs of the participating Member States and the ECB;
- "eligible counterparty" means an entity as defined in Article 5 which fulfils the requirements to receive euro banknotes and coins for the purpose of frontloading;
- "professional third parties" means certain commercial target groups, such as retailers, the cash-operated machine industry and cash in transit companies which are located in the same future participating Member State as an eligible counterparty, and which the eligible counterparty considers to have a legitimate need to be sub-frontloaded and to be able to satisfy the requirements in relation to sub-frontloading;
- "cash in transit company" means an entity providing transport, storage and handling services of banknotes and coins for credit institutions;
- "Eurosystem NCB" means the NCB of a participating Member State;
- "future Eurosystem NCB" means the NCB of a future participating Member State;
- "eligible collateral" means collateral as defined in Article 8;
- "delivering Eurosystem NCB" means a Eurosystem NCB that delivers to a future Eurosystem NCB euro banknotes and coins for the purpose of frontloading, regardless of which Eurosystem NCB is the legal owner of such banknotes and coins;
- "launch requirements" means the quantity of euro banknotes and coins that it is expected will be needed in a future participating Member State on the cash changeover date to cover demand for a period of one year;
- "Eurosystem business day" means a day on which the ECB or one or more NCBs are open for business, on which TARGET or the system replacing TARGET is open, and which is a settlement day for euro money market and foreign exchange transactions involving the euro;
- "TARGET" means the Trans-European Automated Real-time Gross settlement Express Transfer system;
- "credit institution" means an institution as defined in Article 1(1)(a) of Directive 2000/12/EC of the European Parliament and of the Council of 20 March 2000 relating to the taking up and pursuit of the business of credit institutions [2];
- "legacy currency" means the national currency unit of a future participating Member State before the cash changeover date.
Article 2
Applicability of the provisions set out in this Guideline
1. The rules and procedures concerning frontloading and sub-frontloading laid down in this Guideline shall be applied to frontloading and sub-frontloading arrangements regardless of whether a future Eurosystem NCB: (i) borrows the banknotes and coins to be frontloaded; or (ii) produces or procures them.
2. This Guideline shall not apply to the physical delivery of euro banknotes and coins from Eurosystem NCBs to NCBs of non-participating Member States until the latter have the status of future Eurosystem NCBs.
CHAPTER II
LOANS OF EURO BANKNOTES AND COINS FOR FRONTLOADING
Article 3
Delivery
1. One or more Eurosystem NCBs, as the case may be, may deliver euro banknotes and coins to a future Eurosystem NCB for the purpose of frontloading and launch requirements.
2. A delivering Eurosystem NCB shall not request collateral from a receiving future Eurosystem NCB.
3. The delivery of euro banknotes and coins by a Eurosystem NCB to a future Eurosystem NCB shall not take place before the delivering Eurosystem NCB and the receiving future Eurosystem NCB have concluded contractual arrangements stipulating that the conditions set out in this Guideline apply to the loan of euro banknotes and coins to the future Eurosystem NCB, and hence will be applied when establishing the arrangements for frontloading and sub-frontloading.
4. The delivery of euro banknotes and coins shall not commence before a decision on the abrogation of the derogation of a non-participating Member State has been adopted pursuant to Article 122(2) of the Treaty.
5. After having consulted the delivering Eurosystem NCB, the ECB shall clearly specify the stocks from which the euro banknotes and coins to be delivered will be taken, and the name of the delivering Eurosystem NCB. The delivering Eurosystem NCB shall ensure that a decision concerning the replenishment of such stocks has been taken.
Article 4
Conditions applicable to a loan of euro banknotes and coins
1. The conditions set out in this Article shall be specified in the contractual arrangements referred to in Article 3(3).
2. The exact volume, broken down by denomination of euro banknotes and coins to be delivered, and the time of delivery, shall be specified in any contractual arrangements.
3. Euro banknotes and coins shall be transported to a future Eurosystem NCB for the purpose of frontloading and launch requirements in accordance with the rules on security and insurance that normally apply to bulk transfers of euro banknotes and coins between the NCBs. The risk of destruction, loss, theft and robbery of the delivered euro banknotes and coins shall pass to the future Eurosystem NCB from the moment when the euro banknotes and coins leave the vaults of the delivering Eurosystem NCB.
4. A future Eurosystem NCB shall pay the cost of transporting euro banknotes and coins from a Eurosystem NCB to it. The delivering Eurosystem NCB shall ensure that the transport is carried out efficiently.
5. If a future Eurosystem NCB needs a bulk transfer of euro banknotes and coins from the Eurosystem within 12 months of the cash changeover date, these requirements will be regarded as part of the launch requirements and, as regards repayment, shall be treated similarly to frontloaded euro banknotes and coins, as provided for in paragraphs 6 to 8. In all other respects, the fulfilment of such requirements shall be treated in the same way as a bulk transfer.
6. A future Eurosystem NCB shall observe the following accounting and financial reporting obligations in relation to delivering Eurosystem NCB(s):
(a) During the frontloading/sub-frontloading period, the future Eurosystem NCB shall record the amount of euro banknotes and coins delivered for the purpose of frontloading (and for launch requirements) off-balance sheet at their face value.
(b) The future Eurosystem NCB shall report the amount of the frontloaded and sub-frontloaded euro banknotes and coins to the delivering Eurosystem NCB(s).
(c) The future Eurosystem NCB shall report the total amount (broken down by denomination) of any frontloaded or sub-frontloaded euro banknotes that entered into circulation before the cash changeover date, as well as the date on which it became aware that these banknotes had entered into circulation.
7. From the cash changeover date, a future Eurosystem NCB shall observe the following accounting and financial reporting obligations:
(a) Unless they have already been recorded pursuant to paragraph 10, frontloaded euro banknotes shall be recorded as on balance sheet items as at the cash changeover date.
(b) The total amount of frontloaded euro banknotes, excluding any banknotes that entered into circulation before the cash changeover date reported under paragraph 6(c), shall be recorded in the balance sheet of the future Eurosystem NCB within the "banknotes in circulation" figure.
(c) The difference between the total amount of frontloaded euro banknotes and the amounts of frontloaded euro banknotes that have been debited in the accounts of frontloaded eligible counterparties held with a future Eurosystem NCB under the provisions of Article 15 shall be treated as a collateralised, non-remunerated loan granted to the eligible counterparties and to be repaid by them in accordance with Article 15.
8. A future Eurosystem NCB shall repay the euro banknotes borrowed from a Eurosystem NCB for the purpose of frontloading by delivering an equivalent number and quality of euro banknotes that it produces or procures as a result of an allocation to it in the Eurosystem’s euro banknote production for one or more consecutive years immediately following the year in which the cash changeover takes place, in addition to its normal share in the Eurosystem’s euro banknote production allocation for the years concerned. The calculation of the equivalent number and quality of banknotes to be repaid will be decided by the Governing Council. The equivalent number and quality of banknotes to be repaid for the euro second series will be calculated as established by the Governing Council in due time.
9. A future Eurosystem NCB shall conduct frontloading in accordance with the conditions set out in Chapters III and IV. No delivery shall take place until the future Eurosystem NCB and an eligible counterparty have concluded contractual arrangements that incorporate such conditions, unless national statutory provisions on frontloading ensure that equivalent conditions to those set out in Chapters III and IV apply to all eligible counterparties.
10. If frontloaded euro banknotes enter into circulation before the cash changeover date, a delivering Eurosystem NCB shall record them as having been issued and being in circulation. The delivering Eurosystem NCB shall record a claim vis-à-vis the future Eurosystem NCB amounting to the nominal value of the euro banknotes that entered into circulation before the cash changeover date. The future Eurosystem NCB shall pay to the delivering Eurosystem NCB remuneration on that claim. The remuneration shall be payable from the date when the future Eurosystem NCB became aware that these euro banknotes entered into circulation until the first Eurosystem business day following the cash changeover date. On that date, the liability of the future Eurosystem NCB and the related remuneration shall be settled by TARGET or a system replacing TARGET. The reference rate for the remuneration shall be the latest available marginal interest rate used by the Eurosystem in its tenders for main refinancing operations under paragraph 3.1.2 of Annex I to Guideline ECB/2000/7 of 31 August 2000 on monetary policy instruments and procedures of the Eurosystem [3]. In the event that more than one Eurosystem NCB delivers euro banknotes to the future Eurosystem NCB for the purpose of frontloading, the contractual arrangements referred to in Article 3(3) shall specify the delivering NCB which shall record the banknotes in circulation and the claim vis-à-vis the future Eurosystem NCB.
11. A future Eurosystem NCB shall report the following to the ECB:
(a) the final total amount of frontloaded and sub-frontloaded euro banknotes (broken down by denomination); and
(b) the final total amount of frontloaded and sub-frontloaded euro coins (broken down by denomination).
CHAPTER III
FRONTLOADING
Article 5
Eligible counterparties
Credit institutions established in a future participating Member State (including branches of foreign credit institutions located in the future participating Member State) and national post offices that have an account with their future Eurosystem NCB shall be considered eligible to receive euro banknotes and coins for the purpose of frontloading once the contractual arrangements provided for in Article 4(9) have been established.
Article 6
Delivery for frontloading
1. A future Eurosystem NCB may not start delivery of euro banknotes and coins for frontloading before the frontloading/sub-frontloading period has commenced.
2. A future Eurosystem NCB may frontload euro banknotes and coins in accordance with the provisions of this Guideline. The frontloading of euro banknotes and coins shall not commence before the future Eurosystem NCB and the receiving eligible counterparty have concluded contractual arrangements that incorporate the conditions set out in this Chapter and Chapter IV (unless equivalent rules and procedures have been established by statutory provisions in the future participating Member State in question).
Article 7
Provision of collateral
1. Eligible counterparties that are to be frontloaded shall provide their future Eurosystem NCB with eligible collateral, as defined in Article 8, to:
(a) cover the full face value of frontloaded euro banknotes and coins; and
(b) ensure performance of the obligations set out in Article 10 to be included in the contractual arrangements between the future Eurosystem NCB and the eligible counterparty.
2. If collateral is realised to ensure the performance of the obligations set out in Article 10, the eligible counterparty shall provide the future Eurosystem NCB with additional collateral to cover the full face value of frontloaded euro banknotes and coins, as required under paragraph 1(a).
3. The collateral shall be provided to the future Eurosystem NCB before it starts frontloading euro banknotes and coins, and shall cover risks arising from the start of delivery for frontloading.
4. The future Eurosystem NCB shall ensure that the collateral is fully enforceable. For this purpose it shall mobilise the eligible collateral by means of appropriate collateralisation procedures in a manner set out in Annex I to Guideline ECB/2000/7.
5. A future Eurosystem NCB shall implement adequate risk control measures to bear the risks relating to frontloading. It shall consult the ECB prior to frontloading on the risk control measures referred to in this Article. Where the market value of the eligible collateral is adjusted to take into account the applied risk control measures, the amount of collateral should be adjusted accordingly so that it always covers the full face value of frontloaded euro banknotes and coins which has not been debited on eligible counterparties’ accounts with the future Eurosystem NCB that provided the frontloading euro banknotes and coins.
6. The contractual arrangements to be concluded before frontloading shall provide that an eligible counterparty shall grant the future Eurosystem NCB the right to realise the collateral if the frontloaded eligible counterparty breaches any of the obligations referred to in this Guideline as a precondition to frontloading and specifically agreed on between the eligible counterparty and the future Eurosystem NCB, and the frontloaded eligible counterparty does not pay the contractual penalties provided for in Article 10.
Article 8
Eligible collateral
1. All eligible assets for Eurosystem monetary policy operations as defined in Annex I to Guideline ECB/2000/7 shall be considered to be eligible collateral for frontloading purposes.
2. Assets denominated either in the legacy currencies of future participating Member States or in euro, which fulfil the uniform criteria laid down in Annex I to Guideline ECB/2000/7 and are eligible for Eurosystem monetary policy operations (with the exception of the criterion on the place of settlement and currency of denomination), shall be considered to be eligible collateral for frontloading purposes. The assets shall be held (settled) in the euro area, or in the future participating Member State, with a domestic securities settlement system (SSS) assessed against the ECB’s "Standards for the use of EU Securities Settlement Systems in ESCB credit operations".
3. The following may also be provided as eligible collateral: (a) cash deposits denominated in a legacy currency; (b) cash deposits in euro on a dedicated account, remunerated at the same rate as applied for minimum reserves; or (c) deposits denominated in a legacy currency or in euro and in another form considered appropriate by the future Eurosystem NCB.
Article 9
Reporting
1. An eligible counterparty shall report to its future Eurosystem NCB:
(a) the final total amount of sub-frontloaded euro banknotes (broken down by denomination); and
(b) the final total amount of sub-frontloaded euro coins (broken down by denomination).
2. Immediately upon sub-frontloading, a frontloaded eligible counterparty shall provide its future Eurosystem NCB with information on the identity of the professional third parties that have been sub-frontloaded, as well as the amounts of sub-frontloaded euro banknotes and coins per individual customer. The future Eurosystem NCB shall treat such information as confidential and shall only use it to monitor how the professional third parties comply with their obligations relating to the avoidance of early circulation of euro banknotes and coins, and for reporting pursuant to Article 4(11).
3. A frontloaded eligible counterparty shall immediately inform the future Eurosystem NCB that frontloaded it (which shall then inform the ECB thereof):
(a) if there is any reason to believe that any frontloaded euro banknotes or coins have entered into circulation before the cash changeover date; and
(b) of the total amount (broken down by denomination) of frontloaded banknotes, if any, that entered into circulation before the cash changeover date.
Article 10
Commitments of an eligible counterparty regarding sub-frontloading
Before sub-frontloading takes place, the frontloaded eligible counterparties shall undertake to carry out sub-frontloading only in accordance with the rules and procedures laid down in this Guideline, which shall be agreed upon between themselves and the professional third parties to be sub-frontloaded. In particular, the following conditions shall be agreed before the eligible counterparty may sub-frontload:
(a) The eligible counterparty shall ensure that the sub-frontloaded euro banknotes and coins remain on the premises of the sub-frontloaded professional third parties, where they shall be stored separately from any other euro banknotes and coins, other currency or other property, to avoid them entering into circulation prior to the cash changeover date. Such early circulation shall be subject to the payment of appropriate contractual penalties.
(b) The eligible counterparty shall agree with the professional third party to be sub-frontloaded that the latter will allow the future Eurosystem NCB to carry out audits and inspections at the sub-frontloaded professional third party’s premises in order to verify the presence of the sub-frontloaded euro banknotes and coins.
(c) The eligible counterparty shall pay to the future Eurosystem NCB contractual penalties in an amount proportional to any damage suffered, however no less than 10 % of the sub-frontloaded amount if: (i) the future Eurosystem NCB is not given access to carry out the audit and inspections referred to in paragraph (b); or (ii) if the sub-frontloaded euro banknotes and coins are not stored on the premises of the sub-frontloaded professional third party as set out in this Article. A future Eurosystem NCB shall not impose such contractual penalties: (i) if its future participating Member State has established a regulatory framework providing for an equivalent level of protection; or (ii) to the extent that a sub-frontloaded professional third party has already paid penalties pursuant to Article 16(2)(f).
Article 11
Statistical aspects
For the purposes of the application of Regulation (EC) No 2423/2001 (ECB/2001/13) of 22 November 2001 concerning the consolidated balance sheet of the monetary financial institutions sector [4], a future Eurosystem NCB shall ensure that monetary financial institutions within its Member State do not record items and transactions relating to frontloaded euro banknotes and coins on their balance sheet during the frontloading/sub-frontloading period.
Article 12
Distribution to branches
A future Eurosystem NCB shall allow eligible counterparties to distribute frontloaded euro banknotes and coins only to their branches within the future participating Member State.
Article 13
Prohibition on early circulation
1. A future Eurosystem NCB shall prohibit eligible counterparties from disposing of the euro banknotes and coins delivered to them before 00:00 (local time) on the cash changeover date, unless otherwise provided for by this Guideline. In particular, the future Eurosystem NCB shall require that eligible counterparties store the frontloaded euro banknotes and coins in their vaults separately from any other euro banknotes and coins, other currency or other property, and safely in order to avoid destruction, theft, robbery or any other cause of early circulation.
2. Eligible counterparties shall ensure that there is no circulation of any frontloaded euro banknotes and coins prior to the cash changeover date.
3. In order to verify the presence of the frontloaded euro banknotes and coins and the arrangements under which the eligible counterparties carry out sub-frontloading, eligible counterparties shall grant their future Eurosystem NCB the right to audit and inspect their premises.
4. Eligible counterparties shall undertake to pay penalties to the future Eurosystem NCB, in the event that the eligible counterparty breaches the obligations relating to frontloading, including but not limited to putting, or acting in a way that is conducive to putting, the frontloaded banknotes into circulation before the cash changeover date, or refusing to allow auditing or inspections. The future Eurosystem NCB shall ensure that such breaches are subject to contractual or statutory penalties, as appropriate, payable in an amount proportional to any damage suffered. The future Eurosystem NCB shall not impose such penalties if the future participating Member State in question has established a regulatory framework providing for an equivalent level of protection.
Article 14
Risk of destruction, loss, theft and robbery
Eligible counterparties shall bear the risk of destruction, loss, theft and robbery of frontloaded euro banknotes and coins from the moment when such banknotes and coins leave the vaults of the future Eurosystem NCB. A future Eurosystem NCB may require eligible counterparties to cover these risks by taking out adequate insurance or by any other appropriate means. However, the future Eurosystem NCB and the eligible counterparty shall agree that notwithstanding such insurance, the provisions of Article 15 relating to immediate debiting of frontloaded euro banknotes or coins that enter into circulation early and related remuneration payments shall apply. Notwithstanding the above, a future Eurosystem NCB and an eligible counterparty may agree that the future Eurosystem NCB shall take care of the practical arrangements relating to the transport of euro banknotes and coins for the purpose of frontloading on behalf of and at the risk of the eligible counterparty, or if the future Eurosystem NCB so wishes, at the risk of the future Eurosystem NCB.
Article 15
Debiting and crediting
1. Euro banknotes and coins frontloaded to eligible counterparties shall be debited in those counterparties’ accounts with their future Eurosystem NCB at their face value, in accordance with the following "linear debiting model": the total amount of frontloaded euro banknotes and coins shall be debited in three equal instalments, on the settlement date of the first, fourth and fifth Eurosystem main refinancing operations following the cash changeover date.
2. If there are not enough funds available on a frontloaded eligible counterparty’s account with the future Eurosystem NCB that frontloaded it to debit the account as provided for in paragraph 1, then the eligible counterparty shall be considered to have breached its obligation to pay for the frontloaded euro banknotes and coins.
3. Euro banknotes and coins delivered to eligible counterparties on or after the cash changeover date shall be debited in their respective accounts with future Eurosystem NCBs in accordance with current Eurosystem practice. Euro banknotes and coins returned by eligible counterparties on or after the cash changeover date shall likewise be credited to their respective accounts with future Eurosystem NCBs.
4. Banknotes and coins denominated in a legacy currency and returned by eligible counterparties shall be credited to their respective accounts with the future Eurosystem NCB in accordance with current Eurosystem practice.
5. If euro banknotes or coins enter into circulation prior to the cash changeover date then the amount of such banknotes or coins shall be immediately charged to the frontloaded eligible counterparty as foreign exchange. Any such euro banknotes shall be recorded as being "in circulation" in the accounts of the Eurosystem NCB that delivered them to the future Eurosystem NCB for the purpose of frontloading. The recording shall occur regardless of the reason for the banknotes entering into circulation prior to the cash changeover date.
CHAPTER IV
SUB-FRONTLOADING
Article 16
Conditions applying to delivery of euro banknotes and coins for sub-frontloading
1. Sub-frontloading of professional third parties may not start before the frontloading/sub-frontloading period has commenced.
2. Before any sub-frontloading may start, the eligible counterparty and the professional third parties shall conclude contractual arrangements that cover at least the following:
(a) Sub-frontloading shall take place at the full risk and responsibility of the professional third party and subject to any conditions agreed in accordance with this Guideline.
(b) The professional third party shall report all sub-frontloaded euro banknotes and coins to the ECB via its future Eurosystem NCB.
(c) The professional third party shall store sub-frontloaded euro banknotes and coins as required under Article 10(a), and shall not dispose of them prior to 00:00 (local time) on the cash changeover date.
(d) The professional third party shall grant its future Eurosystem NCB the right to audit and inspect its premises to verify the presence of sub-frontloaded banknotes and coins.
(e) The professional third party shall report to the future Eurosystem NCB the total amount (broken down by denomination) of sub-frontloaded banknotes, if any, that entered into circulation before the cash changeover date.
(f) The professional third party shall undertake to pay penalties to the future Eurosystem NCB in the event that the professional third party breaches the obligations relating to sub-frontloading, including but not limited to a breach of the obligation set out in paragraph (c) or a refusal to allow auditing or inspections, as referred to in paragraph (d). Such breaches shall be subject to contractual or statutory penalties, as appropriate, payable in an amount proportional to any damage suffered, however no less than 10 % of the sub-frontloaded amount. The future Eurosystem NCB shall not impose such penalties if the future participating Member State in question has established a regulatory framework providing for an equivalent level of protection.
Article 17
Exclusion of the general public
1. A future Eurosystem NCB shall prohibit eligible counterparties from sub-frontloading euro banknotes and coins to the general public.
2. Paragraph 1 of this Article does not prohibit the provision to the general public of coin starter kits containing small amounts of euro coins of different denominations, as specified by the competent national authorities in some future participating Member States, as the case may be.
CHAPTER V
FINAL PROVISIONS
Article 18
Verification
Future Eurosystem NCBs shall forward to the ECB copies of any legal instruments and measures adopted in their Member State in relation to this Guideline at the latest three months prior to commencement of the frontloading/sub-frontloading period, however not before any decision on abrogation of the derogation has been taken in relation to such Member State.
Article 19
Euro coins
It is recommended that future Eurosystem NCBs should apply the provisions of this Guideline to euro coins unless otherwise provided for within the framework set up by their competent national authorities.
Article 20
Final provisions
1. This Guideline shall enter into force on 19 July 2006.
2. This Guideline is addressed to the NCBs of participating Member States.
Done at Frankfurt am Main, 14 July 2006.
For the Governing Council of the ECB
The President of the ECB
Jean-Claude Trichet
[1] OJ L 139, 11.5.1998, p. 1. Regulation as last amended by Regulation (EC) No 2169/2005 (OJ L 346, 29.12.2005, p. 1).
[2] OJ L 126, 26.5.2000, p. 1.
[3] OJ L 310, 11.12.2000, p. 1. Guideline as last amended by Guideline ECB/2005/17 (OJ L 30, 2.2.2006, p. 26).
[4] OJ L 333, 17.12.2001, p. 1. Regulation as last amended by Regulation (EC) No 2181/2004 (ECB/2004/21) (OJ L 371, 18.12.2004, p. 42).
--------------------------------------------------
