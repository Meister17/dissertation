DECISION No 148 of 25 June 1992 concerning the use of the certificate concerning the applicable legislation (Form E 101) where the period of posting does not exceed three months
(93/68/EEC)THE ADMINISTRATIVE COMMISSION OF THE EUROPEAN COMMUNITIES ON SOCIAL SECURITY FOR MIGRANT WORKERS,
Having regard to Article 81 (a) of Council Regulation (EEC) No 1408/71 of 14 June 1971 on the application of social security schemes to employed persons, to self-employed persons and to the members of their families moving within the Community, under which it is made responsible for dealing with all administrative matters relating to the application of the said Regulation,
Whereas Decision No 125 should be amended because of the need to limit the risk of abuse and whereas to this end it is necessary in all cases to enter a specific date in box 5 of form E 101;
Whereas the conditions of modern economic life make it necessary for many employees of industrial, commercial and agricultural undertakings to make frequent brief trips abroad for the benefit of the undertaking by which they are employed;
Whereas it is difficult in such cases to obtain in time, prior to their departure, a certificate concerning the applicable legislation from the designated institution of the country in which the undertaking has its registered office;
Whereas there are grounds for making the procedure for issuing the certificate concerning the applicable legislation more flexible in order to ensure that the insured person carries with him a document showing that he is staying in one of the Member States on business and that he remains covered by the legislation of the country in which the undertaking by which he is employed is situated,
HAS DECIDED AS FOLLOWS:
1. For periods of posting not exceeding three months the institution referred to in Article 11 (1) of Regulation (EEC) No 574/72 may issue to undertakings, on request, copies of Form E 101 bearing a serial number, of which it shall complete only box 5.
2. The undertaking shall complete two copies of the form. One of these copies shall be handed to the worker prior to his departure and the other copy shall be sent within 24 hours to the designated institution of the country in which the undertaking has its registered office.
3. This Decision, which replaces Decision No 125, shall enter into force on the first day of the month following its publication in the Official Journal of the European Communities.
The Chairman of the Administrative Commission
S. PINTO PIZARRO
