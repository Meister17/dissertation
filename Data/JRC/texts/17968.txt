Council Decision
of 7 December 2004
amending Decision 2000/746/EC authorising the French Republic to apply a measure derogating from Article 11 of the sixth Council Directive (77/388/EEC) on the harmonisation of the laws of the Member States relating to turnover taxes
(2004/856/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community,
Having regard to the sixth Council Directive (77/388/EEC) of 17 May 1977 on the harmonisation of the laws of the Member States relating to turnover taxes – Common system of value added tax: uniform basis of assessment [1], and in particular Article 27(1) thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) By Decision 2000/746/EC [2], the Council authorised the French Republic, by way of derogation from Article 11(A)(1)(a) of Directive 77/388/EEC, to include in the taxable amount of a supply of goods or a supply of services the value of any gold used by the supplier and provided by the recipient in cases where the supply of the gold to the recipient is exempt in accordance with Article 26b of Directive 77/388/EEC.
(2) The aim of that derogation was to avoid abuse of the exemption for investment gold and thus to prevent certain types of tax evasion or avoidance.
(3) By letter registered with the Secretariat-General of the Commission on 6 July 2004, the French Government requested an extension of Decision 2000/746/EC, which expires on 31 December 2004.
(4) In accordance with Article 27(2) of Directive 77/388/EEC, the Commission informed the other Member States by letter dated 10 August 2004 of the request made by the French Republic and notified the French Republic that it had all the information it considered necessary for appraisal of the request.
(5) According to the French authorities, the derogation authorised by Decision 2000/746/EC has been effective in achieving the aims stated above.
(6) The derogations pursuant to Article 27 of Directive 77/388/EEC which counter VAT avoidance linked to the exemption for investment gold may be included in a future proposal for a directive rationalising some of the derogations pursuant to that Article.
(7) It is therefore necessary to extend the validity of the derogation granted under Decision 2000/746/EC until the entry into force of a directive rationalising the derogations pursuant to Article 27 of Directive 77/388/EEC which covers the avoidance of value added tax linked to the exemption for investment gold or until 31 December 2009, whichever is the earlier.
(8) The derogation will have no negative impact on the European Communities' own resources provided from VAT,
HAS ADOPTED THIS DECISION:
Article 1
Article 2 of Decision 2000/746/EC shall be replaced by the following:
"Article 2
The authorisation granted under Article 1 shall expire on the date of entry into force of a directive rationalising the derogations pursuant to Article 27 of Directive 77/388/EEC which counter avoidance of value added tax linked to the exemption for investment gold or on 31 December 2009, whichever is the earlier."
Article 2
This Decision is addressed to the French Republic.
Done at Brussels, 7 December 2004.
For the Council
The President
G. Zalm
[1] OJ L 145, 13.6.1977, p. 1. Directive as last amended by Directive 2004/66/EC (OJ L 168, 1.5.2004, p. 35).
[2] OJ L 302, 1.12.2000, p. 61.
--------------------------------------------------
