Information procedure — Technical rules
(2005/C 178/06)
(Text with EEA relevance)
Directive 98/34/EC of the European Parliament and of the Council of 22 June 1998 laying down a procedure for the provision of information in the field of technical standards and regulations and of rules on Information Society services. (OJ L 204, 21.7.1998, p. 37; OJ L 217, 5.8.1998, p. 18).
Notifications of draft national technical rules received by the Commission
Reference | Title | End of three-month standstill period |
2005/0261/D | Fourth Order amending the Packaging Order | 7.9.2005 |
2005/0262/D | First Order amending the End-of-life Vehicles Order | 7.9.2005 |
2005/0263/A | Act of …, amending the Burgenland Construction Act 1997 (Burgenland Construction Act Amendment 2005) | 8.9.2005 |
2005/0264/S | The National Police Board's administrative provisions and general guidance on the storage and transport of firearms and ammunition by the police and other national authorities (RPSFS 2005:00, FAP 943-1) | 8.9.2005 |
2005/0265/I | Interministerial Decree amending Annexes 1B, 1C and 3 to Law No 748 of 19 October 1984 on: "New standards for the regulation of fertilisers" | 8.9.2005 |
2005/0266/A | Order of the Upper Austrian Provincial Government on provisions concerning safety and environmental protection in relation to heating installations for solid and liquid fuels and for the use and storage of solid and liquid fuels and other combustible liquids (Upper Austrian Heating Installations and Fuels Order 2005 [Austrian designation: Oö. HaBV 2005) | 9.9.2005 |
2005/0267/I | Technical standards for construction | 8.9.2005 |
2005/0268/SI | Act Amending the Articles of Precious Metals Act | 9.9.2005 |
2005/0269/FIN | Government Bill to Parliament for the Fertiliser Product Act (draft) | 9.9.2005 |
2005/0270/NL | Regulation by the Secretary of State for Housing, Planning and the Environment dated {date 2005}, laying down more detailed rules regarding the exemption of plant remains and soil tare from the ban on dumping outside establishments (Plant Remains and Soil Tare Exemption Regulation) | 12.9.2005 |
2005/0271/P | Decree-Law regulating genetically modified varieties, intended to ensure coexistence with conventional crops and with organic production | 12.9.2005 |
2005/0272/E | Draft Order partially amending the Order of 20 September 1985 on the installation and approval of license plates for motor vehicles and trailers | 12.9.2005 |
2005/0273/S | The Medical Products Agency's administrative provisions on exemption from the requirement for a licence under the Act (1999:42) banning certain substances that are harmful to health (LVFS 2005:x) | 14.9.2005 |
2005/0274/S | Order amending the Order (1999:58) banning certain products that are harmful to health | 15.9.2005 |
2005/0275/SK | Slovak Republic Ministry of Agriculture Decree laying down types of fertilizers, composition, packaging and labelling of fertilizers, analytical methods for testing fertilizers, risk elements, limit values for different groups of fertilizers, permissible deviations and limit values for commercial fertilizers | 16.9.2005 |
2005/0276/NL | Decree by the Secretary of State for Economic Affairs of …, increasing the limits for hallmarking gold and silver objects | 16.9.2005 |
2005/0277/PL | Minister of Agriculture and Rural Development Implementing Regulation on Veterinary Requirements for Bee Products Placed on the Market | 16.9.2005 |
2005/0278/UK | Specification for the Reinstatement of Openings in Roads | 16.9.2005 |
2005/0279/PL | Ministerial Decree on the detailed requirements for the marking of prepackages | 19.9.2005 |
2005/0280/UK | UK Low Carbon Car programme — requirements for vehicle accreditation | 16.9.2005 |
2005/0281/I | Draft Decree of the Prime Minister on technical regulations for the creation, transmission and validation, including time-stamping, of certified electronic mail pursuant to Article 17 of Presidential Decree No 68 of 11 February 2005, on the regulation laying down provisions for the use of certified electronic mail | 19.9.2005 |
2005/0282/NL | Regulation amending certain requirements pertaining to the use of building materials on or in the ground, which apply on the basis of the Building Materials (Soil and Surface Water Protection) Decree (Exemption regulation relating to the Building Materials Decree 2005) | 19.9.2005 |
2005/0283/NL | Regulation amending the Regulation implementing the Building Materials (Soil and Surface Water Protection) Decree | 19.9.2005 |
2005/0284/PL | Draft Order of the Minister for Infrastructure on the extent of tests necessary to obtain permits for the use of certain types of structures and equipment intended for rail traffic purposes and of certain types of rail vehicles | 21.9.2005 |
2005/0285/A | Draft Order of the Federal Minister for Labour and Economic Affairs amending the Order on combustible liquids | 21.9.2005 |
2005/0286/PL | Order of the Minister for the Economy and Labour on quality requirements for liquid fuels | 21.9.2005 |
2005/0287/PL | Ministry of the Economy and Labour Decree on the minimum requirements for breaking-up equipment and methods of separating waste into material types | 22.9.2005 |
2005/0288/E | SCO Order on substances for the treatment of water intended for the production of water for human consumption | 26.9.2005 |
2005/0289/I | Draft Decree laying down standards on the definition of criteria for comparison for the purposes of the running and driving, and on the certifying of technical requirements for fitness, of touristic miniature trains | 26.9.2005 |
--------------------------------------------------
