Commission Decision
of 21 March 2001
amending Decision 94/324/EC laying down special conditions governing imports of fishery and aquaculture products originating in Indonesia
(notified under document number C(2001) 748)
(Text with EEA relevance)
(2001/254/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/493/EEC of 22 July 1991 laying down the health conditions for the production and the placing on the market of fishery products(1), as last amended by Directive 97/79/EC(2), and in particular Article 11 thereof,
Whereas:
(1) Article 1 of Commission Decision 94/324/EC of 19 May 1994 laying down special conditions governing imports of fishery and aquaculture products originating in Indonesia(3), as last amended by Decision 97/401/EC(4), states that the Ministry of Agriculture, Directorate-General of Fisheries (Provincial Laboratory for Fish Inspection and Quality Control) shall be the competent authority in Indonesia for verifying and certifying compliance of fishery and aquaculture products with the requirements of Directive 91/493/EEC.
(2) Following a restructuring of the Indonesian administration, the competent authority for health certificates for fishery products has changed to the Directorate-General of Fisheries (DGF) of the Ministry of Marine Affairs and Fisheries. This new authority is capable of effectively verifying the application of the laws in force. It is therefore necessary to modify the nomination of the competent authority mentioned in Decision 94/324/EC and the model health certificate included in Annex A to this Decision.
(3) It is convenient to harmonise the wording of Decision 94/324/EC with the wording of more recently adopted Commission Decisions, laying down special conditions governing imports of fishery and aquaculture products originating in certain third countries.
(4) The measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
Decision 94/324/EC shall be modified as follows:
1. Article 1 shall be replaced by the following:
"Article 1
The Directorate-General of Fisheries (DGF) of the Ministry of Marine Affairs and Fisheries shall be the competent authority in Indonesia for verifying and certifying compliance of fishery and aquaculture products with the requirements of Directive 91/493/EEC."
2. Article 2 shall be replaced by the following:
"Article 2
Fishery and aquaculture products originating in Indonesia must meet the following conditions:
1. each consignment must be accompanied by a numbered original health certificate, duly completed, signed, dated and comprising a single sheet in accordance with the model in Annex A hereto;
2. the products must come from approved establishments, factory vessels, cold stores or registered freezer vessels listed in Annex B hetero;
3. except in the case of frozen fishery products in bulk and intended for the manufacture of preserved foods, all packages must bear the word 'INDONESIA' and the approval/registration number of the establishment, factory vessel, cold store or freezer vessel of origin in indelible letters."
3. Article 3(2) shall be replaced by the following:
"2. Certificates must bear the name, capacity and signature of the representative of the DGF and the latter's official stamp in a colour different from that of other endorsements."
4. Annex A shall be replaced by the Annex hereto.
Article 2
This Decision shall come into effect 45 days after its publication on the Official Journal of the European Communities.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 21 March 2001.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 268, 24.9.1991, p. 15.
(2) OJ L 24, 30.1.1998, p. 31.
(3) OJ L 145, 10.6.1994, p. 23.
(4) OJ L 166, 25.6.1997, p. 14.
ANNEX
"ANNEX A
>PIC FILE= "L_2001091EN.008703.EPS">
>PIC FILE= "L_2001091EN.008801.EPS">"
