Agreement
between the European Community and the Government of Canada on the processing of Advance Passenger Information and Passenger Name Record data
THE EUROPEAN COMMUNITY AND THE GOVERNMENT OF CANADA, hereinafter referred to as the "Parties":
RECOGNISING the importance of respecting fundamental rights and freedoms, notably the right to privacy, and the importance of respecting these values while preventing and combating terrorism and related crimes and other serious crimes that are transnational in nature, including organised crime;
HAVING REGARD to the Government of Canada requirement of air carriers carrying persons to Canada to provide Advance Passenger Information and Passenger Name Record (hereinafter API/PNR) data to the competent Canadian authorities, to the extent it is collected and contained in carriers’ automated reservation systems and departure control systems (DCS);
HAVING REGARD to Directive 95/46/EC of the European Parliament and of the Council of 24 October 1995 on the protection of individuals with regard to the processing of personal data and on the free movement of such data, and in particular Article 7(c) thereof;
HAVING REGARD to the Commitments made by the relevant competent authority with regard to the way in which it will process API/PNR data received from air carriers (hereinafter the Commitments);
HAVING REGARD to the relevant Commission Decision, pursuant to Article 25(6) of Directive 95/46/EC, (hereinafter the Decision), whereby the relevant Canadian competent authority is considered as providing an adequate level of protection for API/PNR data transferred from the European Community (hereinafter the Community) concerning passenger flights to Canada, in accordance with the relevant Commitments, which are annexed to the respective Decision;
HAVING REGARD to the Revised Guidelines on API adopted by the World Customs Organisation (WCO), the International Air Transport Association (IATA) and the International Civil Aviation Organisation (ICAO);
COMMITTED to work together to assist the ICAO in the development of a multilateral standard for the transmission of PNR data obtained from commercial airlines;
HAVING REGARD to the possibility of effecting modifications to Annex I to this Agreement in the future by simplified procedures, particularly with regard to ensuring reciprocity between the Parties,
HAVE AGREED AS FOLLOWS:
Article 1
Purpose
1. The purpose of this Agreement is to ensure that API/PNR data of persons on eligible journeys is provided in full respect of fundamental rights and freedoms, in particular the right to privacy.
2. An eligible journey is a passage by an air carrier from the territory of one Party to the territory of the requesting Party.
Article 2
Competent authorities
A competent authority of a requesting Party is an authority responsible in Canada or in the European Union for processing API/PNR data of persons on eligible journeys as specified in Annex I to this Agreement, which forms an integral part thereof.
Article 3
Processing of API/PNR data
1. The Parties agree that API/PNR data of persons on eligible journeys will be processed as outlined in the Commitments made by the competent authority obtaining the API/PNR data.
2. The Commitments set forth the rules and procedures for the transmission and protection of API/PNR data of persons on eligible journeys provided to a competent authority.
3. The competent authority shall process API/PNR data received and treat persons on eligible journeys to which the API/PNR data relates in accordance with applicable laws and constitutional requirements, without discrimination, in particular on the basis of nationality and/or country of residence.
Article 4
Access, correction and notation
1. A competent authority shall afford to a person who is not present in the territory in which that authority exercises jurisdiction, to whom the API/PNR data processed pursuant to this Agreement relates, access to the data as well as the opportunity to seek correction if it is erroneous or add a notation to indicate a correction request was made.
2. The opportunity provided by the competent authority for access, correction and notation with respect to such data shall be afforded in circumstances similar to those where it would be available to persons present in the territory in which that authority exercises jurisdiction.
Article 5
Obligation to process API/PNR data
1. In relation to the application of this Agreement within the Community, as it relates to the processing of personal data, air carriers operating eligible journeys from the Community to Canada shall process API/PNR data contained in their automated reservation systems and DCS as required by the competent Canadian authorities pursuant to Canadian law. The list of PNR data elements that air carriers operating eligible journeys shall transfer to the Canadian competent authority is contained in Annex II to this Agreement, which forms an integral part thereof.
2. The obligation set forth in paragraph 1 shall only apply for as long as the Decision is applicable, ceasing to have effect on the date that the Decision is repealed, suspended or expires without being renewed.
Article 6
Joint Committee
1. A Joint Committee is hereby established, consisting of representatives of each Party, who will be notified to the other Party through diplomatic channels. The Joint Committee shall meet at a place, on a date and with an agenda fixed by mutual consent. The first meeting shall take place within six months of entry into force of this Agreement.
2. The Joint Committee shall, inter alia:
(a) act as a channel of communication with regard to the implementation of this Agreement and any matters related thereto;
(b) resolve, to the extent possible, any dispute which may arise with respect to the implementation of this Agreement and any matters related thereto;
(c) organise the Joint Reviews referred to in Article 8 and determine the detailed modalities of the joint review;
(d) adopt its rules of procedure.
3. The Parties represented in the Joint Committee may agree modifications to Annex I to this Agreement, which will apply as from the date of such agreement.
Article 7
Settlement of disputes
The Parties shall consult promptly at the request of either concerning any dispute, which has not been resolved by the Joint Committee.
Article 8
Joint reviews
In accordance with Annex III to this Agreement, which forms an integral part thereof, the Parties shall conduct on an annual basis, or as otherwise agreed, a Joint Review of the implementation of this Agreement and any matters related thereto, including developments such as the definition by the ICAO of relevant PNR guidelines.
Article 9
Entry into force, amendments to and termination of the Agreement
1. This Agreement shall enter into force following an exchange of notifications between the Parties advising that the procedures required for entry into force thereof have been completed. This Agreement shall come into force on the date of the second notification.
2. Without prejudice to Article 6(3), this Agreement may be amended by an Agreement between the Parties. Such amendment shall enter into force 90 days after the Parties have exchanged notifications of completion of the relevant internal procedures.
3. This Agreement may be terminated by either Party at any time following written notification not less than 90 days in advance of the proposed termination date.
Article 10
This Agreement is not intended to derogate from or amend legislation of the Parties.
IN WITNESS WHEREOF the undersigned, being duly authorised thereto, have signed this Agreement.
DONE, in duplicate, in Luxembourg, this third day of October two thousand and five, in the Czech, Danish, Dutch, English, Estonian, Finnish, French, German, Greek, Hungarian, Italian, Latvian, Lithuanian, Maltese, Polish, Portuguese, Slovak, Slovenian, Spanish and Swedish languages, each version being equally authentic. In case of divergence the English and French versions shall prevail.
Por la Comunidad Europea
Za Evropské společenství
For Det Europæiske Fællesskab
Für die Europäische Gemeinschaft
Euroopa Ühenduse nimel
Για την Ευρωπαϊκή Κοινότητα
For the European Community
Pour la Communauté européenne
Per la Comunità europea
Eiropas Kopienas vārdā
Europos bendrijos vardu
az Európai Közösség részéről
Għall-Komunità Ewropea
Voor de Europese Gemeenschap
W imieniu Wspólnoty Europejskiej
Pela Comunidade Europeia
Za Európske spoločenstvo
za Evropsko skupnost
Euroopan yhteisön puolesta
På Europeiska gemenskapens vägnar
+++++ TIFF +++++
+++++ TIFF +++++
Por el Gobierno de Canadá
Za vládu Kanady
For Canadas regering
Für die Regierung Kanadas
Kanada valitsuse nimel
Για την Κυβέρνηση του Καναδά
For the Government of Canada
Pour le gouvernement du Canada
Per il governo del Canada
Kanādas Valdības vārdā
Kanados Vyriausybės vardu
Kanada kormánya részéről
Għall-Gvern tal-Kanada
Voor de Regering van Canada
W imieniu rządu Kanady
Pelo Governo do Canadá
Za vládu Kanady
Za Vlado Kanade
Kanadan hallituksen puolesta
På Canadas regerings vägnar
+++++ TIFF +++++
+++++ TIFF +++++
--------------------------------------------------
ANNEX I
Competent authorities
For the purpose of Article 3, the competent authority for Canada is the Canada Border Services Agency (CBSA).
--------------------------------------------------
ANNEX II
PNR data elements to be collected
1. PNR record locator
2. Date of reservation
3. Date(s) of intended travel
4. Name
5. Other names on PNR
6. All forms of payment information
7. Billing address
8. Contact telephone numbers
9. All travel itinerary for specific PNR
10. Frequent flyer information (limited to miles flown and address(es))
11. Travel agency
12. Travel agent
13. Split/divided PNR information
14. Ticketing field information
15. Ticket number
16. Seat number
17. Date of ticket issuance
18. No show history
19. Bag tag numbers
20. Go show information
21. Seat information
22. One-way tickets
23. Any collected APIS information
24. Standby
25. Order at check in
--------------------------------------------------
ANNEX III
Joint review
The Parties will communicate to each other in advance of the joint review the composition of their respective teams, which may include appropriate authorities concerned with privacy/data protection, customs, immigration, enforcement, intelligence and interdiction, and other forms of law enforcement, border security and/or aviation security, including experts from Member States of the European Union.
Subject to applicable laws, any participants in the review will be required to respect the confidentiality of the discussions and have appropriate security clearances. Confidentiality will not however be an obstacle to each Party making an appropriate report on the results of the joint review to their respective competent bodies, including the Parliament of Canada and the European Parliament.
The Parties will jointly determine the detailed modalities of the joint review.
--------------------------------------------------
