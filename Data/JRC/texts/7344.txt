Decision of the EEA Joint Committee No 54/2005
of 29 April 2005
amending Annex II (Technical regulations, standards, testing and certification) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex II to the Agreement was amended by Decision of the EEA Joint Committee No 43/2005 of 11 March 2005 [1].
(2) Commission Directive 2004/95/EC of 24 September 2004 amending Council Directive 90/642/EEC as regards the maximum residues levels of bifenthrin and famoxadone fixed therein [2] is to be incorporated into the Agreement.
(3) Commission Decision 2004/691/EC of 7 October 2004 amending Decision 2002/840/EC adopting the list of approved facilities in third countries for the irradiation of foods [3] is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
Chapter XII of Annex II to the Agreement shall be amended as follows:
1. the following indent shall be added in point 54 (Council Directive 90/642/EEC):
"— 32004 L 0095: Commission Directive 2004/95/EC of 24 September 2004 (OJ L 301, 28.9.2004, p. 42).";
2. the following shall be added in point 54zze (Commission Decision 2002/840/EC):
", as amended by:
- 32004 D 0691: Commission Decision 2004/691/EC of 7 October 2004 (OJ L 314, 13.10.2004, p. 14)."
Article 2
The texts of Directive 2004/95/EC and Decision 2004/691/EC in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 30 April 2005, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [4].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 29 April 2005.
For the EEA Joint Committee
The President
Richard Wright
[1] OJ L 198, 28.7.2005, p. 45.
[2] OJ L 301, 28.9.2004, p. 42.
[3] OJ L 314, 13.10.2004, p. 14.
[4] No constitutional requirements indicated.
--------------------------------------------------
