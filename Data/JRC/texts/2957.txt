Commission Regulation (EC) No 1417/2005
of 29 August 2005
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 30 August 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 29 August 2005.
For the Commission
J. M. Silva Rodríguez
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 1947/2002 (OJ L 299, 1.11.2002, p. 17).
--------------------------------------------------
ANNEX
to Commission Regulation of 29 August 2005 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code | Standard import value |
07020000 | 052 | 65,0 |
096 | 17,5 |
999 | 41,3 |
07070005 | 052 | 72,1 |
999 | 72,1 |
07099070 | 052 | 44,5 |
999 | 44,5 |
08055010 | 382 | 61,0 |
388 | 68,1 |
524 | 66,8 |
528 | 65,1 |
999 | 65,3 |
08061010 | 052 | 87,6 |
220 | 156,4 |
400 | 195,8 |
512 | 89,9 |
624 | 160,8 |
999 | 138,1 |
08081080 | 388 | 51,6 |
400 | 69,1 |
508 | 42,3 |
512 | 37,2 |
528 | 49,8 |
720 | 30,6 |
804 | 58,2 |
999 | 48,4 |
08082050 | 052 | 91,1 |
388 | 17,0 |
512 | 11,4 |
528 | 23,7 |
624 | 114,6 |
999 | 51,6 |
08093010, 08093090 | 052 | 89,4 |
999 | 89,4 |
08094005 | 052 | 136,1 |
093 | 49,2 |
098 | 53,9 |
624 | 113,0 |
999 | 88,1 |
[1] Country nomenclature as fixed by Commission Regulation (EC) No 750/2005 (OJ L 126, 19.5.2005, p. 12). Code "999" stands for "of other origin".
--------------------------------------------------
