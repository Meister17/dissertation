Action brought on 19 September 2005 by the Commission of the European Communities against the Republic of Finland
An action against the Republic of Finland was brought before the Court of Justice of the European Communities on 19 September 2005 by the Commission of the European Communities, represented by M. van Beek and I. Koskinen, acting as Agent, with an address for service in Luxembourg.
The Commission claims that the Court should:
1. declare that, by regularly permitting the hunting of wolves contrary to the principles for derogations laid down in Article 16(1) of Council Directive 92/43/EEC [1] of 21 May 1992 on the conservation of natural habitats and of wild fauna and flora, the Republic of Finland has failed to fulfil its obligations under Articles 12(1) and 16(1) of the directive;
2. order the Republic of Finland to pay the costs.
Pleas in law and main arguments
Article 16 of Directive 92/43/EEC is an exception to the system of the strict protection of species in Article 12, so that it must be interpreted strictly. Article 12(1) lays down two preconditions for derogating on the basis of points (a) to (e). First, the derogation must not be detrimental to the maintenance of the populations of the species concerned at a favourable conservation status in their natural range. Second, a derogation is possible only where there is no other satisfactory solution.
Since the level of protection of the wolf is not favourable in Finland and other alternative methods are available, and since permits for hunting wolves are regularly issued without there being a properly ascertained connection with individuals causing particularly significant damage, the hunting of wolves is permitted in Finland to an extent which exceeds the conditions laid down in Article 16(1) of Directive 92/43/EEC.
[1] OJ L 206 of 22.7.1992, p. 7.
--------------------------------------------------
