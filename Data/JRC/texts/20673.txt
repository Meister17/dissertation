DECISION OF THE EXECUTIVE COMMITTEE
of 26 April 1994
on adjustment measures aiming to remove the obstacles to, and restrictions on, traffic at road crossing points at internal borders
(SCH/Com-ex (94)1 rev. 2)
THE EXECUTIVE COMMITTEE,
Having regard to Article 132 of the Convention implementing the Schengen Agreement,
Having regard to Article 2 of the abovementioned Convention,
Hereby acknowledges and approves the document on the abolition of checks on persons at the internal borders (SCH/I-Front (94)1 rev. 3) referred to in it,
HAS DECIDED AS FOLLOWS:
The adjustment measures aiming to remove obstacles to, and restrictions on, traffic at road crossing points at internal borders shall be implemented in accordance with the document attached hereto. The implementation of these adjustment measures falls within the national competence of the Contracting Parties.
Bonn, 26 April 1994.
The Chairman
Bernd Schmidbauer
ADJUSTMENT MEASURES AIMING TO REMOVING OBSTACLES TO, AND RESTRICTIONS ON, TRAFFIC AT ROAD CROSSING POINTS OF INTERNAL BORDERS
Completing the abolition of checks at the internal borders between the Schengen States also involves the removal of the obstacles restricting travel, in particular those constituted by the installations formerly required for checks.
The Contracting Parties therefore intend to begin immediately dismantling these obstacles progressively as soon as positive indications as to the prospects of bringing the Schengen Information System into operation have been received.
Phase 1 will consist of measures which are particularly necessary to guarantee that the internal borders may be crossed swiftly and which may be implemented relatively quickly without incurring unreasonable expense.
To this end the following measures, in particular, must be taken:
- opening carriageways and lanes previously blocked for border checks, particularly at motorway border crossing points,
- removing customs sheds from central lanes so as not to hinder vehicles passing at high speed,
- dismantling roofs jutting out over the road at border crossing points to improve visibility and reduce nuisance caused by air pressure changes,
- removing speed limits; the sole criteria for introducing any new speed limits is road safety,
- technical adjustments in order to enable vehicles to turn round on motorways and comparable roads in case checks at the internal borders are temporarily reinstated on grounds of public policy or national security and vehicles are refused entry.
The implementation of these programmes shall be the national responsibility of each Contracting Party and, if necessary or expedient from a legal or a practical point of view, by mutual consultation or agreement.
As many as possible of the measures referred to in Phase 1 should be completed by the time the Convention implementing the Schengen Agreement is brought into force, or at least those which can be implemented quickly without any lengthy preparations, (for example the removal of road barriers).
Preparations for the remaining adjustment measures in Phase 1 shall be made in accordance with the following timetable:
1. During the period from 1 July to 15 September 1994, a review of the situation is to be made and a list drawn up of the steps to be taken at each internal border crossing point with a view to implementing the adjustment measures.
It should be borne in mind that installations for police cooperation are to be kept provisionally at certain border crossing points which means, for instance, that the speed limits should be retained for access roads to the services concerned.
2. The period from 15 September to 31 October 1994 is the phase for consultation between neighbouring States. During this period, they are to inform each other of the activities required to put into practice the adjustment measures at each border crossing point.
3. The Contracting Parties shall ensure that the necessary arrangements for bringing into force the Convention implementing the Schengen Agreement are completed by 31 December 1994.
At the end of each of the three preparatory phases, the Contracting Parties shall report to the Central Group on progress made towards completing the timetable, whilst providing detailed overviews.
ABOLITION OF CHECKS ON PERSONS AT THE INTERNAL BORDERS
SCH/I-Front (94) 1 rev. 3
After years of intensive preparation, almost all the various compensatory measures provided for by the Schengen Convention of 19 June 1990 have been fully implemented, and the work that remains to be done in this area, for example on the Schengen Information System, is currently proceeding at a steady pace.
However, certain arrangements that were designed to achieve the objective which these compensatory measures were supposed to permit, i.e. the abolition of checks on persons at the internal borders, are still not in place. The avoidance of any further delay in abolishing such checks at the common borders even after all the compensatory measures have been implemented is contingent on the necessary preconditions being fulfilled. In this connection it is important not only to dispense with the formalities that accompany such checks but to remove the obstacles to freedom of movement at the internal borders that were previously necessary for the purpose of carrying out controls.
In order to ensure that this operation proceeds in an orderly fashion, there must be a concrete action programme for implementing the various stages.
1. Abolition of checks on persons and of the requirement to present cross-border travel documents at land borders, ports and airports
1.1. Abolition of checks on persons
Under Article 2(1) of the Schengen Convention, the internal borders may be crossed at any point without checks on persons being carried out.
Consequently, the competent authorities should dispense with internal border checks except in the cases provided for in Article 2(2) of the Convention.
On the other hand, this would have the effect of exempting all travellers, whatever their nationality, from police checks when crossing the border. Furthermore, they would no longer be obliged to use authorised border crossing points.
The abolition of checks on persons at the internal borders is without prejudice to the provisions of Article 22 of the Schengen Convention. Nor does it affect the exercise by the competent authorities of a Contracting Party, on its national territory, including the border areas, of their powers under national law for the purpose of monitoring compliance with the obligation to possess, carry and present papers and travel documents.
Alternative border checks are incompatible with the provisions of the Schengen Convention on the abolition of controls. Here, however, they are taken to mean systematic checks on persons carried out for the purpose of crossing the border, in a hinterland area adjacent to the border or in defined border areas. This does not contravene the provisions of Article 2(2).
Persons entering via airports and seaports are exempt from checks only if they cross a common border, for example, in the case of air and sea travel inside the Schengen area. Since passengers are channelled into Schengen and non-Schengen flows, checks cannot in practice be abolished for air and sea travel unless airports and seaports have the requisite separate handling areas.
The Contracting Parties should take appropriate steps to inform:
- the public,
- the authorities responsible for border protection,
- the police, and
- the airport and seaport operators and the carriers,
of the various features of the new situation at the internal borders with regard to the abolition of checks.
1.2. Abolition of the requirement to present cross-border travel documents when crossing internal borders
The abolition of border controls goes hand in hand with abolition of the requirement to produce or present a valid document that permits the holder to cross borders.
This does not conflict with national provisions on holding, carrying and presenting identity and residence papers that are applicable on the national territory.
Delegations were to communicate the relevant national provisions before the end of April 1994 and provide details of the identity documents used in their countries and of the checks provided for under their national law.
2. Adjustment measures aimed at eliminating obstacles to and restrictions on movement at road crossing points
The abolition of checks on persons is the main instrument for achieving freedom of movement at the borders. To attain this objective, however, it is necessary to remove infrastructures the existence of which was hitherto justified by the need to conduct checks and which restrict freedom of movement.
2.1. General observations
(a) Crossing points at internal borders - the largest and most important at any rate - consist of numerous buildings and facilities that form a complex.
In the initial phase, the activities undertaken by the Contracting Parties for the purpose of complementing the abolition of checks by accompanying measures will consist solely of steps aimed at guaranteeing that there are no obstacles to crossing the borders.
The question of dismantling other buildings or putting them to a different use, collection of the barriers used previously and important tasks such as rectifying the layout, will be tackled during the second stage.
(b) Most of the Schengen States have concluded bilateral agreements with neighbouring States determining that checks on cross-border traffic may take the form of joint checks carried out on the territory of either State. Accordingly, the implementing agreements provided for checks are to be combined at virtually all crossing points. Consequently, adaptations for the purpose of implementing the Schengen system presuppose the agreement of the two neighbouring States in question. The Contracting Parties should reach such agreement quickly and inform the Executive Committee accordingly.
2.2. Infrastructural modifications of a technical nature
In the first instance, the following measures in particular should be prepared:
- at a number of crossing points, especially on motorways, provision has been made for traffic lanes for vehicles but these are blocked by security ramps or crash barriers. These lanes can be opened to traffic and measures to this end should be taken as soon as possible,
- the control booths erected in the middle lanes, particularly on motorways, are too close to the traffic lane and constitute a safety hazard for vehicles travelling at high speed. They should therefore be demolished,
- if vehicles are allowed to travel at higher speeds, the roofs which overhang the road at border crossing points should be demolished in order to improve visibility and reduce the nuisance caused by air currents.
2.3. Abolition of traffic restrictions
Once the necessary tasks have been completed, it will be possible to abolish the speed limits currently in force. The introduction of new speed limits will be dictated solely by road safety requirements.
2.4. Arrangements for the temporary reintroduction of checks at internal borders
Should it prove necessary to reintroduce internal border checks temporarily in accordance with the provisions of Article 2(2) of the Convention, mobile units of the border police can impose the speed restrictions necessary for conducting checks by means of ad hoc signalling. This obviates the need to install permanent signs for this purpose.
However, in order to enforce a decision to refuse entry, provision needs to be made for installing a device that enables traffic to reverse. Steps therefore need to be taken to ensure that movable components, capable of being opened immediately, if necessary are installed in the security barriers located in the middle of motorways and similar roads.
2.5. Action programmes
With a view to the practical implementation of the Schengen Convention, the Contracting Parties should draft detailed programmes for the first phase of infrastructural adaptation at the border crossing points and submit them to the Executive Committee.
The following measures in particular must be taken in due course for the purpose of bringing the Convention into force:
- road lanes, especially those at motorway crossing points that have up to now been closed for the purpose of conducting border checks, should be opened to traffic,
- the control booths situated on the middle lanes should be demolished so as not to hamper vehicles travelling past at high speed,
- the roofs overhanging the road at the border crossing points should be removed to improve visibility and reduce the nuisance caused by air currents,
- speed limits should be abolished, and the introduction of new restrictions should be determined solely by road safety considerations,
- technical modifications should be carried out to enable vehicles to make a U-turn on motorways and similar roads in the event that internal border checks are temporarily reintroduced on public order or national security grounds.
These programmes are the responsibility of each Contracting Party and should be implemented jointly or by mutual agreement, in so far as circumstances or legal considerations make this necessary or advisable. The Contracting Parties should report to the General Secretariat on the measures they have taken.
3. Information on the abolition of checks prior to the implementation of the Schengen Convention
The strategy underlying the Schengen Convention is that the abolition of internal border checks should be preceded by the introduction of compensatory measures.
One of the principal compensatory measures, the Schengen Information System, has yet to be completed, which means that checks on persons must in principle be maintained at the internal borders.
In the opinion of the Contracting Parties, internal border checks may, where appropriate, be discontinued by bilateral agreement in some cases, as a symbolic gesture and for a trial period, provided that security is not seriously undermined (pilot projects).
If the Contracting Parties envisage abolishing checks before the compensatory measures are in place, they should notify the Executive Committee accordingly.
4. Consultation in the event that alternative border checks are carried out
Article 2(2) of the Convention lays down that a Contracting Party which decides to carry out national border checks for a limited period on public order or national security grounds should consult the other Contracting Parties in advance.
Given the substance and purpose of the above provision, this requirement also applies in the case of alternative border checks carried out in a hinterland area adjacent to the border or in defined border areas (see point 1.1.).
If a Contracting Party is planning to take such a measure, it should proceed as in the case of checks carried out for a temporary period directly at the internal borders and inform the other Partners accordingly.
