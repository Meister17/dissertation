State aid — Sweden
State aid C 35/2006 (ex NN 37/2006) — Sale of land below market price
Invitation to submit comments pursuant to Article 88(2) of the EC Treaty
(2006/C 204/03)
(Text with EEA relevance)
By means of the letter dated 19 July 2006 reproduced in the authentic language on the pages following this summary, the Commission notified Sweden of its decision to initiate the procedure laid down in Article 88(2) of the EC Treaty concerning the above-mentioned aid.
Interested parties may submit their comments on the aid in respect of which the Commission is initiating the procedure within one month of the date of publication of this summary and the following letter, to:
European Commission
Directorate-General for Competition
State aid Registry
Rue de la Loi/Wetstraat, 200
B-1049 Brussels
Fax No: (32-2) 296 12 42
These comments will be communicated to Sweden. Confidential treatment of the identity of the interested party submitting the comments may be requested in writing, stating the reasons for the request.
SUMMARY
On 14 November 2005 the Commission was informed by an interested party about the sale allegedly below market price of a plot of land by the Municipality of Are to Konsum Jamtland ekonomisk forening, a cooperative society which sells consumer goods.
The complainant claims that Lidl Sverige KB, a direct competitor of the buyer of the land in the food and groceries retailing sector, made a formal offer of SEK 6,6 million for the same land, while the actual sale price was SEK 2 million. The alleged aid amount is therefore SEK 4,6 million (currently EUR 0,5 million).
The Commission's preliminary view is that the transaction involves state aid in the meaning of article 87(1) of the EC Treaty. The offer made by Lidl seems to be directly comparable to the actual price of the transaction. The relevant market is open to intra-Community trade. The land sale does not seem to comply with the provisions of the Commission Communication on state aid elements in sales of land and buildings by public authorities [1], namely because the sale was not conducted through an unconditional bidding procedure and there is no formal evidence of an independent expert evaluation of the land.
Since the transaction was not notified to the Commission, it comes to the preliminary conclusion that Sweden failed to comply with its obligation, under Article 88(3) of the EC Treaty, to inform the Commission, in sufficient time to enable it to submit its comments, of any plans to grant or alter aid. The measure has accordingly been treated as an unlawful aid.
The Commission has serious doubts about the compatibility of such state aid with the EC Treaty. The measure cannot be considered as aid for initial investment under the Guidelines on national regional aid [2]. It seems to constitute operating aid which is not allowed in assisted areas under article 87(3)(c) of the EC Treaty, according to the same Guidelines. At this stage, the Commission considers that there is no further legal basis for exempting this measure from the incompatibility principle.
In accordance with Article 14 of Council Regulation (EC) No 659/1999, all unlawful aid can be subject to recovery from the recipient.
TEXT OF LETTER
"Kommissionen önskar informera Sverige om att den, efter att ha granskat uppgifter som tillhandahållits av Era myndigheter om den ovannämnda stödåtgärden, har beslutat inleda det förfarande som anges i artikel 88.2 i EG-fördraget.
1. FÖRFARANDE
1. Genom ett klagomål som registrerades den 14 november 2005 informerade Stiftelsen den nya välfärden (the New Welfare Foundation) kommissionen om kommunen Åres försäljning av tomtmark till Konsum Jämtland ekonomisk förening (nedan kallat Konsum) som påstods omfatta olagligt statligt stöd.
2. Genom två skrivelser av den 3 januari 2006 begärde kommissionen kompletterande upplysningar om transaktionen från de svenska myndigheterna respektive klaganden. Klaganden svarade genom en skrivelse av den 31 januari 2006 (som registrerades den 2 februari 2006) och de svenska myndigheterna svarade genom skrivelser av den 2 mars 2006 och den 28 mars 2006 (som registrerades den 3 mars 2006 respektive den 29 mars 2006).
2. BESKRIVNING AV STÖDÅTGÄRDEN
Påstådd stödmottagare
3. Konsum är en kooperativ förening som säljer konsumentvaror, inklusive livsmedel och dagligvaror, i hela Jämtland. Under 2004 hade Konsum en omsättning på 260 miljoner kronor (nuvärde 28 miljoner euro).
4. Den 1 januari 2006 slogs Konsum Jämtland ekonomisk förening ihop med Konsum Nord ekonomisk förening. Båda föreningarna samarbetar med Kooperativa Förbundet (nedan kallat KF), som är en sammanslutning av svenska kooperativa föreningar. KF är moderbolag till koncernen KF som bland annat äger norska detaljhandelsföretag och som under 2004 hade en omsättning på ungefär 29 miljarder kronor (nuvärde 3 miljarder euro).
Den klagande
5. Klaganden, Stiftelsen den nya välfärden, är en organisation som arbetar för företagande och som finansieras genom bidrag från ett stort antal företag. Stiftelsens mål är att verka för bättre näringslivsförutsättningar, huvudsakligen genom Företagarombudsmannen.
6. Detta klagomål har lämnats för en av dess medlemmars räkning, Lidl Sverige KB (nedan kallat Lidl). Lidl var den första utländska aktören inom livsmedelssektorn att träda in på den svenska marknaden med en egen distributionskanal och företaget är en direkt konkurrent till Konsum inom detaljhandelssektorn för livsmedel och dagligvaror.
Påstått statligt stöd
7. Den 15 oktober 2005 beslutade kommunstyrelsen i Åre att sälja tomtmark i Produkthusområdet (Åre Prästbord 1:30, 1:68 och 1:69) till Konsum som påstås ligga under marknadspris. Försäljningen har inte föregåtts av något anbudsförfarande.
8. Priset för transaktionen var 2 miljoner kronor (nuvärde 215314 euro). Genom ett e-postmeddelande av den 23 augusti 2005 inkom Lidl med ett formellt anbud på 6,6 miljoner kronor (nuvärde 710602 euro) för samma mark. Anbudet offentliggjordes i lokalmedierna. Det påstådda stödet uppgår därmed till 4,6 miljoner kronor (nuvärde 495268 euro).
9. Efter anbudet från Lidl ökade försäljningspriset successivt från 1 krona till 1 miljon kronor och slutligen till 2 miljoner kronor.
10. Transaktionen har överklagats och hänskjutits till länsrätten i Jämtlands län.
De svenska myndigheternas argument
11. Enligt de svenska myndigheterna är inte kommunen rättsligt skyldig att inleda ett anbudsförfarande vid försäljning av mark.
12. Försäljningspriset fastställdes på grundval av informella kontakter med lokala experter och en angränsande kommun och på en värdering av Ernst & Young Real Estate. Inga formella bevis på någon oberoende värdering av marken har dock lämnats.
13. De svenska myndigheterna menade att försäljningen till Konsum var ett led i ett antal marktransaktioner som bland annat omfattade Konsums försäljning av mark i ett annat område i Åre som kommunen hade för avsikt att använda för andra utvecklingsändamål. Genom försäljningen av marken flyttade Konsum sin verksamhet och gjorde det därmed möjligt för kommunen att uppnå sin målsättning. Om Lidls anbud hade godtagits skulle inte kommunen ha kunnat fullfölja den nya utvecklingsplanen, då Konsum skulle ha fortsatt sin verksamhet i de gamla lokalerna. I köpekontraktet nämns ingen annan försäljning av mark och inte heller värdet på den mark Konsum sålde i det andra området i Åre.
Den klagandes argument
14. Enligt klaganden var inte Lidls anbud kopplat till några särskilda villkor. Kommunen skulle därmed ha kunnat ingå ett köpeavtal med Lidl som helt motsvarar det som ingicks med Konsum. Därmed är de två anbuden direkt jämförbara.
15. De systematiska prishöjningarna till följd av Lidls anbud visar att kommunens avsikt varit att sälja marken under marknadsvärdet.
16. Klaganden hävdade vidare att kommunen inte erbjudit Lidl någon alternativ lokalisering och därför utgjorde försäljningen till Konsum ett etableringshinder för Lidl.
3. BEDÖMNING AV STÖDÅTGÄRDEN
Statligt stöd enligt artikel 87.1
17. På grundval av de uppgifter som lämnats kan inte förekomsten av statligt stöd i enlighet med innebörden i artikel 87.1 uteslutas.
Användning av offentliga medel
18. På grundval av tillgängliga uppgifter verkar det troligt att det pris Konsum betalade för marken låg under marknadsvärdet och att offentliga medel därmed har använts.
19. Det påstådda stödbeloppet på 4,6 miljoner kronor (nuvärde 495268 euro) överskrider det tak på 100000 euro som fastställs i kommissionens förordning nr 69/2001 av den 12 januari 2001 om tillämpningen av artiklarna 87 och 88 i EG-fördraget på stöd av mindre betydelse [3].
20. Anbuden från Konsum och Lidl verkar vara direkt jämförbara och inga särskilda villkor verkar vara kopplade till Lidls anbud. Höjningarna av försäljningspriset efter det att Lidl tillkännagav sitt anbud stödjer denna preliminära slutsats.
Gynnande av vissa företag
21. Om statliga medel förekommer finns det inga tvivel på att de gynnar ett visst företag.
Snedvridning av konkurrensen och påverkan på handeln mellan medlemsstaterna
22. Om statliga medel ingår påverkas en stor internationell aktör – Lidl – då en direkt konkurrent tagit emot offentliga medel som Lidl inte haft tillgång till.
23. Konsum verkar vara ett i huvudsak regionalt företag, men det är också kopplat till KF, en stor sammanslutning av svenska kooperativa föreningar, som också bedriver verksamhet i Norge.
24. Den berörda sektorn, detaljhandelsförsäljning av livsmedel och dagligvaror, är öppen för handel inom gemenskapen.
25. Då en internationellt aktiv konkurrent i en sektor som är öppen för handel inom gemenskapen har påverkats, kan det inte uteslutas att också handeln mellan medlemsstaterna påverkas.
26. Försäljningen av marken har inte gjorts i enlighet med bestämmelserna i kommissionens meddelande om inslag av stöd vid statliga myndigheters försäljning av mark och byggnader [4] (nedan kallat meddelandet) av följande orsaker:
- Försäljningen utfördes inte genom ett villkorslöst anbudsförfarande.
- De svenska myndigheterna har inte lämnat bevis på att en oberoende expertvärdering av marken gjorts i enlighet med meddelandet.
- Trots att kommunen gjorde en vinst vid försäljningen förvärvades marken redan under 1998, och inköpspriset kan inte betraktas som en giltig indikator när det gäller marknadsvärdet för marken under 2005 i enlighet med innebörden i meddelandet.
Kravet på anmälan
27. Försäljningen av marken godkändes av kommunstyrelsen i Åre den 5 oktober 2005. Försäljningen anmäldes inte till kommissionen.
28. Därför anser kommissionen preliminärt att Sverige underlåtit att uppfylla sin skyldighet att, i enlighet med artikel 88.3 i EG-fördraget, i god tid informera kommissionen så att den kan lämna synpunkter på eventuella planer på att bevilja eller ändra stöd. Stödåtgärden måste därför behandlas som olagligt stöd.
Förenlighet
29. Om det bekräftas att statligt stöd ingår förefaller detta stöd vara oförenligt med fördraget, av de orsaker som anges nedan.
30. Transaktionen utfördes i ett stödberättigat område enligt artikel 87.3 c i EG-fördraget. Försäljning av mark till ett förmånligt pris kan i princip betraktas som förenligt med investeringsstöd enligt riktlinjerna för statligt stöd för regionala ändamål (nedan kallade riktlinjerna) [5]. Det är dock tydligt att denna stödåtgärd inte ingår som en del i en regional stödordning. Riktlinjerna omfattar normalt inte sådana särskilda åtgärder. Stödåtgärden förefaller inte heller relatera till någon ny investering eller bidra till regional utveckling, då den bara inbegriper en omlokalisering av en befintlig verksamhet inom regionen. Slutligen verkar inte stimulanskriteriet vara uppfyllt, då stödmottagaren inte lämnat någon formell ansökan om stöd och en liknande investering skulle ha kunnat genomföras utan stöd (av Lidl).
31. Försäljningen av marken förefaller utgöra driftstöd som, i enlighet med punkt 4.15 i riktlinjerna, inte är tillåtet i stödberättigade områden i enlighet med artikel 87.3 i EG-fördraget.
32. I detta skede anser kommissionen att det inte finns någon ytterligare rättslig grund för att undanta denna stödåtgärd från principen om stödets oförenlighet.
Slutsats
33. Kommissionen anser att det är sannolikt att det vid försäljningen av marken förekom statligt stöd enligt artikel 87.1 i EG-fördraget. Kommissionen hyser också allvarliga tvivel på om det eventuella statliga stödet är förenligt med EG-fördraget.
4. BESLUT
34. Detta beslut skall betraktas som ett beslut om att inleda det formella granskningsförfarandet i enlighet med artikel 88.2 i EG-fördraget och rådets förordning (EG) nr 659/1999 [6]. Kommissionen uppmanar Sverige att, inom ramen för förfarandet i artikel 88.2 i EG-fördraget, inom en månad från mottagandet av denna skrivelse inkomma med synpunkter och tillhandahålla alla upplysningar som kan bidra till undersökningen av åtgärden. Upplysningarna skall särskilt innehålla följande:
- Om tillgängligt, formella bevis på en oberoende värdering av marken som visar att transaktionspriset låg i linje med marknadspraxis.
- Om tillgängligt, formella bevis som visar att transaktionen ingick som ett led i ett antal marktransaktioner som tillsammans var finansiellt neutrala för Åre kommun.
- Motivering till transaktionens förenlighet med EU:s regler om statligt stöd (särskilt reglerna om statligt regionalstöd).
- En förklaring av eventuella kvalitativa skillnader mellan Lidls anbud och det köpeavtal som ingicks med Konsum.
35. Kommissionen anmodar Sverige att omgående vidarebefordra en kopia av denna skrivelse till Konsum.
36. Kommissionen påminner Sverige om att artikel 88.3 i EG-fördraget har uppskjutande verkan och hänvisar till artikel 14 i rådets förordning (EG) nr 659/1999, som föreskriver att allt olagligt stöd kan återkrävas från mottagaren.
37. Kommissionen meddelar Sverige att den kommer att underrätta alla berörda parter genom att offentliggöra denna skrivelse och en sammanfattning av den i Europeiska unionens officiella tidning. Kommissionen kommer även att underrätta berörda parter i de Eftaländer som är avtalsslutande parter i EES-avtalet genom att offentliggöra ett tillkännagivande i EES-supplementet till EUT, samt Eftas övervakningsmyndighet genom att skicka en kopia av denna skrivelse. Samtliga berörda parter uppmanas att inkomma med synpunkter inom en månad från dagen för offentliggörandet."
[1] OJ C 209, 10.7.1997, p. 3.
[2] OJ C 74, 10.3.1998, p. 9.
[3] EGT L 10, 13.1.2001, s. 30.
[4] EGT C 209, 10.7.1997, s. 3.
[5] EGT C 74, 10.3.1998, s. 9.
[6] EUT L 83, 27.3.1999, s. 1.
--------------------------------------------------
