Commission Regulation (EC) No 631/2005
of 26 April 2005
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 27 April 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 26 April 2005.
For the Commission
J. M. Silva Rodríguez
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 1947/2002 (OJ L 299, 1.11.2002, p. 17).
--------------------------------------------------
ANNEX
to Commission Regulation of 26 April 2005 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code | Standard import value |
07020000 | 052 | 103,6 |
204 | 98,1 |
212 | 129,8 |
624 | 168,0 |
999 | 124,9 |
07070005 | 052 | 152,4 |
204 | 76,2 |
999 | 114,3 |
07099070 | 052 | 99,0 |
204 | 44,2 |
999 | 71,6 |
08051020 | 052 | 54,4 |
204 | 45,2 |
212 | 58,1 |
220 | 47,8 |
388 | 62,0 |
400 | 53,3 |
624 | 73,9 |
999 | 56,4 |
08055010 | 052 | 65,2 |
220 | 65,0 |
388 | 67,8 |
400 | 69,6 |
528 | 65,2 |
624 | 67,3 |
999 | 66,7 |
08081080 | 388 | 86,3 |
400 | 122,8 |
404 | 94,3 |
508 | 67,2 |
512 | 69,4 |
524 | 65,9 |
528 | 65,1 |
720 | 82,6 |
804 | 112,9 |
999 | 85,2 |
08082050 | 388 | 87,9 |
512 | 63,9 |
528 | 65,2 |
720 | 72,2 |
999 | 72,3 |
--------------------------------------------------
