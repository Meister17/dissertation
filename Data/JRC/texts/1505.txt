Commission Decision
of 14 November 2001
amending Decision 2001/296/EC as regards the list of approved laboratories for checking the effectiveness of vaccination against rabies in certain domestic carnivores
(notified under document number C(2001) 3478)
(Text with EEA relevance)
(2001/808/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Decision 2000/258/EC of 20 March 2000 designating a specific institute responsible for establishing the criteria necessary for standardising the serological tests to monitor the effectiveness of rabies vaccines(1), and in particular Article 3 thereof,
Whereas:
(1) Council Directive 92/65/EEC of 13 July 1992 laying down animal health requirements governing trade in and imports into the Community of animals, semen, ova and embryos not subject to animal health requirements laid down in specific Community rules referred to in Annex A (I) to Directive 90/425/EEC(2), as last amended by Commission Decision 2001/298/EC(3), provides for an alternative system to quarantine for the entry of certain domestic carnivores into the territory of certain Member States free from rabies. That system requires checks performed by officially approved laboratories on the effectiveness of the vaccination by titration of antibodies.
(2) Pursuant to Decision 2000/258/EC the AFSSA Laboratory, Nancy, France, was designated as the institute responsible for the proficiency tests necessary to the approval of the laboratories willing to perform these checks.
(3) Commission Decision 2001/296/EC of 29 March 2001 authorising laboratories to check the effectiveness of vaccination against rabies in certain domestic carnivores(4) established a list of approved laboratories in the Member States.
(4) Following the request of the United Kingdom and on the basis of the favourable result of the proficiency test performed by the AFSSA Laboratory, Nancy, it is appropriate to add a new laboratory to that list for the United Kingdom.
(5) The measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
In the Annex to Decision 2001/296/EC the list concerning the United Kingdom is amended as follows:
The present text becomes point 1 and the following point 2 is added: "2. Biobest Pentland Science Park
Bush Loan
Penicuik Midlothian EH26OPZ United Kingdom."
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 14 November 2001.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 79, 30.3.2000, p. 40.
(2) OJ L 268, 14.9.1992, p. 54.
(3) OJ L 102, 12.4.2001, p. 63.
(4) OJ L 102, 12.4.2001, p. 58.
