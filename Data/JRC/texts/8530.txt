COMMISSION REGULATION (EC) No 936/1999
of 27 April 1999
amending or repealing certain regulations on the classification of goods in the Combined Nomenclature
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2658/87 of 23 July 1987 on the tariff and statistical nomenclature and on the Common Customs Tariff(1), as last amended by Commission Regulation (EC) No 2261/98(2), and in particular Article 9 thereof,
(1) Whereas Regulation (EEC) No 2658/87 established a goods nomenclature, hereinafter referred to as the "Combined Nomenclature", to meet, at one and the same time, the requirements both of the Common Customs Tariff and of the external trade statistics of the Community;
(2) Whereas the Community is a signatory to the International Convention on the Harmonized Commodity Description and Coding System, known as the "Harmonized System";
(3) Whereas, as a consequence, the said Combined Nomenclature has been established on the basis of the Harmonized System;
(4) Whereas in order to ensure uniform application of the Combined Nomenclature annexed to the said Regulation, it has been necessary to adopt measures concerning the classification of goods referred to in the Annexes to this Regulation;
(5) Whereas, due to changes to the descriptions of products, their related codes and legal notes in the Harmonized System nomenclature or in the Combined Nomenclature certain classification regulations should be repealed when such regulations are no longer relevant or valid;
(6) Whereas, certain regulations still being relevant but having references to codes and legal notes which no longer exist should be updated in order to take into account the appropriate codes and legal notes in force;
(7) Whereas the measures provided for in this Regulation are in accordance with the opinion of the tariff and Statistical Nomenclature Section of the Customs Code Committee,
HAS ADOPTED THIS REGULATION:
Article 1
In the case of the Regulations listed in column 2 of Annex A, with reference to the goods specified in columns 3 and 4, the codes and legal notes of the Combined Nomenclature listed in column 5 are hereby replaced by the codes and legal notes of the Combined Nomenclature in column 6.
Article 2
The Regulations listed in Annex B are hereby repealed.
Article 3
This Regulation shall enter into force on the day after its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 27 April 1999.
For the Commission
Mario MONTI
Member of the Commission
(1) OJ L 256, 7.9.1987, p. 1.
(2) OJ L 292, 30.10.1998, p. 1.
ANNEX A
>TABLE>
ANNEX B
- Regulation (EEC) No 3417/88 of 31.10.1988, No 2 (Electronic printing system) (OJ L 301, 4.11.1988, p. 8)
- Regulation (EEC) No 1964/90 of 6.7.1990, No 1 (Joystick) (OJ L 178, 11.7.1990, p. 5)
- Regulation (EEC) No 1964/90 of 6.7.1990, No 2 (Computer mouse) (OJ L 178, 11.7.1990, p. 5)
- Regulation (EEC) No 1288/91 of 14.5.1991, No 3 (Keyboards for automatic data processing machines) (OJ L 122, 17.5.1991, p. 11)
- Regulation (EEC) No 442/91, as amended by Regulation (EC) No 2383/96 of 13.12.1996 (Pallet collars) (OJ L 326, 17.12.1996, p. 1)
