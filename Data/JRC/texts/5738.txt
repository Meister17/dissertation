Council opinion
of 8 March 2005
on the updated convergence programme of Lithuania, 2004-2007
(2005/C 177/04)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1466/97 of 7 July 1997 on the strengthening of the surveillance of budgetary positions and the surveillance and coordination of economic policies [1], and in particular Article 9(3) thereof,
Having regard to the recommendation of the Commission,
After consulting the Economic and Financial Committee,
HAS DELIVERED THIS OPINION:
(1) On 8 March 2005 the Council examined the updated convergence programme of Lithuania, which covers the period 2004 to 2007. The programme broadly complies with the data requirements of the revised "code of conduct on the content and format of stability and convergence programmes". Some required data related to external assumptions are not available and also some optional data are not provided, although this does not make more difficult the programme's evaluation. Accordingly, Lithuania is invited to achieve compliance with the data requirements of the code of conduct.
(2) The macroeconomic scenario underlying the programme envisages real GDP growth to decelerate from 6,5 % in 2004 to 6,2 % on average over the rest of the programme period. On the basis of currently available information, this scenario seems to reflect plausible growth assumptions. The programme's projections for inflation also appear realistic, although the projection for 2006 appears on the low side.
(3) The key objective of the medium-term fiscal strategy defined in the programme is the approximation to a cyclically balanced general government budget. The programme foresees the general government deficit to remain at 2,5 % of GDP in 2005 and to decline gradually thereafter to 1,5 % in 2007. The primary deficit is expected to improve marginally from 1,5 % of GDP in 2004 to 1,4 % in 2005, decreasing gradually afterwards to 0,5 % in 2007. Consolidation is planned to be mainly achieved by an increase in the revenue ratio from a low base, with the expenditure side also contributing from 2005. Revenues are expected to increase from 33 % of GDP in 2004 to 34,5 % in 2007, after peaking in 2006. Expenditure is foreseen to initially increase from 35,5 % of GDP in 2004 to 36,9 % in 2005, decreasing gradually to 36 % in 2007. A significant programme of public investment is being implemented, which lifts public investment from 3,4 % of GDP in 2004 to 5 % in 2007, resulting in an average public investment ratio over the programme period of about 4,6 % of GDP, against an EU-average of 2,4 % of GDP in 2004. When compared to the May 2004 programme, the deficit outcome for 2004 is estimated to have been lower than budgeted. The targets for 2005-2007 are maintained, despite a considerable downward revision of GDP growth.
(4) The risks to the budgetary projections in the programme appear broadly balanced. On the one hand, the government has shown a good track record in meeting the fiscal targets in recent years. The new growth assumptions are cautious and downside macroeconomic risks seem limited. The main risk to the fiscal projections stems from the uncertainty about the application of budgetary measures announced in the programme. In particular, the tax revenue target for 2005 might be difficult to achieve if compensatory measures for the abolition of the turnover tax were not introduced. A failure to execute foreseen measures constraining expenditure overruns could threaten the budgetary targets over the programme's period. Other risks stem from the relatively uncertain costs of the pension reform, the high outstanding contingent liabilities, budget arrears, outstanding liabilities related to the savings and real estate restitution obligations and spending related to the decommissioning of the Ignalina nuclear power plant.
(5) In the light of this risk assessment, the budgetary stance in the programme does not seem to provide a sufficient safety margin against breaching the 3 % of GDP deficit threshold with normal macroeconomic fluctuations throughout the programme period, particularly in 2005. It is also insufficient to ensure that the Stability and Growth Pact's medium-term objective of a budgetary position of close to balance is achieved. Against the background of the strong economic performance and the large current account deficit, a prudent budgetary policy is needed, also in view of preventing potential risks of overheating.
(6) The debt ratio is estimated to have reached 20,1 % of GDP in 2004, well below the 60 % of GDP Treaty reference value. The programme projects the debt ratio to remain at the same level in 2007, after peaking at 20,9 % in 2005.
(7) Lithuania appears to be in a relatively favourable position with regard to long-term sustainability of the public finances, of which the projected budgetary costs of an ageing population is an important element. The relatively low debt ratio, the pension reform measures enacted, including the introduction of the funded pillars will contribute to limit the budgetary impact of ageing. The strategy outlined in the programme is based on a contained budgetary deficit over the medium term and the long-term budgetary impact of the pension reform. Nevertheless, risks related to the costs of the pension reform should be monitored. In addition, reform measures in the field of health-care could involve higher expenditures. However, Lithuania has a relatively low tax ratio: the sustainability gap that arises in the long-run could be addressed by raising it.
(8) Overall, the economic policies outlined in the update are broadly consistent with the country-specific broad economic policy guidelines in the area of public finances. There are measures to use better-than-projected revenues and savings in budgeted EU co-financing for deficit reduction and limit expenditure overruns. The announced measures are not extended to savings made in items unrelated to EU co-financing.
In view of the above assessment, the Council is of the opinion that Lithuania should:
i) timely implement the tax measures announced by the Government in the convergence program (real estate tax, vehicle tax) to make further progress towards a close to balance budgetary position. This is necessary particularly in order to manage domestic demand pressures, which are currently fuelled by strong credit growth, and thereby prevent a deterioration of the current account deficit and acceleration of underlying inflation.
ii) implement strictly the budget for 2005 in order to reduce the risk of breaching the 3 % reference value; and
iii) use better-than-projected or additional revenues and unused expenditure items for deficit reduction.
Comparison of key macroeconomic and budgetary projections
Convergence programme (CP); Commission services economic forecasts (COM)
| 2004 | 2005 | 2006 | 2007 |
Real GDP(% change) | CP January 2005 | 6,5 | 6,5 | 6,2 | 6,0 |
COM October 2004 | 7,1 | 6,4 | 5,9 | n.a. |
CP May 2004 | 7,0 | 7,3 | 6,6 | 6,3 |
HICP inflation(%) | CP January 2005 | 1,2 | 2,9 | 2,5 | 2,9 |
COM October 2004 | 1,2 | 2,9 | 2,8 | n.a. |
CP May 2004 | 0,9 | 2,0 | 2,1 | 2,5 |
General government balance(% of GDP) | CP January 2005 | – 2,5 | – 2,5 | – 1,8 | – 1,5 |
COM October 2004 | – 2,6 | – 2,5 | – 1,9 | n.a. |
CP May 2004 | – 2,7 | – 2,5 | – 1,8 | – 1,5 |
Primary balance(% of GDP) | CP January 2005 | – 1,5 | – 1,4 | – 0,8 | – 0,5 |
COM October 2004 | – 1,4 | – 1,4 | – 0,9 | – 0,5 |
CP May 2004 | – 1,3 | – 1,2 | – 0,6 | – 0,4 |
Government gross debt(% of GDP) | CP January 2005 | 20,1 | 20,9 | 20,3 | 20,1 |
COM October 2004 | 21,1 | 21,7 | 21,3 | n.a. |
CP May 2004 | 22,4 | 22,2 | 21,4 | 21,0 |
[1] OJ L 209, 2.8.1997, p. 1. The documents referred to in this text can be found at the following websitehttp://europa.eu.int/comm/economy_finance/about/activities/sgp/main_en.htm
--------------------------------------------------
