Opinion of the European Economic and Social Committee on the "Communication from the Commission to the Council, the European Parliament, the European Economic and Social Committee and the Committee of the Regions: Civil society dialogue between the EU and candidate countries"
(COM(2005) 290 final)
(2006/C 28/22)
On 29 June 2005, the European Commission decided to consult the European Economic and Social Committee, under Article 262 of the Treaty establishing the European Community, on the abovementioned proposal.
The Section for External Relations, which was responsible for preparing the Committee's work on the subject, adopted its opinion on 10 October 2005. The rapporteur was Mr Pezzini.
At its 421st plenary session, held on 26 and 27 October 2005 (meeting of 27 October), the European Economic and Social Committee adopted the following opinion by 99 votes to 5 with 9 abstentions.
1. Summary of the Communication
1.1 Experience drawn from the enlargements so far has shown that the general public had been poorly informed and prepared during the run-up to enlargements. With a view to the EU's future work, dialogue with civil society, the so-called third pillar [1], needs to be improved.
1.2 The strengthening of dialogue between civil societies must be driven by the following objectives:
- strengthening contacts and exchanges of experience between all sectors of civil society in the Member States and candidate countries;
- deepening the knowledge and understanding of the candidate countries within the European Union, particularly their culture and history, with a view to highlighting the opportunities and challenges presented by future enlargements;
- deepening the knowledge and understanding of the European Union within the candidate countries, particularly the values upon which it is founded, its functioning and policies;
- civil society has been defined in various ways. The Commission has opted for the broadest and most inclusive definition as put forward by the EESC: all organisational structures whose members have objectives and responsibilities that are of general interest and who also act as mediators between the public authorities and citizens [2].
1.3 The Commission has outlined a strategic framework which envisages strengthening ongoing action including the decade-long experience of the European Economic and Social Committee, and proposes new measures to strengthen and deepen the current process.
1.3.1 The following programmes are mentioned in the context of stepping up action in Croatia and Turkey: Socrates, Leonardo, Youth, the Jean Monnet, Marie Curie and Culture actions and the media. A programme aimed at NGOs and other civil society organisations is expected to be developed.
1.3.2 As far as future action is concerned, the Communication, while emphasising that it is civil society that will draw upon its experience and develop new programmes, recommends the following:
- a long-term partnership between the NGOs, social partners and professional organisations that will be eligible for Community financing;
- close links between women's rights organisations;
- setting up of a EU/Turkey Business Council;
- new town-twinning arrangements between local communities;
- youth, university and professional exchanges;
- cultural exchanges;
- enhanced participation in Community programmes on culture and the media;
- enhanced language training;
- promotion of public debates, particularly online;
- exchanges of experience and sensitising journalists;
- dialogue between churches and different religions.
1.3.3 Where applicable, visas should be simplified and supplied at short notice.
1.4 In order to implement the programme on social dialogue, an investment of approximately EUR 40 million is planned. In the EESC's opinion, this figure is rather low.
2. Comments
2.1 The European Commission document title suggests a dialogue with all candidate countries, however, except for a brief mention of Croatia, the Communication is almost exclusively devoted to Turkey.
2.2 In the EESC's view, it would have been useful to extend the topic to Croatia, Serbia and the Western Balkans [3].
2.3 Turkey occupies a strategic position to the south of the Mediterranean and thus also forms part of the Euro Mediterranean policy. It would be useful for the Commission to clarify the characteristics and specificities of this dual role.
2.4 Workers and businesses from the candidate countries have been members of European representative bodies (ETUC, UNICE, UEAPME and EUROCHAMBRES) since the 1990s; they have participated in the various stages of social dialogue, and Turkey, in particular, has stood out on account of its commitment and enthusiasm. In the EESC's view it would be useful to explore the impact of this commitment and experience on Turkish trade unions and businesses.
2.5 Social dialogue, seen as a dialogue to pave the way for accession negotiations, plays a significant role in the Community acquis and strategies and cannot be improvised. In the EESC's view, the Commission must focus particular attention on this kind of dialogue in order to ensure the ongoing involvement of all representative bodies.
2.5.1 In Turkey there are many cultural foundations and organisations in which young people play a significant role. In the EESC's view, these cultural groups could represent an excellent opportunity for examining common issues more closely and highlighting the most effective instruments, thus influencing the development of society.
2.5.2 Twinning arrangements between local communities, universities and training institutions ought to be promoted and supported as they have the capacity to ensure an ongoing exchange of experience and encourage civil society actors to evaluate the various ways in which cultural, social and economic issues are addressed and resolved.
2.6 In order to obtain concrete results, it would be advisable to increase the participation of representatives from candidate countries in the work of the Social Affairs DG and representative organisations active at EU level, through the use of appropriate instruments.
2.7 Cooperation between different faiths, particularly between Christians and Muslims, should also be promoted and supported.
2.8 Similarly, dialogue on the various forms of culture should be enhanced by all means possible.
2.9 As regards the programme on NGOs, which will soon be published by the European Commission, the EESC hopes that it will provide concrete and useful proposals for a constructive exchange of experience.
3. Measures taken by the EESC
3.1 Working in close liaison with the Commission, since 1995 the European Economic and Social Committee has set up a series of Joint Consultative Committees (JCCs) with all the enlargement countries.
3.2 A committee has also been set up with the ACP (African, Caribbean and Pacific) states, which is now the Commission's point of reference in applying the Cotonou Agreement and managing Economic Partnership Agreements (EPAs).
3.3 The EESC has also set up a Latin America Follow-up Committee which is responsible for promoting the development of socioeconomic organisations in Latin American countries, facilitating the participation of these organisations in ongoing regional integration processes (Central America, the Andean Community, Mercosur) and ensuring that current association agreements and those under negotiation are followed up.
3.4 The Euromed Committee was set up within the EESC in the second half of the nineties to help the Commission implement the social and economic policy envisaged for the southern Mediterranean rim.
3.4.1 The EU-India Round Table and the Transatlantic Agenda have enabled the European Union to make its social and cultural structure more visible to the rest of the world.
3.5 The Western Balkans Contact Group was set up in 2004 with the task of promoting cooperation between the EESC and Western Balkan civil society organisations, including economic and social councils [4], in order to help civil society organisations in the countries of the Western Balkans to strengthen integration with the EU and, ultimately, EU membership.
3.5.1 The EESC has taken an interest in south-east Europe by drawing up the following:
- an information report on Relations between the European Union and certain countries in south-east Europe [5],
- an own-initiative opinion on Development of human resources in the Western Balkans [6],
- an own-initiative opinion on Promoting the involvement of Civil Society Organisations in South-East Europe (SEE) — past experiences and future challenges [7],
- an exploratory opinion on The role of civil society in the new European strategy for the Western Balkans [8],
- an own-initiative opinion on Croatia's application for EU membership [9].
3.5.2 The Committee has also adopted own-initiative opinions on Bulgaria, Romania and Croatia.
3.5.3 The EESC has already produced two partial assessments of European Neighbourhood Policy [10], and is currently in the process of preparing an exploratory opinion on the [11].
3.6 In contrast to other committees, which stipulated the participation of 6 or 9 EESC members (3 per group), the EU/Turkey JCC was set up with 18 EESC and 18 Turkish members. This increase was justified because of the country's size and the potential problems that could arise from the fact that it was the first Muslim country requesting EU membership.
3.6.1 The EU-Turkey Joint Consultative Committee is a body that brings together representatives of organised civil society of the EU and Turkey. Its members come from various civil society organisations: chambers of commerce, industry and crafts, trade, employer associations, cooperatives, chambers of agriculture, consumer organisations, NGOs, representatives of the third sector, etc.
3.6.2 EU-Turkey JCC meetings [12] have been held alternately in Brussels and Turkish localities and every meeting has addressed social and economic issues: the 19th JCC meeting was held in Istanbul on 7 and 8 July 2005 [13].
3.6.3 The following were among the more interesting issues discussed in the meetings:
- Relations between Turkey and the EU in respect of energy (1996).
- EU-Turkey cooperation in the field of small- and medium-sized enterprises and vocational training (1996).
- Agricultural products in the context of the EC-Turkey customs union (1997).
- The social impact of the customs union (1998).
- The role of women in development and decision-making (1999).
- Migrations (2000).
- Research and development (2000).
- Liberalisation of services (2000).
- Social dialogue and economic and social rights in Turkey (2001).
- Impact of the economic crisis in Turkey (2002).
- Regional disparities in Turkey (2002).
- The development of agriculture in Turkey (2002).
- Turkey on the road to accession (2003).
- Social integration of people with disabilities (2004).
- Micro-enterprises and standardisation processes (2004).
- The development of EU-Turkey relations and the involvement of civil society in the accession negotiations (2005).
3.7 As can be seen above, many areas of the Community acquis have been addressed in meetings held with Turkish civil society organisations. It must be stressed that these meetings, particularly those held in Turkey [14], were attended by many people from Turkey's most representative civil society organisations.
3.8 The meetings held in Turkey, in particular, were attended by many representatives of the various Turkish organisations, in addition to the JCC members, and they played an important part in promoting mutual understanding.
3.9 Among the initiatives developed by the JCC, the commitment to set up a Social and Economic Committee in Turkey based on the EU model and on the committees in many European countries [15] should be highlighted.
3.10 The Commission Communication acknowledges the active role played by the EESC, during the past ten years, in relations with Turkey [16]. The Commission hopes that the Committee of the Regions will play a similar role in relation to Turkish regions.
4. Implementation of the third pillar and grassroots democracy
4.1 Grassroots culture, which alongside the strengthening of subsidiarity is among the trends that have emerged during the last few years, has gained importance and is becoming a cultural stance through which citizens express their desire to be key protagonists in social decisions.
4.2 Two significant developments took place concurrently with the advent of grassroots democracy:
- the new duties of national parliaments;
- strengthening of the subsidiarity principle.
4.3 Parliament is often perceived as being too slow and incoherent in its decisions. Thus an effort must be made to establish a new parliamentary structure and role [17].
4.3.1 The culture of subsidiarity has spread as a result of, in particular, the European Union's impetus, and is based on the principle of multi-level decision-making. The skill lies in identifying the most appropriate level. It is pointless to do in Brussels what can be better performed at national or local level, or vice versa.
4.4 As a result of new technologies, knowledge is spreading at a rate and extent which were previously inconceivable. Many who previously had no access to information, are now better prepared, more knowledgeable and feel that their opinions can contribute towards decision-making [18].
4.4.1 These needs, which represent the expression of a society undergoing cultural growth, not least as a result of experience shared with other European states, can be better addressed by grassroots democracy.
4.4.2 The recently concluded enlargement and future enlargements with Turkey and Croatia require these cultural and social models to be commonly shared.
4.5 These models aim to integrate the various aspects (economic, industrial, trade union and occupational) into the same system of organising consensus.
4.5.1 Integrating different cultures. The many occupational categories often address problems in different ways. Even if many needs now seem to be similar in nature because of cultural development, the instruments and means of meeting needs and increasingly sophisticated aspirations remain diverse. In line with current models, the various positions are reconciled at the top level through political mediation.
4.5.2 This process, however, is increasingly giving rise to dissatisfaction and alienation from the political classes and professional associations. Beginning with the lowest levels, a major and different effort is needed to better integrate the various aspects of the problems. It is not a matter of creating cultural homogeneity but of working together to identify the ways forward that can secure the widest consensus.
4.5.3 In the same system. The integration process can be, and is being, triggered in various ways. Nevertheless, it needs to be organised in a systematic and methodical manner. The systems that have had the greatest success, both within European bodies and Member States, have grouped civil society organisations under three headings: [19] those representing employers, employees and lastly, independent professionals, NGOs, equal opportunities committees, consumer rights groups and grassroots associations.
4.6 At the various levels. An organisation of this kind is responsible for addressing specific, often complex issues, at regional, national or European level. It was for this reason that the Treaty of Rome, which established the European Union in 1957, set up the European Economic and Social Committee in addition to other institutions.
4.6.1 Identifying the most suitable levels, at which to search for shared solutions is part of cultural growth and the search for a grassroots democracy.
4.7 The method of active dialogue between and with organised civil society. Ongoing dialogue between and with organised civil society emerges and is given the room to develop in a mature democracy, where knowledge and information are disseminated among the general public, thus ensuring that even the most complex issues are addressed without the constraints of ideological conditioning and ignorance.
4.7.1 Ongoing dialogue. Ongoing dialogue is an important aspect as it ensures that the limits set by occasional dialogue are overcome. Particularly where complex issues are involved, sustained and organised methodological exchanges may yield unhoped-for results.
4.8 Interaction [20] as a means of reaching rapid and common solutions. Interaction follows naturally from engaging in ongoing and methodological work and ensures that results are valuable and long lasting. When opinions are being drawn up, many positions, which at first appear to be far apart, will come together and be reconciled.
4.9 These processes are normally used in parliaments but do not involve organised civil society at grassroots level. To extend these models to the regions in an organised, sustained and methodical way is to enter a more mature form of democracy, leading in turn to a grassroots democracy.
5. Accession negotiations and civil society
5.1 As is generally known, when setting out the principles to be followed for enlargement, the Copenhagen criteria also obliged the new Member States to fully incorporate the acquis communautaire into their policies and practices. For this reason, too, the EESC welcomes and wholeheartedly endorses the Communication from the Commission on dialogue between civil society in the EU and Turkey, adopted on 29 June.
5.2 In the Committee's opinion, both sides need to put aside stereotypes and present the European Union and Turkey as they are now and find the most suitable ways of improving mutual knowledge and achieving a more effective implementation of grassroots democracy.
5.3 The Committee is pleased to note that a new draft law on the reform of the Turkish Economic and Social Council has been prepared with a number of representatives of organised civil society, including several JCC members.
5.3.1 On the one hand, this draft legislation gives an enhanced role and a better representation to civil society representatives in the Council and, on the other, limits the role of government.
5.4 The EESC trusts that this legislation will be adopted and implemented as soon as possible and strongly reaffirms the principle that the new Turkish Economic and Social Council must be able to operate independently and with the necessary financial and human resources.
5.5 It is vital that EU-Turkey relations be characterised by transparency, participation and accountability.
5.6 Taking into consideration the nature of EU-Turkish relations and the experience of countries that have recently joined the EU, the Committee, through the work of the JCC members, has proposed several measures in order to involve organised civil society in the accession negotiations.
5.6.1 Capacity-building is needed in order to enable civil society organisations to have a say in the accession talks. In order to achieve this, they will require assistance and funding.
5.6.2 This support should be provided mainly by the Turkish government and also, as a complementary support, by the EU institutions and civil society organisations in current EU Member States.
5.7 Reinforcing links with European organisations and taking part in European transnational projects are useful instruments for capacity-building of Turkish organisations.
5.7.1 JCC members can also make a valuable contribution in this respect and their own organisations are invited to start civil society dialogue programmes. Such projects must be aimed at improving mutual knowledge and ensuring collaboration, especially through the exchange of best practices.
5.8 Representatives of Turkish associations could benefit from sustained participation in study group discussions which are held at EU level with a view to implementing European policies, as in the case of the Luxembourg process.
5.9 In the JCC's view, visa procedures in the Member States are an obstacle to the development of relations within civil society. The EESC therefore calls on governments to facilitate these procedures in order to:
- enhance dialogue between civil society representatives;
- enhance trade union cooperation;
- facilitate trade and business relations;
- stimulate trade and economic relations;
- create an NGO platform.
5.10 It is essential to speed up the establishment of the Turkish Economic and Social Council in the independent form mentioned above, as it will be an effective mechanism for permanent dialogue between the government and organised civil society, involving all stakeholders.
5.10.1 This body, which will ensure a better implementation of grassroots democracy, must meet the legitimate aspirations of the whole of organised civil society and reflect its needs and potential in the process of integration into the life of the European Union.
5.10.1 To make sure it is broad-based, the Turkish Economic and Social Council should be organised on a regional basis and be involved in consultation on all the Community acquis chapters of negotiation, which should take place in an ongoing and timely manner.
5.11 Raising awareness and understanding of the country's models and cultural traditions in both Turkey and the EU is of key importance to the success of negotiations. Particular efforts should be made to ensure that public perceptions of the EU vis-à-vis Turkey, and vice versa, correspond to reality.
5.11.1 The most effective way of achieving this is to give civil society organisations the chance to put together effective information campaigns.
5.12 The EESC, through the JCC's extensive work over a long period, has always been committed to:
- encouraging the involvement of organised civil society in the negotiation process for each of the chapters discussed;
- scrutinising the socio-economic consequences of Turkey's adoption of the "acquis communautaire";
- enhancing and consolidating dialogue and cooperation between representatives of organised civil society in the EU Member States and in Turkey.
5.12.1 In doing this, the EESC and JCC can benefit from the experience of members from the new Member States in overcoming the difficulties encountered during negotiations.
5.12.2 The aim is to build on the work of local and regional organisations through a process of grassroots democracy, which the political class could never manage to achieve on its own.
5.13 In the particular case of Turkey, given the size of the country and the complex issues involved, meetings and exchanges of experience have been more in-depth and frequent. But there is more to be done.
5.14 More funding needs to be provided, the commitment intensified, meetings and exchanges of experience increased and the social and occupational categories involved more clearly identified.
6. Conclusions
6.1 The EESC believes that, with the agreement and active participation of the Turkish Economic and Social Council, a permanent structure should be put in place, bringing together civil society representatives and being responsible for following accession negotiations [21], in order to ensure effective and full implementation of the Community acquis.
6.1.1 Given the vastness of the country and its cultural diversity, this body should be able to function at both national and regional level.
6.2 It is vital that the cultural interaction and the benefits of the acquis do not become the exclusive preserve of Istanbul, Ankara and the main Turkish cities, but also extend to provinces and rural areas.
6.3 The aspirations, concerns and expectations of the people are echoed and given concrete expression in civil society organisations. These organisations should have a crucial role to play in planning and conducting the information campaign on Turkey's EU membership.
6.3.1 In the EESC's view, it is also important that a common platform for communication [22], i.e. a forum, be set up which would involve associations and NGOs and enable them to focus on key challenges and seek common solutions, in concrete terms and at grassroots level.
6.4 The pre-accession process involves support programmes and financial assistance. Often, only a handful of people, the specialist staff involved, are familiar with procedures, timeframes and financial opportunities. Clear information must be provided from the start so that it can be disseminated among all the organisations concerned and make projects and proposals possible.
6.4.1 Similarly, the procedures to obtain the necessary funding must, as far as possible, be simple and clearly explained to representations of civil society.
6.5 The European Commission delegation to Turkey, helped not least by the support and experience of the JCC, the EESC and the Committee of the Regions, can contribute significantly by holding practical and structured dialogue between the representatives of all sectors of civil society — including players in the social dialogue — in Turkey and between Turkey and the EU [23]. It is essential that the budget earmarked for strengthening civil society be used to ensure that the social partners in Turkey and the EU can develop joint projects on training on the large number of areas laid down in the 31 chapters of the Community acquis.
6.6 In the EESC's opinion, it is as important as ever to enhance capacity-building in Turkish organisations and thus enable them to build on their understanding of their European counterparts, particularly of their duties and their representative role in a grassroots democracy.
6.6.1 In the process of adopting the acquis, there should be legislation on associations which ought to be substantially in line with current European legislation.
6.7 The Turkish government, guided not least by the provisions of the Community acquis, should improve legislation on organisations and remove the obstacles hampering development of NGOs.
6.8 As far as gender issues are concerned, the Committee urges the Commission to ensure that women are sufficiently involved in actions of cooperation and appropriately represented in forums for dialogue and action programmes.
6.9 The EESC believes that the various Turkish organisations should be supported so that, within a very short space of time, they can become members of European and international organisations in their respective sectors.
6.10 Turkey's participation in education and training programmes should be encouraged by all means possible, not least by new ad hoc programmes to complement current ones.
6.10.1 Graduate traineeships, under a suitably enhanced Erasmus programme, could be the perfect opportunity for students from different countries to get to know and respect one another.
6.11 The Committee believes that much of the paperwork required by the Turkish ministries when Turkish businesses and other economic players decide to hold events in Europe could be reduced or eliminated altogether.
6.12 The links between representatives of similar organisations and between Turkey and the EU need to be supported, stimulated and encouraged as they ensure that the ongoing exchanges of experience and culture are facilitated and speeded up.
6.13 Essentially, all efforts should be directed at ensuring that the largest possible number of Europeans become acquainted with Turkey and the Turkish people with Europe.
Brussels, 27 October 2005.
The President
of the European Economic and Social Committee
Anne-Marie Sigmund
[1] The first pillar comprises reform policy; the second pillar accession negotiations; the third, dialogue between civil societies.
[2] Social partners (trade unions and employer organisations); in broad terms, those organisations which represent the social and economic players (eg. consumer associations); NGOs; grass root associations (eg. family and youth associations); religious communities and the media and chambers of commerce.
[3] The Western Balkans is usually understood to be the territory that made up the former Yugoslavia (less Slovenia) and Albania, i.e. Croatia, Bosnia Herzegovina, Serbia and Montenegro (including Kosovo), the Former Yugosla
v Republic of Macedonia and Albania.
[4] Croatia and Montenegro have set up economic and social councils.
[5] Information report, CESE 1025/98 fin, rapporteur: Mr Sklavounos.
[6] EESC opinion, rapporteur: Mr Sklavounos. OJ C 193 of 10.7.2001, p. 99.
[7] EESC opinion, rapporteur: Mr Wilkinson. OJ C 208 of 3.9.2003, p. 82.
[8] EESC opinion, rapporteur: Mr Confalonieri. OJ C 80 of 30.3.2004, p. 158.
[9] EESC opinion, rapporteur: Mr Strasser. OJ C 112 of 30.4.2004, p. 68.
[10] One deals with central and eastern European, EESC opinion, OJ C 80 of 30.3.2004, p. 148 (rapporteur: Ms Alleweldt) and the other with Mediterranean countries, information report CESE 520/2005 fin (rapporteur: Ms Cassina).
[11] Rapporteur: Ms Cassina (REX 204).
[12] To date, 19 meetings have been held.
[13] The first meeting was held in Brussels on 16 November 1995 and was attended by Mr Özülker, Turkey's ambassador to the EU.
[14] 9 meetings were held in Turkey: 3 in Istanbul, 1in Gaziantep, 2 in Ankara and 1 each in Trabzon, Izmir and Erzurum.
[15] The following countries have an Economic and Social Council: Austria, Belgium, Spain, Finland, France, Greece, Hungary, Italy, Luxembourg, Malta, Poland, Portugal, Slovenia, Bulgaria and Romania (source: EESC).
[16] COM(2005) 290 of 29.6.2005, point 2.2.2.
[17] Jacques Delors explored this subject in an interesting speech he gave at a meeting with the EESC in 1999.
[18] The concept of a grassroots democracy builds on and formalises the concept of participation as it structures and organises consensus through instruments and institutions (such as the EESC and national and regional ESCs) which help to resolve problems and seek solutions to radical social and economic changes. A typical example would be the liberalisation of the services, energy and gas markets in the various EU Member States, and services of general interest in particular.
[19] These three headings – in a slightly different form – have been adopted by the economic and social councils that have been set up in 15 EU Member States.
[20] Interaction paves the way for reciprocal and similar frames of mind: reciprocal in the sense that one involves the other and similar, since they tend to seek analogous and common lines of reasoning (from Alberoni et al).
[21] This entails establishing a horizontal and vertical partnership, involving civil society (horizontal) and institutional (vertical) representatives, as is the case with NUTS II in Objective 1 regions. The partners described above have benefited from consultation and information exercises, which, in turn, have produced significantly better solutions to the problems facing regions that are lagging behind. Cf. DG Regio documents on regional ROPs and SPDs.
[22] A structure for dialogue.
[23] A broad dialogue has already emerged. Trade union and employer organisations and representatives of micro enterprises hold frequent meetings on trade union representation, employment, credit problems, vocational training and product marketing and internationalisation. These meetings are held either in Brussels (UNICE, UEAPME, ETUC..) or Turkey, but more formal relations need to be sought. With regard to the textiles issue, the possibility of a Pan-Euromediterranean trade area as a high-quality alternative to Chinese production, has been explored via DG Enterprise.
--------------------------------------------------
