Decision of the EEA Joint Committee No 76/2005
of 10 June 2005
amending Annex II (Technical regulations, standards, testing and certification) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex II to the Agreement was amended by Decision of the EEA Joint Committee No 43/2005 of 11 March 2005 [1].
(2) Commission Directive 2004/104/EC of 14 October 2004 adapting to technical progress Council Directive 72/245/EEC relating to the radio interference (electromagnetic compatibility) of vehicles and amending Directive 70/156/EEC on the approximation of the laws of the Member States relating to the type-approval of motor vehicles and their trailers [2], as corrected by OJ L 56, 2.3.2005, p. 35, is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
Chapter I of Annex II to the Agreement shall be amended as follows:
1. The following indent shall be added in point 1 (Council Directive 70/156/EEC):
"— 32004 L 0104: Commission Directive 2004/104/EC of 14 October 2004 (OJ L 337, 13.11.2004, p. 13), as corrected by OJ L 56, 2.3.2005, p. 35."
2. The following indent shall be added in point 11 (Council Directive 72/245/EEC):
"— 32004 L 0104: Commission Directive 2004/104/EC of 14 October 2004 (OJ L 337, 13.11.2004, p. 13), as corrected by OJ L 56, 2.3.2005, p. 35."
3. The following adaptation shall be added in point 11 (Council Directive 72/245/EEC):
The provisions of the Directive shall, for the purposes of the present Agreement, be read with the following adaptation:
In Annex I, the following shall be added to point 5.2:
"IS for Iceland
FL for Liechtenstein
16 for Norway"
4. The following point shall be inserted after point 45ze (Commission Decision 2004/90/EC):
"45zf. 32004 L 0104: Commission Directive 2004/104/EC of 14 October 2004 adapting to technical progress Council Directive 72/245/EEC relating to the radio interference (electromagnetic compatibility) of vehicles and amending Directive 70/156/EEC on the approximation of the laws of the Member States relating to the type-approval of motor vehicles and their trailers (OJ L 337, 13.11.2004, p. 13), as corrected by OJ L 56, 2.3.2005, p. 35."
Article 2
The texts of Directive 2004/104/EC, in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 11 June 2005, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [3].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 10 June 2005.
For the EEA Joint Committee
The President
Richard Wright
[1] OJ L 198, 28.7.2005, p. 45.
[2] OJ L 337, 13.11.2004, p. 13.
[3] No constitutional requirements indicated.
--------------------------------------------------
