Council Regulation (EC) No 2165/2005
of 20 December 2005
amending Regulation (EC) No 1493/1999 on the common organisation of the market in wine
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 37 thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament,
Having regard to the opinion of the European Economic and Social Committee,
Whereas:
(1) Article 27 of Regulation (EC) No 1493/1999 [1] prohibits the overpressing of grapes and the pressing of wine lees to guard against poor quality wine, and to this end stipulates the obligation to distil marc and lees. Since the production and marketing facilities in the wine-growing areas of Slovenia and Slovakia enable the objectives of this provision to be met, the obligation for producers in these regions to distil by-products of wine-making should be replaced with the obligation to withdraw such by-products under supervision.
(2) Under Article 1(3) of Regulation (EC) No 1493/1999, the decision would be taken upon accession whether to classify Poland in wine-growing region A in Annex III to the above Regulation which classifies the areas planted with vines in Member States into wine-growing zones. The Polish authorities have provided the Commission with information on the areas of vines planted in Poland and their geographical position, which shows that these wine-growing areas could be classed as zone A.
(3) Following recent simplification of the division of the Czech Republic’s wine-growing areas which are classified in zones A and B of the said Annex III, it is necessary to adapt it as a consequence through the introduction of new descriptions of those wine-growing areas.
(4) Annex IV to Regulation (EC) No 1493/1999 establishes a list of authorised oenological practices and processes for wine production. Several oenological practices and processes not contained in this Annex have been authorised on an experimental basis by some Member States under the conditions set out in Commission Regulation (EC) No 1622/2000 laying down certain detailed rules for implementing Regulation (EC) No 1493/1999 on the common organisation of the market in wine and establishing a Community code of oenological practices and processes [2]. The results obtained suggest that these practices and processes were beneficial to improving the management of wine production and the conservation of these products, whilst posing no health risks to consumers. The International Organisation of Vine and Wine has already recognised and authorised these experimental practices carried out in Member States. Therefore such oenological practices and processes should be definitively authorised at Community level.
(5) Annex VI.D.1 to Regulation (EC) No 1493/1999 provides that quality wines psr may be produced only from grapes of wine varieties which appear on the list of the Member State of production and are harvested within the specified region. However, point D.2 of that Annex provides that, until 31 August 2005 at the latest, in the case of a traditional practice governed by special provisions of the Member State of production, that Member State may permit on certain conditions, by means of express authorisations and subject to suitable controls, that a quality sparkling wine psr be obtained by adding to the basic product from which the wine is made one or more wine-sector products which do not originate in the specified region whose name the wine bears.
(6) Italy has applied this derogation for the preparation of quality sparkling wines psr "Conegliano-Valdobbiadene" and "Montello e Colli Asolani". In order to adapt the structural aspects of the traditional method of producing such wines, this derogation should be extended until 31 December 2007.
(7) By virtue of Annex III.1(c) to Regulation (EC) No 1493/1999, the wine-growing areas of Denmark and Sweden form part of wine-growing zone A. These two Member States are now able to produce table wine with a geographical indication. Accordingly, "Lantvin" and "Regional vin" should be added to Annex VII.A.2.
(8) The derogations provided for in Annex VII.D.1 and Annex VIII.F(a), allowing the information on the label to be given in one or more of the official languages of the Community, should apply to Cyprus.
(9) Regulation (EC) No 1493/1999 should therefore be amended accordingly,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 1493/1999 is hereby amended as follows:
1. in Article 27, paragraph 7 is replaced by the following:
"7. Any natural or legal persons or groups of persons who process grapes harvested in wine-growing zone A or in the German part of wine-growing zone B, or on areas planted with vines in the Czech Republic, Malta, Austria, Slovenia or Slovakia shall be required to withdraw the by-products of such processing under supervision and subject to conditions to be determined.";
2. Annexes III, IV, VI, VII and VIII are amended as set out in the Annex.
Article 2
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Union.
However, point 3 of the Annex shall apply from 1 September 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 20 December 2005.
For the Council
The President
M. Beckett
[1] OJ L 179, 14.7.1999, p. 1. Regulation as last amended by Commission Regulation (EC) No 1795/2003 (OJ L 262, 14.10.2003, p. 13).
[2] OJ L 194, 31.7.2000, p. 1. Regulation as last amended by Commission Regulation (EC) No 1163/2005 (OJ L 188, 20.7.2005, p. 3).
--------------------------------------------------
ANNEX
The Annexes to Regulation (EC) No 1493/1999 are amended as follows:
1. Annex III is amended as follows:
(a) point 1 is amended as follows:
- point (c) is replaced by:
"(c) in Belgium, Denmark, Ireland, the Netherlands, Poland, Sweden and the United Kingdom: the wine-growing areas of these countries;"
- point (d) is replaced by:
"(d) in the Czech Republic: the wine growing region of Čechy.";
(b) point 2(d) is replaced by:
"(d) in the Czech Republic: the wine-growing region of Morava and the areas under vines not included in point 1(d);"
2. Annex IV is amended as follows:
(a) point 1 is amended as follows:
- point (i) is replaced by the following:
"(i) treatment of must and new wine still in fermentation with charcoal for oenological use, within certain limits;"
- in point (j), the following indent is inserted after the first indent:
- "— plant proteins,";
- the following point is added:
"(s) addition of L-ascorbic acid up to certain limits.";
(b) point 3 is amended as follows:
- in point (m), the following indent is inserted after the first indent:
- "— plant proteins,";
- the following points are added:
"(zc) the addition of dimethyldicarbonate (DMDC) to wine for microbiological stabilisation, within certain limits and under conditions to be determined;
(zd) the addition of yeast mannoproteins to ensure the tartaric and protein stabilisation of wines.";
(c) in point 4, the following point is added:
"(e) usage of pieces of oak wood in winemaking.";
3. in Annex VI point D.2, first indent, the date " 31 August 2005" is replaced by the date " 31 December 2007";
4. Annex VII is amended as follows:
(a) in point A.2b, the third indent is replaced by:
- "— one of the following wordings under conditions to be determined: "Vino de la tierra", "οίνος τοπικός", "zemské víno", "regional vin", "Landwein", "ονομασία κατά παράδοση", "regional wine", "vin de pays", "indicazione geografica tipica", "tájbor", "inbid ta’ lokalità tradizzjonali,", "landwijn", "vinho regional", "deželno vino PGO", "deželno vino s priznano geografsko oznako", "geograafilise tähistusega lauavein", "lantvin". Where such a term is used, the words "table wine" is not required;"
(b) in point D.1, the third subparagraph is replaced by the following:
"The information referred to in the second subparagraph may be repeated in one or more official languages of the Community for products originating in Greece and Cyprus.";
5. in Annex VIII point F, point (a) is replaced by:
"(a) the following information shall be provided only in the official language of the Member State in whose territory production took place:
- in the case of quality sparkling wines psr, the name of the specified region as referred to in point B.4, second indent;
- for quality sparkling wines psr or for quality sparkling wines, the name of another geographical area as referred to in point E.1.
The names of the products specified in the first and second indents produced in Greece and in Cyprus may be repeated in one or more other official languages of the Community;".
--------------------------------------------------
