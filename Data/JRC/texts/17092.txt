Commission communication pursuant to Article 4(1)(a) of Council Regulation (EEC) No 2408/92
Modification by the United Kingdom of public service obligations in respect of scheduled air services between the Shetland mainland (Tingwall/Sumburgh) and the islands of Foula, Fair Isle, Out Skerries and Papa Stour
(2004/C 306/08)
(Text with EEA relevance)
1. The United Kingdom has decided to modify the public service obligations in respect of the scheduled air services between the Shetland mainland (Tingwall/Sumburgh) and the islands of Foula, Fair Isle, Out Skerries and Papa Stour as set out in the notice published in the Official Journal of the European Communities C 394 of 30 December 1997, as modified in the Official Journal of the European Communities C 356 of 12 December 2000 and C 358 of 15 December 2001, pursuant to Article 4(1)(a) of Council Regulation (EEC) No 2408/92 of 23 July 1992 on access for Community air carriers to intra-Community air routes.
2. The modified public service obligations are as follows:
- Minimum frequencies:
- to Foula from Tingwall — two return trips on Wednesday and Friday and one return trip on Monday and Tuesday (summer service). Two return trips on Friday and one return trip on Monday, Tuesday and Wednesday (winter service),
- to Fair Isle — two return trips from Tingwall on Monday, Wednesday and Friday and one return trip from Sumburgh and Tingwall on Saturday (summer service). Two return trips from Tingwall on Monday, Wednesday and Friday (winter service),
- to Out Skerries from Tingwall — two return trips on Thursday and one return trip on Monday and Wednesday,
- to Papa Stour from Tingwall — two return trips on Tuesday.
Capacity: the capacity of the aircraft used should be not less than eight passenger seats on each route although, depending on weight, numbers may be restricted on the Out Skerries route due to Civil Aviation Authority restrictions.
Fares:
- the price of a single adult ticket must not exceed GBP 25,00 for Foula, GBP 28,00 for Fair Isle, GBP 19,50 for Out Skerries and GBP 17,50 for Papa Stour,
- an island resident discounted return fare at a 20 % saving on the normal fare shall be applicable.
The maximum fare on each route may be increased once a year with the prior written consent of Shetland Islands Council in line with the United Kingdom's Retail Price Index (all items) or any successor index to this.
No other changes may be made to the fare levels without the prior written consent of Shetland Islands Council.
The new maximum fare on each route must be notified to the Civil Aviation Authority and will not enter into force prior to its notification to the European Commission which may then publish it in the Official Journal of the European Union.
--------------------------------------------------
