COMMISSION DECISION
of 24 June 1999
on the publication of the list of existing class A and B passenger ships notified by Greece in accordance with Council Directive 98/18/EC for which the derogation of Article 6(3)(g) may be applied
(notified under document number C(1999) 1712)
(Text with EEA relevance)
(1999/461/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 98/18/EC of 17 March 1998 on safety rules and standards for passenger ships(1), and in particular Article 6(3)(g) thereof,
Whereas Greece has notified to the Commission the list of existing class A and B ships complying with the conditions for applying the derogation pursuant to Article 6(3)(g) of Directive 98/18/EC,
HAS ADOPTED THIS DECISION:
Article 1
The existing Class A and B passenger ships operating exclusively on domestic voyages between ports situated in Greece for which the derogation of Article 6(3)(g) may be applied are those listed in the Annex to this Decision.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 24 June 1999.
For the Commission
Neil KINNOCK
Member of the Commission
(1) OJ L 144, 15.5.1998, p. 1.
ANNEX
>TABLE>
