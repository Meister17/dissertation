Commission Decision
of 5 July 2006
recognising certain third countries and certain areas of third countries as being free from Xanthomonas campestris (all strains pathogenic to Citrus), Cercospora angolensis Carv. et Mendes and Guignardia citricarpa Kiely (all strains pathogenic to Citrus)
(notified under document number C(2006) 3024)
(2006/473/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 2000/29/EC of 8 May 2000 on protective measures against the introduction into the Community of organisms harmful to plants or plant products and against their spread within the Community [1], and in particular points 16.2, 16.3 and 16.4 of Section I of Part A of Annex IV thereof,
Whereas:
(1) In order to permit the introduction of fruits of Citrus L., Fortunella Swingle, Poncirus Raf., and their hybrids from third countries into the Community or their movement within the Community under Directive 2000/29/EC, Commission Decision 98/83/EC of 8 January 1998 recognising certain third countries and certain areas of third countries as being free of Xanthomonas campestris (all strains pathogenic to Citrus), Cercospora angolensis Carv. et Mendes and Guignardia citricarpa Kiely (all strains pathogenic to Citrus) [2] recognised certain third countries and certain areas of third countries as being free from those harmful organisms.
(2) Since its adoption, Decision 98/83/EC has been amended several times. In the interest of clarity and rationality Decision 98/83/EC should, therefore, be repealed and replaced.
(3) New Zealand has submitted official information showing that its territory is free from Xanthomonas campestris and Guignardia citricarpa. New Zealand should therefore be recognised as being free from those harmful organisms.
(4) South Africa has submitted official information showing that the magisterial districts of Hartswater and Warrenton in Northern Cape are free from Guignardia citricarpa. These districts of South Africa should therefore be recognised as being free from this harmful organism.
(5) Australia has submitted information indicating that Queensland is no longer free from Xanthomonas campestris. Queensland should therefore no longer be recognised as being free from that harmful organism.
(6) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on Plant Health,
HAS ADOPTED THIS DECISION:
Article 1
1. For the purposes of point 16.2 of Section I of Part A of Annex IV, the following third countries are recognised as being free from all strains of Xanthomonas campestris pathogenic to Citrus:
(a) all citrus-growing third countries in Europe, Algeria, Egypt, Israel, Libya, Morocco, Tunisia and Turkey;
(b) Africa: South Africa, Gambia, Ghana, Guinea, Kenya, Sudan, Swaziland and Zimbabwe;
(c) Central and South America and the Caribbean: the Bahamas, Belize, Chile, Colombia, Costa Rica, Cuba, Ecuador, Honduras, Jamaica, Mexico, Nicaragua, Peru, the Dominican Republic, Saint Lucia, El Salvador, Surinam and Venezuela;
(d) Oceania: New Zealand.
2. For the purposes of point 16.2 of Section I of Part A of Annex IV, the following areas are recognised as being free from all strains of Xanthomonas campestris pathogenic to Citrus:
(a) Australia: New South Wales, South Australia and Victoria;
(b) Brazil, except the States of Rio Grande do Sul, Santa Catarina, Paraná, São Paulo, Minas Gerais and Mato Grosso do Sul;
(c) United States: Arizona, California, Guam, Hawaii, Louisiana, Northern Mariana Islands, Puerto Rico, American Samoa, Texas and the United States Virgin Islands;
(d) Uruguay, except the Departments of Salto, Rivera and Paysandu — north of River Chapicuy.
Article 2
For the purposes of point 16.3 of Section I of Part A of Annex IV, the following third countries are recognised as being free from Cercospora angolensis Carv. et Mendes:
(a) all citrus-growing third countries in North, Central and South America, the Caribbean, Asia, except Yemen, Europe and Oceania;
(b) all citrus-growing third countries in Africa, except Angola, Cameroon, Central African Republic, Democratic Republic of Congo, Gabon, Guinea, Kenya, Mozambique, Nigeria, Uganda, Zambia and Zimbabwe.
Article 3
1. For the purposes of point 16.4 of Section I of Part A of Annex IV, the following third countries are recognised as being free from all strains of Guignardia citricarpa Kiely pathogenic to Citrus:
(a) all citrus-growing third countries in North, Central and South America, except Argentina and Brazil, the Caribbean and Europe;
(b) all citrus-growing third countries in Asia, except Bhutan, China, Indonesia, Philippines and Taiwan;
(c) all citrus-growing third countries in Africa, except South Africa, Kenya, Mozambique, Swaziland, Zambia and Zimbabwe;
(d) all citrus-growing third countries in Oceania, except Australia and Vanuatu.
2. For the purposes of point 16.4 of Section I of Part A of Annex IV, the following areas are recognised as being free from all strains of Guignardia citricicarpa Kiely pathogenic to Citrus:
(a) South Africa: Western Cape; Northern Cape: magisterial districts of Hartswater and Warrenton;
(b) Australia: South Australia, Western Australia and Northern Territory;
(c) China: all areas, except Sichuan, Yunnan, Guangdong, Fujian and Zhejiang;
(d) Brazil: all areas, except the States of Rio de Janeiro, São Paulo and Rio Grande do Sul.
Article 4
Decision 98/83/EC shall be repealed.
Article 5
This Decision is addressed to the Member States.
Done at Brussels, 5 July 2006.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 169, 10.7.2000, p. 1. Directive as last amended by Commission Directive 2006/35/EC (OJ L 88, 25.3.2006, p. 9).
[2] OJ L 15, 21.1.1998, p. 41. Decision as last amended by Decision 2003/129/EC (OJ L 51, 26.2.2003, p. 21).
--------------------------------------------------
