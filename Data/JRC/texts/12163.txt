Council Decision
of 14 February 2006
extending the period of application of the measures in Decision 2002/148/EC concluding consultations with Zimbabwe under Article 96 of the ACP-EC Partnership Agreement
(2006/114/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular the second subparagraph of Article 300(2) thereof,
Having regard to the Internal Agreement [1] on measures to be taken and procedures to be followed for the implementation of the ACP-EC Partnership Agreement signed in Cotonou on 23 June 2000 [2], and in particular Article 3 thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) By Decision 2002/148/EC [3], the consultations with the Republic of Zimbabwe under Article 96(2)(c) of the ACP-EC Partnership Agreement were concluded and appropriate measures, as specified in the Annex to that decision, were taken.
(2) By Decision 2005/139/EC [4], the application of the measures referred to in Article 2 of Decision 2002/148/EC, which were extended until 20 February 2004 by Decision 2003/112/EC [5] and until 20 February 2005 by Decision 2004/157/EC [6], was extended until 20 February 2006.
(3) The essential elements cited in Article 9 of the ACP-EC Partnership Agreement continue to be violated by the Government of Zimbabwe and the current conditions in Zimbabwe do not ensure respect for human rights, democratic principles and the rule of law.
(4) The period of application of the measures should therefore be extended,
HAS DECIDED AS FOLLOWS:
Article 1
The period of application of the measures referred to in Article 2 of Decision 2002/148/EC shall be extended until 20 February 2007. The measures shall be kept under constant review.
The letter in Annex to this Decision shall be addressed to the President of Zimbabwe.
Article 2
This Decision shall enter into force on the day of its publication in the Official Journal of the European Union.
Done at Brussels, 14 February 2006.
For the Council
The President
K.-H. Grasser
[1] OJ L 317, 15.12.2000, p. 376.
[2] OJ L 317, 15.12.2000, p. 3.
[3] OJ L 50, 21.2.2002, p. 64.
[4] OJ L 48, 19.2.2005, p. 28.
[5] OJ L 46, 20.2.2003, p. 25.
[6] OJ L 50, 20.2.2004, p. 60.
--------------------------------------------------
ANNEX
Brussels,
LETTER TO THE PRESIDENT OF ZIMBABWE
The European Union attaches the utmost importance to the provisions of Article 9 of the ACP-EC Partnership Agreement. As essential elements of the Partnership Agreement, respect for human rights, democratic institutions and the rule of law are the basis of our relations.
By letter of 19 February 2002, the European Union informed you of its decision to conclude the consultations held under Article 96 of the ACP-EC Partnership Agreement and to take certain "appropriate measures" within the meaning of Article 96(2)(c) of that Agreement.
By letters of 19 February 2003, 19 February 2004 and 18 February 2005, the European Union informed you of its decision not to revoke the application of the "appropriate measures" and to extend the period of application until 20 February 2004, 20 February 2005 and 20 February 2006 respectively.
Twelve months later your country's Government, in the view of the European Union, has made no significant progress in the five areas referred to in the Council Decision of 18 February 2002.
In the light of the above, the European Union does not consider that the appropriate measures can be revoked, and has decided to extend their period of application until 20 February 2007. The European Union will closely follow developments in Zimbabwe and would once again like to emphasise that it is not penalising the Zimbabwean people and will continue with its contribution to operations of a humanitarian nature and projects in direct support of the local population, in particular projects in social sectors, democratisation, respect for human rights and the rule of law, which are not affected by these measures.
The European Union wishes to recall that the application of appropriate measures within the meaning of Article 96 of the ACP-EC Partnership Agreement is no obstacle to political dialogue as defined in the provisions of Article 8 of that same Agreement. With this in mind, the European Union wishes to underline the importance that it attaches to future EC-Zimbabwe cooperation and wishes to express its readiness to engage, once the conditions are met, in the post ninth EDF programming and considers that this would be an opportunity for a dialogue between the two partners.
To this end, the European Union hopes that you and your Government will do everything in your power to restore respect for the fundamental principles enshrined in the Partnership Agreement, and thereby enable the restoration of cooperation as soon as conditions allow.
Yours faithfully,
For the Commission
A. Piebalgs
For the Council
K.-H. Grasser
--------------------------------------------------
