COUNCIL DECISION of 21 June 1989 on the conclusion of a Supplementary Protocol to the Agreement between the European Economic Community and the Republic of Iceland concerning the elimination of existing and prevention of new quantitative restrictions affecting exports or measures having equivalent effect (89/545/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 113 thereof,
Having regard to the proposal from the Commission,
Whereas the Agreement between the European Economic Community and the Republic of Iceland (1), signed in Brussels on 22 July 1972, does not provide for the prohibition of quantitative restrictions affecting exports and measures having equivalent effect;
Whereas it is in the interest of the European Economic Community and the Republic of Iceland to promote the free circulation of raw materials and goods by abolishing any such restrictions and measures and by preventing the creation of new restrictions or measures affecting their mutual trade;
Whereas it is necessary both to make arrangements for a phased abolition of current restrictions affecting certain products or measures having equivalent effect and to provide for safeguard measures in the event either of re-export towards third countries against which the exporting Contracting Party maintains restrictions or measures having equivalent effect or in the event of serious shortage of a particular product;
Whereas under Article 33 (1) of the Agreement, the Contracting Parties may, in the interest of their economies, develop the relations established by the Agreement by extending it to fields not covered thereby;
Whereas the Commission has held negotiations with the Republic of Iceland, which have resulted in a Protocol,
HAS DECIDED AS FOLLOWS:
Article 1
The Supplementary Protocol to the Agreement between
the European Economic Community and the Republic of Iceland concerning the elimination of existing and prevention of new quantitative restrictions affecting exports or measures having equivalent effect is hereby approved on behalf of the Community.
The text of the Protocol is attached to this Decision.
Article 2
The President of the Council shall give the notification provided for in Article 4 of the Supplementary Protocol.
Article 3
This Decision shall take effect on the day following its publication in the Official Journal of the European Communities.
Done at Luxembourg, 21 June 1989.
For the Council
The President
C. ARANZADI
(1) OJ No L 301, 31. 12. 1972, p. 2.
