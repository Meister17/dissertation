Commission communication in the framework of the implementation of the Council Directive 87/404/EEC of 25 June 1987 on the harmonization of the laws of the Member States relating to simple pressure vessels
(2006/C 287/06)
(Text with EEA relevance)
(Publication of titles and references of harmonised standards under the directive)
ESO [1] | Reference and title of the harmonised standard (and reference document) | Reference of superseded standard | Date of cessation of presumption of conformity of superseded standard Note 1 |
CEN | EN 286-1:1998 Simple unfired pressure vessels designed to contain air or nitrogen — Part 1: Pressure vessels for general purposes | EN 286-1:1991 | Date Expired ( 31.8.1998) |
EN 286-1:1998/A1:2002 | Note 3 | Date Expired ( 31.1.2003) |
EN 286-1:1998/A2:2005 | Note 3 | Date Expired ( 30.4.2006) |
EN 286-1:1998/AC:2002 | | |
CEN | EN 286-2:1992 Simple unfired pressure vessels designed to contain air or nitrogen — Part 2: Pressure vessels for air braking and auxiliary systems for motor vehicles and their trailers | — | |
EN 286-2:1992/AC:1992 | | |
CEN | EN 286-3:1994 Simple unfired pressure vessels designed to contain air or nitrogen — Part 3: Steel pressure vessels designed for air braking equipment and auxiliary pneumatic equipment for railway rolling stock | — | |
CEN | EN 286-4:1994 Simple unfired pressure vessels designed to contain air or nitrogen — Part 4: Aluminium alloy pressure vessels designed for air braking equipment and auxiliary pneumatic equipment for railway rolling stock | — | |
CEN | EN 287-1:2004 Qualification test of welders — Fusion welding — Part 1: Steels | — | |
EN 287-1:2004/A2:2006 | Note 3 | Date Expired ( 30.9.2006) |
EN 287-1:2004/AC:2004 | | |
CEN | EN 571-1:1997 Non destructive testing — Penetrant testing — Part 1: General principles | — | |
CEN | EN 583-1:1998 Non-destructive testing — Ultrasonic examination — Part 1: General principles | — | |
CEN | EN 970:1997 Non-destructive examination of fusion welds — Visual examination | — | |
CEN | EN 1011-1:1998 Welding — Recommendations for welding of metallic materials — Part 1: General guidance for arc welding | — | |
CEN | EN 1290:1998 Non-destructive examination of welds — Magnetic particle examination of welds | — | |
CEN | EN 1330-3:1997 Non-destructive testing — Terminology — Part 3: Terms used in industrial radiographic testing | — | |
CEN | EN 1714:1997 Non-destructive examination of welds — Ultrasonic examination of welded joints | — | |
CEN | EN ISO 6520-1:1998 Welding and allied processes — Classification of geometric imperfections in metallic materials — Part 1: Fusion welding (ISO 6520-1:1998) | EN 26520:1991 | Date Expired ( 30.4.1999) |
CEN | EN 10207:2005 Steels for simple pressure vessels — Technical delivery requirements for plates, strips and bars | — | |
CEN | EN 12062:1997 Non-destructive examination of welds — General rules for metallic materials | — | |
CEN | EN ISO 15614-1:2004 Specification and qualification of welding procedures for metallic materials — Welding procedure test — Part 1: Arc and gas welding of steels and arc welding of nickel and nickel alloys (ISO 15614-1:2004) | — | |
CEN | EN ISO 15614-2:2005 Specification and qualification of welding procedures for metallic materials — Welding procedure test — Part 2: Arc welding of aluminium and its alloys (ISO 15614-2:2005) | — | |
Note 1 Generally the date of cessation of presumption of conformity will be the date of withdrawal ("dow"), set by the European Standardisation Organisation, but attention of users of these standards is drawn to the fact that in certain exceptional cases this can be otherwise.
Note 3 In case of amendments, the referenced standard is EN CCCCC:YYYY, its previous amendments, if any, and the new, quoted amendment. The superseded standard (column 3) therefore consists of EN CCCCC:YYYY and its previous amendments, if any, but without the new quoted amendment. On the date stated, the superseded standard ceases to give presumption of conformity with the essential requirements of the directive.
NOTE:
- Any information concerning the availability of the standards can be obtained either from the European Standardisation Organisations or from the national standardisation bodies of which the list is annexed to the Directive 98/34/EC [2] of the European Parliament and Council amended by the Directive 98/48/EC [3].
- Publication of the references in the Official Journal of the European Union does not imply that the standards are available in all the Community languages.
- This list replaces all the previous lists published in the Official Journal of the European Union. The Commission ensures the updating of this list.
More information about harmonised standards on the Internet at
http://europa.eu.int/comm/enterprise/newapproach/standardization/harmstds/
[1] (1) ESO: European Standardisation Organisation:
- CEN: rue de Stassart 36, B-1050 Brussels, Tel. (32-2) 550 08 11; fax (32-2) 550 08 19 (http://www.cenorm.be)
- CENELEC: rue de Stassart 35, B-1050 Brussels, Tel. (32-2) 519 68 71; fax (32-2) 519 69 19 (http://www.cenelec.org)
- ETSI: 650, route des Lucioles, F-06921 Sophia Antipolis, Tel. (33) 492 94 42 00; fax (33) 493 65 47 16 (http://www.etsi.org)
[2] OJ L 204, 21.7.1998, p.37.
[3] OJ L 217, 5.8.1998, p. 18.
--------------------------------------------------
