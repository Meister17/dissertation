Commission Decision
of 21 November 2003
laying down the animal health conditions and certification requirements for imports of live fish, their eggs and gametes intended for farming, and live fish of aquaculture origin and products thereof intended for human consumption
(notified under document number C(2003) 4219)
(Text with EEA relevance)
(2003/858/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/67/EEC of 28 January 1991 concerning the animal health conditions governing the placing on the market of aquaculture animals and products(1), as last amended by Council Regulation (EC) No 806/2003(2), and in particular Article 19(1), Article 20(1) and Article 21(2) thereof,
Whereas:
(1) A list of third countries or parts thereof, from which Member States are authorised to import live fish, their eggs and gametes for farming in the Community, should be established.
(2) It is necessary to lay down specific animal health conditions and model certificates for those third countries, taking into account the animal health situation of the third country concerned and of the fish, eggs or gametes to be imported, in order to prevent the introduction of disease agents that could cause significant impact to the fish stock in the Community.
(3) Attention should be paid to emerging diseases and diseases which are exotic to the Community and which could have serious impact on the fish stocks in the Community. Furthermore, the vaccination policy and the disease situation as regards epizootic haematopoietic necrosis (EHN) and the fish diseases referred to in Annex A to Directive 91/67/EEC, at the place or production and where appropriate at the place of destination should be taken into account.
(4) It is necessary that countries or parts thereof from which Member States are authorised to import live fish, their eggs and gametes for farming, must apply conditions for disease control, and monitoring at least equivalent to Community standards as laid down in Directive 91/67/EEC and in Council Directive 93/53/EC of 24 June 1993 introducing minimum Community measures for the control of certain fish diseases(3), as last amended by Commission Decision 2001/288/EC(4). The sampling and testing methods used should be at least equivalent to Commission Decision 2001/183/EC(5) of 22 February 2001 laying down the sampling plans and diagnostic methods for the detection and confirmation of certain fish diseases and repealing Decision 92/532/EEC, and Commission Decision 2003/466/EC(6) of 13 June 2003 establishing criteria for zoning and official surveillance following suspicion or confirmation of the presence of Infectious salmon anaemia (ISA). In cases where sampling and testing methods are not laid down in the Community legislation, the sampling and testing methods used should be in accordance with those laid down in the International Office of Epizootics (OIE) Manual of Diagnostic Tests for Aquatic Animals.
(5) It is necessary that the responsible competent authorities of these third countries undertake to notify by fax, telegram or electronic mail, the Commission and the Member States within 24 hours, of any occurrence of epizootic haematopoietic necrosis (EHN), or diseases referred to in Annex A to Directive 91/67/EEC, as well as any other disease outbreaks causing a significant impact to the fish stock within their territory or parts thereof from which imports covered by this Decision are authorised. In such event, the responsible competent authorities of those third countries must take measures to prevent the disease spreading into the Community. Furthermore and as applicable, the Commission and the Member States should be notified of any alteration in the vaccination policy against such diseases.
(6) In addition, when importing live fish of aquaculture origin and products thereof for human consumption, it is necessary to prevent the introduction into the Community of serious diseases affecting aquaculture animals.
(7) Therefore, it is necessary to supplement the certification requirements relating to the importation of live fish of aquaculture origin and products thereof under Council Directive 91/493/EEC of 22 July 1991 laying down the health conditions for the production and the placing on the market of fishery products(7), as last amended by Regulation (EC) No 806/2003, with the animal health certification requirements.
(8) It would reduce the possibility to control and eradicate diseases which are exotic to the Community and which could have serious impact on the fish stocks in the Community, if fish that could carry the disease are released into unenclosed waters in the Community. Live fish, eggs and gametes of aquaculture origin, should therefore be imported into the Community only if they are introduced into a farm.
(9) This Decision should not apply to the importation of tropical ornamental fish kept permanently in aquariums.
(10) This Decision should apply without prejudice to the public health conditions established under Directive 91/493/EEC.
(11) This Decision should apply without prejudice to Community or national provision on the conservation of species.
(12) Council Directive 96/93/EC of 17 December 1996 on the certification of animals and animal products(8) lays down standards of certification. The rules and principles applied by third-country certifying officers should provide guarantees, which are equivalent to those laid down in that Directive.
(13) The principles laid down in Council Directive 2002/99/EC of 16 December 2002 laying down the animal health rules governing the production, processing, distribution and introduction of products of animal origin for human consumption(9), in particular Article 3 of that Directive should be taken into account.
(14) A transitional period of time should be provided for the implementation of the new import certification requirements.
(15) The list of approved countries referred to in Annex I to this Decision should be reviewed no later than 12 months after the date of application.
(16) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
Scope
1. This Decision establishes harmonised animal health rules for the importation of:
(a) live fish, their eggs and gametes, intended for farming in the Community;
(b) live fish of aquaculture origin intended for restocking of put-and take fisheries in the Community;
(c) live fish of aquaculture origin and products thereof, intended for immediate human consumption or further processing before human consumption.
2. This Decision shall not to apply to the importation of tropical ornamental fish kept permanently in aquariums.
Article 2
Definitions
1. For the purpose of this Decision, the definitions in Article 2 of Directives 91/67/EEC and 93/53/EEC shall apply.
2. The following definitions shall also apply:
(a) "aquaculture origin" means fish originating from a farm;
(b) "approved import centre" means any establishment in the Community where special bio-security measures have been put in place, approved by the competent authority of the Member State concerned, for further processing of imported live fish of aquaculture origin and products thereof;
(c) "coastal zone" means a zone consisting of a part of the coast or sea water or an estuary:
(i) which has a precise geographical delimitation and consists of a homogeneous hydrological system or a series of such systems, or
(ii) which is situated between the mouths of two watercourses, or
(iii) where there are one or more farms and all farms are surrounded by appropriate buffer zones on both sides of the farm or farms;
(d) "continental zone" means a zone consisting of either:
(i) a part of the territory comprising an entire catchment area from the sources of the waterways to the estuary or more than one catchment area in which fish is reared, kept or caught, as necessary surrounded by a buffer zone in which a monitoring program is carried out without the necessity of obtaining the status of an approved zone, or
(ii) a part of a catchment area from the sources of the waterways to a natural or artificial barrier preventing fish migrating from downstream of that barrier, as necessary surrounded by a buffer zone in which a monitoring program is carried out without the necessity of obtaining the status of an approved zone.
The size and the geographical situation of the continental zone must be such that the possibilities for recontamination e.g. by migrating fish are reduced to a minimum;
(e) "designated farm" means either:
(i) a coastal farm in a third country subject to all necessary measures to prevent the introduction of diseases and to which the water is supplied by means of a system which ensures the complete inactivation of the following pathogens: infectious salmon anaemia (ISA), viral heamorrhagic septicaemia (VHS) and infectious haemorrhagic necrosis (IHN), or
(ii) an inland farm in a third country subject to all necessary measures to prevent the introduction of diseases. The farm is, if necessary, protected against flooding and infiltration of water, and there is a natural or artificial barrier situated down stream, which prevents fish from entering the farm. The water is supplied directly to the farm from a borehole, spring, or well, channelled through a pipe, open channel or a natural conduit, which does not constitute a source of infection for the farm and does not allow the introduction of wild fish. The water channel is under the control of the farm or of the competent authorities;
(f) "establishment" means: any premises approved according to Directive 91/493/EEC, where fishery products are prepared, processed, chilled, frozen, packaged or stored, but excluding auction and wholesale markets in which only display and sale by wholesale takes place;
(g) "farming" means: the activity that takes place on any farm or, in general, any geographically defined installation in which fish are reared or kept with a view to their being placed on the market;
(h) "fish products of aquaculture origin" means any products intended for human consumption derived from fish of aquaculture origin, including whole fish (un-eviscerated), eviscerated fish, and filets, and any products thereof;
(i) "further processing" means preparation and processing before human consumption by any kind of measures and techniques, that produces waste or byproducts which could cause a risk of spreading diseases, including: operations affecting the anatomical wholeness such as bleeding, gutting/evisceration, heading, slicing, filleting;
(j) "immediate human consumption" means that the fish imported for the purpose of human consumption do not undergo any further processing within the Community before being placed on the retail market for human consumption;
(k) "put and take fisheries" means ponds, lakes or unenclosed waters that are sustained by the introduction of fish primarily for recreational fishing rather than for conservation or improvement of natural population;
(l) "territory" means either a whole country, a coastal zone, a continental zone or a designated farm, which is authorised by the central competent authority of the third country concerned for exportation to the Community.
Article 3
Conditions for importation of live fish, their eggs and gametes intended for farming, and of live fish of aquaculture origin for restocking of put-and take fisheries, within the European Community
1. Member States shall authorise the importation into their territory live fish, their eggs and gametes for farming only if:
(a) the fish originate in a territory listed in Annex I;
(b) the consignment complies with the guarantees, including those for packaging and labelling and the appropriate specific additional requirements, as laid down in the animal health certificate, drawn up in conformity with the model in Annex II, taking into account the explanatory notes in Annex III;
(c) the fish have been transported under conditions not altering their health status.
2. Member States shall authorise the importation into their territory live fish of aquaculture origin, their eggs and gametes intended for direct restocking of put-and take fisheries only if:
(a) the consignment comply with the rules laid down in paragraph 1;
(b) the put and take fishery do not represent lakes or unenclosed waters.
3. Member States shall ensure that imported fish of aquaculture origin, their eggs and gametes intended for farming or restocking of put-and take fisheries in Community waters, only are introduced into farms or put-and take fisheries representing ponds, and not introduced into unenclosed waters.
4. Member States shall ensure that imported live fish or aquaculture origin, their eggs and gametes are transported directly to the farm or pond of destination, as stated on the animal health certificate.
Article 4
Conditions related to importation of live fish of aquaculture origin for human consumption
Member States shall authorise the importation into their territory live fish of aquaculture origin intended for immediate human consumption or for further processing before human consumption, only if:
(a) the consignment complies with the conditions laid down in Article 3 paragraph 1 and Article 7 paragraph 1 of this Decision; or
(b) the fish are sent directly to an approved import centre to be slaughtered and eviscerated.
Article 5
Conditions related to importation of fish products of aquaculture origin for further processing before human consumption
1. Member States shall authorise the importation into their territory fish products of aquaculture origin intended for further processing before human consumption only if:
(a) the fish originate in third countries and establishments authorised under Article 11 of Directive 91/493/EEC and comply with the public health certification requirements laid down under that Directive; and
(b) the consignment complies with the guarantees, including those for packaging and labelling and the appropriate specific additional requirements, as laid down in the animal health certificate, drawn up in conformity with the model in Annex IV, taking into account the explanatory notes in Annex III.
2. Member States shall ensure that processing of fish products of aquaculture origin takes place in approved import centres unless:
(a) the fish are eviscerated before dispatch; or
(b) the place of origin has a health status equivalent to the place where they are to be processed in particular as regards epizootic haematopoietic necrosis (EHN) and the diseases referred to in lists I and II, column 1, of Annex A to Directive 91/67/EEC.
Article 6
Conditions related to importation of fish products of aquaculture origin for immediate human consumption
Member States shall authorise the importation into their territory of fish products of aquaculture origin intended for immediate human consumption only if:
(a) the fish originate in third countries and establishments authorised under Article 11 of Directive 91/493/EEC and comply with the public health certification requirements laid down under that Directive;
(b) the consignment complies with the guarantees, including those for packaging and labelling as laid down in the animal health certificate, drawn up in conformity with the model in Annex V, taking into account the explanatory notes in Annex III;
(c) the consignment consists of consumer-ready packages of a size suitable for retail sale directly to the end consumer, like
(i) vacuum packed filets,
(ii) hermetically sealed or other heat-treated products,
(iii) frozen blocks of fish meat,
(iv) eviscerated fish frozen or placed on ice.
Article 7
Certification
1. In the case of live fish, their eggs and gametes, the competent authority at the border inspection post in the Member State of arrival shall complete the document referred to in Annex of Commission Decision 92/527/EEC(10) with one of the statements laid down in Annex VI to this Decision as appropriate.
2. In the case of fish products of aquaculture origin, the competent authority at the border inspection post in the Member State of arrival shall complete the document referred to in Annex B to Commission Decision 93/13/EEC(11) with one of the statements laid down in Annex VI of this Decision as appropriate.
Article 8
Preventing contamination of natural waters
1. Member States shall ensure that imported live fish of aquaculture origin and products thereof intended for human consumption are not introduced into, and do not contaminate any natural waters within their territory.
2. Members States shall ensure that transport water from imported consignments does not lead to contamination of natural waters within their territory.
Article 9
Approval of import centres
1. The competent authority of the Member States shall approve an establishment as an approved import centre provided that it satisfies the minimum animal health conditions of Annex VII to this Decision.
2. The competent authority of the Member State shall draw up a list of approved import centres, each of which shall be given an official number.
3. The list of approved import centres, and any subsequent amendments thereto, shall be communicated by the competent authority of each Member State to the Commission and to the other Member States.
Article 10
Date of application
This Decision shall apply from 1 May 2004.
Article 11
This Decision is addressed to the Member States.
Done at Brussels, 21 November 2003.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 46, 19.2.1991, p. 1.
(2) OJ L 122, 16.5.2003, p. 1.
(3) OJ L 175, 19.7.1993, p. 23.
(4) OJ L 99, 10.4.2001, p. 11.
(5) OJ L 67, 9.3.2001, p. 65.
(6) OJ L 156, 25.6.2003, p. 61.
(7) OJ L 268, 24.9.1991, p. 15.
(8) OJ L 13, 16.1.1997, p. 28.
(9) OJ L 18, 23.1.2003, p. 11.
(10) OJ L 332, 18.11.1992, p. 22.
(11) OJ L 9, 15.1.1993, p. 33.
ANNEX I
>PIC FILE= "L_2003324EN.004202.TIF">
ANNEX II
>PIC FILE= "L_2003324EN.004302.TIF">
>PIC FILE= "L_2003324EN.004401.TIF">
>PIC FILE= "L_2003324EN.004501.TIF">
>PIC FILE= "L_2003324EN.004601.TIF">
>PIC FILE= "L_2003324EN.004701.TIF">
>PIC FILE= "L_2003324EN.004801.TIF">
ANNEX III
>PIC FILE= "L_2003324EN.004902.TIF">
ANNEX IV
>PIC FILE= "L_2003324EN.005002.TIF">
>PIC FILE= "L_2003324EN.005101.TIF">
ANNEX V
>PIC FILE= "L_2003324EN.005202.TIF">
ANNEX VI
Statements to be issued by the competent authority at the border inspection post to complete the document referred to in the Annex to Decision 92/527/EEC or in the Annex B of Decision 93/13/EEC
The competent authority at the border inspection post in the Member State of arrival shall complete the document referred to in the Annex of Decision 92/527/EEC or of Annex B of Decision 93/13/EEC with one of the following statements as appropriate:
A. Statements to be added to the document referred to in the Annex of Decision 92/527/EEC as regards live fish, their eggs and gametes intended for farming, and live fish of aquaculture origin for restocking of put-and take fisheries, in the European Community
either:
"(Live fish)(1) (and)(2) (Eggs)(3) (and)(4) (Gametes)(5) certified for farming in European Community zones and farms except those with a Community approved programme or status, additional guarantees or protective measures with regard to: viral haemorrhagic septicaemia (VHS), and infectious haematopoietic necrosis (IHN), and spring viraemia of carp (SVC), and Gyrodactylus salaris."
or:
"Live fish of aquaculture origin certified for restocking of put-and take fisheries in European Community zones and farms except those with a Community approved programme or status, additional guarantees or protective measures with regard to: viral haemorrhagic septicaemia (VHS), and infectious haematopoietic necrosis (IHN), and spring viraemia of carp (SVC), and Gyrodactylus salaris."
or:
"(Live fish)(6) (and)(7) (Eggs)(8) (and)(9) (Gametes)(10) certified for farming in European Community zones and farms including those with a Community approved programme or status, additional guarantees or protective measures with regard to: (viral haemorrhagic septicaemia (VHS))(11) (and)(12) (infectious haematopoietic necrosis (IHN))(13) (and)(14) (spring viraemia of carp (SVC))(15) (and)(16) (Gyrodactylus salaris)(17)."
or:
"Live fish of aquaculture origin certified for restocking of put-and take fisheries in European Community zones and farms including those with a Community approved programme or status, additional guarantees or protective measures with regard to: (viral haemorrhagic septicaemia (VHS))(18) (and)(19) (infectious haematopoietic necrosis (IHN))(20) (and)(21) (spring viraemia of carp (SVC))(22) (and)(23) (Gyrodactylus salaris)(24)."
B. Statements to be added to the document referred to in the Annex B of Decision 93/13/EEC as regards fish products of aquaculture origin intended for human consumption
either:
"Uneviscerated fish products of aquaculture origin certified for export to the European Community (except to zones with Community approved status as regards (VHS)(25) (and)(26) (IHN)(27))(28), for further processing (in approved import centres)(29) before human consumption."
or
"Eviscerated fish products of aquaculture origin certified for export to the European Community, for further processing before human consumption."
or
"Fish products of aquaculture origin certified for export to the European Community for immediate human consumption.".
(1) Retain as appropriate.
(2) Retain as appropriate.
(3) Retain as appropriate.
(4) Retain as appropriate.
(5) Retain as appropriate.
(6) Retain as appropriate.
(7) Retain as appropriate.
(8) Retain as appropriate.
(9) Retain as appropriate.
(10) Retain as appropriate.
(11) Retain as appropriate.
(12) Retain as appropriate.
(13) Retain as appropriate.
(14) Retain as appropriate.
(15) Retain as appropriate.
(16) Retain as appropriate.
(17) Retain as appropriate.
(18) Retain as appropriate.
(19) Retain as appropriate.
(20) Retain as appropriate.
(21) Retain as appropriate.
(22) Retain as appropriate.
(23) Retain as appropriate.
(24) Retain as appropriate.
(25) Retain as appropriate.
(26) Retain as appropriate.
(27) Retain as appropriate.
(28) Retain as appropriate.
(29) Retain as appropriate.
ANNEX VII
Minimum animal health conditions for the approval of "approved import centres" for processing of fish of aquaculture origin
A. General provisions
1. Member States shall only approve establishments as import centres for further processing of imported live fish of aquaculture origin and products thereof provided that the conditions at the import centre are such that risks of contamination of fish in Community waters, with pathogens capable of causing significant impact to fish stock, via discharges or other waste, or by other means, are avoided.
2. Establishments approved as "approved import centre", must not be allowed to move live fish out of the establishment.
3. In addition to the appropriate public health provisions laid down under Directive 91/493/EEC for any establishments, as well as health rules laid down by Community legislation concerning animal by-products not intended for human consumption, the minimum animal health conditions as laid down below, shall apply.
B. Management provisions
1. Approved import centres must be open to inspection and control by the competent authority at all times.
2. Approved import centres must have an efficient disease control, and monitoring system; in application of Council Directive 93/53/EEC, cases of suspected disease and mortality shall be investigated by the competent authority; the necessary analysis and treatment must be carried out in consultation with and under the control of the competent authority, taking into consideration the requirement in Article 3(1)(a) of Directive 91/67/EEC.
3. Approved import centres must apply a management system, approved by the competent authority, including hygiene and disposal routines for transports, transport containers, facilities, and equipment. The guidelines laid down for disinfection of fish farms in the OIE International Aquatic Animal Health Code, Sixth Edition, 2003, Appendix 5.2.2, should be followed. The disinfectants used must be approved for the purpose by the competent authority and appropriate equipment must be available for cleaning and disinfection. Discharges of by-products and other waste materials including dead fish and their products must be carried out in accordance with Regulation (EC) No 1774/2002 of the European Parliament and of the Council(1). The management system at the approved import centre shall be such that risks of contamination of fish in Community waters with pathogens capable of causing significant impact to fish stock, in particular as regards pathogens exotic to the Community and the fish pathogens referred to in list I and II, column 1, of Annex A to Directive 91/67/EEC, are avoided.
4. Approved import centres must keep an updated record of: observed mortality; and of all the live fish, eggs and gametes entering the centre and products leaving the centre including their source, their suppliers and their destination. The record should be open to scrutiny by the competent authority at all times.
5. Approved import centres must be cleaned and disinfected regularly in accordance with the programme described in point 3 above.
6. Only authorised persons may enter approved import centres and must wear protective clothing including appropriate footwear.
(1) OJ L 273, 10.10.2002, p. 1.
