COMMISSION REGULATION (EEC) No 554/78 of 17 March 1978 amending Regulation (EEC) No 2182/77 as regards the sale of frozen beef from intervention stocks for processing in the Community
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 805/68 of 27 June 1968 on the common organization of the market in beef and veal (1), as last amended by Regulation (EEC) No 425/77 (2), and in particular Article 7 (3) thereof,
Whereas, when hindquarters of beef are sold pursuant to Commission Regulation (EEC) No 2182/77 of 30 September 1977 laying down detailed rules for the sale of frozen beef from intervention stocks for processing in the Community and amending Regulation (EEC) No 1687/76 (3), provision should be made to permit the removal before processing of certain cuts, the price and quality of which do not justify processing ; whereas Regulation (EEC) No 2182/77 should therefore be amended;
Whereas Article 4 (1) of Regulation (EEC) No 2182/77 provides for the lodging, before the contract of sale is concluded of a security designed to guarantee that the products will be processed ; whereas experience has shown the need to fix a deadline for the lodging of this security so as to prevent prospective purchasers from delaying the conclusion of contracts of sale unduly ; whereas, furthermore, it is necessary to adopt a transitional measure for transactions currently being negotiated;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Beef and Veal,
HAS ADOPTED THIS REGULATION:
Article 1
1. In Article 1 (2) and in the Annex to Regulation (EEC) No 2182/77, the word "frozen" is deleted.
2. The following paragraph is added to Article 1 of Regulation (EEC) No 2182/77:
"4. For the purposes of this Regulation, the processing of hindquarters may be undertaken after the removal of the following cuts : fillet and sirloin. In that case, 100 kilograms of bone-in hindquarters, after removal of the fillet and sirloin, shall be deemed to be the equivalent of 66 kilograms of boned meat."
Article 2
The first subparagraph of Article 4 (1) of Regulation (EEC) No 2182/77 is amended to read as follows:
"1. A security calculated to guarantee that the products will be processed shall be lodged with the competent authority of the Member State where the processing is to take place before the contract of sale is concluded and at the latest within two weeks after submission of the purchase application or offer. It shall be lodged in the national currency of that Member State."
Article 3
In the case of purchase applications or offers submitted under Regulation (EEC) No 2182/77, the security referred to in Article 4 (1) of the said Regulation shall be lodged not later than 31 March 1978.
Article 4
This Regulation shall enter into force on 20 March 1978.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 17 March 1978.
For the Commission
Finn GUNDELACH
Vice-President (1)OJ No L 148, 28.6.1968, p. 24. (2)OJ No L 61, 5.3.1977, p. 1. (3)OJ No L 251, 1.10.1977, p. 60.
