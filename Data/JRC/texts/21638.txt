COMMISSION DECISION of 24 June 1992 amending Annex III to Council Directive 90/539/EEC on animal health conditions governing intra-Community trade in, and imports from third countries of, poultry and hatching eggs as regards poultry vaccination conditions (92/369/EEC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Directive 90/539/EEC of 15 October 1990 on animal health conditions governing intra-Community trade in, and imports from third countries of, poultry and hatching eggs (1), as last amended by Directive 91/496/EEC (2), and in particular Article 34 thereof,
Whereas Annex III to Directive 90/539/EEC presently requires, in particular, that poultry for intra-Community trade are vaccinated with vaccines that conform to the requirements of the European Pharmacopoeia;
Whereas European Pharmacopoeia monographs are not available for many poultry vaccines currently in use in Member States;
Whereas it is desirable to amend the said Annex to permit the use of vaccines which are not necessarily the subject of European Pharmacopoeia monographs;
Whereas the measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
Annex III to Directive 90/539/EEC is replaced by the Annex to this Decision.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 24 June 1992. For the Commission
Ray MAC SHARRY
Member of the Commission
(1) OJ No L 303, 31. 10. 1990, p. 6. (2) OJ No L 268, 24. 9. 1991, p. 56.
ANNEX
'ANNEX III
POULTRY VACCINATION CONDITIONS
1. Vaccines used for vaccinating poultry or flocks producing hatching eggs must have a marketing authorization issued by the competent authoritiy of the Member State in which the vaccine is used.
2. The criteria for using vaccines against Newcastle disease in the context of routine-vaccination programmes may be determined by the Commission.'
