Council Decision
of 11 May 2004
abrogating the decision on the existence of an excessive deficit in Portugal
(2005/135/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 104(12) thereof,
Having regard to the recommendation from the Commission,
Whereas:
(1) By Council Decision 2002/923/EC [1], following a recommendation from the Commission in accordance with Article 104(6) of the Treaty, it was decided that an excessive deficit existed in Portugal.
(2) In accordance with Article 104(7) of the Treaty, the Council made a Recommendation addressed to Portugal with a view to bringing the excessive deficit situation to an end [2]. That Recommendation, in conjunction with Article 3(4) of Council Regulation (EC) No 1467/97 of 7 July 1997 on speeding up and clarifying the implementation of the excessive deficit, procedure [3], established a deadline for the correction of the excessive deficit, which should be completed in the year following its identification, i.e. 2003 at the latest.
(3) In accordance with Article 104(12) of the Treaty, a Council decision on the existence of an excessive deficit is to be abrogated when the excessive deficit in the Member State concerned has, in the view of the Council, been corrected.
(4) The definitions of "government" and "deficit" are laid down in the Protocol on the excessive deficit procedure by reference to the European System of Integrated Economic Accounts (ESA), second edition. The data for the excessive deficit procedure are provided by the Commission.
(5) Based on the data provided by the Commission after reporting by Portugal before 1 March 2004 in accordance with Council Regulation (EC) No 3605/93 of 22 November 1993 on the application of the Protocol on the excessive deficit procedure annexed to the Treaty establishing the European Community [4], and on the Commission Spring 2004 forecast, the following conclusions are warranted:
- The general government deficit is estimated at 2,8 % of GDP in 2003, compared with 2,7 % in 2002 and 4,4 % in 2001. The outcome for 2003 complied with the Council Recommendation issued under Article 104(7), particularly as regards the reduction of the government deficit below the reference value of 3 % of GDP by 2003 at the latest. Fiscal adjustment was pursued in 2003 on the back of a sustained deceleration in the pace of total current primary expenditure growth from 8,9 % in 2001 to 7,8 % in 2002 and 4,1 % in 2003. However, the current cyclical downturn, which ended in a recession in 2003, led to a significant deviation of 2,6 percentage points between the GDP growth outcome for the year and the initial budgetary projection. As a result, a massive shortfall in tax revenue developed during 2003, which had to be offset by the adoption of two one-off measures, together worth 2,1 % of GDP.
- The structural measures taken by the Portuguese authorities, having a more direct impact on public finances, fall mainly on three areas: (i) public administration; (ii) the healthcare sector; and (iii) education. In particular, the quasi-freeze of wage scales and employment in the civil service in the period 2003-2004 is expected to have favourable base effects in the future, thereby having a significant structural impact. In addition, the Portuguese authorities estimate that the ongoing comprehensive reform in the healthcare sector has already had, in 2003, some positive effects on both expenditure savings and productivity gains.
- The Commission 2004 Spring forecast projects a general government deficit of 3,4 % of GDP for 2004, thereby significantly above the official target of a deficit of 2,8 % of GDP. The difference can basically be accounted for by: (i) somewhat lower growth than assumed in the budget; (ii) base effects associated with the one-off measures taken in 2003; and (iii) the planned partial replacement so far of such one-off measures. Therefore, additional measures are needed in order to prevent the government deficit from rising above the 3 % of GDP reference value in 2004 and following years.
- After the cut-off date for the Commission Spring 2004 forecast, the Portuguese authorities made public their intention to carry out further (real-estate related) operations to allow the deficit to stay below 3 % of GDP in the current year.
- According to the values reported in the first 2004 EDP notification, the government debt ratio was kept below the 60 % of GDP reference value in 2003, thereby in accordance with the Council Recommendation issued under Article 104(7), although it has steadily increased since 2001, and according to the Commission Spring 2004 forecast, is projected to exceed that value in 2004.
(6) Decision 2002/923/EC should therefore be abrogated. However, in the light of the risks to the budgetary position highlighted by the Commission Spring 2004 forecast, it is of the utmost importance that the Portuguese authorities take the appropriate measures to ensure that the general government deficit remains below 3 % of GDP in 2004 and beyond. Given the continued sizeable negative output gap projected up to 2005, and in order to maintain the momentum of budgetary consolidation, recourse to further temporary measures is acceptable in the short-term. In this regard, the Portuguese authorities should publicly confirm the measures planned and their respective amounts, until measures of a more structural nature exert their full exonerating effect on public finances.
(7) For the consolidation to be sustained and in order to eventually achieve the medium-term objective of a budgetary position of close to balance or in surplus, in line with the broad economic policy guidelines, all one-off measures should be gradually replaced by measures of a more permanent nature, while the cyclically adjusted budgetary position should improve by at least 0,5 of a percentage point of GDP per year,
HAS ADOPTED THIS DECISION:
Article 1
From an overall assessment it follows that the correction of the excessive deficit situation in Portugal was completed in 2003, under the terms of the Recommendation addressed to Portugal on 5 November 2002 in accordance with Article 104(7) of the Treaty.
Article 2
Decision 2002/923/EC is hereby abrogated.
Article 3
This Decision is addressed to the Portuguese Republic.
Done at Brussels, 11 May 2004.
For the Council
The President
C. McCreevy
[1] OJ L 322, 27.11.2002, p. 30.
[2] Council Recommendation of 5 November 2002.
[3] OJ L 209, 2.8.1997, p. 6.
[4] OJ L 332, 31.12.1993, p. 7. Regulation as last amended by Regulation (EC) No 351/2002 (OJ L 55, 26.2.2002, p. 23).
--------------------------------------------------
