Commission Decision
of 29 September 2006
conferring management of aid on implementing agencies for pre-accession measures in agriculture and rural development in Croatia in the pre-accession period
(2006/658/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to the Council Regulation (EC) No 1266/1999 of 21 June 1999 on co-ordinating aid to the applicant countries in the framework of the pre-accession strategy and amending Regulation (EEC) No 3906/1989 [1], and in particular Article 12(2) thereof,
Having regard to the Council Regulation (EC) No 1268/1999 of 21 June 1999 on Community support for pre-accession measures for agriculture and rural development in the applicant countries of central and eastern Europe in the pre-accession period [2], and in particular Article 4(5) and (6) thereof,
Whereas:
(1) The Rural Development Programme for Croatia was approved by Commission Decision C(2006) 301 of 8 February 2006, in accordance with Article 4(5) and (6) of Regulation (EC) No 1268/1999.
(2) The Croatian government and the Commission, acting on behalf of the European Community, signed on 29 December 2005 the Multi-annual Financing Agreement (hereinafter MAFA) laying down the technical, legal and administrative framework for the execution of the Sapard Programme.
(3) The Republic of Croatia notified the Commission of the completion of all the necessary internal procedures for its conclusion on 6 April 2006, what is the date of entry into force of the MAFA.
(4) Regulation (EC) No 1266/1999 provides that the ex-ante approval requirement referred to in Article 12(1) of Regulation (EC) No 1266/1999 may be waived on the basis of a case-by-case analysis of the national and sectorial programme/project management capacity, financial control procedures and structures regarding public finance. Commission Regulation (EC) No 2222/2000 [3] provides for detailed rules for the carrying out of the said analysis.
(5) The Competent Authority of Croatia has appointed the Directorate for Market and Structural Support in Agriculture, an organisational unit of the Ministry of Agriculture, Forestry and Water Management, acting as the Sapard Agency. It will be responsible for implementing the following measures: No 1 "Investments into Agricultural Holdings" and No 2 "Improving the Processing and Marketing of Agricultural and Fishery Products" as defined in the Rural Development Programme that was approved by Decision C(2006) 301; whereas the National Fund, within the Ministry of Finance, has been appointed for the financial functions it is due to perform in the framework of the implementation of the Sapard Programme.
(6) Pursuant to Regulation (EC) No 1266/1999 and Regulation (EC) No 2222/2000, the Commission analysed the national and sectorial programme/project management capacity, financial control procedures and structures regarding public finance and considers that, for the implementation of the aforementioned measures, Croatia complies with the provisions of Articles 4 to 6 and of the Annex to the Regulation (EC) No 2222/2000, with the minimum conditions set out in the Annex to the Regulation (EC) No 1266/1999.
(7) In particular, the Sapard Agency has implemented the following key accreditation criteria satisfactorily: written procedures, segregation of duties, pre-project approval and pre-payment checks, payment procedures, accounting procedures and internal audit.
(8) On 14 March 2006 the Croatian Authorities provided the list of eligible expenditure in conformity with Article 4(1), Section B of the MAFA. This list was partially modified by letter of 11 July 2006. The Commission is called upon to take a decision in this respect.
(9) The National Fund within the Ministry of Finances has implemented the following criteria satisfactorily for the financial functions it is due to perform in the framework of the implementation of the Sapard programme for Croatia: audit trail, treasury management, receipt of funds, disbursement to the Sapard Agency and internal audit.
(10) It is therefore appropriate to waive the ex-ante approval requirement referred to in Article 12(1) of Regulation (EC) No 1266/1999 and to confer on the Sapard Agency and on the National Fund of Croatia the management of aid on a decentralised basis.
(11) However, since the verifications carried out by the Commission for measure No 1 "Investments into Agricultural Holdings" and No 2 "Improving the Processing and Marketing of Agricultural and Fishery Products" are based on a system that is not yet fully operating with regard to all relevant elements, it is therefore appropriate to confer the management of the Sapard Programme on the Sapard Agency and on the Ministry of Finances, National Fund, according to Article 3(2) of Regulation (EC) No 2222/2000, on a provisional basis.
(12) Full conferral of management of the Sapard is only envisaged after further verifications, in order to ensure that the system operates satisfactorily, have been carried out and after any recommendations the Commission may issue, with regard to the conferral of management of aid on the Sapard Agency, in the subordination of the Ministry of Agriculture, Forestry and Water Management and on the Ministry of Finances, National Fund, have been implemented.
(13) In order to take into account the requirements of Article 8(1)b, Section A of the MAFA, the expenditures pursuant of this Decision shall be eligible for Community co-finance only if incurred by beneficiaries from the date of this Decision or, if later, the date of the instrument making them a beneficiary for the project in question, except that for feasibility and related studies, provided in all cases it is not paid by the Sapard Agency prior to the date of this Decision.
HAS DECIDED AS FOLLOWS:
Article 1
The requirement of ex ante approval by the Commission of project selection and contracting for measures No 1 "Investments into Agricultural Holdings" and No 2 "Improving the Processing and Marketing of Agricultural and Fishery Products" by Croatia for in Article 12(1) of Regulation (EC) No 1266/1999 is hereby waived.
Article 2
Management of the Sapard programme is conferred on a provisional basis:
1. Directorate for Market and Structural Support in Agriculture, an organisational unit of the Ministry of Agriculture, Forestry and Water Management, acting as the Sapard Agency of Croatia, Avenija grada Vukovara 269D, 10000 Zagreb, for the implementation of measures No 1 "Investments into Agricultural Holdings" and No 2 "Improving the Processing and Marketing of Agricultural and Fishery Products" as defined in the Rural Development Programme that was approved by Decision C(2006) 301.
2. The National Fund within the Ministry of Finances, Katančičeva 5, 10000 Zagreb, for the financial functions it is due to perform in the framework of the implementation of the Sapard programme for Croatia.
Article 3
Expenditures pursuant to this Decision shall be eligible for Community co-finance only if incurred by beneficiaries from the date of this Decision or, if later, the date of the instrument making them a beneficiary for the project in question, except that for feasibility and related studies, provided in all cases it is not paid by the Sapard Agency prior to the date of this Decision.
Article 4
Without prejudice to any decisions granting aid under the Sapard programme to individual beneficiaries, the rules for eligibility of expenditure proposed by Croatia by letter No "Klasa: 910-01/05–01/8, Urbroj: 513-05-06/06-28" of 14 March 2006 and registered in the Commission on 21 March 2006 under No 08347, as modified by letter No "Klasa: 910-01/06–01/221, Urbroj: 513-05-06/06-9" of 23 June 2006 and registered in the Commission on 11 July 2006 under No 20627, shall apply.
Done at Brussels, 29 September 2006.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 161, 26.6.1999, p. 68.
[2] OJ L 161, 26.6.1999, p. 87. Regulation as last amended by Regulation (EC) No 2112/2005 (OJ L 344, 27.12.2005, p. 23).
[3] OJ L 253, 7.10.2000, p. 5.
--------------------------------------------------
