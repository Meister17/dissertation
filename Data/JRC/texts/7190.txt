Recommendation No 1/2005 of the EU-Israel Association Council
of 26 April 2005
on the implementation of the EU-Israel Action Plan
(2005/641/EC)
THE EU-ISRAEL ASSOCIATION COUNCIL,
Having regard to the Euro-Mediterranean Agreement establishing an Association between the European Communities and their Member States, of the one part, and the State of Israel, of the other part, and in particular Article 69 thereof,
Whereas:
(1) Article 69 of the Euro-Mediterranean Agreement gives the Association Council the power to make appropriate recommendations for the purpose of attaining the objectives of the Agreement.
(2) In terms of Article 79 of the Euro-Mediterranean Agreement, the Parties are to take any general or specific measures required to fulfil their obligations under the Agreement and are to see to it that the objectives set out in the Agreement are attained.
(3) The EU-Israel Action Plan will support the implementation of the Euro-Mediterranean Agreement through the elaboration and agreement between the Parties of concrete steps which will provide practical guidance for such implementation.
(4) The Action Plan serves the dual purpose of setting out concrete steps for the fulfilment of the Parties’ obligations set out in the Euro-Mediterranean Agreement, and of providing a broader framework for further strengthening EU-Israel relations to involve a significant measure of economic integration and a deepening of political cooperation, in accordance with the overall objectives of the Euro-Mediterranean Agreement.
(5) The Parties to the Euro-Mediterranean Agreement have agreed on the text of the EU-Israel Action Plan,
HAS ADOPTED THE FOLLOWING RECOMMENDATION:
Sole Article
The Association Council recommends that the Parties implement the EU-Israel Action Plan [1], insofar as such implementation is directed towards attainment of the objectives of the Euro-Mediterranean Agreement.
Done at Brussels, 26 April 2005.
For the Association Council
The President
S. Shalom
[1] http://register.consilium.eu.int
--------------------------------------------------
