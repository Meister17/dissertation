Commission Regulation (EC) No 1285/2001
of 28 June 2001
rejecting a list of applications for the registration of designations communicated under Article 17 of Council Regulation (EEC) No 2081/92 on the protection of geographical indications and designations of origin for agricultural products and foodstuffs
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2081/92 of 14 July 1992 on the protection of geographical indications and designations of origin for agricultural products and foodstuffs(1), as last amended by Commission Regulation (EC) No 2796/2000(2), and in particular Article 17 thereof,
Whereas:
(1) A decision on 314 German applications, communicated under Article 17(2) of Regulation (EEC) No 2081/92 for the registration of "mineral waters" as protected designations of origin, is pending.
(2) In the case of at least 125 of these 314 applications, the proposed names are not geographical and they are consequently not "designations of origin", as specified in Article 2(a) of that Regulation.
(3) Article 2(3) of Regulation (EEC) No 2081/92 allows under certain circumstances to consider traditional geographical or non-geographical names as designations of origin. This Article, which is an exception to the general rule, may not be applied to any of these 125 applications, as they do not clearly justify that the proposed designations have been traditionally attributed to a particular geographical area. Accordingly these 125 applications may not be registered.
(4) In the case of 15 of the 314 German applications communicated under Article 17(2) of Regulation (EEC) No 2081/92 for registrations of "mineral waters", the proposed names are geographical but include "numbers", the latter helping to distinguish between "mineral waters" having the same designations. Designations of origin including "numbers" shall not be accepted, being the objective of the Regulation the protection of only geographical names. Accordingly these 15 applications may not be registered.
(5) The measures provided for in this Regulation are in accordance with the opinion of the Regulatory Committee on Geographical Indications and Designations of Origin,
HAS ADOPTED THIS REGULATION:
Article 1
The applications for registrations under Article 17 of Regulation (EEC) No 2081/92 of the designations listed in the Annex to this Regulation are rejected.
Article 2
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 28 June 2001.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 208, 24.7.1992, p. 1.
(2) OJ L 324, 21.12.2000, p. 25.
ANNEX
Natural mineral waters and spring waters
GERMANY
Adldorfer Dreibogen-Quelle (M2)
Adelheidquelle
Aegidiusbrunnen
Albertusquelle
Alosa
Alstertaler Mineralbrunnen
Alt-Bürger-Brunn
Andreas Quelle
Antonius-Quelle
Apollinaris
Apollo-Quelle
Aquella
Ariston Mineralwasser
Assindia-Quelle
Astra-Quelle
Augusta Victoria Quelle
Augustinus-Quelle
Bad Driburger Mineralquelle I
Badestädter Mineralquelle
Basinus Quelle
Brunnen III (Hunsrück-Quelle)
Burg-Quelle
Centgraf Stilles Mineralwasser
Centgraf-Brunnen
Dauner Heilquelle, Heilwasser aus der Dauner Quelle IV
Dauner Quelle I
Dauner Quelle II
Dauner Quelle III
Dillenius-Quelle
Drachenquelle
Dreikönigsquelle
Elisabethenquelle
Elisabethen-Quelle
Eltina-Quelle
Erwinaris Mineralbrunnen
Filippo Mineralsprudel
Florian-Quelle
Förstina Sprudel Urquelle
Fortis
Fortuna-Quelle
Fürstenbrunn
Fürstenquelle
Goldrausch-Brunnen
Graf Metternich Quelle
Graf Metternich Varus-Quelle
Graf Simeon Quelle
Granus-Quelle
Haranni Stille
Haranni-Quelle
Hassia Leicht
Hassia-Sprudel
Heerbach-Mineralbrunnen
Helenen-Quelle
Hella
Hellweg Quelle Mineralbrunnen
Henri-Klinkert-Brunnen
Herminenquelle
Herzog-Wigbert-Quelle
Hetalli Quelle
Hubertussprudel
Irisquelle
Jakobbrunnen
Jakobus
Johannis Quell
Johannisquelle
Johanniter Quelle
Josefsquelle
Kaiser-Quelle
Kastell-Mineralwasser
Kellerwald-Quelle 1
Kimi Quelle
Klosterquelle
König Otto-Sprudel
König-Ludwig-I-Quelle
Königsquell
Kronen Quelle (Moers)
Kronen-Quelle (Heilbronn)
Kronia-Quelle
Kronsteiner Felsenquelle
Krönungs-Quelle
Lahnsteiner I
Lahnsteiner II
Leopoldsquelle
Linden-Brunnen
Löwensprudel
Luisen-Brunnen
Magnus-Quelle
Marienquelle
Markusbrunnen
Martinybrunnen 3
Mephisto-Quelle
Mönchsbrunnen
Mühringer Schlossquelle III
Neue Otto-Quelle
Nordquell
Original Schloss-Quelle
Park-Brunnen
Prinzenquelle
Private Quelle Grüneberg I
PurBorn
Raffelberger Mineralbrunnen
Reginaris-Mineralwasser
Reinoldus-Brunnen
Reinsteiner Quelle
Residenz-Quelle
Retzmannbrunnen
Romanis-Quelle
Romina-Quelle
Sankt Martin
Saturn-Quelle
Schloss-Quelle I
Selters Mineralquelle I-VII Selters a. d. Lahn
Seltina-Mineralbrunnen
Selzerbrunnen
Shop
Silvana Quelle
Sinnberger Quelle
St. Angari
St. Burghard
St. Conrad-Brunnen
St. Eligius-Quelle
St. Libori
Stadion
Stauferquelle
Steinbergquelle
Urbanus Mineralwasser
Victoria I
Victoria II
Vitrex-Mineralwasser
Vulkan-Quelle
Walita
Weisenbergerquelle
Wenden Quelle
Wernarzer Heilquelle
Werretaler
Wildsberg-Quelle
Wilhelmsthaler Mineralbrunnen
Wüteria Heiligenquelle Gemmingen (Brunnen 3)
Wüteria Schlossbrunnen Gemmingen (Brunnen 1)
Xaveri-Brunnen Adldorf (M1)
