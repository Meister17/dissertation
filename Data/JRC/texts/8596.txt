COMMISSION REGULATION (EC) No 728/1999
of 7 April 1999
providing, pursuant to Article 7(3) of Council Regulation (EEC) No 2847/93, for a notification period for Community fishing vessels carrying on fishing activities in the Baltic Sea, the Skagerrak and the Kattegat
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2847/93 of 12 October 1993 establishing a control system applicable to the common fisheries policy(1), as last amended by Regulation (EC) No 2846/98(2), and in particular Article 7(3) thereof,
Whereas, under Article 7(1) of Regulation (EEC) No 2847/93, the master of a Community fishing vessel who wishes to utilise landing locations in a Member State other than the flag Member State must, where there is no designated port scheme established by that Member State, inform the competent authorities in that Member State at least four hours in advance of the landing location(s) and estimated time of arrival together with the quantities of each species to be landed;
Whereas, under Article 7(3) of the above Regulation, the Commission may make provision for another notification period taking into account, inter alia, the distance between the fishing grounds, the landing locations and the ports where the vessels in question are registered or listed;
Whereas, in the light of the distance between the fishing grounds, the landing locations and the ports where the vessels in question are registered or listed, a shorter notification period is justified for Community fishing vessels carrying on fishing activities in the Baltic Sea, the Skagerrak and Kattegat which wish to land catches in the ports of certain Member States;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Fisheries and Aquaculture,
HAS ADOPTED THIS REGULATION:
Article 1
Notwithstanding Article 7(1) of Regulation (EEC) No 2847/93, the notification period to be observed by the master of a Community fishing vessel which carries on fishing activities in the Baltic Sea and the Skagerrak and Kattegat, and who wishes to use landing locations in Denmark, Germany, Sweden or Finland, shall be not less than two hours.
Article 2
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Communities.
It shall apply from 1 July 1999.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 7 April 1999.
For the Commission
Emma BONINO
Member of the Commission
(1) OJ L 261, 20.10.1993, p. 1.
(2) OJ L 358, 31.12.1998, p. 5.
