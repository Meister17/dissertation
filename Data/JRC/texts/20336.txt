*****
COUNCIL DIRECTIVE
of 10 September 1984
relating to the approximation of the laws, regulations and administrative provisions of the Member States concerning misleading advertising
(84/450/EEC)
THE COUNCIL OF THE EUROPEAN
COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 100 thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas the laws against misleading advertising now in force in the Member States differ widely; whereas, since advertising reaches beyond the frontiers of individual Member States, it has a direct effect on the establishment and the functioning of the common market;
Whereas misleading advertising can lead to distortion of competition within the common market;
Whereas advertising, whether or not it induces a contract, affects the economic welfare of consumers;
Whereas misleading advertising may cause a consumer to take decisions prejudicial to him when acquiring goods or other property, or using services, and the differences between the laws of the Member States not only lead, in many cases, to inadequate levels of consumer protection, but also hinder the execution of advertising campaigns beyond national boundaries and thus affect the free circulation of goods and provision of services;
Whereas the second programme of the European Economic Community for a consumer protection and information policy (4) provides for appropriate action for the protection of consumers against misleading and unfair advertising;
Whereas it is in the interest of the public in general, as well as that of consumers and all those who, in competition with one another, carry on a trade, business, craft or profession, in the common market, to harmonize in the first instance national provisions against misleading advertising and that, at a second stage, unfair advertising and, as far as necessary, comparative advertising should be dealt with, on the basis of appropriate Commission proposals;
Whereas minimum and objective criteria for determining whether advertising is misleading should be established for this purpose;
Whereas the laws to be adopted by Member States against misleading advertising must be adequate and effective;
Whereas persons or organizations regarded under national law as having a legitimate interest in the matter must have facilities for initiating proceedings against misleading advertising, either before a court or before an administrative authority which is competent to decide upon complaints or to initiate appropriate legal proceedings;
Whereas it should be for each Member State to decide whether to enable the courts or administrative authorities to require prior recourse to other established means of dealing with the complaint;
Whereas the courts or administrative authorities must have powers enabling them to order or obtain the cessation of misleading advertising;
Whereas in certain cases it may be desirable to prohibit misleading advertising even before it is published; whereas, however, this in no way implies that Member States are under an obligation to introduce rules requiring the systematic prior vetting of advertising;
Whereas provision should be made for accelerated procedures under which measures with interim or definitive effect can be taken;
Whereas it may be desirable to order the publication of decisions made by courts or administrative authorities or of corrective statements in order to eliminate any continuing effects of misleading advertising;
Whereas administrative authorities must be impartial and the exercise of their powers must be subject to judicial review;
Whereas the voluntary control exercised by self-regulatory bodies to eliminate misleading advertising may avoid recourse to administrative or judicial action and ought therefore to be encouraged;
Whereas the advertiser should be able to prove, by appropriate means, the material accuracy of the factual claims he makes in his advertising, and may in appropriate cases be required to do so by the court or administrative authority;
Whereas this Directive must not preclude Member States from retaining or adopting provisions with a view to ensuring more extensive protection of consumers, persons carrying on a trade, business, craft or profession, and the general public,
HAS ADOPTED THIS DIRECTIVE:
Article 1
The purpose of this Directive is to protect consumers, persons carrying on a trade or business or practising a craft or profession and the interests of the public in general against misleading advertising and the unfair consequences thereof.
Article 2
For the purposes of this Directive:
1. 'advertising' means the making of a representation in any form in connection with a trade, business, craft or profession in order to promote the supply of goods or services, including immovable property, rights and obligations;
2. 'misleading advertising' means any advertising which in any way, including its presentation, deceives or is likely to deceive the persons to whom it is addressed or whom it reaches and which, by reason of its deceptive nature, is likely to affect their economic behaviour or which, for those reasons, injures or is likely to injure a competitor;
3. 'person' means any natural or legal person.
Article 3
In determining whether advertising is misleading, account shall be taken of all its features, and in particular of any information it contains concerning:
(a) the characteristics of goods or services, such as their availability, nature, execution, composition, method and date of manufacture or provision, fitness for purpose, uses, quantity, specification, geographical or commercial origin or the results to be expected from their use, or the results and material features of tests or checks carried out on the goods or services;
(b) the price or the manner in which the price is calculated, and the conditions on which the goods are supplied or the services provided;
(c) the nature, attributes and rights of the advertiser, such as his identity and assets, his qualifications and ownership of industrial, commercial or intellectual property rights or his awards and distinctions.
Article 4
1. Member States shall ensure that adequate and effective means exist for the control of misleading advertising in the interests of consumers as well as competitors and the general public. Such means shall include legal provisions under which persons or organizations regarded under national law as having a legitimate interest in prohibiting misleading advertising may:
(a) take legal action against such advertising; and/or
(b) bring such advertising before an administrative authority competent either to decide on complaints or to initiate appropriate legal proceedings.
It shall be for each Member State to decide which of these facilities shall be available and whether to enable the courts or administrative authorities to require prior recourse to other established means of dealing with complaints, including those referred to in Article 5.
2. Under the legal provisions referred to in paragraph 1, Member States shall confer upon the courts or administrative authorities powers enabling them, in cases where they deem such measures to be necessary taking into account all the interests involved and in particular the public interest:
- to order the cessation of, or to institute appropriate legal proceedings for an order for the cessation of, misleading advertising, or
- if misleading advertising has not yet been published but publication is imminent, to order the prohibition of, or to institute appropriate legal proceedings for an order for the prohibition of, such publication,
even without proof of actual loss or damage or of intention or negligence on the part of the advertiser.
Member States shall also make provision for the measures referred to in the first subparagraph to be taken under an accelerated procedure:
- either with interim effect, or
- with definitive effect,
on the understanding that it is for each Member State to decide which of the two options to select.
Furthermore, Member States may confer upon the courts or administrative authorities powers enabling them, with a view to eliminating the continuing effects of misleading advertising the cessation of which has been ordered by a final decision:
- to require publication of that decision in full or in part and in such form as they deem adequate,
- to require in addition the publication of a corrective statement.
3. The administrative authorities referred to in paragraph 1 must:
(a) be composed so as not to cast doubt on their impartiality;
(b) have adequate powers, where they decide on complaints, to monitor and enforce the observance of their decisions effectively;
(c) normally give reasons for their decisions.
Where the powers referred to in paragraph 2 are exercised exclusively by an administrative authority, reasons for its decisions shall always be given. Furthermore in this case, provision must be made for procedures whereby improper or unreasonable exercise of its powers by the administrative authority or improper or unreasonable failure to exercise the said powers can be the subject of judicial review.
Article 5
This Directive does not exclude the voluntary control of misleading advertising by self-regulatory bodies and recourse to such bodies by the persons or organizations referred to in Article 4 if proceedings before such bodies are in addition to the court or administrative proceedings referred to in that Article.
Article 6
Member States shall confer upon the courts or administrative authorities powers enabling them in the civil or administrative proceedings provided for in Article 4:
(a) to require the advertiser to furnish evidence as to the accuracy of factual claims in advertising if, taking into account the legitimate interests of the advertiser and any other party to the proceedings, such a requirement appears appropriate on the basis of the circumstances of the particular case; and
(b) to consider factual claims as inaccurate if the evidence demanded in accordance with (a) is not furnished or is deemed insufficient by the court or administrative authority.
Article 7
This Directive shall not preclude Member States from retaining or adopting provisions with a view to ensuring more extensive protection for consumers, persons carrying on a trade, business, craft or profession, and the general public. Article 8
Member States shall bring into force the measures necessary to comply with this Directive by 1 October 1986 at the latest. They shall forthwith inform the Commission thereof.
Member States shall communicate to the Commission the text of all provisions of national law which they adopt in the field covered by this Directive.
Article 9
This Directive is addressed to the Member States.
Done at Brussels, 10 September 1984.
For the Council
The President
P. O'TOOLE
(1) OJ No C 70, 21. 3. 1978, p. 4.
(2) OJ No C 140, 5. 6. 1979, p. 23.
(3) OJ No C 171, 9. 7. 1979, p. 43.
(4) OJ No C 133, 3. 6. 1981, p. 1.
