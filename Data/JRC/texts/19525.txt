Commission Decision
of 29 September 2003
establishing pursuant to Directive 2001/18/EC of the European Parliament and of the Council a format for presenting the results of the deliberate release into the environment of genetically modified higher plants for purposes other than placing on the market
(notified under document number C(2003) 3405)
(Text with EEA relevance)
(2003/701/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Directive 2001/18/EC of the European Parliament and of the Council of 12 March 2001 on the deliberate release into the environment of genetically modified organisms and repealing Council Directive 90/220/EEC(1), and in particular the second sentence of Article 10 thereof,
Whereas:
(1) With regard to the deliberate release of genetically modified organisms (GMOs) for any purpose other than placing on the market, Article 10 of Directive 2001/18/EC requires the notifier of such a release, to send the competent authority, after completion of a release, and thereafter, at any intervals laid down in the consent on the basis of the results of the environmental risk assessment, the results of the release in terms of any risk to human health or the environment, with, where appropriate, particular reference to any kind of product that the notifier intends to notify at a later stage.
(2) To date, most GMOs deliberately released in the Community pursuant to Part B of Directive 2001/18/EC are genetically modified higher plants (GMHP). It is necessary, therefore, with regard to those plants, to establish the format to be used by the notifier when presenting the results of the release to the competent authority. That format should reflect the need to enable the fullest possible exchange of relevant information, presented in a standardised and easily comprehensible manner. The format should be kept as general as possible so that, where appropriate, multi-sites, multi-annual releases or releases of several GMOs can be covered by a single report.
(3) Since genetic engineering is not restricted to higher plants, it will be necessary to establish formats for other types of GMOs, such as genetically modified (GM) animals (including GM insects), veterinary and medicinal products (containing or consisting of GMOs) or GM plants which could produce pharmaceutical products. Future developments may also make it necessary to adapt the report formats which have already been established.
(4) The measures provided for in this Decision are in accordance with the opinion of the Committee established under Article 30 of Directive 2001/18/EC,
HAS ADOPTED THIS DECISION:
Article 1
For the purposes of presenting to the competent authority the results of the deliberate release into the environment of genetically modified higher plants (GMHP) pursuant to Article 10 of Directive 2001/18/EC, the notifier shall use the format set out in the Annex to this Decision, hereinafter "the report format".
Article 2
A report format shall relate to no more than one consent issued by the competent authority and shall be identified by a single notification number.
Article 3
1. For each notification number, a final report shall be delivered by the notifier, and final as well as intermediary post-release monitoring report(s) shall be delivered where appropriate. Both types of report shall be drawn up in accordance with the report format.
2. The final report shall be delivered after the last harvest of the GMHPs. Where no post-release monitoring is required for a notification, no further reports shall be necessary.
3. The final post-release monitoring report shall be delivered after completion of the post-release monitoring.
The competent authority shall, where appropriate, specify in the consent the duration of the post-release monitoring as well as the timetable for submission of the intermediary post-release monitoring reports.
4. The competent authority shall encourage notifiers to provide the report in an electronic form.
Article 4
The competent authority may require from the notifier additional information, in particular in the form of a logbook or interim reports, to be delivered in the course of the research programme, before the completion of a release.
Article 5
This Decision is addressed to the Member States.
Done at Brussels, 29 September 2003.
For the Commission
Margot Wallström
Member of the Commission
(1) OJ L 106, 17.4.2001, p. 1.
ANNEX
>PIC FILE= "L_2003254EN.002302.TIF">
>PIC FILE= "L_2003254EN.002401.TIF">
>PIC FILE= "L_2003254EN.002501.TIF">
>PIC FILE= "L_2003254EN.002601.TIF">
>PIC FILE= "L_2003254EN.002701.TIF">
>PIC FILE= "L_2003254EN.002801.TIF">
