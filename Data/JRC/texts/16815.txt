Commission Regulation (EC) No 498/2004
of 17 March 2004
adapting several regulations concerning the market of products processed from fruit and vegetables by reason of the accession of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia to the European Union
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to the Treaty of Accession of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia, and in particular Article 2(3) thereof,
Having regard to the Act of Accession of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia, and in particular Article 57(2) thereof,
Whereas:
(1) Certain technical amendments are necessary in several Commission Regulations of the common organisation of the market in products processed from fruit and vegetables in order to carry out the necessary adaptations by reason of the accession of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia (hereinafter the new Member States) to the European Union.
(2) Article 3(3) of Commission Regulation (EC) No 1429/95 of 23 June 1995 on implementing rules for export refunds on products processed from fruit and vegetables other than those granted for added sugars(1) contains certain entries in all the languages of the Member States. These provisions should include the language versions of the new Member States.
(3) Article 2 of Commission Regulation (EC) No 1591/95 of 30 June 1995 laying down detailed rules for the application of export refunds to glucose and glucose syrup used in certain products processed from fruit and vegetables(2) contains certain entries in all the languages of the Member States. These provisions should include the language versions of the new Member States.
(4) Article 5 of Commission Regulation (EC) No 2125/95 of 6 September 1995 opening and providing for the administration of tariff quotas for preserved mushrooms(3) contains a reference to Poland. This reference should be deleted upon the accession of the new Member States.
(5) Article 11 of Regulation (EC) No 2125/95 contains certain entries in all the languages of the Member States. These provisions should include the language versions of the new Member States.
(6) Article 2(1)(b) of Commission Regulation (EC) No 2315/95 of 29 September 1995 laying down detailed rules for the application of export refunds to certain sugars covered by the common organisation of the market in sugar used in certain products processed from fruit and vegetables(4) contains certain entries in all the languages of the Member States. These provisions should include the language versions of the new Member States.
(7) The title and Annex II of Commission Regulation (EC) No 1599/97 of 28 July 1997 laying down detailed rules for the application of the system of minimum import prices for certain soft fruits originating in Bulgaria, Hungary, Poland, Romania, Slovakia and the Czech Republic(5) contain references to several new Member States. These references should be deleted upon the accession of the new Member States.
(8) Regulations (EC) No 1429/95, (EC) No 1591/95, (EC) No 2125/95, (EC) No 2315/95 and (EC) No 1599/97 should therefore be amended accordingly,
HAS ADOPTED THIS REGULATION:
Article 1
In Article 3 of Regulation (EC) No 1429/95, paragraph 3 is replaced by the following:
"3. One of the following entries shall be made in box 22 of the licence:
- Restitución válida para ... (cantidad por la que se haya expedido el certificado) como máximo
- Náhrada platná pro nejvýse ... (mnoztví, na které byla licence udelena)
- Restitutionen omfatter højst ... (den mængde, licensen er udstedt for)
- Erstattung gültig für höchstens ... (Menge, für die die Lizenz erteilt wurde)
- Επιστροφή που ισχύει για ... (ποσότητα για την οποία εκδίδεται το πιστοποιητικό) κατ' ανώτατο όριο
- Refund valid for not more than ... (quantity for which licence issued)
- Toetus kehtib maksimaalselt ... (kogus, mille jaoks litsents on välja antud) toote kohta
- Restitution valable pour ... (quantité pour laquelle le certificat est délivré) au maximum
- A visszatérítés az alábbi maximális mennyiségre érvényes: ... (az a mennyiség, amelyre az engedélyt kiállítják)
- Restituzione valida al massimo per ... (quantitativo per il quale è rilasciato il titolo)
- Grazinamoji ismoka taikoma ne daugiau nei ... (kiekis, kuriam isduota licencija)
- Kompensacija attiecas uz ne vairak ka ... (daudzums, par ko izsniegta atlauja)
- Rifuzjoni valida gal mhux aktar minn ... (kwantità li galiha giet maruga l-licenzja)
- Restitutie voor ten hoogste ... (hoeveelheid waarvoor het certificaat is afgegeven)
- Pozwolenie wazne dla nie wiecej niz ... (ilosc, dla której wydano pozwolenie)
- Restituição válida para ... (quantidade em relação à qual é emitido o certificado), no máximo
- Náhrada platná pre maximálne ... (mnozstvo, pre ktoré je povolenie vydané)
- Nadomestilo, veljavno za najvec ... (kolicina, za katero je bilo izdano dovoljenje)
- Vientituki voimassa enintään ... (määrä, jolle todistus on annettu) osalta
- Bidrag som gäller för högst ... (kvantitet för vilken licensen skall utfärdas)."
Article 2
In Article 2 of Regulation (EC) No 1591/95, the second paragraph is replaced by the following:
"2. However, for the purpose of applying this Regulation, section 20 of the licence application and of the licence shall contain one of the following indications:
- Glucosa utilizada en uno o varios productos enumerados en la letra b) del apartado 1 del artículo 1 del Reglamento (CEE) n° 426/86
- Glukosa pouzívaná v jednom nebo více produktech uvedených v cl. 1 odst. 1 písm. b) narízení (EHS) c. 426/86
- Glucose anvendt i et eller flere af de produkter, der er nævnt i artikel 1, stk. 1, litra b), i forordning (EØF) nr. 426/86
- Glukose, einem oder mehreren der in Artikel 1 Absatz 1 Buchstabe b) der Verordnung (EWG) Nr. 426/86 genannten Erzeugnisse zugesetzt
- Γλυκόζη η οποία χρησιμοποιείται σε ένα ή περισσότερα των προϊόντων που απαριθμούνται στο άρθρο 1 παράγραφος 1 στοιχείο β) του κανονισμού (EOK) αριθ. 426/86
- Glucose used in one or more products as listed in Article 1(1)(b) of Regulation (EEC) No 426/86
- Glükoos, mida on kasutatud ühes või mitmes määruse (EMÜ) nr 426/86 artikli 1 lõike 1 punktis b loetletud tootes
- Glucose mis en oeuvre dans un ou plusieurs produits énumérés à l'article 1er, paragraphe 1, point b), du règlement (CEE) n° 426/86
- A 426/86/EGK rendelet 1. cikke (1) bekezdésének b) pontjában felsorolt egy vagy több termékben felhasznált glükóz
- Glucosio incorporato in uno o più prodotti di cui all'articolo 1, paragrafo 1, lettera b), del regolamento (CEE) n. 426/86
- Gliukoze naudojama viename ar daugiau produktu isvardytu Reglamento (EEB) Nr. 426/86 1 straipsnio 1 dalies b punkte
- Glikoze, ko izmanto viena vai vairakos produktos, kuri uzskaititi Regulas (EEK) Nr. 426/86 1. panta 1. punkta b) apakspunkta
- Glukozju wzat f' prodott wieed jew aktar elenkati fl-Artikolu 1 (1) (b) tar-Regolament (KEE) Nru 426/86
- Glucose, verwerkt in een of meer van de in artikel 1, lid 1, onder b), van Verordening (EEG) nr. 426/86 genoemde producten
- Glukoza zastosowana w jednym lub wiecej produktach wymienionych w art. 1 ust. 1 lit. b) rozporzadzenia (EWG) nr 426/86.
- Glicose utilizada num ou mais produtos enumerados no n.o 1, alínea b), do artigo 1.o do Regulamento (CEE) n.o 426/86
- V jednom alebo viacerých produktoch vymenovaných v clánku 1, odsek 1, bod b) nariadenia (EHS) c. 426/86 sa pouzila glukóza
- Glukoza, dodana enemu ali vec proizvodov, navedenih v clenu 1(1)(b) Uredbe (EGS) st. 425/86
- Yhdessä tai useammassa asetuksen (ETY) N:o 426/86 1 artiklan 1 kohdan b alakohdassa luetellussa tuotteessa käytetty glukoosi
- Glukos som tillsätts i en eller flera av produkterna i artikel 1.1 b i förordning (EEG) nr 426/86."
Article 3
Regulation (EC) No 2125/95 is amended as follows:
1. in Article 5(1), the word "Poland" is deleted;
2. in Article 11, paragraph 1 is replaced by the following:
"1. Import licences shall show the following in box 24, in one of the official languages of the European Union:
- Derecho de aduana ... % - Reglamento (CE) n° 2125/95
- Celní sazba ... % - narízení (ES) c. 2125/95
- Toldsats ... % - forordning (EF) nr. 2125/95
- Zollsatz ... % - Verordnung (EG) Nr. 2125/95
- Δασμός ... % - Κανονισμός (ΕΚ) αριθ. 2125/95
- Customs duty ... % - Regulation (EC) No 2125/95
- Tollimaks ... % - määrus (EÜ) nr 2125/95
- Droit de douane: ... % - Règlement (CE) n° 2125/95
- Vám: ... % - 2125/95/EK rendelet
- Dazio: ... % - Regolamento (CE) n. 2125/95
- Muito mokestis ... % - Direktyva (EB) Nr. 2125/95
- Muitas nodoklis ... % - Regula (EK) Nr. 2125/95
- Dazju Doganali ... % - Regolament (KE) Nru 2125/95
- Douanerecht: ... % - Verordening (EG) nr. 2125/95
- co ... % - Rozporzadzenie (WE) nr 2125/95
- Direito aduaneiro: ... % - Regulamento (CE) n.o 2125/95
- Clo ... % - nariadenie (ES) c. 2125/95
- Carina: ... % - Uredba (ES) st. 2125/95
- Tulli ... prosenttia - Asetus (EY) N:o 2125/95
- Tull ... % - Förordning (EG) nr 2125/95."
Article 4
In Article 2(1) of Regulation (EC) No 2315/95, point (b) is replaced by the following:
"(b) Section 20 of the licence application and of the licence shall contain one of the following indications:
- Azúcar utilizado en uno o varios productos enumerados en la letra b) del apartado 1 del artículo 1 del Reglamento (CEE) n° 426/86
- Cukr pouzívaný v jednom nebo více produktech uvedených v cl. 1 odst. 1 písm. b) narízení (EHS) c. 426/86
- Sukker anvendt i et eller flere af de produkter, der er nævnt i artikel 1, stk. 1, litra b), i forordning (EØF) nr. 426/86
- Zucker, einem oder mehreren der in Artikel 1 Absatz 1 Buchstabe b) der Verordnung (EWG) Nr. 426/86 genannten Erzeugnissen zugesetzt
- Ζάχαρη πoυ χρησιμoπoιείται σε ένα ή περισσότερα των πρoϊόντων πoυ απαριθμoύνται στo άρθρo 1 παράγραφoς 1 στoιχείo β) τoυ κανoνισμoύ (ΕΟΚ) αριθ. 426/86
- Sugar used in one or more products as listed in Article 1(1)(b) of Regulation (EEC) No 426/86
- Suhkur, mida on kasutatud ühes või mitmes määruse (EMÜ) nr 426/86 artikli 1 lõike 1 punktis b loetletud tootes
- Sucre mis en oeuvre dans un ou plusieurs produits énumérés à l'article 1er, paragraphe 1, point b), du règlement (CEE) n° 426/86
- A 426/86/EGK rendelet 1. cikke (1) bekezdésének b) pontjában felsorolt egy vagy több termékben felhasznált cukor
- Zucchero incorporato in uno o più prodotti di cui all'articolo 1, paragrafo 1, lettera b), del regolamento (CEE) n. 426/86
- Cukrus naudojamas viename ar daugiau produktu isvardytu Reglamento (EEB) Nr. 426/86 1 straipsnio 1 dalies b punkte
- Cukurs, ko izmanto viena vai vairakos produktos, kuri uzskaititi Regulas (EEK) Nr. 426/86 1. panta 1. punkta b) apakspunkta
- Zokkor uzat f'prodott wieed jew aktar kif elenkat fl-Artikolu 1 (1) (b) tar-Regolament (KEE) Nru 426/86
- Suiker, verwerkt in een of meer van de in artikel 1, lid 1, onder b), van Verordening (EEG) nr. 426/86 genoemde producten
- Cukier zastosowany w jednym lub wiecej produktach wymienionych w art. 1 ust. 1 lit. b) rozporzadzenia (EWG) nr 426/86
- Açúcar utilizado num ou mais produtos enumerados no n.o 1, alínea b), do artigo 1.o do Regulamento (CEE) n.o 426/86
- V jednom alebo viacerých produktoch vymenovaných v clánku 1, odsek 1, bod b) nariadenia (EHS) c. 426/86 sa pouzil cukor
- Sladkor, dodan enemu ali vec proizvodov, navedenih v clenu 1(1)(b) Uredbe (EGS) st. 426/86,
- Yhdessä tai useammassa asetuksen (ETY) N:o 426/86 1 artiklan 1 kohdan b alakohdassa luetellussa tuotteessa käytetty sokeri.
- Socker som tillsätts i en eller flera av produkterna i artikel 1.1 b i förordning (EEG) nr 426/86."
Article 5
Regulation (EC) No 1599/97 is amended as follows:
1. the title is replaced by the following:
"Commission Regulation (EC) No 1599/97 of 28 July 1997 laying down detailed rules for the application of the system of minimum import prices for certain soft fruits originating in Bulgaria and Romania.";
2. in Annex II, the rows concerning Hungary, Poland, Slovakia, the Czech Republic, Estonia, Latvia and Lithuania are deleted.
Article 6
This Regulation shall enter into force on 1 May 2004, subject to the entry into force of the Treaty of Accession of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 17 March 2004.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 141, 24.6.1995, p. 28, Regulation last amended by Regulation (EC) No 1176/2002 (OJ L 170, 29.6.2002, p. 69).
(2) OJ L 150, 1.7.1995, p. 91, Regulation amended by Regulation (EC) No 2625/95 (OJ L 269, 11.11.1995, p. 3).
(3) OJ L 212, 7.9.1995, p. 16, Regulation last amended by Regulation (EC) No 1142/2003 (OJ L 160, 28.6.2003, p. 39).
(4) OJ L 233, 30.9.1995, p. 70.
(5) OJ L 216, 8.8.1997, p. 63, Regulation last amended by Regulation (EC) No 2153/2002 (OJ L 327, 4.12.2002, p. 4).
