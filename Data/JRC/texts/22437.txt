COMMISSION REGULATION (EC) No 1112/97 of 18 June 1997 adopting exceptional support measures for the beef market in Ireland in application of Decision 97/312/EC
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 805/68 of 27 June 1968 on the common organization of the market in beef and veal (1), as last amended by Regulation (EC) No 2222/96 (2), and in particular Article 23 thereof,
Whereas, by Decision 97/312/EC (3), the Commission approved a plan proposed by Ireland for measures to be implemented as regards bovine spongiform encephalopathy (BSE) in that Member State;
Whereas the principal elements of the plan are inter alia compulsory slaughter and destruction of suspect cases of BSE and, if confirmed, slaughter and destruction of all animals in herds where cases of BSE have occurred, and identification and slaughter of animals exposed to the same risks as the affected animals;
Whereas these measures involve serious disturbances in the market of Ireland; whereas it is therefore necessary to take exceptional measures to support that market;
Whereas it is appropriate to establish a scheme co-financed by the Community authorizing Ireland to purchase the animals concerned with a view to killing and subsequently destroying them;
Whereas provision should be made for laboratory examination of the brains from a sample of the animals slaughtered and for the use of a limited number of animals for research or educational purposes;
Whereas it is appropriate, to provide for a Community contribution of 70 % of the purchase price paid by Ireland for each animal destroyed pursuant to this Regulation;
Whereas, for the purpose of establishing the animal's market value, Ireland should set up a system securing a fair and objective evaluation of each animal;
Whereas, it is necessary to ensure that the animals concerned are killed and destroyed in a hygienic manner; whereas the price paid to producers should compensate them for not selling the animals in question; whereas those animals must therefore be prohibited from being marketed; whereas it is therefore necessary to specify the conditions for the destruction of those animals and the controls to be carried out by the Irish authorities;
Whereas, so as to avoid mixing of the animals to be slaughtered pursuant to this Regulation with animals not covered by this Regulation and the occurrence of mistakes as to identity, they should be kept separately in the lairage to a slaughterhouse, as well as in the slaughterhouse itself;
Whereas, provision should be made for Commission experts to check compliance with the conditions as specified;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Beef and Veal,
HAS ADOPTED THIS REGULATION:
Article 1
1. The competent authorities in Ireland shall be authorized to purchase any bovine animal present on a holding located in the territory of Ireland which has not shown any clinical sign of BSE and is to be slaughtered in accordance with Ireland's eradication plan as approved by Decision 97/312/EC.
2. The animals referred to in paragraph 1 shall be killed in a specially designated slaughterhouse. The head, internal organs and carcases shall be permanently stained. The stained material shall be transported in sealed containers to a specially authorized rendering plant for disposal in such a way that no part of it may be placed on the market. No part of the abovementioned animals may enter into the human food or animal feed chains or be used for cosmetic or pharmaceutical products. A representative of the competent authorities in Ireland shall be present in the slaughterhouse referred to above in order to supervise the operations in question.
Subject to the necessary control, after being killed in the specially designated slaughterhouse, the animals shall immediately be transported to a rendering plant for processing and destruction.
Notwithstanding the first subparagraph, the competent authorities in Ireland may allow the on-farm slaughter of an animal where existing animal welfare practice would require this.
Notwithstanding the first subparagraph, the hides of the animals referred to in paragraph 1 do not have to be stained or destroyed provided that they have been treated in such a way that they can only be used for leather production.
3. The slaughterhouses referred to in paragraph 2 shall be organized and operated in such a way as to ensure that:
(a) no bovine animal intended for slaughter for human or animal consumption, is present in the slaughterhouse when animals are being slaughtered pursuant to this Regulation;
(b) where it is necessary for bovine animals to be slaughtered pursuant to this Regulation to be held in lairage, they shall be kept separate from bovine animals intended for slaughter for human or animal consumption;
and
(c) where it is necessary for products derived from animals slaughtered pursuant to this Regulation to be stored, such storage shall be separate from any storage facility used for meat or other products destined for human or animal consumption.
4. The competent authorities in Ireland shall:
(a) notwithstanding paragraph 1, before processing and destruction, be authorized to conduct laboratory examination of the brains from a sample of animals slaughtered;
(b) be authorized, before processing and destruction, to use a limited number of animals for research or educational purposes;
(c) carry out the necessary administrative checks and effective on-the-spot supervision of the operations referred to in paragraphs 2 and 3;
and
(d) control those operations on the basis of frequent and unannounced inspections, in particular to verify that all material has been effectively destroyed.
The results of these checks and controls shall be made available to the Commission on request.
Article 2
1. The price in respect of the animal to be paid to producers or their agents by the competent authorities in Ireland pursuant to Article 1 (1) shall be equal to the objective market value in Ireland of each animal concerned established on the basis of a system of objective evaluation agreed on by the competent authorities in Ireland.
2. The Community shall co-finance the purchase price paid by the competent authorities concerned for each purchased animal which has been destroyed in accordance with Article 1 at a rate of 70 %.
3. The conversion rate to be applied shall be the agricultural rate in force on the first day of the month of purchase of the animal in question.
Article 3
Ireland shall adopt all measures necessary to ensure full compliance with the provisions of this Regulation. It shall inform the Commission as soon as possible of the measures which it has taken and of any amendments thereto.
Article 4
The competent authorities in Ireland:
(a) shall inform the Commission immediately, each time the plan approved by Decision 97/312/EC is applied, of:
- the number of animals selected for slaughter,
- the number of animals slaughtered,
- the average market value of the animals slaughtered,
pursuant to this Regulation during the previous week;
(b) shall establish a detailed report of the controls which they carry out pursuant to the measures referred to in Article 3 and shall communicate it to the Commission each quarter.
Article 5
Without prejudice to Article 9 of Council Regulation (EEC) No 729/70 (4), Commission experts, accompanied where appropriate by experts from the Member States, shall carry out, in collaboration with the competent authorities in Ireland, on-the-spot checks to verify compliance with all the provisions of this Regulation.
Article 6
The measures taken pursuant to this Regulation shall be considered to be intervention measures within the meaning of Article 3 of Regulation (EEC) No 729/70.
Article 7
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Communities.
It shall be applicable from 1 April 1996.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 18 June 1997.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ No L 148, 28. 6. 1968, p. 24.
(2) OJ No L 296, 21. 11. 1996, p. 50.
(3) OJ No L 133, 24. 5. 1997, p. 38.
(4) OJ No L 94, 28. 4. 1970, p. 13.
