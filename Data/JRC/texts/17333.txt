Commission Regulation (EC) No 317/2004
of 23 February 2004
on adopting derogations from the provisions of Regulation (EC) No 2150/2002 of the European Parliament and of the Council on waste statistics as regards Austria, France and Luxembourg
(Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Regulation (EC) No 2150/2002 of the European Parliament and of the Council of 25 November 2002 on waste statistics(1), and in particular Article 4(1) thereof,
Having regard to the request made by Austria on 30 June 2003,
Having regard to the request made by France on 12 June 2003,
Having regard to the request made by Luxembourg on 25 June 2003,
Whereas:
(1) In accordance with Article 4(1) of Regulation (EC) No 2150/2002, derogations from certain provisions of Annexes to that Regulation may be granted by the Commission during a transitional period.
(2) Such derogations should be granted, at their request, to Austria, France and Luxembourg.
(3) The measures provided for in this Regulation are in accordance with the opinion of the Statistical Programme Committee established by Council Decision 89/382/EEC, Euratom(2),
HAS ADOPTED THIS REGULATION:
Article 1
1. The following derogations from the provisions of Regulation (EC) No 2150/2002 are hereby granted:
(a) Austria is granted derogations for the production of results relating to Section 8(1.1), item 1 (agriculture, hunting and forestry) of Annex I.
(b) France is granted derogations for the production of results relating to Section 8(1.1), items 1 (agriculture, hunting and forestry), 2 (fisheries) and 16 (services activities) of Annex I and those relating to Section 8(2) of Annex II.
(c) Luxembourg is granted derogations for the production of results relating to Section 8(1.1), items 1 (agriculture, hunting and forestry) and 2 (fisheries) of Annex I.
2. The derogations provided for in paragraph 1 are granted only in respect of data from the first reference year, namely 2004.
After expiry of the transitional period, Austria, France and Luxembourg shall transmit data from the 2006 reference year.
Article 2
This Regulation shall enter into force on the 20th day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 23 February 2004.
For the Commission
Pedro Solbes Mira
Member of the Commission
(1) OJ L 332, 9.12.2002, p. 1.
(2) OJ L 181, 28.6.1989, p. 47.
