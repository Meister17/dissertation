COMMISSION DIRECTIVE 1999/9/EC of 26 February 1999 amending Directive 97/17/EC implementing Council Directive 92/75/EEC with regard to energy labelling of household dishwashers (Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 92/75/EEC of 22 September 1992 on the indication by labelling and standard product information of the consumption of energy and other resources of household appliances (1), and in particular Article 9 thereof,
Whereas Commission Directive 97/17/EC (2) implements Directive 92/75/EEC with regard to energy labelling of household dishwashers;
Whereas there has been a delay in the development and adoption of measurement methods (EN 50242); whereas in the absence of a harmonised measurement standard, it is impossible for suppliers to fulfil their obligations pursuant to Directive 97/17/EC; whereas the application of that Directive must therefore be deferred;
Whereas the measures provided for in this Directive are in accordance with the opinion of the committee set up under Article 10 of Directive 92/75/EEC,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Article 4(1) of Directive 97/17/EC is replaced by the following:
'1. Member States shall adopt and publish the laws, regulations and administrative provisions necessary to comply with this Directive by 28 February 1999. They shall immediately inform the Commission thereof. They shall apply those provisions from 1 March 1999.
However, Member States shall allow until 31 July 1999:
- the placing on the market, the commercialisation and/or the display of products, and
- the distribution of printed communications referred to in Article 2(4),
which do not conform to this Directive.
When Member States adopt the provisions referred to in the first subparagraph, they shall contain a reference to this Directive or shall be accompanied by such a reference on the occasion of their official publication. Member States shall determine how such reference is to be made.`
Article 2
This Directive shall enter into force on the third day following its publication in the Official Journal of the European Communities.
Article 3
This Directive is addressed to the Member States.
Done at Brussels, 26 February 1999.
For the Commission
Christos PAPOUTSIS
Member of the Commission
(1) OJ L 297, 13. 10. 1992, p. 16.
(2) OJ L 118, 7. 5. 1997, p. 1.
