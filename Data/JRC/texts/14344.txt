Commission Decision
of 9 November 2006
on harmonisation of the radio spectrum for use by short-range devices
(notified under document number C(2006) 5304)
(Text with EEA relevance)
(2006/771/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Decision No 676/2002/EC of the European Parliament and of the Council of 7 March 2002 on a regulatory framework for radio spectrum policy in the European Community (Radio Spectrum Decision) [1], and in particular Article 4(3) thereof,
Whereas:
(1) Given their pervasive use in the European Community and in the world, short-range devices are playing an increasing role in the economy and in the daily life of citizens, with different types of applications such as alarms, local communications equipment, door openers or medical implants. The development of applications based on short-range devices in the European Community could also contribute to achieving specific Community policy goals, such as completion of the internal market, promotion of innovation and research, and development of the information society.
(2) Short-range devices are typically massmarket and/or portable products which can easily be taken and used across borders; differences in spectrum access conditions therefore prevent their free movement, increase their production costs and create risks of harmful interference with other radio applications and services. In order to reap the benefits of the internal market for this type of device, to support the competitiveness of EU manufacturing industry by increasing economies of scale and to lower costs for consumers, radio spectrum must therefore be made available in the Community on the basis of harmonised technical conditions.
(3) As this type of device uses radio spectrum with low emission power and short-range emission capability, its potential to cause interference to other spectrum users is typically limited. Therefore such devices can share frequency bands with other services which are, or are not, subject to authorisation, without causing harmful interference, and can co-exist with other short-range devices. Their use should therefore not be subject to individual authorisation pursuant to the Authorisation Directive 2002/20/EC [2]. In addition, radiocommunications services, as defined in the International Telecommunications Union Radio Regulations, have priority over short-range devices and are not required to ensure protection of particular types of short-range devices against interference. Since no protection against interference can therefore be guaranteed to users of short-range devices, it is the responsibility of manufacturers of short-range devices to protect such devices against harmful interference from radiocommunications services as well as from other short-range devices operating in accordance with the applicable Community or national regulations. Pursuant to Directive 1999/5/EC of the European Parliament and of the Council of 9 March 1999 on radio equipment and telecommunications terminal equipment and the mutual recognition of their conformity (the R&TTE Directive) [3] manufacturers should ensure, that short-range devices effectively use the radio frequency spectrum so as to avoid harmful interference to other short-range devices.
(4) A significant number of these devices are already classified, or are likely to be in the future, as "Class 1" equipment under Commission Decision 2000/299/EC of 6 April 2000 establishing the initial classification of radio equipment and telecommunications terminal equipment and associated identifiers [4] adopted pursuant to Article 4(1) of the R&TTE Directive. Decision 2000/299/EC recognises the equivalence of radio interfaces meeting the conditions of "Class 1" so that radio equipment can be placed on the market and put into service without restriction in the whole Community.
(5) As the availability of harmonised spectrum and associated conditions of use determine "Class 1" classification, this Decision will further consolidate the continuity of such classification once achieved.
(6) On 11 March 2004 the Commission therefore issued a mandate [5] to the CEPT, pursuant to Article 4(2) of the Radio Spectrum Decision, to harmonise frequency use for short-range devices. In response to that mandate, in its report [6] of 15 November 2004 the CEPT established the list of voluntary harmonisation measures which exist in the European Community for short-range devices and stated that a more binding commitment is required from Member States in order to ensure the legal stability of the frequency harmonisation achieved in the CEPT. Therefore, it is necessary to establish a mechanism to make such harmonisation measures legally binding in the European Community.
(7) Member States may allow, at national level, equipment to operate under more permissive conditions than specified in this Decision. However, in this case such equipment could not operate throughout the Community without restrictions and would therefore be considered as "Class 2" equipment under the classification in the R&TTE Directive.
(8) Harmonisation under this Decision does not exclude the possibility for a Member State to apply, where justified, transitional periods or radio spectrum-sharing arrangements pursuant to Article 4(5) of the Radio Spectrum Decision. These should be kept to the minimum, as they would limit the benefits of "Class 1" classification.
(9) This general technical harmonisation Decision applies without prejudice to European Community technical harmonisation measures which apply to specific bands and types of devices, such as Commission Decision 2004/545/EC of 8 July 2004 on the harmonisation of radio spectrum in the 79 GHz range for the use of automotive short-range radar equipment in the Community [7], Commission Decision 2005/50/EC of 17 January 2005 on the harmonisation of the 24 GHz range radio spectrum band for the time-limited use by automotive short-range radar equipment in the Community [8], Commission Decision 2005/513/EC on the harmonised use of radio spectrum in the 5 GHz frequency band for the implementation of wireless access systems including radio local area networks (WAS/RLANs) [9] or Commission Decision 2005/928/EC of 20 December 2005 on the harmonisation of the 169,4-169,8125 MHz frequency band in the Community [10].
(10) The use of spectrum is subject to the requirements of Community law for public health protection in particular Directive 2004/40/EC of the European Parliament and of the Council [11] and Council Recommendation 1999/519/EC [12]. Health protection for radio equipment is ensured by conformity of such equipment to the essential requirements pursuant to the R&TTE Directive.
(11) Due to the rapid changes in technology and societal demands, new applications for short-range devices will emerge, which will require constant scrutiny of spectrum harmonisation conditions, taking into account the economic benefits of new applications and the requirements of industry and users. Member States will have to monitor these evolutions. Regular updates of this Decision will therefore be necessary to respond to new developments in the market and technology. The Annex will be reviewed at least once every year on the basis of the information collected by Member States and provided to the Commission. A review may also be started in cases where appropriate measures will be taken by a Member State pursuant to Article 9 of the R&TTE Directive. If a review reveals the necessity to adapt the Decision, changes will be decided following the procedures specified in the Radio Spectrum Decision for the adoption of implementing measures. The updates could include transition periods to accommodate legacy situations.
(12) The measures provided for in this Decision are in accordance with the opinion of the Radio Spectrum Committee,
HAS ADOPTED THIS DECISION:
Article 1
The purpose of this Decision is to harmonise the frequency bands and the related technical parameters for the availability and efficient use of radio spectrum for short-range devices so that such devices may benefit from "Class 1" classification under Commission Decision 2000/299/EC.
Article 2
For the purpose of this Decision:
1. "short-range device" means radio transmitters which provide either unidirectional or bidirectional communication and which transmit over a short distance at low power;
2. "non-interference and non-protected basis" means that no harmful interference may be caused to any radio communications service and that no claim may be made for protection of these devices against harmful interference originating from radio communications services.
Article 3
1. Member States shall designate and make available, on a non-exclusive, non-interference and non-protected basis, the frequency bands for the types of short-range devices, subject to the specific conditions and by the implementation deadline, as laid down in the Annex to this Decision.
2. Notwithstanding paragraph 1, Member States may request transitional periods and/or radio spectrum-sharing arrangements, pursuant to Article 4(5) of the Radio Spectrum Decision.
3. This Decision is without prejudice to the right of Member States to allow the use of the frequency bands under less restrictive conditions than specified in the Annex to this Decision.
Article 4
Member States shall keep the use of the relevant bands under scrutiny and report their findings to the Commission to allow regular and timely review of the Decision.
Article 5
This Decision is addressed to the Member States.
Done at Brussels, 9 November 2006.
For the Commission
Viviane Reding
Member of the Commission
[1] OJ L 108, 24.4.2002, p. 1.
[2] OJ L 108, 24.4.2002, p. 21.
[3] OJ L 91, 7.4.1999, p. 10.
[4] OJ L 97, 19.4.2000, p. 13.
[5] Mandate to CEPT to analyse further harmonisation of frequency bands in use for short-range devices.
[6] Final report by the ECC in response to the EC mandate to the CEPT on short-range devices radio spectrum harmonisation.
[7] OJ L 241, 13.7.2004, p. 66.
[8] OJ L 21, 25.1.2005, p. 15.
[9] OJ L 187, 19.7.2005, p. 22.
[10] OJ L 344, 27.12.2005, p. 47.
[11] OJ L 159, 30.4.2004, p. 1, corrected by OJ L 184, 24.5.2004, p. 1.
[12] OJ L 199, 30.7.1999, p. 59.
--------------------------------------------------
ANNEX
Harmonised frequency bands and technical parameters for short-range devices
Type of short-range device | Frequency band(s)/Single frequencies | Maximum power/field strength | Additional regulatory parameters/Mitigation requirements | Other restrictions | Implementation deadline |
Non-specific short-range devices [1] | 26,957-27,283 MHz | 10 mW effective radiated power (e.r.p.), which corresponds to 42 dBμA/m at 10 metres | | Video applications are excluded | 1 June 2007 |
40,660-40,700 MHz | 10 mW e.r.p. | | Video applications are excluded | 1 June 2007 |
433,05-434,79 MHz | 10 mW e.r.p. | Duty cycle [2]: up to 10 % | Audio and voice signals, and video applications, are excluded | 1 June 2007 |
868,0-868,6 MHz | 25 mW e.r.p. | Duty cycle [2]: up to 1 % | Video applications are excluded | 1 June 2007 |
868,7-869,2 MHz | 25 mW e.r.p. | Duty cycle [2]: up to 0,1 % | Video applications are excluded | 1 June 2007 |
869,4-869,65 MHz | 500 mW e.r.p. | Duty cycle [2]: up to 10 % Channel spacing: must be 25 kHz, except that the whole band may also be used as one single channel for high-speed data transmission | Video applications are excluded | 1 June 2007 |
869,7-870 MHz | 5 mW e.r.p. | Voice applications allowed with advanced mitigation techniques | Audio and video applications, are excluded | 1 June 2007 |
2400-2483,5 MHz | 10 mW equivalent isotropic radiated power (e.i.r.p.) | | | 1 June 2007 |
5725-5875 MHz | 25 mW e.i.r.p. | | | 1 June 2007 |
Alarm systems | 868,6-868,7 MHz | 10 mW e.r.p. | Channel spacing: 25 kHz The whole frequency band may also be used as one single channel for high-speed data transmission Duty cycle [2]: up to 0,1 % | | 1 June 2007 |
869,25-869,3 MHz | 10 mW e.r.p. | Channel spacing: 25 kHz Duty cycle [2]: below to 0,1 % | | 1 June 2007 |
869,65-869,7 MHz | 25 mW e.r.p. | Channel spacing: 25 kHz Duty cycle [2]: below to 10 % | | 1 June 2007 |
Social alarms [3] | 869,20-869,25 MHz | 10 mW e.r.p. | Channel spacing: 25 kHz Duty cycle [2]: below 0,1 % | | 1 June 2007 |
Inductive applications [4] | 20,05-59,75 kHz | 72 dBμA/m at 10 metres | | | 1 June 2007 |
59,75-60,25 kHz | 42 dBμA/m at 10 metres | | | 1 June 2007 |
60,25-70 kHz | 69 dBμA/m at 10 metres | | | 1 June 2007 |
70-119 kHz | 42 dBμA/m at 10 metres | | | 1 June 2007 |
119-127 kHz | 66 dBμA/m at 10 metres | | | 1 June 2007 |
127-135 kHz | 42 dBμA/m at 10 metres | | | 1 June 2007 |
6765-6795 kHz | 42 dBμA/m at 10 metres | | | 1 June 2007 |
13,553-13,567 MHz | 42 dBμA/m at 10 metres | | | 1 June 2007 |
Active medical implants [5] | 402-405 MHz | 25 μW e.r.p. | Channel spacing: 25 kHz Other channelling restriction: individual transmitters may combine adjacent channels for increased bandwidth with advanced mitigation techniques | | 1 June 2007 |
Wireless audio applications [6] | 863-865 MHz | 10 mW e.r.p. | | | 1 June 2007 |
[1] This category is available for any type of application which fulfils the technical conditions (typical uses are telemetry, telecommand, alarms, data in general and other similar applications).
[2] "Duty cycle" means the ratio of time during any one-hour period when equipment is actively transmitting.
[3] Social alarm devices are used to assist elderly or disabled people living at home when they are in distress.
[4] This category covers, for example, devices for car immobilisation, animal identification, alarm systems, cable detection, waste management, personal identification, wireless voice links, access control, proximity sensors, anti-theft systems including RF anti-theft induction systems, data transfer to handheld devices, automatic article identification, wireless control systems and automatic road tolling.
[5] This category covers the radio part of active implantable medical devices, as defined in Council Directive 90/385/EEC of 20 June 1990 on the approximation of the laws of the Member States relating to active implantable medical devices and their peripherals.
[6] Applications for wireless audio systems, including: cordless loudspeakers; cordless headphones; cordless headphones for portable use, for example portable CD, cassette or radio devices carried on a person; cordless headphones for use in a vehicle, for example for use with a radio or mobile telephone, etc.; in-ear monitoring, for use with concerts or other stage productions.
--------------------------------------------------
