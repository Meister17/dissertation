[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 12.9.2005
COM(2005) 433 final
2005/0174 (ACC)
Proposal for a
COUNCIL REGULATION
on the tariff rates for bananas
(presented by the Commission)
EXPLANATORY MEMORANDUM
1. Council Regulation (EEC) No 404/93 of 13 February 1993 on the common organisation of the market in bananas provides for the entry into force of a tariff only regime for imports of bananas no later than 1 January 2006.
2. On 12 July 2004, the Council authorised the Commission to notify the WTO that the European Community intended to modify concessions on item 0803 00 19 (bananas) included in Schedule CXL of the European Community annexed to the GATT 1994 pursuant to Article XXVIII GATT. Accordingly, the European Community notified the WTO on 15 July 2004 of its intention to modify certain concessions in EC Schedule CXL. Negotiations have been conducted by the Commission in consultation with the Committee established by Article 133 of the EC Treaty and the Special Committee on Agriculture within the framework of the negotiating directives issued by the Council.
3. The Commission has negotiated with Ecuador and Panama, having principal supplying interest and with Colombia and Costa Rica, having substantial supplier interest in products of subheading 0803 00 19 (bananas). Pursuant to the Annex to the Decision of the WTO Ministerial Conference of 14 November 2001 on the European Communities – the ACP-EC Partnership Agreement the Commission has also held consultations with other WTO Members.
4. In spite of its efforts, the Commission was unable to arrive at an acceptable agreement pursuant to Article XXVIII with the WTO Members concerned.
5. On 31 January 2005 the European Community notified to the WTO that it intends to replace its concessions on item 0803 00 19 (bananas) with a bound duty of €230/ton.
6. The arbitration procedure set out in the Annex to the Decision of the Ministerial Conference of 14 November 2001 was initiated on 30 March 2005. The Arbitrator’s Award issued on 1 August 2005 concluded that the MFN tariff rate of €230/ton proposed by the EC was not consistent with the above-mentioned Annex as it would not result in at least maintaining total market access for MFN suppliers.
7. The Commission has revised the EC proposal in light of the arbitrator’s findings.
8. In order to allow the modifications of certain concessions concerning bananas in EC Schedule CXL before 1 January 2006, this proposal asks the Council to establish a new tariff rate for products falling under CN code 0803 00 19 (bananas), as well as a tariff rate quota subject to zero duty applicable to bananas originating in ACP countries.
9. For the purposes of administering that tariff rate quota, the Commission envisages maintaining the method based on taking account of traditional trade flows (“traditionals/newcomers”) as currently applicable to the tariff rate quota C provided for in Article 18 of Council Regulation (EEC) No 404/93 of 13 February 1993 on the common organisation of the market in bananas, with a share reserved for non-traditional operators of 18%.
2005/0174 (ACC)
Proposal for a
COUNCIL REGULATION
on the tariff rates for bananas
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 133 thereof,
Having regard to the proposal from the Commission,
Whereas:
1. Council Regulation (EEC) No 404/93 of 13 February 1993 on the common organisation of the market in bananas[1] provides for the entry into force of a tariff only regime for imports of bananas no later than 1 January 2006.
2. On 12 July 2004, the Council authorised the Commission to open negotiations under Article XXVIII of the GATT 1994 with a view to modifying certain concessions for bananas. Accordingly, the European Community notified the WTO on 15 July 2004 of its intention to modify concessions on item 0803 00 19 (bananas) in EC Schedule CXL. Negotiations have been conducted by the Commission in consultation with the Committee established by Article 133 of the Treaty and the Special Committee on Agriculture and within the framework of the negotiating directives issued by the Council.
3. The Commission has not been able to negotiate an acceptable agreement with Ecuador and Panama, having a principal supplying interest, and Colombia and Costa Rica, having a substantial supplier interest, in products of HS subheading 0803 00 19 (bananas). Pursuant to the Annex to the Decision of the WTO Ministerial Conference of 14 November 2001 on the European Communities – the ACP-EC Partnership Agreement the Commission has also held consultations with other WTO Members. These consultations did not lead to an acceptable agreement.
4. On 31 January 2005 the European Community notified to the WTO that it intends to replace its concessions on item 0803 00 19 (bananas) with a bound duty of €230/ton.
5. The arbitration procedure set out in the Annex to the Decision was initiated on 30 March 2005. The Arbitrator’s Award issued on 1 August 2005 concluded that the MFN tariff rate of €230/ton proposed by the EC was not consistent with the above-mentioned Annex as it would not result in at least maintaining total market access for MFN suppliers.
6. The Commission has revised the EC proposal in light of the arbitrator’s findings.
7. In order to allow the modifications of certain concessions concerning bananas in EC Schedule CXL before 1 January 2006, a new tariff rate for bananas should be established. At the same time, a tariff rate quota for bananas originating in ACP countries should also be opened in accordance with the EC commitments under the ACP-EC Partnership Agreement.
8. The measures necessary for the implementation of this Regulation should be adopted in accordance with Council Decision 1999/468/EC of 28 June 1999 laying down the procedure for the exercise of implementing powers conferred on the Commission[2].
HAS ADOPTED THIS REGULATION:
Article 1
1. As from 1 January 2006 the tariff rate for bananas (CN code 0803 00 19) shall be €187/ton.
2. Each year from 1 January, starting from 1 January 2006, an autonomous tariff quota of 775 000 tons net weight subject to a zero duty rate shall be opened for imports of bananas (CN code 0803 00 19) originating in ACP countries.
Article 2
Detailed rules for implementing this Regulation shall be adopted in accordance with the procedure referred to in Article 3(2).
Article 3
1. The Commission shall be assisted by the Management Committee for Bananas instituted by Article 27 of Regulation (EC) No 404/93.
2. Where reference is made to this paragraph, Articles 4 and 7 of Decision 1999/468/EC shall apply.
The period laid down in Article 4(3) of Decision 1999/468/EC shall be one month.
Article 4
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Union .
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels,
For the Council
The President
LEGISLATIVE FINANCIAL STATEMENT FOR PROPOSALS HAVING A BUDGETARY IMPACT EXCLUSIVELY LIMITED TO THE REVENUE SIDE
1. NAME OF THE PROPOSAL
PROPOSAL FOR A COUNCIL REGULATION ON THE TARIFF RATES FOR BANANAS
2. BUDGET LINES
Chapter 10 – Article 1000:
Agricultural duties established by the Institutions of the European Communities in respect of trade with non-member countries under the common agricultural policy. Amount estimated for the year concerned: 763,4 M€ – B 2006.
3. FINANCIAL IMPACT
( Proposal has no financial implications
X Proposal has no financial impact on expenditure but has a financial impact on revenue – the effect is as follows:
(€ million to one decimal place) |
Budget line | Revenue[3] | 12 month period, starting dd/mm/yyyy | Year 2005 |
Article … | Impact on own resources | – | – |
Article … | Impact on own resources | – | – |
2006 and following years |
Article 1000 | + 277,2 M€ |
Article … |
4. ANTI-FRAUD MEASURES
No additional or specific measures envisaged.
5. OTHER REMARKS
On the expense side the import duty proposed is neutral with respect to the actual market equilibrium. It should not cause a substantial modification of the market price of bananas. Therefore, it can be estimated at this stage that there will not be any noticeable effect on the level of the compensatory support to be paid to Community producers, other than the traditional seasonal fluctuations.
[1] OJ L 47 of 25.2.1993, p. 1. Regulation last amended by Act of Accession of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia (OJ L 236 of 23.9.2003, p. 33).
[2] OJ L 184, 17.7.1999, p. 23.
[3] Regarding traditional own resources (agricultural duties, sugar levies, customs duties) the amounts indicated must be net amounts, i.e. gross amounts after deduction of 25% of collection costs.
