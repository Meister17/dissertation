Decision No 1554/2005/EC of the European Parliament and of the Council
of 7 September 2005
amending Council Decision 2001/51/EC establishing a programme relating to the Community framework strategy on gender equality and Decision No 848/2004/EC establishing a Community action programme to promote organisations active at European level in the field of equality between men and women
THE EUROPEAN PARLIAMENT AND THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 13(2) thereof,
Having regard to the proposal from the Commission,
Acting in accordance with the procedure laid down in Article 251 of the Treaty [1],
Whereas:
(1) The Council by Decision 2001/51/EC [2], established a programme relating to the Community framework strategy on gender equality to improve the understanding of issues related to gender equality, promote and disseminate the values and practices underlying gender equality and develop the capacity of players to promote gender equality effectively.
(2) The European Parliament and the Council by Decision No 848/2004/EC [3], established a Community Action Programme to promote organisations active at European level in the field of equality between men and women to support the activities of such organisations of which an ongoing programme of work or a specific measure serves the general European interest in the field of equality between men and women or pursues an objective which is part of the European Union's policy in this area.
(3) Both programmes expire on 31 December 2005. It is essential to ensure the continuity of Community policy for the promotion of gender equality in view of the objectives enshrined in Article 13 of the Treaty.
(4) It is necessary to extend the programmes for a one-year transitional period until a new multi-annual framework programme on the financing of actions in the employment and social field for the period 2007 to 2013 including a strand on gender equality is established.
(5) Decision 2001/51/EC was based on Article 13 of the Treaty. However, following the amendments introduced by the Treaty of Nice, Article 13(2) constitutes the specific legal basis for the adoption of Community incentive measures intended to contribute to the combating of discrimination. It is therefore appropriate to base the amendment of Decision 2001/51/EC on Article 13(2),
HAVE ADOPTED THIS DECISION:
Article 1
Decision 2001/51/EC is hereby amended as follows:
1. in Article 1, the date " 31 December 2005" shall be replaced by " 31 December 2006";
2. Article 11(1) shall be replaced by the following:
"1. The financial reference amount for the implementation of the programme for the period 2001 to 2006 shall be EUR 61,5 million."
Article 2
Decision No 848/2004/EC is hereby amended as follows:
1. in Article 1(3), the date " 31 December 2005" shall be replaced by " 31 December 2006";
2. Article 6(1) shall be replaced by the following:
"1. The financial framework for the implementation of the programme for the period from 2004 to 2006 shall be EUR 3,3 million."
Article 3
This Decision shall enter into force on the date of its publication in the Official Journal of the European Union.
Article 4
This Decision is addressed to the Member States.
Done at Strasbourg, 7 September 2005.
For the European Parliament
The President
J. Borrel Fontelles
For the Council
The President
C. Clarke
[1] Opinion of the European Parliament of 26 May 2005 (not yet published in the Official Journal) and Council Decision of 12 July 2005.
[2] OJ L 17, 19.1.2001, p. 22.
[3] OJ L 157, 30.4.2004, p. 18. Decision as rectified in OJ L 195, 2.6.2004, p. 7.
--------------------------------------------------
