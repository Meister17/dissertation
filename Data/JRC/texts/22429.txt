COUNCIL REGULATION (EC) No 476/97 of 13 March 1997 amending, with respect to statistical territory, Regulation (EC) No 1172/95 on the statistics relating to the trading of goods by the Community and its Member States with non-member countries
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 113 thereof,
Having regard to the proposal from the Commission,
Whereas the French Republic has decided to include the overseas departments in France's statistical territory as from 1 January 1997;
Whereas the Kingdom of Spain had decided to include the Canary Islands in Spain's statistical territory as from the same date;
Whereas the definition of the statistical territory of the Community set out in Article 3 of Regulation (EC) No 1172/95 (1) and the field of application of the statistics relating to the trading of goods by the Community and its Member States with non-member countries as defined in Article 4 of the abovementioned Regulation need to be adapted accordingly,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 1172/95 is hereby amended as follows:
1. Article 3 (2) shall be replaced by the following:
'2. By way of derogation from paragraph 1, the statistical territory of the Community shall include Helgoland.`;
2. the second subparagraph of Article 4 (1) shall be deleted.
Article 2
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Communities.
It shall apply with effect from 1 January 1997.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 13 March 1997.
For the Council
The President
M. PATIJN
(1) OJ No L 118, 25. 5. 1995, p. 10.
