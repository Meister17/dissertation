Order of the Court of First Instance of 18 November 2005 — Selmani v Council and Commission
(Case T-299/04) [1]
Parties
Applicant(s): Abdelghani Selmani (Dublin, Ireland) (represented by: C. Ó Briain, Solicitor)
Defendant(s): Council of the European Union (represented by: E. Finnegan and D. Canga Fano, Agents) and Commission of the European Communities (represented by: J. Enegren and C. Brown, Agents)
Application for
primarily, annulment of Article 2 of Council Regulation (EC) No 2580/2001 of 27 December 2001 on specific restricted measures directed against certain persons and entities with a view to combating terrorism (OJ 2001 L 344, p. 70) and Article 1 of Council Decision 2004/306/EC of 2 April 2004 implementing Article 2(3) of Regulation No 2580/2001 and repealing Decision 2003/902/EC (OJ 2004 L 99, p. 28) and all decisions adopted by the Council on the basis of Regulation No 2580/2001 and having the same effect as Decision 2004/306, in so far as those measures apply to the applicant,
Operative part of the Order
1. The action is dismissed as manifestly inadmissible.
2. The applicant shall pay the costs.
[1] OJ C 284, 20.11.2004.
--------------------------------------------------
