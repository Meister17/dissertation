Commission Decision
of 20 October 2000
laying down special conditions governing imports of fishery products originating in Venezuela
(notified under document number C(2000) 3056)
(Text with EEA relevance)
(2000/672/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/493/EEC of 22 July 1991 laying down the health conditions for the production and the placing on the market of fishery products(1), as last amended by Directive 97/79/EC(2), and in particular Article 11 thereof,
Whereas:
(1) A Commission expert has conducted an inspection visit to Venezuela to verify the conditions under which fishery products are produced, stored and dispatched to the Community.
(2) The provisions of legislation of the Venezuela on health inspection and monitoring of fishery products may be considered equivalent to those laid down in Directive 91/493/EEC.
(3) In Venezuela the Servicio Autónomo de Recursos Pesqueros (SARPA) of the Ministerio de Agricultura y Cría is capable of effectively verifying the application of the laws in force.
(4) The procedure for obtaining the health certificate referred to in Article 11(4)(a) of Directive 91/493/EEC must also cover the definition of a model certificate, the minimum requirements regarding the language(s) in which it must be drafted and the grade of the person empowered to sign it.
(5) Pursuant to Article 11(4)(b) of Directive 91/493/EEC, a mark should be affixed to packages of fishery products giving the name of the third country and the approval/registration number of the establishment, factory vessel, cold store or freezer vessel of origin.
(6) Pursuant to Article 11(4)(c) of Directive 91/493/EEC a list of approved establishments, factory vessels, or cold stores must be drawn up, and a list of freezer vessels equipped in accordance with points 1 to 7 of Annex II to the Directive 92/48/EEC(3) must be also drawn up. These lists must be drawn up on the basis of a communication from the SARPA to the Commission. It is therefore for the SARPA to ensure compliance with the provisions laid down to that end in Article 11(4) of Directive 91/493/EEC.
(7) The SARPA has provided official assurances regarding compliance with the rules set out in Chapter V of the Annex to Directive 91/493/EEC and regarding the fulfilment of requirements equivalent to those laid down by that Directive.
(8) The measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
The Servicio Autónomo de Recursos Pesqueros (SARPA) of the Ministerio de Agricultura y Cría shall be the competent authority in Venezuela for verifying and certifying compliance of fishery and aquaculture products with the requirements of Directive 91/493/EEC.
Article 2
Fishery and aquaculture products originating in Venezuela must meet the following conditions:
1. each consignment must be accompanied by a numbered original health certificate, duly completed, signed, dated and comprising a single sheet in accordance with the model in Annex A hereto;
2. the products must come from approved establishments, factory vessels, cold stores or registered freezer vessels listed in Annex B hereto;
3. except in the case of frozen fishery products in bulk and intended for the manufacture of preserved foods, all packages must bear the word "VENEZUELA" and the approval/registration number of the establishment, factory vessel, cold store or freezer vessel of origin in indelible letters.
Article 3
1. Certificates as referred to in Article 2(1) must be drawn up in at least one official language of the Member State where the checks are carried out.
2. Certificates must bear the name, capacity and signature of the representative of the SARPA and the latter's official stamp in a colour different from that of other endorsements.
Article 4
This Decision shall come into effect after 60 days of its publication in the Official Journal of the European Communities.
Article 5
This Decision is addressed to the Member States.
Done at Brussels, 20 October 2000.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 268, 24.9.1991, p. 15.
(2) OJ L 24, 30.1.1998, p. 31.
(3) OJ L 187, 7.7.1992, p. 41.
ANNEX A
>PIC FILE= "L_2000280EN.004802.EPS">
>PIC FILE= "L_2000280EN.004901.EPS">
ANNEX B
LIST OF ESTABLISHMENTS AND VESSELS
>TABLE>
