Commission Decision
of 8 March 2005
amending Decision 2004/475/EC adopting a transitional measure in favour of certain establishments in the meat and milk sectors in Slovenia
(notified under document number C(2005) 518)
(Text with EEA relevance)
(2005/193/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to the Act of Accession of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia, and in particular Article 42 thereof,
Whereas:
(1) Since Commission Decision 2004/475/EC [1] was adopted, the situation concerning establishments benefiting from a transitional period to achieve full compliance with Community requirements has changed.
(2) One meat plant ceased its activity on 31 December 2004.
(3) One milk plant on the list of establishments in transition will cease its activity as a high-capacity establishment. It will reduce its production with a view to being in compliance with the rules laid down by Community legislation for establishments with limited capacity. Therefore it should be deleted from the list of establishments in transition.
(4) Three meat establishments on the list of establishments in transition have made considerable efforts to build new facilities. However, those establishments are not in a position to finish their upgrading process by the prescribed deadline due to exceptional technical constraints. Therefore it is justified to allow them further time to complete the upgrading process.
(5) The Annex to Decision 2004/475/EC should therefore be amended accordingly. For the sake of clarity, it should be replaced.
(6) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
The Annex to Decision 2004/475/EC is replaced by the text in the Annex to this Decision.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 8 March 2005.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 160, 30.4.2004, p. 78, corrected version (OJ L 212, 12.6.2004, p. 47).
--------------------------------------------------
ANNEX
--------------------------------------------------
