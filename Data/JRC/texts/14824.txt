Judgement of the Court (Third Chamber) of 1 June 2006 — P&O European Ferries (Vizcaya) SA v Diputación Foral de Vizcaya, Commission of the European Communities
(Joined Cases C-442/03 P and C-471/03 P) [1]
Parties
Appellant (in Case C-442/03 P): P&O European Ferries (Vizcaya) SA (represented by: J. Lever QC, J. Ellison, solicitor, and M. Pickford, barrister, assisted by E. Bourtzalas, abogado)
Appellant (in Case C-471/03 P): Diputación Foral de Vizcaya (represented by: I. Sáenz-Cortabarría Fernández, M. Morales Isasi and J. Forguera Crespo, abogados)
Other party to the proceedings: Commission of the European Communities (represented by: N. Khan and J. Buendía Sierra, Agents)
Re:
Appeals against the judgment of the Court of First Instance (First Chamber, Extended Composition) of 5 August 2003 in Joined Cases T-116/01 and T-118/01 P&O European Ferries (Vizcaya) and Diputación Foral de Vizcaya
v Commission dismissing an application seeking the annulment of Article 2 of Commission Decision 2001/247/EC of 29 November 2000 on the aid scheme implemented by Spain in favour of the shipping company Ferries Golfo de Vizcaya SA, now P&O European Ferries (Vizcaya) SA, and ordering the repayment of the aid held to be incompatible with the common market
Operative part of the judgment
The Court:
1. Dismisses the appeals;
2. Orders P&O Ferries (Vizcaya) SA and the Diputación Foral de Vizcaya to pay the costs.
[1] OJ C 7, 10.1.2004 and OJ C 21, 24.1.2004.
--------------------------------------------------
