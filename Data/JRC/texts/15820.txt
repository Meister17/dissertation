[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 10.7.2006
COM(2006) 366 final
REPORT FROM THE COMMISSION
Annual Report from the Commission on the Guarantee Fund and its Management in 2005 {SEC(2006) 891}
TABLE OF CONTENTS
1. Legal bases 3
2. Position of the Fund at 31 December 2005 3
2.1 Financial analysis 3
2.2 Accounting presentation 4
3. Payments into the Fund 5
3.1. Payments from the general budget in the course of the financial year 5
3.2. Interest from financial investment of the Fund's liquid assets 6
3.3. Recovery from defaulting debtors 7
4. The Fund's liabilities 7
4.1. Default payments 7
4.2. EIB’s remuneration 7
1. LEGAL BASES
Council Regulation (EC, Euratom) No 2728/94 of 31 October 1994 (“the Regulation”) set up a Guarantee Fund for external actions ("the Fund") in order to repay the Community's creditors in the event of default by beneficiaries of loans granted or guaranteed by the Community (OJ L 293, 12.11.1994, p. 1). The Regulation was amended by the Council Regulation (EC, Euratom) No 1149/99 of 25 May 1999 (OJ L 139, 2.6.1999, p. 1) and the Council Regulation (EC, Euratom) No 2273/2004 of 22 December 2004 (OJ L 396, 31.12.2004, p. 28) (“the amended Regulation”).
In accordance with Article 6 of the Regulation, the Commission entrusted financial management of the Fund to the European Investment Bank (EIB) under an Agreement signed between the Community and the EIB on 23 November 1994 in Brussels and on 25 November 1994 in Luxembourg (“the Agreement”).
Under Article 8.2 of the Agreement, by 1 March of each year the Bank must send the Commission this annual status report on the Fund and its management including the revenue and expenditure account, "statement of financial performance", and the financial statement for the Fund for the preceding year, "statement of financial position of the Fund". Further details of the report covering the year 2005 can be found in section 2 of the Commission Staff Working Paper (“the Annex”)[1].
In addition, Article 7 of the amended Regulation requires the Commission to send the European Parliament, the Council and the Court of Auditors a report on the situation of the Fund and its management for each financial year by 31 May of the following year.
2. POSITION OF THE FUND AT 31 DECEMBER 2005
2.1 Financial analysis
The position of the Fund is presented in accordance with International Financial Reporting Standards (IFRS) at the close of the 2005 financial year. The Fund totalled EUR 1,324,663,957.25 (see Section 3 of the Annex: Statement of Financial position of the Fund at 31 December 2005, provided by EIB). This is the sum, since the Fund was established, of all
- budget payments to the Fund EUR 2,672,274,500.00;
- successive yearly net results EUR 523,763,636.64;
- late repayments from non-member countries EUR 575,673,913.77;
- debts corresponding to the repayments not made to the EIB EUR 711,557.47;
- commission received on late recovery in 2002 EUR 5,090,662.91;
- adjustment due to the application of IFRS for the valuation of the Fund’s portfolio EUR 46,981,944.72 (See item "Reserves" in the Liabilities of the Guarantee Fund Balance Sheet in Section 3 of the Annex);
- less calls on the Fund's resources (EUR 477,860,856.19);
- successive repayments to the budget of the surplus in the Fund (EUR 1,683,140,000.00);
- and an exceptional repayment to the budget of EUR 338,831,402.07 was made on 10 January 2005, representing 9% of the outstanding operations as of 1 May 2004 granted to the new Member States (See in Section 1 the last amendment of the Fund’s Regulation).
After the deduction of accruals EUR 725,117.47, thereof EUR 702,497.47 management fees (EIB's remuneration), the Fund total at 31 December 2005 amounts to EUR 1,323,938,839.78.
Article 3 of the Regulation requires that the amount of the Fund has to reach an appropriate level (target amount) set at 9% of the total outstanding capital liabilities arising from each operation, plus unpaid interest due.
Therefore, outstanding lending and loan guarantee operations for third countries[2] plus unpaid interest due totalled EUR 13,680,125,069.70 at 31 December 2005, of which EUR 125,823,921.72 were accounted for by interest due but not yet paid.
The ratio between the Fund's resources EUR 1,323,938,839.78 and outstanding capital liabilities within the meaning of the amended Regulation was 9.68%. Since this is higher than the target amount of 9% of the total guaranteed outstanding amount (rounded amount of 1,231.21 EUR million), a repayment from the Fund to the general budget of the European Union has to be made, as provided for in the third paragraph of Article 3 of the Regulation. The amount to be repaid to the budget in 2006 is EUR 92,730,000.00.
2.2 Accounting presentation
The European Communities are committed to undertake a major change to their accounting framework with the objective to produce its financial statements based on accrual accounts in 2005. In the framework of this modernisation, the Commission decided to present its accounts according to new accounting rules inspired by IPSAS /IFRS principles.
Consequently, pre-consolidated financial statements of the Fund have been prepared in order to comply with these principles.
The total amount of the pre-consolidated balance sheet is EUR 1,325,891,755.37. This includes the total amount of the Fund plus the arrears covered by the Fund, the accruals of interests on late payments and other accounting accruals in order to produce a full set of financial statements for the Fund at year-end to be consolidated in the EU consolidated balance sheet (for details see section 4 of the Annex).
3. Payments into the Fund
3.1. Payments from the general budget in the course of the financial year
The legal bases for the provisioning arrangements are described in section 1 of the Annex.
The Council Regulation (EC) No 2040/2000 of 26 September 2000 (OJ L 244, 29.9.2000, p. 27) concerning budgetary discipline authorised the entry in the general budget of a reserve for loans and guaranteed operations. This item, to which were allocated appropriations of EUR 223 million in 2005, is used for endowing the Fund by transferring amounts to the budget item for payments to the Fund.
In line with the legal bases, one transfer totalling EUR 140,110,000.00 to endow the Fund was adopted by the budgetary authority in 2005.
- Transfer DEC 11/2005[3]: EUR 140.110.000,00: this transfer was for payments into the Fund in respect of the Council Decisions of 22 December 1999, 6 November 2001 and 22 December 2004 as well as the Commission Decision C(2004)2817/3, in accordance with the arrangements set out in the Annex to the Regulation.
The decisions covered by this transfer are given in Section 1 of the Annex.
The transfer was paid under the mechanism for offsetting against the repayment of the Fund’s surplus EUR 187.130.000,00 which took place on 15 July 2005.
On 12 April 2005, the Commission adopted a Proposal for a Council Regulation amending Regulation (EC, Euratom) No 2728/94 establishing a Guarantee Fund for external actions[4] to change the Fund’s provisioning mechanism. The main objective of this proposal is to improve the efficiency of the use of budgetary means. According to the proposal, the Fund would be provisioned ex-post on the basis of observed amounts of guaranteed loans outstanding. At the time of finalizing this report, the Council had not yet adopted this Commission proposal.
According to the Commission Communication on the Financial Perspectives 2007 - 2013[5], the amount available for financing of the Fund will not, in theory, be limited in the future[6] by a budgetary mechanism as the funding of the Fund will be carried out through a budget line under Heading 4 (External Relations) and not, as at present, through a dedicated Reserve.
3.2. Interest from financial investment of the Fund's liquid assets
The Fund's liquid assets are invested in accordance with the management principles laid down in the Annex to the Community/EIB Agreement of 23/25 November 1994, amended by Supplementary Agreement No 1 of 17/23 September 1996 and Supplementary Agreement No 2 of 26 April/8 May 2002. By Supplementary Agreement No 2, the investment principles were changed in 2002 in order to correct the excess liquidity which had risen to over 50% of the Fund's assets and was restricting the yield of the Fund. While retaining the limit whereby at least one third of the Fund must be invested in short-term investments (up to one year), the instruments eligible for short-term liquidity were extended. They now include variable-rate securities, irrespective of their maturity dates, and fixed-rate securities with a maximum of one year remaining to maturity, irrespective of their initial maturity period. This is because fixed-rate securities are reimbursable at 100% of their nominal value at the end of their life, while variable-rate securities can be sold at any time at a price approaching 100%, whatever their remaining period to maturity. To maintain a balance between the various instruments for which liquidity is monitored, a minimum of 18% (corresponding to double the provisioning rate of the Fund) is kept in monetary investments, particularly bank deposits. This new structure helped to improve the Fund's yield while maintaining a prudent liquidity level.
In 2005, a gradual approach was agreed for long-term investments with a view to implement the target structure, i.e. an equal distribution of investments between the 2 and 10 year maturity. To this end, interest rate levels (entry points) have been decided which would trigger long-term investments in those maturities where the actual amounts are below the target. During the course of 2005, not all levels have been reached and therefore, the implementation of the target structure will be finalised in 2006.
The list of banks authorised to receive deposits is agreed by the Commission and the EIB. The original list has been regularly revised in the light of the latest changes in bank ratings. Most of the banks are members of the euro clearing system. They all have a Moody's rating of at least A1 for long-term and P1 for short-term investments, or an equivalent Standard & Poor’s or Fitch rating. The investments made with them are governed by rules to ensure a good spread and to avoid the concentration of risk.
In 2005, interest income on cash and cash equivalents, on the Fund current accounts and on securities totalled EUR 50,439,477.94, broken down as follows:
- Deposits and current accounts : EUR 6,280,520.69; this represents the situation at 31 December 2005 including interest received on bank deposits EUR 5,907,583.42 and changes in accrued interest EUR 203,864.22 in 2005. The current accounts amounted to EUR 37,503.59 comprising interest recorded on the current accounts. Commercial paper amounted to EUR 160,016.91 of interest received and changes in accrued interest (EUR 28,447.45) at 31 December 2005.
- Securities portfolio : EUR 44,158,957.25; this amount includes EUR 48,997,994.99 of interest on securities, and changes in accrued interest (EUR 1,961,071.32) at 31 December 2005. The interest on securities is generated by investments placed in the form of securities in accordance with the investment principles laid down in the Agreement giving the EIB the task of managing the Fund's liquid assets. From this figure must, however, be subtracted the EUR 2,877,966.42 entered in the course of the year as the difference between the entry price and the redemption value divided pro rata temporis over the remaining life of the securities held (corresponding to the spread of the premium or discount entered in the profit-and-loss account).
The interest received is entered in the results for the financial year.
3.3. Recovery from defaulting debtors
No recovery occurred in 2005. An amount of USD 1,448,433.44 relating to non paid penalty interest remains to be recovered by the Fund.
4. The Fund's liabilities
4.1. Default payments
The Fund was not called for default payments in 2005.
4.2. EIB’s remuneration
The second Supplementary Agreement to the Agreement signed on 26 April and 8 May 2002, lays down that the Bank's remuneration is to be determined by applying to each of the tranches of the Fund's assets the degressive annual rates of commission which relate to them respectively. This remuneration is calculated on the basis of the average assets of the Fund.
The Bank's remuneration for 2005 was fixed at EUR 702,497.47 and was entered in the profit-and-loss account and as accruals (liabilities) on the balance sheet. The remuneration will be paid to the EIB in the first quarter of 2006.
[1] SEC(2006) 891
[2] The amended Regulation stipulates that operations towards accession countries covered by the Fund remain covered by the Communities guarantee after the date of accession. However, from that date, they cease to be external actions of the Communities and are covered directly by the general budget of the European Union and no longer by the Fund.
[3] SEC(2005) 479 final.
[4] COM(2005) 130.
[5] COM(2004) 487 of 14.7.2004.
[6] Budgetary discipline will however be ensured as:- for Macro-Financial Assistance loans, individual Council decisions are needed;- for Euratom and EIB guaranteed loans, multiannual ceilings are approved by the Council.
