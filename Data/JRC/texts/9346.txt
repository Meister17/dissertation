Judgement of the Court of First Instance of 30 May 2006 — Bank Austria Creditanstalt AG
v Commission of the European Communities
(Case T-198/03) [1]
Parties
Applicant: Bank Austria Creditanstalt AG, established in Vienna (Austria) (represented by: C. Zschocke and J. Beninca, lawyers)
Defendant: Commission of the European Communities (represented initially by: S. Rating and subsequently by A. Bouquet, acting as Agents, and by D. Waelbroeck and U. Zinsmeister, lawyers)
Re:
Annulment of the decision of the Commission's Hearing Officer of 5 May 2003 to publish the non-confidential version of the Commission decision of 11 June 2002 in Case COMP/36.571/D-1 — Austrian banks ("Lombard Club")
Operative part of the judgement
The Court:
1. Dismisses the action;
2. Orders the applicant to pay the costs.
[1] OJ C 213, 6.9.2003.
--------------------------------------------------
