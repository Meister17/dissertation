COMMISSION DECISION
of 27 July 1995
on treatment of milk and milk-based products for human consumption from third countries or parts of third countries where there is a risk of foot-and-mouth disease
(Text with EEA relevance)
(95/342/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 92/46/EEC of 16 June 1992 laying down the health rules for the production and placing on the market of raw milk, heat-treated milk and milk-based products (1) as last amended by the Act of Accession of Austria, Finland and Sweden, and in particular Article 23 (3) (d) thereof;
Whereas milk and milk-based products for human consumption from certain third countries or parts of third countries may present a risk from the point of view of foot-and-mouth disease;
Whereas the types of treatment to be prescribed for milk and milk-based products for human consumption from such third countries or parts of third countries should be established;
Whereas the types of treatment are to be listed on the model health certificates for imports of milk and milk-based products from third countries or parts of third countries where there is a risk of foot-and-mouth disease;
Whereas the types of treatment to be prescribed must have a scientific basis of the sort recommended by the Scientific Veterinary Committee and must take account of public and animal health protection requirements;
Whereas the measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
Milk and milk-based products for human consumption from third countries or parts of third countries where an outbreak of foot-and-mouth disease has occurred within the last 12 months or which have carried out vaccination against foot-and-mouth disease in the last 12 months must, before they are introduced onto Community territory, undergo:
(a) sterilization such that it has an F° value equal to or higher than 3; or
(b) an initial heat treatment having an effect at least equivalent to that achieved by pasteurization at a temperature of at least 72 °C for at least 15 seconds, so as to produce a negative reaction to the phosphatase test, followed by:
(i) a second heat treatment involving high-temperature pasteurization, UHT or sterilization, so as to produce a negative reaction to the peroxidase test; or
(ii) in the case of milk powder or a dry milk-based product, a second heat treatment having an effect at least equivalent to that achieved by the first heat treatment, so as to produce a negative reaction to the phosphatase test, followed by a drying process; or
(iii) an acidification process such that the pH value is lowered and kept below 6 for at least one hour.
Article 2
This Decision shall apply from 2 February 1996.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 27 July 1995.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ No L 268, 14. 9. 1992, p. 1.
