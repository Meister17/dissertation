Euro exchange rates [1]
15 June 2006
(2006/C 141/01)
| Currency | Exchange rate |
USD | US dollar | 1,2610 |
JPY | Japanese yen | 144,84 |
DKK | Danish krone | 7,4547 |
GBP | Pound sterling | 0,68225 |
SEK | Swedish krona | 9,2765 |
CHF | Swiss franc | 1,5522 |
ISK | Iceland króna | 95,02 |
NOK | Norwegian krone | 7,8475 |
BGN | Bulgarian lev | 1,9558 |
CYP | Cyprus pound | 0,5750 |
CZK | Czech koruna | 28,323 |
EEK | Estonian kroon | 15,6466 |
HUF | Hungarian forint | 271,50 |
LTL | Lithuanian litas | 3,4528 |
LVL | Latvian lats | 0,6960 |
MTL | Maltese lira | 0,4293 |
PLN | Polish zloty | 4,0357 |
RON | Romanian leu | 3,5242 |
SIT | Slovenian tolar | 239,65 |
SKK | Slovak koruna | 38,010 |
TRY | Turkish lira | 2,0055 |
AUD | Australian dollar | 1,7078 |
CAD | Canadian dollar | 1,4045 |
HKD | Hong Kong dollar | 9,7873 |
NZD | New Zealand dollar | 2,0227 |
SGD | Singapore dollar | 2,0053 |
KRW | South Korean won | 1210,06 |
ZAR | South African rand | 8,6391 |
CNY | Chinese yuan renminbi | 10,0879 |
HRK | Croatian kuna | 7,2580 |
IDR | Indonesian rupiah | 11831,33 |
MYR | Malaysian ringgit | 4,605 |
PHP | Philippine peso | 67,003 |
RUB | Russian rouble | 34,0890 |
THB | Thai baht | 48,431 |
[1] Source: reference exchange rate published by the ECB.
--------------------------------------------------
