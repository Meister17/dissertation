Commission Regulation (EC) No 77/2001
of 5 January 2001
amending the Annexes to Regulation (EC) No 1547/1999 and Council Regulation (EC) No 1420/1999 as regards shipments of certain types of waste to Albania, Brazil, Bulgaria, Burundi, Jamaica, Morocco, Nigeria, Peru, Romania, Tunisia and Zimbabwe
(Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 259/93 of 1 February 1993 on the supervision and control of shipments of waste within, into and out of the European Community(1), as last amended by Commission Decision 1999/816/EC(2), and in particular Article 17(3) thereof,
Having regard to Council Regulation (EC) No 1420/1999 of 29 April 1999 establishing common rules and procedures to apply to shipments to certain non-OECD countries of certain types of waste(3), as amended by Commission Regulation (EC) No 1208/2000(4), and in particular Article 3(5) thereof,
Whereas:
(1) In January 2000, the Commission sent a note verbale to all non-OECD countries (plus Hungary and Poland which do not yet apply OECD Decision C(92) 39 final). The purpose of this note verbale was three fold: (a) to inform these countries of the Community's new regulations; (b) to ask for confirmation of the respective positions as outlined in the Annexes to Regulation (EC) No 1420/1999 and Commission Regulation (EC) No 1547/1999 of 12 July 1999 determining the control procedures under Council Regulation (EEC) No 259/93 to apply to shipments of certain types of waste to certain countries to which OECD Decision C(92) 39 final does not apply(5), as last amended by Regulation (EC) No 1552/2000(6), and (c) to have an answer from those countries which did not reply in 1994.
(2) Among the countries that replied, Brazil, Bulgaria, Burundi, Jamaica, Morocco, Nigeria, Peru, Romania, Tunisia and Zimbabwe notified the Commission that the import of certain wastes listd in Annex II to Regulation (EEC) No 259/93 is accepted either without any control procedure or subject to control pursuant to the control procedure applying to Annex III or IV thereto or laid down in Article 15 thereof. Concerning other waste, they have indicated that they do not wish to receive shipments.
(3) Albania replied to the note verbale stating that its position has not changed. However, the provisions concerning Albania need to be amended to take into account the new labelling system for certain types of waste laid down in Annex II to Regulation (EC) No 259/93 as amended by Decision 1999/816/EC.
(4) In accordance with Article 17(3) of Regulation (EEC) No 259/93, the Committee set up by Article 18 of Council Directive 75/442/EEC of 15 July 1975 on waste(7), as last amended by Commission Decision 96/350/EC(8), was notified of the official request of these countries on 23 June 2000 (on 12 July 2000 for Burundi).
(5) In order to take into account the new situation of these countries, it is necessary to amend at the same time Regulation (EC) No 1420/1999 and Regulation (EC) No 1547/1999.
(6) The measures provided for in this Regulation are in accordance with the opinion of the Committee set up by Article 18 of Directive 75/442/EEC,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 1547/1999 is amended as follows:
1. Annex A is amended as set out in Annex A to this Regulation;
2. Annex B is amended as set out in Annex B to this Regulation;
3. Annex C is amended as set out in Annex C to this Regulation;
4. Annex D is amended as set out in Annex D to this Regulation.
Article 2
Regulation (EC) No 1420/1999 is amended as follows:
1. Annex A is amended as set out in Annex E to this Regulation;
2. Annex B is amended as set out in Annex F to this Regulation.
Article 3
This Regulation shall enter into force on the 20th day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 5 January 2001.
For the Commission
Pascal Lamy
Member of the Commission
(1) OJ L 30, 6.2.1993, p. 1.
(2) OJ L 316, 10.12.1999, p. 45.
(3) OJ L 166, 1.7.1999, p. 6.
(4) OJ L 138, 9.6.2000, p. 7.
(5) OJ L 185, 17.7.1999, p. 1.
(6) OJ L 176, 15.7.2000, p. 27.
(7) OJ L 194, 25.7.1975, p. 39.
(8) OJ L 135, 6.6.1996, p. 32.
ANNEX A
Annex A to Regulation (EC) No 1547/1999 is amended as follows:
1. All the text related to Bulgaria is deleted.
2. All the text related to Jamaica is deleted.
3. All the text related to Tunisia is replaced by the text: "TUNISIA
1. All types in section GB ("Metal bearing wastes arising from melting, smelting and refining of metals").
2. In section GC ("Other wastes containing metals"):
>TABLE>
The following metal and metal alloy wastes in metallic dispersible form:
>TABLE>
NB:
mercury is specifically excluded as a contaminant of these metals and their alloys or amalgams.
3. All types in section GH ("Solid plastic wastes").
4. All types in section GI ("Paper, paperboard and paper product wastes").
5. In section GJ ("Textiles wastes"):
>TABLE>
6. In section GK ("Rubber wastes"):
>TABLE>
7. All types in section GM ("Wastes arising from agro-food industries")".
ANNEX B
Annex B to Regulation (EC) No 1547/1999 is amended as follows:
1. All the text related to Brazil is replaced by the text: "BRAZIL
1. In section GA ("Metal and metal-alloy wastes in metallic non-dispersible(1) form"):
(a) The following waste and scrap of non-ferrous metals and their alloys:
>TABLE>
2. In section GB ("Metal bearing wastes arising from melting, smelting, and refining of metals"):
>TABLE>
3. In section GC ("Other wastes containing metals"):
>TABLE>
4. In section GF ("Ceramic wastes in non-dispersible form"):
>TABLE>
5. In section GN ("Wastes arising from tanning and fellmongery operations and leather use"):
>TABLE>"
2. All the text related to Jamaica is replaced by the words: "JAMAICA
All types in section GM ("Wastes arising from agro-food industries")"
3. All the text related to Nigeria is replaced by the text: "NIGERIA
1. All types in section GA ("Metal and metal-alloy wastes in metallic, non-dispersible(2) form").
2. All types in section GB ("Metal bearing wastes arising from melting, smelting and refining of metals").
3. All types in section GH ("Solid plastic wastes").
4. All types in section GI ("Paper, paperboard and paper product wastes").
5. In section GJ ("Textile wastes"):
>TABLE>
6. In section GK ("Rubber wastes"):
>TABLE>
7. In section GM ("Wastes arising from agro-food industries"):
>TABLE>"
4. Between the texts related to Nigeria and Russia, the following text is inserted: "PERU
In section GA ("Metal and metal-alloy wastes in metallic, non-dispersible(3) form"):
>TABLE>"
(1) 'Non-dispersible' does not include any wastes in the form of powder, sludge, dust or solid items containing encased hazardous waste liquids.
(2) 'Non-dispersible' does not include any wastes in the form of powder, sludge, dust or solid items containing encased hazardous waste liquids.
(3) 'Non-dispersible' does not include any wastes in the form of powder, sludge, dust or solid items containing encased hazardous waste liquids.
ANNEX C
Annex C to Regulation (EC) No 1547/1999 is amended as follows:
All the text related to Romania is replaced by the text: "ROMANIA
1. In section GA ("Metal and metal-alloy wastes in metallic, non-dispersible(1) form"):
(a) The following waste and scrap of precious metals and their alloys:
>TABLE>
NB:
Mercury is specifically excluded as a contaminant of these metals or their alloys or amalgams.
(b) The following waste and scrap of non-ferrous metals and their alloys:
>TABLE>
2. In section GE ("Glass waste in non-dispersible form"):
>TABLE>
3. In section GI ("Paper, paperboard and paper product wastes"):
>TABLE>
4. In section GJ ("Textile wastes"):
>TABLE>
5. In section GO ("Other wastes containing principally organic constituents, which may contain metals and inorganic materials"):
>TABLE>"
(1) 'Non-dispersible' does not include any wastes in the form of powder, sludge, dust or solid items containing encased hazardous waste liquids.
ANNEX D
Annex D to Regulation (EC) No 1547/1999 is amended as follows:
1. All the text related to Albania is replaced by the text: "ALBANIA
1. In section GA ("Metal and metal-alloy wastes in metallic, non-dispersible(1) form"):
The following waste and scrap of non-ferrous metals and their alloys:
>TABLE>
2. All types included in section GB ("Metal bearing wastes arising from melting, smelting and refining of metals").
3. All types included in section GE ("Glass waste in non-dispersible form").
4. In section GG ("Other wastes containing principally inorganic constituents, which may contain metals and organic materials"):
>TABLE>
5. All types in section GI ("Paper, paperboard and paper product wastes").
6. In section GJ ("Textile wastes"):
>TABLE>"
2. All the text related to Brazil is replaced by the text: "BRAZIL
All types in Annex II B except those listed in Annex B and except:
1. In section GA ("Metal and metal-alloy wastes in metallic, non-dispersible(2) form"):
The following waste and scrap of non-ferrous metals and their alloys:
>TABLE>
2. In section GB ("Metal bearing wastes arising from melting, smelting and refining of metals"):
>TABLE>
3. In section GG ("Other wastes containing principally inorganic constituents, which may contain metals and organic material"):
>TABLE>
4. In section GK ("Rubber wastes"):
>TABLE>"
3. All the text related to Bulgaria is replaced by the words: "BULGARIA
"All types in Annex II"".
4. Between the texts related to Burkina Faso and Cameroon, the following text is inserted: "BURUNDI
1. In section GA ("Metal and metal-alloy wastes in metallic, non-dispersible(3) form"):
>TABLE>
2. All types in section GI ("Paper, paperboard and paper product wastes").
3. In section GJ ("Textile wastes"):
>TABLE>
4. In section GK ("Rubber wastes"):
>TABLE>
5. In section GM ("Wastes arising from agro-food industries"):
>TABLE>"
5. Between the texts related to Monaco and Netherland Antilles, the following text is inserted: "MOROCCO
All types in section GJ ("Textile wastes")".
6. All the text related to Tunisia is replaced by the text: "TUNISIA
1. All types in section GA ("Metal and metal-alloy wastes in metallic, non-dispersible(4) form").
2. All types in section GD ("Wastes from mining operations: these wastes to be in non-dispersible form").
3. All types in section GE ("Glass waste in non-dispersible form").
4. All types in section GF ("Ceramic waste in non-dispersible form").
5. In section GG ("Other wastes containing principally inorganic constituents, which may contain metals and organic materials"):
>TABLE>
6. In section GJ ("Textile waste"):
>TABLE>
7. In section GK ("Rubber wastes"):
>TABLE>
8. All types in section GL ("Untreated cork and wood wastes").
9. In section GN ("Wastes arising from tanning and fellmongery operations and leather use)":
>TABLE>
10. In section GO ("Other wastes containing principally organic constituents, which may contain metals and inorganic materials"):
>TABLE>"
7. After the texts related to Zambia, the following text is added: "ZIMBABWE
"All types in Annex II"".
(1) 'Non-dispersible' does not include any wastes in the form of powder, sludge, dust or solid items containing encased hazardous waste liquids.
(2) 'Non-dispersible' does not include any wastes in the form of powder, sludge, dust or solid items containing encased hazardous waste liquids.
(3) 'Non-dispersible' does not include any wastes in the form of powder, sludge, dust or solid items containing encased hazardous waste liquids.
(4) 'Non-dispersible' does not include any wastes in the form of powder, sludge, dust or solid items containing encased hazardous waste liquids.
ANNEX E
Annex A to Regulation (EC) No 1420/1999 is amended as follows:
1. All the text related to Albania is replaced by the text: "ALBANIA
All types except:
1. In section GA ("Metal and metal-alloy wastes in metallic, non-dispersible(1) form"):
The following waste and scrap of non-ferrous metals and their alloys:
>TABLE>
2. All types included in section GB ("Metal bearing wastes arising from melting, smelting and refining of metals").
3. All types included in section GE ("Glass waste in non-dispersible form").
4. In section GG ("Other wastes containing principally inorganic constituents, which may contain metals and organic materials"):
>TABLE>
5. All types in section GI ("Paper, paperboard and paper product wastes").
6. In section GJ ("Textile wastes"):
>TABLE>"
2. All the text related to Brazil is replaced by the text: "BRAZIL
1. In section GA ("Metal and metal-alloy wastes in metallic, non-dispersible(2) form"):
The following waste and scrap of non-ferrous metals and their alloys:
>TABLE>
2. In section GB ("Metal bearing wastes arising from melting, smelting and refining of metals"):
>TABLE>
3. In section GG ("Other wastes containing principally inorganic constituents, which may contain metals and organic material"):
>TABLE>
4. In section GK ("Rubber wastes"):
>TABLE>"
3. All the text related to Bulgaria is deleted.
4. Between the texts related to Burkina Faso and Cameroon, the following text is inserted: "BURUNDI
All types except:
1. In section GA ("Metal and metal-alloy wastes in metallic, non-dispersible" form):
>TABLE>
2. All types in section GI ("Paper, paperboard and paper product wastes").
3. In section GJ ("Textile wastes"):
>TABLE>
4. In section GK ("Rubber wastes"):
>TABLE>
5. In section GM ("Wastes from agro-food industries"):
>TABLE>"
5. Between the texts related to Guyana and Kiribati, the following text is inserted: "JAMAICA
All types except:
All types in section GM ("Wastes arising from agro-food industries")".
6. All the text related to Nigeria is replaced by the text: "NIGERIA
All types except:
1. All types in section GA ("Metal and metal-alloy wastes in metallic, non-dispersible(3) form").
2. All types in section GB ("Metal bearing wastes arising from melting, smelting and refining of metals").
3. All types in section GH ("Solid plastic wastes").
4. All types in section GI ("Paper, paperboard and paper product wastes").
5. In section GJ ("Textile wastes"):
>TABLE>
6. In section GK ("Rubber wastes"):
>TABLE>
7. In section GM ("Wastes arising from agro-food industries"):
>TABLE>"
7. The texts related to Peru is replaced by the text: "PERU
All types except:
in section GA ("Metal and metal-alloy wastes in metallic, non-dispersible(4) form")
>TABLE>"
8. Between the texts related to Peru and São Tomé and Principe, the following text is inserted: "ROMANIA
All types except:
1. In section GA ("Metal and metal-alloy wastes in metallic, non-dispersible(5) form"):
(a) The following waste and scrap of precious metals and their alloys:
>TABLE>
NB:
Mercury is specifically excluded as a contaminant of these metals or their alloys or amalgams.
(b) The following waste and scrap of non-ferrous metals and their alloys:
>TABLE>
2. In section GE ("Glass waste in non-dispersible form"):
>TABLE>
3. In section GI ("Paper, paperboard and paper product wastes"):
>TABLE>
4. In section GJ ("Textile wastes"):
>TABLE>
5. In section GO ("Other wastes containing principally organic constituents, which may contain metals and inorganic materials"):
>TABLE>"
9. Between the texts related to Tanzania and Uganda, the following text is inserted: "TUNISIA
1. In section GC ("Other wastes containing metals"):
>TABLE>
2. In section GG ("Other wastes containing principally inorganic constituents, which may contain metals and organic materials"):
>TABLE>
3. In section GJ ("Textile wastes"):
>TABLE>
4. In section GO ("Other wastes containing principally organic constituents, which may contain metals and inorganic materials"):
>TABLE>"
(1) 'Non-dispersible' does not include any wastes in the form of powder, sludge, dust or solid items containing encased hazardous waste liquids.
(2) 'Non-dispersible' does not include any wastes in the form of powder, sludge, dust or solid items containing encased hazardous waste liquids.
(3) 'Non-dispersible' does not include any wastes in the form of powder, sludge, dust or solid items containing encased hazardous waste liquids.
(4) 'Non-dispersible' does not include any wastes in the form of powder, sludge, dust or solid items containing encased hazardous waste liquids.
(5) 'Non-dispersible' does not include any wastes in the form of powder, sludge, dust or solid items containing encased hazardous waste liquids.
ANNEX F
Annex B to Regulation (EC) No 1420/1999 is amended as follows:
1. All the text related to Burundi is deleted
2. All the text related to Morocco is replaced by the text: "MOROCCO
All types except:
All types in section GJ ("Textile wastes")"
3. All the text related to Tunisia is replaced by the text: "TUNISIA
1. In section GC ("Other wastes containing metals")
>TABLE>
2. In section GN ("Wastes arising from tanning and fellmongery operations and leather use")
>TABLE>"
4. All the text related to Zimbabwe is deleted.
