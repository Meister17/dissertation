Commission Regulation (EC) No 480/2005
of 23 March 2005
determining the extent to which applications lodged in March 2005 for import licences for certain egg sector products and poultrymeat pursuant to Regulations (EC) No 593/2004 and (EC) No 1251/96 can be accepted
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 593/2004 of 30 March 2004 opening and providing for the administration of the tariff quotas in the egg sector and for egg albumin [1], and in particular Article 5(5) thereof,
Having regard to Commission Regulation (EC) No 1251/96 of 28 June 1996 opening and providing for the administration of tariff quotas in the poultrymeat sector and albumin [2], and in particular Article 5(5) thereof,
Whereas:
HAS ADOPTED THIS REGULATION:
Article 1
Applications for import licences for the period 1 April to 30 June 2005 submitted pursuant to Regulations (EC) No 593/2004 and (EC) No 1251/96 shall be met as referred to in the Annex to this Regulation.
Article 2
This Regulation shall enter into force on 1 April 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 23 March 2005.
For the Commission
J. M. Silva Rodríguez
Director-General for Agriculture and Rural Development
[1] OJ L 94, 31.3.2004, p. 10.
[2] OJ L 161, 29.6.1996, p. 136. Regulation as last amended by Regulation (EC) No 1043/2001 (OJ L 145, 31.5.2001, p. 24).
--------------------------------------------------
ANNEX
Group No | Percentage of acceptance of import licences submitted for the period of 1 April to 30 June 2005 |
E1 | 100,00 |
E2 | 76,60 |
E3 | 100,00 |
P1 | 100,00 |
P2 | 100,00 |
P3 | 1,74 |
P4 | 100,00 |
--------------------------------------------------
