Uniform application of the Combined Nomenclature (CN)
(Classification of goods)
(2006/C 242/04)
Explanatory notes adopted in accordance with the procedure defined in Article 10 (1) of Council Regulation (EEC) No 2658/87 of 23 July 1987 on the tariff and statistical nomenclature and on the Common Customs Tariff [1]
The explanatory notes to the Combined Nomenclature of the European Communities [2] shall be amended as follows:
On page 374:
9503 "This heading includes:
1. Inflatable articles, in different forms and sizes, intended for play in the water, such as waist rings, animal shapes, etc., decorated or not, whether or not designed to sit in or on.
2. Inflatable boats designed for children to play in.
This heading does not include:
(a) Inflatable arm rings, neck rings, belts or similar articles, not constructed for security or rescuing purposes, providing buoyancy for a person, i.e. to keep them afloat, for example while learning to swim (heading 9506).
(b) Inflatable mattresses (generally constituent material).
(c) Articles which, on account of their design, are intended exclusively for animals (e.g. fabric "mice" containing cat-mint, buffalo hide shoes "for chewing", plastic bones).
See also Note 4 to this chapter."
On page 375 insert the following text:
"95062900 (a) Life-belts and life-jackets (constituent material).
(b) Inflatable articles constructed for play (heading 9503)."
[1] OJ L 256, 7.9.1987, p. 1. Regulation as last amended by Commission Regulation (EC) No 996/2006 (OJ L 179, 1.7.2006, p. 26).
[2] OJ C 50, 28.2.2006, p. 1.
--------------------------------------------------
