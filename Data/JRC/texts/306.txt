Commission Regulation (EC) No 2872/2000
of 28 December 2000
amending Regulation (EEC) No 1859/93 on the application of the system of import licences for garlic imported from third countries
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 2200/96 of 28 October 1996 on the common organisation of the market in fruit and vegetables(1), as last amended by Regulation (EC) No 2699/2000(2), and in particular Article 31(2) thereof,
Whereas:
(1) The Annex to Commission Regulation (EC) No 544/97 of 25 March 1997 introducing certificates of origin for garlic imported from certain third countries(3), as last amended by Regulation (EC) No 2520/98(4), contains a list of third countries. The release for free circulation of garlic from those countries is conditional on the presenttion of a certificate of origin issued by the competent national authorities of those countries in accordance with Articles 55 to 65 of Commission Regulation (EEC) No 2454/93 of 2 July 1993 laying down provisions for the implementation of Council Regulation (EEC) No 2913/92 establishing the Community Customs Code(5), as last amended by Regulation (EC) No 2787/2000(6).
(2) Articles 63, 64 and 65 of Regulation (EEC) No 2454/93 provide that the third countries concerned are to forward certain information necessary for setting up a procedure for administrative cooperation between the Community departments and the authorities in those countries.
(3) Some of the countries listed in the Annex to Regulation (EC) No 544/97, namely Lebanon, the United Arab Emirates, Vietnam and Malaysia, have still not sent the Commission the information needed for setting up administrative cooperation as provided for in Articles 63, 64 and 65 of Regulation (EEC) No 2454/93.
(4) Commission Regulation (EEC) No 1859/93(7), as last amended by Regulation (EC) No 1662/94(8), provides that garlic may be released for free circulation in the Community on presentation of import licences issued by the Member States concerned. No provisions of that Regulation prohibit the issuing of import licences for garlic originating in countries where the administrative cooperation procedure as referred to above has not yet been set up.
(5) The result of the foregoing is a risk of fraud on import. The necessary measures must be taken to eliminate that risk.
(6) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Fresh Fruit and Vegetables,
HAS ADOPTED THIS REGULATION:
Article 1
The following paragraph is hereby added to Article 3 of Regulation (EEC) No 1859/93:
"3. No licence may be issued for imports of products originating in any of the countries listed in the Annex to Regulation (EC) No 544/97 that have not sent the Commission the information necessary for setting up an administrative cooperation procedure in accordance with Articles 63, 64 and 65 of Regulation (EEC) No 2454/93. Such information shall be regarded as having been forwarded on the date of publication provided for in Article 3 of Regulation (EC) No 544/97."
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 28 December 2000.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 297, 21.11.1996, p. 1.
(2) OJ L 311, 12.12.2000, p. 9.
(3) OJ L 84, 26.3.1997, p. 8.
(4) OJ L 315, 25.11.1998, p. 10.
(5) OJ L 253, 11.10.1993, p. 1.
(6) OJ L 330, 27.12.2000, p. 1.
(7) OJ L 170, 13.7.1993, p. 10.
(8) OJ L 176, 9.7.1994, p. 1.
