Information communicated by Member States regarding State aid granted under Commission Regulation (EC) No 68/2001 of 12 January 2001 on the application of Articles 87 and 88 of the EC Treaty to training aid
(2005/C 183/07)
(Text with EEA relevance)
Aid No | XT 53/03 |
Member State | Italy |
Region | Autonomous Province of Trento |
Title of aid scheme or name of company receiving individual aid | Funding for company, sectoral and regional training schemes — 2004 |
Legal basis | Deliberazione della Giunta Provinciale n. 3035 di data 28/10/2003. |
Annual expenditure planned or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount — year 2004 | EUR 615000(EUR 0,62 million) |
Loans guaranteed |
Individual aid | Overall aid amount |
Loans guaranteed |
Maximum aid intensity | In conformity with Article 4(2)-(6) of the Regulation | Yes | |
Date of implementation | 28.11.2003 |
Duration of the scheme or individual aid award | Until 31.12.2006 |
Objective of aid | General training | Yes |
Specific training | Yes |
Economic sectors | All sectors eligible for training aid | Yes |
Limited to specific sectors | |
—Agriculture | |
—Fisheries and aquaculture | |
—Coalmining | |
—All manufacturing | |
or | |
Steel | |
Shipbuilding | |
Synthetic fibres | |
Motor vehicles | |
Other manufacturing | |
—All services | |
or | |
Maritime transport services | |
Other transport services | |
Financial services | |
Other services | |
Name and address of the granting authority | Name:Provincia Autonoma di Trento — Servizio Addestramento e Formazione professionale |
Address:via Gilli, 3 — I-38100 Trento |
Large individual aid grants | In conformity with Article 5 of the RegulationThe measure excludes awards of aid or requires prior notification to the Commission of awards of aid, if the amount of aid granted to one enterprise for a single training project exceeds EUR 1 million. | Yes | |
--------------------------------------------------
