Council Regulation (EC) No 423/2004
of 26 February 2004
establishing measures for the recovery of cod stocks
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 37 thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament(1),
Whereas:
(1) Recent scientific advice from the International Council for the Exploration of the Sea (ICES) has indicated that a number of cod stocks in Community waters have been subjected to levels of mortality by fishing which have eroded the quantities of mature fish in the sea to the point at which the stocks may not be able to replenish themselves by reproduction and that these stocks are therefore threatened with collapse.
(2) These stocks are cod in the Kattegat, the North Sea, including the Skagerrak and the eastern Channel, to the west of Scotland and in the Irish Sea.
(3) Measures need to be taken to establish multi-annual plans for the recovery of these stocks.
(4) It is expected that recovery of these stocks under the conditions of this Regulation will take between five and 10 years.
(5) The objective of the plan concerning such measures should be considered to be achieved for a stock when, for two consecutive years, the quantity of mature cod has been greater than that decided upon by managers as being within safe biological limits.
(6) In order to achieve that objective, the fishing mortality rates should be controlled so that it is highly likely that the quantities of mature fish in the sea increase from year to year.
(7) Such control of fishing mortality rates can be achieved by establishing an appropriate method for the establishment of the level of the total allowable catches (TACs) of the stocks concerned, and a system whereby fishing effort on those stocks is constrained to levels so that the TACs are unlikely to be exceeded.
(8) Once recovery has been achieved, the Commission should propose, and the Council should decide upon, follow-up measures in accordance with Article 6 of Council Regulation (EC) No 2371/2002 of 20 December 2002 on the conservation and sustainable exploitation of fisheries resources under the common fisheries policy(2).
(9) Control measures in addition to those laid down in Council Regulation (EEC) No 2847/93 of 12 October 1993 establishing a control system applicable to the common fisheries policy(3) are required to ensure compliance with the measures laid down in this Regulation,
HAS ADOPTED THIS REGULATION:
CHAPTER I SUBJECT MATTER AND DEFINITIONS
Article 1
Subject-matter
This Regulation establishes a recovery plan for the following cod stocks (hereinafter referred to as "depleted cod stocks"):
(a) cod in the Kattegat;
(b) cod in the North Sea, in the Skagerrak and the eastern Channel;
(c) cod to the west of Scotland;
(d) cod in the Irish Sea.
Article 2
Definitions of geographical areas
For the purposes of this Regulation, the following definitions of geographical areas shall apply:
(a) "Kattegat" means that part of division III a, as delineated by ICES, that is bounded on the north by a line drawn from the Skagen lighthouse to the Tistlarna lighthouse, and from this point to the nearest point on the Swedish coast, and on the south by a line drawn from Hasenore to Gnibens Spids, from Korshage to Spodsbjerg and from Gilbjerg Hoved to Kullen;
(b) "North Sea" means ICES subarea IV and that part of ICES division III a not covered by the Skagerrak and that part of ICES division II a which lies within waters under the sovereignty or jurisdiction of Member States;
(c) "Skagerrak" means that part of ICES division III a bounded on the west by a line drawn from the Hanstholm lighthouse to the Lindesnes lighthouse and on the south by a line drawn from the Skagen lighthouse to the Tistlarna lighthouse and from that point to the nearest point on the Swedish coast;
(d) "eastern Channel" means ICES division VII d;
(e) "Irish Sea" means ICES division VII a;
(f) "west of Scotland" means ICES division VI a and that part of ICES division V b which lies within waters under the sovereignty or jurisdiction of Member States.
CHAPTER II TARGET LEVELS
Article 3
Purpose of the recovery plan
The recovery plan referred to in Article 1 shall aim to increase the quantities of mature fish to values equal to or greater than the target levels specified in the following table:
>TABLE>
Article 4
Reaching of target levels
Where the Commission finds, on the basis of advice from ICES and following agreement on that advice by the Scientific Technical and Economic Committee for Fisheries (STECF), that for two consecutive years the target level for any cod stock concerned has been reached, the Council shall decide by qualified majority on a proposal from the Commission to remove that stock from the scope of this Regulation and to establish a management plan for that stock in accordance with Article 6 of Regulation (EC) No 2371/2002.
CHAPTER III TOTAL ALLOWABLE CATCHES
Article 5
Setting of TACs
A TAC shall be set in accordance with Article 6 where the quantities of mature cod have been estimated by the STECF, in the light of the most recent report of ICES, to be equal to or above the minimum levels specified in the following table:
>TABLE>
Article 6
Procedure for setting TACs
1. Each year, the Council shall decide by qualified majority, on the basis of a proposal from the Commission, on a TAC for the following year for each of the depleted cod stocks.
2. The TACs shall not exceed a level of catches which a scientific evaluation, carried out by the STECF in the light of the most recent report of the ICES, has indicated will result in an increase of 30 % in the quantities of mature fish in the sea at the end of the year of their application, compared to the quantities estimated to have been in the sea at the start of that year.
3. The Council shall not adopt a TAC whose capture is predicted by the STECF, in the light of the most recent report of the ICES, to generate in its year of application a fishing mortality rate greater than the following values:
>TABLE>
4. Where it is expected that application of paragraph 2 will result in a quantity of mature fish at the end of the year of application of the TAC in excess of the quantity indicated in Article 3, the Commission shall carry out a review of the recovery plan and propose any adjustments necessary on the basis of the latest scientific evaluations. Such a review shall in any event be carried out by 16 March 2007.
5. Except for the first year of application of this Article:
(a) where the rules provided for in paragraphs 2 or 4 would lead to a TAC which exceeds the TAC of the preceding year by more than 15 %, the Council shall adopt a TAC which shall not be more than 15 % greater than the TAC of that year; or
(b) where the rules provided for in paragraphs 2 or 4 would lead to a TAC which is more than 15 % less than the TAC of the preceding year, the Council shall adopt a TAC which is not more than 15 % less than the TAC of that year.
6. Paragraphs 4 or 5 shall not apply when their application would entail an exceeding of the values laid down in paragraph 3.
Article 7
Setting TACs in exceptional circumstances
Where the quantities of mature fish of any of the cod stocks concerned have been estimated by the STECF, in the light of the most recent report of the ICES, to be less than the quantities set out in Article 5, the following rules shall apply:
(a) Article 6 shall apply where its application is expected to result in an increase in the quantities of mature fish at the end of the year of application of the TAC to a quantity equal to or greater than the quantity indicated in Article 5;
(b) where the application of Article 6 is not expected to result in an increase in the quantities of mature fish at the end of the year of application of the TAC to a quantity equal to or greater than the quantity indicated in Article 5, the Council shall decide by a qualified majority, on a proposal from the Commission, on a TAC for the following year that is lower than the TAC resulting from the application of the method described in Article 6.
CHAPTER IV FISHING EFFORT LIMITATION
Article 8
Fishing effort limitations and associated conditions
1. The TACs referred to in Chapter III shall be complemented by a system of fishing effort limitation based on the geographical areas and groupings of fishing gear, and the associated conditions for the use of these fishing opportunities specified in Annex V to Council Regulation (EC) No 2287/2003 of 19 December 2003 fixing for 2004 the fishing opportunities and associated conditions for certain fish stocks and groups of fish stocks, applicable in Community waters and, for Community vessels, in waters where catch limitations are required(4).
2. Each year, the Council shall decide by a qualified majority, on the basis of a proposal from the Commission, on adjustments to the number of fishing days for vessels deploying gear of mesh size equal to or greater than 100 mm in direct proportion to the annual adjustments in fishing mortality that are estimated by ICES and STECF as being consistent with the application of the TACs established according to the method described in Article 6.
3. The Council may decide by a qualified majority, on a proposal from the Commission, on alternative arrangements for fishing effort limitations to be applied under the recovery plan in order to manage fishing effort consistently with the TACs established according to the method described in Article 6.
4. If no decision is taken according to paragraphs 2 and 3, the provisions of Annex V of Regulation (EC) No 2287/2003 shall continue to apply until a decision is adopted by the Council pursuant to Article 4.
CHAPTER V MONITORING, INSPECTION AND SURVEILLANCE
Article 9
Fishing effort messages
Notwithstanding Article 19a of Regulation (EEC) No 2847/93, Articles 19b, 19c, 19d and 19e and 19k of that Regulation shall apply to vessels operating in the geographical areas defined in Article 2 of this Regulation. However, vessels not authorised to keep on board and use fishing gear for catching species from stocks referred to in Article 1 of this Regulation shall be exempt from this requirement.
Article 10
Alternative control measures
Member States may implement alternative control measures to ensure compliance with reporting obligations referred in Article 9 which are as effective and transparent as these reporting obligations. Such alternative measures shall be notified to the Commission before being implemented.
Article 11
Prior notification
1. The master of a Community fishing vessel, or his/her representative, prior to any entry into port or any landing location of a Member State carrying more than one tonne of cod on board shall inform the competent authorities of that Member State, at least four hours in advance of such entry, of:
(a) the name of the port or landing location;
(b) the estimated time of arrival at that port or landing location;
(c) the quantities in kg live weight of all species of which more than 50 kg is retained on board.
2. The competent authorities of a Member State in which a landing of more than one tonne of cod is to be made may require that the discharge of catch retained on board shall not commence until authorised by those authorities.
3. The master of a Community fishing vessel, or his/her representative, wishing to tranship or discharge at sea any quantity retained on board or to land in a port or landing location of a third country shall inform the competent authorities of the flag Member State, at least 24 hours prior to transhipping or discharging at sea or to landing in a third country, of the information referred to in paragraph 1.
Article 12
Designated ports
1. Where more than two tonnes of cod are to be landed in the Community from a Community fishing vessel, the master of the vessel shall ensure that such landings are made only at designated ports.
2. Each Member State shall designate ports into which any landing of cod in excess of two tonnes shall take place.
3. Each Member State shall transmit to the Commission by 31 March 2004 the list of designated ports and, within 30 days thereafter, associated inspection and surveillance procedures for those ports, including the terms and conditions for recording and reporting the quantities of cod within each landing.
The Commission shall transmit this information to all Member States.
Article 13
Margin of tolerance in the estimation of quantities reported in the logbook
By way of derogation from Article 5(2) of Commission Regulation (EEC) No 2807/83 of 22 September 1983 laying down detailed rules for recording information on Member States catches of fish(5), the permitted margin of tolerance, in the estimation of quantities, in kg retained on board, shall be 8 % of the logbook figure.
Article 14
Separate stowage of cod
It shall be prohibited to retain on board a Community fishing vessel in any container any quantity of cod mixed with any other species of marine organisms. Containers with cod shall be stowed in the hold in such a way that they are kept separate from other containers.
Article 15
Transport of cod
1. The competent authorities of a Member State may require that any quantity of cod caught in any of the geographical areas defined in Article 2 and first landed in that Member State is weighed in the presence of controllers before being transported elsewhere from the port of first landing. For cod first landed in a port designated pursuant to Article 12, representative samples, amounting to at least 20 % of the landings in number shall be weighed in the presence of controllers authorised by the Member States before they are offered for first sale and sold. To this end, the Member States shall submit to the Commission, within one month of the date of entry into force of this Regulation, details of the sampling regime to be employed.
2. By way of derogation from the conditions laid down in Article 13 of Regulation (EEC) No 2847/93, all quantities of cod greater than 50 kg which are transported to a place other than that of first landing or import shall be accompanied by a copy of one of the declarations provided for in Article 8(1) of that regulation pertaining to the quantities of cod transported. The exemption provided for in Article 13(4)(b) of that Regulation shall not apply.
Article 16
Specific monitoring programme
By way of derogation from Article 34c(1) of Regulation (EEC) No 2847/93, the specific monitoring programmes for the cod stocks concerned may last more than two years from their date of entry into force.
CHAPTER VI FINAL PROVISIONS
Article 17
Entry into force
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 26 February 2004.
For the Council
The President
N. Dempsey
(1) Opinion of 23 October 2003 (not yet published in the Official Journal).
(2) OJ L 358, 31.12.2002, p. 59.
(3) OJ L 261, 20.10.1993, p. 1. Regulation as last amended by Regulation (EC) No 1954/2003 (OJ L 289, 7.11.2003, p. 1).
(4) OJ L 344, 31.12.2003, p. 1.
(5) OJ L 276, 10.10.1983, p. 1. Regulation as last amended by Regulation (EC) No 1965/2001 (OJ L 268, 9.10.2001, p. 23).
