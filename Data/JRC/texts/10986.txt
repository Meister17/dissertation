[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 23.5.2006
COM(2006) 243 final
2006/0078 (CNS)
2006/0079 (CNS)
REPORT FROM THE COMMISSION
to the European Parliament and to the Council concerning the implementation and results of the Pericles programme for the protection of the euro against counterfeiting
Proposal for a
COUNCIL DECISION
amending and extending Decision 2001/923/EC establishing an exchange, assistance and training programme for the protection of the euro against counterfeiting (the ‘Pericles’ programme)
Proposal for a
COUNCIL DECISION
extending to the non-participating Member States the application of Decision 2006/…/EC amending and extending Decision 2001/923/EC establishing an exchange, assistance and training programme for the protection of the euro against counterfeiting (the ‘Pericles’ programme)
(presented by the Commission)
REPORT FROM THE COMMISSION
to the European Parliament and to the Council concerning the implementation and results of the Pericles programme for the protection of the euro against counterfeiting
1. General
Pericles, the Community programme for exchange, assistance and training in the protection of the euro against counterfeiting, was established by Council Decision of 17 December 2001 (2001/923/EC), ‘the Pericles Decision’, as amended and extended by the Council Decision 2006/75/EC and is designed to support and supplement the measures undertaken by the Member States and in existing programmes to protect the euro against counterfeiting. Such measures include information exchange (seminars, workshops, meetings and conferences), placements and exchanges of staff, as well as technical, scientific and operational back-up.
Article 13(3)(a) of the Pericles Decision requires a report evaluating the Programme accompanied by an appropriate proposal on the continuation or adaptation of the programme. The evaluation report of the Pericles Programme was issued on 30 November 2004 and presented to the European Parliament and Council. The Commission presented, on 8 April 2005, a proposal, based on which the Council extended the Programme for the year 2006 with a financial envelope of one million euro.
Article 13(3)(b) requires a detailed report on the implementation and results of the Programme to be submitted to the European Parliament and the Council by 30 June 2006. The present report responds to that requirement.
2. Developments in euro counterfeiting
Since early Summer 2003, the number of counterfeit euro banknotes detected in circulation has stabilised at about 50 000 a month, a level below the pre-euro levels, lower than the US dollar and extremely low compared to the nine billion genuine euro banknotes in circulation. At the same time, the number of counterfeit euro coins is continuing to increase but also remains low by historical standards. In addition, the police forces have successfully conducted a number of operations to dismantle workshops and seize large numbers of counterfeit banknotes and coins before they enter into circulation.
Tables 1a and 1b summarise developments in the counterfeiting of euro banknotes and coins.
Table 1a | Table 1b |
Counterfeit euro banknotes detected in circulation | Counterfeit euro coins detected in circulation |
2002 - 2005 | 2002 - 2005 |
Jan-Jun 2002 | 21965 | Jan-Jun 2002 | 68 |
Jul-Dec 2002 | 145153 | Jul-Dec 2002 | 2271 |
Year 2002 | 167118 | Year 2002 | 2339 |
Jan-Jun 2003 | 230534 | Jan-Jun 2003 | 8100 |
Jul-Dec 2003 | 311925 | Jul-Dec 2003 | 18091 |
Year 2003 | 542459 | Year 2003 | 26191 |
Jan-Jun 2004 | 307000 | Jan-Jun 2004 | 36191 |
Jul-Dec 2004 | 287000 | Jul-Dec20 04 | 38309 |
Year 2004 | 594000 | Year 2004 | 74500 |
Jan-Jun 2005 | 293442 | Jan-Jun 2005 | 184007 |
Jul-Dec 2005 | 287459 | Jul-Dec 2005 | 78677 |
Year 2005 | 580901 | Year 2005 | 262684 |
This overall favourable situation is the result of a long preparation at both legislative and institutional level and demonstrates the high level of cooperation achieved in EU and at international level. The Commission set out the basic ideas for the protection of the euro in a Communication it published as early as 1998[1]. On that basis, the Council adopted a basic Regulation in 2001[2], laying the institutional foundations for the protection structure; that was preceded by a Council Framework Decision[3], adopted in 2000, by which protection of the euro with criminal penalties is strengthened and, to some extent, harmonised. In 1999 Europol’s mandate was extended to include money counterfeiting[4] and it was further enhanced in 2005 with Europol being designated as the Central Office for combating euro counterfeiting. With regard to criminal sanctions, the Commission has published two reports[5] on the implementation of the above-mentioned Framework Decision, which show a satisfactory level of implementation.
With a view to ensuring the clear structure in the fight against currency counterfeiting as well as close cooperation and efficient flow of information National Central Offices (NCO) were established in all Member States. Databases and communication systems are operating in the ECB and Europol. Dedicated bodies were created for the technical analysis of counterfeits in the Member States, the ECB - for banknotes, and the Commission - for coins.
The Pericles programme is playing a significant role in achieving the present results in the protection of the euro and the fight against the crime of counterfeiting, through the exchange of information and the development of cooperation. Continuing vigilance is needed in order to maintain and build on the results currently achieved in the fight euro counterfeiting. Training and technical assistance plays an important role in this respect, hence the need to continue the Pericles programme.
3. The evaluation report
In line with Article 13(3)(a) of the Pericles Decision, the evaluation of the Pericles programme was assigned to the independent auditor of the European Anti-fraud Office (OLAF) who submitted the evaluation report on 30 November 2004.
The evaluator examined the files of 21 of the Pericles actions carried out until March 2004, on Member State or Commission initiative. Based on questionnaires to organisers and participants, as well as discussions with stake holders, he reached the following main conclusions[6]:
- The programme has improved awareness of the Community dimension of the euro and has also developed a greater understanding amongst the participants of the related laws and instruments and, in particular, of the relevant Community and broader European law.
- With regard to the range of information exchanges and methodologies/measures, most have been presented in the various workshops, meetings and seminars.
- The target groups for the programme have been reached in part with a very high participation by law enforcement officials; attendance by commercial banking sector, specialist lawyers or chambers of commerce was not sufficient.
- The activities examined were considered relevant to and among the main objectives of the programme.
- In terms of costs, the evaluator found that some of the projects were particularly costly and highlighted specific cost items.
The evaluator made several recommendations that have been taken into consideration for the Pericles 2006 programme. Notably:
- The programme should continue for a further 4 years at least with the same budget (€ 1 million per year) and with the same measures and target groups. A second evaluation should take place after four years.
- Emphasis should be put on practical training. There should be a prioritisation in favour of staff exchanges and specific training, including case studies. These activities are also more cost efficient.
4. The implementation of the programme
Based on the reference amount of 4 million euro for the period 2002-2005 and 1 million euro for 2006, the annual appropriations authorised under the Pericles programme, were € 1.2 million for 2002; € 0.9 million for 2003; € 0.9 million for 2004; € 1 million for 2005 and € 1 million for 2006.
The implementation of the Programme made a slow start, mainly due to its adoption in December 2001. Thus, the first project under Pericles was only carried out in October 2002 and the amount committed in 2002 was just under 40% of the initial budget allocation (the allocation was reviewed downwards in the course of the year). Subsequently, the programme took off and the budget allocation was committed at high levels in 2003, 2004 and 2005. Member States expressed intentions show complete implementation in 2006.
Based on these statistics/forecasts, the overall level of commitments during the period 2002-2006 will have reached 80% of the initial reference amount.
The main aggregates in the implementation of Pericles are shown on Table 2.
TABLE 2 | PERICLES - SUMMARY OF IMPLEMENTATION 2002 – 2005 AND INTENTIONS FOR 2006 |
MS | 5 |
COM | 2 |
2003 | 16 | PT, IT, DE, ES, FR, FI, EL | Police, judiciary, financial, commerce | 753 | 847 168 | 900 000 | 94.1 % |
MS | 12 |
COM | 4 |
2004 | 15 | AT, FR, DE, EL, IT, ES, PT, | Police, judiciary, financial, banks | 586 | 774 926 | 900 000 | 86.1% |
MS | 11 |
COM | 4 |
2005 | 12 | FR, DE, IT, ES | Financial, banks, police, judiciary, commerce | 738 | 921 912 | 1 000 000 | 92.1% |
MS | 7 |
COM | 5 |
2006 (intentions) | 14 | B, DE, ES, FR, HU, IT, IR, PO | Technical, financial, banks, judiciary | 700 | 980 000 | 1 000 000 | 98.0% |
MS | 13 |
COM | 1 |
TOTAL (estimates) | 64 | 3143 | 3 998 911 | 5 000 000 | 80.0% |
MS | 48 |
COM | 16 |
5. Value added of the programme
The Pericles programme has been offering substantial value added to the protection of the euro against counterfeiting. This has materialised through a broad range of actions undertaken, the precise targeting of the actions and the large numbers of participants. Emphasis was put on the European dimension of the fight against euro counterfeiting and, in addition to the EU Member States, it was possible to target sensitive geographical areas. Finally, the Pericles programme has made a substantial contribution to the further improvement of coordination and cooperation at international, European and Member States level, as well as the creation of more solid structures for the protection of the European currency.
Broad range of Actions
In the course of the first four years of the programme 50 projects were initiated; for 2006 another 14 actions are intended by the Member States and the Commission. Of the total 64 projects, 48 originate from the competent authorities of Member States, while 16 were initiatives of the Commission/OLAF.
Most of the actions carried out have been conferences, seminars and workshops, as well as specialised training courses. Staff exchange has, nonetheless, gained ground and has now become a standard feature of Pericles. Following enlargement, this type of activity is likely to develop further, which is also in line with the recommendation of the Pericles evaluator. Only one technical study was implemented under the current Pericles and another two are in the process of implementation in 2006. The analysis of Pericles by type of project is shown in Table 3.
TABLE 3 | PERICLES – ANALYSIS BY TYPE OF ACTION 2002 – 2006 |
Financial year | Conference-type | Staff exchange | Studies | Total |
2002 | 6 | 1 | - | 7 |
2003 | 11 | 4 | 1 | 16 |
2004 | 10 | 5 | - | 15 |
2005 | 8 | 3 | 1 | 12 |
2006 (intentions) | 9 | 4 | 1 | 14 |
TOTAL (estimates) | 44 | 17 | 3 | 64 |
Selected target groups and participants
About 3150 experts participated in these events. In the first years of the programme, the large majority of participants were law enforcement agents not normally involved in the Community efforts to prevent euro counterfeiting. This focus reflects the initial priority of establishing closer professional ties for a more efficient fight against euro counterfeiting. In this respect, the evaluation report (p. 10 and 11) shows that, until March 2004, 65% of participants were mainly from law enforcement agencies. As of 2003, a greater involvement of judiciary authorities is observed, while as of 2004 and 2005, there is a more pronounced participation of the financial sector (public sector financial intermediaries, National Central Banks, commercial banking and other financial institutions).
This development is in line with the recommendation of the evaluator, while the organisation of more technical seminars is also being promoted. In managing the programme, it was sought to avoid overlapping in participation; and the evaluation report notes that this was achieved.
In terms of origin of the participants, the evaluation report notes that these came from 76 countries, a majority from Member States. It was noted that some countries showed a low level of participation, among them some of the euro area countries. This situation may reflect organisational rather than structural situations and an effort to balance it is currently under way. Some countries are more active in organising events, with Italy representing over 21% of the total number of activities. As of 2005, the new Member States became active in organising Pericles actions.
European Dimension
As the evaluation report also mentions (p. 14) that the organisation of Pericles actions covered all the areas relevant to the protection of the euro: law enforcement, judicial, financial and technical and promoted particularly the creation of networks useful for achieving greater efficiency in the fight against the crime of counterfeiting. The European dimension of the protection of the euro is emphasised through the systematic involvement of ECB, Europol and other European and international organisations in Pericles actions. Europol complements the role of OLAF in by its involvement in the evaluation and implementation phases of the Pericles programme. Europol collaborates with OLAF in the designing and implementation of specialised projects such as the training entitled ‘Bitmap’; Europol supplied the technical expertise, while the Commission/OLAF provided the infrastructure and Pericles funding. With regard to the aspects of euro protection, where the responsibility belongs to the Member States, expertise was systematically sought in their specialised services.
Geographical Emphasis
The Pericles actions have taken place mostly inside the EU. However, a number of actions have been organised in third or candidate countries, reflecting the specific needs to protect the euro. Awareness-raising actions were a priority for candidate countries before accession. By identifying areas having a major impact on the production of counterfeit banknotes, South America, in particular Colombia became a major focus for Pericles actions, as were countries neighbouring the EU, including Bulgaria and Romania.
As a result the geographical areas covered and the diversified target groups reached, the transnationality and multidisciplinarity dimensions of the programme, as required under Article 3 of the Pericles Decision have been respected. Multidisciplinarity has been achieved by ensuring the level of expertise and by controlling the professional background of the participants, as well as verifying the content of the actions.
Coordination among European Bodies and within the Commission
In line with Article 5 of the Pericles Decision, the programme is implemented and coordinated by the Commission and the Member States working closely together. The coordination of the Pericles and other training actions is carried out at the Commission’s Euro Counterfeiting Experts Group, which brings together experts from all Member States and candidate countries, with participation of Europol, the ECB and Interpol. This forum ensures that resources and efforts to protect the euro are used in the most effective manner. Training and technical assistance actions carried out by the ECB and Europol are closely coordinated with Pericles. This coordination is in also line with Recital (7) of the Pericles Decision.
Pericles has now practically centralised Community level initiatives carried out by the Commission and Member State with respect to the protection of the euro and has also largely replaced the Commission’s specific ‘Protection of the euro’ budget line. A small number of actions geared to the protection against currency counterfeiting are carried out under other Community programmes, such as TAIEX and Twinning. Such actions are mainly single-country or single-subject actions (i.e. not eligible under Pericles) and are systematically coordinated with Pericles by the competent service in the Commission in coordination with Member States.
Increased cooperation and coordination among Member States
The success of the Pericles programme is demonstrated by the increased effectiveness of the cooperation among law enforcement agents, and more recently, representatives of the judiciary and financial institutions. This is true particularly between Member States but also with respect to acceding, candidate and other neighbouring countries. In addition to its training and technical content, the Pericles programme provides a forum for regular contacts among experts responsible for the protection of the euro but importantly, also enables professionals to develop links which contribute to closer working relationships and improved cooperation overall. Close coordination of Pericles’ projects with Community and Member State initiatives has also led to more training methods based on best practices and a better managed content of training actions. This has ensured that the best experts available are trained by top level people while the content of each action has been targeted to the audience to achieve particular aims.
Structural improvements
Significantly, in addition to their awareness and training content, Pericles actions have led to a number of structural and other improvements in Member States and in third countries. Among others, National Central Offices for the fight against counterfeiting were created in several countries; two Pericles seminars assisted the (then) acceding countries in their efforts to apply the Community acquis in the specific area of protecting the euro; a code of conduct was drawn with respect to press and communication issues; and one of the workshops led to a proposal, by Member States, for a Council Recommendation.
* * *
EXPLANATORY MEMORANDUM
Pericles, the Community programme for exchange, assistance and training in the protection of the euro against counterfeiting, was established by Council Decision of 17 December 2001 (2001/923/EC) and is designed to support and supplement the measures undertaken by the Member States and in existing programmes to protect the euro against counterfeiting. Based on the Pericles evaluation report of 30 November 2004 it is proposed that the Council Decision 17 December 2001 now be extended until 31 December 2013.
The first extension to 2006
Pericles initially ran from 1 January 2002 until end 2005. On 8 April 2005 the Commission submitted a amending and extending the Pericles Programme for the period from 1 January 2006 to 31 December 2011 (COM/2005/0127/final). Based on the Pericles evaluation report of 30 November 2004, the Council agreed that the Pericles programme should be further extended.
The Council decided to extend the programme up until 31 December 2006 taking into consideration the fact that at the time of discussions Community financial perspectives were available only until 2006.
Proposed further extension and amount
The agreement reached at the Council provides that the Pericles programme should be further extended for a number of years, up to 2011, as was proposed at the time by the Commission. Specifically the Council stated at its meeting on 30 January 2006 the following: ‘the Council considers that Pericles has a multi-annual nature and it should be extended to 2011. To this effect it invites the Commission to present a proposal for extending the programme for the period starting from 2007 as soon as an agreement on the future financial framework 2007-2013 has been reached’[7].
In detail, the Council considered the following grounds for extending the programme:
- continuing vigilance in order to maintain or reduce the current level of euro banknote counterfeiting and avoid any increase in euro coin counterfeiting that would undermine the confidence of the public;
- training/informing new staff and extend the training to sectors that have less benefited from the Pericles programme, namely financial agents, prosecutors and technical staff;
- training relevant staff in the features of the new generation of the euro banknotes, to be issued at the end of this decade;
- particularly insisting on training and technical assistance in the new Member States, with priority to those who will first introduce the euro as their single currency.
There is a manifest need for continuous training and technical assistance for the protection of the euro. This is both because Member States are likely to join the euro area and due to the need for continuous updating of expertise in relevant services.
The Council reached final agreement on the financial perspectives for the period 2007- 2013 at the European Council meeting on 15-16 December 2005[8].
It is current practice that Community financing programmes match the duration of the Community’s financial perspectives. This contributes to streamlining the approval of programmes and avoiding procedures to fill gap periods.
In the light of the above considerations, it is proposed that the Pericles programme be extended for a further period of seven years, until the end of 2013 with an unchanged yearly amount of one million euro per year –7 million euro in total.
Proposed modifications
Specifically, it is proposed to modify:
- Article 1, paragraph 2 of the Pericles Decision to define as new end-date 31 December 2013;
- Article 6 to set the reference amount at EUR 7 million.
Considering the period of extension it is proposed to modify the deadlines referred to in Article 13, paragraph 3 as follows:
- in point (a) replace “30 June 2005” with “30 June 2013”;
- in point (b) define an additional deadline at 30 June 2014.
The date of application in Article 15 should be 1 January 2007.
2006/0078 (CNS)
Proposal for a
COUNCIL DECISION
amending and extending Decision 2001/923/EC establishing an exchange, assistance and training programme for the protection of the euro against counterfeiting (the ‘Pericles’ programme)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular the third sentence of Article 123(4) thereof,
Having regard to the proposal from the Commission[9],
Having regard to the opinion of the European Parliament[10],
Having regard to the opinion of the European Central Bank[11],
Whereas:
(1) In accordance with Article 13(3)(a) of Council Decision 2001/923/EC [12], the Commission is to send to the European Parliament and to the Council by 30 June 2005 a report, which shall be independent of the programme manager, evaluating the relevance, the efficiency and the effectiveness of the programme and a communication on whether this programme should be continued and adapted, accompanied by an appropriate proposal.
(2) The evaluation report provided for in Article 13 of that Decision was issued on 30 November 2004. It concludes that the programme has been achieving its objectives and recommends its continuation.
(3) A financial reference amount, within the meaning of point 34 of the Interinstitutional Agreement of 6 May 1999 between the European Parliament, the Council and the Commission on budgetary discipline and improvement of the budgetary procedure[13] is inserted in this Decision for the entire duration of the programme, without the powers of the budgetary authority as defined by the Treaty being affected thereby.
(4) The continuation of the programme reflects the need for continuing vigilance, training and technical assistance necessary to sustain the protection of the euro against counterfeiting, by providing a stable framework for the planning of Member States programmes, particularly over a period during which new countries become participants in the euro.
(5) In this spirit, the Commission submitted on 8 April 2005 a proposal for the continuation of the “Pericles” programme[14] until 31 December 2011.
(6) In the process of reaching final agreement on the Community financial perspectives for 2007-2013, the Council decided to extend Pericles for the year 2006.
(7) In its conclusions of 30 January 2006, the Council agreed that Pericles has a multi-annual nature and that it should be extended to 2011. To this effect it invited the Commission to present a proposal for extending the programme for the period starting from 2007 as soon as an agreement on the future financial framework 2007-2013 has been reached.
(8) It is appropriate that the Community programmes be in line with the Community’s financial perspectives.
(9) Consequently, and in view of the need for continuous training and assistance for the protection of the euro the Pericles programme should be extended until 31 December 2013. Decision 2001/923/EC should therefore be amended accordingly,
HAS DECIDED AS FOLLOWS:
Article 1 Amendments
Decision 2001/923/EC is hereby amended as follows:
(1) The last sentence of Article 1(2) is replaced by the following:
“It shall run from 1 January 2002 to 31 December 2013.”
(2) The following paragraph is added after the second paragraph of Article 6:
“The financial reference amount for the implementation of the Community programme of action for the period from 1 January 2007 to 31 December 2013 shall be EUR 7 million.”
(3) Article 13(3) is amended as follows:
(a) In point (a), the date “30 June 2005” is replaced by “30 June 2013”.
(b) Point (b) is replaced by the following:
“(b) on completion of the initial and the additional periods of the programme and no later than 30 June 2006 and 2014 respectively, detailed reports on the implementation and the results of the programme setting out in particular the added value of the Community’s financial assistance.”
Article 2 Applicability
This Decision shall have effect in the participating Member States as defined in the first indent of Article 1 of Council Regulation (EC) No 974/98 of 3 May 1998 on the introduction of the euro[15].
Article 3 Entry into force
This Decision shall take effect on the day of its publication in the Official Journal of the European Union .
It shall apply from 1 January 2007.
Done at Brussels,
For the Council
The President
2006/0079 (CNS)
Proposal for a
COUNCIL DECISION
extending to the non-participating Member States the application of Decision 2006/…/EC amending and extending Decision 2001/923/EC establishing an exchange, assistance and training programme for the protection of the euro against counterfeiting (the ‘Pericles’ programme)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 308 thereof,
Having regard to the proposal from the Commission[16],
Having regard to the opinion of the European Parliament[17],
Whereas:
(1) When adopting Decision 2006/…/EC [18] the Council indicated that it should apply in the participating Member States as defined in the first indent of Article 1 of Council Regulation (EC) No 974/98 of 3 May 1998 on the introduction of the euro[19].
(2) However, the exchange of information and staff and the assistance and training measures implemented under the Pericles programme should be uniform throughout the Community and the requisite provisions should therefore be taken to guarantee the same level of protection for the euro in the Member States where the euro is not their official currency,
HAS DECIDED AS FOLLOWS:
Article 1
The application of Decision 2006/…/EC shall be extended to Member States other than the participating Member States as defined in the first indent of Article 1 of Council Regulation (EC) No 974/1998.
Article 2
This Decision shall take effect on the day of its publication in the Official Journal of the European Union .
Done at Brussels,
For the Council
The President
ANNEX
LEGISLATIVE FINANCIAL STATEMENT
1. NAME OF THE PROPOSAL:
Council Decision of […] extending Council Decision of 17 December 2001 establishing an exchange, assistance and training programme for the protection of the euro against counterfeiting (the ‘Pericles’ programme) as last amended and extended by Council Decision 2006/75/EC
2. ABM / ABB FRAMEWORK
Policy Area(s) concerned and associated Activity/Activities: Fight against fraud.
3. BUDGET LINES
3.1. Budget lines (operational lines and related technical and administrative assistance lines (ex- B..A lines)) including headings :
24 02 02 Pericles
3.2. Duration of the action and of the financial impact:
From 1 January 2007 until 31 December 2013
3.3. Budgetary characteristics (add rows if necessary) :
Budget line | Type of expenditure | New | EFTA contribution | Contributions from applicant countries | Heading in financial perspective |
24 02 02 | Non Comp | Diff | NO | NO | NO | No 1a) |
4. SUMMARY OF RESOURCES
4.1. Financial Resources
4.1.1. Summary of commitment appropriations (CA) and payment appropriations (PA)
EUR million (to 3 decimal places)
TOTAL REFERENCE AMOUNT |
20% by Member States | f | 0,324 | 0,324 | 0,324 | 0,324 | 0,324 | 0,324 | 0,324 | 2,268 |
TOTAL CA including co-financing | a+c+d+e+f | 1,621 | 1,621 | 1,621 | 1,621 | 1,621 | 1,621 | 1,621 | 11,347 |
4.1.2. Compatibility with Financial Programming
( The proposal is compatible with existing financial programming for the whole period of its implementation.
4.1.3. Financial impact on Revenue
( Proposal has no financial implications on revenue
5. CHARACTERISTICS AND OBJECTIVES
Details of the context of the proposal are required in the Explanatory Memorandum. This section of the Legislative Financial Statement should include the following specific complementary information:
5.1. Need to be met in the short or long term: Continuation of the facility for training and technical assistance for the protection of the euro; provision of a medium term framework for the planning of Member States.
5.2. Value-added of Community involvement and coherence of the proposal with other financial instruments and possible synergy : Emphasis on the Community dimension of the protection of the euro; strengthening cooperation and awareness on the importance of the protection of the euro against counterfeiting.
5.3. Objectives, expected results and related indicators of the proposal in the context of the ABM framework : Maintain the current overall level of training and technical assistance in the form of Pericles actions.
5.4. Method of Implementation (indicative)
Show below the method(s) [22] chosen for the implementation of the action.
( Centralised Management
( Directly by the Commission
6. MONITORING AND EVALUATION
6.1. Monitoring system
The Commission keeps detailed the Pericles projects and analyses periodically its implementation.
6.2. Evaluation
6.2.1. Ex-ante evaluation
The proposal for the extension of Pericles is based on the evaluation of the programme during the first years of its implementation. This is summarised in section 3 of the Communication (p. 4). In particular, the evaluator recommended that: the programme should continue for a further 4 years at least with the same budget (€ 1 million per year); the programme should give priority to those Member States with low participation or who did not organise actions in the first programme, as well as the new Member States. Emphasis should be put on practical training. There should be a prioritisation in favour of staff exchanges and specific training, including case studies. Co-operation between the European institutions/bodies (Commission/OLAF, ECB and Europol) should be further developed. With a view to enabling the assessment of the effect of the programme, among others on the convergence of high level training for trainers, the evaluator recommends the preparation of a strategy document, to be finalised before the new Pericles enters into effect.
With regard to individual projects, an ex-ante evaluation is carried out by the Pericles Evaluation Committee (Commission).
6.2.2. Measures taken following an intermediate/ex-post evaluation (lessons learned from similar experiences in the past):
The proposal for the extension of Pericles takes into consideration the conclusions reached by the programme evaluator (section 3, p. 4) namely:
- The programme has improved awareness of the Community dimension of the euro and has also developed a greater understanding amongst the participants of the related laws and instruments and in particular of the relevant Community and broader European law.
- With regard to the range of information exchanges and methodologies/measures, most have been presented in the various workshops, meetings and seminars.
- The target groups for the programme have been reached in part with a very high participation by law enforcement officials; attendance by commercial banking sector, specialist lawyers or chambers of commerce was not sufficient.
- The activities examined were considered relevant to and among the main objectives of the programme.
- In terms of costs, the evaluator found that some of the projects were particularly costly and highlighted specific cost items.
With regard to individual projects, the beneficiaries of each project selected submit a final and a financial report to the Commission. The Commission analyses the reports and evaluates, also on the basis of its attendance in the actions, the way in which they have been implemented and the impact they have had in order to gauge whether the objectives have been achieved.
6.2.3. Terms and frequency of future evaluation . The programme will undergo an independent evaluation in 2013 and a detailed report on its implementation will be sent to the Council and the European Parliament by June 2014.
7. ANTI-FRAUD MEASURES
The careful examination at the Evaluation Committee, the discussions at the Commission’s relevant group and the financial analysis constitute guarantees against fraud. In addition, the beneficiaries are government agencies, usually law enforcement, which minimises the likelihood of fraud.
The Commission may carry out on-the-spot checks and inspections under this programme in accordance with Council Regulation (Euratom, EC) N° 2185/96[23]. Where necessary, investigations are conducted by the European Anti-Fraud Office (OLAF) and governed by European Parliament and Council Regulation (EC) N° 1073/1999[24].
The beneficiary of an operation grant must keep available for the Commission all the supporting documents, including the audited financial statement, regarding expenditure incurred during the grant year for a period of five years following the last payment. The beneficiary of a grant must ensure that, where applicable, supporting documents in the possession of partners or members are made available to the Commission.
8 DETAILS OF RESOURCES 8.1. Objectives of the proposal in terms of their financial cost
Commitment appropriations in EUR million (to 3 decimal places)
The needs for human and administrative resources shall be covered within the allocation granted to the managing service in the framework of the annual allocation procedure.
8.2.2. Description of tasks deriving from the action
Evaluation of applications: management of the evaluation committee, contacts with applicants; participation at events.
Coordination: continuous monitoring of the implementation of Pericles; presentations at the relevant groups (Member States, ECB, Europol); contribution to the preparation of projects.
Preparation and implementation of the Pericles actions under a Commission initiative.
8.2.3. Sources of human resources (statutory)
(When more than one source is stated, please indicate the number of posts originating from each of the sources)
( Posts currently allocated to the management of the programme to be replaced or extended
8.2.4. Other Administrative expenditure included in reference amount (XX 01 04/05 – Expenditure on administrative management)
EUR million (to 3 decimal places)
Officials and temporary staff (A3 01 01) | 0,162 | 0,162 | 0,162 | 0,162 | 0,162 | 0,162 | 0,162 |
Staff financed by Art A3 01 02 (auxiliary, END, contract staff, etc.) (specify budget line) | 0,045 | 0,045 | 0,045 | 0,045 | 0,045 | 0,045 | 0,045 |
Total cost of Human Resources and associated costs (NOT in reference amount) | 0,207 | 0,207 | 0,207 | 0,207 | 0,207 | 0,207 | 0,207 |
Calculation– Officials and Temporary agents
Reference should be made to Point 8.2.1, if applicable
0,25A x 108.000 € +1B x 108.000 + 0,25C x 108.000 = 162.000
Calculation– Staff financed under art. A3 01 02
Reference should be made to Point 8.2.1, if applicable
1 END x 45.000
|
2007 |
2008 |
2009 |
2010 |
2011 |
2012 |
2013 |TOTAL | |XX 01 02 11 01 – Missions |0,030 |0,030 |0,030 |0,030 |0,030 |0,030 |0,030 |0,210 | |XX 01 02 11 02 – Meetings & Conferences | | | | | | | | | |XX 01 02 11 03 – Committees |0,060 |0,060 |0,060 |0,060 |0,060 |0,060 |0,060 |0,420 | |XX 01 02 11 04 – Studies & consultations | | | | | | | | | |XX 01 02 11 05 - Information systems | | | | | | | | | | 2 Total Other Management Expenditure (A3 01 02 11) | 0,090 |0,090 |0,090 |0,090 |0,090 |0,090 |0,090 |0,630 | | Total Administrative expenditure, other than human resources and associated costs (NOT included in reference amount) | 0,090 |0,090 |0,090 |0,090 |0,090 |0,090 |0,090 |0,630 | |Calculation - Other administrative expenditure not included in reference amount
Missions 20 x 1.500 € and 4 meetings x 15.000 €
[1] The communication of the Commission of 22 July 1998 to the Council, the European Parliament and the ECB entitled ‘Protection of the euro- combating counterfeiting’ COM(98) 474 final.
[2] OJ L 181, 4/7/2001 Co湵楣⁬敒畧慬楴湯⠠䍅 潮ㄠ㌳⼸〲㄰氠祡湩⁧潤湷洠慥畳敲⁳敮散獳牡⁹潦⁲桴⁥牰瑯捥楴湯漠⁦桴⁥略潲uncil Regulation (EC) no 1338/2001 laying down measures necessary for the protection of the euro against counterfeiting; and OJ L 181, 4/7/2001 Council Regulation (EC) no 1339/2001 extending the effects of Regulation (EC) N° 1338/2001 to those Member States which have not adopted the euro as their single currency.
[3] OJ L 140, 29/05/2000 Council Framework Decision on increasing protection by criminal penalties and other sanctions against counterfeiting in connection with the introduction of the euro
[4] OJ C 149/16, 28/05/1999 Council Decision of 29.04.1999 extending Europol’s mandate to deal with forgery of money and means of payment
[5] Commission Report COM(2001) 771 of 13.12.2001, Second Commission report COM(2003) 532 of 03.09.2003
[6] Evaluation report p.4, 21, 22 and 23
[7] General Affairs and External Relations Council Conclusions 30 January 2006 p. 11
[8] (Doc. 15915/05)
[9] OJ C , , p.
[10] OJ C , , p.
[11] OJ C , , p.
[12] OJ L 339, 21.12.2001, p.50.. Decision as amended by Decision 2006/75/EC (OJ L 36, 8.2.2006, p. 40).
[13] OJ C 172, 18.6.1999, p. 1.
[14] COM (2005)127 final.
[15] OJ L 139, 11.5.1998, p. 1. Regulation as amended by Regulation (EC) No 2169/2005 (OJ L 346, 29.12.2005, p. 1).
[16] OJ C , , p.
[17] OJ C , , p.
[18] See page … of this Official Journal.
[19] OJ L 139, 11.5.1998, p.1.
[20] Expenditure that does not fall under Chapter xx 01 of the Title xx concerned.
[21] Expenditure within article xx 01 04 of Title xx.
[22] If more than one method is indicated please provide additional details in the "Relevant comments" section of this point
[23] OJ L 292, 15.11.1996, p. 2.
[24] OJ L 136 du 31.5.1999, p. 1.
[25] Cost of which is NOT covered by the reference amount
[26] Cost of which is NOT covered by the reference amount
Cost of which is NOT covered by the reference amount
