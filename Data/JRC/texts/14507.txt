Judgment of the Court (Fourth Chamber) of 21 September 2006 — Commission of the European Communities v Grand-Duchy of Luxembourg
(Case C-100/06) [1]
Parties
Applicant: Commission of the European Communities (represented by: B. Schima and J. Hottiaux, Agents)
Defendant: Grand-Duchy of Luxembourg (represented by: S. Schreiner, Agent)
Re:
Failure of a Member State to fulfil obligations — Failure to adopt, within the prescribed period, the measures necessary to comply with Commission Directive 2003/66/EC of 3 July 2003 amending Directive 94/2/EC implementing Council Directive 92/75/EEC with regard to energy labelling of household electric refrigerators, freezers and their combinations (OJ 2003 L 170, p. 10)
Operative part of the judgment
The Court:
1. Declares that, by failing to adopt, within the prescribed period, the laws, regulations and administrative provisions necessary to comply with Commission Directive 2003/66/EC of 3 July 2003 amending Directive 94/2/EC implementing Council Directive 92/75/EEC with regard to energy labelling of household electric refrigerators, freezers and their combinations, the Grand-Duchy of Luxembourg has failed to fulfil its obligations under that directive.
2. Orders the Grand-Duchy of Luxembourg to pay the costs.
[1] OJ C 86, 08.04.2006
--------------------------------------------------
