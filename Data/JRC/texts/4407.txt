Council Decision
of 25 April 2005
on the signing and provisional application of a Protocol to the Euro-Mediterranean Agreement establishing an association between the European Communities and their Member States, of the one part, and the Republic of Tunisia, of the other part, to take account of the accession of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic to the European Union
(2005/721/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 310 in conjunction with the second phrase of first subparagraph of Article 300(2) thereof,
Having regard to the 2003 Act of Accession, and in particular Article 6(2) thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) On 10 February 2004, the Council authorised the Commission, on behalf of the Community and its Member States, to open negotiations with the Republic of Tunisia with a view to adjusting the Euro-Mediterranean Agreement establishing an association between the European Communities and their Member States, of the one part, and the Republic of Tunisia, of the other part [1], to take account of the accession of the new Member States to the European Union.
(2) These negotiations have been concluded to the satisfaction of the Commission.
(3) The Protocol negotiated with the Republic of Tunisia provides, in Article 12(2), for the provisional application of the Protocol before its entry into force.
(4) Subject to its conclusion, the Protocol should be signed on behalf of the Community and applied provisionally,
HAS DECIDED AS FOLLOWS:
Article 1
The President of the Council is hereby authorised to designate the person(s) empowered to sign, on behalf of the Community and its Member States, the Protocol to the Euro-Mediterranean Agreement establishing an association between the European Communities and their Member States, of the one part, and the Republic of Tunisia, of the other part, to take account of the accession of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia, and the Slovak Republic to the European Union.
The text of the Protocol is attached to this Decision.
Article 2
The Protocol referred to in Article 1 shall be applied provisionally from 1 May 2004, subject to its conclusion.
Done at Luxembourg, 25 April 2005.
For the Council
The President
J. Asselborn
[1] OJ L 97, 30.3.1998, p. 2.
--------------------------------------------------
