Amendments to the Rules of Procedure of the Court of Justice of the European Communities
THE COURT,
Having regard to the Treaty establishing the European Community, and in particular the sixth paragraph of Article 223 thereof,
Having regard to the Treaty establishing the European Atomic Energy Community, and in particular the sixth paragraph of Article 139 thereof,
Whereas, in the light of experience, it is necessary to amend certain provisions of the Rules of Procedure, in particular those determining the composition of formations of the Court, and to clarify the drafting of various provisions;
With the Council's approval given on 3 October 2005,
HAS ADOPTED THE FOLLOWING AMENDMENTS TO ITS RULES OF PROCEDURE:
Article 1
The Rules of Procedure of the Court of Justice of the European Communities of 19 June 1991 (OJ L 176, 4.7.1991, p. 7, with corrigendum in OJ L 383, 29.12.1992, p. 117), as amended on 21 February 1995 (OJ L 44, 28.2.1995, p. 61), 11 March 1997 (OJ L 103, 19.4.1997, p. 1, with corrigendum in OJ L 351, 23.12.1997, p. 72), 16 May 2000 (OJ L 122, 24.5.2000, p. 43), 28 November 2000 (OJ L 322, 19.12.2000, p. 1), 3 April 2001 (OJ L 119, 27.4.2001, p. 1), 17 September 2002 (OJ L 272, 10.10.2002, p. 24, with corrigendum in OJ L 281, 19.10.2002, p. 24), 8 April 2003 (OJ L 147, 14.6.2003, p. 17), 19 April 2004 (OJ L 132, 29.4.2004, p. 2), 20 April 2004 (OJ L 127, 29.4.2004, p. 107) and 12 July 2005 (OJ L 203, 4.8.2005, p. 19) are hereby amended as follows:
1. in Article 9, paragraph 2 shall be replaced by the following:
"2. As soon as an application initiating proceedings has been lodged, the President of the Court shall designate the Judge-Rapporteur.";
2. in the second and third paragraphs of Article 11, "are all prevented from attending at the same time" shall be replaced by "are all absent or prevented from attending at the same time";
3. in Article 11b, paragraph 1 shall be replaced by the following:
"1. For each case the Grand Chamber shall be composed of the President of the Court, the Presidents of the Chambers of five Judges, the Judge-Rapporteur and the number of Judges necessary to reach 13. The last-mentioned Judges shall be designated from the list referred to in paragraph 2, following the order laid down therein. The starting-point on that list, in every case assigned to the Grand Chamber, shall be the name of the Judge immediately following the last Judge designated from the list for the preceding case assigned to that formation of the Court.";
4. the following paragraph shall be added to Article 11b:
"3. In cases which are assigned to the Grand Chamber between the beginning of a year in which there is a partial replacement of Judges and the moment when that replacement has taken place, two substitute Judges shall also sit. Those substitute Judges shall be the two Judges appearing in the list referred to in paragraph 2 immediately after the last Judge designated for the composition of the Grand Chamber in the case.
The substitute Judges shall replace, in the order of the list referred to in paragraph 2, such Judges as are unable to take part in the decision on the case.";
5. in Article 11c, paragraph 1 shall be replaced by the following:
"1. The Chambers of five Judges and three Judges shall, for each case, be composed of the President of the Chamber, the Judge-Rapporteur and the number of Judges required to attain the number of five and three Judges respectively. Those last-mentioned Judges shall be designated from the lists referred to in paragraph 2 and following the order laid down in them. The starting-point in those lists, for every case assigned to a Chamber, shall be the name of the Judge immediately following the last Judge designated from the list for the preceding case assigned to the Chamber concerned.";
6. the single paragraph of Article 11d shall become paragraph 1 and the following paragraph shall be added:
"2. Where a Chamber to which a case has been assigned refers the case back to the Court under Article 44(4), in order that it may be reassigned to a formation composed of a greater number of Judges, that formation shall include the members of the Chamber which has referred the case back.";
7. in Article 16(1), "initialled by the President," shall be deleted.;
8. in Article 35, paragraph 1 shall be replaced by the following:
"1. If the Court considers that the conduct of an adviser or lawyer towards the Court, a Judge, an Advocate General or the Registrar is incompatible with the dignity of the Court or with the requirements of the proper administration of justice, or that such adviser or lawyer is using his rights for purposes other than those for which they were granted, it shall inform the person concerned. If the Court informs the competent authorities to whom the person concerned is answerable, a copy of the letter sent to those authorities shall be forwarded to the person concerned.
On the same grounds, the Court may at any time, having heard the person concerned and the Advocate General, exclude the person concerned from the proceedings by order. That order shall have immediate effect.";
9. the following sentence shall be added to Article 37(6): "Article 81(2) shall not be applicable to this period of 10 days.";
10. in the first subparagraph of Article 44(5), "the Chamber referred to in Article 9(2) of these Rules" shall be replaced by "the Judge-Rapporteur";
11. the first subparagraph of Article 45(3) shall be deleted;
12. the first two paragraphs of Article 46 shall be deleted and paragraph 3 shall become the sole paragraph;
13. in Article 60, "the Chamber or" shall be deleted;
14. in Article 74(1), "the Chamber referred to in Article 9(2) of these Rules to which the case has been assigned" shall be replaced by "the formation of the Court to which the case has been referred" and "from which no appeal shall lie" shall be deleted;
15. Article 75 shall be replaced by the following:
"Article 75
1. Sums due from the cashier of the Court and from its debtors shall be paid in euro.
2. Where costs to be recovered have been incurred in a currency other than the euro or where the steps in respect of which payment is due were taken in a country of which the euro is not the currency, conversions of currency shall be made at the European Central Bank's official rates of exchange on the day of payment.";
16. Article 76 is amended as follows:
(a) paragraph 3 shall be replaced by the following:
"3. The President shall designate a Judge to act as Rapporteur. The Court, on the Judge-Rapporteur's proposal and after hearing the Advocate General, shall refer the application to a formation of the Court which shall decide whether legal aid should be granted in full or in part or whether it should be refused. That formation shall consider whether there is manifestly no cause of action.
The formation of the Court shall give its decision by way of order. Where the application for legal aid is refused in whole or in part, the order shall state the reasons for that refusal.";
(b) in paragraph 4, "Chamber" shall be replaced by "formation of the Court";
17. in Article 92(2) the word "consider" shall be replaced by the words, "after hearing the parties, decide" and the words "after hearing the parties" shall be deleted after the words "or declare".;
18. in Article 93(7), "on the basis of the Report for the Hearing communicated to him," shall be deleted.
Article 2
These amendments to the Rules of Procedure, which are authentic in the languages referred to in Article 29(1) of these Rules, shall be published in the Official Journal of the European Union and shall enter into force on the first day of the second month following their publication.
Done at Luxembourg, 18 October 2005.
--------------------------------------------------
