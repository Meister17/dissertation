Commission Regulation (EC) No 1896/2000
of 7 September 2000
on the first phase of the programme referred to in Article 16(2) of Directive 98/8/EC of the European Parliament and of the Council on biocidal products
(Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Directive 98/8/EC of the European Parliament and of the Council of 16 February 1998 concerning the placing of biocidal products on the market(1), and in particular Article 16(2) thereof,
Whereas:
(1) Pursuant to Directive 98/8/EC (hereinafter referred to as "the Directive"), a programme of work is to be initiated for the review of all active substances of biocidal products already on the market on 14 May 2000 (hereinafter referred to as "existing active substances").
(2) The first phase of the review programme is intended to enable the Commission to identify existing active substances of biocidal products and specify those which should be evaluated for a possible inclusion in Annex I, Annex IA or Annex IB to the Directive. Given the expected high number of existing active substances which are candidates for such inclusion, information is needed to set priorities for a further phase of the review programme, which is planned to be initiated in 2002.
(3) It is necessary to specify the relationship between producers, formulators, Member States and the Commission and the obligation on each of the parties for the implementation of the review programme.
(4) In order to establish an exhaustive list of existing active substances, an identification procedure should be laid down by which all producers are to submit information on existing active substances of biocidal products to the Commission. Formulators should also have the opportunity of identifying existing active substances.
(5) A notification procedure should be laid down by which producers and formulators have the right to inform the Commission of their interest in securing the possible inclusion of an existing active substance in Annex I, Annex IA or Annex IB to the Directive for one or more specific product types and of their commitment to submit all the requisite information for a proper evaluation of, and decision on, that active substance.
(6) The information submitted with the notification on active substances should be linked to one or more specific product types or subgroups of product types and should be the minimum necessary for priority setting.
(7) Member States should have the opportunity of indicating an interest in the inclusion in Annex I or Annex IA to the Directive of essential existing active substances that have not been notified by producers or formulators. Member States which have indicated such an interest should carry out all the duties of a notifier.
(8) Existing active substances notified in one or more product types should be allowed to remain on the market in accordance with Article 16(1) of the Directive for those notified product types until a date to be set in the decision on inclusion or non-inclusion of the active substance for that product type in Annex I or Annex IA to the Directive.
(9) For existing active substances not notified in specific product types, decisions should be adopted, in accordance with the procedure laid down in Article 28(3) of the Directive, stating that those substances cannot be included in Annex I or Annex IA to the Directive for those product types under the review programme. A reasonable phase-out period should be allowed for those existing active substances and for biocidal products containing them.
(10) For active substances not identified within the time limits laid down in this Regulation, as well as for biocidal products containing them, no further phase-out period should be allowed after the establishment of the list of existing active substances.
(11) In view of the time limit for the transitional period of 10 years and the time needed to compile complete dossiers, the identification of the first existing active substances to be evaluated should not be delayed until the general priorities are set. In the interests of successful implementation of the Directive, it is advisable to start the evaluation of existing active substances in product types for which experience is already available.
(12) Some existing active substances used in wood preservatives which are used in large volumes in the Community are known to pose potential risks for humans and the environment. The need to establish a harmonised market for wood preservatives was one of the main reasons for the adoption of the Directive. Having regard to national rules in some Member States, the necessary experience to evaluate wood preservatives is available. Experience regarding rodenticides is available in many Member States. The existing active substances in those two specific product types should, therefore, be included in the first list of existing active substances to be evaluated.
(13) The evaluation of the first active substances should also be used to gain experience on the risk assessment process and on the appropriateness of the data requirements in order to carry out an adequate risk assessment. Among other issues, it is necessary to ensure that the risk assessment is carried out in a cost-effective way. For this purpose, notifiers should be encouraged to provide information on the costs of compiling a complete dossier. This information, together with any appropriate recommendations, should be integrated in the report referred to in Article 18(5) of the Directive. However this should not prevent earlier modifications of data requirements or procedures.
(14) In order to avoid duplication of work, and in particular experiments involving vertebrate animals, specific provisions should be adopted to encourage producers to act collectively, in particular by submitting collective notifications and dossiers.
(15) The need to address the concern for the possible effects of existing active substances directly or indirectly entering the food chain will be considered when setting priorities for the further phase of the review programme.
(16) The measures provided for in this Regulation are in accordance with the opinion of the Standing Committee on Biocidal Products,
HAS ADOPTED THIS REGULATION:
Article 1
Scope
This Regulation lays down provisions for the establishment and implementation of the first phase of the programme of work for the systematic examination of all active substances already on the market on 14 May 2000 as active substances of biocidal products (hereinafter referred to as "the review programme") referred to in Article 16(2) of the Directive
Article 2
Definitions
For the purposes of this Regulation, the definitions in Article 2 of the Directive shall apply.
The following definitions shall also apply:
(a) "existing active substance" means an active substance on the market before 14 May 2000 as an active substance of a biocidal product for purposes other than those referred to in Article 2(2)(c) and (d) of the Directive;
(b) "producer" means:
- in the case of an active substance produced within the Community and placed on the market, the manufacturer of that active substance or a person established within the Community designated by the manufacturer as his sole representative for the purposes of this Regulation,
- in the case of an active substance produced outside the Community, the person established within the Community and designated by the manufacturer of that active substance as his sole representative for the purposes of this Regulation or, where no such person has been so designated, the importer into the Community of that active substance,
- in the case of a biocidal product produced outside the Community, the person established within the Community and designated by the manufacturer of that biocidal product as his sole representative for the purposes of this Regulation or, where no such person has been so designated, the importer into the Community of that biocidal product;
(c) "formulator" means, in the case of a biocidal product manufactured within the Community, the manufacturer of that biocidal product, or a person established within the Community designated by the manufacturer as his sole representative for the purposes of this Regulation;
(d) "identification" of an active substance means the submission to the Commission of the information referred to in Annex I. The person or the association of producers/formulators submitting the identification is an "identifier";
(e) "notification" of an active substance means the submission to the Commission of the information referred to in Annex II. The submitter of the notification is a "notifier".
The notifier may be:
- the producer or the formulator who has made a notification in accordance with Article 4 or Article 8,
- the association of producer(s) and/or formulator(s) established within the Community and designated by the producers and/or formulators for the purpose of complying with this Regulation, which has made a joint notification in accordance with Article 4 or Article 8.
Article 3
Identification of existing active substances
1. Each producer of an existing active substance placed on the market for use in biocidal products shall identify that active substance by submitting to the Commission the information on the active substance referred to in Annex I to be received not later than 18 months after this Regulation enters into force. This requirement shall not apply to existing active substances which are no longer on the market after 13 May 2000 either as such or in biocidal products.
Any formulator may identify an existing active substance in accordance with the first subparagraph, except for the requirements in points 5 and 6 of Annex I.
In submitting the information, the identifier shall use the special software package made available free of charge by the Commission.
Member States may require identifiers established in their territory to submit simultaneously to their competent authorities the same information as is submitted to the Commission.
2. Any producer or formulator who notifies an existing active substance in accordance with Article 4 shall not make a separate identification of that active substance in accordance with paragraph 1 of this Article.
3. A working document containing a non-exhaustive list with examples of possible existing active substances shall be made available by the Commission on its Internet website and shall also be made available as a hard-copy at the competent authorities of the Member States not later than 30 days after this Regulation enters into force.
Article 4
Notification of existing active substances
1. Producers, formulators and associations wishing to apply for the inclusion in Annex I or Annex IA to the Directive of an existing active substance in one or more product types shall notify that active substance to the Commission by submitting the information referred to in Annex II to this Regulation to be received not later than 18 months after this Regulation enters into force.
Whenever a formulator or a producer is aware of another notifier's possible intention to notify the same active substance, they shall undertake all reasonable efforts to present a common notification, in whole or in part, in order to minimise animal testing.
In submitting the notification, the notifier shall use the special software package (IUCLID) made available free of charge by the Commission.
Member States may require notifiers established in their territory to submit simultaneously to their competent authorities the same information as is submitted to the Commission.
2. The Commission in cooperation with Member States shall check that a notification submitted to it complies with the requirements of paragraph 1.
If the notification complies with those requirements, the Commission shall accept it.
If the notification does not comply with those requirements, the Commission shall grant the notifier a period of 30 days in which to complete or correct his notification. If, after the expiry of that 30-day period, the notification fails to comply with those requirements, the Commission shall inform the notifier that his notification has been rejected and give the reasons therefor.
Where a notification has been rejected, the notifier may within 30 days address a request to the Commission for a Decision to be taken in accordance with the procedure laid down in Article 28(3) of the Directive.
3. If a notification is accepted by the Commission, the notifier shall provide to the Commission all data and information necessary for the evaluation of the existing active substance with a view to its possible inclusion in Annex I or Annex IA to the Directive during the second phase of the review programme.
4. A notifier may only withdraw his notification if an objective change in the assumptions for the notification justifies the withdrawal. The notifier shall inform the Commission without delay, mentioning the reasons. If the Commission accepts the withdrawal, the notifier shall no longer be subject to the requirement set out in paragraph 3.
Where a withdrawal has been rejected, the notifier may within 30 days address a request to the Commission for a Decision to be taken in accordance with the procedure laid down in Article 28(3) of the Directive.
A Decision shall be taken in accordance with the procedure laid down in Article 28(3) of the Directive not to include in Annex I or Annex IA to the Directive an active substance for which the notification has been withdrawn and for which no other notification has been accepted with the consequences referred to in Article 6(3) of this Regulation.
In the event of non-compliance with paragraph 3 of this Article in relation to any product type, a Decision may be taken in accordance with the procedure laid down in Article 28(3) of the Directive with the consequences referred to in Article 6(3) of this Regulation for the placing of the active substance on the market in other product types according to Annex V to the Directive.
Article 5
Indication of interest by Member States
1. The Commission shall communicate to the Member States a list of all active substances which have been identified as existing active substances under Article 3 or Article 4, indicating those in respect of which a notification has been submitted in accordance with Article 4(1) and accepted by the Commission.
2. Within three months after receiving the list referred to in paragraph 1 and in accordance with the procedure laid down in Article 3(1), Member States may identify additional existing active substances.
3. Within three months after receiving the list referred to in paragraph 1, Member States, alone or collectively, may indicate their interest in the possible inclusion in Annex I or Annex IA to the Directive of an existing active substance in product types where it has uses which the Member State considers essential in particular for the protection of human health or the environment, and for which no notification has been accepted by the Commission.
By indicating such an interest a Member State shall be deemed to carry out the duties of an applicant as set out in the Directive, and the active substance shall without notification in accordance with Article 4(1) of this Regulation, be included in the list referred to in Article 6(1)(b).
Article 6
Consequences of identification and notification
1. In accordance with the procedure laid down in Article 28(3) of the Directive, a Regulation shall be adopted containing:
(a) an exhaustive list of existing active substances placed on the market for use in biocidal products, for which substances at least one identification complies with the requirements of Article 3(1) or Article 5(2) or equivalent information submitted in a notification according to Article 4(1); and
(b) an exhaustive list of existing active substances to be reviewed during the second phase of the review programme containing those existing active substances
(i) for which the Commission has accepted at least one notification in accordance with Article 4(1) or Article 8(1); or
(ii) that Member States have indicated in accordance with Article 5(3); or
(iii) for which, following indications in accordance with Article 8(3) or (4), Member States, alone or collectively, have agreed to provide the necessary data to carry out evaluations for possible inclusion in Annex IB to the Directive in the second phase of the review programme.
The Commission shall make the lists publicly available by electronic means.
2. Without prejudice to Article 16(1), (2) or (3) of the Directive, all producers of an active substance included in the list referred to in paragraph 1(b) and all formulators of biocidal products containing that active substance may start or continue to place on the market the active substance, as such or in biocidal products, in the product type or types for which the Commission has accepted at least one notification.
3. In accordance with the procedure laid down in Article 28(3) of the Directive, Decisions addressed to Member States shall be adopted stating that the following active substances shall not be included in Annex I, Annex IA or Annex IB to the Directive under the review programme and that these active substances, solely or in biocidal products, shall no longer be placed on the market for biocidal purposes:
(a) active substances not included in the list referred to in paragraph 1(b);
(b) active substances included in the list referred to in paragraph 1(b) in product types for which the Commission has not accepted at least one notification.
However if the active substance is included in the list of existing active substances referred to in paragraph 1(a), a reasonable phase-out period shall be allowed of not more than three years from the date on which the Decision referred to in the first subparagraph takes effect.
4. The following applications for inclusion of existing active substances in Annex I, Annex IA or Annex IB to the Directive shall be treated as if the substance was not placed on the market for use in biocidal products before 14 May 2000:
(a) an application for inclusion of an active substance not included in the list referred to in paragraph 1(b);
(b) an application for inclusion of an active substance in product types other than those for which it is included in the list referred to in paragraph 1(b).
Article 7
Submission of dossiers for the inclusion in Annex I, Annex IA or Annex IB to the Directive of active substances in certain product types
1. Existing active substances of biocidal product types 8 (wood preservatives) and 14 (rodenticides) according to Annex V to the Directive that are included in the list referred to in Article 6(1)(b) of this Regulation shall be included in the first list of existing active substances to be reviewed. Notifiers whose notifications have been accepted by the Commission in accordance with Article 4(2) of this Regulation shall submit complete dossiers according to Article 11(1)(a) of the Directive concerning the inclusion in Annex I, Annex IA or Annex IB to the Directive of active substances in those product types. The dossiers referred to in Article 11(1)(a)(ii) of the Directive shall cover representative uses in particular with respect to exposure of humans and the environment to the active substance.
2. Member States may require that advance payment of a charge according to Article 25 of the Directive covering the costs of the work resulting from the requirement set out in Article 11(1)(b) of the Directive concerning the acceptance of the dossier shall be part of a complete dossier.
3. In order to minimise both animal testing and the costs of compilation of complete dossiers, the applicant may ask Member States' advice on the acceptability of justifications submitted by the applicant to waive certain studies.
The advice shall not predetermine the verification in accordance with Article 11(1)(b) of the Directive on whether the dossier can be regarded as complete.
To provide information on the costs related to the application of the Directive's requirements, the notifier may submit to the competent authority together with the complete dossier a breakdown of the costs of the respective actions and studies carried out. The competent authority shall submit this information to the Commission when submitting the evaluation report referred to in Article 11(2) of the Directive.
The Commission shall include in the report referred to in Article 18(5) of the Directive information on the costs related to compilation of complete dossiers, together with any appropriate recommendations concerning modifications of data requirements in order to ensure cost effectiveness.
4. Whenever several notifiers have notified the same active substance, they shall undertake all reasonable efforts to present a collective dossier. Where a collective dossier is not presented by all the notifiers concerned with that active substance, the dossier shall detail the efforts made to secure their participation and the reasons for their non-participation.
5. The dossiers shall be received by the competent authority of the designated Member State not later than 42 months after this Regulation enters into force. The designation shall be carried out by the Commission when the list referred to in Article 6(1)(b) of this Regulation is established.
6. Within a reasonable period after the receipt of the dossier and in any event not later than 45 months after this Regulation enters into force, Member States shall complete the steps referred to in Article 11(1)(b) of the Directive concerning the acceptance of the dossiers for which they have been designated.
If a complete dossier as referred to in paragraph 1 is not received within the time limit referred to in pararaph 5, the designated Member State shall inform the Commission, giving the reasons pleaded by the notifier.
In exceptional cases and on the basis of the report of the designated Member State, a new time limit may be established in accordance with the procedure laid down in Article 28(3) of the Directive, where the notifier demonstrates that the delay was due to force majeure.
If, on the expiry of the time limit, a dossier on an active substance is incomplete and no other dossier concerning that active substance in the same product type has been accepted, a Decision shall be adopted, in accordance with the procedure laid down in Article 28(3) of the Directive, not to include that active substance in Annex I or Annex IA to the Directive.
Article 8
Basic substances
1. Any person wishing to apply for the inclusion of an existing active substance in Annex IB to the Directive in one or more specific product types shall notify the substance to the Commission in accordance with the procedure laid down in Article 4(1) and (2).
2. If a notification is accepted by the Commission, the notifier shall provide to the Commission all data and information necessary for the evaluation of the existing active substance with a view to its possible inclusion in Annex IB to the Directive during the second phase of the review programme.
A notifier may only withdraw his notification, if an objective change in the assumptions for the notification justifies the withdrawal. The notifier shall inform the Commission thereof without delay mentioning the reasons. If the Commission accepts the withdrawal, the notifier shall no longer be subject to the requirement set out in the first subparagraph.
Where a withdrawal has been rejected, the notifier may within 30 days address a request to the Commission for a Decision to be taken in accordance with the procedure laid down in Article 28(3) of the Directive.
3. Member States may indicate existing active substances as potential basic substances for inclusion in Annex IB to the Directive. To this end they shall submit to the Commission not later than six months after the entry into force of this Regulation their indication together with the information referred to in Annex I to this Regulation.
4. The Commission shall communicate to the Member States a list of potential basic substances which have been indicated as existing basic substances. Within three months after receiving that list, Member States may indicate further existing basic substances in accordance with the requirements of paragraph 3.
Article 9
Entry into force
This Regulation shall enter into force on the 20th day following that of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 7 September 2000.
For the Commission
Margot Wallström
Member of the Commission
(1) OJ L 123, 24.4.1998, p. 1.
ANNEX I
INFORMATION REQUIRED FOR IDENTIFICATION IN ACCORDANCE WITH ARTICLE 3 OR INDICATION IN ACCORDANCE WITH ARTICLE 8(3) OR (4)
1. Identity of the identifier(1) etc.:
1.1. Name and address etc. of the identifier and his status as producer, formulator or Member State.
1.2. If the identifier is a producer who is not the manufacturer of the active substance: the authorisation by the manufacturer of the identifier to act as his sole representative within the Community.
1.3. If the identifier is not the manufacturer of the active substance: the name and address of that manufacturer.
2. Identity of the substance:
2.1. Common name proposed or accepted by ISO and synonyms.
2.2. Chemical name (IUPAC nomenclature).
2.3. Manufacturer's development code number(s) (if available).
2.4. CAS and EC numbers.
2.5. Molecular and structural formula (including full details of any isomeric composition), molecular mass.
2.6. Specification of purity of the active substance in g/kg or g/l, as appropriate.
3. Proof that the substance was already on the market as an active substance of a biocidal product before 14 May 2000. Apart from EC number also evidence that the substance was used as an active substance in at least one biocidal product, e.g. in form of an invoice ant the composition of a product and/or a label.
4. Member States where the active substance is placed on the market. For basic substances the Member States where the basic substance is used.
5. If the identifier is a producer: average annual quantities of the active substance placed on the market for the years 1998-2000 per product type according to Annex V to the Directive. If relevant, quantities shall be specified by subgroup as listed below. If statistics are not available, estimation shall be sufficient.
6. By way of derogation from paragraph 5 for potential basic substances: annual quantities placed on the market in total and used as biocidal products per product type according to Annex V to the Directive. If relevant the quantities shall be specified by subgroups as listed below.
Product type according to Annex V to the Directive and subgroups relevant for the priority setting
Product type 1: Human hygiene biocidal products
Product type 2: Private area and public health area disinfectants and other biocidal products
2.01. Disinfectants for medical equipment, biocidal products for accommodation for man or in industrial areas
2.02. Biocidal products to be used in swimming pools etc.
2.03. Biocidal products to be used in air-conditioning system
2.04. Biocidal products for chemical toilets, treatment of waste water or treatment of hospital waste.
2.05. Other biocidal products within product type 2
Product type 3: Veterinary hygiene biocidal products
Product type 4: Food and feed area disinfectants
Product type 5: Drinking water disinfectants
Product type 6: In-can preservatives
6.01. Preservatives for detergents
6.02. Other in-can preservatives
Product type 7: Film preservatives
Product type 8: Wood preservatives
8.01. Pre-treatment in industrial premises (pressure and vacuum impregnation and dipping)
8.02. Other wood preservatives
Product type 9: Fibre, leather, rubber and polymerised materials preservatives
9.01. Preservatives for textiles and leather
9.02. Preservatives for paper
9.03. Preservatives for rubber and polymerised materials and other biocidal products covered by product type 09
Product type 10: Masonry preservatives
Product type 11: Preservatives for liquid-cooling and processing systems
11.01. Preservatives used in once-through systems
11.02. Preservatives used in recirculating systems
Product type 12: Slimicides
12.01. Slimicides for paperpulp
12.02. Slimicides for mineral oil extraction
12.03. Other slimicides
Product type 13: Metalworking-fluid preservatives
Product type 14: Rodenticides
Product type 15: Avicides
Product type 16: Molluscicides
Product type 17: Piscicides
Product type 18: Insecticides, acaricides and products to control other arthropods
18.01. Used by professionals
18.02. Used by non-professionals
Product type 19: Repellents and attractants
19.01. Repellents applied directly on human or animal skin
19.02. Attractants and repellents not applied directly on human or animal skin
Product type 20: Preservatives for food or feedstocks
Product type 21: Anti-fouling products
Product type 22: Embalming and taxidermist fluids
Product type 23: Control of other vertebrate
(1) In the case of identification in accordance with Article 5 or indication in accordance with Article 8: the identity of the Member State.
ANNEX II
INFORMATION REQUIRED FOR NOTIFICATION IN ACCORDANCE WITH ARTICLE 4 OR ARTICLE 8(1)
1. Product type(s) according to Annex V to the Directive for which the notification is submitted.
2. Summary studies, information, relevant endpoints and information on the date of completion of ongoing or committed studies as specified in Table 1 of Annex II. Only information which has to be included in the complete dossier for the use and the nature of the biocidal product shall be submitted.
3. Proof that the substance was already on the market as an active substance of a biocidal product before 14 May 2000. Apart from EC number also evidence that the substance was used as an active substance in at least one biocidal product, e.g. in form of an invoice and the composition of a product and/or a label.
4. Member States where the active substance is placed on the market. For substances applied for as basic substances the Member States where the basic substance is used.
5. If the identifier is a producer. The information shall also include information on quantities in product types not notified:
(a) average annual quantities of the active substance placed on the market for the years 1998-2000 per product type according to Annex V to the Directive. If relevant, quantities shall be specified by subgroup as listed in Annex I. If statistics are not available, estimation shall be sufficient;
(b) an estimation of the notifier's market share percentage for the years 1998-2000 in the EU of:
(i) the total use of the active substance for that product type, if relevant specified by subgroups; and of
(ii) the total use of the substance within the EU.
6. By way of derogation from paragraph 5 for potential basic substances: annual quantities placed on the market in total and used as biocidal products per product type according to Annex V to the Directive and subgroups as listed in Annex I.
7. Declaration confirming that the information provided is honest and correct and that the notifier commits to submit to the competent authorities of the designated reporting Member State the complete dossiers according to Article 11(1)(a) of the Directive within the time period laid down by the Commission. He confirms that the information submitted the notification is based on studies which are available to the notifier and which will be submitted to the rapporteur Member State as part of the dossier referred to in Article 11(1).
Table£>TABLE>
