Commission Regulation (EC) No 727/2006
of 12 May 2006
opening and providing for the administration of an import tariff quota for frozen beef intended for processing ( 1 July 2006 to 30 June 2007)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1254/1999 of 17 May 1999 on the common organisation of the market in beef and veal [1], and in particular Article 32(1) thereof,
Whereas:
(1) The WTO schedule CXL requires the Community to open an annual import tariff quota of 50700 tonnes of frozen beef intended for processing. Moreover, as a result of the negotiations which led to the Agreement in the form of an Exchange of Letters between the European Community and Australia pursuant to Article XXIV:6 and Article XXVIII of the General Agreement on Tariffs and Trade (GATT) 1994 relating to the modification of concessions in the schedules of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic in the course of their accession to the European Union [2], approved by Council Decision (EC) No 2006/106/EC [3], the Community undertook to incorporate in its schedule for all Member States an increase of 4003 tonnes of that import tariff quota.
(2) Implementing rules should be laid down for the quota year 2006/2007, starting on 1 July 2006. However, in view of the forthcoming entry into force of the Treaty of Accession of Bulgaria and Romania to the European Union, without prejudice to Article 39 of that Treaty, and in order to enable operators of those countries to benefit from this quota as of the date of their accession, the quota period should be divided into two sub-periods and the quantity available under this quota should be staggered over these periods, taking into account the traditional trade patterns between the Community and the supplier countries within this quota.
(3) The import of frozen beef under the tariff quota is subject to customs import duties and to the conditions laid down under serial number 13 of Annex 7 to Part three of Annex I to Council Regulation (EEC) No 2658/87 of 23 July 1987 on the tariff and statistical nomenclature and on the Common Customs Tariff [4].
(4) So as to avoid speculation, access to the quota should be allowed only to active processors carrying out processing in a processing establishment approved in accordance with Article 4 of Regulation (EC) No 853/2004 of the European Parliament and of the Council of 29 April 2004 laying down specific hygiene rules for food of animal origin [5], or, subject to the entry into force of the Treaty of Accession of Bulgaria and Romania to the European Union on 1 January 2007, to processing establishments in those countries who have been approved for export into the Community of processed meat products in accordance with Article 12 of Regulation (EC) No 854/2004 of the European Parliament and of the Council of 29 April 2004 laying down specific rules for the organisation of official controls on products of animal origin intended for human consumption [6].
(5) Imports into the Community under the tariff quota are subject to presentation of an import licence in accordance with the first subparagraph of Article 29(1) of Regulation (EC) No 1254/1999. It should be possible to issue licences following allocations of import rights on the basis of applications from eligible processors. The provisions of Commission Regulation (EC) No 1291/2000 of 9 June 2000 laying down common detailed rules for the application of the system of import and export licences and advance fixing certificates for agricultural products [7] and of Commission Regulation (EC) No 1445/95 of 26 June 1995 on rules of application for import and export licences in the beef and veal sector and repealing Regulation (EEC) No 2377/80 [8] should apply to import licences issued under this Regulation.
(6) In order to prevent speculation, import licences should be issued to processors solely for the quantities for which they have been allocated import rights. Moreover, for the same reason, security should be lodged together with the application for import rights. The application for import licences corresponding to the allocated rights should be a primary requirement within the meaning of Commission Regulation (EEC) No 2220/85 of 22 July 1985 laying down common detailed rules for the application of the system of securities for agricultural products [9].
(7) The application of the tariff quota requires strict surveillance of imports and effective checks as to their use and destination. The processing should therefore be authorised only in the establishment referred to in the import licence.
(8) A security should be lodged in order to ensure that the imported meat is used according to the tariff quota specifications. The amount of that security should be fixed taking into account the difference between the customs duties applicable within and outside the quota.
(9) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Beef and Veal,
HAS ADOPTED THIS REGULATION:
Article 1
An import tariff quota of 54703 tonnes, bone-in equivalent of frozen beef falling within CN code 02022030, 02023010, 02023050, 02023090 or 02062991 and intended for processing in the Community (hereinafter referred to as "the quota") is hereby opened for the period from 1 July 2006 to 30 June 2007 subject to the conditions laid down in this Regulation.
Article 2
1. For the purposes of this Regulation, an A-product shall mean a processed product falling within CN code 160210, 16025031, 16025039 or 16025080, not containing meat other than that of animals of the bovine species, with a collagen/protein ratio of no more than 0,45 and containing by weight at least 20 % of lean meat excluding offal and fat with meat and jelly accounting for at least 85 % of the total net weight.
The collagen content shall be considered to be the hydroxyproline content multiplied by the factor 8. The hydroxyproline content shall be determined according to ISO method 3496-1994.
The lean bovine meat content excluding fat shall be determined in accordance with the procedure laid down in the Annex to Commission Regulation (EEC) No 2429/86 [10].
Offal includes the following: heads and cuts thereof (including ears), feet, tails, hearts, udders, livers, kidneys, sweetbreads (thymus glands and pancreas), brains, lungs, throats, thick skirts, spleens, tongues, caul, spinal cords, edible skin, reproductive organs (i.e. uteri, ovaries and testes), thyroid glands, pituitary glands.
The product shall be subjected to a heat treatment sufficient to ensure the coagulation of meat proteins in the whole of the product which may not show any traces of a pinkish liquid on the cut surface when the product is cut along a line passing through its thickest part.
2. For the purposes of this Regulation, a B-product shall mean a processed product containing beef, other than:
(a) the products specified in Article 1(1)(a) of Regulation (EC) No 1254/1999, or
(b) the products referred to under paragraph 1.
However, a processed product falling within CN code 02102090 which has been dried or smoked so that the colour and consistency of the fresh meat has totally disappeared and with a water/protein ratio not exceeding 3.2 shall be considered to be a B-product.
Article 3
1. The overall quantity referred to in Article 1 shall be divided into two quantities and staggered as follows:
(a) 43000 tonnes of frozen beef intended for the manufacture of A-products, of which:
(i) 30000 tonnes for the period from 1 July 2006 to 31 December 2006;
(ii) 13000 tonnes for the period from 1 January 2007 to 30 June 2007;
(b) 11703 tonnes of frozen beef intended for the manufacture of B-products, of which:
(i) 8200 tonnes for the period from 1 July 2006 to 31 December 2006;
(ii) 3503 tonnes for the period from 1 January 2007 to 30 June 2007.
2. The quota shall bear the following order numbers:
- 09.4057 for the quantities referred to in paragraph 1(a),
- 09.4058 for the quantities referred to in paragraph 1(b).
3. The customs import duties to apply on frozen beef under the quota are set out in Annex I.
Article 4
1. The application for import rights under the quota may only be lodged by, or on behalf of processing establishments approved under Article 4 of Regulation (EC) No 853/2004 and which have been active in production of processed products containing beef at least once since 1 July 2005.
Subject to the entry into force of the Treaty of Accession of Bulgaria and Romania to the European Union on 1 January 2007, processing establishments in those countries approved under Article 12 of Regulation (EC) No 854/2004 to export to the Community and which have been active in production of processed products containing beef at least once since 1 July 2005 may apply for import rights in relation to the quantities available for the second sub-period of this quota as referred to in Article 3(1)(a)(ii) and (b)(ii).
For each quantity referred to in Article 3(1) only one application for import rights which shall not exceed 10 % of each quantity available may be accepted in respect of each approved processing establishment.
Applications for import rights may be presented only in the Member State in which the processor is registered for VAT purposes.
2. A security of EUR 6 per 100 kg shall be lodged together with the application for import rights.
3. The evidence of compliance with the conditions laid down in the first and second subparagraphs of paragraph 1 shall be submitted together with the application for import rights.
The competent national authority shall decide what is acceptable documentary evidence of compliance with those conditions.
However, operators that provided that evidence together with their application for import rights in relation to the quantities available for the first sub-period of this quota as referred to in Article 3(1)(a)(i) and (b)(i), are exempted from the obligation to provide such evidence in case of applications for import rights in relation to the quantities available for the second sub-period of this quota as referred to in Article 3(1)(a)(ii) and (b)(ii).
Article 5
1. Each application for import rights for production of A-products or B-products shall be expressed in bone-in equivalence.
For the purpose of this paragraph 100 kilograms of bone-in beef equals 77 kilograms of boneless beef.
2. Applications for import rights for production of either A-products or B-products shall reach the competent authority:
(a) no later than the second Friday following the date of publication of this Regulation in the Official Journal of the European Union by 13.00 Brussels time at the latest, for applications related to the first sub-period as referred to in Article 3(1)(a)(i) and (b)(i);
(b) no later than 13:00, Brussels time, on 12 January 2007 for applications related to the second sub-period as referred to in Article 3(1)(a)(ii) and (b)(ii).
3. Member States shall forward to the Commission no later than the second Friday following the end of the respective periods for the submission of applications referred to in paragraph 2 a list of applicants and quantities applied for under each of the two categories together with the approval numbers of the processing establishments concerned.
All communications, including nil returns, shall be sent by fax or e-mail using the forms set out in Annexes II and III.
4. The Commission shall decide as soon as possible to what extent applications are accepted, where necessary as a percentage of the quantity applied for.
Article 6
1. Any import of frozen beef for which import rights have been allocated pursuant to Article 5(4) shall be subject to presentation of an import licence.
2. As to the security referred to in Article 4(2) the application for import licences corresponding to the allocated import rights shall be a primary requirement within the meaning of Article 20(2) of Regulation (EEC) No 2220/85.
Where in application of Article 5(4) the Commission fixes a reduction coefficient the security lodged shall be released in respect of the import rights applied for which exceed the allocated import rights.
3. Import rights allocated to processors entitle them to import licences for quantities equivalent to the rights allocated.
Licence applications may be lodged solely:
(a) in the Member State in which the application for import rights has been lodged;
(b) by processors or on behalf of processors to whom import rights have been allocated.
4. A security shall be lodged with the competent authority at the time of import ensuring that the processor having been allocated import rights processes the entire quantity of meat imported into the required finished products in his establishment specified in the licence application, within three months of the day of import.
The amounts of the security are fixed in Annex IV.
Article 7
Regulations (EC) No 1291/2000 and (EC) No 1445/95 shall apply, except otherwise provided in this Regulation.
Article 8
1. The licence application and the licence shall contain the following information:
(a) in box 8, the country of origin;
(b) in box 16, one of the eligible CN codes referred to in Article 1;
(c) in box 20, at least one of the entries listed in Annex V.
2. Import licences shall be valid for 120 days from the actual date of issue within the meaning of Article 23(1) of Regulation (EC) No 1291/2000. However, no licence shall be valid after 30 June 2007.
3. In application of Article 50(1) of Regulation (EC) No 1291/2000, the full Common Customs Tariff duty applicable on the date of release for free circulation shall be collected in respect of all quantities imported in excess of those shown on the import licence.
Article 9
Member States shall set up a system of physical and documentary checks to ensure that, within three months of the date of import, all meat is processed in the processing establishment and into the category of product specified on the import licence concerned.
The system shall include physical checks of quantity and quality at the start of the processing, during the processing and after the processing operation is completed. To this end, processors shall at any time be able to demonstrate the identity and use of the imported meat through appropriate production records.
Technical verification of the production method by the competent authority may, to the extent necessary, make allowance for drip losses and trimmings.
In order to verify the quality of the finished product and establish its conformity with the processor’s formula for the composition of the product, Member States shall take representative samples and analyse those products. The costs of such operations shall be borne by the processor concerned.
Article 10
1. The security referred to in Article 6(4) shall be released in proportion to the quantity for which, within seven months of the day of import, proof has been furnished to the satisfaction of the competent authority that all or part of the imported meat has been processed into the relevant products within three months following the day of import in the designated establishment.
However, if processing took place after the three-month time limit referred to in the first subparagraph, the security shall be released minus a 15 % reduction plus 2 % of the remaining amount for each day by which the time limit has been exceeded.
If proof of processing is established within the seven-month time limit referred to in the first subparagraph and produced within 18 months following those seven months the amount forfeited, less 15 % of the security amount, shall be repaid.
2. The amount not released of the security referred to in Article 6(4) shall be forfeited and retained as a customs duty.
Article 11
This Regulation shall enter into force on the day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 12 May 2006.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 160, 26.6.1999, p. 21. Regulation as last amended by Regulation (EC) No 1913/2005 (OJ L 307, 25.11.2005, p. 2).
[2] OJ L 47, 17.2.2006, p. 54.
[3] OJ L 47, 17.2.2006, p. 52.
[4] OJ L 256, 7.9.1987, p. 1. Regulation as last amended by Regulation (EC) No 426/2006 (OJ L 79, 16.3.2006, p. 1).
[5] OJ L 139, 30.4.2004, p. 55 (corrected version in OJ L 226, 25.6.2004, p. 22). Regulation as last amended by Commission Regulation (EC) No 2076/2005 (OJ L 338, 22.12.2005, p. 83).
[6] OJ L 139, 30.4.2004, p. 206 (corrected version in OJ L 226, 25.6.2004, p. 83). Regulation as last amended by Commission Regulation (EC) No 2076/2005 (OJ L 338, 22.12.2005, p. 83).
[7] OJ L 152, 24.6.2000, p. 1. Regulation a last amended by Regulation (EC) No 1856/2005 (OJ L 297, 15.11.2005, p. 7).
[8] OJ L 143, 27.6.1995, p. 35. Regulation a last amended by Regulation (EC) No 1118/2004 (OJ L 217, 17.6.2004, p. 10).
[9] OJ L 205, 3.8.1985, p. 5. Regulation as last amended by Regulation (EC) No 673/2004 (OJ L 105, 14.4.2004, p. 17).
[10] OJ L 210, 1.8.1986, p. 39.
--------------------------------------------------
ANNEX I
IMPORT DUTIES
Product (CN code) | For manufacture of A products | For manufacture of B products |
02022030 | 20 % | 20 % + 994,5 EUR/1000 kg/net |
02023010 | 20 % | 20 % + 1554,3 EUR/1000 kg/net |
02023050 | 20 % | 20 % + 1554,3 EUR/1000 kg/net |
02023090 | 20 % | 20 % + 2138,4 EUR/1000 kg/net |
02062991 | 20 % | 20 % + 2138,4 EUR/1000 kg/net |
--------------------------------------------------
ANNEX II
EC Fax: (32 2) 292 17 34
E-mail: AGRI-IMP-BOVINE@cec.eu.int
Application of Article 5(1) and (2) of Regulation (EC) No 727/2006
Number of applicant (1) | Applicant (name and address) | Approval number | Quantity (in tones bone-in) |
| | | |
Total | |
+++++ TIFF +++++
--------------------------------------------------
ANNEX III
EC Fax: (32 2) 292 17 34
E-mail: AGRI-IMP-BOVINE@cec.eu.int
Application of Article 5(1) and (2) of Regulation No (EC) 727/2006
Number of applicant (1) | Applicant (name and address) | Approval number | Quantity (in tones bone-in) |
| | | |
Total | |
+++++ TIFF +++++
--------------------------------------------------
ANNEX IV
AMOUNTS OF SECURITY [1]
(in EUR/1000 kg net) |
Product (CN code) | For manufacture of A products | For manufacture of B products |
02022030 | 1414 | 420 |
02023010 | 2211 | 657 |
02023050 | 2211 | 657 |
02023090 | 3041 | 903 |
02062991 | 3041 | 903 |
[1] The exchange rate to be applied shall be the exchange rate on the day preceding the lodging of the security.
--------------------------------------------------
ANNEX V
Entries referred to in Article 8(1)(c)
in Spanish : Certificado válido en … (Estado miembro expedidor)/carne destinada a la transformación … (productos A) (productos B) (táchese lo que no proceda) en … (designación exacta y número de registro del establecimiento en el que vaya a procederse a la transformación)/Reglamento (CE) no 727/2006
in Czech : Licence platná v … (vydávající členský stát)/Maso určené ke zpracování … (výrobky A) (výrobky B) (nehodící se škrtněte) v (přesné určení a číslo schválení zpracovatelského zařízení, v němž se má zpracování uskutečnit)/nařízení (ES) č. 727/2006
in Danish : Licens gyldig i … (udstedende medlemsstat)/Kød bestemt til forarbejdning til (A-produkter) (B-produkter) (det ikke gældende overstreges) i … (nøjagtig betegnelse for den virksomhed, hvor forarbejdningen sker)/forordning (EF) nr. 727/2006
in German : In … (ausstellender Mitgliedstaat) gültige Lizenz/Fleisch für die Verarbeitung zu (A-Erzeugnissen) (B-Erzeugnissen) (Unzutreffendes bitte streichen) in … (genaue Bezeichnung des Betriebs, in dem die Verarbeitung erfolgen soll)/Verordnung (EG) Nr. 727/2006
in Estonian : Litsents on kehtiv … (välja andev liikmesriik)/Liha töötlemiseks … (A toode) (B toode) (kustuta mittevajalik) … (ettevõtte asukoht ja loanumber, kus toimub töötlemine/määrus (EÜ) nr 727/2006
in Greek : Η άδεια ισχύει … (κράτος μέλος έκδοσης)/Κρέας που προορίζεται για μεταποίηση … (προϊόντα Α) (προϊόντα Β) (διαγράφεται η περιττή ένδειξη) … (ακριβής περιγραφή και αριθμός έγκρισης της εγκατάστασης όπου πρόκειται να πραγματοποιηθεί η μεταποίηση)/Κανονισμός (ΕΚ) αριθ. 727/2006
in English : Licence valid in … (issuing Member State)/Meat intended for processing … (A-products) (B-products) (delete as appropriate) at … (exact designation and approval No of the establishment where the processing is to take place)/Regulation (EC) No 727/2006
in French : Certificat valable … (État membre émetteur)/viande destinée à la transformation de … (produits A) (produits B) (rayer la mention inutile) dans … (désignation exacte et numéro d’agrément de l’établissement dans lequel la transformation doit avoir lieu)/règlement (CE) no 727/2006
in Italian : Titolo valido in … (Stato membro di rilascio)/Carni destinate alla trasformazione … (prodotti A) (prodotti B) (depennare la voce inutile) presso … (esatta designazione e numero di riconoscimento dello stabilimento nel quale è prevista la trasformazione)/Regolamento (CE) n. 727/2006
in Latvian : Atļauja derīga … (dalībvalsts, kas izsniedz ievešanas atļauju)/pārstrādei paredzēta gaļa … (A produktu) (B produktu) ražošanai (nevajadzīgo nosvītrot) … (precīzs tā uzņēmuma apzīmējums un apstiprinājuma numurs, kurā notiks pārstrāde)/Regula (EK) Nr. 727/2006
in Lithuanian : Licencija galioja … (išdavusioji valstybė narė)/Mėsa skirta perdirbimui … (produktai A) (produktai B) (ištrinti nereikalingą) … (tikslus įmonės, kurioje bus perdirbama, pavadinimas ir registracijos Nr.)/Reglamentas (EB) Nr. 727/2006
in Hungarian : Az engedély … (kibocsátó tagállam) területén érvényes./Feldolgozásra szánt hús … (A-termék) (B-termék) (a nem kívánt törlendő) … (pontos rendeltetési hely és a feldolgozást végző létesítmény engedélyezési száma)/727/2006/EK rendelet
in Dutch : Certificaat geldig in … (lidstaat van afgifte)/Vlees bestemd voor verwerking tot (A-producten) (B-producten) (doorhalen wat niet van toepassing is) in … (nauwkeurige aanduiding en toelatingsnummer van het bedrijf waar de verwerking zal plaatsvinden)/Verordening (EG) nr. 727/2006
in Polish : Pozwolenie ważne w … (wystawiające państwo członkowskie)/Mięso przeznaczone do przetworzenia … (produkty A) (produkty B) (niepotrzebne skreślić) w … (dokładne miejsce przeznaczenia i nr zatwierdzenia zakładu, w którym ma mieć miejsce przetwarzanie)/rozporządzenie (WE) nr 727/2006
in Portuguese : Certificado válido em … (Estado-Membro emissor)/carne destinada à transformação … (produtos A) (produtos B) (riscar o que não interessa) em … (designação exacta e número de aprovação do estabelecimento em que a transformação será efectuada)/Regulamento (CE) n.o 727/2006
in Slovak : Licencia platná v … (vydávajúci členský štát)/Mäso určené na spracovanie … (výrobky A) (výrobky B) (nehodiace sa prečiarknite) v … (presné určenie a číslo schválenia zariadenia, v ktorom spracovanie prebehne)/nariadenie (ES) č. 727/2006
in Slovenian : Dovoljenje velja v … (država članica, ki ga je izdala)/Meso namenjeno predelavi … (proizvodi A) (proizvodi B) (črtaj neustrezno) v … (točno namembno območje in št. odobritve obrata, kjer bo predelava potekala)/Uredba (ES) št. 727/2006
in Finnish : Todistus on voimassa … (myöntäjäjäsenvaltio)/Liha on tarkoitettu (A-luokan tuotteet) (B-luokan tuotteet) (tarpeeton poistettava) jalostukseen … :ssa (tarkka ilmoitus laitoksesta, jossa jalostus suoritetaan, hyväksyntänumero mukaan lukien)/Asetus (EY) N:o 727/2006
in Swedish : Licensen är giltig i … (utfärdande medlemsstat)/Kött avsett för bearbetning … (A-produkter) (B-produkter) (stryk det som inte gäller) vid … (exakt angivelse av och godkännandenummer för anläggningen där bearbetningen skall ske)/Förordning (EG) nr 727/2006
--------------------------------------------------
