REGULATION (EEC) No 3539/73 OF THE COUNCIL of 18 December 1973 of the rate of import charges collected on small non-commercial consignments of agricultural products and goods coming under Regulation (EEC) No 1059/69
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Articles 43 and 235 thereof;
Having regard to the Act (1) annexed to the Treaty on the Accession of new Member States to the European Economic Community and the European Atomic Energy Community, signed on 22 January 1972, and in particular Article 62(1) thereof;
Having regard to the proposal from the Commission;
Having regard to the Opinion of the European Parliament;
Whereas Part II B of the preliminary provisions of the Common Customs Tariff in the Annex to Council Regulation (EEC) No 950/68(2) of 28 June 1968, as last amended by Regulation (EEC) No 2260/73 (3), provides that, where the interested party does not demand the application of the normal rate, customs duty is to be charged at the flat rate of 10 % ad valorem on goods sent in small consignments to private individuals, provided that such importations are not of a commercial nature;
Whereas, it is useful and possible without endangering the aims of the common agricultural policy, to collect, following the same methods, all the other import charges on agricultural products as well as on certain goods coming under the system of exchages laid down in Council Regulation (EEC) No 1059/69 (4) of 28 May 1969 laying down the trade arrangements applicable to certain goods resulting from the processing of agricultural products, as last amended by Regulation (EEC) No 1491/73 (5);
Whereas the same transaction can give rise to the collection of several different charges ; whereas in order to allow a system of flat rate taxation to achieve its aim, it is necessary to apply a single flat rate charge for the whole of these charges,
HAS ADOPTED THIS REGULATION:
Article 1
1. In the case of small non-commercial consignments referred to in Section II B (2) of the preliminary provisions of the Common Customs Tariff, appearing in Annex to Regulation (EEC) No 950/68, the flat rate charge referred to in paragraph 1 of the said Section II B shall be collected in place of the import charges as laid down within the framework of the common agricultural policy and of those provided for in relation to goods in Regulation (EEC) No 1059/69.
2. When the same transaction is liable to the collection of several different charges, the flat rate charge referred to in paragraph 1 is collected for the whole of the charges.
3. However, paragraph 1 shall not apply when the addressee requests, prior to the imposition on such goods and agricultural products of the said flat rate charges, that they be subjected to the appropriate import charges.
Article 2
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 18 December 1973.
For the Council
The President
J. CHRISTENSEN (1)OJ No L 73, 27.3.1972, p. 5. (2)OJ No L 172, 22.7.1968, p. 1. (3)OJ No L 233, 21.8.1973, p. 10. (4)OJ No L 141, 12.6.1969, p. 1. (5)OJ No L 151, 7.6.1973, p. 1.
