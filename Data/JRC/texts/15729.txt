Commission Regulation (EC) No 1601/2006
of 26 October 2006
on the issue of rice import licences for applications lodged in the first 10 working days of October 2006 under Regulation (EC) No 327/98
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1785/2003 of 29 September 2003 on the common organisation of the market in rice [1],
Having regard to Commission Regulation (EC) No 327/98 of 10 February 1998 opening and providing for the administration of certain tariff quotas for imports of rice and broken rice [2], and in particular Article 5(2) thereof,
Whereas:
Examination of the quantities for which import licence applications for rice have been submitted for the October 2006 tranche shows that licences should be issued for the quantities applied for multiplied, where appropriate, by a reduction percentage, and the final percentage take-up of each quota in 2006 should be communicated,
HAS ADOPTED THIS REGULATION:
Article 1
1. Import licence applications for the tariff quotas for rice opened by Regulation (EC) No 327/98, submitted in the first 10 working days of October 2006 and notified to the Commission, shall be subject to percentage reduction coefficients as set out in the Annex to this Regulation.
2. The final use of the quotas concerned for 2006, in percentage terms, is set out in the annex hereto.
Article 2
This Regulation shall enter into force on 27 October 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 26 October 2006.
For the Commission
Jean-Luc Demarty
Director-General for Agriculture and Rural Development
[1] OJ L 270, 21.10.2003, p. 96. Regulation as amended by Regulation (EC) No 247/2006 (OJ L 42, 14.2.2006, p. 1).
[2] OJ L 37, 11.2.1998, p. 5. Regulation as last amended by Commission Regulation (EC) No 965/2006 (OJ L 176, 30.6.2006, p. 12).
--------------------------------------------------
ANNEX
Reduction percentages to be applied to quantities applied for under the tranche for October 2006 and quota use for 2006:
(a) Quota of wholly milled or semi-milled rice falling within CN code 100630 provided for in Article 1(1)(a) of Regulation (EC) No 327/98
Origin | Serial No | Reduction percentage for the October 2006 tranche | Final use of the quota for 2006 in percentage terms |
United States of America | 09.4127 | | 87,70 |
Thailand | 09.4128 | | 98,79 |
Australia | 09.4129 | | 98,53 |
Other origins | 09.4130 | | 100 |
All countries | 09.4138 | 92,33339 | 100 |
(b) Quota of husked rice falling within CN code 100620 provided for in Article 1(1)(b):
Origin | Serial No | Reduction percentage for the October 2006 tranche | Final use of the quota for 2006 in percentage terms |
All countries | 09.4148 | — | 100 |
(c) Quota of broken rice falling within CN code 10064000 provided for in Article 1(1)(c):
Origin | Serial No | Final use of the quota for 2006 in percentage terms |
Thailand | 09.4149 | 71,01 |
Australia | 09.4150 | 0,80 |
Guyana | 09.4152 | 0 |
United States of America | 09.4153 | 5,44 |
Other origins | 09.4154 | 100 |
(d) Quota of wholly milled or semi-milled rice falling within CN code 100630 provided for in Article 1(1)(d):
Origin | Serial No | Final use of the quota for 2006 in percentage terms |
Thailand | 09.4112 | 100 |
United States of America | 09.4116 | 100 |
India | 09.4117 | 100 |
Pakistan | 09.4118 | 100 |
Other origins | 09.4119 | 100 |
All countries | 09.4166 | 100 |
(e) Quota of broken rice falling within CN code 10064000 provided for in Article 1(1)(e) of Regulation (EC) No 327/98:
Origin | Serial No | Reduction percentage for the October 2006 tranche | Final use of the quota for 2006 in percentage terms |
All countries | 09.4168 | — | 100 |
--------------------------------------------------
