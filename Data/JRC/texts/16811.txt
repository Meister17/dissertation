Commission Regulation (EC) No 1549/2004
of 30 August 2004
derogating from Council Regulation (EC) No 1785/2003 as regards the arrangements for importing rice and laying down separate transitional rules for imports of basmati rice
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1785/2003 of 29 September 2003 on the common organisation of the market in rice [1], and in particular Articles 10(2) and 11(4) thereof,
Having regard to Council Decision 2004/619/EC of 11 August 2004 modifying the Community import regime with respect to rice in the Community [2], and in particular Article 2 thereof,
Having regard to Council Decision 2004/617/EC of 11 August 2004 on the conclusion of an Agreement in the form of an Exchange of Letters between the European Community and India pursuant to Article XXVIII of GATT 1994 relating to the modification of concessions with respect to rice provided for in Schedule CXL annexed to the GATT 1994 [3], and in particular Article 2 thereof,
Having regard to Council Decision 2004/618/EC of 11 August 2004 on the conclusion of an Agreement in the form of an Exchange of Letters between the European Community and Pakistan pursuant to Article XXVIII of GATT 1994 for the modification of concessions with respect to rice provided for in EC Schedule CXL annexed to the GATT 1994 [4], and in particular Article 2 thereof,
Whereas:
(1) Decision 2004/619/EC modifies the import regime for husked rice and milled rice in the Community. Decisions 2004/617/EC and 2004/618/EC lay down conditions for importing basmati rice. This change in regime makes it necessary to amend Regulation (EC) No 1785/2003. In order to enable those Decisions to be applied on 1 September 2004, as provided for in the Agreements approved by those Decisions, it is necessary to derogate from Regulation (EC) No 1785/2003 for a transitional period expiring on the date of the entry into force of the amendment to that Regulation, and no later than 30 June 2005.
(2) Decisions 2004/617/EC and 2004/618/EC also provide for a transitional import regime for basmati rice to be set in place until the entry into force of a definitive import regime for this type of rice. Specific transitional rules should be laid down.
(3) In order to be eligible for zero import duty, basmati rice must belong to a variety specified in the Agreements. In order to ascertain that basmati rice imported at a zero rate of duty meets those characteristics, it should be covered by an authenticity certificate drawn up by the competent authorities.
(4) In order to prevent fraud, provision should be made for measures to check the variety of basmati rice declared.
(5) The transitional import regime for basmati rice provides for a procedure for consulting the exporting country in the event of disturbance on the market and possibly applying the full rate of duty if a satisfactory solution has not been found at the end of the consultations. It would be appropriate to define from what point the market may be considered to be disturbed.
(6) Following the introduction of this transitional regime, Commission Regulation (EC) No 1503/96 of 29 July 1996 on the detailed rules for the application of Council Regulation (EC) No 3072/95 with regard to import duties on rice should be repealed [5].
(7) The import duties for husked rice and milled rice provided for in the Article 11(2) of Regulation (EC) No 1785/2003 serve as a basis for calculating the reduced import duties provided for in Commission Regulation (EC) No 638/2003 of 9 April 2003 laying down detailed rules for applying Council Regulation (EC) No 2286/2002 and Council Decision 2001/822/EC as regards the arrangements applicable to imports of rice originating in the African, Caribbean and Pacific States (ACP States) and the overseas countries and territories (OCT) [6], by Commission Regulation (EEC) No 862/91 of 8 April 1991 laying down detailed rules applying Council Regulation (EEC) No 3491/90 to imports of rice originating in Bangladesh [7] and by Council Regulation (EC) No 2184/96 of 28 October 1996 concerning imports into the Community of rice originating in and coming from Egypt [8]. The amounts of import duty laid down by this Regulation should temporarily serve as a basis for calculating reduced duties for the products concerned.
(8) The Management Committee for Cereals has not delivered an opinion within the time limit set by its chairman,
HAS ADOPTED THIS REGULATION:
Article 1
1. By way of derogation from Article 11(2) of Regulation (EC) No 1785/2003, the import duty on husked rice falling within CN code 100620 shall be EUR 65 per tonne and the import duty on milled rice falling within CN code 100630 shall be EUR 175 per tonne.
2. By way of derogation from Article 11(2) of Regulation (EC) No 1785/2003, the varieties of basmati rice falling within CN code 10062017 and CN code 10062098, as specified in Annex I, shall be eligible for zero import duty.
Where the first subparagraph is applicable, the measures provided for in Articles 2 to 8 shall apply.
Article 2
1. Import licence applications for basmati rice shall contain the following details:
(a) in box 8, an indication of the country of origin and the word "yes" marked with a cross;
(b) in box 20, one of the entries listed in Annex II hereto.
2. Import licence applications for basmati rice shall be accompanied by:
(a) proof that the applicant is a natural or legal person who has, for at least 12 months, carried on a commercial activity in the rice sector and is registered in the Member State where the application is made;
(b) a product authenticity certificate issued by a competent body in the exporting country, as listed in Annex III hereto.
Article 3
1. The authenticity certificate shall be drawn up on a form based on the specimen given in Annex IV hereto.
The form shall measure approximately 210 × 297 millimetres. The original shall be drawn up on such paper as shall show up any tampering by mechanical or chemical means.
The forms shall be printed and completed in English.
The original and the copies thereof shall be either typewritten or handwritten. In the latter case, they shall be completed in ink and in block capitals.
Each authenticity certificate shall contain a serial number in the top right-hand box. The copies shall bear the same number as the original.
2. The body issuing the import licence shall keep the original of the authenticity certificate and give the applicant a copy.
The authenticity certificate shall be valid for 90 days from the date of issue.
It shall be valid only if the boxes are duly completed and it is signed.
Article 4
1. Import licences for basmati rice shall contain the following details:
(a) in box 8, indication of the country of origin and the word "yes" marked with a cross;
(b) in box 20, one of the entries listed in Annex V hereto.
2. By way of derogation from Article 9 of Commission Regulation (EEC) No 1291/2000 [9], rights accruing under import licences for basmati rice shall not be transferable.
3. By way of derogation from Article 12 of Commission Regulation (EC) No 1342/2003 [10], the security for import licences for basmati rice shall be EUR 70 per tonne.
Article 5
Member States shall send the Commission the following information by fax or e-mail:
(a) no later than two business days following a refusal, the quantities in respect of which applications for import licences for basmati rice have been refused, with an indication of the date of refusal and the grounds, the CN code, the country of origin, the issuing body and the number of the authenticity certificate, as well as the holder's name and address;
(b) no later than two business days following their issue, the quantities in respect of which applications for import licences for basmati rice have been issued, with an indication of the date, the CN code, the country of origin, the issuing body and the number of the authenticity certificate, as well as the holder's name and address;
(c) in the event of the cancellation of a licence, no later than two business days after cancellation, the quantities in respect of which licences have been cancelled and the names and addresses of the holders of the cancelled licences;
(d) on the last business day of each month following the month of release for free circulation, the quantities actually released for free circulation, with an indication of the CN code, the country of origin, the issuing body and the number of the authenticity certificate.
The information referred to in the first subparagraph shall be sent separately from that relating to the other applications for import licences for rice.
Article 6
In the context of random checks or checks targeted at operations entailing a risk of fraud, Member States shall take representative samples of imported basmati rice. The samples shall be sent to the competent body in the country of origin, as listed in Annex VI, for a DNA-based variety test.
The Member State may also carry out a variety test on the same sample in a Community laboratory. If the results of one of those tests show that the product analysed does not correspond to the variety indicated on the authenticity certificate, the import duty provided for in Article 1(1) shall apply.
Article 7
The rice market shall be considered to be disturbed when, inter alia, a substantial increase in basmati rice imports is noted for one quarter of the year relative to the previous quarter and no satisfactory explanation exists.
Article 8
The Commission shall keep Annexes III and VI up to date.
Article 9
Regulation (EC) No 1503/96 is hereby repealed.
Import licences for basmati rice applied for prior to 1 September 2004 under this Regulation shall remain valid and products imported under such licences shall be eligible for the import duty provided for in Article 1(2) of this Regulation.
Article 10
On a transitional basis, the import duties referred to in Article 1(1) of this Regulation shall serve as a basis for calculating the reduced import duty referred to in Article 1(2) and (3) of Regulation (EEC) No 862/91, Article 1 of Regulation (EEC) No 2184/96 and Article 6 of Regulation (EEC) No 638/2003.
Article 11
This Regulation shall enter into force on the day following that of its publication in the Official Journal of the European Union.
It shall apply from 1 September 2004.
It shall apply until the date of entry into force of the Regulation amending Article 11(2) of Regulation (EC) No 1785/2003, but not beyond 30 June 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 30 August 2004.
For the Commission
Franz Fischler
Member of the Commission
[1] OJ L 270, 21.10.2003, p. 96.
[2] OJ L 279, 28.8.2004, p. 29.
[3] OJ L 279, 28.8.2004, p. 17.
[4] OJ L 279, 28.8.2004, p. 25.
[5] OJ L 189, 30.7.1996, p. 71. Regulation last amended by Regulation (EC) No 2294/2003 (OJ L 340, 24.12.2003, p. 12).
[6] OJ L 93, 10.4.2003, p. 3.
[7] OJ L 88, 9.4.1991, p. 7. Regulation last amended by Regulation (EC) No 1482/98 (OJ L 195, 11.7.1998, p. 14).
[8] OJ L 292, 15.11.1996, p. 1.
[9] OJ L 152, 24.6.2000, p. 1.
[10] OJ L 189, 29.7.2003, p. 12.
--------------------------------------------------
ANNEX I
Varieties referred to in Article 1(2)
Basmati 217
Basmati 370
Basmati 386
Kernel (basmati)
Pusa basmati
Ranbir basmati
Super basmati
Taraori basmati (HBC-19)
Type-3 (Dehradun)
--------------------------------------------------
ANNEX II
Entries referred to in Article 2(1)(b)
in Spanish: Arroz Basmati del código NC 10062017 o 10062098 importado con derecho cero en aplicación del Reglamento (CE) no 1549/2004, acompañado del certificado de autenticidad no … expedido por [nombre de la autoridad competente]
in Czech: rýže Basmati kódu KN 10062017 nebo 10062098, která se dováží za nulové clo na základě nařízení (ES) č. 1549/2004, a ke které se připojí osvědčení o pravosti č. … vydané [název příslušného subjektu]
in Danish: Basmati-ris henhørende under KN-kode 10062017 eller 10062098 importeres med nultold i henhold til forordning (EF) nr. 1549/2004, ledsaget af ægthedscertifikat nr. … udstedt af [den kompetente myndigheds navn]
in German: Basmati-Reis des KN-Codes 10062017 oder 10062098, eingeführt zum Zollsatz Null gemäß der Verordnung (EG) Nr. 1549/2004 und begleitet von einer Kopie des Echtheitszeugnisses Nr. …, ausgestellt durch [Name der zuständigen Behörde]
in Estonian: basmati riis CN-koodiga 10062017 või 10062098, mis on imporditud tollimaksu nullmääraga vastavalt määrusele (EÜ) nr 1549/2004 ning millega on kaasas [pädeva asutuse nimi] välja antud autentsussertifikaat nr …
in Greek: Ρύζι μπασμάτι του κωδικού 10062017 ή 10062098 εισαγόμενο με μηδενικό δασμό κατ’ εφαρμογή του κανονισμού (ΕΚ) αριθ. 1549/2004, συνοδευόμενο με το πιστοποιητικό γνησιότητας αριθ. … που εκδόθηκε από [ονομασία της αρμόδιας αρχής]
in English: basmati rice falling within code of CN 10062017 or 10062098 and imported at a zero rate of duty under Regulation (EC) No 1549/2004, accompanied by authenticity certificate No … drawn up by [name of the competent authority]
in French: Riz Basmati du code NC 10062017 ou 10062098 importé à droit nul en application du règlement (CE) no 1549/2004, accompagné d’une copie du certificat d’authenticité no … établi par [nom de l’autorité compétente]
in Italian: Riso Basmati di cui al codice NC 10062017 o 10062098 importato a dazio zero ai sensi del regolamento (CE) n. 1549/2004, corredato di una copia del certificato di autenticità n. … rilasciato da [nome dell’autorità competente]
in Latvian: Basmati rīsi ar KN kodu 10062017 vai 10062098, ko importē bez ievedmuitas nodokļa saskaņā ar Regulu (EK) Nr. 1549/2004, kuriem pievienota autentiskuma apliecības Nr. … kopija, ko izsniegusi [kompetentās iestādes nosaukums]
in Lithuanian: Basmati ryžiai klasifikuojami KN kodu 10062017 arba 10062098, įvežti pagal nulinį muito mokestį pagal Reglamentas (EB) Nr. 1549/2004, prie kurio pridėtas autentiškumo sertifikatas Nr. …, išduotas [kompetentingos institucijos pavadinimas], kopija.
in Hungarian: az 10062017 vagy az 10062098 KN-kód alá sorolt, a 1549/2004/EK rendelet alkalmazásában nulla vámtétel mellett behozott basmati rizs, a [illetékes hatóság neve] által kiállított, … számú eredetiségigazolással együtt
in Dutch: Basmati-rijst van GN-code 10062017 of 10062098, ingevoerd met nulrecht overeenkomstig Verordening (EG) nr. 1549/2004, vergezeld van het echtheidscertificaat nr. …, opgesteld door [naam van de bevoegde instantie]
in Polish: Ryż Basmati objęty kodem CN 10062017 lub 10062098, do którego przywiezienia zastosowano zerową stawkę celną zgodnie z rozporządzeniem (WE) nr 1549/2004, z załączonym do niego certyfikatem autentyczności nr … sporządzonym przez [nazwa właściwego organu]
in Portuguese: Arroz Basmati do código NC 10062017 ou 10062098 importado com direito nulo em aplicação do Regulamento (CE) n.o 1549/2004, acompanhado do certificado de autenticidade n.o … estabelecido por [nome da autoridade competente]
in Slovak: ryža Basmati s kódom KN 10062017 alebo 10062098 dovážaná s nulovou sadzbou cla v súlade s nariadením (ES) č. 1549/2004, sprevádzaná osvedčením o pravosti č. … vystavenom [názov príslušného orgánu]
in Slovenian: Riž basmati s kodo KN 10062017 ali 10062098, uvožen po stopnji nič ob uporabi Uredbe (ES) št. 1549/2004, s priloženo kopijo potrdila o pristnosti št. …, ki ga je izdal [naziv pristojnega organa]
in Finnish: Asetuksen (EY) N:o 1549/2004 mukaisesti tullivapaasti tuotu CN-koodiin 10062017 tai 10062098 kuuluva Basmati-riisi, jonka mukana on ….:n [toimivaltaisen viranomaisen nimi] myöntämän aitoustodistuksen N:o … jäljennös
in Swedish: Basmatiris med KN-nummer 10062017 eller 10062098 som importeras tullfritt i enlighet med förordning (EG) nr 1549/2004, åtföljt av äkthetsintyg nr … som utfärdats av [den behöriga myndighetens namn]
--------------------------------------------------
ANNEX III
Bodies authorised to issue the authenticity certificates referred to in Article 2(2)(b)
INDIA | Export Inspection Council (Ministry of Commerce, Government of India)Directorate of Marketing and Inspection (Ministry of Agriculture and Rural Development) |
PAKISTAN | Trading Corporation of Pakistan (Pvt) Ltd |
--------------------------------------------------
ANNEX IV
Specimen authenticity certificate referred to in Article 3(1)
MODEL B
1. Exporter (Name and full address)
CERTIFICATE OF AUTHENTICITY B BASMATI RICE for export to the European Community No (1)ORIGINAL issued by (Name and full address of issuing body)
2. Consignee (Name and full address)
3. Country and place of cultivation
4. fob value in US dollars
5. No and date of invoice
6. Marks and numbers Number and kind of packages Description of goods (2)
7. Gross weight (kg)
8. Net weight (kg)
9. DECLARATION BY EXPORTER The undersigned declares that the information shown above is correct. Place and date:Signature:
10. CERTIFICATION BY THE ISSUING BODY It is hereby certified that the rice described above is BASMATI RICE and that the information shown in this certificate is correct. Place and date: Signature: Stamp:
11. CERTIFICATION BY COMPETENT CUSTOMS OFFICE OF COUNTRY OF EXPORT Customs formalities for export to the European Economic Community of the rice described above have been completed. Type, No and date of export document: Name and country of customs office: Signature: Stamp:
12. FOR COMPETENT AUTHORITIES IN THE COMMUNITY
(1) The number of the certificate of authenticity shall be a number of a continuous series given by the country delivering the certificate
(2) The operator shall specify for marks and numbers the reference and number of the batch, for number and kind of packages: the number and weight of packages, for the description of goods: the information on the rice, the CN code as well as the variety, which shall be one on the list of Annex I of Regulation (EC) No 1549/2004. The description of goods should correspond to the information included in the invoice, whose number and date is specified in Box 5.
(*) This certificate is issued in conformity with the national legislation.
--------------------------------------------------
ANNEX V
Entries referred to in Article 4(1)(b)
in Spanish: Arroz Basmati del código NC 10062017 o 10062098 importado con derecho cero en aplicación del Reglamento (CE) no 1549/2004, acompañado del certificado de autenticidad no … expedido por [nombre de la autoridad competente]
in Czech: rýže Basmati kódu KN 10062017 nebo 10062098, která se dováží za nulové clo na základě nařízení (ES) č. 1549/2004, a ke které se připojí osvědčení o pravosti č. … vydané [název příslušného subjektu]
in Danish: Basmati-ris henhørende under KN-kode 10062017 eller 10062098 importeres med nultold i henhold til forordning (EF) nr. 1549/2004, ledsaget af ægthedscertifikat nr. … udstedt af [den kompetente myndigheds navn]
in German: Basmati-Reis des KN-Codes 10062017 oder 10062098, eingeführt zum Zollsatz Null gemäß der Verordnung (EG) Nr. 1549/2004 und begleitet von einer Kopie des Echtheitszeugnisses Nr. …, ausgestellt durch [Name der zuständigen Behörde]
in Estonian: basmati riis CN-koodiga 10062017 või 10062098, mis on imporditud tollimaksu nullmääraga vastavalt määrusele (EÜ) nr 1549/2004 ning millega on kaasas [pädeva asutuse nimi] välja antud autentsussertifikaat nr …
in Greek: Ρύζι μπασμάτι του κωδικού 10062017 ή 10062098 εισαγόμενο με μηδενικό δασμό με εφαρμογή του κανονισμού (ΕΚ) αριθ. 1549/2004, συνοδευόμενο με αντίγραφο του πιστοποιητικού γνησιότητας αριθ. … που εκδόθηκε από [ονομασία της αρμόδιας αρχής]
in English: basmati rice falling within code of CN 10062017 or 10062098 and imported at a zero rate of duty under Regulation (EC) No 1549/2004, accompanied by authenticity certificate No … drawn up by [name of the competent authority]
in French: Riz Basmati du code NC 10062017 ou 10062098 importé à droit nul en application du règlement (CE) no 1549/2004, accompagné d’une copie du certificat d’authenticité no … établi par [nom de l’autorité compétente]
in Italian: Riso Basmati di cui al codice NC 10062017 o 10062098 importato a dazio zero ai sensi del regolamento (CE) n. 1549/2004, corredato di una copia del certificato di autenticità n. … rilasciato da [nome dell’autorità competente]
in Latvian: Basmati rīsi ar KN kodu 10062017 vai 10062098, ko importē bez ievedmuitas nodokļa saskaņā ar Regulu (EK) Nr. 1549/2004, kuriem pievienota autentiskuma apliecības Nr. … kopija, ko izsniegusi [kompetentās iestādes nosaukums]
in Lithuanian: Basmati ryžiai klasifikuojami KN kodu 10062017 arba 10062098, įvežti pagal nulinį muito mokestį pagal Reglamenta (EB) Nr. 1549/2004, prie kurio pridėta autentiškumo sertifikato Nr. …, išduoto [kompetentingos institucijos pavadinimas], kopija.
in Hungarian: az 10062017 vagy az 10062098 KN-kód alá sorolt, a 1549/2004/EK rendelet alkalmazásában nulla vámtétel mellett behozott basmati rizs, a [illetékes hatóság neve] által kiállított, … számú eredetiségigazolással együtt
in Dutch: Basmati-rijst van GN-code 10062017 of 10062098, ingevoerd met nulrecht overeenkomstig Verordening (EG) nr. 1549/2004, vergezeld van het echtheidscertificaat nr. …, opgesteld door [naam van de bevoegde instantie]
in Polish: Ryż Basmati objęty kodem CN 10062017 lub 10062098, do którego przywiezienia zastosowano zerową stawkę celną zgodnie z rozporządzeniem (WE) nr 1549/2004, z załączonym do niego certyfikatem autentyczności nr … sporządzonym przez [nazwa właściwego organu]
in Portuguese: Arroz Basmati do código NC 10062017 ou 10062098 importado com direito nulo em aplicação do Regulamento (CE) n.o 1549/2004, acompanhado de uma cópia do certificado de autenticidade n.o … estabelecido por [nome da autoridade competente]
in Slovak: ryža Basmati s kódom KN 10062017 alebo 10062098 dovážaná s nulovou sadzbou cla v súlade s nariadením (ES) č. 1549/2004, sprevádzaná osvedčením o pravosti č. … vystavenom [názov príslušného orgánu]
in Slovenian: Riž basmati s kodo KN 10062017 ali 10062098, uvožen po stopnji nič ob uporabi Uredbe (ES) št. 1549/2004, s priloženo kopijo potrdila o pristnosti št. …, ki ga je izdal [naziv pristojnega organa]
in Finnish: Asetuksen (EY) N:o 1549/2004 mukaisesti tullivapaasti tuotu CN-koodiin 10062017 tai 10062098 kuuluva Basmati-riisi, jonka mukana on ….:n [toimivaltaisen viranomaisen nimi] myöntämän aitoustodistuksen N:o … jäljennös
in Swedish: Basmatiris med KN-nummer 10062017 eller 10062098 som importeras tullfritt i enlighet med förordning (EG) nr 1549/2004, åtföljt av äkthetsintyg nr … som utfärdats av [den behöriga myndighetens namn]
--------------------------------------------------
ANNEX VI
Bodies authorised to conduct the variety tests referred to in Article 6
INDIA:
Export Inspection Council
Department of Commerce
Ministry of Commerce and Industry
3rd Floor
NDYMCA Cultural Central Bulk
1 Jaisingh Road
New Delhi 110 001
India
Tel.: +91-11/37 48 188/89, 336 55 40
Fax: +91-11/37 48 024
e-mail: eic@eicindia.org
PAKISTAN:
Trading Corporation of Pakistan Limited
4th and 5th Floor,
Finance & Trade Centre,
Shahrah-e-Faisal,
Karachi 75530,
Pakistan
Tel.: +92-21/290 28 47
Fax: +92-21/920 27 22 & 920 27 31
--------------------------------------------------
