Council Regulation (EC) No 1985/2003
of 10 November 2003
amending Regulation (EC) No 427/2003 on a transitional product-specific safeguard mechanism for imports originating in the People's Republic of China
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 133 thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) Regulation (EC) No 427/2003(1) lays down quantitative quotas for certain products originating in the People's Republic of China.
(2) Commission Regulation (EC) No 1351/2003(2) established administrative procedures for the first tranche of the 2004 quantitative quotas for certain products originating in the People's Republic of China.
(3) In view of the enlargement of the European Community on 1 May 2004 it is appropriate to increase the quotas in accordance with Article XXVIII of the GATT 1994 and the Understanding of the interpretation of this Article.
(4) It is therefore appropriate to amend Annex I to Regulation (EC) No 427/2003,
HAS ADOPTED THIS REGULATION:
Article 1
Annex I to Regulation (EC) No 427/2003 shall be replaced by the text in the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 10 November 2003.
For the Council
The President
A. Marzano
(1) OJ L 65, 8.3.2003, p. 1.
(2) OJ L 192, 31.7.2003, p. 8.
ANNEX
"ANNEX I
Phasing-out timetable of industrial (non-textile) quotas on imports originating in China
>TABLE>"
