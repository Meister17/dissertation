COMMISSION REGULATION (EC) No 1439/95 of 26 June 1995 laying down detailed rules for the application of Council Regulation (EEC) No 3013/89 as regards the import and export of products in the sheepmeat and goatmeat sector
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 3013/89 of 25 September 1989 on the common organization of the market in sheepmeat and goatmeat (1), as last amended by Regulation (EC) No 1265/95 (2), and in particular Articles 9 (2) and 12 (4) thereof,
Having regard to Council Regulation (EC) No 3290/94 of 22 December 1994 on the adjustments and transitional arrangements required in the agriculture sector in order to implement the agreements concluded during the Uruguay Round of multilateral trade negotiations (3), and in particular Article 3 thereof,
Having regard to Council Regulation (EC) No 3491/93 of 13 December 1993 on certain procedures for applying the Europe Agreement establishing an association between the European Communities and their Member States, of the one part, and the Republic of Hungary, of the other part (4), and in particular Article 1 thereof,
Having regard to Council Regulation (EC) No 3492/93 of 13 December 1993 on certain procedures for applying the Europe Agreement establishing an association between the European Communities and their Member States, of the one part, and the Republic of Poland, of the other part (5), and in particular Article 1 thereof,
Having regard to Council Regulation (EC) No 3296/94 of 19 December 1994 on certain procedures for applying the Europe Agreement establishing an association between the European Communities and their Member States, of the one part, and the Czech Republic, of the other part (6), and in particular Article 1 thereof,
Having regard to Council Regulation (EC) No 3297/94 of 19 December 1994 on certain procedures for applying the Europe Agreement establishing an association between the European Communities and their Member States, of the one part, and the Slovak Republic, of the other part (7), and in particular Article 1 thereof,
Having regard to Council Regulation (EC) No 3382/94 of 19 December 1994 on certain procedures for applying the Europe Agreement establishing an association between the European Communities and their Member States, of the one part, and Romania, of the other part (8), and in particular Article 1 thereof,
Having regard to Council Regulation (EC) No 3383/94 of 19 December 1994 on certain procedures for applying the Europe Agreement establishing an association between the European Communities and their Member States, of the one part, and the Republic of Bulgaria, of the other part (9), and in particular Article 1 thereof,
Whereas under the Agreement on Agriculture concluded in the framework of the Uruguay Round of multilateral trade negotiations (10), the Community has undertaken to replace the variable import levies by fixed customs duties as from 1 July 1995; whereas the Agreement on Agriculture also provides for the replacement of the present special arrangements with third countries on imports of products in the sheep and goat sector by a system of tariff quotas; whereas these changes require the adoption of new detailed rules and also the repeal of certain existing rules; whereas it is appropriate in the interest of transparency to group the rules on the administration of all tariff quotas in the sector into one single Regulation and to provide for the opening of the various quotas in separate legal texts;
Whereas henceforth the duty payable upon information into the Community is fixed in the Common Customs Tariff;
Whereas it is appropriate to maintain the obligation to submit a licence upon importation and upon exportation of all products of the sector, with the exception of pure-bred sheep and goats and certain offals and fats;
Whereas since the Agreement on Agriculture requires the conversion of Voluntary Restraint Agreements into country-specific tariff quotas it is necessary to provide for a management system which ensures that only products originating in those specific countries can be imported under the tariff quotas; whereas the latter consideration as well as the need to ensure a smooth transition to the new regime warrant a system whereby the issuance of an import licence is made subject to the presentation of a document of origin issued by an authority of the exporting State which fulfils certain criteria and which has been recognized by the Community; whereas it is therefore necessary to fix the said criteria and in particular to require of the issuing authorities of the exporting countries that controls be effected as to the adherence to the quantities that may be imported under the quotas, notably through a system of precise and regular notifications to the Commission of the quantities in respect of which documents of origin have been issued;
Wheres rules should be laid down for the format and the other details of the document of origin as well as for the procedures to be followed in respect of its issuance and its exchange for an import licence; whereas the introduction of yearly tariff quotas also requires strict rules as to the validity of documents of origin and of import licences;
Whereas the additional preferential imports provided for in the Association Agreements with the countries of Central Europe should be administered in the same way as the country-specific quotas resulting from the Uruguay Round of multilateral trade negotiations;
Whereas the Community also undertook in the Uruguay Round of multilateral trade negotiations to open a non-country-specific tariff quota for countries other than those for which a country-specific quota was provided for; whereas it is appropriate to manage this quota in the same way as the autonomous import system laid down in Commission Regulation (EEC) No 3653/85 (11), as last amended by Regulation (EEC) No 2779/93 (12); whereas the detailed rules should therefore provide for the issuance of import licences on a quarterly basis and, where necessary, the application of a reduction coefficient;
Whereas an efficient administration of these tariff quotas also requires a regular flow of information from the Member States to the Commission on the quantities in respect of which import licences have been issued; whereas the frequency of the notifications relating to a country-specific quota should be increased when the annual quota is close to exhaustion whereas the Member States should also inform the Commission of quantities for which export licences have been issued;
Whereas the abolition of the variable import levy and the introduction of tariff quotas require the repeal of Commission Regulations (EEC) No 2668/80 (13), as last amended by Regulation (EEC) No 3890/92 (14), (EEC) No 19/82 (15), as last amended by Regulation (EC) No 3302/94 (16), (EEC) No 20/82 (17), as last amended by Regulations (EC) No 3302/94 and (EEC) No 3653/85; whereas it is, however, necessary to provide that those regulations remain applicable to import licences that have been issued thereunder;
Whereas the Management Committee for Sheep and Goats has not delivered an opinion within the time limit set by its Chairman,
HAS ADOPTED THIS REGULATION:
Article 1
This Regulation lays down detailed rules for the application of Articles 9 and 12 Regulation (EEC) No 3013/89.
Article 2
Notwithstanding the conditions laid down in Title II of this Regulation, the importation into the Community of any of the products listed under points (a), (c) and (d) of Article 1 of Regulation (EEC) No 3013/89 shall be subject to the submission of an import licence issued by the Member State to any applicant who so requests, irrespective of the place of his establishment in the Community.
That import licence shall be valid throughout the Community.
Article 3
1. The exportation from the Community of any of the products listed under points (a), (c) and (d) of Article 1 of Regulation (EEC) No 3013/89 shall be subject to the provision of an export licence issued by the Member State to any applicant who so requests, irrespective of his place of establishment in the Community.
2. The export licence shall be valid for three months from its date of issue within the meaning of Article 21 (1), of Commission Regulation (EC) No 3719/88 (18).
3. The export licence application and the licence itself shall state, in section 7, the country of destination of the product.
TITLE I
Standard Import System
Article 4
An import licence for the import of products not covered by Title II of this regulation shall be valid for three months from its date of issue within the meaning of Article 21 (1) of Regulation (EEC) No 3719/88.
Article 5
1. The import licence application and the import licence itself shall be endorsed with the country of origin. The import licence shall carry with it an obligation to import from that country.
2. The import licence shall be issued on the fifth working day following the date on which the application is lodged.
Article 6
1. The issue of the import licence shall be conditional on the provision of a security as a guarantee that importation will be effected during the period of validity of the licence. The security shall be wholly forfeit if the operation is not carried out or partially forfeit if the operation is only partially carried out, within that period.
2. The amount of security in respect of import licences shall be:
- 1 ECU per head for live animals,
- 7 ECU per 100 kilograms for other products.
Where applications for import licences are refused, the security shall be released forthwith for the quantity for which the application was not granted.
TITLE II
Quotas
Article 7
The import quantities to which this title refers are set out in Commission Regulation (EC) No 1440/95 (19) and in subsequent annual tariff quota regulations.
A. Import of products under CN codes 0104 10 30, 0104 10 80, 0104 20 90 and 0204 under GATT/WTO country-specific tariff quotas and under preferential quota systems
Article 8
Applications for an import licence for imports within country-specific tariff quotas referred to in Article 12 of Regulation (EEC) No 3013/89 and for imports under the Europe Agreements establishing an association between the European Communities and their Member States of the one part, and Bulgaria, the Czech Republic, Hungary, Poland, Slovakia and Romania of the other part, shall be accompanied by a valid document of origin.
Article 9
1. The document of origin referred to in Article 8 shall be valid only if it is duly completed and endorsed, in accordance with the provisions of this Regulation, by an issuing authority shown in the list in Annex I.
2. The document of origin shall be deemed to have been duly endorsed if it specifies the place and date of issue and the final date of validity, and if it bears the stamp of the issuing authority and the signature of the person or persons empowered to sign it.
Article 10
1. The document of origin referred to in Article 8 shall be drawn up in one original and three numbered copies of different colours and shall consist of a form, a model of which is shown in Annex II.
The form shall measure approximately 210 × 297 mm. The original shall be drawn up on such paper as shall show up any tampering by mechanical or chemical means.
2. The forms shall be printed and completed in at least one of the official languages of the Community.
3. The original and the copies thereof shall be either typewritten or handwritten. In the latter case, they must be completed in ink and in block capitals.
4. Each document of origin shall bear an individual serial number allocated by the issuing authority referred to in Article 9. The copies shall bear the same serial number as the original.
5. Each document of origin shall bear the reference 'issued in accordance with Title II A of Regulation (EC) No 1439/95.`
6. The issuing authority shall keep two copies and give the original and one copy to the applicant.
Article 11
1. The document of origin shall be valid for three months from its actual date of issue but in any event not later than 31 December of the year of its issue.
The original of the document of origin shall be submitted, together with a copy, to the competent authorities at the time when the application for the corresponding import licence is submitted.
However, from 1 October, documents of origin valid from 1 January until 31 March of the following year for quantities within the quota for that year may be issued on condition that they are not used in applications for import licences until 1 January of that year.
2. The original shall be retained by the authority issuing the import licence. However, where the application for an import licence relates to only part of the quantity appearing on the document of origin, the issuing authority shall state on the latter the quantity in respect of which it has been used and, after having affixed its stamp thereto, shall pass it to the party concerned.
Article 12
1. An issuing authority shown in the list set out in Annex I shall:
(a) be recognized as such by the exporting third country;
(b) undertake to verify the particulars appearing on document of origin;
(c) undertake to issue documents of origin only within the quantities and the duties provided for in Regulation (EC) No 1440/95 and in subsequent annual tariff quota regulations;
(d) undertake to communicate to the Commission before the fifteenth day of each month the quantities, including the CN codes, in respect of which documents of origin were issued, together with the issue number of each document and the year to which it refers, broken down according to the duty payable and the intended destination during the preceding month; however, for all products they shall as soon as the documents of origin for 75 % of the quantities concerned have been issued (undertake, at the Commission's request, to communicate to the Commission any relevant information more frequently);
(e) at the request of the Commission, undertaken to supply to the Commission and, if appropriate, to the Member States any item of relevant information enabling the particulars appearing on documents of origin to be verified.
2. If the conditions referred to in paragraph 1 are not fully satisfied the list may be revised or it may be decided to introduce new rules for the administration of the import arrangements concerned.
Article 13
1. The import licence referred to in Article 8 shall be issued not later than one working day following that on which the application is lodged. Subject to the third subparagraph of Article 11 (1), it shall be valid until the final date of validity of the document of origin submitted in accordance with Article 8 but not later than 31 December of the year of issue of the document of origin.
However, in duly justified exceptional cases Member States may extend the validity of an import licence for a period up to 25 January of the following year. Member States shall inform the Commission before 31 March each year of the import quantities and the circumstances involved for each supplier country.
However, as soon as the Commission has requested from a supplier country, pursuant to Article 12 (1) (d), more frequent data as to the issue of documents of origin, the Commission can request that the import licence be issued only after the competent authority is satisfied that all information on the document of origin corresponds to the information received from the Commission through more frequent communication on the matter. The licence shall be issued immediately thereafter.
2. Import licences shall be issued only within the quantities laid down in the relevant tariff quotas and only in response to an application accompanied by a valid document of origin issued for the same calendar year.
3. On issue, each import licence shall bear in Box 20 the remark 'issued in accordance with Title II.A of Regulation (EC) No 1439/95`.
4. No security shall be required for the issue of the import licence referred to in paragraph 1.
5. The import licence must be returned to the issuing agency as soon as possible after use and not later than five days after its expiry.
Article 14
1. Licence applications and licences shall bear in Box 8 the name of the country of origin. In the case of produce falling within CN codes 0104 10 30, 0104 10 80 and 0104 20 90, licence applications and licences shall bear in Boxes 17 and 18 particulars of the net mass and where appropriate the number of animals to be imported.
A licence shall make it compulsory to import the products from the country indicated.
2. Notwithstanding Article 8 (4) of Regulation (EEC) No 3719/88, the quantity put in free circulation may not exceed that indicated in Boxes 17 and 18 of the import licence; the number '0 ` shall be entered to this effect in Box 19 of the said licence.
3. Import licences issued in respect of the quantities referred to in Annex I to Regulation (EC) No 1440/95 and in subsequent annual tariff quota regulations shall bear in Box 24 at least one of the following entries:
- Derecho limitado a 0 [aplicación del Anexo I del Reglamento (CE) n° 1440/95 y de posteriores Reglamentos por los que se establecen contingentes arancelarios anuales]
- Told nedsat til 0 (jf. bilag I til forordning (EF) nr. 1440/95 og efterfølgende forordninger om årlige toldkontingenter)
- Beschränkung des Zollsatzes auf Null (Anwendung von Anhang I der Verordnung (EG) Nr. 1440/95 und der späteren jährlichen Verordnungen über die Zollkontingente)
- Äáóìüò ðåñéïñéæüìåíïò óôï ìçäÝí [åöáñìïãÞ ôïõ ðáñáñôÞìáôïò É ôïõ êáíïíéóìïý (ÅÊ) áñéè. 1440/95 êáé ôùí ìåôáãåíÝóôåñùí êáíïíéóìþí ó÷åôéêÜ ìå ôçí åôÞóéá äáóìïëïãéêÞ ðïóüóôùóç]
- Duty limited to zero (application of Annex I of Regulation (EC) No 1440/95 and subsequent annual tariff quota regulations)
- Droit de douane nul [application de l'annexe I du règlement (CE) n° 1440/95]
- Dazio limitato a zero [applicazione dell'allegato I del regolamento (CE) n. 1440/95 e dei successivi regolamenti relativi ai contingenti tariffari annuali]
- Invoerrecht beperkt tot 0 (toepassing van bijlage I bij Verordening (EG) nr. 1440/95)
- Direito limitado a zero (aplicação do anexo I do Regulamento (CE) nº 1440/95 e regulamentos subsequentes relativos aos contingentes pautais anuais)
- Tulli rajoitettu 0 prosenttiin [asetuksen (EY) N:o 1440/95 liitteen I ja sen jälkeen annettujen vuotuisia tariffikiintiötä koskevien asetusten soveltaminen]
- Tull begränsad till noll procent (tillämpning av bilaga I i förordning (EG) nr 1440/95).
4. Import licences issued in respect of the quantities referred to in Annex II of Regulation (EC) No 1440/95 and in subsequent annual tariff quota regulations shall bear in Box 24 at least one of the following entries:
- Derecho limitado a 4 % [aplicación del Anexo II del Reglamento (CE) n° 1440/95 y de posteriores Reglamentos por los que se establecen contingentes arancelarios anuales]
- Told nedsat til 4 % (jf. bilag II til forordning (EF) nr. 1440/95 og efterfølgende forordninger om årlige toldkontingenter)
- Beschränkung des Zollsatzes auf 4 % (Anwendung von Anhang II der Verordnung (EG) Nr. 1440/95 und der späteren jährlichen Verordnungen über die Zollkontingente)
- Äáóìüò ðåñéïñéæüìåíïò óôï 4 % [åöáñìïãÞ ôïõ ðáñáñôÞìáôïò ÉÉ ôïõ êáíïíéóìïý (ÅÊ) áñéè. 1440/95 êáé ôùí ìåôáãåíÝóôåñùí êáíïíéóìþí ó÷åôéêÜ ìå ôçí åôÞóéá äáóìïëïãéêÞ ðïóüóôùóç]
- Duty limited to 4 % (application of Annex II of Regulation (EC) No 1440/95 and subsequent annual tariff quota regulations)
- Droit de douane 4 % [application de l'annexe II du règlement (CE) n° 1440/95]
- Dazio limitato a 4 % (applicazione dell'allegato II del regolamento (CE) n. 1440/95 e dei successivi regolamenti relativi ai contingenti tariffari annuali]
- Invoerrecht beperkt tot 4 % (toepassing van bijlage II bij Verordening (EG) nr. 1440/95)
- Direito limitado a 4 % (aplicação do anexo II do Regulamento (CE) nº 1440/95 e regulamentos subsequentes relativos aos contingentes pautais anuais)
- Tulli rajoitettu 4 % prosenttiin [asetuksen (EY) N:o 1440/95 liitteen II ja sen jälkeen annettujen vuotuisia tariffikiintiötä koskevien asetusten soveltaminen]
- Tull begränsad till 4 % procent (tillämpning av bilaga II i förordning (EG) nr 1440/95).
5. Import licences issued in respect of the quantities referred to in Annex III to Regulation (EC) No 1440/95 and in subsequent annual tariff quota regualtions shall bear in Box 24 at least one of the following entries:
- Derecho limitado a 10 % [aplicación del Anexo III del Reglamento (CE) n° 1440/95 y de posteriores Reglamentos por los que se establecen contingentes arancelarios anuales]
- Told nedsat til 10 % (jf. bilag III til forordning (EF) nr. 1440/95 og efterfølgende forordninger om årlige toldkontingenter)
- Beschränkung des Zollsatzes auf 10 % (Anwendung von Anhang III der Verordnung (EG) Nr. 1440/95 und der späteren jährlichen Verordnungen über die Zollkontingente)
- Äáóìüò ðåñéïñéæüìåíïò óôï 10 % [åöáñìïãÞ ôïõ ðáñáñôÞìáôïò III ôïõ êáíïíéóìïý (ÅÊ) áñéè. 1440/95 êáé ôùí ìåôáãåíÝóôåñùí êáíïíéóìþí ó÷åôéêÜ ìå ôçí åôÞóéá äáóìïëïãéêÞ ðïóüóôùóç]
- Duty limited to 10 % (application of Annex III of Regulation (EC) No 1440/95 and subsequent annual tariff quota regulations)
- Droit de douane 10 % [application de l'annexe III du règlement (CE) n° 1440/95]
- Dazio limitato a 10 % (applicazione dell'allegato III del regolamento (CE) n. 1440/95 e dei successivi regolamenti relativi ai contingenti tariffari annuali]
- Invoerrecht beperkt tot 10 % (toepassing van bijlage III bij Verordening (EG) nr. 1440/95)
- Direito limitado a 10 % (aplicação do anexo III do Regulamento (CE) nº 1440/95 e regulamentos subsequentes relativos aos contingentes pautais anuais)
- Tulli rajoitettu 10 % prosenttiin [asetuksen (EY) N:o 1440/95 liitteen III ja sen jälkeen annettujen vuotuisia tariffikiintiötä koskevien asetusten soveltaminen]
- Tull begränsad till 10 % procent (tillämpning av bilaga III i förordning (EG) nr 1440/95).
B: Imports of products under CN codes 0104 10 30, 0104 10 80, 0104 20 90 and 0204 pursuant to GATT/WTO non-country specific tariff quotas
Article 15
Member States shall issue import licences for the import of products under GATT non-country-specific tariff quotas for supplier countries other than those included in Title II.A.
During each of the first three quarters of each year, such import licences shall be issued within the limits of one quarter of the quantities, expressed in tonnes liveweight and referred to in Annex IV, Part A, and expressed in tonnes of carcase equivalent and referred to in Annex IV, Part B to Regulation (EC) No 1440/95 and in subsequent annual tariff quota regulations.
During September of each year Member States shall issue import licences within the remaining balance of these quantities.
Article 16
1. The maximum overall quantity for which any one party may apply by lodging one or more licence applications shall be that laid down in Annex IV to Regulation (EC) No 1440/95 and in subsequent annual tariff quota regulations for the quarter in which the licence application(s) concerned is (are) lodged.
2. Applications for licences may be lodged only during the first 10 days of each of the first three quarters of the year and during the first 10 days of September.
3. Applications for licences, broken down by product (referring to total quantities expressed in carcasse equivalent) and by country of origin, shall be forwarded by the Member States to the Commission no later than the sixteenth day of each of the first three quarters and by 16 September, at 5 pm.
4. The Commission shall decide, before the twenty-sixth day of each of the first three quarters and before 26 September by product and by country of origin, either:
(a) to authorize the issue of licences for all the quantities applied for, or
(b) to reduce all the quantities applied for by the same percentage.
Subject to the Commission decision Member States shall issue the licences only within the quantities for which they have forwarded application to the Commission.
5. Licences shall be issued on the thirthieth day of each of the first three quarters and on 30 September.
6. On issue each import licence shall bear the reference in Box 20 'issued in accordance with Title II.B of Regulation (EC) No 1439/95.`
Article 17
1. Import licences referred to in Article 15 of this Regulation shall be valid for three months from their date of issue within the meaning of Article 21 (1) of Regulation (EEC) No 3919/88.
2. Licence applications and licences shall bear in Box 8 the name of the country of origin. In the case of products falling within CN codes 0104 10 30, 0104 10 80 and 0104 20 90, licence applications and licences shall bear in Boxes 17 and 18 particulars of the net mass and where appropriate the number of animals to be imported.
A licence shall make it compulsory to import products from the country indicated.
3. Notwithstanding Article 8 (4) of Regulation (EEC) No 3719/88, the quantity put in free circulation may not exceed that indicated in Boxes 17 and 18 of the import licence; the number '0` shall be entered to this effect in Box 19 of the said licence.
4. Import licences issued in respect of the quantities referred to in Annex IV, Part A, to Regulation (EC) No 1440/95 and in subsequent annual tariff quota regulations shall bear in Box 24 at least one of the following entries:
- Derecho limitado a 0 [aplicación de la parte A del Anexo IV del Reglamento (CE) n° 1440/95 y de posteriores Reglamentos por los que se establecen contingentes arancelarios anuales]
- Told nedsat til 0 (jf. bilag IV, del A til forordning (EF) nr. 1440/95 og efterfølgende forordninger om årlige toldkontingenter)
- Beschränkung des Zollsatzes auf Null (Anwendung von Anhang IV Teil A der Verordnung (EG) Nr. 1440/95 und der späteren jährlichen Verordnungen über die Zollkontingente)
- Äáóìüò ðåñéïñéæüìåíïò óôï ìçäÝí [åöáñìïãÞ ôïõ ðáñáñôÞìáôïò ÉV óçìåßï Á ôïõ êáíïíéóìïý (ÅÊ) áñéè. 1440/95 êáé ôùí ìåôáãåíÝóôåñùí êáíïíéóìþí ó÷åôéêÜ ìå ôçí åôÞóéá äáóìïëïãéêÞ ðïóüóôùóç]
- Duty limited to zero (application of Annex IV Part A of Regulation (EC) No 1440/95 and subsequent annual tariff quota regulations)
- Droit de douane nul [application de la partie A de l'annexe IV du règlement (CE) n° 1440/95]
- Dazio limitato a zero [applicazione dell'allegato IV A del regolamento (CE) n. 1440/95 e dei successivi regolamenti relativi ai contingenti tariffari annuali]
- Invoerrecht beperkt tot 0 (toepassing van bijlage IV deel A bij Verordening (EG) nr. 1440/95)
- Direito limitado a zero (aplicação do anexo IV, ponto A, do Regulamento (CE) nº 1440/95 e regulamentos subsequentes relativos aos contingentes pautais anuais)
- Tulli rajoitettu 0:aan [asetuksen (EY) N:o 1440/95 liitteen IV kohta A ja sen jälkeen annettujen vuotuisia tariffikiintiötä koskevien asetusten soveltaminen]
- Tull begränsad till noll (tillämpning av bilaga IV, punkt A, i förordning (EG) nr 1440/95).
5. Import licences issued in respect of the quantities refered to in Annex IV, Part B to Regulation (EC) No 1440/95 and in subsequent annual tariff quota regulations shall bear in Box 24 at least one of the following entries:
- Derecho limitado a 0 [aplicación de la parte B del Anexo IV del Reglamento (CE) n° 1440/95 y de posteriores Reglamentos por los que se establecen contingentes arancelarios anuales]
- Told nedsat til 0 (jf. bilag IV, del B til forordning (EF) nr. 1440/95 og efterfølgende forordninger om årlige toldkontingenter)
- Beschränkung des Zollsatzes auf Null (Anwendung von Anhang IV Teil B der Verordnung (EG) Nr. 1440/95 und der späteren jährlichen Verordnungen über die Zollkontingente)
- Äáóìüò ðåñéïñéæüìåíïò óôï ìçäÝí [åöáñìïãÞ ôïõ ðáñáñôÞìáôïò ÉV óçìåßï Â ôïõ êáíïíéóìïý (ÅÊ) áñéè. 1440/95 êáé ôùí ìåôáãåíÝóôåñùí êáíïíéóìþí ó÷åôéêÜ ìå ôçí åôÞóéá äáóìïëïãéêÞ ðïóüóôùóç]
- Duty limited to zero (application of Annex IV Part B of Regulation (EC) No 1440/95 and subsequent annual tariff quota regulations)
- Droit de douane nul [application de la partie B de l'annexe IV du règlement (CE) n° 1440/95]
- Dazio limitato a zero [applicazione dell'allegato IV B del regolamento (CE) n. 1440/95 e dei successivi regolamenti relativi ai contingenti tariffari annuali]
- Invoerrecht beperkt tot 0 (toepassing van bijlage IV deel B bij Verordening (EG) nr. 1440/95)
- Direito limitado a zero (aplicação do anexo IV, ponto B, do Regulamento (CE) nº 1440/95 e regulamentos subsequentes relativos aos contingentes pautais anuais)
- Tulli rajoitettu 0:aan [asetuksen (EY) N:o 1440/95 liitteen IV kohta B ja sen jälkeen annettujen vuotuisia tariffikiintiötä koskevien asetusten soveltaminen]
- Tull begränsad till noll (tillämpning av bilaga IV, punkt B, i förordning (EG) nr 1440/95).
Article 18
1. The issue of the import licence shall be conditional on the provision of a security as a guarantee that importation will be effected during the period of validity of the licence. The security shall be wholly forfeit if the operation is not carried out, or partially forfeit if the operation is only partially carried out within that period.
2. The level of the security relating to the import licences shall be:
- 1 ECU per animal for live animals,
- 7 ECU per 100 kg for other products.
TITLE III
Notification
Article 19
1. In respect of Title I, Member States shall communicate to the Commission before 15 July and 15 November each year the cumulative situation in respect of import licences issued for the periods January to June and January to October respectively. They shall also communicate before 31 January each year the final cumulative total of import licences issued during the course of the previous year.
2. In respect of Title II.A:
(a) Member States shall communicate to the Commission before the fifth working day of each month, by telex or by fax, the quantities, by product and by origin, in respect of which during the preceding month:
- the import licences referred to in Article 8 have been issued,
- the import licences returned to the issuing agency pursuant to Article 13 (5) have been used.
However, as soon as the Commission has requested from a supplier country, pursuant to Article 12 (1) (d), more frequent data as to the issue of documents of origin, the Member States as well shall communicate to the Commission more frequently the same information.
(b) Member States shall communicate to the Commission before 15 July, 15 September and 15 November each year, the cumulative situation in respect of import licences issued for the periods January to June, January to August and January to October respectively; they shall also communicate before 31 January each year the final cumulative total of import licences issued during the course of the previous year.
3. In respect of Title II.B, Member States shall communicate to the Commission before 15 February, 15 May, 15 August and 15 October each year the cumulative situation in respect of import licences issued for the first three quarters and September of each year.
4. In respect of exports, Member States shall communicate to the Commission before the fifth working day of each month, by telex on fax, the quantities by product and by destination in respect of which export licences have been issued.
Article 20
Regulations (EEC) No 2668/80, (EEC) No 19/82, (EEC) No 20/82 and (EEC) No 3653/85 are hereby repealed. However they shall remain applicable to import licences issued under those regulations.
Article 21
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
It shall apply from 1 July 1995.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 26 June 1995.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ No L 289, 7. 10. 1989, p. 1.
(2) OJ No L 123, 3. 6. 1995, p. 1.
(3) OJ No L 349, 31. 12. 1994, p. 105.
(4) OJ No L 319, 21. 12. 1993, p. 1.
(5) OJ No L 319, 21. 12. 1993, p. 4.
(6) OJ No L 341, 30. 12. 1994, p. 14.
(7) OJ No L 341, 30. 12. 1994, p. 17.
(8) OJ No L 368, 31. 12. 1994, p. 1.
(9) OJ No L 368, 31. 12. 1994, p. 5.
(10) OJ No L 336, 22. 12. 1994, p. 22.
(11) OJ No L 348, 24. 12. 1985, p. 21.
(12) OJ No L 252, 9. 10. 1993, p. 10.
(13) OJ No L 276, 20. 10. 1980, p. 39.
(14) OJ No L 391, 31. 12. 1992, p. 51.
(15) OJ No L 3, 7. 1. 1982, p. 18.
(16) OJ No L 3, 7. 1. 1982, p. 26.
(17) OJ No L 341, 30. 12. 1994, p. 45.
(18) OJ No L 331, 2. 12. 1988, p. 1.
(19) See page 17 of this Official Journal.
ANNEX I
List of authorities in exporting countries empowered to issue Documents of origin
1. Argentina: Secretaria de agricultura ganaderia y pesca
2. Australia: Australian Meat and Livestock Corporation
3. Bosnia-Herzegovina: Economic Chamber of Bosnia and Herzegovina
4. Bulgaria: Ministry of Industry and Trade
5. Chile: Servicio agricola y ganadero del Ministerio de Agricultura - Santiago
6. Croatia: 'EUROINSPEKT`, Zagreb
7. Hungary: Ministry of International Economic Relations
8. Island: Ministry of Trade
9. Former Yugoslav Republic of Macedonia: Chambre d'économie, Skopje
10. New Zealand: New Zealand Meat Producers Board
11. Poland: Ministertwe Wspocpracy gospodarczej z zagranica
12. Romania: Ministère du Commerce et du Tourisme - Département pour le commerce extérieur
13. Slovenia: 'INSPECT`, Ljubljana
14. Slovakia: Ministry of Economy
15. Czech Republic: Ministry of Industry and Trade
16. Uruguay: Instituto nacional de carnes (Inac)
ANNEX II
Document of origin
>REFERENCE TO A FILM>
