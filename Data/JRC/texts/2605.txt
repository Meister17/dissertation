Decision of the EEA Joint Committee No 69/2005
of 29 April 2005
amending Annex XXI (Statistics) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex XXI to the Agreement was amended by Decision of the EEA Joint Committee No 43/2005 of 11 March 2005 [1].
(2) Commission Decision 2004/883/EC of 10 December 2004 adjusting the Annex to Council Directive 95/57/EC on the collection of statistical information in the field of tourism as regards country lists [2] is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
The following indent shall be added in point 7c (Council Directive 95/57/EC) of Annex XXI to the Agreement:
"— 32004 D 0883: Commission Decision 2004/883/EC of 10 December 2004 (OJ L 373, 21.12.2004, p. 69)."
Article 2
The text of Decision 2004/883/EC in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 30 April 2005, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [3].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 29 April 2005.
For the EEA Joint Committee
The President
Richard Wright
[1] OJ L 198, 28.7.2005, p. 45.
[2] OJ L 373, 21.12.2004, p. 69.
[3] No constitutional requirements indicated.
--------------------------------------------------
