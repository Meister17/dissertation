Council Regulation (EC) No 171/2005
of 31 January 2005
amending and suspending the application of Regulation (EC) No 2193/2003 establishing additional customs duties on imports of certain products originating in the United States of America
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 133 thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) On 7 May 2003, the Community was authorised by the Dispute Settlement Body of the World Trade Organisation (WTO) to impose countermeasures up to a level of USD 4,043 million in the form of additional 100 % ad valorem duties on certain products originating in the United States of America. Consequently, on 8 December 2003 the Community adopted Council Regulation (EC) No 2193/2003 [1] establishing additional customs duties on imports of certain products originating in the United States of America.
(2) It is considered that, following the adoption of the American Jobs Creation Act of 2004, the application of additional duties should be suspended and only take effect again on 1 January 2006 or 60 days after the Dispute Settlement Body of the WTO confirms, whichever date is later, the incompatibility of certain aspects of the abovementioned Act with WTO law,
HAS ADOPTED THIS REGULATION:
Article 1
The application of Regulation (EC) No 2193/2003 is hereby suspended.
Article 2
1. Regulation (EC) No 2193/2003 shall be applicable again with effect from 1 January 2006 or 60 days after the confirmation by the Dispute Settlement Body of the WTO, whichever date is later, that certain aspects of the American Jobs Creation Act of 2004 of the United States of America are inconsistent with the United States' WTO obligations.
2. Before the expiry of the above deadline, the Commission shall publish a notice in the Official Journal of the European Union that such confirmation has been given.
Article 3
1. Article 2(1) of Regulation (EC) No 2193/2003 shall be replaced as follows:
"1. A 14 % ad valorem duty additional to the customs duty applicable under Regulation (EEC) No 2913/92 shall be imposed on the products originating in the United States of America listed in the Annex to this Regulation.".
2. The Annex to Regulation (EC) No 2193/2003 shall be replaced by the Annex set out in the Annex to this Regulation.
Article 4
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Union.
With the exception of Article 3, it shall apply with effect from 1 January 2005.
Article 3 shall apply with effect from the date Regulation (EC) No 2193/2003 becomes applicable again in accordance with Article 2(1).
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 31 January 2005.
For the Council
The President
J. Asselborn
--------------------------------------------------
[1] OJ L 328, 17.12.2003, p. 3.
--------------------------------------------------
+++++ ANNEX 1 +++++</br>
