Commission Regulation (EC) No 583/2005
of 15 April 2005
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 16 April 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 15 April 2005.
For the Commission
J. M. Silva Rodríguez
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 1947/2002 (OJ L 299, 1.11.2002, p. 17).
--------------------------------------------------
ANNEX
to Commission Regulation of 15 April 2005 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code | Standard import value |
07020000 | 052 | 107,6 |
204 | 63,6 |
212 | 146,4 |
624 | 101,8 |
999 | 104,9 |
07070005 | 052 | 138,3 |
204 | 48,4 |
999 | 93,4 |
07099070 | 052 | 106,9 |
204 | 36,8 |
999 | 71,9 |
08051020 | 052 | 47,3 |
204 | 44,8 |
212 | 52,7 |
220 | 45,6 |
400 | 53,7 |
624 | 58,6 |
999 | 50,5 |
08055010 | 052 | 56,6 |
220 | 69,6 |
400 | 69,0 |
624 | 68,2 |
999 | 65,9 |
08081080 | 388 | 85,7 |
400 | 140,6 |
404 | 111,3 |
508 | 62,9 |
512 | 71,2 |
524 | 45,3 |
528 | 77,4 |
720 | 81,3 |
804 | 117,0 |
999 | 88,1 |
08082050 | 388 | 83,7 |
512 | 70,3 |
528 | 69,3 |
720 | 59,5 |
999 | 70,7 |
--------------------------------------------------
