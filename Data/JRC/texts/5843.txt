Commission Decision
of 3 March 2005
initiating the investigation provided for in Article 4(3) of Council Regulation (EEC) No 2408/92 on access for Community air carriers to intra-Community air routes
(notified under document number C(2005) 577)
(Text with EEA relevance)
(2005/247/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2408/92 of 23 July 1992 [1], and in particular Article 4(3) thereof,
Whereas:
I. The facts
(1) On 10 December 2004, pursuant to Article 4(1)(a) of Regulation (EEC) No 2408/92, the Italian Republic asked the Commission to publish a notice in the Official Journal of the European Union imposing public service obligations (PSO) on 18 routes between the Sardinian airports and the main national airports [2].
(2) The main points of the notice are as follows:
- It concerns the following 18 routes:
- Alghero–Rome and Rome–Alghero
- Alghero–Milan and Milan–Alghero
- Alghero–Bologna and Bologna–Alghero
- Alghero–Turin and Turin–Alghero
- Alghero–Pisa and Pisa–Alghero
- Cagliari–Rome and Rome–Cagliari
- Cagliari–Milan and Milan–Cagliari
- Cagliari–Bologna and Bologna–Cagliari
- Cagliari–Turin and Turin–Cagliari
- Cagliari–Pisa and Pisa–Cagliari
- Cagliari–Verona and Verona–Cagliari
- Cagliari–Naples and Naples–Cagliari
- Cagliari–Palermo and Palermo–Cagliari
- Olbia–Rome and Rome–Olbia
- Olbia–Milan and Milan–Olbia
- Olbia–Bologna and Bologna–Olbia
- Olbia–Turin and Turin–Olbia
- Olbia–Verona and Verona–Olbia
All 18 routes indicated above and the public service obligations imposed upon them constitute a single package which must be accepted completely and entirely by the interested carriers without any compensation regardless of nature or origin.
Each single carrier (or leading carrier) which accepts the public service obligations must provide a performance security for the purpose of guaranteeing the correct execution and continuation of the service. This security must amount to at least EUR 15 million and be guaranteed by a bank surety to be activated upon the first request for at least EUR 5 million and by an insurance surety for the remaining amount.
The minimum frequency, timetables and capacity offered for each route are given under point "2. PUBLIC SERVICE OBLIGATIONS" of the notice published in Official Journal of the European Union C 306 of 10 December 2004, which is expressly referred to for the purposes of this Decision.
The minimum capacity of the aircraft used is given under point "3. AIRCRAFT TO BE USED" of the notice published in Official Journal of the European Union C 306 of 10 December 2004, which is expressly referred to for the purposes of this Decision.
The fare structure for all the routes concerned is given under point "4. FARES" of the notice published in Official Journal of the European Union C 306 of 10 December 2004, which is expressly referred to for the purposes of this Decision.
In particular, regarding reduced fares, point 4.8 of the notice states that carriers operating on the affected routes are legally bound to apply the reduced fares (specified under "4. FARES"), to at least the following groups of passengers:
- people born in Sardinia, even if they do not live in Sardinia,
- spouses and children of people born in Sardinia.
The public service obligations are valid from 1 January 2005 to 31 December 2007.
Carriers intending to accept the public service obligations must present a formal acceptance to the competent Italian authority within 15 days of publication of the notice in the Official Journal of the European Union.
(3) It should be noted that prior to imposing the public service obligations referred to in this Decision, the Italian Republic had imposed public service obligations, first published in Official Journal of the European Union C 284 of 7 October 2000 [3], on six routes between the Sardinian airports and Rome and Milan. In accordance with Article 4(1)(d) of Regulation (EEC) No 2408/92, these had been put out to tender [4] to select the carriers authorised to operate these routes on an exclusive basis with financial compensation.
(4) The carriers authorised to operate the routes in accordance with the public service obligations imposed were:
- Alitalia: Cagliari–Rome.
- Air One: Cagliari–Milan, Alghero–Milan and Alghero–Rome.
- Merdiana: Olbia–Rome and Olbia–Milan.
(5) These arrangements were replaced by the public service obligations which are the subject of this Decision.
II. Essential elements of the rules on public service obligations
(6) The rules on public service obligations are laid down in Regulation (EEC) No 2408/92, which defines the conditions for applying the principle of freedom to provide services in the air transport sector.
(7) Public service obligations are defined as an exception to the principle of the Regulation that "subject to this Regulation, Community air carriers shall be permitted by the Member State(s) concerned to exercise traffic rights on routes within the Community" [5].
(8) The conditions for imposing them are defined in Article 4. They are interpreted strictly and in accordance with the principles of non-discrimination and proportionality. They must be adequately justified on the basis of the criteria laid down in the same Article.
(9) More precisely, the rules governing public service obligations provide that these may be imposed by a Member State in respect of scheduled air services to an airport serving a peripheral or development region in its territory or on a thin route to any regional airport, provided the route is considered vital for the economic development of the region in which the airport is located and to the extent necessary to ensure on that route the adequate provision of scheduled air services satisfying fixed standards of continuity, regularity, capacity and pricing, standards which air carriers would not meet if they were solely considering their commercial interest.
(10) The adequacy of scheduled air services is assessed by the Member States having regard to the public interest, the possibility of having recourse to other forms of transport, the ability of such forms to meet the transport needs under consideration and the combined effect of all air carriers operating or intending to operate on the route.
(11) Article 4 provides for a two-phase mechanism: in the first phase (Article 4(1)(a)) the Member State concerned imposes a public service obligation on one or more routes, which are open to all Community carriers, provided they meet the obligations. Where no carrier applies to operate the route on which the public service obligation has been imposed, the Member State can move on to a second phase (Article 4(1)(d)) which limits access to that route to only one carrier for a renewable period of up to three years. The carrier is selected by a Community tender procedure. The selected carrier can then receive financial compensation for operating the route in accordance with the public service obligation.
(12) By virtue of Article 4(3) the Commission may decide, following an investigation, carried out either at the request of a Member State or on its own initiative, whether the public service obligation published should continue to apply. The Commission must communicate its decision to the Council and to the Member States. Any Member State may refer the matter to the Council which, acting by a qualified majority, may take a different decision.
III. Elements raising serious doubts as to the conformity of the public service obligations imposed on routes between the Sardinian airports and the main national airports with Article 4 of Regulation (EEC) No 2408/92
(13) Article 4(1)(a) of the Regulation lists a certain number of cumulative criteria for imposing public service obligations:
- Type of route eligible: routes to an airport serving a peripheral or development region in the territory of the Member State concerned or on a thin route to any regional airport in that territory.
- It must be recognised that the route is vital for the economic development of the region in which the airport served is located.
- The principle of adequacy, assessed having regard to the existence of other means of transport or alternative routes, must be observed.
(14) In addition, the public service obligations must comply with the basic principles of proportionality and non-discrimination (see, for example, Court of Justice decision of 20 February 2001, in case C-205/99, Asociación Profesional de Empresas Navieras de Líneas Regulares (Analir) and others
v Administración General del Estado, [2001] ECR p. I-01271).
(15) In the case in point, the notice imposing public service obligations published in the Official Journal at the request of the Italian Republic contains several provisions which raise serious doubts as to their conformity with Article 4 of the Regulation, and are therefore likely to restrict unduly the development of the routes concerned; in particular:
(a) No detailed explanation based on an economic analysis of the air transport market between Sardinia and the rest of Italy has been provided, to justify the need for the new public service obligations, their appropriateness and their proportionality to the objective.
(b) The six routes covered by the previous notice and included in the new one have not been assessed.
(c) It is not evident that the 12 other routes on which public service obligations have been imposed since 1 January 2005 are vital for the economic development of the regions of Sardinia where the airports concerned are located; considering in particular:
- The nature of the routes concerned,
- It has not been shown that these routes are vital for the economic development of the regions of Sardinia in which the airports concerned are located,
- The existence of alternative air routes which allow an adequate and continuous service to be provided to the airports concerned, via the main Italian hubs linked in a satisfactory manner with Sardinia.
(d) The requirement that interested carriers operate all 18 routes to which the public service obligations apply as a single package is a particularly significant restriction of the principle of the freedom to provide services. It is in breach of the principles of proportionality and non-discrimination; considering in particular:
- it has not been shown that grouping all these routes together is vital for the economic development of the regions of Sardinia in which the airports concerned are located,
- the risk of unjustified discrimination between carriers, where only the largest ones have the means to operate in such conditions,
- in addition, such a requirement is contrary to the need for the Member State imposing the public service obligations to take account in its assessment of the combined effect of all air carriers operating or intending to operate on the route [6].
In fact, the Italian authorities wanted to impose the requirement that the eighteen routes be operated as a single package in order to finance the operating losses of thin routes by the expected proceeds of the operation of the most important routes. Such cross-subsidisation is alien to Article 4 of Regulation (EEC) No 2408/92.
(e) The requirement to provide a security of a particularly high amount is also likely to create unjustified discrimination between interested carriers, where only the largest ones have the means to offer such guarantees.
(f) The very short time, 15 days from the publication of the notice in the Official Journal given to interested carriers to accept the public service obligations and twenty two days to begin operating (on 1 January 2005), are likely to create unjustified discrimination between them. In reality, it is impossible for a carrier not already operating on routes to Sardinia to complete the legal and administrative formalities in the time allowed and mobilise the resources needed to set up such an operation.
(g) The requirement, in point 4.8 of the notice, that reduced fares must be applied to passengers solely because of their place of birth (in this case Sardinia) or for the sole reason that they have family links with such persons may in fact be unlawful discrimination based on nationality (see for example case C-338/01 Commission v Italy [2003] ECR p. I-00721).
IV. Procedure
(16) Despite repeated calls from the Commission drawing the attention of the Italian authorities to these problems and expressing doubts as to the conformity of the notice imposing public service obligations with Regulation (EEC) No 2408/92, the Italian Republic decided to have it published.
(17) As soon as it was published, several interested parties contacted the Commission to informally express their concerns regarding the disproportionate and discriminatory nature of the public service obligations. The Commission also received a complaint contesting the legality of these obligations (the author wished to remain anonymous).
(18) In the light of the above, and by virtue of Article 4(3) of Regulation (EEC) No 2408/92 of 23 July 1992, the Commission may carry out an investigation to determine whether the development of one or more routes is unduly restricted by the imposition of public service obligations, in order to decide whether these obligations should continue to be imposed on the routes in question.
HAS ADOPTED THIS DECISION:
Article 1
The Commission will carry out an investigation, as provided for in Article 4(3) of Regulation (EEC) No 2408/92, in order to determine whether the public service obligations imposed on routes between the Sardinian airports and the main national airports, published at the request of the Italian Republic in Official Journal of the European Union C 306 of 10 December 2004, should continue to apply to these routes.
Article 2
1. The Italian Republic shall transmit to the Commission, within one month following the notification of this Decision, all the information necessary for examining the conformity of the public service obligations referred to in Article 1 with Article 4 of Regulation (EEC) No 2408/92.
2. In particular, the following shall be transmitted:
- The legal analysis of the impact on the exercise by all European air carriers of traffic rights in respect of the routes to which the public service obligations published in Official Journal of the European Union C 306 of 10 December 2004 apply, in the event that these obligations are effectively complied with.
- In particular, it must be stated whether the Italian authorities intended to create an exclusive right to operate the 18 routes for the carrier or carriers which formally accepted the obligations.
- The legal analysis, with regard to Community law, justifying the different conditions contained in the notice imposing public service obligations published in Official Journal of the European Union C 306 of 10 December 2004.
- The reasons for imposing reduced fares only for "People born in Sardinia, even if they do not live in Sardinia and spouses and children of people born in Sardinia".
- A detailed assessment of the implementation of the public service obligations published in Official Journal of the European Union C 284 of 7 October 2000.
- A detailed analysis of the economic relations between the regions of Sardinia and the other regions of Italy where the airports concerned by the public service obligations published in Official Journal of the European Union C 306 of 10 December 2004 are located.
- A detailed analysis of the current supply of air transport between the Sardinian airports and the other Italian airports concerned by the public service obligations published in Official Journal of the European Union C 306 of 10 December 2004, including the supply of indirect flights.
- A detailed analysis of the availability of other means of transport and their capacity to meet the transport needs under consideration.
- An analysis of the current demand for air transport for each route concerned by these obligations.
- A precise description of the journey times and frequency required to connect by road the different Sardinian airports concerned by these obligations.
- A description of the situation on the day of notification of this Decision regarding the operation of services in accordance with the obligations and the identity of the carrier or carriers operating the services.
- The operating forecasts (passenger traffic, freight, financial forecasts, etc.) communicated by the carrier or carriers.
- Any claims existing before the national courts on the day of notification of this Decision and the legal situation of the notice imposing the public service obligations.
Article 3
1. This Decision is addressed to the Italian Republic.
2. This Decision shall be published in the Official Journal of the European Union.
Done at Brussels, 3 March 2005.
For the Commission
Jacques Barrot
Vice-President
[1] OJ L 240, 24.8.1992, p. 8. Regulation as last amended by European Parliament and Council Regulation (EC) No 1882/2003 (OJ L 284, 31.10.2003, p. 1).
[2] OJ C 306, 10.12.2004, p. 6.
[3] OJ C 284, 7.10.2000, p. 16. Amendment to OJ C 49, 15.2.2001, p. 2. Corrigendum to OJ C 63, 28.2.2001, p. 12.
[4] OJ C 51, 16.2.2001, p. 22.
[5] Article 3(1) of Regulation (EEC) No 2408/92.
[6] Article 4(1)(b)(iv) of Regulation (EEC) No 2408/92.
--------------------------------------------------
