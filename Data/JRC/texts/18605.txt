COMMISSION REGULATION (EEC) No 2915/93 of 22 October 1993 amending Regulation (EEC) No 3077/78 on the equivalence with Community certificates of attestations accompanying hops imported from non-member countries
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 1696/71 of 26 July 1971 on the common organization of the market in hops (1), as last amended by Regulation (EEC) No 3124/92 (2), and in particular Article 5 (2) thereof,
Whereas Commission Regulation (EEC) No 3077/78 (3), as last amended by Regulation (EEC) No 2238/91 (4), recognizes the equivalence with Community certificates of attestations accompanying hops imported from certain non-member countries and lists the organizations in those countries authorized to issue equivalence attestations as well as the products covered; whereas subsequent political events have made it necessary to revise that list; whereas it is the responsibility of the organizations concerned in those non-member countries to keep up to date the information contained in the Annex to this Regulation and to maintain close cooperation with the Commission by communicating to its departments the information concerned;
Whereas Hungary has subsequently undertaken to comply with the requirements stipulated in respect of the marketing of hops and hop products and has authorized an organization to issue equivalence attestations; whereas such attestations should therefore be recognized as equivalent to Community certificates and the products which they cover be released for free circulation; whereas the Annex to Regulation (EEC) No 3077/78 should be amended accordingly;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Hops,
HAS ADOPTED THIS REGULATION:
Article 1
The following sentence is added to Article 1 of Regulation (EEC) No 3077/78:
'The Annex shall be revised on the basis of information communicated by the non-member countries concerned.'
The Annex to this Regulation replaces the Annex to Commission Regulation (EEC) No 3077/78.
Article 2
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 22 October 1993.
For the Commission
René STEICHEN
Member of the Commission
(1) OJ No L 175, 4. 8. 1971, p. 1.
(2) OJ No L 313, 30. 10. 1992, p. 1.
(3) OJ No L 367, 28. 12. 1978, p. 28.
(4) OJ No L 204, 27. 7. 1991, p. 13.
ANNEX
"" ID="01">USA> ID="02">Inspection Division, Federal Grain Inspection Service
- Idaho Department of Agriculture, Boise, Idaho
- California Department of Agriculture, Sacramento, California
- Oregon Department of Agriculture, Salem, Oregon
- Washington Department of Agriculture, Yakima, Washington> ID="03">Hop cones
Hop powders
Saps and extracts of hops> ID="04">ex 1210
ex 1210
1302 13 00 "> ID="01">Poland> ID="02">Ministère de la coopération économique avec l'étranger, Service du contrôle de la qualité des produits alimentaires, Varsovie> ID="03">Hop cones
Hop powders
Saps and extracts of hops> ID="04">ex 1210
ex 1210
1302 13 00 "> ID="01">Bulgaria> ID="02">Bulgaria, 1738 Gourubliane, Sofia, Pivoimpexengineering> ID="03">Hop cones
Hop powders
Saps and extracts of hops> ID="04">ex 1210
ex 1210
1302 13 00 "> ID="01">Ex-Yugoslavia> ID="02">Poljoprivredni Fakultet Novi Sad
Institut za Ratarstvo I Povrtarstvo - Zavod za Hmelj I Sirak, Backi Petrovac> ID="03">Hop cones
Hop powders
Saps and extracts of hops> ID="04">ex 1210
ex 1210
1302 13 00 "> ID="01">Slovenia> ID="02">Institut za Hmaljarstvo, Pivovarstvo, Zalec> ID="03">Hop cones
Hop powders
Saps and extracts of hops> ID="04">ex 1210
ex 1210
1302 13 00 "> ID="01">People's Republic of China> ID="02">1. Tianjin Import and Export Commodity Inspection Bureau
2. Xinjiang Import and Export Commodity Inspection Bureau
3. Neimonggol Import and Export Commodity Inspection Bureau> ID="03">Hop cones
Hop powders
Saps and extracts of hops> ID="04">ex 1210
ex 1210
1302 13 00 "> ID="01">Slovak Republic> ID="02">UEstredny kontrolny a skúsobny ústav polnohospodársky, Matúsková 21, 833 16 Bratislava> ID="03">Hop cones
Hop powders
Saps and extracts of hops> ID="04">ex 1210
ex 1210
1302 13 00 "> ID="01">Czech Republic> ID="02">Ustredni Kontrolni a zkusebni ustav zemedelsky, Pobocka, Zatec> ID="03">Hop cones
Hop powders
Saps and extracts of hops> ID="04">ex 1210
ex 1210
1302 13 00 "> ID="01">Australia> ID="02">1. Department of Primary Industry and Fisheries, Tasmania
2. Victorian Employers Chamber of Commerce and Industry, Melbourne> ID="03">Hop cones
Hop powders
Saps and extracts of hops> ID="04">ex 1210
ex 1210
1302 13 00 "> ID="01">New Zealand> ID="02">1. Cawthron Institute, Nelson, South Island
2. Ministry of Agriculture and Fisheries, Wellington> ID="03">Hop cones
Hop powders
Saps and extracts of hops> ID="04">ex 1210
ex 1210
1302 13 00 "> ID="01">Romania> ID="02">Institut agronomique 'Docteur Petru Groza' Cluj - Napoca> ID="03">Hop cones
Hop powders
Saps and extracts of hops> ID="04">ex 1210
ex 1210
1302 13 00 "> ID="01">Canada> ID="02">Division de la quarantaine des plantes> ID="03">Hop cones
Hop powders
Saps and extracts of hops> ID="04">ex 1210
ex 1210
1302 13 00 "> ID="01">Austria> ID="02">Bundesanstalt fuer Agrarbiologie, Wieningerstrasse 8, 4025 Linz> ID="03">Hop cones
Hop powders
Saps and extracts of hops> ID="04">ex 1210
ex 1210
1302 13 00 "> ID="01">Hungary> ID="02">Budapest (Foevárosi) Allategészséguegyi és Élelmiszer Ellenoerzoe Allomás (Budapest Veterinary Health and Food Control Station), 1135 Budapest, Lehel u. 43-47> ID="03">Hop cones
Hop powders
Saps and extracts of hops> ID="04">ex 1210
ex 1210
1302 13 00 ">
