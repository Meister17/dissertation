Commission Regulation (EC) No 2094/2004
of 8 December 2004
opening and providing for the administration of a tariff quota of 10000 tonnes of oat grains otherwise worked falling within CN code 11042298
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1784/2003 of 29 September 2003 on the common organisation of the market in cereals [1], and in particular Article 12(1) thereof,
Whereas:
(1) In order to meet its international commitments, the Community has established for each marketing year since 1 January 1996 a zero duty tariff quota for oat grains otherwise worked falling within CN code 11042298.
(2) Up to the 2004/05 marketing year this tariff quota has been managed under Commission Regulation (EC) No 2369/96 of 12 December 1996 opening and providing for the administration of a Community tariff quota for 10000 tonnes of oat grains otherwise worked falling within CN code 11042298 [2]. This Regulation provides for management through import licences, for which applications are submitted monthly.
(3) Commission Regulation (EC) No 2454/93 of 2 July 1993 laying down provisions for the implementation of Council Regulation (EEC) No 2913/92 establishing the Community Customs Code [3] codifies the management rules for tariff quotas designed to be used following the chronological order of dates of customs declarations and for surveillance of preferential imports.
(4) With a view to simplification and given the small volume of the tariff quota for oat grains otherwise worked falling within CN code 11042298, Regulation (EC) No 2454/93 should be applied to the management of this quota from the 2005/06 marketing year on, and Regulation (EC) No 2369/96 should be repealed. For administrative reasons a new serial number must be introduced for the quota in question.
(5) Article 9(1) of Regulation (EC) No 1784/2003 provides for an exemption from the requirement to submit an import licence for products having no significant appreciable impact on the supply situation of the cereals market. The Community annually imports an average of 6000 tonnes of oat grains otherwise worked falling within CN code 11042298. This involves a limited quantity of very specific products for industrial use, with no impact on the cereals market. The exemption from the import licence requirement provided for in Article 9(1) of Regulation (EC) No 1784/2003 can therefore be applied.
(6) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Cereals,
HAS ADOPTED THIS REGULATION:
Article 1
The tariff quota in the Annex is hereby opened for each marketing year running from 1 July to 30 June.
Article 2
The tariff quota referred to in Article 1 shall be managed by the Commission in accordance with the Articles 308a, 308b and 308c of Regulation (EEC) No 2454/93.
Article 3
Imports of oat grains under the tariff quota referred to in Article 1 shall not require the submission of an import licence.
Article 4
Regulation (EC) No 2369/96 is hereby repealed.
Article 5
This Regulation shall enter into force on the seventh day following that of its publication in the Official Journal of the European Union.
It shall apply from 1 July 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 8 December 2004.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 270, 21.10.2003, p. 78.
[2] OJ L 323, 13.12.1996, p. 8. Regulation as last amended by Regulation (EC) No 777/2004 (OJ L 123, 27.4.2004, p. 50).
[3] OJ L 253, 11.10.1993, p. 1. Regulation as last amended by Regulation (EC) No 2286/2003 (OJ L 343, 31.12.2003, p. 1).
--------------------------------------------------
ANNEX
Tariff quota with a quota period running from 1 July to 30 June
Serial number | CN code | Description | Quota volume in net weight(tonnes) | Tariff quota duty | Origin |
09.0043 | 11042298 | Oat grains otherwise worked | 10000 | 0 | All third countries (erga omnes) |
[1] Without prejudice to the rules for interpreting the Combined Nomenclature, the wording for the description of the products is to be considered as having no more than an indicative value, the preferential treatment being determined, in the context of this Annex, by the coverage of the CN codes as they exist at the time of adoption of this Regulation.
--------------------------------------------------
