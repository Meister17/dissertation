Agreement
between the European Community and the Principality of Andorra providing for measures equivalent to those laid down in Council Directive 2003/48/EC on taxation of savings income in the form of interest payments
THE EUROPEAN COMMUNITY,
and
THE PRINCIPALITY OF ANDORRA,
hereinafter referred to as "Contracting Party" or "Contracting Parties" as the context may require,
With a view to introducing measures equivalent to those laid down in Council Directive 2003/48/EC of 3 June 2003 on taxation of savings income in the form of interest payments, hereinafter referred to as ‘the Directive’, within a framework of cooperation which takes account of the legitimate interests of both Contracting Parties and in a context where other third countries in a situation similar to that of the Principality of Andorra will also be applying measures equivalent to the Directive,
HAVE AGREED AS FOLLOWS:
Article 1
Aim
1. Within a framework of cooperation between the European Community and the Principality of Andorra savings income in the form of interest payments made in the Principality of Andorra to beneficial owners who are individuals identified as residents of a Member State of the European Community in accordance with the procedures laid down in Article 3 shall be subject to a withholding tax to be levied by paying agents established on the territory of the Principality of Andorra under the conditions laid down in Article 7.
This withholding tax shall be levied unless the voluntary disclosure measures are applied in accordance with the rules set out in Article 9. The income corresponding to the amount of withholding tax levied in accordance with Articles 7 and 9 shall be shared between the Member States of the European Community and the Principality of Andorra according to the rules set out in Article 8.
To ensure that this Agreement is equivalent to the Directive, these measures shall be supplemented by rules on the exchange of information on request set out in Article 12 and by the consultation and review procedures described in Article 13.
2. The Contracting Parties shall take the necessary measures to implement this Agreement. The Principality of Andorra shall in particular take the necessary measures to ensure the tasks required to implement this Agreement are carried out, irrespective of the place of establishment of the debtor of the debt claim which produces the interest, by the paying agents established on its territory, and shall expressly provide for provisions on procedures and penalties.
Article 2
Definition of beneficial owner
1. For the purposes of this Agreement, "beneficial owner" means any individual who receives an interest payment or any individual for whom an interest payment is secured, unless he provides proof that this payment has not been made or secured on his own account, where:
(a) he acts as a paying agent within the meaning of Article 4; or
(b) he acts on behalf of a legal person, an entity whose profits are taxed under the general arrangements for business taxation, an undertaking for collective investment in transferable securities established in a Member State of the European Community or in the Principality of Andorra; or
(c) he acts on behalf of another individual who is the beneficial owner and informs the paying agent of the identity of this beneficial owner in accordance with Article 3(1).
2. Where a paying agent possesses information suggesting the individual receiving an interest payment or for whom an interest payment is secured may not be the beneficial owner, it must take reasonable measures to establish the identity of the beneficial owner in accordance with Article 3(1). If the paying agent is not able to identify the beneficial owner, it shall treat the individual in question as the beneficial owner.
Article 3
Identity and residence of beneficial owners
1. The paying agent shall establish the identity of the beneficial owner in the form of his name, forename and address according to the anti-money-laundering provisions applying in the Principality of Andorra.
2. The paying agent shall establish the residence of the beneficial owner on the basis of rules which may vary according to the time at which the relations between the paying agent and the beneficial owner of the interest are entered into. Subject to the conditions set out below residence shall be considered to be in the country where the beneficial owner has his permanent address:
(a) for contractual relations entered into before 1 January 2004, the paying agent shall establish the residence of the beneficial owner according to the anti-money-laundering provisions applying in the Principality of Andorra;
(b) for contractual relations entered into, or transactions carried out in the absence of contractual relations, on or after 1 January 2004, the paying agent shall establish the residence of the beneficial owner on the basis of the address mentioned in the official identity document or, if necessary, on the basis of any documentary proof presented by the beneficial owner and according to the following procedure: for individuals presenting an official identity document issued by a Member State of the European Community who declare that they are resident in a State which is not a Member of the European Community, residence shall be established by means of a residence certificate or a document authorising residence issued by the competent authority of that third country in which the individual declares he is resident. If such a certificate of residence or document authorising residence cannot be provided, residence shall be considered to be in the Member State of the European Community which issued the official identity document.
Article 4
Definition of paying agent
For the purposes of this Agreement, "paying agent" means any economic operator established in the Principality of Andorra who pays interest to, or secures the payment of interest for the immediate benefit of, the beneficial owner, whether the operator is the debtor of the debt claim which produces the interest or the operator charged by the debtor or the beneficial owner with paying interest or securing payment of the interest.
Article 5
Definition of competent authority
1. For the purposes of this Agreement the "competent authorities" of the Contracting Parties means those listed in Annex I.
2. For third countries, the competent authority is that defined for the purposes of bilateral or multilateral tax conventions or, failing that, such other authority as is competent to issue certificates of residence for tax purposes.
Article 6
Definition of interest payment
1. For the purposes of this Agreement, "interest payment" means:
(a) interest paid or credited to an account relating to debt claims of any kind, whether or not secured by mortgage and whether or not carrying a right to participate in the debtor's profits, and in particular, income from government securities and income from bonds or debentures, including premiums and prizes attaching to such securities, bonds or debentures; penalty charges for late payment shall not be regarded as interest payments;
(b) interest accrued or capitalised on the sale, refund or redemption of the debt claims referred to in (a);
(c) income deriving from interest payments, either directly or through an entity referred to in Article 4(2) of the Directive distributed by:
(i) undertakings for collective investment established in a Member State of the European Community or in the Principality of Andorra;
(ii) entities which qualify for the option under Article 4(3) of the Directive;
(iii) undertakings for collective investment established outside the territory referred to in Article 17;
(d) income realised on the sale, refund or redemption of shares or units in the following undertakings and entities, if they invest directly or indirectly via other undertakings for collective investment or entities referred to below, more than 40 % of their assets in debt claims as referred to in (a):
(i) undertakings for collective investment established in a Member State of the European Community or in the Principality of Andorra;
(ii) entities which qualify for the option under Article 4(3) of the Directive;
(iii) undertakings for collective investment established outside the territory referred to in Article 17.
However, the Principality of Andorra shall have the option of including income mentioned under (d) in the definition of interest payment only to the extent that such income corresponds to gains directly or indirectly deriving from interest payments within the meaning of (a) and (b).
2. As regards paragraph 1(c) and (d), when a paying agent possesses no information concerning the proportion of the income which derives from interest payments, the total amount of the income shall be considered to be an interest payment.
3. As regards paragraph 1(d), when a paying agent possesses no information concerning the percentage of the assets invested in debt claims or in shares or units defined in that paragraph, that percentage shall be considered to be more than 40 %. Where it cannot determine the amount of income realised by the beneficial owner, the income shall be deemed to correspond to the proceeds of the sale, refund or redemption of the shares or units.
4. As regards paragraph 1(b) and (d), the Principality of Andorra shall have the option of requiring paying agents in its territory to annualise the interest over a period of time which may not exceed one year, and treating such annualised interest as an interest payment even if no sale, redemption or refund occurs during this period.
5. Income derived from undertakings or entities that have invested up to 15 % of their assets in debt-claims within the meaning of paragraph 1(a) are not considered as a payment of interest within the meaning of paragraph 1(c) and (d).
6. From 1 January 2011, the percentage referred to in paragraph 1(d) and paragraph 3 shall be 25 %.
7. The percentages referred to in paragraph 1(d) and paragraph 5 shall be determined by reference to the investment policy laid down in the fund rules or instruments of incorporation of the undertakings or entities concerned and failing that, by reference to the actual composition of the assets of the undertakings or entities concerned.
Article 7
Withholding tax
1. Where the beneficial owner of the interest is resident in a Member State of the European Community, the Principality of Andorra shall levy a withholding tax at a rate of 15 % in the first three years of application of this Agreement, 20 % in the subsequent three years and 35 % thereafter.
2. The paying agent shall levy withholding tax as follows:
(a) in the case of an interest payment within the meaning of Article 6(1)(a): on the amount of interest paid or credited;
(b) in the case of an interest payment within the meaning of Article 6(1)(b) or (d): on the amount of interest or income referred to in those paragraphs or by means of a levy of equivalent effect to be borne by the recipient on the full amount of the proceeds of the sale, redemption or refund;
(c) in the case of an interest payment within the meaning of Article 6(1)(c): on the amount of income referred to in that paragraph;
(d) where the Principality of Andorra exercises the option under Article 6(4): on the amount of annualised interest.
3. For the purposes of points (a) and (b) of paragraph 2, withholding tax shall be levied pro rata to the period of holding of the debt claim by the beneficial owner. When the paying agent is unable to determine the period of holding on the basis of information in its possession, it shall treat the beneficial owner as having held the debt claim throughout its period of existence unless he provides evidence of the date of acquisition.
4. Tax imposed on and tax withheld on an interest payment other than the withholding tax provided for under this Agreement shall be deducted from the withholding tax calculated in accordance with paragraphs 1 to 3 on the same interest payment.
5. Subject to the provisions of Article 10, the levying of withholding tax by a paying agent established in the Principality of Andorra shall not preclude the Member State of the European Community of residence for tax purposes of the beneficial owner from taxing the income in accordance with its national law.
Where a taxpayer declares income from interest paid by a paying agent established in the Principality of Andorra to the tax authorities of the Member State of the European Community where he resides, this income shall be taxed at the same rate as that applying to interest earned in this Member State.
Article 8
Revenue sharing
1. The Principality of Andorra shall retain 25 % of its revenue from the withholding tax referred to in Article 7 and transfer 75 % to the Member State of the European Community of residence of the beneficial owner.
2. Such transfers shall take place in one single operation for each Member State for each calendar year at the latest within a period of six months following the end of the calendar year in which the tax was levied.
The Principality of Andorra shall take the necessary measures to ensure the proper functioning of the revenue-sharing system.
Article 9
Voluntary disclosure
1. The Principality of Andorra shall provide for a procedure allowing beneficial owners not to bear the withholding tax referred to Article 7 where the beneficial owner provides his paying agent with a certificate drawn up in his name by the competent authority of his Member State of residence in accordance with paragraph 2 of this Article.
2. At the request of the beneficial owner, the competent authority of his Member State of residence shall issue a certificate containing the following information:
(a) the name, forename, address and tax identification number or, failing such, the date and place of birth of the beneficial owner;
(b) the name and address of the paying agent;
(c) the account number of the beneficial owner or, where there is none, the identification of the security.
This certificate shall be valid for a period not exceeding three years. It shall be issued to any beneficial owner who requests it within two months of the date of submission of the request.
Article 10
Elimination of double taxation
1. The Member State of the European Community of residence for tax purposes of the beneficial owner shall ensure the elimination of any double taxation which might result from the imposition of the withholding tax referred to in Article 7, in accordance with paragraphs 2 and 3 of this Article.
2. If interest received by a beneficial owner has been subject to the withholding tax referred to in Article 7 in the Principality of Andorra, the Member State of the European Community of residence for tax purposes of the beneficial owner shall grant him, in accordance with its national law, a tax credit equal to the amount of tax withheld. Where this amount exceeds the amount of tax due in accordance with its national law on the total amount of interest subject to withholding tax, the Member State of residence for tax purposes shall repay the excess amount of tax withheld to the beneficial owner.
3. If, in addition to the withholding tax referred to in Article 7, interest received by a beneficial owner has been subject to any other type of withholding tax and the Member State of the European Community of residence for tax purposes grants a tax credit for such withholding tax in accordance with its national law or double taxation conventions, such other withholding tax shall be credited before the procedure in paragraph 2 is applied.
4. The Member State of the European Community of residence for tax purposes of the beneficial owner may replace the tax credit mechanism referred to in paragraphs 2 and 3 by a refund of the withholding tax referred to in Article 7.
Article 11
Negotiable debt securities
1. From the date of application of this Agreement and as long as the Principality of Andorra imposes the withholding tax referred to in Article 7, and at least one Member State of the European Community applies a similar withholding tax, but until 31 December 2010 at the latest, domestic and international bonds and other negotiable debt securities which were first issued before 1 March 2001 or for which the original issuing prospectuses were approved before that date by the competent authorities within the meaning of Council Directive 80/390/EEC, or by the responsible authorities in the Principality of Andorra, or by the responsible authorities in third countries shall not be considered to be debt claims within the meaning of Article 6(1)(a), provided that no further issues of such negotiable debt securities are made after 1 March 2002.
2. However, as long as at least one of the Member States of the European Community also applies similar measures, the provisions of this Article shall continue to apply beyond 31 December 2010 in respect of negotiable debt securities:
- which contain gross-up and early redemption clauses, and
- where the paying agent defined in Article 4 is established in the Principality of Andorra, and
- where the paying agent pays interest to, or secures the payment of interest for the immediate benefit of, a beneficial owner resident in a Member State of the European Community.
If and where all Member States of the European Community cease to apply similar provisions, the provisions of this Article shall continue to apply solely in respect of negotiable debt securities:
- which contain gross-up and early redemption clauses, and
- where the paying agent of the issuer is established in the Principality of Andorra, and
- where that paying agent pays interest to, or secures the payment of interest for the immediate benefit of, a beneficial owner resident in a Member State of the European Community.
If a further issue is made on or after 1 March 2002 of an abovementioned negotiable debt security issued by a government or a related entity acting as a public authority or whose role is recognised by an international Treaty, the entire issue of such security, consisting of the original issue and any further issue, shall be considered a debt claim within the meaning of Article 6(1)(a).
If a further issue is made on or after 1 March 2002 of an abovementioned negotiable debt security issued by any other issuer not covered by the preceding subparagraph, such further issue shall be considered a debt claim within the meaning of Article 6(1)(a).
3. This Article shall not prevent Member States of the European Community and the Principality of Andorra from taxing the income from the negotiable debt securities referred to in paragraph 1 in accordance with their national laws.
Article 12
Exchange of information on request
1. The competent authorities of the Principality of Andorra and the Member States of the European Community shall exchange information concerning the income covered by this Agreement on conduct constituting a crime of tax fraud under the laws of the requested State or the like. The like includes only an offence with the same level of wrongfulness as conduct constituting a crime of tax fraud under the laws of the requested State.
Until it introduces the concept of the crime of tax fraud in its internal law, the Principality of Andorra shall undertake, where it is the requested State, to treat as tax fraud for the purposes of the first subparagraph, conduct which, as a result of deception, damages the financial interests of the tax authorities of the requesting State and constitutes under the laws of the Principality of Andorra the crime of fraud.
In response to a duly justified request, the requested State shall provide information concerning matters mentioned above in this Article which are subject to, or likely to be subject to, an investigation by the requesting State on a non-criminal or criminal basis.
2. In order to determine whether information may be provided in response to a request, the requested State shall apply the statute of limitations under the law of the requesting State instead of the statute of limitations applicable under the law of the requested State.
3. The requested State shall provide information where the requesting State has reasonable grounds for suspecting that such conduct constitutes the crime of tax fraud or the like. Where the Principality of Andorra is the requested State, the acceptability of the request must be determined within a time limit of two months, by the judicial authorities of the Principality of Andorra in relation to the conditions laid down in this Article.
4. The requesting State's grounds for suspecting that an offence may have been committed may be based on:
(a) documents, whether authenticated or not, including books of accounts or accounting documents or documents relating to bank accounts;
(b) statements by the taxpayer;
(c) information obtained from an informant or third person that has been independently corroborated or otherwise appears credible; or
(d) circumstantial indirect evidence.
5. Any information exchanged in this way shall be treated as confidential and may be disclosed only to persons or the competent authorities of the Contracting Party with competence for taxation of the interest payments referred to in Article 1, either as regards withholding tax, and the revenue deriving therefrom referred to in Articles 7 and 8, or as regards the voluntary disclosure arrangements referred to in Article 9. Those persons or authorities may disclose the information received in public court proceedings or in judicial decisions concerning such taxation.
Information may be communicated to another person or authority only with the written and prior agreement of the competent authority of the party providing the information.
6. The Principality of Andorra will agree to enter into bilateral negotiations with any Member State wishing to do so, in order to define the individual categories of cases falling under "the like" in accordance with the procedure applied by that State.
Article 13
Consultation and review
1. The Contracting Parties shall consult each other at least every three years or at the request of either Contracting Party with a view to examining, and — if they consider it necessary — improving the technical functioning of this Agreement and assessing international developments. Consultations shall be held within one month of the request or as soon as possible in urgent cases.
On the basis of such an assessment, the Contracting Parties may consult each other in order to examine whether changes to the Agreement are necessary in the light of international developments.
2. Once they have acquired sufficient experience of the full implementation of Article 7(1) of the Agreement, the Contracting Parties shall consult each other in order to examine whether changes to this Agreement are necessary in the light of international developments.
3. For the purposes of the consultations referred to in paragraphs 1 and 2, the Contracting Parties shall inform each other of any developments which could affect the proper functioning of this Agreement. This shall also include any relevant agreement between one of the Contracting Parties and a third country.
4. In the event of disagreement between the competent authorities of the Principality of Andorra and one or more of the other competent authorities of the Member States of the European Community in accordance with Article 5 of this Agreement on the interpretation or application of the Agreement, they shall endeavour to resolve their differences amicably. They shall immediately notify the Commission of the European Communities and the competent authorities of the other Member States of the European Community of the results of their consultations. The Commission of the European Communities may take part in the consultations at the request of any of the competent authority on issues of interpretation.
Article 14
Application
1. Application of this Agreement is conditional on the adoption and implementation by the dependent or associated territories of Member States mentioned in the report from the Council (Economic and Financial Affairs) to the European Council of Santa Maria de Feira of 19 and 20 June 2000, and by the United States of America, Monaco, Liechtenstein, Switzerland and San Marino of measures which are respectively identical or equivalent to those laid down in the Directive or in this Agreement and providing for the same dates of implementation.
2. The Contracting Parties shall decide by mutual consent, at least six months before the date referred to in paragraph 6, whether the condition set out in paragraph 1 is satisfied as regards the dates of entry into force of the relevant measures in third countries and the dependent or associated territories concerned. If the Contracting Parties do not decide that this condition is met, they shall fix by mutual consent a new date for the purposes of paragraph 6.
3. Notwithstanding its institutional arrangements, the Principality of Andorra shall implement this Agreement from the date referred to in paragraph 6 and shall notify it to the European Community.
4. Implementation of this Agreement or parts thereof may be suspended by either Contracting Party with immediate effect by means of notification addressed to the other party where the Directive or a part thereof ceases to be applicable either temporarily or permanently in accordance with European Community law, or where one of the Member States of the European Community suspends the application of its implementing legislation.
5. Each Contracting Party may also suspend implementation of this Agreement by notifying the other if one of the five third countries referred to above (United States of America, Monaco, Liechtenstein, Switzerland or San Marino) or one of the dependent or associated territories of the Member States of the European Community referred to in paragraph 1 subsequently cease applying measures identical or equivalent to those of the directive. Suspension of implementation shall not come into effect until two months after notification. The Agreement shall begin applying again once measures have been reincorporated.
6. The Contracting Parties shall adopt the laws, regulations and administrative provisions necessary to comply with this Agreement by 1 July 2005 at the latest.
Article 15
Signing, entry into force and termination
1. This Agreement shall be ratified or approved by the Contracting Parties in accordance with their internal procedures. The Contracting Parties shall notify each other when these procedures have been completed. This Agreement shall enter into force on the first day of the second month following the last notification.
2. Either Contracting Party may terminate this Agreement by giving notice addressed to the other. In such a case, the Agreement shall cease to have effect twelve months after the serving of said notice.
Article 16
Claims and final account
1. Termination or the total or partial suspension of this Agreement shall not affect claims by individuals.
2. In such a case, the Principality of Andorra shall draw up a final account before this Agreement ceases to apply and make a final payment to the Member States of the European Community.
Article 17
Territorial scope
This Agreement shall apply, on the one hand, to the territories where the Treaty establishing the European Community applies, and under the conditions provided in the said Treaty, and on the other hand, to the territory of the Principality of Andorra.
Article 18
Annexes
1. The two Annexes shall form an integral part of this Agreement.
2. The list of competent authorities featured in Annex I may be modified by a simple notification to the other Contracting Party by the Principality of Andorra in so far as it concerns the authority identified in point (a) of the said Annex, and by the European Community insofar as it concerns the other authorities.
The list of related entities featured in Annex II may be amended by common accord.
Article 19
Languages
1. This Agreement is drafted in duplicate in the following languages: Catalan, Czech, Danish, Dutch, English, Estonian, Finnish, French, German, Greek, Hungarian, Italian, Latvian, Lithuanian, Polish, Portuguese, Slovak, Slovenian, Spanish and Swedish; the texts in each language being equally authentic.
2. The Maltese language version shall be authenticated by the Contracting Parties on the basis of an Exchange of Letters. They shall also be authentic, in the same way as for the languages referred to in paragraph 1.
IN WITNESS WHEREOF, the undersigned plenipotentiaries, have signed the present Agreement.
Hecho en Bruselas, el quince de noviembre del dos mil cuatro.
V Bruselu dne patnáctého listopadu dva tisíce čtyři.
Udfærdiget i Bruxelles den femtende november to tusind og fire.
Geschehen zu Brüssel am fünfzehnten November zweitausendundvier.
Kahe tuhande neljanda aasta novembrikuu viieteistkümnendal päeval Brüsselis.
Έγινε στις Βρυξέλλες, στις δέκα πέντε Νοεμβρίου δύο χιλιάδες τέσσερα.
Done at Brussels on the fifteenth day of November in the year two thousand and four.
Fait à Bruxelles, le quinze novembre deux mille quatre.
Fatto a Bruxelles, addì quindici novembre duemilaquattro.
Briselē, divi tūkstoši ceturtā gada piecpadsmitajā novembrī.
Pasirašyta du tūkstančiai ketvirtų metų lapkričio penkioliktą dieną Briuselyje.
Kelt Brüsszelben, a kétezer-negyedik év november havának tizenötödik napján.
Magħmul fi Brussel fil-ħmistax il-jum ta' Novembru tas-sena elfejn u erbgħa.
Gedaan te Brussel, de vijftiende november tweeduizendvier.
Sporządzono w Brukseli w dniu piętnastego października roku dwutysięcznego czwartego.
Feito em Bruxelas, em quinze de Novembro de dois mil e quatro.
V Bruseli pätnásteho novembra dvetisícštyri.
V Bruslju, petnajstega novembra leta dva tisoč štiri.
Tehty Brysselissä viidentenätoista päivänä marraskuuta vuonna kaksituhattaneljä.
Som skedde i Bryssel den femtonde november tjugohundrafyra.
Fet a Brussel les el dia quinze de novembre de l'any dos mil quatre.
Por la Comunidad Europea
Za Evropské společenství
For Det Europæiske Fællesskab
Für die Europäische Gemeinschaft
Euroopa Ühenduse nimel
Για την Ευρωπαϊκή Κοινότητα
For the European Community
Pour la Communauté européenne
Per la Comunità europea
Eiropas Kopienas vārdā
Europos bendrijos vardu
az Európai Közösség részéről
Għall-Komunità Ewropea
Voor de Europese Gemeenschap
W imieniu Wspólnoty Europejskiej
Pela Comunidade Europeia
Za Európske spoločenstvo
za Evropsko skupnost
Euroopan yhteisön puolesta
På Europeiska gemenskapens vägnar
Per la Comunitat Europea
Pel Principat d’Andorra
--------------------------------------------------
ANNEX I
LIST OF COMPETENT AUTHORITIES OF THE CONTRACTING PARTIES
The competent authorities for the purposes of this Agreement are:
(a) in the Principality of Andorra: El Ministre eucarregat de les Finances or an authorised representative; however, for the application of Article 3 the competent authority is el Ministre eucarregat de l'Interior or an authorised representative,
(b) in the Kingdom of Belgium: De Minister van Financiën/Le Ministre des Finances or an authorised representative,
(c) in the Czech Republic: Ministr financí or an authorised representative,
(d) in the Kingdom of Denmark: Skatteministeren, or an authorised representative,
(e) in the Federal Republic of Germany: Der Bundesminister der Finanzen or an authorised representative,
(f) in the Republic of Estonia: Rahandusminister or an authorised representative,
(g) in the Hellenic Republic: Ο Υπουργός Οικονομίας και Οικονομικών or an authorised representative,
(h) in the Kingdom of Spain: El Ministro de Economía y Hacienda or an authorised representative,
(i) in the French Republic: Le Ministre chargé du budget or an authorised representative,
(j) in Ireland: The Revenue Commissioners, or their authorised representative,
(k) in the Italian Republic: Il Capo del Dipartimento per le Politiche Fiscali or an authorised representative,
(l) in the Republic of Cyprus: Υπουργός Οικονομικών, or an authorised representative,
(m) in the Republic of Latvia: Finanšu ministrs or an authorised representative,
(n) in the Republic of Lithuania: Finansų ministras or an authorised representative,
(o) in the Grand Duchy of Luxembourg: Le Ministre des Finances or an authorised representative; however, for the purposes of Article 12, the competent authority shall be the Procureur Général d'Etat luxembourgeois,
(p) in the Republic of Hungary: A pénzügyminiszter or an authorised representative,
(q) in the Republic of Malta: Il-Ministru responsabbli għall-Finanzi or an authorised representative,
(r) in the Kingdom of the Netherlands: De Minister van Financiën or an authorised representative,
(s) in the Republic of Austria: Der Bundesminister für Finanzen or an authorised representative,
(t) in the Republic of Poland: Minister Finansów or an authorised representative,
(u) in the Portuguese Republic: O Ministro das Finanças or an authorised representative,
(v) in the Republic of Slovenia: Minister za financií or an authorised representative,
(w) in the Slovak Republic: Minister financií or an authorised representative,
(x) in the Republic of Finland: Valtiovarainministeriö/Finansministeriet or an authorised representative,
(y) in the Kingdom of Sweden: Chefen för Finansdepartementet or an authorised representative,
(z) in the United Kingdom of Great Britain and Northern Ireland and in the European territories for whose external relations the United Kingdom is responsible: the Commissioners of Inland Revenue or their authorised representative and the competent authority in Gibraltar, which the United Kingdom shall designate in accordance with the Agreed Arrangements relating to the competent authority in Gibraltar with the EU and EC instruments and relevant Treaties, notified to the Member States and institutions of the European Union on 19 April 2000, of which a copy shall be notified to the Principality of Andorra by the Secretary-General of the Council of the European Union, and which shall apply to this Agreement.
--------------------------------------------------
ANNEX II
LIST OF RELATED ENTITIES
For the purposes of Article 11 of this Agreement, the following entities shall be considered as "related entity acting as a public authority or whose role is recognised by an international Treaty":
ENTITIES WITHIN THE EUROPEAN UNION:
Belgium
- Région flamande (Vlaams Gewest)
- Région wallonne
- Région bruxelloise (Brussels Gewest)
- Communauté française
- Communauté flamande (Vlaamse Gemeenschap)
- Communauté germanophone (Deutschsprachige Gemeinschaft)
Spain
- Xunta de Galicia (Government of the Autonomous Community of Galicia)
- Junta de Andalucía (Government of the Autonomous Community of Andalucia)
- Junta de Extremadura (Government of the Autonomous Community of Extremadura)
- Junta de Castilla-La Mancha (Government of the Autonomous Community of Castilla-La-Mancha)
- Junta de Castilla-León (Government of the Autonomous Community of Castilla-León)
- Gobierno Foral de Navarra (Government of the Autonomous Community of Navarra)
- Govern de les Illes Balears (Government of the Autonomous Community of the Balearic Islands)
- Generalitat de Catalunya (Government of the Autonomous Community of Catalunya)
- Generalitat de Valencia (Government of the Autonomous Community of Valencia)
- Diputación General de Aragón (Government of the Autonomous Community of Aragon)
- Gobierno de las Islas Canarias (Government of the Autonomous Community of the Canary Islands)
- Gobierno de Murcia (Government of the Autonomous Community of Murcia)
- Gobierno de Madrid (Government of the Autonomous Community of Madrid)
- Gobierno de la Comunidad Autónoma del País Vasco/Euzkadi (Government of the Autonomous Community of the Basque country)
- Diputación Foral de Guipúzcoa (Provincial Council of Guipúzcoa)
- Diputación Foral de Vizcaya/Bizkaia (Provincial Council of Vizcaya)
- Diputación Foral de Alava (Provincial Council of Alava)
- Ayuntamiento de Madrid (Commune of Madrid)
- Ayuntamiento de Barcelona (Commune of Barcelona)
- Cabildo Insular de Gran Canaria (Council of the Island of Grand Canaria)
- Cabildo Insular de Tenerife (Council of the Island of Tenerife)
- Instituto de Crédito Oficial (State Credit Office)
- Instituto Catalán de Finanzas (Catalan Finance Institute)
- Instituto Valenciano de Finanzas (Finance Institute of Valencia)
Greece
- Оργανισμός Тηλεπικοινωνιών Ελλάδος (Greek Telecommunications Organisation)
- Оργανισμός Σιδηροδρόμων Ελλάδος (Greek Railways Organisation)
- Δημόσια Επιχείρηση Ηλεκτρισμού (Public Electricity Company)
France
- La Caisse d'amortissement de la dette sociale (CADES)
- L'Agence française de développement (AFD)
- Réseau Ferré de France (RFF)
- Caisse Nationale des Autoroutes (CNA)
- Assistance publique Hôpitaux de Paris (APHP)
- Charbonnages de France (CDF)
- Entreprise minière et chimique (EMC)
Italy
- Regions
- Provinces
- Communes
- Cassa Depositi e Prestiti (Savings bank for deposits and loans)
Latvia
- Pašvaldības (local Governments)
Poland
- gminy (communes)
- powiaty (districts)
- województwa (provinces)
- związki gmin (associations of communes)
- związki powiatów (associations of districts)
- związki województw (associations of provinces)
- miasto stołeczne Warszawa (Warsaw capital)
- Agencja Restrukturyzacji i Modernizacji Rolnictwa (Agency for the restructuring and the modernisation of agriculture)
- Agencja Nieruchomości Rolnych (Agency for agricultural property)
Portugal
- Região Autónoma da Madeira (Autonomous Region of Madeira)
- Região Autónoma dos Açores (Autonomous Region of Azores)
- Municipalities
Slovakia
- mestá a obce (municipalities)
- Železnice Slovenskej republiky (Slovak Railway Company)
- Štátny fond cestného hospodárstva (State Road Management Fund)
- Slovenské elektrárne (Slovak Power Plants)
- Vodohospodárska výstavba (Water Economy Building Company)
INTERNATIONAL ENTITIES:
- European Bank for Reconstruction and Development
- European Investment Bank
- Asian Development Bank
- African Development Bank
- World Bank/IBRD/IMF
- International Finance Corporation
- Interamerican Development Bank
- Council of Europe Social Development Fund
- EURATOM
- European Community
- Andean Development Corporation
- Eurofima
- European Coal and Steel Community
- Nordic Investment Bank
- Caribbean Development Bank
The provisions of Article 11 are without prejudice to any international obligations that the Contracting Parties may have entered into in respect of the abovementioned international entities.
ENTITIES IN THIRD COUNTRIES:
Entities satisfying the following criteria:
1. the entity is considered to be a public entity according to national criteria;
2. this public entity is a non-market producer which administers and finances a group of activities, principally non-market goods or services intended for the benefit of the Community, and which is effectively controlled by public administration;
3. this public entity is a large and regular issuer of debt securities;
4. the State concerned is able to guarantee that this public entity will not exercise early redemption in the event of gross-up clauses.
--------------------------------------------------
