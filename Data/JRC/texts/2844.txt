P6_TA(2005)0091
Procedural rights in criminal proceedings *
European Parliament legislative resolution on the proposal for a Council framework decision on certain procedural rights in criminal proceedings throughout the European Union (COM(2004) 0328 — C6-0071/2004 — 2004/0113(CNS))
(Consultation procedure)
The European Parliament,
- having regard to the Commission proposal (COM(2004)0328) [1],
- having regard to Article 31(1)(c) of the EU Treaty,
- having regard to Article 39(1) of the EU Treaty, pursuant to which the Council consulted Parliament (C6-0071/2004),
- having regard to Rules 93 and 51 of its Rules of Procedure,
- having regard to the report of the Committee on Civil Liberties, Justice and Home Affairs and the opinion of the Committee on Legal Affairs (A6-0064/2005),
1. Approves the Commission proposal as amended;
2. Calls on the Commission to alter its proposal accordingly, pursuant to Article 250(2) of the EC Treaty;
3. Calls on the Council to notify Parliament if it intends to depart from the text approved by Parliament;
4. Instructs its President to forward its position to the Council and Commission.
TEXT PROPOSED BY THE COMMISSION | AMENDMENTS BY PARLIAMENT |
Amendment 1
Entire text
| The words "as soon as possible" to be replaced by "without undue delay" (This amendment applies throughout the text.) |
Amendment 2
Recital 5a (new)
| (5a) The rights laid down in the ECHR should be regarded as minimum standards with which Member States should in any event comply, just as they should comply with the caselaw of the European Court on Human Rights. |
Amendment 3
Recital 7
(7) The principle of mutual recognition is based on a high level of confidence between Member States. In order to enhance this confidence, this Framework Decision provides certain safeguards to protect fundamental rights. These safeguards reflect the traditions of the Member States in following the provisions of the ECHR. | (7) The principle of mutual recognition is based on a high level of confidence between Member States. In order to enhance this confidence, this Framework Decision provides certain safeguards to protect fundamental rights. These safeguards reflect the traditions of the Member States in following the provisions of the ECHR and of the Charter of Fundamental Rights of the European Union. |
Amendment 51
Recital 8
(8) The proposed provisions are not intended to affect specific measures in force in national legislations in the context of the fight against certain serious and complex forms of crime in particular terrorism. | (8) The proposed provisions are not intended to affect specific measures in force in national legislations in the context of the fight against certain serious and complex forms of crime in particular terrorism. All measures shall be in conformity with the ECHR and the Charter of Fundamental Rights of the European Union. |
Amendment 5
Recital 10
(10) Five areas have been identified as appropriate ones in which common standards may be applied in the first instance. These are: access to legal representation, access to interpretation and translation, ensuring that persons in need of specific attention because they are unable to follow the proceedings receive it, consular assistance to foreign detainees and notifying suspects and defendants of their rights in writing. | (10) In order to promote mutual trust between Member States, safeguards should be put in place to protect the fundamental rights not only of suspected persons, but also of victims of crime and witnesses to crime. However, the focus of this Framework Decision is the safeguarding of the rights of suspected persons. Five areas have been identified as appropriate ones in which common standards may be applied in the first instance. These are: access to legal representation, access to interpretation and translation, ensuring that persons in need of specific attention because they are unable to follow the proceedings receive it, consular assistance to foreign detainees and notifying suspects and defendants of their rights in writing. |
Amendment 6
Recital 10a (new)
| (10a) This Framework Decision should be evaluated within two years after its entry into force in the light of the experience gained. If appropriate, it should be amended so as to improve the safeguards laid down. |
Amendment 8
Recital 16
(16) The right to consular assistance exists by virtue of Article 36 of the 1963 Vienna Convention on Consular Relations where it is a right conferred on States to have access to their nationals. The provisions of this Framework Decision confer the right on the European citizen rather than the State. They enhance its visibility and therefore its effectiveness. That said, in the longer term, the creation of an area of freedom, security and justice in which trust is reciprocated between Member States should reduce and ultimately abolish the need for consular assistance. | (16) The right to consular assistance exists by virtue of Article 36 of the 1963 Vienna Convention on Consular Relations where it is a right conferred on States to have access to their nationals. The provisions of this Framework Decision confer the right on the European citizen rather than the State. They enhance its visibility and therefore its effectiveness. |
Amendment 9
Recital 17
(17) Notifying suspects and defendants of their basic rights in writing is a measure that improves fairness in proceedings, and goes some way to ensuring that everyone suspected of, or charged with, a criminal offence is aware of their rights. If suspects and defendants are unaware of them, it is more difficult for them to insist upon having the benefit of those rights. Giving suspects written notification of their rights, by way of a simple "Letter of Rights", will remedy this problem. | (17) Notifying suspects and defendants of their basic rights in writing is a measure that improves fairness in proceedings, and goes some way to ensuring that everyone suspected of, or charged with, a criminal offence is aware of their rights. If suspects and defendants are unaware of them, it is more difficult for them to insist upon having the benefit of those rights. Giving suspects written notification of their rights, by way of a simple "Letter of Rights", will remedy this problem. Suspected persons with a visual handicap or reading disabilities should be notified orally of their basic rights. |
Amendment 10
Recital 18
(18) It is necessary to establish a mechanism to assess the effectiveness of this Framework Decision. Member States should therefore gather and record information for the purpose of evaluation and monitoring. The information gathered will be used by the Commission to produce reports that will be made publicly available. This will enhance mutual trust since each Member State will know that other Member States are complying with fair trial rights. | (18) It is necessary to establish a mechanism to assess the effectiveness of this Framework Decision. Member States should therefore gather and record information, including information from NGOs, intergovernmental organisations and the professional bodies of lawyers, interpreters and translators, for the purpose of evaluation and monitoring. The information gathered will be used by the Commission to produce reports that will be made publicly available. This will enhance mutual trust since each Member State will know that other Member States are complying with fair trial rights. |
Amendment 11
Article 1, paragraph 1, subparagraph 2
Such proceedings are referred to hereafter as "criminal proceedings". | Deleted |
Amendment 12
Article 1, paragraph 2
2. The rights will apply to any person suspected of having committed a criminal offence ("a suspected person") from the time when he is informed by the competent authorities of a Member State that he is suspected of having committed a criminal offence until finally judged. | 2. The rights shall apply to any person suspected of having committed a criminal offence ("a suspected person") or, where the suspected person is a legal person, to the legal person's representative, from the time when he is approached by the competent authorities of a Member State until final judgment, including sentencing and the resolution of any appeal. |
Amendment 13
Article 1a (new)
| Article 1a Definitions For the purposes of this Framework Decision, the following definitions shall apply: (a)"legal advice" means:the assistance provided by a lawyer, or a duly qualified person as referred to in Article 4(1), to a suspected person before and during any police questioning in relation to the offence of which that person is suspected;the assistance provided to, and the representation of, a suspected person by a lawyer, or a duly qualified person as referred to in Article 4(1), throughout criminal proceedings;(b)"criminal proceedings" means:(i)proceedings for establishing the guilt or innocence of a suspected person or for sentencing that person;(ii)an appeal from proceedings as referred to in point (i), or(iii)proceedings brought by administrative authorities in respect of acts which are punishable under the law of a Member State, and where the decision may give rise to proceedings before a court having jurisdiction in particular in criminal matters;(c)"persons assimilated to family members" means:persons who, under the law of a Member State, live in a registered or otherwise legalised same-sex partnership with the suspected person,persons who cohabit permanently with the suspected person in a non-marital relationship. |
Amendment 14
Article 1b (new)
| Article 1b Right of defence Before they make statements or as soon as they are subject to measures which restrict their freedom, whichever is sooner, suspected persons shall be entitled to be informed by the authorities of the charges laid against them and of the grounds for suspicion. |
Amendment 15
Article 2
1. A suspected person has the right to legal advice as soon as possible and throughout the criminal proceedings if he wishes to receive it. | 1. A suspected person shall have the right to legal advice without undue delay (within 24 hours after arrest). |
2. A suspected person has the right to receive legal advice before answering questions in relation to the charge. | 2. A suspected person shall have the right to receive legal advice in all cases before any questioning takes place, at any stage and level of the criminal proceedings and during any kind of questioning. |
Amendment 16
Article 2, paragraph 2a (new)
| 2a. Suspected persons shall have the right to: consult their lawyer in private (even if they are required for security reasons to be kept in police custody), with their conversation with the lawyer to remain entirely confidential;have access to all material relating to the criminal proceedings, including through the intermediary of their lawyer;ensure that their lawyer is informed regarding the progress of the criminal proceedings and that he is present during questioning;ensure that their lawyer is present and that he puts questions to the court, either during the pre-trial stage or during the trial itself. |
Amendment 17
Article 2, paragraph 2b (new)
| 2b. Failure to respect the right to legal advice shall invalidate all subsequent acts and those dependent on them throughout the criminal proceedings. |
Amendment 18
Article 2, paragraph 2c (new)
| 2c. Member States shall ensure that the lawyer has access to the entire case-file within good enough time to be able to prepare the defence. |
Amendment 19
Article 3, introductory part
Notwithstanding the right of a suspected person to refuse legal advice or to represent himself in any proceedings, it is required that certain suspected persons be offered legal advice so as to safeguard fairness of proceedings. Accordingly, Member States shall ensure that legal advice is available to any suspected person who: | Notwithstanding the right of a suspected person to refuse legal advice or to represent himself in any proceedings, it is required that suspected persons be offered legal advice so as to safeguard fairness of proceedings. Accordingly, Member States shall ensure that legal advice is available to any suspected person, and notably to anyone who: |
Amendment 20
Article 3, indent 2
—is formally accused of having committed a criminal offence which involves a complex factual or legal situation or which is subject to severe punishment, in particular where in a Member State, there is a mandatory sentence of more than one year's imprisonment for the offence, or | —is formally accused of having committed a criminal offence which involves a complex factual or legal situation or which is subject to severe punishment, in particular where in a Member State, there is a mandatory sentence of imprisonment for the offence, or |
Amendment 21
Article 3, indent 5
—appears not to be able to understand or follow the content or the meaning of the proceedings owing to his age, mental, physical or emotional condition. | —is likely or appears not to be able to understand or follow the content or the meaning of the proceedings owing to his age, mental, physical or emotional condition. |
Amendment 22
Article 3, indent 5a (new)
| —is detained for the purpose of making a statement in criminal proceedings. |
Amendment 23
Article 4, paragraph 1
1. Member States shall ensure that only lawyers as described in Article 1(2)(a) of Directive 98/5/EC are entitled to give legal advice in accordance with this Framework Decision. | 1. Member States shall ensure that lawyers as described in Article 1(2)(a) of Directive 98/5/EC or other persons duly qualified in accordance with applicable national provisions are entitled to give legal advice in accordance with this Framework Decision. |
Amendment 24
Article 4, paragraph 2
2. Member States shall ensure that a mechanism exists to provide a replacement lawyer if the legal advice given is found not to be effective. | 2. Member States shall ensure that an independent body is charged with hearing complaints about the effectiveness of a defence lawyer. If appropriate, that body may provide a replacement lawyer. |
Amendment 25
Article 4, paragraph 2a (new)
| 2a. The procedural time limits laid down in this framework decision shall not start to run until the lawyer has been notified — irrespective of whether the suspected person was notified on an earlier date. |
Amendment 26
Article 5, paragraph 1
1. Where Article 3 applies, the costs of legal advice shall be borne in whole or in part by the Member States if these costs would cause undue financial hardship to the suspected person or his dependents. | 1. Member States shall ensure that free legal advice and the legal costs themselves (be they fees or expenses) are provided to suspected persons, or the costs of legal advice shall be borne in whole or in part by the Member State in which the criminal proceedings take place, if these costs would cause undue financial hardship to the suspected person, his dependents, or the persons responsible for supporting him financially. |
Amendment 27
Article 6, paragraph 1
1. Member States shall ensure that a suspected person who does not understand the language of the proceedings is provided with free interpretation in order to safeguard the fairness of the proceedings. | 1. Member States shall ensure that, where a suspected person does not speak or understand the language of the proceedings, he shall be assisted free of charge by an interpreter at each stage and level of the proceedings and also (if he so requests) when consulting his lawyer. |
Amendment 28
Article 6, paragraph 2
2. Member States shall ensure that, where necessary, a suspected person receives free interpretation of legal advice received throughout the criminal proceedings. | 2. Member States shall ensure that, where a suspected person does not understand or speak the language of the proceedings, an interpreter is present: at all meetings between the suspected person and his lawyer, if the lawyer or the suspected person considers it necessary;whenever the suspected person is questioned by law enforcement officers in relation to the offence of which he is suspected;whenever the suspected person is required to appear in court in connection with the offence. |
Amendment 29
Article 6, paragraph 3a (new)
| 3a. Interpreters certified by the competent judicial authorities shall be listed in a national register of interpreters. |
Amendment 30
Article 7, paragraph 1
1. Member States shall ensure that a suspected person who does not understand the language of the proceedings is provided with free translations of all relevant documents in order to safeguard the fairness of the proceedings. | 1. Member States shall ensure that a suspected person who does not understand or read the language of the proceedings, or the language in which relevant documents are drafted where they are not in the language of the proceedings, is provided with free translations of all relevant documents in any of the official languages of the European Union or in another language that the suspected person understands, as appropriate, in order to safeguard the fairness of the proceedings. |
Amendment 31
Article 7, paragraph 2
2. The decision regarding which documents need to be translated shall be taken by the competent authorities. The suspected person's lawyer may ask for translation of further documents. | Deleted |
Amendment 32
Article 8, paragraph 1
1. Member States shall ensure that the translators and interpreters employed are sufficiently qualified to provide accurate translation and interpretation. | 1. Member States shall ensure that a national register of sworn translators and interpreters accessible to professional linguists in all the Member States with an equivalent level of qualification throughout the Union is established. Those listed on the register shall be obliged to respect a national or Community code of conduct designed to ensure the impartial and faithful exercise of translation and interpretation work. |
Amendment 33
Article 9
Member States shall ensure that, where proceedings are conducted through an interpreter, an audio or video recording is made in order to ensure quality control. A transcript of the recording shall be provided to any party in the event of a dispute. The transcript may only be used for the purposes of verifying the accuracy of the interpretation. | Member States shall ensure that, where proceedings are conducted through an interpreter, an audio or video recording is made in order to ensure quality control. A transcript of the recording shall be provided to any party in the event of a dispute. |
Amendment 34
Article 10, paragraph 1
1. Member States shall ensure that a suspected person who cannot understand or follow the content or the meaning of the proceedings owing to his age, mental, physical or emotional condition is given specific attention in order to safeguard the fairness of the proceedings. | 1. Member States shall ensure that a suspected person who cannot understand or follow the content or the meaning of the proceedings owing to his age, state of health, physical or mental disability, illiteracy or particular emotional condition is given specific attention in order to safeguard the fairness of the proceedings. |
Amendment 35
Article 10, paragraph 3a (new)
| 3a. Failure to assess and notify the vulnerability of the suspected person shall, if not remedied, invalidate any subsequent action taken in the criminal proceedings. |
Amendment 36
Article 11, paragraph 2
2. Member States shall ensure that medical assistance is provided whenever necessary. | 2. Member States shall ensure that medical and psychological assistance is provided whenever necessary, and if the suspected person or his lawyer considers it necessary. |
Amendment 37
Article 11, paragraph 3
3. Where appropriate, specific attention may include the right to have a third person present during any questioning by police or judicial authorities. | 3. A suspected person entitled to specific attention or his lawyer shall have the right to request a third person to be present during any questioning by police or judicial authorities. |
Amendment 38
Article 12, paragraph 1
1. A suspected person remanded in custody has the right have his family, persons assimilated to his family or his place of employment informed of the detention as soon as possible. | 1. A suspected person remanded in custody or transferred to another place of custody shall have the right to have his family or persons assimilated to his family informed of the detention or transfer without undue delay. |
Amendment 39
Article 12, paragraph 1a (new)
| 1a. A suspected person remanded in custody shall have the right to have his place of employment informed of the detention without undue delay. |
Amendment 40
Article 13, paragraph 2
2. Member States shall ensure that, if a detained suspected person does not wish to have assistance from the consular authorities of his home State, the assistance of a recognised international humanitarian organisation is offered as an alternative. | 2. Member States shall ensure that, if a detained suspected person does not wish to have assistance from the consular authorities of his home State, the assistance of a recognised international humanitarian organisation is offered without undue delay as an alternative. |
Amendment 41
Article 14, paragraph 1
1. Member States shall ensure that all suspected persons are made aware of the procedural rights that are immediately relevant to them by written notification of them. This information shall include, but not be limited to, the rights set out in this Framework Decision. | 1. Member States shall ensure that all suspected persons are made aware of the procedural rights that are immediately relevant to them by written notification of them. This information shall include, but not be limited to, the rights set out in this Framework Decision. The written notification — the Letter of Rights — shall be presented to the suspected person when he is first questioned, whether in the police station or elsewhere. |
Amendment 42
Article 14, paragraph 1, subparagraph 1a (new)
| Member States shall ensure that the Letter of Rights is made accessible on line for ease of access. Member States shall ensure that, where a suspected person has a visual handicap or reading disabilities, the Letter of Rights is read to that person. |
Amendment 44
Article 14, paragraph 3a (new)
| 3a. The Member States shall determine into which other languages the Letter of Rights should be translated, bearing in mind the languages most commonly used on the territory of the Union as a result of third-country citizens immigrating into or residing in the Union. Paragraphs 2 and 3 shall apply. |
Amendment 45
Article 14, paragraph 4
4. Member States shall require that both the law enforcement officer and the suspect, if he is willing, sign the Letter of Rights, as evidence that it has been offered, given and accepted. The Letter of Rights should be produced in duplicate, with one (signed) copy being retained by the law enforcement officer and one (signed) copy being retained by the suspect. A note should be made in the record stating that the Letter of Rights was offered, and whether or not the suspect agreed to sign it. | 4. The investigating authority shall draw up a statement to the effect that the Letter of Rights has been issued to the suspected person. The statement shall indicate the time at which the Letter was issued and, possibly, the persons present. |
Amendment 46
Article 14a (new)
| Article 14a Prohibition of discrimination Member States shall take preventive measures to ensure that all suspected persons, irrespective of their racial or ethnic background, or sexual orientation, receive equal access to legal assistance and equal treatment at each and every stage of the criminal proceedings. |
Amendment 47
Article 15, paragraph 1
1. Member States shall facilitate the collection of the information necessary for evaluation and monitoring of this Framework Decision. | 1. Every year, Member States shall gather, including from NGOs, intergovernmental organisations and the professional bodies of lawyers, interpreters and translators, and forward to the Commission the information necessary for evaluation and monitoring of this Framework Decision. |
Amendment 48
Article 15, paragraph 2
2. Evaluation and monitoring shall be carried out under the supervision of the European Commission which shall co-ordinate reports on the evaluation and monitoring exercise. Such reports may be published. | 2. Evaluation and monitoring shall be carried out yearly under the supervision of the Commission which shall co-ordinate reports on the evaluation and monitoring exercise. Such reports shall be published. |
Amendment 49
Article 16, paragraph 1, introductory part
1. In order that evaluation and monitoring of the provisions of this Framework Decision may be carried out, Member States shall ensure that data such as relevant statistics are kept and made available, inter alia, as regards the following: | 1. Member States shall take the necessary measures to ensure that, by 31 March each year, the following information in respect of the preceding calendar year is kept and made available: |
Amendment 50
Article 16, paragraph 2
2. Evaluation and monitoring shall be carried out at regular intervals, by analysis of the data provided for that purpose and collected by the Member States in accordance with the provisions of this article. | Deleted |
[1] Not yet published in OJ.
--------------------------------------------------
