Order of the President of the Court of 19 September 2006 — Commission of the European Communities
v Grand Duchy of Luxembourg
(Case C-113/06) [1]
(2006/C 294/75)
Language of the case: French
The President of the Court has ordered that the case be removed from the register.
[1] OJ C 86, 08.04.2006.
--------------------------------------------------
