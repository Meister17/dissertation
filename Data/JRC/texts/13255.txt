Judgment of the Court of First Instance of 8 December 2005 — Rounis
v Commission
(Case T-274/04) [1]
Parties
Applicant: Georgios Rounis (Brussels, Belgium) (represented by: E. Boigelot, lawyer)
Defendant: Commission of the European Communities (represented by: G. Berscheid and M. Velardo, Agents)
Application for
Firstly, annulment of the appeal assessor's decision to confirm the applicant's staff reports for the periods 1997/1999 and 1999/2001 and, secondly, damages.
Operative part of the judgment
The Court:
1. Declares that there is no longer any need to adjudicate on the claims for annulment;
2. Orders the Commission to pay the applicant a sum of EUR 3500;
3. Dismisses the remainder of the action;
4. Orders the Commission to bear its own costs and to pay two thirds of those incurred by the applicant.
[1] OJ C 262, 21.10.2004.
--------------------------------------------------
