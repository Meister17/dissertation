Agreement
in the form of an Exchange of Letters between the European Community and the Republic of Croatia concerning the system of ecopoints to be applied to Croatian transit traffic through Austria as from 1 January 2003
A. Letter from the European Community
Sir,
I have the honour to inform you that, following negotiations between the delegation of the Republic of Croatia and the delegation of the European Community, in accordance with the provisions of Article 2(2)(b) of the Protocol 6 to the Interim Agreement between the European Community and the Republic of Croatia, the following has been agreed:
1. Ecopoints (rights of transit) for Croatian heavy goods vehicles transiting through Austria allocated for 2003: 171904 ecopoints.
Additional ecopoints allocated for Croatian users of the Rollenden Landstrasse (RoLa) (up to a maximum of 40 % of the total number of ecopoints for 2003): 68762 ecopoints.
Ecopoints for RoLa users shall be allocated to the Croatian authorities, on the basis of ecopoints for two road journeys for every two round trips made on RoLa.
The Austrian companies for combined transport, will regularly provide, each month, information to the Ministry of Maritime Affairs, Transport and Communications of the Republic of Croatia concerning Croatian users of combined transport in transit through Austria.
Transit journeys made in the circumstances listed in Annex A or under ECMT authorisations shall be exempt from the ecopoints system.
2. The driver of a Croatian heavy goods vehicle on the territory of Austria shall carry, and shall make available for inspection at the request of the supervisory authorities, either:
(a) a duly completed standard form or an Austrian certificate confirming payment of the ecopoints for the journey in question, modelled on Annex B, hereinafter referred to as "the ecocard"; or
(b) an electronic device, fitted to the motor vehicle which enables the automatic debiting of ecopoints, hereinafter referred to as "the ecotag"; or
(c) appropriate documentation to demonstrate that an ecopoint-free transit journey, as defined in Annex A or under ECMT authorisation is being made; or
(d) appropriate documentation to demonstrate that a non-transit journey is being made, and when the vehicle is fitted with an ecotag, the ecotag is set for this purpose.
The competent Austrian authorities shall issue the ecocard against payment of the cost of production and distribution of ecopoints and ecocards.
3. Ecotags shall be manufactured, programmed and installed in accordance with the general technical specifications laid down in Annex C. The Ministry of Maritime Affairs, Transport and Communications of the Republic of Croatia is authorised to approve, programme and install the ecotags.
The ecotag shall be programmed to contain information on the country of registration and the NOx value of the motor vehicle, as stated in the conformity of production (COP) document, as defined in point 4.
The ecotag shall be affixed to the windscreen of the motor vehicle. It shall be positioned in accordance with Annex D. It shall be non-transferable.
4. The driver of a Croatian heavy goods vehicle registered on or after 1 October 1990 shall also carry, and produce upon request, a COP document, modelled on Annex E, as evidence of the NOx emissions of that vehicle. Heavy goods vehicles first registered before 1 October 1990 or in respect of which no document is produced shall be assumed to have a COP value of 15,8 g/kWh.
5. The Ministry of Maritime Affairs, Transport and Communications of the Republic of Croatia is authorised to issue the documents and ecotags referred to in points 2 to 4.
6. Unless a vehicle is using an ecotag, the requisite number of ecopoints shall be affixed to the ecocard and cancelled. The ecocard shall be cancelled by any one of the following methods:
(a) stamping the ecocard in an ecocard stamping machine;
(b) having the ecocard stamped by the Austrian border control authorities upon entering Austria;
(c) having the ecocard stamped and dated by the national authorities of the haulier before entering Austrian territory;
(d) having the ecocard stamped at an office where the initialisation of ecotags is performed.
Austrian frontier stations that are equipped with an ecocard stamping machine are listed in Annex F.
For statistical purposes page 1 of the completed ecocard shall either be collected by the Austrian authorities or subsequently returned by the competent authorities within three months of the completion of the journey to the Austrian authorities. The statistics thus collected shall assist the Commission in making any proposal for the distribution of the reserve ecopoints.
If the vehicle is fitted with an ecotag, upon confirmation of its undertaking a transit journey requiring ecopoints, a number of ecopoints, equivalent to the NOx emission information stored in the ecotag of the vehicle, shall be deducted from the total of ecopoints allocated to the Republic of Croatia. This shall be done by infrastructure provided and operated by the Austrian authorities.
For vehicles fitted with ecotags that are making bilateral journeys they must set the ecotag to demonstrate that a non-transit journey is being made prior to entering Austrian territory.
In the case where an ecocard is used and where a tractor unit is switched during a transit journey, the proof of payment on entry shall remain valid and be retained. Where the COP value of the new tractor unit exceeds that indicated on the form, additional ecopoints, affixed to a new card, shall be cancelled on leaving the country.
7. Continuous journeys which involve crossing the Austrian frontier once by train, whether by conventional rail transport or in a combined transport operation, and crossing the frontier by road before or after crossing by rail, shall be regarded not as transit of goods by road through Austria but as bilateral journeys.
Continuous transit journeys through Austria using the following rail terminals shall be deemed as bilateral journeys:
Fürnitz, Villach Süd, Sillian, Innsbruck/Hall, Brennersee, Graz.
8. Ecopoints shall be valid between 1 January of the year for which they are attributed and 31 January of the following year.
9. Infringements of this Agreement by a driver of a Croatian heavy goods vehicle or an undertaking shall be prosecuted in accordance with the national legislation in force.
The Commission and the competent authorities of Austria and Croatia shall, each within the limits of their jurisdiction, provide each other with administrative assistance in investigating and prosecuting these infringements, in particular by ensuring that ecocards and ecotags are correctly used and handled.
Controls may be carried out at a point other than the internal border, at the discretion of the European Community Member State, with due regard to the principle of non-discrimination.
10. The Austrian supervisory authorities may, having due regard to the principles of proportionality, take appropriate measures if a vehicle is fitted with an ecotag and at least one of the following situations occur:
(a) the vehicle or the operator of the vehicle has repeatedly committed infringements;
(b) there are insufficient ecopoints remaining in the allocation of Croatia;
(c) the ecotag has been tampered with or has been changed by a party other than those authorised in point 3;
(d) Croatia has not allocated sufficient ecopoints for the vehicle to make a transit journey;
(e) the vehicle does not have appropriate documentation in accordance with point 2(c) or (d) to justify why the ecotag has been set to demonstrate that a non-transit journey is being made on Austrian territory;
(f) when the ecotag specified in Annex C is not loaded with sufficient ecopoints to make a transit journey.
The Austrian supervisory authorities may, having due regard to the principle of proportionality, take appropriate measures if a vehicle is not fitted with an ecotag and at least one of the following situations occurs:
(a) an ecocard is not presented to the supervisory authorities in accordance with the provisions of this Agreement;
(b) an ecocard is presented which is incomplete or incorrect, or where the ecopoints are not correctly affixed;
(c) the vehicle does not possess the appropriate documentation to justify that it does not need ecopoints;
(d) the ecocard has not been cancelled in accordance with the procedure laid down in point 6.
11. The printed ecopoints which are intended for affixing to ecocards shall be made available each year before 1 November of the preceding year.
12. In the case of vehicles registered before 1 October 1990 which have had a change of engine since this date, the COP value of the new engine shall apply. In such a case the certificate issued by the appropriate authority shall mention the change of engine and give details of the new COP value for NOx emissions.
13. A transit journey shall be exempt from the payment of ecopoints if the following three conditions are met:
(a) the sole purpose of the journey is to deliver a brand new vehicle, or vehicle combination, from the manufacturers to a destination in another State;
(b) no goods are transported on the journey;
(c) the vehicle or vehicle combination has appropriate international registration papers and export licence plates.
14. A transit journey shall be exempt from the payment of ecopoints if it is the unladen leg of a journey exempt from ecopoints as listed in Annex A and the vehicle carries suitable documentation to demonstrate this. Such suitable documentation shall be either:
(a) a bill of lading; or
(b) a completed ecocard to which no ecopoints have been attached; or
(c) a completed ecocard with ecopoints, which are subsequently reinstated.
15. Any problems arising from the management of this regime of ecopoints shall be submitted to the Community/the Republic of Croatia Interim Subcommittee on Transport provided for in Article 41 of the Interim Agreement which shall assess the situation and recommend appropriate actions. Any measure to be taken shall be implemented immediately, shall be proportional and of non-discriminatory nature.
I should be obliged if you would confirm the agreement of your Government to the contents of this letter.
Please accept, Sir, the assurance of my highest consideration.
On behalf of the Council of the European Union
ANNEX A
Journeys for which no ecopoints are required
1. The occasional transport of goods to and from airports when flights are diverted.
2. The carriage of baggage in trailers attached to passenger vehicles, and the carriage of baggage to and from airports by vehicles of all kinds.
3. The carriage of postal consignments.
4. The carriage of damaged vehicles or vehicles in need of repair.
5. The carriage of waste and sewage.
6. The carriage of animal carcasses for disposal.
7. The carriage of bees and fish spawn.
8. The transport of corpses.
9. The carriage of works of art for exhibition or commercial purposes.
10. The occasional carriage of goods for advertising or educational purposes.
11. The carriage of goods by removal firms possessing the appropriate personnel and equipment.
12. The carriage of equipment, accessories and animals to and from theatrical, musical, cinema, sporting or circus events, exhibitions or fairs, or to and from radio, cinema or television recordings.
13. The carriage of spare parts for ships and aircraft.
14. The empty journey of a goods vehicle sent to replace a vehicle that has broken down in transit and the continuation of the journey by the replacement vehicle using the authorisation issued for the first vehicle.
15. The carriage of emergency medical aid (particularly in the case of natural disasters).
16. The carriage of valuable goods (e.g. precious metals) in special vehicles escorted by the police or another security service.
ANNEX B
>PIC FILE= "L_2003150EN.003702.TIF">
>PIC FILE= "L_2003150EN.003801.TIF">
>PIC FILE= "L_2003150EN.003901.TIF">
>PIC FILE= "L_2003150EN.004001.TIF">
>PIC FILE= "L_2003150EN.004101.TIF">
>PIC FILE= "L_2003150EN.004201.TIF">
ANNEX C
General technical specifications of the ecotag
Short-range communication beacon - vehicle
(Pre)standards and technical reports relevant to DSRC
The following requirements provided by CEN/TC 278 with regard to short-range communication between the vehicles and the roadside infrastructure are to be met:
(a) prENV278/No 62 "DSRC physical layer using microwave at 5,8 GHz";
(b) prENV278/No 64 "DSRC data link layer";
(c) prENV278/No 65 "DSRC application layer".
Type test
The supplier of the ecotag must provide type-test certificates for the appliances from an accredited test institute conforming compliance with all the limit values specified in the current I-ETS 300674.
Operating conditions
The ecotag for the automatic ecopoint system must guarantee the required functionality under the following operating conditions:
- ambient conditions: temperature from - 25 °C to + 70 °C,
- weather conditions: all eventualities,
- traffic: several lanes, moving,
- speed range: from "stop-and-go" to 120 km/h.
The above operating conditions are minimum requirements pending the adoption of (pre)standards relevant to DSRC.
The ecotag may react only to microwave signals specific to its own applications.
Ecotag
Identification
Each ecotag must have a unique identification number. In addition to the number of digits necessary to make it distinguishable, this number must also contain a check sum for integrity verification.
Installation
The ecotag should be designed for installation behind the windscreen of the lorry or traction unit. It shall be installed so as to be completely non-detachable from the vehicle.
Transit declaration
The ecotag must have an input facility for declaring a journey exempt from the payment of ecopoints.
The facility must be clearly visible on the ecotag for control purposes; alternatively, it must be possible to set the ecotag at a defined initial position. At all events, it must be ensured that only the status at the time of entry is taken into account for evaluation in the system.
External marking
Every ecotag must also be clearly identifiable on visual inspection. To this end, the abovementioned unique identification number must be indelibly applied to the surface of the appliance.
A non-detachable, indelible marking to the ecotag in the form of prepared stickers shall be affixed to the ecotag. This marking must show the number of ecopoints for the individual vehicle (5, 6, ... 16).
These special stickers must be tamper-proof; they must have mechanical strength and be light- and temperature-resistant. They must have a high adhesive strength and any attempt to remove them must result in their destruction.
Integrity
The casing must be constructed in such way that any manipulation of the internal components is excluded and any interference can subsequently be detected.
Memory
The ecotag must have sufficient memory capacity for the following data:
- identification number,
- vehicle data,
- COP value,
- transaction data,
- identification of the border post,
- date/time,
- status of journey declaration,
- blocking (blacklisting) information,
- status data,
- manipulation,
- battery status,
- status of latest communication.
There must be a reserve of memory of at least 30 %.
ANNEX D
Installation requirements for eoctag
>PIC FILE= "L_2003150EN.004502.TIF">
the ecotag shall be located on the interior side of the windscreen within the marked area (illustrated above) where the dimensions are as follows:
x= 100 cm
y= 80 cm.
ANNEX E
>PIC FILE= "L_2003150EN.004602.TIF">
ANNEX F
Austrian border stations equipped with an ecocard stamping machine
Achenkirch
Arnoldstein
Braunau
Brennerpass
Ehrwald
Hangendenstein
Hörbranz
Kiefersfelden
Musau
Nauders
Neuhaus
Pinswang
Reit im Winkl
Saalbrücke
Scharnitz
Schleching
Sillian
Springen
Suben
Steinpass
Walserberg
Wegscheid
B. Letter from the Republic of Croatia
Sir,
I have the honour to refer to your letter of ..., in which you inform me of the following:
"I have the honour to inform you that, following negotiations between the delegation of the Republic of Croatia and the delegation of the European Community, in accordance with the provisions of Article 2(2)(b) of the Protocol 6 to the Interim Agreement between the European Community and the Republic of Croatia, the following has been agreed:
1. Ecopoints (rights of transit) for Croatian heavy goods vehicles transiting through Austria allocated for 2003: 171904 ecopoints.
Additional ecopoints allocated for Croatian users of the Rollenden Landstrasse (RoLa) (up to a maximum of 40 % of the total number of ecopoints for 2003): 68762 ecopoints.
Ecopoints for RoLa users shall be allocated to the Croatian authorities, on the basis of ecopoints for two road journeys for every two round trips made on RoLa.
The Austrian companies for combined transport, will regularly provide, each month, information to the Ministry of Maritime Affairs, Transport and Communications of the Republic of Croatia concerning Croatian users of combined transport in transit through Austria.
Transit journeys made in the circumstances listed in Annex A or under ECMT authorisations shall be exempt from the ecopoints system.
2. The driver of a Croatian heavy goods vehicle on the territory of Austria shall carry, and shall make available for inspection at the request of the supervisory authorities, either:
(a) a duly completed standard form or an Austrian certificate confirming payment of the ecopoints for the journey in question, modelled on Annex B, hereinafter referred to as 'the ecocard'; or
(b) an electronic device, fitted to the motor vehicle which enables the automatic debiting of ecopoints, hereinafter referred to as 'the ecotag'; or
(c) appropriate documentation to demonstrate that an ecopoint-free transit journey, as defined in Annex A or under ECMT authorisation is being made; or
(d) appropriate documentation to demonstrate that a non-transit journey is being made, and when the vehicle is fitted with an ecotag, the ecotag is set for this purpose.
The competent Austrian authorities shall issue the ecocard against payment of the cost of production and distribution of ecopoints and ecocards.
3. Ecotags shall be manufactured, programmed and installed in accordance with the general technical specifications laid down in Annex C. The Ministry of Maritime Affairs, Transport and Communications of the Republic of Croatia is authorised to approve, programme and install the ecotags.
The ecotag shall be programmed to contain information on the country of registration and the NOx value of the motor vehicle, as stated in the conformity of production (COP) document, as defined in point 4.
The ecotag shall be affixed to the windscreen of the motor vehicle. It shall be positioned in accordance with Annex D. It shall be non-transferable.
4. The driver of a Croatian heavy goods vehicle registered on or after 1 October 1990 shall also carry, and produce upon request, a COP document, modelled on Annex E, as evidence of the NOx emissions of that vehicle. Heavy goods vehicles first registered before 1 October 1990 or in respect of which no document is produced shall be assumed to have a COP value of 15,8 g/kWh.
5. The Ministry of Maritime Affairs, Transport and Communications of the Republic of Croatia is authorised to issue the documents and ecotags referred to in points 2 to 4.
6. Unless a vehicle is using an ecotag, the requisite number of ecopoints shall be affixed to the ecocard and cancelled. The ecocard shall be cancelled by any one of the following methods:
(a) stamping the ecocard in an ecocard stamping machine;
(b) having the ecocard stamped by the Austrian border control authorities upon entering Austria;
(c) having the ecocard stamped and dated by the national authorities of the haulier before entering Austrian territory;
(d) having the ecocard stamped at an office where the initialisation of ecotags is performed.
Austrian frontier stations that are equipped with an ecocard stamping machine are listed in Annex F.
For statistical purposes page 1 of the completed ecocard shall either be collected by the Austrian authorities or subsequently returned by the competent authorities within three months of the completion of the journey to the Austrian authorities. The statistics thus collected shall assist the Commission in making any proposal for the distribution of the reserve ecopoints.
If the vehicle is fitted with an ecotag, upon confirmation of its undertaking a transit journey requiring ecopoints, a number of ecopoints, equivalent to the NOx emission information stored in the ecotag of the vehicle, shall be deducted from the total of ecopoints allocated to the Republic of Croatia. This shall be done by infrastructure provided and operated by the Austrian authorities.
For vehicles fitted with ecotags that are making bilateral journeys they must set the ecotag to demonstrate that a non-transit journey is being made prior to entering Austrian territory.
In the case where an ecocard is used and where a tractor unit is switched during a transit journey, the proof of payment on entry shall remain valid and be retained. Where the COP value of the new tractor unit exceeds that indicated on the form, additional ecopoints, affixed to a new card, shall be cancelled on leaving the country.
7. Continuous journeys which involve crossing the Austrian frontier once by train, whether by conventional rail transport or in a combined transport operation, and crossing the frontier by road before or after crossing by rail, shall be regarded not as transit of goods by road through Austria but as bilateral journeys.
Continuous transit journeys through Austria using the following rail terminals shall be deemed as bilateral journeys:
Fürnitz, Villach Süd, Sillian, Innsbruck/Hall, Brennersee, Graz.
8. Ecopoints shall be valid between 1 January of the year for which they are attributed and 31 January of the following year.
9. Infringements of this agreement by a driver of a Croatian heavy goods vehicle or an undertaking shall be prosecuted in accordance with the national legislation in force.
The Commission and the competent authorities of Austria and Croatia shall, each within the limits of their jurisdiction, provide each other with administrative assistance in investigating and prosecuting these infringements, in particular by ensuring that ecocards and ecotags are correctly used and handled.
Controls may be carried out at a point other than the internal border, at the discretion of the European Community Member State, with due regard to the principle of non-discrimination.
10. The Austrian supervisory authorities may, having due regard to the principles of proportionality, take appropriate measures if a vehicle is fitted with an ecotag and at least one of the following situations occur:
(a) the vehicle or the operator of the vehicle has repeatedly committed infringements;
(b) there are insufficient ecopoints remaining in the allocation of Croatia;
(c) the ecotag has been tampered with or has been changed by a party other than those authorised in point 3;
(d) Croatia has not allocated sufficient ecopoints for the vehicle to make a transit journey;
(e) the vehicle does not have appropriate documentation in accordance with point 2(c) or (d) to justify why the ecotag has been set to demonstrate that a non-transit journey is being made on Austrian territory;
(f) when the ecotag specified in Annex C is not loaded with sufficient ecopoints to make a transit journey.
The Austrian supervisory authorities may, having due regard to the principle of proportionality, take appropriate measures if a vehicle is not fitted with an ecotag and at least one of the following situations occurs:
(a) an ecocard is not presented to the supervisory authorities in accordance with the provisions of this Agreement;
(b) an ecocard is presented which is incomplete or incorrect, or where the ecopoints are not correctly affixed;
(c) the vehicle does not possess the appropriate documentation to justify that it does not need ecopoints;
(d) the ecocard has not been cancelled in accordance with the procedure laid down in point 6.
11. The printed ecopoints which are intended for affixing to ecocards shall be made available each year before 1 November of the preceding year.
12. In the case of vehicles registered before 1 October 1990 which have had a change of engine since this date, the COP value of the new engine shall apply. In such a case the certificate issued by the appropriate authority shall mention the change of engine and give details of the new COP value for NOx emissions.
13. A transit journey shall be exempt from the payment of ecopoints if the following three conditions are met:
(a) the sole purpose of the journey is to deliver a brand new vehicle, or vehicle combination, from the manufacturers to a destination in another State;
(b) no goods are transported on the journey;
(c) the vehicle or vehicle combination has appropriate international registration papers and export licence plates.
14. A transit journey shall be exempt from the payment of ecopoints if it is the unladen leg of a journey exempt from ecopoints as listed in Annex A and the vehicle carries suitable documentation to demonstrate this. Such suitable documentation shall be either:
(a) a bill of lading; or
(b) a completed ecocard to which no ecopoints have been attached; or
(c) a completed ecocard with ecopoints, which are subsequently reinstated.
15. Any problems arising from the management of this regime of ecopoints shall be submitted to the Community/the Republic of Croatia Interim Subcommittee on Transport provided for in Article 41 of the Interim Agreement which shall assess the situation and recommend appropriate actions. Any measure to be taken shall be implemented immediately, shall be proportional and of non-discriminatory nature.
I should be obliged if you would confirm the agreement of your Government to the contents of this letter."
I have the honour to confirm that my Government is in agreement with the contents of your letter.
Please accept, Sir, the assurance of my highest consideration.
On behalf of the Government of the Republic of Croatia
