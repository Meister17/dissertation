Commission Regulation (EC) No 2260/2004
of 28 December 2004
fixing the reference prices for certain fishery products for the 2005 fishing year
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 104/2000 of 17 December 1999 on the common organisation of the markets in fishery and aquaculture products [1], and in particular Article 29(1) and (5) thereof,
Whereas:
(1) Regulation (EC) No 104/2000 provides that reference prices valid for the Community may be fixed each year, by product category, for products that are the subject of a tariff suspension under Article 28(1). The same holds for products which, by virtue of being either the subject of a binding tariff reduction under the WTO or some other preferential arrangements, must comply with a reference price.
(2) For the products listed in Annex I(A) and (B) to Regulation (EC) No 104/2000, the reference price is the same as the withdrawal price fixed in accordance with Article 20(1) of that Regulation.
(3) The Community withdrawal and selling prices for the products concerned are fixed for the 2005 fishing year by Commission Regulation (EC) No 2258/2004 of the Commission [2].
(4) The reference price for products other than those listed in Annexes I and II to Regulation (EC) No 104/2000 is established on the basis of the weighted average of customs values recorded on the import markets or in the ports of import in the three years immediately preceding the date on which the reference price is fixed.
(5) There is no need to fix reference prices for all the species covered by the criteria laid down in Regulation (EC) No 104/2000, and particularly not for those imported from third countries in insignificant volumes.
(6) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Fishery Products,
HAS ADOPTED THIS REGULATION:
Article 1
The reference prices for the 2005 fishing year of fishery products as provided for in accordance with Article 29 of Regulation (EC) No 104/2000 are set out in the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Union.
It shall apply from 1 January 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 28 December 2004.
For the Commission
Joe Borg
Member of the Commission
--------------------------------------------------
[1] OJ L 17, 21.1.2000, p. 22.
[2] See page 5 of this Official Journal.
--------------------------------------------------
+++++ ANNEX 1 +++++</br>
