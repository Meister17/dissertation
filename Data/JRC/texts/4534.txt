Information concerning the extension of the International Agreement on Tropical Timber, 1994
(2005/C 237/01)
At its 34th session (Panamá, 12- 16 May 2003), the International Tropical Timber Council decided, under Article 46(2) of the International Agreement on Tropical Timber, 1994 [1], to extend the Agreement for a period of three years with effect from 1 January 2004 until 31 December 2006.
[1] OJ L 208, 17.8.1996, p. 1.
--------------------------------------------------
