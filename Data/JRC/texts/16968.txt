Decision of the EEA Joint Committee
No 75/2004
of 8 June 2004
amending Annex XI (Telecommunication services) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex XI to the Agreement was amended by the Agreement on the participation of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Hungary, the Republic of Latvia, the Republic of Lithuania, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic in the European Economic Area signed on 14 October 2003 in Luxembourg [1].
(2) Commission Decision 2003/490/EC of 30 June 2003 pursuant to Directive 95/46/EC of the European Parliament and of the Council on the adequate protection of personal data in Argentina [2] is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
The following point shall be inserted after point 5ef (Commission Decision 2002/16/EC) of Annex XI to the Agreement:
"5eg. 32003 D 0490: Commission Decision 2003/490/EC of 30 June 2003 pursuant to Directive 95/46/EC of the European Parliament and of the Council on the adequate protection of personal data in Argentina (OJ L 168, 5.7.2003, p. 19)."
Article 2
The texts of Decision 2003/490/EC in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 9 June 2004, provided that all the notifications pursuant to Article 103(1) of the Agreement have been made to the EEA Joint Committee [3].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 8 June 2004.
For the EEA Joint Committee
The President
S. Gillespie
--------------------------------------------------
[1] OJ L 130, 29.4.2004, p. 3.
[2] OJ L 168, 5.7.2003, p. 19.
[3] No constitutional requirements indicated.
