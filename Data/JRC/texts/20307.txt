*****
SECOND COUNCIL DIRECTIVE
of 30 December 1983
on the approximation of the laws of the Member States relating to insurance against civil liability in respect of the use of motor vehicles
(84/5/EEC)
THE COUNCIL OF THE EUROPEAN
COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 100 thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas, by Council Directive 72/166/EEC (4), as amended by Directive 72/430/EEC (5), the Council approximated the laws of the Member States relating to insurance against civil liability in respect of the use of motor vehicles and to the enforcement of the obligation to insure against such liability;
Whereas Article 3 of Directive 72/166/EEC requires each Member State to take all appropriate measures to ensure that civil liability in respect of the use of vehicles normally based in its territory is covered by insurance; whereas the extent of the liability covered and the terms and conditions of the insurance cover are to be determined on the basis of those measures;
Whereas, however, major disparities continue to exist between the laws of the different Member States concerning the extent of this obligation of insurance cover; whereas these disparities have a direct effect upon the establishment and the operation of the common market;
Whereas, in particular, the extension of the obligation of insurance cover to include liability incurred in respect of damage to property is justified;
Whereas the amounts in respect of which insurance is compulsory must in any event guarantee victims adequate compensation irrespective of the Member State in which the accident occurred;
Whereas it is necessary to make provision for a body to guarantee that the victim will not remain without compensation where the vehicle which caused the accident is uninsured or unidentified; whereas it is important, without amending the provisions applied by the Member States with regard to the subsidiary or non-subsidiary nature of the compensation paid by that body and to the rules applicable with regard to subrogation, to provide that the victim of such an accident should be able to apply directly to that body as a first point of contact; whereas, however, Member States should be given the possibility of applying certain limited exclusions as regards the payment of compensation by that body and of providing that compensation for damage to property caused by an unidentified vehicle may be limited or excluded in view of the danger of fraud;
Whereas it is in the interest of victims that the effects of certain exclusion clauses be limited to the relationship between the insurer and the person responsible
for the accident; whereas, however, in the case of vehicles stolen or obtained by violence, Member States may specify that compensation will be payable by the abovementioned body;
Whereas in order to alleviate the financial burden on that body, Member States may make provision for the application of certain excesses where the body provides compensation for damage to property caused by uninsured vehicles or, where appropriate, vehicles stolen or obtained by violence;
Whereas the members of the family of the insured person, driver or any other person liable should be afforded protection comparable to that of other third parties, in any event in respect of their personal injuries;
Whereas the abolition of checks on insurance is conditional on the granting by the national insurers' bureau of the host country of a guarantee of compensation for damage caused by vehicles normally based in another Member State; whereas the most convenient criterion for determining whether a vehicle is normally based in a given Member State is the bearing of a registration plate of the State; whereas the first indent of Article 1 (4) of Directive 72/166/EEC should therefore be amended to that effect;
Whereas, in view of the situation in certain Member States at the outset as regards on the one hand the minimum amounts, and on the other hand the cover and the excesses applicable by the abovementioned body in respect of damage to property, provision should be made for transitional measures concerning the gradual implementation in those Member States of the provisions of the Directive concerning minimum amounts and compensation for damage to property by that body,
HAS ADOPTED THIS DIRECTIVE:
Article 1
1. The insurance referred to in Article 3 (1) of Directive 72/166/EEC shall cover compulsorily both damage to property and personal injuries.
2. Without prejudice to any higher guarantees which Member States may lay down, each Member State shall require that the amounts for which such insurance is compulsory are at least:
- in the case of personal injury, 350 000 ECU where there is only one victim; where more than one victim is involved in a single claim, this amount shall be multiplied by the number of victims,
- in the case of damage to property 100 000 ECU per claim, whatever the number of victims.
Member States may, in place of the above minimum amounts, provide for a minimum amount of 500 000 ECU for personal injury where more than one victim is involved in a single claim or, in the case of personal injury and damage to property, a minimum overall amount of 600 000 ECU per claim whatever the number of victims or the nature of the damage.
3. For the purposes of this Directive, 'ECU' means the unit of account as defined in Article 1 of Regulation (EEC) No 3180/78 (1). The conversion value in national currency to be adopted for successive four-year periods from 1 January of the first year of each period shall be that obtaining on the last day of the preceding September for which ECU conversion values are available in all the Community currencies. The first period shall begin on 1 January 1984.
4. Each Member State shall set up or authorize a body with the task of providing compensation, at least up to the limits of the insurance obligation for damage to property or personal injuries caused by an unidentified vehicle or a vehicle for which the insurance obligation provided for in paragraph 1 has not been satisfied. This provision shall be without prejudice to the right of the Member States to regard compensation by that body as subsidiary or non-subsidiary and the right to make provision for the settlement of claims between that body and the person or persons responsible for the accident and other insurers or social security bodies required to compensate the victim in respect of the same accident.
The victim may in any case apply directly to the body which, on the basis of information provided at its request by the victim, shall be obliged to give him a reasoned reply regarding the payment of any compensation.
However, Member States may exclude the payment of compensation by that body in respect of persons who voluntarily entered the vehicle which caused the damage or injury when the body can prove that they knew it was uninsured.
Member States may limit or exclude the payment of compensation by that body in the event of damage to property by an unidentified vehicle.
They may also authorize, in the case of damage to property caused by an uninsured vehicle an excess of not more than 500 ECU for which the victim may be responsible.
Furthermore, each Member State shall apply its laws, regulations and administrative provisions to the payment of compensation by this body, without prejudice to any other practice which is more favourable to the victim.
Article 2
1. Each Member State shall take the necessary measures to ensure that any statutory provision or any contractual clause contained in an insurance policy issued in accordance with Article 3 (1) of Directive 72/166/EEC, which excludes from insurance the use or driving of vehicles by:
- persons who do not have express or implied authorization thereto, or
- persons who do not hold a licence permitting them to drive the vehicle concerned, or
- persons who are in breach of the statutory technical requirements concerning the condition and safety of the vehicle concerned,
shall, for the purposes of Article 3 (1) of Directive 72/166/EEC, be deemed to be void in respect of claims by third parties who have been victims of an accident.
However the provision or clause referred to in the first indent may be invoked against persons who voluntarily entered the vehicle which caused the damage or injury, when the insurer can prove that they knew the vehicle was stolen.
Member States shall have the option - in the case of accidents occurring on their territory - of not applying the provision in the first subparagraph if and in so far as the victim may obtain compensation for the damage suffered from a social security body.
2. In the case of vehicles stolen or obtained by violence, Member States may lay down that the body specified in Article 1 (4) will pay compensation instead of the insurer under the conditions set out in paragraph 1 of this Article; where the vehicle is normally based in another Member State, that body can make no claim against any body in that Member State.
The Member States which, in the case of vehicles stolen or obtained by violence, provide that the body referred to in Article 1 (4) shall pay compensation, may fix in respect of damage to property an excess of not more than 250 ECU for which the victim may be responsible.
Article 3
The members of the family of the insured person, driver or any other person who is liable under civil law in the event of an accident, and whose liability is covered by the insurance referred to in Article 1 (1) shall not be excluded from insurance in respect of their personal injuries by virtue of that relationship.
Article 4
The first indent of Article 1 (4) of Directive 72/166/EEC shall be replaced by the following:
'- the territory of the State of which the vehicle bears a registration plate, or'.
Article 5
1. Member States shall amend their national provisions to comply with this Directive not later than 31 December 1987. They shall forthwith inform the Commission thereof.
2. The provisions thus amended shall be applied not later than 31 December 1988.
3. Notwithstanding paragraph 2:
(a) the Hellenic Republic shall have a period until 31 December 1995 in which to increase guarantees to the levels required by Article 1 (2). If it avails itself of this option the guarantee must reach, by reference to the amounts laid down in that Article:
- more than 16 % not later than 31 December 1988,
- 31 % not later than 31 December 1992;
(b) the other Member States shall have a period until 31 December 1990 in which to increase guarantees to the levels required by Article 1 (2). Member States which avail themselves of this option must, by the date indicated in paragraph 1, increase guarantees by at least half the difference between the guarantees in force on 1 January 1984 and the amounts laid down in Article 1 (2).
4. Notwithstanding paragraph 2:
(a) the Italian Republic may provide that the excess laid down in the fifth subparagraph of Article 1 (4) shall be 1 000 ECU until 31 December 1990;
(b) the Hellenic Republic and Ireland may provide that:
- compensation by the body referred to in Article 1 (4) for damage to property shall be excluded until 31 December 1992,
- the excess referred to in the fifth subparagraph of Article 1 (4) and the excess referred to in the second subparagraph of Article 2 (2) shall be 1 500 ECU until 31 December 1995.
Article 6
1. Not later than 31 December 1989 the Commission shall present to the Council a report on the situation in the Member States benefiting from the transi tional measures provided for in Article 5 (3) (a) and (4) (b) and shall, where appropriate, submit proposals to review these measures in the light of developments.
2. Not later than 31 December 1993 the Commission shall present to the Council a progress report on the implementation of this Directive and shall, where appropriate, submit proposals in particular as regards adjustment of the amounts laid down in Article 1 (2) and (4).
Article 7
This Directive is addressed to the Member States.
Done at Brussels, 30 December 1983.
For the Council
The President
G. VARFIS
(1) OJ No C 214, 21. 8. 1980, p. 9 and OJ No C 78, 30. 3. 1982, p. 17,
(2) OJ No C 287, 9. 11. 1981, p. 44.
(3) OJ No C 138, 9. 6. 1981, p. 15.
(4) OJ No L 103, 2. 5. 1972, p. 2.
(5) OJ No L 291, 28. 12. 1972, p. 162.
(1) OJ No L 379, 30. 12. 1978, p. 1.
