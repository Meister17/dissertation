*****
COUNCIL DECISION
of 7 March 1985
accepting, on behalf of the Community, the recommendation of the Customs Cooperation Council of 15 June 1983 concerning action against customs fraud relating to containers
(85/187/EEC)
THE COUNCIL OF THE EUROPEAN
COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to the recommendation from the Commission,
Whereas the recommendation of the Customs Cooperation Council of 15 June 1983 concerning action against customs fraud relating to containers can be accepted by the Community with immediate effect,
HAS DECIDED AS FOLLOWS:
Article 1
The recommendation of the Customs Cooperation Council of 15 June 1983 concerning action against customs fraud relating to containers is hereby accepted on behalf of the Community with immediate effect. The Community shall apply this recommendation in respect of its external frontiers in so far as this is not contrary to Community legislation.
The text of the said recommendation is attached to this Decision.
Article 2
The President of the Council is hereby authorized to designate the person empowered to notify the Secretary-General of the Customs Cooperation Council of the acceptance by the Community, with immediate effect, of the recommendation referred to in Article 1.
Done at Brussels, 7 March 1985.
For the Council
The President
A. BIONDI
RECOMMENDATION OF THE CUSTOMS COOPERATION COUNCIL
of 15 June 1983
concerning action against customs fraud relating to containers
THE CUSTOMS COOPERATION COUNCIL,
Considering that Customs fraud is prejudicial to the economic and fiscal interests of States and Customs and Economic Unions, and to the legitimate interests of trade,
Noting that Customs fraud relating to containers is giving increasing cause for concern,
Noting that containerization has become one of the most commonly used means of facilitating the carriage of goods,
Noting that containers are being used in illicit traffic in high-duty goods and prohibited or restricted goods, such as arms and ammunition,
Noting also that containers are being used in illicit traffic in nationally and internationally controlled narcotic drugs and psychotropic substances which are an ever-growing danger to human health and society,
Considering that the Customs authorities are responsible for checking goods at importation and exportation to ensure that Customs and related laws and regulations are applied, whilst at the same time endeavouring to facilitate the rapid passage of goods,
Having regard to the international Convention on mutual administrative assistance for the prevention, investigation, and repression of Customs offences (Nairobi, 9 June 1977),
Having regard to the international Convention on the simplification and harmonization of Customs procedures (Kyoto, 18 May 1973),
Having regard to the Customs Convention on Containers, 1972 (Geneva, 2 December 1972),
Having regard to the recommendation of the Customs Cooperation Council on mutual administrative assistance (5 December 1953),
Having regard to the recommendation of the Customs Cooperation Council on Customs sealing systems in connection with the international transport of goods (11 June 1968),
Having regard to the recommendation of the Customs Cooperation Council on the pooling of information concerning Customs fraud (22 May 1975),
RECOMMENDS
that States, whether or not Members of the Council, and Customs or Economic Unions, should:
1. provide for the possibility of examining containers and their contents, to the extent that it is considered necessary, at the places where the goods are packed into or unpacked from the containers or at any other appropriate place designated or approved by the Customs authorities;
2. employ methods for selection of containers for examination which take into account physical, documentary and intelligence factors and random and systematic selection procedures. The basis for selection should be flexible enough to adapt to changes in fraud patterns and the flow of goods.
The number of containers examined should be consistent with adjudged risk and capacity of the authorities concerned to carry out such examination;
3. examine the selected containers and their contents to a degree compatible with the objectives of the search and method of packing used;
4. pay adequate attention to the value of post facto controls of documentation relating to goods carried in containers, particularly those which have not been physically examined;
5. check, if appropriate, in connection with the Customs examination, that containers still comply with the technical conditions of approval;
6. ensure, for the purposes of Customs control, the provision of appropriate levels of security in port installations and container storage areas; 7. promote the highest effective degree of exchange of information between the country of exportation, countries of transit and country of destination, with a view to ensuring a proper control and security of containers and the goods carried; and
conclude, where the need exists, bilateral or multilateral arrangements for the communication of all relevant details in respect of containers carried, including, wherever possible, place of loading, name and address of the carrier, the exporter and the real consignee, list of goods carried in the container, place of unloading, and nature of seals affixed to the container, to achieve the highest degree of effectiveness of control;
8. ensure that Customs officials concerned with the control and examination of containers receive training which takes particular account of the specific nature of the transport and the control of containers;
9. promote the closest possible cooperation between Customs authorities and professional bodies and authorities concerned with container operation,
REQUESTS
States, whether or not members of the Council, and Customs or Economic Unions which accept this recommendation to notify the Secretary-General of their acceptance, and of the date from which they will apply the recommendation and the conditions of its application. The Secretary-General will transmit this information to the Customs administrations of all members. He will also transmit it to any Customs administrations of non-members or any Customs or Economic Unions which have accepted this recommendation.
