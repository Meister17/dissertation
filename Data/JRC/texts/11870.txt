Action brought on 18 July 2006 — Ralf Schräder v Community Plant Variety Office
Parties
Applicant(s): Ralf Schräder (Lüdinghausen, Germany) (represented by: T, Leidereiter, W.-A. Schmidt and I. Memmler, lawyers)
Defendant(s): Community Plant Variety Office (CPVO)
Form of order sought
- Amendment of the decision by the Board of Appeal of the CPVO of 2 May 2006 (Az. 003/2004) so as to allow the applicant's appeal against the defendant's decision No R 446 and to grant plant variety right in respect of plant variety right application SUMCOL 01(No 2001/0905);
- In the alternative, annulment of the decision by the defendant's Board of Appeal of 2 May 2006 (Az. A003/2004) with a direction to the defendant to make a fresh decision on that application in the light of the judgment;
- In the alternative, annulment of the decision of the defendant's Board of Appeal of 2 May 2006 (Az. A003/2004);
- The CPVO to pay the costs.
Pleas in law and main arguments
Community plant variety right at issue: SUMCOL 01 (Variety application No. 2001/0905).
Decision of the Committee: Rejection of the application.
Decision of the Board of Appeal: Dismissal of the appeal.
Grounds: In particular, infringement of Article 62 in conjunction with Article 7 of Council Regulation (EC) No 2100/94 [1] through legal misassessment of the content; according to the applicant, the plant variety in respect of which application was made is eligible for protection because it has the necessary distinctiveness; Infringement of Article 76 of Regulation No 2100/94 on account of insufficient examination of the content, and of Article 75 of that regulation for infringement of the right to a fair hearing.
[1] Council Regulation (EC) No 2100/94 of 27 July 1994 on Community plant variety rights (OJ 1994 L 227, p. 1).
--------------------------------------------------
