Appeal brought on 28 May 2006 by Guido Strack against the order made on 22 March 2006 by the Court of First Instance (First Chamber) in Case T-4/05, Guido Strack v Commission of the European Communities
Parties
Appellant: Guido Strack (represented by: L. Füllkrug, Rechtsanwalt)
Other party to the proceedings: Commission of the European Communities
Form of order sought
- Set aside the order of the Court of First Instance (First Chamber) of 22 March 2006 in Case T-4/05 Guido Strack v Commission of the European Communities [1]
- Annul the decision of 5 February 2004 to close the OLAF investigation number OF/2002/0356 and the final case report (NT/sr D(2003)-AC-19723-01687 of 5 February 2004) on which that decision was based
- Order the Commission of the European Communities to pay the costs
Pleas in law and main arguments
In his appeal the appellant submits that the Court of First Instance made procedural errors and infringed the rule of Community law in Article 3(3) of Council Decision 2004/752/EC, Euratom of 2 November 2004 establishing the European Union Civil Service Tribunal. The Court of First Instance did not have jurisdiction at the time the order was made as it should already have referred Case T-4/05 to the European Union Civil Service Tribunal in December 2005 on the basis of the abovementioned provision.
Furthermore, the appellant submits that the Court of First Instance erred in procedure as it did not substantiate the contested order with reference to a number of independent grounds on which the action was based.
Moreover, the Court of First Instance infringed Community law by wrongly interpreting the concept of "an act adversely affecting him" which is used in Article 90(2) and 90a of the Staff Regulations. It did this by wrongly interpreting the concept itself and not taking into consideration previous case-law, wrongly interpreting the rules in Articles 22a, 22b and 43 of the Staff Regulations, the fundamental right to physical and mental integrity and the principle of effective judicial protection, and by misjudging the nature of claims for damages.
Lastly, the appellant also submits that the Court of First Instance made procedural errors, which are apparent from the case-file due to incorrect findings of fact and an incorrect legal assessment of the facts as regards the OLAF investigations, and also the illogical presentation thereof in the statement of reasons in the order.
[1] OJ C 121, 20.05.2006, p. 12
--------------------------------------------------
