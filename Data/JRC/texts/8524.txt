COUNCIL DECISION of 22 June 1998 on the conclusion of an Agreement on Mutual Recognition between the European Community and the United States of America (1999/78/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 113, in conjunction with Article 228(2), first sentence and (3), first subparagraph, and Article 228(4) thereof,
Having regard to the proposal from the Commission,
Whereas the Agreement on Mutual Recognition between the European Community and the United States of America, signed in London on 18 May 1998, has been negotiated and should be approved;
Whereas certain tasks for implementation have been attributed to the Joint Committee established by the Agreement, and in particular the power to amend certain aspects of the Sectoral Annexes thereto;
Whereas the appropriate internal procedures should be established to ensure the proper functioning of the Agreement, and whereas it is necessary to empower the Commission to make certain technical amendments to the Agreement and to take certain decisions for its implementation,
HAS DECIDED AS FOLLOWS:
Article 1
The Agreement on Mutual Recognition between the European Community and the United States of America, including its Annexes, is hereby approved on behalf of the Community.
The text of the Agreement and the Annexes thereto is attached to this Decision.
Article 2
The President of the Council shall, on behalf of the Community, transmit the letter provided for in Article 21(1) of the Agreement (1).
Article 3
1. The Commission shall represent the Community in the Joint Committee provided for in Article 14 of the Agreement, and in the Joint Sectoral Committees established by the Sectoral Annexes, assisted by the special committee designated by the Council. The Commission shall proceed, after consultation with this special committee, to the appointments, notifications, exchange of information and the requests for verifications referred to in Articles 10(b), 12, 13, and 14(2) of the Agreement and the equivalent provisions of its Sectoral Annexes.
2. The position of the Community with regard to decisions to be taken by the Joint Committee or if appropriate in the Joint Sectoral Committees shall be determined, with regard to amendments of the Sectoral Annexes (Article 14(4)(b), and Articles 7, 8 and 9 of the Agreement and the equivalent provisions of its Sectoral Annexes), and verification of compliance in accordance with Article 7(d) of the Agreement, by the Commission, following consultation of the special committee, referred to in paragraph 1 of this Article.
3. In all other cases the position of the Community in the Joint Committee or Joint Sectoral Committees shall be determined by the Council, acting by qualified majority on a proposal from the Commission. The same procedure shall apply to decisions taken by the Community in the framework of Articles 16 and 21 of the Agreement.
Done at Luxembourg, 22 June 1998.
For the Council
J. BATTLE
The President
(1) The date of entry into force of the Agreement will be published in the Official Journal of the European Communities.
