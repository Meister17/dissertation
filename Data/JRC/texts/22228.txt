COUNCIL DECISION of 27 January 1997 establishing a reciprocal exchange of information and data from networks and individual stations measuring ambient air pollution within the Member States (97/101/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community and in particular Article 130s (1), thereof,
Having regard to the proposal from the Commission (1),
Having regard to the Opinion of the Economic and Social Committee (2),
Acting in accordance with the procedure laid down in Article 189c of the Treaty (3),
(1) Whereas the fifth European Community action programme on the environment (4) provides for the collection of baseline data on the environment and an improvement in their compatibility, comparability and transparency;
(2) Whereas the objectives and tasks of the European Environment Agency are set out in Council Regulation (EEC) No 1210/90 of 7 May 1990 on the establishment of the European Environment Agency and the European Environment Information and Observation Network (5);
(3) Whereas it is necessary to establish a procedure for the exchange of information on air quality in order to help combat pollution and nuisance, with a view to improving the quality of life and environment throughout the Community, through monitoring long-term trends and improvements resulting from national and Community legislation to combat air pollution;
(4) Whereas duplication in transferring information should be avoided, in particular as regards information to be transmitted to the European Environment Agency and the Commission;
(5) Whereas experience built up as the result of exchanges of information pursuant to Council Decision 75/441/EEC of 24 June 1975 establishing a common procedure for the exchange of information between the surveillance and monitoring networks based on data relating to atmospheric pollution caused by certain compounds and suspended particulates (6) and by Council Decision 82/459/EEC of 24 June 1982 establishing a reciprocal exchange of information and data from networks and individual stations measuring air pollution within the Member States (7) makes it possible to establish a more complete and representative exchange of information by increasing the number of pollutants considered and by including networks and individual stations measuring ambient air pollution;
(6) Whereas a distinction should be made between information that must always be transmitted, in particular relating to Council Directive 96/62/EC of 27 September 1996 on ambient air quality assessment and management (8) (hereinafter referred to as 'the Directive on air quality`), and information that must be submitted where it is available;
(7) Whereas the information collected needs to be sufficiently representative to enable pollution levels to be mapped throughout the Community;
(8) Whereas using common criteria for validating and processing the measurement results will increase the compatibility and comparability of the data transmitted,
HAS ADOPTED THIS DECISION:
Article 1
Objectives
1. A reciprocal exchange of information and data from networks and individual stations measuring ambient air pollution, hereinafter referred to as 'reciprocal exchange`, is hereby established. It shall apply to:
- networks and stations, covering detailed information describing the air-pollution monitoring networks and stations operating in the Member States,
- measurements of air quality obtained from stations: the exchange covers data calculated in accordance with points 3 and 4 of Annex I from measurements of air pollution by stations in the Member States.
2. The Commission and the bodies referred to in Article 6 shall be responsible for the operation of the reciprocal exchange. In order to benefit from the experience acquired by the European Environment Agency and within its sphere of competence, the Commission shall call upon the Environmental Agency, inter alia as regards the operation and practical implementation of the information system.
Article 2
Pollutants
1. The reciprocal exchange shall cover the air pollutants listed in Annex I to the Directive on air quality.
2. Within the framework of the reciprocal exchange, the Member States shall also report on air pollutants listed in point 2 of Annex I to the extent that the relevant data are available to the bodies mentioned in Article 6 and are measured continuously by Member States.
Article 3
Stations
The reciprocal exchange, within the meaning of Article 1, covers stations:
- which are used in the framework of the implementation of directives adopted in accordance with Article 4 of the Directive on air quality,
- which, without being covered by the directives referred to in the first indent, will be selected for this purpose amongst existing stations at national level by the Member States in order to estimate local pollution levels for pollutants listed under point 2 of Annex I and regional (so called 'background`) pollution levels for all pollutants listed in Annex I,
- as far as possible, which took part in the reciprocal exchange of information established by Decision 82/459/EEC, provided that they are not covered by the second indent.
Article 4
Required information on networks and stations
1. The information to be communicated to the Commission shall concern the characteristics of the measurement stations, the measurement equipment and the operational procedures followed in those stations and the structure and organization of the network to which they belong. This information shall be transmitted unless it has been made available to the Commission within the existing legislation on air quality. The information required is specified on an indicative basis in Annex II. In accordance with the procedure laid down in Article 7, the Commission shall specify the minimum information which Member States shall transmit.
2. As regards stations referred to in the first indent of Article 3 the reciprocal exchange will apply once the legislation referred to in Article 4 of the Directive on air quality is in force.
3. Six months after the entry into force of this Decision at the latest, the Commission will make available to the Member States the existing data base containing the information already collected by its services on the subject, and software enabling it to be used and updated. The Member States shall correct, amend and/or supplement that information. The updated computer files shall be sent to the Commission during the second year following the entry into force of this Decision and by 1 October at the latest.
This information will be accessible to the public via an information system set up by the European Environment Agency; it may also be supplied by the Agency or by the Member States upon request.
4. In accordance with the procedure laid down in Article 7, the Commission shall specify the technical procedures for the transfer of information, taking into account the provisions of Article 1 (2).
5. Following the first sending of information by the Member States, the Commission shall include the information transmitted in its data base and prepare each year a technical report on the information collected; it shall make available to the Member States the updated 'networks-stations` data base at the latest by 1 July. The Member States shall correct, amend and/or supplement that information. The updated computer files shall be sent to the Commission by 1 October at the latest.
Article 5
Information to be provided on data obtained by stations
1. The Commission shall be provided with the following results:
(a) data as defined in points 3 and 4 of Annex I for those stations referred to in the first indent of Article 3 and selected according to criteria specified in directives adopted in accordance with Article 4 of the Directive on air quality; account shall be taken of the various conditions of air quality in each Member State for the selection of those stations;
(b) at least annual data as defined in point 4 of Annex I for all the other stations referred to in the second indent of Article 3;
(c) data as defined in points 3 and 4 of Annex I for all the stations referred to in the third indent of Article 3.
These data shall be transmitted unless they have been made available to the Commission under the existing legislation on air quality.
2. The Member States shall be responsible for validating the data transmitted or used to calculate the values transmitted in accordance with the general rules set out in Annex III. Any aggregation of data and the calculation of statistics by a Member State shall comply with criteria at least as stringent as those indicated in Annex IV.
3. The Member States shall transmit the results for the calendar year by 1 October of the following year at the latest; the first transfer shall cover the calendar year 1997.
4. As fas as possible, the Member States shall transmit to the Commission the information collected from 1 October 1989 to the date of entry into force of this Decision by the stations which took part in the reciprocal exchange of information established by Decision 82/459/EEC.
5. In accordance with the procedure laid down in Article 7, the Commission shall specify the technical procedures for the transfer of results, taking into account the provisions of Article 1 (2).
6. The Commission shall include the data transmitted in its data base and prepare each year a technical report on the information collected and shall make available to the Member States the updated 'results` data base.
The information will be accessible to the public via an information system set up by the European Environment Agency; it may also be supplied by the Agency upon request.
The information, whether accessible, supplied or included in the report, will be based only on validated data.
7. The Commission shall prepare a general report for the public, summarizing the collected data and outlining the underlying trends in air quality in the European Union.
8. In agreement with the Member States, the Commission shall ensure the transfer to international bodies of selected data needed for the purposes of various international programmes.
Article 6
Each Member State shall designate one or several bodies responsible for the implementation and operation of the reciprocal exchange and shall forthwith inform the Commission thereof.
Article 7
In accordance with the procedure laid down in Article 12 of the Directive on air quality, the Commission shall specify, where appropriate:
- the preparation and updating of procedures concerning the transfer of data and information,
- liaison with the activities undertaken by the European Environment Agency in the field of air pollution,
- amendments to points 2, 3 and 4 of Annex I, Annex II, Annex III and Annex IV,
- how to take into account new concepts of measurement techniques in the reciprocal exchange procedure,
- the extension of the procedure to data and information from third countries.
Article 8
No later than the end of a five-year period following the entry into force of this Decision, the Commission shall submit to the Council a report on its implementation. This report shall be accompanied by any proposal the Commission deems appropriate to amend this Decision.
Article 9
This Decision shall apply with effect from 1 January 1997.
Article 10
This Decision is addressed to the Member States.
Done at Brussels, 27 January 1997.
For the Council
The President
G. ZALM
(1) OJ No C 281, 7. 10. 1994, p. 9.
(2) OJ No C 110, 2. 5. 1995, p. 3.
(3) Opinion of the European Parliament of 16 June 1995 (OJ No C 166, 3. 7. 1995, p. 177), Council Common Position of 26 February 1996 (OJ No C 219, 27. 7. 1996, p. 1) and Decision of the European Parliament of 18 September 1996 (OJ No C 320, 28. 10. 1996, p. 74).
(4) OJ No C 138, 17. 5. 1993, p. 5.
(5) OJ No L 120, 11. 5. 1990, p. 1.
(6) OJ No L 194, 25. 7. 1975, p. 32. Decision repealed by Decision 82/459/EEC (OJ No L 210, 19. 7. 1982, p. 1).
(7) OJ No L 210, 19. 7. 1982, p. 1.
(8) OJ No L 296, 21. 11. 1996, p. 55.
ANNEX I
LIST OF POLLUTANTS, STATISTICAL PARAMETERS AND UNITS OF MEASUREMENT
1. Pollutants listed in Annex I to the Directive on air quality
2. Pollutants not listed in Annex I to the Directive on air quality:
>TABLE>
3. Data, units of measurement and averaging times:
>TABLE>
4. Data, calculated over the calendar year, to be transmitted to the Commission:
- For pollutants 1 to 35:
the arithmetic mean, the median, the percentiles 98 (and 99,9 which may be transmitted on a voluntary basis for pollutants for which the mean is calculated over 1 hour) and the maximum calculated from raw data corresponding to the recommended averaging times indicated in the table above; for pollutant 7 (ozone), the statistical parameters should also be calculated from mean values over 8 hours;
- For pollutants 2, 36 and 37:
the arithmetic mean, calculated from raw data corresponding to the recommended averaging times indicated in the table above.
The xth percentile should be calculated from the values actually measured. All the values should be listed in increasing order:
1
ANNEX II
INFORMATION CONCERNING NETWORKS, STATIONS AND MEASUREMENT TECHNIQUES
To the extent possible, as much information as feasible should be supplied about the following indicative points:
I. INFORMATION CONCERNING NETWORKS
- Name,
- Abbreviation,
- Geographical coverage (local industry, town/city, urban area/conurbation, county, region, entire country),
- Body responsible for network management
- name,
- name of person responsible,
- address,
- telephone and fax numbers,
- Time reference basis (GMT, local)
II. INFORMATION CONCERNING STATIONS
1. General information
- Name,
- Reference number or code,
- Name of technical body responsible for the station (if different from that responsible for the network),
- Type of station
- traffic,
- industrial,
- background,
- Purpose of the station (local, national, EU dir., GEMS, OECD, EMEP, . . .),
- Geographical coordinates,
- Altitude,
- NUTS level III,
- Pollutants measured,
- Meteorological parameters measured,
- other relevant information: prevailing wind direction, ratio between distance from and height of closest obstacles, . . . .
2. Local environment/Landscape morphology
- Type of zone
- urban,
- suburban,
- rural,
- Characterization of zone
- residential,
- commercial,
- industrial,
- agricultural,
- natural,
- Number of inhabitants of the zone.
3. Main sources of emission
- public power, co-generation and district heating,
- commercial, institutional and residential combustion,
- industrial combustion,
- production processes,
- extraction and distribution of fossil fuels,
- solvent use,
- road transport,
- other mobiles sources and machinery (to be specified),
- waste treatment and disposal,
- agriculture,
- nature.
4. Characterization of traffic (only for traffic-orientated stations)
- wide street with
- large volume of traffic (in excess of 10 000 vehicles a day),
- moderate volume of traffic (between 2 000 and 10 000 vehicles a day),
- low volume of traffic (less than 2 000 vehicles a day),
- narrow street with
- large volume of traffic (in excess of 10 000 vehicles a day),
- moderate volume of traffic (between 2 000 and 10 000 vehicles a day),
- low volume of traffic (less than 2 000 vehicles a day),
- canyon street with
- large volume of traffic (in excess of 10 000 vehicles a day),
- moderate volume of traffic (between 2 000 and 10 000 vehicles a day),
- low volume of traffic (less than 2 000 vehicles a day),
- highway
- large volume of traffic (in excess of 10 000 vehicles a day),
- moderate volume of traffic (between 2 000 and 10 000 vehicles a day),
- low volume of traffic (less than 2 000 vehicles a day),
- others: crossroad, signal lights, parking, bus stop, taxi stop . . ..
III. INFORMATION CONCERNING MEASUREMENT TECHNIQUES
- Equipment
- name,
- analytical principle,
- Characteristics of sampling
- location of sampling point (facade of building, pavement, kerbside, courtyard),
- height of sampling point,
- length of sampling line,
- result-integrating time,
- sampling time,
- Calibration
- type: automatic, manual, automatic and manual,
- method,
- frequency.
ANNEX III
DATA VALIDATION PROCEDURE AND QUALITY CODES
1. Validation procedure
The validation procedure should:
- take into consideration, for example, disturbances due to maintenance, calibration or technical problems, off-scale measurements and data indicating rapid variations, such as excessive falls or rises,
The data should also be revised on the basis of criteria based on a knowledge of climatic and meteorological influences specific to the site during the measurement period, and
- enable the detection of erroneous measurements by techniques such as comparison with preceding months and with other pollutants and standard deviation analysis.
The validation list drawn up during the marking of data should also be examined and verified.
2. Quality codes
All transmitted data are deemed to be valid, except when associated with code T or N as defined below:
- Code T: corresponds to data not (or not yet) subjected to the validation procedure as specified in point 1.
- Code N: corresponds to data characterized as erroneous or doubtful, during the validation procedure as specified in point 1.
ANNEX IV
CRITERIA FOR THE AGGREGATION OF DATA AND THE CALCULATION OF STATISTICAL PARAMETERS
(a) Aggregation of data
The criteria for the calculation of one-hour and 24-hour values from data with a smaller averaging time are:
>TABLE>
(b) Calculation of statistical parameters
>TABLE>
The ratio between the number of valid data for the two seasons of the year considered cannot be greater than 2, the two seasons being winter (from January to March inclusive and from October to December inclusive) and summer (from April to September inclusive).
