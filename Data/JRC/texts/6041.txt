Commission Regulation (EC) No 533/2005
of 6 April 2005
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 7 April 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 6 April 2005.
For the Commission
J. M. Silva Rodríguez
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 1947/2002 (OJ L 299, 1.11.2002, p. 17).
--------------------------------------------------
ANNEX
to Commission Regulation of 6 April 2005 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code | Standard import value |
07020000 | 052 | 103,2 |
204 | 57,5 |
212 | 111,3 |
624 | 166,8 |
999 | 109,7 |
07070005 | 052 | 149,8 |
068 | 95,9 |
096 | 39,9 |
204 | 47,5 |
220 | 155,5 |
999 | 97,7 |
07091000 | 220 | 74,5 |
999 | 74,5 |
07099070 | 052 | 102,6 |
204 | 50,6 |
999 | 76,6 |
08051020 | 052 | 52,5 |
204 | 49,7 |
212 | 47,1 |
220 | 34,0 |
400 | 60,3 |
624 | 59,3 |
999 | 50,5 |
08055010 | 052 | 59,1 |
400 | 67,7 |
624 | 63,3 |
999 | 63,4 |
08081080 | 388 | 78,9 |
400 | 115,0 |
404 | 86,1 |
508 | 68,7 |
512 | 75,3 |
524 | 73,3 |
528 | 68,7 |
720 | 72,6 |
804 | 118,8 |
999 | 84,2 |
08082050 | 388 | 71,9 |
512 | 58,6 |
528 | 56,6 |
720 | 39,8 |
999 | 56,7 |
--------------------------------------------------
