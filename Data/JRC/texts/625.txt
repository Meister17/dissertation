Council Decision
of 20 November 2000
amending Decision 1999/70/EC concerning the external auditors of the national central banks
(2000/737/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 122(2) thereof,
Having regard to the Statute of the European System of Central Banks and of the European Central Bank, and in particular to Article 27(1) thereof,
Having regard to the recommendation of the European Central Bank (hereinafter referred to as the ECB) of 5 October 2000,
Whereas:
(1) The accounts of the ECB and of the national central banks are to be audited by independent external auditors recommended by the Governing Council of the ECB and approved by the Council of the European Union.
(2) Pursuant to Decision 2000/427/EC(1), Greece having fulfilled the necessary conditions for the adoption of the single currency on 1 January 2001, the derogation of Greece referred to in recital 4 of Decision 98/317/EC(2) has been abrogated, with effect from 1 January 2001.
(3) Following the abrogation of the derogation of Greece, the Governing Council of the ECB recommended that the Council approve as external auditors for the Bank of Greece for the annual accounts starting from the financial year 2001: Ernst & Young (Hellas) Certified Auditors SA and Mr Charalambos Stathakis, a registered certified public accountant.
(4) It is appropriate to follow the recommendation of the Governing Council and to amend Decision 1999/70/EC(3) accordingly,
HAS DECIDED AS FOLLOWS:
Article 1
The following paragraph shall be added to Article 1 of Decision 1999/70/EC:
"12. Ernst & Young (Hellas) Certified Auditors SA and Mr Charalambos Stathakis, a registered certified public accountant, are hereby approved as the external auditors of the Bank of Greece for the annual accounts starting from the financial year 2001."
Article 2
This Decision shall be notified to the ECB.
Article 3
This Decision shall be published in the Official Journal of the European Communities.
Done at Brussels, 20 November 2000.
For the Council
The President
H. Védrine
(1) OJ L 167, 7.7.2000, p. 19.
(2) OJ L 139, 11.5.1998, p. 30.
(3) OJ L 22, 29.1.1999, p. 69. Decision as amended by Decision 2000/223/EC (OJ L 71, 18.3.2000, p. 24).
