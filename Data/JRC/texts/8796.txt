*****
COUNCIL DIRECTIVE
of 3 May 1988
amending Directive 72/462/EEC on health and veterinary inspection problems upon importation of bovine animals and swine and fresh meat from third countries
(88/289/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 43 thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas Directive 72/462/EEC (4), as last amended by Directive 87/64/EEC (5), lays down health and veterinary inspection requirements for importation of bovine animals and swine and fresh meat from third countries;
Whereas Council Directive 64/433/EEC of 26 June 1964 on health problems affecting intra-Community trade in fresh meat (6) has been amended by Directive 88/288/EEC (7) in relation to trade in offal, as well as in relation to the possibility of adopting other examinations in the framework of ante-mortem and post-mortem health inspection in order to take account of particular local situations; whereas the same health guarantees offered by Directive 64/433/EEC should also apply to imports from third countries,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Directive 72/462/EEC is hereby amended as follows:
1. The second sentence of Article 4 (1) shall be replaced by the following:
'In accordance with the detailed implementing rules to be established by the Commission under the procedure laid down in Article 30, the list or lists may be amended or supplemented by the Commission in line with the result of the inspections provided for in Article 5, of which it has previously informed the Member States.
In the event of difficulties, the matter shall be referred to the Committee in accordance with the procedure laid down in Article 29.
Before 1 January 1990, the Council shall review these provisions on the basis of a Commission report.'
2. The following sentence shall be added to Article 17 (2) (b) and (d):
'In accordance with the procedure laid down in Article 29, additional requirements adapted to the specific situation of countries specified by name with respect to certain diseases likely to endanger human health may be decided on.'
3. In Article 18 (1) (b) the first phrase shall be replaced by:
'(b) cuts smaller than quarters or boned meat or offal or sliced livers of bovine animals from cutting plants . . . (remainder unchanged) . . .'.
4. The following paragraph shall be added to Article 18:
'4. The admission of sliced livers of animals other than of the bovine species may be decided upon by the Council acting by a qualified majority on a proposal from the Commission.'
5. Article 20 (b) shall be replaced by the following:
'(b) fresh meat:
(i) from animals to which hormonal substances prohibited under Directives 81/602/EEC and 88/146/EEC (*) have been administered;
(ii) containing residues of hormonal substances authorized in accordance with the exceptions provided for in Article 4 of Directive 81/602/EEC and Articles 2 and 7 of Directive 85/649/EEC, residues of antibiotics, pesticides, or of other substances . . . (remainder unchanged) . . . . .
(*) OJ No L 70, 16. 3. 1988, p. 16.'
Article 2
Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive not later than 1 January 1989 and shall forthwith inform the Commission thereof.
Article 3
This Directive is addressed to the Member States.
Done at Brussels, 3 May 1988.
For the Council
The President
M. BANGEMANN
(1) OJ No C 276, 1. 11. 1986, p. 11.
(2) OJ No C 156, 15. 6. 1987, p. 190.
(3) OJ No C 68, 16. 3. 1987, p. 2.
(4) OJ No L 302, 31. 12. 1972, p. 28.
(5) OJ No L 34, 5. 2. 1987, p. 52.
(6) OJ No 121, 29. 7. 1964, p. 2012/64.
(7) See page 28 of this Official Journal.
