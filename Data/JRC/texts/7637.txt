*****
COUNCIL DECISION
of 6 May 1986
concerning the accession of the European Economic Community to the Agreement on the temporary importation, free of duty, of medical, surgical and laboratory equipment for use on free loan in hospitals and other medical institutions for purposes of diagnosis or treatment
(86/181/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to the proposal from the Commission,
Whereas Articles 1 and 2 of the Agreement on the temporary importation, free of duty, of medical, surgical and laboratory equipment for use on free loan in hospitals and other medical institutions for purposes of diagnosis and treatment, drawn up on the initiative of the Council of Europe, provide that each Contracting Party shall grant all possible facilities for the duty-free importation on a temporary basis of such equipment made available to it by another Contracting Party;
Whereas any derogation from the Common Customs Tariff, whether autonomous or conventional, falls within the competence of the Community;
Whereas the entry into force of an Additional Protocol enabling the Community to become a Contracting Party to the abovementioned Agreement allows the Community to exercise its power in this matter; whereas the derogations established by that Agreement are already provided for by Community temporary importation rules;
Whereas the Community ought therefore to become a Contracting Party to the Agreement in question,
HAS DECIDED AS FOLLOWS:
Article 1
The Accession of the Community to the Agreement on the temporary importation, free of duty, of medical, surgical and laboratory equipment for use on free loan in hospitals and other medical institutions for purposes of diagnosis or treatment is hereby approved on behalf of the Community.
The text of the Agreement is attached to this Decision.
Article 2
The President of the Council is hereby authorized to designate the persons empowered to sign the Agreement in order to bind the Community.
Done at Brussels, 6 May 1986.
For the Council
The President
P. H. van ZEIL
