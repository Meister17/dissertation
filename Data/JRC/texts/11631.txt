Council Decision
of 27 February 2006
on the conclusion of an Agreement in the form of an Exchange of Letters between the European Community and the Kingdom of Thailand pursuant to Article XXIV:6 and Article XXVIII of the General Agreement on Tariffs and Trade (GATT) 1994 relating to the modification of concessions in the schedules of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic in the course of their accession to the European Union
(2006/324/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 133 in conjunction with the first sentence of the first subparagraph of Article 300(2) thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) On 22 March 2004 the Council authorised the Commission to open negotiations with certain other Members of the WTO under Article XXIV:6 of the General Agreement on Tariffs and Trade (GATT) 1994, in the course of the accessions to the European Union of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic.
(2) Negotiations have been conducted by the Commission in consultation with the Committee established by Article 133 of the Treaty and within the framework of the negotiating directives issued by the Council.
(3) The Commission has finalised negotiations for an Agreement in the form of an Exchange of Letters between the European Community and the Kingdom of Thailand pursuant to Article XXIV:6 and Article XXVIII of the GATT 1994. The said Agreement should therefore be approved.
(4) The measures necessary for the implementation of this Decision should be adopted in accordance with Council Decision 1999/468/EC of 28 June 1999 laying down the procedures for the exercise of implementing powers conferred on the Commission [1],
HAS DECIDED AS FOLLOWS:
Article 1
The Agreement in the form of an Exchange of Letters between the European Community and the Kingdom of Thailand pursuant to Article XXIV:6 and Article XXVIII of the General Agreement on Tariffs and Trade (GATT) 1994 relating to the modification of concessions in the schedules of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic in the course of their accession to the European Union, with respect to the withdrawal of specific concessions in relation to the withdrawal of the schedules of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic in the course of their accession to the European Union, is hereby approved on behalf of the Community.
The text of the Agreement in the form of an Exchange of Letters is attached to this Decision.
Article 2
The Commission shall adopt the detailed rules for implementing the Agreement in the form of an Exchange of Letters in accordance with the procedure referred to in Article 3(2), or in case of tariff rate quotas for tariff lines 160420, Article 4(2) of this Decision.
Article 3
1. The Commission shall be assisted by the Management Committee for Cereals instituted by Article 25 of Council Regulation (EC) No 1784/2003 of 29 September 2003 on the common organisation of the market in cereals [2] or the relevant committee instituted by the corresponding Article of the Regulation on the common market organisation for the product concerned.
2. Where reference is made to this paragraph, Articles 4 and 7 of Decision 1999/468/EC shall apply.
The period provided for in Article 4(3) of Decision 1999/468/EC shall be set at one month.
3. The Committee shall adopt its Rules of Procedure.
Article 4
1. The Commission shall be assisted by the Customs Code Committee instituted by Article 248a of Council Regulation (EEC) No 2913/92 of 12 October 1992 establishing the Community Customs Code [3].
2. Where reference is made to this paragraph, Articles 4 and 7 of Decision 1999/468/EC shall apply.
The period provided for in Article 4(3) of Decision 1999/468/EC shall be set at three months.
3. The Committee shall adopt its Rules of Procedure.
Article 5
The President of the Council is hereby authorised to designate the person(s) empowered to sign the Agreement in order to bind the Community [4].
Done at Brussels, 27 February 2006.
For the Council
The President
U. Plassnik
[1] OJ L 184, 17.7.1999, p. 23.
[2] OJ L 270, 21.10.2003, p. 78. Regulation as amended by Commission (EC) No 1154/2005 (OJ L 187, 19.7.2005, p. 11).
[3] OJ L 302, 19.10.1992, p. 1. Regulation as last amended by Regulation (EC) No 648/2005 of the European Parliament and of the Council (OJ L 117, 4.5.2005, p. 13).
[4] The date of entry into force of the Agreement will be published in the Official Journal of the European Union.
--------------------------------------------------
