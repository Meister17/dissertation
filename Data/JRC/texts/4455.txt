Information communicated by Member States regarding State aid granted under Commission Regulation (EC) No 2204/2002 of 12 December 2002 on the application of Articles 87 and 88 of the EC Treaty to State aid for employment
(2005/C 159/09)
(Text with EEA relevance)
Aid No: XE 04/03
Member State: Italy
Region: Campania
Title of aid scheme: Regional rules on aid for employment.
Legal basis: Deliberazione della Giunta regionale della Campania n. 1448 dell' 11.4.2003
Annual expenditure planned under the scheme:
The scheme will be implemented using funds earmarked for the Campania Objective 1 regional operational programme, measures 1.11, 2.3, 3.4, 3.9, 3.11, 3.12, 3.13, 3.14, 4.4, 5.3 and 6.4; the appropriation for employment aid will be set by the regional executive within the approved bands.
The resources to be drawn from these facilities to finance the scheme amount to EUR 110000000.
Any further appropriations to finance the scheme will be approved by the regional executive and notified to the Commission in the annual report.
Types of aid | Maximum |
Aid for job creation not linked to the carrying-out of a project of investmentAid for job creation linked to the carrying-out of a project of investment | The intensities are those stated in the regional aid map, i.e.:for large enterprises, 35 % net;for SMEs, 35 % net plus 15 % gross.The increase of 15 % gross is available to SMEs only provided that the total net intensity of the aid does not exceed 75 %, that the recipient's own contribution is no less than 25 % of the financing obtained, and that the jobs are maintained in the Region.Where the aid is of one of these types, a further 50 % gross of the gross wage cost for a year may be granted if the net job creation relates to a worker who is disadvantaged within the meaning of Article 2(f) of Regulation (EC) No 2204/2002 or 60 % of the gross wage cost for a year if the worker is disabled within the meaning of Article 2(g) of the same Regulation. |
Aid towards the recruitment of disadvantaged and disabled workers | The maximum intensity of aid as a percentage of the wage cost over the year following recruitment may not exceed 50 % gross in the case of disadvantaged workers and 60 % gross in the case of disabled workers. |
Aid linked to the extra cost of recruiting a disabled worker | All of the extra costs and related expenditure incurred during the period within which the disabled person works for the recipient enterprise in respect of:conversion of premises;the working time spent by the responsible staff solely on the assistance of the disabled worker or workers;the costs of adapting or acquiring equipment for the use of the disabled worker or workers. |
Date of implementation: April 2003
Duration of scheme: Until December 2006
Objective of aid: In line with recital 7 to Regulation (EC) No 2204/2002, with the European employment strategy and with the strategy of the Campania Objective 1 regional operational programme, the objective of this scheme is to encourage job creation, the recruitment of disadvantaged and disabled workers and the employment of disabled workers, without adversely affecting trading conditions to an extent contrary to the common interest.
Economic sector(s) concerned: All firms operating in Campania qualify for the employment aid provided for in these rules, except those operating in transport (though the aid is available in the case of disadvantaged and disabled workers), shipbuilding and ship repair, and coal.
Name and address of granting authority Regione Campania
Area Generale di Coordinamento Istruzione, Educazione, Formazione, Politiche Giovanili e del Forum Regionale,
Ormel - Centro Direzionale
Isola A/6, 80141 — Napoli.
Tel. 081/7966303
Fax 081/7966045
E-mail c.neri@regione.campania.it
Other information: The scheme is cofinanced by the ESF under the Campania regional operational programme for 2000/2006, which was approved by EU decision C(2000) 2347, adopted on 8 August 2000, notified on 12 August 2000, and published in a special issue of the Region of Campania Official Gazette (BURC) in September 2000.
--------------------------------------------------
