Judgment of the Court
(Fourth Chamber)
of 8 September 2005
in Case C-427/04: Commission of the European Communities
v Hellenic Republic [1]
In Case C-427/04 Commission of the European Communities (Agents: W. Wils and G. Zavvos) v Hellenic Republic (Agent: N. Dafniou) — action under Article 226 EC for failure to comply with obligations, brought on 5 October 2004 — the Court (Fourth Chamber), composed of K. Lenaerts, President of the Chamber, M. Ilešič and E. Levits (Rapporteur), Judges; J. Kokott, Advocate General; R. Grass, Registrar, gave a judgment on 8 September 2005, in which it:
1. Declares that, by not adopting all the laws, regulations and administrative provisions necessary to comply with Directive 2001/16/EC of the European Parliament and of the Council of 19 March 2001 on the interoperability of the trans-European conventional rail system, the Hellenic Republic has failed to fulfil its obligations under that directive;
2. Orders the Hellenic Republic to pay the costs.
[1] OJ C 6 of 08.01.2005
--------------------------------------------------
