Commission Decision
of 20 February 2004
amending Decision 1999/815/EC concerning measures prohibiting the placing on the market of toys and childcare articles intended to be placed in the mouth by children under three years of age made of soft PVC containing certain phthalates
(notified under document number C(2004) 524)
(Text with EEA relevance)
(2004/178/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Directive 2001/95/EC of the European Parliament and of the Council of 3 December 2001 on general product safety(1) and in particular Article 13(2) thereof,
Whereas:
(1) On 7 December 1999, the Commission adopted Decision 1999/815/EC(2) based on Article 9 of Directive 92/59/EEC(3), requiring the Member States to prohibit the placing on the market of toys and childcare articles intended to be placed in the mouth by children under three years of age, made of soft PVC containing one or more of the substances di-iso-nonyl phthalate (DINP), di(2-ethylhexyl) phthalate (DEHP), dibutyl phthalate (DBP), di-iso-decyl phthalate (DIDP), di-n-octyl phthalate (DNOP), and butylbenzyl phthalate (BBP).
(2) The validity of Decision 1999/815/EC was limited to three months, in accordance with the provision of Article 11(2) of Directive 92/59/EEC. Therefore, the validity of the Decision was to expire on 8 March 2000.
(3) When adopting Decision 1999/815/EC it was provided for to prolong its validity if necessary. The validity of the measures adopted under Decision 1999/815/EC was prolonged under several decisions for an additional period of three months each time, and is now to expire on 20 February 2004.
(4) Some relevant developments have taken place concerning the validation of phthalates migration test methods and the comprehensive risk assessment of these phthalates under Council Regulation (EEC) No 793/93 of 23 March 1993 on the evaluation and control of the risks of existing substances(4). The Parliament and the Council are considering permanent measures to deal with the risks posed by the products in question, but more time is needed to complete deliberations on the subject, in particular in order to take into account all the new scientific developments.
(5) Pending resolution of the outstanding issues, and in order to guarantee the objectives of Decision 1999/815/EC and its prolongations, it is necessary to maintain the prohibition of the placing on the market of the products considered.
(6) Certain Member States have implemented Decision 1999/815/EC by measures applicable until 20 February 2004. Therefore it is necessary to ensure that the validity of these measures is prolonged.
(7) It is therefore necessary to prolong the validity of Decision 1999/815/EC in order to ensure that all the Member States maintain the prohibition provided for by that Decision.
(8) Directive 92/59/EEC has been repealed from 15 January 2004 and replaced on the same date by Directive 2001/95/EC. Article 13(2) of Directive 2001/95/EC states that Commission decisions requiring Member States to take measures to prevent serious risks posed by certain products shall be valid for periods not exceeding one year and may be confirmed for additional periods none of which shall exceed one year. It is appropriate to prolong the validity of Decision 1999/815/EC for a period of six months, in order to allow sufficient time to make progress with the permanent measures referred to in recital 4, while ensuring the possibility of re-examining, in due time, the duration of the validity of the Decision.
(9) The measures provided for in this Decision are in accordance with the opinion of the Committee established by Article 15 of Directive 2001/95/EC,
HAS ADOPTED THIS DECISION:
Article 1
In Article 5 of Decision 1999/815/EC "20 February 2004" is replaced by "20 August 2004".
Article 2
Member States shall immediately take the necessary measures to comply with this Decision and publish those measures. They shall immediately inform the Commission thereof.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 20 February 2004.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 11, 15.1.2002, p. 4.
(2) OJ L 315, 9.12.1999, p. 46. Decision as last amended by Decision 2003/819/EC (OJ L 308, 25.11.2003, p. 23).
(3) OJ L 228, 11.8.1992, p. 24. Directive as amended by Regulation (EC) No 1882/2003 of the European Parliament and of the Council (OJ L 284, 31.10.2003, p. 1).
(4) OJ L 84, 5.4.1993, p. 1. Regulation as amended by Regulation (EC) No 1882/2003.
