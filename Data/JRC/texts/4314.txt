Budget of the European Training Foundation for the financial year 2005
(2005/C 50/07)
Pursuant to article 26(2) of the Financial Regulation of the European Training Foundation (ETF) adopted by the Management Board on 17 June 2003, the budget and amending budgets, as finally adopted, shall be published in the Official Journal of the European Communities within two months of their adoption.
The European Parliament has adopted on 16 December 2004 the general budget 2005 applicable to the European Communities in which ETF subsidies appear under the following headings:
- External Actions, Education and Culture,
15.03.03.01 — ETF — Subsidy under Title 1 and Title 2 | EUR 11565000 |
15.03.03.02 — ETF — Subsidy under Title 3 | EUR 4435000 |
- Pre-Accession Strategy, Education and Culture,
15.03.02.01 — PHARE contribution to the ETF — Subsidy under Title 1 and Title 2 | EUR 1935000 |
15.03.02.02 — PHARE contribution to the ETF — Subsidy under Title 3 | EUR 565000 |
Total | EUR 18500000 |
Regarding article 27(6) of the ETF Financial Regulation and with the adoption of the general budget 2005 by European Parliament on the 16 December 2004, the ETF budget 2005 including the establishment plan, adopted by the Governing board on 9 November 2004 (ETF-GB-027-01), becomes definitive.
Furthermore, the ETF manages Phare/Cards, Tacis and Meda funds for a total amount of EUR 185 million.
For complete details about 2005 budget including the establishment plan please visit ETF website at the following address: http://www.etf.eu.int (Documentation centre-institutional reports)
European Training Foundation
Budgets 2004/2005
Expenditure
| Title | Budget 2004After transfers | Budget 2005 |
TITLE 1 | EXPENDITURE RELATING TO PERSONS WORKING WITH THE FOUNDATION | | |
| TOTAL TITLE 1 | 11493973 | 12047000 |
TITLE 2 | BUILDING, EQUIPEMENT AND MISCELLANEOUS OPERATING EXPENDITURE | | |
| TOTAL TITLE 2 | 1470027 | 1453000 |
TITLE 3 | EXPENSES RELATING TO PERFORMANCE OF SPECIFIC MISSIONS | | |
Chapter 30 | Operational expenses (Documentation, publications, translation, meetings, etc) | | |
| Total chapter | 1095351 | 1033400 |
Chapter 31 | Priority actions: work programme activities (Support to the Commission, information provision and analysis through the National Observatory Network, development activities) | | |
| Total chapter | 3540649 | 3966000 |
TITLE 3 | TOTAL TITLE 3 | 4636000 | 5000000 |
TITLE 9 | RESERVE | — | — |
| GRAND TOTAL | 17600000 | 18500000 |
TITLE 4 | EARMARKED EXPENDITURE | 800000 | |
| TOTAL TITLE 4 | 800000 | |
--------------------------------------------------
