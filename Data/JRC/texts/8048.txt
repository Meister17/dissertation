COUNCIL REGULATION (EEC) No 3573/90 of 4 December 1990 amending, as a result of German unification, Regulation (EEC) No 4055/86 applying the principle of freedom to provide services to maritime transport between Member States and between Member States and third countries
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 84 (2) thereof,
Having regard to the proposal from the Commission(1),
Having regard to the opinion of the European Parliament(2),
Having regard to the opinion of the Economic and Social Committee(3),
Whereas the Community has adopted a set of rules governing maritime transport;
Whereas, from the date of German unification onwards, Community law will be fully applicable in the territory of the former German Democratic Republic;
Whereas Regulation (EEC) No 4055/86(4) must be amended to take account of the special situation arising from German unification in respect of bilateral agreements concluded between the former German Democratic Republic and third countries;
Whereas the agreements concluded by the former German Democratic Republic relate only to cargoes originating in
that country; whereas, therefore, the rights which may be enjoyed by third countries as a result of cargo-sharing arrangements relate only to cargoes originating in the territory of the former German Democratic Republic;
Whereas the time allowed for Member States to adjust agreements relating to trades not governed by the United Nations Code of Conduct for Liner conferences must be extended in the case of bilateral agreements concluded with third countries by the former German Democratic Republic to enable the Federal Republic of Germany to conduct the necessary negotiations for adjusting those agreements,
HAS ADOPTED THIS REGULATION:
Article 1
The following paragraph is hereby added to Article 4 (1) (b) of Regulation (EEC) No 4055/86:
'Agreements concluded by the former German Democratic Republic shall be adjusted as soon as possible and in any event not later than 1 January 1995.'
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 4 December 1990.
For the Council The President G. DE MICHELIS
(1)OJ No C 248, 2. 10. 1990, p. 13, as amended on 25 October 1990.
(2)Opinion delivered on 21 November 1990 (not yet published in the Official Journal).
(3)Opinion delivered on 20 November 1990 (not yet published in the Official Journal).
(4)OJ No L 378, 21. 12. 1986, p. 1.
