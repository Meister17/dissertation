COUNCIL DIRECTIVE 95/68/EC
of 22 December 1995
amending Directive 77/99/EEC on health problems affecting the production and marketing of meat products and certain other products of animal origin
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community,
Having regard to Directive 77/99/EEC (1) and in particular Article 19 thereof,
Having regard to the proposal from the Commission,
Whereas certain aspects of the Annexes to Directive 77/99/EEC should be adapted in order to take account of technological progress in the meat processing sector and in order to bring technical requirements into line with current practice;
Whereas it is therefore necessary to amend the requirements relating to the general conditions for approval of establishments, the general conditions of hygiene applicable to premises, equipment and tools, the special conditions of hygiene for establishments preparing meat products, the requirements for the wrapping, packaging, labelling, health marking, storage and transport of meat products and those relating to the special conditions for prepared meat meals and for rendered fats;
Whereas, until such time as measures have been taken to simplify existing texts, it is necessary to adopt provisional measures to avoid the application of several health marks to meat products containing other products of animal origin;
Whereas the amendments made by the Council to Council Directive 64/433/EEC of 26 June 1964 on health problems affecting intra-Community trade in fresh meat (2) including small cold stores and Council Directive 88/657/EEC of 14 December 1988 lying down the requirements for the production of, and trade in, minced meat, meat in pieces of less than 100 grams and meat preparations and amending Directives 64/533/EEC, 71/118/EEC and 72/462/EEC (3) require subsequent adjustments to Directive 77/99/EEC; whereas, pending such proposals, measures should be adopted to adjust the Annexes to Directive 77/99/EEC to technological progress,
HAS ADOPTED THIS DIRECTIVE:
Article 1
The Annexes to Directive 77/99/EEC are hereby amended as follows:
1. in Annex A, Chapter I, point 2 (e) shall be replaced by the following:
'(e) adequate ventilation and, where necessary, good steam and water-vapour extraction facilities in order to eliminate as far as possible condensation on surfaces such as walls and ceilings or roof linings;'
2. in Annex A, Chapter I, the following shall be added to point 8:
'for disinfecting equipment and utensils, water of a temperature of not less than 82° C, or other disinfection methods approved by the competent authority, must be used;'
3. in Annex A, Chapter I, the following shall be added to point 12:
'where the competent authority is not required to be present at all times, a lockable device of sufficient capacity for storage of equipment and materials is sufficient;';
4. in Annex A, Chapter I, point 15 shall be replaced by the following:
'15. adequate facilities for cleaning and desinfecting means of transport; unless, with the agreement of the competent authority, facilities not situated in the establishment may be used;'
5. in Annex A, Chapter I, the following point 16 shall be added:
'16. where the treatment applied requires the absence of water for manufacture of the products, certain requirements of this Chapter, in particular those laid down in points 2 (a) and (g), may be adjusted. Should recourse be had to such a derogation, cleaning and disinfecting processes which do not make use of water may, with the authorization of the competent authority, be applied in the parts of the establishment concerned.';
6. in Annex A, Chapter II A, the last sentence of point 1 shall be replaced by the following:
'Cleaning and disinfecting must be performed with a frequency and by means of processes which are in line with the principles set out in Article 7 of the Directive.';
7. in Annex A, Chapter II A, point 5 shall be replaced by the following:
'5. Detergents, disinfectants and similar substances must be used in accordance with the manufacturers' instructions in such a way that they do not have adverse effects on the machinery, equipment, raw materials and products. Their use must be followed by thorough rinsing of such instruments and working equipment with potable water except where the directions for use of such substances render such rinsing unnecessary.
Products for maintenance and cleaning must be kept in the room or facility provided for in Chapter I (14) of this Annex.';
8. (concerns only the German text) in Annex A, Chapter II B, the second subparagraph of point 2 shall read:
'. . . bearbeitet und behandelt';
9. in Annex B, Chapter III, point 3 shall be replaced by the following:
'3. The presence of products of animal origin, other than meat as defined in Article 2 (d) of the Directive contained in the meat products, is authorized only if these products comply with the requirements laid down in the relevant Community legislation.';
10. in Annex B, Chapter V, the third indent of point 4 shall be replaced by the following:
'- for packaging not intended for the final consumer, the date of preparation or a code which can be interpreted by the recipient and by the competent authority allowing the identification of that date.';
11. in Annex B, Chapter VI shall be replaced by the following:
'CHAPTER VI
HEALTH MARK
1. Meat products must carry a health mark. Marking must be carried out during or immediately after manufacture in the establishment or wrapping centre, in an easily visible place in legible and indelible characters which are easy to distinguish. The health mark may be applied directly to the product or to its wrapping if the meat product is individually wrapped, or to a label affixed to the wrapping, in accordance with point 4 (b). However, where a meat product is wrapped and packaged individually, a health mark applied to the packaging is sufficient.
2. Where meat products carrying a health mark in accordance with point 1 are then packaged, the health mark must also be applied to the packaging.
3. By way of derogation from points 1 and 2, the health marking of meat products is not necessary:
(a) where the health mark, in compliance with point 4, is applied to the external surface of each sales unit containing them;
(b) where, for meat products in consignments intended for further processing or wrapping in an approved establishment:
- the said consignments bear the health mark of the approved establishment consigning them in a visible place on the external surface, together with a clear indication of the intended destination,
- the recipient establishment maintains a record of the quantities, type and origin of meat products received in accordance with this point and stores that record for the period laid down in the fourth indent of the second subparagraph of Article 7 (1) of the Directive. However, meat products in large packagings which are intended for immediate sale without further processing or wrapping must bear a health mark in compliance with point 1, 2 or 3 (a);
(c) where, for meat products which are not wrapped or packaged but sold in bulk by the manufacturer directly to a retailer:
- the health mark, in compliance with point 1, is applied to the container carrying them,
- the manufacturer maintains a record of the quantities and type of the meat products consigned in accordance with this point and of the name of the recipient and stores that record for the period laid down in the fourth indent of the second subparagraph of Article 7 (1) of the Directive.
4. (a) The health mark must give the following particulars within an oval surround:
(i) either:
- above: the initial letter or letters of the consigning country in printed capitals, i.e. B - DK - D - EL - E - F - IRL - I - L - NL - AT - P - FI - S - UK, followed by the approval number of the establishment or, the rewrapping centre, in accordance with Decision 94/837/EC, if necessary accompanied by a code number stating the type of product for which the establishment is approved,
- below: one of the following sets of initials: CEE - EOEF - EWG - EOK - ETY - EC - EEG;
(ii) or:
- above: the name of the consigning country in capitals,
- in the centre: the approval number of the establishment or the rewrapping centre, in accordance with Decision 94/837/EC, if necessary accompanied by a code number stating the type of product for which the establishment is approved,
- below: one of the following sets of initials: CEE - EOEF - EWG - EOK - ETY - EEC - EEG;
(b) the health mark may be applied directly to the product by authorized means or be pre-printed on its wrapping or packaging, or to a label affixed to the product, its wrapping or packaging. Where it is applied to the wrapping, the stamp must be destroyed when the wrapping is opened. Failure to destroy the stamp can be tolerated only where the wrapping is destroyed by opening it. In the case of products in hermetically sealed containers, the stamp must be applied indelibly on either the lid or the can;
(c) the health mark may also consist of an irremovable plate of resistant material, complying with all the hygiene requirements and containing all the information listed in point (a).
5. Where a meat product contains other foodstuffs of animal origin such as fishery products, dairy products or egg products, only one health mark must be applied.';
12. in Annex B, Chapter VII, point 1 shall be replaced by the following:
'1. Meat products must be stored in the rooms provided for in Annex B, Chapter I, point 1(a).
However, meat products may also be stored outside the rooms provided for in that point on the following conditions:
(a) meat products which cannot be kept at ambient temperatures may be stored in cold storage plant as referred to in Article 3 (A) (8) of the Directive or in cold storage plant approved in accordance with the other relevant Directives;
(b) meat products which can be kept at ambient temperatures may be stored in stores of solid construction, easy to clean and disinfect, and approved by the competent authority.';
13. in Annex B, Chapter VII, the following shall be added:
'5. The commercial document referred to in Article 3 A (9) (b) (i) of the Directive must accompany meat products during the first stage of marketing.
For transport and marketing at subsequent stages, the products must be accompanied by a commercial document bearing the approval number of the consigning establishment identifying the competent authority responsible for control';
14. in Annex B, Chapter VIII, point B shall be replaced by the following:
'B. The operator or manager of an establishment manufacturing meat products in hermetically sealed containers must also check by sampling that:
(1) a heat treatment is applied to meat products intended for storing at ambient temperature which is capable of destroying or inactivating pathogenic germs and the spores of pathogenic micro-organisms. A register of manufacturing parameters such as duration of heating, temperature, filling, size of containers, etc. must be kept.
The heat treatment apparatus must be fitted with control devices making it possible to check that containers have undergone effective heat treatment;
(2) the material used for the containers meets Community requirements relating to materials intended to come into contact with foodstuffs;
(3) checks on the daily output are carried out at intervals determined in advance, to ensure the efficacy of the sealing. To this end, suitable equipment must be available for examining perpendicular sections and the seams of the sealed containers;
(4) additional checks by sampling are carried out by the manufacturer to ensure that:
(a) sterilized products have undergone effective treatment, by means of:
- incubation tests. Incubation must be performed at a least 37° C for seven days or at at least 35° C for 10 days, or any other time/temperature combination recognized as equivalent by the competent authority,
- microbiological examination of the contents and the containers in the establishment's laboratory or in another approved laboratory;
(b) pasteurized products in hermetically sealed containers satisfy criteria recognized by the competent authority,
(5) the necessary checks are carried out to ensure that the cooling water contains a residual level of chlorine after use. Member States may, however, grant a derogation from this requirement if the water fulfils the requirements of Directive 80/778/EEC.';
15. in Annex B, Chapter IX, point 2 (a) shall be replaced by the following:
'2. (a) The meat product contained in the prepared meal must, as soon as it has been cooked:
(i) either be mixed with the other ingredients as soon as practically possible; in that event the time during which the temperature of the meat products is between 10° C and 60° C must be kept to a maximum of two hours;
(ii) or be refrigerated to 10° C or less before being mixed with the other ingredients.
Where other preparation methods are applied, these must be approved by the competent authority, which shall inform the Commission accordingly.';
16. in Annex C, Chapter II A, point 2 (a) shall be replaced by the following:
'(a) a cold store, unless the raw materials are collected and rendered within the time limits laid down in B (3) (b) and B (3) (c);';
17. in Annex C, Chapter II B, the words 'for the production of raw materials' shall be deleted in point 7;
18. in Annex C, Chapter II B, point 8 shall be replaced by the following:
'8. Rendered animal fat, depending on type, must meet the following standards:
>TABLE>
'
Article 2
1. Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive before 1 October 1996. They shall forthwith inform the Commission thereof.
When Member States adopt these provisions, they shall contain a reference to this Directive or shall be accompanied by such reference on the occasion of their official publication. The methods of making such reference shall be laid down by Member States.
2. Member States shall communicate to the Commission the text of the main provisions of domestic law which they adopt in the field governed by this Directive.
Article 3
This Directive shall enter into force on the 20th day following that of its publication in the Official Journal of the European Communities.
Article 4
This Directive is addressed to the Member States.
Done at Brussels, 22 December 1995.
For the Council
The President
L. ATIENZA SERNA
(1) OJ No L 26, 31. 1. 1977, p. 85. Directive as last amended by the 1994 Act of Accession.
(2) OJ No 121, 29. 7. 1964, p. 2012/64. Directive as last amended by Directive 95/23/EC (OJ No L 243, 11. 10. 1995, p. 7).
(3) OJ No L 382, 31. 12. 1988, p. 3. Directive as last amended by the 1994 Act of Accession.
