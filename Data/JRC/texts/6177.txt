Council Decision
of 18 January 2005
appointing members of the Committee provided for in Article 3(3) of Annex I to the Protocol on the Statute of the Court of Justice
(2005/151/EC, Euratom)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community,
Having regard to the Treaty establishing the European Atomic Energy Community,
Having regard to the Protocol on the Statute of the Court of Justice, as amended by Council Decision 2004/752/EC, Euratom of 2 November 2004 establishing the European Union Civil Service Tribunal [1], and in particular Article 3(3) of Annex I thereto,
Having regard to Council Decision 2005/49/EC, Euratom of 18 January 2005 concerning the operating rules of the Committee provided for by Article 3(3) of Annex I to the Protocol on the Statute of the Court of Justice [2], and in particular point 3 of the Annex thereto,
Having regard to the recommendation of the President of the Court of Justice on 2 December 2004,
Whereas:
(1) Article 3(3) of Annex I to the Protocol on the Statute of the Court of Justice provides for the setting up of a committee comprising seven persons chosen from among former members of the Court of Justice and the Court of First Instance and lawyers of recognised competence. By virtue of Article 3(3), members of the committee are appointed by the Council, acting by a qualified majority on a recommendation by the President of the Court of Justice.
(2) Furthermore, point 3 of the Annex to Decision 2005/49/EC, Euratom provides that the Council should appoint the President of that committee.
(3) These provisions should be applied,
HAS DECIDED AS FOLLOWS:
Article 1
For a period of four years from 10 November 2004, the following shall be appointed members of the committee provided for by Article 3(3) of Annex I to the Protocol on the Statute of the Court of Justice:
Mr Leif SEVÓN, President
Sir Christopher BELLAMY
Mr Yves GALMOT
Mr Peter GRILC
Ms Gabriele KUCSKO-STADLMAYER
Mr Giuseppe TESAURO
Mr Miroslaw WYRZYKOWSKI.
Article 2
This Decision shall take effect on the day following its publication in the Official Journal of the European Union.
Done at Brussels, 18 January 2005.
For the Council
The President
J.-C. Juncker
[1] OJ L 333, 9.11.2004, p. 7.
[2] OJ L 21, 25.1.2005, p. 13.
--------------------------------------------------
