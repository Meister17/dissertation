Commission Regulation (EC) No 827/2000
of 25 April 2000
amending Regulation (EC) No 2461/1999 laying down detailed rules for the application of Council Regulation (EC) No 1251/1999 as regards the use of land set aside for the production of raw materials for the manufacture within the Community of products not primarily intended for human or animal consumption
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1251/1999 of 17 May 1999 establishing a support system for producers of certain arable crops(1), as amended by Regulation (EC) No 2704/1999(2), and in particular Article 9 thereof,
Whereas:
(1) The first subparagraph of Article 6(3) of Regulation (EC) No 1251/1999 provides that land set aside may be used for the production of raw materials for the manufacture within the Community of products not primarily intended for human or animal consumption, provided that effective control systems are applied.
(2) In order to ensure compliance with point 7 of the Memorandum of Understanding on oilseeds between the European Economic Community and the United States of America within the framework of the GATT, as approved by Council Decision 93/355/EEC(3), the third subparagraph of Article 6(3) of Regulation (EC) No 1251/1999 provides that if the quantity of by-products for feed or food uses likely to be made available as a result of the cultivation of oilseeds on land set aside under the first subparagraph exceeds, on the basis of the forecast quantities covered by contracts made with producers, 1 million metric tonnes annually expressed in soya bean meal equivalent, in order to limit that quantity to 1 million metric tonnes, the amount of the forecast quantity under each contract, which may be used for feed or food uses, is to be reduced.
(3) In order to be able to implement the rules laid down in the third subparagraph of Article 6(3) of Regulation (EC) No 1251/1999, the monitoring system for assessing the quantity of by-products, introduced by Commission Regulation (EC) No 2461/1999(4), should now be amplified.
(4) It is necessary also to define the method of calculation to be used by the Commission as part of the monitoring system and to introduce a procedure for fixing the coefficient for reducing the quantities of by-products intended for feed or food uses to be applied to each contract where the 1 million tonnes ceiling is exceeded. In that situation, the pure protein content is the decisive criterion for drawing up the method of calculation.
(5) To ensure that the maximum quantity of by-products intended for animal or human consumption is not exceeded, evidence should be required concerning the disposal of quantities of by-products for the non-feed or non-food markets, to be provided upon release of the security lodged by the collector or first processor.
(6) In order to enable effective checks to be made on compliance with the requirement not to exceed the 1 million tonnes ceiling, it is necessary to impose a deadline for disposing of quantities of by-products intended for the non-feed or non-food markets.
(7) The new rules on support for rural development adopted in 1999 no longer expressly rule out agri-environmental aid for areas qualifying under the Community set-aside arrangements which are used to grow non-feed or non-food crops. In the interests of consistency, provisions should be adopted prohibiting aggregation with other Community support schemes.
(8) Since the Memorandum of Understanding was published in the Official Journal of the European Communities in 1993, operators are deemed to be aware of its content from that date. In view of the threat of the 1 million tonnes ceiling being exceeded in the 2000/01 marketing year, provision needs to be made for applying the measures contained in this Regulation to all contracts concluded in respect of that marketing year.
(9) The measures laid down in this Regulation are in accordance with the opinion of the Management Committee for Cereals,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 2461/1999 is hereby amended as follows:
1. Article 4(4) is replaced by the following:
"4. Where contracts relate to rapeseed, colza seed, sunflower seed or soya beans covered by CN codes ex 1205 00 90, 1206 00 91, 1206 00 99 or 1201 00 90, the applicants shall ensure that, in addition to the information required under paragraph 2, they specify the total anticipated quantity of by-products and the anticipated quantity of by-products intended for uses other than human or animal consumption, by species in both cases.
The quantities shall be calculated on the basis of the following ratios:
(a) 100 kilograms of rapeseed and/or colza seed covered by CN code 1205 00 90 shall be deemed equivalent to 56 kilograms of by-products;
(b) 100 kilograms of sunflower seed covered by CN codes 1206 00 91 or 1206 00 99 shall be deemed equivalent to 56 kilograms of by-products;
(c) 100 kilograms of soya beans covered by CN code 1201 00 90 shall be deemed equivalent to 78 kilograms of by-products."
2. Article 14 is replaced by the following:
"1. Without prejudice to Article 13, as soon as possible and by 30 June at the latest of the year in which the raw materials are to be harvested, the competent authorities referred to in paragraph 1 of that Article shall inform the Commission of the total quantity of each species of by-products intended for human or animal consumption resulting from the contracts provided for in Article 4, covering rapeseed, colza seed, sunflower seeds and soya beans falling within CN codes ex 1205 00 90, 1206 00 91, 1206 00 99 and 1201 00 90.
2. On the basis of the information provided pursuant to paragraph 1, the Commission shall calculate the forecast total quantity of by-products for human or animal consumption, expressed in terms of soya bean meal equivalent, applying the method based on the following coefficients:
- soya cake: 48 %,
- colza cake: 32 %,
- sunflower cake: 28 %.
If, as a result of the calculation made pursuant to the first subparagraph, the Commission finds that there is an overrun of the 1 million tonnes ceiling for by-products intended for food or feed uses, it shall fix, as soon as possible and by 31 July at the latest of the year in which the raw materials are to be harvested, the percentage reduction to be applied to each contract, for the purpose of calculating the maximum quantity of by-products for human or animal consumption."
3. Article 15(4) is replaced by the following:
"4. The security shall be released on a proportional basis on condition that the competent authority of the collector or first processor concerned is in possession of proof:
(a) that the quantity of raw material in question has been processed in compliance with the requirement laid down in Article 4(2)(f), account being taken, where necessary, of any changes pursuant to Article 8; and
(b) where contracts relate to rapeseed, colza seed, sunflower seed or soya beans covered by CN codes ex 1205 00 90, 1206 00 91, 1206 00 99 or 1201 00 90 and the procedure laid down in Article 14(2) applies, that the quantity of by-products in excess of the maximum quantity for human or animal consumption has found outlets other than the feed or foodstuffs markets."
4. Article 16(2) is replaced by the following:
"2. The following shall constitute primary requirements within the meaning of Article 20 of Regulation (EEC) No 2220/85:
(a) the obligation to process the quantities of raw materials principally into the end products specified in the contract;
(b) where the procedure laid down in Article 14(2) applies, the obligation to seek outlets other than the feed or foodstuffs markets for quantities of by-products in excess of the maximum quantity for human or animal consumption.
Processing into one or more of the end products specified in Annex III shall be carried out by 31 July of the second year following the year in which the applicant harvests the raw material. Where the procedure laid down in Article 14(2) applies, this deadline shall be observed also for the disposal on markets other than the feed or foodstuffs markets of the quantity of by-products in excess of the maximum quantity for human or animal consumption."
5. Points (a) and (b) of Article 25 are replaced by the following:
"(a) financing by the Guarantee Section of the European Agricultural Guidance and Guarantee Fund pursuant to Article 1(2)(a) and (b) of Regulation (EC) No 1258/1999;
(b) Community aid as provided for in Chapter VIII of Regulation (EC) No 1257/1999 with the exception of support granted in respect of the costs of planting fast-growing species as provided for in the second subparagraph of Article 31(3) thereof."
Article 2
This Regulation shall enter into force on the 20th day following its publication in the Official Journal of the European Communities.
It shall apply to all contracts concluded in respect of the 2000/01 and subsequent marketing years, irrespective of the date of their conclusion.
Notwithstanding the preceding paragraph, the amendments to Articles 4(4) and 14(1) of Regulation (EC) No 2461/1999 provided for in this Regulation shall apply to contracts concluded in respect of the 2001/02 and subsequent marketing years.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 25 April 2000.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 160, 26.6.1999, p. 1.
(2) OJ L 327, 21.12.1999, p. 12.
(3) OJ L 147, 18.6.1993, p. 25.
(4) OJ L 299, 20.11.1999, p. 16.
