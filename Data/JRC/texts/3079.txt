Commission Decision
of 15 July 2005,
excluding from Community financing certain expenditure incurred by the Member States under the Guarantee Section of the European Agricultural Guidance and Guarantee Fund (EAGGF)
(notified under document number C(2005) 2685)
(Only the Spanish, Danish, German, Greek, English, French, Italian, Dutch and Portuguese texts are authentic)
(2005/555/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 729/70 of 21 April 1970 on the financing of the common agricultural policy [1], and in particular Article 5(2)(c) thereof,
Having regard to Council Regulation (EC) No 1258/1999 of 17 May 1999 on the financing of the common agricultural policy [2], and in particular Article 7(4) thereof,
Having consulted the Fund Committee,
Whereas:
(1) Article 5 of Regulation (EEC) No 729/70, Article 7 of Regulation (EC) No 1258/1999, and Article 8(1) and (2) of Commission Regulation (EC) No 1663/95 of 7 July 1995 laying down detailed rules for the application of Council Regulation (EEC) No 729/70 regarding the procedure for the clearance of the accounts of the EAGGF Guarantee Section [3], provide that the Commission is to make the necessary verifications, inform the Member States of its findings, take account of the Member States' comments, initiate bilateral discussions and then formally communicate its conclusions to the Member States, referring to Commission Decision 94/442/EC of 1 July 1994 setting up a conciliation procedure in the context of the clearance of the accounts of the European Agricultural Guidance and Guarantee Fund (EAGGF) Guarantee Section [4].
(2) The Member States have had an opportunity to request that a conciliation procedure be initiated. That opportunity has been used in some cases and the report issued on the outcome has been examined by the Commission.
(3) Under Articles 2 and 3 of Regulation (EEC) No 729/70 and Article 2 of Regulation (EC) No 1258/1999, only refunds on exports to third countries and intervention to stabilise agricultural markets, respectively granted and undertaken according to Community rules within the framework of the common organisation of the agricultural markets, may be financed.
(4) In the light of the checks carried out, the outcome of the bilateral discussions and the conciliation procedures, part of the expenditure declared by the Member States does not fulfil these requirements and cannot, therefore, be financed under the EAGGF Guarantee Section.
(5) The amounts that are not recognised as being chargeable to the EAGGF Guarantee Section should be indicated. Those amounts do not relate to expenditure incurred more than twenty-four months before the Commission's written notification of the results of the checks to the Member States.
(6) As regards the cases covered by this Decision, the assessment of the amounts to be excluded on grounds of non-compliance with Community rules was notified by the Commission to the Member States in a summary report on the subject.
(7) This Decision is without prejudice to any financial conclusions that the Commission may draw from the judgments of the Court of Justice in cases pending on 15 February 2005 and relating to its content,
HAS ADOPTED THIS DECISION:
Article 1
The expenditure itemised in the Annex hereto that has been incurred by the Member States' accredited paying agencies and declared under the EAGGF Guarantee Section shall be excluded from Community financing because it does not comply with Community rules.
Article 2
This Decision is addressed to the Kingdom of Belgium, the Kingdom of Denmark, the Federal Republic of Germany, the Hellenic Republic, the Kingdom of Spain, the French Republic, Ireland, the Italian Republic, the Grand Duchy of Luxembourg, the Kingdom of the Netherlands, the Republic of Austria, the Portuguese Republic, and the United Kingdom of Great Britain and Northern Ireland.
Done at Brussels, 15 July 2005.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 94, 28.4.1970, p. 13. Regulation as last amended by Commission Regulation (EC) No 1287/95 (OJ L 125, 8.6.1995, p. 1).
[2] OJ L 160, 26.6.1999, p. 103.
[3] OJ L 158, 8.7.1995, p. 6. Regulation as last amended by Regulation (EC) No 465/2005 (OJ L 77, 23.3.2005, p. 6).
[4] OJ L 182, 16.7.1994, p. 45. Decision as last amended by Decision 2001/535/EC (OJ L 193, 17.7.2001, p. 25).
--------------------------------------------------
ANNEX
Sector | Member State | Budget item | Reason | Nat. Currency | Expenditure to exclude from financing | Deductions already made | Financial impact of this decision | Financialyear |
Animal premia | AT | 21 20, 21 21, 21 25, 21 28 | Flat-rate correction of 5 % — over-declaration of alpine forage areas, non-application of the area-measurement system. | EUR | – 1580413,53 | 0,00 | – 1580413,53 | 2001-2002 |
| Total AT | | | | – 1580413,53 | 0,00 | – 1580413,53 | |
Financial audit | BE | Miscellaneous | Financial correction — certification of accounts. | EUR | – 73583,84 | 0,00 | – 73583,84 | 2002 |
| Total BE | | | | – 73583,84 | 0,00 | – 73583,84 | |
Financial audit | DE | 21 20, 21 24, 23 20 | Correction — application of Council Regulation (EC) No 1258/1999 — non-compliance with payment deadlines. | EUR | – 300160,13 | – 332346,61 | 32186,48 | 2003 |
Financial audit | DE | Miscellaneous | Financial correction — certification of accounts. | EUR | – 1179506,70 | 0,00 | – 1179506,70 | 2002 |
| Total DE | | | | – 1479666,83 | – 332346,61 | – 1147320,22 | |
Animal premia | DK | 21 24, 21 28 | Erroneous control statistics submitted for 2002, non-compliance with Commission Regulation (EEC) No 3887/92, inadequate risk analysis. | DKK | – 1385375,53 | 0,00 | – 1385375,53 | 2002-2003 |
Financial audit | DK | 21 25 | Correction — application of Regulation (EC) No 1258/1999 — non-compliance with payment deadlines. | DKK | – 208243,99 | – 208243,99 | 0,00 | 2003 |
| Total DK | | | | – 1593619,52 | – 208243,99 | – 1385375,53 | |
Fruit and vegetables | GR | 15 15 | Multiannual contracts: specific financial correction — failure to achieve minimum quantities. Annual and multiannual contracts: flat-rate financial correction of 5 % — quantities delivered for processing were smaller than specified in contractual agreements. | EUR | – 330595,00 | 0,00 | – 330595,00 | 2000-2001 |
Animal premia | GR | 21 25 | Exclusion of total declared expenditure — no quality control to establish the stocking density, no control of extensification premium, deficient procedures to establish eligibility for extensification premium in eligible land taken into account to establish eligibility, no confirmation of penalties. | EUR | – 34530717,69 | 0,00 | – 34530717,69 | 2001-2003 |
Arable crops | GR | 10 40-10 62, 13 10, 18 58 | Flat-rate correction of 5 % — no full implementation of IACS, key component controls not fully operational. | EUR | – 25437255,00 | 0,00 | – 25437255,00 | 2003 |
| Total GR | | | | – 60298567,69 | 0,00 | – 60298567,69 | |
Fruit and vegetables | ES | 15 07 | Flat-rate correction of 10 % — non-compliance with payment deadlines. | EUR | – 13341081,60 | 0,00 | – 13341081,60 | 2000-2001 |
Fruit and vegetables | ES | 15 01, 15 02, 15 11, 15 12, 15 15, 314 | Flat-rate correction of 5 % — shortcomings in the control system of the Andalusian authorities. 2 % correction for measures within the processing sector. Flat-rate correction of 10 % — non-fixing of planned penalties. | EUR | – 1821776,35 | 0,00 | – 1821776,35 | 1999-2001 |
Fruit and vegetables | ES | 15 02 | Expenses to be excluded from Community funding — unbalanced operational programmes, inadequate environmental activities. | EUR | – 2902219,77 | 0,00 | – 2902219,77 | 2000-2002 |
Milk | ES | 20 71 | Annulment of Commission Decision 2001/137/EC of 5 February 2001, amount to be reimbursed to Spain. | EUR | 14582115,50 | 0,00 | 14582115,50 | 1996 |
Public Storage | ES | 32 20 | Scale of correction: as established in Article 4 of Regulation (EC) No 296/96 — non-compliance with payment deadlines. | EUR | – 1479326,74 | 0,00 | – 1479326,74 | 2000-2002 |
Wine and tobacco products | ES | 16 50 | Correction of 100 % for part of the expenditure — non-compliance with Council Regulation (EC) No 1493/1999. | EUR | – 4790799,61 | 0,00 | – 4790799,61 | 2001-2003 |
Animal premia | ES | 21 20, 21 21, 21 22, 21 24, 21 25, 21 28 | Flat-rate correction of 5 % — insufficient level of on-the-spot-checks. | EUR | – 3066016,28 | 0,00 | – 3066016,28 | 2002-2003 |
Honey | ES | 23 20 | Flat-rate correction of 5 % — inadequate controls, absence of an increase in the number of on-the-spot checks, VAT unduly charged. | EUR | – 71495,57 | 0,00 | – 71495,57 | 2002-2003 |
| Total ES | | | | – 12890600,42 | 0,00 | – 12890600,42 | |
Fruit and vegetables | FR | 15 01, 15 02, 15 09, 15 12, 314 | Specific correction of 25,3 % and flat-rate correction of 10 % — acknowledgment: shortcomings in control system. flat-rate correction of 5 % — activities relating to products withdrawn from the market: shortcomings in control system. flat-rate correction of 10 % — non-fixing of penalties. | EUR | – 63792464,12 | 0,00 | – 63792464,12 | 1999-2002 |
Fruit and vegetables | FR | 15 02 | Flat-rate correction of 2 % — use of flat-rate sums and erroneous method for calculating contributions to operational funds. | EUR | – 2669660,60 | 0,00 | – 2669660,60 | 2000-2003 |
Financial audit | FR | 12 10, 15 02, 16 10, 21 21, 21 24, 21 25, 21 28 | Correction — application of Regulation (EC) No 1258/1999 — non-compliance with payment deadlines. | EUR | – 5648423,18 | – 5648423,18 | 0,00 | 2003 |
| Total FR | | | | – 72110547,90 | – 5648423,18 | – 66462124,72 | |
Fishery | IE | 26 10 | Flat-rate correction of 2 % — lack of proper control over the withdrawal of fish. Flat-rate correction of 5 % — inadequate control over the rendering of goods unfit for human consumption. Flat-rate correction of 10 % — shortcomings in the inventories of stores. Specific correction — claims received after deadline. | EUR | – 582331,61 | 0,00 | – 582331,61 | 2001-2003 |
Financial audit | IE | 21 25, 22 21 | Correction — application of Regulation (EC) No 1258/1999 — non-compliance with payment deadlines. | EUR | – 625136,09 | – 625136,09 | 0,00 | 2003 |
| Total IE | | | | – 1207467,70 | – 625136,09 | – 582331,61 | |
Fruit and vegetables | IT | 15 02 | Scale of correction: as established in Article 4 of Regulation (EC) No 296/96 — non-compliance with payment deadlines. | EUR | – 1223457,00 | 0,00 | – 1223457,00 | 2002 |
Honey | IT | 23 20 | Flat-rate correction of 5 % for 2001 and 2002, 2 % for 2003 — inadequate controls, insufficient evidence of supervision by the paying agency, ineligible expenditure charged to the Fund. | EUR | – 582226,79 | 0,00 | – 582226,79 | 2001-2003 |
Financial audit | IT | Miscellaneous | Correction — application of Regulation (EC) No 1258/1999 — non-compliance with payment deadlines. | EUR | – 28852472,77 | – 29264364,26 | 411891,49 | 2002 |
| Total IT | | | | – 30658156,56 | – 29264364,26 | – 1393792,30 | |
Animal premia | LU | 22 20, 22 21, 22 22 | Flat-rate correction of 5 % — insufficient attention to compliance with conditions of eligibility, absence of checks on supporting documents, inspections announced too early, absence of checks on premises where animals kept. | EUR | – 13978,14 | 0,00 | – 13978,14 | 2002-2004 |
Arable crops | LU | 10 40-10 60, 10 62, 1310 | Flat-rate correction of 2 % — recurrent deficiencies in the administration of on-site audits, late on-site audits, shortcomings in the calculation of aid amounts, application of incorrect reduction rate. | EUR | – 621471,00 | 0,00 | – 621471,00 | 2000-2002 |
Rural development | LU | 40 40 | Correction — non-observation of eligibility requirement. | EUR | – 122153,00 | 0,00 | – 122153,00 | 2002 |
Financial audit | LU | 21 28 | Correction — application of Regulation (EC) No 1258/1999 — non-compliance with payment deadlines. | EUR | 0,00 | – 1523705,30 | 1523705,30 | 2003 |
| Total LU | | | | – 757602,14 | – 1523705,30 | 766103,16 | |
Animal premia | NL | 22 20, 22 21, 22 22 | Flat-rate correction of 2 % — inadequate implementation of flock registers. | EUR | – 676571,03 | 0,00 | – 676571,03 | 2002-2004 |
Financial audit | NL | 10 49, 18 00, 21 20, 21 24, 21 25, 21 28 | Correction — application of Regulation (EC) No 1258/1999 — non-compliance with payment deadlines. | EUR | – 763843,83 | – 1317551,36 | 553707,53 | 2003 |
| Total NL | | | | – 1440414,86 | – 1317551,36 | – 122863,50 | |
Animal premia | PT | 21 20, 21 21, 21 22, 21 24, 21 25, 21 28 | Flat-rate correction of 5 % for claim year 2000, 2 % for claim year 2001, 5 % for claim years 2002 and 2003 — shortcomings with regard to the I & R system, insufficient control of claims, inadequate administrative cross-checks. | EUR | – 6805576,11 | 0,00 | – 6805576,11 | 2001-2004 |
Animal premia | PT | 22 20, 22 21, 22 22 | Flat-rate correction of 5 % (2 % for some regions for 2001) — inadequate implementation of flock registers, insufficient number of on-the-spot-checks. | EUR | – 5103616,08 | 0,00 | – 5103616,08 | 2001-2003 |
Financial audit | PT | 16 30 | Correction — application of Regulation (EC) No 1258/1999 — non-compliance with payment deadlines. | EUR | – 24809,86 | – 24809,86 | 0,00 | 2003 |
| Total PT | | | | – 11934002,05 | – 24809,86 | – 11909192,19 | |
Intervention storage | UK | 38 00 | Scale of correction: as established in Article 4 of Regulation (EC) No 296/96 — non-compliance with payment deadlines. | GBP | – 523063,45 | 0,00 | – 523063,45 | 2002 |
Financial audit | UK | Miscellaneous | Correction — application of Regulation (EC) No 1258/1999 — non-compliance with payment deadlines. | GBP | – 34151974,35 | – 34151974,35 | 0,00 | 2003 |
| Total UK | | | | – 34675037,80 | – 34151974,35 | – 523063,45 | |
--------------------------------------------------
