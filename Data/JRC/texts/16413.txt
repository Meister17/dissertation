COMMISSION REGULATION (EC) No 216/96 of 5 February 1996 laying down the rules of procedure of the Boards of Appeal of the Office for Harmonization in the Internal Market (Trade Marks and Designs)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 40/94 of 20 December 1994 on the Community trade mark (1), as amended by Regulation (EC) No 3288/94 (2), and in particular Article 140 (3) thereof,
Whereas Regulation (EC) No 40/94 (hereinafter 'the Regulation`) creates a new trade mark system allowing a trade mark having effect throughout the Community to be obtained on the basis of an application to the Office for Harmonization in the Internal Market (Trade Marks and Designs) ('the Office`);
Whereas for this purpose the Regulation contains in particular the necessary provisions for a procedure leading to the registration of a Community trade marks, as well as for the administration of Community trade marks, for appeals against decisions of the Office and for proceedings in relation to revocation or invalidity of a Community trade mark;
Whereas under Article 130 of the Regulation, the Boards of Appeal are to be responsible for deciding on appeals from decisions of the examiners, the Opposition Divisions, the Administration of Trade Marks and Legal Division and the Cancellation Divisions;
Whereas Title VII of the Regulation contains basic principles regarding appeals against decisions of examiners, the Opposition Divisions, the Administration of Trade Marks and Legal Division and the Cancellation Divisions;
Whereas Title X of Commission Regulation (EC) No 2868/95 of 13 December 1995 implementing Council Regulation No 40/94 on the Community Trade Mark (3) contains implementing rules to Title VII of the Regulation;
Whereas this Regulation supplements those other rules, in particular as regards the organization of the Boards and the oral procedure;
Whereas before the beginning of each working year a scheme should be established for the distribution of business between the Boards of Appeal by an Authority established for that purpose; whereas to this end the said Authority should apply objective criteria such as classes of products and services or initial letters of the names of applicants;
Whereas to facilitate the handling and disposal of appeals, a rapporteur should be designated for each case, who should be responsible inter alia for preparing communications with the parties and drafting decisions;
Whereas the parties to proceedings before the Boards of Appeal may not be in a position or may not be willing to bring questions of general relevance to a pending case to the attention of the Boards of Appeal; whereas, therefore, the Boards of Appeal should have the power, of their own motion or pursuant to a request by the President, to invite the President of the Office, to submit comments on questions of general interest in relation to a case pending before the Boards of Appeal;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Committee established under Article 141 of the Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
Allocation of duties and Authority competent to allocate
1. Before the beginning of each working year, duties shall be allocated to the Boards of Appeal according to objective criteria, and the members of each of the Boards and their alternates shall be designated. Any member of a Board of Appeal may be designated for several Boards of Appeal as a member or an alternate. These measures may, where necessary, be amended during the working year in question.
2. The measures referred to in paragraph 1 shall be taken by an Authority composed of the President of the Office as Chairman, the Vice-President of the Office responsible for the Boards of Appeal, the Chairmen of the Boards of Appeal and three other members of the Boards of Appeal elected by the full membership of those Boards, except the Chairmen, for the working year in question. The Authority may validly deliberate only if at least five of its members are present, including the President or the Vice-President of the Office and two Chairmen of Boards of Appeal. Decisions shall be taken by majority vote. In the event of a tie, the vote of the Chairman shall be decisive. The Authority may lay down its internal rules of procedure.
3. The Authority provided for in paragraph 2 shall decide on conflicts regarding the allocation of duties among different Boards of Appeal.
4. Until more than three Boards of Appeal have been set up, the Authority referred to in paragraph 2 shall consist of the President of the Office, who shall act as Chairman, the Vice-President of the Office responsible for the Boards of Appeal, the Chairman or Chairmen of the Boards of Appeal which have already been set up and one other member of the Boards of Appeal elected by their full membership of the Board, except the Chairman or Chairmen, for the working year in question. The Authority may validly deliberate only if at least three of its members are present, including the President or the Vice-President of the Office.
Article 2
Replacement of members
1. Reasons for replacement by alternates shall in particular include leave, sickness, inescapable commitments and the grounds of exclusion set out in Article 132 of the Regulation.
2. Any member asking to be replaced by an alternate shall without delay inform the Chairman of the Board concerned of his unavailability.
Article 3
Exclusion and objection
1. If a Board has knowledge of a possible reason for exclusion or objection under Article 132 (3) of the Regulation which does not originate from a member himself or from any party to the proceedings, the procedure of Article 132 (4) of the Regulation shall be applied.
2. The member concerned shall be invited to present his comments as to whether there is a reason for exclusion or objection.
3. Before a decision is taken on the action to be taken pursuant to Article 132 (4) of the Regulation, there shall be no further proceedings in the case.
Article 4
Rapporteurs
1. The Chairman of each Board shall for each appeal designate a member of his Board, or himself, as rapporteur.
2. The rapporteur shall carry out a preliminary study of the appeal. He may prepare communications to the parties subject to the direction of the Chairman of the Board. Communications shall be signed by the rapporteur on behalf of the Board.
3. The rapporteur shall prepare internal meetings of the Board and the oral proceedings.
4. The rapporteur shall draft decisions.
Article 5
Registries
1. Registries shall be established for the Boards of Appeal. Registrars shall be responsible for the discharge of the functions of the Registries. One of the Registrars may be designated Senior Registrar.
2. The Authority provided for in Article 1 (2) may entrust to the Registrars the performance of functions which involve no legal or technical difficulties, particularly with regard to representation, the submission of translations, inspection of files and notifications.
3. The Registrar shall submit to the Chairman of the Board concerned a report on the admissibility of each newly-filed appeal.
4. Minutes of oral proceedings and of the taking of evidence shall be drawn up by the Registrar or, if the President of the Office has agreed thereto, such other officer of the Office as the Chairman of the Board may designate.
Article 6
Change in the composition of a Board
1. If the composition of a Board is changed after oral proceedings, the parties to the proceedings shall be informed that, at the request of any party, fresh oral proceedings shall be held before the Board in its new composition. Fresh oral proceedings shall also be held if so requested by the new member and if the other members of the Board have given their agreement.
2. The new member shall be bound to the same extent as the other members by an interim decision which has already been taken.
3. If, when a Board has already reached a final decision, a member is unable to act, he shall not be replaced by an alternate. If the Chairman is unable to act, then the member of the Board concerned having the longer service on the Board, or where members have the same length of service, the older member, shall sign the decision on behalf of the Chairman.
Article 7
Joinder of appeal proceedings
1. If several appeals are filed against a decision, those appeals shall be considered in the same proceedings.
2. If appeals are filed against separate decisions and all the appeals are designated to be examined by one Board having the same composition, that Board may deal with those appeals in joined proceedings with the consent of the parties.
Article 8
Remission to the department of first instance
Where the proceedings of the department of first instance whose decision is the subject of an appeal are vitiated by fundamental deficiencies, the Board shall set aside the decision and, unless there are reasons for not doing so, remit the case to that instance or decide the matter itself.
Article 9
Oral proceedings
1. If oral proceedings are to take place, the Board shall ensure that the parties have provided all relevant information and documents before the hearing.
2. The Board may, when issuing the summons to attend oral proceedings, add a communication drawing attention to matters which seem to be of special significance, or to the fact that certain questions appear no longer to be contentious, or containing other observations that may help to concentrate on essentials during the oral proceedings.
3. The Board shall ensure that the case is ready for decision at the conclusion of the oral proceedings, unless there are special reasons to the contrary.
Article 10
Communications to the parties
If a Board deems it expedient to communicate with the parties regarding a possible appraisal of substantive or legal matters, such communication shall be made in such a way as not to imply that the Board is in any way bound by it.
Article 11
Comments on questions of general interest
The Board may, on its own initiative or at the written, reasoned request of the President of the Office, invite him to comment in writing or orally on questions of general interest which arise in the course of proceedings pending before it. The parties shall be entitled to submit their observations on the President's comments.
Article 12
Deliberations preceding decisions
The rapporteur shall submit to the other members of the Board a draft of the decision to be taken and shall set a reasonable time-limit within which to oppose it or to ask for changes. The Board shall meet to deliberate on the decision to be taken if it appears that the members of a Board are not all of the same opinion. Only members of the Board shall participate in the deliberations; the Chairman of the Board concerned may, however, authorize other officers such as registrars or interpreters to attend. Deliberations shall be secret.
Article 13
Order of voting
1. During the deliberations between members of a Board, the opinion of the rapporteur shall be heard first, and, if the rapporteur is not the Chairman, the Chairman last.
2. If voting is necessary, votes shall be taken in the same sequence, save that if the Chairman is also the rapporteur, be shall vote last. Abstentions shall not be permitted.
Article 14
Entry into force
This Regulation shall enter into force the third day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 5 February 1996.
For the Commission
Mario MONTI
Member of the Commission
(1) OJ No L 11, 14. 1. 1994, p. 1.
(2) OJ No L 349, 31. 12. 1994, p. 83.
(3) OJ No L 303, 15. 12. 1995, p. 1.
