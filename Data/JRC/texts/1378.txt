Commission Regulation (EC) No 2590/2001
of 21 December 2001
approving operations to checks conformity to the marketing standards applicable to fresh fruit and vegetables carried out in Switzerland prior to import into the European Community
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 2200/96 of 28 October 1996 on the common organisation of the market in fruit and vegetables(1), as last amended by Commission Regulation (EC) No 911/2001(2), and in particular Article 10 thereof,
Whereas:
(1) Commission Regulation (EC) No 1148/2001 of 12 June 2001 on checks on conformity to the marketing standards applicable to fresh fruit and vegetables(3), as amended by Regulation (EC) No 2379/2001(4), lays down the conditions under which the Commission may approve checking operations performed by certain third countries which so request prior to import into the Community, in compliance with the conditions laid down in Article 7 of Regulation (EC) No 1148/2001.
(2) The Swiss authorities have sent the Commission a request for the approval of checking operations performed by Qualiservice under the responsibility of the Office fédéral de l'agriculture. This states that that establishment has the necessary staff, equipment and facilities to carry out checks, that is uses methods equivalent to those referred to in Article 9 of Regulation (EC) No 1148/2001 and that the fresh fruit and vegetables exported from Switzerland to the Community must meet the Community marketing standards.
(3) The information sent by the Member States and in the possession of the Commission shows that, in the period 1997 to 2000, imports of fresh fruit and vegetables from Switzerland presented a relatively low incidence of non-conformity with the marketing standards.
(4) For a number of years the Swiss inspection bodies, and their supervisory authorities, have been regular participants in international efforts to agree trading standards for fruit and vegetables, such as the Working Party on Standardisation of Perishable Produce and Quality Development of the United Nations Economic Commission for Europe (UNECE) and the OECD Scheme for the Application of International Standards for Fruit and Vegetables.
(5) Imports of fresh fruit and vegetables by Switzerland from the Community are not subject to a quality check prior to release for free circulation on the Swiss market.
(6) Annex 10 to the Agreement on Trade in Agricultural Products between the European Community and the Swiss Confederation stipulates that the controls performed under the responsibility of the Office fédéral de l'agriculture are to be recognised by the Community, including those on products originating in the Community and re-exported from Switzerland into the Community. The relevant provisions of that Agreement should be implemented before they become applicable and consequently a derogation should be granted from Regulation (EC) No 1148/2001 as regards the origin of eligible products.
(7) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Fresh Fruit and Vegetables,
HAS ADOPTED THIS REGULATION:
Article 1
1. Checks on conformity to the marketing standards carried out by Switzerland on fresh fruit and vegetables from Switzerland shall be approved in accordance with the conditions laid down in Article 7(1) of Regulation (EC) No 1148/2001.
2. By derogation from Article 7(1) of Regulation (EC) No 1148/2001, paragraph 1 shall also apply to fruit and vegetables originating in the Community re-exported from Switzerland to the Community.
3. Paragraphs 1 and 2 shall not apply to citrus fruit.
Article 2
The official correspondent in Switzerland, under whose responsibility the checking operations are performed, and the inspection bodies in charge of carrying out those checks, as referred to in Article 7(2) of Regulation (EC) No 1148/2001, are given in Annex I to this Regulation.
Article 3
The certificates referred to in the second subparagraph of Article 7(3) of Regulation (EC) No 1148/2001, issued following the checks referred to in Article 1 of this Regulation, must be drawn up on forms in conformity with the model given in Annex II to this Regulation.
Article 4
This Regulation shall enter into force on the 20th day following its publication in the Official Journal of the European Communities.
It shall apply from the date of publication in the C series of the Official Journal of the European Communities of the notice referred to in Article 7(8) of Regulation (EC) No 1148/2001 relating to the establishment of administrative cooperation between the European Community and Switzerland.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 21 December 2001.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 297, 21.11.1996, p. 1.
(2) OJ L 129, 11.5.2001, p. 3.
(3) OJ L 156, 13.6.2001, p. 9.
(4) OJ L 321, 6.12.2001, p. 15.
ANNEX I
Official correspondent pursuant to Article 7(2) of Regulation (EC) No 1148/2001:
Office fédéral de l'agriculture Département fédéral de l'économie Mattenhofstrasse 5, CH - 3003 Berne Tel. (41-31) 324 84 21 Fax (41-31) 323 05 55
Inspection body pursuant to Article 7(2) of Regulation (EC) No 1148/2001:
Qualiservice Sàrl Kapellenstrasse 5 Case postale 7960 CH - 3001 Berne Tel. (41-31) 385 36 90 Fax (41-31) 385 36 99
ANNEX II
>PIC FILE= "L_2001345EN.002202.TIF">
