COMMISSION REGULATION (EC) No 2333/96 of 5 December 1996 amending Regulation (EC) No 2051/96 laying down certain detailed rules for granting of assistance for the export of beef and veal which may benefit from a special import treatment in Canada and amending Regulation (EC) No 1445/95
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 805/68 of 27 June 1968 on the common organization of the market in beef and veal (1), as last amended by Regulation (EC) No 2222/96 (2), and in particular Articles 9 and 13 thereof,
Whereas Commission Regulation (EC) No 2051/96 (3) lays down certain detailed rules for export of beef to Canada in particular by providing for a quarterly management of certificates; whereas in the light of the Canadian rules of implementation of the WTO tariff quota for beef certain provisions of Commission Regulations (EC) No 2051/96 and (EC) No 1445/95 (4), as last amended by Regulation (EC) No 2051/96, should be amended;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Beef and Veal,
HAS ADOPTED THIS REGULATION:
Article 1
Article 1 (1) of Commission Regulation (EC) No 2051/96 is replaced by the following:
'1. This Regulation establishes certain detailed rules of application in respect of exports to Canada of 5 000 tonnes per calendar year of beef and veal of Community origin, fresh, chilled or frozen, qualifying for special treatment.`
Article 2
Commission Regulation (EC) No 1445/95 is hereby amended as follows:
1. Article 12 a (6) is replaced by the following:
'6. Licence applications may be lodged only in the first five days of each month.`;
2. in the last sentence of Article 12 a (8) the word 'quarter` shall be replaced by the word 'month.`;
3. Article 12 a (9) is replaced by the following:
'9. Licences shall be issued on the 21 st day of each month at the latest.`
Article 3
This Regulation enters into force on the seventh day following its publication in the Official Journal of the European Communities.
It shall apply from 1 January 1997.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 5 December 1996.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ No L 148, 28. 6. 1968, p. 24.
(2) OJ No L 296, 21. 11. 1996, p. 50.
(3) OJ No L 274, 26. 10. 1996, p. 18.
(4) OJ No L 143, 27. 6. 1995, p. 35.
