COUNCIL DECISION of 13 December 1993 on the conclusion of an Agreement in the form of an exchange of letters relating to the Agreement on fisheries between the European Economic Community and the Kingdom of Norway (93/740/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 43 in conjunction with Article 228 (3), first subparagraph thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament (1),
Whereas in the framework of the negotiations on the European Economic Area (EEA) it has been possible to negotiate an Agreement in the form of an exchange of letters between the European Economic Community and the Kingdom of Norway relating to the Agreement on fisheries between the European Economic Community and the Kingdom of Norway;
Whereas this Agreement is part of the overall balance of the results of the EEA negotiations and an essential element for the approval by the Community of the EEA Agreement;
Whereas this Agreement should be approved,
HAS DECIDED AS FOLLOWS:
Article 1
The Agreement in the form of an exchange of letters relating to the Agreement on fisheries between the European Economic Community and the Kingdom of Norway, is hereby approved on behalf of the Community.
The text of the Agreement is attached to this Decision.
Article 2
The President of the Council shall give the notification of the approval of the Agreement by the Community.
Done at Brussels, 13 December 1993.
For the Council
The President
Ph. MAYSTADT
(1) OJ No C 305, 23. 11. 1992, p. 63.
