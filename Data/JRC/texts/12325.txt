Action brought on 29 December 2005 — Bang & Olufsen/OHIM
Parties
Applicant: Bang & Olufsen AS (Struer, Denmark) [represented by: K. Wallberg, lawyer]
Defendant: Office for Harmonisation in the Internal Market (Trade Marks and Designs)
Form of order sought
- The decision taken by the First Board of Appeal of the Office for Harmonisation in the Internal Market (Trade Marks and Designs) on 22 October 2005 in case No. R0497/2005-1 be annulled and
- the defendant pay the costs.
Pleas in law and main arguments
Community trade mark concerned: Three-dimensional mark in form of a vertical pencil-shaped loudspeaker on a low pedestal for goods in classes 9 and 20 — application No 3354371
Decision of the examiner: Refusal of the application for all the claimed goods
Decision of the Board of Appeal: Dismissal of the appeal
Pleas in law: The mark is in accordance with Article 7(1)(b) of Council Regulation No 40/94 inherently distinctive for all the goods covered by the application and if not so, it has acquired distinctiveness through use in accordance with Article 7(3) of the Regulation.
--------------------------------------------------
