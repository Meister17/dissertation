Appeal brought on 13 April 2006 by TEA-CEGOS, SA and Services techniques globaux (STG) SA against the judgment of the Court of First Instance (Second Chamber) delivered on 14 February 2006 in Joined Cases T-376/05 and T-383/05 TEA-CEGOS, SA, STG SA and GHK Consulting Ltd v Commission of the European Communities
Parties
Appellants: TEA-CEGOS, SA, Services techniques globaux (STG) SA (represented by: G. Vandersanden and L. Levi, lawyers)
Other parties to the proceedings: GHK Consulting Ltd, Commission of the European Communities
Form of order sought
The appellants claim that the Court should:
- set aside the judgment of 14 February 2006 of the Court of First Instance in Joined Cases T-376/05 and T-383/05;
- consequently, grant the appellants the relief they claimed at first instance and, therefore,
- annul the decision of 12 October 2005 rejecting the candidature and bid of the TEA-CEGOS consortium and withdrawing the decision awarding the framework contract to the TEA-CEGOS consortium under the call for tenders EuropeAid — 2/119860/C-LOT No 7;
- annul all the other decisions taken by the defendant under that call for tenders following the decision of 12 October 2005 and, in particular, the award decisions and the contracts concluded by the Commission implementing those decisions;
- order the defendant to pay all the costs at first instance and on appeal.
Pleas in law and main arguments
The appellants base their appeal on breach of Community law by, and on procedural irregularities before, the Court of First Instance. The appellants submit that the Court of First Instance disregarded the principle of legal certainty, its obligation to state reasons and the principle of sound administration and distorted the evidence.
--------------------------------------------------
