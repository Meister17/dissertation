Commission Regulation (EC) No 1596/2002
of 6 September 2002
amending Regulation (EC) No 2760/98 concerning the implementation of a programme for cross-border cooperation in the framework of the PHARE programme
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 3906/89 of 18 December 1989 on economic aid to certain countries of central and eastern Europe(1), as last amended by Regulation (EC) No 2500/2001(2), and in particular Article 8 thereof,
Whereas:
(1) The PHARE 2000 review communication "Strengthening preparation for membership" announced a more programme-oriented approach through the use of "schemes" (measures) which allows PHARE cross-border cooperation to co-finance projects similar in size and nature to Interreg projects.
(2) The Commission communication of 28 April 2000 on Interreg III guidelines(3) provides in point 11 and Annex II an indicative list of priority topics and eligible measures related to cross-border cooperation (Interreg III strand A).
(3) The experience gained since the entry into force of Commission Regulation (EC) No 2760/98(4), notably through setting up joint cooperation committees and implementing joint programming documents, has highlighted the need for a further alignment of the eligible actions with Interreg.
(4) Regulation (EC) No 2760/98 should therefore be amended in order to remove the second subparagraph of Article 5(1) thereof, under which certain actions could only be financed under the terms of Article 5(2).
(5) The measures provided for in this Regulation are in accordance with the opinion of the Committee for Economic Restructuring in Certain Countries of Central and Eastern Europe,
HAS ADOPTED THIS REGULATION:
Article 1
In Article 5(1) of Regulation (EC) No 2760/98 the second subparagraph is deleted.
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 6 September 2002.
For the Commission
Günter Verheugen
Member of the Commission
(1) OJ L 375, 23.12.1989, p. 11.
(2) OJ L 342, 27.12.2001, p. 1.
(3) OJ C 143, 23.5.2000, p. 6.
(4) OJ L 345, 19.12.1998, p. 49.
