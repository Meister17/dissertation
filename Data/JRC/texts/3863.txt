Commission Regulation (EC) No 987/2005
of 28 June 2005
altering the export refunds on white sugar and raw sugar exported in the natural state fixed by Regulation (EC) No 846/2005
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1260/2001 of 19 June 2001 on the common organisation of the markets in the sugar sector [1], and in particular the third subparagraph of Article 27(5) thereof,
Whereas:
(1) The export refunds on white sugar and raw sugar exported in the natural state were fixed by Commission Regulation (EC) No 846/2005 [2].
(2) Since the data currently available to the Commission are different to the data at the time Regulation (EC) No 846/2005 was adopted, those refunds should be adjusted,
HAS ADOPTED THIS REGULATION:
Article 1
The export refunds on the products listed in Article 1(1)(a) of Regulation (EC) No 1260/2001, undenatured and exported in the natural state, as fixed in the Annex to Regulation (EC) No 846/2005 are hereby altered to the amounts shown in the Annex to this Regulation.
Article 2
This Regulation shall enter into force on 29 June 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 28 June 2005.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 178, 30.6.2001, p. 1. Regulation as last amended by Commission Regulation (EC) No 39/2004 (OJ L 6, 10.1.2004, p. 16).
[2] OJ L 140, 3.6.2005, p. 6.
--------------------------------------------------
ANNEX
Product code | Destination | Unit of measurement | Amount of refund |
1701 11 90 9100 | S00 | EUR/100 kg | 33,21 |
1701 11 90 9910 | S00 | EUR/100 kg | 31,44 |
1701 12 90 9100 | S00 | EUR/100 kg | 33,21 |
1701 12 90 9910 | S00 | EUR/100 kg | 31,44 |
1701 91 00 9000 | S00 | EUR/1 % of sucrose × 100 kg product net | 0,3610 |
1701 99 10 9100 | S00 | EUR/100 kg | 36,10 |
1701 99 10 9910 | S00 | EUR/100 kg | 34,18 |
1701 99 10 9950 | S00 | EUR/100 kg | 34,18 |
1701 99 90 9100 | S00 | EUR/1 % of sucrose × 100 kg of net product | 0,3610 |
--------------------------------------------------
