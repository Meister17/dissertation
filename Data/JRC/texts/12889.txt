European Agency for Safety and Health at Work — Publication of the final accounts for the financial year 2005
(2006/C 266/07)
The complete version of the final accounts may be found at the following address:
http://osha.europa.eu/about/finance/
Source: Data compiled by the Agency. These tables summarise the data supplied by the Agency in its annual accounts.
Table 1
Implementation of the budget for the financial year 2005
(1000 euro) |
Revenue | Expenditure |
Source of revenue | Revenue entered in the final budget for the financial year | Revenue collected | Expenditure allocation | Final budget appropriations | Appropriations carried over from the previous financial year | Appropriations for the financial year and carried over from the previous financial year |
final | committed | paid | carried over | cancelled | available | paid | cancelled | available | committed | paid | carried over | cancelled |
Community subsidies | 13200 | 12208 | Title I Staff | 4579 | 3796 | 3435 | 75 | 1069 | 60 | 42 | 18 | 4639 | 3856 | 3477 | 75 | 1087 |
Phare | 358 | 340 | Title II Administration | 1501 | 1406 | 1052 | 331 | 118 | 248 | 187 | 61 | 1749 | 1654 | 1239 | 331 | 179 |
Other revenue | 180 | 217 | Title III Operating activities | 7300 | 7090 | 4589 | 2213 | 498 | 2549 | 2224 | 325 | 9849 | 9639 | 6813 | 2213 | 823 |
| | | Phare | 358 | 172 | 133 | 225 | — | 0 | 0 | — | 358 | 172 | 133 | 225 | — |
Total | 13738 | 12765 | Total | 13738 | 12464 | 9209 | 2844 | 1685 | 2857 | 2453 | 404 | 16595 | 15321 | 11662 | 2844 | 2089 |
Table 2
Revenue and expenditure account for the financial year 2005 [1]
(1000 euro) |
| 2005 |
Operating revenue
Community subsidies | 13673 |
Other subsidies/ revenue | 92 |
Total (a) | 13765 |
Operating expenses
Staff | 3312 |
Administration | 1679 |
Operating activities | 8584 |
Total (b) | 13575 |
Operating result (c = a – b) | 190 |
Other income (d) | 0 |
Other charges (e) | 7 |
Economic result (f = c + d – e) | 183 |
Table 3
Balance sheet at 31 December 2005 and 2004 [2]
(1000 euro) |
| 2005 | 2004 |
Assets
Fixed Assets | 339 | 325 |
Receivables | 596 | 426 |
Cash and cash equivalents | 3392 | 3117 |
Total | 4327 | 3868 |
Liabilities
Accumulated surplus/ deficit | 1637 | 1637 |
Economic result for the year | 183 | 0 |
Current liabilities | 2507 | 2231 |
Total | 4327 | 3868 |
[1] Data for the financial year 2004 are not shown as they are not comparable with the data for 2005 because of the change in the accounting principles applied.
[2] Data for the financial year 2004 have been restated to allow comparison with those for 2005.
--------------------------------------------------
