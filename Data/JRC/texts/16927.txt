Commission Regulation (EC) No 365/2004
of 27 February 2004
amending Regulation (EC) No 2233/2003 opening Community tariff quotas for 2004 for sheep, goats, sheepmeat and goatmeat
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 2529/2001 of 19 December 2001 on the common organisation of the market in sheepmeat and goatmeat(1), and in particular Article 16(1) thereof,
Whereas:
(1) Article 11(1) of Commission Regulation (EC) No 1439/1995 of 26 June 1995 laying down detailed rules for the application of Council Regulation (EEC) No 3013/89 as regards the import and export of products in the sheepmeat and goatmeat sector(2) indicates the validity period for documents of origin issued by third country authorities in view of imports into the Community of sheep, goats, sheepmeat and goatmeat under tariff quotas.
(2) Commission Regulation (EC) No 2233/2003(3) introduced the management of those quotas under the first-come, first-served system as of 1 January 2004. However, with regard to certain third countries, that Regulation provides for the continuation of the licence-system until 30 April 2004. In those cases, provisions should be made to allow a smooth transition from the import licence system to the first-come, first-served system.
(3) To that end, a higher degree of flexibility as concerns the period of validity of the document of origin as stipulated in Article 11(1) of Regulation (EC) No 1439/95 should be allowed to the extent that the authorities of the third country concerned may issue such documents with a period of validity of less than three months from the date of issue.
(4) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Sheep and Goats,
HAS ADOPTED THIS REGULATION:
Article 1
In Article 5(1) of Regulation (EC) No 2233/2003, the following subparagraph is added:"By way of derogation from Article 11(1) of Regulation (EC) No 1439/95, the issuing authorities of Australia and New Zealand may, until 30 April 2004, issue documents of origin with a validity period of less than three months from their actual date of issue."
Article 2
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Union.
It shall apply from 1 January 2004.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 27 February 2004.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 341, 22.12.2001, p. 3.
(2) OJ L 143, 27.6.1995, p. 7, Regulation as last amended by Regulation (EC) No 272/2001 (OJ L 41, 10.2.2001, p. 3).
(3) OJ L 339, 24.12.2003, p. 22.
