COUNCIL REGULATION (EURATOM, ECSC, EEC) N° 3911/90 of 21 December 1990 adapting the representation and special-duty allowances for the President and Members of the Commission, the President, Judges, Advocates-General and Registrar of the Court of Justice and the President, Members and Registrar of the Court of First Instance
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to Council Regulation N° 422/67/EEC, N° 5/67/Euratom of 25 July 1967 determining the emoluments of the President and Members of the Commission, of the President, Judges, Advocates-General and Registrar of the Court of Justice and of the President, Members and Registrar of the Court of First Instance(1), as last amended by Regulation (EEC/Euratom/ECSC) N° 3777/89(2), and in particular Article 4 (4) thereof,
Having regard to Council Regulation (ECSC/ EEC/Euratom) N° 4045/88 of 19 December 1988 laying down the emoluments of the President, Members and Registrar of the Court of First Instance of the European Communities(3), and amending accordingly Regulation N° 422/67/EEC, N° 5/67/Euratom,
Whereas the representation and special-duty allowances provided for under Article 4 (2) and (3) and Article 21a (3) of Regulation N° 422/67/EEC, N° 5/67/Euratom should be increased,
HAS ADOPTED THIS REGULATION:
Article 1
With effect from 1 July 1990:
(a)the amounts listed in Article 4 (2) of Regulation N° 422/67/EEC, N° 5/67/Euratom shall be as follows:
President:Bfrs 53 095,
Vice-President:Bfrs 34 120,
Other Members:Bfrs 22 750;
(b)the amounts listed in the first subparagraph of Article 4 (3) of Regulation N° 422/67/EEC, N° 5/67/Euratom shall be as follows:
President:Bfrs 53 095,
Judge or Advocate-General:Bfrs 22 750,
Registrar:Bfrs 20 750;
(c)the amount listed in the second subparagraph of Article 4 (3) of Regulation N° 422/67/EEC,
N° 5/67/Euratom shall be replaced by Bfrs 30 355.
Article 2
With effect from 1 July 1990:
(a)the amounts listed in the first subparagraph of Article 21a (3) of Regulation N° 422/67/EEC, N° 5/67/Euratom shall be as follows:
President:Bfrs 22 750,
Members:Bfrs 20 750,
Registrar:Bfrs 17 645;
(b)the amount listed in the second subparagraph of Article 21a (3) shall be replaced by Bfrs 27 685.
Article 3
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 21 December 1990.
For the CouncilThe PresidentA. RUBERTI
(1)OJ N° L 187, 8. 8. 1967, p. 1.
(2)OJ N° L 367, 16. 12. 1989, p. 1.
(3)OJ N° L 356, 24. 12. 1988, p. 1.
