COUNCIL DIRECTIVE of 25 July 1978 concerning the coordination of provisions laid down by law, regulation or administrative action in respect of the activities of dental practitioners (78/687/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Articles 49, 57, 66 and 235 thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament (1),
Having regard to the opinion of the Economic and Social Committee (2),
Whereas, with a view to achieving the mutual recognition of diplomas, certificates and other evidence of the formal qualifications in dentistry, laid down by Council Directive 78/686/EEC of 25 July 1978 concerning the mutual recognition of diplomas, certificates and other evidence of the formal qualifications of practitioners of dentistry, including measures to facilitate the effective exercise of the right of establishment and freedom to provide services (3), the comparable nature of training courses in the Member States enables coordination in this field to be confined to the requirement that minimum standards be observed, which then leaves the Member States freedom of organization as regards teaching;
Whereas, with a view to mutual recognition of diplomas, certificates and other evidence of formal qualifications of a practitioner of specialized dentistry and in order to put all members of the profession who are nationals of the Member States on an equal footing within the Community, some coordination of the requirements for training as a practitioner of specialized dentistry is necessary ; whereas certain minimum criteria should be laid down for this purpose concerning the right to take up specialized training, the minimum training period, the method by which such training is given and the place where it is to be carried out, as well as the supervision to which it should be subject ; whereas these criteria only concern the specializations common to several Member States;
Whereas it is necessary for reasons of public health to move within the Community towards a common definition of the field of activity of the professional persons concerned ; whereas this Directive does not at this stage enable complete coordination to be achieved as regards the field of activity of dental practitioners in the various Member States;
Whereas Member States will ensure that, as from the implementation of this Directive, the training of dental practitioners will provide them with the skills necessary for carrying out all activities involving the prevention, diagnosis and treatment of anomalies and diseases of the teeth, mouth, jaws and associated tissues;
Whereas coordination of the conditions for the pursuit of these activities, as provided for under this Directive, does not exclude any subsequent coordination;
Whereas the coordination envisaged by this Directive covers the professional training of dental practitioners ; whereas, as far as training is concerned, most Member States do not at present distinguish between dental practioners who pursue their activities as employed persons and those who are self-employed ; whereas for this reason and in order to encourage as far as possible the free movement of professional persons within the Community, it appears necessary to extend the application of this Directive to dental practitioners pursuing their activities as employed persons;
Whereas, at the time of notification of this Directive, dentistry is practised in Italy solely by doctors, whether or not specializing in odontostomatology ; whereas, under this Directive, Italy is obliged to create a new category of professional persons entitled to practise dentistry under a title other than that of doctor ; whereas in creating a new profession Italy must not only introduce a specific system of training complying with the criteria laid down in this Directive, but also set up structures proper to this new profession, such as a Council, for example ; whereas, therefore, in view of the extent of the measures to be taken, Italy should be granted an additional period to allow it to comply with this Directive, (1)OJ No C 101, 4.8.1970, p. 19. (2)OJ No C 36, 28.3.1970, p. 19. (3)See page 1 of this Official Journal.
HAS ADOPTED THIS DIRECTIVE:
CHAPTER I TRAINING REQUIREMENTS
Article 1
1. The Member States shall require persons wishing to take up and pursue a dental profession under the titles referred to in Article 1 of Directive 78/686/EEC to hold a diploma, certificate or other evidence of formal qualifications referred to in Article 3 of the same Directive which guarantees that during his complete training period the person concerned has acquired: (a) adequate knowledge of the sciences on which dentistry is based and a good understanding of scientific methods, including the principles of measuring biological functions, the evaluation of scientifically established facts and the analysis of data;
(b) adequate knowledge of the constitution, physiology and behaviour of healthy and sick persons as well as the influence of the natural and social environment on the state of health of the human being, in so far as these factors affect dentistry;
(c) adequate knowledge of the structure and function of the teeth, mouth, jaws and associated tissues, both healthy and diseased, and their relationship to the general state of health and to the physical and social well-being of the patient;
(d) adequate knowledge of clinical disciplines and methods, providing the dentist with a coherent picture of anomalies, lesions and diseases of the teeth, mouth, jaws and associated tissues and of preventive, diagnostic and therapeutic dentistry;
(e) suitable clinical experience under appropriate supervision.
This training shall provide him with the skills necessary for carrying out all activities involving the prevention, diagnosis and treatment of anomalies and diseases of the teeth, mouth, jaws and associated tissues.
2. A complete period of dental training of this kind shall comprise at least a five-year full time course of theoretical and practical instruction given in a university, in a higher-education institution recognized as having equivalent status or under the supervision of a university and shall include the subjects listed in the Annex.
3. In order to be accepted for such training, the candidate must have a diploma or a certificate which entitles him to be admitted for the course of study concerned to the universities of a Member State or to the higher education institutions recognized as having equivalent status.
4. Nothing in this Directive shall prejudice any facility which may be granted in accordance with their own rules by Member States in respect of their own territory to authorize holders of diplomas, certificates or other evidence of formal qualifications which have not been obtained in a Member State to take up and pursue the activities of a dental practitioner.
Article 2
1. Member States shall ensure that the training leading to a diploma, certificate or other evidence of formal qualifications as a practitioner of specialized dentistry meets the following requirements at least: (a) it shall entail the completion and validation of a five-year full-time course of theoretical and practical instruction within the framework of the training referred to in Article 1, or possession of the documents referred to in Article 7 (1) of Directive 78/686/EEC.
(b) it shall comprise theoretical and practical instruction;
(c) it shall be a full-time course of a minimum of three years' duration supervised by the competent authorities or bodies;
(d) it shall be in a university centre, in a treatment, teaching and research centre or, where appropriate, in a health establishment approved for this purpose by the competent authorities or bodies;
(e) it shall involve the personal participation of the dental practitioner training to be a specialist in the activity and in the responsibilities of the establishments concerned.
2. Member States shall make the award of a diploma, certificate or other evidence of formal qualifications as a practitioner of specialized dentistry subject to the possession of one of the diplomas, certificates or other evidence of formal qualifications in dentistry referred to in Article 1, or to the possession of the documents referred to in Article 7 (1) of Directive 78/686/EEC.
3. Within the time limit laid down in Article 8 Member States shall designate the authorities or bodies competent to issue the diplomas, certificates or other evidence of formal qualifications referred to in paragraph 1.
4. Member States may derogate from paragraph 1 (a). Persons in respect of whom such derogation is made shall not be entitled to avail themselves of Article 4 of Directive 78/686/EEC.
Article 3
1. Without prejudice to the principle of full-time training as set out in Article 2 (1) (c), and until such time as the Council takes a decision in accordance with paragraph 3, Member States may permit part-time specialist training, under conditions approved by the competent national authorities, when training on a full-time basis would not be practicable for well-founded reasons.
2. The total period of specialized training may not be shortened by virtue of paragraph 1. The standard of the training may not be impaired, either by its part-time nature or by the practice of private, remunerated professional activity.
3. Four years at the latest after notification of this Directive and in the light of a review of the situation, acting on a proposal from the Commission, and bearing in mind that the possibility of part-time training should continue to exist in certain circumstances to be examined separately for each specialization, the Council shall decide whether the provisions of paragraphs 1 and 2 should be retained or amended.
Article 4
As a transitional measure and notwithstanding Articles 2 (1) (c) and 3, Member States whose provisions laid down by law, regulation or administrative action permit a method of part-time specialist training at the time of notification of this Directive may continue to apply these provisions to candidates who have begun their training as specialists no later than four years after the notification of this Directive. This period may be extended if the Council has not taken a decision in accordance with Article 3 (3).
CHAPTER II FIELD OF ACTIVITY
Article 5
Member States shall ensure that dental surgeons shall generally be entitled to take up and pursue activities involving the prevention, diagnosis and treatment of anomalies and diseases of the teeth, mouth, jaws and associated tissues in accordance with the regulatory provisions and the rules of professional conduct governing the profession at the time of notification of this Directive.
Those Member States which do not have such provisions or rules may define or limit the pursuit of certain activities referred to in the first subparagraph to an extent which is comparable to that existing in the other Member States.
CHAPTER III FINAL PROVISIONS
Article 6
Persons covered by Article 19 of Directive 78/686/EEC shall be regarded as fulfilling the requirements laid down in Article 2 (1) (a).
For the purposes of applying Article 2 (2), persons covered by Article 19 of Directive 78/686/EEC shall be treated in the same way as those holding one of the diplomas, certificates or other evidence of formal qualifications in dentistry referred to in Article 1.
Article 7
This Directive shall also apply to nationals of Member States who, in accordance with Council Regulation (EEC) No 1612/68 of 15 October 1968 on freedom of movement for workers within the Community (1), are or will be pursuing, as employed persons, any of the activities referred to in Article 1 of Directive 78/686/EEC.
Article 8
1. Member States shall take the measures necessary to comply with this Directive within 18 months of its notification and shall forthwith inform the Commission thereof. However, Italy shall take these measures within a maximum of six years.
2. Member States shall communicate to the Commission the texts of the main provisions of national law which they adopt in the field covered by this Directive.
Article 9
Where a Member State encounters major difficulties in certain fields when applying this Directive, the Commission shall examine these difficulties in conjunction with that State and shall request the opinion of the Committee of Senior Officials on Public Health set up (1)OJ No L 257, 19.10.1968, p. 2.
by Decision 75/365/EEC (1), as last amended by Decision 78/689/EEC (2).
Where necessary, the Commission shall submit appropriate proposals to the Council.
Article 10
This Directive is addressed to the Member States.
Done at Brussels, 25 July 1978.
For the Council
The President
K. von DOHNANYI (1)OJ No L 167, 30.6.1975, p. 19. (2)See page 17 of this Official Journal.
ANNEX Study programme for dental practitioners
The programme of studies leading to a diploma, certificate or other evidence of formal qualifications in dentistry shall include at least the following subjects. One or more of these subjects may be taught in the context of the other disciplines or in conjunction therewith. (a) Basic subjects
chemistry,
physics,
biology.
(b) Medico-biological subjects and general medical subjects
anatomy,
embryology,
histology, including cytology,
physiology,
biochemistry (or physiological chemistry),
pathological anatomy,
general pathology,
pharmacology,
microbiology,
hygiene,
preventive medicine and epidemiology,
radiology,
physiotherapy,
general surgery,
general medicine, including paediatrics,
oto-rhino-laryngology,
dermato-venereology,
general psychology - psychopathology - neuropathology,
anaesthetics.
(c) Subjects directly related to dentistry
prosthodontics,
dental materials and equipment,
conservative dentistry,
preventive dentistry,
anaesthetics and sedation in dentistry,
special surgery,
special pathology,
clinical practice,
paedodontics,
orthodontics,
periodontics,
dental radiology,
dental occlusion and function of the jaw,
professional organization, ethics and legislation,
social aspects of dental practice.
