Commission Regulation (EC) No 2024/2005
of 12 December 2005
amending Council Regulation (EC) No 872/2004 concerning further restrictive measures in relation to Liberia
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 872/2004 concerning further restrictive measures in relation to Liberia [1], and in particular Article 11(a) thereof,
Whereas:
(1) Annex I to Regulation (EC) No 872/2004 lists the natural and legal persons, bodies and entities covered by the freezing of funds and economic resources under that Regulation.
(2) On 30 November 2005, the Sanctions Committee of the United Nations Security Council decided to add two natural persons and several legal persons or entities to the list of persons, groups and entities to whom the freezing of funds and economic resources should apply. Annex I should therefore be amended accordingly.
(3) In order to ensure that the measures provided for in this Regulation are effective, this Regulation must enter into force immediately,
HAS ADOPTED THIS REGULATION:
Article 1
Annex I to Regulation (EC) No 872/2004 is hereby amended as set out in the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 12 December 2005.
For the Commission
Eneko Landáburu
Director-General for External Relations
[1] OJ L 162, 30.4.2004, p. 32. Regulation as last amended by Commission Regulation (EC) No 1453/2005 (OJ L 230, 7.9.2005, p. 14).
--------------------------------------------------
ANNEX
Annex I to Council Regulation (EC) No 872/2004 is amended as follows:
1. The following natural persons shall be added:
(a) Richard Ammar Chichakli (alias Ammar M. Chichakli). Address: (a) 225 Syracuse Place, Richardson, Texas 75081, USA; (b) 811 South Central Expressway Suite 210 Richardson, Texas 75080, USA. Date of birth: 29.3.1959. Place of birth: Syria. Nationality: USA. Other information: (a) Social Security No: 405 41 5342 or 467 79 1065; (b) a certified public accountant and certified fraud examiner; (c) an officer of San Air General Trading.
(b) Valeriy Naydo (alias Valerii Naido). Address: c/o CET Aviation, P.O. Box 932-20C, Ajman, United Arab Emirates. Date of birth: 10.8.1957. Nationality: Ukraine. Passport No: (a) AC251295 (Ukraine), (b) KC024178 (Ukraine). Other information: (a) a pilot, (b) one of the directors of Air Pass (Pietersburg Aviation Services and Systems), (c) Chief Executive Officer of CET Aviation.
2. The following legal persons or entities shall be added:
(a) Abidjan Freight. Address: Abidjan, Côte d’Ivoire.
(b) Air Cess (alias (a) Air Cess Equatorial Guinea, (b) Air Cess Holdings, Ltd., (c) Air Cess Liberia, (d) Air Cess Rwanda, (e) Air Cess Swaziland (Pty.) Ltd., (f) Air Cess, Inc. 360-C, (g) Air Pas, (h) Air Pass, (i) Chess Air Group, (j) Pietersburg Aviation Services & Systems, (k) Cessavia). Address: (a) Malabo, Equatorial Guinea; (b) P.O. Box 7837, Sharjah, United Arab Emirates; (c) P.O. Box 3962, Sharjah, United Arab Emirates; (d) Islamabad, Pakistan; (e) Entebbe, Uganda.
(c) Air Zory (alias (a) Air Zori, (b) Air Zori, Ltd.). Address: (a) 54 G.M. Dimitrov Blvd., BG-1125, Sofia, Bulgaria; (b) 6 Zenas Kanther Str., 1065 Nicosia, Cyprus. Other information: majority shareholder is Sergei Bout.
(d) Airbas Transportation FZE (alias (a) Air Bas, (b) Air Bass, (c) Airbas Transportation, Inc., (d) Aviabas). Address: (a) P.O. Box 8299, Sharjah, United Arab Emirates; (b) 811 S. Central Expressway, Suite 210 Richardson, Texas 75080, USA. Other information: created in 1995 by Sergei Bout.
(e) ATC, Ltd. Address: Gibraltar, United Kingdom.
(f) Bukava Aviation Transport. Address: Democratic Republic of the Congo.
(g) Business Air Services. Address: Democratic Republic of the Congo.
(h) Centrafrican Airlines (alias (a) Centrafricain Airlines, (b) Central African Airways, (c) Central African Air, (d) Central African Airlines). Address: (a) P.O. Box 2760, Bangui, Central African Republic; (b) c/o Transavia Travel Agency, P.O. Box 3962, Sharjah, United Arab Emirates; (c) P.O. Box 2190, Ajman, United Arab Emirates; (d) Kigali, Rwanda; (e) Ras-al-Khaimah, United Arab Emirates.
(i) Central Africa Development Fund. Address: (a) 811 S. Central Expressway, Suite 210, Richardson, Texas 75080, USA; (b) P.O. Box 850431, Richardson, Texas 75085, USA.
(j) CET Aviation Enterprise (FZE). Address: (a) P.O. Box 932 — C20, Ajman, United Arab Emirates; (b) Equatorial Guinea.
(k) Chichakli & Associates, PLLC (alias (a) Chichakli Hickman-Riggs & Riggs, PLLC, (b) Chichakli Hickmanriggs & Riggs). Address: 811 S. Central Expressway, Suite 210, Richardson, Texas 75080, USA. Other information: an accounting and auditing firm.
(l) Continue Professional Education, Inc. (alias Gulf Motor Sales). Address: 811 S. Central Expressway, Suite 210, Richardson, Texas 75080, USA.
(m) Daytona Pools, Inc. Address: 225 Syracuse Place, Richardson, Texas 75081, USA.
(n) DHH Enterprise, Inc. Address: 811 S. Central Expressway, Suite 210, Richardson, Texas 75080, USA.
(o) Gambia New Millennium Air Company (alias (a) Gambia New Millennium Air, (b) Gambia Millennium Airline). Address: State House, Banjul, Gambia.
(p) IB of America Holdings, Inc. Address: 811 S. Central Expressway, Suite 210, Richardson, Texas 75080, USA.
(q) Irbis Air Company. Address: ul. Furmanova 65, Office 317, Almaty, Kazakhstan 48004. Other information: created in 1998.
(r) Moldtransavia SRL. Address: Aeroport MD-2026, Chisinau, Moldova.
(s) Nordic, Ltd. (alias Nordik Limited EOOD). Address: 9 Fredrick J. Curie Street, Sofia, Bulgaria 1113.
(t) Odessa Air (alias Okapi Air). Address: Entebbe, Uganda.
(u) Orient Star Cooperation (alias Orient Star Aviation). Address: 811 S. Central Expressway, Suite 210, Richardson, Texas 75080, USA.
(v) Richard A. Chichakli, P.C. Address: (a) 811 S. Central Expressway, Suite 210, Richardson, Texas 75080, USA; (b) P.O. Box 850432, Richardson, Texas 75085, USA.
(w) Rockman, Ltd. (alias Rokman EOOD). Address: 9 Fredrick J. Curie Street, Sofia, Bulgaria 1113.
(x) San Air General Trading FZE (alias San Air General Trading, LLC). Address: (a) P.O. Box 932-20C, Ajman, United Arab Emirates; (b) P.O. Box 2190, Ajman, United Arab Emirates; (c) 811 S. Central Expressway, Suite 210, Richardson, Texas 75080, USA. Other information: General manager is Serguei Denissenko.
(y) Santa Cruz Imperial Airlines. Address: (a) P.O. Box 60315, Dubai, United Arab Emirates; (b) Sharjah, United Arab Emirates.
(z) Southbound, Ltd. Address: P.O. Box 398, Suite 52 and 553 Monrovia House, 26 Main Street, Gibraltar, UK.
(aa) Trans Aviation Global Group, Inc. Address: 811 S. Central Expressway, Suite 210, Richardson, Texas 75080, USA.
(bb) Transavia Network (alias (a) NV Trans Aviation Network Group, (b) TAN Group, (c) Trans Aviation, (d) Transavia Travel Agency, (e) Transavia Travel Cargo). Address: (a) 1304 Boorj Building, Bank Street, Sharjah, United Arab Emirates; (b) P.O. Box 3962, Sharjah, United Arab Emirates; (c) P.O. Box 2190, Ajman, United Arab Emirates; (d) Ostende Airport, Belgium.
(cc) Vial Company. Address: Delaware, USA.
(dd) Westbound, Ltd. Address: P.O. Box 399, 26 Main Street, Gibraltar, UK.
--------------------------------------------------
