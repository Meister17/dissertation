COMMISSION DECISION of 28 July 1981 amending Decisions 80/790/EEC, 80/799/EEC, 80/800/EEC, 80/801/EEC, 80/804/EEC and 80/805/EEC concerning animal health conditions and veterinary certification for the importation of fresh meat from Finland, Sweden, Norway, Australia, Canada and New Zealand respectively (81/662/EEC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Directive 72/462/EEC of 12 December 1972 on health and veterinary inspection problems upon importation of bovine animals and swine and fresh meat from third countries (1), as last amended by Directive 77/98/EEC (2), and in particular Article 16 thereof,
Whereas Commission Decisions 80/790/EEC, 80/799/EEC, 80/800/EEC, 80/801/EEC, 80/804/EEC and 80/805/EEC of 25 July 1980 concerning animal health conditions and veterinary certification for the importation of fresh meat from Finland (3), Sweden (4), Norway (5), Australia (6), Canada (7) and New Zealand (8) respectively laid down models of animal health certificates to accompany consignments of fresh meat;
Whereas it is necessary to modify the wording of the attestation of health concerning porcine, ovine and caprine brucellosis in order to bring it into line with Community provisions already adopted in respect of other non-member countries;
Whereas the measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
The Annexes to Decisions 80/790/EEC, 80/799/EEC, 80/800/EEC, 80/801/EEC, 80/804/EEC and 80/805/EEC are hereby amended by replacing the second and third indents of paragraph IV (Attestation of health) by the following:
" - in the case of fresh meat from swine, animals which have not come from holdings which for health reasons are subject to prohibition as a result of an outbreak of porcine brucellosis during the previous six weeks;
- in the case of fresh meat from sheep and goats, animals which have not come from holdings which for health reasons are subject to prohibition as a result of an outbreak of ovine or caprine brucellosis during the previous six weeks
."
Article 2
This Decision shall apply not later than 1 January 1982.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 28 July 1981.
For the Commission
The President
Gaston THORN
(1) OJ No L 302, 31.12.1972, p. 28. (2) OJ No L 26, 31.1.1977, p. 81. (3) OJ No L 233, 4.9.1980, p. 47. (4) OJ No L 234, 5.9.1980, p. 35. (5) OJ No L 234, 5.9.1980, p. 38. (6) OJ No L 234, 5.9.1980, p. 41. (7) OJ No L 236, 9.9.1980, p. 25. (8) OJ No L 236, 9.9.1980, p. 28.
