COUNCIL DECISION of 28 April 1987 concerning the conclusion of a Convention between the European Economic Community and the Republic of Austria, the Republic of Finland, the Republic of Iceland, the Kingdom of Norway, the Kingdom of Sweden and the Swiss Confederation on the simplification of formalities in trade in goods (87/267/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 113 thereof,
Having regard to the recommendation from the Commission,
Whereas the conclusion of a Convention with Austria, Finland, Iceland, Norway, Sweden and Switzerland with a view to introducing, in trade between the Community and those countries, as well as between those countries themselves, a single administrative document replacing the present declarations, must enable the formalities to be completed in such trade to be eased and simplified; whereas it is therefore appropriate to conclude such a Convention;
Whereas this Convention falls within the framework of follow-up action to the Joint Declaration made in Luxembourg on 9 April 1984 by the Ministers of the Member States of the Community, the European Free Trade Association (EFTA) and the Commission expressing their political will to extend cooperation between the Community and these countries, 'with the aim of creating a dynamic European economic space of benefit to their countries',
HAS DECIDED AS FOLLOWS:
Article 1
The Convention between the European Economic Community and the Republic of Austria, the Republic of
Finland, the Republic of Iceland, the Kingdom of Norway, the Kingdom of Sweden and the Swiss Confederation on the simplification of formalities in trade in goods is hereby approved on behalf of the Community.
The text of the Convention is attached to this Decision.
Article 2
The Community shall be represented in the Joint Committee provided for in Article 10 of the Convention by the Commission, assisted by representatives of the Member States.
Article 3
The President of the Council shall deposit the acts provided for in Article 17 of the Convention (1).
Done at Luxembourg, 28 April 1987.
For the Council
The President
P. DE KEERSMAEKER
EWG:L999UMBE00.96
FF: 9UEN; SETUP: 01; Hoehe: 385 mm; 58 Zeilen; 2501 Zeichen;
Bediener: MARK Pr.: C;
Kunde: ................................
(1) The date of entry into force of the Convention will be published in the Official Journal of the European Communities by the General Secretariat of the Council.
