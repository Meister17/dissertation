State aid — Hungary
State aid No C 41/2005 (ex NN 49/2005) — Hungarian Stranded Costs
Invitation to submit comments pursuant to Article 88(2) of the EC Treaty
(2005/C 324/05)
(Text with EEA relevance)
By means of the letter dated 9 November 2005 reproduced in the authentic language on the pages following this summary, the Commission notified Hungary of its decision to initiate the procedure laid down in Article 88(2) of the EC Treaty concerning the abovementioned measures.
Interested parties may submit their comments on measures in respect of which the Commission is initiating the procedure within one month of the date of publication of this summary and the following letter, to:
Commission of the European Communities
Competition DG, State aid Greffe
B-1049 Brussels
Fax: (32-2) 296 12 42
These comments will be communicated to Hungary. Confidential treatment of the identity of the interested party submitting the comments may be requested in writing, stating the reasons for the request.
TEXT OF SUMMARY
1. PROCEDURE
The Commission has decided to register the present file on its own initiative in order to assess the possible existence of State aid in the meaning of Article 87(1) of the Treaty and its compatibility with the common market.
By letter dated 24 May 2005, the Commission asked complementary information from the Hungarian authorities. The reply, dated 20 July 2005, was registered by the Commission on 25 July 2005.
2. DESCRIPTION OF THE MEASURES
In the mid-90s', the main objective of the Hungarian State in the energy sector was to ensure the security of supply and the modernisation of the infrastructure of power generation. In order to achieve these objectives, the 99,9 % State owned network operator "MVM" entered into long term Power Purchase Agreements ("PPAs") with power generators that would invest in Hungary.
Under these PPAs, MVM has the obligation to buy a fixed quantity of electricity at a fixed price, thereby guaranteeing a return on investment to the generators with no risk. The PPAs also include 8 to 10 % of guaranteed profit. The PPAs were signed between 1995 and 2001 and end between 2010 and 2020 depending on the generators.
At present, PPAs cover 85 % of the Hungarian electricity demand.
The main investors that entered into PPAs with the Hungarian network operator and still benefit from those agreements are: RWE (Germany), EDF (France), Electrabel (Belgium), ATEL (Suisse) and AES (USA).
Under the government decree No 183/2002 (VIII.23.) on stranded costs, MVM has the obligation to initiate the renegotiation of the purchase quantities fixed by the PPAs. Due to the advantageous conditions of the PPAs guaranteeing a return on investment and profit with no risk, none of the power generators accepted such a renegotiation that would lead to a decrease in the guaranteed purchase quantities.
3. ASSESSMENT OF THE MEASURE
Existence of State aid
The PPAs are likely to provide a selective competitive advantage to the contracting power generators, which can distort competition and affect trade between Member States.
At this stage of the procedure, the Commission is of the view that the terms and conditions of the PPAs put the power generators, parties to a PPA, in a more advantageous economic situation than other power generators that are not covered by PPAs and than other comparable sectors of activities where such long term agreements were not even proposed to the market players. The measure therefore confers a selective advantage to those generators.
The electricity markets have been opened to competition and electricity is traded between Member States in particular since the entry into force of Directive 96/92/EC of the European Parliament and of the Council of 19 December 1996 concerning common rules for the internal market in electricity [1].
Measures that favour undertakings of the energy sector in one Member State may therefore impede the ability for undertakings from other Member States to export electricity to the former, or favour export of electricity to the latter. This is especially true for Hungary that is centrally located in Europe and is connected or easily connectable to several current and future Member States.
Furthermore, the generators benefiting from the PPAs are major international groups that are present in several Member States. Conferring a competitive advantage to those groups has the potential to affect trade and distort competition.
At present, the Commission is also of the view that this advantage is granted via State resources, as the signature of the PPAs was a State policy carried out through the fully State owned network operator MVM. In line with the Court's case law [2], when a publicly owned company uses its funds in a way which is imputable to the State, then these funds should be qualified as State resources in the meaning of Article 87(1) of the EC Treaty.
In light of the above, at this stage of the procedure, it seems likely that the PPAs constitute State aid to the power generators, within the meaning of Article 87(1) of the EC Treaty.
Compatibility of the measure with the common market
The Commission's Methodology for analysing State aid linked to Stranded costs (the "Methodology") would be the tool used to analyse the State aid that the generators receive. Based on the documents at our disposal at present, the Commission has doubts as to the compatibility of the PPAs with the criteria of the Methodology.
Firstly, the Commission has doubts that the very principles of long term power purchase agreement foreclosing a significant part of the market could be compatible with the mere objectives of the Methodology, which is to increase the pace of the liberalisation of the sector by granting fair compensations to incumbents that are faced with competition on unequal feet.
Secondly, the Commission has doubts that the aid element that is included in the PPAs is compatible with the detailed criteria of the Methodology as regards computation of eligible stranded costs and attribution of actual compensations.
4. CONCLUSION
The Commission initiates a formal investigation procedure in line with Article 88(2) of the EC Treaty with respect to the PPAs that were in force at the date of accession of Hungary to the EU ( 1 May 2004).
The Commission requests the Republic of Hungary to provide all information that may help the assessment of this measure.
In accordance with Article 14 of Council Regulation (EC) No 659/1999, all unlawful aid can be subject to recovery from the recipient.
All interested parties are invited to submit their comments within one month of the date of such publication.
TEXT OF LETTER
"1. ELJÁRÁS
2004. március 31-i keltezésű, ugyanaznap iktatott levelükben a magyar hatóságok tájékoztatták a Bizottságot a közüzemi nagykereskedő által viselt költségek kompenzációs rendszeréről, a Cseh Köztársaság, az Észt Köztársaság, a Ciprusi Köztársaság, a Lett Köztársaság, a Litván Köztársaság, a Magyar Köztársaság, a Máltai Köztársaság, a Lengyel Köztársaság, a Szlovén Köztársaság és a Szlovák Köztársaság Európai Unióhoz történő csatlakozásáról szóló szerződés IV. melléklete (3) bekezdésének 1 c) albekezdésében hivatkozott eljárás ("ideiglenes eljárás") szerint. A bejelentést a Bizottság a HU 1/2004. állami támogatás iktatási szám alatt regisztrálta.
Az intézkedésre vonatkozóan a magyar hatóságok és a Bizottság között több alkalommal is hivatalos levélváltásra került sor. A Bizottság egyéb érdekelt felektől is kapott észrevételeket.
2005. április 13-i keltezésű, 2005. április 15-én iktatott levelükben a magyar hatóságok visszavonták a 2004. március 31-i keltezésű bejelentésüket, azzal a szándékkal, hogy egy későbbi időpontban várhatóan új rendszert jelentenek be.
Amíg az új rendszer kialakítására és bevezetésére sor nem kerül, a villamos energia ágazat eredeti szerkezete továbbra is fennáll abban a formájában, ahogy Magyarország Európai Unióhoz történő csatlakozása idején. Mivel ez a fennálló rendszer a Bizottság megítélése szerint, a rendelkezésére álló iratok alapján, az EK-Szerződés 87. cikkének (1) bekezdése szerinti állami támogatást tartalmaz, a Bizottság az ügyet NN 49/2005. szám alatt hivatalból nyilvántartásba vette.
2005. május 24-i keltezésű levelében (D/54013) a Bizottság az ügy tartalmát illető kérdéseket intézett a magyar hatóságokhoz. A magyar hatóságok 2005. július 20-i keltezésű válaszát a Bizottság 2005. július 25-én vette nyilvántartásba.
2. AZ INTÉZKEDÉS LEÍRÁSA
2.1. Az intézkedés háttere
Az 1994. évi XLVIII. számú törvény a villamos energiáról (a "Villamos Energia Törvény") értelmében a magyar közüzemi nagykereskedő, a 99,9 százalékban állami tulajdonú Magyar Villamos Művek ("MVM") köteles volt felmérni a szolgáltatók teljes energiaszükségletét, és ez alapján kidolgozni az úgynevezett Országos Erőműépítési Tervet. E tervet a felelős miniszter köteles volt elfogadásra előterjeszteni a magyar kormány és az Országgyűlés részére.
Ebben az időszakban a magyar energiapiacon a legsürgősebb probléma a villamosenergia-ellátás biztonságának a legkisebb költség elve melletti megteremtése, az infrastruktúra korszerűsítése – különös figyelemmel a mindenkori környezetvédelmi normákra-, valamint az áramszolgáltatási ágazat szükséges átszervezése volt. E célkitűzések megvalósítása leghatékonyabb módjának az tűnt, ha hosszú távú villamos energia vásárlási megállapodást ("HTM") ajánlanak fel azon befektetőknek, akik vállalják, hogy befektetnek a magyarországi erőművek építésébe és modernizálásába.
2.2. A HTM-ek
Az aláírás időpontjától függően kétféle HTM létezik.
- azok, amelyeket 1995-1996 folyamán kötöttek, a privatizációs folyamat keretében, és a privatizációs szerződések részét képezik. Ezek odaítélésére nem versenytárgyalási eljárás keretén belül került sor;
- azok, amelyeket 1997-2001 között írtak alá, versenytárgyalási eljárás keretein belül.
Mindegyik HTM-et egyrészről az MVM, másrészről egy erőmű írt alá.
A HTM-ek értelmében az MVM köteles meghatározott mennyiségű villamos energiát megvenni az erőműtől, az összes költség fedezésén túl 8-10 százalékos profitot biztosító áron. A HTM-ek garantált átvételi mennyisége a magyar villamos energia piacának összességében mintegy 81-87 százalékát teszi ki (a 2000-2004 közötti időszakra számolva). A fennmaradó szükségleteket túlnyomó részt import fedezi.
2003.12.31-ig a HTM-ekben előirányzott árrögzítő mechanizmus nem került alkalmazásra, mivel az elektromos áram árát miniszteri rendeletek rögzítették. Ezen időpontig a Magyar Energia Hivatal elemezte az egyes erőművek költségstruktúráját, és úgy határozta meg az MVM által az erőművektől megvásárolandó elektromos áram árát, hogy az fedezze az erőművek költségeit, és garantáljon egy előre rögzített nyereséget.
Ezen árszabályozási mechanizmus során figyelembe vett költségnemek a következők voltak: személyi jellegű költségek, hitelkamatok, értékcsökkenés, biztosítás, egyéb ráfordítások, rendkívüli ráfordítások, előre meghatározottan nem árkalkulációs tényezők, tüzelőanyag-költség, vásárolt villamos energia, rekultivációs költségek, környezetvédelmi költségek, Központi Nukleáris Alap befizetés (az atomerőműnél), befagyott költségek, önfogyasztás és értékesített hőenergia költségei.
A magyar hatóságok és az erőművektől kapott információk szerint, a Magyar Energia Hivatal hatósági árszabályozásának megszűnését követően a szerződéses felek lényegében megtartották az árrendeletekben alkalmazott árszabályozási módszert, mivel az megfelelőnek bizonyult az erőművek költségeinek kiszámítására.
Az alábbi táblázat a 2004-ben HTM alatt álló erőműveket sorolja fel, feltüntetve az erőművek tulajdonosát és annak többségi részvényesét.
Az erőmű tulajdonosa és fő részvényese | Erőművek |
Budapesti Erőmű Rt. (EDF) | Kelenföldi Erőmű |
| Újpesti Erőmű |
| Kispesti Erőmű |
Dunamenti Erőmű Rt. (Electrabel) | Dunamenti Erőmű |
Mátrai Erőmű Rt. (RWE) | Mátrai Erőmű |
AES-Tisza Erőmű Kft. (AES) | Tisza Erőmű |
Csepeli Áramtermelő Kft. (ATEL) | Csepel II Erőmű |
Paski Atomerőmű Rt. (MVM) | Paksi Atomerőmű |
Pannonpower Holding Rt. | Pécsi Erőmű |
[1] OJ L 27, 30.1.1997, p. 20.
[2] See the "Stardust ruling": Case C-482/99 of 16 May 2002 of the Court of Justice
--------------------------------------------------
