Judgment of the Court (First Chamber) of 14 September 2006 — Commission of the European Communities v Hellenic Republic
(Case C-82/05) [1]
Parties
Applicant: Commission of the European Communities (represented by: M. Patakia, Agent)
Defendant: Hellenic Republic (represented by: N. Dafniou and M. Apessos, Agents)
Re:
Failure of a Member State to fulfil obligations — Infringement of Article 28 EC — National legislation considering the "bake-off" process (thawing and reheating of pre-baked and frozen bread) to be a process of manufacturing bread and allowing only bakeries to sell bread manufactured in that way.
Operative part of the judgment
The Court:
1. Declares that, by treating the process of final baking or reheating of "bake-off" products in the same way as the full process of manufacturing bread and by making it subject to the conditions prescribed by the national legislation with regard to bakeries, the Hellenic Republic has failed to fulfil its obligations under Article 28 EC.
2. Orders the Hellenic Republic to pay the costs.
[1] OJ C 93, 16.04.2005
--------------------------------------------------
