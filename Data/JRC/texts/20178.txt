COUNCIL REGULATION (EC, EURATOM, ECSC) No 840/95 of 10 April 1995 amending Regulation (EEC, Euratom, ECSC) No 2290/77 determining the emoluments of the members of the Court of Auditors
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 188 b (8) thereof,
Having regard to the Treaty establishing the European Coal and Steel Community, and in particular Article 45b (8) thereof,
Having regard to the Treaty establishing the European Atomic Energy Community, and in particular Article 160b (8) thereof,
Whereas it is for the Council to determine the salaries, allowances and pensions of the President and members of the Court of Auditors;
Whereas, following the entry into force of the Treaty on European Union, the Court of Auditors has become an institution of the European Communities and it therefore seems desirable to amend the provisions of Regulation (EEC, Euratom, ECSC) No 2290/77 (1) with regard to the salaries and transitional termination-of-service allowances,
HAS ADOPTED THIS REGULATION:
Article 1
1. Regulation (EEC, Euratom, ECSC) No 2290/77 is hereby amended as follows: Article 2 shall be replaced by the following:
'Article 2
The basic monthly salary of Members of the Court of Auditors shall be equal to the amount resulting from the application of the following percentages to the basic salary of an official of the European Communities on the last step of Grade A 1:
President: 115 %,
Other members: 108 %`.
2. Article 8 (1) shall be replaced by the following:
'1. For three years from the first day of the month following that in which he ceases to hold office, a former member of the Court of Auditors shall receive a monthly transitional allowance determined in accordance with the following procedure:
- 40 % of the basic salary which he was receiving when he ceased to hold office if his period of service is less than two years,
- 45 % of the same salary if that period is over two years but less than three years,
- 50 % of the same salary if that period is over three years but less than five years,
- 55 % of the same salary if that period is over five years but less than 10 years,
- 60 % of the same salary if that period is over 10 years but less than 15 years,
- 65 % of the same salary in other cases.`
Article 2
Pensions acquired on the date of entry into force of this Regulation shall not be altered by this Regulation.
Article 3
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Communities.
It shall apply from the first date of the month following its publication.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Luxembourg, 10 April 1995.
For the Council
The President
A. JUPPÉ
(1) OJ No L 268, 20. 10. 1977, p. 1. Regulation as last amended by Regulation (EEC, Euratom, ECSC) No 1084/92 (OJ No L 117, 1. 5. 1992, p. 1).
