Order of the Court of First Instance of 7 September 2005 — Krahl
v Commission
(Case T-358/03) [1]
Parties
Applicant(s): Sigfried Krahl (Zagreb, Croatia) (represented by: S. Orlandi, A. Coolen, J.-N. Louis and E. Marchel, lawyers)
Defendant(s): Commission of the European Communities (represented by: J. Currall and H. Krämer, Agents)
Application for
annulment of the Commission's decision refusing to reimburse all the accommodation expenses incurred by the applicant following his posting to Zagreb
Operative part of the Order
1. The action is dismissed as inadmissible;
2. Each of the parties shall bear its own costs.
[1] OJ C 7, 10.1.2004.
--------------------------------------------------
