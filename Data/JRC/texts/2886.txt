Report
on the annual accounts of the European Medicines Agency for the financial year 2004 together with the Agency's replies
(2005/C 332/02)
CONTENTS
1-2 INTRODUCTION
3-6 THE COURT'S OPINION
7-9 OBSERVATIONS
Tables 1 to 4
The Agency's replies
INTRODUCTION
1. The European Medicines Agency (hereinafter referred to as the Agency) was created by Council Regulation (EEC) No 2309/93 of 22 July 1993 which was replaced by Regulation (EC) No 726/2004 of the European Parliament and of the Council of 31 March 2004 [1]. The Agency operates through a network and coordinates the scientific resources made available by the national authorities in order to ensure the evaluation and supervision of medicinal products for human or veterinary use. Table 1 summarises the powers and activities of the Agency on the basis of the information supplied by it.
2. For information, the annual accounts drawn up by the Agency for the financial year 2004 are summarised in Tables 2, 3 and 4.
THE COURT'S OPINION
3. The Court's opinion is addressed to the European Parliament and the Council in accordance with Article 185(2) of Council Regulation (EC, Euratom) No 1605/2002 [2]; it was drawn up following an examination of the Agency's accounts, as required by Article 248 of the Treaty establishing the European Community.
4. The Agency's accounts for the financial year ended 31 December 2004 [3] were drawn up by its Executive Director, pursuant to Article 68 of Regulation (EC) No 726/2004, and sent to the Court, which is required to give its opinion on their reliability and on the legality and regularity of the underlying transactions.
5. The Court conducted an audit in accordance with its policies and standards, which are based on international auditing standards that have been adapted to the Community context. The audit was planned and performed to obtain reasonable assurance that the accounts are reliable and the underlying transactions are legal and regular.
6. The Court has obtained a reasonable basis for the opinion expressed below.
Reliability of the accounts
The Agency's accounts for the financial year ended 31 December 2004 are, in all material respects, reliable.
Legality and regularity of the underlying transactions
The transactions underlying the Agency's annual accounts, taken as a whole, are legal and regular.
The observations which follow do not call the Court's opinion into question.
OBSERVATIONS
7. The Agency's Management Board has set up an Audit Advisory Committee to advise the Executive Director on matters regarding quality assurance and risk-mitigating strategies. The existence of such a body and its operating procedures, including the recruitment procedure for its members, must be provided for, on account of its permanent nature, in the rules which govern the Agency's internal organisation.
8. The contracts concluded with the banks have been in force for over five years even though the detailed rules for the implementation of the Agency's financial regulation lay down that there should be a new invitation to tender at least once every five years [4].
9. The Court notes that the Agency's new financial regulation, as finally adopted after receipt of the Commission's opinion, takes account of the observations which the Court made in its previous report [5]. Similarly, the system for managing the Agency's fixed assets has been considerably improved [6].
This Report was adopted by the Court of Auditors in Luxembourg at its meeting of 5 October 2005.
For the Court of Auditors
Hubert Weber
President
[1] OJ L 214, 24.8.1993, p. 1 and OJ L 136, 30.4.2004, p. 1. Pursuant to the latter Regulation the Agency's original name, European Agency for the Evaluation of Medicinal Products, was changed to European Medicines Agency.
[2] OJ L 248, 16.9.2002, p. 1.
[3] These accounts were drawn up on 29 September 2005 and received by the Court on 29 September 2005.
[4] Article 53(3).
[5] See paragraph 7 of the Report for the financial year 2003 (OJ C 324, 30.12.2004, p. 31).
[6] See paragraph 10 of the Report for the financial year 2003).
--------------------------------------------------
Table 1
European Medicines Agency (London)
Source: Information supplied by the Agency.
Areas of Community competence deriving from the Treaty | Competences of the Agency Regulation (EC) No 726/2004 of the European Parliament and of the Council of 31 March 2004 | Governance | Resources made available to the Agency (2003 figures) | Products and services (2003 figures) |
A high level of human health protection shall be ensured in the definition and implementation of all Community policies and activities. Community action, which shall complement national policies, shall be directed towards improving public health, preventing human illness and diseases and obviating sources of danger to human health. (…) (Article 152 of the Treaty) | Objectives To coordinate the scientific resources that the Member States’ authorities make available to the Agency for the authorisation and supervision of medicinal products for human and veterinary use;to provide the Member States and the institutions of the European Union with scientific advice on medicinal products for human or veterinary use. | Tasks To coordinate the scientific evaluation of medicinal products which are subject to Community marketing authorisation procedures;to coordinate the supervision of medicinal products which have been authorised within the Community; (pharmacovigilance);to advise on the maximum limits for residues of veterinary medicinal products which may be accepted in foodstuffs of animal origin;to coordinate verification of compliance with the principles of good manufacturing practice, good laboratory practice and good clinical practice;to record the status of marketing authorisations granted for medicinal products. | 1.The Committee for Proprietary Medicinal Products, consisting of two members from each Member State, advises on any question relating to the evaluation of medicinal products for human use.2.The Committee for Veterinary Medicinal Products, consisting of two members from each Member State, advises on any question relating to the evaluation of veterinary medicinal products.3.The Management Board, consisting of two representatives of each Member State, two representatives of the Commission and two representatives appointed by the European Parliament. The Board adopts the work programme and the annual report.4.The Executive Director is appointed by the Management Board on a proposal from the Commission.5.External audit: European Court of Auditors6.Discharge is given by the Parliament on a recommendation from the Council. | Final budget: 99,1 million euro (84,2 million euro) including the Community contribution (excluding subsidy for orphan medicines): 24,7 % (22,9 %) Staff numbers at 31 December 2004: 314 (287) posts provided for in the establishment plan Posts occupied: 290 (256) + 50 (48) other posts (auxiliary contracts, seconded national experts, local staff, employment agency staff) Total staff: 340 (304) Assigned to the following duties: operational: 271 (242) administrative: 69 (62) | Medicinal products for human use Applications for marketing authorisations: 51 (39) Favourable opinions: 34 (39) Average evaluation time: 187 days (190 days) Opinions after authorisation: 926 (941) Pharmacovigilance: 64186 reports (45538 reports) Periodic reliability reports: 253 (276) Monitoring measures: 948 (1025) Scientific opinions: 77 (65) Procedures for mutual recognition: 7081 (4080) Veterinary medicinal products New applications: 8 (10) Applications in respect of variants: 40 (64) Inspection: 93 (76) |
Table 2
European Medicines Agency — Implementation of the budget for the financial year 2004
Source: The Agency's data — This table summarises the data provided by the Agency in its annual accounts.
(1000 euro) |
Revenue | Expenditure |
Origin of revenue | Revenue entered in the final budget for the financial year | Revenue collected | Allocation of expenditure | Appropriations in the final budget | Appropriations carried over from the previous financial year | Appropriations available (2004 budget and financial year 2003) |
entered | committed | paid | carried over | cancelled | outstanding commitments | paid | cancelled | appropriations | committed | paid | carried over | cancelled |
Community subsidies | 29073 | 29022 | Title I Staff | 36064 | 34151 | 33509 | 642 | 1913 | 534 | 424 | 110 | 36598 | 34685 | 33933 | 642 | 2023 |
Own revenue | 67000 | 67350 | Title II Administration | 23994 | 23878 | 12943 | 10935 | 116 | 7265 | 6749 | 516 | 31259 | 31143 | 19692 | 10935 | 632 |
Other revenue | 3016 | 3013 | Title III Operating activities | 39031 | 38686 | 27512 | 11174 | 345 | 8316 | 7878 | 438 | 47347 | 47002 | 35390 | 11174 | 783 |
Total | 99089 | 99385 | Total | 99089 | 96715 | 73964 | 22751 | 2374 | 16115 | 15051 | 1064 | 115204 | 112830 | 89015 | 22751 | 3438 |
Table 3
European Medicines Agency — Revenue and expenditure account for the financial years 2004 and 2003
Source: The Agency's data — This table summarises the data provided by the Agency in its annual accounts.
(1000 euro) |
| 2004 | 2003 |
Revenue
Fees relating to marketing authorisations | 68412 | 58657 |
Commission subsidy including contributions received from the EEA | 20529 | 19786 |
Community subsidy for orphan medicines | 4026 | 2814 |
Contributions for Community programmes | 0 | 1208 |
Administrative revenue | 1973 | 1703 |
Sundry revenue | 1473 | 1788 |
Total (a) | 96413 | 85956 |
Expenditure
Staff expenditure | 34333 | 29663 |
Administrative expenditure | 11224 | 10835 |
Operating expenditure | 38573 | 32838 |
Depreciation | 3650 | 2364 |
Other expenses | 280 | 0 |
Total (b) | 88060 | 75700 |
Operating result (c = a – b) | 8353 | 10256 |
Financial result (e) | 1160 | 676 |
Economic outturn (f = c + e) | 9513 | 10932 |
Table 4
European Medicines Agency — Balance sheet at 31 December 2004 and 31 December 2003
Source: The Agency's data — This table summarises the data provided by the Agency in its annual accounts.
(1000 euro) |
Assets | 2004 | 2003 | Liabilities | 2004 | 2003 |
Intangible assets | 5109 | 3401 | Own capital | | |
| | | Outturn carried over from previous financial years | 13767 | 6872 |
Fixed assets | | | Economic outturn | 9513 | 10932 |
Plant, machinery and tools | 2480 | 1635 | Subtotal | 23280 | 17804 |
Furniture and vehicle fleet | 1375 | 1011 | | | |
Computer equipment | 3151 | 2548 | Current liabilities | | |
Subtotal | 7006 | 5194 | Amounts owed in social security charges | 320 | 0 |
| | | Amounts owed to Community institutions and bodies | 4910 | 479 |
Current assets | | | Current liabilities | 13151 | 11936 |
VAT paid and to be recovered | 1888 | 1105 | Other accounts payable | 106 | 127 |
Amounts owed by Community institutions and bodies | 148 | 107 | Advances from customers | 11250 | 8845 |
Sundry accounts receivable | 1795 | 1034 | Subtotal | 29737 | 21387 |
Sundry receivables and advances | 2061 | 64 | | | |
Subtotal | 5892 | 2310 | | | |
Available assets | 35010 | 28286 | | | |
Total | 53017 | 39191 | Total | 53017 | 39191 |
--------------------------------------------------
THE AGENCY'S REPLIES
7. The Audit Advisory Committee is a consultative body and has no operational role in the internal organisation of the Agency. On 4 February 2005 the Management Board of the agency adopted the Terms of Reference of the Audit Advisory Committee which include the mission statement of this committee as well as the rules of proceedings.
8. The Agency has had to implement a wide-ranging reform of the Financial Regulation and Accounting procedures over the last few years. It was considered prudent not to seek a change in the main bank at the same time due to the integration of our systems with this bank's electronic payment system. Now that the Agency is in the final stage of implementing the new Financial Regulation, a call for tender will be launched in the last quarter of this year. However it should be noted that substantial reductions in bank transfer costs have been achieved through direct negotiations with the bank and automation of payments. Also placements of funds are subject to individual bids from up to three banks based on the market rates on a particular day.
9. The Agency notes with satisfaction that these points, mentioned in the Court's report on the 2003 accounts, have been resolved.
--------------------------------------------------
