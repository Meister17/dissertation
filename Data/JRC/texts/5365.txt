Commission Decision
of 23 December 2005
on the continuation in the year 2006 of Community comparative trials and tests on propagating material of Paeonia spp. and Geranium spp. under Council Directive 98/56/EC started in 2005
(2005/945/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 98/56/EC of 20 July 1998 on the marketing of propagating material of ornamental plants [1],
Having regard to Commission Decision 2005/2/EC of 27 December 2004 setting out the arrangements for Community comparative trials and tests on propagating material of certain species under Council Directive 98/56/EC for the years 2005 and 2006 [2], and in particular Article 3 thereof,
Whereas:
(1) Decision 2005/2/EC sets out the arrangements for the comparative trials and tests to be carried out under Directive 98/56/EC as regards Paeonia spp. and Geranium spp. for 2005 and 2006.
(2) Tests and trials carried out in 2005 should be continued in 2006,
HAS DECIDED AS FOLLOWS:
Sole Article
Community comparative trials and tests which began in 2005 on propagating material of Paeonia spp. and Geranium spp. shall be continued in 2006 in accordance with Decision 2005/2/EC.
Done at Brussels, 23 December 2005.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 226, 13.8.1998, p. 16. Directive as last amended by Directive 2003/61/EC (OJ L 165, 3.7.2003, p. 23).
[2] OJ L 1, 4.1.2005, p. 12.
--------------------------------------------------
