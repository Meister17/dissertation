COMMISSION REGULATION (EC) No 2052/97 of 20 October 1997 amending Regulation (EC) No 1501/95 laying down certain detailed rules for the application of Council Regulation (EEC) No 1766/92 on the granting of export refunds on cereals and the measures to be taken in the event of disturbance on the market for cereals
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 1766/92 of 30 June 1992 on the common organization of the market in cereals (1), as last amended by Commission Regulation (EC) No 923/96 (2), and in particular Article 16 thereof,
Whereas Commission Regulation (EC) No 1501/95 (3), as last amended by Regulation (EC) No 1259/97 (4), lays down the measures which may be adopted pursuant to Article 16 of Regulation (EEC) No 1766/92; whereas, in order to clarify a number of questions regarding responsibilities and procedure, the text of Article 17 of Regulation (EC) No 1501/95 should be redrafted;
Whereas the Management Committee for Cereals has not delivered an opinion within the time limit set by its chairman,
HAS ADOPTED THIS REGULATION:
Article 1
Article 17 of Regulation (EC) No 1501/95 is hereby replaced by the following:
'Article 17
The measures referred to in Article 15 shall be adopted in accordance with the procedure laid down in Article 23 of Regulation (EEC) No 1766/92. However, in emergencies, the Commission may adopt the measures.`
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 20 October 1997.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ L 181, 1. 7. 1992, p. 21.
(2) OJ L 126, 24. 5. 1996, p. 37.
(3) OJ L 147, 30. 6. 1995, p. 7.
(4) OJ L 174, 2. 7. 1997, p. 10.
