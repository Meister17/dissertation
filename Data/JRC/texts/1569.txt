Commission Decision
of 23 May 2005
definitively allocating between the Member States for 2005 Community Tobacco Fund resources for financing the measures indicated in Articles 13 and 14 of Regulation (EC) No 2182/2002
(notified under document number C(2005) 1513)
(Only the Dutch, French, German, Greek, Italian, Portuguese and Spanish versions are authentic)
(Text with EEA relevance)
(2005/397/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2075/92 of 30 June 1992 on the common organisation of the market in raw tobacco [1] and in particular Article 14a thereof,
Whereas:
(1) Articles 13 and 14 of Commission Regulation (EC) No 2182/2002 of 6 December 2002 laying down detailed rules for the application of Council Regulation (EEC) No 2075/92 with regard to the Community Tobacco Fund [2] specify measures to promote switching of production. These are to be financed from the Community Tobacco Fund set up under Article 13 of Regulation (EEC) No 2075/92.
(2) Total resources available from the Community Tobacco Fund for 2005 amount to EUR 28,8 million, 50 % of which is to be allocated to specific measures relating to switching by tobacco growers to other crops or other economic activities generating employment, and to studies on these topics.
(3) Under Article 17(4) of Regulation (EC) No 2182/2002, the amount available for 2005 should therefore be allocated between the Member States before 31 May 2005 on the basis of the provisional plans notified by them for funding the measures in the applications for aid.
(4) The measures provided for in this Decision are in accordance with the opinion of the Management Committee for Tobacco,
HAS ADOPTED THIS DECISION:
Article 1
The definitive allocation for 2005 between the Member States of the Community Tobacco Fund resources for financing the measures indicated in Articles 13 and 14 of Regulation (EC) No 2182/2002 is annexed hereto.
Article 2
This Decision is addressed to the Kingdom of Belgium, the Federal Republic of Germany, the Hellenic Republic, the Kingdom of Spain, the French Republic, the Italian Republic, the Republic of Austria and the Portuguese Republic.
Done at Brussels, 23 May 2005.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 215, 30.7.1992, p. 70. Regulation last amended by Regulation (EC) No 2319/2003 (OJ L 345, 31.12.2003, p. 17).
[2] OJ L 331, 7.12.2002, p. 16. Regulation amended by Regulation (EC) No 480/2004 (OJ L 78, 16.3.2004, p. 8).
--------------------------------------------------
ANNEX
(EUR) |
Basis | Definitive allocation |
90 % of the quantities irrevocably bought back under the quotas | 10 % of the national guarantee threshold |
Member State | Value | Value |
Belgium | 99062 | 3260 |
Germany | 0 | 48876 |
Greece | 8972907 | 388757 |
Spain | 0 | 184000 |
France | 0 | 120000 |
Italy | 1961308 | 662236 |
Austria | 0 | 0 |
Portugal | 379265 | 25069 |
Total | 11412542 | 1432198 |
--------------------------------------------------
