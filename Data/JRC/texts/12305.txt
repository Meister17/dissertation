Interest rate applied by the European Central Bank to its main refinancing operations [1]:
3,30 % on 1 December 2006
Euro exchange rates [2]
5 December 2006
(2006/C 296/01)
| Currency | Exchange rate |
USD | US dollar | 1,3331 |
JPY | Japanese yen | 152,74 |
DKK | Danish krone | 7,4559 |
GBP | Pound sterling | 0,67430 |
SEK | Swedish krona | 9,0495 |
CHF | Swiss franc | 1,5885 |
ISK | Iceland króna | 90,65 |
NOK | Norwegian krone | 8,1300 |
BGN | Bulgarian lev | 1,9558 |
CYP | Cyprus pound | 0,5781 |
CZK | Czech koruna | 28,019 |
EEK | Estonian kroon | 15,6466 |
HUF | Hungarian forint | 255,50 |
LTL | Lithuanian litas | 3,4528 |
LVL | Latvian lats | 0,6980 |
MTL | Maltese lira | 0,4293 |
PLN | Polish zloty | 3,8140 |
RON | Romanian leu | 3,4415 |
SIT | Slovenian tolar | 239,66 |
SKK | Slovak koruna | 35,535 |
TRY | Turkish lira | 1,9330 |
AUD | Australian dollar | 1,6946 |
CAD | Canadian dollar | 1,5204 |
HKD | Hong Kong dollar | 10,3598 |
NZD | New Zealand dollar | 1,9422 |
SGD | Singapore dollar | 2,0509 |
KRW | South Korean won | 1232,25 |
ZAR | South African rand | 9,5170 |
CNY | Chinese yuan renminbi | 10,4272 |
HRK | Croatian kuna | 7,3617 |
IDR | Indonesian rupiah | 12151,87 |
MYR | Malaysian ringgit | 4,7412 |
PHP | Philippine peso | 66,095 |
RUB | Russian rouble | 34,8930 |
THB | Thai baht | 47,489 |
[1] Rate applied to the most recent operation carried out before the indicated day. In the case of a variable rate tender, the interest rate is the marginal rate.
[2] Source: reference exchange rate published by the ECB.
--------------------------------------------------
