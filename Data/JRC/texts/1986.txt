Ex-post publicity of Eurostat grants in 2004
(2005/C 55/05)
In accordance with Article 110(2) of the Financial Regulation and Article 169 of its implementing rules, the public is hereby informed of the actions which benefited from a Eurostat grant during 2004.
The electronic file covering the actions in question can be found in the form of a list on the EUROPA server (http://europa.eu.int). This list can be accessed via "Institutions" — "European Commission" — "Statistics" — "other activities" — "Calls for tenders and grants" and then by entering the file "Eurostat — List of grants awarded in 2004".
The list indicates the file number, the unit concerned, the name and the country of the beneficiary, the title of the action, the amount of the commitment (grant) and the percentage of co-financing.
All these actions were the subject of ex-ante publicity.
--------------------------------------------------
