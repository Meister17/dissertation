Commission Regulation (EC) No 1911/2005
of 23 November 2005
amending Annex I to Council Regulation (EEC) No 2377/90 laying down a Community procedure for the establishment of maximum residue limits of veterinary medicinal products in foodstuffs of animal origin, as regards flugestone acetate
(Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2377/90 of 26 June 1990 laying down a Community procedure for the establishment of maximum residue limits of veterinary medicinal products in foodstuffs of animal origin [1], and in particular Article 2 thereof,
Having regard to the opinion of the European Medicines Agency formulated by the Committee for Medicinal Products for Veterinary Use,
Whereas:
(1) All pharmacologically active substances which are used within the Community in veterinary medicinal products intended for administration to food-producing animals should be evaluated in accordance with Regulation (EEC) No 2377/90.
(2) Flugestone acetate has been included in Annex I to Regulation (EEC) No 2377/90 for ovine and caprine, for milk, for intravaginal use and for zootechnical purposes only. That substance has also been included in Annex III to that Regulation for muscle, fat, liver and kidney, for ovine and caprine species, for therapeutic and zootechnical purposes only, awaiting completion of scientific studies. These studies have now been completed and flugestone acetate should therefore be inserted in Annex I to that Regulation for the same purposes and target tissues as in Annex III.
(3) Regulation (EEC) No 2377/90 should be amended accordingly.
(4) An adequate period should be allowed before the applicability of this Regulation in order to enable Member States to make any adjustment which may be necessary in the light of this Regulation to the marketing authorisations granted in accordance with Directive 2001/82/EC of the European Parliament and of the Council of 6 November 2001 on the Community code relating to veterinary medicinal products [2].
(5) The measures provided for in this Regulation are in accordance with the opinion of the Standing Committee on Veterinary Medicinal Products,
HAS ADOPTED THIS REGULATION:
Article 1
Annex I to Regulation (EEC) No 2377/90 is amended in accordance with the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Union.
It shall apply from 23 January 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 23 November 2005.
For the Commission
Günter Verheugen
Vice-President
[1] OJ L 224, 18.8.1990, p. 1. Regulation as last amended by Commission Regulation (EC) No 1518/2005 (OJ L 244, 20.9.2005, p. 11).
[2] OJ L 311, 28.11.2001, p. 1. Directive as last amended by Directive 2004/28/EC (OJ L 136, 30.4.2004, p. 58).
--------------------------------------------------
ANNEX
A. The following substance(s) is(are) inserted in Annex I (list of pharmacologically active substances for which maximum residue limits have been fixed)
"6. Agents acting on the reproductive system
6.1. Progestagens
Pharmacologically active substance(s) | Marker residue | Animal species | MRLs | Target tissues |
Flugestone acetate | Flugestone acetate | Ovine, caprine | 0,5 μg/kg | Muscle |
0,5 μg/kg | Fat |
0,5 μg/kg | Liver |
0,5 μg/kg | Kidney |
[1] For therapeutic and zootechnical purposes only."
--------------------------------------------------
