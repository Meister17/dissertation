COUNCIL REGULATION (EEC) No 2155/91 of 20 June 1991 laying down particular provisions for the application of Articles 37, 39 and 40 of the Agreement between the European Economic Community and the Swiss Confederation on direct insurance other than life assurance
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular the last sentence of Article 57 (2) and Article 235 thereof,
Having regard to the proposal from the Commission (1),
In cooperation with the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas an Agreement between the European Economic Community and the Swiss Confederation on direct insurance other than life assurance was signed at Luxembourg on
10 October 1989;
Whereas under that Agreement a Joint Committee is to
be set up to administer the Agreement, ensure that
it is properly implemented and take decisions in
the circumstances provided for therein; whereas the Community's representatives on the Joint Committee have to be designated and particular provisions have to be adopted
concerning the determination of the Community's positions in the Committee,
HAS ADOPTED THIS REGULATION:
Article 1
The Community shall be represented on the Joint Committee provided for in Article 37 of the Agreement by the Commission, assisted by representatives of the Member States.
Article 2
The Community's position in the Joint Committee shall be adopted by the Council acting by a qualified majority on a proposal from the Commission.
With regard to the adoption of decisions to be taken by the Joint Committee pursuant to Articles 37, 39 and 40 of the Agreement, the Commission shall submit proposals to the Council, which shall act by a qualified majority.
Article 3
This Regulation shall enter into force on the day following that of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Luxembourg, 20 June 1991.
For the Council
The President
R. GOEBBELS
(1) OJ No C 53, 5. 3. 1990, p. 46.
(2) OJ No C 72, 18. 3. 1991, p. 175, and Decision of 12 June 1991 (not yet published in the Official Journal).
(3) OJ No C 56, 7. 3. 1990, p. 27.
