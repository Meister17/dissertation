Commission Regulation (EC) No 116/2006
of 24 January 2006
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 25 January 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 24 January 2006.
For the Commission
J. L. Demarty
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 386/2005 (OJ L 62, 9.3.2005, p. 3).
--------------------------------------------------
ANNEX
to Commission Regulation of 24 January 2006 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code | Standard import value |
07020000 | 052 | 112,0 |
204 | 54,8 |
212 | 97,4 |
624 | 120,2 |
999 | 96,1 |
07070005 | 052 | 136,4 |
204 | 101,5 |
999 | 119,0 |
07091000 | 220 | 77,3 |
999 | 77,3 |
07099070 | 052 | 115,9 |
204 | 136,2 |
999 | 126,1 |
08051020 | 052 | 51,0 |
204 | 56,8 |
212 | 50,0 |
220 | 50,0 |
624 | 58,5 |
999 | 53,3 |
08052010 | 204 | 77,7 |
999 | 77,7 |
08052030, 08052050, 08052070, 08052090 | 052 | 67,2 |
204 | 100,4 |
400 | 88,1 |
464 | 148,0 |
624 | 72,6 |
662 | 32,0 |
999 | 84,7 |
08055010 | 052 | 44,9 |
220 | 60,5 |
999 | 52,7 |
08081080 | 400 | 123,8 |
404 | 102,3 |
720 | 67,7 |
999 | 97,9 |
08082050 | 388 | 104,4 |
400 | 80,2 |
720 | 47,4 |
999 | 77,3 |
--------------------------------------------------
