Reference for a preliminary ruling from the Finanzgericht München lodged on 13 April 2006 — RUMA GmbH
v Oberfinanzdirektion Nürnberg
Referring court
Finanzgericht München
Parties to the main proceedings
Applicant: RUMA GmbH
Defendant: Oberfinanzdirektion Nürnberg
Question referred
Is the Combined Nomenclature (CN), in the version of Annex I to Commission Regulation (EC) No 1789/2003 [1] of 11 September 2003 amending Annex I to Council Regulation (EEC) No 2658/87 on the tariff and statistical nomenclature and on the Common Customs Tariff, to be interpreted as meaning that keypads which have non-conductive contact pins on the underside are to be classified under heading 8538?
[1] OJ 2003 L 281, p. 1.
--------------------------------------------------
