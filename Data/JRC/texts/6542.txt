Commission Regulation (EC) No 971/2005
of 24 June 2005
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 25 June 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 24 June 2005.
For the Commission
J. M. Silva Rodríguez
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 1947/2002 (OJ L 299, 1.11.2002, p. 17).
--------------------------------------------------
ANNEX
to Commission Regulation of 24 June 2005 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code [1] | Standard import value |
07020000 | 052 | 62,6 |
204 | 35,2 |
999 | 48,9 |
07070005 | 052 | 85,7 |
999 | 85,7 |
07099070 | 052 | 89,1 |
999 | 89,1 |
08055010 | 388 | 66,5 |
528 | 56,5 |
624 | 71,1 |
999 | 64,7 |
08081080 | 388 | 93,3 |
400 | 102,8 |
508 | 107,1 |
512 | 67,0 |
524 | 46,4 |
528 | 63,7 |
720 | 47,6 |
804 | 93,7 |
999 | 77,7 |
08091000 | 052 | 187,8 |
624 | 188,8 |
999 | 188,3 |
08092095 | 052 | 266,1 |
068 | 148,4 |
400 | 325,6 |
999 | 246,7 |
08093010, 08093090 | 052 | 157,0 |
999 | 157,0 |
08094005 | 624 | 166,0 |
999 | 166,0 |
[1] Country nomenclature as fixed by Commission Regulation (EC) No 750/2005 (OJ L 126, 19.5.2005, p. 12). Code "999" stands for "of other origin".
--------------------------------------------------
