SIXTH COMMISSION DIRECTIVE 95/32/EC
of 7 July 1995
relating to methods of analysis necessary for checking the composition of cosmetic products
(Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 76/768/EEC of 27 July 1976 on the approximation of the laws of the Member States relating to cosmetic products (1), as last amended by Commission Directive 94/32/EC (2), and in particular Article 8 (1) thereof,
Whereas Directive 76/768/EEC provides for the official testing of cosmetic products with the aim of ensuring that the conditions laid down by Commission provisions concerning the composition of cosmetic products are satisfied;
Whereas all the necessary methods of analysis should be laid down as quickly as possible; whereas certain methods have already been adopted in Commission Directives 80/1335/EEC (3), as amended by Directive 87/143/EEC (4), 82/434/EEC (5), as amended by Directive 90/207/EEC (6), 83/514/EEC (7), 85/490/EEC (8) and 93/73/EEC (9);
Whereas the identification and determination of benzoic acid, 4-hydroxybenzoic acid, sorbic acid, salicylic acid and propionic acid in cosmetic products and the identification and determination of hydroquinone, hydroquinone monomethylether, hydroquinone monoethylether and hydroquinone monobenzylether in cosmetic products constitute a sixth step;
Whereas the measures provided for in this Directive are in accordance with the opinion of the Committee on the adaptation of Directive 76/768/EEC to technical progress,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Member States shall take all the necessary steps to ensure that during official testing of cosmetic products:
- identification and determination of benzoic acid, 4-hydroxybenzoic acid, sorbic acid, salicylic acid and propionic acid,
- identification and determination of hydroquinone, hydroquinone monomethylether, hydroquinone monoethylether and hydroquinone monobenzylether,
shall be carried out in accordance with the methods described in the Annex.
Article 2
1. Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive no later than 30 September 1996. They shall forthwith inform the Commission thereof.
When Member States adopt these provisions, these shall contain a reference to this Directive or shall be accompanied by such reference at the time of their official publication. The procedure for such reference shall be adopted by Member States.
2. Member States shall communicate to the Commission the provisions of national law which they adopt in the field covered by this Directive.
Article 3
This Directive shall enter into force on the 20th day following its publication in the Official Journal of the European Communities.
Article 4
This Directive is addressed to the Member States.
Done at Brussels, 7 July 1995.
For the Commission
Emma BONINO
Member of the Commission
(1) OJ No L 262, 27. 9. 1976, p. 169.
(2) OJ No L 181, 15. 7. 1994, p. 31.
(3) OJ No L 383, 31. 12. 1980, p. 27.
(4) OJ No L 57, 27. 2. 1987, p. 56.
(5) OJ No L 185, 30. 6. 1982, p. 1.
(6) OJ No L 108, 28. 4. 1990, p. 92.
(7) OJ No L 291, 24. 10. 1983, p. 9.
(8) OJ No L 295, 7. 11. 1985, p. 30.
(9) OJ No L 231, 14. 9. 1993, p. 34.
ANNEX
I. IDENTIFICATION AND DETERMINATION OF BENZOIC ACID, 4-HYDROXYBENZOIC ACID, SORBIC ACID, SALICYLIC ACID AND PROPIONIC ACID IN COSMETIC PRODUCTS
1. Scope and field of application
The method is applicable to the identification and determination of benzoic acid, 4-hydroxybenzoic acid, sorbic acid, salicylic acid and propionic acid in cosmetic products. Separate procedures describe the identification of these preservatives; the determination of propionic acid; and the determination of 4-hydroxybenzoic acid, salicylic acid, sorbic acid and benzoic acid.
2. Definition
The amounts of benzoic acid, 4-hydroxybenzoic acid, salicylic acid, sorbic acid and propionic acid determined by this method are expressed as percentage by mass of the free acids.
A. IDENTIFICATION
1. Principle
Following acid/base extraction of the preservatives, the extract is analysed by thin layer chromatography (TLC) employing on-date derivatization. Depending on the results, the identification is confirmed by high performance liquid chromatography (HPLC) or, in the case of propionic acid, by gas chromatography (GC).
2. Reagents
2.1. General
All reagents must be of analytical purity. Water used must be distilled water, or water of at least equivalent purity
2.2. Acetone
2.3. Diethyl ether
2.4. Acetonitrile
2.5. Toluene
2.6. n-Hexane
2.7. Paraffin, liquid
2.8. Hydrochloric acid, 4 M
2.9. Potassium hydroxide, aqueous, 4 M
2.10. Calcium chloride, CaCl2.2H2O
2.11. Lithium carbonate, Li2CO3
2.12. 2-Bromo-2&prime;-acetonaphthone
2.13. 4-Hydroxybenzoic acid
2.14. Salicylic acid
2.15. Benzoic acid
2.16. Sorbic acid
2.17. Propionic acid
2.18. Reference solutions
Prepare 0,1 % (m/v) solutions (100 mg/100 ml) of each of the five preservatives (2.13 to 2.17) in diethyl ether
2.19. Derivatization reagent
0,5 % (m/v) solution of 2-bromo-2&prime;-acetonaphthone (2.12) in acetonitrile (2.4) (50 mg/10 ml). This solution should be prepared weekly and stored in a refrigerator
2.20. Catalyst solution
0,3 % (m/v) solution of lithium carbonate (2.11) in water (300 mg/100 ml). This solution should be freshly prepared
2.21. Development solvent
Toluene (2.5)/Acetone (2.2) (20:0,5, v/v)
2.22. Liquid paraffin (2.7)/n-hexane (2.6) (1:2, v/v)
3. Apparatus
Ordinary laboratory equipment
3.1. Water bath, capable of maintaining a temperature of 60 °C
3.2. Developing tank
3.3. Ultraviolet light source, 254 and 366 nm
3.4. Thin layer plates, Kieselgel 60, without fluorescence indicator, 20 × 20 cm, layer thickness 0,25 mm with concentrating zone 2,5 × 20 cm (Merck 11845, or equivalent)
3.5. Microsyringe, 10 µl
3.6. Microsyringe, 25 µl
3.7. Oven, capable of maintaining temperatures up to 105 °C
3.8. 50-ml glass tubes with screw cap
3.9. Filter paper, diameter 90 mm, Schleicher & Schull, Weissband No 5892, or equivalent
3.10. Universal pH indicator paper, pH 1-11
3.11. 5-ml glass sample vials
3.12. Rotating film evaporator (Rotavapor or equivalent)
3.13. Hot plate
4. Procedure
4.1. Sample prepartion
Weigh approximately 1 g of the sample into a 50-ml glass tube with screw cap (3.8). Add four drops of hydrochloric acid 4 M (2.8) and 40 ml acetone (2.2). For strongly basic products such as toilet soap, 20 drops of hydrochloric acid 4 M (2.8) should be added. Check that the pH is approximately two, using indicator paper (3.10). Close the tube and shake vigorously for one minute.
If necessary to facilitate the extrraction of the preservatives into the acetone phase, heat the mixture gently to about 60 °C to melt the liquid phase.
Cool the solution to room temperature and filter through a filter paper (3.9) into a conical flask.
Transfer 20 ml of the filtrate to a 200-ml conical flask, add 20 ml water and mix. Adjust the pH of the mixture to approximately 10 with potassium hydroxide 4 M (2.9), using indicator paper (3.10) to measure the pH.
Add 1 g calcium chloride (2.10) and shake vigorously. Filter through a filter paper (3.9) into a 250-ml separating funnel containing 75 ml diethyl ether (2.3) and shake vigorously for one minute. Allow to separate and draw off the aqueous layer into a 250 ml conical flask. Discard the ether layer. Using indicator paper (3.10), adjust the pH of the aqueous solution to approximately two with hydrochloric acid 4 M (2.8). Add 10 ml diethyl ether (2.3), stopper the flask and shake vigorously for one minute. allow to separate and transfer the ether layer to a rotating film evaporator (3.12). Discard the aqueous layer.
Evaporate the ether layer almost to dryness and redissolve the residue in 1 ml of diethyl ether (2.3). Transfer the solution to a sample vial (3.11).
4.2. Thin layer chromatography
For each of the references and the samples to be chromatographed, apply approximately 3µl lithium carbonate solution (2.20) and with a syringe (3.5) at equal distances on the start line in the concentration zone of a TLC plate (3.4) and dry in a stream of cold air.
Transfer the TLC plate to a hot plate (3.13), heated at 40 °C, in order to keep the spots as small as possible. With a microsyringe (3.5) apply 10 µl of each of the reference solutions (2.18) and the sample solution (4.1) to the start line of the plate, on the exact spots where the lithium carbonate solution has been applied.
Finally apply approximately 15 µl derivatization reagent (2.19) (2-bromo-2&prime;-acetonaphthone solution), again on the exact spots where the reference/sample solutions and the lithium carbonate solution have been applied.
Heat the TLC plate in an oven (3.7) at 80 °C for 45 minutes. After cooling, develop the plate in a tank (3.2), that has equilibrated for 15 minutes (without the use of filter paper lining), using development solvent 2.21 (toluene/acetone), until the solvent front has reached a distance of 15 cm (this may take approximately 80 minutes).
Dry the plate in a stream of cold air and examine the spots obtained under UV light (3.3). To enhance the fluorescence of the weak spots, the TLC plate may be dipped in liquid paraffin/n-hexane (2.22).
5. Identification
Calculate the Rf for each spot.
Compare the Rf and the behaviour under UV radiation obtained for the sample with that obtained for the reference solutions.
Draw a preliminary conclusion about the presence and identity of the preservatives present. Perform the HPLC described in Section B, or, when it appears that propionic acid is present, the GC described in Section C. Compare the retention times obtained with those of the reference solutions.
Combine the results from the TLC and HPLC or GC and base the final identification of the preservatives present in the sample on the combined results.
B. DETERMINATION OF BENZOIC ACID, 4-HYDROXYBENZOIC ACID, SORBIC ACID AND SALICYLIC ACID
1. Principle
After acidification, the sample is extracted with a mixture of ethanol and water. Following filtration the preservatives are determined by high performance liquid chromatography (HPLC).
2. Reagents
2.1. All reagegnts must be of analytical purity, and suitable for HPLC where appropriate. Water used must be distilled water, or water of at least equivalent purity
2.2. Ethanol, absolute
2.3. 4-Hydroxybenzoic acid
2.4. Salicylic acid
2.5. Benzoic acid
2.6. Sorbic acid
2.7. Sodium acetate, (CH3COONa.3H2O)
2.8. Acetic acid, (á)²04= 1,05 g/ml
2.9. Acetonitrile
2.10. Sulfuric acid, 2 M
2.11. Potassium hydroxide, aqueous, 0,2 M
2.12. 2-Methoxybenzoic acid
2.13. Ethanol/water mixture
Mix nine volumes of ethanol (2.2) and one volume of water (2:1)
2.14. Internal standard solution
Prepare a solution containing approximately 1 g 2-methoxybenzoic acid (2.12) in 500 ml ethanol/water mixture (2.13)
2.15. Mobile phase for HPLC
2.15.1. Acetate buffer: to 1 l of water add 6,35 g sodium acetate (2.7) and 20,0 ml acetic acid (2.8) and mix
2.15.2. Prepare the mobile phase by mixing nine volumes acetate buffer (2.15.1) and one volume acetonitrile (2.9)
2.16. Preservative stock solution
Accurately weigh approximately 0,05 g 4-hydroxybenzoic acid (2.3), 0,2 g salicylic acid (2.4), 0,2 g benzoic acid (2.5) and 0,05 g sorbic acid (2.6) in a 50-ml volumetric flask and make up to volume with ethanol/water mixture (2.13). Store this solution in a refrigerator. The solution is stable for one week
2.17. Standard preservative solutions
Transfer respectively 8,00, 4,00, 2,00, 1,00 and 0,50 ml of the stock solution (2.16) into a series of 20-ml volumetric flasks. To each flask, add 10,00 ml internal standard solution (2.14) and 0,5 ml sulfuric acid 2 M (2.10). Make up to volume with ethanol/water mixture (2.13). These solutions must be freshly prepared
3. Apparatus
Usual laboratory equipment not otherwise specified, and:
3.1. Water bath, set at 60 °C
3.2. High performance liquid chromatograph with variable-wavelength UV detector and 10-µl injection loop
3.3. Analytical column
Stainless steel, length 12,5 to 25 cm, internal diameter 4,6 mm, packed with Nucleosil 5C18, or equivalent
3.4. Filter paper, diameter: 90 mm, Schleicher and Schull, Weissband No 5892, or equivalent
3.5. 50-ml glass tubes with screw cap
3.6. 5-ml glass sample vials
3.7. Boiling chips, carborundum, size 2 to 4 mm, or equivalent
4. Procedure
4.1. Sample preparation
4.1.1. Sample preparation without addition of internal standard
Weigh 1 g of the sample in a 50-ml glass tube with screw cap (3.5). Pipette 1,00 ml sulfuric acid 2 M (2.10) and 40,0 ml ethanol/water mixture (2.13) into the tube. Add approximately 1 g of boiling chips (3.7), close the tube and shake vigorously for at least one minute until a homogeneous suspension is obtained. To facilitate the extraction of the preservatives into the ethanol phase, place the tube for exactly five minutes in a water bath (3.1) kept at 60 °C.
Cool the tube immediately in a stream of cold water and store the extract at 5 °C for one hour.
Filter the extract through a filter paper (3.4). Transfer approximately 2 ml of the extract into a sample vial (3.6). Store the extract at 5 °C and perform the HPLC determination within 24 hours of preparation.
4.1.2. Sample preparation including addition of internal standard
Weigh to three decimal places 1 ± 0,1 g (a grams) of the sample in a 50-ml glass tube with screw cap (3.5). Add by pipette 1,00 ml sulfuric acid 2 M (2.10) and 30,0 ml ethanol/water mixture (2.13). Add approximately 1 g of boiling chips (3.7) and 10,00 ml internal standard solution (2.14). Close the tube and shake vigorously for at least one minute until an homogeneous suspension is obtained. To facilitate extraction of the preservatives into the ethanol phase place the tube for exactly five minutes in a water bath (3.1) kept at 60 °C.
Cool the tube immediately in a stream of cold water and store the extract at 5 °C for one hour.
Filter the extract through a filter paper (3.4). Transfer approximately 2 ml of the filtrate into a sample vial (3.6). Store the filtrate at 5 °C and perform the HPLC-determination within 24 hours of preparation.
4.2. High performance liquid chromatography
Mobile phase: acetonitrile/acetate buffer (2.15).
Adjust the flow rate of mobile phase through the column to 2,0 ± 0,5 ml/minute. Set the detector wavelength to 240 nm.
4.2.1. Calibration
Inject 10 µl portions of each of the standard preservative solutions (2.17) into the liquid chromatography (3.2). For each solution determine the ratios of the peak heights of the investigated preservatives to the height of the internal standard peak obtained from the chromatograms. Plot a graph for each preservative relating the peak height ratio to the concentration of each standard solution.
Ascertain that a linear response is obtained for the standard solutions in the calibration procedure.
4.2.2. Determination
Inject 10 µl of the sample extract (4.1.1) into the liquid chromatograph (3.2) and record the chromatogram. Inject 10 µl of a standard preservative solution (2.17) and record the chromatogram. Compare the chromatograms obtained. If in the chromatogram of the sample extract (4.1.1) no peak appears to be present having approximately the same retention time as 2-methoxybenzoic acid (recommended internal standard), inject 10 µl sample extract with added internal standard (4.1.2) into the liquid chromatograph and record the chromatogram.
If an interfering peak is observed in the chromatogram of the sample extract (4.1.1) having the same retention time as 2-methoxybenzoic acid, another appropriate internal standard should be selected. (If one of the preservatives under investigation is absent from the chromatogram, this preservative can be used as the internal standard.)
Ascertain whether the chromatograms obtained for a standard solution and the sample solution meet the following requirements:
- the peak separation of the worst separated pair shall be at least 0,90. (For definition of peak separation, see figure 1).
>REFERENCE TO A FILM>
Figure 1: Peak separation
OJ No L 231, 14. 9. 1993, p. 34.- If the required separation is not achieved, either a more efficient column should be used, or the mobile phase composition should be adjusted until the requirement is met.
- The asymmetry factor As of all peaks obtained shall range between 0,9 to 1,5. (For definition of the peak asymmetry factor, see Figure 2). To record the chromatogram for the determination of the asymmetry factor a chart speed of at least 2 cm/minute is recommended.
>REFERENCE TO A FILM>
Figure 2: Peak asymmetry factor
- A steady baseline shall be obtained.
5. Calculation
Use the ratios of the heights of the peaks of the investigated preservatives to the height of the 2-methoxybenzoic acid (internal standard) peak and the calibration graph to calculate the concentration of the acid preservatives in the sample solution. Calculate the benzoic acid, 4-hydroxybenzoic acid, sorbic acid or salicylic acid content of the sample, as a percentage by mass (xi), using the formula:
xi % (m/m) = 100 7 20 7 b 106 7 a = b 500 7 a
in which:
a = the mass (g) of the test portion (4.1.2),
b = the concentration (µg/ml) of the preservative in the sample extract (4.1.2) obtained from the calibration graph.
6. Repeatability (1)
For a 4-hydroxybenzoic acid content of 0,40 % the difference between the results of two determinations in parallel carried out on the same sample should not exceed an absolute value of 0,035 %.
For a benzoic acid content of 0,50 % the difference between the results of two determinations in parallel carried out on the same sample should not exceed an absolute value of 0,050 %.
For a salicylic acid content of 0,50 % the difference between the results of two determinations in parallel carried out on the same sample should not exceed an absolute value of 0,045 %.
For a sorbic acid content of 0,60 % the difference between the results of two determinations in parallel carried out on the same sample should not exceed an absolute value of 0,035 %.
7. Remarks
7.1. Results of a ruggedness test performed on the method indicated that the amount of sulfuric acid added to extract the acids from the sample is critical, and the limits for the amount of sample worked up should be kept within the prescribed boundaries.
7.2. If desired, an appropriate guard column can be used.
C. DETERMINATION OF PROPIONIC ACID
1. Scope and field of application
This method is suitable for the determination of propionic acid, maximum concentration 2 % (m/m) in cosmetic products.
2. Definition
The concentration of propionic acid measured by this method is expressed as a percentage by mass (% m/m) of the product.
3. Principle
Following extraction of propionic acid from the product determination is performed by means of gas chromatography with the use of 2-methylpropionic acid as internal standard.
4. Reagents
All the reagents must be of analytical purity; distilled water or water of equivalent quality must be used.
4.1. Ethanol 96 % (v/v)
4.2. Propionic acid
4.3. 2-Methylpropionic acid
4.4. Orthophosphoric acid, 10 % (m/v)
4.5. Propionic acid solution
Accurately weigh approximately 1,00 g (p grams) of propionic acid into a 50-ml volumetric flask and make up to volume with ethanol (4.1)
4.6. Internal standard solution
Accurately weigh approximately 1,00 g (e grams) of 2-methylpropionic acid into a 50-ml volumetric flask and make up to volume with ethanol (4.1)
5. Apparatus
5.1. Usual laboratory equipment, and:
5.2. Gas chromatograph with flame-ionization detector
5.3. Glass tube (20 × 150 mm) with screw cap
5.4. Water bath at 60 °C
5.5. 10 ml glass syringe with filter membrane (pore diameter: 0,45 µm)
6. Procedure
6.1. Sample preparation
6.1.1. Sample prepartion without the internal standard
Into a glass tube (5.3), weigh approximately 1 g of the sample. Add 0,5 ml of phosphoric acid (4.4) and 9,5 ml of ethanol (4.1).
Close the tube and shake vigorously. If necessary, place the tube in a water bath heated at 60 °C (5.4) for five minutes in order to completely dissolve the lipid phase. Cool rapidly under running water. Filter part of the solution through a membrane filter (5.5). Chromatograph the filtrate the same day.
6.1.2. Sample preparation with the internal standard
Weigh to three decimal places 1 ± 0,1 g (a grams) of the sample into a glass tube (5.3). Add 0,5 ml of phosphoric acid (4.4), 0,50 ml of the internal standard solution (4.6) and 9 ml of ethanol (4.1).
Close the tube and shake vigorously. If necessary, place the tube in a water bath heated to 60 °C (5.4) for five minutes in order to dissolve the lipid phase. Cool rapidly under running water. Filter part of the solution through a membrane filter (5.5). Chromatograph the filtrate the same day.
6.2. Conditions for gas chromatography
The following operation conditions are recommended:
Column
Type Stainless steel
Length 2 m
Diameter 1/8 &Prime;
Packing 10 % SPTM 1000 (or equivalent) + 1 % H3PO4 on Chromosorb WAW 100 to 120 mesh
Temperature
Injector 200 °C
Column 120 °C
Detector 200 °C
Carrier gas nitrogen
Flow rate 25 ml/min
6.3. Chromatography
6.3.1. Calibration
Into a series of 20-ml volumetric flasks, transfer by pipette 0,25, 0,50, 1,00, 2,00 and 4,00 ml respectively of the propionic acid solution (4.5). To each volumetric flask transfer by pipette 1,00 ml of the internal standard solution (4.6); make up to volume with ethanol (4.1) and mix. The solutions prepared in this way contain e mg/ml of 2-methylpropionic acid as internal standard (that is to say, 1 mg/ml if e = 1 000) and p/4, p/2, p, 2p, 4p mg/ml propionic acid (that is to say, 0,25, 0,50, 1,00, 2,00, 4,00 mg/ml if p = 1 000).
Inject 1 µl of each of these solutions and obtain the calibration curve by plotting the ratio of the propionic acid/2-methylpropionic acid mass on the x-axis and the ratio of the corresponding peak areas on the y-axis.
Make three injections of each solution and calculate the average peak area ratio.
6.3.2. Determination
Inject 1 µl of the sample filtrate 6.1.1. Compare the chromatogram with that of one of the standard solutions (6.3.1). If a peak has approximately the same retention time as 2-methylpropionic acid, change the internal standard. If no interference is observed, inject 1 µl of the sample filtrate 6.1.2 and measure the areas of the propionic acid peak and the internal standard peak.
Make three injections of each solution and calculate the average peak area ratio.
7. Calculations
7.1. From the calibration curve obtained in 6.3.1, obtain the ratio of mass (K) corresponding to the peak area ratio calculated in 6.3.2.
7.2. From the ratio of mass thus obtained calculate the propionic acid content of the sampe (X) as a percentage by mass using the formula:
x % (m/m) = K 0,5 7 100 7 e 50 7 a = K e a
in which:
K = the ratio calculated in 7.1,
e = mass in grams of the internal standard weighed in 4.6,
a = mass in grams of the sample weighed in 6.1.2.
Round off results to one decimal place.
8. Repeatability (1)
For a propionic acid content of 2 % (m/m) the difference between the results of two determinations in parallel carried out on the same sample should not exceed 0,12 %.
II. IDENTIFICATION AND DETERMINATION OF HYDROQUINONE, HYDROQUINONE MONOMETHYLETHER, HYDROQUINONE MONOETHYLETHER AND HYDROQUINONE MONOBENZYLETHER IN COSMETIC PRODUCTS
A. IDENTIFICATION
1. Scope and field of application
The method describes the detection and identification of hydroquinone, hydroquinone monomethylether, hydroquinone monoethylether and hydroquinone monobenzylether (monobenzone) in cosmetic products for lightening the skin.
2. Principle
Hydroquinone and its ethers are identified by thin layer chromatography (TLC).
3. Reagents
All reagents must be of analytical grade.
3.1. Ethanol, 96 % (v/v)
3.2. Chloroform
3.3. Diethyl ether
3.4. Developing solvent:
Chloroform/Diethyl ether, 66:33 (v/v)
3.5. Ammonia, 25 % (m/m) (d²04 = 0,91 g/ml)
3.6. Ascorbic acid
3.7. Hydroquinone
3.8. Hydroquinone monomethylether
3.9. Hydroquinone monoethylether
3.10. Hydroquinone monobenzylether (monobenzone)
3.11. Reference solutions
The following reference solutions should be freshly prepared, and are stable for one day.
3.11.1. Weigh 0,05 g hydroquinone (3.7) into a 10-ml graduated test tube. Add 0,250 g of ascorbic acid (3.6) and 5 ml of ethanol (3.1). Add ammonia (3.5) until the pH is 10 and make up to a volume of 10 ml with ethanol (3.1).
3.11.2. Weigh 0,05 g hydroquinone monomethylether (3.8) into a 10-ml graduated test tube. Add 0,250 g of ascorbic acid (3.6) and 5 ml of ethanol (3.1). Add ammonia (3.5) until the pH is 10 and make up to a volume of 10ml with ethanol (3.1).
3.11.3. Weigh 0,05 g hydroquinone monoethylether (3.9) into a 10-ml graduated test tube. Add 0,250 g of ascorbic acid (3.6) and 5 ml of ethanol (3.1). Add ammonia (3.5) until the pH is 10 and make up to a volume of 10ml with ethanol (3.1).
3.11.4. Weigh 0,05 g hydroquinone monobenzylether (3.10) into a 10-ml graduated test tube. Add 0,250 g of ascorbic acid (3.6) and 5 ml of ethanol (3.1). Add ammonia (3.5) until the pH is 10 and make up to a volume of 10ml with ethanol (3.1).
3.12. Silver nitrate
3.13. 12-Molybdophosphoric acid
3.14. Potassium ferricyanide hexahydrate
3.15. Ferric chloride, hexahydrate
3.16. Spray reagents
3.16.1. To a 5 % (m/v) aqueous solution of silver nitrate (3.12), add ammonia (3.5) until the precipitate that forms dissolves.
Warning:
the solution becomes explosively unstable on standing and should be discarded after use.
3.16.2. 10 % (m/v) solution of 12-molybdophosphoric acid (3.13) in ethanol (3.1).
3.16.3. Prepare a 1 % (m/v) aqueous solution of potassium ferricyanide (3.14) and a 2 % (m/v) aqueous solution of ferric chloride (3.15). Mix equal parts of both solutions just before use.
4. Apparatus
Normal laboratory equipment, and:
4.1. Usual TLC equipment
4.2. TLC plates, ready for use: silica gel GHR/UV254; 20 × 20 cm (Machery, Nagel, or equivalent). Layer thickness 0,25 mm
4.3. Ultrasonic bath
4.4. Centrifuge
4.5. UV lamp, 254 nm
5. Procedure
5.1. Preparation of the sample
Weigh 3,0 g of sample into a 10-ml graduated tube. Add 0,250 g of ascorbic acid (3.6) and 5 ml of ethanol (3.1). Adjust the pH of the solution to 10, using amonia (3.5). Make up to a volume of 10 ml with ethanol (3.1). Close the tube with a stopper and homogenize in an ultrasonic bath for 10 minutes. Filter through a filter paper or centrifuge at 3 000 rpm.
5.2. TLC
5.2.1. Saturate a chromatographic tank with developing solvent (3.4).
5.2.2. Deposit on a plate 2 µl of the reference solutions (3.11) and 2 µl of the sample solution (5.1). Develop in the dark at ambient temperature until the solvent front has migrated 15 cm from the start.
5.2.3. Remove the plate and allow to dry at room temperature.
5.3. Detection
5.3.1. Observe the plate under UV light at 254 nm, and mark the position of the spots.
5.3.2. Spray the plate with:
- silver nitrate reagent (3.16.1), or
- 12-molybdophosphoric acid reagent (3.16.2); heat to approximately 120 °C, or
- potassium ferricyanide solution and ferric chloride solution (3.16.3).
6. Identification
Calculate the Rf value for each spot.
Compare the spots obtained for the sample solution with those for the reference solutions with respect to: their Rf values; the colour of the spots under UV radiation; and the colours of the spots after visualization with the spray reagent.
Perform the HPLC described in the following section (B), and compare the retention times obtained for the sample peak(s) with those for the reference solutions.
Combine the results from TLC and HPLC to identify the presence of hydroquinone and/or its ethers.
7. Remarks
Under the conditions described, the following Rf values were observed:
hydroquinone: 0,32
hydroquinone monomethylether: 0,53
hydroquinone monoethylether: 0,55
hydroquinone monobenzylether: 0,58
B. DETERMINATION
1. Scope and field of application
This method specifies a procedure for the determination of hydroquinone, hydroquinone monomethylether, hydroquinone monoethylether and hydroquinone monobenzylether in cosmetic products for lightening the skin.
2. Principle
The sample is extracted with a water/methanol mixture under gentle heating to melt any lipid material. Determination of the analytes in the resulting solution is performed by reversed phase liquid chromatography with UV detection.
3. Reagents
3.1. All reagents must be of analytical quality. Water used must be distilled water, or water of at least equivalent purity
3.2. Methanol
3.3. Hydroquinone
3.4. Hydroquinone monomethylether
3.5. Hydroquinone monethylether
3.6. Hydroquinone monobenzylether (monobenzone)
3.7. Tetrahydrofuran, HPLC grade
3.8. Water/methanol mixture 1:1 (v/v). Mix one volume of water and one volume of methanol (3.2)
3.9. Mobile phase: tetrahydrofuran/water mixture 45:55 (v/v). Mix 45 volumes of tetrahydrofuran (3.7) and 55 volumes of water
3.10. Reference solution
Weigh 0,06 g of hydroquinone (3.3), 0,08 g hydroquinone monomethylether (3.4), 0,10 g hydroquinone monoethylether (3.5) and 0,12 g hydroquinone monobenzylether (3.6) into a 50-ml volumetric flask. Dissolve and make up to volume with methanol (3.2). Prepare the reference solution by diluting 10,00 ml of this solution to 50,00 ml with water/methanol mixture (3.8). These solutions must be freshly prepared.
4. Apparatus
Normal laboratory equipment and:
4.1. Water bath, capable of maintaining a temperature of 60 °C
4.2. High-performance liquid chromatograph with a variable-wavelength UV detector and 10-µl injection loop
4.3. Analytical column:
Stainless steel chromatographic column, length 250 mm, internal diameter 4,6 mm, packed with Zorbax phenyl (chemically bonded phenethylsilane on Zorbax SIL, end-capped with trimethylchlorosilane), particle size 6 µm, or equivalent. Do not use a guard column, except phenyl guard, or equivalent
4.4. Filter paper, diameter 90 mm, Schleicher and Schull, Weissband No 5892, or equivalent
5. Procedure
5.1. Sample preparation
Weigh to three decimal places 1 ± 0,1 g (a gram) of sample into a 50-ml volumetric flask. Disperse the sample in 25 ml water/methanol mixture (3.8). Close the flask and shake vigorously until a homogeneous suspension is obtained. Shake for at least one minute. Place the flask in a water bath (4.1) kept at 60 °C to enhance the extraction. Cool the flask, and make up to volume with water/methanol (3.8). Filter the extract using a filter paper (4.4). Perform the HPLC determination within 24 hours of preparing the extract.
5.2. High performance liquid chromatography.
5.2.1. Adjust the flow rate of the mobile phase (3.9) to 1,0 ml/min and set the detector wavelength to 295 nm.
5.2.2. Inject 10 µl of the sample solution obtained as described in section 5.1, and record the chromatogram. Measure the peak areas. Perform a calibration as described under 5.2.3. Compare the chromatograms obtained for sample and standard solutions. Use the peak aeras and the response factors (RF) calculated under 5.2.3 to calculate the concentration of the analytes in the sample solution.
5.2.3. Calibration
Inject 10 µl of the reference solution (3.10) and record the chromatogram. Inject several times until a constant peak area is obtained.
Determine the response factor RFi:
RFi = pi ci
in which:
pi = peak area for hydroquinone, hydroquinone monomethylether, hydroquinone monoethylether or hydroquinone monobenzylether, and
ci = concentration (g/50 ml) in the reference solution (3.10) of hydroquinone, hydroquinone monomethylether, hydroquinone monoethylether or hydroquinone monobenzylether.
Ascertain whether the chromatograms obtained for a standard solution and the sample solution meet the following requirements:
- the peak separation of the worst separated pair shall be at least 0,90. (For definition of peak separation, see Figure 1.)
>REFERENCE TO A FILM>
Figure 1: Peak separation
If the required separation is not achieved, either a more efficient column should be used, or the mobile phase composition should be adjusted until the requirement is met.
- The asymmetry factor AS, of all peaks obtained shall range between 0,9 to 1,5. (For definition of the peak asymmetry factor, see Figure 2.) To record the chromatogram for the determination of the asymmetry factor a chart speed of at lest 2 cm/min is recommended.
>REFERENCE TO A FILM>
Figure 2: Peak asymmetry factor
- A steady baseline shall be obtained.
6. Calculation
Use the areas of the analyte peaks to calculate the concentration(s) of the analyte(s) in the sample. Calculate the analyte concentration in the sample, as a percentage by mass, (xi) using the formula:
xi % (m/m) = bi 7 100 RFi 7 a
in which:
a= mass of the sample in grams, and
bi = peak area of analyte i in the sample.
7. Repeatability (1)
7.1. For a hydroquinone content of 2,0 % the difference between the results of two determinations carried out in parallel on the same sample should not exceed an absolute value of 0,13 %.
7.2. For a hydroquinone monomethylether content of 1,0 % the difference between the results of two determinations carried out in parallel on the same sample should not exceed an absolute value of 0,1 %.
7.3. For a hydroquinone monoethylether content of 1,0 % the difference between the results of two determinations carried out in parallel on the same sample should not exceed an absolute value of 0,11 %.
7.4. For a hydroquinone monobenzylether content of 1,0 % the difference between the results of two determinations carried out in parallel on the same sample should not exceed an absolute value of 0,11 %.
8. Reproducibility (1)
8.1. For a hydroquinone content of 2,0 % the difference between the results of two determinations carried out on the same sample under different conditions (different laboratories, different operators, different apparatus and/or different time) should not exceed an absolute value of 0,37 %.
8.2. For a hydroquinone monomethylether content of 1,0 % the difference between the results of two determinations carried out on the same sample under different conditions (different laboratories, different operators, different apparatus and/or different time) should not exceed an absolute value of 0,21 %.
8.3. For a hydroquinone monoethylether content of 1,0 % the difference between the results of two determinations carried out on the same sample under different conditions (different laboratories, different operators, different apparatus and/or different time) should not exceed an absolute value of 0,19 %.
8.4. For a hydroquinone monobenzylether content of 1,0 % the difference between the results of two determinations carried out on the same sample under different conditions (different laboratories, different operators, different apparatus and/or different time) should not exceed an absolute value of 0,11 %.
9. Remarks
9.1. When a hydroquinone content considerably higher than 2 % is found and an accurate estimate of the content is required, the sample extract (5.1) should be diluted to a similar concentration as would be obtained from a sample containing 2 % hydroquinone, and the determination repeated.
(In some instruments the absorbance may be out of the linear range of the detector for high hydroquinone concentrations.)
9.2. Interferences
The method described above allows the determination of hydroquinone and its ethers in a single isocratic run. The use of the phenyl column assures sufficient retention for hydroquinone, which cannot be guaranteed when a C18 column is used with the mobile phase described.
However, this method is prone to interferences by a number of parabens. In such cases the determination should be repeated employing a different mobile phase/stationary phase system. Suitable methods may be found in references 1 and 2, namely:
Column: Zorbax ODS, 4,6 mm × 25 mm, or equivalent:
temperaure: 36 °C
flow: 1,5 ml/min
mobile phase:
for hydroquinone: methanol/water 5/95 (V/V)
for hydroquinone monomethylether: methanol/water 30/70 (V/V)
for hydroquinone monobenzylether: methanol/water 80/20 (V/V) (1).
Column: Spherisorb S5-ODS, or equivalent:
mobile phase: water/methanol 90/10 (V/V)
flow: 1,5 ml/min.
These conditions are suitable for hydroquinone (2).
(1) ISO 5725.
(1) ISO 5725.
(1) ISO 5725.
(1) M. Herpol-Borremans et M.-O. Masse, Identification et dosage de l'hydroquinone et de ses éthers méthylique et benzylique dans les produits cosmétiques pour blanchir la peau. Int. j. Cosmet. Sci. 8-203-214 (1986).
(2) J. Firth and I. Rix, Determination of hydroquinone in skin toning creams, Analyst (1986), 111, p. 129.
