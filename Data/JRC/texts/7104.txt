Council Regulation (EC) No 1947/2005
of 23 November 2005
on the common organisation of the market in seeds and repealing Regulations (EEC) No 2358/71 and (EEC) No 1674/72
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 36 and the third subparagraph of Article 37(2) thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament [1],
Having regard to the opinion of the European Economic and Social Committee [2],
Whereas:
(1) The operation and development of the common market in agricultural products should be accompanied by the establishment of a common agricultural policy to include, in particular, a common organisation of the agricultural markets which may take various forms depending on the product concerned.
(2) Council Regulation (EEC) No 2358/71 of 26 October 1971 on the common organisation of the market in seeds [3] has been substantially amended several times, particularly by Council Regulation (EC) No 1782/2003 of 29 September 2003 establishing common rules for direct support schemes under the common agricultural policy and establishing certain support schemes for farmers [4]. In the interests of clarity, Regulation (EEC) No 2358/71 should be repealed and replaced by a new Regulation.
(3) The provisions of Council Regulation (EEC) No 1674/72 of 2 August 1972 laying down general rules for granting and financing aid for seed [5] were incorporated into the implementing rules in Chapter 10 of Commission Regulation (EC) No 1973/2004 of 29 October 2004 laying down detailed rules for the application of Council Regulation (EC) No 1782/2003 as regards the support schemes provided for in Titles IV and IVa of that Regulation and the use of land set aside for the production of raw materials [6]. Regulation (EEC) No 1674/72 should therefore be repealed.
(4) In order to monitor the volume of trade in seeds with third countries, provision should be made for an import licence scheme with the lodging of a security to ensure that the transactions for which such licences are requested are effected.
(5) The system of customs duties makes it possible to dispense with all other protective measures in the case of goods imported from third countries.
(6) The internal market and customs duty mechanism could, in exceptional circumstances, prove inadequate. In such cases, in order not to leave the Community market without defence against disturbances that might ensue, the Community should be able to take all necessary measures without delay. Those measures should comply with the Community’s international obligations.
(7) The proper working of the internal market in seeds would be jeopardised by the granting of national aid. The provisions of the Treaty governing State aid should, therefore, apply to the products covered by this common market organisation. However, since accession, Finland may, subject to authorisation by the Commission, grant aid respectively for certain quantities of seeds and for certain quantities of cereal seed produced solely in Finland, because of its specific climatic conditions.
(8) Since the common market in seeds is constantly evolving, the Member States and the Commission should keep each other supplied with information relevant to developments in this area.
(9) The measures necessary for the implementation of this Regulation should be adopted in accordance with Council Decision 1999/468/EC of 28 June 1999 laying down the procedures for the exercise of implementing powers conferred on the Commission [7],
HAS ADOPTED THIS REGULATION:
CHAPTER I
INTRODUCTORY PROVISIONS
Article 1
A common organisation of the market in seeds shall be established and shall cover the following products:
CN code | Description of goods |
07129011 | Sweetcorn hybrids for sowing |
07131010 | Peas (Pisum sativum) for sowing |
ex07132000 | Chickpeas for sowing |
ex07133100 | Beans of the species Vigna mungo (L.) Hepper or Vigna radiata (L.) Wilczek for sowing |
ex07133200 | Small red (Adzuki) beans (Phaseolus or Vigna angularis) for sowing |
07133310 | Kidney beans, including white pea beans (Phaseolus vulgaris) for sowing |
ex07133900 | Other beans for sowing |
ex07134000 | Lentils for sowing |
ex07135000 | Broad beans (Vicia faba var. major) and horse beans (Vicia faba var. equina, Vicia faba var. mino) |
ex07139000 | Other dried leguminous vegetables for sowing |
10019010 | Spelt for sowing |
ex100510 | Hybrid maize (corn) seed |
10061010 | Rice in the husk (paddy or rough) for sowing |
10070010 | Grain sorghum hybrids for sowing |
12010010 | Soya beans, whether or not broken, for sowing |
12021010 | Groundnuts, not roasted or otherwise cooked, in shell, for sowing |
12040010 | Linseed, whether or not broken, for sowing |
12051010 | Rape or colza seeds, whether or not broken, for sowing |
12060010 | Sunflower seeds, whether or not broken, for sowing |
ex1207 | Other oil seeds and oleaginous fruits, whether or not broken, for sowing |
1209 | Seeds, fruit and spores, of a kind used for sowing |
Article 2
The marketing year for seeds shall begin on 1 July of each year and end on 30 June of the following year.
Article 3
This Regulation shall apply without prejudice to the measures provided for by Regulation (EC) No 1782/2003.
CHAPTER II
TRADE WITH THIRD COUNTRIES
Article 4
1. Imports into the Community of any of the products listed in Article 1 may be subject to the presentation of an import licence. The products for which import licences are required shall be determined in accordance with the procedure referred to in Article 10(2).
2. Import licences shall be issued by the Member States to any persons who so request, irrespective of their place of establishment in the Community.
3. Licences shall be valid for imports carried out anywhere in the Community. Their issue shall be subject to the lodging of a security guaranteeing that the products are imported during the validity period of the licence. Except in cases of force majeure, the security shall be forfeited in whole or in part if the transaction is not carried out, or is carried out only partially, within that period.
Article 5
Unless this Regulation provides otherwise, the rates of duty in the Common Customs Tariff shall apply to the products listed in Article 1.
Article 6
1. The general rules for the interpretation of the Combined Nomenclature and the detailed rules for its application shall apply to the tariff classification of products referred to in Article 1. The tariff nomenclature resulting from the application of this Regulation shall be included in the Common Customs Tariff.
2. Save as otherwise provided for in this Regulation or in provisions adopted pursuant thereto, the following shall be prohibited in trade with third countries:
(a) the levying of any charge having equivalent effect to a customs duty;
(b) the application of any quantitative restriction or measure having equivalent effect.
Article 7
1. If, by reason of imports or exports, the Community market in one or more of the products listed in Article 1 is affected by, or is threatened with, serious disturbance likely to jeopardise the achievement of the objectives set out in Article 33 of the Treaty, appropriate measures may be applied in trade with non-member countries of the World Trade Organisation until such disturbance or threat of it has ceased.
2. If the situation referred to in paragraph 1 arises, the Commission shall, at the request of a Member State or on its own initiative, decide upon the necessary measures. The Member States shall be notified of such measures, which shall be immediately applicable. If the Commission receives a request from a Member State, it shall take a decision thereon within three working days following receipt of the request.
3. The measures mentioned in paragraph 2 may be referred to the Council by any Member State within three working days of the day on which they are notified. The Council shall meet immediately. It may, acting by a qualified majority, amend or repeal the measures in question within one month of the day on which the measures are referred to it.
4. Arrangements adopted under this Article shall take account of the obligations arising from agreements concluded in accordance with Article 300(2) of the Treaty.
CHAPTER III
GENERAL PROVISIONS
Article 8
1. Save as otherwise provided in this Regulation, Articles 87, 88 and 89 of the Treaty shall apply to the production of, and trade in, the products listed in Article 1.
2. However, Finland may, subject to authorisation by the Commission, grant aid respectively for certain quantities of seeds and for certain quantities of cereal seed produced solely in Finland, because of its specific climatic conditions.
Prior to 1 January 2006, the Commission shall transmit to the Council, on the basis of the information supplied in due time by Finland, a report on the results of the aid authorised, accompanied by the necessary proposals.
Article 9
The Member States and the Commission shall communicate to each other the information necessary for implementing this Regulation.
Article 10
1. The Commission shall be assisted by a Management Committee for Seeds (hereinafter referred to as "the Committee").
2. Where reference is made to this paragraph, Articles 4 and 7 of Decision 1999/468/EC shall apply.
The period laid down in Article 4(3) of Decision 1999/468/EC shall be set at one month.
3. The Committee shall adopt its Rules of Procedure.
Article 11
Detailed rules on the application of this Regulation, and in particular the validity period of the licences referred to in Article 4 and the arrangements for communicating the information referred to in Article 9, shall be adopted in accordance with the procedure referred to in Article 10(2).
CHAPTER IV
TRANSITIONAL AND FINAL PROVISIONS
Article 12
1. Regulations (EEC) No 2358/71 and (EEC) No 1674/72 are hereby repealed.
2. References to Regulation (EEC) No 2358/71 shall be construed as references to this Regulation and shall be read in accordance with the correlation table in the Annex.
Article 13
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Union.
It shall apply from 1 July 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 23 November 2005.
For the Council
The President
M. Beckett
[1] Not yet published in the Official Journal.
[2] Opinion delivered on 26 October 2005 following non-compulsory consultation (not yet published in the Official Journal).
[3] OJ L 246, 5.11.1971, p. 1. Regulation as last amended by Regulation (EC) No 1782/2003 (OJ L 270, 21.10.2003, p. 1).
[4] OJ L 270, 21.10.2003, p. 1. Regulation as last amended by Commission Regulation (EC) No 118/2005 (OJ L 24, 27.1.2005, p. 15).
[5] OJ L 177, 4.8.1972, p. 1. Regulation as last amended by Regulation (EEC) No 3795/85 (OJ L 367, 31.12.1985, p. 21).
[6] OJ L 345, 20.11.2004, p. 1. Regulation as last amended by Regulation (EC) No 1044/2005 (OJ L 172, 5.7.2005, p. 76).
[7] OJ L 184, 17.7.1999, p. 23.
--------------------------------------------------
ANNEX
Correlation table
Regulation (EEC) No 2358/71 | This Regulation |
Article 1 | Article 1 |
Article 2 | Article 2 |
— | Article 3 |
Article 3 | — |
Article 3a | — |
Article 4(1) and (2), first subparagraph | Article 4 |
Article 4(2), second subparagraph | Article 11 |
Article 5(1) | Article 5 |
Article 5(2) and Article 6 | Article 6 |
Article 7 | Article 7 |
Article 8 | Article 8 |
Article 9, first sentence | Article 9 |
Article 9, second sentence | Article 11 |
Article 11 | Article 10 |
Article 12 | — |
Article 13 | — |
Article 14 | — |
Article 15 | — |
Article 16 | — |
— | Article 12 |
Article 17 | Article 13 |
--------------------------------------------------
