Council Regulation (EC) No 1617/2006
of 24 October 2006
amending Regulation (EC) No 1207/2001 as regards the consequences of the introduction of the system of pan-Euro-Mediterranean cumulation of origin
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 133 thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) Council Regulation (EC) No 1207/2001 of 11 June 2001 on procedures to facilitate the issue of movement certificates EUR.1, the making out of invoice declarations and forms EUR.2 and the issue of certain approved exporter authorisations under the provisions governing preferential trade between the European Community and certain countries [1], lays down rules intended to facilitate the correct issue or making out of proofs of origin in relation to exports of products from the Community in the context of its preferential trade relations with certain third countries.
(2) A system of pan-European diagonal cumulation of origin was put in place in 1997 between the Community, Bulgaria, the Czech Republic, Estonia, Latvia, Lithuania, Hungary, Poland, Romania, Slovenia, Slovakia, Iceland, Norway and Switzerland (including Liechtenstein) and extended in 1999 to Turkey. On 1 May 2004, the Czech Republic, Estonia, Latvia, Lithuania, Hungary, Poland, Slovenia and Slovakia acceded to the European Union.
(3) At the Euro-Med Trade Ministerial Meeting in Toledo of March 2002, Ministers agreed upon the extension of this system to the Mediterranean countries, other than Turkey, which participate in the Euro-Mediterranean partnership, based on the Barcelona Declaration adopted at the Euro-Mediterranean Conference held on 27 and 28 November 1995. At the Euro-Med Trade Ministerial Meeting in Palermo, on 7 July 2003, the Ministers, with a view to allowing such an extension, endorsed a new pan-Euro-Mediterranean model of Protocol to the Euro-Mediterranean Agreements, concerning the definition of the concept of "originating products" and methods of administrative cooperation. Further to the outcome of the EC-Faroe Islands/Denmark Joint Committee of 28 November 2003, it has been agreed to include also the Faroe Islands in the system of pan-Euro-Mediterranean diagonal cumulation of origin.
(4) Decisions of the respective Association Council or Joint Committee introducing the new pan-Euro-Mediterranean Protocol in the Euro-Mediterranean Agreements and in the Agreement between the EC and the Faroe Islands/Denmark have already been, or will be, adopted.
(5) The application of that new system of diagonal cumulation implies using new types of proof of preferential origin, consisting in movement certificates EUR-MED and invoice declarations EUR-MED. Regulation (EC) No 1207/2001 should therefore cover also those types of proofs of preferential origin.
(6) To allow a correct determination of the originating status of products and properly support the establishment of the proofs of origin in that new context, the supplier's declaration for products having preferential originating status should incorporate an additional statement showing whether diagonal cumulation has been applied and with what countries.
(7) Regulation (EC) No 1207/2001 should therefore be amended accordingly,
HAS ADOPTED THIS DECISION:
Article 1
Regulation (EC) No 1207/2001 is hereby amended as follows:
1. The title shall be replaced by the following:
"Council Regulation (EC) No 1207/2001 of 11 June 2001 on procedures to facilitate the issue or the making out in the Community of proofs of origin and the issue of certain approved exporter authorisations under the provisions governing preferential trade between the European Community and certain countries".
2. In Article 1, point (a) shall be replaced by the following:
"(a) the issue or the making-out in the Community of proofs of origin under the provisions governing preferential trade between the Community and certain countries;".
3. Article 2(2) shall be replaced by the following:
"2. Supplier's declarations shall be used by exporters as evidence, in particular in support of applications for the issue or the making out in the Community of proofs of origin under the provisions governing preferential trade between the Community and certain countries.".
4. Article 10(5) shall be replaced by the following:
"5. Where there is no reply within five months of the date of the verification request or where the reply does not contain sufficient information to demonstrate the real origin of the products, the customs authorities of the country of export shall declare invalid the proof of origin established on the basis of the documents in question.".
5. Annex I shall be replaced by the text set out in Annex I to this Regulation.
6. Annex II shall be replaced by the text set out in Annex II to this Regulation.
Article 2
This Regulation shall enter into force on the 20th day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Luxembourg, 24 October 2006.
For the Council
The President
J. Korkeaoja
[1] OJ L 165, 21.6.2001, p. 1.
--------------------------------------------------
ANNEX I
"
"ANNEX I
+++++ TIFF +++++
Supplier's declaration for products having preferential origin statusThe supplier's declaration, the text of which is given below, must be made out in accordance with the footnotes. However, the footnotes do not have to be reproduced.DECLARATIONI, the undersigned, declare that the goods listed on this document (1) originate in (2) and satisfy the rules of origin governing preferential trade with (3).I declare that (4):Cumulation applied with (name of the country/countries)No cumulation appliedI undertake to make available to the customs authorities any further supporting documents they require:(5)(6)(7)(1) If only some of the goods listed on the document are concerned, they shall be clearly indicated or marked and this marking entered in the declaration as follows:“… listed on this invoice and marked … originate in …”.(2) The Community, country, group of countries or territory, in which the goods originate.(3) Country, group of countries or territory concerned.(4) To be completed, where necessary, only for goods having preferential origin status in the context of preferential trade relations with one of the countries referred to in Articles 3 and 4 of the relevant origin Protocol, with which pan-Euro-Mediterranean cumulation of origin is applicable.(5) Place and date.(6) Name and position in the company.(7) Signature."
"
--------------------------------------------------
ANNEX II
"
"ANNEX II
+++++ TIFF +++++
Long-term supplier's declaration for products having preferential origin statusThe supplier's declaration, the text of which is given below, must be made out in accordance with the footnotes. However, the footnotes do not have to be reproduced.DECLARATIONI, the undersigned, declare that the goods described below:(1)(2)which are regularly supplied to (3) originate in (4) and satisfy the rules of origin governing preferential trade with (5).I declare that (6):Cumulation applied with (name of the country/countries)No cumulation appliedThis declaration is valid for all further shipments of these products dispatched from:to (7).I undertake to inform immediately if this declaration is no longer valid.I undertake to make available to the customs authorities any further supporting documents they require:(8)(9)(10)(1) Description.(2) Commercial designation as used on the invoices, e.g. model No.(3) Name of company to which goods are supplied.(4) The Community, country, group of countries or territory, in which the goods originate.(5) Country, group of countries or territory concerned.(6) To be completed, where necessary, only for goods having preferential origin status in the context of preferential trade relations with one of the countries referred to in Articles 3 and 4 of the relevant origin Protocol, with which pan-Euro-Mediterranean cumulation of origin is applicable.(7) Give the dates. The period shall not exceed 12 months.(8) Place and date.(9) Name and position, name and address of company.(10) Signature."
"
--------------------------------------------------
