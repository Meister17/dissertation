Commission communication in the framework of the implementation of Commission Directive 95/12/EC
(2005/C 329/05)
(Text with EEA relevance)
Publication of titles and references of harmonized standards under the directive
European Standardisation Organisation | Reference and title of the standard (Reference document) | Reference of the superseded standard | Date of cessation of use of the superseded standard Note 1 |
CENELEC | EN 60456:1999 Clothes washing machines for household use — Methods for measuring the performance (IEC 60456:1998 (Modified)) | EN 60456:1994 +A11:1995 Note 2.1 | Date expired ( 1.10.1999) |
Amendment A11:2001 to EN 60456:1999 | Note 3 | Date expired ( 1.8.2001) |
Amendment A13:2003 to EN 60456:1999 | Note 3 | Date expired ( 1.6.2003) |
Amendment A12:2001 to EN 60456:1999 | Note 3 | Date expired ( 1.2.2004) |
CENELEC | EN 60456:2005 Clothes washing machines for household use — Methods for measuring the performance (IEC 60456:2003 (Modified)) | EN 60456:1999 and its amendments Note 2.1 | 1.10.2007 |
Note 1: Generally the date of cessation of use will be the date of withdrawal ("dow"), set by the European Standardisation Organisation, but attention of users of these standards is drawn to the fact that in certain exceptional cases this can be otherwise.
Note 2.1: The new (or amended) standard has the same scope as the superseded standard. On the date stated, the superseded standard cannot be used any longer in the context of the directive.
Note 3: In case of amendments, the referenced standard is EN CCCCC:YYYY, its previous amendments, if any, and the new, quoted amendment. The superseded standard (column 3) therefore consists of EN CCCCC:YYYY and its previous amendments, if any, but without the new quoted amendment. On the date stated, the superseded standard cannot be used any longer in the context of the directive.
Example: For EN 60456:1999, the following applies:
CENELEC | EN 60456:1999 Clothes washing machines for household use — Methods for measuring the performance IEC 60456:1998 (Modified) [The referenced standard is EN 60456:1999] | EN 60456:1994 and its amendment Note 2.1 [The superseded standard is EN 60456:1994+A11:1995 to EN 60456:1994] | Date expired ( 1.10.1999) |
Amendment A11:2001 to EN 60456:1999 [The referenced standard is EN 60456:1999 + A11:2001 to EN 60456:1999] | Note 3 [The superseded standard is EN 60456:1999] | Date expired ( 1.8.2001) |
Amendment A13:2003 to EN 60456:1999 [The referenced standard is EN 60456:1999 + A11:2001 to EN 60456:1999 + A13:2003 to EN 60456:1999] | Note 3 [The superseded standard is EN 60456:1999 + A11:2001 to EN 60456:1999] | Date expired ( 1.6.2003) |
Amendment A12:2001 to EN 60456:1999 [The referenced standard is EN 60456:1999 + A11:2001 to EN 60456:1999 + A13:2003 to EN 60456:1999 + A12:2001 to EN 60456:1999] | Note 3 [The superseded standard is EN 60456:1999 + A11:2001 to EN 60456:1999 + A13:2003 to EN 60456:1999] | Date expired ( 1.2.2004) |
--------------------------------------------------
