[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 29.7.2005
SEC(2005) 1024 final
Draft
DECISION No 1/2005 OF THE EC-EFTA JOINT COMMITTEE ON SIMPLIFICATION OF FORMALITIES IN TRADE IN GOODS
concerning the invitation to Romania, to accede to the Convention of 20 May 1987 on the simplification of formalities in trade in goods
- Draft common position of the Community - (presented by the Commission)
EXPLANATORY MEMORANDUM
CONTEXT OF THE PROPOSAL |
110 | Grounds for and objectives of the proposal Invitation of accession to the Convention of 20 May 1987 on the simplification of formalities in trade in goods made by the EC-EFTA Joint Committee on simplification of formalities in trade in goods at the address of Romania following its request, facilitating the movement of goods between this country, the European Community and the EFTA countries. |
120 | General context The convention of 20 May 1987 on the simplification of formalities in trade in goods envisages the measures facilitating the movement of goods between the EC and the EFTA countries. In accordance with the provisions of Article 11(3) of the aforementioned convention, the Joint Committee adopts via decision the invitation to a third country within the meaning of Article 1 (2), to access to the Convention according to the procedure fixed at Article 11 a. Romania wished formally to be able to access to the Convention of 20 May 1987 on the simplification of formalities in trade in goods after having satisfied the essential, legal, structural and computer conditions, preliminary with this accession. The Joint Committee decided to launch such an invitation provided that the country could show that it was in a position to conform to the detailed rules for the application of the arrangement. Mandated by the EC-EFTA working group on simplification of formalities in trade in goods an evaluation mission covering mainly the adaptation of the Romanian national customs legislation, the creation of the structures necessary to manage the procedure and the implementation of the New Computerised Transit system (NCTS) allowing for the application of the common transit procedure Romania's established that the conditions of an invitation were met. |
139 | Existing provisions in the area of the proposal There are no existing provisions in the area of the proposal. |
141 | Consistency with other policies and objectives of the Union The proposal complies with the pre-accession strategy concerning the accession to the European Union. |
CONSULTATION OF INTERESTED PARTIES AND IMPACT ASSESSMENT |
Consultation of interested parties |
211 | Consultation methods, main sectors targeted and general profile of respondents Consultation and approval of the EC-EFTA on simplification of formalities in trade in goods working group, representing the contracting parties to the Convention. |
212 | Summary of responses and how they have been taken into account Favourable opinion |
Collection and use of expertise |
229 | There was no need for external expertise. |
230 | Impact assessment Accession to the Convention of 20 May 1987 on the simplification of formalities in trade in goods within the framework of the pre-accession strategy to the European Union the 1st January 2007. Facilitation from the movements of goods between the Romania, the European Community, the Republic of Iceland, the Kingdom of Norway and the Swiss Confederation. Introduction of common provisions to the Convention on the simplification of formalities in trade in goods and to the Community legislation. |
LEGAL ELEMENTS OF THE PROPOSAL |
305 | Summary of the proposed action The Joint Committee wishes to take a decision and to launch the invitation. The drafts decision nr. 1/2005 has an object the invitation of Romania to access to the Convention of 20 May 1987 on the simplification of formalities in trade in goods. The Commission is invited to approve this draft decision via the written procedure, in order to submit it to the Council in order to obtain a common position for its final adoption by the EC-EFTA on simplification of formalities in trade in goods Joint Committee at its next meeting. |
310 | Legal basis Article 11 (a) of the Convention of 20 May 1987 on the simplification of formalities in trade in goods. |
329 | Subsidiarity principle The proposal falls under the exclusive competence of the Community. The subsidiarity principle therefore does not apply. |
Proportionality principle The proposal complies with the proportionality principle for the following reason(s). |
331 | The form of proposed action is the only possible. |
332 | The form of proposed action does not comprise any financing cost. |
Choice of instruments |
341 | Proposed instruments: other. |
342 | Other means would not be adequate for the following reason(s). There is not any other adequate instrument. |
BUDGETARY IMPLICATION |
409 | The proposal has no implication for the Community budget. |
ADDITIONAL INFORMATION |
510 | Simplification |
511 | The proposal provides for simplification of administrative procedures for public authorities (EU or national), simplification of administrative procedures for private parties. |
513 | The proposition introduces a simplification of formalities in trade in goods for all the contracting parties to the Convention. |
514 | The simplification of the formalities in trade in goods allows to simplify the application of the common transit procedure. |
1. Draft
DECISION No 1/2005 OF THE EC-EFTA JOINT COMMITTEE ON SIMPLIFICATION OF FORMALITIES IN TRADE IN GOODS
concerning the invitation to Romania, to accede to the Convention of 20 May 1987 on the simplification of formalities in trade in goods
THE JOINT COMMITTEE,
Having regard to the Convention of 20 May 1987 on the simplification of formalities in trade in goods,[1] and in particular Article 11(3) thereof,
Whereas:
(1) In preparing the enlargement of the European Union to Romania the exchange of goods with this country would be facilitated by the simplification of formalities which affect the trade in goods between this country and the European Community, the Republic of Iceland, the Kingdom of Norway and the Swiss Confederation.
(2) With a view to achieving such facilitation it is appropriate to invite this country to accede to the Convention.
HAS DECIDED AS FOLLOWS:
Article 1
Romania is invited, in the form of an exchange of letters between the Council of the European Union and Romania, shown in the Annex to this Decision, to accede to the Convention in accordance with Article 11a of this Convention as from […..]
Article 2
This Decision shall enter into force on the date of its adoption.
Done at Brussels
For the Joint committee
The President
ANNEX
Letter No 1
Communication of the Decision of the EC-EFTA Joint Committeeto invite Romania to accede to theConvention of 20 May 1987 on the simplification of formalities in trade in goods
Sir,
I have the honour to apprise you of the Decision of the EC-EFTA Joint Committee on simplification of formalities in trade in goods of ……. (Decision No1/2005) inviting Romania to become a Contracting Party to the Convention of 20 May 1987 on the simplification of formalities in trade in goods.
The accession by Romania to the Convention may be effected by lodging its Instrument of Accession with the General Secretariat of the Council of the European Union together with a translation of the Convention in the Official language of Romania in accordance with Article 11a of the Convention.
Please accept, Sir, the assurance of my highest consideration.
General Secretary General Secretariat of the Council of the European Union
Letter No 2
Instrument of Accession of Romaniato the Convention on the simplification of formalities in trade in goods
Romania,
Acknowledging the Decision of the EC-EFTA Joint Committee on simplification of formalities in trade in goods of …. (Decision No 1/2005) to invite Romania to accede to the Convention of 20 May 1987 on the simplification of formalities in trade in goods ("the Convention"),
Desiring to become a Contracting Party to that Convention,
HEREBY
Accedes to the Convention;
Attaches to this Instrument a translation of the Convention in the official language of Romania;
Declares that it accepts all the Recommendations and Decisions of the EC-EFTA Joint Committee on simplification of formalities in trade in goods adopted between the date of the Decision of ……… and the date that the Accession of Romania becomes effective in accordance with Article 11a of the Convention.
Done at
[1] OJ L 134, 22.05.1987, p. 2.
