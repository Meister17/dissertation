Commission Decision
of 11 March 2005
amending Decision 2004/432/EC on the approval of residue monitoring plans submitted by third countries in accordance with Council Directive 96/23/EC
(notified under document number C(2005) 568)
(Text with EEA relevance)
(2005/233/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 96/23/EC of 29 April 1996 on measures to monitor certain substances and residues thereof in live animals and animal products and repealing Directives 85/358/EEC and 86/469/EEC and Decisions 89/187/EEC and 91/664/EEC [1], and in particular the fourth subparagraph of Article 29(1) thereof,
Whereas:
(1) Under Directive 96/23/EC inclusion and retention on the lists of third countries, provided for in Community legislation, from which Member States are authorised to import animals and primary products of animal origin covered by that Directive, are subject to submission by the third countries concerned of a plan setting out the guarantees which they offer as regards the monitoring of the groups of residues and substances referred to in that Directive. That Directive also lays down certain requirements concerning time limits for submission of plans.
(2) Commission Decision 2004/432/EC [2] lists the third countries which have submitted a residue monitoring plan, setting out the guarantees offered by them in compliance with the requirements of that Directive.
(3) Certain third countries have presented residue monitoring plans to the Commission for animals and products not listed in Decision 2004/432/EC. The evaluation of those plans and the additional information requested by the Commission provide sufficient guarantees on the residue monitoring in those countries for the animals and products concerned. Those animals and products should therefore be included in the list for those third countries.
(4) Decision 2004/432/EC should therefore be amended accordingly.
(5) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
The Annex to Decision 2004/432/EC is replaced by the Annex to this Decision.
Article 2
This Decision shall apply from 21 March 2005.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 11 March 2005.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 125, 23.5.1996, p. 10. Directive as last amended by Regulation (EC) No 882/2004 of the European Parliament and of the Council (OJ L 165, 30.4.2004, p. 1).
[2] OJ L 154, 30.4.2004, p. 44. Decision as amended by Decision 2004/685/EC (OJ L 312, 9.10.2004, p. 19).
--------------------------------------------------
ANNEX
--------------------------------------------------
