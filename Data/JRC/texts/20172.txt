COMMON POSITION
of 20 November 1995
defined by the Council on the basis of Article J.2 of the Treaty on European Union, on Nigeria
(95/515/CFSP)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty on European Union, and in particular Article J.2 thereof,
HAS DEFINED THE FOLLOWING COMMON POSITION:
1. It strongly condemns the execution, on 10 November, of Mr Ken Saro-Wiwa and his eight co-defendants. This constituted a clear failure by Nigeria to honour its commitment to human rights, as it stems from a number of international instruments to which Nigeria is a party.
2. The European Union condemns the human rights abuses perpetrated by the military regime, including capital punishment and harsh prison sentences, implemented after flawed judicial process and without granting the possibility of recourse to a higher court. In this context, it expresses its particular concern at the detention without trial of political figures and the suspension of habeas corpus.
3. The European Union recalls its deep concern at the annulment in June 1993 of elections that were considered free and fair and the subsequent installation of a new military dictatorship. It notes that the military regime has yet to demonstrate convincingly its intention to return to civilian democratic rule within a credible and rapid time-frame and:
(a) reaffirms the following measures adopted in 1993:
- suspension of military cooperation,
- visa restrictions for members of the military or the security forces, and their families,
- suspension of visits of members of the military,
- restriction of movement of all military personnel of Nigerian diplomatic missions,
- cancellation of training courses for all Nigerian military personnel,
- suspension of all high-level visits that are not indispensable to and from Nigeria;
(b) introduces the following, additional measures:
(i) visa restrictions on members of the Provisional Ruling Council and the Federal Executive Council and their families,
(ii) an embargo on arms, munitions and military equipment (1).
4. Development cooperation with Nigeria is suspended. Exceptions may be made for projects and programmes in support of human rights and democracy as well as those concentrating on poverty alleviation and, in particular, the provision of basic needs for the poorest section of the population, in the context of decentralized cooperation through local civilian authorities and non-governmental organizations.
5. This Common Position will be monitored by the Council, to which the Presidency and the Commission will regularly report and will be reviewed in the light of developments in Nigeria. Further measures are under examination.
6. This Common Position shall take effect on 20 November 1995.
7. This Common Position shall be published in the Official Journal.
Done at Brussels, 20 November 1995.
For the Council
The President
J. SOLANA
(1) The aforementioned embargo covers weapons designed to kill and their ammunition, weapon platforms, non-weapon platforms and ancillary equipment. The embargo also covers spare parts, repairs, maintenance and transfer of military technology. Contracts entered into prior to the date of entry into force of the embargo are not affected by this Common Position.
