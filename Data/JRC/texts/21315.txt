Commission Decision
of 29 July 2002
establishing the European Regulators Group for Electronic Communications Networks and Services
(Text with EEA relevance)
(2002/627/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Whereas:
(1) A new regulatory framework for electronic communications networks and services has been established in accordance with European Parliament and Council Directives 2002/21/EC of 7 March 2002 on a common regulatory framework for electronic communications networks and services (Framework Directive)(1), 2002/19/EC of 7 March 2002 on access to, and interconnection of, electronic communications networks and associated facilities (Access Directive)(2), 2002/20/EC of 7 March 2002 on the authorisation of electronic communications networks and services (Authorisation Directive)(3) and 2002/22/EC of 7 March 2002 on the universal services and users' rights related to electronic communications networks and services (Universal Service Directive)(4).
(2) National regulatory authorities have been set up in all Member States to carry out the regulatory tasks specified in these Directives and as to be notified to the Commission in accordance with Article 3(6) of the Framework Directive. In accordance with the Framework Directive, Member States must guarantee the independence of national regulatory authorities by ensuring that they are legally distinct from and functionally independent of all organisations providing electronic communications networks, equipment or services. Member States that retain ownership or control of undertakings providing electronic communications networks and/or services must also ensure effective structural separation of the regulatory function from activities associated with ownership or control.
(3) Detailed responsibilities and tasks of the national regulatory authorities differ among the various Member States, but all of them have at least one national regulatory authority who is charged with application of the rules once they have been transposed into national law, in particular the rules concerning day-to-day supervision of the market.
(4) The need for the relevant rules to be consistently applied in all Member States is essential for the successful development of an internal market for electronic communications networks and services. The new regulatory framework sets out objectives to be achieved and provides a framework for action by national regulatory authorities, whilst granting them flexibility in certain areas to apply the rules in the light of national conditions.
(5) A European Regulators Group for Electronic Communications Networks and Services (hereinafter referred to as the Group) should be established to provide an interface for advising and assisting the Commission in the electronic communications field.
(6) The Group should provide an interface between national regulatory authorities and the Commission in such a way as to contribute to the development of the internal market. It should also allow cooperation between national regulatory authorities and the Commission in a transparent manner so as to ensure the consistent application in all Member States of the regulatory framework for electronic communications networks and services.
(7) The Group should serve as a body for reflection, debate and advice for the Commission in the electronic communications field, including on matters related to the implementation and revision of Recommendation on Relevant Product and Service Markets and in drawing up the Decision on transnational markets.
(8) Close cooperation should be maintained between the Group and the Communications Committee established under the Framework Directive. The work of the Group should not interfere with the work of the Committee.
(9) Coordination should be ensured with the Radio Spectrum Committee established under a Decision No 676/2002/EC of the European Parliament and Council of 7 March 2002 on a regulatory framework for radio spectrum policy in the European Community (Radio Spectrum Decision)(5), the Radio Spectrum Policy Group established under the Commission Decision 2002/622/EC of 26 July 2002 establishing a Radio Spectrum Policy Group(6) and the Television Without frontiers Contact Committee, created pursuant to Directive 97/36/EC of the European Parliament and of the Council(7) on the coordination of certain provisions laid down by law, regulation or administrative action in Member States concerning the pursuit of television broadcasting activities,
HAS DECIDED AS FOLLOWS:
Article 1
Subject matter
An advisory group of the independent national regulatory authorities on electronic communications networks and services, called the European Regulators Group for Electronic Communications Networks and Services (hereinafter referred to as the Group), is hereby established.
Article 2
Definition
For the purpose of this Decision: "relevant national regulatory authority" means the public authority established in each Member State to oversee the day-to-day interpretation and application of the provisions of the Directives relating to electronic communications network and services as defined in "Framework" Directive.
Article 3
Aims
The role of the Group shall be to advise and assist the Commission in consolidating the internal market for electronic communications networks and services.
The Group shall provide an interface between national regulatory authorities and the Commission in such a way as to contribute to the development of the internal market and to the consistent application in all Member States of the regulatory framework for electronic communications networks and services.
Article 4
Membership
The Group shall be composed of the heads of each relevant national regulatory authority in each Member State or their representatives.
The Commission shall be represented at an appropriate level and shall provide the secretariat to the Group.
Article 5
Operational arrangements
At its own initiative or at the Commission's request the Group shall advise and assist the Commission on any matter related to electronic communications networks and services.
The Group shall elect a chairperson from among its members. The work of the group may be organised into subgroups and expert working groups as appropriate.
The chairperson shall convene the meetings of the Group in agreement with the Commission.
The Group shall adopt its rules of procedure by consensus or, in the absence of consensus, by a two-thirds majority vote, one vote being expressed per Member State, subject to the approval of the Commission.
The Commission shall be represented at all meetings of the Group and be able to attend all meetings of its subgroups and expert working groups.
Experts from EEA States and those states that are candidates for accession to the European Union may participate as observers in the Group. The Group may invite other experts and observers to attend its meetings
Article 6
Consultation
The Group shall consult extensively and at an early stage with market participants, consumers and end-users in an open and transparent manner.
Article 7
Confidentiality
Without prejudice to the provisions of Article 287 of the Treaty, where the Commission informs them that the advice requested or the question raised is of a confidential nature, members of the Group as well as observers and any other person shall be under an obligation not to disclose information which has come to their knowledge through the work of the Group, its subgroups or expert groups. The Commission may decide in such cases that only members of the Group may be present at meetings.
Article 8
Annual report
The Group shall submit an annual report of its activities to the Commission. The Commission shall transmit the report to the European Parliament and to the Council, where appropriate with comments.
Article 9
Entry into force
This Decision shall enter into force the day of its publication in the Official Journal of the European Communities.
The Group shall take up its duties on the date of entry into force of this Decision.
Done at Brussels, 29 July 2002.
For the Commission
Erkki Liikanen
Member of the Commission
(1) OJ L 108, 24.4.2002, p. 33.
(2) OJ L 108, 24.4.2002, p. 7.
(3) OJ L 108, 24.4.2002, p. 21.
(4) OJ L 108, 24.4.2002, p. 51.
(5) OJ L 108, 24.4.2002, p. 1.
(6) OJ L 198, 27.7.2002, p. 49.
(7) OJ L 202, 30.7.1997, p. 60.
