Decision of the President of the Court of Justice recording that the European Union Civil Service Tribunal has been constituted in accordance with law
THE PRESIDENT OF THE COURT OF JUSTICE,
Having regard to Article 225a of the Treaty establishing the European Community,
Having regard to Article 140b of the Treaty establishing the European Atomic Energy Community,
Having regard to Council Decision 2004/752/EC, Euratom of 2 November 2004 establishing the European Union Civil Service Tribunal [1], and in particular Article 4 thereof,
Whereas
(1) The Judges of the Civil Service Tribunal appointed by Council Decision 2005/577/EC, Euratom of 22 July 2005 appointing Judges of the European Union Civil Service Tribunal [2] have taken oath before the Court of Justice.
(2) In accordance with Article 3 of Decision 2005/577/EC, Euratom, for the purposes of appointing the first President of the Civil Service Tribunal, the procedure laid down in Article 4(1) of Annex I to the Statute of the Court of Justice is to apply.
(3) Following that procedure the Judges of the Civil Service Tribunal have appointed the President of that Tribunal.
(4) The Civil Service Tribunal has appointed its Registrar who has taken the oath provided for.
(5) The Civil Service Tribunal is accordingly in a position to carry out the judicial duties entrusted to it,
DECLARES
That the Civil Service Tribunal is duly constituted;
And that Article 1 of Annex I to the Protocol on the Statute of the Court of Justice shall enter into force on the day of the publication of this decision in the Official Journal of the European Union.
Done at Luxembourg, 2 December 2005.
The President of the Court
[1] OJ L 333, 9.11.2004, p. 7.
[2] OJ L 197, 28.7.2005, p. 28.
--------------------------------------------------
