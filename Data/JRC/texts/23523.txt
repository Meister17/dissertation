COMMISSION REGULATION (EC) No 1980/98 of 17 September 1998 opening and providing for the administration of Community tariff quotas and tariff ceilings and establishing a Community surveillance in the framework of reference quantities for certain agricultural products originating in the African, Caribbean and Pacific (ACP) States
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1706/98 of 20 July 1998 on the arrangements applicable to agricultural products and goods resulting from the processing of agricultural products originating in the African, Caribbean and Pacific States (ACP States) and repealing Regulation (EEC) No 715/90 (1), and in particular Article 30 thereof,
Whereas the fourth ACP-EC Convention (2), hereinafter referred to as 'the Convention`, provides for customs duties on imports into the Community of certain products originating in the ACP States to be waived or reduced within the framework of tariff quotas, tariff ceilings or reference quantities; whereas the tariff quotas, tariff ceilings or reference quantities provided for in the Convention are to be opened annually until the Convention expires;
Whereas it falls to the Commission to adopt implementing measures relating to the opening and administration of Community tariff quotas, tariff ceilings and reference quantities;
Whereas Commission Regulation (EEC) No 2454/93 of 2 July 1993 laying down provisions for the implementation of Council Regulation (EEC) No 2913/92 establishing the Community Customs Code (3), as last amended by Regulation (EC) No 1677/98 (4), has consolidated the provisions for the management of tariff quotas to be used according to the chronological order of dates of acceptance of declarations for release for free circulation and the provisions governing the surveillance of preferential imports;
Whereas it was agreed at the negotiations for the mid-term revision of the Lomé Convention that changes to the arrangements should take effect from 1 January 1996; whereas provision should therefore be made for the application of this Regulation and the repeal of Commission Regulations (EC) No 1280/94 (5), as amended by Regulation (EC) No 896/95 (6), (EC) No 2763/94 (7), as last amended by Regulation (EC) No 2411/96 (8), and (EC) No 2942/95 (9), as amended by Regulation (EC) No 982/96 (10), to take effect from that date;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Customs Code Committee,
HAS ADOPTED THIS REGULATION:
Article 1
1. Customs duties on products listed in Annex A originating in the ACP States, released for free circulation in the Community and accompanied by proof of origin in accordance with the Origin Protocol to the Convention may be waived or reduced within the limits of the tariff quotas specified in that Annex.
2. The tariff quotas referred to in this Article shall be managed by the Commission in accordance with Article 308a to 308c of Regulation (EEC) No 2454/93.
3. Each Member State shall ensure that importers of the products in question have equal and continuous access to the quotas for as long as the balance of the relevant quota volume so permits.
Article 2
1. Duties on products listed in Annex B originating in the ACP States, released for free circulation in the Community and accompanied by proof of origin in accordance with the Origin Protocol to the Convention may be waived within the limits of the tariff ceiling specified in that Annex.
2. The tariff ceiling referred to in this Article shall be subject to Community surveillance by the Commission, in close cooperation with the Member States, in accordance with Article 308d of Regulation (EEC) No 2454/93.
Article 3
1. Duties on products listed in Annex C originating in the ACP States, released for free circulation in the Community and accompanied by proof of origin in accordance with the Origin Protocol to the Convention may be waived within the limits of the reference quantities specified in that Annex, such products being subject to Community surveillance.
2. The take up of reference quantities shall be recorded at Community level using data sent to the Commission by the Member States in accordance with Article 308d of Regulation (EEC) No 2454/93.
Article 4
The Commission, in close cooperation with the Member States, shall take whatever steps are necessary for the application of this Regulation.
Article 5
Regulations (EC) No 1280/94, (EC) No 2763/94 and (EC) No 2942/95 are hereby repealed.
Article 6
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Communities.
It shall apply with effect from 1 January 1996.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 17 September 1998.
For the Commission
Mario MONTI
Member of the Commission
(1) OJ L 215, 1. 8. 1998, p. 12.
(2) OJ L 229, 17. 8. 1991, p. 3.
(3) OJ L 253, 11. 10. 1993, p. 1.
(4) OJ L 212, 30. 7. 1998, p. 18.
(5) OJ L 140, 3. 6. 1994, p. 10.
(6) OJ L 92, 25. 4. 1995, p. 12.
(7) OJ L 294, 15. 11. 1994, p. 6.
(8) OJ L 329, 19. 12. 1996, p. 8.
(9) OJ L 308, 21. 12. 1995, p. 9.
(10) OJ L 131, 1. 6. 1996, p. 43.
ANNEX A concerning products referred to in Article 1
Notwithstanding the rules for the interpretation of the Combined Nomenclature, the wording for the description of the products is to be considered as having no more than an indicative value, the preferential scheme being determined, within the context of this Annex, by the coverage of the CN codes as they exist at the time of adoption of the current Regulation. Where ex CN codes are indicated, the preferential scheme is to be determined by application of the CN code and corresponding description taken together.
>TABLE>
ANNEX B concerning products referred to in Article 2
Notwithstanding the rules for the interpretation of the Combined Nomenclature, the wording for the description of the products is to be considered as having no more than an indicative value, the preferential scheme being determined, within the context of this Annex, by the coverage of the CN codes as they exist at the time of adoption of the current Regulation. Where ex CN codes are indicated, the preferential scheme is to be determined by application of the CN code and corresponding description taken together.
>TABLE>
ANNEX C concerning products referred to in Article 3
Notwithstanding the rules for the interpretation of the Combined Nomenclature, the wording for the description of the products is to be considered as having no more than an indicative value, the preferential scheme being determined, within the context of this Annex, by the coverage of the CN codes as they exist at the time of adoption of the current Regulation. Where ex CN codes are indicated, the preferential scheme is to be determined by application of the CN code and corresponding description taken together.
>TABLE>
