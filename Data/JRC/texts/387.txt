Council Regulation (EC) No 1010/2000
of 8 May 2000
concerning further calls of foreign reserve assets by the European Central Bank
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Protocol on the Statute of the European System of Central Banks and of the European Central Bank (hereinafter referred to as the "Statute") and in particular Article 30(4) thereof,
Having regard to the recommendation of the European Central Bank (ECB)(1),
Having regard to the opinion of the European Parliament(2),
Having regard to the opinion of the Commission of the European Communities(3),
Acting in accordance with the procedure laid down in Article 107(6) of the Treaty establishing the European Community ("the Treaty") and Article 42 of the Statute and under the conditions set out in Article 122(5) of the Treaty and paragraph 7 of the Protocol on certain provisions relating to the United Kingdom of Great Britain and Northern Ireland,
Whereas:
(1) Article 30(1) of the Statute requires that the ECB be provided by the national central banks of participating Member States with foreign reserve assets, other than Member States' currencies, euro, IMF reserve positions and SDRs, up to an amount equivalent to EUR 50000 million.
(2) Article 30(4) of the Statute provides that further calls of foreign reserve assets beyond the limit set in Article 30(1) of the Statute may be effected by the ECB within the limits and under the conditions set by the Council.
(3) Article 123(1), in conjunction with Article 107(6), of the Treaty provides that immediately after 1 July 1998 the Council shall adopt the provision referred to in Article 30(4) of the Statute.
(4) This Regulation establishes a limit for further calls of foreign reserve assets, thereby enabling the Governing Council of the ECB to decide on actual calls at some point in the future in order to replenish already depleted holdings of foreign reserve assets and not to increase the ECB's holdings beyond the maximum amount equivalent to EUR 50000 million set for the initial transfers of foreign reserve assets by the national central banks to the ECB.
(5) Any replenishment of foreign reserves is not to entail an increase in the subscribed capital of the ECB.
(6) Article 30(4) of the Statute requires that further calls of foreign reserve assets be effected in accordance with Article 30(2) of the Statute; Article 30(2) in conjunction with Article 43(6) of the Statute and paragraph 10(b) of the Protocol on certain provisions relating to the United Kingdom of Great Britain and Northern Ireland requires that the contributions of each national central bank shall be fixed in proportion to its share of the capital of the ECB subscribed by the national central banks of participating Member States.
(7) Article 10(3) in conjunction with Article 43(4) of the Statute requires that for any decisions to be taken under Article 30 of the Statute, the votes in the Governing Council of the ECB shall be weighted according to the shares of the national central banks of participating Member States in the subscribed capital of the ECB.
(8) Article 30(4) of the Statute, in conjunction with Articles 43(4) and 43(6) of the Statute and paragraph 8 of the Protocol on certain provisions relating to the United Kingdom of Great Britain and Northern Ireland and paragraph 2 of the Protocol on certain provisions relating to Denmark, does not confer any rights or impose any obligations on the non-participating Member States.
(9) Article 49(1) of the Statute in conjunction with paragraph 10(b) of the Protocol on certain provisions relating to the United Kingdom of Great Britain and Northern Ireland requires that the central bank of a Member State whose derogation has been abrogated, or who is treated on the same basis as the central bank of a Member State whose derogation has been abrogated, is to transfer to the ECB foreign reserve assets in accordance with Article 30(1) of the Statute. Article 49(1) of the Statute requires that the sum to be transfered is to be determined by multiplying the euro value at current exchange rates of the foreign reserve assets which have already been transferred to the ECB in accordance with Article 30(1), by the ratio between the number of shares subscribed by the national central bank concerned and the number of shares already paid up by the other national central banks.
(10) All references to amounts in euro in the aforementioned provisions of the Treaty, in this Regulation and in any call of or request by the ECB for foreign reserve assets are references to nominal amounts in euro at the time a call of such foreign reserve assets by the ECB is effected,
HAS ADOPTED THIS REGULATION:
Article 1
Definitions
For the purposes of this Regulation:
- "foreign reserve assets" mean any official foreign reserve assets of the participating Member States held by national central banks that are denominated in, or comprise, currencies, units of account or gold other than Member States' currencies, euro, IMF reserve positions and SDRs,
- "national central bank" means the central bank of a participating Member State, and
- "participating Member State" means a Member State which has adopted the single currency in accordance with the Treaty.
Article 2
Further calls of foreign reserve assets
1. The ECB may effect further calls of foreign reserve assets from the national central banks beyond the limit set in Article 30(1) of the Statute, up to an amount equivalent to an additional EUR 50000 million, in case of need for such foreign reserve assets.
2. The central bank of a Member State whose derogation has been abrogated, or who is treated on the same basis as the central bank of a Member State whose derogation has been abrogated, shall transfer to the ECB a sum of foreign reserve assets determined by multiplying the euro value at current exchange rates of the foreign reserve assets which have already been transferred to the ECB in accordance with paragraph 1, by the ratio between the number of shares subscribed by the central bank concerned and the number of shares already paid up by the other national central banks.
Article 3
Final provisions
This Regulation shall enter into force on the day following that of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in the Member States in accordance with the Treaty establishing the European Community.
Done at Brussels, 8 May 2000.
For the Council
The President
J. Pina Moura
(1) OJ C 269, 23.9.1999, p. 9.
(2) Opinion delivered on 17 March 2000 (not yet published in the Official Journal).
(3) Opinion delivered on 8 March 2000 (not yet published in the Official Journal).
