Action brought on 14 September 2005 — Henkel v OHIM
Parties
Applicant(s): Henkel KGaA (Düsseldorf, Germany) (represented by: C. Osterrieth, lawyer)
Defendant: Office for Harmonisation in the Internal Market (Trade Marks and Designs)
Other party or parties to the proceedings before the Board of Appeal of OHIM: Serra Y Roca S.A. (Barcelona, Spain)
Form of order sought
The applicant claims that the Court should:
- Annul the decision of the First Board of Appeal of the Office for Harmonisation in the Internal Market of 14 July 2005 in the appeal proceedings R 0556/2003-1 regarding the application for Community trade mark No 1 284 470, served on 19 July 2005;
- Order the Office for Harmonisation in the Internal Market to pay the costs.
Pleas in law and main arguments
Applicant for a Community trade mark: SERRA Y ROCA, S.A.
Community trade mark concerned: The word mark "COR" for goods in Class 3 — application No 1 284 470
Proprietor of the mark or sign cited in the opposition proceedings: The applicant
Mark or sign cited in opposition: The national mark "Dor" for goods in Classes 3, 5 and 21
Decision of the Opposition Division: Rejection of the opposition in respect of the goods "scouring and abrasive preparations; soaps" in Class 3
Decision of the Board of Appeal: Dismissal of the applicant's appeal
Pleas in law: Infringement of Article 8(1)(b) of Council Regulation No 40/94 because of likelihood of confusion of the marks in question due to visual and aural similarity. In addition the applicant's mark has above average distinctive character due to intensive use.
--------------------------------------------------
