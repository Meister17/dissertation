Commission Decision
of 21 April 2006
amending Commission Decision 2004/407/EC as regards imports of photographic gelatine
(notified under document number C(2006) 1627)
(Only the Dutch, English, French and German texts are authentic)
(2006/311/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Regulation (EC) No 1774/2002 of the European Parliament and of the Council of 3 October 2002 laying down health rules concerning animal by-products not intended for human consumption [1], and in particular Articles 4(4) and 32(1) thereof,
Whereas:
(1) Regulation (EC) No 1774/2002 prohibits the importation and transit of animal by-products and processed products into the Community, except in accordance with that Regulation.
(2) Commission Decision 2004/407/EC of 26 April 2004 on transitional sanitary and certification rules under Regulation (EC) No 1774/2002 of the European Parliament and of the Council as regards import from certain third countries of photographic gelatine [2] provides, by way of derogation from that prohibition in Regulation (EC) No 1774/2002, that France, the Netherlands and the United Kingdom are to authorise the import of certain gelatine exclusively intended for the photographic industry (photographic gelatine).
(3) Decision 2004/407/EC provides that photographic gelatine is only allowed from the third countries listed in that Decision, namely Japan and the United States of America.
(4) Luxembourg has confirmed the need to source photographic gelatine from the United States of America for the purposes of the photographic industry in Luxembourg. Accordingly Luxembourg should be allowed to authorise the import of photographic gelatine subject to compliance with the conditions set out in Decision 2004/407/EC. However, those imports may take place in Belgium.
(5) In order to facilitate the transfer from Belgium to Luxembourg of the imported photographic gelatine, the conditions in Annexes I and III to Decision 2004/407/EC should be amended slightly.
(6) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
Decision 2004/407/EC is amended as follows:
1. Article 1 is replaced by the following:
"Article 1
Derogation regarding the import of photographic gelatine
By way of derogation from Article 29(1) of Regulation (EC) No 1774/2002, Belgium, France, Luxembourg, the Netherlands and the United Kingdom shall authorise the import of gelatine produced from materials containing bovine vertebral column classified as Category 1 material under that Regulation, exclusively intended for the photographic industry (photographic gelatine), in compliance with this Decision."
2. Article 9 is replaced by the following:
"Article 9
Addresses
This Decision is addressed to the Kingdom of Belgium, the French Republic, the Grand Duchy of Luxembourg, the Kingdom of the Netherlands and the United Kingdom of Great Britain and Northern Ireland."
3. Annexes I and III are amended in accordance with the Annex to this Decision.
Article 2
This Decision shall apply on the third day following that of its publication in the Official Journal of the European Union.
Article 3
This Decision is addressed to the Kingdom of Belgium, the French Republic, the Grand Duchy of Luxembourg, the Kingdom of the Netherlands and the United Kingdom of Great Britain and Northern Ireland.
Done at Brussels, 21 April 2006.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 273, 10.10.2002, p. 1. Regulation as last amended by Commission Regulation (EC) No 208/2006 (OJ L 36, 8.2.2006, p. 25).
[2] OJ L 208, 10.6.2004, p. 9.
--------------------------------------------------
ANNEX
1. Annex I is replaced by the following:
"
"ANNEX I
Third countries and plants of origin, Member States of destination, border inspection posts of first entry and approved photographic factories
Third Country of origin | Plants of origin | Member State of destination | Border Inspection Post of first entry | Approved Photographic Factories |
Japan | Nitta Gelatin Inc.2-22 FutamataYao-City, Osaka581 — 0024 JapanJellie Co. Ltd.7-1, Wakabayashi 2-Chome,Wakabayashi-ku,Sendai-city, Miyagi,982 JapanNIPPI Inc. Gelatin Division1 Yumizawa-Cho,Fujinomiya CityShizuoka418 — 0073 Japan | The Netherlands | Rotterdam | Fuji Photo Film BV, Tilburg |
Japan | Nitta Gelatin Inc 2-22 Futamata Yao-City Osaka 581 — 0024, Japan | France | Le Havre | Kodak Zone Industrielle Nord, 71100 Châlon sur Saône |
The United Kingdom | Liverpool Felixstowe | Kodak Ltd Headstone Drive, Harrow, MIDDX HA4 4TY |
USA | Eastman Gelatine Corporation,227 Washington Street,Peabody, MA,01960 USAGelita North America,2445 Port Neal Industrial Road Sergeant Bluff, Iowa,51054 USA | Luxembourg | Antwerp Zaventem Luxembourg | DuPont Teijin Luxembourg SA PO Box 1681 L-1016 Luxembourg |
France | Le Havre | Kodak Zone Industrielle Nord, 71100 Châlon sur Saône |
The United Kingdom | Liverpool Felixstowe | Kodak Ltd Headstone Drive, Harrow, MIDDX HA4 4TY " |
"
2. Annex III is replaced by:
"
"ANNEX III
Model health certificates for the importation from third countries of technical gelatine to be used by the photographic industry
Notes
(a) Veterinary certificates for the importation of technical gelatine to be used by the photographic industry shall be produced by the exporting country, based on the model appearing in this Annex III. They shall contain the attestations that are required for any third country and, as the case may be, those supplementary guarantees that are required for the exporting third country or part thereof.
(b) The original of each certificate shall consist of a single page, both sides, or, where more text is required, it shall be in such a form that all pages needed are part of an integrated whole and indivisible.
(c) It shall be drawn up in at least one of the official languages of the EU Member State in which the inspection at the EU border inspection post shall be carried out and of the EU Member State of destination. However, these Member States may allow other languages, if necessary, accompanied by an official translation.
(d) If for reasons of identification of the items of the consignment, additional pages are attached to the certificate, these pages shall also be considered as forming part of the original of the certificate by the application of the signature and stamp of the certifying official veterinarian, in each of the pages.
(e) When the certificate, including additional schedules referred to in (d), comprises more than one page, each page shall be numbered — (page number) of (total number of pages) — on its bottom and shall bear the code number of the certificate that has been designated by the competent authority on its top.
(f) The original of the certificate must be completed and signed by an official veterinarian. In doing so, the competent authorities of the exporting country shall ensure that the principles of certification equivalent to those laid down in Council Directive 96/93/EC are followed.
(g) The colour of the signature shall be different to that of the printing. The same rule applies to stamps other than those embossed or watermark.
(h) The original of the certificate must accompany the consignment at the EU border inspection post until it reaches the photographic factory of destination.
+++++ TIFF +++++
+++++ TIFF +++++
"
--------------------------------------------------
