COMMISSION DECISION of 24 July 1997 amending Decision 92/167/EEC setting up a Committee of Experts on the Transit of Electricity between Grids (97/559/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Whereas for reasons of continuity of the work of the Committee of Experts on the Transit of Electricity between Grids set up by Commission Decision 92/167/EEC (1), as amended by the Act of Accession of Austria, Finland and Sweden, it is necessary that the term of office of the members be renewable;
Whereas, for reasons of legal certainty, it is also appropriate to delete certain provisions which have become obsolete;
Whereas Decision 92/167/EEC should be amended accordingly,
HAS DECIDED AS FOLLOWS:
Sole Article
Article 6 of Decision 92/167/EEC is amended as follows:
(a) paragraph 2 is replaced by the following:
'2. Their term of office shall be renewable once.`;
(b) paragraph 3 is deleted.
Done at Brussels, 24 July 1997.
For the Commission
Christos PAPOUTSIS
Member of the Commission
(1) OJ No L 74, 20. 3. 1992, p. 43.
