[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 21.06.2005
COM(2005) 264 final
2005/0112 (CNS)
Proposal for a
COUNCIL DECISION
on the signature and provisional application of the Agreement between the European Community and the Government of Australia on certain aspects of air services
Proposal for a
COUNCIL DECISION
on the conclusion of the Agreement between the European Community and the Government of Australia on certain aspects of air services
(presented by the Commission)
EXPLANATORY MEMORANDUM
International aviation relations between Member States and third countries have traditionally been governed by bilateral air services agreements between Member States and third countries, their Annexes and other related bilateral or multilateral arrangements.
Following the judgements of the Court of Justice of the European Communities in cases C-466/98, C-467/98, C-468/98, C-471/98, C-472/98, C-475/98 and C-476/98, the Community has exclusive competence with respect to various aspects of external aviation. The Court of Justice also clarified the right of Community air carriers to benefit from the right of establishment within the Community, including the right to non-discriminatory market access.
Traditional designation clauses in Member States’ bilateral air services agreements infringe Community law. They allow a third country to reject, withdraw or suspend the permissions or authorisations of an air carrier that has been designated by a Member State but that is not substantially owned and effectively controlled by that Member State or its nationals. This has been found to constitute discrimination against Community carriers established in the territory of a Member State but owned and controlled by nationals of other Member States. This is contrary to Article 43 of the Treaty which guarantees nationals of Member States who have exercised their freedom of establishment the same treatment in the host Member State as that accorded to nationals of that Member State.
Following the Court of Justice judgements, the Council authorised the Commission in June 2003 to open negotiations with third countries on the replacement of certain provisions in existing bilateral agreements with a Community agreement.[1]
In accordance with the mechanisms and directives in the Annex to the Council decision authorising the Commission to open negotiations with third countries on the replacement of certain provisions in existing bilateral agreements with a Community agreement, the Commission has negotiated an agreement with Australia that replaces certain provisions in the existing bilateral air services agreements between Member States and Australia. Article 2 of the Agreement replaces the traditional designation clauses with a Community designation clause, permitting all Community carriers to benefit from the right of establishment. Article 4 (Pricing) resolves conflicts between the existing bilateral air services agreements and Council Regulation No 2409/92 on fares and rates for air services which prohibits third country carriers from being price leaders on air services for carriage wholly within the Community.
The Council is asked to approve the decisions on the signature and provisional application and on the conclusion of the Agreement between the European Community and the Government of Australia on certain aspects of air services and to designate the persons authorised to sign the agreement on behalf of the Community.
Proposal for a
COUNCIL DECISION
on the signature and provisional application of the Agreement between the European Community and the Government of Australia on certain aspects of air services
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 80(2), in conjunction with the first sentence of the first subparagraph of Article 300 (2) thereof,
Having regard to the proposal from the Commission[2],
Whereas:
(1) The Council authorised the Commission on 5 June 2003 to open negotiations with third countries on the replacement of certain provisions in existing bilateral agreements with a Community agreement;
(2) On behalf of the Community, the Commission has negotiated an Agreement with the Government of Australia on certain aspects of air services in accordance with the mechanisms and directives in the Annex to the Council Decision authorising the Commission to open negotiations with third countries on the replacement of certain provisions in existing bilateral agreements with a Community agreement;
(3) Subject to its possible conclusion at a later date, the Agreement negotiated by the Commission should be signed and provisionally applied,
HAS DECIDED AS FOLLOWS:
Sole Article
1. Subject to its conclusion at a later date, the President of the Council is hereby authorised to designate the person or persons empowered to sign on behalf of the Community the Agreement between the European Community and the Government of Australia on certain aspects of air services.
2. Pending its entry into force, the Agreement shall be applied provisionally from the first day of the first month following the date on which the parties have notified each other of the completion of the necessary procedures for this purpose. The President of the Council is hereby authorised to make the notification provided in Article 7(2) of the Agreement.
3. The text of the Agreement is annexed to this decision.
Done at Brussels,
For the Council
The President
2005/0112 (CNS)
Proposal for a
COUNCIL DECISION
on the conclusion of the Agreement between the European Community and the Government of Australia on certain aspects of air services
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 80(2) in conjunction with the first sentence of the first subparagraph of Article 300 (2) and the first subparagraph of Article 300 (3) thereof,
Having regard to the proposal from the Commission[3],
Having regard to the opinion of the European Parliament[4],
Whereas:
(1) The Council authorised the Commission on 5 June 2003 to open negotiations with third countries on the replacement of certain provisions in existing bilateral agreements with a Community agreement;
(2) On behalf of the Community, the Commission has negotiated an Agreement with the Government of Australia on certain aspects of air services in accordance with the mechanisms and directives in the Annex to the Council Decision authorising the Commission to open negotiations with third countries on the replacement of certain provisions in existing bilateral agreements with a Community agreement;
(3) The Agreement was signed on behalf of the Community on subject to its possible conclusion at a later date, in conformity with Decision …/…/EC of the Council on [5];
(4) The Agreement should be approved,
HAS DECIDED AS FOLLOWS:
Article 1
1. The Agreement between the European Community and the Government of Australia on certain aspects of air services is approved on behalf of the Community.
2. The text of the Agreement is annexed to this decision.
Article 2
The President of the Council is authorised to designate the person empowered to make the notification provided in Article 7(1) of the Agreement.
Done at Brussels,
For the Council
The President
ANNEX
AGREEMENT
between the European Community and the Government of Australia
on certain aspects of air services
THE EUROPEAN COMMUNITY
of the one part, and
THE GOVERNMENT OF AUSTRALIA
of the other part
(hereinafter referred to as ‘the Contracting Parties’)
NOTING that the European Court of Justice has found that certain provisions of bilateral agreements entered into by several Member States with third countries are incompatible with Community law,
NOTING that a number of bilateral air services agreements have been concluded between several Member States of the European Community and Australia containing similar provisions and that there is an obligation on Member States to take all appropriate steps to eliminate incompatibilities between such agreements and the EC Treaty,
NOTING that the European Community has exclusive competence with respect to a number of aspects that may be included in bilateral air service agreements between Member States of the European Community and third countries,
NOTING that, under European Community law, Community air carriers established in a Member State have the right to non-discriminatory access to air routes between the Member States of the European Community and third countries,
HAVING REGARD to the agreements between the European Community and certain third countries providing for the possibility for the nationals of such third countries to acquire ownership in air carriers licensed in accordance with European Community law,
RECOGNISING that consistency between European Community law and provisions of bilateral air service agreements between Member States of the European Community and Australia will provide a sound legal basis for air services between the European Community and Australia and preserve the continuity of such air services,
NOTING that provisions of the bilateral air services agreements between Member States of the European Community and Australia, which are not inconsistent with European Community law, do not need to be amended or replaced,
NOTING that it is not a purpose of the European Community in this Agreement to increase the total volume of air traffic between the European Community and Australia, to affect the balance between Community air carriers and air carriers of Australia, or to prevail over the interpretation of the provisions of existing bilateral air service agreements concerning traffic rights,
HAVE AGREED AS FOLLOWS:
ARTICLE 1
General Provisions
1. For the purposes of this Agreement, ‘Member States’ shall mean Member States of the European Community; ‘Contracting Party’ shall mean a contracting party to this Agreement; ‘party’ shall mean the contracting party to the relevant bilateral air services agreement; ‘air carrier’ shall also mean airline; ‘territory of the European Community’ shall mean territories of the Member States to which the Treaty establishing the European Community applies.
2. References in each of the agreements listed in Annex 1 to nationals of the Member State that is a party to that agreement shall be understood as referring to nationals of the Member States of the European Community.
3. References in each of the agreements listed in Annex 1 to air carriers or airlines of the Member State that is a party to that agreement shall be understood as referring to air carriers or airlines designated by that Member State.
ARTICLE 2
Designation, Authorisation and Revocation
1. The provisions in paragraphs 3 and 4 of this Article shall prevail over the corresponding provisions in the articles listed in Annex 2 (a) and (b) respectively, in relation to the designation of air carriers by the Member State concerned, its authorisations and permissions granted by Australia, and the refusal, revocation, suspension or limitation of the authorisations or permissions of the air carrier, respectively.
2. The provisions in paragraphs 3 and 4 of this Article shall prevail over the corresponding provisions in the articles listed in Annex 2 (a) and (b) respectively, in relation to the designation of air carriers by Australia, its authorisations and permissions granted by the Member State concerned, and the refusal, revocation, suspension or limitation of the authorisations or permissions of the air carrier, respectively.
3. On receipt of such a designation, and of applications from the designated air carrier(s), in the form and manner prescribed for operating authorisations and technical permissions, each party shall, subject to paragraphs 4 and 5 grant the appropriate authorisations and permissions with minimum procedural delay, provided that:
a. in the case of an air carrier designated by a Member State:
(i.) the air carrier is established in the territory of the designating Member State under the Treaty establishing the European Community and has a valid Operating Licence from a Member State in accordance with European Community law; and
(ii.) effective regulatory control of the air carrier is exercised and maintained by the Member State responsible for issuing its Air Operators Certificate and the relevant aeronautical authority is clearly identified in the designation; and
(iii.) the air carrier has its principal place of business in the territory of the Member State from which it has received the valid Operating Licence ; and
(iv.) the air carrier is owned directly or through majority ownership and is effectively controlled by Member States and/or nationals of Member States, and/or by other states listed in Annex 3 and/or nationals of such other states.
b. in the case of an air carrier designated by Australia:
(i.) Australia has and maintains effective regulatory control of the air carrier; and
ii.) it has its principal place of business in Australia.
4. Either party may refuse, revoke, suspend or limit the operating authorisation or technical permissions of an air carrier designated by the other party where:
(a) in the case of an air carrier designated by a Member State:
(i.) the air carrier is not established in the territory of the designating Member State under the Treaty establishing the European Community or does not have a valid Operating Licence from a Member State in accordance with European Community law; or
(ii.) effective regulatory control of the air carrier is not exercised or not maintained by the Member State responsible for issuing its Air Operators Certificate, or the relevant aeronautical authority is not clearly identified in the designation; or
(iii.) the air carrier does not have its principal place of business in the territory of the Member State from which it has received its Operating Licence; or
(iv.) the air carrier is not owned directly or through majority ownership and is not effectively controlled by Member States and/or nationals of Member States, and/or by other states listed in Annex 3 and/or nationals of such other states; or
(v.) the air carrier is already authorised to operate under a bilateral agreement between Australia and another Member State and Australia can demonstrate that, by exercising traffic rights under this Agreement on a route that includes a point in that other Member State, it would be circumventing restrictions on the third or fourth or fifth freedom traffic rights imposed by that other agreement; or
(vi.) the air carrier holds an Air Operators Certificate issued by a Member State and there is no bilateral air services agreement between Australia and that Member State and Australia can demonstrate that the necessary traffic rights to conduct the proposed operation are not reciprocally available to the designated air carrier(s) of Australia;
(b) in the case of an air carrier designated by Australia:
(i.) Australia is not maintaining effective regulatory control of the air carrier; or
ii.) it does not have its principal place of business in Australia.
5. In exercising its right under paragraph 4, and without prejudice to its rights under paragraph 4(a) (v) and (vi) of this Article, Australia shall not discriminate between air carriers of Member States on the grounds of nationality.
ARTICLE 3
Rights with regard to regulatory control
1. The provisions in paragraph 2 of this Article shall complement the articles listed in Annex 2 (c).
2. Where a Member State (the first Member State) has designated an air carrier whose regulatory control is exercised and maintained by a second Member State, the rights of Australia under the safety provisions of the agreement between the first Member State that has designated the air carrier and Australia shall apply equally in respect of the adoption, exercise or maintenance of safety standards by that second Member State and in respect of the operating authorisation of that air carrier.
ARTICLE 4
Tariffs for carriage within the European Community
1. The provisions in paragraph 2 of this Article shall complement the articles listed in Annex 2 (d).
2. The tariffs to be charged by the air carrier(s) designated by Australia under an agreement listed in Annex 1 containing a provision listed in Annex 2 (d) for carriage wholly within the European Community shall be subject to European Community law.
ARTICLE 5
Annexes to the Agreement
The Annexes to this Agreement shall form an integral part thereof.
ARTICLE 6
Revision or amendment
The Parties may, at any time, revise or amend this Agreement by mutual consent.
ARTICLE 7
Entry into force
1. This Agreement shall enter in force when the Contracting Parties have notified each other in writing that their respective internal procedures necessary for its entry into force have been completed.
2. Notwithstanding paragraph 1, the Parties agree to provisionally apply this Agreement from the first day of the month following the date on which the Parties have notified each other of the completion of the procedures necessary for this purpose.
3. Agreements and other arrangements between Member States and Australia which, at the date of signature of this Agreement, have not yet entered into force and are not being applied provisionally are listed in Annex 1 (b). This Agreement shall apply to all such Agreements and arrangements upon their entry into force or provisional application.
ARTICLE 8
Termination
1. In the event that an agreement listed in Annex 1 is terminated, all provisions of this Agreement that relate to the agreement listed in Annex 1 concerned shall terminate at the same time.
2. In the event that all agreements listed in Annex 1 are terminated, this Agreement shall terminate at the same time.
IN WITNESS WHEREOF, the undersigned, being duly authorised, have signed this Agreement.
Done at [….] in duplicate, on this […] day of […, …] in the Czech, Danish, Dutch, English, Estonian, Finnish, French, German, Greek, Hungarian, Italian, Latvian, Lithuanian, Maltese, Polish, Portuguese, Slovak, Slovene, Spanish, and Swedish languages. In case of divergence the English text shall prevail over the other language texts.
FOR THE EUROPEAN COMMUNITY: FOR THE GOVERNMENT OF AUSTRALIA:
Annex 1
List of agreements referred to in Article 1 of this Agreement
(a) Air services agreements between the Commonwealth of Australia and Member States of the European Community which, at the date of signature of this Agreement, have been concluded, signed and/or are being applied provisionally
- Agreement between the Austrian Federal Government and the Government of the Commonwealth of Australia relating to Air Services done at Vienna on 22 March 1967, as amended; hereinafter referred to as “Australia-Austria Agreement”;
Amended by the Memorandum of Understanding signed at Vienna on 25 March 1999.
- Air Services Agreement between the Government of the Kingdom of Denmark and the Government of Australia initialled at Canberra on 16 October 1998; hereinafter referred to as “Draft Australia-Denmark Agreement” ;
Supplemented by the Memorandum of Understanding on the co-operation between the Scandinavian countries regarding Scandinavian Airlines System (SAS) initialled at Canberra on 16 October 1998;
Supplemented by the Agreed Minutes dated 16 October 1998.
- Agreement between the Government of the Republic of Finland and the Government of the Commonwealth of Australia relating to Air Services initialled on 15 June 1999; hereinafter referred to as “Draft Australia-Finland Agreement”;
Supplemented by the Memorandum of Understanding signed at Helsinki on 15 June 1999.
- Agreement between the Government of the Commonwealth of Australia and the Government of the French Republic and relating to Air Transport done at Canberra on 13 April 1965; hereinafter referred to as ”Australia-France Agreement” ;
Modified by Exchange of Letters signed in Paris on 22 December 1970 and 7 January 1971.
- Agreement between the Federal Republic of Germany and the Commonwealth of Australia relating to Air Transport done at Bonn on 22 May 1957, as amended; hereinafter referred to as “Australia-Germany Agreement”.
- Agreement between the Government of the Kingdom of Greece and the Government of the Commonwealth of Australia relating to Air Services done at Athens on 10 June 1971, as amended; hereinafter referred to as “Australia-Greece Agreement”.
- Agreement between the Government of the Hellenic Republic and the Government of Australia relating to Air Services initialled at Athens on 11 November 1997 and attached to the Memorandum of Understanding signed at Athens on 11 November 1997; hereinafter referred to as “Draft Revised Australia-Greece Agreement”.
- Air Transport Agreement between Ireland and Australia concluded by exchange of notes dated 26 November 1957 and 30 December 1957; hereinafter referred to as “Australia-Ireland Agreement”.
- Agreement between the Government of the Commonwealth of Australia and the Government of the Republic of Italy relating to Air Services done at Rome on 10 November 1960, as amended, hereinafter referred to as “Australia-Italy Agreement”.
- Agreement between the Government of Australia and the Government of the Grand-Duchy of Luxembourg on Air Services as annexed to the Memorandum of Understanding done at Luxembourg on 3 September 1997; hereinafter referred to as “Draft Australia-Luxembourg Agreement”.
- Agreement between the Government of Malta and the Government of Australia relating to Air Services, done at Canberra on 11 September 1996; hereinafter referred to as “Australia-Malta Agreement”;
Amended by exchange of letters on 1 December 2003.
- Agreement between the Government of the Kingdom of the Netherlands and the Government of the Commonwealth of Australia for the establishment of air services, done at Canberra on 25 September 1951; hereinafter referred to as “Australia-Netherlands Agreement”.
- Agreement between the Government of the Republic of Poland and the Government of Australia relating to Air Services, done in Warsaw on 28 April 2004; hereinafter referred to as “Australia-Poland Agreement”.
- Air Services Agreement between the Government of the Kingdom of Sweden and the Government of Australia initialled at Canberra on 16 October 1998; hereinafter referred to as “Draft Australia-Sweden Agreement”;
Supplemented by the Memorandum of Understanding on the co-operation between the Scandinavian countries regarding Scandinavian Airlines System (SAS) initialled at Canberra on 16 October 1998;
Supplemented by the Agreed Minutes dated 16 October 1998.
- Agreement between the Government of the United Kingdom of Great Britain and Northern Ireland and the Government of the Commonwealth of Australia for Air Services between and beyond their respective territories, done at London on 7 February 1958; hereinafter referred to as “Australia-United Kingdom Agreement”.
- Revised Agreement between the Government of the United Kingdom of Great Britain and Northern Ireland and the Government of Australia concerning Air Services as provided for in the Memorandum of Understanding signed at London on 14 November 1996; hereinafter referred to as “Draft Revised Australia-United Kingdom Agreement”;
Amended by the Agreed Minute signed at Canberra on 11 February 1999;
Amended by the Agreed Record signed at London on 28 October 1999.
(b) Air services agreements and other arrangements initialled or signed between the Commonwealth of Australia and Member States of the European Community which, at the date of signature of this Agreement, have not yet entered into force and are not being applied provisionally
[Annex 1b is intentionally left blank.]
Annex 2
List of articles in the agreements listed in Annex 1 and referred to in Articles 2 to 5 of this Agreement
(a) Designation by a Member State:
- Article 4 of the Australia-Austria Agreement;
- Article 3 of the Draft Australia-Denmark Agreement;
- Article 3 of the Australia-Germany Agreement;
- Article 4 of the Australia-Greece Agreement;
- Article 4 of the Draft Australia-Greece Agreement;
- Article 3 of the Draft Australia-Luxembourg Agreement;
- Article 4 of the Australia-Ireland Agreement;
- Article 4 of the Australia-Italy Agreement;
- Article 4 of the Australia-Malta Agreement;
- Article 3 of the Australia-Netherlands Agreement;
- Article 2 of the Australia-Poland Agreement;
- Article 3 of the Draft Australia-Sweden Agreement;
- Article 3 of the Draft Revised Australia-United Kingdom Agreement.
(b) Refusal, Revocation, Suspension or Limitation of Authorisations or Permissions:
- Article 7 of the Australia-Austria Agreement;
- Article 4 of the Draft Australia-Denmark Agreement;
- Article 5 of the Draft Australia-Finland Agreement;
- Article 8 of the Australia-France Agreement;
- Article 4 of the Australia-Germany Agreement;
- Article 5 of the Australia-Greece Agreement;
- Article 5 of the Draft Australia-Greece Agreement;
- Article 7 of the Australia-Ireland Agreement;
- Article 5 of the Australia-Italy Agreement;
- Article 4 of the Draft Australia-Luxembourg Agreement;
- Article 5 of the Australia-Malta Agreement;
- Article 6 of the Australia-Netherlands Agreement;
- Article 2 of the Australia-Poland Agreement;
- Article 4 of the Draft Australia-Sweden Agreement;
- Article 3 of the Draft Revised Australia-United Kingdom Agreement.
(c) Regulatory control:
- Article 10 of the Australia-Austria Agreement;
- Article 17 of the Draft Australia-Denmark Agreement;
- Article 8 of the Draft Australia-Finland Agreement;
- Attachment C to the Memorandum of Understanding between the aeronautical authorities of the Government of Australia and the Government of the Federal Republic of Germany, signed at Canberra on 12 June 1998 – as applied provisionally in the framework of the Australia – Germany Agreement;
- Article 8 of the Draft Australia-Greece Agreement;
- Article 7 of the Draft Australia-Luxembourg Agreement;
- Article 8 of the Australia-Malta Agreement;
- Attachment C to the Memorandum of Understanding between the aeronautical authorities of the Government of Australia and the Government of the Kingdom of the Netherlands, signed at The Hague on 4 September 1997 – as applied provisionally in the framework of the Australia-Netherlands Agreement
- Article 5 of the Australia-Poland Agreement;
- Article 17 of the Draft Australia-Sweden Agreement;
- Article 11 of the Draft Revised Australia-United Kingdom Agreement.
(d) Tariffs for carriage within the European Community:
- Article 11 of the Australia-Austria Agreement;
- Article 13 of the Draft Australia-Denmark Agreement;
- Article10 of the Australia-France Agreement;
- Article 9 of the Australia-Germany Agreement;
- Article 9 of the Australia-Greece Agreement;
- Article 14 of the Draft Australia-Greece Agreement;
- Article 9 of the Australia-Ireland Agreement;
- Article 9 of the Australia-Italy Agreement;
- Article 11 of the Draft Australia-Luxembourg Agreement;
- Article 14 of the Australia-Malta Agreement;
- Section IV of the Annex to the Australia-Netherlands Agreement;
- Article 10 of the Australia-Poland Agreement;
- Article 13 of the Draft Australia-Sweden Agreement;
- Article 7 of the Draft Revised Australia-United Kingdom Agreement.
ANNEX 3
List of other states referred to in Article 2 of this Agreement
(a) The Republic of Iceland (under the Agreement on the European Economic Area) ;
(b) The Principality of Liechtenstein (under the Agreement on the European Economic Area) ;
(c ) The Kingdom of Norway (under the Agreement on the European Economic Area) ;
(d ) The Swiss Confederation (under the Agreement between the European Community and the Swiss Confederation on Air Transport).
[1] Council decision 11323/03 of 5 June 2003 (restricted document)
[2] OJ C […] […], p. […]
[3] OJ C […] […], p. […]
[4] OJ C […] […], p. […]
[5] OJ C […] […], p. […]
