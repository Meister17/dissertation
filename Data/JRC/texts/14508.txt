Action brought on 31 July 2006 — NewSoft Technology v OHIM — Soft (Presto! BizCard Reader)
Parties
Applicant: NewSoft Technology Corporation (Taipei, Taiwan) (represented by: M. Dirksen-Schwanenland, lawyer)
Defendant: Office for Harmonisation in the Internal Market (Trade Marks and Designs)
Other party/parties to the proceedings before the Board of Appeal of OHIM: Soft, S.A.
Form of order sought
- Annulment of the decision of the Second Board of Appeal of the Office for Harmonisation in the Internal Market of 19 May in Case R 601/2005, notified on 30 May 2006;
- Rejection of the application for a declaration of invalidity of the registration of European Community trade mark No 002625457 "Presto! BizCard Reader" made by Soft, S.A., Spain.
Pleas in law and main arguments
Registered Community trade mark in respect of which a declaration of invalidity has been sought: "Presto! BizCard Reader" for goods and services in Classes 9, 16 and 42 (Community trade mark No 2625457).
Proprietor of the Community trade mark: The applicant.
Applicant for the declaration of invalidity: SOFT, S.A.
Trade mark right of applicant for the declaration: Spanish figurative marks "Presto" for goods and services in Classes 9 and 42.
Decision of the Cancellation Division: Declaration of invalidity of the Community trade mark.
Decision of the Board of Appeal: Dismissal of the appeal.
Pleas in law: Breach of Article 8(1)(b) of Regulation (EC) No 40/94 [1], since it is not established that there is a likelihood of confusion between the conflicting trade marks.
[1] Council Regulation (EC) No 40/94 of 20 December 1993 on the Community trade mark (OJ 1994 L 11, p. 1).
--------------------------------------------------
