Summary information communicated by Member States regarding State aid granted under Commission Regulation (EC) No 1/2004 of 23 December 2003 on the application of Articles 87 and 88 of the EC Treaty to State aid to small and medium-sized enterprises active in the production, processing and marketing of agricultural products
(Text with EEA relevance)
(2006/C 296/02)
XA Number : XA 92/06
Member State : United Kingdom
Region : Lancashire (including the unitary authority areas of Blackpool and Blackburn).
Title of Aid scheme or name of company receiving an individual aid : Lancashire Rural Recovery Grant Fund
Legal basis : Section 5 of the Regional Development Agencies Act 1998
The total level of public sector funding made available through the scheme will be GBP 1034061. This will be split over 2 financial years:
1.4.2006 — 31.3.2007: GBP 743108
1.4.2007 — 31.3.2008: GBP 290953
The gross aid intensity shall not exceed the levels set out below:
Article 4. Investment in Agricultural Holdings. Up to 50 % of eligible costs in Less Favoured Areas and 40 % in other areas
Article 7. Processing and Marketing of Agricultural Products. Up to 40 % of eligible costs.
Article 10. Aid for Producer Groups. The total amount of public support granted under this article will not exceed EUR 100000 per beneficiary over any three-year period. Aid may be granted at a rate of up to 100 % of eligible costs incurred in the first year and will be reduced by 20 percentage points for each subsequent year of operation, so that in the second year the amount of aid will be limited to 80 % of eligible expenditure.
Article 14. The Provision of Technical Support. Aid may be granted at a rate of up to 100 % of eligible costs under this article. The maximum amount of aid granted to an individual beneficiary will however not be permitted to exceed EUR 100000 over any three-year period.
Date of implementation : The scheme will open on 19 October 2006.
Duration of scheme or individual aid award : The scheme will remain open until 31st March 2008. However alterations may need to be made to the scheme rules before this time to reflect any changes to European State aid rules.
The scheme will aid the long term development of Lancashire's rural economy through the provision of financial aid to Small and Medium sized Enterprises as defined in Annex 1 of Commission Regulation 70/2001 (as amended). In order to be eligible for support from the RRGF, a businesses must either be situated in a ward which is categorised as being either rural or town and fringe in the Office of National Statistics Rural and Urban Area Classification 2004. or alternatively must be an "Urban Business with Rural Interests" that is to say be a business situated in a ward categorized as urban, which undertakes activity that impacts in rural areas. This scheme seeks to help rural businesses not eligible for existing grant schemes.
The following types of aid will be available.
Investment in Agricultural Holdings The eligible costs will be (a) the construction acquisition or improvement of immovable property; (b) the purchase or lease purchase of new machinery and equipment; and (c) expenditure on professional fees, feasibility studies or the acquisition of patents and licences where this expenditure totals 12 % or less than the expenditure incurred under points a and/or b above. This complies with Article 4 of Regulation 1/2004.
Processing and Marketing of Agricultural Products The eligible costs will be (a) the construction acquisition or improvement of immovable property; (b) purchase or lease purchase of new machinery and equipment; and (c) professional fees, feasibility studies or the acquisition of patents and licences where this expenditure totals 12 % or less than the expenditure incurred under points a and/or b above. This complies with Article 7 of Regulation 1/2004.
Aid for Producer Groups The eligible costs will be (a) the rental of suitable premises; (b) the acquisition of office equipment, including computer hardware and software; (c) administrative staff costs; (d) overheads; and (e) legal and administrative fees. This complies with Article 10 of Regulation 1/2004.
The Provision of Technical Support Expenses eligible for support under this measure are;
(a)
concerning education and training of farmers and farm workers:
(i)
costs of organising the training programme;(ii)
travel and subsistence expenses of participants;(iii)
cost of the provision of replacement services during the absence of the farmer or the farm worker;(b)
concerning farm replacement services, the actual costs of the replacement of a farmer, the farmer's partner, or a farm worker, during illness and holidays;(c)
concerning consultancy services, the fees for services which do not constitute a continuous or periodic activity nor relate to the enterprise's usual operating expenditure, such as routine tax consultancy services, regular legal services, or advertising;(d)
concerning the organisation of and participation in competitions, exhibitions and fairs:
(i)
participation fees;(ii)
travel costs;(iii)
costs of publications;(iv)
the rent of exhibition premises.
This complies with Article 14 of Regulation 1/2004.
Sectors concerned :
This scheme will be available to all SME's from within the target area regardless of the sector in which they operate except where special rules regulations and directives are in place concerning the provision of aid to sectors such as, fisheries and aquaculture. Aid will be provided under this exemption to businesses producing, processing or marketing any agricultural product.
Support to non agricultural businesses from the scheme will be delivered either under the de minimis rules as established in Commission Regulation (EC) No 69/2001 or alternatively, if more appropriate, under Commission Regulation (EC) No 70/2001 of 12 January 2001 on the application of Articles 87 and 88 of the EC Treaty to State aid to small and medium-sized enterprises. The RRGF has been notified under Regulation 70/2001 and has been registered by the Competition Directorate-General under scheme reference number XS 56/06.
Name and address of the granting authority :
The Rural Recovery Grant Fund is administered by Lancashire County Developments Ltd, the Economic Development Department of Lancashire County Council. The postal address for which is
Lancashire County Developments Ltd
Robert House
Starkie Street
PR1 3LU Preston
United Kingdom
Web-Address :
Further information of the Rural Recovery Grant Programme can be obtained from
http://www.lcdl.co.uk/grant_programmes/RRGF%20Scheme%20Criteria%20(Agricultural%20Businesses).pdf.
Alternatively, Alternatively, you can visit the UK's central website for exempted agricultural State aids at www.defra.gov.uk/farm/policy/state-aid/setup/exist-exempt.htm. Click on the link "Lancashire Rural Recovery Grant Fund".
Other information :
The choice of the provider of technical support services funded through the scheme will be left to the beneficiary of the service.
Signed and dated on behalf of the Department of Environment, Food and Rural Affairs (UK competent authority)
Neil Marr
Agricultural State Aid Advisor
Defra
8B 9 Millbank
c/o 17 Smith Square
SW1P 3JR London
United Kingdom
XA Number : XA 87/06
Member State : United Kingdom
Somerset. The aid will be available in the administrative County of Somerset,
West Somerset District Council
Taunton Deane Borough Council
Sedgemoor District Council
Mendip District Council
South Somerset District Council
Title of Aid scheme or name of company receiving an individual aid : Somerset Rural Business Support Service
Legal basis : Sections 4 and 5 of The Regional Development Agencies Act 1998
1 October 2006 — 30 September 2007: GBP 167000
1 October 2007 — 30 September 2008: GBP 175000
Maximum aid intensity : 100 %
Date of implementation : 1 October 2006
Duration of scheme or individual aid award : The scheme will open to applications on 1 October 2006 and will run until 30th September 2008. However alterations may need to be made to the scheme rules before this time to reflect any changes to European State Aid rules.
Objective of aid :
Sectoral Development
The objective of the scheme is to provide consultancy services across the County of Somerset (as defined above) and signposting to additional rural support offered by other organizations.
The aid will be paid in line with Article 14 of Regulation 1/2004. The eligible costs will be consultancy services.
Sector(s) concerned : The scheme will apply to businesses involved in the agricultural and land based sectors and all its subsectors. This will include those involved in production, processing and marketing. Where appropriate the scheme may elect to signpost to alternative business advice/service providers.
Name and address of the granting authority :
South Somerset District Council
Brympton Way
Yeovil
BA20 2HT Somerset
United Kingdom
The organisation operating the scheme is
Somerset Rural Business Support Scheme
Address as above
Main Contact:
David Julian
Principal Economic Development Officer
South Somerset District Council
Web Address :
http://www.southsomerset.gov.uk/index.jsp?articleid=1929
Click on Somerset Rural Business Support Service — Somerset in the left hand menu. Alternatively, you can visit the UK's central page for exempted agricultural State aids at
http://www.defra.gov.uk/farm/state-aid/setup/exist-exempt.htm
Click on the Somerset Rural Business Support Scheme
Other information :
The scheme will be made available to all land managers not just farmers. Aid to non-agricultural businesses will be paid with regard to Commission Regulation 69/2001 on de minimis aid.
Beneficiaries will not be able to choose their service provider. The service provider will be Somerset Rural Business Support Service who were selected by a tendering exercise on the basis of market principles in line with Article 14(5) of Regulation 1/2004.
Signed and dated on behalf of the Department for Environment, Food and Rural Affairs (UK competent authority)
Neil Marr
Agricultural State Aid Advisor
Department for Environment, Food and Rural Affairs
Area 8e
9, Millbank
c/o Nobel House
17, Smith Square
SW1P 3JR London
United Kingdom
Aid No : XA 88/06
Member State : Spain
Region : Andalusia
Title of aid scheme or name of company receiving an individual aid : Aid for processing and marketing of agri-food products
Sección 8a del Decreto 280/2001, por el que se establecen las ayudas de la Junta de Andalucía a los sectores agrícola, ganadero y forestal incluidas en el Programa Operativo-Integrado Regional de Andalucía para el Desarrollo del Marco Comunitario de Apoyo 2000-2006, publicado en el Boletín Oficial de la Junta de Andalucía el 29 de diciembre de 2001.
La Orden de 10 de julio de 2002 por la que se desarrolla parcialmente la sección 8, sobre ayudas para la transformación y comercialización de los productos agroalimentarios, del Decreto 280/2001, publicada en el Boletín Oficial de la Junta de Andalucía el 27 de julio de 2002.
La Resolución de 29 de diciembre de 2003 de la Dirección General de Industrias y Promoción Agroalimentarias, por la que se convocan para el año 2003 y 2004 las ayudas previstas en la orden de 10 de julio de 2002.
Due to the large number of applications meeting the eligibility requirements and to inadequate European funds to provide all the aid, it is deemed advisable to allocate some of the aid from the self-financed budget. Self-financed aid is not covered by the exemption in Articles 51 and 52 of Regulation (EC) No 1257/1999, as indicated in Article 4 of the Decision of 29 December 2000, adopting Andalusia's IOP.
Some of the aid granted to SMEs for eligible projects under Article 40(3)(a), (b) and (c) of Decree 280/2001 as part of the aforementioned call for proposals may be admitted under Commission Regulation (EC) No 1/2004 of 23 December 2003 on the application of Articles 87 and 88 of the EC Treaty to State aid to small and medium-sized enterprises active in the production, processing and marketing of agricultural products (OJ L 1, 3.01.2004).
La Resolución de concesión de las ayudas, de la Dirección General de Industrias y Promoción Agroalimentarias, por la que se concede al solicitante la ayuda prevista en la orden de 10 de julio de 2002. The applicant will be informed by means of a specific reference in the decision to grant aid that his application has been admitted under the above Regulation. A model of the decision, including such a reference, is attached.
Once the deadline for issuing decisions has lapsed, the list of aid granted will be published in the Official Journal, specifying which aid has been admitted under Regulation (EC) No 1/2004.
Annual expenditure planned under the scheme or overall amount of individual aid granted to the company : EUR 45000000 planned to be allocated from 1 September 2006 in the decision relating to the call for proposals for 2004 under the system of aid for processing and marketing agri-food products included in Regulation (EC) No 1/2004.
Maximum aid intensity : 29 %
Date of implementation : From 1 September 2006
Duration of aid scheme or individual aid award : The deadline for granting aid is 31 December 2006
Objective of aid : Investment in processing and marketing, in accordance with Article 7 of Regulation (EC) No 1/2004
construction, acquisition and improvement of immovable property;
purchase of new machinery and equipment up to the market value of the asset;
general costs, such as architects', engineers' and consultation fees, feasibility studies, the acquisition of patents and licences, up to 12 % of the investment expenditure.
Sector(s) concerned : Processing sector
Name and address of the granting authority :
Dirección General de Industrias y Promoción Agroalimentarias,
Consejería de Agricultura y Pesca de la Junta de Andalucía.
calle Tabladilla s/n
E-Sevilla
Web address : http:// www.cap.junta-andalucia.es/agriculturaypesca/portal/opencms/portal/portada.jsp
Aid No : XA 89/06
Member State : Italy
Region : Friuli-Venezia Giulia
Title of aid scheme or name of company receiving individual aid : "Sabatini" Law No 1329/65 — Incentives for the purchase or leasing of machine-tools or production equipment. Agricultural sector.
Legal basis :
Legge 28 novembre 1965 n. 1329.
Regolamento concernente criteri e modalità per la concessione delle agevolazioni di cui alla legge 1329/1965 emanato con decreto del Presidente della Regione 9 agosto 2006, n. 0244/Pres.
Annual expenditure planned under the scheme or overall amount of individual aid granted to the company : Total budget for all the production sectors receiving incentives according to Law No 1329/65: EUR 11876528,04
Maximum aid intensity :
For enterprises active in less favoured areas (Articles 18 to 20 of Regulation (EC) No 1257/99), the gross aid intensity may not exceed 50 % of eligible costs.
For enterprises active in non-less-favoured areas, the gross aid intensity may not exceed 40 % of eligible costs.
Date of implementation : The aid scheme comes into force on the 30th day following its publication in the Official Journal of the Region and 10 working days after this form is sent, as provided for in Article 19(1) of Regulation (EC) No 1/2004.
Duration of scheme or individual aid award : Until 31 December 2006.
The objective of the aid scheme is to help the purchase or leasing by SMEs of only brand new machine-tools or production equipment for an overall cost exceeding EUR 1000. Assembly, testing, transport and packaging costs are eligible within the maximum limit of 15 % of the cost of the machine or equipment.
Eligible investment must pursue one or more of the following objectives:
reduction of production costs;
improvement and redeployment of production;
improvement in quality;
preservation and improvement of the natural environment, hygiene conditions and animal welfare standards;
promotion of the diversification of farm activities.
This aid scheme falls under aid provided by Article 4 of Regulation (EC) No 1/2004 of 23 December 2003, published in OJ L 1/1, 3 January 2004.
Sector or sectors concerned : SMEs active in the production, processing and/or marketing of agricultural products.
Name and address of granting authority :
Regione autonoma Friuli Venezia Giulia
Direzione centrale attività produttive
Servizio sostegno e promozione comparto produttivo industriale
Via Trento, 2
I-34123-Trieste
Website : www.regione.fvg.it
Other information :
Il Direttore del Servizio sostegno e promozione del comparto produttivo industriale
Dott. Massimo Zanini
--------------------------------------------------
