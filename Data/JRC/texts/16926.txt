Commission Regulation (EC) No 2000/2004
of 19 November 2004
authorising transfers between the quantitative limits of textiles and clothing products originating in the Republic of Korea
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 3030/93 of 12 October 1993 on common rules for imports of certain textile products from third countries [1], and in particular Article 7 thereof,
Whereas:
(1) The Agreement between the European Economic Community and the Republic of Korea on trade in textile products, initialled on 7 August 1986 and approved by Council Decision 87/471/EEC [2], as last amended by an Agreement in the form of an Exchange of Letters, initialled on 22 December 1994 and approved by Council Decision 95/131/EC [3], provides that transfers may be made between quota years.
(2) The Republic of Korea submitted a request for transfers between quota years on 6 September 2004.
(3) The transfers requested by the Republic of Korea fall within the limits of the flexibility provisions referred to in Article 7 of Regulation (EEC) No 3030/93 and set out in column 9 of Annex VIII thereto.
(4) It is appropriate to grant the request.
(5) It is desirable for this Regulation to enter into force on the day after its publication in order to allow operators to benefit from it as soon as possible.
(6) The measures provided for in this Regulation are in accordance with the opinion of the Textile Committee set up by Article 17 of Regulation (EEC) No 3030/93,
HAS ADOPTED THIS REGULATION:
Article 1
Transfers between the quantitative limits for textile goods originating in the Republic of Korea are authorised for the quota year 2004 in accordance with the Annex.
Article 2
This Regulation shall enter into force on the day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 19 November 2004.
For the Commission
Pascal Lamy
Member of the Commission
--------------------------------------------------
[1] OJ L 275, 8.11.1993, p. 1. Regulation as last amended by Regulation (EC) No 1627/2004 (OJ L 295, 18.9.2004, p. 1).
[2] OJ L 263, 19.9.1987, p. 37.
[3] OJ L 94, 26.4.1995, p. 1.
--------------------------------------------------
+++++ ANNEX 1 +++++
