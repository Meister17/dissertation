COMMISSION REGULATION (EC) No 1637/94 of 5 July 1994 concerning the classification of certain goods in the combined nomenclature
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2658/87 on the tariff and statistical nomenclature and on the Common Customs Tariff (1), as last amended by Commission Regulation (EC) No 882/94 (2), and in particular Article 9,
Whereas in order to ensure uniform application of the combined nomenclature annexed to the said Regulation, it is necessary to adopt measures concerning the classification of the goods referred to in the Annex to this Regulation;
Whereas Regulation (EEC) No 2658/87 has set down the general rules for the interpretation of the combined nomenclature and those rules also apply to any other nomenclature which is wholly or partly based on it or which adds any additional subdivision to it and which is established by specific Community provisions, with a view to the application of tariff and other measures relating to trade in goods;
Whereas, pursuant to the said general rules, the goods described in column 1 of the table annexed to the present Regulation must be classified under the appropriate CN codes indicated in column 2, by virtue of the reasons set out in column 3;
Whereas it is acceptance that binding tariff information issued by the customs authorities of Member States in respect of the classification of goods in the combined nomenclature and which do not conform to the rights established by this Regulation, can continue to be invoked, under the provisions in Article 12 (6) of Council Regulation (EEC) No 2913/92 establishing the Community Customs Code (3), for a period of three months by the holder;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Tariff and Statistical Nomenclature Section of the Customs Code Committee,
HAS ADOPTED THIS REGULATION:
Article 1
The goods described in column 1 of the annexed table are now classified within the combined nomenclature under the appropriate CN codes indicated in column 2 of the said table.
Article 2
Binding tariff information issued by the customs authorities of Member States which do not conform to the rights established by this Regulation can continue to be invoked under the provisions of Article 12 (6) of Regulation (EEC) No 2913/92 for a period of three months.
Article 3
This Regulation shall enter into force on the 21st day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 5 July 1994.
For the Commission
Christiane SCRIVENER
Member of the Commission
(1) OJ No L 256, 7. 9. 1987, p. 1.
(2) OJ No L 103, 22. 4. 1994, p. 5.
(3) OJ No L 302, 19. 10. 1992, p. 1.
ANNEX
"" ID="1">Preparation in the form af a syrupy liquid, tasting and smelling of oranges, composed of about 58 % by weight of sugar and having an essential oil content of 0,25 ml/100 gm of product.> ID="2">2106 90 99> ID="3">Classification is determined by the provisions of general rules 1 and 6 for the interpretation of the Combined Nomenclature and by the wording of CN codes 2106, 2106 90 and 2106 90 99."> ID="1">When diluted with water the product may be consumed as a beverage.> ID="3">Due, in particular, to its high essential oil content this product has lost the character of a natural fruit juice of heading 2009."> ID="3">See also the HS Explanatory Notes to heading 2009, seventh subparagraph, item 4 and heading 2106, third subparagraph, item 12.">
