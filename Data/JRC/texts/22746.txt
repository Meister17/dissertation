COMMISSION REGULATION (EEC) No 2429/81 of 21 August 1981 amending Regulations (EEC) No 2191/81 and (EEC) No 2192/81 on the granting of aid for the purchase of butter
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 804/68 of 27 June 1968 on the common organization of the market in milk and milk products (1), as last amended by the Act of Accession of Greece, and in particular Article 12 (3) thereof,
Whereas verification of the published texts of Commission Regulations (EEC) No 2191/81 (2) and (EEC) No 2192/81 (3) shows that they are not in all respects identical to the texts voted by the Management Committee and the errors in question must accordingly be corrected;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Milk and Milk Products,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EEC) No 2191/81 is hereby amended as follows: 1. Article 7 is hereby replaced by the following:
"Article 7
The application of Regulation (EEC) No 1717/72 is hereby suspended, save in respect of butter sold upon submission of certificates referred to in Article 4 of Regulation (EEC) No 1717/72, issued prior to the entry into force of this Regulation."
2. In the French version, in Article 3 (7) (a), the expression "au moyen de ce bon ;" is replaced by the expression "au moyen de ce bon, ou".
Article 2
Regulation (EEC) No 2192/81 is hereby amended as follows: 1. In the English version: (a) in Article 1 (2) (a), the expression "purchased in the manufacturing Member State" is replaced by the expression "purchased in the Member State";
(b) in the first indent of Article 1 (2) (b), the expression "the packaging of which is marked accordingly, or" is replaced by the expression "the packaging is marked accordingly, pursuant to the rules in the Member State where the beneficiary unit makes the purchase, or".
2. In the French version, in Article 3 (5) (a), the expression "au moyen de ce bon ;" is replaced by the expression "au moyen de ce bon, ou".
3. In the Italian version, in Article 3 (5) (a), at the end, the word "nonché" is replaced by the word "o".
4. In the Dutch version, in Article 3 (5) (a), at the end, the expression "van deze bon blijken ;" is replaced by the expression "van deze bon blijken, of".
Article 3
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Communitites.
It shall apply with effect from 4 August 1981.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 21 August 1981.
For the Commission
Poul DALSAGER
Member of the Commission
(1) OJ No L 148, 28.6.1968, p. 13. (2) OJ No L 213, 1.8.1981, p. 20. (3) OJ No L 213, 1.8.1981, p. 24.
