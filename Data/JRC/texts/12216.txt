Euro exchange rates [1]
25 April 2006
(2006/C 98/01)
| Currency | Exchange rate |
USD | US dollar | 1,2425 |
JPY | Japanese yen | 142,11 |
DKK | Danish krone | 7,4610 |
GBP | Pound sterling | 0,69440 |
SEK | Swedish krona | 9,3326 |
CHF | Swiss franc | 1,5733 |
ISK | Iceland króna | 92,00 |
NOK | Norwegian krone | 7,8475 |
BGN | Bulgarian lev | 1,9558 |
CYP | Cyprus pound | 0,5761 |
CZK | Czech koruna | 28,395 |
EEK | Estonian kroon | 15,6466 |
HUF | Hungarian forint | 263,45 |
LTL | Lithuanian litas | 3,4528 |
LVL | Latvian lats | 0,6961 |
MTL | Maltese lira | 0,4293 |
PLN | Polish zloty | 3,8699 |
RON | Romanian leu | 3,4570 |
SIT | Slovenian tolar | 239,58 |
SKK | Slovak koruna | 37,100 |
TRY | Turkish lira | 1,6437 |
AUD | Australian dollar | 1,6636 |
CAD | Canadian dollar | 1,4130 |
HKD | Hong Kong dollar | 9,6338 |
NZD | New Zealand dollar | 1,9728 |
SGD | Singapore dollar | 1,9722 |
KRW | South Korean won | 1174,29 |
ZAR | South African rand | 7,5066 |
CNY | Chinese yuan renminbi | 9,9599 |
HRK | Croatian kuna | 7,2995 |
IDR | Indonesian rupiah | 10950,15 |
MYR | Malaysian ringgit | 4,525 |
PHP | Philippine peso | 64,480 |
RUB | Russian rouble | 34,0250 |
THB | Thai baht | 46,745 |
[1] Source: reference exchange rate published by the ECB.
--------------------------------------------------
