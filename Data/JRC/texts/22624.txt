*****
COUNCIL DECISION
of 19 July 1982
on the conclusion of a Cooperation Agreement between the European Economic Community and the Kingdom of Thailand on manioc production, marketing and trade
(82/495/EEC)
THE COUNCIL OF THE EUROPEAN
COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 113 thereof,
Having regard to the recommendation from the Commission,
Whereas the conclusion of a Cooperation Agreement between the European Economic Community and the Kingdom of Thailand on manioc production, marketing and trade is in their mutual interest,
HAS DECIDED AS FOLLOWS:
Article 1
The Cooperation Agreement between the European Economic Community and the Kingdom of Thailand on manioc production, marketing and trade is hereby approved on behalf of the Community.
The text of the Agreement is attached to this Decision.
Article 2
The President of the Council is hereby authorized to designate the person empowered to sign the Agreement in order to bind the Community.
Done at Brussels, 19 July 1982.
For the Council
The President
K. OLESEN
