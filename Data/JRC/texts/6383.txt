Commission Regulation (EC) No 203/2005
of 4 February 2005
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 5 February 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 4 February 2005.
For the Commission
J. M. Silva Rodríguez
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 1947/2002 (OJ L 299, 1.11.2002, p. 17).
--------------------------------------------------
ANNEX
to Commission Regulation of 4 February 2005 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code | Standard import value |
07020000 | 052 | 109,0 |
204 | 73,4 |
212 | 157,6 |
248 | 82,5 |
624 | 81,4 |
999 | 100,8 |
07070005 | 052 | 176,0 |
204 | 87,7 |
999 | 131,9 |
07091000 | 220 | 65,9 |
999 | 65,9 |
07099070 | 052 | 185,8 |
204 | 183,1 |
999 | 184,5 |
08051020 | 052 | 44,6 |
204 | 48,2 |
212 | 50,3 |
220 | 38,4 |
421 | 23,4 |
448 | 35,9 |
624 | 68,4 |
999 | 44,2 |
08052010 | 052 | 76,5 |
204 | 71,1 |
624 | 72,5 |
999 | 73,4 |
08052030, 08052050, 08052070, 08052090 | 052 | 61,7 |
204 | 85,3 |
400 | 77,7 |
464 | 131,4 |
624 | 70,7 |
662 | 36,0 |
999 | 77,1 |
08055010 | 052 | 54,5 |
999 | 54,5 |
08081080 | 052 | 104,3 |
400 | 118,0 |
404 | 65,2 |
720 | 47,9 |
999 | 83,9 |
08082050 | 388 | 94,0 |
400 | 93,0 |
528 | 59,8 |
720 | 41,5 |
999 | 72,1 |
--------------------------------------------------
