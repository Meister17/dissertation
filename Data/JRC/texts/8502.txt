COUNCIL DIRECTIVE 1999/22/EC
of 29 March 1999
relating to the keeping of wild animals in zoos
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 130s(1) thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the Economic and Social Committee(1),
Acting in accordance with the procedure laid down in Article 189c of the Treaty(2),
Whereas Council Regulation (EEC) No 338/97 of 9 December 1996 on the protection of species of wild fauna and flora by regulating trade therein(3) requires evidence of the availability of adequate facilities for the accomodation and care of live specimens of a great many species before their importation into the Community is authorised; whereas that Regulation prohibits the display to the public for commercial purposes of specimens of species listed in Annex A thereof unless a specific exemption was granted for education, research or breeding purposes;
Whereas Council Directive 79/409/EEC of 2 April 1979 on the conservation of wild birds(4), and Council Directive 92/43/EEC of 21 May 1992 on the conservation of natural habitats and of wild fauna and flora(5), prohibit the capture and keeping of and trade in a great number of species, whilst providing for exemptions for specific reasons, such as research and education, repopulation, reintroduction and breeding;
Whereas the proper implementation of existing and future Community legislation on the conservation of wild fauna and the need to ensure that zoos adequately fulfil their important role in the conservation of species, public education, and/or scientific research make it necessary to provide a common basis for Member States' legislation with regard to the licensing and inspection of zoos, the keeping of animals in zoos, the training of staff and the education of the visiting public;
Whereas action at the Community level is required in order to have zoos throughout the Community contributing to the conservation of biodiversity in accordance with the Community's obligation to adopt measures for ex situ conservation under Article 9 of the Convention on Biological Diversity;
Whereas a number of organisations such as the European Association of Zoos and Aquaria have produced guidelines for the care and accomodation of animals in zoos which could, where appropriate, assist in the development and adoption of national standards,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Aim
The objectives of this Directive are to protect wild fauna and to conserve biodiversity by providing for the adoption of measures by Member States for the licensing and inspection of zoos in the Community, thereby strengthening the role of zoos in the conservation of biodiversity.
Article 2
Definition
For the purpose of this Directive, "zoos" means all permanent establishments where animals of wild species are kept for exhibition to the public for 7 or more days a year, with the exception of circuses, pet shops and establishments which Member States exempt from the requirements of this Directive on the grounds that they do not exhibit a significant number of animals or species to the public and that the exemption will not jeopardise the objectives of this Directive.
Article 3
Requirements applicable to zoos
Member States shall take measures under Articles 4, 5, 6 and 7 to ensure all zoos implement the following conservation measures:
- participating in research from which conservation benefits accrue to the species, and/or training in relevant conservation skills, and/or the exchange of information relating to species conservation and/or, where appropriate, captive breeding, repopulation or reintroduction of species into the wild,
- promoting public education and awareness in relation to the conservation of biodiversity, particularly by providing information about the species exhibited and their natural habitats,
- accommodating their animals under conditions which aim to satisfy the biological and conservation requirements of the individual species, inter alia, by providing species specific enrichment of the enclosures; and maintaining a high standard of animal husbandry with a developed programme of preventive and curative veterinary care and nutrition,
- preventing the escape of animals in order to avoid possible ecological threats to indigenous species and preventing intrusion of outside pests and vermin,
- keeping of up-to-date records of the zoo's collection appropriate to the species recorded.
Article 4
Licensing and inspection
1. Member States shall adopt measures for licensing and inspection of existing and new zoos in order to ensure that the requirements of Article 3 are met.
2. Every zoo shall have a licence within four years after the entry into force of this Directive or, in the case of new zoos, before they are open to the public.
3. Each licence shall contain conditions to enforce the requirements of Article 3. Compliance with the conditions shall be monitored inter alia by means of regular inspection and appropriate steps shall be taken to ensure such compliance.
4. Before granting, refusing, extending the period of, or significantly amending a licence, an inspection by Member States' competent authorities shall be carried out in order to determine whether or not the licensing conditions or proposed licensing conditions are met.
5. If the zoo is not licensed in accordance with this Directive or the licensing conditions are not met, the zoo or part thereof:
(a) shall be closed to the public by the competent authority; and/or
(b) shall comply with appropriate requirements imposed by the competent authority to ensure that the licensing conditions are met.
Should these requirements not be complied with within an appropriate period to be determined by the competent authorities but not exceeding two years, the competent authority shall withdraw or modify the licence and close the zoo or part thereof.
Article 5
Licensing requirements set out in Article 4 shall not apply where a Member State can demonstrate to the satisfaction of the Commission that the objective of this Directive as set out in Article 1 and the requirements applicable to zoos set out in Article 3 are being met and continously maintained by means of a system or regulation and registration. Such a system should, inter alia, contain provisions regarding inspection and closure of zoos equivalent to those in Article 4(4) and (5).
Article 6
Closure of zoos
In the event of a zoo or part thereof being closed, the competent authority shall ensure that the animals concerned are treated or disposed of under conditions which the Member State deems appropriate and consistent with the purposes and provisions of this Directive.
Article 7
Competent authorities
Member States shall designate competent authorities for the purposes of this Directive.
Article 8
Penalties
Member States shall determine the penalties applicable to breaches of the national provisions adopted pursuant to this Directive. The penalties shall be effective, proportionate and dissuasive.
Article 9
Implementation
1. Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this directive not later than 9 April 2002. They shall forthwith inform the Commission thereof.
When Member States adopt these measures, they shall contain a reference to this Directive or shall be accompanied by such reference on the occasion of their official publication. The methods of making such a reference shall be laid down by the Member States.
2. Member States shall communicate to the Commission the main provisions of national law which they adopt in the field covered by this Directive.
Article 10
Entry in force
This Directive shall enter into force on the day of its publication in the Official Journal of the European Communities.
Article 11
This Directive is addressed to the Member States.
Done at Brussels, 29 March 1999.
For the Council
The President
F. MÜNTEFERING
(1) OJ C 204, 15.7.1996, p. 63.
(2) Opinion of the European Parliament of 29 January 1998. (OJ C 56, 23.2.1998, p. 34), Council Common Position of 20 July 1998 (OJ C 364, 25.11.1998, p. 9), and Decision of the European Parliament of 10 February 1999 (not yet published in the Official Journal).
(3) OJ L 61, 3.3.1997, p. 1. Regulation as last amended by Commission Regulation (EC) No 2307/97 (OJ L 325, 27.11.1997, p. 1).
(4) OJ L 103, 25.4.1979, p. 1. Directive as last amended by Directive 97/49/EC (OJ L 223,. 13.8.1997, p. 9).
(5) OJ L 206, 22.7.1992, p. 7. Directive as last amended by Commission Directive 97/62/EC (OJ L 305, 8.11.1997, p. 42).
