Judgment of the Court of First Instance of 27 September 2006 — Akzo Nobel NV
v Commission
(Case T-330/01) [1]
Parties
Applicant: Akzo Nobel NV (Arnhem, Netherlands) (represented by: initially by M. van Empel and C. Swaak, and subsequently by C. Swaak, lawyers)
Defendant: Commission of the European Communities (represented by: A. Whelan, A. Bouquet and W. Wils, Agents, assisted by H.van der Woude, lawyer)
Re:
Annulment of Articles 3 and 4 of Decision C(2001) 2931 of 2 October 2001 concerning a procedure under Article 81 of the Treaty and Article 53 of the EEA Agreement (Case No COMP/E 1/36.756 — Sodium Gluconate), in so far as it pertains to the applicant or, in the alternative, reduction of the fine imposed on the applicant.
Operative part of the judgment
The Court:
1. Dismisses the action;
2. Orders Akzo Nobel NV to pay the costs.
[1] OJ C 68, 16.3.2002.
--------------------------------------------------
