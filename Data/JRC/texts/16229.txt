COMMISSION REGULATION (EC) No 2058/96 of 28 October 1996 opening and providing for the management of a tariff quota for broken rice of CN code 1006 40 00 for production of food preparations of CN code 1901 10
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1095/96 of 18 June 1996 on the implementation of the concessions set out in Schedule CXL drawn up in the wake of the conclusion of the GATT XXIV.6 negotiations (1), and in particular Article 1 thereof,
Whereas among the concessions granted is an annual quota of 1 000 tonnes at zero duty of broken rice of CN code 1006 40 00 for production of food preparations of CN code 1901 10;
Whereas to ensure that the quota is properly managed specific rules on submission of applications and issuing of licences are required; whereas these rules either supplement or derogate from the provisions of Commission Regulation (EEC) No 3719/88 (2), as last amended by Regulation (EC) No 2137/95 (3);
Whereas specific provisions are required to ensure that the broken rice imported is not deflected from the prescribed use; whereas duty exemption should therefore be made conditional on submission of an undertaking by the importer as to the use of the rice and lodging of a security equal to the uncharged duty; whereas proper management of the quota requires that a reasonable time be allowed for processing; whereas consignment of the goods requires a T5 control copy to be made out in the Member State of entry for free circulation in accordance with Commission Regulation (EEC) No 2454/93 laying down provisions for implementation of Council Regulation (EEC) No 2913/92 establishing the Community Customs Code (4), as last amended by Regulation (EC) No 1676/96 (5), as the appropriate means of giving proof of processing; whereas when processing takes place in the Member State of entry for free circulation proof of processing may be furnished by means of an equivalent national document;
Whereas, although the purpose of the security is to ensure payment of a newly arising import debt, there should be some flexibility in release of the security;
Whereas it should be stated that Commission Regulation (EC) No 1162/95 of 23 May 1995 laying down special detailed rules for the application of the system of import and export licences for cereals and rice (6), as last amended by Regulation (EC) No 1527/96 (7), applies to imports under this Regulation;
Whereas security against import licences of ECU 25 per tonne should suffice for proper management of the quota;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Cereals,
HAS ADOPTED THIS REGULATION:
Article 1
An annual zero duty tariff quota of 1 000 tonnes of broken rice of CN code 1006 40 for use in the production of food preparations of CN code 1901 10 is hereby opened in accordance with the provisions of this Regulation.
Article 2
1. Applications for import licences shall concern a quantity of at least 5 tonnes and not more than 500 tonnes of broken rice.
2. Applications for import licences shall be accompanied by:
- proof that the applicant is a natural or legal person who has carried out a commercial activity in the rice sector for at least 12 months and who is registered in the Member State in which the application is submitted,
- a written declaration by the applicant stating that he has submitted one application only. Where an applicant submits more than one application for an import licence, all his applications shall be rejected.
3. Box 7 of the licence application and the import licence shall indicate the country of origin and the word 'yes` shall be marked with a cross.
4. Licence applications and licences shall contain:
(a) in box 20, one of the following entries:
- Partidos de arroz, del código NC 1006 40 00, destinados a la producción de preparaciones alimenticias del código NC 1901 10
- Brudris, henhørende under KN-kode 1006 40 00, bestemt til fremstilling af tilberedte næringsmidler, henhørende under KN-kode 1901 10
- Bruchreis des KN-Codes 1006 40 00, bestimmt zur Herstellung von Lebensmittelzubereitungen des KN-Codes 1901 10
- Èñáýóìáôá ñõæéïý õðáãüìåíá óôïí êùäéêü ÓÏ 1006 40 00, ðïõ ðñïïñßæïíôáé ãéá ôçí ðáñáãùãÞ ðáñáóêåõáóìÜôùí äéáôñïöÞò ôïõ êùäéêïý ÓÏ 1901 10
- Broken rice of CN code 1006 40 00 for production of food preparations of CN code 1901 10
- Brisures de riz, relevant du code NC 1006 40 00, destinées à la production de préparations alimentaires du code NC 1901 10
- Rotture di riso, di cui al codice NC 1006 40 00, destinate alla produzione di preparazioni alimentari del codice NC 1901 10
- Breukrijst van GN-code 1006 40 00, voor de produktie van voor voeding bestemde bereidingen van GN-code 1901 10
- Trincas de arroz do código NC 1006 40 00, destinadas à produção de preparações alimentares do código NC 1901 10
- CN-koodiin 1006 40 00 kuuluvat rikkoutuneet riisinjyvät CN-koodiin 1901 10 kuuluvien elintarvikevalmisteiden valmistamiseksi
- Brutet ris som omfattas av KN-nummer 1006 40 00, avsett för produktion av livsmedelsberedningar som omfattas av KN-nummer 1901 10;
(b) in box 24, one of the following entries:
- Exención del derecho de aduana [Reglamento (CE) n° 2058/96]
- Toldfri (Forordning (EF) nr. 2058/96)
- Zollfrei (Verordnung (EG) Nr. 2058/96)
- ÁðáëëáãÞ äáóìïý [êáíïíéóìüò (ÅÊ) áñéè. 2058/96]
- Free of customs duty (Regulation (EC) No 2058/96)
- Exemption du droit de douane [Règlement (CE) n° 2058/96]
- Esenzione dal dazio doganale [Regolamento (CE) n. 2058/96]
- Vrijgesteld van douanerecht (Verordening (EG) nr. 2058/96)
- Isenção de direito aduaneiro [Regulamento (CE) nº 2058/96]
- Tullivapaa (asetuksen (EY) N:o 2058/96)
- Tullfri (Förordning (EG) nr 2058/96).
5. Notwithstanding Article 10 of Regulation (EC) No 1162/95, the security against import licences provided for in this Regulation shall be ECU 25 per tonne.
Article 3
1. On the day on which licence applications are lodged, the Member States shall inform the Commission's departments by telex or fax of the quantities by country of origin for which import licences have been applied for and the names and addresses of the applicants.
2. Import licences shall be issued on the 11th working day following that on which the application was lodged provided that the quantity specified in Article 1 is not reached.
3. On the day on which the quantities applied for exceed that specified in Article 1 the Commission shall set a single percentage reduction in the quantities requested and notify this to the Member States within 10 working days of the day on which applications were lodged.
4. If the reduction referred to in paragraph 3 results in one or more quantities of less than 20 tonnes per application, allocation of all these quantities among importers shall be carried out by the Member State by drawing lots for lots of 20 tonnes and, if applicable, a lot containing the remaining quantity.
5. If the quantity for which the import licence is issued is less than the quantity applied for, the amount of the security referred to in Article 2 (5) shall be reduced proportionately.
6. Notwithstanding Article 9 of Regulation (EEC) No 3719/88, the rights arising from import licences shall not be transferable.
Article 4
Member States shall notify to the Commission by telex or fax:
(a) within two working days following issue, the quantities for which licences have been issued, specifying date, country of origin and name and address of holder,
(b) if a licence is cancelled, within two working days following cancellation, the quantities for which licences have been cancelled and the names and addresses of the holders of the cancelled licences;
(c) on the last working day of the following month, the quantities by country of origin actually entered for free circulation during each month.
The above information must be notified in the same way but separately from information on other import licence applications in the rice sector.
Article 5
1. Exemption from customs duty shall be subject to:
(a) submission on entry for free circulation of a written undertaking by the importer that all the goods entered will be processed as indicated in box 20 of the licence within six months of the date of acceptance of the entry for free circulation;
(b) lodging by the importer, on entry for free circulation, of security for an amount equal to the customs duty for broken rice set in the combined nomenclature.
2. On entry for free circulation the importer shall indicate as the place of processing either the name of a processing undertaking and a Member State or not more than five different processing plants. Consignment of the rice shall require a T5 control copy to be made out in the Member State of departure which, in accordance with Regulation (EEC) No 2454/93, shall also constitute proof of processing.
However, where processing takes place in the Member State of entry into free circulation, proof of processing may be an equivalent national document.
3. The T5 control copy shall carry:
(a) in box 104, one of the following entries:
- Destinadas a la producción de preparaciones alimenticias del código NC 1901 10
- Bestemt til fremstilling af tilberedte næringsmidler, henhørende under KN-kode 1901 10
- Bestimmt zur Herstellung von Lebensmittelzubereitungen des KN-Codes 1901 10
- Ðñïïñßæïíôáé ãéá ôçí ðáñáãùãÞ ðáñáóêåõáóìÜôùí äéáôñïöÞò ôïõ êùäéêïý ÓÏ 1901 10
- For production of food preparations of CN code 1901 10
- Destinées à la production de préparations alimentaires du code NC 1901 10
- Destinate alla produzione di preparazioni alimentari del codice NC 1901 10
- Bestemd voor de produktie van voor voeding bestemde bereidingen van GN-code 1901 10
- Destinadas à produção de preparações alimentares do código NC 1901 10
- Tarkoitettu CN-koodiin 1901 10 kuuluvien elintarvikevalmisteiden valmistukseen
- Avsett för produktion av livsmedelsberedningar som omfattas av KN-nummer 1901 10;
(b) in box 107, one of the following entries:
- Reglamento (CE) n° 2058/96 - artículo 4
- Forordning (EF) nr. 2058/96 - artikel 4
- Verordnung (EG) Nr. 2058/96 - Artikel 4
- Êáíïíéóìüò (ÅÊ) áñéè. 2058/96 - Üñèñï 4
- Article 4 of Regulation (EC) No 2058/96
- Règlement (CE) n° 2058/96 - article 4
- Regolamento (CE) n. 2058/96 - articolo 4
- Verordening (EG) nr. 2058/96, artikel 4
- Regulamento (CE) nº 2058/96 - artigo 4º
- Asetuksen (EY) N:o 2058/96 - 4 artikla
- Förordning (EG) nr 2058/96 - artikel 4.
4. Except in cases of force majeure the security referred to in paragraph 1 (b) shall be released when the importer gives proof to the competent authority of the Member State of entry into free circulation that all the rice entered has been processed into the product indicated in the import licence. Processing is deemed to have taken place when the product has been manufactured either in one or more of the factories belonging to the firm referred to in Article 5 (2) situated in the Member State referred to therein, or in the factory or one of the factories referred to in the same provision, within the time limit indicated in paragraph 1 (a).
Where rice entered for free circulation has not been processed within the specified time limit the security released shall be reduced by 2 % for each day by which the time limit is exceeded.
5. Proof of processing shall be given to the competent authority within six months following the time limit for processing.
If proof is not given within the time limit laid down in this paragraph, the security referred to in paragraph 1 (b), where applicable minus the percentage provided for in the second subparagraph of paragraph 4, shall be reduced by 2 % for each day by which the time limit is exceeded.
The amount of the security which is not released shall be forfeit as customs duties.
Article 6
1. Notwithstanding Article 8 (4) of Regulation (EEC) No 3719/88, the quantity entered for free circulation may not exceed that entered in boxes 17 and 18 of the import licence. The figure '0` shall accordingly be entered in box 19 of the licence.
2. Article 33 (5) of Regulation (EEC) No 3719/88 shall apply.
Article 7
This Regulation shall enter into force on the seventh day following that of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 28 October 1996.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ No L 146, 20. 6. 1996, p. 1.
(2) OJ No L 331, 2. 12. 1988, p. 1.
(3) OJ No L 214, 8. 9. 1995, p. 21.
(4) OJ No L 253, 11. 10. 1993, p. 1.
(5) OJ No L 218, 28. 8. 1996, p. 1.
(6) OJ No L 117, 24. 5. 1995, p. 2.
(7) OJ No L 190, 31. 7. 1996, p. 23.
