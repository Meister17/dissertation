Opinion of the European Economic and Social Committee on the "Communication from the Commission to the Council, the European Parliament and the European Economic and Social Committee: Dividend taxation of individuals in the Internal Market"
(COM(2003) 810 final)
(2004/C 302/15)
On 19 December 2003, the EU Commission decided, in accordance with Article 262 of the Treaty establishing the European Community, to consult the European Economic and Social Committee on the abovementioned proposal.
The Section for Economic and Monetary Union and Economic and Social Cohesion, responsible for preparing the Committee's work on the subject, adopted its opinion on 8 June 2004. The rapporteur was Mr Retureau.
At its 410th plenary session, held on 30 June and 1 July 2004 (meeting of 30 June 2004), the European Economic and Social Committee adopted the following opinion by 151 votes to one, with 12 abstentions.
1. Introduction
1.1 The communication submitted for an opinion concerns primarily the taxation of dividends received by individuals for their portfolio investments.
1.2 It is part of the follow-up to the Communication accompanying the Company Taxation Study [1], which already proposed to develop guidelines for applying the main ECJ rulings in this area and, in the case of dividends received by individuals, refers to the Verkooijen ruling [2]. The inclusion of inbound and outbound dividends under the free movement of capital is something new; dividends are not expressly mentioned in the Treaty or in the directive.
1.3 Differences between the tax systems of the Member States with regard to the "double taxation of corporate profits distributed to individual shareholders in the form of dividends" [3] would constitute a major source of discrimination, and an obstacle to the free movement of capital within the single market.
1.4 The proposed guidelines concern the implications of EU law for Member States' dividend taxation systems and are aimed, in the light of the ECJ case law referred to above, at eliminating restrictions encountered by individuals when their income from shares held in a portfolio is taxed. The proposal also seeks to reduce excessively high withholding tax rates in the states of origin of dividends.
1.5 The aim is "to help Member States to ensure that their systems are compatible with the requirements of the internal market in accordance with the Treaty principles regarding the free movement of capital."
1.6 If Member States do not adopt the proposed method for removing obstacles to freedom of movement for equity investments, the Commission, as guardian of the Treaties, could then use Article 226 of the EC Treaty.
1.7 It should be pointed out that the ECJ can highlight elements relating to the interpretation of EU law, so as to enable that judge to resolve the legal problem referred to him [4].
2. Dividend taxation in the internal market
2.1 Taxation on companies' earnings consists of a tax on profits, the rate of which varies from 12.5 to 40 % depending on the country (approximately 30 % on average). Taxation of the dividends paid out of profits after corporation tax may be carried out at source and be deducted from the dividend distributed, but it may also be imposed as personal tax at the marginal rate or at a separate rate.
2.2 According to the Commission, the taxation of companies' earnings and dividends is "economic double taxation", and individuals also run the risk of "international juridical double taxation" (when two states tax dividends received abroad).
2.2.1 The OECD Model Tax Convention, which has been proposed in order to avoid international juridical double taxation, does not deal with economic double taxation.
2.2.2 On the basis of the OECD Model Tax Convention, tax already paid at source on dividends in the state of origin should be deducted from tax due in the state where the shareholder has his tax domicile, in the form of an ordinary charge limited to any tax that may be due on dividends in the state of tax domicile.
2.2.3 The OECD model, according to the Commission, applies to all the tax systems on dividends, whether in a pure or mixed form (classical, schedular, imputation and exemption).
3. The Verkooijen ruling and some other relevant judgements
3.1 In the ECJ judgment on the Verkooijen case, the person named was refused exemption from income tax for share dividends from a company established in a Member State other than the Netherlands.
3.2 That exemption applied to income from shares on which Netherlands dividend tax had been levied at source in the Netherlands, which excluded income from shares received in other countries.
3.2.1 Firstly, the exemption was intended to raise the level of undertakings' equity capital and to stimulate interest on the part of private individuals in Netherlands shares; secondly, in particular for small investors, the exemption was intended to compensate in some measure for the double taxation through an exemption of one thousand guilders.
3.2.1.1 For the purpose of taxing Mr Verkooijen's income, the tax inspector did not apply the dividend exemption on the ground that Mr Verkooijen was not entitled to it since the dividends received by him "had not been subject to the Netherlands dividend tax."
3.2.2 When requested by the national court with jurisdiction for a preliminary question, the Court considered that the receipt of foreign dividends was indissociable from a capital movement; thus, if the tax rules applicable to incoming dividends was different from and less advantageous than that applicable to domestic dividends, this was a prohibited restriction on the free movement of capital.
3.2.2.1 The Court stated that a legislative provision such as the one at issue … "has the effect of dissuading nationals of a Member State residing in the Netherlands from investing their capital in companies which have their seat in another Member State."
3.2.2.2 "Such a provision also has a restrictive effect as regards companies established in other Member States: it constitutes an obstacle to the raising of capital in the Netherlands."
3.3 In the Schmid case [5], the Advocate-General noted that dividends from foreign shares, which are not subject in Austria to the final withholding at source by way of tax on revenue from capital assets, are therefore subject in full to income tax there and cannot, moreover, benefit from the application of the 50 % tax rate. The Advocate-General concluded that the freedom of capital movements was violated.
4. General comments of the EESC
4.1 The Member States continue to have jurisdiction on tax matters. However, Articles 56 and 58 ECT currently in force limit this national jurisdiction, which must not violate a fundamental freedom or circumvent Community law: Article 56 makes it illegal to obstruct the free movement of capital, while Article 58 recognises that, while the provisions of national tax laws may "distinguish between taxpayers who are not in the same situation with regard to their place of residence or with regard to the place where their capital is invested" and while states may take " take all requisite measures to prevent infringements of national law and regulations, in particular in the field of taxation and the prudential supervision of financial institutions.... or … take measures which are justified on grounds of public policy or public security," such measures "shall not constitute a means of arbitrary discrimination or a disguised restriction on the free movement of capital and payments."
4.2 The case law of the Court, in essence, asks that persons liable for taxation be treated equally, and condemns international double taxation.
4.3 With EU enlargement, and even greater differences in rates of corporation tax and personal income tax on dividends, the EESC thinks it is urgent to encourage all the Member States who have not yet done so to conclude international agreements against double taxation on the basis, at least, of the model proposed by the OECD, so as to bring about equality of treatment at national level for dividends received by portfolio investors, irrespective of their origin within the Community.
4.4 The EESC notes that the Treaty also envisages the free movement of capital to or from non-EU countries, and that a number of international bilateral agreements also exist between some Member States and some non-EU countries.
4.5 Full neutrality could only ideally be achieved, combining all the conditions set out in the communication and sticking to the EU area, with a single Community rate of corporation tax in an exemption system, provided that the conditions for imposing personal income tax were equal in all the countries concerned, and if income from shares were considered to be the only income of a taxpayer investing in a portfolio. The Commission recognises itself, moreover, that full tax neutrality could only be achieved by the complete harmonisation of Member States' taxation.
4.6 The tax sovereignty of parliaments and states, deciding on personal and corporate taxation and on the national budget, lies historically at the heart of Europe's democracies. The equality of citizens before charges levied by the state is a basic principle of constitutional values. Member States, at the present stage of European integration, still have serious reasons for wishing to retain their national jurisdiction over tax matters, as provided for in the treaties. This situation could, of course, change in the future. But the EESC would not like to see the latitude that the Member States have at present giving rise to cases of "fiscal dumping."
4.7 The EESC considers that the guidelines proposed, if they are limited to the matters actually dealt with by the Court, should be included on these terms among the respective terms of reference of the Commission and the Member States. If this were decided, the European Parliament and the Community's consultative bodies should be fully involved in the monitoring of such a procedure.
4.8 Finally, the EESC wonders whether the threat of a referral to the Court of Justice is really likely to facilitate the search for a solution; nevertheless, the EESC feels that the Member States concerned must rapidly adopt provisions to prevent inbound or outbound dividends being subject to discrimination. This could also amount to trying to make the Court a substitute for Community tax legislation, beyond the jurisdiction adopted by the Member States, which would risk leading to a confusion of powers.
5. Specific comments
5.1 The EESC notes that the Commission's relatively simple analysis model covers only one shareholding scenario: that of an individual portfolio made up of shares of companies based in two or more Member States. A portfolio may contain shares from companies in several EU and non-EU countries.
5.2 The EESC would also point out that income from securities may also come from investment trusts or pension funds, in forms where it is impossible to know the national origin of the various components of the distributed dividends and capital gains. Moreover, the tax rules applying to capital gains from these forms of investment and to the distributed income are sometimes different from those governing dividends paid direct to individuals with their own share portfolio. The Commission does not deal with these issues.
5.3 The EESC notes that the communication does not deal either with the question of capital gains tax on sales of listed securities. Individuals do not invest in shares just so they can collect the dividends. Re-selling shares to take a profit is sometimes an even more fundamental reason for investing, and is part and parcel of managing a portfolio and one's income. This issue too should doubtless also be studied.
5.4 Regarding the debate on economic double taxation, the EESC considers it legitimate to distinguish between individuals and companies, irrespective of the methods and tax rates used. The part of earnings that is distributed to shareholders constitutes disposable income for the latter, but not all earnings are necessarily distributed. Some are ploughed back into the company, which adds value to its shares and shareholders' wealth; this part of earnings is only covered by corporation tax and not by personal income tax in the Commission's scheme of things. It should therefore also be made known if these capital gains are taxed or not when they are realised and under which conditions; the communication does not deal with this question, which the EESC considers to be an important one.
6. Conclusions
6.1 The EESC considers that the treatment of double taxation and the taxation at source of inbound and outbound domestic dividends with a view to ensuring their non-discriminatory treatment are major objectives, without jeopardising the basic principle of the equality of individuals before charges levied by the state at national level. The Member States could also consider cooperation between countries with similar tax practices, in order to study the best tax practices available.
6.2 The issues raised by the EESC in its specific comments could be examined later on, with a view to greater harmonisation of corporation tax and taxes on income and gains from securities, so as to improve the operation of the internal market.
6.3 Finally, the EESC feels that the Commission's communication opens the door to resolving problems that are often the subject of referrals to the Court of Justice and that should be avoided in the future so as to avoid overburdening it needlessly with appeals in this area.
Brussels, 30 June 2004.
The President
of the European Economic and Social Committee
Roger Briesch
[1] "Towards an internal market without tax obstacles" COM (2001) 582 final
[2] Case C-35/98 Verkooijen [2000] ECR I-4071
[3] Ruding report of March 1992 pp. 207-208
[4] Judgement of 28 January 1992, Bachmann, CR09204/90, Rec. p. I-249
[5] C-516/99 case, on 30 May 2002
--------------------------------------------------
APPENDIX
to the opinion of the European Economic and Social Committee
The following amendments, though rejected, were supported by at least one-quarter of the votes cast:
Point 4.6
Delete the last sentence in this point.
Voting
For: | 58 |
Against: | 84 |
Abstentions: | 9 |
Point 4.8
Delete this point.
Voting
For: | 53 |
Against: | 85 |
Abstentions: | 16 |
--------------------------------------------------
