Council Regulation (EC) No 1456/2001
of 16 July 2001
amending Regulation (EC) No 2549/2000 establishing additional technical measures for the recovery of the stock of cod in the Irish Sea (ICES Division VIIa)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 37 thereof,
Having regard to the proposal from the Commission(1),
Having regard to the opinion of the European Parliament(2),
Having regard to the opinion of the Economic and Social Committee(3),
Whereas:
(1) The conditions laid down in Regulation (EC) No 2549/2000(4) are intended to ensure that the selectivity of fishing gears deployed in the Irish sea are such that few young cod are captured.
(2) Article 2(1) and (2) stipulate that it is prohibited to use any demersal towed net other than beam trawls incorporating a cod/end and/or extension piece made entirely or partly of multiple twine netting materials and any demersal towed net other than beam trawls incorporating a cod-end and/or extension piece of which the thickness of the twine exceeds 6 mm.
(3) However, recent scientific advice concurs with the opinion of fishermen that a cod-end and/or extension piece constructed of double twine of thickness no more than 4 mm is technically equivalent to a cod-end and/or extension piece as currently defined.
(4) There is a requirement on the part of some fishermen to deploy double-twine cod-ends.
(5) Article 3 of the current regulation refers to conditions which were relevant only during 2000 and it should therefore be replaced with provisions which set out the required amendment.
(6) Regulation (EC) No 2549/2000 should accordingly be amended,
HAS ADOPTED THIS REGULATION:
Article 1
Article 3 of Regulation (EC) No 2549/2000 shall be replaced by the following: "Article 3
Notwithstanding the conditions laid down in Article 2(1) and (2), when fishing with towed gears in the Irish Sea, it shall be permitted to use a cod-end and/or extension piece constructed of double-twine netting material of which the thickness of any individual twine does not exceed 4 mm."
Article 2
This Regulation shall enter into force on the day following that of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 16 July 2001.
For the Council
The President
L. Michel
(1) OJ C 180 E, 26.6.2001, p. 311.
(2) Opinion delivered 5.7.2001 (not yet published in the Official Journal)
(3) Opinion delivered 30.5.2001 (not yet published in the Official Journal)
(4) OJ L 292, 21.11.2000, p. 5.
