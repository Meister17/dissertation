COMMISSION REGULATION (EC) No 3127/94 of 20 December 1994 amending Regulation (EC) No 2967/85 laying down detailed rules for the application of the Community scale for grading pig carcases
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 3220/84 of 13 November 1984 determining the Community scale for grading pig carcases (1), as last amended by Regulation (EC) No 3513/93 (2), and in particular Article 5 (1) thereof,
Whereas Regulation (EEC) No 3220/84 introduced the use of partial dissection of pig carcases as a means to obtain the total weight of the red striated muscles; whereas it is necessary to lay down in Commission Regulation (EEC) No 2967/85 (3), the details of the new assessment method in particular the number of carcases to be dissected and the new dissection method; whereas the use of partial dissection leads to a new specification of the maximum tolerance for statistical error in assessment;
Whereas it is appropriate to improve the procedure for authorization of new grading methods by introducing a two-step protocol and by enumerating the different elements which it should contain;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Pigmeat,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EEC) No 2967/85 is hereby amended as follows:
1. Article 3 is replaced by the following:
'Article 3
1. The method for assessing the lean meat content of carcases authorized as a grading method within the meaning of Article 2 (3) of Regulation (EEC) No 3220/84 is based on a representative sample of the national or regional pigmeat production concerned by the assessment method, consisting of at least 50 carcases whose lean meat content has been ascertained in accordance with the dissection method laid down in Annex I, combined with a nationally established quick method of carcase assessment using double regression or other statistically proven procedure, and such that the resulting precision is at least equal to that obtained using standard regression techniques on 120 carcases using the method in Annex I.
2. Authorization of the grading methods shall, moreover, be subjected to the root mean square deviation of the errors, measured about zero, being less than 2,5.
3. Member States shall inform the Commission, by way of a protocol, of the grading methods they wish to have authorized for application in their territory, describing the dissection trial and indicating the principles on which these methods are based and the equations used for assessing the percentage of lean meat. The protocol should have two parts and should include the elements provided for in Annex II. Part one of the protocol is presented to the Commission prior to the start of the dissection trial.
Application of grading methods in the territory of a Member State shall be authorized in accordance with the procedure provided for in Article 24 of Regulation (EEC) No 2759/75 on the basis of the protocol.
4. The application of grading methods must correspond in all particulars to the description given in the Community Decision authorizing them.`;
2. The Annex to the present Regulation is added as Annex I and II to Regulation (EEC) No 2967/85.
Article 2
This Regulation shall enter into force on 1 January 1995.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 20 December 1994.
For the Commission
René STEICHEN
Member of the Commission
(1) OJ No L 301, 20. 11. 1984, p. 1.
(2) OJ No L 320, 22. 12. 1993, p. 5.
(3) OJ No L 285, 25. 10. 1985, p. 39.
ANNEX
'ANNEX I
1. The prediction of the lean meat percentage is based on the dissection of the four major cuts. The dissection is executed in accordance with the reference method.
2. The reference lean meat percentage is calculated as follows:
y = 1,3 × 100 × >NUM>weight of tender loin + weight of lean (fascia included) in shoulder, loin, ham and belly
>DEN>weight of tender loin + weight of dissected cuts + weight of remaining cuts
The weight of the lean in those four cuts will be calculated by subtracting the total of the non-lean elements of the four cuts from the total weight of the cuts before dissection.
ANNEX II
1. Part one of the protocol should give a detailed description of the dissection trial and include in particular:
- the trial period and time schedule for the whole authorization procedure,
- the number and location of the abattoirs,
- the description of the pig population concerned by the assessment method,
- a presentation of the statistical methods used in relation to the sampling method chosen,
- the description of the national quick method,
- the exact presentation of the carcases to be used.
2. Part two of the protocol should give a detailed description of the results of the dissection trial and include in particular:
- a presentation of the statistical methods used in relation to the sampling method chosen,
- the equation which will be introduced or amended,
- a numerical and a graphic description of the results,
- a description of the new apparatus,
- the weight limit of the pigs for which the new method may be used and any other limitation in relation to the practical use of the method.`
