Authorisation for State aid pursuant to Articles 87 and 88 of the EC Treaty
Cases where the Commission raises no objections
(2005/C 24/04)
(Text with EEA relevance)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at
http://europa.eu.int/comm/secretariat_general/sgb/state_aids
--------------------------------------------------
