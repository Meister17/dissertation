COUNCIL DIRECTIVE of 17 December 1973 on the approximation of the laws of the Member States relating to the interior fittings of motor vehicles (interior parts of the passenger compartment other than the interior rear-view mirrors, layout of the controls, the roof or sliding roof, the backrest and rear part of the seats) (74/60/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 100 thereof;
Having regard to the proposal from the Commission;
Having regard to the Opinion of the European Parliament (1);
Having regard to the Opinion of the Economic and Social Committee (2);
Whereas the technical requirements which motor vehicles must satisfy pursuant to national laws relate, inter alia, to interior fittings for the protection of the occupants;
Whereas these requirements differ from one Member State to another ; whereas it is therefore necessary that all Member States adopt the same requirements either in addition to or in place of their existing regulations in order, in particular, to allow for the EEC type approval procedure, which was the subject of the Council Directive of 6 February 1970 (3) on the approximation of the laws of the Member States relating to the type approval of motor vehicles and their trailers to be applied in respect of each type of vehicle;
Whereas common requirements for interior rear view mirrors have been laid down by the Council Directive of 1 March 1971 (4) and requirements should also be drawn up for the interior fittings of the passenger compartment, the layout of the controls, the roof and backrest and rear part of the seats ; whereas other requirements will be adopted subsequently on interior fittings and relating to the anchorages for safety belts and seats, head restraints, the protection of the driver against the steering unit and the layout of the controls;
Whereas harmonized requirements must reduce the risk or the severity of injuries of which motor vehicle drivers may be victims and thereby ensure road safety throughout the entire Community;
Whereas, with regard to technical requirements, it is appropriate to utilize basically those adopted by the United Nations Economic Commission for Europe in its Regulation No 21 ("Uniform requirements concerning the approval of vehicles with regard to their interior fittings") which is annexed to the Agreement of 20 March 1958 concerning the adoption of uniform conditions of approval and reciprocal recognition of approval for motor vehicle equipment and parts,
HAS ADOPTED THIS DIRECTIVE:
Article 1
For the purposes of the Directive, "vehicle" means any motor vehicle in category M1 (defined in Annex I of the Directive of 6 February 1970) designed for use on the road, having at least four wheels and a maximum design speed exceeding 25 km/h.
Article 2
No Member State may refuse to grant EEC type approval or national type approval of a vehicle on grounds relating to such interior fittings of the vehicles as: - the interior parts of the passenger compartment other than the interior rear-view mirror or mirrors,
- the layout of the controls, (1)OJ No C 112, 27.10.1972, p. 14. (2)OJ No C 123, 27.11.1972, p. 32. (3)OJ No L 42, 23.2.1970, p. 1. (4)OJ No L 68, 22.3.1971, p. 1.
- the roof or sliding roof,
- the backrest and rear part of the seats,
if these meet the requirements set out in the Annexes.
Article 3
No Member State may refuse to register or prohibit the sale, entry into service or use of any vehicle on grounds relating to: - the interior parts of the passenger compartment other than the interior rear-view mirror or mirrors,
- the layout of the controls,
- the roof or sliding roof,
- the backrest and the rear part of the seats,
if these items meet the requirements set out in the Annexes.
Article 4
The Member State which has granted type approval must take the necessary measures to ensure that it is informed of any modification of a part or characteristic referred to in Annex I, 2.2. The competent authorities of that State shall determine whether fresh tests should be carried out on the modified vehicle and a fresh report drawn up. Where such tests reveal failure to comply with the requirement of this Directive, the modification shall not be approved.
Article 5
Modifications which are necessary to adapt the provisions of the Annexes to take account of technical progress shall be adopted in accordance with the procedure laid down in Article 13 of the Council Directive of 6 February 1970 on the approximation of the laws of Member States relating to the type approval of motor vehicles and their trailers.
Article 6
1. Member States shall put into force provisions necessary in order to comply with this Directive within eighteen months of its notification and shall immediately inform the Commission thereof.
2. Member States shall ensure that the texts of the main provisions of national law which they adopt in the field covered by this Directive are communicated to the Commission.
Article 7
This Directive is addressed to the Member States.
Done at Brussels, 17 December 1973.
For the Council
The President
I. NØRGAARD
ANNEX I(1) DEFINITIONS, APPLICATIONS FOR EEC APPROVAL AND SPECIFICATIONS
(1.)
2. DEFINITIONS
For the purposes of this Directive (2.1.)
2.2. "Vehicle type" with respect to the interior fittings of the passenger compartment (other than the rear-view mirror(s) : the arrangement of the controls, the roof or sliding roof, the back rest and rear part of the seats) means motor vehicles which do not differ in such essential respects as: 2.2.1. the lines of constituent materials of the bodywork of the passenger compartment,
2.2.2. the arrangement of the controls,
2.3. "reference zone" means the head impact zone as defined in Annex II except: 2.3.1. the area bounded by the forward horizontal projection of a circle circumscribing the outer limits of the steering control, increased by a peripheral band 127 mm in width ; this area is bounded below by the horizontal plane tangential to the lower edge of the steering control when the latter is in the position for driving straight ahead,
2.3.2. the part of the surface of the instrument panel comprised between the edge of the area specified in point 2.3.1 above and the nearest inner side-wall of the vehicle ; this surface is bounded below by the horizontal plane tangential to the lower edge of the steering control ; and
2.3.3. the windscreen side pillars,
2.4. "level of the instrument panel" shall mean the line defined by the points of contact of vertical tangents to the instrument panel,
2.5. "roof" shall mean the upper part of the vehicle extending from the upper edge of the windscreen to the upper edge of the rear window, bounded at the sides by the upper framework of the side-walls,
2.6. "belt line" shall mean the line constituted by the visible lower contour of the side windows of the vehicle,
2.7. "convertible vehicle" shall mean a vehicle which may, in certain cases, have no rigid structural element above the belt line other than the windscreen pillars or roll bar(s),
2.8. "vehicle with sliding roof" shall mean a vehicle of which only the roof or a part of it can be folded back or removed, leaving the existing structural elements of the vehicle above the belt line.
3. APPLICATION FOR EEC TYPE APPROVAL 3.1. The application for type approval of a vehicle shall be submitted by the vehicle manufacturer or by his representative. (1)The text of the Annexes is basically similar to that of Regulation No 21 of the United Nations' Economic Commission for Europe and in particular the subdivision into points is the same ; for this reason when an item in Regulation No 21 does not figure as a corresponding item in this Directive, its number is shown for the record in brackets.
3.2. It shall be accompanied by the undermentioned documents in triplicate and by the following particulars: - a detailed description of the vehicle type with regard to the items mentioned in point 2.2 above;
- a photograph or an exploded view of the passenger compartment ; and
- the numbers and/or symbols identifying the vehicle type must be specified.
3.3. The following must be submitted to the technical service responsible for conducting the tests: 3.3.1. at the manufacturer's discretion, either a vehicle representative of the vehicle type to be approved or the part(s) of the vehicle regarded as essential for the checks and tests prescribed by this Directive ; and
3.3.2. at the request of the aforesaid technical service, certain components and certain samples of the materials used.
(4.)
5. SPECIFICATIONS 5.1. Forward interior parts of the passenger compartment above the level of the instrument panel in front of the front seat H points, excluding the side doors 5.1.1. The reference zone defined in point 2.3 above must not contain any dangerous roughness or sharp edges likely to increase the risk of serious injury to the occupants. Those parts referred to in points 5.1.2 to 5.1.6 hereafter shall be deemed satisfactory if they comply with the requirements thereof.
5.1.2. Vehicle parts within the reference zone with the exception of those which are not part of the instrument panel and which are placed at less than 10 cm from glazed surfaces shall be energy-dissipating as prescribed in Annex III. Those parts within the reference zone which satisfy both of the following conditions shall also be excluded from consideration: - if, during a test in accordance with the requirements of Annex III, the pendulum makes contact with parts outside the reference zone ; and
- if the parts to be tested are placed less than 10 cm away from the parts contacted outside the reference zone, this distance being measured on the surface of the reference zone;
any metal support fittings shall have no protruding edges.
5.1.3. The lower edge of the instrument panel, unless it meets the requirements of point 5.1.2 above, shall be rounded to a radius of curvature of not less than 19 mm.
5.1.4. Switches, pull-knobs, etc, made of rigid material, which, measured in accordance with the method described in Annex V from 3 72 mm to 9 75 mm from the panel, shall have a cross-sectional area of not less than 2 cm2, measured 2.5 mm from the point projecting furthest, and shall have rounded edges with a radius of curvature of not less than 2 75 mm.
5.1.5. If these components project by more than 9 75 mm from the surface of the instrument panel, they shall be so designed and constructed as to be able, under the effect of a longitudinal horizontal force of 37.8 daN delivered by a flat ended ram of not more than 50 mm diameter either to retract into the surface of the panel until they do not project by more than 9 75 mm or to become detached ; in the latter case no dangerous projections of more than 9 75 mm shall remain ; a cross-section not more than 6 75 mm from the point of maximum projection shall be not less than 6 750 cm2 in area.
5.1.6. In the case of a projection consisting of a component made of non-rigid material of less than 50 shore A hardness mounted on a rigid support, the requirements of points 5.1.4 and 5.1.5 shall apply only to the rigid support.
5.2. Forward interior parts of the passenger compartment below the level of the instrument panel and in front of the front seat H points, excluding the side doors and the pedals 5.2.1. Except for the pedals and their fixtures and those components that cannot be contacted by the device described in Annex VI and used in accordance with the procedure described therein, components covered by point 5.2 shall comply with the requirements of points 5.1.4 to 5.1.6 above.
5.2.2. The hand-brake control, if mounted on or under the instrument panel, shall be so placed that, when it is released, there is no possibility of occupants of the vehicle's contacting it in the event of a frontal impact. If this condition is not met, the surface area of the control shall satisfy the requirements of point 5.3.2.3 below.
5.2.3. Shelves and other similar items shall be so designed and constructed that their supports in no case have protruding edges and they meet one or other of the following conditions: 5.2.3.1. the part facing into the vehicle shall present a surface not less than 25 mm high with edges rounded to a radius of curvature of not less than 3.2 mm. This surface shall be covered with an energy-dissipating material as defined in Annex III, and shall be tested in accordance therewith, the impact being applied in a horizontal longitudinal direction.
5.2.3.2. Shelves and other similar items shall, under the effect of a forward-acting horizontal longitudinal force of 37.8 daN exerted by a cylinder of 110 mm diameter with its axis vertical, become detached, break up, be substantially distorted or retract without producing dangerous features on the rim of the shelf. The force must be directed at the strongest part of the shelves or other similar items.
5.2.4. If the items in question contain a part made of material softer than 50 shore A hardness when fitted to a rigid support, the above requirements, except for the requirements covered by Annex III relating to energy absorption, shall apply.
5.3. Other interior fittings in the passenger compartment in front of the transverse plane passing through the torso reference line of the manikin placed on the rear seats 5.3.1. Scope
The requirements of point 5.3.2 below shall apply to control handles, levers and knobs and to any other protruding objects not referred to in point 5.1 and 5.2 above.
5.3.2. Requirements
If the items referred to in point 5.3.1 are so placed that occupants of the vehicle can contact them, they shall meet the requirements of point 5.3.2.1 to 5.3.4. If they can be contacted by a 165 mm diameter sphere and are above the H point of the front seats and forward of the transverse plane of the vehicle reference line of the manikin on the rear seat, and outside the zones defined in points 2.3.1 and 2.3.2, these requirements shall be considered to have been fullfilled if... 5.3.2.1. Their surface shall terminate in rounded edges, the radii of curvature being not less than 3 72 mm.
5.3.2.2. Control levers and knobs shall be so designed and constructed that, under the effect of a forward-acting longitudinal horizontal force of 37 78 daN either the projection in its most unfavourable position shall be reduced to not more than 25 mm from the surface of the panel or the said fittings shall become detached or bent : in the two latter cases no dangerous projections shall remain.
Window winders may, however, project 35 mm from the surface of the panel.
5.3.2.3. The hand brake control when in the released position and the gear lever when in any forward gear position, except when placed in the zones defined in points 2.3.1 and 2.3.2 and zones below the horizontal plane passing through the H point of the front seats, shall have a surface area of at least 6 75 cm2 measured at a cross-section normal to the longitudinal horizontal direction up to a distance of 6 75 mm from the furthest projecting part, the radius of curvature being not less than 3 72 mm.
5.3.3. The requirements in point 5.3.2.3 shall not apply to floor-mounted handbrake controls if the height of the grip in the released position is below a horizontal plane passing through the H point (see Annex IV).
5.3.4. Other items of equipment in the vehicle not covered by the preceding points such as seat slide rails, equipment for regulating the horizontal or vertical part of the seat, devices for retracting safety belts, etc. shall not be subject to any of these provisions if they are situated below a horizontal plane passing through the H point of each seat, even though the occupant is likely to come into contact with such items.
5.3.5. If the items in question include a part made of material softer than 50 shore A hardness mounted on a rigid support, the above requirements shall apply only to the rigid support.
5.4. Roof 5.4.1. Scope 5.4.1.1. The requirements of point 5.4.2 below shall apply to the inner face of the roof.
5.4.1.2. However, they shall not apply to such parts of the roof as cannot be contacted by a sphere 165 mm in diameter.
5.4.2. Requirements 5.4.2.1. That part of the inner face of the roof which is situated above or forward of the occupants shall exhibit no dangerous roughness or sharp edges, directed rearwards or downwards. The width of the projecting parts shall not be less than the amount of their downward projection and the edges shall have a radius of curvature of not less than 5 mm. In particular, the rigid roof sticks or ribs shall not project downwards more than 19 mm and must be streamlined as described in Annex V.
5.4.2.2. If the roof sticks or ribs do not meet the requirements of point 5.4.2.1, they must be covered with an energy-dissipating material as prescribed in Annex III.
5.5. Sliding roof 5.5.1. Requirements 5.5.1.1. The following requirements and those of point 5.4 above concerning the roof shall apply to the sliding roof when it is in the closed position.
5.5.1.2. In addition, the opening and operating devices shall:
be so designed and constructed as to exclude as far as possible accidental operation; 5.5.1.2.2. where possible, be streamlined as described in Annex V : their surfaces shall terminate in rounded edges, the radii of curvature being not less than 5 mm;
5.5.1.2.3. be accommodated, when in the position of rest, in areas which cannot be contacted by a sphere 165 mm in diameter. If this condition cannot be met, the opening and operating devices shall, in the position of rest, either remain retracted or be so designed and constructed that, under the effect of a force of 37 78 daN applied in the direction of impact defined in Annex III as the tangent to the trajectory of the headform, either the projection as described in Annex V shall be reduced to not more than 25 mm beyond the surface on which the devices are mounted or the devices shall become detached ; in the latter case no dangerous projections shall remain.
5.6. Convertible vehicles and vehicles with sliding roof 5.6.1. In the case of convertible vehicles only, the underside of the top of the roll-bar and the top of the windscreen frame shall comply with the requirements of point 5.4.
5.6.2. Vehicles with a sliding roof shall be subject to the requirements of point 5.5, applicable to vehicles with a sliding roof.
5.7. Rear parts of seats 5.7.1. Requirements 5.7.1.1. The surface of the rear parts of seats shall exhibit no dangerous roughness or sharp edges likely to increase the risk or severity of injury to the occupants.
5.7.1.2. Except as provided in points 5.7.1.2.1., 5.7.1.2.2, and 5.7.1.2.3, that part of the back of the front seat which is in the head-impact zone, defined in Annex II, shall be energy-dissipating, as prescribed in Annex III. For determining the head-impact zone, the front seats shall, if they are adjustable, be in the rearmost driving position with their backs inclined as near as possible to 25º unless indicated otherwise by the manufacturer.
5.7.1.2.1. In the case of separate front seats, the rear passengers' head-impact zone shall extend for 10 cm on either side of the seat centre-line, in the top part of the rear of the seat-back.
5.7.1.2.1. bis (1) In the case of seats fitted with head-restraints each test shall be carried out with the head-restraint in the lowest position and at a point situated on the vertical line passing through the centre of the head-restraint.
5.7.1.2.1. ter (1) In the case of a seat which is designed to be fitted in several types of vehicle, the impact zone shall be determined by the vehicle whose rearmost driving position is, of each of the types considered, the least favourable ; the resultant impact zone will be deemed adequate for the other types.
5.7.1.2.2. In the case of front bench seats, the impact zone shall extend between the longitudinal vertical planes 10 cm outboard of the centre line of each designated outboard seating position. The centre line of each outboard seating position of a bench seat shall be specified by the manufacturer.
5.7.1.2.3. In the head impact zone outside the limits prescribed in points 5.7.1.2.1 (a) and (b) and 5.7.1.2.2, the seat frame structure shall be padded to avoid direct (1)These points are not included in Regulation No 21.
contact of the head with it : and, in these zones, shall have a radius of curvature of at least 5 mm. These parts may satisfy the energy dissipating requirements specified in Annex III.
5.7.2. If the impact zones of the seats, head-restraints and their supports contain parts covered with material softer than 50 shore A hardness the above requirements, with the exception of those relating to energy dissipation described in Annex III, shall apply only to the rigid parts. These requirements shall not apply to the rearmost seats, to seats facing sideways or rearwards, to back-to-back seats.
5.8. The requirements of paragraph 5 shall apply to such fittings not mentioned in previous paragraphs which, within the meaning of the various requirements in points 5.1 to 5.7 and according to their location in the vehicle, are capable of being contacted by the occupants. If such parts are made of a material softer than 50 shore A hardness and mounted on (a) rigid support(s), the requirements in question shall apply only to the rigid support(s).
(6.)
(7.)
(8.)
(9.)
ANNEX II DETERMINATION OF THE HEAD-IMPACT ZONE
1. The head-impact zone shall comprise all the non-glazed surfaces of the interior of a vehicle which are capable of entering into static contact with a spherical head 165 mm in diameter which is an integral part of a measuring apparatus whose dimensions from the pivotal point of the hip to the top of the head is continuously adjustable between 736 mm and 840 mm.
2. The aforesaid zone must be determined by the following procedure or its graphic equivalent: 2.1. The pivotal point of the measuring apparatus shall be placed as follows for each seating position for which the manufacturer has made provision: 2.1.1. in the case of sliding seats 2.1.1.1. at the H point (see Annex IV) and
2.1.1.2. at a point situated horizontally 127 mm forward of the H point and at a height either resulting from the variation in the height of the H point caused by a forward shift of 127 mm or of 19 mm.
2.1.2. in the case of non-sliding seats, at the H point of the seat under consideration.
2.2. All points of contact situated below the lower edge of the windscreen and forward of the H point shall be determined for each dimension from the pivotal point to the top of the head capable of being measured by the test apparatus within the interior dimensions of the vehicle.
2.3. If there is no point of contact in the case of adjustment within the above limits, with the test apparatus vertical, possible points of contact shall be determined by pivoting the measuring apparatus forwards and downwards through all arcs of vertical planes as far as 90º from the vertical plane perpendicular in relation to the longitudinal vertical plane of the vehicle and passing through the H point.
3. A "point of contact" shall be a point at which the head of the apparatus contacts a part of the interior of the vehicle. The maximum downward movement shall be limited to a position where the head is tangential to a horizontal plane situated 25 74 mm above the H point.
ANNEX III PROCEDURE FOR TESTING ENERGY DISSIPATING MATERIALS
1. SETTING UP, TEST APPARATUS AND PROCEDURE 1.1. Setting up 1.1.1. The energy-dissipating material shall be mounted and tested on the structural supporting member on which it is to be installed on the vehicle. The test shall preferably be carried out, where possible, directly on the body. The structural member, or the body, shall be firmly attached to the test bench so that it does not move under impact.
1.1.2. However, at the manufacturer's request, the item may be mounted on a fitting simulating its installation on the vehicle, on condition that the "component/fitting" assembly has the same geometrical arrangement and a degree of rigidity not lower and on energy-dissipating capacity not higher than those of the real "component/structural supporting member" assembly.
1.2. Test apparatus 1.2.1. This apparatus shall consist of a pendulum whose pivot is supported by ballbearings and whose reduced mass (1) at its centre of percussion is 6 78 kg. The lower extremity of the pendulum shall consist of a rigid headform 165 mm in diameter whose centre is identical with the centre of percussion of the pendulum.
1.2.2. The headform shall be fitted with 2 decelerometers and a speed measuring device, all capable of measuring values in the direction of impact.
1.3. Recording instruments
The recording instruments used shall be such that measurements can be made with the following degrees of accuracy: 1.3.1. Acceleration: - accuracy = ± 5 % of the real value
- frequency response = up to 1 000 Hz
- cross axis sensitivity =
1.3.2. Speed: - accuracy = ± 2.5 % of the real value
- sensitivity = 0 75 km/h
1.3.3. Indentation of the test component by the headform: - accuracy = ± 5 % of the real value
- sensitivity = 1 mm (1)Note : The relationship of the reduced mass "mr" of the pendulum to the total mass "m" of the pendulum at a distance "a" between the centre of percussion and the axis of rotation and at a distance "l" between the centre of gravity and the axis of rotation is given by the formula: >PIC FILE= "T0005453">
1.3.4. Time recording: - the instrumentation shall enable the action to be recorded throughout its duration and readings to be made to within one thousandth of a second
- the beginning of the impact at the moment of first contact between the headform and the test component shall be noted on the recordings used for analysing the test.
1.4. Test procedure 1.4.1. At every point of impact on the surface to be tested, the direction of impact shall be the tangent to the trajectory of the headform of the measuring apparatus described in Annex II.
1.4.2. Where the angle between the direction of impact and the perpendicular to the surface at the point of impact is 5º or less, the test shall be carried out in such a way that the tangent to the trajectory of the centre of percussion of the pendulum coincides with the direction defined in point 1.4.1. The headform shall strike the test component at a speed of 24 71 km/h ; this speed shall be achieved either by the mere energy of propulsion or by using an additional propelling device.
1.4.3. Where the angle between the direction of impact and the perpendicular to the surface at the point of impact is more than 5º, the test may be carried out in such a way that the tangent to the trajectory of the centre of percussion of the pendulum coincides with the perpendicular to the point of impact. The test speed shall then be reduced to the value of the normal component of the speed prescribed in point 1.4.2.
2. RESULTS
In tests carried out according to the above procedures, the deceleration of the headform shall not exceed 80 g continuously for more than 3 milliseconds. The deceleration rate taken shall be the average of the readings of the two decelerometers.
3. EQUIVALENT PROCEDURES 3.1. Equivalent test procedures shall be permitted, on condition that the results required in paragraph 2 above can be obtained.
3.2. Responsibility for demonstrating the equivalence of a method other than that described in paragraph 1 shall rest with the person using such a method.
ANNEX IV PROCEDURE FOR DETERMINING THE H POINT AND CHECKING THE RELATIVE POSITION OF POINTS R AND H
1. DEFINITIONS 1.1. The H point, which indicates the position of a seated occupant in the passenger compartment, is the trace, in a longitudinal vertical plane, of the theoretical axis of rotation between the leg and the torso of the human body, represented by a manikin.
1.2. The R point, being the "seating reference point", is the manufacturer's design reference point which: 1.2.1. establishes the rearmost normal driving or travelling position of each seat provided in the vehicle by the manufacturer;
1.2.2. has coordinates established relative to the designed vehicle structure;
1.2.3. simulates the position of pivot centre of the human torso and thigh (the H point).
2. DETERMINATION OF H POINTS 2.1. An H point shall be determined for each seat provided by the vehicle manufacturer. When the seats in the same row can be regarded as similar (bench seat, identical seats, etc.), only one H point shall be determined for each row of seats, the manikin described in paragraph 3 below being seated in a place regarded as representative for the row. This place shall be: 2.1.1. in the case of the front row, the driver's seat;
2.1.2. in the case of the rear row (or rows), an outboard seat.
2.2. When an H point is being determined, the seat in question shall be placed in the rearmost normal position provided by the manufacturer for driving or travelling, the back, if adjustable, being locked in a position corresponding as near as possible to a 25º rearward inclination in relation to the vertical of the torso reference line of the manikin described in point 3 below, unless otherwise specified by the manufacturer.
3. DESCRIPTION OF THE MANIKIN 3.1. A three-dimensional manikin of a mass and contour corresponding to those of an adult male of average height shall be used. Such a manikin is illustrated in figures 1 and 2 of the Appendix to this Annex.
3.2. The manikin shall comprise: 3.2.1. two components, one simulating the back and the other the seat of the body, pivoting on an axis representing the axis of rotation between the torso and the thigh. The trace of this axis on the side of the manikin is the manikin's H point:
3.2.2. two components simulating the legs and pivotally attached to the component simulating the seat;
3.2.3. two components simulating the feet and connected to the legs by pivotal joints simulating ankles;
3.2.4. in addition, the component simulating the seat shall be provided with a level enabling its transverse orientation to be verified.
3.3. Body segment weights shall be attached at appropriate points corresponding to the relevant centres of gravity, so as to bring the total mass of the manikin up to about 75 76 kg. Details of the various weights are given in the table on figure 2 of the Appendix to this Annex.
4. SETTING UP THE MANIKIN
The three-dimensional manikin shall be set up in the following manner: 4.1. The vehicle shall be levelled and the seats adjusted as prescribed in point 2.2 above.
4.2. The seat to be tested shall be covered with a piece of material to facilitate correct setting up of the manikin.
4.3. The manikin shall be placed on the seat concerned, the pivotal axis being perpendicular to the longitudinal plane of symmetry of the vehicle.
4.4. The feet of the manikin shall be placed as follows: 4.4.1. in the case of the front seats so that the level verifying the transverse orientation of the seat of the manikin is brought to the horizontal;
4.4.2. in the case of the rear seats, the feet shall as far as possible be so arranged as to be in contact with the front seats. If the feet then rest on parts of the floor which are at different levels, the foot which first comes into contact with the front seat shall serve as a reference point and the other foot shall be so arranged that the level verifying the transverse orientation of the seat of the manikin is brought to the horizontal;
4.4.3. if the H point is being determined at a centre seat, the feet shall be placed one on each side of the tunnel.
4.5. The weights shall be placed on the thighs, the transverse level of the seat of the manikin brought to the horizontal and the weights placed on the seat of the manikin.
4.6. The manikin shall be moved away from the seat back by means of the knee-pivot bar and the back tilted forwards. The manikin shall be re-positioned on the seat of the vehicle by being slid backwards on its seat until resistance is encountered, the back of the manikin then being replaced against the seat back.
4.7. A horizontal force of 10 ± 1 daN shall be applied twice to the manikin. The direction and point of application of the force are shown by a black arrow on figure 2 of the Appendix.
4.8. The weights shall be installed on the right and left sides, and subsequently on the torso. The transverse level of the manikin shall be kept horizontal.
4.9. The manikin being kept horizontal in the transverse direction, the back shall be tilted forwards until the torso weights are above the H point, so as to eliminate any friction with the seat back.
4.10. The back of the manikin shall be gently moved rearwards so as to complete the setting-up operation ; the transverse level of the manikin shall be horizontal. If it is not, the above procedure shall be repeated.
5. RESULTS 5.1. When the manikin has been set up as described in point 4 above, the H point of the vehicle seat concerned shall be the H point on the manikin.
5.2. Each of the coordinates of the H point shall be measured as accurately as possible. The same shall apply to the coordinates representing specific points of the passenger compartment. The projections of these points on a vertical longitudinal plane shall then be plotted on a graph.
6. CHECKING OF THE RELATIONSHIP BETWEEN THE R AND H POINTS 6.1. The results obtained under point 5.2. regarding the H point must be compared with the coordinates of the R point as supplied by the vehicle manufacturer.
6.2. The checking of the relationship between the two points shall be considered satisfactory for the seating position in question, provided the H point coordinates lie within a longitudinal rectangle whose horizontal and vertical sides are 30 mm and 20 mm respectively, and whose diagonals intersect at the R point. If this is the case the R point shall be used for the test and, if necessary, the manikin shall be so adjusted that the H point coincides with the R point.
6.3. If the H point does not lie within the rectangle described in point 6.2 above, two further determinations of the H point shall be carried out (three in all). If two of the three points so determined lie within the rectangle, the result of the test shall be considered satisfactory.
6.4. If at least two of the three points determined lie outside the rectangle, the result of the test shall be considered unsatisfactory.
6.5. In the case of the situation described in point 6.4 above, or when verification cannot be carried out because the vehicle manufacturer has failed to provide data on the position of the R point, the average result of the three determinations of the H point may be used and considered applicable in all cases where the R point is mentioned in this Directive.
6.6. When checking the relationship of the R and the H points in a vehicle in current production, the rectangle mentioned in point 6.2 above shall be replaced by a square whose side measures 50 mm.
>PIC FILE= "T0005454">
>PIC FILE= "T0005455">
ANNEX V METHOD OF MEASURING PROJECTIONS
1. To determine the amount by which an item projects in relation to the panel on which it is mounted, a 165 mm sphere shall be moved tangentially to the panel and component under consideration.
The gradient shall be considered as being formed by the relationship between the "y" variation measured from the centre of the sphere perpendicular to the panel and the "x" variation of measurement from the same centre parallel to the panel.
A form shall be deemed streamlined if the resultant horizontal longitudinal gradient is less than 1. When a gradient is more than or equal to 1, there shall be a projection the value of which measured by the determinant "y".
If the panels and components, etc., are covered with materials softer than 50 shore A hardness, the procedure for the measuring of forms and projections described above shall apply only after removal of such materials.
2. The projection of switches, pull-knobs, etc., situated in the reference area shall be measured by using the test apparatus and procedure described below: 2.1. Apparatus 2.1.1. The measuring apparatus for projections shall consist of a hemispherical headform 165 mm in diameter, in which there is a sliding ram of 50 mm diameter.
2.1.2. Relative positions of the flat end of the ram and the edge of the headform shall be shown on a graduated scale, on which a mobile index shall register the maximum measurement achieved when the apparatus is moved away from the item tested. A minimum distance of 30 mm shall be measurable ; the measuring scale shall be graduated in half-millimeters to make possible an indication of the extent of the projections in question.
2.1.3. Gauging procedure: 2.1.3.1. The apparatus shall be placed on a flat surface so that its axis is perpendicular to that surface. When the flat end of the ram contacts the surface, the scale shall be set at zero.
2.1.3.2. A 10 mm strut shall be inserted between the flat end of the ram and the retaining surface ; a check shall be made to ensure that the mobile index records this measurement.
2.1.4. The apparatus for measuring projections is illustrated in figure 1.
2.2. Test procedure 2.2.1. A cavity shall be formed in the headform by pulling back the ram and the mobile index shall be placed against the ram.
2.2.2. The apparatus shall be applied to the projection to be measured so that the headform contacts the maximum surrounding surface area, with a force not exceeding 2 daN.
2.2.3. The ram shall be pushed forward until it makes contact with the projection to be measured and the amount of the projection shall be observed on the scale.
2.2.4. The headform shall be adjusted to obtain maximum projection. The amount of the projection shall be recorded.
2.2.5. If two or more controls are situated sufficiently close for the ram or the headform to contact them simultaneously, they shall be treated as follows: 2.2.5.1. Multiple controls, all of which can be contained in the headform cavity, shall be regarded as forming a single projection.
2.2.5.2. If other controls prevent normal testing by contacting the headform, they shall be removed and the test shall be conducted without them. They may subsequently be re-installed and tested in their turn with other controls that have been removed to facilitate the procedure.
>PIC FILE= "T0005456">
ANNEX VI APPARATUS AND PROCEDURE FOR APPLICATION OF ITEM 5.2.1 OF ANNEX I
Those parts (switches, pull-knobs etc.) which can be contacted by using the apparatus and procedure described below shall be considered as being likely to be contacted by the knees of an occupant: 1. Apparatus
Diagram of apparatus >PIC FILE= "T0005457">
2. Procedure
The apparatus may be placed in any position below the instrument panel so that: - the plane XX' remains parallel to the median longitudinal plane of the vehicle
- the axis X can be rotated above and below the horizontal through angles up to 30º.
3. In carrying out the above test, all materials of less than 50 shore A hardness shall be removed.
