Decision of the EEA Joint Committee
No 2/2005
of 8 February 2005
amending Annex I (Veterinary and phytosanitary matters) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex I to the Agreement was amended by Decision of the EEA Joint Committee No 140/2004 of 29 October 2004 [1]
(2) Council Regulation (EC) No 355/2003 of 20 February 2003 on the authorisation of the additive avilamycin in feedingstuffs [2] is to be incorporated into the Agreement.
(3) Commission Regulation (EC) No 1334/2003 of 25 July 2003 amending the conditions for authorisation of a number of additives in feedingstuffs belonging to the group of trace elements [3], as corrected by OJ L 14, 21.1.2004, p. 54, is to be incorporated into the Agreement.
(4) Regulation (EC) No 1831/2003 of the European Parliament and of the Council of 22 September 2003 on additives for use in animal nutrition [4] is to be incorporated into the Agreement.
(5) Commission Regulation (EC) No 1852/2003 of 21 October 2003 authorising the use for 10 years of a coccidiostat in feedingstuffs [5] is to be incorporated into the Agreement.
(6) Commission Regulation (EC) No 2112/2003 of 1 December 2003 correcting Regulation (EC) No 1334/2003 amending the conditions for authorisation of a number of additives in feedingstuffs belonging to the group trace elements [6] is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
Chapter II of Annex I to the Agreement shall be amended as specified in the Annex to this Decision.
Article 2
The texts of Regulations (EC) Nos 355/2003, 1334/2003, as corrected by OJ L 14, 21.1.2004, p. 54, 1831/2003, 1852/2003 and 2112/2003 in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 9 February 2005, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [7].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 8 February 2005.
For the EEA Joint Committee
The President
Richard Wright
[1] OJ L 102, 21.4.2005, p. 4.
[2] OJ L 53, 28.2.2003, p. 1.
[3] OJ L 187, 26.7.2003, p. 11.
[4] OJ L 268, 18.10.2003, p. 29.
[5] OJ L 271, 22.10.2003, p. 13.
[6] OJ L 317, 2.12.2003, p. 22.
[7] No constitutional requirements indicated
--------------------------------------------------
ANNEX
to Decision of the EEA Joint Committee No 2/2005
1. Point 1a shall be renumbered as point 1aa.
2. The following point shall be inserted after point 1 (Council Directive 70/524/EEC):
"1a. 32003 R 1831: Regulation (EC) No 1831/2003 of the European Parliament and of the Council of 22 September 2003 on additives for use in animal nutrition (OJ L 268, 18.10.2003, p. 29).
The provisions of this Regulation shall, for the purposes of the present Agreement, be read with the following adaptations:
- with regard to coccidiostates and histomonostats, maintain its national legislation;
- with regard to antibiotics, maintain its national legislation until such substances are deleted from the Community Register of Feed Additives in accordance with Article 11 paragraph 2 of the Regulation.
- with regard to coccidiostates and histomonostats and trace element copper as growth promoter, maintain its national legislation;
- with regard to antibiotics, maintain its national legislation until such substances are deleted from the Community Register of Feed Additives in accordance with Article 11 paragraph 2 of the Regulation.
c) Adaptations a) and b) shall also apply to authorisations of coccidistates and histomonostats, trace element copper as growth promoter and antibiotics for use as additives in animals nutrition."
3. The following points shall be inserted after point 1zo (Commission Regulation (EC) No 880/2004):
"1zp. 32003 R 0355: Council Regulation (EC) No 355/2003 of 20 February 2003 on the authorisation of the additive avilamycin in feedingstuffs (OJ L 53, 28.2.2003, p. 1).
1zq. 32003 R 1334: Commission Regulation (EC) No 1334/2003 of 25 July 2003 amending the conditions for authorisation of a number of additives in feedingstuffs belonging to the group of trace elements, "(OJ L 187, 26.7.2003, p. 11) as corrected by OJ L 14, 21.1.2004, p. 54", as amended by:
- 32003 R 2112: Commission Regulation (EC) No 2112/2003 of 1 December 2003 (OJ L 317, 2.12.2003, p. 22).
1zr. 32003 R 1852: Commission Regulation (EC) No 1852/2003 of 21 October 2003 authorising the use for 10 years of a coccidiostat in feedingstuffs (OJ L 271, 22.10.2003, p. 13)."
4. The following indent shall be added in point 15 (Council Directive 82/471/EEC):
"— 32003 R 1831: Regulation (EC) No 1831/2003 of the European Parliament and of the Council of 22 September 2003 (OJ L 268, 18.10.2003, p. 29)."
--------------------------------------------------
