Commission Regulation (EC) No 1041/2006
of 7 July 2006
amending Annex III to Regulation (EC) No 999/2001 of the European Parliament and of the Council as regards monitoring of transmissible spongiform encephalopathies in ovine animals
(Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Regulation (EC) No 999/2001 of the European Parliament and of the Council of 22 May 2001 laying down rules for the prevention, control and eradication of certain transmissible spongiform encephalopathies [1], and in particular the first paragraph of Article 23 thereof,
Whereas:
(1) Regulation (EC) No 999/2001 lays down rules for the monitoring of transmissible spongiform encephalopathies (TSE) in ovine animals.
(2) On 8 March 2006, a panel of experts on TSEs in small ruminants, chaired by the Community Reference Laboratory for TSEs (CRL), confirmed that bovine spongiform encephalopathy (BSE) in those animals cannot be excluded following the results of the second stage of discriminatory testing of brain samples of two sheep from France and one sheep from Cyprus. Further testing is required in order to exclude the presence of BSE in such animals.
(3) In April 2002, the former Scientific Steering Committee of the European Commission (SSC) adopted an opinion on safe sourcing of small ruminant materials should BSE become likely in small ruminants. In its opinion of November 2003, the Scientific Panel on Biological Hazards of the European Food Safety Authority (EFSA) endorsed the recommendations of the SSC opinion with regard to the TSE-related safety of certain small ruminant products.
(4) The significance of those TSE cases in France and Cyprus, where the presence of BSE cannot be excluded should be assessed. In order to do so, the results of an increased monitoring of TSEs in sheep is essential. Therefore, and in line with the SSC and EFSA opinions, the monitoring of sheep should be extended in order to improve the Community eradication programmes. Those programmes also increase the level of consumer protection, in addition to the safe sourcing of sheep products assured by the current measures, in particular the provisions on the removal of specified risk materials, in Regulation (EC) No 999/2001.
(5) The extended monitoring should be based on a statistically valid survey in order to determine the likely prevalence of BSE in sheep as soon as possible and to improve knowledge of the geographical distribution of the disease.
(6) In view of the high level of TSEs in the ovine and caprine population in Cyprus the extended monitoring of ovine animals can be limited to non-infected flocks.
(7) The monitoring programme in ovine animals should be reviewed after at least six months of effective monitoring.
(8) Regulation (EC) No 999/2001 should therefore be amended accordingly.
(9) In order to ensure the highest possible level of consumer protection by assessing the possible prevalence of BSE in ovine animals, the amendments made by this Regulation should enter into force without delay.
(10) The measures provided for in this Regulation are in accordance with the opinion of the Standing Committee of the Food Chain and Animal Health,
HAS ADOPTED THIS REGULATION:
Article 1
Annex III to Regulation (EC) No 999/2001 is amended in accordance with the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 7 July 2006.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 147, 31.5.2001, p. 1. Regulation as last amended by Commission Regulation (EC) No 688/2006 (OJ L 120, 5.5.2006, p. 10).
--------------------------------------------------
ANNEX
In Part II of Annex III to Regulation (EC) No 999/2001 points 2 and 3 of Chapter A are replaced by the following:
"2. Monitoring in ovine and caprine animals slaughtered for human consumption
(a) Ovine animals
Member States shall test healthy slaughtered ovine animals in accordance with the minimum sample sizes listed in Table A of this point and the sampling rules set out in point 4.
Table A
Member State | Minimum sample size in healthy slaughtered ovine animals [1] |
Germany | 37500 |
Greece | 23000 |
Spain | 41800 |
France | 42400 |
Ireland | 40500 |
Italy | 43700 |
the Netherlands | 23300 |
Austria | 14300 |
Poland | 23300 |
Portugal | 14300 |
United Kingdom | 44000 |
Other Member States | all |
By way of derogation from the minimum sample sizes listed in Table A Cyprus may decide to test only a minimum of two ovine animals sent for slaughter for human consumption from every flock where no TSE cases have been registered.
(b) Caprine animals
Member States shall test healthy slaughtered caprine animals in accordance with the minimum sample sizes listed in Table B of this point and the sampling rules set out in point 4.
Table B
Member State | Minimum sample size in healthy slaughtered caprine animals [2] |
Greece | 20000 |
Spain | 125500 |
France | 93000 |
Italy | 60000 |
Cyprus | 5000 |
Austria | 5000 |
Other Member States | all |
(c) Where a Member State experiences difficulty in collecting sufficient numbers of healthy slaughtered ovine or caprine animals to reach its allotted minimum sample size established in points (a) and (b), it may choose to replace a maximum of 50 % of its minimum sample size by testing dead ovine or caprine animals over the age of 18 months at the ratio of one to one and in addition to the minimum sample size set out in point 3. In addition a Member State may choose to replace a maximum of 10 % of its minimum sample size by testing ovine or caprine animals killed in the framework of a disease eradication campaign over the age of 18 months at the ratio of one to one.
3. Monitoring in ovine and caprine animals not slaughtered for human consumption
Member States shall test in accordance with the sampling rules set out in point 4 and the minimum sample sizes indicated in Table C and Table D, ovine and caprine animals which have died or been killed, but which were not:
- killed in the framework of a disease eradication campaign, or
- slaughtered for human consumption.
Table C
Member State population of ewes and ewe lambs put to the ram | Minimum sample size of dead ovine animals [3] |
> 750000 | 20000 |
100000-750000 | 3000 |
40000-100000 | 100 % up to 1000 |
< 40000 | 100 % up to 200 |
Table D
Member State population of goats which have already kidded and goats mated | Minimum sample size of dead caprine animals [4] |
> 750000 | 10000 |
250000-750000 | 3000 |
40000-250000 | 100 % up to 1000 |
< 40000 | 100 % up to 200 |
[1] Minimum sample sizes are set to take account of the size of the number of healthy slaughtered ovine animals and are intended to provide achievable targets. The minimum sample sizes above 30000 allow the detection of a prevalence of 0,003 % with a 95 % confidence.
[2] Minimum sample sizes are set to take account of the size of the number of healthy slaughtered caprine animals and the prevalence of BSE in the individual Member State. They are also intended to provide achievable targets. The minimum sample sizes above 60000 allow the detection of a prevalence of 0,0017 % with a 95 % confidence.
[3] Minimum sample sizes are set to take account of the size of the ovine and caprine populations in the individual Member States and are intended to provide achievable targets.
[4] Minimum sample sizes are set to take account of the size of the ovine and caprine populations in the individual Member States and are intended to provide achievable targets."
--------------------------------------------------
