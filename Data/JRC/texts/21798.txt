COMMISSION DECISION of 11 June 1992 laying down certain rules to ensure coordination between organizations and associations which maintain or establish stud-books for registered equidae (92/354/EEC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Directive 90/427/EEC of 26 June 1990 on the zootechnical and genealogical conditions governing intra-Community trade in equidae (1), and in particular Article 4 (2) (e) thereof,
Whereas Decision 92/353/EEC (2) lays down the criteria for the approval or recognition of organizations and associations which maintain or establish stud-books for registered equidae; whereas that Decision provides that the authorities of the Member State concerned are responsible for approving or recognizing organizations or associations which maintain or establish stud-books;
Whereas in this context, therefore, rules should be laid down to govern the relations between the organization or association which maintains the stud-book of the origin of the breed and an organization or association which maintains or establishes a stud-book or a section of a stud-book for that breed;
Whereas provisions should be made for a procedure enabling the competent authorities concerned to ensure, in special circumstances, coordination between two organizations or associations; whereas it is also important that all the information required for the adoption of the specific coordination rules pursuant to Article 4 (2) (e) of Directive 90/427/EEC be made available to the Commission;
Whereas the measures provided for in this Decision are in accordance with the opinion of the Standing Committee on Zootechnics,
HAS ADOPTED THIS DECISION:
Article 1
The organization or association which maintains the stud-book of the origin of the breed must ensure close collaboration with the organization and associations which maintain stud-books or sections of a stud-book of the same breed, particularly with a view to preventing any dispute.
Article 2
1. Where the competent authority of a Member State considers that an approved or recognized organization or association in another Member State is not complying with the rules laid down in the relevant Community legislation, and particularly with the principles laid down by the organization or association which maintains the stud-book of the origin of the breed, it shall contact the competent authority of the other Member State concerned forthwith.
The latter shall take all the necessary measures and inform the competent authority of the first Member State of the type of checks carried out, the decisions taken and the reasons for those decisions.
2. If the competent authorities of the first Member State considers these measures likely to be insufficient, it shall explore with the authority of the other Member State ways of remedying the situation, if necessary by visiting that Member State.
3. The competent authorities of the Member States shall inform the Commission of the solutions decided upon.
4. In the event that no solution is found within six months, at the request of one of the authorities of the Member States concerned or on its own initiative, the Commission may, in particular, send an inspection team in collaboration with the competent national authorities.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 11 June 1992. For the Commission
Ray MAC SHARRY
Member of the Commission
(1) OJ No L 224, 18. 8. 1990, p. 55. (2) See page 65 of this Official Journal.
