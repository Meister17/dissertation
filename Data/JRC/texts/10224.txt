Judgment of the Court
(Grand Chamber)
of 6 December 2005
in Case C-66/04: United Kingdom of Great Britain and Northern Ireland v European Parliament, Council of the European Union [1]
In Case C-66/04: Action for annulment under Article 230 EC, brought on 11 February 2004, United Kingdom of Great Britain and Northern Ireland (Agents: R. Caudwell and M. Bethell, assisted by Lord Goldsmith QC, N. Paines QC and T. Ward, Barrister)
v European Parliament (Agents: K. Bradley and M. Moore), Council of the European Union (Agents: M. Sims, E. Karlsson and F. Ruggeri Laderchi), supported by: Commission of the European Communities (Agents: J.-P. Keppenne, N. Yerrell and M. Shotter) — the Court (Grand Chamber), composed of V. Skouris, President, P. Jann, C.W.A. Timmermans, A. Rosas and K. Schiemann, Presidents of Chambers, S. von Bahr, J.N. Cunha Rodrigues, R. Silva de Lapuerta (Rapporteur), K. Lenaerts, P. Kūris, E. Juhász, A. Borg Barthet and M. Ilešič, Judges; J. Kokott, Advocate General; K. Sztranc, Administrator, for the Registrar, gave a judgment on 6 December 2005, in which it:
1. Dismisses the action;
2. Orders the United Kingdom of Great Britain and Northern Ireland to pay the costs;
3. Orders the Commission of the European Communities to bear its own costs.
[1] OJ C 94 of 17. 04. 2004.
--------------------------------------------------
