Commission Regulation (EC) No 1186/2006
of 3 August 2006
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 4 August 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 3 August 2006.
For the Commission
Jean-Luc Demarty
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 386/2005 (OJ L 62, 9.3.2005, p. 3).
--------------------------------------------------
ANNEX
to Commission Regulation of 3 August 2006 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code [1] | Standard import value |
07070005 | 052 | 58,1 |
388 | 52,4 |
524 | 46,9 |
999 | 52,5 |
07099070 | 052 | 52,0 |
999 | 52,0 |
08055010 | 388 | 70,8 |
524 | 42,6 |
528 | 57,2 |
999 | 56,9 |
08061010 | 052 | 95,9 |
204 | 173,8 |
220 | 190,1 |
508 | 55,0 |
999 | 128,7 |
08081080 | 388 | 90,5 |
400 | 104,7 |
508 | 82,9 |
512 | 96,9 |
524 | 66,4 |
528 | 123,9 |
720 | 81,3 |
804 | 99,8 |
999 | 93,3 |
08082050 | 052 | 138,2 |
388 | 98,2 |
512 | 77,8 |
528 | 73,7 |
720 | 31,1 |
804 | 186,4 |
999 | 100,9 |
08092095 | 052 | 328,4 |
400 | 287,4 |
404 | 316,7 |
999 | 310,8 |
08093010, 08093090 | 052 | 148,4 |
999 | 148,4 |
08094005 | 068 | 110,8 |
093 | 52,7 |
098 | 59,4 |
624 | 124,4 |
999 | 86,8 |
[1] Country nomenclature as fixed by Commission Regulation (EC) No 750/2005 (OJ L 126, 19.5.2005, p. 12). Code "999" stands for "of other origin".
--------------------------------------------------
