Time limit for objection for registrations pursuant to Article 9 of Council Regulation (EC) No 509/2006 of 20 March 2006 on agricultural products and foodstuffs as traditional specialities guaranteed [1]
(2006/C 139/05)
Third countries and natural or legal persons having a legitimate interest, established or resident in a third country, may object to the applications pursuant to Article 9 of the abovementioned Regulation directly to the Commission. For this purpose, the period of time referred to in Article 9(1) of Council Regulation (EC) No 509/2006 starts on the day of publication of the present notice for the following application:
| OJ reference |
Boerenkaas | C 316, 13.12.2005, p. 16. |
[1] OJ L 93, 31.3.2006, p. 1.
--------------------------------------------------
