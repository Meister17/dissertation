SEVENTH COMMISSION DIRECTIVE of 1 March 1976 establishing Community methods of analysis for the official control of feedingstuffs (76/372/EEC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to the Council Directive of 20 July 1970 on the introduction of Community methods of sampling and analysis for the official control of feedingstuffs (1), as last amended by the Act of Accession (2), and in particular Article 2 thereof,
Whereas that Directive requires that official control of feedingstuffs be carried out using Community methods of sampling and analysis for the purpose of checking compliance with requirements arising under the provisions laid down by law, regulation or administrative action concerning the quality and composition of feedingstuffs;
Whereas Commission Directives 71/250/EEC of 15 June 1971 (3), 71/393/EEC of 18 November 1971 (4), 72/199/EEC of 27 April 1972 (5), 73/46/EEC of 5 December 1972 (6), 74/203/EEC of 25 March 1974 (7) and 75/84/EEC of 20 December 1974 (8) have already established a number of Community methods of analysis ; whereas the progress of work since then makes it advisable to adopt a seventh set of methods;
Whereas the measures provided for in this Directive are in accordance with the opinion of the Standing Committee for Feedingstuffs,
HAS ADOPTED THIS DIRECTIVE:
Article 1
The Member States shall require that analyses for official controls of feedingstuffs, as regards their content of aflatoxin B1, be carried out in accordance with the methods described in the Annex to this Directive.
The general provisions set out in Part 1 (Introduction) of the Annex to first Commission Directive 71/250/EEC of 15 June 1971, with the exception of the part dealing with preparation of the sample to be analyzed, shall be applicable to the methods described in the Annex to this Directive.
Article 2
The Member States shall, not later than 1 October 1976, bring into force the laws, regulations or administrative provisions necessary to comply with this Directive. They shall forthwith notify the Commission thereof.
Article 3
This Directive is addressed to the Member States.
Done at Brussels, 1 March 1976.
For the Commission
P.J. LARDINOIS
Member of the Commission (1)OJ No L 170, 3.8.1970, p. 2. (2)OJ No L 73, 27.3.1972, p. 14. (3)OJ No L 155, 12.7.1971, p. 13. (4)OJ No L 279, 20.12.1971, p. 7. (5)OJ No L 123, 29.5.1972, p. 6. (6)OJ No L 83, 30.3.1973, p. 21. (7)OJ No L 108, 22.4.1974, p. 7. (8)OJ No L 32, 5.2.1975, p. 26.
ANNEX DETERMINATION OF AFLATOXIN B1
A. ONE-DIMENSIONAL THIN LAYER CHROMATOGRAPHIC METHOD
1. Purpose and scope
The method makes it possible to determine the level of aflatoxin B1 in the following feedingstuffs : groundnut, copra, linseed, soya, sesame, babassu palm and maizegerm oil cakes, cereals and cereal products, pea meal, potato pulp and starch. The lower limit of determination is 0 701 mg/kg (10 ppb).
If the presence of interfering substances hinders the determination, it is necessary to start the analysis again using method B (two-dimensional thin layer chromatography).
2. Principle
The sample is subjected to extraction with chloroform. The extract is filtered, and an aliquot portion taken and purified by column chromatography on silica gel. The eluate is evaporated and the residue redissolved in a specific volume of chloroform or of a mixture of benzene and acetonitrile. An aliquot portion of this solution is subjected to thin-layer chromatography (TLC). The quantity of aflatoxin B1 is determined under UV irradiation of the chromatogram, either visually or by fluorodensitometry, by comparison with known quantities of standard aflatoxin B1. The identity of the aflatoxin B1 extracted from the feedingstuff must be confirmed by the procedure indicated.
3. Reagents
NB : All the reagents must be of "analytical reagent" quality unless otherwise stated. 3.1. Acetone.
3.2. Chloroform, stabilized with 0 75 to 1 70 % of 96 % ethanol (v/v).
3.3. N-hexane.
3.4. Methanol.
3.5. Anhydrous diethyl ether, free from peroxides.
3.6. Mixture of benzene and acetonitrile : 98/2 (v/v).
3.7. Mixture of chloroform (3.2) and methanol (3.4) : 97/3 (v/v).
3.8. Silica gel, for column chromatography, particle size 0 705 to 0 720 mm.
3.9. Absorbent cotton wool, previously defatted with chloroform, or glass wool.
3.10. Sodium sulphate, anhydrous, granular.
3.11. Inert gas, e.g. nitrogen.
3.12. 1 N Hydrochloric acid.
3.13. 50 % (v/v) sulphuric acid.
3.14. Kieselguhr (hyflosupercel), washed in acid.
3.15. Silica gel G-HR or equivalent, for TLC.
3.16. Standard solution with about 0 71 ¶g of aflatoxin B1 per ml in chloroform (3.2) or the benzene/acetonitrile mixture (3.6), prepared and checked as indicated in Section 7.
3.17. Standard solution for qualitative testing purposes containing about 0 71 ¶g of aflatoxin B1 and B2 per ml in chloroform (3.2) or the benzene/acetonitrile mixture (3.6). These concentrations are given as a guide. They must be adjusted so as to obtain the same intensity of fluorescence for both aflatoxins.
3.18. Developing solvents: 3.18.1. Chloroform (3.2)/acetone (3.1) : 9/1 (v/v), unsaturated tank;
3.18.2. Diethyl ether (3.5)/methanol (3.4)/water : 96/3/1 (v/v/v), unsaturated tank;
3.18.3. Diethyl ether (3.5)/methanol (3.4)/water : 94/4.5/1.5 (v/v/v), saturated tank;
3.18.4. Chloroform (3.2)/methanol (3.4) : 94/6 (v/v), saturated tank;
3.18.5. Chloroform (3.2)/methanol (3.4) : 97/3 (v/v), saturated tank.
4. Apparatus. 4.1. Grinder-mixer.
4.2. Shaking apparatus or magnetic stirrer.
4.3. Fluted filter papers, Schleicher and Schüll No 588 or equivalent, diameter : 24 cm
4.4. Glass tube for chromatography (internal diameter : 22 mm, length : 300 mm), with a PTFE cock and a 250-ml reservoir.
4.5. Rotary vacuum evaporator, with a 500-ml round-bottom flask.
4.6. 500-ml conical flasks, with ground-glass stoppers.
4.7. TLC apparatus.
4.8. Glass plates for TLC, 200 × 200 mm, prepared as follows (the quantities indicated are sufficient to cover five plates). Put 30 g of silica gel G-HR (3.15) into a conical flask. Add 60 ml water, stopper and shake for a minute. Spread the suspension on the plates so as to obtain a uniform layer 0 725 mm thick. Leave to dry in the air and then store in a desiccator containing silica gel. At the time of use, activate the plates by keeping them in the oven at 110 ºC for one hour.
Ready-to-use plates are suitable if they give results similar to those obtained with the plates prepared as indicated above.
4.9. Long-wavelength (360 nm) UV lamp. The intensity of irradiation must make it possible for a spot of 1 ng of aflatoxin B1 to be still clearly distinguished on a TLC plate at a distance of 10 cm from the lamp.
4.10. 10 ml graduated tubes with polyethylene stoppers.
4.11. UV spectrophotometer.
4.12. Fluorodensitometer (optional).
5. Procedure 5.1. Preparation of the sample (see under "Observations", Part C, point 1).
Grind the sample so that the whole of it will pass through a sieve with a 1-mm mesh (in accordance with recommendation ISO R 565).
5.2. Extraction
Put 50 g of ground, homogenized sample into a 500-ml conical flask (4.6). Add 25 g of Kieselguhr (3.14), 25 ml of water and 250 ml of chloroform (3.2). Stopper the flask, shake or stir for 30 minutes with the apparatus (4.2) and filter through a fluted filter paper (4.3). Discard the first 10 ml of the filtrate and then collect 50 ml.
5.3. Column clean-up
Insert into the lower end of a chromatography tube (4.4) a cotton or glass wool plug (3.9), fill two-thirds of the tube with chloroform (3.2) and add 5 g of sodium sulphate (3.10).
Check that the upper surface of the sodium sulphate layer is flat, then add 10 g of silica gel (3.8) in small portions. Stir carefully after each addition to eliminate air bubbles. Leave to stand for 15 minutes and then carefully add 15 g of sodium sulphate (3.10). Let the liquid fall until it is just above the upper surface of the sodium sulphate layer.
Mix the 50 ml of extract collected in 5.2 with 100 ml of n-hexane (3.3) and quantitatively transfer the mixture to the column. Let the liquid fall until it is just above the upper surface of the sodium sulphate layer. Discard this washing. Then add 100 ml of diethylether (3.5) and again allow it to fall to the upper surface of the sodium sulphate layer. During these operations see that the rate of flow is 8 to 12 ml per minute and that the column does not run dry. Discard the liquid that comes out. Then elute with 150 ml of the chloroform/methanol mixture (3.7) and collect the whole of the eluate.
Evaporate the latter almost to dryness at a temperature not exceeding 50 ºC and under a stream of inert gas (3.11) with the rotary evaporator (4.5). Quantitatively transfer the residue, using chloroform (3.2) or the benzene-acetonitrile mixture (3.6), to a 10 ml graduated tube (4.10). Concentrate the solution under a stream of inert gas (3.11) and then adjust the volume to 2 ml with chloroform (3.2) or the benzene/acetonitrile mixture (3.6).
5.4. Thin-layer chromatography
Spot on a TLC plate (4.8), 2 cm from the lower edge and at intervals of 2 cm, the volumes indicated below of the standard solution and the extract: - 10, 15, 20, 30 and 40 ¶l of the standard aflatoxin B1 solution (3.16);
- 10 ¶l of the extract obtained in 5.3 and, superimposed on the same point, 20 ¶l of the standard solution (3.16);
- 10 and 20 ¶l of the extract obtained in 5.3.
Develop the chromatogram in the dark with one of the developing solvents (3.18). The choice of the solvent must be made beforehand, by depositing 25 ¶l of the qualitative standard solution (3.17) on the plate and checking that, when developed, aflatoxin B1 and B2 are completely separated.
Let the solvents evaporate in the dark and then irradiate with UV light, placing the plate 10 cm from the lamp (4.9). The spots of aflatoxin B1 give a blue fluorescence.
5.5. Quantitative determinations
Determine either visually or by fluorodensitometry as indicated below. 5.5.1. Visual measurements
Determine the quantity of aflatoxin B1 in the extract by matching the fluorescence intensity of the extract spots with that of one of the standard solution spots. Interpolate if necessary. The fluorescence obtained by the superimposition of the extract on the standard solution must be more intense than that of the 10 ¶l of extract and there must not be more than one visible spot. If the fluorescence intensity given by the 10 ¶l of extract is greater than that of the 40 ¶l of standard solution, dilute the extract 10 or 100 times with chloroform (3.2) or the benzene/acetonitrile mixture (3.6) before subjecting it again to thin-layer chromatography.
5.5.2. Measurements by fluorodensitometry
Measure the fluorescence intensity of the aflatoxin B1 spots with the fluorodensitometer (4.12) at an excitation wavelength of 365 nm and an emission wavelength of 443 nm. Determine the quantity of aflatoxin B1 in the extract spots by comparison of their fluorescence intensities with that of the standard aflatoxin B1 spots.
5.6. Confirmation of the identity of aflatoxin B1
Confirm the identity of the aflatoxin B1 in the extract by the processes indicated below. 5.6.1. Treatment with sulphuric acid
Spray sulphuric acid (3.13) on to the chromatogram obtained in 5.4. The fluorescence of the aflatoxin B1 spots must turn from blue to yellow under UV irradiation.
5.6.2. Two-dimensional chromatography involving the formation of aflatoxin B1-hemiacetal (aflatoxin B2a)
NB : The operations described below must be carried out following carefully the diagram in figure 3. 5.6.2.1. Application of the solutions
Score two straight lines on the plate (4.8) parallel to two contiguous sides (6 cm in from each side) to limit migration of the solvent fronts. Spot the following solutions on the plate using capillary pipettes or microsyringes: - on point A : a volume of purified extract of the sample, obtained in 5.3, containing about 2 75 nm of aflatoxin B1;
- on points B and C : 25 ¶l of the standard solution (3.16).
5.6.2.2. Development
Develop the chromatogram in direction I, in the dark, using the developing solvent (3.18.1) (1 cm layer in an unsaturated tank) until the solvent front reaches the solvent limit line.
Remove the plate from the tank and allow to dry in the dark at ambient temperature for five minutes. Then spray hydrochloric acid (3.12) along a band 2 75 cm high, covering points A and B (indicated by the hatched area in figure 3) until it darkens, protecting the rest of the plate with a glass sheet. Allow to react for 10 minutes in the dark and dry with a stream of air at ambient temperature.
Next, develop the chromatogram in direction II, in the dark, using the developing solvent (3.18.1) (1 cm layer in an unsaturated tank) until the solvent front reaches the solvent limit line. Remove the plate from the tank and allow to dry at ambient temperature.
5.6.2.3. Interpretation of the chromatogram Examine the chromatogram under UV light (4.9) and check for the following features. (a) Appearance of a blue fluorescent spot of aflatoxin B1 originating from the standard solution applied at C (migration in the direction I).
(b) Appearance of a blue fluorescent spot of unreacted (with the hydrochloric acid) aflatoxin B1 and a more intense blue fluorescent spot of aflatoxin B1-hemiacetal, both originating from the standard solution applied at B (migration in direction II).
(c) Appearance of spots matching those described in (b) ; originating from the sample extract applied at A. The position of these spots is defined first by the migration distance of the aflatoxin B1 from point A in direction I (the same as that travelled by the standard applied at C), and then by the migration distances from there in direction II of the aflatoxin B1-hemiacetal (same as those travelled by the standard applied at B). The fluorescence intensities of hemiacetal spots originating from the extract and from the standard applied at B should match.
6. Calculation of the results 6.1. From the visual measurements
The content in micrograms of aflatoxin B1 per kg of sample (ppb) is given by the formula: >PIC FILE= "T0009133">
in which:
Y and X are respectively the volumes in microlitres of the standard solution of aflatoxin B1 (3.16) and of the extract having a similar intensity of fluorescence;
S = concentration in micrograms of aflatoxin B1 per ml in the standard solution (3.16);
V = final volume of the extract in microlitres, allowing for any dilution that was necessary;
W = weight in grammes of the sample corresponding to the volume of extract subjected to column clean-up.
6.2. From the fluorodensitometric measurements
The content in micrograms of aflatoxin B1 per kg of sample is given by the formula: >PIC FILE= "T0009134">
in which:
Y = volume in microlitres of the extract spotted on the plate (10 or 20 ¶l);
S = quantity in nanograms of aflatoxin B1 in the extract spot (proportional to the value of Y taken), deduced from the measurements;
V = final volume of the extract in microlitres, allowing for any dilution that was necessary;
W = weight in grammes of the sample corresponding to the volume of extract subjected to column clean-up.
7. Preparation and testing of the standard solution (3.16) 7.1. Determination of the concentration of aflatoxin B1
Prepare a standard solution of aflatoxin B1 in the chloroform (3.2) or the benzene/acetonitrile mixture (3.6) with a concentration of 8 to 10 ¶g/ml. Determine the absorption spectrum between 330 and 370 nm with the aid of the spectrophotometer (4.11).
Measure the optical density (A) at 363 nm in the case of the chloroform solution ; or at 348 nm in the case of the solution in the benzene/acetonitrile mixture.
Calculate the concentration in micrograms of aflatoxin B1 per ml of solution from the formulae below: >PIC FILE= "T0009135">
Dilute as appropriate, away from day light, to obtain a working standard solution with a concentration of aflatoxin B1 of about 0 71 ¶g/ml. If kept in a refrigerator at 4 ºC, this solution is stable for two weeks.
7.2. Testing of chromatographic purity
Spot on a plate (4.8) 5 ¶l of the standard solution of aflatoxin B1 containing 8 to 10 ¶g/ml (7.1). Develop the chromatogram as indicated in 5.4. In UV light the chromatogram should show only one spot and no fluorescence must be perceptible in the original deposit zone.
8. Repeatability
The difference between the results of two parallel determinations carried out on the same sample by the same analyst should not exceed: - 25 % related to the highest result for contents of aflatoxin B1 from 10 and up to 20 ¶g/kg;
- 5 ¶g, in absolute value, for contents greater than 20 and up to 50 ¶g/kg;
- 10 % related to the highest value for contents above 50 ¶g/kg.
9. Reproducibility
See under "Observations", Part C, point 2.
>B TWO-DIMENSIONAL THIN LAYER CHROMATOGRAPHIC METHOD
1. Purpose and scope
The method makes it possible to determine the level of aflatoxin B1 in feedingstuffs not falling within the scope of method A. The lower limit of determination is 0 701 mg/kg (10 ppb). The method is not applicable to feedingstuffs containing citrus pulp.
2. Principle
The sample is subjected to extraction with chloroform. The extract is filtered, and an aliquot portion taken and purified by column chromatography on silica gel. The eluate is evaporated and the residue redissolved in a specific volume of chloroform or of a mixture of benzene and acetonitrile. An aliquot portion of this solution is subjected to two-dimensional thin-layer chromatography (TLC). The quantity of aflatoxin B1 is determined under UV irradiation of the chromatogram, either visually or by fluorodensitometry, by comparison with known quantities of standard aflatoxin B1. The identity of the aflatoxin B1 extracted from the feedingstuff must be confirmed by the procedure indicated.
3. Reagents
NB : All the reagents must be of "analytical reagent" quality unless otherwise stated. 3.1. Acetone.
3.2. Chloroform, stabilized with 0 75 to 1 % of 96 % ethanol (v/v).
3.3. N-hexane.
3.4. Methanol.
3.5. Anhydrous diethyl ether, free from peroxides.
3.6. Mixture of benzene and acetonitrile : 98/2 (v/v).
3.7. Mixture of chloroform (3.2) and methanol (3.4) : 97/3 (v/v).
3.8. Silica gel, for column chromatography, particle size 0 705 to 0 720 mm.
3.9. Absorbent cotton wool, previously defatted with chloroform, or glass wool.
3.10. Sodium sulphate, anhydrous, granular.
3.11. Inert gas, e.g. nitrogen.
3.12. 1 n-hydrochloric acid.
3.13. Kieselguhr, (hyflosupercel), washed in acid.
3.14. Silica gel G-HR or equivalent, for TLC.
3.15. Developing solvents. 3.15.1. Diethyl ether (3.5)/methanol (3.4)/water : 94/4.5/1.5 (v/v/v), saturated tank.
3.15.2. Chloroform (3.2)/acetone (3.1) : 9/1 (v/v), unsaturated tank.
3.16. Standard solution with about 0 71 ¶g aflatoxin B1 per ml in chloroform (3.2) or the benzene/acetonitrile mixture (3.6), prepared and checked as described in point 7 of method A.
4. Apparatus See under point 4 of method A.
>PIC FILE= "T0009136"> 5.4. Two dimensional TLC 5.4.1. Application of the solutions (follow the diagram in figure 1)
Score two straight lines on a plate (4.8) parallel to two contiguous sides (5 and 6 cm from each side respectively), to limit migration of the solvent fronts. Spot the following solutions on the plate using capillary pipettes or micro-syringes: - on point A, 20 ¶l of the purified sample extract obtained in 5.3;
- on point B, 20 ¶l of the standard solution (3.16);
- on point C, 10 ¶l of the standard solution (3.16);
- on point D, 20 ¶l of the standard solution (3.16);
- on point E, 40 ¶l of the standard solution (3.16).
Dry in a slow stream of air or inert gas (3.11). The spots obtained must have a diameter of about 5 mm.
5.4.2. Development (follow the diagram in figure 1)
Develop the chromatogram in direction I, in the dark, using the developing solvent (3.15.1) (1-cm layer in a saturated tank) until the solvent front reaches the solvent limit line. Remove the plate from the tank and allow to dry in the dark at ambient temperature for 15 minutes.
Then develop the chromatogram in direction II, in the dark, using the developing solvent (3.15.2) (1-cm layer in an unsaturated tank) until the solvent front reaches the solvent limit line. Remove the plate from the tank and allow to dry, in the dark, at ambient temperature.
5.4.3. Interpretation of the chromatogram (follow the diagram in figure 2)
Irradiate the chromatogram with UV light by placing the plate 10 cm from the (4.9). Locate the position of the blue fluorescent spots B, C, D and E of the aflatoxin B1 from the standard solution. Project two imaginary lines passing through these spots and at right angles to the development directions. The intersection P of these lines is the location in which to expect to find the aflatoxin B1 spot originating from the sample extract applied at A (figure 1). However, the actual location of the aflatoxin B1 spot may be at a point Q at the intersection of two imaginary straight lines forming an angle of about 100º between them and passing through spots B and C respectively. Determine the quantity of aflatoxin B1 in the sample extract as indicated in 5.5.
5.4.4. Supplementary chromatography
Score two straight lines on a new plate (4.8) parallel to two contiguous sides, as indicated in the diagram in figure 1, and apply on point A (see figure 1) 20 ¶l of the purified sample extract obtained in 5.3 and, superimposed on it, 20 ¶l of the standard solution (3.16). Develop as indicated in 5.4.2. Irradiate the chromatogram with UV light (4.9) and check that: - the aflatoxin B1 spots from the extract and the standard solution are superimposed,
- the fluorescence of this spot is more intense than that of the aflatoxin B1, spot developed at Q on the first plate.
5.5. Quantitative determinations
Determine either visually or by fluorodensitometry as indicated below. 5.5.1. Visual measurements
Determine the quantity of aflatoxin B1 in the extract by matching the fluorescence intensity of the extract spot with that of spots C, D and E of the standard solution. Interpolate if necessary. If the fluorescence intensity given by the 20 ¶l of extract is greater than that of the 40 ¶l of standard solution, dilute the extract 10 or 100 times with chloroform (3.2) or the benzene/acetonitrile mixture (3.6) before subjecting it again to thin-layer chromatography.
5.5.2. Measurements by fluorodensitometry
Measure the fluorescence intensity of the aflatoxin B1 spots with the fluorodensitometer (4.12), using an excitation wavelength of 365 nm and an emission wavelength of 443 nm.
Determine the quantity of aflatoxin B1 in the extract spot by comparison of its fluorescence intensity with that of spots C, D and E of the standard solution.
5.6. Confirmation of the identity of aflatoxin B1
See under point 5.6 of method A.
6. Calculation of the results
See under point 6 of method A.
7. Repeatability
See under point 8 of method A.
8. Reproducibility
See under "Observations", Part C, point 2.
C. OBSERVATIONS CONCERNING METHODS A AND B
1. Defatting
Samples containing more than 5 % fats must be defatted with light petroleum (bp 40 to 60 ºC) after the preparation indicated in 5.1.
In such cases, the analytical results must be expressed in terms of the weight of the non-defatted sample.
2. Reproducibility of the results
The reproducibility of the results, i.e. the variation between the results obtained by two or more laboratories on the same sample has been estimated at:
± 50 % of the mean value for mean values of aflatoxin B1 from 10 and up to 20 ¶g/kg;
± 10 ¶g/kg on the mean value for mean values greater than 20 and up to 50 ¶g/kg;
± 20 % of the mean value for mean values above 50 ¶g/kg.
Appendix to Annex
>PIC FILE= "T0009137"> >PIC FILE= "T0009138">
