Commission Regulation (EC) No 1240/2006
of 17 August 2006
amending Regulation (EC) No 902/2006 as regards the available quantity for which import licence applications for certain pigmeat products may be lodged for the period from 1 October to 31 December 2006
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2759/75 of 29 October 1975 on the common organisation of the market in pigmeat [1],
Having regard to Commission Regulation (EC) No 1458/2003 of 18 August 2003 opening and providing for the administration of tariff quotas for certain products in the pigmeat sector [2], and in particular Article 5(6) thereof,
Whereas:
(1) In accordance with the Agreement in the form of an Exchange of Letters between the European Community and the United States of America under Article XXIV:6 and Article XXVIII of the General Agreement on Tariffs and Trade (GATT) 1994 [3], approved by Council Decision 2006/333/EC [4], the quantities provided for by the quotas in Regulation (EC) No 1458/2003 have been amended.
(2) It is therefore necessary to amend Commission Regulation (EC) No 902/2006 of 19 June 2006 determining the extent to which applications lodged in June 2006 for import licences under the regime provided for by tariff quotas for certain products in the pigmeat sector for the period 1 July to 30 September 2006 can be accepted [5], and to adapt the available quantities for the period from 1 October to 31 December 2006 in proportion to those fixed in Annex I to Regulation (EC) No 1458/2003,
HAS ADOPTED THIS REGULATION:
Article 1
Annex II to Regulation (EC) No 902/2006 is replaced by the text in the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 17 August 2006.
For the Commission
Jean-Luc Demarty
Director-General for Agriculture and Rural Development
[1] OJ L 282, 1.11.1975, p. 1. Regulation as last amended by Regulation (EC) No 1913/2005 (OJ L 307, 25.11.2005, p. 2).
[2] OJ L 208, 19.8.2003, p. 3. Regulation as last amended by Regulation (EC) No 1191/2006 (OJ L 215, 5.8.2006, p. 3).
[3] OJ L 124, 11.5.2006, p. 15.
[4] OJ L 124, 11.5.2006, p. 13.
[5] OJ L 167, 20.6.2006, p. 24.
--------------------------------------------------
ANNEX
"
"ANNEX II
(tonnes) |
Order No | Total quantity available for the period from 1 October to 31 December 2006 |
09.4038 | 15286,248 |
09.4039 | 2250,000 |
09.4071 | 1501,000 |
09.4072 | 3080,500 |
09.4073 | 7533,500 |
09.4074 | 2658,200" |
"
--------------------------------------------------
