COMMISSION REGULATION (EC) No 1007/97 of 4 June 1997 amending Regulation (EC) No 1429/95 on implementing rules for export refunds on products processed from fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 2201/96 of 28 October 1996 on the common organization of the market in products processed from fruit and vegetables (1), and in particular Article 16 (8) thereof,
Whereas Commission Regulation (EC) No 1429/95 (2), as amended by Regulation (EC) No 341/96 (3), lays down implementing rules for export refunds on products processed from fruit and vegetables other than those granted for added sugars;
Whereas, in the light of experience gained in applying the arrangements, a number of changes should be made;
Whereas, at the same time, in the interests of uniformity, certain aspects of the arrangements should be aligned on those for export refunds on fruit and vegetables which are governed by Commission Regulation (EC) No 2190/96 (4), as last amended by Regulation (EC) No 610/97 (5);
Whereas provision should be made for including in licence applications and licences more than one agricultural product nomenclature code for export refunds provided these codes correspond to products in the same category;
Whereas account should be taken also, in the measures for the issue of licences, of quantities for which licences are in the process of being issued, that is to say, licences for which no specific measures have been taken by the Commission and which will be issued on the fifth working day following the day on which the application was lodged;
Whereas in order to avoid duplication with Article 49 of Commission Regulation (EEC) No 3665/87 of 27 November 1987 laying down common detailed rules for the application of the system of export refunds on agricultural products (6), as last amended by Regulation (EC) No 815/97 (7), notification of quantities for which refunds have been granted without a licence pursuant to the first paragraph of Article 2a of Regulation (EEC) No 3665/87 should be discontinued;
Whereas the meaning of date of issue of licences should be defined by reference to Commission Regulation (EEC) No 3719/88 laying down common detailed rules for the application of the system of import and export licences and advance fixing certificates for agricultural products (8), as last amended by Regulation (EC) No 495/97 (9);
Whereas, where a licence application is withdrawn after the licence has been issued, provision should be made for the cancellation of the licence;
Whereas in the interests of transparency and flexibility the automatic carry-over of unused quantities from one period to the next should be terminated;
Whereas, where the day set for notification to the Commission is a national holiday, provision should be made that it be brought forward to the preceding working day;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Products Processed from Fruit and Vegetables,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 1429/95 is hereby amended as follows:
1. The second subparagraph of Article 3 (2) is replaced by the following:
'However, more than one code may appear simultaneously in licence applications and licences provided the codes are those of products in the same category and the rate of refund is the same.`
2. Article 4 is amended as follows:
(a) In the first indent of paragraph 1 the words 'less the quantities for which licences with advance fixing of the refund have been issued` are replaced by 'less the quantities for which licences with advance fixing of the refund have been issued or are in the process of being issued`.
(b) The second and third indents of paragraph 1 are deleted.
(c) In paragraph 3 the words 'date of their issue` are replaced by 'date of their issue within the meaning of Article 21 (2) of Regulation (EEC) No 3719/88`.
(d) The following subparagraph is added to paragraph 4:
'Where a licence has been issued before the application is withdrawn it must be returned for cancellation to the responsible agency referred to in Article 2 at the same time as the notification of withdrawal of the licence application`.
3. Article 5 is deleted.
4. Article 6 is amended as follows:
(a) The second indent of the first paragraph is deleted.
(b) The following paragraph is added:
'Where the date set for notification is a public holiday, the Member State concerned shall notify the Commission on the working day preceding the national holiday`.
5. The Annex is replaced by the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Communities.
It shall apply to licences applied for from 24 June 1997.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 4 June 1997.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ No L 297, 21. 11. 1996, p. 29.
(2) OJ No L 141, 24. 6. 1995, p. 28.
(3) OJ No L 48, 27. 2. 1996, p. 8.
(4) OJ No L 292, 15. 11. 1996, p. 12.
(5) OJ No L 93, 8. 4. 1997, p. 16.
(6) OJ No L 351, 14. 12. 1987, p. 1.
(7) OJ No L 116, 6. 5. 1997, p. 22.
(8) OJ No L 331, 2. 12. 1988, p. 1.
(9) OJ No L 77, 19. 3. 1997, p. 12.
ANNEX
>START OF GRAPHIC>
FORM FOR NOTIFICATION OF INFORMATION AS PROVIDED FOR IN ARTICLE 6 OF REGULATION (EC) No 1429/95
>END OF GRAPHIC>
