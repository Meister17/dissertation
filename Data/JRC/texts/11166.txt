Commission Decision
of 7 June 2006
amending Decisions 2005/710/EC, 2005/734/EC, 2005/758/EC, 2005/759/EC, 2005/760/EC, 2006/247/EC and 2006/265/EC as regards certain protection measures in relation to highly pathogenic avian influenza
(notified under document number C(2006) 2177)
(Text with EEA relevance)
(2006/405/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 90/425/EEC of 26 June 1990 concerning veterinary and zootechnical checks applicable in intra-Community trade in certain live animals and products with a view to the completion of the internal market [1], and in particular Article 10(4) thereof,
Having regard to Council Directive 91/496/EEC of 15 July 1991 laying down the principles governing the organisation of veterinary checks on animals entering the Community from third countries and amending Directives 89/662/EEC, 90/425/EEC and 90/675/EEC [2], and in particular Article 18(7) thereof,
Having regard to Council Directive 97/78/EC of 18 December 1997 laying down the principles governing the organisation of veterinary checks on products entering the Community from third countries [3], and in particular Article 22(6) thereof,
Having regard to Regulation (EC) No 998/2003 of the European Parliament and of the Council of 26 May 2003 on the animal health requirements applicable to the non-commercial movement of pet animals and amending Council Directive 92/65/EEC [4], and in particular Article 18 thereof,
Whereas:
(1) Following the outbreak of avian influenza, caused by a highly pathogenic H5N1 virus strain, in south-eastern Asia starting in December 2003, the Commission adopted several protection measures in relation to that disease.
(2) Commission Decision 2005/710/EC of 13 October 2005 concerning certain protection measures in relation to highly pathogenic avian influenza in Romania [5] provides that Member States are to suspend imports of live poultry, ratites and farmed and wild feathered game and hatching eggs of those species from the whole territory of Romania and of certain products from birds from parts of that territory.
(3) Commission Decision 2005/734/EC of 19 October 2005 laying down biosecurity measures to reduce the risk of transmission of highly pathogenic avian influenza caused by influenza virus A subtype H5N1 from birds living in the wild to poultry and other captive birds and providing for an early detection system in areas at particular risk [6] provides that Member States shall take appropriate and practical measures to reduce the risk of transmission of that disease from birds living in the wild to poultry and other captive birds, taking into account certain criteria and risk factors.
(4) Commission Decision 2005/758/EC of 27 October 2005 concerning certain protection measures in relation to a suspicion of highly pathogenic avian influenza in Croatia and repealing Decision 2005/749/EC [7] provides that Member States are to suspend imports of live poultry, ratites, farmed and wild feathered game, certain live birds other than poultry, including pet birds, and hatching eggs of those species as well as certain products from birds, from parts of the territory of Croatia.
(5) Commission Decision 2005/759/EC of 27 October 2005 concerning certain protection measures in relation to highly pathogenic avian influenza in certain third countries and the movement from third countries of birds accompanying their owners [8] and Commission Decision 2005/760/EC of 27 October 2005 concerning certain protection measures in relation to highly pathogenic avian influenza in certain third countries for the import of captive birds [9] lay down safeguard measures in relation to imports into the Community of birds other than poultry, including the movement of pet birds.
(6) Commission Decision 2006/247/EC of 27 March 2006 concerning certain protection measures regarding imports from Bulgaria in relation to highly pathogenic avian influenza in that third country [10] provides that Member States are to suspend imports of live poultry, ratites and farmed and wild feathered game and hatching eggs of those species from the whole territory of Bulgaria and of certain products from birds from parts of that territory.
(7) Commission Decision 2006/265/EC of 31 March 2006 concerning certain protection measures in relation to a suspicion of highly pathogenic avian influenza in Switzerland [11] provides that Member States are to suspend imports of live poultry, ratites, farmed and wild feathered game, live birds other than poultry, including certain pet birds, and hatching eggs of those species and of certain products of birds from all areas of the territory of Switzerland for which the authorities of that third country have applied equivalent restrictions to those laid down in Commission Decisions 2006/115/EC [12] and 2006/135/EC [13].
(8) The threat posed to the Community by the Asian strain of the avian influenza virus has not abated. Outbreaks are still detected in wild birds in the Community and in wild birds and poultry in several third countries, including member countries of the World Organisation for Animal Health (OIE). In addition, that virus appears to become more and more endemic in certain parts of the world. The validity of the protection measures laid down in Decisions 2005/710/EC, 2005/734/EC, 2005/759/EC, 2005/760/EC, 2006/247/EC and 2006/265/EC should therefore be extended.
(9) Information sent to the Commission by Romania and Bulgaria and the surveillance undertaken in those third countries makes it clear that they have controlled the disease on their territory and also ensured that the virus has not spread to those areas which to date have been free of the disease. Accordingly, it is appropriate to limit the suspension of the imports provided for in Decisions 2005/710/EC and 2006/247/EC to those parts of Romania and Bulgaria that have been affected by the virus and are at risk.
(10) Croatia has reported further cases of the virus in wild birds outside the area currently regionalised in Decision 2005/758/EC. Accordingly, it is necessary to extend the suspension of certain imports from Croatia as laid down in that Decision to cover the newly affected part of the territory of that third country.
(11) Decisions 2005/710/EC, 2005/734/EC, 2005/759/EC, 2005/760/EC, 2006/247/EC and 2006/265/EC expired on 31 May 2006. However, in the interests of animal health and in view of the existing epidemiological situation, it is necessary to ensure the continuity of the protection measures provided for in those Decisions. Those measures should therefore continue to apply without interruption. Accordingly, the provisions in this Decision concerning the dates of application of those six Decisions should have retroactive effect.
(12) Decisions 2005/710/EC, 2005/734/EC, 2005/758/EC, 2005/759/EC, 2005/760/EC, 2006/247/EC and 2006/265/EC should therefore be amended accordingly.
(13) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
Decision 2005/710/EC is amended as follows:
1. Paragraph 1(a) of Article 1 is replaced by the following:
"Article 1
1. Member States shall suspend the importation of:
(a) live poultry, ratites, farmed and wild feathered game, and hatching eggs of these species coming from the part of the territory of Romania referred to in Part B of the Annex;"
2. In Article 4, the date " 31 July 2006" is replaced by " 31 December 2006".
Article 2
In Article 4 of Decision 2005/734/EC, the date " 31 May 2006" is replaced by " 31 December 2006".
Article 3
The Annex to Decision 2005/758/EC is replaced by the text in the Annex to this Decision.
Article 4
In Article 5 of Decision 2005/759/EC, the date " 31 May 2006" is replaced by " 31 July 2006".
Article 5
In Article 6 of Decision 2005/760/EC, the date " 31 May 2006" is replaced by " 31 July 2006".
Article 6
Decision 2006/247/EC is amended as follows:
1. Article 1(a) is replaced by the following:
"Article 1
Member States shall suspend imports of:
(a) live poultry, ratites and farmed and wild feathered game, and hatching eggs of these species coming from the part of the territory of Bulgaria referred to in Part B of the Annex;"
2. In Article 5, the date " 31 May 2006" is replaced by " 31 December 2006".
Article 7
In Article 3 of Decision 2006/265/EC, the date " 31 May 2006" is replaced by " 31 December 2006".
Article 8
The Member States shall immediately take the necessary measures to comply with this Decision and publish those measures. They shall immediately inform the Commission thereof.
Article 9
Articles 2, 4, 5, Article 6(2) and Article 7 shall apply from 1 June 2006.
Article 10
This Decision is addressed to the Member States.
Done at Brussels, 7 June 2006.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 224, 18.8.1990, p. 29. Directive as last amended by Directive 2002/33/EC of the European Parliament and of the Council (OJ L 315, 19.11.2002, p. 14).
[2] OJ L 268, 24.9.1991, p. 56. Directive as last amended by the 2003 Act of Accession.
[3] OJ L 24, 30.1.1998, p. 9. Directive as last amended by Regulation (EC) No 882/2004 of the European Parliament and of the Council (OJ L 165, 30.4.2004, p. 1, as corrected by OJ L 191, 28.5.2004, p. 1).
[4] OJ L 146, 13.6.2003, p. 1. Regulation as last amended by Commission Regulation (EC) No 590/2006 (OJ L 104, 13.4.2006, p. 8).
[5] OJ L 269, 14.10.2005, p. 42. Decision as last amended by Decision 2006/321/EC (OJ L 118, 3.5.2006, p. 18).
[6] OJ L 274, 20.10.2005, p. 105. Decision as last amended by Decision 2005/855/EC (OJ L 316, 2.12.2005, p. 21).
[7] OJ L 285, 28.10.2005, p. 50. Decision as last amended by Decision 2006/321/EC.
[8] OJ L 285, 28.10.2005, p. 52. Decision as last amended by Decision 2006/79/EC (OJ L 36, 8.2.2006, p. 48).
[9] OJ L 285, 28.10.2005, p. 60. Decision as last amended by Decision 2006/79/EC.
[10] OJ L 89, 28.3.2006, p. 52.
[11] OJ L 95, 4.4.2006, p. 9.
[12] OJ L 48, 18.2.2006, p. 28. Decision as last amended by Decision 2006/277/EC (OJ L 103, 12.4.2006, p. 29).
[13] OJ L 52, 23.2.2006, p. 41, Decision as last amended by Decision 2006/293/EC (OJ L 107, 20.4.2006, p. 44).
--------------------------------------------------
ANNEX
"ANNEX
Part of the territory of Croatia referred to in Article 1(1)
ISO country code | Name of country | Part of territory |
HR | Croatia | In Croatia the counties of: Viroviticko-PodravskaOsjecko-BaranjskaSplitsko-DalmatinskaZagreb" |
--------------------------------------------------
