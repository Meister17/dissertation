Decision of the EEA Joint Committee No 52/2005
of 29 April 2005
amending Annex I (Veterinary and phytosanitary matters) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex I to the Agreement was amended by Decision of the EEA Joint Committee No 29/2005 of 11 March 2005 [1].
(2) Commission Regulation (EC) No 1800/2004 of 15 October 2004 concerning the authorisation for 10 years of the additive Cycostat 66G in feedingstuffs, belonging to the group of coccidiostats and other medicinal substances [2] is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
The following point shall be inserted after point 1zzc (Commission Regulation (EC) No 1464/2004) of Chapter II of Annex I to the Agreement:
"1zzd. 32004 R 1800: Commission Regulation (EC) No 1800/2004 of 15 October 2004 concerning the authorisation for 10 years of the additive Cycostat 66G in feedingstuffs, belonging to the group of coccidiostats and other medicinal substances (OJ L 317, 16.10.2004, p. 37)."
Article 2
The texts of Regulation (EC) No 1800/2004 in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 30 April 2005, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [3].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 29 April 2005.
For the EEA Joint Committee
The president
Richard Wright
[1] OJ L 198, 28.7.2005, p. 15.
[2] OJ L 317, 16.10.2004, p. 37.
[3] No constitutional requirements indicated.
--------------------------------------------------
