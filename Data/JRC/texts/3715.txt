Commission Decision
of 29 December 2004
concerning the common position of the Community on a Decision of the Joint Committee on Agriculture amending point B(9) of Appendix 1 to Annex 7 to the Agreement between the European Community and the Swiss Confederation on trade in agricultural products
(2005/9/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Decision 2002/309/EC, Euratom of the Council, and of the Commission as regards the Agreement on Scientific and Technological Cooperation, of 4 April 2002 on the conclusion of seven Agreements with the Swiss Confederation [1], most notably the Agreement on trade in agricultural products [2], and in particular the fourth indent of Article 5(2) thereof,
Whereas:
(1) The Agreement between the European Community and the Swiss Confederation on trade in agricultural products (hereinafter referred to as the Agriculture Agreement) entered into force on 1 June 2002.
(2) Article 6 of the Agriculture Agreement sets up a Joint Committee on Agriculture to be responsible for the administration of the Agreement and its proper functioning.
(3) In accordance with Article 6(4) and (7) of the Agriculture Agreement, on 21 October 2003 the Joint Committee on Agriculture (hereinafter referred to as the Joint Committee) adopted its Rules of Procedure [3] and set up the working groups required to manage the Annexes to the Agriculture Agreement [4]. The Working Group on "Wine-Sector Products" met to examine questions relating to Annex 7 and its implementation, in accordance with Article 27(1) of Annex 7 to the Agreement, and to put forward proposals to the Joint Committee with a view to adapting the Appendices to Annex 7, in accordance with Article 27(2) of Annex 7 to the Agreement. Article 11 of the Agreement provides that the Joint Committee may decide to amend Annexes 1 and 2 and the Appendices to the other Annexes to the Agreement.
(4) The Community must decide on the position to be adopted by the Commission in the Joint Committee as regards the amendments to the Appendices.
(5) The Commission is competent to adopt the common position on the amendments to the Appendices to Annex 7.
(6) The measures provided for in this Decision are in accordance with the opinion of the Management Committee for Wine,
DECIDES:
Article 1
The Community position to be adopted by the Commission in the Joint Committee on Agriculture set up by Article 6 of the Agreement between the European Community and the Swiss Confederation on trade in agricultural products shall be based on the draft Decision of the Joint Committee, which is annexed to this Decision.
Article 2
The Head of the Community Delegation within the Joint Committee on Agriculture is hereby authorised to adopt the Decision on behalf of the Community. For matters falling within the competence of the Directorate-General for Agriculture, this authorisation shall also apply to the Unit Head responsible for bilateral relations with Switzerland.
Done at Brussels, 29 December 2004.
For the Commission
Mariann Fischer Boel
Member of the Commission
--------------------------------------------------
[1] OJ L 114, 30.4.2002, p. 1.
[2] OJ L 114, 30.4.2002, p. 132. Agreement as last amended by Decision No 1/2004 of the Joint Veterinary Committee set up under the Agreement between the European Community and the Swiss Confederation on trade in agricultural products (OJ L 160, 30.4.2004, p. 116, corrigendum in OJ L 212, 12.6.2004, p. 72).
[3] Common position adopted by the Council on 21 July 2003; Joint Committee Decision No 1/2003 of 21 October 2003 concerning the adoption of its Rules of Procedure (OJ L 303, 21.11.2003, p. 24).
[4] Common position adopted by the Council on 21 July 2003; Joint Committee Decision No 2/2003 of 21 October 2003 concerning the setting-up of the working groups and the adoption of the terms of reference of those groups (OJ L 303, 21.11.2003, p. 27).
--------------------------------------------------
+++++ ANNEX 1 +++++</br>
