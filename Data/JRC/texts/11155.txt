European Network and Information Security Agency— Publication of the final accounts for the financial year 2005
(2006/C 266/06)
The complete version of the final accounts may be found at the following address:
www.enisa.europa.eu
Source: Agency data — These tables summarise the data supplied by the Agency in its annual accounts.
Table 1
Implementation of the budget for the financial year 2005
(1000 euro) |
Revenue | Expenditure |
Source of revenue | Revenue entered in the final budget for the financial year | Revenue received | Allocation of expenditure | Final budget appropriations |
entered | committed | paid | carried over | cancelled |
Community subsidies | 6346 | 4400 | Title I Staff | 2375 | 1764 | 1490 | 257 | 628 |
| | | Title II Administration | 2483 | 1772 | 453 | 1065 | 965 |
| | | Title III Operating activities | 1488 | 1004 | 196 | 790 | 502 |
Total | 6346 | 4400 | Total | 6346 | 4540 | 2139 | 2112 | 2095 |
Table 2
Revenue and expenditure account for the financial year 2005
(1000 euro) |
| 2005 |
Operating revenue
Community subsidies | 4251 |
Total (a) | 4251 |
Operating expenditure
Staff | 1040 |
Administration | 1594 |
Operating activities | 517 |
Total (b) | 3151 |
Operating result (c = a - b) | 1100 |
Table 3
Balance sheet at 31 December 2005
(1000 euro) |
| 2005 |
Assets
Fixed assets | 344 |
Receivables | 13 |
Cash and cash equivalents | 2510 |
Total | 2867 |
Liabilities
Accumulated surplus/deficit | 0 |
Economic result for the year | 1100 |
Current liabilities | 1767 |
Total | 2867 |
--------------------------------------------------
