Council Decision
of 13 June 2005
appointing an Italian alternate member of the Committee of the Regions
(2005/461/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 263 thereof,
Having regard to the proposal from the Italian Government,
Whereas:
(1) On 22 January 2002 the Council adopted Decision 2002/60/EC appointing the members and alternate members of the Committee of the Regions for the period 26 January 2002 to 25 January 2006 [1].
(2) A seat as an alternate member of the Committee of the Regions has become vacant following the resignation of Mr Lorenzo DELLAI, notified to the Council on 16 March 2005,
HAS DECIDED AS FOLLOWS:
Article 1
Mr Mario MAGNANI, "consigliere provinciale della Provincia autonoma di Trento" (Provincial Councillor from the Autonomous Province of Trento), is appointed an alternate member of the Committee of the Regions for the remainder of the term of office, which runs until 25 January 2006.
Article 2
This Decision shall be published in the Official Journal of the European Union.
It shall take effect on the date of its adoption.
Done at Luxembourg, 13 June 2005.
For the Council
The President
J. Asselborn
[1] OJ L 24, 26.1.2002, p. 38.
--------------------------------------------------
