Action brought on 8 February 2006 — Commission of the European Communities v Hellenic Republic
Parties
Applicant: Commission of the European Communities (represented by: D. Triantafillou, acting as Agent)
Defendant: Hellenic Republic
Form of order sought
The applicant asks the Court:
- to declare that, by applying only a single criterion, for the purpose of determining the taxable value of used cars imported into Greek territory from another Member State, as regards depreciation, based solely on the age of the vehicle — in accordance with which a reduction of 7 % is allowed for cars from six months to one year old or 14 % for cars over one year old — which does not ensure that the tax due does not exceed, even in certain cases, the amount of the residual tax which has been incorporated in the value of similar used vehicles which have already been registered in that State, while the basis for calculating depreciation is not made known to the public and examination of the cars by experts is subject to the payment of a fee of EUR 300, the Hellenic Republic has failed to fulfil its obligations under Article 90 of the EC Treaty;
- order the Hellenic Republic to pay the costs.
Pleas in law and main arguments
1. The fixed scale of depreciation applied by the Hellenic Republic to imported used cars does not reflect, with the precision required by the case-law, their actual depreciation and, consequently, it does not ensure that the registration tax due does not exceed, even in certain cases, the amount of the residual tax incorporated in the value of similar used cars which have already been registered in Greece.
2. The procedure before the commission hearing objections is not sufficient to remedy the defects in that basic system, it requires a deterrent payment of a significant fee and is not accompanied by publication of the criteria to be taken into account when determining the value of used cars, rendering that procedure ineffective.
--------------------------------------------------
