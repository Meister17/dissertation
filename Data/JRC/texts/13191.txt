Commission Decision
of 10 February 2006
amending Decision 98/536/EC establishing the list of national reference laboratories for the detection of residues
(notified under document number C(2006) 330)
(Text with EEA relevance)
(2006/130/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 96/23/EC of 29 April 1996 on measures to monitor certain substances and residues thereof in live animals and animal products and repealing Directives 85/358/EEC and 86/469/EEC and Decisions 89/187/EEC and 91/664/EEC [1], and in particular Article 14(1) thereof,
Whereas:
(1) The Annex to Commission Decision 98/536/EC [2] was to have been reviewed by 31 December 2000. Member States have reorganised their laboratories in order to fulfil the requirements of Directive 96/23/EC taking into account in particular the requirement that one residue or residue group is to be assigned to one national reference laboratory (NRL) only.
(2) This reorganisation having been concluded, the list of NRLs in the Annex to Decision 98/536/EC should now be adapted accordingly. At the same time the list of NRLs of the new Member States should be adapted in the light of information received from them.
(3) Decision 98/536/EC should therefore be amended accordingly.
(4) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
The Annex to Decision 98/536/EC is replaced by the text in the Annex to this Decision.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 10 February 2006.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 125, 23.5.1996, p. 10.
[2] OJ L 251, 11.9.1998, p. 39.
--------------------------------------------------
ANNEX
The Annex to Decision 98/536/EC is replaced by the following:
"ANNEX
NATIONAL REFERENCE LABORATORIES
Member State | Reference laboratories | Groups of residues |
Austria | Österreichische Agentur für Gesundheit und Ernährungssicherheit GmbH — CC Tierarzneimittel und Hormone, Wien Spargelfeldstraße 191 1226 Wien | A1, A2, A3, A4, A5, A6, B1, B2a, B2b, B2d, B2e, B2f |
Österreichische Agentur für Gesundheit und Ernährungssicherheit GmbH — CC Rückstandsanalytik, Wien Spargelfeldstraße 191 1226 Wien | B3a (PCBs, excluding dioxins), B3b, B3d |
Österreichische Agentur für Gesundheit und Ernährungssicherheit GmbH — CC Pflanzenschutzmittelrückstände, Innsbruck Technikerstraße 70 6020 Innsbruck | B2c |
Österreichische Agentur für Gesundheit und Ernährungssicherheit GmbH — CC Elemente, Wien Spargelfeldstraße 191 1226 Wien | B3c |
Austrian Research Centres GmbH — ARC 2444 Seibersdorf | B3a (dioxins) |
Lebensmitteluntersuchungsanstalt der Stadt Wien Henneberggasse 3 1030 Wien | B3e |
Belgium | Institut scientifique de la santé publique Rue J. Wytsman 14 1050 Bruxelles Wetenschappelijk Instituut Volksgezondheid J. Wytsmanstraat 14 1050 Brussel | All groups |
Denmark | Danmarks Fødevareforskning (DFVF) Mørkhøj Bygade 19 DK-2860 Søborg | All groups |
Finland | Eläinlääkintä- ja elintarviketutkimuslaitos, EELA Hämeentie 57 Box 45 00581 Helsinki | All groups |
France | LABERCA (Laboratoire d'Etude des Résidus et Contaminants dans les Aliments) Ecole Nationale Vétérinaire de Nantes Route de Gachet — BP 50707 44307 Nantes cedex 03 | A1 to A5, B2f (glucocorticoides), B3f |
AFSSA-Fougères (Laboratoire d'Etudes et de Recherches sur les Médicaments Vétérinaires et les Désinfectants) La Haute Marche 35133 Javene | A6, B1, B2a, B2b, B2d, B2e, B2f (excluding glucocorticoides), B3e |
AFSSA-Maisons-Alfort (Laboratoire d'Etudes et de Recherches sur la Qualité des Aliments et les Procédés agro-alimentaires) 23 avenue du Général de Gaulle 94706 Maisons-Alfort Cedex | B2c, B3a, B3b, B3c, B3d |
Germany | Bundesamt für Verbraucherschutz und Lebensmittelsicherheit Diedersdorfer Weg 1 12277 Berlin | All groups |
Greece | Ινστιτούτο Υγιεινής Τροφίμων Αθηνών Institute of Food Hygiene of Athens Neapoleos 25, Athens 153 10, Aghia Paraskevi Νεαπόλεως 25 15310 Αγ. Παρασκευή, Αθήνα | A2, A5, B1, B2d, B3a (PCB), B3b, B3c, B3e |
Ινστιτούτο Βιοχημείας, Τοξικολογίας και Διατροφής των Ζώων Institute of Biochemistry, Toxicology and Feed Neapoleos 25 153 10 Aghia Paraskevi, Athens Νεαπόλεως 25 15310 Αγ. Παρασκευή, Αθήνα | B3d |
Κτηνιατρικό Εργαστήριο Χανίων Veterinary Laboratory of Chania M. Botsari 66 73100 Chania Μ. Μπότσαρη 66 73100 Χανιά | B1 in honey |
Κτηνιατρικό Εργαστήριο Σερρών Veterinary Diagnostic Laboratory of Serres Terma Omonias 621 10 Serres Τέρμα Ομονοίας 621 10 Σέρρες | A1, A3, A4, B2f, B3a (excluding PCBs) |
Κτηνιατρικό Εργαστήριο Λάρισας Veterinary Diagnostic Laboratory Larissa 7th km N.R. of Larissa 411 10 Larissa 7ο χλμ. Εθνικής οδού Λαρίσης-Τρικάλων, 411 10 Λάρισα | A6 (nitroimidazoles), B2a, B2b |
Κτηνιατρικό Εργαστήριο Τρίπολης Veterinary Diagnostic Laboratory Tripolis Pelagos Arkadias 221 00 Tripolis Πέλαγος Αρκαδίας 22100 Τρίπολη | A6 (cloramphenicol and nitrofurans), B2c |
Κτηνιατρικό Εργαστήριο Πατρών Veterinary Diagnostic Laboratory of Patras Notara 15 264 42 Patra Νοταρά 15 264 42 Πάτρα | B2e |
Ireland | State Laboratory Young’s Cross Celbridge Co. Kildare | A1, A3, A4, A6 (nitromidazoles only), B2e, B2f (dexamethasone only), B3d |
Central Meat Control Laboratory Young’s Cross Celbridge Co. Kildare | A2, A5, A6, (exc. nitrofurans, nitromidazoles), B1, B2d, B2f (Carbadox only), B3c |
Ashtown Food Research Centre, Teagasc Ashtown Dublin 15 | A6 (only nitrofurans), B2a (anthelmintics except emamectin), B2b (anticoccidials), B2c |
Marine Institute Fisheries Research Centre Abbotstown, Dublin 15 | B2a, (only emamectin), B2f (only teflubenzuron and diflubenzuron), B3e (only MG + LMG) |
Pesticide Control Laboratory Young’s Cross Celbridge Co. Kildare | B3a (organochlorine pesticides and 7 PCBs only), B3b |
Italy | Istituto Superiore di Sanità Dipartimento di Sanità Alimentare e Animale Viale Regina Elena 299 00161 Roma | All groups |
Luxembourg | Institut scientifique de la Santé publique Rue J. Wytsman 14 1050 Bruxelles | All groups |
Portugal | Laboratório Nacional de Investigação Veterinária Estrada de Benfica 701 549-011 Lisboa | All groups |
Instituto Nacional de Investigação Agrária e das Pescas/Instituto de Investigação das Pescas e do Mar Av. de Brasília 1449-006 Lisboa | B3c aquaculture |
Spain | Centro Nacional de Alimentación (Agencia Española de Seguridad Alimentaria) Carretera Pozuelo-Majadahonda Km 6,2 Majadahonda Madrid | A1, A3, A4, A5, A6 (chloramphenicol and nitrofurans), B2f (corticosteroids), B3c (only aquaculture), B3d, B3e |
Laboratorio Central de Sanidad y producción Animal de Santa Fe (Ministerio de Agricultura, Pesca y Alimentación) Camino del Jau, s/n 18.18320 Santa Fe, Granada | A2, A6 (nitroimidazoles), B2a B2b, B2c, B2d, B2e, B2f (excluding corticosteroides) |
Grupo Arbitral Agroalimentario (Ministerio de Agricultura, Pesca y Alimentación) Carretera de La Coruña, Km 10.700 28023 Madrid | B3a, B3b, B3c (excluding aquaculture) |
Laboratorios anteriormente mencionados según la acción farmacológica | B3f |
Sweden | Statens Livsmedelsverk, Box 622 751 26 Uppsala | All groups |
The Netherlands | Rijkskinstituut voor Volksgezondheid en Milieu (RIVM) Antoine van Leeuwenhoeklaan 9 Bilthoven 3721 MA | A1, A2, A3, A4, A5, A6 (chlorpromazyne, colchicine, somatotropines, chloramphenicol), B2d, B3c, B3d, B3e |
Rijkskwaliteitsinstituut voor land-en tuinbouwproducten (RIKILT) Institute of food safety Bornsesteeg 45 Wageningen 6708 PD | A6 (nitrofurans, dapsone, nitroimidazoles, chloroform), B1, B2a, B2b, B2c, B2e, B3a, B3b, B3f |
United Kingdom | Central Science Laboratory Sand Hutton York YO41 1LZ | A6 (chloramphenicol, nitrofurans honey, dapsone). B1, B2a, B2b (ionophores) |
LGC Queens Road Teddington Middlesex TW11 OLY | A6 (chlorpromazine), B2c, B2d, B3a, B3b, B3c, B3d, B3e |
Veterinary Science Division Stoney Road Stormont Belfast BT4 3SD | A1, A2, A3, A4, A5, A6 (nitrofurans exc. honey, nitroimidazoles), B2b, (nicarbazin), B2f |
Czech Republic | Národní referenční laboratoř pro sledování reziduí veterinárních léčiv Ústav pro státní kontrolu veterinárních biopreparátů a léčiv Brno Hudcova 56 A CZ-621 00 Brno | All A |
Národní referenční laboratoř pro rezidua pesticidů a PCB Státní veterinární ústav Praha Sídlištní 136/24 CZ-165 03 Praha | B3a, B3b |
Národní referenční laboratoř pro chemické prvky Státní veterinární ústav Olomouc, laboratoř Kroměříž Hulínská 2286 CZ-767 60 Kroměříž | B3c |
Národní referenční laboratoř pro mykotoxiny a další přírodní toxiny, barviva, antibakteriální inhibiční látky a rezidua veterinárních léčiv Státní veterinární ústav Jihlava Rantířovská 93 CZ-586 05 Jihlava | B1, B2, B3d, B3e |
Cyprus | Γενικό Χημείο του Κράτους Υπουργείο Υγείας Οδός Κίμωνος 44, 1451, Λευκωσία, Κύπρος General State Laboratory Ministry of Health Kimonos Street 44 1451 Nicosia | All groups |
Hungary | Orzságos Élelmiszervizsgáló Intézet Budapest, Mester u. 81. Hungary, H-1095 Budapest 94 POB 1740 H-1465 | All groups |
Estonia | Veterinaar- ja Toidulaboratoorium Tallinna osakond Väike-Paala 3 Tallinn 11415 | A1, A2, A3, A4, A5, A6, B1 |
Veterinaar- ja Toidulaboratoorium Tartu osakond Kreutzwaldi 30 Tartu 51006 | B3c |
Tervisekaitseinspektsiooni Tartu laboratoorium Põllu 1A Tartu 50303 | B2c, B3a, B3b |
Põllumajandusuuringute Keskus Teaduse 4/6 Saku Harjumaa 75501 | B3d |
Latvia | Valsts veterinārmedicīnas diagnostikas centrs Lejupes iela 3, LV-1076 Rīga | All groups (excluding B3d aquaculture) |
Lithuania | Nacionalinė veterinarijos laboratorija J. Kairiūkščio g. LT-08409 Vilnius | All groups |
Malta | Laboratorju Veterinarju Nazzjonali Dipartiment ta’ l-Ikel Alimentari u Djanjostika Taqsima ta’ l-Ikel u Attivita’ Veterinarja Ministeru għall-Affarijiet Rurali u l-Ambjent National Veterinary Laboratory Department of Food Health and Diagnostics Food and Veterinary Regulation Division Ministry for Rural Affairs and the Environment Albertown Marsa | All groups |
Poland | Państwowy Instytut Weterynaryjny-Państwowy Instytut Badawczy w Puławach Al. Partyzantów 57 24-100 Puławy | All groups |
Slovak Republic | Štátny veterinárny a potravinový ústav Nitra Akademická 3 Nitra 949 01 | A1, A3, A4, A5 |
Štátny veterinárny a potravinový ústav Košice Hlinkova 1B Košice 040 01 | A2, B2a, B2b, B2d, B3c, B3d |
Štátny veterinárny a potravinový ústav Dolný Kubín Jánoskova 1611/58 Dolný Kubín 026 01 | A6 (chloramphenicol, nitrofurans), B1, B2f, B3e |
Štátny veterinárny a potravinový ústav Bratislava Botanická 15 Bratislava 842 13 | A6 (nitroimidazoles), B2c, B2e, B3a, B3b |
Slovenia | Univerza v Ljubljani, Veterinarska fakulteta, Nacionalni veterinarski inštitut Gerbičeva 60 1000 Ljubljana | A6 — (chloramphenicol in milk, eggs, meat, water) B1, B2a (avermectins), B2b (lasalocid, salynomicin, narasin, monesin) B2d, B3c (excluding Hg in fish), B3d, B3e |
Univerza v Ljubljani, Veterinarska fakulteta, Nacionalni veterinarski inštitut Gerbičeva 60 1000 Ljubljana | A1, A3, A4, A5, A6 (chloramphenicol in urine, honey and feed, nitrofurans, dapson, chlopromazin, metronidazol, ronidazol, dimetridazol), B2b — (amprolium, maduramycin, methylchlopindol, nicarbazin, robenidin), B2e — (phenylbutazon), NSAID, B2f |
Zavod za zdravstveno varstvo Maribor Prvomajska 1 2000 Maribor | A2, (colchicin, chloroform), B2a (levamisol, thiabendazol, febantel, oxfendazol, fenbendazol), B2c, B2e (diclofenac, carprofen), |
Zavod za zdravstveno varstvo Nova Gorica Vipavska cesta 13 Rožna Dolina 5000 Nova Gorica | B3a, B3b, B2f (only amitraz in honey), B3b (only organophosphorus compounds in honey) |
Inštitut za varovanje zdravja Republike Slovenije Grablovičeva 44 1000 Ljubljana | B3c (only mercury in fish)" |
--------------------------------------------------
