Order of the Civil Service Tribunal of 21 March 2006 — Marenco
v Commission
(Case F-96/05) [1]
(2006/C 108/65)
Language of the case: French
The President of the First Chamber has ordered that the case be removed from the register.
[1] OJ C 10 of 14.1.2006.
--------------------------------------------------
