Information procedure — Technical rules
(2006/C 18/06)
(Text with EEA relevance)
Directive 98/34/EC of the European Parliament and of the Council of 22 June 1998 laying down a procedure for the provision of information in the field of technical standards and regulations and of rules on Information Society services. (OJ L 204, 21.7.1998, p. 37; OJ L 217, 5.8.1998, p. 18).
Notifications of draft national technical rules received by the Commission
Reference | Title | End of three-month standstill period |
2005/0690/D | BNetzA SSB FE-OE 017 Interface description for radio relay equipment in the fixed radio service in the 58 GHz band | 9.3.2006 |
2005/0691/D | BnetzA SSB FE-OE 019 Interface description for point-to-point digital radio relay equipment in the fixed radio service in the 52 GHz band | 9.3.2006 |
2005/0692/I | Draft Legislative Decree amending and supplementing Legislative Decree No 82 of 7 March 2005 on the "Digital Administration Code" | 10.3.2006 |
2005/0693/S | Draft amending the National Board of Housing, Building and Planning's Construction Regulations [Swedish designation: BKR], (BFS 1993:58 as amended up to BFS 2004:9) | 10.3.2006 |
2005/0694/DK | Danish Radio Interface No 00 007 for low-power radio equipment for local area networks in the 5 GHz frequency band | 10.3.2006 |
2005/0695/UK | IR 2045 — UK Interface Requirement 2045 (Version 1.1) for Concurrent Spectrum Access | 13.3.2006 |
2005/0696/SI | Rules on the Veterinary Conditions for the Import of Non-Harmonised Animals into the Republic of Slovenia, Trade in Non-Harmonised Animals and Products, and Non-Commercial Movements of Non-Harmonised Domestic Animals | 13.3.2006 |
2005/0697/SI | Rules on the Management of Packaging and Packaging Waste | 14.3.2006 |
2005/0698/SK | Technical, design and operating requirements for connection to the transmission system (Slovak Electricity Transmission System Joint Stock Company, Bratislava) | 15.3.2006 |
2005/0699/SK | Technical rules (West Slovak Power Engineering Joint Stock Company, Bratislava) | 15.3.2006 |
2005/0700/SK | Technical rules (Central Slovak Power Engineering Joint Stock Company, Žilina) | 15.3.2006 |
2005/0701/SK | Technical rules (East Slovak Power Engineering Joint Stock Company, Košice) | 15.3.2006 |
2005/0702/NL | Regulation implementing the energy investment allowance, with Energy List 2006 | |
2005/0703/F | Order adopting the safety regulation to counter the risks of fire and panic in penal establishments | 15.3.2006 |
2005/0704/HU | The Draft amending Law XXXI of 1996 (hereinafter referred to as: LFP) on procedures for the provisions of fire protection, technical salvage operations and fire brigades | 16.3.2006 |
2005/0705/HU | Governmental Decree No …/2005. (……...) amending the Governmental Decree No 53/2003. (11 April) on exemptions from the environmental product tariff and on conditions of reclaiming and taking over environmental product tariffs as well as on import conditions of used tyres | |
2005/0706/F | Order adopting the safety regulation to counter the risks of fire and panic in covered carparks | 16.3.2006 |
2005/0707/B | Programme Law (Title VII — Chapter 5) | |
2005/0708/HU | The Minister for Environmental Protection and Water Affairs Decree (MEPWA) …./2005 (…) on the environmental product tariff, further on amending the MEPWA Decree 10/1995 (28 Sept.) on enacting provisions concerning Law LVI of 1995 on Environmental Product Tariffs of certain products | |
2005/0709/S | Draft Act amending the Act (2001:1080) on vehicle exhaust gas cleaning and engine fuels | 17.3.2006 |
2005/0710/SK | Draft National Security Authority Decree of ..............2005, amending National Security Authority Decree No. 337/2004 Coll., laying down details of the certification of mechanical barrier devices and technical protection devices and their use | 17.3.2006 |
2005/0711/P | Repeal of the prohibition on affixing coloured films to the windows of motor vehicles and their trailers as laid down by Article 2(1) of Decree-Law No 40 of 11 March 2003 | 17.3.2006 |
2005/0712/P | Affixing coloured films to the windows of passenger or goods vehicles | 17.3.2006 |
2005/0713/NL | Regulation by the Secretary of State for Housing, Planning and the Environment regulating the designation of investments that are in the interest of the Dutch environment (Designation Regulation on the random depreciation of and investment allowance for environmental investments 2006) | |
2005/0714/F | Draft Order on nutritional substances which can be used to fortify edible salt | 20.3.2006 |
2005/0715/S | Draft administrative provisions and general guidance of the Swedish Maritime Authority (SJÖFS 2003:5) on the safety of navigation and navigational equipment | 20.3.2006 |
2005/0716/I | Draft Decree of the Minister for Agriculture and Forestry in concert with the Minister for Production Activities on the origin and provenance of fresh tomatoes used in the production of "Tomato passata" | 20.3.2006 |
2005/0717/I | Regulation on the hygiene control of aluminium materials and items intended to come into contact with foodstuffs | 20.3.2006 |
2005/0718/F | Draft Decree on salt for human consumption | 20.3.2006 |
2005/0719/DK | Guideline of 27 May 2005 on the labelling of fresh poultry meat with guidance on good culinary hygiene | 20.3.2006 |
2005/0720/B | Royal Decree on the microbiological criteria applicable to foodstuffs | 21.3.2006 |
2005/0721/GR | Supreme Chemical Council decision 300/2005 — Specifications for commercial liquid gases | 21.3.2006 |
2005/0722/GR | Supreme Chemical Council decision 437/2005 — Harmonisation of Greek legislation with Directive 2004/42/ΕC of the European Parliament and of the Council of 21 April 2004 on the limitation of emissions of volatile organic compounds due to the use of organic solvents in certain paints and varnishes and vehicle finishing products and amending Directive 1999/13/EC | 21.3.2006 |
2005/0723/A | Provincial Act amending the Upper Austrian Structural Engineering Act (Upper Austrian Structural Engineering Act Amendment 2006) | 21.3.2006 |
2005/0724/E | Draft Royal Decree approving the quality standards for cheese and processed cheese | 21.3.2006 |
2005/0725/NL | Dutch radio interfaces | 21.3.2006 |
2005/0726/HU | Common Ministerial Decree …/2006.(…) EüM-FVM of the Minister for Public Health and the Minister for Agriculture and Regional Development amending the Common Ministerial Decree 5/2002. (22 February) 22.) EüM-FVM relating to the fixing of maximum levels for pesticide residues in and on plants and vegetal products | 22.3.2006 |
2005/0727/NL | Regulation by the State Secretary for Housing, Planning and the Environment governing new regulations to prevent leakage from primary refrigerants during the use of, or maintenance activities on, cooling units and, in this connection, an amendment to the Regulation on leaktightness regulations for cooling units 1997 (Regulation on leaktightness during the use of cooling units under the Environmentally Hazardous Substances Act 2006) | 22.3.2006 |
2005/0728/CZ | Draft Decree amending Decree No. 333/1997 Coll., which implements Section 18(a), (d), (h), (i), (j) and (k) of Act No. 110/1997 Coll. on foodstuffs and tobacco products and amending certain associated Acts, for milled corn products, pasta, bakery products and confectionery products and pastries, as amended by Decree No. 93/2000 Coll. | 22.3.2006 |
2005/0729/SI | Rules Amending the Rules on Conformity Assessment Procedures for Vehicles | 22.3.2006 |
2005/0730/E | Royal Decree regulating the State metrological control of measurement equipment | 23.3.2006 |
2005/0731/UK | The Measuring Instruments (Gas Meters) Regulations 2006 | 23.3.2006 |
2005/0732/UK | The Measuring Instruments (Active Electrical Energy Meters) Regulations 2006 | 23.3.2006 |
2005/0733/D | Order on electronic legal transactions at the local courts in the Land of North Rhine-Westphalia in matters relating to the register of companies and the register of cooperative societies | 24.3.2006 |
2005/0734/LV | Draft Cabinet Regulation "Regulations on the circulation of erotic and pornographic materials" | 29.3.2006 |
2005/0735/FIN | Council of State Decree concerning the recovery of certain wastes in earth construction | 29.3.2006 |
2005/0736/NL | Decree laying down again the amendment to the Building Decree 2003 (amendment regarding the tightening-up of the energy performance coefficient for residences and certain other amendments) | 29.3.2006 |
2005/0737/LV | Regulations for the technical inspection of tractors and trailers thereof, and technical control on roads | 29.3.2006 |
2005/0738/D | Specimen guideline on fire protection requirements for conduits (Specimen Guideline on Conduits) [German designation: MLAR], issued 17 November 2005 | 30.3.2006 |
2005/0739/S | Draft Road Tax Act | |
--------------------------------------------------
