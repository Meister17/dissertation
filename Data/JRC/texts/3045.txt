Brussels, 11.2.2005
COM(2005) 39 final
Proposal for a
COUNCIL REGULATION
terminating the partial interim review of anti-dumping measures on imports of polyethylene terephthalate (PET) film originating, inter alia, in India
(presented by the Commission)
EXPLANATORY MEMORANDUM
By Council Regulation (EC) No 1676/2001 [1], the Council imposed definitive anti-dumping duties on imports of polyethylene terephthalate (PET) film originating, inter alia, in India. The duties ranged between 0% and 62.6% on imports of PET film originating in India. The existing duty applicable to Jindal Poly Films Limited is 0%.
Following a request lodged by a number of large Community producers of PET film, the Commission initiated in February 2004 a partial interim review limited to dumping in respect of Jindal Poly Films Limited.
The investigation showed that Jindal Poly Films Limited was not dumping.
It is therefore proposed that the anti-dumping duty in respect of Jindal Poly Films Limited should remain unchanged at 0%.
Proposal for a
COUNCIL REGULATION
terminating the partial interim review of anti-dumping measures on imports of polyethylene terephthalate (PET) film originating, inter alia , in India
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 384/96[2] of 22 December 1995 (‘the basic Regulation’), and in particular Article 11(3) thereof,
After consulting the Advisory Committee,
Whereas:
A. PROCEDURE
1. Previous procedure and existing measures
(1) Following an investigation initiated in May 2000 (‘the original investigation’), the Council imposed definitive anti-dumping duties in August 2001, pursuant to Regulation (EC) No 1676/2001[3] on imports of polyethylene terephthalate (PET) film originating, inter alia, in India. The duties ranged between 0% and 62.6% on imports of PET film originating in India.
(2) The existing anti-dumping duty applicable to imports from Jindal Poly Films Limited, previously Jindal Polyester Limited (notice of change of name published on 2 December 2004[4]), is 0%. Imports of PET film from the company are also subject to a countervailing duty of 7%, which was imposed in 1999 by Council Regulation (EC) No 2597/1999[5].
2. Request for a review
(3) A request for a partial interim review limited to dumping in respect of Jindal Poly Films Limited was lodged by the following Community producers: Du Pont Teijin Films, Mitsubishi Polyester Film GmbH and Nuroll SpA (‘the applicants’). The applicants represent a major proportion of the Community production of PET film. Toray Plastics Europe, indicated its support for the request, although it was not a formal applicant.
(4) The applicants alleged that the dumping margin of Jindal Poly Films Limited had changed and was higher than in the original investigation leading to the imposition of the existing measures.
3. Investigation
(5) Having determined, after consulting the Advisory Committee, that the request contained sufficient prima facie evidence, the Commission announced on 19 February 2004 the initiation of a partial interim review pursuant to Article 11(3) of the basic Regulation by a notice (‘Notice of Initiation’) published in the Official Journal of the European Union [6].
(6) The review was limited in scope to the examination of dumping in respect of Jindal Poly Films Limited. The investigation period (‘IP’) was 1 January 2003 to 31 December 2003.
(7) The Commission officially informed Jindal Poly Films Limited, the representatives of the exporting country and the Community producers about the initiation of the review. Interested parties were given the opportunity to make their views known in writing and to request a hearing within the time limit set in the notice of initiation.
(8) In order to obtain the information deemed necessary for its investigation, the Commission sent a questionnaire to Jindal Poly Films Limited, the exporting producer concerned, which cooperated by replying to the questionnaire. A verification visit was carried out at the premises of the company in New Delhi to ensure the integrity of the information submitted.
B. PRODUCT CONCERNED AND LIKE PRODUCT
1. Product concerned
(9) The product concerned is, as defined in the original investigation, polyethylene terephthalate (PET) film originating in India, normally declared under CN codes ex 3920 62 19 and ex 3920 62 90.
2. Like product
(10) As in the original investigation, it was found that PET film produced and sold on the domestic market in India and PET film exported to the Community from India, as well as PET film produced and sold by the Community industry on the Community market have the same physical and technical characteristics and uses. Therefore, they are like products within the meaning of Article 1(4) of the basic Regulation.
C. DUMPING
1. Normal value
(11) In order to establish normal value, it was first verified that the total domestic sales of the exporting producer were representative in accordance with Article 2(2) of the basic Regulation, i.e. that they accounted for 5% or more of the total sales volume of the product concerned exported to the Community.
(12) It was then ascertained whether total domestic sales of each product type constituted 5% or more of the sales volume of the same type exported to the Community.
(13) For those product types where domestic sales constituted 5% or more of the sales volume of the same type exported to the Community, it was then examined whether sufficient sales had been made in the ordinary course of trade pursuant to Article 2(4) of the basic Regulation. For each product type, where the volume of domestic sales made above the cost of production represented at least 80% of sales, normal value was established on the basis of the weighted average price actually paid for all domestic sales. For those product types where the volume of profitable transactions was equal to or lower than 80%, but not lower than 10% of sales, normal value was based on the weighted average price actually paid for the profitable domestic sales only.
(14) For the product types where domestic prices of the exporting producer could not be used to establish normal value, due to insufficient representativity or to a lack of sales in the ordinary course of trade, normal value was constructed on the basis of the manufacturing costs incurred by the exporting producer concerned plus a reasonable amount for selling, general and administrative costs (‘SG&A costs’) and for profits, in accordance with Article 2(3) and (6) of the basic Regulation.
(15) The SG&A costs were based on such costs incurred by the exporting producer with regard to its domestic sales of the product concerned, which were found to be representative. The profit margin was calculated on the basis of the weighted average profit margin of the company for those product types sold on the domestic market in sufficient quantities in the ordinary course of trade.
2. Export price
(16) The exporting producer reported sales (one consignment) made to a related company in the Community. Given that the quantity involved was negligible, these sales were disregarded.
(17) The other export sales of the product concerned to the Community in the IP had been made to independent customers. Therefore, the export price was established in accordance with Article 2(8) of the basic Regulation on the basis of export price actually paid or payable.
3. Comparison
(18) The normal value and export price were compared on an ex-works basis. For the purpose of ensuring a fair comparison, due allowance in the form of adjustments was made for differences affecting price comparability in accordance with Article 2(10) of the basic Regulation. Accordingly, allowances for differences in discounts, rebates, transport, insurance, handling, loading and ancillary costs, packing, credit and commissions were granted, where applicable and supported by verified evidence.
(a) Import charges
(19) Jindal Poly Films Limited claimed an adjustment to the normal value for the import duty not collected under the Advance Licence Scheme (ALS) on imports of raw material used in the manufacture of goods for export. The ALS permits the importation of raw materials free of duty, provided that the company exports a corresponding quantity and value of finished product determined in accordance with officially set standard input-output norms. Imports under the ALS can either be used for the production of export goods or for the replenishment of domestic inputs used to produce such goods. The company claimed that exports of the product concerned to the EC were used to satisfy the requirements under the ALS in respect of raw materials imported.
(20) No conclusion was made as to whether or not an adjustment was warranted for this claim, given that it would have no impact on the final outcome of the review investigation.
(b) Other allowances
(21) The exporting producer claimed, for a limited number of exports, an adjustment on the export price pursuant to Article 2(10)(k) of the basic Regulation, based on the amount of the benefits received on exportation under the Duty Entitlement Passbook Scheme (‘DEPB’) on a post-export basis. Under this scheme, the credits received when exporting the product concerned could be used to offset customs duties due on imports of any goods or could be freely sold to other companies. In addition, there is no constraint that the imported goods should only be used in the production of the exported product. The producer did not demonstrate that the benefit under the DEPB scheme on a post-export basis affected price comparability, and in particular that the customers consistently paid different prices on the domestic market because of the DEPB benefits. Therefore, the claim was rejected.
4. Dumping margin
(22) The dumping margin was established on the basis of a comparison of a weighted average normal value with a weighted average export price, in accordance with Article 2(11) of the basic Regulation.
(23) This comparison showed a dumping margin of 0%.
D. CONCLUSION
(24) On the basis of the above facts and considerations, and in view of the information available, it is concluded that, in accordance with Article 11(3) of the basic Regulation, the current review investigation should be terminated and the anti-dumping duty of 0% imposed by Regulation (EC) No 1676/2001 on imports of PET film produced and exported to the EC by Jindal Poly Films Limited should be maintained.
(25) All parties concerned were informed of the essential facts and considerations on the basis of which the decision to maintain the current anti-dumping duty was made and were given the opportunity to comment.
(26) The Community Industry argued that the dumping calculation should have been made on the basis of a comparison of the weighted average normal value to individual export transactions, since they alleged that targeted dumping to a ‘special category of purchasers’ existed. They argued that, if sales of a particular film type were only sold to customers within a specific end-use segment, this would amount to targeting by customer. They also considered that the approach was justified in view of an increase in Indian production capacity, which they believed related mainly to that particular film type. In this regard, it should first be noted that the film type concerned was not only sold to customers solely purchasing that film type, but also to customers purchasing other film types. A comparison of prices for the same film types sold to different customers did not show a pattern of price differentiation by client. In the absence of such a pattern, the existence of an alleged pattern of dumping by model is irrelevant for the purpose of the selection of the dumping calculation methodology, as indicated in Article 2(11) of the basic Regulation. Moreover, it should also be noted that changes in the production capacity are also not a relevant factor in determining the methodology to be followed for the determination of the dumping margin. Since no targeting by customer, region or time period was identified, the Community industry’s claim is rejected and the comparison of normal value to export price on the basis of a weighted average to weighted average approach is maintained as the appropriate methodology,
HAS ADOPTED THIS REGULATION:
Article 1
The partial interim review of the anti-dumping measures concerning imports of polyethylene terephthalate (PET) film originating, inter alia , in India, normally declared under CN codes ex 3920 62 19 and ex 3920 62 90, insofar as these measures concern the Indian exporting producer Jindal Poly Films Limited, is hereby terminated.
Article 2
This Regulation shall enter into force on the day following that of its publication in the Official Journal of the European Union .
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels,
For the Council
The President
[1] OJ L 227, 23.8.2001, p. 1.
[2] OJ L 56, 6.3.1996, p. 1. Regulation as last amended by Regulation (EC) No 461/2004 (OJ No L 77, 13.3.2004, p. 12).
[3] OJ L 227, 23.8.2001, p. 1.
[4] OJ C 297, 2.12.2004, p. 2.
[5] OJ L 316, 10.12.1999, p. 1.
[6] OJ C 43, 19.2.2004, p. 14.
