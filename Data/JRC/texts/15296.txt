Commission Regulation (EC) No 630/2006
of 24 April 2006
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 25 April 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 24 April 2006.
For the Commission
J. L. Demarty
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 386/2005 (OJ L 62, 9.3.2005, p. 3).
--------------------------------------------------
ANNEX
to Commission Regulation of 24 April 2006 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code [1] | Standard import value |
07020000 | 052 | 116,6 |
204 | 57,2 |
212 | 139,0 |
624 | 138,6 |
999 | 112,9 |
07070005 | 052 | 123,5 |
204 | 47,4 |
628 | 147,3 |
999 | 106,1 |
07091000 | 624 | 119,2 |
999 | 119,2 |
07099070 | 052 | 128,6 |
204 | 46,2 |
999 | 87,4 |
08051020 | 052 | 67,5 |
204 | 33,7 |
212 | 52,7 |
220 | 42,7 |
624 | 66,6 |
999 | 52,6 |
08055010 | 052 | 43,0 |
624 | 56,8 |
999 | 49,9 |
08081080 | 388 | 89,8 |
400 | 122,8 |
404 | 77,2 |
508 | 77,7 |
512 | 80,4 |
524 | 86,0 |
528 | 84,8 |
720 | 81,2 |
804 | 113,9 |
999 | 90,4 |
08082050 | 052 | 75,0 |
388 | 86,9 |
512 | 85,0 |
528 | 72,2 |
720 | 91,3 |
999 | 82,1 |
[1] Country nomenclature as fixed by Commission Regulation (EC) No 750/2005 (OJ L 126, 19.5.2005, p. 12). Code "999" stands for "of other origin".
--------------------------------------------------
