Commission Decision
of 18 August 2005
amending for the sixth time Decision 2004/122/EC concerning certain protection measures in relation to avian influenza in certain Asian countries
(notified under document number C(2005) 3183)
(Text with EEA relevance)
(2005/619/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/496/EEC of 15 July 1991 laying down the principles governing the organisation of veterinary checks on animals entering the Community from third countries and amending Directives 89/662/EEC, 90/425/EEC and 90/675/EEC [1], and in particular Article 18 (1) and (6) thereof,
Having regard to Council Directive 97/78/EC of 18 December 1997 laying down the principles governing the organisation of veterinary checks on products entering the Community from third countries [2], and in particular Article 22 (1) and (6) thereof,
Whereas:
(1) Commission Decision 2004/122/EC of 6 February 2004 concerning certain protection measures in relation to avian influenza in several Asian countries [3] was adopted in response to outbreaks of avian influenza in several Asian countries.
(2) Commission Decision 2000/666/EC of 16 October 2000 laying down the animal health requirements and the veterinary certification for the import of birds, other than poultry and the conditions for quarantine [4] provides that Member States are to authorise the import of birds from third countries listed as members of the World Organisation for Animal Health (OIE) and that those birds are to be subjected to quarantine and testing upon entry into the Community.
(3) Russia has confirmed an outbreak of H5N1 avian influenza on their territory on 5 August 2005 to the Commission.
(4) Kazakhstan has confirmed an outbreak of H5 avian influenza to the OIE although the neuraminidase type is not yet known. However in view of the proximity with the outbreak in Russia it is probably the same strain.
(5) Kazakhstan and Russia are members of the OIE and accordingly Member States are to accept imports of birds, other than poultry, from these countries under Decision 2000/666/EC. Taking into account the potential serious consequences related to the specific avian influenza virus strain (H5N1) involved, which is the same as that confirmed in a number of Asian countries, the importation of these birds from Kazakhstan and Russia should be suspended as a precautionary measure.
(6) Under Regulation (EC) No 1774/2002 of the European Parliament and of the Council of 3 October 2002 laying down health rules concerning animal by-products not intended for human consumption [5], the importation of unprocessed feathers and parts of feathers originating in Kazakhstan and Russia is authorised. In view of the current disease situation in Kazakhstan and Russia those imports should also be suspended as a precautionary measure.
(7) Article 4 of Decision 2004/122/EC suspends the importation from certain third countries of unprocessed feathers and parts of feathers and live birds other than poultry, as defined in Decision 2000/666/EC. Therefore in the interests of animal and public health, Kazakhstan and Russia should be added to the third countries referred to in Article 4 of Decision 2004/122/EC.
(8) Decision 2004/122/EC should be amended accordingly.
(9) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee of the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
Decision 2004/122/EC is amended as follows:
1. in the title, "concerning certain protection measures in relation to avian influenza in several Asian countries" is replaced by "concerning certain protection measures in relation to avian influenza in several third countries";
2. Article 4(1) is replaced by the following:
"1. Member States shall suspend the importation from Cambodia, China including Hong Kong, Indonesia, Kazakhstan, Laos, Malaysia, North Korea, Pakistan, Russia, Thailand and Vietnam of:
- unprocessed feathers and parts of feathers, and
- "live birds other than poultry" as defined in Article 1, third indent, of Decision 2000/666/EC, including birds accompanying their owners (pet birds)."
Article 2
Member States shall amend the measures they apply so as to bring them into compliance with this Decision and they shall give immediate appropriate publicity to the measures adopted. They shall immediately inform the Commission thereof.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 18 August 2005.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 268, 24.9.1991, p. 56. Directive as last amended by the 2003 Act of Accession.
[2] OJ L 24, 30.1.1998, p. 9. Directive as last amended by Regulation (EC) No 882/2004 of the European Parliament and of the Council (OJ L 165, 30.4.2004, p. 1) (corrected version in OJ L 191, 28.5.2004, p. 1).
[3] OJ L 36, 7.2.2004, p. 59. Decision as last amended by Decision 2005/390/EC (OJ L 128, 21.5.2005, p. 77).
[4] OJ L 278, 31.10.2000, p. 26. Decision as last amended by Decision 2002/279/EC (OJ L 99, 16.4.2002, p. 17).
[5] OJ L 273, 10.10.2002, p. 1. Regulation as last amended by Commission Regulation (EC) No 416/2005 (OJ L 66, 12.3.2005, p. 10).
--------------------------------------------------
