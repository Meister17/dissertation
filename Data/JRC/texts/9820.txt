Euro exchange rates [1]
6 April 2006
(2006/C 84/01)
| Currency | Exchange rate |
USD | US dollar | 1,2312 |
JPY | Japanese yen | 144,81 |
DKK | Danish krone | 7,4631 |
GBP | Pound sterling | 0,70055 |
SEK | Swedish krona | 9,3220 |
CHF | Swiss franc | 1,5793 |
ISK | Iceland króna | 89,53 |
NOK | Norwegian krone | 7,8920 |
BGN | Bulgarian lev | 1,9558 |
CYP | Cyprus pound | 0,5760 |
CZK | Czech koruna | 28,550 |
EEK | Estonian kroon | 15,6466 |
HUF | Hungarian forint | 268,02 |
LTL | Lithuanian litas | 3,4528 |
LVL | Latvian lats | 0,6960 |
MTL | Maltese lira | 0,4293 |
PLN | Polish zloty | 3,9716 |
RON | Romanian leu | 3,5162 |
SIT | Slovenian tolar | 239,63 |
SKK | Slovak koruna | 37,480 |
TRY | Turkish lira | 1,6489 |
AUD | Australian dollar | 1,6799 |
CAD | Canadian dollar | 1,4225 |
HKD | Hong Kong dollar | 9,5529 |
NZD | New Zealand dollar | 1,9998 |
SGD | Singapore dollar | 1,9746 |
KRW | South Korean won | 1172,10 |
ZAR | South African rand | 7,4308 |
CNY | Chinese yuan renminbi | 9,8617 |
HRK | Croatian kuna | 7,3280 |
IDR | Indonesian rupiah | 11086,96 |
MYR | Malaysian ringgit | 4,515 |
PHP | Philippine peso | 62,902 |
RUB | Russian rouble | 33,8580 |
THB | Thai baht | 46,976 |
[1] Source: reference exchange rate published by the ECB.
--------------------------------------------------
