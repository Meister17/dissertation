Commission Decision
of 3 August 2005
on the extension of the limited recognition of the Hellenic Register of Shipping
(notified under document number C(2005) 2940)
(Only the Greek text is authentic)
(Text with EEA relevance)
(2005/623/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 94/57/EC of 22 November 1994 on common rules and standards for ship inspection and survey organisations and for the relevant activities of maritime administrations [1] and in particular Article 4(3) thereof,
Having regard to the letter dated 2 April 2004 from the Cypriot authorities, requesting the extension to Cyprus of the limited recognition of the Hellenic Register of Shipping (hereinafter HRS) pursuant to Article 4(2) of Directive 94/57/EC,
Having regard to the letter dated 7 September 2004 from the Greek authorities, requesting the unconditional extension of the limited recognition of HRS pursuant to Article 4(3) of that Directive,
Whereas:
(1) The limited recognition under Article 4(3) of Directive 94/57/EC is a recognition granted to organisations known as classification societies, which fulfil all criteria other than those set out under paragraphs 2 and 3 of the "General" section A of the Annex thereto, but limited in time and scope in order for the organisation concerned to gain further experience.
(2) Commission Decision 2001/890/EC [2] recognised HRS for Greece on the basis of Article 4(3), for a period of three years from 13 December 2001.
(3) The Commission has verified that HRS meets all criteria of the Annex to Directive 94/57/EC other than those set out under paragraphs 2 and 3 of the "General" section A of that Annex.
(4) The assessment carried out by the Commission has furthermore revealed signs that HRS needs to keep developing its system of quality indicators in order to enhance its ability to measure both risk and performance.
(5) The organisation’s safety and pollution performance records as published by the Paris Memorandum of Understanding, albeit poor during the period 2000 to 2002, showed a trend towards moderate improvement in 2003 and HRS has committed itself to the objective of bringing its performance records into line with the average of the recognised organisations.
(6) The measures provided for in this Decision are in accordance with the opinion of the COSS Committee set up by Article 7 of Directive 94/57/EC,
HAS ADOPTED THIS DECISION:
Article 1
The limited recognition of the Hellenic Register of Shipping granted by Decision 2001/890/EC is extended for a period of three years as from the date of adoption of this Decision.
Article 2
The effects of the extended recognition are limited to Greece and Cyprus.
Article 3
This Decision is addressed to the Hellenic Republic and the Republic of Cyprus.
Done at Brussels, 3 August 2005.
For the Commission
Jacques Barrot
Vice-President
[1] OJ L 319, 12.12.1994, p. 20. Directive as last amended by Directive 2002/84/EC of the European Parliament and of the Council (OJ L 324, 29.11.2002, p. 53).
[2] OJ L 329, 14.12.2001, p. 72.
--------------------------------------------------
