Order of the Court
(Fifth Chamber)
of 23 September 2005
in Case C-357/04 P: Antonio Andolfi
v Commission of the European Communities [1]
In Case C-357/04 P: APPEAL under Article 56 of the Statute of the Court of Justice, brought on 17 August 2004, by Antonio Andolfi, residing in Rome (Italy), (Avvocato: S. Amato) the other party to the proceedings being: Commission of the European Communities (Agent: E. Montaguti), assisted by Avvocato: A. Dal Ferro), the Court (Fifth Chamber), composed of R. Silva de Lapuerta, President of the Chamber, C. Gulmann and P. Kūris (Rapporteur), Judges; L.A. Geelhoed, Advocate General; R. Grass, Registrar, made an order on 23 September 2005, the operative part of which is as follows:
1. The appeal is dismissed.
2. Mr Andolfi shall pay the costs.
[1] OJ C 284 of 20.11.2004.
--------------------------------------------------
