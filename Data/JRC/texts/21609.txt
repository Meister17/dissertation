COUNCIL REGULATION (EEC) No 2082/92 of 14 July 1992 on certificates of specific character for agricultural products and foodstuffs
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 43 thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas the production, manufacture and distribution of agricultural products and foodstuffs play an important role in the Community economy;
Whereas, in the context of the reorientation of the common agricultural policy, the diversification of agricultural production should be encouraged; whereas the promotion of specific products could be of considerable benefit to the rural economy, particularly in less-favoured or remote areas, both by improving the income of farmers and by retaining the rural population in these areas;
Whereas, in the context of the completion of the internal market in foodstuffs, economic operators should be provided with instruments which enable them to enhance the market value of their products while protecting consumers against improper practices and guaranteeing at the same time fair trade;
Whereas, in accordance with the Council resolution of 9 November 1989 on future priorities for relaunching consumer protection policy (4), account should be taken of increasing consumer demand for greater emphasis on quality and information as regards the nature, method of production and processing of foodstuffs and their special characteristics; whereas, given the diversity of products on the market and the abundance of information concerning them, consumers must, in order to be able to make a better choice, be provided with clear and succinct information regarding the specific characteristics of foodstuffs;
Whereas a voluntary system based on regulatory criteria will help attain these aims; whereas such a system enabling producers to make known the quality of a foodstuff throughout the Community must offer every guarantee so that any references which may be made to it in the trade are substantiated;
Whereas certain producers would like to derive market value from the specific character of agricultural products or foodstuffs because their inherent characteristics distinguish them clearly from similar products or foodstuffs; whereas, in order to protect the consumer, the certified specific character should be subject to inspection;
Whereas, given the specific character of such products or foodstuffs, special provisions should be adopted to supplement the labelling rules laid down in Council Directive 79/112/EEC of 18 December 1978 on the approximation of the laws of the Member States relating to the labelling, presentation and advertising of foodstuffs (5) and whereas, in particular, an expression and, as appropriate, a Community symbol should be devised to accompany the trade description of such products or foodstuffs informing the consumer that it is a product or foodstuff presenting inspected specific characteristics;
Whereas, to guarantee that agricultural products and foodstuffs consistently possess the certified specific characteristics, groups of producers must themselves define the said characteristics in a product specification but whereas the rules for approving inspection bodies responsible for checking that the product specification is complied with must be uniform throughout the Community;
Whereas, in order not to create unfair conditions of competition, any producer must be able to use either a registered trade description together with details and, where appropriate, a Community symbol or a trade description registered as such, as long as the agricultural product or foodstuff he produces or processes complies with the requirements of the relevant specification and the inspection body he has selected is approved;
Whereas provision should be made for allowing trade with third countries offering equivalent guarantees for the issue and inspection of certificates of specific character in their territory;
Whereas, if they are to be attractive to producers and reliable for consumers, expressions relating to the specific character of an agricultural product or a foodstuff must be granted legal protection and be subject to official inspection;
Whereas a procedure should be provided for to establish close cooperation between the Member States and the Commission in a regulatory committee set up for the purpose,
HAS ADOPTED THIS REGULATION:
Article 1
1. This Regulation lays down rules under which a Community certificate of specific character may be obtained for:
- agricultural products listed in Annex II to the Treaty and intended for human consumption,
- foodstuffs listed in the Annex to this Regulation.
The Annex may be amended in accordance with the procedure set out in Article 19.
2. This Regulation shall apply without prejudice to other specific Community provisions.
3. Council Directive 83/189/EEC of 28 March 1989 laying down a procedure for the provision of information in the field of technical standards and regulations (6) shall not apply to certificates of specific character which are the subject of this Regulation.
Article 2
For the purposes of this Regulation:
1. 'specific character` shall mean the feature or set of features which distinguishes an agricultural product or a foodstuff clearly from other similar products or foodstuffs belonging to the same category.
The presentation of an agricultural product or a foodstuff is not regarded as a feature within the meaning of the first subparagraph.
Specific character may not be restricted to qualitative or quantitative composition or to a mode of production laid down in Community or national legislation, in standards set by standardization bodies or in voluntary standards; however, this rule shall not apply where the said legislation or standard has been established in order to define the specific character of a product;
2. 'group` shall mean any association, irrespective of its legal form or composition, of producers and/or processors working with the same agricultural product or foodstuff. Other interested parties may participate in the group;
3. 'certificate of specific character` shall mean recognition by the Community of the specific character of a product by means of its registration in accordance with this Regulation.
Article 3
The Commission shall set up and administer a register of certificates of specific character which will list the names of agricultural products and foodstuffs of which the specific character has been recognized at Community level in accordance with this Regulation.
The register shall distinguish between the names referred to in Article 13 (1) and those referred to in Article 13 (2).
Article 4
1. In order to appear in the register referred to in Article 3, an agricultural product or foodstuff must either be produced using traditional raw materials or be characterized by a traditional composition or a mode of production and/or processing reflecting a traditional type of production and/or processing.
2. Registration shall not be permitted in the case of an agricultural product or foodstuff the specific character of which is due:
(a) to its provenance or geographical origin;
(b) solely to application of a technological innovation.
Article 5
1. To be registered, the name must:
- be specific in itself, or
- express the specific character of the agricultural product or the foodstuff.
2. A name expressing specific character, as referred to in the second indent of paragraph 1, may not be registered if:
- it refers only to claims of a general nature used for a set of agricultural products or foodstuffs, or to those provided for by specific Community legislation,
- it is misleading, such as that, in particular, which refers to an obvious characteristic of the product or does not correspond to the specification or to the consumer's expectations in view of the characteristics of the product.
3. In order to be registered, a specific name as referred to in the first indent of paragraph 1 must be traditional and comply with national provisions or be established by custom.
4. The use of geographical terms shall be authorized in a name not covered by Council Regulation (EEC) No 2081/92 of 14 July 1992 on the protection of geographical indications and designations of origin for agricultural products and foodstuffs (7).
Article 6
1. In order to qualify for a certificate of specific character, an agricultural product or foodstuff must comply with a product specification.
2. The product specification shall include at least:
- the name within the meaning of Article 5, in one or more languages,
- a description of the method of production, including the nature and characteristics of the raw material and/or ingredients used and/or the method of preparation of the agricultural product or the foodstuff, referring to its specific character,
- aspects allowing appraisal of traditional character, within the meaning of Article 4 (1),
- a description of the characteristics of the agricultural product or the foodstuff giving its main physical, chemical, microbiological and/or organoleptic characteristics which relate to the specific character,
- the minimum requirements and inspection procedures to which specific character is subject.
Article 7
1. Only a group shall be entitled to apply for registration of the specific character of an agricultural product or a foodstuff.
2. The application for registration comprising the product specification shall be submitted to the competent authority of the Member State in which the group is established.
3. The competent authority shall forward the application to the Commission if it considers that the requirements of Articles 4, 5 and 6 are fulfilled.
4. No later than the date of entry into force of this Regulation, Member States shall publish the particulars of the competent authorities which they have designated and shall inform the Commission accordingly.
Article 8
1. The Commission shall forward the translated application for registration to the other Member States within a period of six months from the date of receipt of the application referred to in Article 7 (3).
As soon as the forwarding referred to in the first subparagraph has been carried out, the Commission shall publish in the Official Journal of the European Communities the main points of the application forwarded by the competent authority referred to in Article 7 and, in particular, the name of the agricultural product or the foodstuff, as prescribed by the first indent of Article 6 (2), and the applicant's references.
2. The competent authorities of the Member States shall ensure that all persons who can demonstrate a legitimate economic interest are authorized to consult the application referred to in paragraph 1. In addition, and in accordance with the rules in force in the Member States, the said competent authorities may provide access to other parties with a legitimate interest.
3. Within five months of the date of publication referred to in paragraph 1, any natural or legal person legitimately concerned by the registration may object to the intended registration by sending a duly substantiated statement to the competent authorities of the Member State in which that person resides or is established.
4. The competent authorities of the Member States shall adopt the necessary measures to take account of the statement referred to in paragraph 3 within the period laid down. Member States may also submit objections on their own initiative.
Article 9
1. If no objections are notified to the Commission within six months, the Commission shall enter in the register provided for in Article 3 the main points referred to in Article 8 (1) and publish them in the Official Journal of the European Communities.
2. If objections are notified, the Commission shall, within three months, ask the Member States concerned to seek agreement between themselves in accordance with their internal procedures within a further period of three months. If:
(a) such agreement is reached, the Member States in question shall notify the Commission of all the factors which enabled that agreement to be reached and the opinions of the applicant and the objector. If the information received pursuant to Article 6 (2) is unchanged, the Commission shall proceed in accordance with paragraph 1 above. Otherwise, it shall again initiate the procedure laid down in Article 8;
(b) no agreement is reached, the Commission shall decide on the registration in accordance with the procedure laid down in Article 19. If a decision is taken to register the specific character, the Commission shall proceed in accordance with paragraph 1 above.
Article 10
1. Any Member State may submit that a criterion laid down in the product specification of an agricultural product or a foodstuff covered by a Community certificate of specific character has ceased to be met.2. The Member State referred to in paragraph 1 shall make its submission to the Member State concerned. The Member State concerned shall examine the complaint and inform the other Member State of its findings and of any measures taken.
3. In the event of repeated irregularities and the failure of the Member States to come to an agreement, a duly substantiated application must be sent to the Commission.
4. The Commission shall examine the application by consulting the Member States concerned. Where appropriate, the Commission shall take the necessary steps in accordance with the procedure laid down in Article 19. These may include cancellation of the registration.
Article 11
1. A Member State may, at the request of a group established in its territory, apply for an amendment to the product specification.
2. The Commission shall ensure that the request for amendment and the applicant's references are published in the Official Journal of the European Communities. Article 8 (2), (3) and (4) shall apply.
The competent authorities of the Member State shall ensure that any producer and/or processor applying the product specification for which an amendment has been requested is informed of the publication.
3. Within three months of the date of the publication provided for in paragraph 2, any producer and/or processor applying the product specification for which an amendment has been requested may exercise his right to preserve the initial product specification by forwarding a statement to the competent authority of the Member State in which he is established, which must forward it to the Commission together with its comments, if appropriate.
4. If no objection or statement as referred to in paragraph 3 is notified to the Commission within four months of the date of publication laid down in paragraph 2, the Commission shall enter the requested amendment in the register provided for in Article 3 and publish it in the Official Journal of the European Communities.
5. If an objection or a statement as referred to in paragraph 3 is notified to the Commission, the amendment shall not be registered. In such case the requesting group, referred to in paragraph 1, may apply for a new certificate of specific character in accordance with the procedure laid down in Articles 7 to 9.
Article 12
In accordance with the procedure laid down in Article 19, the Commission may define a Community symbol which may be used in the labelling, presentation and advertising of agricultural products or foodstuffs carrying a Community certificate of specific character in accordance with this Regulation.
Article 13
1. From the date of publication provided for in Article 9 (1), the name referred to in Article 5, together with the indication referred to in Article 15 (1), and, where appropriate, the Community symbol referred to in Article 12, shall be reserved for the agricultural product or the foodstuff corresponding to the published product specification.
2. By way of derogation from paragraph 1, the name alone shall be reserved for the agricultural product or the foodstuff corresponding to the published product specification where:
(a) the group so requested in its application for registration;
(b) the procedure referred to in Article 9 (2) (b) does not show that use of the name is lawful, recognized and economically significant for similar agricultural products or foodstuffs.
Article 14
1. Member States shall ensure that at the latest six months following the date of entry into force of this Regulation inspection structures are in place, the function of which shall be to ensure that agricultural products and foodstuffs carrying a certificate of specific character meet the criteria laid down in the specifications.
2. An inspection structure may comprise one or more designated inspection authorities and/or private bodies approved for that purpose by the Member State. Member States shall forward to the Commission lists of the authorities and/or bodies approved and their respective powers. The Commission shall publish these particulars in the Official Journal of the European Communities.
3. Designated inspection authorities and/or private bodies must offer adequate guarantees of objectivity and impartiality with regard to all producers or processors subject to their control and have permanently at their disposal the qualified staff and resources necessary to carry out inspections of agricultural products and foodstuffs covered by a Community certificate of specific character.
If an inspection structure uses the services of another body for some inspections, that body must offer the same guarantees. However, the designated inspection authorities and/or approved private bodies shall continue to be responsible vis-à-vis the Member State for all inspections.
As from 1 January 1998, in order to be approved by a Member State for the purpose of this Regulation, bodies must fulfil the requirements laid down in standard EN 45011 of 26 June 1989.
4. If a Member State's designated inspection authority and/or private body establishes that an agricultural product or a foodstuff carrying a certificate of specific character issued by that Member State does not meet the criteria of the specification, it shall take the steps necessary to ensure that this Regulation is complied with. It shall inform the Member State of the measures taken in carrying out its inspections. The parties concerned must be notified of all decisions taken.
5. A Member State must withdraw approval from an inspection body where the criteria referred to in paragraphs 2 and 3 are no longer fulfilled. It shall inform the Commission, which shall publish in the Official Journal of the European Communities a revised list of approved bodies.
6. Member States shall adopt the measures necessary to ensure that a producer who complies with this Regulation has access to the inspection system.
7. The costs of the inspections provided for by this Regulation shall be borne by the users of the certificate of specific character.
Article 15
1. The following may be used only by producers complying with the registered product specification:
- an indication to be determined in accordance with the procedure laid down in Article 19,
- where appropriate, the Community symbol, and,
- subject to Article 13 (2), the registered name.
2. A producer using, for the first time after registration, a name reserved pursuant to Article 13 (1) or (2), even if he belongs to the group making the original application, shall in due course notify a designated inspection authority or body of the Member State in which he is established thereof.
3. The designated inspection authority or body shall ensure that the producer complies with the published information before the product is placed on the market.
Article 16
Without prejudice to international agreements, this Regulation shall apply to agricultural products and foodstuffs coming from a third country, on condition that the third country:
- is able to provide guarantees identical or equivalent to those referred to in Articles 4 and 6,
- has inspection arrangements equivalent to those defined in Article 14,
- is prepared to give protection equivalent to that available in the Community to corresponding agricultural products or foodstuffs coming from the Community and covered by a Community certificate of specific character.
Article 17
1. Member States shall take the necessary measures to ensure legal protection against any misuse or misleading use of the term referred to in Article 15 (1) and, where applicable, of the Community symbol referred to in Article 12 and against any imitation of names registered and reserved pursuant to Article 13.
2. Registered names shall be protected against any practice liable to mislead the public including, inter alia, practices suggesting that the agricultural product or foodstuff is covered by a certificate of specific character issued by the Community.
3. Member States shall inform the Commission and the other Member States of the measures taken.
Article 18
Member States shall take all appropriate measures to ensure that sales descriptions used at national level do not give rise to confusion with names registered and reserved pursuant to Article 13 (2).
Article 19
The Commission shall be assisted by a committee composed of the representatives of the Member States and chaired by the representative of the Commission.
The representative of the Commission shall submit to the committee a draft of the measures to be taken. The committee shall deliver its opinion on the draft within a time limit which the chairman may lay down according to the urgency of the matter. The opinion shall be delivered by the majority laid down in Article 148 (2) of the Treaty in the case of decisions which the Council is required to adopt on a proposal from the Commission. The votes of the representatives of the Member States within the committee shall be weighted in the manner set out in that Article. The chairman shall not vote.
The Commission shall adopt the measures envisaged if they are in accordance with the opinion of the committee.
If the measures envisaged are not in accordance with the opinion of the committee, or if no opinion is delivered, the Commission shall, without delay, submit to the Council a proposal relating to the measures to be taken. The Council shall act by a qualified majority.
If, on the expiry of a period of three months from the date of referral to the Council, the Council has not acted, the proposed measures shall be adopted by the Commission.
Article 20
Detailed rules for applying this Regulation shall be adopted in accordance with the procedure laid down in Article 19.
Article 21
Within five years of the date on which this Regulation enters into force, the Commission shall submit to the Council a report on the application of the Regulation, together with any appropriate proposals.
The report shall cover, in particular, the consequences of applying Articles 9 and 13.
Article 22
This Regulation shall enter into force twelve months after its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 14 July 1992.
For the Council
The President
J. GUMMER
(1) OJ No C 30, 6. 2. 1991, p. 4 and
OJ No C 71, 20. 3. 1992, p. 14.
(2) OJ No C 326, 16. 12. 1991, p. 40.
(3) OJ No C 40, 17. 2. 1992, p. 3.
(4) OJ No C 294, 22. 11. 1989, p. 1.
(5) OJ No L 33, 8. 2. 1979, p. 1. Last amended by Directive 91/72/EEC (OJ N L 42, 15. 2. 1991, p. 27).
(6) OJ No L 109, 26. 4. 1983, p. 8. Last amended by Decision 90/230/EEC (OJ No L 128, 18. 5. 1990, p. 15).
(7) See p. 1 of this Official Journal.
ANNEX
Foodstuffs referred to in Article 1 (1)
- Beer,
- Chocolate and other food preparations containing cocoa,
- Confectionery, bread, pastry, cakes, biscuits and other baker's wares,
- Pasta, whether or not cooked or stuffed,
- Pre-cooked meals,
- Prepared condiment sauces,
- Soups or broths,
- Beverages made from plant extracts,
- Ice-cream and sorbets.
