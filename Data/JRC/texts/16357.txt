COUNCIL REGULATION (EC) No 2466/96 of 17 December 1996 amending Regulation (EEC) No 3508/92 establishing an integrated administrative and control system for certain Community aid schemes
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 43 thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Whereas Article 6 (2) of Regulation (EEC) No 3508/92 (3) provides that area aid applications must be submitted during the first quarter of the year; whereas, however, the Commission may allow a Member State to fix a date for the submission of area aid applications between 1 April and the dates referred to in Articles 10, 11 and 12 of Council Regulation (EEC) No 1765/92 of 30 June 1992 establishing a support system for producers of certain arable crops (4); whereas, in the light of experience, it should be possible for Member States to set the deadline for submission on their own responsibility without seeking the authorization of the Commission, taking into account in particular the time required for all relevant data to be available for the proper administrative and financial management of the aid and for checks to be carried out;
Whereas Article 13 (1) of Regulation (EEC) No 3508/92 provides that all the elements of the integrated system are to apply from 1 January 1996 at the latest; whereas, in the light of experience gained, notably in setting up alphanumeric identification systems for agricultural parcels and data bases, this deadline should be postponed for one year;
Whereas, given the considerable investment required to ensure that the integrated system is established definitively, provision should be made to prolong by one year the period for which the Community financial contribution may be granted,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EEC) No 3508/92 is hereby amended as follows:
1. Article 6 (2) shall be replaced by the following:
'2. Area aid applications must be submitted by a date to be set by the Member State which may not be later than the dates referred to in Articles 10, 11 and 12 of Regulation (EEC) No 1765/92.
In all cases, the date shall be set bearing in mind, inter alia, the period required for all relevant data to be available for the proper administrative and financial management of the aid and for the checks provided for in Article 8 to be carried out.`;
2. Article 10 (2) shall be amended as follows:
(a) the first subparagraph shall be replaced by the following:
'The Community's financial contribution shall be granted for a period of five years from 1992, within the limits of the appropriations allocated for this purpose.`;
(b) the third subparagraph shall be replaced by the following:
'The total amount shall be shared among the Member States as follows:
- for 1995:>TABLE>
- for 1996:
>TABLE>
- for 1997:
>TABLE>
(c) the following sentence shall be added to the fourth subparagraph:
'However, appropriations which have not been used may be redistributed, under the conditions laid down by this Regulation, to those Member States which apply for them.`;
3. Article 13 (1) (b) shall be replaced by the following:
'(b) as regards the other elements referred to in Article 2, at the latest from:
- 1 January 1998 in the case of Austria, Finland and Sweden,
- 1 January 1997 in the case of the other Member States.`
Article 2
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Communities.
Point 2 of Article 1 shall apply with effect from 1 January 1996.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 17 December 1996.
For the Council
The President
I. YATES
(1) OJ No C 176, 19. 6. 1996, p. 13.
(2) Opinion delivered on 13 December 1996 (not yet published in the Official Journal).
(3) OJ No L 355, 5. 12. 1992, p. 1. Regulation last amended by Regulation (EC) No 1577/96 (OJ No L 206, 16. 8. 1996, p. 4).
(4) OJ No L 181, 1. 7. 1992, p. 12. Regulation last amended by Regulation (C) No 1575/96 (OJ No L 206, 16. 8. 1996, p. 1).
