Authorisation for State aid pursuant to Articles 87 and 88 of the EC Treaty
Cases where the Commission raises no objections
(2006/C 98/04)
(Text with EEA relevance)
Date of adoption of the decision : 6.12.2002
Member State : Netherlands
Aid No : N 321/2002
Title : Support for the processing and marketing of fishery products and the aquaculture sectors in Urk (province of Flevoland — Objective 1)
Objective : To part-finance structural assistance for investments in the processing and marketing of fishery products and the aquaculture sectors in Urk (province of Flevoland — Objective 1)
Legal basis - Artikel 145 van de Provinciewet waarin wordt gesteld dat Provinciale Staten verordeningen maken die zij in het belang van de provincie acht
- Council Regulation (EC) No 2792/1999 of 17 December 1999 laying down the detailed rules and arrangements regarding Community structural assistance in the fisheries sector [1]
Budget : EUR 3 million for the whole period
Duration : 2000 — 2006
Aid intensity and amount : Within the limits laid down in Regulation (EC) No 2792/1999
Other information : Annual report
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 15.10.2004
Member State : Italy (Liguria)
Aid No : N 401/2002
Title : Programme agreements
Objective : Aid for training and advisory services
Legal basis : Decreto 13 dicembre 2001, n. 2806 della Giunta Regionale della Regione Liguria
Budget : EUR 168169
Duration : 2002-2004
Rate of aid/amount : 100 % or 60 % as appropriate
Other details :
Inform Commission before applying this measure for a particular case
Annual report
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 26.11.2002
Member State : Belgium
Aid No : N 464/2002
Title : Measures to support sea fishing (FIFG part-financing)
Objective : Part-financing of structural assistance for various measures to support sea fishing in Belgium in the period 2000-2006
Legal basis - Decreet van 13 mei 1997 tot oprichting van een Financieringsinstrument voor de Vlaamse visserij- en aquicultuursector;
- Council Regulation (EC) No 2792/1999 of 17 December 1999 laying down the detailed rules and arrangements regarding Community structural assistance in the fisheries sector [2]
Budget : Approximately EUR 500000 per year
Form of aid and intensity : Up to the limits set by Regulation (EC) No 2792/1999
Other details : Annual report
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption : 22.12.2005
Member State : France
Aid No : N 484/2005
Title : Prolongation du programme Eureka ITEA — aide d'État N 73/2000
Objective : Research and development (Limited to electrical and optical equipment — Limited to post and telecommunications — Limited to computer and related activities)
Legal basis : Filière électronique SG(86) D/14441
Budget : 274000000 EUR
Maximum aid intensity : 50 %
Duration : 1.7.1999 — 31.12.2008
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of decision : 6.2.2004
Member State : United Kingdom
Aid No : N 486/2003
Title : Yorkshire and Humber Research and Development Scheme
Objective : R&D
Legal basis : Regional Development Agencies Act 1998
Budget : Approximately GBP 20 million (EUR 28,715 million) of which about GBP 4 million (EUR 5,743 million) in 2004/2005, GBP 4 million (EUR 5,743 million) in 2005/2006, GBP 6 million ( EUR 8,614 million) in 2006/2007 and GBP 6 million (EUR 8,614 million) in 2007/2008
Duration : 1 April 2004 to 31 March 2008
Other information : The United Kingdom has to submit an annual report on the implementation of the scheme including the assessment of incentive effects
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 5.11.2004
Member State : Italy (Friuli — Venezia — Giulia)
Aid No : N 500/2003 and N 98/2004
Title : Interventions in the fisheries and aquaculture sector
Objective : The objectives of both schemes are modernisation of the fleet, aquaculture and freshwater fisheries and processing and marketing facilities
Legal basis : La base giuridica del regime N500/2003 è la delibera della Giunta regionale n. 2255 del 25 luglio 2003. La base giuridica del regime N 98/2004 è la delibera della Giunta regionale n. 2819 del 19 settembre 2003
Budget : EUR 1000000 per year in the period 2003-2005 in N 500/2003 and EUR 500000 EUR per year in the period 2003-2005 in N 98/2004
Form of aid and intensity : Rates of assistance established on the basis of Council Regulation (EC) No 2792/1999, as amended by Council Regulation (EC) No 2369/2002 of 20 December 2002
Period of assistance : 2003-2005
Other information : Annual report
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 15.10.2004
Member State : Italy (Lombardy)
Aid No : N 563/2003
Title : Aid for fishermen on Lake Maggiore
Objective : To compensate fishermen affected by pollution of the lake
Legal basis : Deliberazione n. 14918 del 7 novembre 2003 della Giunta Regionale della Lombardia
Budget : EUR 200000 per year
Duration : 3 years
Rate of aid/amount : Compensation based on fishing carried out for contaminated species
Other details : Report
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 4.10.2002
Member State : Belgium
Aid No : NN 20/2002
Title : Assistance in the region of Wallonie for the fisheries and aquaculture sector not covered by Objective 1, for the period 2000-2006
Objective : Co-financing by the region of Wallonia of structural assistance in the fisheries sector not covered by Objective 1 for the period 2000-2006
Legal basis - Arrêté ministériel du 18 octobre 2001 relatif aux interventions structurelles fédérales en Belgique dans le secteur de la pêche hors objectif 1 pour la période 2000-2006
- Council Regulation (EC) No 2792/1999 of 17 December 1999 laying down the detailed rules and arrangements regarding Community structural assistance in the fisheries sector
Budget : EUR 1,765 million
Duration : 2000 — 2006
Aid intensity and amount : Within the limits laid down in Regulation (EC) No 2792/1999
Other information : Annual report
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
[1] OJ L 337, 30.12.1999, p. 10.
[2] OJ L 337, 30.12.1999, p. 10.
--------------------------------------------------
