Commission Regulation (EC) No 216/2004
of 6 February 2004
amending Regulation (EEC) No 1722/93 laying down detailed rules for the application of Council Regulations (EEC) No 1766/92 and (EEC) No 1418/76 concerning production refunds in the cereals and rice sectors respectively
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 1766/92 of 30 June 1992 on the common organisation of the market in cereals(1), and in particular Article 7(3) thereof,
Having regard to Council Regulation (EEC) No 3072/95 of 22 December 1995 on the common organisation of the market in rice(2), and in particular Article 8(e) thereof,
Whereas:
(1) Commission Regulation (EEC) No 1722/93(3) stipulates how the refund is to be calculated. The market situation for potato starch, being particularly influenced by the minimum price for potatoes, can differ from that for cereal starch. It should therefore be possible to set a different refund for potato starch.
(2) Applications for refund certificates can be lodged each working day before 3 p.m. Brussels time. As this time limit may fall after commencement of a meeting of the Management Committee for Cereals at which refund rates are being decided there is a risk of speculative applications. The time limit should therefore be brought forward.
(3) Regulation (EEC) No 1722/93 stipulates that production refund certificates are valid until the last day of the fifth month following that of issue. This carries a risk that certificates will be issued for over-large volumes; since the Commission can neither reject certificate applications in the event of speculation nor set a different refund rate without consulting the Management Committee for Cereals, the validity of licences should be shortened to end on the last day of the third month following that of issue.
(4) The Commission does not have detailed information on the quantities on which production refunds have been granted until three months after the end of each quarter of the calendar year. To make a better picture of the situation Member States should notify monthly the quantities of starch for which certificate applications have been made.
(5) Regulation (EEC) No 1722/93 should be amended accordingly.
(6) The Management Committee for Cereals has not delivered an opinion within the time limit set by its chairman,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EEC) No 1722/93 is hereby amended as follows:
1. In Article 3(2) the following subparagraph is added:"For potato starch a different rate may be set that reflects the minimum price indicated in Article 8(1) of Regulation (EEC) No 1766/92."
2. In Article 5(1) the second sentence is replaced by:"Applications may be lodged every working day before 13.00 Brussels time."
3. In Article 6(3) the first subparagraph is replaced by:
"3. The refund certificate shall include the information required in accordance with Article 5(2) and state the refund rate and the last day of validity of the licence, which shall be the last day of the third month following the month of issue."
4. Article 12 is replaced by:
"Article 12
Member States shall notify to the Commission:
(a) by the end of the first week of each month, the quantities of starch for which certificate applications as indicated in Article 5(1) were made during the previous month;
(b) within three months of the end of each quarter of the calendar year the type, quantities and origin of starch (maize, wheat, potatoes, barley, oats or rice) on which refunds were paid and the quantities of products for which the starch was used."
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 6 February 2004.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 181, 1.7.1992, p. 21. Regulation last amended by Regulation (EC) No 1104/2003 (OJ L 158, 27.6.2003, p. 1).
(2) OJ L 329, 30.12.1995, p. 18. Regulation last amended by Regulation (EC) No 411/2002 (OJ L 62, 5.3.2002, p. 27).
(3) OJ L 159, 1.7.1993, p. 112. Regulation last amended by Regulation (EC) No 1786/2001 (OJ L 242, 12.9.2001, p. 3).
