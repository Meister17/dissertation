COUNCIL REGULATION (EC) No 3294/94 of 22 December 1994 amending Regulation (EEC) No 302/93 on the establishment of the European Monitoring Centre for Drugs and Drug Addiction
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 235 thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Whereas on 8 February 1993 the Council adopted Regulation (EEC) No 302/93 on the establishment of the European Monitoring Centre for Drugs and Drug Addiction (3);
Whereas the financial provisions relating to bodies set up by the Community should be harmonized;
Whereas Article 11 of the aforementioned Regulation, which concerns the financial provisions governing the Centre, should be amended to take account of this need for harmonization;
Whereas, pursuant to Article 130 of the Financial Regulation of 21 December 1977 applicable to the general budget of the European Communities (4), the provisions of the Financial Regulation should be taken into account as far as possible when adopting the Centre's internal financial rules,
HAS ADOPTED THIS REGULATION:
Article 1
Article 11 (12) of Regulation (EEC) No 302/93 shall be replaced by the following:
'12. After receiving the opinion of the Court of Auditors, the management board shall adopt internal financial provisions laying down in particular the detailed rules for establishing and implementing the Centre's budget.'
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 22 December 1994.
For the Council
The President
H. SEEHOFER
(1) OJ No C 225, 20. 8. 1993, p. 3.
(2) OJ No C 61, 28. 2. 1994, p. 241.
(3) OJ No L 36, 12. 2. 1993, p. 1.
(4) OJ No L 356, 31. 12. 1977, p. 1. Regulation as last amended by Regulation (ECSC, EC, Euratom) No 1923/94 (OJ No L 198, 30. 7. 1994, p. 4).
