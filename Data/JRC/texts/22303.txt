COMMISSION REGULATION (EC) No 641/97 of 14 April 1997 amending Commission Regulation (EC) No 1249/96 of 28 June 1996 on rules of application (cereals sector import duties) for Council Regulation (EEC) No 1766/92
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 1766/92 of 30 June 1992 on the common organization of the market in cereals (1), as last amended by Commission Regulation (EC) No 923/96 (2), and in particular Article 10 (4) thereof,
Whereas provisions on the treatment of cereals imported into the Community are set out in Commission Regulation (EC) No 1249/96 (3);
Whereas experience in applying the provisions of Regulation (EC) No 1249/96 to flint maize imports indicates a need to adjust its provisions on customs control of this product and on the duty reduction to be granted; whereas Regulation (EC) No 1249/96 should therefore be amended;
Whereas the Management Committee for Cereals has not delivered an opinion within the time limit set by its chairman,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 1249/96 is hereby amended as follows:
1. Article 2 (5) is replaced by the following:
'5. Import duties shall be reduced by:
- ECU 14 per tonne on standard high quality common wheat,
- ECU 8 per tonne on malting barley,
- ECU 14 per tonne on flint maize meeting the specifications of Annex I.
For granting of the reduction the following requirements must be met:
(a) The applicant must enter in box 20 of the import licence the processed product to be made from the cereals;
(b) The importer must give a written undertaking along with the import licence application that all the cereals will be processed in line with box 20 of the licence within six months of the date of entry for free circulation. He shall specify the processing location by stating either:
- a processing firm or Member State, or
- a maximum of five processing plants.
Before the goods are consigned for processing a control copy T5 shall be made out in the office of customs clearance in accordance with Commission Regulation (EEC) No 2454/93 (1). The information required under (a) above and the name and location of the processing plant shall be given in box 104 of the T5.
(c) The importer must lodge with the competent authority a security of ECU 14 per tonne for common wheat and flint maize and ECU 8 per tonne for barley. If however the duty in force on the day of completion of the customs import formalities is less than ECU 14 per tonne for common wheat or flint maize or ECU 8 per tonne for barley the security shall be equal to the duty amount.
The security shall be released on production of evidence of the specific final use warranting a quality premium on the price of the basic product mentioned at (a). This evidence, possibly by means of the control copy T5, must demonstrate to the satisfaction of the competent authorities of the Member State of importation that all the cereals imported have been processed into the product indicated at (a).
Processing shall be deemed to have occurred when, within the time limit specified at (b):
- in the case of common wheat the product indicated at (a) has been produced at either:
- one or more of the plants belonging to the firm in the Member State, or
- the plant or one of the plants referred to in (b).
- the malting barley has undergone steeping,
- the flint maize has been processed into a product of CN code 1904 10 10 or 1103 13.`
2. Annex I is replaced by Annex I to this Regulation.
3. The following Article 2a is added:
'Article 2a
1. In the case of imports of flint maize of CN code 1005 90 00 into the Community for which import licence applications were made between 1 July 1996 and the date of entry into force of this Regulation and on which a duty reduction of ECU 8 per tonne was granted, payment shall be made, at the request of the importer or his agent, of the difference between the import duty paid on the quantities actually imported and the amount due had a reduction of ECU 14 per tonne been applied.
2. On request by the party concerned the competent authority of the Member State that issued the import licence shall, in accordance with Article 880 of Regulation (EEC) No 2454/93, issue a certificate, taking the form shown in Annex III, specifying the quantity on which part repayment of the duty as referred to in paragraph 1 may be made.
3. Applications for repayment supported by the certificate referred to in paragraph 2 and the evidence of specific final use referred to in Article 2 (5) (c) must be presented within 30 days of the date of entry into force of this Regulation. They must be accompanied by the import licence, the certificate referred to in paragraph 2 and the declaration of entry for free circulation of the cereals in question.`
4. Annex II to this Regulation is added as Annex III.
5. In Article 6 (1) the first subparagraph is replaced by the following:
'1. Representative samples shall be taken of every consignment of durum wheat, common wheat of standard high or medium quality and flint maize by the customs office of entry for free circulation, the provisions of the Annex to Commission Directive 76/371/EEC (2) applying, for determination of:
- in the case of common wheat of standard high or medium quality, protein content, specific weight and impurity content (Schwarzbesatz) as defined in Council Regulation (EEC) No 2731/75 (3),
- in the case of durum wheat, specific weight, impurity content (Schwarzbesatz) and vitreous grain content,
- in the case of flint maize, flotation index, specific weight and vitreous grain content.
If however the Commission officially recognizes a quality certificate for common wheat, durum wheat or flint maize issued by the country of origin of the cereals samples shall be taken for verification of the certified quality only from a sufficiently representative number of consignments.
Certificates issued by the Argentine Servicio Nacional de Sanidad y Calidad Agroalimentaria (Senasa) shall be officially recognized by the Commission under an administrative cooperation procedure as specified in Articles 63 to 65 of Regulation (EEC) No 2454/93. When the analytical parameters entered on the quality certificate issued by Senasa show conformity with the flint maize quality standards given in Annex I samples shall be taken from at least 3 % of the cargoes arriving at each entry port during the marketing year. A blank specimen of the quality certificates to be issued by Senasa is given in Annex IV. Reproductions of the stamp and signatures authorized by the Argentine Government shall be published in the 'C` series of the Official Journal of the European Communities.`
6. Annex III to this Regulation is added as Annex IV.
7. Article 6 (2) is replaced by:
'The standard methods for the determinations referred to in paragraph 1 shall be those given in Commission Regulations (EEC) Nos 1908/84 (1) and 2731/75.
Flint maize is maize of the species Zea mays indurata the grains of which present a dominantly vitreous endosperm (hard or horny texture). They are generally orange or red. The upper part (opposite the germ), or crown, shows no fissure.
Vitreous grains of flint maize are defined as grains meeting two criteria:
- their crown shows no fissure,
- when cut lengthwise their endosperm shows a central mealy part completely surrounded by a horny part. The horny part must account for the dominant part of the total cut surface.
The vitreous grain percentage shall be established by counting in a representative sample of 100 grains the number meeting these criteria.
The reference method for determining the flotation index is given in Annex V.`
8. Annex IV to this Regulation is added as Annex V.
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 14 April 1997.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ No L 181, 1. 7. 1992, p. 21.
(2) OJ No L 126, 24. 5. 1996, p. 37.
(3) OJ No L 161, 29. 6. 1996, p. 125.
ANNEX I
'ANNEX I
>TABLE>
>TABLE>
ANNEX II
>START OF GRAPHIC>
'ANNEX III
SPECIMEN BLANK CERTIFICATE FOR REPAYMENT OF DUTY UNDER ARTICLE 2A
Import licence No: Holder (name, full address and Member State): Issuing agency (name and address): Rights assigned to (name, full address and Member State): Quantity (in kilograms) for which repayment may be requested under Regulation (EC) No 1249/96: (Date and signature)`
>END OF GRAPHIC>
ANNEX III
>START OF GRAPHIC>
'ANNEX IV
BLANK QUALITY CERTIFICATE FROM SENASA AUTHORIZED BY ARGENTINE GOVERNMENT (ARTICLE 6(1))
REPÚBLICA ARGENTINA
SECRETARÍA DE AGRICULTURA, GANADERÍA, PESCA Y ALIMENTACIÓN
SECRETARY OF AGRICULTURE, LIVESTOCK, FISHERIES AND FOOD
SERVICIO NACIONAL DE SANIDAD Y CALIDAD AGROALIMENTARIA (SENASA)
NATIONAL AGRIFOOD HEALTH AND QUALITY SERVICE
CERTIFICADO DE CALIDAD DE MAÍZ FLINT O PLATA CON DESTINO A LA UNIÓN EUROPEA
QUALITY CERTIFICATE OF FLINT MAIZE OR PLATA MAIZE TO EUROPEAN UNION
MAÍZ FLINT
Grano Cosecha Certificado No Grain Crop Certificate
Exportador
Shipper or Seller
Embarcó en el Puerto de el Loaded at the Port of on
En el vapor Bandera Vessel Flag
Bodega Con destino a Hold Destination
Granel kg
In bulk
Peso total en kilogramos Total weight Embolsado kg
In bags
Calidad
(quality) * Granos de Maíz Flint (%):
* Peso hectolítrico (kg/hl):
* Test de flotación (%):
Definición
(definition)
Maíz flint o maíz plata son los granos de la especie Zea mays que presentan endosperma predominantemente vítreo (textura dura o córnea) con escasa zona almidonosa, generalmente de color colorado y/o anaranjado, sin hendidura en la parte superior o corona.
OBSERVACIONES
REMARQUES
Los datos de calidad (grado) se refieren a la mercadería en conjunto, y no necesariamente a los parciales que de él se extraigan.
The data quality (grade) refers to the grain as a whole, and not necessarily to the sublote obtained therefrom.
Cualquier raspadura, enmienda o agregado invalida este documento.
Any erasure, correction or addendum tenders this document null and void. FIRMA Y SELLO SIGNATURE AND SEAL FIRMA Y SELLO SIGNATURE AND SEAL` >END OF GRAPHIC>
ANNEX IV
'ANNEX V
STANDARD METHOD FOR DETERMINING FLOTATION INDEX (ARTICLE 6(2))
Prepare an aqueous solution of sodium nitrate of specific weight 1,25 and conserve it at 35 °C.
Place in the solution 100 grains of maize from a representative sample of maximum moisture content 14,5 %.
Shake the solution at 30 second intervals for five minutes to eliminate air bubbles.
Separate the floating from the submerged grains and count them.
Flotation index for trial =
>NUM>number of floating grains
>DEN>number of submerged grains
× 100Repeat five times.
The flotation index is the arithmetic mean for the five trials excluding the two extreme values.`
