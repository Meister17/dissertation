Corrigendum to Commission Regulation (EC) No 2256/2004 of 14 October 2004 amending Council Regulation (EC) No 747/2001 as regards Community tariff quotas for certain products originating in Egypt, in Malta and in Cyprus and as regards reference quantities for certain products originating in Malta and in Cyprus
(Official Journal of the European Union L 385 of 29 December 2004)
On page 24, in the first recital,
for:
"… Decision 2004/664/EC (3) …",
read:
"… Decision 2005/89/EC (3) …".
And at the foot of the page, footnote (3),
for:
"(3) OJ L 303, 30.9.2004, p. 28.",
read:
"(3) OJ L 31, 4.2.2005, p. 30."
--------------------------------------------------
