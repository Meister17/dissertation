Text adopted at the XXXIVth Conference of Committees for European and Community Affairs of the European Union Parliaments (COSAC)
London, 11 October 2005
COSAC contribution addressed to the EU institutions
(2005/C 322/01)
Scrutiny of CFSP
1. COSAC welcomes the Council's decision to publish information regarding which Member States participate in which CFSP/ESDP missions and calls on the Council to continue this good practice, which improves openness and helps parliamentary scrutiny.
Better regulation: impact assessments
2.1. COSAC welcomes the better regulation initiative and calls on the Commission to produce an integrated impact assessment for all major initiatives in its work programme, as proposed in the Communication, "Better Regulation for Growth and Jobs in the European Union" (COM(2005) 97 final).
2.2. COSAC calls on the Commission to produce one-page summaries of all its impact assessments to assist in understanding of the material quickly and efficiently; to translate these summaries into all the official Community languages; and to send them directly to the national parliaments without delay.
2.3. COSAC calls on the Commission to send all impact assessments and roadmaps directly to national parliaments and to publish its impact assessments and roadmaps in all the official Community languages. Commission documents should be made available to the public as soon as possible following their adoption by the College.
2.4. COSAC calls on the EU institutions and Member States to develop a common approach to assessing administrative costs.
2.5. COSAC encourages the European Parliament and the Council of Ministers to produce impact assessments for their proposals for substantial amendments to legislative proposals. COSAC suggests that the Commission, the European Parliament and the Council agree under what circumstances a proposed amendment requires an impact assessment. COSAC requests that under the co-decision procedure the Commission update its impact assessments following first reading in the European Parliament, a common position from the Council and second reading in the European Parliament and before the meeting of a conciliation committee.
2.6. COSAC stresses the need for impact assessments to be objective.
2.7. COSAC calls on the Commission to focus its impact assessments on the three elements of the Lisbon Strategy, that is, the economic, social and environmental impacts.
2.8. COSAC requests the Commission to produce impact assessments for those legislative proposals that it proposes to withdraw.
2.9. COSAC calls on the Commission to create a public database to include all the proposals in the annual legislative and work programme, with links to their impact assessments and roadmaps.
Openness in the Council
3. COSAC calls on the Council of Ministers immediately to change its Rules of Procedure so as to provide for its meetings to be in public whenever it considers and votes on draft legislation, in order to reduce the gap between citizens and the Union, to make possible more effective scrutiny of Ministers' decisions by national parliaments and to remedy the intolerable situation whereby legislation is discussed and agreed to in secret.
Subsidiarity and proportionality
4. Those national parliaments which wish to participate shall conduct a subsidiarity and proportionality check on a forthcoming EU legislative proposal or proposals, developing their existing scrutiny role as recognised in the Protocol on the Role of National Parliaments attached to the Treaty of Amsterdam, allowing them to test their systems for reaching decisions on subsidiarity and proportionality, enabling an assessment of the justifications presented by the Commission and stressing to the Commission national parliaments' role in relation to subsidiarity.
Debate on the future of Europe
5. COSAC agrees that overcoming the current EU crisis requires a wide debate involving the citizens of the Union, not only its institutions and elites. Such a debate should take place at all levels — local, regional, national and European. Special responsibility for this endeavour lies with national parliaments and the European Parliament. A series of meetings should seek to stimulate, steer and synthesise the different debates, raise European awareness and lead to a clear definition of the role and objectives of the EU, understood and accepted by European citizens. This would in turn facilitate further decisions on the future of the Constitutional Treaty.
--------------------------------------------------
