Council Decision
of 14 March 2005
amending Decision 2000/256/EC authorising the Kingdom of the Netherlands to apply a measure derogating from Article 11 of the Sixth Directive 77/388/EEC on the harmonisation of the laws of the Member States relating to turnover taxes
(2005/257/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community,
Having regard to the Sixth Council Directive 77/388/EEC of 17 May 1977 on the harmonisation of the laws of the Member States relating to turnover taxes — Common system of value added tax: uniform basis of assessment [1], and in particular Article 27(1) thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) By Decision 2000/256/EC [2] the Council authorised the Kingdom of the Netherlands, by way of derogation from Article 11(A)(1)(a) of Directive 77/388/EEC to include in the taxable amount of a supply of goods or a supply of services, the value of any gold used by the supplier and provided by the recipient in the case where the supply of the gold to the recipient was exempt in accordance with Article 26(b) of Directive 77/388/EEC.
(2) The aim of that derogation was to avoid abuse of the exemption for investment gold and thus to prevent certain types of tax evasion or avoidance.
(3) By letter registered with the Secretariat-General of the Commission on 8 September 2004, the Dutch Government requested an extension for the validity of Decision 2000/256/EC, which expired on 31 December 2004.
(4) In accordance with Article 27(2) of Directive 77/388/EEC, the Commission informed the other Member States by letter dated 22 November 2004 of the request made by the Kingdom of the Netherlands. By letter dated 24 November 2004, the Commission notified the Kingdom of the Netherlands that it had all the information it considered necessary for appraisal of the request.
(5) According to the Dutch authorities, the derogation authorised by Decision 2000/256/EC has been effective in achieving the aims stated above.
(6) The derogations pursuant to Article 27 of Directive 77/388/EEC which counter VAT avoidance linked to the exemption for investment gold may be included in a future proposal for a directive rationalising some of the derogations pursuant to that Article.
(7) It is therefore necessary to extend the validity of the derogation granted under Decision 2000/256/EC until the entry into force of a directive rationalising the derogations pursuant to Article 27 of Directive 77/388/EEC which covers the avoidance of value added tax linked to the exemption for investment gold or until 31 December 2009, whichever is the earlier.
(8) The derogation will have no negative impact on the Community’s own resources provided from VAT,
HAS ADOPTED THIS DECISION:
Article 1
Article 2 of Decision 2000/256/EC shall be replaced by the following:
"Article 2
The authorisation granted under Article 1 shall expire on the date of entry into force of a directive rationalising the derogations pursuant to Article 27 of Directive 77/388/EEC which counter avoidance of value added tax linked to the exemption for investment gold or on 31 December 2009 whichever is the earlier."
Article 2
This Decision shall apply from 1 January 2005.
Article 3
This Decision is addressed to the Kingdom of the Netherlands.
Done at Brussels, 14 March 2005.
For the Council
The President
F. Boden
[1] OJ L 145, 13.6.1977, p. 1. Directive as last amended by Directive 2004/66/EC (OJ L 168, 1.5.2004, p. 35).
[2] OJ L 79, 30.3.2000, p. 36.
--------------------------------------------------
