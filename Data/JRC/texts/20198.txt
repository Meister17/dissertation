COMMISSION DECISION of 13 July 1995 concerning the withdrawal of authorizations for plant protection products containing ferbam or azinphos-ethyl as active substances (95/276/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EEC) No 3600/92 of 11 December 1992 laying down the detailed rules for the implementation of the first stage of the programme of work referred to in Article 8 (2) of Council Directive 91/414/EEC concerning the placing of plant protection products on the market (1), as amended by Regulation (EC) No 491/95 (2), and in particular Article 6 (5) thereof,
Whereas Commission Regulation (EC) No 933/94 (3), as amended by Regulation (EC) No 491/95, has laid down the active substances of plant protection products and designated the rapporteur Member States for the implementation of Regulation (EEC) No 3600/92;
Whereas ferbam and azinphos-ethyl were two of the 90 active substances covered by the first stage of the work programme provided for in Article 8 (2) of Council Directive 91/414/EEC (4);
Whereas for these two substances the designated rapporteur Member States have informed the Commission that the notifiers concerned informed them formally that they will not submit the information required pursuant to Article 6 (1) of Regulation (EEC) No 3600/92 to support the inclusion of an active substance in Annex I to Directive 91/414/EEC; whereas no request was made for granting a new time limit;
Whereas no Member State has informed the Commission of its wish to secure the inclusion of either of these substances in Annex I to Directive 91/414/EEC;
Whereas, therefore, it has to be considered that the data required for re-evaluation of these substances will not be submitted in the framework of the work programme and that therefore an evaluation of these substances is not possible in this framework; whereas, consequently, a decision should be taken with the effect that current authorizations for plant protection products containing these active substances are withdrawn;
Whereas this Decision does not exclude that in future ferbam and azinphos-ethyl may be evaluated in the framework of the proceedings for new active substances provided for in Article 6 of Directive 91/414/EEC;
Whereas the measures provided for in this Decision are in accordance with the opinion of the Standing Committee on Plant Health,
HAS ADOPTED THIS DECISION:
Article 1
The Member States shall ensure:
1. that authorizations for plant protection products containing ferbam or azinphos-ethyl are withdrawn within a period of six months from the date of the present Decision;
2. that from the date of the present Decision no authorizations for plant protection products containing ferbam or azinphos-ethyl will be granted or renewed pursuant to the derogation provided for in Article 8 (2) of Directive 91/414/EEC.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 13 July 1995.
For the Commission Ritt BJERREGAARD Member of the Commission
