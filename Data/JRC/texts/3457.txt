Agreement in the form of an Exchange of Letters
between the European Community and New Zealand pursuant to Article XXIV:6 and Article XXVIII of the General Agreement on Tariffs and Trade (GATT) 1994 relating to the modification of concessions in the schedules of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic in the course of their accession to the European Union
A. Letter from the European Community
Brussels,
Sir,
Following the initiation of negotiations between the European Communities (EC) and New Zealand under Article XXIV:6 and Article XXVIII of GATT 1994 for the modification of concessions in the schedules of Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic in the course of their accession to the EC, the following is agreed between the EC and New Zealand with a view to concluding the negotiations opened following the EC’s notification of 19 January 2004 to the WTO pursuant to Article XXIV:6 of GATT 1994.
The EC agrees to incorporate in its schedule for the customs territory of EC 25, the concessions that were included in its previous schedule.
The EC agrees that it will incorporate in its schedule for the EC 25 the concessions contained in the annex to this agreement.
New Zealand accepts the basic components of the EC’s approach to adjusting the GATT obligations of the EC-15 and those of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic following the recent enlargement of the EC: netting out of export commitments; netting out of tariff quotas; and aggregation of domestic support commitments. The applicable legal modalities for implementation shall draw on the precedent from the last EU enlargement.
This agreement shall enter into force on the date of New Zealand’s letter in reply indicating its agreement, following consideration by the parties in accordance with their own procedures. The EC undertakes to use its best endeavours to ensure that the appropriate implementing measures are in place as soon as possible, though no later than 1 January 2006.
Consultations may be held at any time with regard to any matter in this Agreement at the request of either Party.
Please accept, Sir, the assurance of my highest consideration.
On behalf of the European Community
+++++ TIFF +++++
ANNEX
- add 1154 tonnes (carcase weight) to the allocation for New Zealand under the EC tariff rate quota for sheep meat; "meat of sheep or goats, fresh, chilled or frozen",
- add 735 tonnes to the allocation for New Zealand under the EC tariff rate quota for butter; "butter of New Zealand origin, at least 6 weeks old, with a fat content of not less than 80 % but less than 82 % by weight, manufactured directly from milk or cream",
- add 1000 tonnes to the tariff rate quota for "high quality" beef; "selected chilled or frozen premium beef cuts derived from exclusively pasture-grazed bovine animals which do not have more than four permanent incisor teeth in wear, the carcases of which have a dressed weight of not more than 325 kilograms, a compact appearance with a good eye of meat of light and uniform colour and adequate but not excessive fat cover. All cuts will be vacuum packaged and referred to as high quality beef".
B. Letter from New Zealand
Brussels,
Sir,
Reference is made to your letter stating:
"Following the initiation of negotiations between the European Communities (EC) and New Zealand under Article XXIV:6 and Article XXVIII of GATT 1994 for the modification of concessions in the schedules of Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic in the course of their accession to the EC, the following is agreed between the EC and New Zealand with a view to concluding the negotiations opened following the EC’s notification of 19 January 2004 to the WTO pursuant to Article XXIV:6 of GATT 1994.
The EC agrees to incorporate in its schedule for the customs territory of EC 25, the concessions that were included in its previous schedule.
The EC agrees that it will incorporate in its schedule for the EC 25 the concessions contained in the annex to this agreement.
New Zealand accepts the basic components of the EC’s approach to adjusting the GATT obligations of the EC-15 and those of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic following the recent enlargement of the EC: netting out of export commitments; netting out of tariff quotas; and aggregation of domestic support commitments. The applicable legal modalities for implementation shall draw on the precedent from the last EU enlargement.
This agreement shall enter into force on the date of New Zealand’s letter in reply indicating its agreement, following consideration by the parties in accordance with their own procedures. The EC undertakes to use its best endeavours to ensure that the appropriate implementing measures are in place as soon as possible, though no later than 1 January 2006.
Consultations may be held at any time with regard to any matter in this Agreement at the request of either Party."
I hereby have the honour to express my government’s agreement.
Please accept, Sir, the assurance of my highest consideration.
On behalf of The Government of New Zealand
+++++ TIFF +++++
--------------------------------------------------
