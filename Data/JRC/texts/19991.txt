DECISION OF THE COUNCIL AND THE COMMISSION of 10 April 1995 concerning the conclusion of the Agreement between the European Communities and the Government of the United States of America regarding the application of their competition laws (95/145/EC, ECSC)
THE COUNCIL OF THE EUROPEAN UNION,
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community, and in particular Articles 87 and 235, in conjunction with the first subparagraph of Article 228 (3) thereof,
Having regard to the Treaty establishing the European Coal and Steel Community, and in particular Articles 65 and 66 thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament (1),
Whereas Article 235 of the Treaty establishing the European Community must be invoked owing to the inclusion in the text of the Agreement of mergers and acquisitions which are covered by Council Regulation (EEC) No 4064/89 of 21 December 1989 on the control of concentrations between undertakings (2), which is essentially based on Article 235;
Whereas, given the increasingly pronounced international dimension to competition problems, international cooperation in this field should be strengthened;
Whereas, to this end, the Commission has negotiated an Agreement with the Government of the United States of America on the application of the competition rules of the European Communities and of the United States of America;
Whereas the Agreement, including the exchange of interpretative letters, should be approved,
HAVE DECIDED AS FOLLOWS:
Article 1
The Agreement between the European Communities and the Government of the United States of America regarding the application of their competition laws, including the exchange of interpretative letters, is hereby approved on behalf of the European Community and the European Coal and Steel Community.
The texts of the Agreement and of the exchange of interpretative letters, drawn up in the English language, are attached to this Decision.
Article 2
The Agreement shall apply with effect from 23 September 1991.
Article 3
The President of the Council is hereby authorized to designate the person(s) empowered to notify the Government of the United States of America of approval of the Agreement, on behalf of the European Community, and to sign the exchange of interpretative letters.
The Commission shall designate the person(s) empowered to notify the Government of the United States of America of approval of the Agreement, on behalf of the European Coal and Steel Community, and to sign the exchange of interpretative letters.
Done at Luxembourg, 10 April 1995.
For the Council
The President
J. PUECH For the Commission
The President
J. SANTER
(1) Opinion delivered on 20 January 1995 (OJ No C 43, 20. 2. 1995) and 17 March 1995 (OJ No C 89, 10. 4. 1995).
(2) OJ No L 395, 30. 12. 1989, p. 1 (corrected version: OJ No L 257, 21. 9. 1990, p. 13).
