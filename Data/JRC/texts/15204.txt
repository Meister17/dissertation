Authorisation for State aid pursuant to Articles 87 and 88 of the EC Treaty
Cases where the Commission raises no objections
(2006/C 26/05)
Date of adoption : 2.3.2005
Member State : Germany (whole territory)
Aid No : N 1/2005
Title : Navigation and Maritime Technology for the 21st Century — Prolongation
Objective : R&D aid (infrastructures, maritime technology, shipbuilding)
Legal basis : Jährlicher Bundeshaushaltplan für den Geschäftsbereich des BMBF, Einzelplan 30
Budget : EUR 103,2 million for the whole duration
Intensity or amount Basic intensities: fundamental research — up to100 %,
industrial research — 50 %,
precompetitive development — 25 %;
Feasibility studies:
- 75 % preparatory to industrial research,
- 50 % preparatory to precompetitive development;
Bonuses: 10 % SMEs,
10 % assisted areas in accordance with Article 87(3)(a) of the EC Treaty,
10 % if one of the conditions of point 5.10.4 of the R&D Framework is fulfilled;
Maximum ceilings (including bonuses and cumulation): 75 % for industrial research and 50 % for precompetitive development
Duration : until 31.12.2010
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of decision : 27.7.2005
Member State : Estonia
Aid No : N 282/2005
Title : Amendments to tourism product development and marketing support scheme (ex No. EE 8/2003)
Objective(s) : Regional aid
Legal basis 1) EAS turismiettevõtjate tootearenduse ja turunduse programm ( 13.11.2003).
2) Majandus- ja Kommunikatsiooniministri 13. aprilli 2005 a. määrus nr 45 (RTL 26.4.2005, 45, 624), millega muudetakse 7. mai 2004 a. Majandus- ja Kommunikatsiooniministri määrust nr 126 (RTL 19.5.2004, 62, 1034)
Budget : EUR 3,09 million
Aid intensity or amount : 40 % in North Estonia; 50 % in other regions
Duration : 31.12.2006
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption : 7.12.2005
Member State : France
Aid No : N 372/2005
Title (in original language) : Projet d'aide au moteur franco-russe SaM 146
Objective : Research and development (Limited to the manufacturing industry)
Legal basis : Décret 99-160 du 16 décembre 1999 relatif aux subventions de l'État pour des projet d'investissement
Budget : EUR 140 million
Aid intensity or amount : 25 %
Duration : 2005-2008
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption : 8.11.2005
Member State : United Kingdom
Aid No : N 425/2005
Title : "Prolongation Viridian Growth Fund"
Objective : unchanged (the scheme is intended to address perceived gaps in the provision of venture capital funding for SMEs established in Northern Ireland)
Legal basis : unchanged
Budget : unchanged: (GBP 10 million)
Duration : extended to 31.12.2008
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption : 29.11.2005
Member State : Italy — Piemonte Region
Aid No : N 483/2005
Title : "EOS project"
Objective : the scheme is intended to fund precompetitive development of CHP production technologies from SOFC (Solid Oxide Fuel Cells)
Legal basis : Deliberazione della Giunta Regionale della Regione Piemonte n. 43-825 del 12 settembre 2005
Budget : EUR 1,5 million
Intensity : 25 % GGE increased by 10 plus 5 percentage points according respectively to point 5.10.4(b) and 5.10.2 of the R&D guidelines
Duration : 4 years
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption : 29.11.2005
Member State : Greece
Aid No : N 488/2005
Title : "Modification to the aid scheme N 547/2001 — Effective cooperation in R&D" (Research consortia)
Objective : unchanged (the scheme aims at promoting innovation in the greek economy through stimulating enterprises to implement basic, industrial and initial demonstration research in cooperation with R&D entities)
Legal basis : Προεδρικό Διάταγμα 274/ 6.10.2000 (ΠΔ 274/2000) όπως αυτό τροποποιήθηκε από το ΠΔ 103/2003
Budget : increased from EUR 106 to 150 million
Intensity : unchanged: 100, 50 and 25 % respectively for fundamental research, industrial research and precompetitive activities for large enterprises in Article 87(3)(a) assisted areas, plus 10 percentage points bonus for SMEs
Duration : unchanged (until 31.12.2006)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
--------------------------------------------------
