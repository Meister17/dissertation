Judgment of the Court (Third Chamber) of 6 July 2006 (reference for a preliminary ruling from the Cour de cassation — Belgium) — Axel Kittel v Belgian State
(Joined Cases C-439/04 and C-440/04) [1]
Referring court
Cour de cassation
Parties to the main proceedings
Applicants: Axel Kittel (C-439/04) Belgian State (C-440/04)
Defendants: Belgian State (C-439/04) Recolta Recycling SPRL (C-440-04)
Re:
Reference for a preliminary ruling — Belgian Cour de Cassation — Interpretation of Sixth Council Directive 77/388/EEC of 17 May 1977 on the harmonisation of the laws of the Member States relating to turnover taxes — Common system of value added tax: uniform basis of assessment (OJ 1977 L 145, p. 1) — Principle of fiscal neutrality — Supplies of goods under a contract of sale which is incurably void — Carousel fraud — Loss of the right to deduct for a purchaser in good faith
Operative part of the judgment
Where a recipient of a supply of goods is a taxable person who did not and could not know that the transaction concerned was connected with a fraud committed by the seller, Article 17 of Sixth Council Directive 77/388/EEC of 17 May 1977 on the harmonisation of the laws of the Member States relating to turnover taxes — Common system of value added tax: uniform basis of assessment, as amended by Council Directive 95/7/EC of 10 April 1995, must be interpreted as meaning that it precludes a rule of national law under which the fact that the contract of sale is void — by reason of a civil law provision which renders that contract incurably void as contrary to public policy for unlawful basis of the contract attributable to the seller — causes that taxable person to lose the right to deduct the value added tax he has paid. It is irrelevant in this respect whether the fact that the contract is void is due to fraudulent evasion of value added tax or to other fraud.
By contrast, where it is ascertained, having regard to objective factors, that the supply is to a taxable person who knew or should have known that, by his purchase, he was participating in a transaction connected with fraudulent evasion of value added tax, it is for the national court to refuse that taxable person entitlement to the right to deduct.
[1] OJ C 6, 08.01.2005.
--------------------------------------------------
