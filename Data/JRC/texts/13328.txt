Euro exchange rates [1]
1 November 2006
(2006/C 268/02)
| Currency | Exchange rate |
USD | US dollar | 1,2757 |
JPY | Japanese yen | 149,24 |
DKK | Danish krone | 7,4547 |
GBP | Pound sterling | 0,66845 |
SEK | Swedish krona | 9,2017 |
CHF | Swiss franc | 1,5880 |
ISK | Iceland króna | 86,57 |
NOK | Norwegian krone | 8,3005 |
BGN | Bulgarian lev | 1,9558 |
CYP | Cyprus pound | 0,5770 |
CZK | Czech koruna | 28,066 |
EEK | Estonian kroon | 15,6466 |
HUF | Hungarian forint | 259,34 |
LTL | Lithuanian litas | 3,4528 |
LVL | Latvian lats | 0,6963 |
MTL | Maltese lira | 0,4293 |
PLN | Polish zloty | 3,8633 |
RON | Romanian leu | 3,5125 |
SIT | Slovenian tolar | 239,51 |
SKK | Slovak koruna | 36,275 |
TRY | Turkish lira | 1,8575 |
AUD | Australian dollar | 1,6480 |
CAD | Canadian dollar | 1,4471 |
HKD | Hong Kong dollar | 9,9232 |
NZD | New Zealand dollar | 1,8976 |
SGD | Singapore dollar | 1,9900 |
KRW | South Korean won | 1198,39 |
ZAR | South African rand | 9,3850 |
CNY | Chinese yuan renminbi | 10,0428 |
HRK | Croatian kuna | 7,3612 |
IDR | Indonesian rupiah | 11612,06 |
MYR | Malaysian ringgit | 4,6565 |
PHP | Philippine peso | 63,581 |
RUB | Russian rouble | 34,0690 |
THB | Thai baht | 46,763 |
[1] Source: reference exchange rate published by the ECB.
--------------------------------------------------
