Commission Regulation (EC) No 2348/2000
of 23 October 2000
amending Regulation (EC) No 2799/1999 laying down detailed rules for applying Council Regulation (EC) No 1255/1999 as regards the grant of aid for skimmed milk and skimmed-milk powder intended for animal feed and the sale of such skimmed-milk powder
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1255/1999 of 17 May 1999 on the common organisation of the market in milk and milk products(1), as last amended by Regulation (EC) No 1670/2000(2), and in particular Articles 10 and 15 thereof,
Whereas:
(1) Article 7(1) of Commission Regulation (EC) No 2799/1999(3), as last amended by Regulation (EC) No 1550/2000(4), fixes the amount of aid for skimmed milk and skimmed-milk powder intended for animal feed. In view of developments in the supply situation for skimmed milk and skimmed-milk powder, the amount of aid should be reduced.
(2) The Management Committee for Milk and Milk Products has not delivered an opinion within the time limit set by its chairman,
HAS ADOPTED THIS REGULATION:
Article 1
Article 7(1) of Regulation (EC) No 2799/1999 is replaced by the following:
"1. Aid is hereby fixed at:
(a) EUR 4,93 per 100 kg of skimmed milk with a protein content of not less than 35,6 % of the non-fatty dry extract;
(b) EUR 4,35 per 100 kg of skimmed milk with a protein content of not less than 31,4 % but less than 35,6 % of the non-fatty dry extract;
(c) EUR 61,00 per 100 kg of skimmed-milk powder with a protein content of not less than 35,6 % of the non-fatty dry extract;
(d) EUR 53,80 per 100 kg of skimmed-milk powder with a protein content of not less than 31,4 % but less than 35,6 % of the non-fatty dry extract."
Article 2
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 23 October 2000.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 160, 26.6.1999, p. 48.
(2) OJ L 193, 29.7.2000, p. 10.
(3) OJ L 340, 31.12.1999, p. 3.
(4) OJ L 176, 15.7.2000, p. 25.
