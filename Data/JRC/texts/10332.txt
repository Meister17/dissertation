Commission Regulation (EC) No 131/2006
of 26 January 2006
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 27 January 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 26 January 2006.
For the Commission
J. L. Demarty
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 386/2005 (OJ L 62, 9.3.2005, p. 3).
--------------------------------------------------
ANNEX
to Commission Regulation of 26 January 2006 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code | Standard import value |
07020000 | 052 | 109,1 |
204 | 46,7 |
212 | 97,4 |
624 | 140,9 |
999 | 98,5 |
07070005 | 052 | 138,3 |
204 | 101,5 |
999 | 119,9 |
07091000 | 220 | 80,1 |
624 | 101,2 |
999 | 90,7 |
07099070 | 052 | 147,8 |
204 | 139,4 |
999 | 143,6 |
08051020 | 052 | 43,4 |
204 | 55,6 |
212 | 48,0 |
220 | 51,3 |
624 | 58,2 |
999 | 51,3 |
08052010 | 204 | 78,4 |
999 | 78,4 |
08052030, 08052050, 08052070, 08052090 | 052 | 62,6 |
204 | 98,5 |
400 | 86,7 |
464 | 148,0 |
624 | 75,3 |
662 | 32,0 |
999 | 83,9 |
08055010 | 052 | 58,6 |
220 | 60,5 |
999 | 59,6 |
08081080 | 400 | 132,0 |
404 | 106,8 |
720 | 67,9 |
999 | 102,2 |
08082050 | 388 | 109,6 |
400 | 82,3 |
720 | 37,7 |
999 | 76,5 |
--------------------------------------------------
