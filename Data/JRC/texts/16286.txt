COMMISSION DECISION of 29 January 1996 amending Decisions 94/187/EC, 94/309/EC, 94/344/EC, 94/446/EC, 95/341/EC and 95/343/EC laying down the animal health requirements and certification for the import of certain products and amending Decision 95/340/EC drawing up a provisional list of third countries from which Member States authorize imports of milk and milk products (Text with EEA relevance) (96/106/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 92/118/EEC of 17 December 1992 laying down animal health and public health requirements governing trade in and imports into the Community of products not subject to the said requirements laid down in specific Community rules referred to in Annex A (I) to Directive 89/662/EEC and, as regards pathogens, to Directive 90/425/EEC (1) as last amended by Commission Decision 96/103/EC (2), and in particular Article 10 paragraph 2 (c) thereof,
Having regard to Council Directive 92/46/EEC of 16 June 1992 laying down the health rules for the production and placing on the market of raw milk, heat treated milk and milk based products (3), as last amended by the Act of Accession for Austria, Finland and Sweden and in particular Articles 23 (2) and (3) thereof,
Whereas Commission Decisions 94/187/EC (4), 94/309/EC (5), 94/344/EC (6) and 94/446/EC (7), as last amended by Decision 95/230/EC (8), respectively lay down the animal health conditions and the veterinary certification for imports from third countries of animal casings, certain petfoods and certain untanned edible products for pets, containing low risk materials, processed animal protein including products containing this protein intended for animal consumption, and, bones and bone products, horns and horn products and hooves and hoof products for further processing not intended for human animal consumption; whereas Decision 95/230/EC amended these Decisions providing for their application from 2 February 1996;
Whereas Commission Decision 95/340/EC (9) draws up a provisional list of third countries from which Member States authorize imports of milk and milk-based products; whereas Commission Decision 95/341/EC (10) establishes the animal health conditions and veterinary certification for imports of milk and milk-based products, not intended for human consumption; Whereas Commission Decision 95/343/EC (11) establishes the animal health conditions and veterinary certification for imports of milk and milk-based products intended for human consumption; whereas the date of application of these Decisions is 2 February 1996;
Whereas it appears that third countries will not be able to fulfill the new import conditions by that date; whereas in order to avoid disruptions in trade, it is necessary to postpone the date of application of those Decisions;
Whereas Decisions 94/187/EC, 94/309/EC, 94/344/EC, 94/446/EC, 95/340/EC, 95/341/EC and 95/343/EC must be amended accordingly;
Whereas the measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
In Article 2 of Decision 94/187/EC, the date of '2 February 1996` is replaced by '1 January 1997`.
Article 2
In Article 2 of Decision 94/309/EC, the date of '2 February 1996` is replaced by '1 January 1997`.
Article 3
In Article 2 of Decision 94/344/EC, the date of '2 February 1996` is replaced by '1 January 1997`.
Article 4
In Article 4 of Decision 94/446/EC, the date of '2 February 1996` is replaced by '1 January 1997`.
Article 5
In Article 5 of Decision 95/340/EC, the date of '2 February 1996` is replaced by '1 May 1996`.
Article 6
In Article 3 of Decision 95/341/EC the date of '2 February 1996` is replaced by '1 May 1996`.
Article 7
In Article 8 of Decision 95/343/EC, the date of '2 February 1996` is replaced by '1 January 1997`.
Article 8
This Decision is addressed to the Member States.
Done at Brussels, 29 January 1996.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ No L 62, 15. 3. 1993, p. 49.
(2) See page 28 of this Official Journal.
(3) OJ No L 268, 14. 9. 1992, p. 1.
(4) OJ No L 89, 6. 4. 1994, p. 18.
(5) OJ No L 137, 1. 6. 1994, p. 62.
(6) OJ No L 154, 21. 6. 1994, p. 45.
(7) OJ No L 183, 19. 7. 1994, p. 46.
(8) OJ No L 154, 5. 7. 1995, p. 19.
(9) OJ No L 200, 24. 8. 1995, p. 38.
(10) OJ No L 200, 24. 8. 1995, p. 42.
(11) OJ No L 200, 24. 8. 1995, p. 52.
