COMMISSION DECISION of 25 November 1996 amending Decision 93/436/EEC laying down specific conditions for importing fishery and aquaculture products originating in Chile (Text with EEA relevance) (96/674/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/493/EEC of 22 July 1991 laying down the health conditions for the production and the placing on the market of fishery products (1), as last amended by the Act of Accession of Austria, Finland and Sweden, and in particular Article 11 (5) thereof,
Whereas the list of establishments and factory ships approved by Chile for importing fishery products into the Community has been drawn up in Commission Decision 93/436/EEC of 30 June 1993 laying down specific conditions for importing fishery and aquaculture products originating in Chile (2), as last amended by Decision 96/220/EC (3); whereas this list may be amended following the communication of a new list by the competent authority in Chile;
Whereas the name of the competent authority in Chile has changed; whereas Annex A to the Decision 93/436/EEC should be amended as a consequence;
Whereas the competent authority in Chile has communicated a new list of 103 establishments and 19 factory ships;
Whereas it is necessary to amend the list of approved establishments and factory ships accordingly;
Whereas the measures provided for in this Decision have been drawn up in accordance with the procedure laid down by Commission Decision 90/13/EEC (4),
HAS ADOPTED THIS DECISION:
Article 1
The Servicio Nacional de Pesca (Sernapesca) shall be the competent authority in Chile for verifying and certifying compliance of fishery and aquaculture products with the requirements of Directive 91/493/EEC.
Article 2
Annex A to Decision 93/436/EEC is replaced by Annex A to this Decision.
Article 3
Annex B to Decision 93/436/EEC is replaced by Annex B to this Decision.
Article 4
This Decision is addressed to the Member States.
Done at Brussels, 25 November 1996.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ No L 268, 24. 9. 1991, p. 15.
(2) OJ No L 202, 12. 8. 1993, p. 31.
(3) OJ No L 74, 22. 3. 1996, p. 35.
(4) OJ No L 8, 11. 1. 1990, p. 70.
ANNEX A
'ANNEX A
>START OF GRAPHIC>
HEALTH CERTIFICATE
for fishery or aquaculture products originating in Chile and intended for export to the European Community
Reference No:
Country of dispatch: Chile
Competent authority: Servicio Nacional de Pesca (Sernapesca)
I. Details identifying the fishery products
Description of fishery/acquaculture products (1)
- species (scientific name):
- presentation of product and type of treatment (2):
Code number (where available):
Type of packaging:
Number of packages:
Net weight:
Requisite storage and transport temperature:
II. Origin of products
Name(s) and official approval number(s) of establishment(s) approved by the Sernapesca for exports to the EC:
III. Destination of products
The products are dispatched
from: (place of dispatch)
to: (country and place of destination)
by the following means of transport:
Name and address of dispatcher:
Name of consignee and address at place of destination:
(1) Delete where applicable.
(2) Live, refrigerated, frozen, salted, smoked, preserved, etc.
IV. Health attestation
The official inspector hereby certifies that the fishery and aquaculture products specified above:
1. were caught and handled on board vessels in accordance with the health rules laid down by Directive 92/48/EEC;
2. were landed, handled and, where appropriate, packaged, prepared, processed, frozen, thawed and stored hygienically in compliance with the requirements laid down in Chapters II, III and IV of the Annex to Directive 91/493/EEC;
3. have undergone health controls in accordance with Chapter V of the Annex to Directive 91/493/EEC;
4. are packaged, marked, stored and transported in accordance with Chapters VI, VII and VIII of the Annex to Directive 91/493/EEC;
5. do not come from toxic species or species containing biotoxins;
6. have satisfactorily undergone the organoleptic, parasitological, chemical and microbiological checks laid down for certain categories of fishery products by Directive 91/493/EEC and in the implementing decisions thereto;
7. in addition, where the fishery products are frozen or processed bivalve molluscs: the molluscs were obtained from approved production areas laid down by Annex to Decision 96/674/EC laying down special conditions for the import of bivalve molluscs echinoderms, tunicates, and marine gastropods originating in Chile.
The undersigned official inspector hereby declares that he is aware of the provisions of Directive 91/493/EEC, Directive 92/48/EEC and Decision 96/674/EC.
Done at ,
(Place)
(Date)
(Signature of official inspector) (1)
Official stamp (1)
(Name in capital letters, capacity and qualifications of person signing)
(1) The colour of the stamp and signature must be different from that of the other particulars in the certificate.`
>END OF GRAPHIC>
ANNEX B
'ANNEX B
LIST OF APPROVED ESTABLISHMENTS AND FACTORY SHIPS
I. Establishments
>TABLE>
II. Factory ships
>TABLE>
