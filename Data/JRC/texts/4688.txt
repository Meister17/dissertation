Decision of the Representatives of the Governments of the Member States
of 20 July 2005
appointing a judge to the Court of Justice of the European Communities
(2005/595/EC, Euratom)
THE REPRESENTATIVES OF THE GOVERNMENTS OF THE MEMBER STATES OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community, and in particular Article 223 thereof,
Having regard to the Treaty establishing the European Atomic Energy Community, and in particular Article 139 thereof,
Whereas:
HAVE DECIDED AS FOLLOWS:
Article 1
Mr Lars BAY LARSEN is hereby appointed judge to the Court of Justice of the European Communities from the date of his swearing in until 6 October 2009.
Article 2
This Decision shall be published in the Official Journal of the European Union.
Done at Brussels, 20 July 2005.
The President
J. Grant
--------------------------------------------------
