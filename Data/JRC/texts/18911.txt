Council Regulation (EC) No 1786/2003
of 29 September 2003
on the common organisation of the market in dried fodder
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Articles 36 and the third subparagraph of Article 37(2) thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament(1),
Having regard to the opinion of the European Economic and Social Committee(2),
Having regard to the Opinion of the Committee of the Regions(3),
Whereas:
(1) Council Regulation (EC) No 603/95 of 21 February 1995 of the common organisation of the market in dried fodder(4) establishes a common organisation of that market with aid granted at two flat rates, one for dehydrated fodder and one for sun-dried fodder.
(2) Regulation (EC) No 603/95 has been substantially amended several times. As a consequence of further amendments it should be repealed and replaced in the interests of clarity.
(3) The main part of fodder production under the scheme established by Regulation (EC) No 603/95 relies on the use of fossil fuel for dehydrating and, in some Member States, on the use of irrigation. Due to concerns about its effects on the environment, the scheme should be amended.
(4) Council Regulation (EC) No 1782/2003 of 29 September 2003 establishing common rules for direct support schemes under the common agricultural policy and establishing support schemes for farmers(5).
(5) Following these elements, the two aid rates set by Regulation (EC) No 603/95 should be reduced to a single rate applicable to both dehydrated and sun-dried fodder.
(6) Since production in southern countries begins in April, the marketing year for dried fodder on which aid is granted should be from 1 April to 31 March.
(7) To guarantee budget neutrality for dried fodder there should be a ceiling for the volume of Community production. To that end a maximum guaranteed quantity should be set covering both dehydrated and sun-dried fodder.
(8) That quantity should be divided among the Member States on the basis of the historical quantities recognised for the purposes of Regulation (EC) No 603/95.
(9) To secure respect for the guaranteed maximum quantity and discourage excess production throughout the Community, the aid should be reduced if that quantity is exceeded. That reduction should be applied in each Member State which has exceeded its guaranteed national quantity, in proportion to the excess recorded for it.
(10) The aid amount finally due cannot be paid until it is known whether the guaranteed maximum quantity has been exceeded. An advance on the aid should therefore be paid once the dried fodder has left the processor.
(11) Minimum quality requirements for entitlement to the aid should be set.
(12) To encourage a steady flow of green fodder to processors, eligibility for the aid should in certain cases require conclusion of a contract between producers and processing undertakings.
(13) To promote transparency of the production chain and facilitate essential checking, certain particulars in contracts should be made compulsory.
(14) To receive the aid, processors should therefore be required to keep stock records providing necessary information for checking entitlement and to furnish any other supporting document needed.
(15) Where there is no contract between the producers and the processing undertakings, the latter should have to provide other information allowing entitlement to be checked.
(16) It should be ensured that, where a contract is a special-order one for processing of fodder delivered by the grower, the aid is passed back to him.
(17) The proper working of a single market in dried fodder would be jeopardised by the granting of national aid. Therefore, the provisions of the Treaty governing State aid should apply to the products covered by this common market organisation.
(18) In view of simplification, the committee assisting the Commission should be the Management Committee for Cereals.
(19) The measures necessary for the implementation of this Regulation should be adopted in accordance with Council Decision 1999/468/EC of 28 June 1999 laying down the procedures for the exercise of implementing powers conferred on the Commission(6).
(20) The internal market and the custom duties could, in exceptional circumstances, prove inadequate, In such cases, so as not to leave the Community market without defence against disturbances that might ensue, the Community should be able to take all necessary measures without delay. All such measures should be in conformity with the Community's international obligations.
(21) In order to take account of possible evolution of the dried fodder production, the Commission should, on the basis of an evaluation of the common market organisation for dried fodder, present a report to the Council on the sector dealing in particular with the development of areas of leguminous and other green fodder, the production of dried fodder and the savings of fossil fuels achieved. The report should be accompanied, if needed, by appropriate proposals.
(22) Expenditure incurred by the Member States as a result of the obligations arising from the application of this Regulation should be financed by the Community in accordance with Council Regulation (EC) No 1258/1999 of 17 May 1999 on the financing of the common agricultural policy(7).
(23) Due to the application of the single payment scheme from 1 January 2005, this scheme should apply from 1 April 2005,
HAS ADOPTED THIS REGULATION:
CHAPTER I
INTRODUCTORY PROVISIONS
Article 1
The common organisation of the market in dried fodder shall be established and cover the following products:
>TABLE>
Article 2
The marketing year for the products listed in Article 1 shall begin on 1 April and end on 31 March of the following year.
Article 3
This Regulation shall apply without prejudice to the measures provided for by Council Regulation (EC) No 1782/2003.
CHAPTER II
AID
Article 4
1. Aid shall be granted for the products listed in Article 1.
2. Without prejudice to Article 6, the aid shall be set at EUR 33/t.
Article 5
1. A maximum guaranteed quantity (MGQ) per marketing year of 4855900 tonnes of dehydrated and/or sun-dried fodder for which the aid provided for in Article 4(2) may be granted is hereby established.
2. The maximum guaranteed quantity provided for in paragraph 1 shall be divided among the Member States as follows:
Guaranteed national quantities (tonnes)
>TABLE>
Article 6
Where during a marketing year the volume of dried fodder for which aid as provided for in Article 4(2) is claimed exceeds the guaranteed maximum quantity set out in Article 5(1), the aid to be paid in that marketing year shall be reduced in each Member State in which production exceeds the guaranteed national quantity by a percentage proportionate to that excess.
The reduction shall be set, in accordance with the procedure referred to in Article 18(2), at a level ensuring that budget expenditure expressed in euros does not exceed that what would been attained if the guaranteed maximum quantity had not been exceeded.
Article 7
1. Processing undertakings who apply for aid under this Regulation shall be entitled to an advance payment of EUR 19,80 per tonne, or EUR 26,40 per tonne if they have lodged a security of EUR 6,60 per tonne.
Member States shall make the necessary checks to verify entitlement to the aid. Once entitlement has been established the advance shall be paid.
However, the advance may be paid before entitlement has been established provided the processor lodges a security equal to the amount of the advance plus 10 %. This security shall also serve as security for the purposes of the first subparagraph. It shall be reduced to the level specified in the first subparagraph as soon as entitlement to aid has been established and shall be released in full when the balance of the aid is paid.
2. Before an advance can be paid the dried fodder must have left the processing undertaking.
3. Where an advance has been paid, the balance amounting to the difference between the amount of the advance and the total aid due to the processing undertaking shall be paid subject to application of Article 6.
4. Where the advance exceeds the total to which the processing undertaking is entitled following the application of Article 6, the processor shall reimburse the excess to the competent authority of the Member State on request.
Article 8
At the latest by 31 May of each year, Member States shall notify the Commission of the quantities of dried fodder that in the previous marketing year were eligible for aid as provided for in Article 4(2).
Article 9
The aid provided for in Article 4(2) shall be paid on application from the party concerned, in respect of dried fodder that has left the processing plant and meets the following requirements:
(a) its maximum moisture content is from 11 % to 14 % which may vary depending on the presentation of the product;
(b) its minimum total crude protein content in the dry matter not less than:
(i) 15 % for the products referred to in point (a) and the second indent of point (b) in Article 1;
(ii) 45 % for the products referred to in the first indent of point (b) in Article 1;
(c) it is of sound and fair merchantable quality.
Further requirements, in particular on carotene and fibre content, may be adopted in accordance with the procedure referred to in Article 18(2).
Article 10
Aid as provided for in Article 4(2) shall only be granted to undertakings processing the products listed in Article 1 which comply with the following conditions:
(a) they keep stock records containing at least the following information:
(i) the quantities of green fodder and, where applicable, sun-dried fodder processed; however, where the particular circumstances of the undertaking so require, quantities may be estimated on the basis of areas sown;
(ii) the quantities of dried fodder produced and the quantities, with their quality, that leave the processor;
(b) they provide any other supporting documents needed for verifying entitlement to the aid;
(c) they fall into at least one of the following categories:
(i) processors who have concluded contracts with producers of fodder for drying;
(ii) undertakings which have processed its own crop or, in the case of a group, that of its members;
(iii) undertakings which have obtained their supplies from natural or legal persons providing certain guarantees to be determined and having concluded contracts with producers of fodder for drying; such buyers shall be approved, on terms defined in accordance with the procedure referred to in Article 18(2), by the competent authority of the Member State in which the fodder is harvested.
Article 11
Undertakings processing their own crops or those of their members shall each year submit to the competent body of their Member State, before a date to be set, a declaration of the areas from which the fodder crop is to be processed.
Article 12
1. A contract as referred to in point (c) of Article 10 shall state not only the price to be paid to the grower of the green fodder or, if appropriate, sun-dried fodder but also at least the following:
(a) the area from which the crop is to be delivered to the processor;
(b) the delivery and payment terms.
2. Where a contract as referred to in point (c)(i) of Article 10 is a special-order contract for processing of fodder delivered by a producer, it shall specify at least the area from which the crop is to be delivered and include a clause containing an obligation for the processing undertakings to pay the producer the aid as provided for in Article 4 and received for the quantity processed under the contract.
Article 13
1. Member States shall introduce inspection systems for verifying that each processing undertaking has complied with the following:
(a) the conditions laid down in Articles 1 to 12;
(b) the quantities covered by aid applications correspond to the quantities of dried fodder meeting the minimum quality that leave the processing undertakings.
2. Dried fodder shall be weighed on leaving the processing plant and samples taken.
3. Before adopting provisions for the application of paragraph 1, Member States shall notify such provisions to the Commission.
CHAPTER III
TRADE WITH THIRD COUNTRIES
Article 14
Unless this Regulation provides otherwise, the Common Customs Tariff duty rates shall apply to the products listed in Article 1.
Article 15
1. The general rules for the interpretation of the Combined Nomenclature and the detailed rules for its application shall apply to the tariff classification of products listed in Article 1. The tariff nomenclature resulting from the application of this Regulation shall be incorporated in the Common Customs Tariff.
2. Unless otherwise provided for in this Regulation or in provisions adopted pursuant thereto, the following shall be prohibited in trade with third countries:
(a) the levying of any charge having equivalent effect to a customs duty;
(b) the application of any quantitative restriction or measure having equivalent effect.
Article 16
1. If by reason of imports or exports the Community market in one or more of the products listed in Article 1 is affected by or threatened with serious disturbance likely to jeopardise the achievement of the objectives set out Article 33 of the Treaty, appropriate measures may be applied to trade with non-WTO member countries until such disturbance or threat of it ceases.
2. If the situation referred to in paragraph 1 arises, the Commission shall at a request of a Member State or on its own initiative decide upon the necessary measures. The Member States shall be notified of such measures which shall be immediately applicable. If the Commission receives a request from a Member State, it shall take a decision thereon within three working days following receipt of the request.
3. Measures decided by the Commission may be referred to the Council by any Member State within three working days of the day on which they were notified. The Council shall meet without delay. It may, acting by qualified majority, amend or repeal the measure in question within one month from the date on which it was referred to the Council.
4. Provisions adopted under this Article shall be applied having regard to the obligations arising from agreements concluded in accordance with Article 300(2) of the Treaty.
CHAPTER IV
GENERAL PROVISIONS
Article 17
Unless this Regulation provides otherwise, Articles 87, 88 and 89 of the Treaty shall apply to production of and trade in the products listed in Article 1 of this Regulation.
Article 18
1. The Commission shall be assisted by the Management Committee for Cereals instituted by Article 25 of Council Regulation (EC) No 1784/2003 of 29 September 2003 on the common organisation of the market in cereals(8), hereinafter referred to as "the Committee".
2. Where reference is made to this paragraph, Articles 4 and 7 of Decision 1999/468/EC shall apply.
The period laid down in Article 4(3) of Decision 1999/468/EC shall be set at one month.
3. The Committee shall adopt its Rules of Procedure.
Article 19
The Committee may consider any question raised by its chairman, either on his own initiative or at the request of a representative of a Member State.
Article 20
Detailed rules for the application for this Regulation shall be adopted in accordance with the procedure referred to in Article 18(2), in particular on:
(a) granting of the aid provided for in Article 4 and the advance provided for in Article 7;
(b) verification and establishment of entitlement to the aid, including any necessary controls, all of which may make use of certain elements of the integrated system;
(c) release of the securities indicated in Article 7(1);
(d) criteria for determining the quality standards referred to in Article 9;
(e) conditions to be fulfilled by the undertakings as set out in point (c)(ii) in Article 10 and Article 11;
(f) control measure to be carried out referred to in Article 13(2);
(g) criteria to be fulfilled for the conclusion of contracts as referred to in Article 10 and information which they must contain, in addition to the criteria laid down in Article 12;
(h) application of the maximum guaranteed quantity (MGQ) as referred to in Article 5(1).
Article 21
Transitional measures may be adopted in accordance with the procedure referred to in Article 18(2).
Article 22
Member States shall notify the Commission of the measures they take in order to apply this Regulation.
Article 23
Before 30 September 2008 the Commission shall, on the basis of an evaluation of the common market organisation for dried fodder, present a report to the Council on this sector dealing in particular with the development of areas of leguminous and other green fodder, the production of dried fodder and the savings of fossil fuels achieved. The report shall be accompanied, if needed, by appropriate proposals.
Article 24
Regulation (EC) No 1258/1999 and the provisions adopted in implementation thereof shall apply to the expenditure incurred by the Member States in carrying out obligations under this Regulation.
Article 25
Regulation (EC) No 603/95 is hereby repealed.
References made to the repealed Regulation shall be construed as references to this Regulation and shall be read in accordance with the correlation table in Annex.
Article 26
This Regulation shall enter into force on the seventh day following that of its publication in the Official Journal of the European Union.
It shall apply from 1 April 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 29 September 2003.
For the Council
The President
G. Alemanno
(1) Opinion delivered on 5 June 2003 (not yet published in the Official Journal).
(2) OJ C 208, 3.9.2003, p. 41.
(3) Opinion delivered on 2 July 2003 (not yet published in the Official Journal).
(4) OJ L 63, 21.3.1995, p. 1. Regulation as last amended by Regulation (EC) No 806/2003 (OJ L 122, 16.5.2003, p. 1).
(5) See page 1 of this Official Journal.
(6) OJ L 184, 17.7.1999, p. 23.
(7) OJ L 160, 26.6.1999, p. 103.
(8) See page 78 of this Official Journal.
ANNEX
CORRELATION TABLE
>TABLE>
