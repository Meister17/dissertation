Commission Regulation (EC) No 970/2006
of 29 June 2006
amending Regulation (EC) No 2305/2003 opening and providing for the administration of a Community tariff quota for imports of barley from third countries
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1784/2003 of 29 September 2003 on the common organisation of the market in cereals [1], and in particular Article 12(1) thereof,
Whereas:
(1) The Agreement in the form of an Exchange of Letters between the European Community and the United States of America pursuant to Article XXIV:6 and Article XXVIII of the General Agreement on Tariffs and Trade (GATT) 1994 [2], approved by Council Decision 2006/333/EC [3], provides for an increase of 6215 tonnes in the tariff quota for barley.
(2) Commission Regulation (EC) No 2305/2003 [4] opens a Community tariff quota for barley. The quantity of barley covered by the quota should be increased in application of the Agreement approved by Decision 2006/333/EC.
(3) In the interests of simplification, the obsolete provisions of Regulation (EC) No 2305/2003, relating to 2004, should be deleted.
(4) In order to clarify the rules, it should be stipulated that import licence applications must be lodged on Monday at the latest but may be lodged earlier.
(5) With a view to modernising the administration of the system, provision should be made for the electronic transmission of the information required by the Commission.
(6) In order to clarify the rules, the expression "reduction coefficient" should moreover be replaced by "allocation coefficient".
(7) Regulation (EC) No 2305/2003 should therefore be amended.
(8) Since the Agreement approved by Decision 2006/333/EC provides for implementation on 1 July 2006, this Regulation must apply from the date of its publication in the Official Journal of the European Union.
(9) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Cereals,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 2305/2003 is hereby amended as follows:
1. Article 1 is replaced by the following:
"Article 1
1. A tariff quota is hereby opened for the import of 306215 tonnes of barley falling within CN code 100300 (serial No 09.4126).
2. The tariff quota shall be opened on 1 January each year. Duties on imports within the tariff quota shall be levied at a rate of EUR 16 per tonne.
Article 10(1) of Regulation (EC) No 1784/2003 shall apply to imports of the products referred to in this Regulation in excess of the quantity provided for in paragraph 1 of this Article."
2. Article 3 is amended as follows:
(a) Paragraph 1 is amended as follows:
(i) The first subparagraph is replaced by the following:
"1. Applications for import licences shall be lodged with the competent authorities of the Member States each week no later than Monday at 13.00 (Brussels time)."
(ii) The third subparagraph is deleted.
(b) In the first subparagraph of paragraph 2, the first sentence is replaced by the following:
"No later than 18.00 Brussels time on the final day for the lodging of licence applications, the competent authorities shall forward to the Commission electronically a notification in accordance with the model given in the Annex and the total quantity for which import licence applications have been submitted."
(c) Paragraph 3 is replaced by the following:
"3. If the combined total of the quantities granted since the start of the year and the quantity referred to in paragraph 2 exceeds the relevant quota for the year concerned, the Commission shall fix, no later than the third working day following the final day for the lodging of applications, an allocation coefficient to be applied to the quantities requested."
(d) In paragraph 4, the first sentence is replaced by the following:
"Without prejudice to paragraph 3, licences shall be issued on the fourth working day following the final day for the lodging of applications."
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Union.
It shall apply from 1 July 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 29 June 2006.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 270, 21.10.2003, p. 78. Regulation as amended by Commission Regulation (EC) No 1154/2005 (OJ L 187, 19.7.2005, p. 11).
[2] OJ L 124, 11.5.2006, p. 15.
[3] OJ L 124, 11.5.2006, p. 13.
[4] OJ L 342, 30.12.2003, p. 7. Regulation as amended by Regulation (EC) No 777/2004 (OJ L 123, 27.4.2004, p. 50).
--------------------------------------------------
