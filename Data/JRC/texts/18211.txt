Commission Regulation (EC) No 386/2004
of 1 March 2004
amending Council Regulation (EC) No 2201/96 and Regulation (EC) No 1535/2003 as regards the combined nomenclature codes for certain products processed from fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 234/79 of 5 February 1979 on the procedure for adjusting the common customs tariff nomenclature used for agricultural products(1), and in particular Article 2(1) thereof,
Whereas:
(1) Article 1(2) of Council Regulation (EC) No 2201/96 of 28 October 1996 on the common organisation of the markets in processed fruit and vegetable products(2) fixes the products governed by that common market organisation.
(2) Annex I to Regulation (EC) No 2201/96 establishes the products referred to in Article 2 of that Regulation.
(3) Article 2 of Commission Regulation (EC) No 1535/2003 of 29 August 2003 laying down detailed rules for applying Council Regulation (EC) No 2201/96 as regards the aid scheme for products processed from fruit and vegetables(3) defines the products referred to in Article 6a(1) of Regulation (EC) No 2201/96 and also in Annex I thereto.
(4) Commission Regulation (EC) No 1789/2003 of 11 September 2003 amending Annex I to Council Regulation (EEC) No 2658/87 of 23 July 1987 on the tariff and statistical nomenclature and on the Common Customs Tariff(4) provides for amendments to the combined nomenclature for certain products processed from fruit and vegetables.
(5) Article 1(2) of Regulation (EC) No 2201/96 and Annex I thereto must therefore be amended, as must Article 2 of Regulation (EC) No 1535/2003.
(6) These amendments should apply at the same time as Regulation (EC) No 1789/2003.
(7) Regulation (EC) No 2201/96 and Regulation (EC) No 1535/2003 must therefore be amended.
(8) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for products processed from fruit and vegetables,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 2201/96 is hereby amended as follows:
1. In Article 1(2), point (b) of the Table is amended as follows:
(a) under code " ex 2001 ", sixth indent, the code " ex 2001 90 96 " is replaced by " ex 2001 90 99 ";
(b) under code " ex 2007 ", second indent, the code " ex 2007 99 58 " is replaced by " ex 2007 99 57 ";
(c) under code " ex 2008 ", seventh indent, the code " ex 2008 99 68 " is replaced by " ex 2008 99 67 ";
2. Annex I is amended as follows:
(a) the codes " ex 2008 40 91 " and " ex 2008 40 99 " are replaced by " ex 2008 40 90 ";
(b) the codes " ex 2008 70 94 " and " ex 2008 70 99 " are replaced by " ex 2008 70 98 ".
Article 2
Article 2 of Regulation (EC) No 1535/2003 is amended as follows:
1. in point (1), the terms " ex 2008 70 94 and ex 2008 70 99 " are replaced by "and ex 2008 70 98 ";
2. in point (2), the terms " ex 2008 40 91 and ex 2008 40 99 " are replaced by "and ex 2008 40 90 ".
Article 3
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Union.
It shall apply from 1 January 2004.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 1 March 2004.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 34, 9.2.1979, p. 2. Regulation as last amended by Regulation (EC) No 3290/94 (OJ L 349, 31.12.1994, p. 105).
(2) OJ L 297, 21.11.1996, p. 29. Regulation as last amended by Regulation (EC) No 453/2002 (OJ L 72, 14.3.2002, p. 9).
(3) OJ L 218, 30.8.2003, p. 14.
(4) OJ L 281, 30.10.2003, p. 1.
