COMMISSION REGULATION (EC) No 174/1999 of 26 January 1999 laying down special detailed rules for the application of Council Regulation (EEC) No 804/68 as regards export licences and export refunds in the case of milk and milk products
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 804/68 of 27 June 1968 on the common organisation of the market in milk and milk products (1), as last amended by Regulation (EC) No 1587/96 (2), and in particular Articles 13(3), 16a(1) and 17(9) and (14) thereof,
1. Whereas Commission Regulation (EC) No 1466/95 of 27 June 1995 laying down special detailed rules of application for export refunds on milk and milk products (3), as last amended by Regulation (EC) No 2184/98 (4), has been substantially amended several times; whereas, since further amendments are to be made, it should be recast in the interests of clarity and efficiency;
2. Whereas under the Agriculture Agreement concluded during the Uruguay Round of the GATT trade negotiations (5) (hereinafter referred to as the 'Agriculture Agreement`), export refunds on agricultural products, including milk products, are limited within each 12-month period starting from 1 July 1995 to a maximum quantity and a maximum value; whereas, in order to ensure compliance with those limits, the issue of export licences should be monitored; whereas procedures must also be adopted for allocating the quantities which may be exported with a refund;
3. Whereas Regulation (EEC) No 804/68 lays down general rules for granting of export refunds in the milk and milk products sector, in order, in particular, to permit the monitoring of the value and quantity limits for refunds; whereas detailed rules for the application of those arrangements should be laid down;
4. Whereas, by way of derogation from Commission Regulation (EEC) No 3665/87 of 27 November 1987 laying down common detailed rules for the application of the system of export refunds on agricultural products (6), as last amended by Regulation (EC) No 2334/98 (7), cases where a refund may be granted without presentation of an export licence should be defined and the maximum time that products may remain under customs control should be specified;
5. Whereas special provisions should be adopted for the milk and milk products sector, in particular with regard to licences, by way of derogation from Commission Regulation (EEC) No 3719/88 of 16 November 1988 laying down common detailed rules for the application of the system of import and export licences and advance fixing certificates for agricultural products (8), as last amended by Regulation (EC) No 1044/98 (9); whereas, in addition, the tolerance permitted by that Regulation as regards the quantity of goods exported compared with the quantity indicated on the licence should be reduced and, in order to ensure effective controls on limits, there should be no refunds paid on quantities exceeding what is indicated on the licence; whereas the securities to be lodged when licence applications are submitted should be sufficient to prevent speculative applications;
6. Whereas the term of validity of export licences should be fixed; whereas that term should vary according to the products concerned, with a shorter term for products where there is a higher risk of speculation;
7. Whereas, in order to ensure accurate checking of products exported and to minimise the risk of speculation, the possibility of changing the product for which a licence has been issued should be restricted;
8. Whereas Article 2a(2) of Regulation (EEC) No 3665/87 lays down rules for the use of export licences with advance fixing of the refund for the exportation of products with a 12-digit code other than that shown in Section 16 of the licence; whereas this provision becomes applicable in a specific sector only if product categories as referred to in Article 13a of Regulation (EEC) No 3719/88 and product groups as referred to in point (b) of the first subparagraph of Article 2a(2) of Regulation (EEC) No 3665/87 have been defined;
9. Whereas for the milk and milk product sector, product categories are already defined with reference to the categories provided for in the Agriculture Agreement; whereas in the interests of sound management this use of categories should be retained and Article 2a(2) of Regulation (EEC) No 3665/87 applied on the basis only of defined product groups;
10. Whereas in the milk sector refund rates are highly differentiated, notably according to fat content; whereas, so that this arrangement will not be called into question while at the same time the proportionality objective of Article 2a(2) of Regulation (EEC) No 3665/87 is respected, the product groups should be narrowly defined and for certain products the validity of export licences should extend to products that in fat content are immediately adjacent in CN code to the product for which a refund has been fixed in advance;
11. Whereas, to enable traders to participate in invitations to tender opened by third countries without affecting the restrictions as regards volume, a system of provisional licences should be introduced giving successful tenderers the right to a full licence;
12. Whereas, in order to ensure effective monitoring of licences issued, which depends on the notification of information to the Commission by the Member States, a waiting time should be provided for before licences are issued; whereas, in order to ensure the smooth operation of the arrangements and, in particular, an equitable allocation of the quantities available within the limits laid down by the Agriculture Agreement, various management measures should be laid down and, in particular, provision should be made for the issue of licences to be suspended and for a reduction coefficient to be applied to the quantities applied for;
13. Whereas the refund rate should be determined for products eligible for export refunds in the context of food-aid operations;
14. Whereas, to ensure that licences are correctly used, for certain exports with refunds the country of destination should be defined as a compulsory destination as referred to in Article 21(1) of Regulation (EEC) No 3665/87;
15. Whereas experience has shown that the number of applications for export licences for particular cheese varies according to destination; whereas, in order that special measures can be applied according to the destination indicated on licence applications, destination zones should be fixed and the destination zone indicated on export licences made obligatory for products falling within CN code 0406;
16. Whereas the method for fixing the refund on milk products containing added sugar, the price of which is determined by the price of the ingredients, should be laid down according to the percentage of the ingredients contained therein; whereas, however, to facilitate the management of refunds for these products, and particularly measures to ensure compliance with commitments regarding exports in the context of the Agriculture Agreement, a maximum quantity should be fixed for sucrose incorporated for which a refund may be granted; whereas 43 % by weight of whole product is representative of the sucrose content of such products;
17. Whereas Article 8(3) of Regulation (EEC) No 3665/87 provides that refunds may be granted for ingredients of Community origin in processed cheese manufactured under the inward processing arrangements; whereas certain special rules are needed to ensure that this specific measure operates properly and can be effectively checked;
18. Whereas, under the Agreement concluded between the European Community and Canada approved by Council Decision 95/591/EC (10) export licences issued by the European Community must be presented for cheese qualifying for preferential terms on import into Canada; whereas the detailed rules for issuing such licences should be laid down; whereas, to ensure that the quantities of cheese imported into Canada under the import quota correspond to the quantities for which licences have been issued, the licences duly stamped by the Canadian authorities should be returned to the competent bodies of the Member States and the data on exports should be forwarded by the Member States to the Commission;
19. Whereas, in the framework of the consultations with Switzerland on the implementation of the results of the Uruguay Round, it has been agreed to apply a set of measures providing in particular for a reduction in the customs duties on certain Community cheeses imported into Switzerland; whereas the Community origin of such products should be guaranteed; whereas, to that end, export licences should be compulsory for all cheese exported under these arrangements, including cheese on which export refunds are not payable; whereas licences should be issued subject to presentation by the exporter of a declaration certifying the Community origin of the product;
20. Whereas the Community has the option of designating which importers may import Community cheeses into the United States under the additional quota arising from the Agriculture Agreement; whereas this would allow the Community to maximise the value of the quota; whereas a procedure should therefore be laid down for designating importers on the basis of the allocation of export licences for the products concerned;
21). Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Milk and Milk Products,
HAS ADOPTED THIS REGULATION:
CHAPTER I
General arrangements for export refunds
Article 1
1. Exports from the Community of products listed in Article 1 of Regulation (EEC) No 804/68 for which an export refund is requested shall be subject to the presentation of an export licence, except in the cases referred to in Article 2. The refund to be paid shall be the rate valid on the day the application for the export licence or, where relevant, the provisional licence, is submitted.
2. Section 7 of licence applications and licences shall show the code number of the country of destination as referred to in the Annex to Commission Regulation (EC) No 2317/97 (11).
3. Licence applications which, within the meaning of Article 15 of Regulation (EEC) No 3719/88, were lodged on a Thursday, shall be deemed to have been submitted on the next working day.
Article 2
Refunds shall be granted only on presentation of an export licence.
By way of derogation from Article 2a of Regulation (EEC) No 3665/87, however, no licence shall be required:
(a) where the refund for an export declaration, calculated on the basis of the refund applicable on the first day of the month of export, is not more than EUR 60;
(b) in the cases referred to in Articles 34, 38, 42, 43 and 44(1) of Regulation (EEC) No 3665/87.
For the purposes of point (a) of the second paragraph, where several separate codes in the nomenclature for refunds, as laid down by Commission Regulation (EC) No 3846/87 (12), or the Combined Nomenclature are entered in an export declaration, the particulars relating to each code shall be deemed to constitute a separate declaration.
Article 3
No refund shall be granted on exports of cheese where the free-at-frontier price prior to application of the refund in the Member State of export is less than EUR 230 per 100 kilograms.
However, the first paragraph shall not apply to cheese falling within code 0406 90 33 9919 of the nomenclature for refunds.
Article 4
1. The four product categories for the purposes of the Agriculture Agreement shall be those set out in Annex I hereto.
2. The second paragraph of Article 13a of Regulation (EEC) No 3719/88 shall not apply to the products referred to in Article 1(1) of this Regulation.
3. Without prejudice to the application of Article 5(3) of this Regulation, the product groups within the meaning of point (b) of the first subparagraph of Article 2a(2) of Regulation (EEC) No 3665/87 shall be those set out in Annex II.
Article 5
1. Section 16 of licence applications and licences shall show the 12-digit product code of the nomenclature for refunds. Licences shall be valid for that product alone except in the cases specified in paragraphs 2 and 3.
2. For products falling within CN codes 0401, 0402, 0403, 0404, 0405 and 2309, licence holders may on request obtain a change of the code in Section 16 of the export licence to another code in the same category, as referred to in Annex I, where the refund is identical. Such requests must be lodged before completion of the formalities referred to in Article 3 or 25 of Regulation (EEC) No 3665/87.
3. By way of derogation from point (b) of the first subparagraph of Article 2a(2) of Regulation (EEC) No 3665/87, export licences with advance fixing of the refund shall also be valid for exportation of a product with a 12-digit code other than that entered in Section 16 of the licence if the two products are adjacent in one of the groups laid down in Annex II or both are in group 23.
4. In cases referred to in paragraph 3, refunds shall be calculated in accordance with the second subparagraph of Article 2a(2) of Regulation (EEC) No 3665/87.
Article 6
Export licences shall be valid from the day of issue, within the meaning of Article 21(1) of Regulation (EEC) No 3719/88, until:
(a) the end of the sixth month following issue in the case of products falling within CN code 0402 10;
(b) the end of the fourth month following issue in the case of products falling within CN code 0405;
(c) the end of the second month following issue in the case of products falling within CN code 0406;
(d) the end of the fourth month following issue for the other products referred to in Article 1 of Regulation (EEC) No 804/68;
(e) the date by which the obligations arising from invitations to tender pursuant to Article 8(1) must be fulfilled and by the end of the eighth month following issue of the full licence referred to in Article 8(3) at the latest.
Article 7
By way of derogation from Articles 27(5) and 28(5) of Regulation (EEC) No 3665/87, the period during which the products referred to in Article 1 of Regulation (EEC) No 804/68 may remain covered by the arrangements provided for in Council Regulation (EEC) No 565/80 (13) shall be equal to the remainder of the term of validity of the export licence.
Article 8
1. In the case of an invitation to tender issued by a public body in a third country as referred to in Article 44(1) of Regulation (EEC) No 3719/88, except for invitations to tender concerning products falling within CN code 0406, traders may apply for a provisional export licence for the quantity covered by their tender subject to the lodging of a security. The security for provisional licences shall be equal to 75 % of the rate fixed in accordance with Article 9.
Traders shall furnish proof that the body is public or subject to public law.
2. Provisional licences shall be issued on the fifth working day following that on which the application is lodged, provided that the special measures referred to in Article 10(3) have not been adopted in the meantime.
3. By way of derogation from Article 44(5) of Regulation (EEC) No 3719/88, the period for submitting the information referred to in that paragraph shall be 60 days. Before the end of that period, traders shall apply for the full export licence, which shall be issued on presentation of proof that they have been awarded a contract.
On presentation of proof that the tender has been rejected or that the quantity awarded by the contract is less than indicated on the provisional licence, the whole or part of the security shall be released as appropriate.
4. Licence applications referred to in paragraphs 2 and 3 shall be made in accordance with Article 13 of Regulation (EEC) No 3719/88.
5. The provisions of this chapter, with the exception of Article 10, shall apply to full licences.
Article 9
The security referred to in Article 14(2) of Regulation (EEC) No 3719/88 shall be equal to the percentage of the refund fixed for each product code applicable on the day the export licence application is lodged, as follows:
(a) 5 % for products covered by CN code 0405;
(b) l5 % for products covered by CN code 0402 10;
(c) 30 % for products covered by CN code 0406;
(d) 20 % for other products.
The amount of the refund referred to in the first paragraph shall be that calculated for the total quantity of the product concerned except for milk products containing added sugar.
For milk products containing added sugar, the amount of the refund referred to in the first paragraph shall be equal to the total quantity of the whole product concerned, multiplied by the refund rate applicable per kilogram of milk product.
Article 10
1. Export licences shall be issued on the fifth working day following the day on which applications are submitted, provided that the quantities for which licences have been applied for have been communicated in accordance with Article 6(1) of Commission Regulation (EEC) No 210/69 (14) and that the special measures referred to in paragraph 3 have not been adopted in the meantime.
2. A decision may be taken to adopt one or more of the special measures provided for in paragraph 3 where:
(a) the issue of the licences applied for appears likely to result in an overrun in the budget resources available or in the exhaustion of the maximum quantities which may be exported with refund during the 12-month period in question or in a shorter period to be determined pursuant to Article 11;
or
(b) the issue of the licences applied for does not allow the continuity of exports to be guaranteed during the rest of the period concerned.
For the purposes of the first subparagraph, account shall be taken, as regards the product in question, of:
(a) the seasonal nature of trade, the market situation and in particular the trend in prices on the market and the export conditions resulting therefrom;
(b) the need to prevent speculation from distorting competition between traders.
3. In the cases referred to in paragraph 2, the Commission may decide, for the product or products in question:
(a) to suspend the issue of licences for a maximum of five working days;
(b) to apply an allocation coefficient to the quantities requested. Where a coefficient of less than 0,4 is applied to the quantities requested, applicants may, within three working days of publication of the decision fixing the coefficient, request the cancellation of their licence application and the release of their security.
Furthermore, in accordance with the procedure provided for in Article 30 of Regulation (EEC) No 804/68, the Commission may decide:
(a) to suspend the issue of licences for the product or products concerned, without prejudice to the first indent of (a), for a period exceeding five working days;
(b) at the end of the period of suspension, to fix refunds by invitation to tender for products falling within CN codes 0402 10 19, 0405 10 90, 0405 90 10, 0405 90 90 and 0405 10 19. The relevant licences shall then be issued.
4. Licence applications submitted during the period of suspension shall not be admissible.
Article 11
Where the total quantity covered by the licence applications submitted is such that there is a risk of early exhaustion of the maximum quantities which may be exported with refund during the 12-month period in question, the Commission may decide, in accordance with the procedure provided for in Article 30 of Regulation (EEC) No 804/68, to allocate those maximum quantities over periods to be determined.
Article 12
1. Where the quantity exported exceeds that shown on the licence, no refund shall be payable on the overrun.
To that end, Section 22 of licences shall contain the words: 'Payment of the refund restricted to the quantity shown in Sections 17 and 18`.
2. By way of derogation from the provisions of Articles 8(5) and 33(2) of Regulation (EEC) No 3719/88 on tolerances for quantities exported, the following rates shall apply:
(a) the rate provided for in Article 8(5) shall be 2 %;
(b) the rate provided for in Article 33(2), first and second subparagraphs, shall be 98 %;
(c) the rate provided for in Article 33(2), third subparagraph, shall be 2 %.
Article 44(9)(c) of Regulation (EEC) No 3719/88 shall not apply.
Article 13
1. Article 10 shall not apply to the issuing of export licences for food-aid supplies in accordance with Article 10(4) of the Agriculture Agreement.
2. The rate of refund applicable to national food-aid supplies shall be that applying on the day the Member State opens the invitation to tender for the supply of food aid.
Article 14
The country of destination referred to in Article 1(2) shall be a compulsory destination for the purposes of Article 21(1) of Regulation (EEC) No 3665/87 for licences issued pursuant to Article 8 of this Regulation.
Article 15
1. For licences issued for products falling within CN code 0406, the following words shall be entered in Section 20 of applications and licences:
'Licence valid for zone . . . as defined in Article 15(3) of Regulation (EC) No 174/1999`.
The zone indicated shall be the zone, as defined in paragraph 3, to which the country of destination indicated in Section 7 of licence applications and licences belongs.
2. The zone as referred to in paragraph 1 shall be a compulsory destination for the purposes of Article 21(1) of Regulation (EEC) No 3665/87.
No refund shall be paid where the actual destination is in a zone other than that indicated in the licence application and in the licence.
3. The following zones shall be defined for the purposes of paragraph 1:
>TABLE>
Article 16
1. For milk products containing added sugar, the refund shall be equal to the sum of the following components:
(a) a component representing the quantity of milk product;
(b) a component representing the quantity of added sucrose, up to a maximum of 43 % by weight of whole product.
However, the component referred to in (b) shall apply only if the added sucrose has been produced from sugar beet or cane harvested in the Community.
2. The component referred to in paragraph 1(a) shall be calculated by multiplying the basic amount of the refund by the milk product content of the whole product.
The basic amount referred to in the first subparagraph shall be the refund on one kilogram of milk product contained in the whole product.
3. The component referred to in paragraph 1(b) shall be calculated by multiplying the sucrose content of the whole product, up to a maximum of 43 %, by the basic amount of the refund applicable on the day the licence application is submitted for the products listed in Article 1(1)(d) of Council Regulation (EEC) No 1785/81 (15).
However, the sucrose component shall not be taken into account where the basic amount of the refund for the milk product content referred to in the second subparagraph of paragraph 2 is zero.
4. For the purposes of paragraph 1(b), sucrose shall be treated as sucrose produced from beet or cane harvested in the Community if it has been:
(a) imported into the Community under Protocol 8 on sugar annexed to the ACP-EEC Lomé Convention (16), or the Agreement on cane sugar between the European Economic Community and the Republic of India (17);
(b) obtained from products imported under the provisions referred to in (a).
Article 17
1. Export licence applications for milk and milk products exported as products falling within CN code 0406 30 in accordance with the third indent of Article 8(3) of Regulation (EEC) No 3665/87 shall be accompanied by the authorisation granted by the competent authorities to use the customs arrangements in question.
2. Section 20 of licence applications and licences shall contain a reference to this Article.
3. The Member States shall take the necessary steps under the arrangements referred to in paragraph 1 to identify and check the quality and quantity of products referred to in that paragraph for which a refund has been applied for and to apply the provisions on entitlement to the refund.
CHAPTER II
Specific arrangements
Article 18
1. An export licence shall be required for all exports of cheese to Canada under the quota referred to in the Agreement concluded between the European Community and Canada.
2. Licence applications and licences shall show:
(a) in Section 7, the words 'CANADA - 404`;
(b) in Section 15, the six-digit description of the goods in accordance with the Combined Nomenclature for products falling within CN codes 0406 10, 0406 20, 0406 30 and 0406 40 and the eight-digit description for products falling within CN code 0406 90. Section 15 of applications and licences may contain no more than six products thus described;
(c) in Section 16, the eight-figure CN code and the quantity in kilograms for each of the products referred to in Section 15. The licence shall be valid only for the products and quantities so designated;
(d) in Sections 17 and 18, the total quantity of products referred to in Section 16;
(e) in Section 20, the following words:
'Cheeses for direct export to Canada. Article 18 of Regulation (EC) No 174/1999. Quota for . . . (year)`
or, as the case may be,
'Cheeses for export directly/via New York to Canada. Article 18 of Regulation (EC) No 174/1999. Quota for . . . (year)`.
Where cheese is transported to Canada via European third countries, such countries must be indicated instead of, or with, a reference to New York;
(f) in Section 22, the words 'without export refund`.
3. Licence applications shall be admissible only where applicants:
(a) declare in writing that all material falling within Chapter 4 of the Combined Nomenclature and used in the manufacture of products covered by their application has been produced entirely within the Community;
(b) undertake in writing to provide, at the request of the competent authorities, any further substantiation which the latter consider necessary for the issuing of licences and to accept, where applicable, any checks by those authorities on the book-keeping and manufacturing conditions of the products concerned.
4. Licences shall be issued immediately after applications are submitted. At the request of applicants, a certified copy of the licence shall be issued.
5. Licences shall be valid from their date of issue within the meaning of Article 21(1) of Regulation (EEC) No 3719/88 until 31 December following that date.
However, licences may be issued from 20 December and shall be valid from l January until 31 December of the following year provided the words 'quota for . . . (year)` in Section 20 of licence applications and licences refer to the following year.
6. Holders of export licences shall ensure that the licence or a certified copy thereof:
(a) is presented to the competent Canadian authority on the day on which the import customs formalities are conducted;
(b) is returned, stamped by the competent Canadian authority, to the issuing body within two months of the date referred to in (a).
The Member States shall take the measures necessary to ensure that point (b) of the first subparagraph is complied with.
7. By way of derogation from Article 9 of Regulation (EEC) No 3719/88, licences shall not be transferable.
8. The competent authority of the Member State shall report to the Commission, in accordance with Annex IV, before the end of January for the preceding six-month period and before the end of July for the previous quota year, the number of licences issued and the quantity of cheese concerned, together with the number of licences stamped by the Canadian authorities and submitted by the declarant and the quantity concerned.
9. Chapter I shall not apply.
Article 19
1. An export licence shall be required for exports to Switzerland of the cheeses listed in Annex III, which are eligible for an exemption from or a reduction in customs duties on import into that country.
2. Section 20 of licence applications and licences shall contain the words 'Article 19 of Regulation (EC) No 174/1999`.
Licences issued under this Article shall be valid only for the exports referred to in paragraph 1.
3. Licence applications shall be admissible only where applicants:
(a) declare in writing that all material falling within Chapter 4 of the Combined Nomenclature and used in the manufacture of products covered by their application has been produced entirely within the Community;
(b) undertake in writing to provide, at the request of the competent authorities, any further substantiation which the latter consider necessary for the issuing of licences and to accept, where applicable, any checks by those authorities of the book-keeping and manufacturing conditions of the products concerned.
4. Chapter I shall apply to exports covered by refunds applications.
5. In the case of exports not covered by refund applications, Section 22 of licence applications and licences shall contain the words 'Without an export refund`.
Licences shall be issued as soon as possible after applications are submitted.
Licences shall be valid from their date of issue within the meaning of Article 21(1) of Regulation (EEC) No 3719/88 until the following 30 June.
6. With the exception of Article 5(1), the provisions of Chapter I shall not apply to the exports referred to in paragraph 5.
For cheeses not included in the nomenclature for refund however, the eight-figure code of the Combined Nomenclature shall be entered in Section 16 of licence applications and licences.
7. Certified copies of licences shall be issued to applicants on request.
Article 20
1. In accordance with the procedure provided for in Article 30 of Regulation (EEC) No 804/68, the Commission may decide that export licences for products falling within CN code 0406 for export to the United States of America as part of the additional quota under the Agriculture Agreement and the tariff quotas originally resulting from the Tokyo Round and granted to Austria, Finland and Sweden by the United States in Uruguay Round list XX shall be issued in accordance with paragraphs 2 to 11.
2. Traders may apply, within a period to be determined, for a provisional export licence for the export of the products referred to in paragraph 1 during the following calendar year subject to the lodging of a security equal to 50 % of the rate fixed in accordance with Article 9.
Traders shall at the same time indicate:
(a) the designation of product group covered by the United States quota in accordance with Additional Notes 16 to 23 and 25 in Chapter 4 of the Harmonized Tariff Schedule of the United States of America (most recent version);
(b) the product names in accordance with the Harmonized Tariff Schedule of the United States of America (most recent version);
(c) the quantity of products for which provisional licences are being sought which they have exported to the United States during the preceding three calendar years. To this end, the trader whose name appears on the relevant export declaration shall be regarded as the exporter;
(d) the name and address of the importer in the United States designated by the applicant;
(e) whether the importer is a subsidiary of the applicant.
In addition, applications shall be accompanied by a certificate from the designated importer stating that he is eligible under the rules in force in the United States on the issue of import licences for the products referred to in paragraph 1 as part of the quota.
3. Where applications for provisional licences are submitted in respect of a quantity of products in excess of one of the quotas referred to in paragraph 1 for the year in question, the Commission may:
(a) allocate provisional licences taking into account the quantity of the same products exported to the United States by the applicant in the past;
and/or
(b) allocate provisional licences in preference to applicants whose designated importers are subsidiaries;
and/or
(c) apply a reduction coefficient to the quantities for which application is made.
4. Where applying a reduction coefficient would mean allocating provisional licences for quantities of less than 5 tonnes, the Commission may allocate licences by drawing lots.
5. Where applications are submitted for provisional licences for quantities of product not exceeding the quotas referred to in paragraph 1 for the year concerned, the Commission may allocate the remaining quantities to applicants in proportion to the quantities applied for.
6. Section 20 of provisional licences as referred to in the first subparagraph of paragraph 2 shall include the following:
'Provisional export licence referred to in the first subparagraph of Article 20(2) of Regulation (EC) No 174/1999: not valid for exports`.
7. The names of importers designated by traders to whom provisional licences are issued shall be forwarded to the competent United States authorities.
8. In the case where an import licence for the quantities concerned is not allocated to the importer designated by an operator, in circumstances which do not cast doubt on the sincerity of the attestation referred to in the third subparagraph of paragraph 2, the trader may be authorised by the Member State to designate another importer provided that the latter appears on the list communicated to the competent authorities of the United States of America, in accordance with paragraph 7. The Member State shall inform the Commission as soon as possible of the change of designated importer and the Commission shall notify the change to the competent authorities of the United States.
9. Securities shall be released in whole or in part for rejected applications or for quantities in excess of those allocated.
10. Before the end of the year for which provisional licences are issued, the trader shall apply for the full licence, even in the case of part quantities, which shall be issued forthwith. The following words shall be entered in Section 20 of the application for the full licence and in the licence itself:
'For export to the United States of America: Article 20 of Regulation (EC) No 174/1999`.
The full licences issued shall be valid only for exports as referred to in paragraph 1 for the year in question.
11. With the exception of Articles 4 and 10, the provisions of Chapter I shall apply to full licences.
CHAPTER III
Final provisions
Article 21
Regulations (EEC) No 3665/87 and (EEC) No 3719/88 shall apply save as otherwise provided in this Regulation.
Article 22
Regulation (EC) No 1466/95 is hereby repealed.
References to the repealed Regulation shall be construed as references to this Regulation.
Regulation (EC) No 1466/95 shall continue to apply to licences issued against applications submitted before the entry into force of the present Regulation.
Article 23
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Communities.
It shall apply from 1 February 1999.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 26 January 1999.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ L 148, 28. 6. 1968, p. 13.
(2) OJ L 206, 16. 8. 1996, p. 21.
(3) OJ L 144, 28. 6. 1995, p. 22.
(4) OJ L 275, 10. 10. 1998, p. 21.
(5) OJ L 336, 23. 12. 1994, p. 1.
(6) OJ L 351, 14. 12. 1987, p. 1.
(7) OJ L 291, 30. 10. 1998, p. 15.
(8) OJ L 331, 2. 12. 1988, p. 1.
(9) OJ L 149, 20. 5. 1998, p. 11.
(10) OJ L 334, 30. 12. 1995, p. 25.
(11) OJ L 321, 21. 11. 1997, p. 19.
(12) OJ L 366, 24. 12. 1987, p. 1.
(13) OJ L 62, 7. 3. 1980, p. 5.
(14) OJ L 28, 5. 2. 1969, p. 1.
(15) OJ L 177, 1. 7. 1981, p. 4.
(16) OJ L 229, 17. 8. 1991, p. 3.
(17) OJ L 190, 23. 7. 1975, p. 36.
ANNEX I
>TABLE>
ANNEX II
>TABLE>
ANNEX III
>TABLE>
ANNEX IV
Canada Information requested under Article 18(8)
>START OF GRAPHIC>
>END OF GRAPHIC>
