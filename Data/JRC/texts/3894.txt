Commission Decision
of 31 March 2005
amending Decision 97/467/EC as regards the inclusion of one establishment in Croatia in provisional lists of third country establishments from which Member States are authorised to import ratite meat
(notified under document number C(2005) 985)
(Text with EEA relevance)
(2005/302/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Decision 95/408/EC of 22 June 1995 on the conditions for drawing up, for an interim period, provisional lists of third country establishments from which Member States are authorised to import certain products of animal origin, fishery products or live bivalve molluscs [1], and in particular Article 2(4) thereof,
Whereas:
(1) Commission Decision 97/467/EC of 7 July 1997 drawing up provisional lists of third country establishments from which the Member States authorise imports of rabbit meat and farmed game meat [2] sets out provisional lists of establishments in third countries from which the Member States are authorised to import farmed game meat, rabbit meat and ratite meat.
(2) Croatia has provided the name of one establishment producing ratite meat for which the responsible authorities certify that the establishment complies with Community rules.
(3) Accordingly, that establishment should be included in the lists set out in Decision 97/467/EC.
(4) As on-the-spot inspections of the concerned establishment have not yet been carried out, imports from it should not be eligible for reduced physical checks pursuant to Council Directive 97/78/EC of 18 December 1997 laying down the principles governing the organisation of veterinary checks on products entering the Community from third countries [3].
(5) Decision 97/467/EC should therefore be amended accordingly.
(6) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
Annex II to Decision 97/467/EC is amended in accordance with the Annex to this Decision.
Article 2
This Decision shall apply from 21 April 2005.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 31 March 2005.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 243, 11.10.1995, p. 17. Decision as last amended by Directive 2004/41/EC of the European Parliament and of the Council (OJ L 157, 30.4.2004, p. 33); Corrigendum in OJ L 195, 2.6.2004, p. 12.
[2] OJ L 199, 26.7.1997, p. 57. Decision as last amended by Decision 2004/591/EC (OJ L 263, 10.8.2004, p. 21).
[3] OJ L 24, 30.1.1998, p. 9. Directive as last amended by Regulation (EC) No 882/2004 of the European Parliament and of the Council (OJ L 165, 30.4.2004, p. 1); Corrigendum in OJ L 191, 28.5.2004, p. 1.
--------------------------------------------------
ANNEX
The following text is added to Annex II:
"País: Croacia/Země: Chorvatsko/Land: Kroatien/Land: Kroatien/Riik: Horvaatia/Χώρα: Κροατία/Country: Croatia/Pays: Croatie/Paese: Croazia/Valsts: Horvātija/Šalis: Kroatija/Ország: Horvátorszag/Pajjiż: Il-Kroazja/Land: Kroatie/Państwo: Chorwacja/País: Croácia/Krajina: Chorvátsko/Država: Hrvaška/Maa: Kroatia/Land: Kroatien
1 | 2 | 3 | 4 | 5 | 6 |
1962 | Klaonica nojeva Ltd. | Virje | Koprivničko križevačka županija | CP, SH" | |
--------------------------------------------------
