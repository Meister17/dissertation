Council Decision
of 20 December 2005
on the conclusion of an agreement in the form of an Exchange of Letters between the European Community and Thailand pursuant to Article XXVIII of GATT 1994 relating to the modification of concessions with respect to rice provided for in EC Schedule CXL annexed to GATT 1994
(2005/953/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 133 in conjunction with the first sentence of the first subparagraph of Article 300(2) thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) On 26 June 2003, the Council authorised the Commission to open negotiations under Article XXVIII of the GATT 1994 with a view to modifying certain concessions for rice. Accordingly, the European Community notified the WTO on 2 July 2003 of its intention to modify certain concessions in EC Schedule CXL.
(2) Negotiations have been conducted by the Commission in consultation with the Committee established by Article 133 of the Treaty and within the framework of the negotiating directives issued by the Council.
(3) The Commission has negotiated with the United States of America, having a principal supplying interest in products of HS code 1006 20 (husked rice) and substantial supplier interest in products of HS code 1006 30 (milled rice), Thailand, having a principal supplying interest in products of HS code 1006 30 (milled rice) and substantial supplier interest in products of HS code 1006 20 (husked rice) and India and Pakistan, each having a substantial supplier interest in products of HS code 1006 20 (husked rice).
(4) The agreements with India and with Pakistan have been approved on behalf of the Community by Decisions 2004/617/EC [1] and 2004/618/EC [2] respectively. A new tariff rate for husked rice (CN code 100620) and milled rice (CN code 100630) was fixed by Decision 2004/619/EC [3]. The agreement with the United States was approved by Decision 2005/476/EC [4].
(5) The Commission has now successfully negotiated an agreement in the form of an Exchange of Letters between the Community and Thailand which should therefore be approved.
(6) In order to ensure that the agreement may be fully applied as from 1 September 2005 and pending the amendment of Council Regulation (EC) No 1785/2003 of 29 September 2003 on the common organisation of the market in rice [5], the Commission should be authorised to adopt temporary derogations from that Regulation and to adopt the necessary implementing measures.
(7) The measures necessary for the implementation of this Decision should be adopted in accordance with Council Decision 1999/468/EC of 28 June 1999 laying down the procedures for the exercise of implementing powers conferred on the Commission [6],
HAS DECIDED AS FOLLOWS:
Article 1
The agreement in the form of an Exchange of Letters between the European Community and Thailand pursuant to Article XXVIII of GATT 1994 relating to the modification of concessions with respect to rice provided for in EC Schedule CXL annexed to GATT 1994 is hereby approved on behalf of the Community.
The text of the agreement is attached to this Decision.
Article 2
1. To the extent necessary to permit the full application of the agreement as from 1 September 2005, the Commission may derogate from Regulation (EC) No 1785/2003, in accordance with the procedure referred to in Article 3(2) of this Decision, until that Regulation is amended and, in any event, no later than 30 June 2006.
2. The Commission shall adopt the detailed rules for implementing the agreement in accordance with the procedure laid down in Article 3(2) of this Decision.
Article 3
1. The Commission shall be assisted by the Management Committee for Cereals instituted by Article 25 of Regulation (EC) No 1784/2003 [7].
2. Where reference is made to this paragraph, Articles 4 and 7 of Decision 1999/468/EC shall apply.
The period provided for in Article 4(3) of Decision 1999/468/EC shall be one month.
3. The Committee shall adopt its rules of procedure.
Article 4
The President of the Council is hereby authorised to designate the person(s) empowered to sign the agreement in order to bind the Community [8].
Done at Brussels, 20 December 2005.
For the Council
The President
M. Beckett
[1] OJ L 279, 28.8.2004, p. 17.
[2] OJ L 279, 28.8.2004, p. 23.
[3] OJ L 279, 28.8.2004, p. 29.
[4] OJ L 170, 1.7.2005, p. 67.
[5] OJ L 270, 21.10.2003, p. 96.
[6] OJ L 184, 17.7.1999, p. 23.
[7] OJ L 270, 21.10.2003, p. 78.
[8] The date of entry into force of the agreement will be published in the Official Journal of the European Union by the General Secretariat of the Council.
--------------------------------------------------
