COUNCIL REGULATION (EEC) No 2407/92 of 23 July 1992 on licensing of air carriers
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 84 (2) thereof,
Having regard to the proposal from the Commission(1) ,
Having regard to the opinion of the European Parliament(2) ,
Having regard to the opinion of the Economic and Social Committee(3) ,
Whereas it is important to establish an air transport policy for the internal market over a period expiring on 31 December 1992 as provided for in Article 8a of the Treaty;
Whereas the internal market shall comprise an area without internal frontiers in which the free movement of goods, persons, services and capital is ensured;
Whereas the application in the air transport sector of the principle of the freedom to provide services needs to take into account the specific characteristics of that sector;
Whereas in Council Regulation (EEC) No 2343/90 of 24 July 1990 on access for air carriers to scheduled intra-Community air service routes and on the sharing of passenger capacity between air carriers on scheduled air services between Member States(4) the Council decided to adopt for implementation not later than 1 July 1992 common rules governing the licensing of air carriers;
Whereas, however, it is necessary to allow Member States a reasonable period, until 1 January 1993, for the application of this Regulation;
Whereas it is important to define non-descriminatory requirements in relation to the location and control of an undertaking applying for a licence;
Whereas in order to ensure dependable and adequate service it is necessary to ensure that an air carrier is at all times operating at sound economic and high safety levels;
Whereas for the protection of users and other parties concerned it is important to ensure that air carriers are sufficiently insured in respect of liability risks;
Whereas within the internal market air carriers should be able to use aircraft owned anywhere in the Community, without prejudice to the responsibilities of the licensing Member State with respect to the technical fitness of the carrier;
Whereas it should also be possible to lease aircraft registered outside the Community for a short term or in exceptional circumstances, providing safety standards are equivalent to those applicable within the Community;
Whereas procedures for the granting of licences to air carriers should be transparent and non-discriminatory,
HAS ADOPTED THIS REGULATION:
Article 1
1. This Regulation concerns requirements for the granting and maintenance of operating licences by Member States in relation to air carriers established in the Community.
2. The carriage by air of passengers, mail and/or cargo, performed by non-power driven aircraft and/or ultra-light power driven aircraft, as well as local flights not involving carriage between different airports, are not subject to this Regulation. In respect of these operations, national law concerning operating licences, if any, and Community and national law concerning the air operator's certificate (AOC) shall apply.
Article 2
For the purposes of this Regulation:
(a) 'undertaking' means any natural person, any legal person, whether profit-making or not, or any official body whether having its own legal personality or not;
(b) 'air carrier' means an air transport undertaking with a valid operating licence;
(c) 'operating licence' means an authorization granted by the Member State responsible to an undertaking, permitting it to carry out carriage by air of passengers, mail and/or cargo, as stated in the operating licence, for remuneration and/or hire;
(d) 'air operator's certificate (AOC)' means a document issued to an undertaking or a group of undertakings by the competent authorities of the Member States which affirms that the operator in question has the professional ability and organization to secure the safe operation of aircraft for the aviation activities specified in the certificate;
(e) 'business plan' means a detailed description of the air carrier's intended commercial activities for the period in question, in particular in relation to the market development and investments to be carried out, including the financial and economic implications of these activities;
(f) 'management account' means a detailed statement of income and costs for the period in question including a breakdown between air-transport-related and other activities as well as between pecuniary and non-pecuniary elements;
(g) 'effective control' means a relationship constituted by rights, contracts or any other means which, either separately or jointly and having regard to the considerations of fact or law involved, confer the possibility of directly or indirectly exercising a decisive influence on an undertaking, in particular by:
(a) the right to use all or part of the assets of an undertaking;
(b) rights or contracts which confer a decisive influence on the composition, voting or decisions of the bodies of an undertaking or otherwise confer a decisive influence on the running of the business of the undertaking.
Article 3
1. Without prejudice to Article 5 (5), Member States shall not grant operating licences or maintain them in force where the requirements of this Regulation are not complied with.
2. An undertaking meeting the requirements of this Regulation shall be entitled to receive an operating licence. Such licence does not confer in itself any rights of access to specific routes or markets.
3. Without prejudice to Article 1 (2), no undertaking established in the Community shall be permitted within the territory of the Community to carry by air passengers, mail and/or cargo for remuneration and/or hire unless the undertaking has been granted the appropriate operating licence.
Operating licence
Article 4
1. No undertaking shall be granted an operating licence by a Member State unless:
(a) its principal place of business and, if any, its registered office are located in that Member State; and
(b) its main occupation is air transport in isolation or combined with any other commercial operation of aircraft or repair and maintenance of aircraft.
2. Without prejudice to agreements and conventions to which the Community is a contracting party, the undertaking shall be owned and continue to be owned directly or through majority ownership by Member States and/or nationals of Member States. It shall at all times be effectively controlled by such States or such nationals.
3. (a) Notwithstanding paragraphs 2 and 4, air carriers which have already been recognized in Annex I to Council Regulation (EEC) No 2343/90 and Council Regulation (EEC) No 294/91 of 4 February 1991 on the operation of air cargo services between Member States(5) shall retain their rights under this and associated Regulations as long as they meet the other obligations in this Regulation and they continue to be controlled directly or indirectly by the same third countries and/or by nationals of the same third country as those exercising such control at the time of adoption of this Regulation. Such control may, however, be transferred to Member States and/or to Member State nationals at any time.
(b) The possibility of buying and selling shares under subparagraph (a) does not cover nationals who have a significant interest in an air carrier of a third country.
4. Any undertaking which directly or indirectly participates in a controlling shareholding in an air carrier shall meet the requirements of paragraph 2.
5. An air carrier shall at all times be able on request to demonstrate to the Member State responsible for the operating licence that it meets the requirements of this Article. The Commission acting at the request of a Member State shall examine compliance with the requirements of this Article and take a decision if necessary.
Article 5
1. An applicant air transport undertaking to which an operating licence is granted for the first time must be able to demonstrate to the reasonable satisfaction of the competent authorities of the licensing Member State that:
(a) it can meet at any time its actual and potential obligations, established under realistic assumptions, for a period of 24 months from the start of operations; and
(b) it can meet its fixed and operational costs incurred from operations according to its business plan and established under realistic assumptions, for a period of three months from the start of operations, without taking into account any income from its operations.
2. For the purpose of paragraph 1, each applicant shall submit a business plan for, at least, the first two years of operation. The business plan shall also detail the applicant's financial links with any other commercial activities in which the applicant is engaged either directly or through related undertakings. The applicant shall also provide all relevant information, in particular the data referred to in part A of the Annex.
3. An air carrier shall notify in advance to its licensing authority plans for: operation of a new scheduled service or a non-scheduled service to a continent or world region not previously served, changes in the type or number of aircraft used or a substantial change in the scale of its activities. It shall also notify in advance any intended mergers or acquisitions and shall notify its licensing authority within fourteen days of any change in the ownership of any single shareholding which represents 10 % or more of the total shareholding of the air carrier or of its parent or ultimate holding company. The submission of a 12 month business plan two months in advance of the period to which it refers shall constitute sufficient notice under this paragraph for the purpose of changes to current operations and/or circumstances which are included in that business plan.
4. If the licensing authority deems the changes notified under paragraph 3 to have a significant bearing on the finances of the air carrier, it shall require the submission of a revised business plan incorporating the changes in question and covering, at least, a period of 12 months from its date of implementation, as well as all the relevant information, including the data referred to in part B of the Annex, to assess whether the air carrier can meet its existing and potential obligations during that period of 12 months. The licensing authority shall take a decision on the revised business plan not later than three months after all the necessary information has been submitted to it.
5. Licensing authorities may, at any time and in any event whenever there are clear indications that financial problems exist with an air carrier licensed by them, assess its financial performance and may suspend or revoke the licence if they are no longer satisfied that the air carrier can meet its actual and potential obligations for a 12-month period. Licensing authorities may also grant a temporary licence pending financial reorganization of the air carrier provided safety is not at risk.
6. An air carrier shall provide to its licensing authority every financial year without undue delay the audited accounts relating to the previous financial year. At any time upon request of the licensing authority an air carrier shall provide the information relevant for the purposes of paragraph 5 and, in particular, the data referred to in part C of the Annex.
7. (a) Paragraphs 1, 2, 3, 4 and 6 of this Article shall not apply to air carriers exclusively engaged in operations with aircraft of less than 10 tonnes mto w (maximum take off weight) and/or less than 20 seats. Such air carriers shall at all times be able to demonstrate that their net capital is at least ECU 80 000 or to provide when required by the licensing authority the information relevant for the purposes of paragraph 5. A Member State may nevertheless apply paragraphs 1, 2, 3, 4 and 6 to air carriers licensed by it that operate scheduled services or whose turnover exceeds ECU 3 million per year.
(b) The Commission may, after consulting the Member States, increase as appropriate the values referred to in subparagraph (a) if economic developments indicate the necessity of such a decision. Such change shall be published in the Official Journal of the European Communities.
(c) Any Member State may refer the Commission's decision to the Council within a time limit of one month. The Council, acting by a qualified majority, may in exceptional circumstances take a different decision within a period of one month.
Article 6
1. Where the competent authorities of a Member State require, for the purpose of issuing an operating licence, proof that the persons who will continuously and effectively manage the operations of the undertaking are of good repute or that they have not been declared bankrupt, or suspend or revoke the licence in the event of serious professional misconduct or a criminal offence, that Member State shall accept as sufficient evidence in respect of nationals of other Member States the production of documents issued by competent authorities in the Member State of origin or the Member State from which the foreign national comes showing that those requirements are met.
Where the competent authorities of the Member State of origin or of the Member State from which the foreign national comes do not issue the documents referred to in the first subparagraph, such documents shall be replaced by a declaration on oath - or, in Member States where there is no provision for declaration on oath, by a solemn declaration - made by the person concerned before a competent judicial or administrative authority or, where appropriate, a notary or qualified professional body of the Member State of origin or the Member State from which the person comes; such authority or notary shall issue a certificate attesting the authenticity of the declaration on oath or solemn declaration.
2. The competent authorities of Member States may require that the documents and certificates referred to in paragraph 1 be presented no more than three months after their date of issue.
Article 7
An air carrier shall be insured to cover liability in case of accidents, in particular in respect of passengers, luggage, cargo, mail and third parties.
Article 8
1. Ownership of aircraft shall not be a condition for granting or maintaining an operating licence but a Member State shall require, in relation to air carriers licensed by it that they habe one or more aircraft at their disposal, through ownership or any form of lease agreement.
2. (a) Without prejudice to paragraph 3, aircraft used by an air carrier shall be registered, at the option of the Member State issuing the operating licence, in its national register or within the Community.
(b) If a lease agreement for an aircraft registered within the Community has been deemed acceptable under Article 10, a Member State shall not require the registration of that aircraft on its own register if this would require structural changes to the aircraft.
3. In the case of short-term lease agreements to meet temporary needs of the air carrier or otherwise in exceptional circumstances, a Member State may grant waivers to the requirement of paragraph 2 (a).
4. When applying paragraph 2 (a) a Member State shall, subject to applicable laws and regulations, including those relating to airworthiness certification, accept on its national register, without any discriminatory fee and without delay, aircraft owned by nationals of other Member States and transfers from aircraft registers of other Member States. No fee shall be applied to transfer of aircraft in addition to the normal registration fee.
Air operator's certificates (AOC)
Article 9
1. The granting and validity at any time of an operating licence shall be dependent upon the possession of a valid AOC specifying the activities covered by the operating licence and complying with the criteria established in the relevant Council Regulation.
2. Until such time as the Council Regulation referred to in paragraph 1 is applicable, national regulations concerning the AOC, or equivalent title concerning the certification of air transport operators, shall apply.
Article 10
1. For the purposes of ensuring safety and liability standards an air carrier using an aircraft from another undertaking or providing it to another undertaking shall obtain prior approval for the operation from the appropriate licensing authority. The conditions of the approval shall be part of the lease agreement between the parties.
2. A Member State shall not approve agreements leasing aircraft with crew to an air carrier to which it has granted an operating licence unless safety standards equivalent to those imposed under Article 9 are met.
General provisions
Article 11
1. An operating licence shall be valid as long as the air carrier meets the obligations of this Regulation. However, a Member State may make provision for a review one year after a new operating licence has been granted and every five years thereafter.
2. When an air carrier has ceased operations for six months or has not started operations for six months after the granting of an operating licence, the Member State responsible shall decide whether the operating licence shall be resubmitted for approval.
3. In relation to air carriers licensed by them, Member States shall decide whether the operating licence shall be resubmitted for approval in case of change in one or more elements affecting the legal situation of the undertaking and, in particular, in the case of mergers or takeovers. The air carrier(s) in question may continue its (their) operations unless the licensing authority decides that safety is at risk, stating the reasons.
Article 12
An air carrier against which insolvency or similar proceedings are opened shall not be permitted by a Member State to retain its operating licence if the competent body in that Member State is convinced that there is no realistic prospect of a satisfactory financial reconstruction within a reasonable time.
Article 13
1. Procedures for the granting of operating licences shall be made public by the Member State concerned and the Commission shall be informed.
2. The Member State concerned shall take a decision on an application as soon as possible, and not later than three months after all the necessary information has been submitted, taking into account all available evidence. The decision shall be communicated to the applicant air transport undertaking. A refusal shall indicate the reasons therefor.
3. An undertaking whose application for an operating licence has been refused may refer the question to the Commission. If the Commission finds that the requirements of this Regulation have not been fulfilled it shall state its views on the correct interpretation of the Regulation without prejudice to Article 169 of the Treaty.
4. Decisions by Member States to grant or revoke operating licences shall be published in the Official Journal of the European Communities.
Article 14
1. In order to carry out its duties under Article 4 the Commission may obtain all necessary information from the Member States concerned, which shall also ensure the provision of information by air carriers licensed by them.
2. When the information requested is not supplied within the time limit fixed by the Commission, or is supplied in incomplete form, the Commission shall by decision addressed to the Member State concerned require the information to be supplied. The decision shall specify what information is required and fix an appropriate time limit within which it is to be supplied.
3. If the information required under paragraph 2 is not provided by the time limit set or the air carrier has not otherwise demonstrated that it meets the requirements of Article 4, the Commission shall, except where special circumstances exist, forthwith inform all Member States of the situation. Member States may, until notified by the Commission that documentation has been provided to demonstrate the fulfilment of the requirements in question, suspend any market access rights to which the air carrier is entitled under Council Regulation (EEC) No 2408/92 of 23 July 1992 on access for Community air carriers to intra-Comunity air routes(6) .
Article 15
In addition to the rules of this Regulation the air carrier shall also respect the requirements of national law compatible with Community law.
Article 16
Notwithstanding Article 3 (1), operating licences in force in a Member State at the date of entry into force of the Regulation shall remain valid, subject to the laws on the basis of which they were granted, for a maximum period of one year except in the case of Article 4 (1) (b) for which a maximum period of three years shall apply, during which periods the air carriers holding such licences shall make the necessary arrangements to conform with all the requirements of this Regulation. For the purposes of this Article, carriers holding operating licences shall be deemed to include carriers legitimately operating with a valid AOC at the date of entry into force of this Regulation but without holding such licences.
This Article shall be without prejudice to Article 4 (2) (3) (4) and (5) and Article 9, except that air carriers which operated by virtue of exemptions prior to the entry into force of this Regulation may continue to do so, for a period not exceeding the maximum periods specified above, pending enquiries by Member States as to their compliance with Article 4.
Article 17
Member States shall consult the Commission before adopting laws, regulations or administrative provisions in implementation of this Regulation. They shall communicate any such measures to the Commission when adopted.
Article 18
1. Member States and the Commission shall cooperate in implementing this Regulation.
2. Confidential information obtained in application of this Regulation shall be covered by professional secrecy.
Article 19
This Regulation shall enter into force on 1 January 1993.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 23 July 1992
For the Council The President J. COPE
(1) OJ No C 258, 4. 10. 1991, p. 2.
(2) OJ No C 125, 18. 5. 1992, p. 140.
(3) OJ No C 169, 6. 7. 1992, p. 15.
(4) OJ No L 217, 11. 8. 1990, p. 8.
(5) OJ No L 36, 8. 2. 1991, p. 1.
(6) See page 8 of this Official Journal.
ANNEX
Information for use in association with Article 5 of financial fitness of air carriers A. Information to be provided by a first-time applicant from a financial fitness point of view 1. The most recent internal management accounts and, if available, audited accounts for the previous financial year.
2. A projected balance sheet, including profit and loss account, for the following two years.
3. The basis for projected expenditure and income figures on such items as fuel, fares and rates, salaries, maintenance, depreciation, exchange rate fluctuations, airport charges, insurance, etc. Traffic/revenue forecasts.
4. Details of the start-up costs incurred in the period from submission of application to commencement of operations and an explanation of how it is proposes to finance these costs.
5. Details of existing and projected sources of finance.
6. Details of shareholders, including nationality and type of shares to be held, and the Articles of Association. If part of a group of undertakings, information on the relationship between them.
7. Projected cash-flow statements and liquidity plans for the first two years of operation.
8. Details of the financing of aircraft purchase/leasing including, in the case of leasing, the terms and conditions of contract.
B. Information to be provided for assessment of the continuing financial fitness of existing licence holders planning a change in their structures or in their activities with a significant bearing on their finances 1. If necessary, the most recent internal management balance sheet and audited accounts for the previous financial year.
2. Precise details of all proposed changes e.g. change of type of service, proposed takeover or merger, modifications in share capital, changes in shareholders, etc.
3. A projected balance sheet, with a profit and loss account, for the current financial year, including all proposed changes in structure or activities with a significant bearing on finances.
4. Past and projected expenditure and income figures on such items as fuel, fares and rates, salaries, maintenance, depreciation, exchange rate fluctuations, airport charges, insurance, etc. Traffic/revenue forecasts.
5. Cash-flow statements and liquidity plans for the following year, including all proposed changes in structure or activities with a significant bearing on finances.
6. Details of the financing of aircraft purchase/leasing including, in the case of leasing, the terms and conditions of contract.
C. Information to be provided for assessment of the continuing financial fitness of existing licence holders 1. Audited accounts not later than six months after the end of the relevant period and, if necessary, the most recent internal management balance sheet.
2. A projected balance sheet, including profit and loss account, for the forthcoming year.
3. Past and projected expenditure and income figures on such items as fuel, fares and rates, salaries, maintenance, depreciation, exchange rate fluctuations, airport charges, insurance, etc. Traffic/revenue forecasts.
4. Cash-flow statements and liquidity plans for the following year.
