Council opinion
of 8 March 2005
on the updated convergence programme of Latvia, 2004-2007
(2005/C 177/03)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1466/97 of 7 July 1997 on the strengthening of the surveillance of budgetary positions and the surveillance and coordination of economic policies [1], and in particular Article 9(3) thereof,
Having regard to the recommendation of the Commission,
After consulting the Economic and Financial Committee,
HAS DELIVERED THIS OPINION:
(1) On 8 March 2005 the Council examined the updated convergence programme of Latvia, which covers the period 2004 to 2007. The programme broadly complies with the data requirements of the revised "code of conduct on the content and format of stability and convergence programmes". However, the ESA95 classification of data on the composition of the general government revenue and expenditure needs to be improved. Accordingly, Latvia is invited to achieve compliance with the requirements of the code of conduct.
(2) The macroeconomic scenario underlying the programme envisages real GDP growth, estimated at 8,1 % in 2004, remaining strong over the rest of the programme period, averaging 6,6 % per year. On the basis of currently available information, this scenario seems to reflect plausible growth assumptions. The programme's projections for inflation appear slightly on the low side.
(3) The programme update aims at keeping the deficit below the 3 % of GDP reference value in each programme year with the general government budget deficit gradually falling from an estimated 1,7 % of GDP in 2004 to 1,4 % of GDP in 2007. In 2005 both revenue and expenditure ratios increase sharply, linked to EU accession. Thereafter, the medium-term adjustment in public finances is based on a reduction of both the revenue and the expenditure ratios. The primary expenditure ratio falls mainly through lower ratios of transfers and subsidies while at the same time allowing for an increase in the investment ratio. The revenue ratio falls through tax reductions, mainly on income, lowering the tax burden moderately.
(4) The risks to the budgetary projections in the programme appear broadly balanced. On the one hand, assumptions about the tax intensity of economic activity seem somewhat pessimistic, suggesting that revenues could be better than expected. On the other hand, given the commitment of expenditures predicated on EU funding, the achievement of the deficit targets is conditional on the receipt of such funds in government revenues. Tight expenditure discipline (including in managing partly EU-funded projects) might be difficult given weaknesses in administrative capacity.
(5) In view of this risk assessment, the budgetary stance in the programme is insufficient to achieve the close to balance or surplus medium-term objective of the Stability and Growth Pact within the programme period. Furthermore, it may provide insufficient margin against breaching the 3 % of GDP deficit threshold with normal macroeconomic fluctuations. However, it is sufficient to maintain the debt-to-GDP ratio at a very low level.
(6) The debt ratio is estimated at 14,2 % of GDP in 2004, well below the 60 % of GDP Treaty reference value. The slight increase in the debt ratio over the programme period, to 15,0 % of GDP in 2007, mainly results from persisting deficits, substantially offset by nominal GDP growth.
(7) The budgetary strategy outlined in the programme puts Latvia in a relatively favourable position with regard to long-term sustainability of the public finances, despite the projected budgetary costs of an ageing population. The relatively low debt ratio, the pension reform measures enacted, including the introduction of the funded pillar, and the accumulation of assets in the funded pension scheme will contribute to limit the budgetary impact of ageing. The strategy outlined in the programme is based on a contained budgetary deficit over the medium term and on the budgetary impact of the pension reform. Reforms in the field of health and long-term care could, however, involve higher expenditures. A risk to sustainability may emerge in the long run. Latvia's relatively low tax ratio should, however, ease the accommodation of any such sustainability gap that may arise.
(8) The economic policies outlined in the programme are broadly consistent with the country-specific broad economic policy guidelines (BEPGs) in the area of public finances. Under the challenge to address the sizable current account deficit, Latvia was recommended to reduce the general government deficit in a credible and sustainable way within a multi-annual framework. In the light of the narrowing current account deficit foreseen in the programme, the lower-than-projected budget deficit in 2004 and the consolidation path foreseen in the update are in line with the recommendation of the BEPGs. However, this assessment is conditional on the favourable development of the external balance and is thus subject to further monitoring. Furthermore, as was pointed out in the Council Opinion on the programme of last May, assessment of the appropriate fiscal position is also dependent on demand pressures in the economy, even if it is difficult to determine the country's position in the business cycle; the assessment is also therefore conditional on macroeconomic policy being appropriate to consolidating the moderation in inflation from its recent peak.
Comparison of key macroeconomic and budgetary projections
Convergence programme (CP); Commission services autumn 2004 economic forecasts (COM); Commission services calculations
| 2004 | 2005 | 2006 | 2007 |
Real GDP(% change) | CP Dec 2004 | 8,1 | 6,7 | 6,5 | 6,5 |
COM Oct 2004 | 7,5 | 6,7 | 6,7 | n.a. |
CP May 2004 | 6,7 | 6,7 | 6,5 | 6,5 |
CPI inflation(%) | CP Dec 2004 | 6,2 | 4,3 | 3,0 | 3,0 |
COM Oct 2004 | 6,8 | 4,7 | 3,5 | n.a. |
CP May 2004 | 4,5 | 3,7 | 3,0 | 3,0 |
General government balance(% of GDP) | CP Dec 2004 | – 1,7 | – 1,6 | – 1,5 | – 1,4 |
COM Oct 2004 | – 2,0 | – 2,8 | – 2,9 | n.a. |
CP May 2004 | – 2,1 | – 2,2 | – 2,0 | – 2,0 |
Primary balance(% of GDP) | CP Dec 2004 | – 0,9 | – 0,9 | – 0,8 | – 0,7 |
COM Oct 2004 | – 1,2 | – 2,0 | – 2,0 | n.a. |
CP May 2004 | – 1,2 | – 1,3 | – 1,2 | – 1,2 |
Government gross debt(% of GDP) | CP Dec 2004 | 14,2 | 14,5 | 15,8 | 15,0 |
COM Oct 2004 | 14,6 | 15,4 | 16,6 | n.a. |
CP May 2004 | 16,2 | 16,8 | 17,3 | 17,7 |
[1] OJ L 209, 2.8.1997, p. 1. The documents referred to in this text can be found at the following websitehttp://europa.eu.int/comm/economy_finance/about/activities/sgp/main_en.htm
--------------------------------------------------
