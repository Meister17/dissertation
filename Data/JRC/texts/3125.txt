Authorisation for State aid pursuant to Articles 87 and 88 of the EC Treaty
Cases where the Commission raises no objections
(2005/C 223/02)
(Text with EEA relevance)
Date of adoption: 8.4.2005
Member State: Greece
Aid No: N 24/2005
Title: Programme for the development of industrial research and technology in enterprises (PAVET)
Objective: Research and development
Legal basis: Π.δ. 274/ 6.10.2000, as amended by π.δ. 103/2003 and ν.3259 (αρ.34)
Budget: EUR 20 million
Intensity or amount: Up to 60 % for industrial research and up to 35 % for pre-competitive development
Duration: Until 31.12.2008
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption: 6.4.2005
Member State: United Kingdom (Scotland)
Aid No: N 244/03
Title: Credit Union Provision of Access to Basic Financial Services
Objective (sector): SGEI
Legal basis: Work Act (Scotland) of 1968 and contract awarded under the Act to each Credit Union
Budget: GBP 5 million per annum
Duration: 5 years
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of decision: 29.3.2005
Member State: Finland
Aid No: N 331/2004
Title: Modification de la carte des aides à finalité régionale 2000-2006
Objective(s): Regional aid
Duration: From to 31.12.2006
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption: 22.6.2005
Member State: Italy
Aid No: N 356/2004
Title: Aid to Tessenderlo — Grant
Objective: Environmental protection
Legal basis: Fondo per la promozione dello Sviluppo Sostenibile: Art. 109 della legge 388/2000
Aid intensity or amount: EUR 5735000
Duration: 2004 — 2006
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 19.5.2004
Member State: Belgium (Flemish Region)
Aid No: N 632/2003
Title: ARKimedes scheme — Activating risk capital in Flanders
Objective: Regional development
Legal basis:
Besluit van de Vlaamse regering houdende uitvoering van het decreet betreffende het activeren van risicokapitaal in Vlaanderen (ARK-besluit)
Ontwerp van decreet betreffende het activeren van risicokapitaal in Vlaanderen (ARK-decreet)
Duration: The last fund will be created in 2009.
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 22.6.2005
Member State: The Czech Republic
Aid No: NN 52/2005
Title: Subsidies to mortgage loan instalments
Objective: Social objective to improve access of persons under 36 years to the proprietary ownership of their first habitation [housing]
Legal basis: Nařízení vlády č. 249/2002 Sb. ze dne 22. května 2002 o podmínkách poskytování příspěvku k hypotečnímu úvěru osobám mladším 36 let, ve znění pozdějších předpisů
Budget: cumulative budget until 2012 — CZK 1228 million (app. EUR 41 million)
Duration: unlimited, in 2007 to be reviewed
Other information: Measure does not constitute State aid in the sense of Article 87(1) of the EC Treaty
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption: 19.1.2005
Member State: Sweden
Case No: NN 66/2003
Title: Prolongation and modification of environmental aid scheme
Objective: To promote the use of windpower
Legal basis: Lagen (1994:1776) om skatt på energi 11 kap. 10 §
Budget: Total loss of tax revenue: SEK 662 million (ca EUR 72 million)
Duration: 2003-2009
Other information: Annual report
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
--------------------------------------------------
