Commission Regulation (EC) No 564/2006
of 6 April 2006
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 7 April 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 6 April 2006.
For the Commission
J. L. Demarty
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 386/2005 (OJ L 62, 9.3.2005, p. 3).
--------------------------------------------------
ANNEX
to Commission Regulation of 6 April 2006 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code [1] | Standard import value |
07020000 | 052 | 98,8 |
204 | 75,3 |
212 | 129,8 |
624 | 88,4 |
999 | 98,1 |
07070005 | 052 | 141,0 |
204 | 66,3 |
999 | 103,7 |
07099070 | 052 | 117,4 |
204 | 52,0 |
999 | 84,7 |
08051020 | 052 | 39,6 |
204 | 40,4 |
212 | 47,7 |
220 | 40,7 |
400 | 62,7 |
624 | 63,8 |
999 | 49,2 |
08055010 | 624 | 64,7 |
999 | 64,7 |
08081080 | 388 | 73,2 |
400 | 139,6 |
404 | 96,7 |
508 | 83,7 |
512 | 82,6 |
524 | 61,0 |
528 | 82,4 |
720 | 102,7 |
804 | 113,5 |
999 | 92,8 |
08082050 | 388 | 83,6 |
512 | 78,8 |
528 | 62,9 |
720 | 44,1 |
999 | 67,4 |
[1] Country nomenclature as fixed by Commission Regulation (EC) No 750/2005 (OJ L 126, 19.5.2005, p. 12). Code "999" stands for "of other origin".
--------------------------------------------------
