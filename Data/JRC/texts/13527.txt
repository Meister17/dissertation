Action brought on 18 July 2006 — SNIA v Commission
Parties
Applicant(s): SNIA S.p.A. (represented by: Alberto Santa Maria and Claudi Biscaretti di Rufia, lawyers,)
Defendant(s): Commission of the European Communities
Forms of order sought
- Annulment of Commission Decision C(2006) 1766 final of 3 May 2006 in Case COMP/F/38.620 — Hydrogen peroxide and sodium perborate in so far as it imposes on SNIA S.p.A., jointly and severally with Caffaro S.r.l., a fine of EUR 1078000.
- The Commission to pay the costs.
Pleas in law and main arguments
The contested decision in this case is the same as in Case T-185/06 L'air Liquide v Commission.
In that decision, the Commission maintains that, at the time of the facts, Industrie Chimiche Caffaro S.p.A. (ICC) was managerially dependent not only on Caffaro S.p.A., a company quoted on the Italian stock exchange and controlling ICC as to 100 %, but also on the applicant, a majority shareholder of Caffaro S.p.A. of between 53 % and 59 %. Essentially on the basis of that indirect link, the applicant is considered jointly and severally liable for the infringement of which the Commission accuses Caffaro S.r.l.
In support of its arguments, the applicant claims:
- that the Commission has not established a relationship of dependency between SNIA and ICC during the period in question. Nor has it demonstrated the existence, during the same period, of such a relationship between Caffaro S.p.A. and ICC.
- that, as regards the relevance of the merger between Caffaro S.p.A. and SNIA for the purposes of identifying a decisive influence of SNIA, the Commission ignored the fact that the merger by incorporation of Caffaro S.p.A. into SNIA S.p.A. (like the change of the company name of ICC to Caffaro S.p.A., now Caffaro S.r.l.) occurred in 2000, that is to say a year after the exit from the market in bleaching agents, and that, as already stated, the Commission has not in any way demonstrated decisive influence by Caffaro S.p.A. over ICC.
- that the only party responsible for the presumed infringement is ICC (now Caffaro S.r.l.), which, moreover, has not ceased to exist as a legal entity but has merely changed its name. On the other hand, even if one were to hold that Caffaro S.p.A. was responsible for the presumed infringement, the legal sucessor of the latter is Caffaro S.r.l. and not SNIA.
--------------------------------------------------
