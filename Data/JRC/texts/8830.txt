*****
COMMISSION DIRECTIVE
of 22 June 1988
adapting to technical progress Council Directive 74/152/EEC on the approximation of the laws of the Member States relating to the maximum design speed and load platforms of wheeled agricultural and forestry tractors
(88/412/EEC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Directive 74/150/EEC of 4 March 1974 on the approximation of the laws of the Member States relating to the type-approval of wheeled agricultural or forestry tractors (1), as last amended by Directive 88/297/EEC (2), and in particular Article 11 thereof,
Whereas, in view of the experience acquired, and taking account of the current state of the art, it is now possible to make certain provisions of Council Directive 74/152/EEC (3), as amended by Directive 82/890/EEC (4), more precise and complete;
Whereas the measures provided for in this Directive are in accordance with the opinion of the Committee on the Adaptation to Technical Progress of the directives aimed at the removal of technical barriers to trade in the agricultural or forestry tractor sector,
HAS ADOPTED THIS DIRECTIVE:
Article 1
In the Annex to Directive 74/152/EEC point 1.3 is hereby replaced by the following:
'1.3. During the test the tractor shall be fitted with new pneumatic tyres having the greatest rolling radius intended by the manufacturer for the tractor'.
Article 2
1. From 1 October 1988 no Member State may:
- refuse, in respect of a type of tractor, to grant EEC type-approval, to issue the document referred to in Article 10 (1), final indent, of Directive 74/150/EEC, or to grant national type approval; or
- prohibit the entry into service of tractors;
if the maximum design speed and load platforms of this type of tractor or tractors comply with the provisions of this Directive.
2. From 1 October 1989 Member States:
- may no longer issue the document referred to in Article 10 (1), final indent, of Directive 74/150/EEC for a type of tractor of which the maximum design speed and load platforms do not comply with the provisions of this Directive;
- may refuse to grant national type approval in respect of a type of tractor of which the maximum design speed and load platforms do not comply with the provisions of this Directive.
Article 3
Member States shall bring into force the provisions necessary in order to comply with this Directive not later than 30 September 1988. They shall forthwith inform the Commission thereof.
Article 4
This Directive is addressed to the Member States.
Done at Brussels, 22 June 1988.
For the Commission
COCKFIELD
Vice-President
(1) OJ No L 84, 28. 3. 1974, p. 10.
(2) OJ No L 126, 20. 5. 1988, p. 52.
(3) OJ No L 84, 28. 3. 1974, p. 33.
(4) OJ No L 378, 31. 12. 1982, p. 45.
