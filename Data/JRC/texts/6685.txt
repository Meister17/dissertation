Agreement
between Ukraine and the European Union on the security procedures for the exchange of classified information
UKRAINE
of the one part, and
THE EUROPEAN UNION, hereinafter the "EU", represented by the Presidency of the Council of the European Union,
of the other part,
hereinafter referred to as "the Parties",
RECOGNISING THAT full and effective consultation and cooperation may require access to Ukraine and EU classified information and material, as well as the exchange of classified information and related material between Ukraine and the EU;
CONSCIOUS THAT such access to and exchange of classified information and related material requires appropriate security measures,
HAVE AGREED AS FOLLOWS:
Article 1
In order to fulfill the objectives of strengthening the security of each of the Parties in all ways, this Agreement shall apply to classified information or material in any form either provided or exchanged between the Parties.
Article 2
For the purposes of this Agreement, classified information shall mean any information (namely, knowledge that can be communicated in any form) or material determined to require protection against unauthorised disclosure and which has been so designated by a security classification (hereinafter classified information).
Article 3
For the purposes of this Agreement, "EU" shall mean the Council of the European Union (hereinafter Council), the Secretary-General/High Representative and the General Secretariat of the Council, and the Commission of the European Communities (hereinafter European Commission).
Article 4
Each Party shall:
(a) protect and safeguard classified information subject to this Agreement provided or exchanged by the other Party;
(b) ensure that classified information subject to this Agreement provided or exchanged keeps the security classification given to it by the providing Party. The receiving Party shall protect and safeguard the classified information according to the provisions set out in its own security regulations for information or material holding an equivalent security classification, as specified in the security arrangements established pursuant to Article 11;
(c) not use such classified information subject to this Agreement for purposes other than those established by the originator and those for which the information is provided or exchanged;
(d) not disclose such classified information subject to this Agreement to third parties, or to any EU institution or entity not mentioned in Article 3, without the prior consent of the originator.
Article 5
1. Classified information may be disclosed or released, in accordance with the principle of originator control, by one Party, "the providing Party", to the other Party, "the receiving Party".
2. For release to recipients other than the Parties to this Agreement, a decision on disclosure or release of classified information shall be made by the receiving Party following the consent of the providing Party, in accordance with the principle of originator control as defined in its security regulations.
3. In implementing paragraphs 1 and 2, the release of batches of specific categories of classified information relevant to operational requirements shall only be possible provided that appropriate procedures are established and agreed between the Parties.
Article 6
Ukraine and the EU and the entities of the latter as defined in Article 3 of this Agreement shall have a security organisation, security regulations and security programmes, based upon such basic principles and minimum standards of security which shall be implemented in the security systems of the Parties established pursuant to Article 11, to ensure that an equivalent level of protection is applied to classified information subject to this Agreement.
Article 7
1. The Parties shall ensure that all persons who, in the conduct of their official duties require access, or whose duties or functions may afford access, to classified information provided or exchanged under this Agreement are appropriately security cleared before they are granted access to such information.
2. The security clearance procedures shall be designed to determine whether an individual may have access to classified information, taking into account the requirements, including those for establishing their trustworthiness and reliability, laid down in regulations and guidelines established by each of the Parties.
Article 8
The Parties shall provide mutual assistance with regard to security of classified information subject to this Agreement and matters of common security interest. Reciprocal security consultations and inspections shall be conducted by the authorities as defined in Article 11 to assess the effectiveness of the security arrangements within their respective responsibility established pursuant to Article 11.
Article 9
1. For the purpose of the present Agreement
(a) As regards the EU:
all correspondence shall be sent to the Council at the following address:
Council of the European Union
Chief Registry Officer
Rue de la Loi/Wetstraat, 175
B-1048 Brussels.
All correspondence shall be forwarded by the Chief Registry Officer of the Council to the Member States and to the European Commission subject to paragraph 2.
(b) As regards Ukraine:
all correspondence shall be addressed to the Chief of the EU Documentation Central Registry Office of the Ministry of Foreign Affairs of Ukraine, at the following address:
Ministry of Foreign Affairs of Ukraine
Chief of the EU Documentation Central Registry Office
Mykhailivska square, 1
01018 Kiev
Ukraine.
2. Exceptionally, correspondence from one Party which is only accessible to specific competent officials, organs or services of that Party may, for operational reasons, be addressed and only be accessible to specific competent officials, organs or services of the other Party specifically designated as recipients, taking into account their competencies and according to the need-to-know principle. Such correspondence shall be appropriately marked. As far as the European Union is concerned, this correspondence shall be transmitted through the Chief Registry Officer of the Council. As far as Ukraine is concerned, this correspondence shall be transmitted through the Chief of the EU Documentation Central Registry Office of the Ministry of Foreign Affairs of Ukraine.
Article 10
The Ministry of Foreign Affairs of Ukraine and the Secretaries-General of the Council and of the European Commission shall oversee the implementation of this Agreement.
Article 11
1. In order to implement this Agreement, security arrangements shall be established between the three authorities designated in paragraphs 2 to 4 in order to lay down the standards of the reciprocal security protection for classified information subject to this Agreement.
2. The Security Service of Ukraine, acting in the name of Ukraine and under its authority, shall be responsible for developing the security arrangements mentioned in paragraph 1 for the protection and safeguarding of classified information provided to Ukraine under this Agreement.
3. The Security Office of the General Secretariat of the Council, under the direction and on behalf of the Secretary General of the Council, acting in the name of the Council and under its authority, shall be responsible for developing the security arrangements mentioned in paragraph 1 for the protection and safeguarding of classified information provided to the EU under this Agreement.
4. The European Commission Security Directorate, acting in the name of the European Commission and under its authority, shall be responsible for developing the security arrangements mentioned in paragraph 1 for the protection of classified information provided or exchanged under this Agreement within the European Commission and its premises.
5. For the EU, these standards shall be subject to approval by the Council Security Committee.
Article 12
The authorities defined in Article 11 shall establish procedures to be followed in the case of proven or suspected compromise of classified information subject to this Agreement.
Article 13
Prior to the provision of classified information subject to this Agreement between the Parties, the responsible security authorities defined in Article 11 must agree that the receiving Party is able to protect and safeguard the information subject to this Agreement in a way consistent with the arrangements established pursuant to Article 11.
Article 14
This Agreement shall in no way prevent the Parties from concluding other agreements relating to the provision or exchange of classified information subject to this Agreement provided that they do not conflict with the provisions of this Agreement.
Article 15
All differences between the EU and Ukraine arising out of the interpretation or application of this Agreement shall be dealt with by negotiation between the Parties.
Article 16
1. This Agreement shall enter into force on the first day of the first month after the Parties have notified each other of the completion of the internal procedures necessary for this purpose.
2. This Agreement may be reviewed for consideration of possible amendments at the request of either Party.
3. Any amendment to this Agreement shall only be made in writing and by common agreement of the Parties. It shall enter into force upon mutual notification as provided under paragraph 1.
Article 17
This Agreement may be denounced by one Party by written notice of denunciation given to the other Party. Such denunciation shall take effect six months after receipt of notification by the other Party, but shall not affect obligations already contracted under the provisions of this Agreement. In particular, all classified information provided or exchanged pursuant to this Agreement shall continue to be protected in accordance with the provisions set forth herein.
IN WITNESS WHEREOF the undersigned, respectively duly authorised, have signed this Agreement.
Done at Luxembourg, 13 June 2005 in two copies each in the English language.
For Ukraine
For the European Union
--------------------------------------------------
