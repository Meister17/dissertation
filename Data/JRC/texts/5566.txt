Commission Decision
of 14 March 2005
amending Annex II to Council Decision 79/542/EEC as regards imports of fresh meat from Argentina
(notified under document number C(2005) 602)
(Text with EEA relevance)
(2005/234/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 72/462/EEC of 12 December 1972 on health and veterinary inspection problems upon importation of bovine, ovine and caprine animals and swine, fresh meat or meat products from third countries [1], and in particular the second subparagraph of Articles 3(1) and 16(1) thereof,
Having regard to Council Directive 2002/99/EC of 16 December 2002 laying down the animal health rules governing the production, processing, distribution and introduction of products of animal origin for human consumption [2], and in particular Article 8(1) and (4),
Whereas:
(1) Part 1 of Annex II to Council Decision 79/542/EEC of 21 December 1979 drawing up a list of third countries or parts of third countries, and laying down animal and public health and veterinary certification conditions, for importation into the Community of certain live animals and their fresh meat [3] sets out a list of third countries and parts of third countries from which Member States are authorised to import certain live animals and their fresh meat.
(2) Following an outbreak of foot-and-mouth disease ("FMD") that occurred in September 2003 in Argentina in the province of Salta, in the north of the country close to the border with Paraguay, Commission Decision 93/402/EEC of 10 June 1993 concerning animal health conditions and veterinary certification for imports of fresh meat from South American countries [4] was amended by Decisions 2003/658/EC [5] and 2003/758/EC [6], in order to suspend importation of deboned and matured bovine meat from the Argentinean provinces of Salta, Jujuy, Chaco and Formosa. Decision 93/402/EEC has been repealed by Decision 2004/212/EC [7] and its provisions have been incorporated in Decision 79/542/EEC.
(3) The outbreak has been closed for more than 12 months and no new outbreaks have been detected in the territory of Argentina. In the meantime, inspection missions carried out in 2004 have shown that the situation in Argentina has improved in regard to both animal and public health.
(4) The Commission requested Argentina to provide for a buffer zone along its borders with Bolivia and Paraguay in order to avoid the risk of introduction of FMD. Argentina proposed a zone with a width of 25 kilometres along its border with those countries ("the buffer zone"). Detailed maps and information, in particular concerning control measures in place in the buffer zone, have now been provided to the Commission. Importation into the Community from the buffer zone of fresh meat from species susceptible to FMD should be prohibited.
(5) It is therefore appropriate to resume importation of deboned and matured bovine fresh meat from the provinces of Salta, Jujuy, Chaco and Formosa with the exception of the buffer zone.
(6) Decision 79/542/EEC has been amended by Decision 2004/212/EC inter alia in order to exclude meat products from its scope. It is therefore appropriate to delete in Article 1 the erroneous reference to meat products.
(7) Article 1 and Part 1 of Annex II to Decision 79/542/EEC should be amended accordingly.
(8) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
In Article 1 to Decision 79/542/EEC the words "and meat products" are deleted.
Article 2
Part 1 of Annex II to Decision 79/542/EEC is replaced by the text in the Annex to this Decision.
Article 3
This Decision shall apply from 18 March 2005.
Article 4
This Decision is addressed to the Member States.
Done at Brussels, 14 March 2005.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 302, 31.12.1972, p. 28. Directive as last amended by Regulation (EC) No 807/2003 (OJ L 122, 16.5.2003, p. 36).
[2] OJ L 18, 23.1.2003, p. 11.
[3] OJ L 146, 14.6.1979, p. 15. Decision as last amended by Commission Decision 2004/620/EC (OJ L 279, 28.8.2004, p. 30).
[4] OJ L 179, 22.7.1993, p. 11.
[5] OJ L 232, 18.9.2003, p. 59.
[6] OJ L 272, 23.10.2003, p. 16.
[7] OJ L 73, 11.3.2004, p. 11.
--------------------------------------------------
ANNEX
--------------------------------------------------
