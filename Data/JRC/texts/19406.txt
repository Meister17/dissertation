Commission Regulation (EC) No 1989/2003
of 6 November 2003
amending Regulation (EEC) No 2568/91 on the characteristics of olive oil and olive-pomace oil and on the relevant methods of analysis
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation No 136/66/EEC of 22 September 1966 on the establishment of a common organisation of the market in oils and fats(1), as last amended by Regulation (EC) No 1513/2001(2), and in particular Article 35a thereof,
Whereas:
(1) Commission Regulation (EC) No 2568/91(3), as last amended by Regulation (EC) No 796/2002(4), defines the physical, chemical and organoleptic characteristics of olive oils and olive-pomace oils and lays down methods of assessing these characteristics. The characteristics of the oils in question must be adjusted to take into account new descriptions and definitions of olive oil and olive-pomace oil applicable from 1 November 2003 following an amendment of the Annex to Regulation No 136/66/EEC, and in particular the inclusion of the category of ordinary virgin olive oil in the category of lampante olive oil and the reduction of the free acidity of all the categories.
(2) To continue the process of harmonisation with the international standards laid down by the International Olive Oil Council and the Codex Alimentarius, certain limits concerning the characteristics of olive oil and olive-pomace oil laid down in Regulation (EEC) No 2568/91 should be adjusted.
(3) To reduce the number of analyses required to classify the samples of olive oils concerned, it is preferable that the control laboratories carry out the analyses of the quality and purity of the oils in the order laid down in a decision tree to be adopted for verifying whether a sample is consistent with the category declared. It is also necessary to delete the methods of analysis provided for in Annexes VIII and XIII to Regulation (EEC) No 2568/91, which have been replaced by other, more reliable analyses already set out in that Regulation.
(4) Sampling of batches of olive oils and oil-pomace oils put up in small packages, in accordance with Annex Ia to Regulation (EEC) No 2568/91, involves a number of practical difficulties for the control laboratories. To resolve these difficulties and reduce the quantities taken as much as possible, the content of the primary sample should be amended.
(5) In order to allow a period of adjustment to the new standards and to give time for introducing the means of applying them and to avoid disturbance to commercial transactions, the amendments to this Regulation should not apply until 1 November 2003. For the same reasons, it should be laid down that olive oil and olive-pomace oil packaged prior to that date may be sold until the stocks concerned are used up.
(6) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Oils and Fats,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EEC) No 2568/91 is hereby amended as follows:
1. Article 1 is replaced by the following:
"Article 1
1. Oils, the characteristics of which comply with those set out in points 1 and 2 of Annex I to this Regulation, shall be deemed to be virgin olive oils within the meaning of point 1(a) and (b) of the Annex to Regulation No 136/66/EEC.
2. Oil, the characteristics of which comply with those set out in point 3 of Annex I to this Regulation, shall be deemed to be lampante olive oil within the meaning of point 1(c) of the Annex to Regulation No 136/66/EEC.
3. Oil, the characteristics of which comply with those set out in point 4 of Annex I to this Regulation, shall be deemed to be refined olive oil within the meaning of point 2 of the Annex to Regulation No 136/66/EEC.
4. Oil, the characteristics of which comply with those set out in point 5 of Annex I to this Regulation, shall be deemed to be olive oil composed of refined olive oils and virgin olive oils within the meaning of point 3 of the Annex to Regulation No 136/66/EEC.
5. Oil, the characteristics of which comply with those set out in point 6 of Annex I to this Regulation, shall be deemed to be crude olive-pomace oil within the meaning of point 4 of the Annex to Regulation No 136/66/EEC.
6. Oil, the characteristics of which comply with those set out in point 7 of Annex I to this Regulation, shall be deemed to be refined olive-pomace oil within the meaning of point 5 of the Annex to Regulation No 136/66/EEC.
7. Oil, the characteristics of which comply with those set out in point 8 of Annex I to this Regulation, shall be deemed to be olive-pomace oil within the meaning of point 6 of the Annex to Regulation No 136/66/EEC."
2. In Article 2(1), the seventh and 12th indents are deleted.
3. The first subparagraph of Article 2(4) is replaced by the following:"For the purposes of the verification provided for in paragraph 3, the analyses referred to in Annexes II, III, IX, X and XII and, where applicable, any counter-analyses required under national law, shall be carried out before the minimum durability date. Where sampling is done more than four months before the minimum durability date, the analyses shall be carried out no later than the fourth month after the month in which the sample was taken. No time limit shall apply to the other analyses provided for in that Regulation."
4. The following Article 2a is inserted:
"Article 2a
The national authorities or their representatives may verify whether a sample is consistent with the category declared:
(a) either by carrying out, in any order, the analyses provided for in Annex I;
(b) or by following the order set out in Annex Ib on the decision tree until one of the decisions appearing in the decision tree is reached."
5. Article 7 is replaced by the following:
"Article 7
The Community provisions concerning the presence of contaminants shall apply.
As regards halogenated solvents, the limits for all categories of olive oils are as follows:
- maximum content of each halogenated solvent detected: 0,1 mg/kg,
- maximum total content of halogenated solvents detected: 0,2 mg/kg."
6. The Annexes are amended in accordance with the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Union.
It shall apply from 1 November 2003.
However, products which have been legally manufactured and labelled in the Community or legally imported into the Community and put into free circulation before 1 November 2003 may be marketed until all stocks are used up.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 6 November 2003.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ 172, 30.9.1966, p. 3025/1966.
(2) OJ L 201, 26.7.2001, p. 4.
(3) OJ L 248, 5.9.1991, p. 1.
(4) OJ L 128, 15.5.2002, p. 8.
ANNEX
The Annexes to Regulation (EEC) No 2568/91 are amended as follows:
1. The summary is amended as follows:
(a) the following line is inserted after the line corresponding to Annexe Ia:
"Annexe Ib: Decision tree";
(b) the lines corresponding to Annexes VIII and XIII are deleted.
2. Annex I is replaced by the following tables and texts:
"ANNEX I
CHARACTERISTICS OF OLIVE OIL TYPES
>TABLE>
>TABLE>
Notes:
(a) The results of the analyses must be expressed to the same number of decimal places as used for each characteristic. The last digit must be increased by one unit if the following digit is greater than 4.
(b) If just a single characteristic does not match the values stated, the category of an oil can be changed or the oil declared impure for the purposes of this Regulation.
(c) If a characteristic is marked with an asterisk (*), referring to the quality of the oil, this means the following:
- for lampante olive oil, it is possible for both the relevant limits to be different from the stated values at the same time,
- for virgin olive oils, if at least one of these limits is different from the stated values, the category of the oil will be changed, although they will still be classified in one of the categories of virgin olive oil.
(d) If a characteristic is marked with two asterisks (**) this means that for all types of olive-pomace oil, it is possible for both the relevant limits to be different from the stated values at the same time."
3. Annex Ia is replaced by the following:
"ANNEX Ia
Sampling of olive oil or olive-pomace oil delivered in immediate packaging not exceeding 100 litres
This method of sampling applies to deliveries of olive oil or olive-pomace oil not exceeding 125000 litres, put up in immediate packaging not exceeding 100 litres.
If the delivery exceeds 125000 litres, it is to be subdivided into batches of 125000 litres or under. If the delivery is less than 125000 litres it shall constitute one batch. The method shall then be applied to each batch.
The minimum number of primary samples to be taken is determined by the size of the batch in accordance with the table set out in point 1.
The size of the primary sample is determined on the basis of the capacity of the immediate packaging, in accordance with the table set out in point 2.1.
Delivery, primary sample and laboratory sample shall mean the definitions given in standard EN ISO 5555.
"Batch" shall mean a set of sales units which are produced, manufactured and packed in circumstances such that the oil contained in each sales unit is considered to be homogenous in terms of all analytical characteristics.
1. NUMBER OF PRIMARY SAMPLES TO BE TAKEN
The minimum number of primary samples to be taken will be determined by the size of the batch in accordance with the following table:
>TABLE>
The immediate packs selected to form a primary sample must be adjacent to each other in the batch.
In cases of doubt, Member States shall increase the number of primary samples to be taken.
2. CONTENT OF PRIMARY SAMPLES
2.1 Primary samples must comprise the following:
>TABLE>
2.2 The primary samples are to be kept in the immediate packaging up to the time of analysis. The oil in the primary samples shall then, as applicable, be subdivided into three laboratory samples in order to carry out:
(a) the analyses referred to in Annexes II, III, IX and X,
(b) the analysis referred to in Annex XII,
(c) the other analyses.
2.3 The packs constituting a primary sample shall be subdivided in accordance with the control procedures provided for in national law.
3. ANALYSES AND RESULTS
(a) Each of the primary samples referred to in point 1 shall be subdivided into laboratory samples, in accordance with point 2.5 of standard EN ISO 5555, and analysed as follows:
- determination of free fatty acids, as referred to in the first indent of Article 2(1),
- determination of the peroxide value, as referred to in the second indent of Article 2(1),
- spectrophotometric analysis, as referred to in the eighth indent of Article 2(1),
- determination of the fatty acid composition, as referred to in the ninth indent of Article 2(1).
(b) Where one of the results of the analyses referred to in (a) for at least one of the primary samples taken from the same batch does not comply with the characteristics of the category of oil declared, the whole of the batch concerned is to be declared not to comply.
Where the results of the analyses referred to in (a) for each of the primary samples taken from the same batch are not all uniform, given the repeatability characteristics of the methods concerned, the entire batch is to be declared non-uniform and each primary sample is to be subject to the other analysis required. Otherwise, one primary sample from that batch is to be subject to the other analysis required.
(c) Where one of the results of the analyses referred to in the second paragraph of point (b) does not comply with the characteristics of the category of oil declared, the whole of the batch concerned is to be declared not to comply.
Where all the results of the analyses referred to in the second paragraph of point (b) comply with the characteristics of the category of oil declared, the whole batch is to be declared to comply."
4. The following Annex Ib is inserted after Annex Ia.
"ANNEX Ib
DECISION TREE FOR VERIFYING WHETHER AN OLIVE OIL SAMPLE IS CONSISTENT WITH THE CATEGORY DECLARED
The analysis to verify whether an olive oil or olive-pomace oil is consistent with the category declared may be undertaken:
(a) either by carrying out in any random order the analyses envisaged for the purpose of verifying compliance with the characteristics specified in Annex I; or
(b) by carrying out in the order shown in the decision tree the analyses specified therein until one of the decisions appearing in the decision tree is reached.
The analyses relating to contaminants required for verifying compliance with European Community standards are to be carried out separately.
The decision tree applies to all categories of olive oil and olive-pomace oil. It consists of tables numbered 1 to 11 which must be approached on the basis of the declared category of oil concerned in the order set out in the general table.
Key to general tables to 11:
- the double line (=) indicates the route to be followed in case of compliance (positive answer) with the criteria specified in the preceding box. The dotted line (...) indicates the alternative route to be followed in case of non-compliance,
- the headings in the boxes in tables 1 to 11 refer to the analyses provided for in this Regulation on the basis of the table of equivalence set out in Appendix 1 to this Annex,
- the letters in brackets appearing in the negative decision circles in tables 1 to 11 cross-refer to indicative information given in Appendix 2 to this Annex. The letters in themselves do not entail the obligation to pursue the analyses or imply the veracity of the assumptions stated.
General table
>PIC FILE= "L_2003295EN.006401.TIF">
Table 1
>PIC FILE= "L_2003295EN.006501.TIF">
Table 2
>PIC FILE= "L_2003295EN.006601.TIF">
Table 3
>PIC FILE= "L_2003295EN.006701.TIF">
Table 4
>PIC FILE= "L_2003295EN.006801.TIF">
Table 5
>PIC FILE= "L_2003295EN.006901.TIF">
Table 6
>PIC FILE= "L_2003295EN.007001.TIF">
Table 7
>PIC FILE= "L_2003295EN.007101.TIF">
Table 8
>PIC FILE= "L_2003295EN.007201.TIF">
Table 9
>PIC FILE= "L_2003295EN.007301.TIF">
Table 10
>PIC FILE= "L_2003295EN.007401.TIF">
Table 11
>PIC FILE= "L_2003295EN.007501.TIF">
Appendix 1
Table of equivalence between the annexes to this Regulation and the analyses specified in the decision tree
>TABLE>
Appendix 2
Table 1
(a) See virgin or lampante olive oil (Quality criteria Table 2, or Quality and purity criteria Table 4)
(b) See lampante olive oil (Quality and purity criteria Table 4)
Table 2
(a) See lampante olive oil (Quality and purity criteria Table 4)
(b) See extra virgin olive oil (Quality criteria Table 1)
Table 3
(a) Presence of refined oil (olive or others)
(b) Presence of olive-pomace oil
Table 4
(a) See extra virgin olive oil and virgin olive oil (Quality criteria Table 1 and Table 2)
(b) Presence of refined oil (olive or others)
(c) Presence of olive-pomace oil
(d) Presence of esterified oils
Table 7
(a) Presence of olive-pomace oil
(b) Presence of esterified oils
Table 8
(a) Presence of refined oil (olive or others)
(b) See lampante olive oil (Quality and purity criteria Table 4)
(c) Presence of esterified oils
Table 11
(a) Presence of esterified oils"
5. Annex VIII is deleted.
6. Annex XIII is deleted.
