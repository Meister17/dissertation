Reference for a preliminary ruling from the Commissione Tributaria Regionale di Genova lodged on 3 April 2006 — Agrover S.r.l. v Agenzia Dogane Circoscrizione Doganale di Genova
Referring court
Commissione Tributaria Regionale di Genova
Parties to the main proceedings
Applicant: Agrover S.r.l.
Defendant: Agenzia Dogane Circoscrizione Doganale di Genova
Questions referred
1. Can Article 216 of the Community Customs Code (Regulation (EEC) No 2913/92 of 12 October 1992) apply where a Community product (rice) previously exported under the inward processing procedure with an EUR1 certificate to a non-member country (with which an agreement on preferential tariff treatment is in force) gives rise to the application of customs duties at the time of the subsequent compensating reimportation of the same (or equivalent) goods from a so-called "non-agreement" non-member country?
2. If duties under Article 216 of the Community Customs Code are not levied at the time of the compensating importation, may the customs authorities seek to recover them a posteriori, or does the exemption referred to in Article 220 of the Community Customs Code apply?
--------------------------------------------------
