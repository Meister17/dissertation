COUNCIL DECISION of 8 November 1971 applying Articles 123 to 127 of the Treaty to the French overseas departments (71/364/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 227 (2) thereof;
Having regard to the proposal from the Commission;
Whereas the second subparagraph of Article 227 (2) provides that the Council shall determine the conditions under which the provisions of the Treaty other than those referred to in the first subparagraph of Article 227 (2) and in particular the provisions of Articles 123 to 127 of the Treaty, are to apply to the French overseas departments;
Whereas intervention by the European Social Fund is likely to promote the economic and social development of those departments;
HAS DECIDED AS FOLLOWS:
Article 1
Articles 123 to 127 of the Treaty establishing the European Economic Community and the measures taken in implementation of those Articles shall apply to the French overseas departments.
Article 2
This Decision shall be published in the Official Journal of the European Communities.
It shall enter into force on the date on which Council Regulation (EEC) No 2396/71 1 of 8 November 1971, applying the Council Decision of 1 February 1971 on the reform of the European Social Fund, enters into force.
Done at Brussels, 8 November 1971.
For the Council
The President
A. MORO 1OJ No L 249, 10.11.1971, p. 54.
