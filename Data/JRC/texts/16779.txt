Commission Regulation (EC) No 672/2004
of 13 April 2004
setting, for the 2003/2004 marketing year, the amounts to be paid to olive oil producer organisations and associations thereof recognised under Council Regulation No 136/66/EEC
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation No 136/66/EEC of 22 September 1966 on the establishment of a common organisation of the market in oils and fats(1), and in particular Article 20d(4) thereof,
Whereas:
(1) Article 20d(1) of Regulation No 136/66/EEC provides for a percentage of production aid to be withheld to help finance the work of recognised producer organisations and associations thereof. For the 1998/1999, 1999/2000, 2000/2001, 2001/2002, 2002/2003 and 2003/2004 marketing years that percentage is 0,8 %.
(2) Article 21(1) of Commission Regulation (EC) No 2366/98 of 30 October 1998 laying down detailed rules for the application of the system of production aid for olive oil for the 1998/1999, 1999/2000, 2000/2001, 2001/2002, 2002/2003 and 2003/2004 marketing years(2) provides that the unit amounts to be paid to producer organisations and associations thereof are to be fixed on the basis of the forecasts of the overall sum to be distributed. The funds that will become available in each Member State as a result of the amount withheld as referred to above must be distributed in an appropriate way among those eligible.
(3) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Oils and Fats,
HAS ADOPTED THIS REGULATION:
Article 1
For the 2003/04 marketing year, the amounts provided for in Article 21(1)(a) and (b) of Regulation (EC) No 2366/98 shall be as follows:
>TABLE>
Article 2
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 13 April 2004.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ 172, 30.9.1966, p. 3025/66. Regulation as last amended by Regulation (EC) No 1513/2001 (OJ L 201, 26.7.2001, p. 4).
(2) OJ L 293, 31.10.1998, p. 50. Regulation as last amended by Regulation (EC) No 1780/2003 (OJ L 260, 11.10.2003, p. 6).
