P6_TA(2004)0084
Euro-Mediterranean partnership *
European Parliament legislative resolution on the proposal for a Council regulation on financial and technical measures to accompany (MEDA) the reform of economic and social structures in the framework of the Euro-Mediterranean partnership (Codified version) (COM(2004)0077 — C5-0091/2004 — 2004/0024(CNS))
(Consultation procedure)
The European Parliament,
- having regard to the Commission proposal to the Council (COM(2004)0077) [1],
- having regard to Article 308 of the EC Treaty, pursuant to which the Council consulted Parliament (C5-0091/2004),
- having regard to Rules 51, 80 and 43(1) of its Rules of Procedure,
- having regard to the report of the Committee on Legal Affairs (A6-0045/2004),
1. Approves the Commission proposal;
2. Calls on the Council to notify Parliament if it intends to depart from the text approved by Parliament;
3. Asks the Council to consult Parliament again if it intends to amend the Commission proposal substantially;
4. Instructs its President to forward its position to the Council and Commission.
[1] Not yet published in OJ.
--------------------------------------------------
