DECISION OF THE EXECUTIVE COMMITTEE
of 16 December 1998
on the introduction of a harmonised form providing proof of invitation, sponsorship and accommodation
(SCH/Com-ex (98) 57)
THE EXECUTIVE COMMITTEE,
Having regard to Article 132 of the Convention implementing the Schengen Agreement,
Having regard to Article 9 of the abovementioned Convention,
Whereas it is in the interest of all the Schengen States to apply uniform rules to the issue of visas in the context of their common policy on the movement of persons, with a view to preventing possible negative consequences as regards entry into Schengen territory and internal security,
Wishing to build on hitherto positive experiences of the Common Consular Instructions and to harmonise further the visa-issue procedure,
Guided by the principle of solidarity between the Schengen Partners,
HAS DECIDED AS FOLLOWS:
Point 1.4, Chapter V of the Common Consular Instructions on Visas (Verification of other documents depending on the application) lays down the use of a harmonised form providing proof of accommodation.
The form attached hereto allows for a high degree of flexibility and for use of the form adapted to the legal situation of each Contracting Party, since the Schengen States currently use very differing forms, for different types of obligation.
These differences have the effect in particular of increasing the risk of misuse and for that reason, the Schengen States are introducing a document incorporating features aimed at preventing counterfeiting and falsification.
Consequently, the standard document is uniform in terms of its:
- layout and structure and;
- security features.
The standard form shall be introduced in 1999 in the States implementing the Schengen Convention in which national law makes provision for this kind of proof.
1. The following sentence shall be added to point 1.4, Chapter V of the Common Consular Instructions: "Where the national laws of the Schengen States require proof of invitations from private individuals or for business trips, sponsorship declarations or proof of accommodation, a harmonised form shall be used(1)."
2. The Schengen Contracting Parties shall complete the harmonised form in accordance with their national laws.
3. The harmonised form to be used by the Schengen Contracting Parties for sponsorship declarations, invitations or proof of accommodation shall be drawn up at a central level, in accordance with the specifications laid down in Annex A (technical description of security features) and in Annexes A1 and A2 (specimen). The obligatory standard elements of the harmonised form are given in Annex B.
4. Specimens of the documents issued by the Contracting Parties shall be added to the Common Consular Instructions in the form of Annex 15.
5. France shall supply the films required for the production of the forms to the other Schengen States. The Contracting Parties shall bear the costs jointly.
6. The security features of the document shall be scrutinised at regular intervals (every two years, if possible) regardless of any general amendments which may prove necessary if the form is falsified by forgers or counterfeiters or if protective measures relating to security features have been revealed.
7. The document shall be produced in at least three languages.
8. This Decision shall enter into force when the Schengen States have sent notification that they have implemented the requisite measures.
Berlin, 16 December 1998.
The Chairman
C. H. Schapper
(1) Belgium, Denmark, Finland, France, Germany, Greece, Iceland, Italy, Luxembourg, the Netherlands, Norway, Austria, Portugal and Sweden apply these principles.
CONFIDENTIAL
Annex A
Technical description of the form
Annex A1
>PIC FILE= "L_2000239EN.030302.EPS">
>PIC FILE= "L_2000239EN.030401.EPS">
Annex A2
>PIC FILE= "L_2000239EN.030502.EPS">
>PIC FILE= "L_2000239EN.030601.EPS">
Annex B
>PIC FILE= "L_2000239EN.030702.EPS">
