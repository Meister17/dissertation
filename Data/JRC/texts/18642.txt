COUNCIL REGULATION (EEC) No 404/93 of 13 February 1993 on the common organization of the market in bananas
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Articles 42 and 43 thereof,
Having regard to the Protocol on the tariff quota for imports of bananas annexed to the Implementing Convention on the Association of the Overseas Countries and Territories of the Community provided for in Article 136 of the Treaty, and in particular paragraph 4 thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas the operation and development of the common market for agricultural products must be accompanied by the introduction of a common agricultural policy including in particular a common organization of the agricultural markets which may take different forms depending on the products;
Whereas there currently exist within the Member States of the Community producing bananas national market organizations which seek to ensure that producers can dispose of their products on the national market and receive an income in line with the costs of production; whereas these national market organizations impose quantitative restrictions which hamper achievement of a single market for bananas; whereas some of the Member States which do not produce bananas provide preferential outlets for bananas from the ACP States while others have liberal importation rules, which even in one case include a privileged tariff situation; whereas these different arrangements prevent the free movement of bananas within the Community and implementation of common arrangements for trade with third countries; whereas, for the purposes of achievement of the single market, a balanced and flexible common organization of the market for the banana sector must replace the various national arrangements;
Whereas, so that the Community can respect Community Preference and its various international obligations, that common organization of the market should permit bananas produced in the Community and those from the ACP States which are traditional suppliers to be disposed of on the Community market providing an adequate income for producers and at fair prices for consumers without undermining imports of bananas from other third countries suppliers;
Whereas, to permit the market to be supplied with products of uniform and satisfactory quality with due regard to local peculiarities and the varieties produced and to ensure the disposal of Community products at profitable prices guaranteeing an adequate income, quality standards for fresh bananas and, if necessary, marketing rules for processed products on bananas should be introduced;
Whereas, in order to maximize income from bananas produced within the Community, the formation of producers' organizations should be encouraged, principally through the granting of start-up aid; whereas, in order for such organizations to be given an effective role in concentrating supply, their members should undertake to market the whole of their production through the organization; whereas the formation of other types of associations, comparising producers' organizations and representatives of the other stages of production, should also be permitted; whereas the conditions under which such associations representing the various aspects of the banana sector may undertake measures of general interest and have their rules extended to non-members of either locally or regionally should be defined at a later date; whereas such organizations could also be consulted when framework programmes are drawn up and play an active role in implementing structural measures introduced as part of the market organization;
Whereas the structural shortcomings which restrict the ability of Community production to compete should be mitigated, chiefly to raise productivity; whereas programmes to achieve this in each region of production should be laid down under the Community support frameworks through cooperation between the Commission and the national and regional authorities and with the various types of organization in the sector referred to above being associated as closely as possible with preparation of the measures to be introduced;
Whereas national market organizations have hitherto enabled national banana producers to obtain from the market an adequate income to cover their production costs; whereas, since introduction of the market organization should not place producers in a worse situation than at present, and since it is likely to alter the levels of prices on those markets, provision should be made for compensation to cover the loss of income which may derive from implementation of the new system so as to permit the continuation of Community production at the costs entailed by the specific structural situation for as long as this remains unadjusted by the structural measures implemented; whereas provision should be made for aid to be adjusted to take account of increased productivity and trends in the various quality categories;
Whereas in certain very small regions of the Community where conditions are particularly unsuited to the production of bananas but better suited to alternative crops the definitive cessation of banana production should be encouraged through a premium for the cessation of banana production; whereas, in order to limit the cost of this operation, grubbing up should be carried out as soon as possible;
Whereas a forecast supply balance drawn up each year should assess the prospects for production and consumption in the Community; whereas it should be possible to revise that balance during the year in the light of circumstances, including specific climatic events;
Whereas in order to ensure satisfactory marketing of bananas produced within the Community and of products originating in the ACP States within the framework of the Lomé Convention Agreements, while maintaining traditional trade patterns as far as possible, provision should be made for the annual openin of a tariff quota; whereas, on the one hand, imports of bananas from third countries would be subject to a tariff of ECU 100 per tonne, which corresponds to the current rate under the Common Customs Tariff, and, on the other hand, imports of non-traditional bananas from the ACP would be subject to zero duty in accordance with the abovementioned agreements; whereas provision should be made in order to ensure the adaptation of the amount of the tariff quota based on changes in Community demand recorded in the forecast supply balance;
Whereas imports not falling within the tariff quota must be subject to sufficiently high rates of duty to ensure that Community production and traditional ACP quantities are disposed of in acceptable conditions;
Whereas traditional imports of bananas from the ACP fall outside the duty-free tariff quota as part of traditional quantities which takes account of specific investments already made under programmes for increasing production;
Whereas in order to comply with the aims stated above, while taking into account the special features of marketing bananas, a distinction must be made when administering the tariff quota between, on the one hand, operators who have previously marketed third country bananas and non-traditional ACP bananas and, on the other, operators who have previously marketed bananas produced in the Community while leaving a quantity available for new operators who have recently embarked on commercial activity or are about to embark on commercial activity in this sector;
Whereas, in order not to disrupt existing commercial links, while at the same time allowing some development of marketing structures, the issue to each operator of separate import licences for each of the categories defined above must be on the basis of the average quantity of bananas marketed by the operator over the three preceding years for which statistical data are available;
Whereas in adopting additional criteria which operators should respect, the Commission is guided by the principle whereby the licences must be granted to natural or legal persons who have undertaken the commercial risk of marketing bananas and by the necessity of avoiding disturbing normal trading relations between persons occupying different points in the marketing chain;
Whereas, taking into account marketing structures, Member States must, on the basis of procedures and criteria adopted by the Commission, conduct a census of operators and establish quantities marketed to be used as a reference for the issue of certificates;
Whereas monitoring imports within the framework of the tariff quota requires an import licence scheme backed by a security;
Whereas the Commission should be able to take appropriate measures to deal with serious disturbances, or the threat of such disturbances, likely to jeopardize achievement of the objectives of Article 39 of the Treaty;
Whereas operation of the market organization would be undermined by the granting of certain assistance; whereas therefore the Treaty provisions permitting consideration of national aids granted by the Member States and the prohibition of those incompatible with the common market should be applied to the banana sector;
Whereas, to facilitate implementation of these provisions, a procedure involving close cooperation between the Member States and the Commission within a Management Committee should be included;
Whereas the common organization of the markets in the banana sector must also take appropriate account of the objectives set out in Articles 39 and 110 of the Treaty;
Whereas the replacement of the various national arrangements in operation when this Regulation comes into force by this common organization of the market threatens to disturb the internal market; whereas the Commission, as of 1 July 1993, should be able to take any transitional measures required to overcome the difficulties of implementing the new arrangements;
Whereas the scope of Council Regulation (EEC) No 1319/85 of 23 May 1985 on the reinforcement of supervision of the application of Community rules on fruit and vegetables (4) should be extended to the banana sector to permit checks that the standards laid down for bananas under that Regulation are being complied with;
Having regard to the social, economic, cultural and environmental importance of banana-growing in the Community regions of the French Overseas Departments, Madeira, the Azores, the Algarve, Crete, Lakonia and the Canary Islands, which are regions characterized by insularity, remoteness and structural backwardness, aggravated in some cases by economic dependence on bananagrowing,
Whereas the operation of this Regulation should be examined after an interim period of application and before the end of the tenth year after its entry into force so that the new arrangements to apply after that date can be considered,
HAS ADOPTED THIS REGULATION:
Article 1
1. A common organization of the market in bananas is hereby established.
2. The organization shall cover the following products:
"" ID="1">ex 0803> ID="2">Bananas, excluding plantains, fresh or dried"> ID="1">ex 0811 90 90> ID="2">Frozen bananas"> ID="1">ex 0812 90 90> ID="2">Bananas provisionally preserved"> ID="1">1106 30 10> ID="2">Powder and flakes of bananas"> ID="1">ex 2006 00 90> ID="2">Bananas preserved in sugar"> ID="1">ex 2007 10> ID="2">Homogenized preparations of bananas"> ID="1">ex 2007 99 39
ex 2007 99 90> ID="2">Jams, jellies, marmalades, purées and pastes of bananas"> ID="1">ex 2008 99 48
ex 2008 99 69
ex 2008 99 99> ID="2">Bananas otherwise prepared or preserved"> ID="1">ex 2008 92 50
ex 2008 92 79
ex 2008 92 91
ex 2008 92 99> ID="2">Mixtures of bananas otherwise prepared or preserved"> ID="1">ex 2009 80> ID="2">Banana juice
">
3. The marketing year shall run from 1 January to 31 December.
TITLE I Common quality and marketing standards
Article 2
1. Quality standards shall be laid down for bananas intended to be supplied fresh to the consumer, excluding plantains, which shall take account of the different varieties produced.
2. Marketing standards may also be laid down for processed products made from bananas.
Article 3
1. Unless the Commission decides otherwise in accordance with the procedure laid down in Article 27 products for which common standards have been laid down may be marketed within the Community only if they comply with those standards.
2. For the purpose of establishing whether products comply with quality standards, checks may be carried out by the bodies designated by the Member States.
Article 4
Quality and marketing standards, the marketing stages at which products must comply with such standards and measures to ensure the uniform implementation of the provisions of Articles 2 and 3, including those concerning checks, shall be laid down in accordance with the procedure provided for in Article 27.
TITLE II Producers' organizations and concentration mechanisms
Article 5
1. For the purposes of this Regulation, 'producers' organization' means any organization of producers established in the Community which:
(a) is set up by the producers themselves in order to:
- concentrate supply and regulate prices at the production stage for one or more of the products refered to in Article 1,
- make available to member producers appropriate technical facilities for the packing and marketing of the products in question;
(b) is able to demonstrate a minimum volume of marketable production and a minimum number of producers;
(c) has rules containing provisions:
- requiring producers to market through the organization the whole of their production of the product or products for which they joined that organization,
- giving the producers control of the organization and its decision-making procedures,
- penalizing any violation by member producers of the rules laid down by the organization,
- imposing levies on its members,
- concerning the admission of new members;
(d) lays down rules concerning production intelligence, production, and in particular rules designed to improve quality and marketing rules;
(e) keeps separate accounts for its activities relating to bananas;
(f) is recognized by the Member State concerned in accordance with paragraph 2.
2. On application, the Member States shall recognize the organizations concerned provided they offer adequate guarantees of the length and effectiveness of their work, with particular reference to the tasks listed in paragraph 1, and provided they satisfy the conditions laid down therein.
Article 6
1. For the five years following the date of recognition, the Member States shall grant recognized producers' organizations assistance to encourage their establishment and assist their administrative operation.
2. Article 14 (1), (3) and (5) and Article 36 (2) of Council Regulation (EEC) No 1035/72 of 18 May 1972 on the common organization of the market in fruit and vegetables (5) shall apply.
Article 7
1. Associations of producers or of producers' organizations set up to carry out one or more measures of common interest may participate in the preparation of the measures provided for in the operational programmes referred to in Article 10. The members of such associations may include processors and traders.
2. The measures of common interest referred to in paragraph 1 may include applied research, training of producers, a strategy on quality and the development of environmentally sound production methods.
Article 8
1. In accordance with the procedure provided for in Article 43 (2) of the Treaty, the Council shall lay down conditions for the recognition and operation of groups encompassing one or more of the economic activities relating to the production, marketing or processing of bananas in order in particular:
- to improve market intelligence and knowledge of the likely development of markets and marketing conditions;
and
- to reduce the dispersion of supply, guide production and promote improvements in the quality of bananas in order better to meet market needs and demand from consumers.
2. The provisions to be adopted shall include, under conditions to be determined, the possibility of extending to non-members the rules adopted by these groups provided such rules are of general interest to the sector as a whole and their extension complies with the Treaty's competition rules.
Article 9
Implementing rules for this Title shall be adopted in accordance with the procedure laid down in Article 27.
TITLE III Assistance
Article 10
1. Acting in the framework of cooperation between the Commission and the national and regional authorities, the competent authorities of the Member States may draw up operational programmes under the Community support frameworks for the regions eligible defining the measures to be undertaken in the banana sector to achieve at least two of the following objectives:
- a quality and commercial strategy for the products of the area in the light of likely developments in costs and markets;
- improved utilization of resources while respecting the environment;
- greater competitiveness.
2. Acting through the cooperation referred to in paragraph 1, the competent authorities shall, as far as possible, associate in the preparation of the measures referred to in paragraph 1 groups, associations and organizations in the banana sector, as referred to in Articles 5, 7 and 8 respectively, and technical and economic research centres.
3. As regards the measures in the operational programmes, organization, decision-making and implementation shall be carried out in compliance with the regulations in force for the management of the structural Funds.
Article 11
In the framework of cooperation between the Commission and the national and regional authorities, the producers' organizations, associations and groups referred to in Articles 5, 7 and 8 may be asked to submit to the competent authorities their views on the implementation of the measures to be proposed.
Article 12
1. Compensation for any loss of income shall be granted to Community producers who are members of a recognized producers' organization which is marketing in the Community bananas complying with the common standards laid down. However, compensation may be paid to an individual producer who, because of his specific situation, and in particular his geographical location, is unable to join a producers' organization.
2. The maximum quantity of bananas produced in the Community and marketed for which compensation may be paid shall be fixed at 854 000 tonnes (net weight) to be broken down as follows for each producer region in the Community:
1. 420 000 tonnes for the Canary Islands,
2. 150 000 tonnes for Guadeloupe,
3. 219 000 tonnes for Martinique,
4. 50 000 tonnes for Madeira, the Azores and the Algarve,
5. 15 000 tonnes for Crete and Lakonia.
Subject to the maximum quantity for the Community, the quantity for each region may be adjusted.
3. Compensation shall be calculated on the basis of the difference between:
- the 'flat-rate reference income' for bananas produced and marketed within the Community, and
- the 'average production income' obtained on the Community market during the year in question for bananas produced and marketed within the Community.
4. The 'flat-rate reference income' shall be:
- the average price of bananas produced in the Community and marketed during a reference period prior to 1 January 1993 to be determined in accordance with the procedure laid down in Article 27;
- less the average cost of transport and delivery fob.
It shall be reviewed by the Commission when the aid is fixed after three years to take account in particular of increased productivity and trends in the various quality categories.
5. The 'average production income' for Community bananas shall be calculated each year from:
- the average price of bananas produced in the Community and marketed during the year in question,
- less the average cost of transport and delivery fob.
6. Before 1 March of each year, the Commission shall determine compensatory aid for the previous year in accordance with the procedure laid down in Article 27.
Supplementary aid shall be granted to one or more producer regions when average income from production is significantly lower than the average Community income.
7. Advances may be paid, against a security, on the basis of compensatory aid granted in the previous year.
8. The Commission shall carry out in 1993 an interim examination of average production income for that year. On the basis of this examination, advances may be paid by the Commission in accordance with the procedure laid down in Article 27.
Article 13
1. A single premium shall be granted to banana producers in the Community who cease to produce bananas.
2. Granting of the premium shall be subject to the written agreement of the recipient:
(a) to undertake or have undertaken in a single operation to take place during a period to be determined in 1993 or 1994;
- the grubbing-up of all banana trees on the holding where the area planted to bananas is less than five hectares;
- the grubbing-up of half the banana trees on the holding where the area planted to bananas is five hectares or more;
(b) not to plant banana trees on the holding concerned for a period of twenty years from the year in which grubbing-up took place.
Areas planted to bananas after this Regulation has come into force and holdings of less than 0,2 hectares shall not be eligible for the premium.
3. The premium shall be ECU 1 000 per hectare. In accordance with the procedure laid down in Article 27, this amount may be adjusted to take account of the specific conditions in certain areas.
4. In accordance with the procedure laid down in Article 27, the Commission may authorize a Member State to exclude from eligibility for the premium for cessation of banana production producers situated in areas where disappearance of this crop would have negative consequences, in particular on the maintenance of microclimatic soil, or environmental conditions or the landscape.
5. This premium shall be compatible with the granting of aid under Title III of Regulation (EEC) No 3763/91 (6), Title II of Regulation (EEC) No 1600/92 (7) and Title III of Regulation (EEC) No 1601/92 (8) and with the granting of structural assistance under Regulations (EEC) No 2052/88 (9) and (EEC) No 4253/88 (10).
Article 14
Detailed rules for the application of this Title shall be adopted in accordance with the procedure laid down in Article 27.
However, detailed rules for the application of Articles 6 and 10 shall be adopted in accordance with the procedure laid down in Article 29 of Regulation (EEC) No 4253/88.
TITLE IV Trade with third countries
Article 15
This title shall apply only to fresh products falling within CN code ex 0803, excluding plaintains.
For the purpose of this Title:
1. 'traditional imports from ACP States' means the quantities of bananas set out in the Annex exported by each ACP State which has traditionally exported bananas to the Community. Such bananas shall be referred to as 'traditional ACP bananas';
2. 'non-traditional imports from ACP States' means the quantities of bananas exported by the ACP States which exceed the quantity defined at 1 above. Such bananas shall be referred to as 'non-traditional ACP bananas';
3. 'imports from non ACP-third countries' means quantities exported by other third countries. Such bananas shall be referred to as 'third country bananas';
4. 'Community bananas' means bananas produced within the Community;
5. 'market' and 'marketing' mean placing on the market, not including making the product available to the final consumer.
Article 16
1. Each year, a forecast supply balance shall be prepared on production and consumption in the Community and of imports and exports.
2. The forecast supply balance shall be prepared on the basis of:
- available figures concerning quantities of bananas marketed in the Community during the previous year, broken down according to their origin,
- forecasts of the production and marketing of Community bananas,
- forecasts of imports of traditional ACP bananas,
- forecasts of consumption based in particular on recent trends in consumption and the evolution in market prices.
3. Where necessary, in particular to take account of the effects of exceptional circumstances affecting production or import conditions, the balance may be adjusted during the marketing year. In such a case, the tariff quota provided for in Article 18 shall be adapted in accordance with the procedure laid down in Article 27.
Article 17
Any importation of bananas into the Community shall be subject to the submission of an import licence issued by the Member States at the request of any party concerned, irrespective of his place of establishment within the Community, without prejudice to the special provisions made for the implementation of Articles 18 and 19.
The import licence shall be valid throughout the Community. Except where derogations are granted under the procedure laid down in Article 27, the issue of such licences shall be subject to the provision of security for compliance with the import commitment, under the conditions laid down by this Regulation during the period of validity of the licence, which shall be totally or partially forfeit if the transaction is not carried out within that period or is only partially carried out.
Article 18
1. A tariff quota of two million tonnes (net weight) shall be opened each year for imports of third-country bananas and non-traditional ACP bananas.
Within the framework of the tariff quota, imports of third-country bananas shall be subject to a levy of ECU 100 per tonne and imports of non-traditional ACP bananas shall be subject to a zero duty.
For the second half of 1993, the volume of the tariff quota shall be set at one million tonnes (net weight).
Where Community demand determined on the basis of the supply balance referred to in Article 16 increases, the volume of the quota shall be increased in consequence, in accordance with the procedure laid down in Article 27. Where necessary, that adjustment shall be carried out prior to the date of 30 November preceding the marketing year concerned.
2. Apart from the quota referred to in paragraph 1,
- imports of non-traditional ACP bananas shall be subject to a levy of ECU 750 per tonne,
- imports of third country bananas shall be subject to a levy of ECU 850 per tonne.
3. The quantities of third-country bananas and nontraditional ACP bananas re-exported out of the Community shall not be charged to the quota referred to in paragraph 1.
Article 19
1. The tariff quota shall be opened form 1 July 1993 for:
(a) 66,5 % to the category of operators who marketed third country and/or non-traditional ACP bananas;
(b) 30 % to the category of operators who marketed Community and/or traditional ACP bananas;
(c) 3,5 % to the category of operators established in the Community who started marketing bananas other than Community and/or traditional ACP bananas from 1992.
The import opportunities pursuant to (a) and (b) shall be available to operators established in the Community who marketed on their own account a minimum quantity of bananas of the above origins, to be determined.
Supplementary criteria to be met by operators shall be laid down in accordance with the procedure provided for in Article 27. The Member States shall draw up the list of importers and the average quantity per operator referred to in paragraph 2.
2. On the basis of separate calculations for each of the categories of operators referred to in patragraph 1 (a) and (b), each operator shall obtain import licences on the basis of the average quantities of bananas that he has sold in the three most recent years for which figures are available. For the category of operators referred to in paragraph 1 (a), the quantities to be taken into consideration shall be the sales of third country and/or non-traditional ACP bananas. In the case of operators referred to in paragraph 1 (b), sales of traditional ACP and/or Community bananas shall be taken into consideration. Third country and/or non traditional ACP bananas imported on the basis of licences issued under paragraph 1 (b) shall not be taken into account to determine the rights to be established pursuant to paragraph 1 (a), so that the initial allocation of licences between the two categories of operators remains identical.
For the second half of 1993, each operator shall be issued licences on the basis of half of the annual average quantity marketed between 1989 and 1991.
3. If the volume of application form new operators exceeds the quantities fixed pursuant to paragraph 1 (c), each application shall be reduced by an equal percentage.
Any quantities available shall be reallocated to the operators referred to paragraph 1 (a) and (b) under conditions laid down by the procedures referred to in Article 27.
4. If the tariff quota is increased, the additional available quantity shall be allocated to importers in the categories referred to in paragraph 1 in accordance with the preceding paragraphs.
Article 20
In accordance with the procedure laid down in Article 27, the Commission shall adopt and adjust the forecast supply balance referred to in Article 16.
In accordance with the same procedure, the Commission shall adopt detailed rules for implementing this Title. Those rules may cover in particular:
- additional measures concerning the issue of licences, their term of validity, the conditions governing transferability and the requisite security mechanism; those rules may also include determination of a reconsideration period,
- frequency of issue of licences,
- the minimum quantity of bananas marketed as referred to in the second subparagraph of Article 19 (1).
TITLE V General provisions
Article 21
1. Except where otherwise provided for in this Regulation,
- the levying of any charge having an effect equivalent to a customs duty,
- the application of any quantitative restrictions or measure having equivalent effect,
shall be forbidden on the import of products referred to in Article 1.
2. The tariff quota laid down in the Protocol on the tariff quota for imports of bananas annexed to the Implementing Convention on the Assocation for the Overseas Countries and Territories provided for in Article 136 of the Treaty shall be discontinued.
Article 22
The general rules for the interpretation of the common customs tariff and the particular rules for its application shall apply to the classification of products coming under this Regulation. The tariff nomenclature resulting from application of this Regulation is contained in the common customs tariff.
Article 23
1. If, by reason of imports or exports, the Community market in one or more of the products listed in Article 1 experiences, or is threatened with, serious disturbances which may endanger the objectives set out in Article 39 of the Treaty, appropriate measures may be applied in trade with third countries until such disturbance or threat of disturbance has ceased.
2. If the situation mentioned in paragraph 1 arises, the Commission shall, at the request of a Member State or on its own initiative, decide upon the necessary measures; the measures shall be communicated to the Member States and shall be immediately applicable. If the Commission receives a request from a Member State, it shall take a decision thereon within three working days following receipt of the request.
3. Measures decided upon by the Commission may be referred to the Council by any Member State within three working days of the day on which they were communicated. The Council shall meet without delay. It may, acting by a qualified majority, amend or repeal the measures in question.
4. Arrangements for the implementation of this Article shall be adopted in accordance with the procedure laid down in Article 27.
Article 24
Save as otherwise provided for in this Regulation, Articles 92 to 94 of the Treaty shall apply to the production of and trade in the products referred to in Article 1.
Article 25
1. The measures laid down in Articles 12 and 13 shall constitute intervention intended to stabilize the agricultural markets wihtin the meaning of Article 3 (1) of Council Regulation (EEC) No 729/70 of 21 April 1970 on the financing of the common agricultural policy (11).
2. The measures laid down in Articles 6 and 10 shall be part-financed by the Guidance Section of the EAGGF.
3. Detailed rules for the application of this Article, and in particular a definition of the conditions to be met before financial aid from the Community can be paid, shall be adopted in accordance with the procedure laid down in Article 27.
Article 26
1. A Management Committee for Bananas (hereinafter called the 'Committee') shall be established, consisting of representatives of the Member States and chaired by a representative of the Commission.
2. Within the Committee the votes of Member States shall be weighted in accordance with Article 148 (2) of the Treaty. The Chairman shall not vote.
Article 27
1. Where reference is made to the procedure laid down in this Article, the Chairman shall refer the matter to the Committee either on his own initiative or at the request of the representative of a Member State.
2. The representative of the Commission shall submit a draft of the measures to be taken. The Committee shall deliver its opinion on such measures within a time limit to be set by the Chairman according to the urgency of the questions under consideration. An opinion shall be adopted by the majority laid down in Article 148 (2) of the Treaty.
3. The Commission shall adopt measures which shall apply immediately. However, if these measures are not in accordance with the opinion of the Committee, they shall forthwith be communicated by the Commission to the Council. In that event, the Commission may defer application of the measures which it has adopted for not more than one month from the date of such communication.
The Council, acting by a qualified majority, may take a different decision within one month.
Article 28
The Committee may consider any other question referred to it by its Chairman either on his own initiative or at the request of a representative of a Member State.
Article 29
The Member States shall provide the Commission with the information it requires to implement this Regulation, in particular concerning;
- measures taken to apply and check common quality standards,
- producers' organizations,
- the content and implementation of the regional framework programmes for bananas,
- provisions for the management of any compensatory aid,
- lists of operators,
- data on production and prices,
- quantities of Community, traditional ACP, non-traditional ACP and third country bananas marketed in their territoriy,
- forecasts of production and consumption in the following year.
Article 30
If specific measures are required after July 1993 to assist the transition from arrangements existing before the entry into force of this Regulation to those laid down by this Regulation, and in particular to overcome difficulties of a sensitive nature, the Commission, acting in accordance with the procedure laid down in Article 27, shall take any transitional measures it judges necessary.
Article 31
In Article 1 (1) or Regulation (EEC) No 1319/85:
1. The second indent is replaced by the following:
'- inspection of the conformity with quality standards, or certain quality requirements
(a) of products listed in Annex II to Regulation (EEC) No 1035/72 which are withdrawn from the market in accordance with Articles 15 and 15a or bought in in accordance with Articles 19 and 19a of that Regulation and
(b) products in the banana sector covered by Council Regulation (EEC) No 404/93 (*);
(1) OJ No L 47, 25. 2. 1993, p. 1.'
2. The fourth indent shall be replaced by:
'verification of the recording of the prices referred to in Articles 17 and 24 of Regulation (EEC) No 1035/72'.
Article 32
No later than the end of the third year after the entry into force of this Regulation and, in any case, when the flat-rate reference income referred to in Article 12 (4) is reviewed, the Commission shall submit a report to the European Parliament and the Council on the operation of this Regulation.
This report shall contain among other things an analysis of the development of Community, of third-country and ACP banana marketing flows since the implementation of these arrangements. The report shall be accompanied where necessary by appropriate proposals.
The Commission shall again report to the European Parliament and the Council by 31 December 2001 on the operation of this Regulation and make appropriate proposals concerning the new arrangements to apply after 31 December 2002.
Article 33
This Regulation shall enter into force on the day following that of its publication in the Official Journal of the European Communities.
It shall apply as from 1 July 1993.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 13 February 1993.
For the Council
The President
B. WESTH
(1) OJ No C 232, 10. 9. 1992, p. 3.
(2) OJ No C 21, 25. 1. 1993.
(3) OJ No C 19, 25. 1. 1993, p. 99.
(4) OJ No L 137, 27. 5. 1985, p. 39.
(5) OJ No L 118, 20. 5. 1972, p. 1. Regulation as last amended by Regulation (EEC) No 1754/92 (OJ No L 180, 1. 7. 1992, p. 23).
(6) OJ No L 356, 24. 12. 1991, p. 1.
(7) OJ No L 173, 27. 6. 1992, p. 1.
(8) OJ No L 173, 27. 6. 1992, p. 13.
(9) OJ No L 185, 15. 7. 1988, p. 9.
(10) OJ No L 374, 31. 12. 1988, p. 1.
(11) OJ No L 94, 28. 4. 1970, p. 13. Regulation as last amended by Regulation (EEC) No 2048/88 (OJ No L 185, 15. 7. 1988, p. 1).
ANNEX
Traditional quantities of bananas from the ACP States Tonnes/net weight Cote d'Ivoire 155 000
Cameroon 155 000
Suriname 38 000
Somalia 60 000
Jamaica 105 000
St Lucia 127 000
St Vincent and the Grenadine 82 000
Dominica 71 000
Belize 40 000
Cape Verde 4 800
Grenada 14 000
Madagascar 5 900
857 700
