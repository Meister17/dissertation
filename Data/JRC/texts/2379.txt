[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 15.07.2005
COM(2005) 322 final
COMMUNICATION FROM THE COMMISSION
TSE Road map
[pic]
TABLE OF CONTENTS
1. Introduction 4
2. Amendments in the short and medium term (2005 - 2009). 5
2.1. Specified Risk Material 5
2.1.1. Current legislation 5
2.1.2. Future policy option 5
2.2. Feed Ban 6
2.2.1. Current legislation 6
2.2.2. Future policy option 6
2.3. Monitoring programmes 7
2.3.1. Bovine animals 7
2.3.2. Small ruminants 9
2.3.3. Cervids 10
2.4. The categorisation of countries according their BSE risk. 10
2.5. Review of culling policy with regard to TSEs in small ruminants 11
2.5.1. Current legislation 11
2.5.2. Future policy option 12
2.6. Cohort culling in bovine animals 12
2.6.1. Current legislation 12
2.6.2. Future policy option 12
2.7. UK restrictions 13
2.7.1. Current legislation 13
2.7.2. Future policy option 13
3. Amendments on the long term (2009 - 2014) 14
3.1. Surveillance 14
3.2. Specified risk material 14
3.3. Certification of herds 14
3.4. Genetic resistance in goat 15
4. alternative scenarios if the positive trend does not continue 15
5. Conclusion 15
6. Annex I 17
7. Annex II: Monitoring data 20
8. Annex III: costs linked to the surveillance programme 22
1. INTRODUCTION
The Commission have discussed on several occasions with the Member States and the European Parliament about the next steps in the BSE policy on different points such as SRM, feed ban and age of testing.
We have come to the stage that amendments of certain measures could be envisaged if the positive trend continues and scientific conditions are in place without endangering the health of the consumer or the policy to eradicate BSE. Indeed different indicators already suggest a favourable trend in the BSE epidemic and a clear improvement of the situation in recent years due to the risk reducing measures in place. Furthermore inspection reports indicate that implementation of BSE requirements in the Member States has improved. The main indicators are presented in the charts 1 -3 in Annex I.
There is a significant overall decrease in the number of cases of the disease across the EU (about 850 BSE cases in 2004 in the EU 25 compared to 2129 BSE cases in 2002 in the EU 15). It clearly demonstrates the downward trend of positive BSE cases detected over the last four years with a 35 % reduction since 2002.The decrease in the number of cases by birth cohort since 1996 is presented in the chart I in Annex I.
The chart 2 in Annex I shows the year of birth of the positive BSE cases detected since 2001 excluding UK. The top of the curve and subsequent downward trend indicate the main contamination through feed in the years 1994 – 1995 followed by a sharp decrease due to the BSE measures taken e.g. partial feed ban in 1994 and total feed ban in 2001. The impact of the 2001 total feed ban can only be assessed in the coming years due to the mean incubation period of BSE of 6 – 8 years.
The chart 3 in Annex I illustrates that the mean age of the positive cases in healthy slaughtered animals in EU 15 increased between 2001 to 2004 from 76.2 months to 95.0 months of age; in 2004, the mean age of the positive cases in healthy slaughtered animals in the new member states was 79.9 months. This indicates contamination during a well defined period in the past well before the stringent BSE measures came into force at Community level.
Based on the improved situation, the Commission has taken this initiative to present a road map on the BSE strategy in the short, medium and long term.
Any relaxation following the scientific assessment should be initiated by an open discussion with all stakeholders and supported by a strong communication strategy. Bearing in mind the political consequences following the first and second BSE crisis any relaxation although scientifically justified may call upon the political courage of the Member states to reduce the current measures in place. In setting our strategy the greatest importance is attached to maintaining the high level of consumer protection built up over the years in the field of prevention, control and eradication of the transmissible spongiform encephalopathies. However, in this process it is also important not to lose sight of other threats to animal and public health which have emerged in recent years, including diseases such as SARS and new variants of avian influenza. The balance of evidence is increasingly pointing towards the need to re-consider the current priorities in the field of food safety and animal health. The encouraging trends in relation to BSE merit a considered review of the opportunities to focus on these new threats.
2. AMENDMENTS IN THE SHORT AND MEDIUM TERM (2005 - 2009).
2.1. Specified Risk Material
Strategic goal:
To ensure and maintain the current level of consumer protection by continuing to assure safe removal of SRM but modify list/age based on new & evolving scientific opinion
2.1.1. Current legislation
Specified risk material is the most important public health protection measure. The initial list of SRM was established based on the scientific knowledge before 1995 and the precautionary principle. Since then, the overall situation has been improved and new scientific data has become available. The restrictions on the use of SRM include a prohibition to use such products for the production of derived products for use in food and feed such as tallow, gelatine, collagen and dicalcium phosphate.
2.1.2. Future policy option
Any amendment of the current list of specified risk material should be based on new evolving scientific knowledge while maintaining the existing high level of consumer protection within the European Union. In addition, data from BSE active monitoring and surveillance should also be used to revise SRM policies.
On 27-28 April 2005 the European Food Safety Authority (EFSA) adopted an opinion on SRM supporting an increase of the current age limit for central nervous tissue from 12 to 21 or 30 months depending the value given to the extremely rare BSE cases detected in young animals. This opinion allows for reflection on an amendment of the current SRM list and in particular on the age limit for the removal of the vertebral column as a first step.
The list of SRM in small ruminants depends on the outcome of the risk assessment currently being carried out by EFSA.
Pending scientific advice on tallow, collagen and gelatine processing standards for those commodities will be further refined with possible relaxation compared to the current requirements.
2.2. Feed Ban
Strategic goal:
A relaxation of certain measures of the current total feed ban when certain conditions are met
2.2.1. Current legislation
A ban on the feeding of mammalian meat and bone meal (MBM) to cattle, sheep and goats was introduced as of July 1994. This partial ban was extended to a total EU wide suspension on the use of processed animal protein in feeds for any animals farmed for the production of food on 1 January 2001 with some exceptions like the use of fish meal for non-ruminants. Any presence of prohibited constituents of animal origin in feed is considered as a breach of the feed ban i.e. the zero-tolerance.
2.2.2. Future policy option
The starting point when revising the current feed ban provisions should be risk-based but at the same time taking into account the control tools in place to evaluate and ensure the proper implementation of this feed ban.
2.2.2.1. Environmental contamination (beet pulp)
Analyses in Germany demonstrate that presence of bones in beet pulp is frequent (up to 10%) and cannot be avoided. The likely origin is bone fragments of wild animals in soil which stick to the beets and end up in sugar beet pulp fed to ruminants. The introduction of a tolerance on the presence of bone fragments in sugar beet pulp and other feedingstuffs due to this environmental contamination is being considered only when a robust risk assessment has demonstrated the absence of cross contamination or the fraudulent incorporation of meat-and-bone meal (MBM).
2.2.2.2. Fish meal
• The use of fishmeal is currently prohibited in feed intended for ruminants and there are also important conditions applying to its use in feed intended for non-ruminants. In order to obtain a more risk based policy, an introduction of a tolerance level with regard to a small presence of fishmeal in ruminant feed due to cross contamination may be proposed. The purpose of such tolerance is to solve the unwanted side effects of the fishmeal ban on the feeding of non-ruminants while respecting the position of the European Parliament, currently against the use of fishmeal in feedingstuffs for ruminants.
• The European Parliament is expected to have a general discussion of fishmeal mid 2005. A decision on a possible relaxation regarding the fishmeal ban will take into consideration the outcome of the discussion in the European Parliament.
2.2.2.3. Lifting feed ban provisions for non-ruminants
Further improvement in differentiating animal proteins specific to certain species may result in an amendment of the provisions with regard to the use in feedingstuffs of animal products, in particular non-ruminant proteins taking into account the prohibition on intra-species recycling in Regulation (EC) No 1774/2002 (e.g. poultry MBM to pigs). Differential tests are already awaited since 2001. The mandatory treatment of mammalian proteins at 133°C, 3 Bars during 20 minutes results in very small fragments of animal proteins which are hard to detect by the current analytical methods.
Awaiting the validation of discriminatory tests and pending the results of the quantitative risk assessment by the EFSA on the risk linked to small amount of MBM, an introduction of a tolerance level with regard to a small presence of meat-and-bone meal in feed may be proposed without jeopardising the current eradication measures.
2.2.2.4. Tallow
Currently there are no specific restrictions with regard to the use of tallow in feed (or food) to prevent transmission of TSEs. The restriction on the use of specified risk material is applicable in addition to the mandatory purification of rendered fat at 0.15% insoluble impurities in Regulation (EC) No 1774/2002. A possible need for future provisions on tallow, in particular for use in milk replacers depends on the result of the quantitative risk assessment (QRA).
2.3. Monitoring programmes
Strategic goal:
To reduce the numbers of tests of bovine animals and at the same time continue to measure the effectiveness of the measures in place with a better targeting of the surveillance activity
2.3.1. Bovine animals
2.3.1.1. Current legislation
Up until the middle of 2000, the majority of BSE cases detected were found by means of traditional passive surveillance, i.e. through the examination and mandatory reporting of animals suspected of showing signs or clinical symptoms of BSE. The detection of BSE in healthy slaughtered cattle in 2000 indicated the need for active monitoring which was introduced in the whole community in the beginning of 2001. The active monitoring programme became fully operational in July 2001 and still includes:
- The testing of all risk animals over 24 months of age (fallen stock, emergency slaughtered animals and animals with clinical signs at ante-mortem inspection)
- The testing of all healthy slaughtered bovine animals above 30 months of age (a total of 10 million cattle per year)
The table 1 in Annex II gives the number of animals tested since 2001 for the different age categories and different categories (healthy slaughter, risk animals, fallen stock).
For countries that have determined that BSE exists within the cattle population, the goal of surveillance is monitoring of the effectiveness of control measures such as feed ban and SRM removal by following the evolution of BSE prevalence. In addition in countries without (recent) BSE cases, the final goal of surveillance is to demonstrate that BSE is below an agreed threshold. It should be noted as well that, although active BSE monitoring is not a public health protection measure, it has contributed to increased consumer confidence and has played a role in the communication strategy of certain member states. In addition the surveillance results have provided the necessary data to evaluate an amendment of the current list of specified risk materials.
2.3.1.2. Future policy option
(a) Epidemiological considerations
Based on the results of the ongoing monitoring programme and forthcoming results of the full testing programmes in the new Member States, a revision of the monitoring programme may be envisaged in 2005 in order to lay down a strategy which moves from the current testing regime to maintenance surveillance based on the CRL model in the long term. Different options for such transition should be analysed with epidemiologists and statisticians taking into account the above goals and cost benefits. Such options are:
• The gradual increase of the age limit starting for healthy slaughtered animals and fallen stock. The increase of the age would depend on the results of the ongoing surveillance programme.
• A decrease of monitoring of cattle born in years from which statistically sufficient information on the BSE prevalence is available but focussing on (recent) years of birth from which only limited information is available.
(b) Cost – benefit considerations
Bearing in mind the goal of the surveillance the costs linked to the surveillance programme should be weighted against the information received through the surveillance. The detailed costs are enclosed as Annex II to this paper. The costs include the co-financing by the Commission and the cost financed by the Member states[1].
The table 2 in Annex II gives the costs linked to the finding per positive case found in the different age groups. Note that the cost linked to the finding of the one positive case in 2002 in healthy slaughter surveillance stream in the age group 30 -35 months is 302 Mio EURO .
2.3.2. Small ruminants
2.3.2.1. Current legislation
Apart from the mandatory examination and reporting of sheep and goats suspected of showing signs or clinical symptoms of a TSE, an active monitoring programme was introduced in the beginning of 2002. Since the confirmation of BSE in a goat in the beginning of 2005, the active monitoring includes:
- The testing of a minimum sample size including up to 10 000 sheep and 10 000 goats per Member State of risk animals over 18 months of age (fallen stock, emergency slaughtered animals and animals with clinical signs at ante-mortem inspection)
- The testing of 10 000 healthy slaughtered ovine animals above 18 months of age in Member States with a large population and a minimum, statistically based but high number of healthy slaughtered goats over 18 months of age. In most Member States all healthy slaughtered goats are tested.
The current extended monitoring in goats will be reviewed in the second half of 2005 and allows a fast and better estimate of BSE in goats in order to assess if the positive BSE case in the goat born in 2000 and slaughtered in France in 2002 was an isolated case.
2.3.2.2. Future policy option
Future monitoring in small ruminants depends on the estimated prevalence of BSE in such animals based on the results of the current extended monitoring in goats and the results from discriminatory testing of new cases and, if possible, analysis of TSE cases in the past. In case the results of the ongoing monitoring indicate that the single BSE cases found in the goat was an isolated case, monitoring can be reduced or only continued for a limited period of time to increase the confidence in the results of the increased testing.
In the worst case scenario where an increase of BSE cases is detected in the small ruminant population, an increase of the surveillance might be envisaged.
2.3.3. Cervids
At this moment no measures are foreseen within EU regarding Chronic Wasting Disease (CWD. There is no evidence to suggest that CWD has occurred in the EU, or at least if it has it must be at a very low prevalence, since otherwise it should be expected to have been detected by now. Nor is there any concrete evidence of transmission of CWD to humans, or of susceptibility of deer to BSE. If experimental or field evidence of either were discovered, this would warrant a change in policy as regards CWD.
Because of the limited surveillance data in EU, a survey will be initiated in the beginning of 2006, the goal of which is to confirm the situation. This will be a targeted survey, in line with the EFSA opinion. However before starting with the survey different risk management measures should be considered during the course of survey preparation mainly related to the action following the detection of a positive CWD case in cervids taking account of the scientific opinion linked to the risk.
2.4. The categorisation of countries according their BSE risk.
Strategic goal:
Simplification of the categorisation criteria and conclude the categorisation of the countries before 1 July 2007.
The objective of a categorisation according to the BSE risk is to define trade rules for every risk category which will give the necessary guarantees to protect animal and public health for the importing countries. The conditions for such trade are already laid down in the current recommendations of the Terrestrial Animal Health Code (“Code”) of the World Organisation for Animal health (OIE).
The current five categorisation criteria in the Code lead to a final BSE status, which did not necessarily reflect the real situation. This is because some of the other criteria are unnecessarily rigid, and not adapted to the risk. Furthermore the present system with five categories is very complicated. It is also not necessary to maintain three different categories for countries with a BSE risk, as the trade conditions are largely similar. The five categories were introduced in the TSE Regulation, pending agreement in the OIE on a revised system. OIE started two years ago a process to review and simplify the categorisation of countries according their BSE risk into three categories.
At the General Session in May 2005 an agreement was reached on the simplified categorisation procedure including the requirements on surveillance within the different categories. The simplified categorisation procedure includes three categories:
• Category 1 : Countries with a negligible BSE risk and a reduced active surveillance programme detecting a design prevalence of 1 per 50 000 - imports allowed without restrictions;
• Category 2 : Countries with a controlled BSE risk and active surveillance programme detecting a design prevalence of 1 per 100 000 – imports allowed when SRM removed;
• Category 3 : Countries with an undetermined BSE risk. The country will be allowed to export only a limited list of tradable products.
Based on this new international standard the current provisions under the TSE Regulation should be amended. Following adoption of the new categorisation criteria the countries should be categorised starting with the major trading partners. If OIE does not succeed in categorising the countries before 1 July 2007 the Community should categorise the countries according to the new international standard.
2.5. Review of culling policy with regard to TSEs in small ruminants
Strategic goal:
Review and relaxation of the eradication measures for small ruminants taking into account the new diagnostic tools available but ensuring the current level of consumer protection
2.5.1. Current legislation
The current provisions require a whole herd culling if TSE is detected in goats and a whole or partial (sensitive genotypes) culling if TSE is detected in sheep including atypical cases. In order to improve the genetic resistance within the sheep population a minimal breeding programme became mandatory for flocks of high genetic merit from 1 April 2005 on. Atypical cases are TSE cases in which BSE is excluded often detected by active surveillance without clinical signs, with no or very limited spreading in a flock, but present in sheep with genotypes considered resistant to BSE (in contrast to classical scrapie). Furthermore the molecular discriminatory testing in force since January 2005, may exclude the presence of BSE within a few weeks in most TSE cases. When BSE is excluded, a public health risk is no longer present and total herd culling might be considered disproportionate on public health grounds.
The table below gives the number of positives sheep and goats within infected herds
Sheep | Goats |
2002 | 1.3% | 0.7% |
2003 | 2.8% | 0.3% |
2004 | 3.5% | 1.2% |
It should be noted that in most flocks no additional cases are detected by culling, in particular when atypical cases had been found. In other flocks, a TSE infection was demonstrated in up to 40% of the animals.
2.5.2. Future policy option
The Commission would like to propose a relaxation of the culling policy for all cases where BSE is excluded (sheep and goats) with an increased testing regime within the infected flocks and the slaughter for human consumption of all animals of all ages in infected herds if negative by rapid testing. Conditions for herd certification should also be considered as an additional way of eradicating TSEs.
2.6. Cohort culling in bovine animals
Strategic goal:
To stop the immediate culling of the cohort
2.6.1. Current legislation
The current legislation on TSE foresees the killing and destruction (culling) of the cohort animals linked to a positive BSE. The cohort animals are animals without any symptoms but which are assumed to be a higher risk of being infected with BSE due to an epidemiological link. This includes in case of female animals the progeny of the positive BSE case (“birth” cohort) or animals which received the same feed as the positive animal in the first year of their life (“feed” cohorts).
2.6.2. Future policy option
Reflection on alternatives to the current destruction of the cohort can be made. A proposed alternative would be to defer the culling and destruction at the end of the productive life, as foreseen within the International OIE Animal Health Code or to allow the slaughtered animals into the food chain following a negative rapid testing result. Although this relaxation would allow breeding and use of milk the decision to derogate from the culling should be the responsibility of the Member states in order to take into account the potential consequences for their export markets. The derogation to defer the culling would be the Member States decision. This relaxation would not endanger the current level of consumer protection. A relaxation would not only reduce the economical impact but also the social consequences following the complete destruction of the cohorts being often one of the main reasons to object to the culling policy.
The table below shows the number of animals culled and tested in 2003 – 2004 and the number of positives detected within the cohort animals.
Year | 2003 | 2004 | Total |
Animals tested | 25.747 | 16.471 | 42.218 |
BSE cases | 10 | 5 | 15 |
In the medium term slaughter for human consumption can be considered subject to a negative TSE test, removal of specified risk materials and a decreasing prevalence. However, as the BSE prevalence reduces, the cost of culling also reduces and from a consumers perception (and trade) point of view the total herd culling for destruction may be the preferred option in particular in Member states where BSE was absent or very rare. This would be a Member States decision.
2.7. UK restrictions
Strategic goal:
To discuss the lifting of the additional restrictions on exports of beef and beef products from the UK if the preset conditions are complied with.
2.7.1. Current legislation
Council Decision 98/256/EC (UK embargo Decision) and subsequent amendments provides that the United Kingdom is to ensure that live bovine animals and products thereof are not dispatched from its territory to other Member States or to third countries except under certain specified conditions (deboned, exclusion of cattle aged over 30 months etc).
2.7.2. Future policy option
In 1996 the United Kingdom introduced the Over-Thirty-Months (OTM) Rule as a public health protection measure which bans meat from most cattle aged over 30 months at slaughter from being destined for human consumption. On 1 December 2004 the United Kingdom informed the Commission on the decision taken to replace the OTM rule with a testing regime as in the other Member States and to exclude permanently cattle born before 1 August 1996 from the food and feed chain.
Regarding the possible lifting of the UK embargo different conditions should be in place to start the discussion. These include a UK incidence fallen below the 200 BSE cases per million adult cattle and a favourable outcome of the audit mission of the Food and Veterinary Office (FVO) in June 2005. On the 10 March 2005 the European Food Safety Authority (EFSA) confirmed the incidence below 200 BSE cases per million adult cattle.
When both conditions are complied with, in particular a favourable outcome of the FVO inspection, a discussion with the Member states on the lifting of the embargo could be initiated likely in the fourth quarter of 2005.
3. AMENDMENTS ON THE LONG TERM (2009 - 2014)
Strategic goal:
To modify measures in line with current technology and new evolving scientific knowledge.
In case the positive trend continues, taking into account the relaxation of the measures in the short and medium term, a further relaxation of the measures can be envisaged. The use of validated live animal tests may in the future allow tracing and culling the positive animals and therefore shortening the period before further relaxation measures can be taken. The different options could include:
3.1. Surveillance
Different scenarios can be envisaged on the long term:
- The gradual decrease in the level of surveillance can be maintained if the positive trend continues focusing on older animals or on birth cohorts from which only limited information is available.
- If only BSE cases are detected in animals above 10 year i.e. born before 1 January 2002, it may be decided to exclude those animals permanently from the feed and food chain (destruction scheme) and provide financial support for the culling of those animals at the end of their productive live. The final surveillance strategy would be reduced to the examination of the clinical suspect animals (passive surveillance) and a maintenance strategy in line with the OIE recommendations.
- The option to test every animal with a live animal test, if available, at a certain age could also be envisaged.
3.2. Specified risk material
If BSE cases are not detected below a certain age or drop below an agreed prevalence the obligation to remove the specified risk material may be withdrawn within this age group. Alternatively, a permanent minimal list of SRM in particular nervous tissues (brain, spinal cord) of cattle of certain age groups may be considered as a precautionary measure against future epidemics or sporadic cases.
3.3. Certification of herds
Following the testing of all cattle with a live animal test a certification of the herd status could be introduced in line with the farm certification as it is the case for tuberculosis or brucellosis.
3.4. Genetic resistance in goat
In case further research indicates genetic resistance of certain genotypes within the goat population the eradication strategy should be reviewed in the light of these new developments.
4. ALTERNATIVE SCENARIOS IF THE POSITIVE TREND DOES NOT CONTINUE
All indicators regarding the prevalence of BSE in bovine animals suggest that a future increase of BSE cases is unlikely. An alternative scenario should be included if the positive trend is not confirmed in certain Member states. Furthermore when considering a medium and long term strategy regarding small ruminants and cervids a possible worst case scenario should also be envisaged. This would include:
• Non-favourable trend of BSE in certain Member states
If the positive trend is not confirmed in certain Member states more stringent measures regarding SRM removal compared to the other Member states could be envisaged. At the last stage a temporary embargo might be envisaged which will allow addressing the situation in the individual Member state without penalising the other Member states where the negative trend is not confirmed.
• BSE in small ruminants
If, following the intensive surveillance in goats, BSE would be confirmed at large scale within the sheep and goat population stringent measures should be taken which would include the full testing of small ruminants and consequently discriminatory testing of the TSE cases and an extensive list of SRM based on the scientific opinions which could lead in the worst scenario to the complete destruction of the carcass.
This will also enforce the need for a breeding programme for resistance within the sheep population.
• Chronic Wasting Disease (CWD) in cervids
If CWD would be confirmed following the survey planned in 2006 a decision on the removal of specified risk material may be permanently in place.
5. CONCLUSION
We have come a long way and the Commission has introduced a comprehensive set of stringent Community measures. In the last 10 years, the Commission has generated 70 primary and implementing acts which set stringent measures at Community level. The key piece of legislation to protect human and animal health from the risk of BSE and other TSE's was adopted on 22 May 2001. This Regulation (EC) No 999/2001 of the European Parliament and of the Council lays down rules for the prevention, control and eradication of certain transmissible spongiform encephalopathies, and is commonly known as the "TSE Regulation". This Regulation was applicable, within a very short time frame, as of 1 July 2001.
Different factors indicate a favourable trend in the BSE epidemic and a clear improvement of the situation over the past years due to the risk reducing measures in place. The goal for the coming years for the TSE regulation is to ensure a relaxation of the measures while assuring the high level of food safety introduced through the TSE controls over the past 10 years. The relaxation of the measures should be risk based and reflect advances in technology as well as evolving scientific knowledge and would also have a positive impact on the competitiveness of the industries and farmers involved within the Community.
6. ANNEX I
Chart 1: BSE cases from 2001 to 2004
[pic]
Chart 2 : BSE cases by birth cohorts
[pic]
Chart 3: Mean age of positive cases in healthy slaughtered animals in EU 15
[pic]
7. ANNEX II: MONITORING DATA
Table 1: number of bovine animals tested from January 2001 to December 2004
Age group | Healthy slaughtered | Risk animals | Fallen Stock |
< 24 months | 3.370.000 | 70.000 | 55.000 |
24-29 | 3.035.000 | 455.000 | 355.000 |
30-35 | 6.715.000 | 655.000 | 515.000 |
36-41 | 3.065.000 | 395.000 | 310.000 |
42-47 months | 2.400.000 | 330.000 | 260.000 |
>= 48 months | 17.235.000 | 3.060.000 | 2.405.000 |
Total | 35.820.000 | 4.965.000 | 3.900.000 |
Table 2: costs (Mio €) per BSE case detected between January 2001 until December 2004
Age group | Healthy slaughter | Risk animals |
<24 months | No cases | No cases |
24-29 months | No cases | 10,2 |
30-35 months | 302 | 29,5 |
36-41 months | 69 | 17,8 |
42-47 months | 11 | 0,9 |
All <48 months | 64 | 4,3 |
All =>48 months | 0,76 | 0,04 |
All Ages | 1,56 | 0,07 |
8. ANNEX III: COSTS LINKED TO THE SURVEILLANCE PROGRAMME
Table 1: estimated age distribution of bovine animals tested from January 2001 to December 2004
Age group | Healthy slaughtered | Risk animals | (Fallen Stock) |
< 24 months | 3.370.000 | 70.000 | 55.000 |
24-29 | 3.035.000 | 455.000 | 355.000 |
30-35 | 6.715.000 | 655.000 | 515.000 |
36-41 | 3.065.000 | 395.000 | 310.000 |
42-47 months | 2.400.000 | 330.000 | 260.000 |
>= 48 months | 17.235.000 | 3.060.000 | 2.405.000 |
Total | 35.820.000 | 4.965.000 | 3.900.000 |
Table 2: Number of BSE cases in young bovine animals detected from January 2001 until December 2004
Healthy slaughter | Risk animals |
24-29 months | No cases | 177.500 |
30-35 months | 6.715.000 | 515.000 |
36-41 months | 1.532.500 | 310.000 |
42-47 months | 240.000 | 16.250 |
All ages | 34.743 | 1.572 |
Table 4: Costs (Mio €) of BSE monitoring between January 2001 until December 2004
The global costs of BSE testing is estimated at 40-50 € per tests with 45€ is taken as a mean cost
Age group | Healthy slaughter | Risk animals |
<24 months | 152 | 3 |
24-29 months | 137 | 20 |
30-35 months | 302 | 29 |
36-41 months | 138 | 18 |
42-47 months | 108 | 15 |
All <48 months | 836 | 86 |
All Ages | 1.612 | 223 |
Table 5: costs (Mio €) per BSE case detected between January 2001 until December 2004
Age group | Healthy slaughter | Risk animals |
<24 months | No cases | No cases |
24-29 months | No cases | 10,2 |
30-35 months | 302 | 29,5 |
36-41 months | 69 | 17,8 |
42-47 months | 11 | 0,9 |
All <48 months | 64 | 4,3 |
All =>48 months | 0,76 | 0,04 |
All Ages | 1,56 | 0,07 |
[1] Average cost of a BSE rapid test is budgeted at 50 EURO with 8 Euro co-financed by the Commission.
