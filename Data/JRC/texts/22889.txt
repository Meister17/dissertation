COUNCIL DIRECTIVE of 3 June 1991 on the frequency band to be designated for the coordinated introduction of digital European cordless telecommunications (DECT) into the Community (91/287/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 100a thereof,
Having regard to the proposal from the Commission (1),
In cooperation with the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas recommendation 84/549/EEC (4) calls for the introduction of services on the basis of a common harmonized approach in the field of telecommunications;
Whereas the Council in its resolution of 30 June 1988 (5) on the development of the common market for telecommunications services and equipment calls for the promotion of Europe-wide services according to market requirements;
Whereas the resources offered by modern telecommunications networks should be utilized to the full for the economic development of the Community;
Whereas Council Directive 89/336/EEC of 3 May 1989 on the approximation of the laws of Member States relating to electromagnetic compatibility (6) is applicable, and particular attention should be taken to avoid harmful electromagnetic interference;
Whereas current cordless telephone systems in use in the Community, and the frequency bands they operate in, vary widely and may not allow the benefits of Europe-wide services or benefit from the economies of scale associated with a truly European market;
Whereas the European Telecommunications Standard Institute (ETSI) is currently developing the European Telecommunications Standard (ETS) or digital European cordless telecommunications (DECT);
Whereas the development of the European Telecommunications Standard (ETS) must take account of the safety of users, and the need for Europe-wide interoperability and enable users provided with a service based on DECT technology in one Member State to gain access to the service in any other Member State, where appropriate;
Whereas the European implementation of DECT will provide an important opportunity to establish truly European digital cordless telephone facilities;
Whereas ETSI has estimated that DECT will require 20 MHz in high density areas;
Whereas the European Conference of Postal and Telecommunications Administrations (CEPT) has recommended the common European frequency band 1880-1900 MHz for DECT, recognizing that, subject to the system, development of DECT additional frequency spectrum may be required;
Whereas this should be taken into account in the preparation for the 1992 World Administrative Radio Conference (WARC);
Whereas after the date of designation of the frequency band for DECT, existing services may continue in the band, providing that they do not interfere with DECT systems that may be established according to commercial demand;
Whereas the implementation of Council recommendation 91/288/EEC of 3 June 1991 on the coordinated introduction of DECT into the Community (7), will ensure the implementation of DECT by 31 December 1992 at the latest;
Whereas Council Directive 91/263/EEC of 29 April 1991 on the approximation of the laws of the Member States concerning telecommunications terminal equipment, including the mutual recognition of their conformity (8) will allow the rapid establishment of common conformity specifications for DECT;
Whereas the establishment of DECT depends on the allocation and availability of a frequency band in order to transmit and receive between fixed-base stations and mobile stations;
Whereas some flexibility will be needed in order to take account of different frequency requirements in different Member States; it will be necessary to ensure that such flexibility does not slow down the implementation of DECT technology according to commercial demand across the Community;
Whereas the progressive availability of the full range of the frequency band set out above will be indispensable for the establishment of DECT on a Europe-wide basis,
HAS ADOPTED THIS DIRECTIVE: Article 1
For the purposes of this Directive, the digital European cordless telecommunications (DECT) system shall mean technology conforming to the European Telecommunications Standard (ETS) for digital cordless telecommunications referred to in recommendation 91/288/EEC, and the telecommunications systems, both public and private, which directly utilize such technology. Article 2
Member States shall, in accordance with CEPT Recommendation T/R 22-02 of the European Conference of Postal and Telecommunications Administration designate the frequency band 1880-1900 MHz for digital European cordless telecommunications (DECT) by 1 January 1992.
In accordance with the CEPT Recommendation, DECT shall have priority over other services in the same band, and be protected in the designated band. Article 3
1. Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive by 31 December 1991. They shall forthwith inform the Commission thereof.
2. When Member States adopt these measures, they shall contain a reference to this Directive or shall be accompanied by such reference on the occasion of their official publication. The methods of making such a reference shall be laid down by the Member States. Article 4
The Commission shall report to the Council on the implementation of this Directive not later than the end of 1995. Article 5
This Directive is addressed to the Member States.
Done at Luxembourg, 3 June 1991. For the Council
The President
A. BODRY (1) OJ No C 187, 27. 7. 1990, p. 5. (2) OJ No C 19, 28. 1. 1991, p. 97 and OJ No C 106, 22. 4. 1991, p. 78. (3) OJ No C 332, 31. 12. 1990, p. 172. (4) OJ No L 298, 16. 11. 1984, p. 49. (5) OJ No C 257, 4. 10. 1988, p. 1. (6) OJ No L 139, 23. 5. 1989, p. 19. (7) See page 47 of this Official Journal. (8) OJ No L 128, 23. 5. 1991, p. 1.
