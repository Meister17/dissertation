Council Decision
of 21 January 2002
concerning the provisional application of the Agreement between the European Community and the Republic of South Africa on trade in wine
(2002/53/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community and in particular Article 133, in conjunction with Article 300(2), first paragraph, first sentence, thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) It is necessary for the Community and South Africa to provide for the provisional application of the Agreement between the European Community and the Republic of South Africa on trade in wine (the "Agreement") as from 28 January 2002 pending the completion of the procedures required by South Africa to bring the Agreement into force.
(2) In order to facilitate the implementation of certain provisions of the Agreement, the Commission should be allowed to make the necessary technical adjustments in accordance with the procedure laid down in Article 75 of Council Regulation (EC) No 1493/1999 of 17 May 1999 on the common organisation of the market in wine(1).
(3) The Agreement in the form of an Exchange of Letters to that effect should therefore be approved,
HAS ADOPTED THIS DECISION:
Article 1
The Agreement in the form of an Exchange of Letters which provides for the provisional application of the Agreement between the European Community and the Republic of South Africa on trade in wine as from 28 January 2002 is hereby approved on behalf of the Community.
The text of the Agreement in the form of an Exchange of Letters and the text of the Agreement on trade in wine are attached to this Decision.
Article 2
The President of the Council is authorised to designate the person(s) authorised to sign the Agreement in the form of an Exchange of Letters and thus express the Community's agreement to be bound by it.
Article 3
For the purposes of applying Articles 7(8) and 18(2) of the Agreement on trade in wine, the Commission is hereby authorised, in accordance with the procedure laid down in Article 75 of Council Regulation (EC) No 1493/1999, to conclude the instruments required to amend the Agreement.
Article 4
The Commission shall represent the Community in the Joint Committees set up under Article 19 of the Agreement on trade in wine.
Article 5
This Decision shall be published in the Official Journal of the European Communities.
Done at Brussels, 21 January 2002.
For the Council
The President
M. Arias Cañete
(1) OJ L 179, 14.7.1999, p. 1. Regulation as last amended by Regulation (EC) No 2585/2001 (OJ L 345, 29.12.2001, p. 10).
