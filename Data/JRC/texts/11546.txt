Commission Decision
of 26 April 2006
on State Aid which France is planning to implement for Euromoteurs (C 1/2005 (ex N 426/2004))
(notified under document number C(2006) 1540)
(Only the French text is authentic)
(Text with EEA relevance)
(2006/747/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community, and in particular the first subparagraph of Article 88(2) thereof,
Having regard to the Agreement on the European Economic Area, and in particular Article 62(1)(a) thereof,
Having called on interested parties to submit their comments pursuant to the provisions cited above [1] and having regard to their comments,
Whereas:
1. PROCEDURE
(1) By letter registered as received on 5 October 2004, France notified the Commission of its intention to contribute an amount of €2 million to the restructuring of Euromoteurs. The case was registered as N426/2004. By letter dated 18 October 2004, the Commission asked for additional information concerning the notification. France replied by letter dated 1 December 2004.
(2) By letter dated 19 January 2005, the Commission informed France that it had decided to initiate the procedure laid down in Article 88(2) of the Treaty in respect of the measure. The decision was published in the Official Journal of the European Communities [2]. The Commission invited interested parties to submit their comments on the measure.
(3) The Commission received comments from the French authorities on 19 May 2005. A meeting between the French authorities and Commission representatives took place on 12 October 2005. The French authorities sent information to the Commission by letter dated 10 November 2005 and by email dated 31 January 2006.
2. DESCRIPTION OF THE MEASURES
2.1. The recipient
(4) Euromoteurs S.A.S. ("Euromoteurs") has its origins in a former subsidiary of Moulinex responsible for producing electric motors intended primarily for the household appliances market.
(5) More precisely, in December 1999, Moulinex set up Compagnie Générale des Moteurs Electriques ("CGME") in order to turn its motors production business into a subsidiary. In September 2001, Moulinex went into receivership, followed by CGME. When the SEB group ("SEB") acquired part of Moulinex in 2001, it did not take over CGME but instead concluded with the latter a four-year supply contract which enabled it to start up in business again.
(6) In January 2002, 12 executives from CGME established the private limited company Compagnie Financière des Moteurs Electriques ("COFIME"), which acts as a consultancy and has a majority stake in Euromoteurs, which was formed in September of the same year.
(7) In September 2002, COFIME and Euromoteurs took over CGME’s assets. The judgment of the commercial court authorising the operation prohibited until September 2004 any economic layoffs or asset disposals.
(8) This prohibition runs counter to the purchasers’ original plan, which was to concentrate CGME’s means of production on one site instead of two and to dismiss more than half the workforce. Moreover, the company is faced with a sharp decline in sales due, in France’s view, to the poor state of the world economy, SEB's shrinking order book and the falling dollar/euro exchange rate.
(9) The trend in Euromoteurs’ accounting figures is as follows:
(EUR millions) | 2002 (4 months’ operations) | 2003 | 2004 |
Turnover | 13 | 25 | 18 |
Net profit | -0 | -1 | -5 |
Own capital | 4 | 3 | -3 |
(10) In 2004, sales to SEB accounted for 93 % of Euromoteurs’ turnover.
2.2. The market
(11) According to the information communicated in December 2004, Euromoteurs’ production of universal motors for household appliances is equivalent to 25 % of European consumption. As part of its diversification strategy, the company envisages producing nearly 10 % of European consumption of seat motors in 2006.
(12) According to the French authorities, Euromoteurs’ main competitors are in Europe and Asia both for universal motors (Ametek, Domel, LG, Johnson Electric, Sun Motors) and for permanent magnet motors (Valeo, Bosch, Meritor, Johnson Electric).
2.3. Restructuring programme
(13) The restructuring programme communicated by the French authorities extends over a period of two years from the date of payment of the notified aid. It consists of three components: industrial, financial and social, for a total amount of €5,95 million:
- the industrial restructuring will cost an estimated €1,10 million and includes:
1) the closure of one of the two production sites;
2) a search for cheaper suppliers;
3) a search for new business partners;
4) diversification into the motor vehicle industry (seat motors).
- the financial restructuring is aimed at clearing the company’s €2,5 million worth of debts;
- the social restructuring is aimed at helping the 246 dismissed employees find a new job and will cost €2,35 million.
(14) The programme is to be financed as follows:
- sale of one of the two production sites: €1,45 million;
- advance on an order from SEB: €1,5 million;
- freeing-up of shareholders’ capital: €1 million;
- restructuring aid.
2.4. Description of the aid
(15) According to the notification of 5 October 2004, the restructuring aid amounts to €2 million.
(16) In the letter from the French authorities dated 1 December 2004, the notified aid takes the form of a €1 million government grant and a €1,25 million debt write-off towards local authorities (€1 million by the Regional Council and €0,25 million by the General Councils of La Manche and Calvados), giving a total of €2,25 million.
(17) Lastly, in their letter of 10 November 2005, the French authorities state that "a minimum of €2,65 million is needed in the way of public financing (…)."
(18) Consequently, uncertainty still surrounds the amount of the notified aid, which may be either €2 million, €2,25 million or €2,65 million.
3. GROUNDS FOR THE INITIATION OF THE PROCEDURE UNDER ARTICLE 88(2) OF THE TREATY
(19) The decision to initiate the procedure laid down in Article 88(2) of the Treaty includes a preliminary assessment of the measure in the light inter alia of the 1999 Community guidelines on state aid for rescuing and restructuring firms in difficulty [3] ("the guidelines").
(20) In its decision, the Commission expressed doubts about whether the restructuring plan was capable of restoring Euromoteurs’ viability, whether undue distortions of competition would be prevented and whether the aid would be restricted to the necessary minimum.
(21) The Commission also pointed out that Euromoteurs had benefited from certain tax exemptions under Article 44 septies of the General Tax Code ("Article 44 septies"). This aid had been declared unlawful and incompatible by Commission Decision 2004/343/EC of 16 December 2003 on the aid scheme implemented by France for the takeover of firms in difficulty [4], and the Commission expressed doubts about the compatibility of the notified aid under the "Deggendorf" case law.
4. OBSERVATIONS FROM THIRD PARTIES AND COMMENTS FROM FRANCE
(22) Following the initiation of the procedure, the Commission has received no observations from third parties. The comments from France may be summarised as follows:
Unlawful incompatible aid received by Euromoteurs under Article 44 septies
(23) By letter dated 19 May 2005, the French authorities confirmed that Euromoteurs had benefited from certain tax exemptions under Article 44 septies.
(24) In a previous letter to the Commission dated 15 March 2005, the French authorities estimated the financial advantage thus received at a maximum [5] of €1,7 million for Euromoteurs and €1,5 for COFIME.
(25) At the time when Euromoteurs was in receipt of the advantages provided for by Article 44 septies, the company did not belong to the category of small and medium-sized enterprises as defined in Commission Recommendation 2003/361/EC of 6 May 2003 concerning the definition of micro, small and medium-sized enterprises [6] and was not located in an area eligible for regional aid. Consequently, even though it is not known at the time of this Decision exactly how much incompatible aid has to be recovered, the Commission considers that Euromoteurs will have to repay a sum close to €1,7 million. To this sum must be added interest in accordance with Article 14(2) of Council Regulation (EC) No 659/1999 of 22 March 1999 laying down detailed rules for the application of Article 93 of the EC Treaty [7].
(26) In their comments dated 19 May 2005, the French authorities stated that the amount of aid needed to restructure Euromoteurs did not take into account the prospect of a reimbursement of the unlawful incompatible aid received by the company and that "such a reimbursement, were it to take place, would have the effect of seriously undermining Euromoteurs’ financial situation".
Restoration of viability upon completion of the restructuring
(27) The French authorities have informed the Commission that, in the middle of 2005, Euromoteurs concluded with Johnson Electric Industrial Manufacturing Ltd ("Johnson") a three-year tapered supply contract (replacing Euromoteurs’ contracts with SEB) worth €12 million in 2005 and €9 million in 2006. This contract also enables Euromoteurs to obtain supplies of raw materials and sub-assemblies from Johnson on advantageous terms.
(28) France points out that, according to the Groupement Interprofessionnel des Fabricants d’Appareils d’Equipement Ménager (Inter-trade organisation of household goods manufacturers), supply contracts are infrequent in the household goods sector and rarely exceed one year’s duration. It concludes from this that this three-year contract signals the contractor’s wish to establish a lasting relationship with its supplier.
(29) The French authorities have also transmitted a forecast profit and loss and cash flow account for Euromoteurs for 2006. In addition to the €9 million in sales to Johnson, Euromoteurs plans on achieving €6 million in turnover by diversifying its customer base. In November 2005, 25 % of this target was covered by orders, and contracts for sales totalling €0,6 million were being negotiated.
Avoidance of undue distortions of competition
(30) The French authorities have stressed that, by the time it had completed its industrial restructuring, Euromoteurs had dismissed 60 % of its workforce, closed one production site out of two and become a medium-sized enterprise within the meaning of Community legislation. They point out that the company faces competition from large groups such as Ametek in Italy, Domel in Slovenia and Goldstar in Korea, which have much bigger sales networks than Euromoteurs.
(31) Lastly, the French authorities suggest that, as Euromoteurs has become a medium-sized enterprise, the Commission’s analysis should be based on the new restructuring aid guidelines [8].
5. ASSESSMENT
5.1. Existence of state aid
(32) The measure notified by France is in effect state aid within the meaning of Article 87(1) of the Treaty. Granted by the State, it will be financed through state resources for the benefit of a specific undertaking, Euromoteurs. What is more, Euromoteurs has competitors in the common market, such as Ametek in Italy and Domel in Slovenia, and its products are traded internationally (Euromoteurs has customers in Germany and Egypt, for example). Consequently, the notified measure affects trade between Member States and distorts, or threatens to distort, competition.
(33) France has therefore fulfilled its obligations under Article 88(3) of the Treaty.
5.2. Compatibility of the aid with the common market
Preliminary remark
(34) As the Court of Justice of the European Communities held in its judgment of 3 October 1991 in Italy v Commission [9], "when the Commission considers the compatibility of a State aid with the common market it must take all the relevant factors into account, including, where relevant, the circumstances already considered in a prior decision and the obligations which that decision may have imposed on a Member State".
(35) In its judgment of 15 May 1997 in Deggendorf [10], the Court went so far as to state that, where earlier unlawful incompatible aid has still not been recovered despite a Commission decision to that effect, the assessment of new aid to the same recipient must take into consideration, first, any cumulative effect of the earlier, unlawful incompatible aid and the new aid and, secondly, the fact that the earlier aid had not been repaid.
(36) In its assessment of the compatibility of the measure notified by France, the Commission will therefore take into account all the relevant factors, including the fact that, according to the information furnished by the French authorities, Euromoteurs has received earlier aid under a scheme declared unlawful and partly incompatible by the Commission and that that aid, which does not form part of the measures either considered not to constitute aid or declared compatible by the Commission, has still not been recovered despite Decision 2004/343/EC.
Derogations from the prohibition in principle of state aid
(37) In the context of the present investigation, the aid falls to be assessed as ad hoc state aid. Article 87(2) and (3) of the Treaty provides for derogations from the general incompatibility rule set out in Article 87(1).
(38) The derogations in Article 87(2) of the Treaty are not applicable here, given that the aid does not have a social character and is not granted to individual consumers, does not serve to make good the damage caused by natural disasters or other exceptional occurrences and is not granted to the economy of certain areas of the Federal Republic of Germany affected by the division of Germany. The same holds true for the derogations provided for in Article 87(3)(b) and (d), which are manifestly not applicable here.
(39) Further derogations are provided for in Article 87(3)(a) and (c) of the EC Treaty. As the primary objective of the aid is not regional but concerns the restructuring of an undertaking in difficulty, only the derogations provided for in Article 87(3)(c) apply. Article 87(3)(c) provides for the authorisation of state aid granted to facilitate the development of certain economic activities, where such aid does not adversely affect trading conditions to an extent contrary to the common interest. The Commission has published specific guidelines for assessing aid for rescuing and restructuring firms in difficulty. Contrary to what the French authorities maintain, the aid in this case, having been notified before 10 October 2004, must be assessed in the light of the criteria set out in the 1999 guidelines [11]. Clearly, the measure has no other horizontal objective in view. Moreover, France mentions no other objective and bases itself on the said guidelines to justify the compatibility of the notified measure.
Assessment of the aid as restructuring aid
Eligibility: firm in difficulty
(40) In order to be eligible for restructuring aid, a firm must qualify as a firm in difficulty. Point 2.1 of the guidelines defines this concept. With a subscribed capital of €4 million, Euromoteurs recorded a loss in 2004 of €5,4 million, which reduced its own capital to minus €2,6 million. The firm may therefore be considered to be a firm in difficulty within the meaning of point 5(a) of the guidelines.
(41) Point 7 of the guidelines stipulates that a newly created firm is not eligible for restructuring aid even if its initial financial position is insecure. Having been set up two years and a month prior to the notification, the company cannot, under the Commission’s practice in applying the guidelines, be considered to be newly created.
Prevention of distortions of competition (points 35 to 39 of the guidelines)
(42) By the time it had completed its industrial restructuring, Euromoteurs had dismissed 60 % of its workforce, closed one production site out of two and become a medium-sized enterprise. As a result, the Commission considers that, viewed in isolation from the unlawful incompatible aid, the restructuring aid is not likely to give rise to any undue distortions of competition.
(43) However, as stressed by the Court’s case law [12], when the Commission assesses whether an aid scheme is compatible with the common market, it has to take into account all relevant factors, including the circumstances already considered for the purposes of an earlier decision and any obligations which that decision might have imposed on a Member State.
(44) In the present case, the Commission would point out that, until such time as Euromoteurs pays back the unlawfully granted aid, that aid and the newly notified aid would have the cumulative effect of giving Euromoteurs an excessive, undue advantage which would adversely affect trading conditions to an extent contrary to the common interest. Until such time as the unlawful incompatible aid is repaid, the undue distortion of competition to which it has given rise will not have been remedied. This distortion would be further increased if Euromoteurs were to receive restructuring aid on top of the unlawful incompatible aid.
(45) In conclusion, in order to prevent the creation of undue distortions of competition, it is essential that Euromoteurs repay the unlawfully granted aid before it can receive the notified restructuring aid.
Restoration of viability (points 32 to 34 of the guidelines)
(46) According to point 3.2.2 of the guidelines, the grant of the aid is conditional on implementation of a restructuring plan which must restore the long-term viability of the company within a reasonable time scale and on the basis of realistic assumptions as to its future operating conditions.
(47) If Euromoteurs’ viability is to be restored, the company will have to meet two challenges: it will have to rationalise its production facilities, and it will have to diversify its customer base so as to end its reliance on orders from SEB (which have been trailing off since 2002 and are now channelled through Johnson).
(48) On the first point, the Commission would observe that the closure of the Carpiquet plant and the dismissal of 246 people have enabled Euromoteurs to reduce its operating costs substantially (France estimates the savings from focusing production on one site at €1,491 million) and to tailor its production facilities to the volume of its sales.
(49) On the second point, the contract with Johnson, which covers the period 2005-07, gives the company time to find its legs by guaranteeing it a significant level of sales until 2007. On the basis of the information in its possession, the Commission is of the opinion that, in the electric motors sector, Euromoteurs’ diversification strategy is starting to bear fruit. On the other hand, in the motor vehicle seat motor market, the second pillar of Euromoteurs’ diversification plan, none of the negotiations have reached an advanced stage.
(50) The Commission has received only forecasts for 2006 from the French authorities. According to these forecasts, Euromoteurs’ turnover will be €15 million, with an operating result of €0,2 million and a net result of €1,7 million. The Commission considers that, inasmuch as they are limited to one year and show an operating margin of 1,3 %, these forecasts do not enable it to conclude that the restructuring plan will succeed in restoring the company’s viability on a lasting basis. In 2006, Euromoteurs still benefits from the contract signed with Johnson. The contract ends, however, in 2007. The results for the 2006 financial year alone are therefore not sufficient to enable the Commission to conclude that the company’s viability will be restored on a lasting basis.
(51) Moreover, as France stated in its comments of 19 May 2005, the notified aid and the accompanying restructuring plan do not take into account the possibility that the unlawful incompatible aid which Euromoteurs received under Article 44 septies might have to be repaid. Its repayment was ordered by the Commission in its Decision 2004/343/EC, and the amount to be recovered from Euromoteurs is estimated at €1,7 million. Such repayment will worsen the company’s financial problems, and the Commission considers that, under the circumstances, the plan cannot be deemed realistic. This assessment is borne out by the fact that, in November 2005, the French authorities informed the Commission that the difficulties inherent in the restructuring of Euromoteurs (in particular its financing needs) had been underestimated at the time of the notification and that the company’s public financing need had to be revised upwards (+132,5 %).
(52) In the light of the above, the Commission concludes that the French authorities have not demonstrated that the notified restructuring plan is based on realistic assumptions and will help restore the company’s viability.
Aid limited to the minimum (points 40 to 41 of the guidelines)
(53) The cost of the restructuring plan communicated to the Commission is estimated by the French authorities at €5,95 million. The plan calls for €2 million of public financing. In their letter dated 10 November 2005, the French authorities informed the Commission that the difficulties inherent in the restructuring of Euromoteurs (in particular its financing needs) had been underestimated at the time of the notification and that the company’s public financing need could be estimated at not less than €2,65 million. No details as to how this new need was calculated (whether in terms of additional restructuring costs or of new cash flow requirements) were communicated to the Commission.
(54) The Commission accordingly considers that, at all events, the French authorities have not demonstrated that aid in excess of €2 million is needed to restore the company’s viability and it cannot therefore conclude that the notified aid is limited to the minimum necessary.
One time, last time
(55) According to the French authorities, no restructuring aid has been paid to Euromoteurs in the past,
HAS ADOPTED THIS DECISION:
Article 1
The state aid which France is planning to implement for Euromoteurs for an amount of €2 million, €2,25 million or €2,65 million is incompatible with the common market.
The aid may accordingly not be implemented.
Article 2
This Decision is addressed to the French Republic.
Done at Brussels, 26 April 2006.
For the Commission
Neelie Kroes
Member of the Commission
[1] OJ C 137, 4.6.2005, p. 16.
[2] See footnote 1.
[3] OJ C 288, 9.10.1999, p. 2.
[4] OJ C 108, 16.4.2004, p. 38.
[5] Advantage calculated without taking account of the deductions allowed by the applicable Community frameworks.
[6] OJ C 124, 20.5.2003, p. 36.
[7] OJ C 83, 27.3.1999, p. 1. Regulation as amended by the 2003 Act of Accession.
[8] OJ C 244, 1.10.2004, p. 2.
[9] Case C-261/89 [1991] ECR I-4437.
[10] Case C-355/95 P Textilwerke Deggendorf GmbH v Commission and Germany [1997] ECR I-2549.
[11] See point 103 of the new guidelines.
[12] See paragraph 34 of this Decision.
--------------------------------------------------
