Interim Agreement
on trade and trade-related matters between the European community and the European Atomic Energy Community, of the one part, and the Republic of Tajikistan, of the other part
The EUROPEAN COMMUNITY and the EUROPEAN ATOMIC ENERGY COMMUNITY, herein after referred to as "THE COMMUNITY",of the one part,and the REPUBLIC OF TAJIKISTAN,of the other part,
HAVE AGREED AS FOLLOWS:
TITLE I
GENERAL PRINCIPLES
(PCA Tajikistan: Title I)
Article 1
(PCA Tajikistan: Article 2)
Respect for democratic principles and fundamental and human rights, as defined in particular in the Universal Declaration of Human Rights, the United Nations Charter, the Helsinki Final Act and the Charter of Paris for a New Europe, as well as the principles of market economy, underpin the internal and external policies of the Parties and constitute an essential element of this Agreement.
Article 2
(PCA Tajikistan: Article 3)
The Parties consider that it is essential for their future prosperity and stability that the newly independent States which have emerged from the dissolution of the Union of Soviet Socialist Republics, hereinafter called "Independent States", should maintain and develop cooperation among themselves in compliance with the principles of the Helsinki Final Act and with international law and in the spirit of good neighbourly relations, and will make every effort to encourage this process.
TITLE II
TRADE IN GOODS
(PCA Tajikistan: Title III)
Article 3
(PCA Tajikistan: Article 7)
1. The Parties shall accord one another most-favoured-nation treatment in all areas in respect of:
- customs duties and charges applied to imports and exports, including the method of collecting such duties and charges,
- provisions relating to customs clearance, transit, warehouses and transhipment,
- taxes and other internal charges of any kind applied directly or indirectly to imported goods,
- methods of payment and the transfer of such payments,
- the rules relating to the sale, purchase, transport, distribution and use of goods on the domestic market.
2. The provisions of paragraph 1 shall not apply to:
(a) advantages granted with the aim of creating a customs union or a free-trade area or pursuant to the creation of such a union or area;
(b) advantages granted to particular countries in accordance with WTO rules and with other international arrangements in favour of developing countries;
(c) advantages accorded to adjacent countries in order to facilitate frontier traffic.
3. The provisions of paragraph 1 shall not apply, during a transitional period expiring five years after the entry into force of the Partnership and Cooperation Agreement, to advantages defined in Annex I granted by the Republic of Tajikistan to other States which have emerged from the dissolution of the USSR.
Article 4
(PCA Tajikistan: Article 8)
1. The Parties agree that the principle of free transit is an essential condition of attaining the objectives of this Agreement.
In this connection each Party shall secure unrestricted transit via or through its territory of goods originating in the customs territory or destined for the customs territory of the other Party.
2. The rules described in Article V(2), (3), (4) and (5) of the GATT 1994 are applicable between the Parties.
3. The rules contained in this Article are without prejudice to any special rules agreed between the Parties relating to specific sectors, in particular transport or products.
Article 5
(PCA Tajikistan: Article 9)
Without prejudice to the rights and obligations stemming from international conventions on the temporary admission of goods which bind the Parties, each Party shall grant the other Party exemption from import charges and duties on goods admitted temporarily, in the instances and according to the procedures stipulated by any other international convention on this matter binding upon it, in conformity with its legislation. Account shall be taken of the conditions under which the obligations stemming from such a convention have been accepted by the Party in question.
Article 6
(PCA Tajikistan: Article 10)
1. Goods originating in the Republic of Tajikistan shall be imported into the Community free of quantitative restrictions or measures having equivalent effect, without prejudice to the provisions of Articles 8, 11 and 12 of this Agreement.
2. Goods originating in the Community shall be imported into Tajikistan free of quantitative restrictions or measures having equivalent effect, without prejudice to the provisions of Articles 8, 11 and 12 of this Agreement.
Article 7
(PCA Tajikistan: Article 11)
Goods shall be traded between the Parties at market-related prices.
Article 8
(PCA Tajikistan: Article 12)
1. Where any product is being imported into the territory of one of the Parties in such increased quantities or under such conditions as to cause or threaten to cause injury to domestic producers of like or directly competing products, the Community or the Republic of Tajikistan, as the case may be, may take appropriate measures in accordance with the following procedures and conditions.
2. Before taking any measures, or in cases to which paragraph 4 applies as soon as possible thereafter, the Community or the Republic of Tajikistan, as the case may be, shall supply the Joint Committee referred to in Article 22 with all relevant information with a view to seeking a solution acceptable to the Parties as provided for in Title IV.
3. If, as a result of the consultations, the Parties do not reach agreement within 30 days of referral to the Joint Committee on actions to remedy the situation, the Party which requested consultations shall be free to restrict imports of the products concerned to the extent and for such time as is necessary to prevent or remedy the injury, or to adopt other appropriate measures.
4. In critical circumstances where delay would cause damage difficult to repair, the Parties may take the measures before the consultations, on condition that consultations are offered immediately after taking such action.
5. In the selection of measures pursuant to this Article, the Parties shall give priority to those which cause least disturbance to the achievement of the aims of this Agreement.
6. Nothing in this Article shall prejudice or affect in any way the taking, by either Party, of anti-dumping or countervailing measures in accordance with Article VI of GATT 1994, the Agreement on implementation of Article VI of the GATT 1994, the Agreement on Subsidies and Countervailing Measures or related internal legislation.
Article 9
(PCA Tajikistan: Article 13)
The Parties undertake to adjust the provisions in this Agreement on trade in goods between them, in the light of circumstances, and in particular of the situation arising from the future accession of the Republic of Tajikistan to the WTO. The Joint Committee may make recommendations on such adjustments to the Parties which could be put into effect, where accepted, by virtue of agreement between the Parties in accordance with their respective procedures.
Article 10
(PCA Tajikistan: Article 14)
This Agreement shall not preclude prohibitions or restrictions on imports, exports or goods in transit justified on grounds of public morality, public policy or public security; the protection of health and life of humans, animals or plants; the protection of natural resources; the protection of national treasures of artistic, historic or archaeological value or the protection of intellectual, industrial and commercial property or rules relating to gold and silver. Such prohibitions or restrictions shall not, however, constitute a means of arbitrary discrimination or a disguised restriction on trade between the Parties.
Article 11
(PCA Tajikistan: Article 15)
Trade in textile products falling under Chapters 50 to 63 of the Combined Nomenclature is governed by a separate bilateral agreement. After expiry of the separate agreement, textile products shall be included in this Agreement.
Article 12
(PCA Tajikistan: Article 16)
Trade in nuclear materials shall be conducted in accordance with the provisions of the Treaty establishing the European Atomic Energy Community. If necessary, trade in nuclear materials shall be subject to the provisions of a specific Agreement to be concluded between the European Atomic Energy Community and the Republic of Tajikistan.
TITLE III
PAYMENTS, COMPETITION, AND OTHER ECONOMIC PROVISIONS
(PCA Tajikistan: Title IV)
Article 13
(PCA Tajikistan: Article 38(1))
The Parties undertake to authorise in freely convertible currency any payments on the current account of the balance of payments between residents of the Community and of the Republic of Tajikistan connected with the movement of goods, services or persons made in accordance with the provisions of this Agreement.
Article 14
(PCA Tajikistan: Article 40(4))
The Parties agree to examine ways to apply their respective competition laws on a concerted basis in such cases where trade between them is affected.
Article 15
(PCA Tajikistan: Article 39(1))
Pursuant to the provisions of this Article and of Annex II, the Republic of Tajikistan shall continue to improve the protection of intellectual, industrial and commercial property rights in order to provide, by the end of the fifth year after the entry into force of this Agreement, a level of protection similar to that existing in the Community, including effective means of enforcing such rights.
Article 16
(PCA Tajikistan: Article 42)Cooperation in the field of trade in goods and services
The Parties will cooperate with a view to ensuring that the Republic of Tajikistan's international trade is conducted in conformity with the rules of the WTO. The Community shall provide the Republic of Tajikistan with technical assistance for this purpose.
Such cooperation shall include specific issues directly relevant to trade facilitation, in particular with a view to assisting the Republic of Tajikistan to harmonise its legislation and regulations with WTO rules and so fulfil as soon as possible the conditions of accession to that Organisation. These include:
- the formulation of policy on trade and trade-related questions, including payments and clearing mechanisms,
- the drafting of relevant legislation.
Article 17
(PCA Tajikistan: Article 45)Public procurement
The Parties shall cooperate to develop conditions for open and competitive award of contracts for goods and services, in particular through calls for tenders.
Article 18
(PCA Tajikistan: Article 46)Cooperation in the field of standards and conformity assessment
1. Cooperation between the Parties shall promote alignment with internationally agreed criteria, principles and guidelines in the field of metrology, standards and conformity assessment, to facilitate progress towards mutual recognition in the field of conformity assessment and to improve the quality of Tajik products.
2. To this end the Parties shall seek to cooperate in technical assistance projects which will:
- promote appropriate cooperation with organisations and institutions specialised in these fields;
- promote the use of Community technical regulations and the application of European standards and conformity-assessment procedures;
- permit the sharing of experience and technical information in the field of quality management.
Article 19
(PCA Tajikistan: Article 50)Agriculture and the agro-industrial sector
The purpose of cooperation in this area shall be the pursuit of agrarian reform and the reform of agricultural structures, the modernisation, privatisation and restructuring of agriculture, stock farming and the agro-industrial and services sectors in the Republic of Tajikistan, and the development of domestic and foreign markets for Tajik products, in conditions that ensure the protection of the environment, taking into account the necessity to improve security of food supply and to develop agri-business and the processing and distribution of agricultural products. The Parties shall also pursue the gradual approximation of Tajik standards to Community technical regulations concerning industrial and agricultural food products, including sanitary and phytosanitary standards.
Article 20
(PCA Tajikistan: Article 63)Customs
1. The aim of cooperation shall be to guarantee compliance with all the provisions scheduled for adoption in connection with trade and fair trade and to achieve the approximation of the Republic of Tajikistan's customs system to that of the Community.
2. Cooperation shall take place particularly through:
- the exchange of information,
- the improvement of working methods,
- the introduction of the Combined Nomenclature and the single administrative document,
- the simplification of controls and formalities in respect of the carriage of goods,
- support for the introduction of modern customs information systems,
- the organisation of seminars and training periods.
Technical assistance shall be provided where necessary.
3. Without prejudice to other cooperation under this Agreement, mutual assistance in customs matters between administrative authorities of the Parties shall take place in accordance with the provisions of the Protocol attached to this Agreement.
Article 21
(PCA Tajikistan: Article 64)Statistical cooperation
Cooperation in this area shall pursue the development of an efficient statistical system to provide the reliable statistics needed to support and monitor the process of socio-economic reform and contribute to the development of private enterprise in the Republic of Tajikistan.
The Parties shall, in particular, cooperate in the following fields:
- the adaptation of the Tajik statistical system to international methods, standards and classification,
- the exchange of statistical information,
- the provision of the macro- and microeconomic statistics necessary to implement and manage economic reforms.
The Community shall provide the Republic of Tajikistan with technical assistance for this purpose.
TITLE IV
INSTITUTIONAL, GENERAL AND FINAL PROVISIONS
(PCA Tajikistan: Title XI)
Article 22
The Joint Committee set up by the Agreement between the European Economic Community and the European Atomic Energy Community and the Union of Soviet Socialist Republics on Trade and Commercial and Economic Cooperation signed on 18 December 1989 shall perform the duties assigned to it by this Agreement until the Cooperation Council provided for in Article 77 of the Partnership and Cooperation Agreement is established.
Article 23
The Joint Committee may, for the purpose of attaining the objectives of the Agreement, make recommendations in the cases provided for therein.
It shall draw up its recommendations by agreement between the Parties.
Article 24
(PCA Tajikistan: Article 81)
When examining any issue arising within the framework of this Agreement in relation to a provision referring to an Article of one of the Agreements constituting the WTO, the Joint Committee shall take into account to the greatest extent possible the interpretation that is generally given to the Article in question by the members of the WTO.
Article 25
(PCA Tajikistan: Article 85)
1. Within the scope of this Agreement, each Party undertakes to ensure that natural and legal persons of the other Party have access free of discrimination in relation to its own nationals to the competent courts and administrative organs of the Parties to defend their individual rights and their property rights, including those concerning intellectual, industrial and commercial property.
2. Within the limits of their respective powers and competences, the Parties:
- shall encourage the adoption of arbitration for the settlement of disputes arising from commercial and cooperation transactions concluded by economic operators of the Community and those of the Republic of Tajikistan,
- agree that where a dispute is submitted to arbitration, each party to the dispute may, except where the rules of the arbitration centre chosen by the parties provide otherwise, choose its own arbitrator, irrespective of his nationality, and that the presiding third arbitrator or the sole arbitrator may be a citizen of a third country,
- shall recommend their economic operators to choose by mutual consent the law applicable to their contracts,
- shall encourage recourse to the arbitration rules elaborated by the United Nations Commission on International Trade Law (Uncitral) and to arbitration by any centre of a State signatory to the Convention on Recognition and Enforcement of Foreign Arbitral Awards
done at New York on 10 June 1958.
Article 26
(PCA Tajikistan: Article 86)
Nothing in this Agreement shall prevent a Party, within the limits of its respective powers and competences, from taking any measures:
(a) which it considers necessary to prevent the disclosure of information contrary to its essential security interests;
(b) which relate to the production of, or trade in arms, munitions or war materials or to research, development or production indispensable for defence purposes, provided that such measures do not impair the conditions of competition in respect of products not intended for specifically military purposes;
(c) which it considers essential to its own security in the event of serious internal disturbances affecting the maintenance of law and order, in time of war or serious international tension constituting threat of war or in order to carry out obligations it has accepted for the purpose of maintaining peace and international security;
(d) which it considers necessary to respect its international obligations and commitments in the control of dual use industrial goods and technology.
Article 27
(PCA Tajikistan: Article 87)
1. In the fields covered by this Agreement and without prejudice to any special provisions contained therein:
- the arrangements applied by the Republic of Tajikistan in respect of the Community shall not give rise to any discrimination between the Member States, their nationals or their companies or firms,
- the arrangements applied by the Community in respect of the Republic of Tajikistan shall not give rise to any discrimination between Tajik nationals, companies or firms.
2. The provisions of paragraph 1 are without prejudice to the right of the Parties to apply the relevant provisions of their fiscal legislation to taxpayers who are not in identical situations as regards their place of residence.
Article 28
(PCA Tajikistan: Article 88)
1. Each of the Parties may refer to the Joint Committee any dispute relating to the application or interpretation of this Agreement.
2. The Joint Committee may settle the dispute by means of a recommendation.
3. If it is not possible to settle the dispute in accordance with paragraph 2 of this Article, either Party may notify the other of the appointment of a conciliator; the other Party must then appoint a second conciliator within two months. For the application of this procedure, the Community and the Member States shall be deemed to be a single Party to the dispute.
The Joint Committee shall appoint a third conciliator.
The conciliators' recommendations shall be taken by majority vote. Such recommendations shall not be binding upon the Parties.
Article 29
(PCA Tajikistan: Article 89)
The Parties agree to consult each other promptly, through appropriate channels and at the request of either Party, on any matter concerning the interpretation or implementation of this Agreement and other relevant aspects of the relations between the Parties.
The provisions of this Article shall in no way affect, and are without prejudice to, Articles 8, 28 and 33.
The Joint Committee may establish rules of procedure for the settlement of disputes.
Article 30
(PCA Tajikistan: Article 90)
The treatment granted to the Republic of Tajikistan under this Agreement shall in no case be more favourable than that granted by the Member States to each other.
Article 31
(PCA Tajikistan: Article 92)
In so far as matters covered by this Agreement are covered by the Energy Charter Treaty and Protocols thereto, such Treaty and Protocols shall upon entry into force apply to such matters but only to the extent that such application is provided for therein.
Article 32
1. This Agreement shall be applicable until the entry into force of the Partnership and Cooperation Agreement initialled on 16 December 2003.
2. Either Party may denounce this Agreement by notifying the other Party. This Agreement shall cease to apply six months after the date of such notification.
Article 33
(PCA Tajikistan: Article 94)
1. The Parties shall take any general or specific measures required to fulfil their obligations under this Agreement. They shall see to it that the objectives set out in this Agreement are attained.
2. If either Party considers that the other Party has failed to fulfil an obligation under this Agreement, it may take appropriate measures. Before so doing, except in cases of special urgency, it shall supply the Joint Committee with all relevant information required for a thorough examination of the situation with a view to seeking a solution acceptable to the Parties.
In the selection of these measures, priority must be given to those which least disturb the functioning of this Agreement. These measures shall be notified immediately to the Joint Committee if the other Party so requests.
Article 34
(PCA Tajikistan: Article 95)
Annexes I and II together with the Protocol on mutual administrative assistance in customs matters shall form an integral part of this Agreement.
Article 35
(PCA Tajikistan: Article 97)
This Agreement shall apply, on the one hand, to the territories in which the Treaties establishing the European Community and the European Atomic Energy Community are applied and under the conditions laid down in those Treaties and, on the other hand, to the territory of the Republic of Tajikistan.
Article 36
(PCA Tajikistan: Article 99)
The original of this Agreement, of which the Czech, Danish, Dutch, English, Estonian, Finnish, French, German, Greek, Hungarian, Italian, Latvian, Lithuanian, Polish, Portuguese, Slovak, Slovene, Spanish, Swedish and Tajik languages are equally authentic, shall be deposited with the Secretary-General of the Council of the European Union.
Article 37
This Agreement will be approved by the Parties in accordance with their own procedures.
This Agreement shall enter into force on the first day of the second month following the date on which the Parties notify that the procedures referred to in the first paragraph have been completed.
Upon its entry into force, and as far as relations between Tajikistan and the Community are concerned, this Agreement shall replace Article 2, Article 3, except for the fourth indent thereof, and Articles 4 to 16 of the Agreement between the European Economic Community and the European Energy Community and the Union of Soviet Socialist Republics on Trade and Economic and Commercial Cooperation signed in Brussels on 18 December 1989.
--------------------------------------------------
