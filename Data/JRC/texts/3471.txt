Removal from the register of Case C-330/04 [1]
(2005/C 296/38)
(Language of the case: Italian)
By order of 22 June 2005, the President of the Court of Justice of the European Communities has ordered the removal from the register of Case C-330/04: Commission of the European Communities
v Italian Republic.
[1] OJ C 262, 23.10.2004.
--------------------------------------------------
