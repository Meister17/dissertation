Commission Regulation (EC) No 2302/2002
of 20 December 2002
amending Regulation (EC) No 2535/2001 laying down detailed rules for applying Council Regulation (EC) No 1255/1999 as regards the import arrangements for milk and milk products and opening tariff quotas
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 3448/93 of 6 December 1993 laying down the trade arrangements applicable to certain goods resulting from the processing of agricultural products(1), as last amended by Regulation (EC) No 2580/2000(2), and in particular Article 6(2) and Article 7(2) and (3) thereof,
Having regard to Council Regulation (EC) No 1255/1999 of 17 May 1999 on the common organisation of the market in milk and milk products(3), as last amended by Commission Regulation (EC) No 509/2002(4), and in particular Article 29(1) thereof,
Having regard to Decision 2002/309/EC, Euratom of the Council and of the Commission of 4 April 2002 on the conclusion of seven Agreements with the Swiss Confederation(5), and in particular Article 5(3) thereof,
Whereas:
(1) Under Annex 2 to the Agreement between the European Community and the Swiss Confederation on trade in agricultural products, signed in Luxembourg on 21 June 1999 and approved by Decision 2002/309/EC, Euratom, quota 09.4155 covers all yoghurt, including flavoured yoghurt. The description of the goods covered by that quota should therefore be amended to reflect this.
(2) Halloumi cheese can be classified in both quota number 2 and quota number 3 in Annex III(C) to Commission Regulation (EC) No 2535/2001(6), as last amended by Regulation (EC) No 1667/2002(7). To avoid confusion, the description in that Annex should be adjusted.
(3) Regulation (EC) No 2535/2001 should therefore be amended accordingly.
(4) The measures provided for in this Regulation are in accordance with the opinion of the joint meeting of the Management Committees on Milk and Milk Products and on horizontal matters relating to trade in processed agricultural products not covered by Annex I,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 2535/2001 is hereby amended as follows:
1. Annex I.F is replaced by the text in Annex I to this Regulation.
2. Annex III.C is replaced by the text in Annex II to this Regulation.
Article 2
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Communities.
Point 1 of Article 1 shall apply from 1 January 2003.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 20 December 2002.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 318, 20.12.1993, p. 18.
(2) OJ L 298, 25.11.2000, p. 5.
(3) OJ L 160, 26.6.1999, p. 48.
(4) OJ L 79, 22.3.2002, p. 15.
(5) OJ L 114, 30.4.2002, p. 1.
(6) OJ L 341, 22.12.2001, p. 29.
(7) OJ L 252, 20.9.2002, p. 8.
ANNEX I
">TABLE>"
ANNEX ΙΙ
"III.C
PREFERENTIAL IMPORT ARRANGEMENTS - OTHER
>TABLE>"
