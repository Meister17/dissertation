Council Decision
of 21 June 2005
concerning the signing, on behalf of the European Community,of the Agreement amending the Partnership Agreement between the members of the African, Caribbean and Pacific Group of States, of the one part, and the European Community and its Member States, of the other part, signed in Cotonou on 23 June 2000
(2005/599/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 310 thereof, in conjunction with the second sentence of the first subparagraph of Article 300(2) thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) The Council, by virtue of its Decision of 27 April 2004, authorised the Commission to open negotiations with the ACP States with a view to amending the Partnership Agreement between the members of the African, Caribbean and Pacific Group of States, of the one part, and the European Community and its Member States, of the other part, signed in Cotonou on 23 June 2000 [1](hereinafter referred to as the "Cotonou Agreement"). The negotiations were concluded in February 2005.
(2) The Agreement amending the Cotonou Agreement should therefore be signed on behalf of the European Community,
HAS DECIDED AS FOLLOWS:
Article 1
The signing of the Agreement amending the Partnership Agreement between the members of the African, Caribbean and Pacific Group of States, of the one part, and the European Community and its Member States, of the other part, signed in Cotonou on 23 June 2000, together with the declarations made by the Community unilaterally or jointly with other Parties that are attached to the Final Act, is hereby approved on behalf of the Community, subject to the Council Decision concerning the conclusion of the said Agreement.
The texts of the Agreement and of the Final Act are attached to this Decision.
Article 2
The President of the Council is hereby authorised to designate the person(s) empowered to sign the Agreement on behalf of the Community subject to its conclusion.
Done at Luxembourg, 21 June 2005.
The Council
The President
F. Boden
[1] OJ L 317, 15.12.2000, p. 3. Agreement as rectified by OJ L 385, 29.12.2004, p. 88.
--------------------------------------------------
