Council Decision 2005/846/CFSP
of 29 November 2005
implementing Common Position 2005/440/CFSP concerning restrictive measures against the Democratic Republic of Congo
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to Council Common Position 2005/440/CFSP of 13 June 2005 [1], and in particular Article 6 thereof, in conjunction with Article 23(2) of the Treaty on European Union,
Whereas:
(1) On 1 November 2005, the Committee established pursuant to United Nations Security Council (UNSC) Resolution 1533 (2004) approved the list of individuals and entities subject to the measures imposed by paragraphs 13 and 15 of UNSC Resolution 1596 (2005) concerning the Democratic Republic of the Congo.
(2) The Annex to Common Position 2005/440/CFSP should be completed accordingly,
HAS DECIDED AS FOLLOWS:
Article 1
The list of persons and entity set out in the Annex to this Decision shall be inserted in the Annex to Common Position 2005/440/CFSP.
Article 2
This Decision shall take effect on the date of its adoption.
Article 3
This Decision shall be published in the Official Journal of the European Union.
Done at Brussels, 29 November 2005.
For the Council
The President
A. Johnson
[1] OJ L 152, 15.6.2005, p. 22.
--------------------------------------------------
ANNEX
List of persons and entity referred to in Article 1
1. Surname, First Name: BWAMBALE, Frank Kakolele
Alias: Frank Kakorere, Frank Kakorere Bwambale
Sex:
Title, Function:
Address (No, street, postal code, town, country):
Date of birth:
Place of birth (town, country):
Passport or ID Number (including country that issued and date and place of issue):
Nationality:
Other information: Former RCD-ML leader, exercising influence over policies and maintaining command and control over the activities of RCD-ML forces, one of the armed groups and militias referred to in paragraph 20 of Res. 1493 (2003), responsible for trafficking of arms, in violation of the arms embargo.
2. Surname, First Name: KAKWAVU BUKANDE, Jérôme
Alias: Jérôme Kakwavu
Sex:
Title, Function:
Address (No, street, postal code, town, country):
Date of birth:
Place of birth (town, country):
Passport or ID Number (including country that issued and date and place of issue):
Nationality: Congolese
Other information: Known as: "Commandant Jérôme". Former President of UCD/FAPC. FAPC’s control of illegal border posts between Uganda and the DRC — a key transit route for arms flows. As President of the FAPC, he exercises influence over policies and maintains command and control over the activities of FAPC forces, which have been involved in arms trafficking and, consequently, in violations of the arms embargo. Given the rank of General in the FARDC in December 2004.
3. Surname, First Name: KATANGA, Germain
Alias:
Sex:
Title, Function:
Address (No, street, postal code, town, country):
Date of birth:
Place of birth (town, country):
Passport or ID Number (including country that issued and date and place of issue):
Nationality: Congolese
Other information: Under house arrest in Kinshasa from March 2005 for FRPI involvement in human rights abuses. FRPI chief. Appointed General in the FARDC in December 2004. Involved in weapons transfers, in violation of the arms embargo.
4. Surname, First Name: LUBANGA, Thomas
Alias:
Sex:
Title, Function:
Address (No, street, postal code, town, country):
Date of birth:
Place of birth (town, country): Ituri
Passport or ID Number (including country that issued and date and place of issue):
Nationality: Congolese
Other information: Arrested in Kinshasa from March 2005 for UPC/L involvement in human rights abuses violations. President of the UPC/L, one of the armed groups and militias referred to in paragraph 20 of Res. 1493 (2003), involved in the trafficking of arms, in violation of the arms embargo.
5. Surname, First Name: MANDRO, Khawa Panga
Alias: Kawa Panga, Kawa Panga Mandro, Kawa Mandro, Yves Andoul Karim
Sex:
Title, Function:
Address (No, street, postal code, town, country):
Date of birth: 20.8.1973
Place of birth (town, country): Bunia
Passport or ID Number (including country that issued and date and place of issue):
Nationality: Congolese
Other information: Known as: "Chief Kahwa", "Kawa". Ex-President of PUSIC, one of the armed groups and militia referred to in paragraph 20 of Res. 1493 (2003) involved in arms trafficking, in violation of the arms embargo. In prison in Bunia since 04/05 for sabotage of the Ituri peace process.
6. Surname, First Name: MPANO, Douglas
Alias:
Sex:
Title, Function:
Address (No, street, postal code, town, country):
Date of birth:
Place of birth (town, country):
Passport or ID Number (including country that issued and date and place of issue):
Nationality: Congolese
Other information: Based in Goma. Manager of the Compagnie Aérienne des Grands Lacs and of Great Lakes Business Company, whose aircraft were used to provide assistance to armed groups and militias referred to in paragraph 20 of Res. 1493 (2003). Also responsible for disguising information on flights and cargo apparently to allow for the violation of the arms embargo.
7. Surname, First Name: MUDACUMURA, Sylvestre
Alias:
Sex:
Title, Function:
Address (No, street, postal code, town, country):
Date of birth:
Place of birth (town, country):
Passport or ID Number (including country that issued and date and place of issue):
Nationality: Rwandan
Other information: Known as: "Radja", "Mupenzi Bernard", "General Major Mupenzi". FDLR Commander on the ground, exercising influence over policies, and maintaining command and control over the activities of FDLR forces, one of the armed groups and militias referred to in paragraph 20 of Res. 1493 (2003), involved in trafficking of arms, in violation of the arms embargo.
8. Surname, First Name: MURWANASHY-AKA, Dr Ignace
Alias: Ignace
Sex:
Title, Function:
Address (No, street, postal code, town, country):
Date of birth:
Place of birth (town, country):
Passport or ID Number (including country that issued and date and place of issue):
Nationality: Rwandan
Other information: Resident in Germany. President of FDLR, exercising influence over policies, and maintaining command and control over the activities of FDLR forces, one of the armed groups and militias referred to in paragraph 20 of Res. 1493 (2003), involved in trafficking of arms, in violation of the arms embargo.
9. Surname, First Name: MUTEBUTSI, Jules
Alias: Jules Mutebusi, Jules Mutebuzi
Sex:
Title, Function:
Address (No, street, postal code, town, country):
Date of birth:
Place of birth (town, country): South Kivu
Passport or ID Number (including country that issued and date and place of issue):
Nationality: Congolese (South Kivu)
Other information: Currently detained in Rwanda. Known as: "Colonel Mutebutsi". Former FARDC Deputy Military Regional Commander of 10th MR in April 2004, dismissed for indiscipline and joined forces with other renegade elements of former RCD-G to take town of Bukavu in May 2004 by force. Implicated in the receipt of weapons outside of FARDC structures and provision of supplies to armed groups and militia mentioned in paragraph 20 of Res. 1493 (2003), in violation of the arms embargo.
10. Surname, First Name: NGUDJOLO, Matthieu
Alias: Cui Ngudjolo
Sex:
Title, Function:
Address (No, street, postal code, town, country):
Date of birth:
Place of birth (town, country):
Passport or ID Number (including country that issued and date and place of issue):
Nationality:
Other information: "Colonel" or "General". FNI Chief of Staff and former Chief of Staff of the FRPI, exercising influence over policies and maintaining command and control the activities of FRPI forces, one of the armed groups and militias referred to in paragraph 20 of Res. 1493 (2003), responsible for trafficking of arms, in violation of the arms embargo. Arrested by MONUC in Bunia in October 2003.
11. Surname, First Name: NJABU, Floribert Ngabu
Alias: Floribert Njabu, Floribert Ndjabu, Floribert Ngabu Ndjabu
Sex:
Title, Function:
Address (No, street, postal code, town, country):
Date of birth:
Place of birth (town, country):
Passport or ID Number (including country that issued and date and place of issue):
Nationality:
Other information: Arrested and placed under house arrest in Kinshasa from March 2005 for FNI involvement in human rights abuses. President of FNI, one of the armed groups and militias referred to in paragraph 20 of Res. 1493 (2003), involved in the trafficking of arms, in violation of the arms embargo.
12. Surname, First Name: NKUNDA, Laurent
Alias: Laurent Nkunda Bwatare, Laurent Nkundabatware, Laurent Nkunda Mahoro Batware
Sex:
Title, Function:
Address (No, street, postal code, town, country):
Date of birth: 6.2.1967
Place of birth (town, country): North Kivu/Rutshuru
Passport or ID Number (including country that issued and date and place of issue):
Nationality: Congolese
Other information: Currently unlocated. Sightings in Rwanda and Goma. Known as: "General Nkunda". Former RCD-G General. Joined forces with other renegade elements of former RCD-G to take Bukavu in May 2004 by force. In receipt of weapons outside of FARDC in violation of the arms embargo.
13. Surname, First Name: NYAKUNI, James
Alias:
Sex:
Title, Function:
Address (No, street, postal code, town, country):
Date of birth:
Place of birth (town, country):
Passport or ID Number (including country that issued and date and place of issue):
Nationality: Ugandan
Other information: Trade partnership with Commandant Jerome, particularly smuggling across the DRC/Uganda border, including suspectted smuggling of weapons and military material in unchecked trucks. Violation of the arms embargo and provision of assistance to armed groups and militia referred to in paragraph 20 of Res. 1493 (2003), including financial support that allows them to operate militarily.
14. Surname, First Name: OZIA MAZIO, Dieudonné
Alias: Ozia Mazio
Sex:
Title, Function:
Address (No, street, postal code, town, country):
Date of birth: 6.6.1949
Place of birth (town, country): Ariwara, DRC
Passport or ID Number (including country that issued and date and place of issue):
Nationality: Congolese
Other information: Known as: "Omari", "Mr Omari". President of FEC in Aru territory. Financial schemes with Commandant Jerome and FAPC and smuggling across the DRC/Uganda border, allowing supplies and cash to be made available to Commandant Jerome and his troops. Violation of the arms embargo, including by providing assistance to armed groups and militia referred to in paragraph 20 of Res. 1493 (2003).
15. Surname, First Name: TAGANDA, Bosco
Alias: Bosco Ntaganda, Bosco Ntagenda
Sex:
Title, Function:
Address (No, street, postal code, town, country):
Date of birth:
Place of birth (town, country):
Passport or ID Number (including country that issued and date and place of issue):
Nationality: Congolese
Other information: Known as: "Terminator", "Major". UPC/L military commander, exercising influence over policies and maintaining command and control over the activities of UPC/L, one of the armed groups and militias referred to in paragraph 20 of Res. 1493 (2003), involved in the trafficking of arms, in violation of the arms embargo. He was appointed General in the FARDC in December 2004 but refused to accept the promotion, therefore remaining outside of the FARDC.
16. Name: TOUS POUR LA PAIX ET LE DEVELOPPMENT (NGO)
Alias: TPD
Address (No, street, postal code, town, country): Goma, North Kivu
Place of registration (town, country):
Date of registration:
Registration number:
Principal place of business:
Other information: Implicated in violation of the arms embargo, by providing assistance to RCD-G, particularly in supplying trucks to transport arms and troops, and also by transporting weapons to be distributed, to parts of the population in Masisi and Rutshuru, North Kivu, in early 2005.
--------------------------------------------------
