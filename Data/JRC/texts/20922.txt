Commission Regulation (EC) No 2042/2002
of 18 November 2002
on periodical sales by tender of beef held by certain intervention agencies
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 1254/1999 of 17 May 1999 on the common organisation of the market in beef and veal(1), as last amended by Commission Regulation (EC) No 2345/2001(2), and in particular Article 28(2) thereof,
Whereas:
(1) The application of intervention measures in respect of beef has resulted in a build-up of stocks in several Member States. In order to prevent storage being prolonged excessively, part of those stocks should be put up for sale by periodical tender.
(2) The sale should be conducted in accordance with Commission Regulation (EEC) No 2173/79 of 4 October 1979 on detailed rules of application for the disposal of beef bought in by intervention agencies(3), as last amended by Regulation (EC) No 2417/95(4), and in particular Titles II and III thereof.
(3) In the light of the frequency and nature of tenders under this Regulation it is necessary to derogate from Article 6 and 7 of Regulation (EEC) No 2173/79 with regard to the information and deadlines to be provided by the notice of invitation to tender.
(4) In order to ensure that the sales by tender are conducted properly and uniformly, measures in addition to those provided for in Article 8(1) of Regulation (EEC) No 2173/79 should be adopted.
(5) Provisions should be made for derogations from Article 8(2)(b) of Regulation (EEC) No 2173/79 in view of the administrative difficulties which the application of that point is creating in the Member States concerned.
(6) In order to ensure a proper functioning of the tender arrangements it is necessary to provide for a higher amount of security than the one fixed in Article 15(1) of Regulation (EEC) No 2173/79.
(7) On the basis of experience gained with regard to the disposal of bone-in intervention beef, it is necessary to reinforce the quality controls of the products before their delivery to the purchasers, in particular to ensure that the products comply with the provisions in Annex III of Commission Regulation (EC) No 562/2000 of 15 March 2000 laying down detailed rules for the application of Council Regulation (EC) No 1254/1999 as regards the buying-in of beef(5), as last amended by Regulation (EC) No 1592/2001(6).
(8) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Beef and Veal,
HAS ADOPTED THIS REGULATION:
Article 1
1. The following approximate quantities of intervention beef shall be put up for sale:
- 3000 tonnes of bone-in hindquarters held by the German intervention agency,
- 3000 tonnes of bone-in hindquarters held by the Italian intervention agency,
- 3000 tonnes of bone-in hindquarters held by the French intervention agency,
- 3000 tonnes of bone-in hindquarters held by the Spanish intervention agency,
- 3000 tonnes of bone-in forequarters held by the German intervention agency,
- 3000 tonnes of bone-in forequarters held by the Italian intervention agency,
- 3000 tonnes of bone-in forequarters held by the Austrian intervention agency,
- 3000 tonnes of bone-in forequarters held by the French intervention agency,
- 3000 tonnes of bone-in forequarters held by the Spanish intervention agency,
- 400 tonnes of bone-in forequarters held by the Danish intervention agency,
- 67 tonnes of bone-in forequarters held by the Dutch intervention agency,
- 3542 tonnes of boneless beef held by the German intervention agency,
- 341 tonnes of boneless beef held by the Spanish intervention agency,
- 4700 tonnes of boneless beef held by the French intervention agency,
- 1097 tonnes of boneless beef held by the Italian intervention agency,
- 144 tonnes of boneless beef held by the Dutch intervention agency.
Detailed information concerning quantities is given in Annex I.
2. Subject to the provisions of this Regulation, the sale shall be conducted in accordance with Regulation (EEC) No 2173/79, and in particular Titles II and III thereof.
Article 2
1. Tenders shall be submitted for the following closing dates:
(a) 25 November 2002;
(b) 9 December 2002;
(c) 13 January 2003;
(d) 27 January 2003,
until the quantities put up for sale are used up.
2. Notwithstanding Articles 6 and 7 of Regulation (EEC) No 2173/79, this Regulation shall serve as a general notice of invitation to tender.
The intervention agencies concerned shall draw up notices of invitation to tender for each sale, setting out in particular:
- the quantities of beef put up for sale, and
- the deadline and place for the submission of tenders.
3. Particulars of the quantities and the places where the products are stored may be obtained by the parties concerned at the addresses set out in the Annex II. The intervention agencies shall, in addition, display the notices referred to in paragraph 2 at their head offices and may also publish them in other ways.
4. The intervention agencies concerned shall sell first meat which has been in storage for the longest time. However, Member States may in exceptional cases and after having obtained authorisation from the Commission derogate from that obligation.
5. Only tenders reaching the intervention agencies concerned by 12 noon on the relevant closing date for each sale by tender shall be considered.
6. Notwithstanding Article 8(1) of Regulation (EEC) No 2173/79, tenders must be submitted to the intervention agency concerned in sealed envelopes bearing a reference to this Regulation and the relevant date. The sealed envelopes must not be opened by the intervention agency before the deadline for submission as referred to in paragraph 5 has expired.
7. Notwithstanding Article 8(2)(b) of Regulation (EEC) No 2173/79, tenders shall not specify the store or stores where the products are held.
8. Notwithstanding Article 15(1) of Regulation (EEC) No 2173/79, the security shall be EUR 12 per 100 kilograms.
Article 3
1. Not later than the day following the closing date for the submission of tenders, the Member States shall send the Commission details of tenders received.
2. Following scrutiny of the tenders, a minimum selling price shall be set or no award shall be made.
Article 4
1. The intervention agency shall send each tenderer the information referred to in Article 11 of Regulation (EEC) No 2173/79 by fax.
2. Notwithstanding Article 18(1) of Regulation (EEC) No 2173/79 the time limit for taking over meat sold pursuant to this Regulation shall be two months from the day of the notification referred to in Article 11 of the same Regulation.
Article 5
1. The Member States shall take all necessary measures to ensure that bone-in intervention products delivered to the purchasers are presented in a state which fully complies with Annex III of Regulation (EC) No 562/2000 and in particular the sixth indent of point 2(a) of that Annex.
2. The costs related to the measures referred to in paragraph 1 shall be borne by the Member States and shall, in particular, not be imposed on the purchaser or any other third party.
3. Member States shall notify the Commission(7) of all cases where a bone-in intervention quarter has been identified as not complying with Annex III as referred to in paragraph 1, specifying the quality and quantity of the quarter as well as the slaughterhouse where it was produced.
Article 6
This Regulation shall enter into force on the day following that of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 18 November 2002.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 160, 26.6.1999, p. 21.
(2) OJ L 315, 1.12.2001, p. 29.
(3) OJ L 251, 5.10.1979, p. 12.
(4) OJ L 248, 14.10.1995, p. 39.
(5) OJ L 68, 16.3.2000, p. 22.
(6) OJ L 210, 3.8.2001, p. 14.
(7) DG Agriculture, DZ: fax (00-32) 2 295 36 13.
ANEXO I/BILAG I/ANHANG I/ΠΑΡΑΡΤΗΜΑ I/ANNEX I/ANNEXE I/ALLEGATO I/BIJLAGE I/ANEXO I/LIITE I/BILAGA I
>TABLE>
ANEXO II/BILAG II/ANHANG II/ΠΑΡΑΡΤΗΜΑ II/ANNEX II/ANNEXE II/ALLEGATO II/BIJLAGE II/ANEXO II/LIITE II/BILAGA II
Direcciones de los organismos de intervención/Interventionsorganernes adresser/Anschriften der Interventionsstellen/Διευθύνσεις των οργανισμών παρεμβάσεως/Addresses of the intervention agencies/Adresses des organismes d'intervention/Indirizzi degli organismi d'intervento/Adressen van de interventiebureaus/Endereços dos organismos de intervenção/Interventioelinten osoitteet/Interventionsorganens adresser
BUNDESREPUBLIK DEUTSCHLAND
Bundesanstalt für Landwirtschaft und Ernährung (BLE) Postfach 180203 D - 60083 Frankfurt am Main Adickesallee 40 D - 60322 Frankfurt am Main Tel. (49-69) 1564-704/772; Telex 411727; Fax (49-69) 1564-790/985
DANMARK
Minister for Fødevarer, Landbrug og Fiskeri Direktoratet for Fødevare Erhver
v Kampmannsgade 3 DK - 1780 København V Tlf. (45) 33 95 80 00; telex 151317 DK; fax (45) 33 95 80 34
ESPAÑA
FEGA (Fondo Español de Garantía Agraria) Beneficencia, 8 E - 28005 Madrid Teléfono: (0034) 913 47 65 00, 913 47 63 10; télex: FEGA 23427 E, FEGA 41818 E; fax: (0034) 915 21 98 32, 915 22 43 87
FRANCE
OFIVAL 80, avenue des Terroirs de France F - 75607 Paris Cedex 12 Téléphone: (33-1) 44 68 50 00; télex: 215330; télécopieur: (33-1) 44 68 52 33
ITALIA
AGEA (Agenzia Erogazioni in Agricoltura) Via Palestro 81 I - 00185 Roma Tel. (00 39) 06 449 49 91; telex 61 30 03; fax (00 39) 06 445 39 40/444 19 58
NEDERLAND
Ministerie van Landbouw, Natuurbeheer en Visserij p/a LASER Roermond Slachthuisstraat 71 Postbus 965 6040 AZ Roermond Tel. (31-475) 35 54 44; fax (31-475) 31 89 39
ÖSTERREICH
AMA-Agramarkt Austria Dresdner Straße 70 A - 1201 Wien Tel. (43-1) 33 15 12 20; Fax (43-1) 33 15 12 97
