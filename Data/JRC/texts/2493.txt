Commission Regulation (EC) No 1901/2005
of 21 November 2005
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 22 November 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 21 November 2005.
For the Commission
J. M. Silva Rodríguez
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 386/2005 (OJ L 62, 9.3.2005, p. 3).
--------------------------------------------------
ANNEX
to Commission Regulation of 21 November 2005 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code [1] | Standard import value |
07020000 | 052 | 56,5 |
204 | 39,5 |
999 | 48,0 |
07070005 | 052 | 122,8 |
204 | 41,3 |
999 | 82,1 |
07099070 | 052 | 108,7 |
204 | 77,1 |
999 | 92,9 |
08052010 | 204 | 64,0 |
388 | 85,5 |
999 | 74,8 |
08052030, 08052050, 08052070, 08052090 | 052 | 51,3 |
624 | 92,5 |
999 | 71,9 |
08055010 | 052 | 80,6 |
388 | 74,2 |
999 | 77,4 |
08081080 | 388 | 73,7 |
400 | 106,6 |
404 | 101,3 |
512 | 132,0 |
720 | 43,1 |
800 | 141,8 |
999 | 99,8 |
08082050 | 052 | 95,1 |
720 | 56,6 |
999 | 75,9 |
[1] Country nomenclature as fixed by Commission Regulation (EC) No 750/2005 (OJ L 126, 19.5.2005, p. 12). Code "999" stands for "of other origin".
--------------------------------------------------
