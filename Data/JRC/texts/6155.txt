Decision of the EEA Joint Committee
No 117/2005
of 30 September 2005
amending Annex VI (Social security) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex VI to the Agreement was amended by Decision of the EEA Joint Committee No 43/2005 of 11 March 2005 [1].
(2) Commission Regulation (EC) No 77/2005 of 13 January 2005 amending Council Regulation (EEC) No 574/72 laying down the procedure for implementing Regulation (EEC) No 1408/71 on the application of social security schemes to employed persons, to self-employed persons and to members of their families moving within the Community [2] is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
Point 2 (Council Regulation (EEC) No 574/72) of Annex VI to the Agreement shall be amended as follows:
1. the following indent shall be added:
- "— 32005 R 0077: Commission Regulation (EC) No 77/2005 of 13 January 2005 (OJ L 16, 20.1.2005, p. 3).";
2. the texts of points 303. (ICELAND — DENMARK), 323. (ICELAND — FINLAND), 324. (ICELAND — SWEDEN) and 327. (ICELAND — NORWAY) in adaptation (g) shall be replaced with the following:
"Article 15 of the Nordic Convention on social security of 18 August 2003: Agreement of the reciprocal waiver of refunds pursuant to Articles 36(3), 63(3) and 70(3) of the Regulation (costs of benefits in kind in respect of sickness and maternity, accidents at work and occupational diseases, and unemployment benefits) and Article 105(2) of the implementing Regulation (costs of administrative checks and medical examinations).";
3. the text of point 314. (ICELAND — LUXEMBOURG) in adaptation (g) shall be replaced with the following:
"Arrangement of 30 November 2001 on the reimbursement of costs in the field of social security."
Article 2
The text of Regulation (EC) No 77/2005 in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 1 October 2005, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [3].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 30 September 2005.
For the EEA Joint Committee
The President
HSH Prinz Nikolaus von Liechtenstein
[1] OJ L 198, 28.7.2005, p. 45.
[2] OJ L 16, 20.1.2005, p. 3.
[3] No constitutional requirements indicated.
--------------------------------------------------
