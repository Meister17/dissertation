Euro exchange rates [1]
12 July 2006
(2006/C 162/01)
| Currency | Exchange rate |
USD | US dollar | 1,2722 |
JPY | Japanese yen | 146,56 |
DKK | Danish krone | 7,4594 |
GBP | Pound sterling | 0,69150 |
SEK | Swedish krona | 9,1625 |
CHF | Swiss franc | 1,5658 |
ISK | Iceland króna | 94,55 |
NOK | Norwegian krone | 7,9475 |
BGN | Bulgarian lev | 1,9558 |
CYP | Cyprus pound | 0,5750 |
CZK | Czech koruna | 28,490 |
EEK | Estonian kroon | 15,6466 |
HUF | Hungarian forint | 277,50 |
LTL | Lithuanian litas | 3,4528 |
LVL | Latvian lats | 0,6961 |
MTL | Maltese lira | 0,4293 |
PLN | Polish zloty | 4,0458 |
RON | Romanian leu | 3,5743 |
SIT | Slovenian tolar | 239,64 |
SKK | Slovak koruna | 38,715 |
TRY | Turkish lira | 1,9854 |
AUD | Australian dollar | 1,6880 |
CAD | Canadian dollar | 1,4452 |
HKD | Hong Kong dollar | 9,8933 |
NZD | New Zealand dollar | 2,0633 |
SGD | Singapore dollar | 2,0151 |
KRW | South Korean won | 1207,00 |
ZAR | South African rand | 9,0749 |
CNY | Chinese yuan renminbi | 10,1669 |
HRK | Croatian kuna | 7,2530 |
IDR | Indonesian rupiah | 11589,74 |
MYR | Malaysian ringgit | 4,650 |
PHP | Philippine peso | 66,371 |
RUB | Russian rouble | 34,2280 |
THB | Thai baht | 48,208 |
[1] Source: reference exchange rate published by the ECB.
--------------------------------------------------
