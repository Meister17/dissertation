REGULATION (EEC) No 234/68 OF THE COUNCIL of 27 February 1968 on the establishment of a common organisation of the market in live trees and other plants, bulbs, roots and the like, cut flowers and ornamental foliage
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Articles 42 and 43 thereof;
Having regard to the proposal from the Commission;
Having regard to the Opinion of the European Parliament 1;
Whereas the operation and development of the common market in agricultural products must be accompanied by the establishment of a common agricultural policy to include in particular a common organisation of agricultural markets which may take various forms depending on the product;
Whereas the production of live trees and other plants, bulbs, roots and the like, cut flowers and ornamental foliage (hereinafter where appropriate called "live plants") is of particular importance to the agricultural economy of certain regions of the Community ; whereas for growers in these regions the proceeds of such production represent a major part of their income ; whereas therefore efforts should be made, through appropriate measures, to promote the rational marketing of such production and to ensure stable market conditions;
Whereas one of the measures to be taken with a view to the establishment of the common organisation of the market is the application of common quality standards to the products in question ; whereas the application of these standards should have the effect of eliminating from the market products of unsatisfactory quality and of promoting commercial relations on the basis of genuine competition, thus contributing to an improvement in the profitability of production;
Whereas the application of these standards makes some form of inspection of quality necessary for the products which are subject to standardisation ; whereas therefore provision should be made to ensure such inspection;
Whereas exports of flowering bulbs to third countries are of considerable economic importance to the Community ; whereas the continuation and development of such exports may be ensured by stabilising prices in this trade ; whereas provision should therefore be made for minimum export prices for the products in question;
Whereas the common organisation of the market in live plants justifies the application of the Common Customs Tariff ; whereas, moreover, the regulations on imports from third countries should be co-ordinated and unified without delay;
Whereas, so as not to leave the Community market without defence against exceptional disturbances which may arise by reason of imports and exports, the Community should be enabled to take all necessary measures without delay;
Whereas the common organisation of the market involes the removal at the internal frontiers of the Community of all obstacles to the free movement of the goods in question;
Whereas the provisions of the Treaty which allow the assessment of aids granted by Member States and the prohibition of those which are incompatible with the common market should be made to apply to live plants;
Whereas, in order to facilitate implementation of the proposed measures, a procedure should be provided for establishing close co-operation between Member States and the Commission within a Management Committee;
Whereas the common organisation of the market in live plants must take appropriate account, at the same time, of the objectives set out in Articles 39 and 110 of the Treaty;
HAS ADOPTED THIS REGULATION: 1OJ No 156, 15.7.1967, p. 27.
Article 1
A common organisation of the market in live trees and other plants, bulbs, roots and the like, cut flowers and ornamental foliage shall be established in respect of the products falling within Chapter 6 of the Common Customs Tariff ; it shall comprise common quality standards and a trading system.
Article 2
In order to encourage action by trade and joint trade organisations, the following Community measures may be taken in respect of the products referred to in Article 1: - measures to improve quality and stimulate demand;
- measures to promote better organisation of production and marketing;
- measures to facilitate the recording of market price trends.
General rules concerning measures shall be adopted in accordance with the procedure laid down in Article 43 (2) of the Treaty.
Article 3
Standards of quality, sizing and packaging, and the scope of these standards, may be determined for the products referred to in Article 1, or for groups of such products ; the standards may relate in particular to quality grading, to wrapping, to presentation and to marking.
When standards have been adopted, the products to which they apply may not be displayed for sale, offered for sale, sold, delivered or otherwise marketed except in accordance with the said standards.
Standards and the general rules for their application shall be adopted by the Council, acting in accordance with the voting procedure laid down in Article 43 (2) of the Treaty on a proposal from the Commission.
Article 4
Adjustments to quality standards to take account of the requirements of production and marketing techniques shall be decided in accordance with the procedure laid down in Article 14.
Article 5
1. Member States shall subject to quality inspection products for which quality standards have been determined. They shall notify other Member States and the Commission, not later than one month after the entry into force of each quality standard, of the name and address of the bodies entrusted with the inspection of each product or group of products for which the standard is laid down.
2. Detailed rules for the application of paragraph 1 shall be adopted as necessary in accordance with the procedure laid down in Article 14, account being taken in particular of the need to ensure co-ordination of the work of the inspection bodies and uniformity of interpretation and application of quality standards.
Article 6
When standards have been determined, all offers made to the public by way of advertisements, catalogues or price lists must include, if the price is quoted, mention of the nature of the product and of its size grading.
Article 7
1. For each of the products falling within heading No 06.01 A of the Common Customs Tariff, one or more minimum prices for exports to third countries may be fixed each year in good time before the marketing season, beginning in 1968, in accordance with the procedure laid down in Article 14.
Exportation of such products shall be permitted only at a price equal to or above the minimum price fixed for the product in question.
2. Detailed rules for the application of paragraph 1 shall be adopted in accordance with the procedure laid down in Article 14.
Article 8
1. The Common Customs Tariff shall be applied from 1 July 1968 to the products referred to in Article 1 ; from that date no other customs duties shall be levied.
2. The necessary provisions for the co-ordination and unification of the import systems applied by each of the Member States with regard to third countries shall be adopted before 1 July 1968 by the Council, acting in accordance with the voting procedure laid down in Article 43 (2) of the Treaty on a proposal from the Commission. These measures shall be put into effect at the latest on 1 January 1969.
Article 9
1. If by reason of imports or exports the Community market in one or more of the products referred to in Article 1 experiences or is threatened with serious disturbances which may endanger the objectives set out in Article 39 of the Treaty, appropriate measures may be applied in trade with third countries until such disturbance or threat of disturbance has ceased.
The Council, acting in accordance with the voting procedure laid down in Article 43 (2) of the Treaty on a proposal from the Commission, shall adopt detailed rules for the application of this paragraph and define the cases in which and the limits within which Member States may take protective measures.
2. If the situation mentioned in paragraph 1 arises, the Commission shall, at the request of a Member State or on its own initative, decide upon the necessary measures ; the measures shall be communicated to the Member States and shall be immediately applicable. If the Commission receives a request from a Member State, it shall take a decision thereon within twenty-four hours following receipt of the request.
3. The measures decided upon by the Commission may be referred to the Council by any Member State within three working days following the day on which they were communicated. The Council shall meet without delay. It may amend or repeal the measures in question in accordance with the voting procedure laid down in Article 43 (2) of the Treaty.
Article 10
1. The following shall be prohibited in the internal trade of the Community: - the levying of any customs duty or charge having equivalent effect;
- any quantitative restriction or measure having equivalent effect;
- recourse to Article 44 o the Treaty.
2. By way of derogation from the provisions of the second and third indents of paragraph 1, the maintenance of quantitative restrictions or measures having equivalent effect and recourse to Article 44 of the Treaty shall continue to be authorised: - for unrooted cuttings and slips of vines (Common Customs Tariff heading No 06.02 A I) and vine slips, grafted or rooted (Common Customs Tariff heading No 06.02 B) until the date fixed for the application in all Member States of the provisions to be adopted by the Council on the marketing of materials for the vegetative propagation of the vine;
- for potted plants and sapling fruit trees and bushes (Common Customs Tariff heading No 06.02 C II) until 31 December 1968.
As regards potted plants and sapling fruit trees and bushes (Common Customs Tariff heading No 06.02 C II) the Council shall adopt such measures as may be required in pursuance of Articles 3, 12 or 18 of this Regulation.
Article 11
Save as otherwise provided in this Regulation, Articles 92 to 94 of the Treaty shall apply to the production of and trade in the products referred to in Article 1.
Article 12
The Council, acting in accordance with the procedure laid down in Article 43 (2) of the Treaty, shall add to this Regulation such further provisions as may be required in the light of experience.
Article 13
1. A Management Committee for Live Plants (hereinafter called the "Committee") shall be established, consisting of representatives of Member States and presided over by a representative of the Commission.
2. Within the Committee the votes of Member States shall be weighted in accordance with Article 148 (2) of the Treaty. The Chairman shall not vote.
Article 14
1. Where the procedure laid down in this Article is to be followed, the Chairman shall refer the matter to the Committee either on his own initiative or at the request of the representative of a Member State.
2. The representative of the Commission shall submit a draft of the measures to be taken. The Committee shall deliver its Opinion on such measures within a time limit to be set by the Chairman according to the urgency of the questions under consideration. An Opinion shall be adopted by a majority twelve votes.
3. The Commission shall adopt measures which shall apply immediately. However, if these measures are not in accordance with the Opinion of the Committee they shall forthwith be communicated by the Commission to the Council. In that event the Commission may defer application of the measures which it has adopted for not more than one month from the date of such communication.
The Council, acting in accordance with the voting procedure laid down in Article 43 (2) of the Treaty, may take a different decision within one month.
Article 15
The Committee may consider any other question referred to it by its Chairman either on his own initiative or at the request of the representative of a Member State.
Article 16
At the end of the transitional period the Council, acting in accordance with the voting procedure laid down in Article 43 (2) of the Treaty on a proposal from the Commission, shall decide in the light of experience whether to retain or amend the provisions of Article 14.
Article 17
This Regulation shall be so applied that appropriate account is taken, at the same time, of the objectives set out in Articles 39 and 110 of the Treaty.
Article 18
This Regulation shall be applied without prejudice to the provisions adopted or to be adopted with a view to approximating the provisions which have been laid down by law, regulation or administrative action in Member States and which are designed to maintain or improve the technical or genetic level of production of certain products covered by Article 1 and intended specifically for reproduction.
Article 19
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
It shall apply from 1 July 1968.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 27 February 1968.
For the Council
The President
E. FAURE
