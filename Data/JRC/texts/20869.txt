Commission Regulation (EC) No 780/2002
of 8 May 2002
amending Regulation (EC) No 3063/93 laying down detailed rules for the application of Council Regulation (EEC) No 2019/93 with regard to the aid scheme for the production of honey of specific quality
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2019/93 of 19 July 1993 introducing specific measures for the smaller Aegean islands concerning certain agricultural products(1), as last amended by Regulation (EC) No 442/2002(2), and in particular Article 12(3) thereof,
Whereas:
(1) Regulation (EEC) No 2019/93 introduces a scheme for the smaller Aegean islands of aid for hives for the production of honey of specific quality. Article 12 of that Regulation, as amended by Regulation (EC) No 442/2002, now refers to "associations of beekeepers", so the terminology used in Commission Regulation (EC) No 3063/93(3) should be amended.
(2) To bring Regulation (EC) No 3063/93 up to date, the derogations for 1993 relating to the dates for aid applications and payment and for notification to the Commission of information on aid paid out, and to the percentage of aid applications checked on the spot should be deleted. The reference to the agricultural conversion rate should also be deleted.
(3) Regulation (EC) No 3063/93 should be amended accordingly.
(4) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Poultrymeat and Eggs,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 3063/93 is amended as follows:
1. Article 1 is replaced by the following: "Article 1
Aid for the production of quality honey specific to the smaller Aegean islands containing a large proportion of thyme honey shall be granted to associations of beekeepers recognised by the competent authorities who undertake annual programmes of initiatives to improve the conditions under which quality honey is marketed and promoted."
2. In Article 2(2), the first sentence is replaced by the following: "The programmes shall be submitted by the associations of beekeepers to the Greek authority for approval."
3. Article 3 is amended as follows:
(a) the second subparagraph of paragraph 1 is deleted;
(b) the first indent of paragraph 2 is replaced by the following: "- the name and address of the association of beekeepers, or the name, forename and address of the beekeeper,".
4. In Article 4, the second paragraph is deleted.
5. Article 5 is amended as follows:
(a) in the first paragraph, the first and second indents are replaced by the following: "- the number of associations of beekeepers and the number of individual beekeepers who have submitted an aid application,
- the number of hives in respect of which aid has been applied for by associations of beekeepers and individual beekeepers respectively, and granted,";
(b) the second paragraph is deleted.
6. In the first subparagraph of Article 6(2), the second sentence is deleted.
7. Article 8 is deleted.
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 8 May 2002.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 184, 27.7.1993, p. 1.
(2) OJ L 68, 12.3.2002, p. 4.
(3) OJ L 274, 6.11.1993, p. 5.
