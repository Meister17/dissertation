Commission Regulation (EC) No 2206/2003
of 17 December 2003
supplementing the Annex to Regulation (EC) No 2400/96 on the entry of certain names in the "Register of protected designations of origin and protected geographical indications" (Thüringer Leberwurst, Thüringer Rotwurst, Thüringer Rostbratwurst)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2081/92 of 14 July 1992 on the protection of geographical indications and designations of origin for agricultural products and foodstuffs(1), as last amended by Regulation (EC) No 806/2003(2), and in particular Article 6(3) and (4) thereof,
Whereas:
(1) In accordance with Article 5 of Regulation (EEC) No 2081/92, Germany has sent to the Commission applications for registration of the names "Thüringer Leberwurst", "Thüringer Rotwurst" and "Thüringer Rostbratwurst" as geographical indications.
(2) In accordance with Article 6(1) of that Regulation, the application has been found to meet all the requirements laid down therein and in particular to contain all the information required in accordance with Article 4 thereof.
(3) One statement of objection, receivable under Article 7 of Regulation (EEC) No 2081/92 was received by the Commission in respect of the names given in the Annex hereto following publication in the Official Journal of the European Communities in accordance with Article 6(2) of that Regulation(3), but afterwards that has been repealed.
(4) The names should therefore be entered in the "Register of protected designations of origin and protected geographical indications" and hence be protected throughout the Community as protected geographical indications.
(5) The Annex to this Regulation supplements the Annex to Commission Regulation (EC) No 2400/96(4), as last amended by Regulation (EC) No 2054/2003(5),
HAS ADOPTED THIS REGULATION:
Article 1
The names in the Annex hereto are added to the Annex to Regulation (EC) No 2400/96 and entered as protected geographical indications (PGI) in the "Register of protected designations of origin and protected geographical indications" provided for in Article 6(3) of Regulation (EEC) No 2081/92.
Article 2
This Regulation shall enter into force on the 20th day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 17 December 2003.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 208, 24.7.1992, p. 1.
(2) OJ L 122, 16.5.2003, p. 1.
(3) OJ C 114, 15.5.2002, p. 10 (Thüringer Leberwurst);
OJ C 114, 15.5.2002, p. 12 (Thüringer Rotwurst);
OJ C 114, 15.5.2002, p. 14 (Thüringer Rostbratwurst).
(4) OJ L 327, 18.12.1996, p. 11.
(5) OJ L 305, 22.11.2003, p. 5.
ANNEX
PRODUCTS LISTED IN ANNEX I TO THE EC TREATY INTENDED FOR HUMAN CONSUMPTION
Meat-based products
GERMANY
Thüringer Leberwurst (PGI)
Thüringer Rotwurst (PGI)
Thüringer Rostbratwurst (PGI)
