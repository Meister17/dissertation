Judgment of the Court (Grand Chamber) of 2 May 2006 — Regione Siciliana
v Commission of the European Communities
(Case C-417/04 P) [1]
Parties
Appellant: Regione Siciliana (represented by: A. Cingolo and G. Aiello, lawyers)
Other party to the proceedings: Commission of the European Communities (represented by: E. de March and L. Flynn, agents)
Re:
Appeal against the order made on 8 July 2004 by the Court of First Instance (Third Chamber) in Case T-341/02 Regione Siciliana v Commission declaring inadmissible an application seeking annulment of Commission Decision D(2002)810439 of 5 September 2002 closing the file relating to the European Regional Development Fund (ERDF) financial assistance for the major project "Messina-Palermo motorway"
Operative part of the judgment
The Court hereby:
1. Dismisses the appeal;
2. Orders the Regione Siciliana to pay the costs.
[1] OJ C 300, 4.12.2004.
--------------------------------------------------
