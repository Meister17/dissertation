COMMISSION REGULATION (EC) No 3059/94 of 15 December 1994 amending Annexes I, II, and III of Council Regulation (EEC) No 2377/90 laying down a Community procedure for the establishment of maximum residue limits of veterinary medicinal products in foodstuffs of animal origin (Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2377/90 of 26 June 1990 laying down a Community procedure for the establishment of maximum residue limits of veterinary medicinal products in foodstuffs of animal origin (1), as last amended by Commission Regulation (EC) No 2703/94 (2) and in particular Articles 7 and 8 thereof,
Whereas, in accordance with Regulation (EEC) No 2377/90, maximum residue limits must be established progressively for all pharmacologically active substances which are used within the Community in veterinary medicinal products intended for administration to foodproducing animals;
Whereas maximum residue limits should be established only after the examination within the Committee for Veterinary Medicinal Products of all the relevant information concerning the safety of residues of the substance concerned for the consumer of foodstuffs of animal origin and the impact of residues on the industrial processing of foodstuffs;
Whereas, in establishing maximum residue limits for residues of veterinary medicinal products in foodstuffs of animal origin, it is necessary to specify the animal species in which residues may be present, the levels which may be present in each of the relevant meat tissues obtained from the treated animal (target tissue) and the nature of the residue which is relevant for the monitoring of residues (marker residue);
Whereas, for the control of residues, as provided for in appropriate Community legislation, maximum residue limits should usually be established for the target tissues of liver or kidney; whereas, however, the liver and kidney are frequently removed from carcasses moving in international trade, and maximum residue limits should therefore also always be established for muscle or fat tissues;
Whereas, in the case of veterinary medicinal products intended for use in laying birds, lactating animals or honey bees, maximum residue limits must also be established for eggs, milk or honey;
Whereas levamisole should be inserted into Annex I to Regulation (EEC) No 2377/90;
Whereas 17v-oestradiol should be inserted into Annex II to Regulation (EEC) No 2377/90;
Whereas pregnant mare serum gonadotrophin should be inserted into Annex II to Regulation (EEC) No 2377/90; whereas by extrapolation of scientific data this classification into Annex II shall apply to all food-producing animals;
Whereas, in order to allow for the completion of scientific studies, spectinomycin should be inserted into Annex III to Regulation (EEC) No 2377/90;
Whereas a period of 60 days should be allowed before the entry into force of this Regulation in order to allow Member States to make any adjustment which may be necessary to the authorizations to place the veterinary medicinal products concerned on the market which have been granted in accordance with Council Directive 81/851/EEC (3), as last amended by Directive 93/40/EEC (4) to take account of the provisions of this Regulation;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Committee for the Adaptation to Technical Progress of the Directives on the Removal of Technical Barriers to Trade in the Veterinary Medicinal Products Sector,
HAS ADOPTED THIS REGULATION:
Article 1
Annexes I, II and III of Regulation (EEC) No 2377/90 are hereby amended as set out in the Annex hereto.
Article 2
This Regulation shall enter into force on the sixtieth day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 15 December 1994.
For the Commission
Martin BANGEMANN
Member of the Commission
(1) OJ No L 224, 18. 8. 1990, p. 1.
(2) OJ No L 287, 8. 11. 1994, p. 19.
(3) OJ No L 317, 6. 11. 1981, p. 1.
(4) OJ No L 214, 24. 8. 1993, p. 31.
ANNEX
A. Annex I is modified as follows:
2. Antiparasitic agents
2.1. Agents acting against endoparasites
2.1.3. Tetra-hydro-imidazoles (imidazolthiazoles)
"" ID="1">'2.1.3.1. Levamisole> ID="2">Levamisole> ID="3">Bovine, ovine, porcine poultry> ID="4">10 mg/kg 100 mg/kg> ID="5">Muscle, kidney, fat Liver'">
B. In Annex II, point '2. Organic compounds' the following heading added:
2. Organic compounds
"" ID="1">'2.10. Pregnant mare serum gonadotrophin> ID="2">All food producing species"> ID="1">2.11. 17v-Oestradiol> ID="2">All food producing mammals> ID="3">For therapeutic and zootechnical uses only'">
C. Annex III is modified as follows:
1. Anti-infectious agents
1.2. Antibiotics
1.2.5. Aminoglycosides
"" ID="1">'1.2.5.1. Spectinomycin> ID="2">Spectinomycin> ID="3">Bovine, porcine, poultry> ID="4">5 000 mg/kg 2 000 mg/kg 300 mg/kg 500 mg/kg> ID="5">Kidney Liver Muscle Fat> ID="6">Provisional MRLs expire on 1 July 1998'"> ID="3">Bovine> ID="4">200 mg/kg> ID="5">Milk">
