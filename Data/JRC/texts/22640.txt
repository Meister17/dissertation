*****
COUNCIL REGULATION (EEC) No 620/82
of 16 March 1982
establishing, in the relations between the Italian institutions and the institutions of the other Member States, special measures for the reimbursement of sickness and maternity insurance benefits in kind
THE COUNCIL OF THE EUROPEAN
COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 51 thereof,
Having regard to the proposal from the Commission, drawn up after consulting the Administrative Commission on Social Security for Migrant Workers,
Whereas Article 36 of Council Regulation (EEC) No 1408/71 of 14 June 1971 on the application of social security schemes to employed persons and their families moving within the Community (1), as last amended by Regulation (EEC) No 2793/81 (2), provides that the benefits in kind provided by the institution of a Member State on behalf of the institution of another shall be fully refunded, either on production of proof of actual expenditure or on the basis of lump-sum payments;
Whereas Articles 93, 94, 95 and 100 of Regulation (EEC) No 574/72 (3), as last amended by Regulation (EEC) No 2793/81, fixing the detailed rules for the application of Regulation (EEC) No 1408/71, lay down the method for determining the amounts to be refunded and for making such refunds;
Whereas the establishment of a national health service in Italy has entailed the abolition of all the sickness and maternity insurance management bodies and in particular that of the 'Istituto nazionale per l'assicurazione contro le malattie' (National Sickness Insurance Institution), hereinafter referred to as the INAM, which was the liaison body and the competent institution within the meaning of Regulations (EEC) No 1408/71 and (EEC) No 574/72;
Whereas this abolition has given rise to major difficulties in carrying out the operations for checking claims submitted by the institutions of the other Member States, on the one hand, and the operations for drawing up the INAM claims vis-à-vis the said institutions, on the other hand, for the purposes of the refunds provided for in Article 36 of Regulation (EEC) No 1408/71;
Whereas it appears advisable to resolve this particular situation by a single Community method applying simultaneously to all the claims by and on the INAM so as to expedite the overall and final settlement of these claims;
Whereas this method should deviate as little as possible from the abovementioned provisions of Regulation (EEC) No 574/72 and recourse should be had to other criteria for the assessment of claims and debts or to special rules only where absolutely necessary,
HAS ADOPTED THIS REGULATION:
Article 1
By way of derogation from the provisions of Title V of Regulation (EEC) No 574/72, the reimbursements provided for in Article 36 of Regulation (EEC) No 1408/71 of sickness and maternity insurance benefits in kind provided up to 31 December 1980 by the institutions of the Member States on behalf of the INAM and by the INAM on behalf of the said institutions, which have not been effected following the abolition of the INAM, shall be determined and effected in accordance with the procedure laid down in this Regulation.
In the relations between Germany and Italy, the claims and debts of the 'Cassa mutua provinciale di malattia di Bolzano' shall not be covered by this Regulation.
Article 2
The amounts to be reimbursed pursuant to Article 93 of Regulation (EEC) No 574/72 shall be determined as follows:
1. the sums which the institutions of the Member States claimed from the INAM on 30 September 1981 in respect of benefits in kind provided up to 31 December 1980 on the basis of documentary evidence shall be aggregated as such, account being taken of the accepted rejects;
2. the sums which the institutions may still claim pursuant to Article 100 of Regulation (EEC) No 574/72 but for which no documentary evidence is available, in particular for benefits provided in 1980, shall be estimated; they shall be added to the sums referred to in point 1 above;
3. this estimate shall for each Member State constitute a ceiling; should the documentary evidence submitted for the INAM to the Italian Ministry of Health up to 30 April 1982 - the date of the postmark being recognized as the date of despatch - correspond to an amount not exceeding this ceiling, this amount shall be reimbursed; however, if the ceiling is exceeded, the amount to be reimbursed shall be equal to this ceiling;
4. the sums claimed by the INAM which were submitted for claims relating to the financial years 1976 and earlier shall be aggregated as such, account being taken of the accepted rejects;
5. the amounts of the INAM claims for the financial years 1977 to 1980 shall be determined by projecting the amounts reimbursed to the INAM for the financial years 1973 to 1976; however, with regard to Germany, the amount adopted for the financial year 1977 shall be that corresponding to the amount based on the documentary evidence which the Italian Ministry has communicated to the German liaison body.
Article 3
The amounts to be reimbursed pursuant to Articles 94 and 95 of Regulation (EEC) No 574/72 shall be determined as follows:
1. the lump sums determined for the benefits provided up to 1979 shall be aggregated as such, account being taken of the accepted rejects;
2. the lump sums for benefits provided in 1980 shall be estimated with regard to the annual average number of families and the annual average number of pensioners covered by Articles 94 and 95 respectively of Regulation (EEC) No 574/72; these numbers shall be obtained by projecting the data relating to 1977, 1978 and 1979;
3. in dealings between Germany and Italy, claims and debts and the deadlines for settlement shall be calculated in accordance with Annex II.
Article 4
1. The amounts of the claims determined in accordance with Articles 1 to 3 shall be aggregated for each Member State, in the currency of that State, as at 31 December 1981.
2. For the purpose of an offsetting operation, the amounts referred to in paragraph 1 shall then be converted into Italian lire to enable the Italian competent authority to know the balance payable by it.
3. The conversion shall be made on the basis of the official exchange rate as at 31 December 1981 determined by the Commission in the framework of the European monetary system.
Article 5
1. Taking account of point 3 of Article 2, the amount to be offset in accordance with Article 4 shall be expressed in the national currency of the creditor country and shall be reimbursed in that currency.
2. For the conversion of currencies for the purpose of making the reimbursements in accordance with paragraph 3, the rate of exchange to be applied shall be that valid on the day of payment.
3. The reimbursements due shall be made by the liaison bodies as follows:
- on 31 March 1982, 15 to 20 % of the amount due,
- on 30 June 1982, 50 % of the balance still due,
- on 31 December 1982, the balance outstanding.
Article 6
1. The application of Articles 1 to 5 shall constitute a settlement of accounts. This settlement of accounts shall also comprise the amounts provided for by the waiver agreements or other agreements concluded or to be concluded.
2. By way of derogation from Article 100 of Regulation (EEC) No 574/72, no request for reimbursement relating to the benefits referred to in this Regulation may be submitted after 30 April 1982. 3. The competent authority of each Member State shall take the necessary measures for the application of this Regulation, in particular for proceeding from the national balance, determined at Community level, to the settlement of the accounts of the institutions concerned.
Article 7
1. The amounts of the claims for each Member State determined pursuant to the provisions of Articles 2 to 5 are listed in Annex I.
2. The procedure to be followed by the German institutions for determining the amounts to be refunded and apportioning the amounts to be paid is laid down in Annex II.
Article 8
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Communities.
It shall apply from 1 March 1982.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 16 March 1982.
For the Council
The President
P. de KEERSMAEKER
(1) OJ No L 149, 5. 7. 1971, p. 2.
(2) OJ No L 275, 29. 9. 1981, p. 1.
(3) OJ No L 74, 27. 3. 1972, p. 1.
ANNEX I
Statement of claims by and on the INAM (Italy) relating to benefits in kind provided up to and including 31 December 1980
Settlement of accounts as at 31 December 1981
Amounts determined pursuant to Articles 2, 3 and 4
1.2.3 // // I. ITALY as debtor // II. ITALY as creditor // // A. BELGIUM as creditor // B. BELGIUM as debtor // // // // Article // Amounts (Bfrs) // Amounts (Lit) // // // // 2, points 1, 4 and 5 // 747 427 908 // 2 381 011 627 // 2, point 2 // 66 190 047 // - // 3, point 1 // 483 815 // 1 011 732 004 // 3, point 2 // 249 407 807 // 12 123 931 379 // 4 (1) (TOTAL) // 1 063 509 577 // 15 516 675 010 1.2,3 // // // // (Lit) // 1.2.3 // // 33 208 831 000 // // 4 (2) // - 15 516 675 010 // 15 516 675 010 // // // // // 17 692 155 990 // - // // // // 5 (1) // 566 589 570 (Bfrs) // - 1,2.3 // // // C. DENMARK as creditor // D. DENMARK as debtor
WAIVER
(Waiver agreement to be signed)
1.2.3 // // E. GERMANY as creditor // F. GERMANY as debtor // // // // Article // Amounts (DM) // Amounts (Lit) // // // // 2, points 1, 4 and 5 // 43 976 412 // 27 831 565 224 // 2, point 2 // 6 023 588 // - // 3, point 1 (1) // ; // ; // 3, point 2 // ; // ; // 4 (1) (TOTAL) // (50 000 000) // (27 831 565 224) 1.2,3 // // // // (Lit) // 1.2.3 // // // (27 831 565 224) // 4 (2) // 26 664 850 000 // - (26 664 850 000) // // // // // - // ( 1 166 715 224) // // // // 5 (1) // ; // ; // // //
(1) Amounts to be determined by the German management bodies by 30 September 1982.
1.2.3 // // // // // // // // G. FRANCE as creditor // H. FRANCE as debtor // // // // Article // Amounts (FF) // Amounts (Lit) // // // // 2, points 1, 4 and 5 // 523 004 819 // 1 299 250 076 // 2, point 2 // 81 593 356 // 2 754 450 000 // 3, point 1 3, point 2 // 50 330 885 // 14 988 420 084 // // // // 4 (1) (TOTAL) // 654 929 060 // 19 042 120 160 // // // - 808 895 630 (1) 1.2,3 // // // // (Lit) // 1.2.3 // // 137 661 503 910 // // 4 (2) // - 18 233 224 530 // 18 233 224 530 // // // // // 119 428 279 380 // - // // // // 5 (1) // 568 183 900 (FF) // - // // //
(1) Previous offsetting operation.
1,2.3 // I. IRELAND as creditor // J. IRELAND as debtor
WAIVER
(Waiver agreement to be signed)
1.2.3 // // K. LUXEMBOURG as creditor // L. LUXEMBOURG as debtor // // // // Article // Amounts (Lfrs) // Amounts (Lit) // // // // 2, points 1, 4 and 5 // 9 057 823 // // 2, point 2 // - // 171 081 660 // 3, point 1 3, point 2 // 7 707 530 // 188 287 740 431 319 035 // // // // 4 (1) (TOTAL) // 16 765 353 // 790 688 435 1.2,3 // // // // (Lit) // 1.2.3 // 4 (2) // 523 509 885 // 790 688 435 // // // - 523 509 885 // // // // // - // 267 178 550 // // // // 5 (1) // - // 267 178 550 // // // 1.2.3 // // // // // // // // M. NETHERLANDS as creditor // N. NETHERLANDS as debtor // // // // Article // Amounts (Fl) // Amounts (Lit) // // // // 2, points 1, 4 and 5 // 5 150 315 // // 2, point 2 // - // 372 447 205 // 3, point 1 // 28 862 // 238 347 410 // 3, point 2 // 104 909 // 261 020 350 // 4 (1) (TOTAL) // 5 284 086 // 871 814 965 1.2,3 // // // // (Lit) // 1.2.3 // 4 (2) // 2 567 257 330 // 871 815 965 // // - 871 815 965 // // // // // // 1 695 441 365 // - // // // // 5 (1) // 3 489 660 (Fl) // - // // // // // O. UNITED KINGDOM as creditor // P. UNITED KINGDOM as debtor // // // // Article // Amounts (£) // Amounts (Lit) // // // // 2, points 1, 4 and 5 // 522 197;66 // 801 967 351 // 2, point 2 // - // - // 3, point 1 // 810 878;64 // 2 222 176 221 // 3, point 2 // 274 467;41 // 804 568 514 // 4 (1) (TOTAL) // 1 607 543;71 // 3 828 712 086 1.2,3 // // // // (Lit) // 1.2.3 // // // 3 828 712 086 // 4 (2) // 3 698 958 077 // - 3 698 958 077 // // // // // - // 129 754 000 // // // // 5 (1) // - // 129 754 000 // // //
SUMMARY OF OFFSETTING OPERATIONS IN LIRE
1.2.3.4 // // I. ITALY as debtor // // II. ITALY as creditor // // // // // Creditor // Amounts (Lit) // Debtor // Amounts (Lit) // // // // // BELGIUM FRANCE NETHERLANDS // 17 692 155 990 119 428 279 380 1 695 441 365 // GERMANY LUXEMBOURG UNITED KINGDOM // (1 166 715 224) 267 178 550 129 754 000 // // // // // Total // 138 815 876 735 // Total // (1 563 647 774) // // // 1,2.3.4 // Total I - Total II // //
ANNEX II
Rules of procedure for applying Article 3, point 3 and Article 7 (2)
1. The following provisions shall apply for determining and refunding payments under Articles 94 and 95 of Regulation (EEC) No 574/72 in dealings between Germany and Italy:
(a) lump-sum payments under Article 94 of Regulation (EEC) No 574/72 up to 1980 shall be determined in accordance with Title V of that Regulation and with the Agreement between Germany and Italy of 5 November 1968;
(b) lump-sum payments under Article 95 of Regulation (EEC) No 574/72 shall be determined with respect to Italian claims up to 1979 and German claims up to 1980 in accordance with Title V of that Regulation;
(c) lump-sum payments owing to Italy under Article 95 of Regulation (EEC) No 574/72 for 1980 shall be established by projection of the lump-sum settlement between the German and Italian institutions for the period 1977, 1978 and 1979, account being taken of the pensioners' register of the Allgemeine Ortskrankenkasse Bonn (Bonn Local General Sickness Fund) for 1977 to 1980;
(d) claims shall be calculated by the German liaison body by 30 September 1982 and settled by 31 December 1982.
2. The amount laid down in Annex I to this Regulation as the debt of the German institutions under Article 93 of Regulation (EEC) No 574/72 for the period 1977 to 1980 shall be apportioned by the Bundesverband der Ortskrankenkassen (National Federation of Local Sickness Funds), in its capacity as liaison body, between the various types of German sickness insurance institutions in the proportions resulting from the settlements made between 1973 and 1976. Within the various types of fund, apportionment shall be effected according to the number of members in 1980 exclusive of pensioners;
3. The amount established as the debt of the German institutions under Article 95 of Regulation (EEC) No 574/72 for 1980 shall be settled by the Allgemeine Ortskrankenkasse Bonn (Bonn Local General Sickness Fund) on behalf of all German sickness insurance institutions with the exception of the Bundesknappschaft (Federal Insurance Fund for Miners) and the agricultural sickness funds. The sums payable by the Bundesknappschaft and the rural sickness funds shall be determined by the Bundesverband der Ortskrankenkassen (National Federation of Local Sickness Funds), in its capacity as liaison body, by projection from the figures for the period 1977, 1978 and 1979.
3 828 712 086
1.2,3 // // //
( LIT ) //
1.2.3 // //
3 828 712 086
4 ( 2 )
3 698 958 077
- 3 698 958 077 // // // //
-
129 754 000 // // //
5 ( 1 )
-
129 754 000 // // //
SUMMARY OF OFFSETTING OPERATIONS IN LIRE
1.2.3.4 //
I . ITALY AS DEBTOR //
II . ITALY AS CREDITOR // // // //
CREDITOR
AMOUNTS ( LIT )
DEBTOR
AMOUNTS ( LIT ) // // // //
BELGIUM FRANCE NETHERLANDS
17 692 155 990 119 428 279 380 1 695 441 365
GERMANY LUXEMBOURG UNITED KINGDOM
( 1 166 715 224 ) 267 178 550 129 754 000 // // // //
TOTAL
138 815 876 735
TOTAL
( 1 563 647 774 ) // // //
1,2.3.4TOTAL I - TOTAL II // //
ANNEX II
RULES OF PROCEDURE FOR APPLYING ARTICLE 3, POINT 3 AND ARTICLE 7 ( 2 )
1 . THE FOLLOWING PROVISIONS SHALL APPLY FOR DETERMINING AND REFUNDING PAYMENTS UNDER ARTICLES 94 AND 95 OF REGULATION ( EEC ) NO 574/72 IN DEALINGS BETWEEN GERMANY AND ITALY :
( A ) LUMP-SUM PAYMENTS UNDER ARTICLE 94 OF REGULATION ( EEC ) NO 574/72 UP TO 1980 SHALL BE DETERMINED IN ACCORDANCE WITH TITLE V OF THAT REGULATION AND WITH THE AGREEMENT BETWEEN GERMANY AND ITALY OF 5 NOVEMBER 1968;
( B ) LUMP-SUM PAYMENTS UNDER ARTICLE 95 OF REGULATION ( EEC ) NO 574/72 SHALL BE DETERMINED WITH RESPECT TO ITALIAN CLAIMS UP TO 1979 AND GERMAN CLAIMS UP TO 1980 IN ACCORDANCE WITH TITLE V OF THAT REGULATION;
( C ) LUMP-SUM PAYMENTS OWING TO ITALY UNDER ARTICLE 95 OF REGULATION ( EEC ) NO 574/72 FOR 1980 SHALL BE ESTABLISHED BY PROJECTION OF THE LUMP-SUM SETTLEMENT BETWEEN THE GERMAN AND ITALIAN INSTITUTIONS FOR THE PERIOD 1977, 1978 AND 1979, ACCOUNT BEING TAKEN OF THE PENSIONERS' REGISTER OF THE ALLGEMEINE ORTSKRANKENKASSE BONN ( BONN LOCAL GENERAL SICKNESS FUND ) FOR 1977 TO 1980;
( D ) CLAIMS SHALL BE CALCULATED BY THE GERMAN LIAISON BODY BY 30 SEPTEMBER 1982 AND SETTLED BY 31 DECEMBER 1982 .
2 . THE AMOUNT LAID DOWN IN ANNEX I TO THIS REGULATION AS THE DEBT OF THE GERMAN INSTITUTIONS UNDER ARTICLE 93 OF REGULATION ( EEC ) NO 574/72 FOR THE PERIOD 1977 TO 1980 SHALL BE APPORTIONED BY THE BUNDESVERBAND DER ORTSKRANKENKASSEN ( NATIONAL FEDERATION OF LOCAL SICKNESS FUNDS ), IN ITS CAPACITY AS LIAISON BODY, BETWEEN THE VARIOUS TYPES OF GERMAN SICKNESS INSURANCE INSTITUTIONS IN THE PROPORTIONS RESULTING FROM THE SETTLEMENTS MADE BETWEEN 1973 AND 1976 . WITHIN THE VARIOUS TYPES OF FUND, APPORTIONMENT SHALL BE EFFECTED ACCORDING TO THE NUMBER OF MEMBERS IN 1980 EXCLUSIVE OF PENSIONERS;
3 . THE AMOUNT ESTABLISHED AS THE DEBT OF THE GERMAN INSTITUTIONS UNDER ARTICLE 95 OF REGULATION ( EEC ) NO 574/72 FOR 1980 SHALL BE SETTLED BY THE ALLGEMEINE ORTSKRANKENKASSE BONN ( BONN LOCAL GENERAL SICKNESS FUND ) ON BEHALF OF ALL GERMAN SICKNESS INSURANCE INSTITUTIONS WITH THE EXCEPTION OF THE BUNDESKNAPPSCHAFT ( FEDERAL INSURANCE FUND FOR MINERS ) AND THE AGRICULTURAL SICKNESS FUNDS . THE SUMS PAYABLE BY THE BUNDESKNAPPSCHAFT AND THE RURAL SICKNESS FUNDS SHALL BE DETERMINED BY THE BUNDESVERBAND DER ORTSKRANKENKASSEN ( NATIONAL FEDERATION OF LOCAL SICKNESS FUNDS ), IN ITS CAPACITY AS LIAISON BODY, BY PROJECTION FROM THE FIGURES FOR THE PERIOD 1977, 1978 AND 1979 .
