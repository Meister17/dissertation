Commission Regulation (EC) No 607/2003
of 2 April 2003
amending Council Regulation (EC) No 2007/2000 and Commission Regulation (EC) No 2497/2001 to take account of Commission Regulation (EC) No 1832/2002 amending Annex I to Council Regulation (EEC) No 2658/87 on the tariff and statistical nomenclature and on the Common Customs Tariff
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 2007/2000 of 18 September 2000 introducing exceptional trade measures for countries and territories participating in or linked to the European Union's Stabilisation and Association process, amending Regulation (EC) No 2820/98 and repealing Regulations (EC) No 1763/1999 and (EC) No 6/2000(1), as last amended by Regulation (EC) No 2487/2001(2), and in particular Articles 9 and 10 thereof,
Having regard to Council Regulation (EC) No 2248/2001 of 19 November 2001 on certain procedures for applying the Stabilisation and Association Agreement between the European Communities and their Member States, of the one part, and the Republic of Croatia, of the other part, and for applying the Interim Agreement between the European Community and the Republic of Croatia(3), as amended by Council Regulation (EC) No 2/2003(4), and in particular Articles 4 and 5 thereof,
Whereas:
(1) Commission Regulation (EC) No 1832/2002 of 1 August 2002, amending Annex I to Council Regulation (EEC) No 2658/87 on the tariff and statistical nomenclature and on the Common Customs Tariff(5), made changes to the nomenclature for certain fishery products covered by Regulation (EC) No 2007/2000 as well as by Commission Regulation (EC) No 2497/2001 of 19 December 2001 opening and providing for the administration of Community tariff quotas for certain fish and fishery products originating in the Republic of Croatia(6). For reasons of clarity, Regulation (EC) No 2007/2000 and Regulation (EC) No 2497/2001 should be adjusted accordingly.
(2) The above adjustments should apply from the date of entry into force of Regulation (EC) No 1832/2002.
(3) The measures provided for in this Regulation are in accordance with the opinion of the Customs Code Committee,
HAS ADOPTED THIS REGULATION:
Article 1
In the second column of Annex I to Regulation (EC) No 2007/2000, for order No 09.1571, the following amendments are incorporated:
- CN code " 0302 11 90 " is replaced by CN codes " 0302 11 20 " and " 0302 11 80 ",
- CN code " 0303 21 90 " is replaced by CN codes " 0303 21 20 " and " 0303 21 80 ",
- CN code " 0304 10 11 " is replaced by CN codes " 0304 10 15 " and " 0304 10 17 ",
- CN code " 0304 20 11 " is replaced by CN codes " 0304 20 15 " and " 0304 20 17 ".
Article 2
The Annex to Regulation (EC) No 2497/2001 is amended as follows:
(a) for order No 09.1581, in the second column,
- CN code " 0302 11 90 " is replaced by CN codes " 0302 11 20 " and " 0302 11 80 ",
- CN code " 0303 21 90 " is replaced by CN codes " 0303 21 20 " and " 0303 21 80 ",
- CN code " 0304 10 11 " is replaced by CN codes " 0304 10 15 " and " 0304 10 17 ",
- CN code " 0304 20 11 " is replaced by CN codes " 0304 20 15 " and " 0304 20 17 ";
(b) for order No 09.1584, for CN code " ex 0301 99 90 ", in the third column, the TARIC subdivision "23" is replaced by TARIC subdivisions "15" and "17".
Article 3
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Union.
It shall apply from 1 January 2003.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 2 April 2003.
For the Commission
Frederik Bolkestein
Member of the Commission
(1) OJ L 240, 23.9.2000, p. 1.
(2) OJ L 335, 19.12.2001, p. 9.
(3) OJ L 304, 21.11.2001, p. 1.
(4) OJ L 1, 4.1.2003, p. 26.
(5) OJ L 290, 28.10.2002, p. 1.
(6) OJ L 337, 20.12.2001, p. 27.
