Action brought on 23 June 2006 — Commission of the European Communities v Kingdom of Spain
Parties
Applicant: Commission of the European Communities (represented by: H.Støvlbæk and R.Vidal Puig, Agents)
Defendant: Kingdom of Spain
Form of order sought
- Declare that the Kingdom of Spain has failed to fulfil its obligations under Article 56 of the EC Treaty by maintaining in force measures limiting voting rights of public entities in Spanish undertakings in the energy sector, such as the measures provided for in Supplementary Provision no 27 to Law 55/1999 of 29 December on fiscal, administrative and social measures, as amended by Article 94 of Law 62/2003 of 30 December.
- Order the Kingdom of Spain to pay the costs.
Pleas in law and main arguments
Supplementary Provision no 27 to Law 55/199 provides that where an entity controlled directly or indirectly by a public authority takes control of, or acquires a significant shareholding in, an undertaking in the energy sector, the Council of Ministers may within a period of two months decide "not to recognise", or to impose certain conditions on, exercise of the corresponding political rights. The decision is to be based on certain criteria allegedly designed to safeguard the energy supply.
The Commission considers that Supplementary Provision no 27 to Law 55/1999 is incompatible with Article 56 of the EC Treaty for the following reasons:
- where public entities take control of, or acquire a significant shareholding in, Spanish energy-sector undertakings, that is a "movement of capital" within the meaning of Article 56 of the EC Treaty;
- the restriction of political rights which the Spanish authorities may decide to impose in connection with such taking of control or acquisition of a significant shareholding constitutes a restriction on the free movement of capital, which is prohibited as a matter of principle by Article 56 EC; and
- there is no justification in the Treaty for that restriction.
Specifically, the Commission considers that Supplementary Provision no 27 to Law 55/1999 is not justified by the need to safeguard the energy supply for the following reasons:
- the fact that the entities which take control or acquire a significant shareholding are controlled by a public authority does not pose any extra threat to the energy supply, and therefore cannot justify the introduction of restrictions on the free movement of capital solely in that case;
- restricting voting rights is not an appropriate means of safeguarding the energy supply, there being other more suitable means of doing so;
- even if restricting voting rights were an appropriate means of safeguarding the energy supply, the means at issue are disproportionate, inasmuch as the "non-recognition" of the exercise of voting rights extends to all the activities and decisions of the company;
- the power of the Council of Ministers to decide on the "recognition" or the "non-recognition" of the exercise of voting rights is not governed by criteria which are objective and sufficiently precise, amenable to effective review by the courts.
--------------------------------------------------
