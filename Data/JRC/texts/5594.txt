Authorisation for State aid pursuant to Articles 87 and 88 of the EC Treaty
Cases where the Commission raises no objections
(2005/C 230/04)
(Text with EEA relevance)
Date of adoption of the decision: 1.6.2005
Member State: Sweden
Aid No: N 75/2005
Title: Aid to Volvo Trucks
Objective: Environmental Protection
Aid intensity or amount: SEK 85000000 (EUR 9000000)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 31.8.2004
Member State: Ireland
Aid No: N 264/2004
Title: Extension to the scheme of reliefs for the renewal of certain urban areas (Ex N 270/2002)
Objective: Promote regional development
Legal basis: Taxes Consolidation Act 1997 as amended by Finance Act 2004 section 26(1)(b)
Budget: EUR 40 million in 2005, EUR 50 million in 2006
Aid intensity or amount: In accordance with regional aid map 2000-2006
Duration: 31.12.2004 until 31.7.2006
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 23.12.2004
Member State: Latvia
Aid No: N 360/2004
Title: Amendments to the State aid Programme "Risk Capital Financing for Small and Medium-Sized Enterprises"
Objective(s): Risk Capital for SMEs
Legal basis: Vienotais programmdokuments Latvijai laika posmam no 2004.-2006. gadam un Programmas papildinÿjums vienotajam programmdokumentam Latvijai laika posmÿ no 2004.-2006. gadam
Budget: The total funding of the Programme for the period 2004-2006 will be LVL 10,3 million (approx. EUR 15,4 million)
Duration: 31.12.2006
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 3.5.2005
Member State: France (Limousin)
Aid No: N 382/04
Title: Setting-up of a high-speed infrastructure in the Limousin region
Objective: Financing of a service of general economic interest
Legal basis: Article 1425-1 du Code Général des Collectivités territoriales (JO no 143 du 22 juin 2004. Loi no 2004-575 du 21 juin 2004 pour la confiance dans l'économie numérique)
Budget: The total amount of public funding for the proposed project is EUR 38400000
Aid intensity or amount: Measure not constituting State aid
Duration: 20-year concession
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 1.6.2005
Member State: Lithuania
Aid No: N 425/2004
Title: Kaunas Free Economic Zone
Objective: Regional aid
Legal basis: Lietuvos Respublikos laisvųjų ekonominių zonų pagrindų įstatymas (Žin., 1995, Nr. 59-1462); Lietuvos Respublikos Kauno laisvosios ekonominės zonos įstatymas (Žin., 1996, Nr. 109-2474
Budget: Estimated at EUR 205 million
Aid intensity or amount: 50 % NGE, can be raised with a top-up to 15 % GGE for SMEs
Duration: The term of activity of the zone shall be 49 years. State aid to each individual undertaking shall be terminated as soon as the limits of State aid intensity under the scheme are reached
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 24.3.2004
Member State: Germany
Aid No: N 441/2003
Title: Programme for waste management in Mecklenburg-Western Pomerania
Objective: Environmental Aid
Legal basis: Richtlinie zur Förderung von Maßnahmen der Kreislaufwirtschaft und der umweltverträglichen Beseitigung von Abfällen iVm § 44 Landeshaushaltsordnung
Budget: Ca. EUR 333400 per year
Intensity or amount: Up to 50 %
Duration: 1.1.2004 until 31.12.2006
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 7.6.2005
Member State: United Kingdom (Northern Ireland)
Aid No: N 503/2004
Title: Irish Language Broadcast Fund
Objective: To promote the production of films and other audiovisual works
Legal basis: Educational and Library Services Etc. Grants Regulation (Northern Ireland) 1994
Budget: Overall amount GBP 12 million
Aid intensity or amount: Maximum of 75 % of production costs
Duration: 4 years
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 1.6.2005
Member State: The Netherlands
Aid No: N 580/2004 (prolongation of aid N 746/2001 and N 530/2003)
Title: Film Production Support Scheme
Objective: Cinema and Audiovisual
Legal basis:
Wet Inkomstenbelasting 2001.
Wijziging van enkele belastingwetten (Belastingplan 2005 — artikel XXXIA)
Budget: EUR 45 million
Aid intensity or amount: variable, in any case minor than 50 %
Duration: 1.1.2005 until 30.6.2007
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 2.2.2005
Member State: Greece
Aid No: N 596/2003
Title: Extension of delivery deadline for a passenger cruise vessel
Objective: Shipbuilding
Legal basis: Υπουργική απόφαση 6157/248 της 1ης Απριλίου 2002
Aid intensity: < 9 %
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 3.3.2005
Member State: United Kingdom
Aid No: N 601/2003
Title: Special Purpose Equity Vehicle ("SPEV")
Objective(s): Risk Capital for SMEs
Legal basis: The Industrial Development Act 1982 (Section 8), the Local Government Act 2000 (Section 2), the Regional Development Agencies Act 1998 (Sections 5 and 6) and the Leasehold Reform, Housing and the Urban Development Act 1993 (Part 3 — Section 126)
Budget: The intended size of the fund is GBP 10 million
Duration: The initial investment phase of the SPEV fund will be for 4,75 years from the date of its creation
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 23.6.2005
Member State: The Netherlands
Aid No: NN 24/2005
Title: Environmental investment deduction scheme (Milieu-investeringsaftrek — MIA)
Objective: Stimulate investments in environment-friendly material assets
Legal basis: Aanwijzingsregeling milieu-investeringsaftrek 2005
Budget: EUR 83 million per year
Aid intensity or amount: Maximum 21 %
Duration: Not limited in time
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
--------------------------------------------------
