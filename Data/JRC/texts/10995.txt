Initiation of proceedings
(Case COMP/M.4180 — Gaz de France/Suez)
(2006/C 150/04)
(Text with EEA relevance)
On 19 June 2006, the Commission decided to initiate proceedings in the above-mentioned case after finding that the notified concentration raises serious doubts as to its compatibility with the common market. The initiation of proceedings opens a second phase investigation with regard to the notified concentration. The decision is based on Article 6(1)(c) of Council Regulation (EC) No 139/2004.
The Commission invites interested third parties to submit their observations on the proposed concentration to the Commission.
In order to be fully taken into account in the procedure, observations should reach the Commission not later than 15 days following the date of this publication. Observations can be sent to the Commission by fax (fax No (32-2) 296 43 01 — 296 72 44) or by post, under reference COMP/M.4180 — Gaz de France/Suez, to the following address:
Commission of the European Communities
Competition DG
Merger Registry
Rue Joseph II/Jozef II-straat 70
B-1000 Brussels
--------------------------------------------------
