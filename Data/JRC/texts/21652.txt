COMMISSION DIRECTIVE 92/105/EEC of 3 December 1992 establishing a degree of standardization for plant passports to be used for the movement of certain plants, plant products or other objects within the Community, and establishing the detailed procedures related to the issuing of such plant passports and the conditions and detailed procedures for their replacement
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Directive 77/93/EEC of 21 December 1976 on protective measures against the introduction into the Community of organisms harmful to plants or plant products and against their spread within the Community (1), as last amended by Commission Directive 92/10/EEC (2), and in particular Article 2 (1) (f) second subparagraph, and Article 10 (4) thereof,
Whereas the application of the Community plant-health system to the Community as an area without internal frontiers will necessitate plant-health checks on potentially hazardous Community products before their movement within the Community; whereas the most appropriate place for carrying out these checks is the place of production of producers listed in an official register;
Whereas, if the result of these checks is satisfactory, instead of the phytosanitary certificate used in international trade, a plant passport adapted to the type of product must be attached to the plants, to their packaging or to the vehicle transporting them, to ensure its free movement throughout the Community or those parts thereof for which it is valid;
Whereas, in the case of plants, plant products or other objects originating outside the Community, which have successfully undergone the required plant-health checks on first introduction into the Community, a plant passport must also be attached for the same purpose;
Whereas it is necessary to provide for a standardized layout for the different types of plants or plant products;
Whereas, however, during an initial phase a system making use of a simplified plant passport with a degree of standardization should be used in order to make it possible for plants, plant products or other objects to be moved, as from 1 January 1993; whereas this system will be reconsidered on the basis of an assessment of the gained during the said phase;
Whereas, if one plant passport is to be replaced by another, a special mark must be defined for the replacement passport;
Whereas, with a view to ensuring that the movement of plants, plant products or other objects by Member States is properly monitored, it is necessary to establish more detailed and more uniform procedures for the issuing and replacement of plant passports;
Whereas the measures provided for in this Directive are in accordance with the opinion of the Standing Committee on Plant Health,
HAS ADOPTED THIS DIRECTIVE:
Article 1
1. Member States shall ensure that the conditions laid down in paragraph 2 are met when a plant passport referred to in Article 2 (1) (f), first subparagraph, of Council Directive 77/93/EEC is prepared by their responsible official bodies for use in accordance with the provisions of Articles 2 and 3 of this Directive.
2. The following conditions shall be met:
(a) the plant passport shall consist of an official label and an accompanying document containing the required information as laid down in the Annex hereto. The label shall not have previously been used and shall be of suitable material. The use of official adhesive labels shall be authorized. The 'accompanying document' shall mean any document which is normally used for trade purposes. This document shall not be necessary if the required information as laid down in the Annex is set out on the said label;
(b) the required information shall preferably be printed and shall be in at least one of the official languages of the Community;
(c) in the case of a plant passport for tubers of Solanum tuberosum L., intended for planting, it shall be the official label as specified in Council Directive 66/403/EEC (3). Compliance with the provisions governing the introduction of seed potatoes into, and their movement within, a protected zone recognized in respect of harmful organisms relating to seed potatoes, shall be noted either on the label or on any other trade document.
3. Member States shall require that when the plant passport consists of the label and the accompanying document:
(a) the part of the plant passport consisting of the label shall provide at least the particulars required under items 1 to 5 of the Annex; and
(b) the part of the plant passport consisting of the accompanying document shall provide at least the particulars required under items 1 to 10 of the Annex.
4. Any particulars other than those listed in the Annex, of relevance for labelling purposes under Council Directives 91/682/EEC (4), 92/33/EEC (5) or 92/34/EEC (6) may also be provided on the said accompanying document, but shall be clearly separated from the information specified in the Annex.
Article 2
1. Member States shall ensure that the conditions laid down in paragraph 2 are met when a plant passport is produced, printed and stored.
2. The plant passport shall be produced, printed and/or subsequently stored either by the responsible official bodies referred to in Article 1 (1) directly, or - under their control - by the producer referred to in Article 6 (4), third subparagraph, or person referred to in Article 10 (3), second indent, or importer referred to in Article 12 (6), second subparagraph of Directive 77/93/EEC.
Article 3
1. Member States shall ensure that the conditions laid down in paragraph 2 are met when a plant passport is issued and attached to plants, plant products or other objects, to their packaging or to the vehicles transporting them.
The issuing includes the preparation of the passport, in particular the filling-in of the information, and the action necessary to make the plant passport available for use by the applicant.
2. For the purpose of paragraph 1, the responsible official bodies referred to in Article 1 (1) shall, without prejudice to the requirements laid down in Directive 77/93/EEC:
(a) ensure that the producer, person or importer referred to in Article 2 (2) applies to them for the issuance of a plant passport, or for the replacement of a plant passport;
(b) determine, where appropriate, on the basis of examinations provided for in Article 6 (1) (2) and (3) of Directive 77/93/EEC and carried out in accordance with Article 6 (4) or on the basis of the requirements laid down in Article 10 (3) or 12 (6) of the said Directive, the restrictions applicable to the plants, plant products or other objects, and accordingly the territorial validity of the plant passport, or determine the replacement of the said plant passport, as well as the information to be filled in.
If the producer, person or importer referred to in Article 2 (2) intends to dispatch a plant, plant product or other object into a protected zone referred to in Article 2 (1) (h) of the said Directive for which he does not have a valid plant passport, the said responsible official bodies shall take the necessary steps and determine accordingly whether the product is qualified for the relevant protected zone. The said responsible official bodies shall ensure that the producer, person or importer referred to in Article 2 (2) shall notify the abovementioned intention to them within a reasonable period of time prior to dispatch and shall apply simultaneously for the corresponding plant passport;
(c) ensure that the information is filled in, either entirely in capital letters if the plant passport is pre-printed, or in capital letters or entirely in typescript in all other cases. The botanical name of the plants or plant products shall be indicated in Latin characters; uncertified alterations or erasures shall invalidate the said plant passport;
(d) ensure that if a plant, plant product or other object has received the qualification by them for a specific protected zone(s), the code for the protected zone(s) shall be indicated on the plant passport, against the distinctive marking 'ZP' (zona protecta) indicating that the said plant passport covers a plant, plant product or other object qualified for a protected zone(s);
(e) ensure that if a plant passport is to be delivered for a plant, plant product or other object originating outside the Community, the plant passport shall be used, with the indication of the name of the country of origin or, where appropriate, the consignor country on the said plant passport;
(f) ensure that if a plant passport is to be replaced by another plant passport, the plant passport referred to in Article 1 (1) shall be used; the code for the originally registered producer or importer shall be indicated on the said plant passport, against the distinctive marking 'RP' ('replacement passport') indicating that the said plant passport replaces another plant passport;
(g) depending on where the said plant passport is physically stored, either deliver the said plant passport, or authorize the producer, person or importer referred to in Article 2 (2) to use it accordingly;
(h) ensure that the part of the said plant passport consisting of the label be attached under the responsibility of the producer, person or importer referred to in Article 2 (2), to the plants, plant products or other objects, to their packaging or to the vehicles transporting them in such a manner that it cannot be reused.
Article 4
The system whereby the plant passport referred to in Article 1 (1) is used shall be reconsidered not later than 30 June 1994.
The use of the plant passport referred to in Article 1 (2) (c) shall apply for a period expiring on 30 June 1993.
Article 5
1. Member States shall bring into force the laws, regulations or administrative provisions necessary to comply with this Directive on the date referred to in Article 3 (1) of Council Directive 91/683/EEC (7). They shall forthwith inform the Commission thereof.
2. When Member States adopt these measures, these shall contain a reference to this Directive or shall be accompanied by such reference at the time of their official publication. The procedure for such reference shall be adopted by Member States.
3. Member States shall immediately communicate to the Commission all provisions of domestic law which they adopt in the field governed by this Directive. The Commission shall inform the other Member States thereof.
Article 6
This Directive is addressed to the Member States.
Done at Brussels, 3 December 1992. For the Commission
Ray MAC SHARRY
Member of the Commission
(1) OJ No L 26, 31. 1. 1977, p. 20. (2) OJ No L 70, 17. 3. 1992, p. 27. (3) OJ No 125, 11. 7. 1966, p. 2320/66. (4) OJ No L 376, 31. 12. 1991, p. 21. (5) OJ No L 157, 10. 6. 1992, p. 1. (6) OJ No L 157, 10. 6. 1992, p. 10. (7) OJ No L 376, 31. 12. 1991, p. 29.
ANNEX
REQUIRED INFORMATION
1. 'EEC-plant passport'.
2. Indication of EC Member State code.
3. Indication of responsible official body or its distinguishing code.
4. Registration number.
5. Individual serial, or week or batch number.
6. Botanical name.
7. Quantity.
8. The distinctive marking 'ZP' for the territorial validity of the passport and, where appropriate, the name of the protected zone(s) for which the product is qualified.
9. The distinctive marking 'RP' in case of replacement of a plant passport and, where appropriate, the code for the originally registered producer or importer.
10. Where appropriate, the name of the country of origin or consignor country, for third country products.
