[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 31.01.2006
SEC(2006) 115 final
Draft
DECISION No 1/2006 of the EC – SWITZERLAND JOINT COMMITTEE
replacing Tables III and IV b) of Protocol No 2
- Draft common position of the Community - (presented by the Commission)
Draft
DECISION No 1/2006 of the EC – SWITZERLAND JOINT COMMITTEE
replacing Tables III and IV b) of Protocol No 2
THE JOINT COMMITTEE,
Having regard to the Agreement between the European Economic Community, of the one part, and the Swiss Confederation, of the other part signed in Brussels on 22 July 1972, hereinafter referred to as ‘the Agreement’, as amended by the Agreement between the European Community and the Swiss Confederation amending the Agreement as regards the provisions applicable to processed agricultural products signed in Luxembourg on 26 October 2004, and its Protocol No 2, and in particular Article 7 thereof
Whereas for the implementation of Protocol No 2 to the Agreement, internal reference prices are fixed for the Contracting Parties by the Joint Committee
Whereas actual prices have changed on the domestic markets of the Contracting Parties as regards raw materials for which price compensation measures are applied
Whereas it is therefore necessary to update the reference prices and amounts listed in Tables III and IV b) to Protocol No 2 accordingly
HAS DECIDED AS FOLLOWS:
Article 1
Table III and the table under Table IV point b) of Protocol No 2 are replaced by the tables in Annex I and Annex II to this Decision.
Article 2
This decision shall enter into force on 1 February 2006.
Done at Brussels,
For the Joint Committee
The Chairman
ANNEXE I
Table III
EC and Swiss domestic reference prices
Agricultural | Swiss domestic | EC domestic | Difference Swiss / |
raw material | reference price | reference price | EC reference price |
CHF per | CHF per | CHF per |
100 kg net | 100 kg net | 100 kg net |
Common wheat | 55.36 | 17.88 | 37.48 |
Durum wheat | 35.39 | 26.51 | 8.88 |
Rye | 48.45 | 17.82 | 30.63 |
Barley | 26.48 | 20.33 | 6.15 |
Maize | 29.42 | 20.67 | 8.75 |
Common wheat flour | 99.96 | 37.36 | 62.60 |
Whole-milk powder | 583.10 | 370.70 | 212.40 |
Skimmed-milk powder | 456.50 | 315.29 | 141.21 |
Butter | 897.00 | 433.29 | 463.71 |
White sugar | – | – | 0.00 |
Eggs (1) | 255.00 | 205.50 | 49.50 |
Fresh potatoes | 42.00 | 21.00 | 21.00 |
Vegetable fat (2) | 390.00 | 160.00 | 230.00 |
(1) Derived from the prices for liquid birds’ eggs, not in shell multiplied with factor 0.85.
(2) Prices for vegetable fats (for the baking and food industry) with 100% fat content.
ANNEX II
Table IV
b) The basic amounts for agricultural raw materials taken into account for the calculation of the agricultural components:
Agricultural raw material | Applied basic amount as from the entry into force | Applied basic amount as from three years after the entry into force |
CHF per 100 kg net | CHF per 100 kg net |
Common wheat | 34.00 | 32.00 |
Durum wheat | 8.00 | 8.00 |
Rye | 28.00 | 26.00 |
Barley | 6.00 | 5.00 |
Maize | 8.00 | 7.00 |
Common wheat flour | 54.00 | 51.00 |
Whole-milk powder | 191.00 | 181.00 |
Skimmed-milk powder | 127.00 | 120.00 |
Butter | 464.00 (1) | 464.00 (1) |
White sugar | Zero | Zero |
Eggs | 36.00 | 36.00 |
Fresh potatoes | 19.00 | 18.00 |
Vegetable fat | 207.00 | 196.00 |
(1) Taking into account benefits from the aid for butter granted under Commission Regulation (EC) No 2571/97 of 15 December 1997 and its successors, the applied basic amount of butter is not reduced compared to the price difference in Table III.
FINANCIAL DETAILS | ENTR |
1. The financial details have not yet been communicated. Draft decision replacing Tables III and IV b) of Protocol 2 to be adopted as Community position to be taken in EC – Switzerland Joint Committee. | The financial details have already been communicated, this being : The implementation of a Council Regulation, see financial statement annexed to Written Procedure No ....... |
2. Does the measure as such entail financial consequences? concerns the technical implementing arrangements (1) negligible …………. Financial consequences of the measure as such (on revenue/expenditure) : * for current financial year: -….mio € * for next financial year:-….mio € |
3. Budget headings concerned and appropriations for current financial year - item(s) : all tariff lines mentioned in Table I of Protocol 2 - appropriations: since 1 February 2005 there are no export refunds applicable on exports to Switzerland. |
Financing of measure: a) Possible using appropriations entered in current budget? -under item(s) concerned -under chapter(s) concerned b) If no, is there a risk because of this measure: of insufficient total EAGGF-Guarantee appropriations entered for the current financial year? c) Is it planned/envisaged in the preliminary draft budget for the following year? |
Comments: (1) It concerns a yearly update of measures applicable on the Swiss side based on reduced price differences. |
X
NO
YES
X
