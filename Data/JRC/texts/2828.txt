Opinion of the European Economic and Social Committee on the "Proposal for a Directive of the European Parliament and of the Council correcting Directive 2004/18/EC on the coordination of procedures for the award of public works contracts, public supply contracts and public service contracts"
(COM(2005) 214 final — 2005/0100 (COD))
(2006/C 24/02)
On 8 June 2005 the Council decided, under Article 95 of the Treaty establishing the European Community, to consult the European Economic and Social Committee on the abovementioned proposal.
The Section for the Single Market, Production and Consumption, which was responsible for preparing the Committee's work on the subject, adopted its opinion on 27 July 2005. The rapporteur working alone was Mr Petringa.
At its 420th plenary session, held on 28 and 29 September 2005 (meeting of 28 September 2005), the European Economic and Social Committee adopted the following opinion by 161 votes in favour with three abstentions.
1. Introduction
1.1 Directive 2004/18/EC of 31 March 2004 regulates and guarantees the coordination of the procedures for the award of public works contracts, public supply contracts and public service contracts. This directive consolidated the previous directives on the subject, modifying their content and creating a simplified legal framework.
1.2 In defining its scope, the directive lays down a series of thresholds below which it does not apply; it also provides for a specific procedure through which these thresholds can be periodically realigned.
1.3 The thresholds in the directive are expressed in euro, while those defining the international obligations of the Union on public contracts in WTO terms are expressed in Special Drawing Rights (SDRs). Article 78 of the Directive allows the Commission to check and modify the thresholds if the development of SDR/euro exchange rates makes it necessary. Even when a revision takes place, however, the level of the thresholds should remain essentially the same.
1.4 The Commission had decided in particular to make no change in the thresholds of EUR 249,000 applying to public service contracts subsidised to a level of more than 50 % by the awarding authorities [Article 8b)], and to public supply contracts awarded by the awarding authorities non included in Annex IV (that is to say awarding authorities other than central government authorities).
1.5 Owing to a clerical error, Article 78 provides that contracts subsidised to a level of more than 50 % by the awarding authorities, mentioned in Article 8b), shall be realigned on a different value, which in fact reduces their level.
2. Conclusions
2.1 The present draft directive simply calls for the correction of the clerical error. By correcting Article 78, consistency is restored between the threshold given in Article 8, which regulates the scope of the directive, and the mechanism for revising the thresholds provided for in Article 78 itself.
2.2 The EESC can only endorse a modification which restores consistency to the regulatory text.
2.3 The above is particularly important in view of the next deadline for revising the thresholds under Article 78(4), namely November 2005.
Brussels, 28 September 2005.
The President
of the European Economic and Social Committee
Anne-Marie Sigmund
--------------------------------------------------
