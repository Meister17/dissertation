COUNCIL DECISION
of 3 May 1999
authorising the Secretary-General of the Council of the European Union in the context of the integration of the Schengen acquis into the framework of the European Union to act as representative of certain Member States for the purposes of concluding contracts relating to the installation and functioning of the "Help Desk Server" of the Management Unit and of the Sirene Network Phase II and to manage such contracts
(1999/322/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Acting on the basis of Article 7 of the Protocol annexed to the Treaty on European Union and the Treaty establishing the European Community, integrating the Schengen acquis into the framework of the European Union,
(1) Whereas within the framework of cooperation between the Member States which are signatories to the 1990 Schengen Convention implementing the 1985 Schengen Agreement and instruments of accession thereto, the Secretary-General of the Benelux Economic Union has been authorised to conclude and manage certain contracts on behalf of such Member States;
(2) Whereas as a result of the integration of the Schengen Secretariat into the General Secretariat of the Council of the European Union, this task performed hitherto by the Secretary-General of the Benelux Economic Union within the framework of the Schengen cooperation should be taken over by the Secretary-General of the Council;
(3) Whereas the performance of such a task by the Secretary-General of the Council on behalf of certain Member States will constitute a new task, distinct from the tasks performed by the Secretary-General pursuant to his obligations under the Treaty establishing the European Community and the Treaty on European Union;
(4) Whereas it is therefore appropriate to have this new task assigned to the Secretary-General by way of an explicit decision of the Council,
HAS DECIDED AS FOLLOWS:
Article 1
1. The Council hereby authorises the Secretary-General of the Council of the European Union to act as the representative of the Member States concerned, by replacing the Secretary-General of the Benelux Economic Union in the Agreement concluded by the latter on behalf of these Member States on 23 August 1996 with France Telecom Network Services Belgium, now Global One Belgium, on the delivery, installation and management of the Sirene Network Phase II and the supply of services relating to the use thereof.
2. The Council hereby authorises the Secretary-General of the Council to act as the representative of the Member States concerned, for the purpose of concluding an Agreement on behalf of the latter with Digital Equipment SA for the operation and setting-up of a Help Desk Server on the expiry of the current Agreement concluded between the Secretary General of the Benelux Economic Union and Digital Equipment SA on 8 May 1996.
3. The authorisations under paragraphs 1 and 2 above shall apply so long as payments under the aforementioned Agreements are not charged to the general budget of the European Communities but continue to be charged to the Member States concerned.
Article 2
The work involved in managing the Agreements referred to in Article 1 on behalf of the Member States concerned shall be performed by the General Secretariat of the Council as part of its normal administrative tasks.
Article 3
All questions relating to any non-contractual liability resulting from the acts or omissions of the General-Secretariat of the Council in the performance of its administrative tasks pursuant to this Decision shall be governed by Article 288, second paragraph, of the Treaty establishing the European Community. Article 235 of that Treaty shall therefore apply to any disputes relating to compensation for damage.
Article 4
The Secretary-General of the Council shall open a special bank account in his name for the purpose of taking up, as from the date of the entry into force of the Treaty of Amsterdam, the balance of the budget relating to the management, up till that date, by the Secretary-General of the Benelux Economic Union, of contracts with the companies mentioned in Article 1(1) and (2).
Article 5
This Decision shall take effect on the day of its adoption.
Article 6
This Decision shall be published in the Official Journal of the European Communities.
Done at Brussels, 3 May 1999.
For the Council
The President
J. FISCHER
