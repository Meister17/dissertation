*****
COUNCIL REGULATION (EEC) No 1716/84
of 18 June 1984
amending Regulation (EEC) No 1883/78 laying down general rules for the financing of interventions by the Guarantee Section of the European Agricultural Guidance and Guarantee Fund
THE COUNCIL OF THE EUROPEAN
COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 729/70 of 21 April 1970 on the financing of the common agricultural policy (1), as last amended by Regulation (EEC) No 3509/80 (2), and in particular Article 3 (2) thereof,
Having regard to the proposal from the Commission,
Whereas the Annex to Regulation (EEC) No 1883/78 (3), as last amended by Regulation (EEC) No 1550/83 (4), lists measures complying with the concept of intervention intended to stabilize the agricultural markets within the meaning of Article 3 (1) of Regulation (EEC) No 729/70; whereas that Annex should be brought up to date as a number of measures have been adopted or repealed since it was last amended;
Whereas since the entire Annex was redrafted in the form of a single list in 1981, it has been amended by Regulations (EEC) No 1262/82 (5) and (EEC) No 1550/83; whereas the entire Annex should therefore be redrafted in the form of a single list once again;
Whereas, for measures which do not appear in the new Annex but in respect of which expenditure may be in the process of being incurred, the former Annexes shall continue to apply,
HAS ADOPTED THIS REGULATION:
Sole Article
The Annex to Regulation (EEC) No 1883/78 is hereby replaced by the Annex to this Regulation.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Luxembourg, 18 June 1984.
For the Council
The President
M. ROCARD
(1) OJ No L 94, 28. 4. 1970, p. 13.
(2) OJ No L 367, 31. 12. 1980, p. 87.
(3) OJ No L 216, 5. 8. 1978, p. 1.
(4) OJ No L 158, 16. 6. 1983, p. 9.
(5) OJ No L 148, 27. 5. 1982, p. 1.
ANNEX
'ANNEX
MEASURES REFERRED TO IN ARTICLE 1
I. CEREALS AND RICE
A. Cereals
1. The buying in and consequent transactions carried out by an intervention agency pursuant to Article 7 (1), (2) and (3) of Regulation (EEC) No 2727/75.
2. The particular and special intervention measures provided for in Article 8 (1) and (2) of Regulation (EEC) No 2727/75.
3. The carry-over payments in respect of stocks remaining at the end of the marketing year provided for in Article 9 of Regulation (EEC) No 2727/75.
4. The production aid for durum wheat provided for in Article 10 of Regulation (EEC) No 2727/75.
5. The production refunds and premiums for potato starch provided for in Article 11 of Regulation (EEC) No 2727/75.
6. The subsidies provided for in Article 23 of Regulation (EEC) No 2727/75.
B. Rice
1. The buying in and consequent transactions carried out by an intervention agency pursuant to Article 5 (1), (2) and (3) of Regulation (EEC) No 1418/76.
2. The special intervention measures provided for in Article 6 of Regulation (EEC) No 1418/76.
3. The carry-over payments in respect of stocks remaining at the end of the marketing year provided for in Article 8 of Regulation (EEC) No 1418/76.
4. The production refunds provided for in Article 9 of Regulation (EEC) No 1418/76.
5. The subsidies for deliveries of Community rice to the French overseas department of Réunion provided for in Article 11a of Regulation (EEC) No 1418/76.
II. SUGAR
1. The storage costs provided for in Article 8 (2) of Regulation (EEC) No 1785/81.
2. The buying in and consequent transactions carried out by an intervention agency pursuant to Article 9 (1), 11 and 34 of Regulation (EEC) No 1785/81.
3. The premiums for sugar rendered unfit for human consumption provided for in Article 9 (2) of Regulation (EEC) No 1785/81.
4. The production refunds provided for in Article 9 (3) of Regulation (EEC) No 1785/81.
5. The measures taken to permit the marketing of sugar produced in the French overseas departments pursuant to Article 9 (4) of Regulation (EEC) No 1785/81.
6. The special intervention measures to help guarantee supplies provided for in Article 10 of Regulation (EEC) No 1785/81.
7. The import subsidies provided for in Article 18 (2) of Regulation (EEC) No 1785/81.
8. The amount referred to in Article 6 of Regulation (EEC) No 1789/81 levied on sugar from the minimum stock marketed other than in accordance with the rules laid down.
III. OLIVE OIL
1. The production aid provided for in Article 5 (1) of Regulation No 136/66/EEC.
2. The consumption aid provided for in Article 11 (1) of Regulation No 136/66/EEC.
3. The publicity campaigns and other projects to promote the consumption of olive oil provided for in Article 11 (6) of Regulation No 136/66/EEC. 4. The buying in and consequent transactions carried out by an intervention agency pursuant to Article 12 (1) and (2) of Regulation No 136/66/EEC.
5. The measures provided for in Article 13 of Regulation No 136/66/EEC (buffer stock).
6. The production refunds in respect of olive oil used in the manufacture of preserved fish and vegetables provided for in Article 20a of Regulation No 136/66/EEC.
7. The storage contracts provided for in Article 20d (3) of Regulation No 136/66/EEC.
8. The corrective amount granted or levied in trade between Greece and the other Member States provided for in Article 3 of Regulation (EEC) No 2919/82.
IV. OILSEEDS AND PROTEIN PLANTS
A. Oilseeds
A.1 Colza, rape and sunflower seeds
1. The buying in and consequent transactions carried out by an intervention agency pursuant to Article 26 (1) of Regulation No 136/66/EEC.
2. The aid in respect of harvested and processed oilseeds provided for in Article 27 (1) of Regulation No 136/66/EEC.
3. The early marketing allowance provided for in Article 27 (2) of Regulation No 136/66/EEC.
4. Any derogating measures adopted pursuant to Article 36 of Regulation No 136/66/EEC.
5. The differential amounts granted or levied at the time of the processing of colza, rape and sunflower seed provided for in Regulation (EEC) No 1569/72.
A.2 Other oilseeds
1. The aid for soya provided for in Article 2 of Regulation (EEC) No 1614/79.
2. The aid for linseed provided for in Article 2 of Regulation (EEC) No 569/76.
3. The aid for castor seed provided for in Article 2 of Regulation (EEC) No 2874/77.
4. The supplementary aid for castor seed provided for in Article 1 of Regulation (EEC) No 1610/79.
B. Protein plants
B.1 Peas, field beans and sweet lupin
1. The aid for Community products used in the manufacture of feedingstuffs provided for in Article 3 (1) of Regulation (EEC) No 1431/82.
2. The aid for Community products used for human or animal consumption provided for in Article 3 (2) of Regulation (EEC) No 1431/82.
B.2 Dried fodder
1. The flat-rate production aid provided for in Article 3 of Regulation (EEC) No 1117/78.
2. The supplementary aid provided for in Article 5 of Regulation (EEC) No 1117/78.
V. TEXTILE PLANTS AND SILKWORMS
A. Fibre flax and hemp
1. The production aid provided for in Article 4 of Regulation (EEC) No 1308/70.
2. The private storage aid provided for in Article 5 of Regulation (EEC) No 1308/70.
3. The measures to encourage the use of flax fibres provided for in Article 1 of Regulations (EEC) No 2511/80 and (EEC) No 1423/82.
B. Silkworms
The aid for silkworm rearing provided for in Article 2 of Regulation (EEC) No 845/72. VI. FRUIT AND VEGETABLES
A. Fresh fruit and vegetables
1. The financial compensation granted to producers' organizations provided for in Article 18 of Regulation (EEC) No 1035/72.
2. The compensation to non-member producers referred to in Article 18a of Regulation (EEC) No 1035/72.
3. The buying in provided for in Articles 19 and 19a of Regulation (EEC) No 1035/72 when the Community market is in a state of serious crisis.
4. The measures to dispose of products withdrawn from the market provided for in Article 21 (1) and (3) of Regulation (EEC) No 1035/72.
5. The compensation granted to farmers pursuant to Article 21 (2) of Regulation (EEC) No 1035/72.
6. The financial compensation designed to promote the marketing of Community citrus fruit provided for in Articles 6 and 8 of Regulation (EEC) No 2511/69.
7. The financial compensation designed to encourage the processing of certain varieties of oranges provided for in Article 3 of Regulation (EEC) No 2601/69.
8. The financial compensation intended to encourage the marketing of products processed from lemons provided for in Regulation (EEC) No 1035/77.
B. Processed fruit and vegetables
1. The production aid for tinned pineapple provided for in Article 1 of Regulation (EEC) No 525/77.
2. The production aid for certain products processed from fruit and vegetables provided for in Article 3 of Regulation (EEC) No 516/77.
3. The special measures for dried grapes and dried figs provided for in Article 4 (4) of Regulation (EEC) No 516/77.
4. The storage aid and financial compensation for dried grapes and dried figs provided for in Article 4 (5) and (6) of Regulation (EEC) No 516/77 and in Article 10 of Regulation (EEC) No 2194/81 (1981 to 1983 harvests).
5. The aid for the restorage of dried grapes of the sultana variety provided for in Article 10 (a) of Regulation (EEC) No 2194/81.
6. The financial compensation provided for in Article 1 (4) of Regulation (EEC) No 1603/83.
VII. PRODUCTS OF THE WINE-GROWING SECTOR
1. The aid for the private storage of table wine, grape must, concentrated grape must and rectified concentrated grape must provided for in Article 7 of Regulation (EEC) No 337/79.
2. The aid for the restorage of table wine provided for in Article 10 of Regulation (EEC) No 337/79.
3. The preventive distillation provided for in Article 11 of Regulation (EEC) No 337/79.
4. The additional measures reserved to the holders of long-term storage contracts provided for in Article 12a of Regulation (EEC) No 337/79.
5. The measures intended to encourage expansion of the markets for table wine provided for in Article 12b of Regulation (EEC) No 337/79.
6. The aid for concentrated grape must and rectified concentrated grape must used to increase alcoholic strength provided for in Article 14 of Regulation (EEC) No 337/79.
7. The aid for grape must used to produce grape juice, British wines, Irish wines and other similar drinks, provided for in Article 14a of Regulation (EEC) No 337/79.
8. The distillation of table wine and any other appropriate measure provided for in Article 15 of Regulation (EEC) No 337/79. 9. The aid granted, as well as the portion of the expenditure incurred by the intervention agencies in respect of the distillation operations provided for in Articles 39 and 40 of Regulation (EEC) No 337/79, financed by the EAGGF Guarantee Section.
10. The measures to dispose of the products of the distillation operations provided for in Articles 39 and 40 taken pursuant to Article 40a of Regulation (EEC) No 337/79.
11. The aid granted in respect of the compulsory distillation of table wine provided for in Article 41 of Regulation (EEC) No 337/79.
12. The buying in of alcohol and consequent transactions carried out by an intervention agency pursuant to Articles 41 and 41a of Regulation (EEC) No 337/79.
13. The measures intended to encourage the use of methods other than distillation provided for in Article 41c of Regulation (EEC) No 337/79.
14. The intervention measures for products other than table wine provided for in Article 57 of Regulation (EEC) No 337/79.
15. The derogating measures consequent on natural disasters provided for in Article 62 of Regulation (EEC) No 337/79.
VIII. RAW TOBACCO
1. The premiums provided for in Articles 3 and 4 of Regulation (EEC) No 727/70.
2. The buying in and consequent transactions carried out by an intervention agency pursuant to Articles 5, 6 and 7 of Regulation (EEC) No 727/70.
IX. OTHER AGRICULTURAL SECTORS OR PRODUCTS
A. Seeds
The production aid provided for in Article 3 of Regulation (EEC) No 2358/71.
B. Hops
The production aid provided for in Article 12 of Regulation (EEC) No 1696/71.
C. Bee-keeping
1. The aid provided for in Article 1 of Regulation (EEC) No 1196/81.
2. The ad hoc research programme into varroamite disease provided for in Article 2a of Regulation (EEC) No 1196/81.
X. MILK AND MILK PRODUCTS
A. Skimmed milk
1. The buying in of skimmed-milk powder and consequent transactions carried out by an intervention agency pursuant to Article 7 (1) and (2) of Regulation (EEC) No 804/68.
2. The private storage aid for skimmed-milk powder provided for in Article 7 (3) of Regulation (EEC) No 804/68.
3. The aid for skimmed milk and skimmed-milk powder used for animal feedingstuffs provided for in Article 10 of Regulation (EEC) No 804/68.
4. The aid for skimmed milk processed into casein or caseinates provided for in Article 11 of Regulation (EEC) No 804/68.
B. Butter
1. The buying in of butter and consequent transactions carried out by an intervention agency pursuant to Article 6 (1) and the first subparagraph or the first sentence of the second subparagraph of Article 6 (3) of Regulation (EEC) No 804/68.
2. The private storage aid for butter and cream provided for in Article 6 (2) of Regulation (EEC) No 804/68.
3. The special measures for the disposal of butter provided for in the second sentence of the second subparagraph of Article 6 (3) of Regulation (EEC) No 804/68. C. Other measures
1. The additional levy provided for in Article 5c of Regulation (EEC) No 804/68.
2. The buying in of Grana Padano and Parmigiano Reggiano cheeses and consequent transactions carried out by an intervention agency pursuant to Article 8 (1) and (2) of Regulation (EEC) No 804/68.
3. The private storage aid for Grana Padano, Parmigiano Reggiano and Provolone cheeses provided for in Article 8 (3) of Regulation (EEC) No 804/68.
4. The intervention measures for long-keeping cheeses provided for in Article 9 of Regulation (EEC) No 804/68.
5. The measures relating to the reduction of surpluses of milk products provided for in Article 12 of Regulation (EEC) No 804/68.
6. The Community aid granted for supplying milk products to pupils in educational establishments pursuant to Article 26 (1) of Regulation (EEC) No 804/68.
7. The non-marketing premiums for milk and milk products and the premiums for the conversion of dairy herds to meat production provided for in Article 1 of Regulation (EEC) No 1078/77 (1).
8. The co-responsibility levy and the measures to expand the markets for milk products pursuant to Articles 1 and 4 of Regulation (EEC) No 1079/77.
9. The measures intended to support the incomes of small-scale milk producers provided for in Article 2a of Regulation (EEC) No 1079/77.
XI. BEEF AND VEAL
1. The private storage aid provided for in Article 5 (1) (a) of Regulation (EEC) No 805/68.
2. The buying in and consequent transactions carried out by an intervention agency pursuant to Articles 5, 6 and 7 of Regulation (EEC) No 805/68.
3. The exceptional market support measures provided for in Article 23 of Regulation (EEC) No 805/68.
4. The calf premiums provided for in Article 1 of Regulation (EEC) No 1201/82, as amended by Regulation (EEC) No 1215/83, and in Article 1 of Regulation (EEC) No 1064/84.
5. The premiums for the slaughter of certain adult cattle provided for in Article 1 of Regulation (EEC) No 1200/82, as amended by Regulation (EEC) No 1217/83.
6. The premium for the slaughter of certain adult bovine animals provided for in Article 1 of Regulation (EEC) No 1063/84 and the equivalent amount referred to in Article 3 of that same Regulation.
7. The premiums for maintaining suckler cows provided for in Articles 1 and 3 (1) of Regulation (EEC) No 1357/80.
8. The premiums additional to the premiums for maintaining suckler cows provided for in Regulation (EEC) No 1199/82, as amended by Regulation (EEC) No 870/84.
XII. SHEEPMEAT AND GOATMEAT
1. The premiums granted to sheepmeat producers to offset loss of income provided for in Article 5 of Regulation (EEC) No 1837/80.
2. The private storage aid provided for in Article 6 (1) (a) of Regulation (EEC) No 1837/80.
3. The buying in and consequent transactions carried out by an intervention agency pursuant to Article 6 (1) (b) Regulation (EEC) No 1837/80.
4. The variable slaughter premiums for sheep provided for in Article 9 (1) of Regulation (EEC) No 1837/80.
5. The exceptional market support measures provided for in Article 22 of Regulation (EEC) No 1837/80.
XIII. PIGMEAT
1. The private storage aid provided for in the first indent of the first subparagraph of Article 3 of Regulation (EEC) No 2759/75.
2. The buying in and consequent transactions carried out by an intervention agency pursuant to Articles 3, 4, 5 and 6 of Regulation (EEC) No 2759/75.
3. The private storage aid provided for in Article 20 of Regulation (EEC) No 2759/75.
XIV. PROVISIONS COMMON TO SEVERAL SECTORS
1. The monetary compensatory amounts levied and granted in trade between Member States pursuant to Regulation (EEC) No 974/71.
2. The accession compensatory amounts granted in trade between Member States and Greece pursuant to Articles 43 and 61 of the 1979 Act of Accession.
XV. FISHERY PRODUCTS
1. The financial compensation granted by the Member States to producers' organizations provided for in Article 13 of Regulation (EEC) No 3796/81.
2. The free distribution of goods withdrawn from the market pursuant to Article 13 of Regulation (EEC) No 3796/81.
3. The carry-over premiums provided for in Article 14 (1) of Regulation (EEC) No 3796/81.
4. The special carry-over premiums for anchovies and sardines provided for in Article 14 (3) of Regulation (EEC) No 3796/81.
5. The private storage aid provided for in Article 16 of Regulation (EEC) No 3796/81.
6. The compensation to Community tuna producers provided for in Article 17 of Regulation (EEC) No 3796/81.
7. The compensation to Community salmon and lobster producers provided for in Article 18 of Regulation (EEC) No 3796/81.
XVI. MEASURE TO WHICH THE PROVISIONS OF REGULATION (EEC) No 729/70 HAVE BEEN ADDITIONALLY APPLIED, 'MUTATIS MUTANDIS'
Aid for unginned cotton provided for in Article 5 of Regulation (EEC) No 2169/81.
(1) EAGGF financing: 60 % by the Guarantee Section and 40 % by the Guidance Section.
