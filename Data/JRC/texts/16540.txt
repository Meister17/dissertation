COMMISSION DIRECTIVE 96/87/EC of 13 December 1996 adapting to technical progress Council Directive 96/49/EC on the approximation of the laws of the Member States with regard to the transport of dangerous goods by rail (Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 96/49/EC of 23 July 1996 on the approximation of the laws of the Member States with regard to the transport of dangerous goods by rail (1), and in particular Article 8 thereof,
Whereas the Regulations concerning the international carriage of dangerous goods by rail, usually known as the RID, as amended, should be attached to Directive 96/49/EC as an Annex and be applicable not only to cross-frontier transport, but also to transport within the individual Member States;
Whereas the Annex to Directive 96/49/EC contains the RID as applicable from 1 January 1995;
Whereas the RID is updated every two years and therefore an amended version will be in force as from 1 January 1997;
Whereas in accordance with Article 8, all the necessary modifications for adapting to technical and scientific progress in the field covered by the Directive and aiming at aligning it to the new rules have to be adopted in accordance with the procedure set out in Article 9;
Whereas it is necessary to adapt the sector to the new RID rules and therefore to amend the Annex to Directive 96/49/EC;
Whereas the measures provided for in the Directive are in accordance with the opinion of the Committee provided by Article 9 of Directive 96/49/EC,
HAS ADOPTED THIS DIRECTIVE:
Article 1
The Annex to Directive 96/49/EC is modified as follows:
'The Annex comprises the provisions of the Regulations concerning the international carriage of dangerous goods by rail (RID), appearing as Annex I to Appendix B to Cotif, as applicable with effect from 1 January 1997, on the understanding that "Contracting Party" and "the States or the railways" will be replaced by "Member State".
NB: Versions of the RID in all the official languages of the Community will be published as soon as a text is ready in all languages.`
Article 2
1. The Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive before 1 January 1997. They shall forthwith inform the Commission thereof.
When the Member States adopt these measures they shall contain a reference to this Directive or shall be accompanied by such reference on the occasion of their official publication. The methods of making such reference shall be laid down by Member States.
2. Member States shall communicate to the Commission the texts of the provisions of national law which they adopt in the field governed by this Directive.
Article 3
This Directive shall enter into force on the day following that of its publication in the Official Journal of the European Communities.
Article 4
This Directive is addressed to the Member States.
Done at Brussels, 13 December 1996.
For the Commission
Neil KINNOCK
Member of the Commission
(1) OJ No L 235, 17. 9. 1996, p. 25.
