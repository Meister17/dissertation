Euro exchange rates [1]
10 November 2006
(2006/C 275/01)
| Currency | Exchange rate |
USD | US dollar | 1,2864 |
JPY | Japanese yen | 151,17 |
DKK | Danish krone | 7,4576 |
GBP | Pound sterling | 0,67230 |
SEK | Swedish krona | 9,0888 |
CHF | Swiss franc | 1,5933 |
ISK | Iceland króna | 87,09 |
NOK | Norwegian krone | 8,2005 |
BGN | Bulgarian lev | 1,9558 |
CYP | Cyprus pound | 0,5777 |
CZK | Czech koruna | 28,196 |
EEK | Estonian kroon | 15,6466 |
HUF | Hungarian forint | 260,80 |
LTL | Lithuanian litas | 3,4528 |
LVL | Latvian lats | 0,6962 |
MTL | Maltese lira | 0,4293 |
PLN | Polish zloty | 3,8326 |
RON | Romanian leu | 3,5110 |
SIT | Slovenian tolar | 239,68 |
SKK | Slovak koruna | 36,198 |
TRY | Turkish lira | 1,8600 |
AUD | Australian dollar | 1,6755 |
CAD | Canadian dollar | 1,4531 |
HKD | Hong Kong dollar | 10,0126 |
NZD | New Zealand dollar | 1,9336 |
SGD | Singapore dollar | 2,0021 |
KRW | South Korean won | 1201,56 |
ZAR | South African rand | 9,2970 |
CNY | Chinese yuan renminbi | 10,1169 |
HRK | Croatian kuna | 7,3225 |
IDR | Indonesian rupiah | 11727,47 |
MYR | Malaysian ringgit | 4,6799 |
PHP | Philippine peso | 64,159 |
RUB | Russian rouble | 34,2400 |
THB | Thai baht | 47,110 |
[1] Source: reference exchange rate published by the ECB.
--------------------------------------------------
