Commission Regulation (EC) No 1376/2006
of 18 September 2006
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 19 September 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 18 September 2006.
For the Commission
Jean-Luc Demarty
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 386/2005 (OJ L 62, 9.3.2005, p. 3).
--------------------------------------------------
ANNEX
to Commission Regulation of 18 September 2006 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code [1] | Standard import value |
07020000 | 052 | 76,9 |
096 | 39,6 |
999 | 58,3 |
07070005 | 052 | 94,7 |
999 | 94,7 |
07099070 | 052 | 96,4 |
999 | 96,4 |
08055010 | 388 | 58,4 |
524 | 53,3 |
528 | 56,1 |
999 | 55,9 |
08061010 | 052 | 75,3 |
220 | 32,1 |
624 | 105,3 |
999 | 70,9 |
08081080 | 388 | 87,2 |
400 | 92,4 |
508 | 57,4 |
512 | 92,7 |
528 | 74,1 |
720 | 82,6 |
800 | 164,6 |
804 | 92,0 |
999 | 92,9 |
08082050 | 052 | 118,9 |
388 | 89,8 |
999 | 104,4 |
08093010, 08093090 | 052 | 121,3 |
999 | 121,3 |
08094005 | 052 | 86,8 |
066 | 66,2 |
098 | 33,4 |
624 | 128,7 |
999 | 78,8 |
[1] Country nomenclature as fixed by Commission Regulation (EC) No 750/2005 (OJ L 126, 19.5.2005, p. 12). Code "999" stands for "of other origin".
--------------------------------------------------
