Commission Decision
of 25 April 2006
concerning the non-inclusion of methabenzthiazuron in Annex I to Council Directive 91/414/EEC and the withdrawal of authorisations for plant protection products containing this active substance
(notified under document number C(2006) 1653)
(Text with EEA relevance)
(2006/302/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/414/EEC of 15 July 1991 concerning the placing of plant protection products on the market [1], as last amended by Commission Directive 2004/20/EC [2], and in particular the fourth subparagraph of Article 8(2) thereof,
Whereas:
(1) Article 8(2) of Directive 91/414/EEC provides that a Member State may, during a period of 12 years following the notification of that Directive, authorise the placing on the market of plant protection products containing active substances not listed in Annex I of that Directive that are already on the market two years after the date of notification, while those substances are gradually being examined within the framework of a programme of work.
(2) Commission Regulation (EC) No 1490/2002 [3] lay down the detailed rules for the implementation of the second and third stages of the programme of work referred to in Article 8(2) of Directive 91/414/EEC. For active substances for which a notifier fails to fulfil its obligations under these Regulations no completeness check or evaluation of the dossier shall be performed. For methabenzthiazuron no complete dossier has been submitted within the prescribed time limit. As a consequence, this active substance should not be included in Annex I to Directive 91/414/EEC and Member States should withdraw all authorisations for plant protection products containing this substance.
(3) For the active substances for which there is only a short period of advance notice for the withdrawal of plant protection products containing such substances, it is reasonable to provide for a period of grace for disposal, storage, placing on the market and use of existing stocks for a period no longer than twelve months to allow existing stocks to be used in no more than one further growing. In cases where a longer advance notice period is provided, such period can be shortened to expire at the end of the growing season.
(4) For methabenzthiazuron information has been presented and evaluated by the Commission together with Member State experts which has shown a need for further use of the substance concerned. In such cases temporary measures should be provided for to enable the development of alternatives.
(5) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
Methabenzthiazuron shall not be included in Annex I to Directive 91/414/EEC.
Article 2
Member States shall ensure that:
(a) Authorisations for plant protection products containing methabenzthiazuron are withdrawn by 25 October 2006;
(b) From 26 April 2006 no authorisations for plant protection products containing methabenzthiazuron are granted or renewed under the derogation provided for in Article 8(2) of Directive 91/414/EEC.
Article 3
1. By derogation from Article 2, a Member State listed in column B of the Annex may maintain authorisations for plant protection products containing substances listed in column A for uses listed in column C of that Annex until 30 June 2009 at the latest.
A Member State making use of the derogation provided for in the first subparagraph shall ensure that the following conditions are complied with:
(a) the continued use is only accepted so far as it has no harmful effects on human or animal health and no unacceptable influence on the environment;
(b) such plant protection products remaining on the market after 25 October 2006 are relabelled in order to match the restricted use conditions;
(c) all appropriate risk mitigation measures are imposed to reduce any possible risks;
(d) alternatives for such uses are being seriously sought.
2. The Member State concerned shall inform the Commission about the measures taken in application of paragraph 1, and in particular about the actions taken pursuant to points (a) to (d), by 31 December of each year.
Article 4
Any period of grace granted by Member States in accordance with Article 4(6) of Directive 91/414/EEC, shall be as short as possible.
Where authorisations shall be withdrawn in accordance with Article 2 by 25 October 2006 at the latest, the period shall expire on 25 October 2007 at the latest.
Where authorisations shall be withdrawn in accordance with Article 3(1) by 30 June 2009 at the latest, the period shall expire on 31 December 2009 at the latest.
Article 5
This Decision is addressed to the Member States.
Done at Brussels, 25 April 2006.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 230, 19.8.1991, p. 1.
[2] OJ L 70, 9.3.2004, p. 32.
[3] OJ L 224, 21.8.2002, p. 23. Regulation as amended by Commission Regulation (EC) No 1044/2003 of 18 June 2003 (OJ L 151, 19.6.2003, p. 32).
--------------------------------------------------
ANNEX
List of authorisations referred to in Article 3(1)
Column A | Column B | Column C |
Active substance | Member State | Use |
Methabenzthiazuron | Belgium | Leek, peas |
France | Allium crops Forage grasses Leguminous crops |
--------------------------------------------------
