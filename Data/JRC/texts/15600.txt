Summary information communicated by Member States regarding State aid granted under Commission Regulation (EC) No 1/2004 of 23 December 2003 on the application of Articles 87 and 88 of the EC Treaty to State aid to small and medium-sized enterprises active in the production, processing and marketing of agricultural products
(2006/C 181/03)
Aid No : XA 72/04
Member State : Spain
Region : Castile-Leon
Title of aid scheme or name of company receiving an individual aid : Support for SMEs to gain tax incentives regarding activities relating to research and development (R&D) and to technological innovation (TI)
Legal basis : Acuerdo del Consejo Rector de la Agencia de Desarrollo Económico de Castilla y León por el que se aprueban las bases de la convocatoria de determinadas ayudas o incentivos de la Agencia de Desarrollo Económico de Castilla y León cofinanciados con fondos estructurales
Annual expenditure planned under the scheme or overall amount of individual aid granted to the company : EUR 600000 annually
Maximum aid intensity (a) Expenditure needed for external consultants to prepare the request to the Tax Authority for an agreed prior assessment of expenditure attributable to R&D and TI activities.
(b) Expenditure needed for external consultants to prepare the request to the Tax Authority for binding consultations on the nature of the R&D and TI element of the projects.
(c) Expenditure needed for external consultants to prepare the request to the Ministry of Industry, Tourism and Trade, or to its satellite agencies, for the reasoned report relating to the R&D and TI content of activities, and expenditure and investment relating to such activities.
(d) External expenditure linked to subcontracting the technical report on the qualification of the activities and identification of R&D and TI-related expenditure and investment to a body duly accredited by the National Accreditation Body (ENAC). Such certification must take place in accordance with standard UNE 166001.
(e) Expenditure needed for external consultants to prepare the request for the technical report on qualification mentioned in paragraph (d)
Date of implementation : 3 January 2005
Duration of aid scheme or individual aid award : 31 December 2006
Objective of aid (a) Preparation of the request to the Tax Authority for an agreed prior assessment of expenditure attributable to R&D and TI.
(b) Preparation of the request to the Tax Authority for binding consultations on the nature of the R&D and TI element of the projects.
(c) Preparation of the request to the Ministry of Industry, Tourism and Trade, or to its satellite agencies, for the reasoned report relating to the R&D and TI content of activities, and expenditure and investment relating to such activities.
(d) Preparation of the request to a body accredited by ENAC for the technical report on the qualification of the activities and identification of R&D and TI-related expenditure and investment.
Also considered eligible is external expenditure linked to subcontracting the technical report on the qualification of the activities and identification of R&D and TI-related expenditure and investment to a body duly accredited by the National Accreditation Body (ENAC)
Sector(s) concerned : SMEs active in the production, processing or marketing of agricultural products, except for the processing of agricultural products listed in Annex I to the EC Treaty into products not listed therein
Name and address of the granting authority ADE — Agencia de Desarrollo Económico de Castilla y León
C/ Duque de la Victoria 23
E-47001 Valladolid
Web address : www.jcyl.es/ade
Aid No : XA 24/06
Member State : Spain
Region : La Rioja
Title of aid scheme or name of company receiving an individual aid : Rules governing the aid scheme relating to the programme "New developments in information and communications technology for SMEs"
Legal basis : Resolución del Presidente de la Agencia de Desarrollo Económico de La Rioja de 17 de marzo de 2006 por la que se aprueban las Bases Reguladoras del régimen de ayudas del programa Nuevas Tecnologías de la Información y las Comunicaciones para Pymes
Annual expenditure planned under the scheme or overall amount of individual aid granted to the company : EUR 2000000
Maximum aid intensity : 40 % of eligible investment
Date of implementation : From 23 March 2006
Duration of aid scheme or individual aid award : 2006
Objective of aid E-commerce: introducing systems for selling products and providing customer services electronically.
E-business: introducing integrated management systems.
E-marketing: introducing systems which optimise relations with clients.
E-marketplaces: introducing systems which enable the enterprise to participate in platforms facilitating electronic transactions between buyers and sellers.
E-procurement: introducing systems for the enterprise to manage the purchase of products and services electronically.
The enterprise's web site, provided that, in addition to providing information on the enterprise, it offers the possibility of conducting business transactions.
Enterprise intranet and/or extranet which optimise communication between employees and between the enterprise and its clients/suppliers.
Internet security systems
Sector(s) concerned : SMEs (processing and marketing) with a head office in the Autonomous Community of Rioja, or at least having a centre of activity there
Name and address of the granting authority Agencia de Desarrollo Económico de La Rioja
Muro de la Mata 13-14
E-26071 Logroño (La Rioja)
Web address : www.ader.es
Aid Number : XA 37/06
Member State : France
Region : Lower Normandy
Title of aid scheme : Aid for animal welfare and on-farmenvironment
Legal basis - Article 4 of Commission Regulation (EC) No 1/2004 of 23 December 2003,
- Articles L 1511 et suivants du Code général des collectivités territoriales permettant aux collectivités territoriales d'intervenir dans le cadre de la création ou de l'extension d'activités économiques en accordant des aides directes et indirectes aux entreprises;
- Délibération de l'assemblée régionale en date du 11 février 2005
Annual expenditure planned under the scheme : EUR 385000
Maximum aid intensity - 40 % of eligible investments for equipment to improve working conditions with animals (containment), building landscaping or to reduce odours in spreading
- 30 % for aid to reduce feeding costs (targeted investments for bi-phase or multi-phase feeding) or to manage covered spaces used for livestock (better indoor conditions to improve animal health and welfare)
Date of implementation : On receipt of an acknowledgement of receipt from the European Commission
Duration of scheme : Two years, renewable
Objective of aid :
The scheme aims to improve on-farm production conditions and to maintain and improve the environment and animal health and welfare. It is limited to cases where an umbrella programme cannot be put together in the near future (two years) to form part of the modernisation scheme for livestock buildings ("PMBE") and cases that do not qualify for the building scheme due to multiple applications.
No proportion of investments to increase production capacity may be included in the application. In no cases may it involve work merely to meet the standards set under EU legislation governing health and safety, environment or animal welfare.Only new equipment and first-use buildings are eligible. Replacements and second-hand equipment are not eligible for aid.
Examples of eligible investments: round-up area, fixed or adjustable containment corridors, reinforcing buildings, landscaping by planting flower beds or hedges, improving access, bi- or multi-phase feeding equipment, spreading and burying equipment.
Sector(s) concerned : All livestock farms in Lower Normandy
Name and address of the granting authority Monsieur le Président du Conseil régional de Basse-Normandie
Abbaye aux Dames
BP 523
F-14035 Caen CEDEX
Web address : www.region-basse-normandie.fr
Aid No : XA 40/06
Member State : Italy
Region : Liguria
Title of aid scheme or name of company receiving individual aid : Aid for the modernisation of agricultural holdings run by cooperatives, and aid for the processing and marketing of agricultural products and service activities
Legal basis : Articoli 4, 5 e 6 della legge regionale 19 aprile 2006, n. 9 "Interventi strutturali a favore delle cooperative agricole"
Annual expenditure planned under the scheme or overall amount of individual aid granted to the company : The total budget allocation for 2006 is EUR 400000. For the following years, a similar amount is expected to be available for the abovementioned Regional Law
Maximum aid intensity - for the aid referred to in Article 4 — Aid for the modernisation of agricultural holdings run by cooperatives -, a financial contribution will be granted amounting to 50 % of eligible costs (excluding VAT) for investments in less-favoured areas as defined in the Regional Rural Development Plan, adopted by Commission Decision No C(2000) 2727 of 26 September 2000, and 40 % of eligible costs (excluding VAT) for investments in other areas;
- for the aid referred to in Article 5 — Aid for the processing and marketing of agricultural products — a financial contribution of 40 % of eligible costs (excluding VAT) will be granted;
- aid referred to in Article 6 — Aid for activities relating to the collective purchase of production means and the purchase of machinery and equipment for collective use for agricultural production — will be granted under the 'de minimis' rule, pursuant to Article 2 of Regulation (EC) No 69/2001
Date of implementation : Starting from 2006, but not earlier than ten working days after the submission of this form, as required by Article 19(1) of Regulation (EC) No 1/2004
Duration of scheme : Indefinite
Objective of aid (a) modernisation of agricultural holdings run by cooperatives;
(b) processing and marketing of agricultural products;
(c) collective purchase of means of production; and
(d) purchase of machinery and equipment for agricultural production to be used collectively
Reference Articles of Regulation (EC) No 1/2004 : Article 4 "Investments in agricultural holdings" and Article 7 "Investments in processing and marketing"
Sector(s) concerned : The aid scheme applies to all agricultural and agri-food products listed in Annex I to the Treaty, excluding fishery and aquaculture products
Name and address of the authority granting the aid Regione Liguria — Dipartimento Agricoltura e Protezione civile
Via G. D'Annunzio 113
I-16121 Genova
Website : http://www.agriligurianet.it/cgi-bin/liguria/agrinet3/ep/linkPagina.do?canale=/Portale Agrinet 3/Home/AttivitaRegionali/<! - - 0030 - - >sostegno economico/<! - - 0050 - - >contributi alle coop agricole
--------------------------------------------------
