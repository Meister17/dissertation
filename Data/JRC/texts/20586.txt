COMMISSION REGULATION (EC) No 3330/94 of 21 December 1994 on the tariff classification of certain poultry cuts and amending Regulation (EEC) No 2658/87 on the tariff and statistical nomenclature and on the Common Customs Tariff
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2777/75 of 29 October 1975 on the common organization of the market in poultrymeat (1), as last amended by Regulation (EEC) No 1574/93 (2), and in particular Articles 3 and 5 (3) thereof,
Whereas it has been found that classification of certain poultry cuts presents problems resulting from the absence of precise definitions in the tariff and statistical nomenclature set up by Council Regulation (EEC) No 2658/87 (3) as last amended by Commission Regulation (EEC) No 3115/94 (4); whereas these definitions should be laid down in order to assure uniform application of levies in the poultrymeat sector;
Whereas in application of Article 11 (1) of Regulation (EEC) No 2777/75 the tariff nomenclature applicable as a result of that Regulation is set out in the Combined Nomenclature; whereas it should therefore be amended;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Poultrymeat and Eggs,
HAS ADOPTED THIS REGULATION:
Article 1
For the purposes of applying levies in the poultrymeat sector, the following expressions shall have the meanings hereunder assigned to them:
1. 'Halves', for the purposes of subheadings 0207 39 13, 0207 39 33, 0207 39 57, 0207 39 61, 0207 39 63, 0207 41 11, 0207 42 11, 0207 43 21, 0207 43 23 and 0207 43 25: halves of poultry carcases, obtained by a longitudinal cut in a plane along the sternum and the backbone;
2. 'Quarters', for the purposes of subheadings 0207 39 13, 0207 39 33, 0207 39 57, 0207 39 61, 0207 39 63, 0207 41 11, 0207 43 21, 0207 43 23 and 0207 43 25: leg quarters, or breast quarters, obtained by a transversal out of a half;
3. 'Whole wings, with or without tips', for the purposes of subheadings 0207 39 15, 0207 39 35, 0207 39 65, 0207 41 21, 0207 42 21, 0207 43 31: poultry cuts consisting of the humerus, radius and ulna, together with the surrounding musculature. The tip, including the carpal bones, may or may not have been removed. The cuts shall be made at the joints;
4. 'Breasts', for the purposes of subheadings 0207 39 21, 0207 39 41, 0207 39 71, 0207 39 73, 0207 41 41, 0207 42 41, 0207 43 51 and 0207 43 53: poultry cuts consisting of the sternum and the ribs, distributed on both sides of it, together with the surrounding musculature;
5. 'Legs', for the purposes of subheadings 0207 39 23, 0207 39 75, 0207 39 77, 0207 41 51, 0207 43 61 and 0207 43 63: poultry cuts consisting of the femur, tibia and fibula together with the surrounding musculature. The two cuts shall be made at the joints;
6. 'Turkey drumsticks', for the purposes of subheadings 0207 39 43, 0207 42 51: turkey cuts consisting of the tibia and fibula together with the surrounding musculature. The two cuts shall be made at the joints;
7. 'Other turkey legs', for the purposes of subheadings 0207 39 45 and 0207 42 59: turkey cuts consisting of the femur together with the surrounding musculature or of the femur, tibia and fibula together with the surrounding musculature. The two cuts shall be made at the joints.
Article 2
Chapter 2 of Annex I to Regulation (EEC) No 2658/87 is hereby amended as follows:
1. Additional note 4 shall be replaced by the following:
'4. The following expressions shall have the meanings hereunder assigned to them:
(a) "Poultry cuts, with bone in", for the purposes of subheadings 0207 39 13 to 0207 39 23, 0207 39 33 to 0207 39 45, 0207 39 57 to 0207 39 77, 0207 41 11 to 0207 41 51, 0207 42 11 to 0207 42 59 and 0207 43 21 to 0207 43 63: the cuts specified therein, including all bones.
Poultry cuts as referred in (a) which have been partly boned shall fall within subheading 0207 39 25, 0207 39 47, 0207 39 83, 0207 41 71 or 0207 43 81.
(b) "Halves", for the purposes of subheadings 0207 39 13, 0207 39 33, 0207 39 57, 0207 39 61, 0207 39 63, 0207 41 11, 0207 42 11, 0207 43 21, 0207 43 23 and 0207 43 25: halves of poultry carcases, obtained by a longitudinal cut in a plane along the sternum and the backbone;
(c) "Quarters", for the purposes of subheadings 0207 39 13, 0207 39 33, 0207 39 57, 0207 39 61, 0207 39 63, 0207 41 11, 0207 42 11, 0207 43 21, 0207 43 23 and 0207 43 25: leg quarters, or breast quarters, obtained by a transversal cut of a half;
(d) "Whole wings, with or without tips", for the purposes of subheadings 0207 39 15, 0207 39 35, 0207 39 65, 0207 41 21, 0207 42 21, 0207 43 31: poultry cuts consisting of the humerus, radius and ulna, together with the surrounding musculature. The tip, including the carpal bones, may or may not have been removed. The cuts shall be made at the joints;
(e) "Breasts", for the purposes of subheadings 0207 39 21, 0207 39 41, 0207 39 71, 0207 39 73, 0207 41 41, 0207 42 41, 0207 43 51 and 0207 43 53: poultry cuts consisting of the sternum and the ribs, distributed on both sides of it, together with the surrounding musculature;
(f) "Legs", for the purposes of subheadings 0207 39 23, 0207 39 75, 0207 39 77, 0207 41 51, 0207 43 61 and 0207 43 63: poultry cuts consisting of the femur, tibia and fibula together with the surrounding musculature. The two cuts shall be made at the joints;
(g) "Turkey drumsticks", for the purposes of subheadings 0207 39 43 and 0207 42 51: turkey cuts consisting of the tibia and fibula together with the surrounding musculature. The two cuts shall be made at the joints;
(h) "Other turkey legs", for the purposes of subheadings 0207 39 45 and 0207 42 59: turkey cuts consisting of the femur together with the surrounding musculature or of the femur, tibia and fibula together with the surrounding musculature. The two cuts shall be made at the joints.
(i) "Goose or duck paletots", for the purposes of subheadings 0207 39 81 and 0207 43 71: geese or ducks plucked and completely drawn, without heads or feet, with carcase bones (breastbone, ribs, backbone and sacrum) removed but with the femurs, tibias and humeri.'
2. Additional note 7 shall be deleted. Additional note 8 shall become additional note 7.
Article 3
This Regulation shall enter into force on 1 january 1995.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 21 December 1994.
For the Commission
René STEICHEN
Member of the Commission
(1) OJ No L 282, 1. 11. 1975, p. 77.
(2) OJ No L 152, 24. 6. 1993, p. 1.
(3) OJ No L 256, 7. 9. 1987, p. 1.
(4) OJ No L 345, 31. 12. 1994, p. 1.
