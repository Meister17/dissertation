Commission Decision
of 3 October 2003
on the procedure for attesting the conformity of construction products pursuant to Article 20(2) of Council Directive 89/106/EEC as regards metal frame building kits, concrete frame building kits, prefabricated building units, cold storage room kits and rock-fall protection kits
(notified under document number C(2003) 3452)
(Text with EEA relevance)
(2003/728/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 89/106/EEC of 21 December 1988 on the approximation of laws, regulations and administrative provisions of the Member States relating to construction products(1), as amended by Directive 93/68/EEC(2), and in particular Article 13(4) thereof,
Whereas:
(1) The Commission is required to select, between the two procedures under Article 13(3) of Directive 89/106/EEC for attesting the conformity of a product, the least onerous possible procedure consistent with safety. Whereas this means that it is necessary to decide whether, for a given product or family of products, the existence of a factory production control system under the responsibility of the manufacturer is a necessary and sufficient condition for an attestation of conformity, or whether, for reasons related to compliance with the criteria mentioned in Article 13(4), the intervention of an approved certification body is required.
(2) Article 13(4) requires that the procedure thus determined shall be indicated in the mandates and in the technical specifications. Therefore, it is desirable to define the concept of products or family of products as used in the mandates and in the technical specifications.
(3) The two procedures provided for in Article 13(3) are described in detail in Annex III to Directive 89/106/EEC. It is necessary therefore to specify clearly the methods by which the two procedures shall be implemented, by reference to Annex III, for each product or family of products, since Annex III gives preference to certain systems.
(4) The procedure referred to in Article 13(3)(a) corresponds to the systems set out in the first possibility, without continuous surveillance, and the second and third possibilities of Annex III(2)(ii). The procedure referred to in Article 13(3)(b) corresponds to the systems set out in Annex III(2)(i), and in the first possibility, with continuous surveillance, of III(2)(ii).
(5) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on Construction,
HAS ADOPTED THIS DECISION:
Article 1
The products and families of products set out in Annex I shall have their conformity attested by a procedure whereby, in addition to a factory production control system operated by the manufacturer, an approved certification body is involved in assessment and surveillance of the production control and the product itself.
Article 2
The procedure for attesting conformity as set out in Annex II shall be indicated in the mandates for Guidelines for European technical approvals.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 3 October 2003.
For the Commission
Erkki Liikanen
Member of the Commission
(1) OJ L 40, 11.2.1989, p. 12.
(2) OJ L 220, 30.8.1993, p. 1.
ANNEX I
1. Metal frame building kits
This Decision covers those industrially prepared kits, marketed as a building, that are made of predesigned and prefabricated components intended for production in series. This Decision covers only those kits for which the minimum requirements defined below are met. Partial kits falling below these minimum requirements are outside the scope of this Decision. The minimum requirements comprise all of the following: the structural elements of the building, the connection of the building to the substructure and the specification of the essential components of the external envelope as thermal insulation, cladding, roof covering, internal lining, windows and external doors in so far as they are necessary for the satisfaction of the essential requirements applied to the building.
Although some components may be prepared in different factories, only the final kit for delivery, and not the different components, are considered by this Decision.
- for use in building works
2. Concrete frame building kits
This Decision covers those industrially prepared kits, marketed as a building, that are made of predesigned and prefabricated components intended for production in series. This Decision covers only those kits for which the minimum requirements defined below are met. Partial kits falling below these minimum requirements are outside the scope of this Decision. The minimum requirements comprise all of the following: the structural elements of the building, the connection of the building to the substructure and the specification of the essential components of the external envelope as thermal insulation, cladding, roof covering, internal lining, windows and external doors in so far as they are necessary for the satisfaction of the essential requirements applied to the building.
Although some components may be prepared in different factories, only the final kit for delivery, and not the different components, are considered by this Decision.
- for use in building works
3. Prefabricated building units
This Decision covers prefabricated building units capable of being transported to site in self-contained or volumetric format and rapidly providing a weatherproof envelope, possibly subject to final weatherproofing, jointing between units, connection to services and any foundation connections.
Although some components may be prepared in different factories, only the final kit for delivery, and not the different components, are considered by this Decision.
- for use in building works
4. Cold storage room kits
This Decision covers prefabricated cold storage room kits for installation inside an existing building or at least protected from external climate exposure, i.e. the cold storage room kits are not exposed to an external climate. The assembled kits do not contribute to the load-bearing capacity of the works, but a load-bearing supporting system may be foreseen, to support the whole assembled kit or parts of it. The technical equipment (e.g. cooling systems) is excluded.
Although some components may be prepared in different factories, only the final kit for delivery, and not the different components, are considered by this Decision.
- for use in building works
5. Rock-fall protection kits
This Decision covers rock-fall protection kits consisting of a single or multiple net or mesh or similar, supported by a metallic or wooden structure (e.g. metal posts) and, possibly, cables.
Although some components may be prepared in different factories, only the final kit for delivery, and not the different components, are considered by this Decision.
- for use in civil engineering works
ANNEX II
Systems of attestation of conformity
For the product(s) and intended use(s) listed below, EOTA is requested to specify the following system(s) of attestation of conformity in the relevant guideline for European technical approvals:
>TABLE>
System 1: see Directive 89/106/EEC Annex III.2.(i), without audit-testing of samples.
The specification for the system should be such that it can be implemented even where performance does not need to be determined for a certain characteristic, because at least one Member State has no legal requirement at all for such characteristic (see Article 2(1) of Directive 89/106/EEC and, where applicable, clause 1.2.3 of the interpretative document).
In those cases the verification of such a characteristic must not be imposed on the manufacturer if he does not wish to declare the performance of the product in that respect.
