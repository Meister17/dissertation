Action brought on 7 February 2006 — MEGGLE Aktiengesellschaft v Office for Harmonisation in the Internal Market (Trade Marks and Designs)
Parties
Applicant: MEGGLE Aktiengesellschaft (Wasserburg a. Inn, Germany) (represented by: T. Rabb, lawyer)
Defendant: Office for Harmonisation in the Internal Market (Trade Marks and Designs)
Other party to the proceedings before the Board of Appeal: Clover Corporation Limited (North Sydney, Australia)
Form of order sought
The applicant claims that the Court should:
- annul the decision of the Second Board of Appeal of the Office for Harmonisation in the Internal Market (Trade Marks and Designs) of 22 November 2005 and the opposition decision of the Opposition Division of the Office with competence for trade marks of 30 September 2004;
- order the defendant to pay the costs.
Pleas in law and main arguments
Applicant for Community trade mark: Clover Corporation Limited
Community trade mark sought: Figurative mark "HiQ with cloverleaf" for goods in Classes 5, 29 and 30 (No 2171114)
Proprietor of mark or sign cited in the opposition proceedings: The applicant
Mark or sign cited in opposition: the German figurative mark "Cloverleaf" for goods in Classes 1, 3, 5, 29, 30, 31, 32 and 33 (No 980458) and the German figurative mark "Cloverleaf" for goods in Classes 1, 3, 5, 29, 30, 31, 32, 33 (No 39652600)
Decision of the Opposition Division: Rejection of the opposition
Decision of the Board of Appeal: Dismissal of the appeal
Pleas in law: Article 8(1)(b) of Regulation (EC) No 40/94 [1] has been incorrectly applied, on the ground that there exists a likelihood of confusion between the opposing marks. The marks show a high degree of similarity and the earlier mark has particular distinctive character. Article 74(1) of Regulation No 40/94 has been breached, on the ground that the defendant Office is in breach of its duty to examine the facts before it.
[1] Council Regulation (EC) No 40/94 of 20 December 1993 on the Community trade mark.
--------------------------------------------------
