Commission Regulation (EC) No 1043/2006
of 7 July 2006
setting the actual production of olive oil and the unit amount of the production aid for the 2004/2005 marketing year
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation No 136/66/EEC of 22 September 1966 on the establishment of a common organisation of the market in oils and fats [1], and in particular Article 5 thereof,
Having regard to Council Regulation (EEC) No 2261/84 of 17 July 1984 laying down general rules on the granting of aid for the production of olive oil and of aid to olive oil producer organisations [2], and in particular Article 17a(2) thereof,
Whereas:
(1) Under Article 5 of Regulation No 136/66/EEC, the unit production aid must be adjusted in each Member State where actual production exceeds the guaranteed national quantity referred to in paragraph 3 of that Article. With a view to assessing the extent of the overrun in Greece, Spain, France, Italy and Portugal, account should be taken of the estimates for the production of table olives, expressed as olive-oil equivalent using the relevant coefficients referred to, in the case of Greece, in Commission Decision 2001/649/EC [3], in the case of Spain, in Commission Decision 2001/650/EC [4], in the case of France, in Commission Decision 2001/648/EC [5], in the case of Italy, in Commission Decision 2001/658/EC [6] and in the case of Portugal, in Commission Decision 2001/670/EC [7].
(2) Article 17a(1) of Regulation (EEC) No 2261/84 provides that, in order to determine the unit amount of the production aid for olive oil that can be paid in advance, the estimated production for the marketing year concerned should be determined. That amount must be set at a level that rules out any risk of undue payment to olive growers. The amount also applies to table olives, expressed as olive-oil equivalent. For the 2004/05 marketing year, the estimated production and the unit amount of the production aid that can be paid in advance were laid down in Commission Regulation (EC) No 1709/2005 [8].
(3) In order to determine the actual production for which entitlement to aid is recognised, the individual Member States concerned must inform the Commission no later than 15 May following each marketing year of the quantity on which the aid is payable in that Member State, in accordance with Article 14(4) of Commission Regulation (EC) No 2366/98 [9]. According to that information, the quantity on which the aid is payable for the 2004/05 marketing year is 484598 tonnes for Greece, 1107906 tonnes for Spain, 3107 tonnes for France, 951528 tonnes for Italy, 45296 tonnes for Portugal and 26 tonnes for Slovenia.
(4) Confirmation by the Member States that aid is payable on those quantities implies that the controls referred to in Regulations (EEC) No 2261/84 and (EC) No 2366/98 have been carried out. However, setting actual production on the basis of information from the Member States on the quantities on which aid is payable does not prejudge the conclusions that may be drawn from verification of the accuracy of that information under the accounts clearance procedure.
(5) Taking account of the actual production figures, the unit amount of the production aid provided for in Article 5(1) of Regulation No 136/66/EEC payable on the eligible quantities of actual production should also be set.
(6) For Slovenia, the unit amount of production aid set by this Regulation results from the application in 2005 of the percentage referred to in Article 143a of Council Regulation (EC) No 1782/2003 [10], which established common rules for direct support schemes under the common agricultural policy and certain support schemes for farmers.
(7) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Olive Oil and Table Olives,
HAS ADOPTED THIS REGULATION:
Article 1
1. For the 2004/2005 marketing year, the actual production to be used to calculate the aid for olive oil as referred to in Article 5 of Regulation No 136/66/EEC shall be:
- 484598 tonnes for Greece,
- 1107906 tonnes for Spain,
- 3107 tonnes for France,
- 951528 tonnes for Italy,
- 45296 tonnes for Portugal,
- 26 tonnes for Slovenia.
2. For the 2004/2005 marketing year, the unit amount of the production aid referred to in Article 5 of Regulation No 136/66/EEC payable on the eligible quantities of actual production shall be:
- 130,27 EUR/100 kg for Greece,
- 90,53 EUR/100 kg for Spain,
- 132,25 EUR/100 kg for France,
- 73,93 EUR/100 kg for Italy,
- 132,25 EUR/100 kg for Portugal,
- 39,68 EUR/100 kg for Slovenia.
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 7 July 2006.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ 172, 30.9.1966, p. 3025/66. Regulation as last amended by Regulation (EC) No 865/2004 (OJ L 161, 30.4.2004, p. 97). Corrected by OJ L 206, 9.6.2004, p. 37).
[2] OJ L 208, 3.8.1984, p. 3. Regulation as last amended by Regulation (EC) No 1639/1998 (OJ L 210, 28.7.1998, p. 38).
[3] OJ L 229, 25.8.2001, p. 16. Decision as last amended by Decision 2004/607/EC (OJ L 274, 24.8.2004, p. 13).
[4] OJ L 229, 25.8.2001, p. 20. Decision as last amended by Decision 2004/607/EC.
[5] OJ L 229, 25.8.2001, p. 12. Decision as last amended by Decision 2004/607/EC.
[6] OJ L 231, 29.8.2001, p. 16. Decision as last amended by Decision 2004/607/EC.
[7] OJ L 235, 4.9.2001, p. 16. Decision as last amended by Decision 2004/607/EC.
[8] OJ L 274, 20.10.2005, p. 11.
[9] OJ L 293, 31.10.1998, p. 50. Regulation as last amended by Regulation (EC) No 1795/2005 (OJ L 288, 29.10.2005, p. 40).
[10] OJ L 270, 21.10.2003, p. 1. Regulation as last amended by Regulation (EC) No 319/2006 (OJ L 58, 28.2.2006, p. 32).
--------------------------------------------------
