*****
COUNCIL REGULATION (EEC) No 3488/89
of 21 November 1989
laying down the method of decision for certain provisions laid down for agricultural products in the framework of Mediterranean agreements
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the Community, and in particular Article 113 thereof,
Having regard to the proposal from the Commission,
Whereas, in accordance with:
- Article 20 (5) of the Cooperation Agreement between the Community and Algeria (1), as amended by the Additional Protocol of 25 June 1987 (2),
- Article 21 (2) of the Protocol of 19 October 1987 laying down the conditions and procedures for the implementation of the second stage of the Agreement establishing an Association between the Community and Cyprus (3),
- Article 21 (5) of the Cooperation Agreement between the Community and Morocco (4), as amended by the Additional Protocol of 26 May 1988 (5),
- Article 20 (5) of the Cooperation Agreement between the Community and Tunisia (6), as amended by the Additional Protocol of 26 May 1987 (7), and
- Article 22 (7) of the Cooperation Agreement between the Community and Yugoslavia (8), as amended by the Additional Protocol of 10 December 1987 establishing new trade arrangements (9),
for wine of fresh grapes covered by CN code ex 2204 put up in containers of a content exceeding two litres, the Community may decide to fix a special frontier price under certain conditions;
Whereas, in accordance with:
- Article 20 (1) of the Protocol of 19 October 1987 laying down the conditions and procedures for the implementation of the second stage of the Agreement establishing an Association between the Community and Cyprus,
- Article 3 (1) of the Additional Protocol of 25 June 1987 to the Cooperation Agreement between the Community and Egypt (10),
- Article 3 (1) of the Additional Protocol of 15 December 1987 to the Agreement between the Community and Israel (11),
- Article 3 (1) of the Additional Protocol of 26 May 1988 to the Cooperation Agreement between the Community and Morocco,
- Article 2 (1) of the Additional Protocol of 26 May 1987 to the Cooperation Agreement between the Community and Tunisia, and
- Article 1 (1) of the Supplementary Protocol of 23 July 1987 to the Agreement establishing an Association between the Community and Turkey (12),
the Community may decide on an adjustment of the entry price for certain fruit and vegetables originating in those countries under certain conditions;
Whereas the abovementioned decisions of the Community should be taken by the Commission in accordance with the relevant management committee procedure,
HAS ADOPTED THIS REGULATION:
Article 1
For wine of fresh grapes falling within CN code ex 2204 29, as mentioned below, put up in containers of a content exceeding two litres, originating in the following countries and within the limits of the quantities allocated to each country, the fixing of any special frontier price in accordance with the Protocols concluded with those countries and in compliance with the conditions, shall be carried out by the Commission in accordance with the procedure laid down in Article 83 of Council Regulation (EEC) No 822/87 (13):
(a) wine of fresh grapes of an actual alcoholic strength not exceeding 15 % vol
1.2 // // // Country // Quantity // // // Algeria // 160 000 hl // Cyprus // 26 000 hl // Morocco // 75 000 hl // Tunisia // 150 000 hl // Yugoslavia // 516 000 hl // //
(b) liqueur wine of an actual alcoholic strength of not less than 15 % vol
1.2 // // // Country // Quantity // // // Cyprus 2. 1988, p. 91. (13) OJ No L 84, 27. 3. 1987, p. 1.
Article 2
For 1990 and each following marketing year, any adjustment of the entry price for the fruit and vegetables listed below and originating in one of the countries indicated below, in order to maintain traditional trade flows in the context of enlargement, shall be carried out by the Commission in accordance with the procedure laid down in Article 33 of Council Regulation (EEC) No 1035/72 (1) on the basis of the statistical review and all the relevant factors provided for in the Protocol concluded with the country concerned and in compliance with the conditions and quantities laid down in that Protocol:
1.2.3.4 // // // // // CN code // Product // Quantity (in tonnes) // Country // // // // // // // // // 0805 10 11 to 0805 10 49 // Fresh oranges // 67 000 7 000 293 000 265 000 28 000 // Cyprus Egypt Israel Morocco Tunisia // ex 0805 20 10 ex 0805 20 30 ex 0805 20 50 ex 0805 20 70 ex 0805 20 90 // Fresh, mandarins, clementines, etc // 14 200 110 000 // Israel Morocco // ex 0805 30 10 // Fresh lemons // 15 000 6 400 12 000 // Cyprus Israel Turkey // ex 0806 10 11 ex 0806 10 15 ex 0806 10 59 // Fresh table grapes from 8 June to 4 August // 10 500 // Cyprus // ex 0702 00 10 ex 0702 00 90 // Tomatoes // 86 000 // Morocco // // including: April May // 15 000 10 000 // // // // //
Article 3
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 21 November 1989.
For the Council
The President
J. PELLETIER // 73 000 hl // //
(1) OJ No L 263, 27. 9. 1978, p. 2. (2) OJ No L 297, 21. 10. 1987, p. 2. (3) OJ No L 393, 31. 12. 1987, p. 2. (4) OJ No L 264, 27. 9. 1978, p. 2. (5) OJ No L 224, 13. 8. 1988, p. 18. (6) OJ No L 265, 27. 9. 1978, p. 2. (7) OJ No L 297, 21. 10. 1987, p. 36. (8) OJ No L 41, 14. 2. 1983, p. 2. (9) OJ No L 389, 31. 12. 1987, p. 73. (10) OJ No L 297, 21. 10. 1987, p. 11. (11) OJ No L 327, 30. 11. 1988, p. 35. (12) OJ No L 53, 27.
(1) OJ No L 118, 20. 5. 1972, p. 1.
