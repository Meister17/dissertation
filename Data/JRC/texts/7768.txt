*****
COUNCIL REGULATION (EEC) No 1708/87
of 15 June 1987
concerning the conclusion of the Agreement between the European Economic Community and the Republic of Seychelles on fishing off Seychelles
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community and in particular Article 43 thereof,
Having regard to the Act of Accession of Spain and Portugal, and in particular Article 167 (3) thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opnion of the European Parliament,
Whereas the Agreement between the Republic of Seychelles and the European Economic Community on fishing off Seychelles, signed at Brussels on 23 May 1985 (2), has been denounced by the Republic of Seychelles at the end of its first three-year period of application;
Whereas, pursuant to Article 12 of the Agreement, the Community and the Republic of Seychelles have conducted negotiations in order to determine the necessary amendments to the Agreement;
Whereas, as a result of those negotiations, a new Agreement was initialled on 3 December 1986; whereas it is in the Community's interest to approve this Agreement;
Whereas, as a result of those negotiations, a new Agreement was initialled on 3 December 1986; whereas it is in the Community's interest to approve this Agreement,
HAS ADOPTED THIS REGULATION:
Article 1
The Agreement between the European Economic Community and the Republic of Seychelles on fishing off Seychelles is hereby approved on behalf of the Community.
The text of the Agreement is attached to this Regulation.
Article 2
The President of the Council is hereby authorized to designate the persons empowered to sign the Agreement in order to bind the Community (3).
Article 3
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Luxembourg, 15 June 1987.
For the Council
The President
P. DE KEERSMAEKER
(1) OJ No C 81, 28. 3. 1987, p. 7.
(2) OJ No L 149, 8. 6. 1985, p. 1.
(3) The date of entry into force of the Agreement will be published in the Official Journal of the European Communities by the General Secretariat of the Council.
