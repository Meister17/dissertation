Decision of the European Central Bank
of 10 November 2006
on the annual accounts of the European Central Bank
(ECB/2006/17)
(2006/888/EC)
THE GOVERNING COUNCIL OF THE EUROPEAN CENTRAL BANK,
Having regard to the Statute of the European System of Central Banks and of the European Central Bank, and in particular to Article 26.2 thereof,
Whereas:
(1) Decision ECB/2002/11 of 5 December 2002 on the annual accounts of the European Central Bank [1] (ECB) requires significant amendment. From 1 January 2007 the European System of Central Banks (ESCB) will use the economic approach as defined in Guideline ECB/2006/16 of 10 November 2006 on the legal framework for accounting and financial reporting in the European System of Central Banks [2] to record foreign exchange transactions, financial instruments denominated in foreign currency and related accruals. It is desirable for reasons of clarity to recast the Decision in a single text.
(2) Decisions ECB/2002/11, ECB/2005/12 and ECB/2006/3 which are replaced by this Decision should be repealed,
HAS DECIDED AS FOLLOWS:
CHAPTER I
GENERAL PROVISIONS
Article 1
Definitions
1. For the purposes of this Decision, "Eurosystem" and "national central banks" (NCBs) shall have the same meaning as in Article 1 of Guideline ECB/2006/16.
2. Other technical terms used in this Decision shall have the same meaning as in Annex II to Guideline ECB/2006/16.
Article 2
Scope of application
The rules set out in this Decision shall apply to the annual accounts of the ECB comprising the balance sheet, items recorded in the books of the ECB off-balance sheet, the profit and loss account and the notes to the annual accounts of the ECB.
Article 3
Basic accounting assumptions
The basic accounting assumptions defined in Article 3 of Guideline ECB/2006/16 shall also apply for the purposes of this Decision.
Article 4
Recognition of assets and liabilities
A financial or other asset/liability shall only be recognised in the balance sheet of the ECB in accordance with Article 4 of Guideline ECB/2006/16.
Article 5
Economic and cash/settlement approaches
Article 5 of Guideline ECB/2006/16 shall apply to this Decision.
CHAPTER II
COMPOSITION AND VALUATION RULES FOR THE BALANCE SHEET
Article 6
Composition of the balance sheet
The composition of the balance sheet shall be based on the structure set out in Annex I.
Article 7
Provision for foreign exchange rate, interest rate and gold price risks
Taking into due consideration the nature of the ECB's activities, the Governing Council may establish a provision for foreign exchange rate, interest rate and gold price risks in the balance sheet of the ECB. The Governing Council shall decide on the size and use of the provision on the basis of a reasoned estimate of the ECB's risk exposures.
Article 8
Balance sheet valuation rules
1. Current market rates and prices shall be used for balance sheet valuation purposes unless specified otherwise in Annex I.
2. The revaluation of gold, foreign currency instruments, securities and financial instruments both on-balance-sheet and off-balance-sheet, shall be performed at the year-end at mid-market rates and prices.
3. No distinction shall be made between price and currency revaluation differences for gold, but a single gold revaluation difference shall be accounted for, based on the euro price per defined unit of weight of gold derived from the euro/US dollar exchange rate on the revaluation date. Revaluation shall take place on a currency-by-currency basis for foreign exchange, including on-balance-sheet and off-balance-sheet transactions, and on a code-by-code basis i.e. same ISIN number/type for securities, except for those securities included in the item "Other financial assets", which shall be treated as separate holdings.
Article 9
Reverse transactions
Reverse transactions shall be accounted for in accordance with Article 8 of Guideline ECB/2006/16.
Article 10
Marketable equity instruments
Marketable equity instruments shall be accounted for in accordance with Article 9 of Guideline ECB/2006/16. All points of Article 9(3) shall apply to the ECB.
CHAPTER III
INCOME RECOGNITION
Article 11
Income recognition
1. Article 11(1), (2), (3), (5) and (7) of Guideline ECB/2006/16 shall apply to income recognition.
2. Holdings on special revaluation accounts stemming from contributions according to Article 49.2 of the Statute with respect to central banks of Member States for which the derogation has been abrogated shall be used to offset unrealised losses when exceeding previous revaluation gains registered in the corresponding standard revaluation account as laid down by Article 11(1)(c) of Guideline ECB/2006/16, prior to the offsetting of such losses in accordance with Article 33.2 of the Statute. The holdings on special revaluation accounts for gold, for currencies and for securities shall be reduced pro rata in the event of a reduction in the holdings of the relevant assets.
Article 12
Cost of transactions
Article 12 of Guideline ECB/2006/16 shall apply to this Decision.
CHAPTER IV
ACCOUNTING RULES FOR OFF-BALANCE-SHEET INSTRUMENTS
Article 13
General rules
Article 13 of Guideline ECB/2006/16 shall apply to this Decision.
Article 14
Foreign exchange forward transactions
Foreign exchange forward transactions shall be accounted for in accordance with Article 14 of Guideline ECB/2006/16.
Article 15
Foreign exchange swaps
Foreign exchange swaps shall be accounted for in accordance with Article 15 of Guideline ECB/2006/16.
Article 16
Interest rate futures
Interest rate futures shall be accounted for in accordance with Article 16 of Guideline ECB/2006/16.
Article 17
Interest rate swaps
Interest rate swaps shall be accounted for in accordance with Article 17 of Guideline ECB/2006/16. Unrealised losses taken to the profit and loss account at the year-end shall be amortised in subsequent years according to the straight-line method.
Article 18
Forward rate agreements
Forward rate agreements shall be accounted for in accordance with Article 18 of Guideline ECB/2006/16.
Article 19
Forward transactions in securities
Forward transactions in securities shall be accounted for in accordance with Method A in Article 19(1) of Guideline ECB/2006/16.
Article 20
Options
Options shall be accounted for in accordance with Article 20 of Guideline ECB/2006/16.
CHAPTER V
ANNUAL PUBLISHED BALANCE SHEET AND PROFIT AND LOSS ACCOUNT
Article 21
Formats
1. The format for the ECB's published annual balance sheet shall follow the format in Annex II.
2. The format of the ECB's published profit and loss account shall comply with Annex III.
CHAPTER VI
FINAL PROVISIONS
Article 22
Development, application and interpretation of rules
1. The ESCB's Accounting and Monetary Income Committee (AMICO) shall report to the Governing Council, via the Executive Board, on the development, application and implementation of the ESCB's accounting and financial reporting rules.
2. In interpreting this Decision, account shall be taken of the preparatory work, the accounting principles harmonised by Community law and generally accepted international accounting standards.
3. If a specific accounting treatment is not laid down in this Decision and in the absence of a decision to the contrary by the Governing Council, the ECB shall follow valuation principles in accordance with International Accounting Standards as adopted by the European Union relevant to the activities and accounts of the ECB.
Article 23
Repeal
Decisions ECB/2002/11, ECB/2005/12 and ECB/2006/3 are hereby repealed. References to the repealed Decisions shall be construed as references to this Decision and shall be read in accordance with the correlation table in Annex IV.
Article 24
Final provision
This Decision shall enter into force on 1 January 2007.
Done at Frankfurt am Main, 10 November 2006.
The President of the ECB
Jean-Claude Trichet
[1] OJ L 58, 3.3.2003, p. 38. Decision as last amended by Decision ECB/2006/3 (OJ L 89, 28.3.2006, p. 56).
[2] See page … of this Official Journal.
--------------------------------------------------
ANNEX I
COMPOSITION AND VALUATION RULES FOR THE BALANCE SHEET
Note: the numbering relates to the balance sheet format given in Annex II.
ASSETS
Balance sheet item | Categorisation of contents of balance sheet items | Valuation principle |
1 | Gold and gold receivables | Physical gold (i.e. bars, coins, plates, nuggets) in storage or "under way". Non-physical gold, such as balances in gold sight accounts (unallocated accounts), term deposits and claims to receive gold arising from the following transactions: upgrading or downgrading transactions and gold location or purity swaps where there is a difference of more than one business day between release and receipt | Market value |
2 | Claim on non-euro area residents denominated in foreign currency | Claims on counterparties resident outside the euro area including international and supranational institutions and central banks outside the euro area denominated in foreign currency | |
2.1 | Receivables from the IMF | (a)Drawing rights within the reserve tranche (net)National quota minus balances in euro at the disposal of the IMF. The No 2 account of the IMF (euro account for administrative expenses) may be included in this position or under the item "Liabilities to non-euro area residents denominated in euro" | (a)Drawing rights within the reserve tranche (net)Nominal value, translation at the year-end foreign exchange market rate |
(b)Special drawing rightsHoldings of special drawing rights (gross) | (b)Special drawing rightsNominal value, translation at the year-end foreign exchange market rate |
(c)Other claimsGeneral arrangements to borrow (GAB), loans under special borrowing arrangements, deposits within the framework of the Poverty Reduction and Growth Facility (PRGF) | (c)Other claimsNominal value, translation at the year-end foreign exchange market rate |
2.2 | Balances with banks and security investments, external loans and other external assets | (a)Balances with banks outside the euro areaCurrent accounts, fixed-term deposits, day-to-day money, reverse repo transactions | (a)Balances with banks outside the euro areaNominal value, translation at the year-end foreign exchange market rate |
(b)Security investments other than equity shares, participations and other securities under asset item "Other financial assets" outside the euro areaMarketable notes and bonds, bills, zero bonds, money market paper, all issued by non-euro area residents | (b)Marketable securitiesMarket price and foreign exchange market rate at year-end |
(c)External loans (deposits)Loans to and non-marketable securities other than equity shares, participations and other securities under asset item "Other financial assets" issued by non-euro area residents | (c)External loansDeposits at nominal value, non-marketable securities at cost; both translated at the year-end foreign exchange market rate |
(d)Other external assetsNon-euro area banknotes and coins | (d)Other external assetsNominal value, translation at the year-end foreign exchange market rate |
3 | Claims on euro area residents denominated in foreign currency | (a)SecuritiesMarketable notes and bonds, bills, zero bonds, money market paper other than equity shares, participations and other securities under asset item "Other financial assets" | (a)Marketable securitiesMarket price and foreign exchange market rate at year-end |
(b)Other claimsNon-marketable securities other than equity shares, participations and other securities under asset item "Other financial assets", loans, deposits, reverse repo transactions, sundry lending | (b)Other claimsDeposits at nominal value, non-marketable securities at cost, both translated at the year-end foreign exchange market rate |
4 | Claims on non-euro area residents denominated in euro | | |
4.1 | Balances with banks, security investments and loans | (a)Balances with banks outside the euro areaCurrent accounts, fixed-term deposits, day-to-day money, reverse repo transactions in connection with the management of securities denominated in euro | (a)Balances with banks outside the euro areaNominal value |
(b)Security investments other than equity shares, participations and other securities under asset item "Other financial assets" outside the euro areaMarketable notes and bonds, bills, zero bonds, money market paper, all issued by non-euro area residents | (b)Marketable securitiesMarket price at year-end |
(c)Loans outside the euro areaLoans to and non-marketable securities issued by non-euro area residents | (c)Loans outside the euro areaDeposits at nominal value, non-marketable securities at cost |
(d)Securities issued by entities outside the euro areaSecurities issued by supranational or international organisations e.g. the EIB, irrespective of their geographical location | (d)Securities issued by entities outside the euro areaMarket price at year-end |
4.2 | Claims arising from the credit facility under ERM II | Lending according to the ERM II conditions | Nominal value |
5 | Lending to euro area credit institutions related to monetary policy operations denominated in euro | Items 5.1 to 5.5: transactions according to the respective monetary policy instruments described in Annex I to Guideline ECB/2000/7 of 31 August 2000 on monetary policy instruments and procedures of the Eurosystem [1] | |
5.1 | Main refinancing operations | Regular liquidity-providing reverse transactions with a weekly frequency and normally a maturity of one week | Nominal value or repo cost |
5.2 | Longer-term refinancing operations | Regular liquidity-providing reverse transactions with a monthly frequency and normally a maturity of three months | Nominal value or repo cost |
5.3 | Fine-tuning reverse operations | Reverse transactions, executed as ad hoc transactions for fine-tuning purposes | Nominal value or repo cost |
5.4 | Structural reverse operations | Reverse transactions adjusting the structural position of the Eurosystem vis-à-vis the financial sector | Nominal value or repo cost |
5.5 | Marginal lending facility | Overnight liquidity facility at a pre-specified interest rate against eligible assets (standing facility) | Nominal value or repo cost |
5.6 | Credits related to margin calls | Additional credit to credit institutions, arising from value increases of underlying assets regarding other credit to these credit institutions | Nominal value or cost |
6 | Other claims on euro area credit institutions denominated in euro | Current accounts, fixed-term deposits, day-to-day money, reverse repo transactions in connection with the management of security portfolios under the asset item "Securities of euro area residents denominated in euro", including transactions resulting from the transformation of former foreign currency reserves of the euro area, and other claims. Correspondent accounts with non-domestic euro area credit institutions. Other claims and operations unrelated to monetary policy operations of the Eurosystem | Nominal value or cost |
7 | Securities of euro area residents denominated in euro | Marketable securities other than equity shares, participations and other securities under asset item "Other financial assets": notes and bonds, bills, zero bonds, money market paper held outright including government securities stemming from before EMU denominated in euro; ECB debt certificates purchased for fine-tuning purposes | Market price at year-end |
8 | General government debt denominated in euro | Claims on government stemming from before EMU (non-marketable securities, loans) | Deposits/loans at nominal value, non-marketable securities at cost |
9 | Intra-Eurosystem claims | | |
9.1 | Claims related to promissory notes backing the issuance of ECB debt certificates | Only an ECB balance sheet item Promissory notes issued by NCBs, due to the back-to-back agreement in connection with ECB debt certificates | Nominal value |
9.2 | Claims related to the allocation of euro banknotes within the Eurosystem | Claims related to the ECB's banknote issue, according to Decision ECB/2001/15 of 6 December 2001 on the issue of euro banknotes [2] | Nominal value |
9.3 | Other claims within the Eurosystem (net) | Net position of the following sub-items: | |
(a)net claims arising from balances of TARGET accounts and correspondent accounts of NCBs i.e. the net figure of claims and liabilities - see also liability item "Other liabilities within the Eurosystem (net)" | (a)Nominal value |
(b)other intra-Eurosystem claims that may arise, including the interim distribution of ECB seigniorage income to NCBs | (b)Nominal value |
10 | Items in course of settlement | Settlement account balances (claims), including the float of cheques in collection | Nominal value |
11 | Other assets | | |
11.1 | Coins of euro area | Euro coins | Nominal value |
11.2 | Tangible and intangible fixed assets | Land and buildings, furniture and equipment including computer equipment, software | Cost less depreciation Depreciation is the systematic allocation of the depreciable amount of an asset over its useful life. The useful life is the period over which a fixed asset is expected to be available for use by the entity. Useful lives of individual material fixed assets may be reviewed on a systematic basis, if expectations differ from previous estimates. Major assets may comprise components with different useful lives. The lives of such components should be assessed individually Capitalisation of expenditure: limit based (below EUR 10000 excluding VAT: no capitalisation) |
11.3 | Other financial assets | Equity instruments, participating interests and investments in subsidiariesSecurities held as an earmarked portfolioFinancial fixed assetsReverse repo transactions with credit institutions in connection with the management of securities portfolios under this item | (a)Marketable equity instrumentsMarket value(b)Participating interests and illiquid equity sharesCost subject to impairment(c)Investment in subsidiaries or significant interestsNet asset value(d)Marketable securitiesMarket value(e)Non-marketable securitiesCost(f)Financial fixed assetsCost subject to impairmentPremiums/discounts are amortised For equity instruments, the detailed rules are set out in Article 10 of this Decision |
11.4 | Off-balance sheet instruments revaluation differences | Valuation results of foreign exchange forwards, foreign exchange swaps, interest rate swaps, forward rate agreements, forward transactions in securities, foreign exchange spot transactions from trade date to settlement date | Net position between forward and spot, at the foreign exchange market rate |
11.5 | Accruals and prepaid expediture | Income not due in, but assignable to the reported period. Prepaid expenditure and accrued interest paid [3] | Nominal value, foreign exchange translated at market rate |
11.6 | Sundry | (a)Advances, loans, other minor items. Loans on a trust basis | (a)Nominal value/cost |
(b)Investments related to customer gold deposits | (b)Market value |
(c)Net pension assets | (c)As per Article 22(3) |
12 | Loss for the year | | Nominal value |
LIABILITIES
Balance sheet item | Categorisation of contents of balance sheet items | Valuation principle |
1 | Banknotes in circulation | Euro banknotes issued by the ECB, according to Decision ECB/2001/15 | Nominal value |
2 | Liabilities to euro area credit institutions related to monetary policy operations denominated in euro | Items 2.1, 2.2, 2.3 and 2.5: deposits in euro as described in Annex I to Guideline ECB/2000/7 | |
2.1 | Current accounts (covering the minimum reserve system) | Euro accounts of credit institutions that are included in the list of financial institutions subject to minimum reserves according to the Statute. This item contains primarily accounts used in order to hold minimum reserves | Nominal value |
2.2 | Deposit facility | Overnight deposits at a pre-specified interest rate (standing facility) | Nominal value |
2.3 | Fixed-term deposits | Collection for liquidity absorption purposes owing to fine-tuning operations | Nominal value |
2.4 | Fine-tuning reverse operations | Monetary policy-related transactions with the aim of liquidity absorption | Nominal value or repo cost |
2.5 | Deposits related to margin calls | Deposits of credit institutions, arising from value decreases of underlying assets regarding credits to these credit institutions | Nominal value |
3 | Other liabilities to euro area credit institutions denominated in euro | Repo transactions in connection with simultaneous reverse repo transactions for the management of securities portfolios under asset item "Securities of euro area residents denominated in euro". Other operations unrelated to Eurosystem monetary policy operations. No current accounts of credit institutions | Nominal value or repo cost |
4 | ECB debt certificates issued | Only an ECB balance sheet item Debt certificates as described in Annex I to Guideline ECB/2000/7 Discount paper, issued with the aim of liquidity absorption | Nominal value |
5 | Liabilities to other euro area residents denominated in euro | | |
5.1 | General government | Current accounts, fixed-term deposits, deposits repayable on demand | Nominal value |
5.2 | Other liabilities | Current accounts of staff, companies and clients including financial institutions listed as exempt from the obligation to hold minimum reserves - see liability item 2.1, etc.; fixed-term deposits, deposits repayable on demand | Nominal value |
6 | Liabilities to non-euro area residents denominated in euro | Current accounts, fixed-term deposits, deposits repayable on demand including accounts held for payment purposes and accounts held for reserve management purposes: of other banks, central banks, international/supranational institutions including the Commission of the European Communities; current accounts of other depositors. Repo transactions in connection with simultaneous reverse repo transactions for the management of securities denominated in euro. Balances of TARGET accounts of central banks of Member States that have not adopted the euro | Nominal value or repo cost |
7 | Liabilities to euro area residents denominated in foreign currency | Current accounts. Liabilities under repo transactions; usually investment transactions using foreign currency assets or gold | Nominal value, translation at year-end foreign exchange market rate |
8 | Liabilities to non-euro area residents denominated in foreign currency | | |
8.1 | Deposits, balances and other liabilities | Current accounts. Liabilities under repo transactions; usually investment transactions using foreign currency assets or gold | Nominal value, translation at the year-end foreign exchange market rate |
8.2 | Liabilities arising from the credit facility under ERM II | Borrowing according to the ERM II conditions | Nominal value, translation at the year-end foreign exchange market rate |
9 | Counterpart of special drawing rights allocated by the IMF | SDR-denominated item which shows the amount of SDRs that were originally allocated to the respective country/NCB | Nominal value, translation at the year-end foreign exchange market rate |
10 | Intra-Eurosystem liabilities | | |
10.1 | Liabilities equivalent to the transfer of foreign reserves | ECB balance sheet item, denominated in euro | Nominal value |
10.2 | Other liabilities within the Eurosystem (net) | Net position of the following sub-items: | |
(a)net liabilities arising from balances of TARGET accounts and correspondent accounts of NCBs i.e. the net figure of claims and liabilities -see also asset item "Other claims within the Eurosystem (net)" | (a)Nominal value |
(b)other intra-Eurosystem liabilities that may arise, including the interim distribution of ECB income on euro banknotes to NCBs | (b)Nominal value |
11 | Items in course of settlement | Settlement account balances (liabilities), including the float of giro transfers | Nominal value |
12 | Other liabilities | | |
12.1 | Off-balance sheet instruments revaluation differences | Valuation results of foreign exchange forwards, foreign exchange swaps, interest rate swaps, forward rate agreements, forward transactions in securities, foreign exchange spot transactions from trade date to settlement date | Net position between forward and spot, at the foreign exchange market rate |
12.2 | Accruals and income collected in advance | Expenditure falling due in a future period but relating to the reporting period. Income received in the reported period but relating to a future period | Nominal value, foreign exchange translated at market rate |
12.3 | Sundry | (a)Taxation suspense accounts. Foreign currency credit or guarantee cover accounts. Repo transactions with credit institutions in connection with simultaneous reverse repo transactions for the management of securities portfolios under asset item "Other financial assets". Compulsory deposits other than reserve deposits. Other minor items. Liabilities on a trust basis | (a)Nominal value or (repo) cost |
(b)Customer gold deposits | (b)Market value |
(c)Net pension liabilities | (c)As per Article 22(3) |
13 | Provisions | For foreign exchange rate, interest rate and gold price risks, and for other purposes e.g. expected future expenses and contributions according to Article 49.2 of the Statute with respect to central banks of Member States whose derogations have been abrogated | Cost/nominal value |
14 | Revaluation accounts | (a)Revaluation accounts related to price movements for gold, for every type of euro-denominated securities, for every type of foreign currency-denominated securities, for options; market valuation differences related to interest rate risk derivatives; revaluation accounts related to foreign exchange rate movements for every currency net position held, including foreign exchange swaps/forwards and SDRs(b)Special revaluation accounts stemming from contributions according to Article 49.2 of the Statute with respect to central banks of Member States whose derogations have been abrogated - see Article 11(2) | Revaluation difference between average cost and market value, foreign exchange translated at market rate |
15 | Capital and reserves | | |
15.1 | Capital | Paid-up capital | Nominal value |
15.2 | Reserves | Legal reserves, according to Article 33 of the Statute and contributions according to Article 49.2 of the Statute with respect to central banks of Member States whose derogations have been abrogated | Nominal value |
16 | Profit for the year | | Nominal value |
[1] OJ L 310, 11.12.2000, p. 1.
[2] OJ L 337, 20.12.2001, p. 52.
[3] I.e. accrued interest purchased with a security.
--------------------------------------------------
ANNEX II
Annual balance sheet of the ECB
(EUR million) [1] |
Assets | Reporting year | Previous year | Liabilities | Reporting year | Previous year |
1Gold and gold receivables2Claims on non-euro area residents denominated in foreign currency2.1Receivables from the IMF2.2Balances with banks and security investments, external loans and other external assets3Claims on euro area residents denominated in foreign currency4Claims on non-euro area residents denominated in euro4.1Balances with banks, security investments and loans4.2Claims arising from the credit facility under ERM II5Lending to euro area credit institutions related to monetary policy operations denominated in euro5.1Main refinancing operations5.2Longer-term refinancing operations5.3Fine-tuning reverse operations5.4Structural reverse operations5.5Marginal lending facility5.6Credits related to margin calls6Other claims on euro area credit institutions denominated in euro7Securities of euro area residents denominated in euro8General government debt denominated in euro9Intra-Eurosystem claims9.1Claims related to promissory notes backing the issuance of ECB debt certificates9.2Claims related to the allocation of euro banknotes within the Eurosystem9.3Other claims within the Eurosystem (net)10Items in course of settlement11Other assets11.1Coins of euro area11.2Tangible and intangible fixed assets11.3Other financial assets11.4Off-balance sheet instruments revaluation differences11.5Accruals and prepaid expenses11.6Sundry12Loss for the year | | | 1Banknotes in circulation2Liabilities to euro area credit institutions related to monetary policy operations denominated in euro2.1Current accounts covering the minimum reserve system2.2Deposit facility2.3Fixed-term deposits2.4Fine-tuning reverse operations2.5Deposits related to margin calls3Other liabilities to euro area credit institutions denominated in euro4ECB debt certificates issued5Liabilities to other euro area residents denominated in euro5.1General government5.2Other liabilities6Liabilities to non-euro area residents denominated in euro7Liabilities to euro area residents denominated in foreign currency8Liabilities to non-euro area residents denominated in foreign currency8.1Deposits, balances and other liabilities8.2Liabilities arising from the credit facility under ERM II9Counterpart of special drawing rights allocated by the IMF10Intra-Eurosystem liabilities10.1Liabilities equivalent to the transfer of foreign reserves10.2Other liabilities within the Eurosystem (net)11Items in course of settlement12Other liabilities12.1Off-balance sheet instruments revaluation differences12.2Accruals and income collected in advance12.3Sundry13Provisions14Revaluation accounts15Capital and reserves15.1Capital15.2Reserves16Profit for the year | | |
Total assets | | | Total liabilities | | |
[1] The ECB may alternatively publish exact euro amounts, or amounts rounded in a different manner.
--------------------------------------------------
ANNEX III
PUBLISHED PROFIT AND LOSS ACCOUNT OF THE ECB
(EUR million) [1] |
Profit and loss account for the year ending 31 December … | Reporting year | Previous year |
1.1.1 | Interest income on foreign reserve assets | | |
1.1.2 | Interest income arising from the allocation of euro banknotes within the Eurosystem | | |
1.1.3 | Other interest income | | |
1.1 | Interest income | | |
1.2.1 | Remuneration of NCBs’ claims in respect of foreign reserves transferred | | |
1.2.2 | Other interest expense | | |
1.2 | Interest expense | | |
1 | Net interest income | | |
2.1 | Realised gains/losses arising from financial operations | | |
2.2 | Write-downs on financial assets and positions | | |
2.3 | Transfer to/from provisions for foreign exchange rate and price risks | | |
2 | Net result of financial operations, write downs and risk provisions | | |
3.1 | Fees and commissions income | | |
3.2 | Fees and commissions expense | | |
3 | Net income/expense from fees and commissions [2] | | |
4 | Income from equity shares and participating interests | | |
5 | Other income | | |
Total net income | | |
6 | Staff costs [3] | | |
7 | Administrative expenses [3] | | |
8 | Depreciation of tangible and intangible fixed assets | | |
9 | Banknote production services [4] | | |
10 | Other expenses | | |
(Loss)/profit for the year | | |
[1] The ECB may alternatively publish exact euro amounts, or amounts rounded in a different manner.
[2] The breakdown between income and expense may alternatively be provided in the explanatory notes to the annual accounts.
[3] Including administrative provisions.
[4] This item is used in case of outsourced banknote production (for the cost of the services provided by external companies in charge of the production of banknotes on behalf of the central banks). It is recommended that the costs incurred in connection with the issue of euro banknotes should be taken to the profit and loss account as they are invoiced or otherwise incurred, see also Guideline ECB/2006/16.
--------------------------------------------------
ANNEX IV
CORRELATION TABLE
Decision ECB/2002/11 | This Decision |
— | Article 7 |
Article 7 | Article 8 |
Article 8 | Article 9 |
Article 9 | Article 10 |
Article 10 | Article 11 |
Article 11 | Article 12 |
Article 12 | Article 13 |
Article 13 | Article 14 |
Article 14 | Article 15 |
Article 15 | Article 16 |
Article 16 | Article 17 |
Article 17 | Article 18 |
Article 18 | Article 19 |
— | Article 20 |
Article 19 | Article 21 |
Article 20 | Article 22 |
Article 21 | Article 23 |
Article 22 | Article 24 |
--------------------------------------------------
