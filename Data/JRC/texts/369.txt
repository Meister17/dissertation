Council Regulation (EC) No 1262/2000
of 8 June 2000
laying down certain control measures in respect of vessels flying the flag of non-Contracting Parties to the Northwest Atlantic Fisheries Organisation (NAFO)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 37 thereof,
Having regard to the proposal from the Commission(1),
Having regard to the Opinion of the European Parliament(2),
Whereas:
(1) The Community is a Contracting Party to the Convention on Future Multilateral Cooperation in the Northwest Atlantic Fisheries, hereafter referred to as the "NAFO Convention"(3).
(2) The NAFO Convention provides a suitable framework for regional cooperation in the conservation and management of fishery resources through, inter alia, the setting up of an international organisation known as the Northwest Atlantic Fisheries Organisation, hereafter referred to as "NAFO", and the adoption of proposals for conservation and enforcement measures for the fishery resources of the NAFO regulatory area, which become binding upon the Contracting Parties.
(3) The practice of operating fishing vessels under the flag of non-contracting parties to NAFO as a means of avoiding compliance with conservation and enforcement measures established by NAFO remains one of the factors which seriously undermines the effectiveness of such measures and should, therefore, be discouraged.
(4) NAFO has consistently invited the non-contracting parties concerned either to become a member of NAFO or to agree to apply the conservation and enforcement measures established by NAFO with a view to fulfilling their responsibilities with regard to fishing vessels entitled to fly their flag.
(5) At its 19th annual meeting in September 1997, NAFO resolved to adopt a "scheme to promote compliance by non-contracting party vessels with the conservation and enforcement measures established by NAFO", the objective of which is to ensure that the effectiveness of conservation and enforcement measures established by NAFO is not undermined by non-contracting vessels.
(6) The said Scheme provides, inter alia, for the mandatory inspection of non-contracting party vessels when such vessels are voluntarily in the ports of Contracting Parties and, with due regard to the multispecies fisheries in the regulatory area of NAFO, a prohibition of landings and transhipments if, in the course of such inspection, it is established that the catch has been taken in contravention of conservation and enforcement measures established by NAFO as well as certain other collateral measures to be taken by Contracting Parties.
(7) At the 20th annual meeting of NAFO in September 1998, certain specifications were made to the provisions of the scheme relating to at-sea transhipments and relevant sightings.
(8) At the 21st annual meeting of NAFO in September 1999, additional specifications for Stateless vessels were made.
(9) Under the Treaty, the authority over internal waters and ports is exercised by the Member States. However, as regards access to port facilities in the Community by non-contracting party vessels which are sighted engaging in fishing activities in the NAFO regulatory area, it is necessary to enact additional uniform measures at Community level, which supplement the measures provided for in Council Regulation (EEC) No 2847/93 of 12 October 1993 establishing a control system applicable to the common fisheries policy(4) and ensure that the operations of such vessels in Community ports do not undermine the effectiveness of the conservation and enforcement measures established by NAFO,
HAS ADOPTED THIS REGULATION:
Article 1
For the purposes of this Regulation, the following definitions shall apply:
(a) "fishing activities" shall mean fishing, fish processing operations, the transhipment of fish or fish products and any other activity in preparation for, or related to, fishing in the NAFO regulatory area;
(b) "NAFO regulatory area" shall mean the area referred to in Article I(2) of the NAFO Convention;
(c) "non-contracting party vessel" shall mean a vessel which has been sighted and reported as having engaged in fishing activities in the NAFO regulatory area and
(i) which flies the flag of a State which is not a contracting party to the NAFO Convention; or
(ii) for which there are reasonable grounds for suspecting it to be without nationality.
Article 2
Upon receipt of a report on a sighting of a non-contracting party vessel by a Community inspector assigned to the NAFO scheme of joint international inspection and surveillance, the Commission shall transmit this information without delay to the Secretariat of NAFO and, where possible, to the non-contracting party vessel, informing it that the information will be transmitted to all the Contracting Parties to the NAFO Convention and to its flag State.
Article 3
The Commission shall communicate to all Member States without delay each report on a sighting which it has received pursuant to Article 2 or by way of a notification from the Secretariat of NAFO or another Contracting Party.
Article 4
Community fishing vessels shall be prohibited from receiving transhipments of fish from non-contracting party vessels.
Article 5
1. Member States shall ensure that each non-contracting party vessel which enters a designated port within the meaning of Article 28(e)(2) of Regulation (EEC) No 2847/93 is inspected by their competent authorities. The vessel may not land or tranship any catch until this inspection has been completed.
2. If, on completion of inspection, the competent authorities find that the non-contracting party vessel holds on board any of the species listed in Annexes I and II, the Member State concerned shall prohibit any landing and/or transhipment.
3. However, no such prohibition shall apply if the master of the inspected vessel or his representative proves to the satisfaction of the competent authorities of the Member State concerned that:
- species held on board were caught outside the NAFO regulatory area, or
- species held on board and listed in Annex II were caught in accordance with the conservation and enforcement measures established by NAFO.
Article 6
1. Member States shall communicate without delay to the Commission the results of each inspection and, where appropriate, any subsequent prohibition of landing and/or transhipment, which was applied as a result of the inspection. This information shall include the name of the inspected non-contracting party vessel and its flag State, the date and the port of inspection, the grounds for a subsequent prohibition of landing and/or transhipment or, where no such prohibition was applied, the evidence presented pursuant to Article 5(3).
2. The Commission shall transmit this information without delay to the Secretariat of NAFO and as soon as possible to the flag State of the inspected non-contracting party vessel.
Article 7
This Regulation shall enter into force on the seventh day following that of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Luxembourg, 8 June 2000.
For the Council
The President
G. Oliveira Martins
(1) OJ C 56, 29.2.2000, p. 1.
(2) Opinion delivered on 19 May 2000 (not yet published in the Official Journal).
(3) OJ L 378, 30.12.1978, p. 1.
(4) OJ L 261, 20.10.1993, p. 1. Regulation as last amended by Regulation (EC) No 2846/98 (OJ L 358, 31.12.1998, p. 5).
ANNEX I
LIST OF REGULATED SPECIES
>TABLE>
ANNEX II
LIST OF NON-REGULATED SPECIES
>TABLE>
