Commission Regulation (EC) No 2100/2005
of 20 December 2005
amending for the 60th time Council Regulation (EC) No 881/2002 imposing certain specific restrictive measures directed against certain persons and entities associated with Usama bin Laden, the Al-Qaida network and the Taliban, and repealing Council Regulation (EC) No 467/2001
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 881/2002 imposing certain specific restrictive measures directed against certain persons and entities associated with Usama bin Laden, the Al-Qaida network and the Taliban, and repealing Council Regulation (EC) No 467/2001 prohibiting the export of certain goods and services to Afghanistan, strengthening the flight ban and extending the freeze of funds and other financial resources in respect of the Taliban of Afghanistan [1], and in particular Article 7(1), first indent, thereof,
Whereas:
(1) Annex I to Regulation (EC) No 881/2002 lists the persons, groups and entities covered by the freezing of funds and economic resources under that Regulation.
(2) On 15 December 2005, the Sanctions Committee of the United Nations Security Council decided to amend the list of persons, groups and entities to whom the freezing of funds and economic resources should apply. Annex I should therefore be amended accordingly.
(3) In order to ensure that the measures provided for in this Regulation are effective, this Regulation must enter into force immediately,
HAS ADOPTED THIS REGULATION:
Article 1
Annex I to Regulation (EC) No 881/2002 is hereby amended as set out in the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 20 December 2005.
For the Commission
Eneko Landáburu
Director-General of External Relations
[1] OJ L 139, 29.5.2002, p. 9. Regulation as last amended by Commission Regulation (EC) No 2018/2005 (OJ L 324, 10.12.2005, p. 21).
--------------------------------------------------
ANNEX
The following entry shall be added to Annex I to Regulation (EC) No 881/2002 under the heading "Natural persons":
Sajid Mohammed Badat (alias (a) Abu Issa, (b) Saajid Badat, (c) Sajid Badat, (d) Muhammed Badat, (e) Sajid Muhammad Badat, (f) Saajid Mohammad Badet, (g) Muhammed Badet, (h) Sajid Muhammad Badet). Date of birth: (a) 28.3.1979, (b) 8.3.1976. Place of birth: Gloucester, United Kingdom. Passport No: (a) United Kingdom passport number 703114075, (b) United Kingdom passport number 026725401. Other information: Currently in custody in the United Kingdom. Previous address is Gloucester, United Kingdom.
--------------------------------------------------
