COUNCIL DIRECTIVE of 12 July 1982 amending Directive 78/663/EEC laying down specific criteria of purity for emulsifiers, stabilizers, thickeners and gelling agents for use in foodstuffs (82/504/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Directive 74/329/EEC of 18 June 1974 on the approximation of the laws of the Member States relating to emulsifiers, stabilizers, thickeners and gelling agents for use in foodstuffs (1), as last amended by Directive 80/597/EEC (2), and in particular Article 7 (1) thereof,
Having regard to the proposal from the Commission,
Whereas Council Directive 78/663/EEC (3) lays down specific criteria of purity for emulsifiers, stabilizers, thickness and gelling agents for use in foodstuffs;
Whereas Directive 80/597/EEC has amended Annex I to Directive 74/329/EEC so as to authorize Xanthan gum (E 415) and powdered cellulose (E 460 - (ii)) and, for this reason, the criteria of purity of these substances should be defined and the nomenclature of E 460 amended in consequence;
Whereas Directive 78/663/EEC provides that, as regards substances E 474 and E 477, the Council may, acting unanimously on a proposal from the Commission, decide on any necessary amendments by 31 December 1981;
Whereas the criteria of purity for substances E 400, E 401, E 402, E 403, E 404 and E 405 should be modified to take account of scientific developments, particularly of methods of analysis,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Directive 78/663/EEC is hereby amended as follows: 1. Article 2 shall be replaced by the following:
"Article 2
As regards the substances referred to in the Annex under E 477, Member States may, until 31 December 1984, authorize for use in foodstuffs a product containing not more than 4 % dimer and trimer of propane-1,2-diol."
2. The Annex shall be amended as follows: (a) Under E 400, E 401, E 402, E 403, E 404 and E 405, the entries relating to insoluble matter in dilute NaOH shall be deleted, and the text of the entries relating to acid-insoluble ash shall be replaced by "Not more than 2 %". (1) OJ No L 189, 12.7.1974, p. 1. (2) OJ No L 155, 23.6.1980, p. 23. (3) OJ No L 223, 14.8.1978, p. 7.
(b) The following shall be inserted between E 414 and E 420 - (i): >PIC FILE= "T0022631">
(c) The number "E 460" shall become "E 460 - (i)".
(d) The following shall be inserted between E 460 - (i) and E 461: >PIC FILE= "T0022632">
(e) Under E 474:
- The last sentence of the text of the entry relating to chemical description shall be replaced by the following:
"No organic solvents shall be used in their preparation other than cyclohexane, dimethylformamide, ethyl acetate, isobutanol and isopropanol."
- The following new entry shall be added: >PIC FILE= "T0022633">
(f) Under E 477, the entry relating to dimer and trimer of propane-1,2-diol shall be replaced by the following:
"Not more than 0 75 %."
Article 2
Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive not later than 1 January 1984. They shall forthwith inform the Commission thereof.
Article 3
This Directive is addressed to the Member States.
Done at Brussels, 12 July 1982.
For the Council
The President
J. NØRGAARD
