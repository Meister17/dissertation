*****
COMMISSION DECISION
of 28 March 1990
on the list of establishments in Madagascar approved for the purpose of importing fresh meat into the Community
(90/165/EEC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Directive 72/462/EEC of 12 December 1972 on health and veterinary inspection problems upon importation of bovine animals and swine and fresh meat or meat products from third countries (1), as last amended by Directive 89/227/EEC (2), and in particular Articles 4 (1) and 18 (1) thereof,
Whereas establishments in third countries cannot be authorized to export fresh meat to the Community unless they satisfy the general and special conditions laid down in Directive 72/462/EEC;
Whereas Commission Decision 84/576/EEC (3) prohibited imports of fresh meat coming from establishments in Madagascar; whereas however to avoid an abrupt interruption of existing trade flow, importations were maintained until 30 June 1985;
Whereas a further inspection carried out pursuant to Article 5 of Directive 72/462/EEC and Article 2 (1) of Commission Decision 86/474/EEC on the implementation of the on-the-spot inspections to be carried out in respect of the importation of bovine animals and swine and fresh meat from non-member countries (4) has shown that the standard of hygiene in two establishments has been raised and can thus be regarded as satisfactory;
Whereas these establishments can, in these circumstances, be included in a list of establishments authorized to export to the Community and therefore, Decision 84/576/EEC must be repealed;
Whereas import of fresh meat from the establishments appearing in the Annex remains subject to Community provisions laid down elsewhere, in particular concerning animal health, examination for the presence of residues in fresh meat, prohibition of the use in livestock farming of certain substances having a hormonal action;
Whereas the measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
1. The establishments in Madagascar appearing in the Annex are hereby approved for the import of fresh meat into the Community pursuant to the said Annex.
2. Imports from establishments listed in the Annex shall remain subject to the Community veterinary provisions laid down elsewhere.
Article 2
Member States shall prohibit imports of fresh meat coming from establishments not appearing in the Annex.
Article 3
Decision 84/576/EEC is hereby repealed.
Article 4
This Decision is addressed to the Member States.
Done at Brussels, 28 March 1990.
For the Commission
Ray MAC SHARRY
Member of the Commission
(1) OJ No L 302, 31. 12. 1972, p. 28.
(2) OJ No L 93, 6. 4. 1989, p. 25.
(3) OJ No L 315, 5. 12. 1984, p. 25.
(4) OJ No L 279, 30. 9. 1986, p. 55.
ANNEX
LIST OF ESTABLISHMENTS
1.2.3,9.10 // // // // // Approval No // Establishment/address // Category (*) // 1.2.3.4.5.6.7.8.9.10 // // // SL // CP // CS // B // S/G // P // SP // SR // // // // // // // // // // // 16/26 // Abattoir de Morondava, Morondava, Tulea // × // × // // × // // // // (1) // // // // // // // // // // // 17/27 // Abattoir Frigorifique de Majunga, Majunga // × // × // // × // // // // (1) // // // // // // // // // // 1.2.3.4.5.6.7.8 // (*) // SL // = Slaughterhouse // B // = Bovine meat // // SR // = Special remarks // // CP // = Cutting premises // S/G // = Sheepmeat/Goatmeat // // // // // CS // = Cold store // P // = Pigmeat // // // // // // // SP // = Meat from solipeds // // //
(1) Offal excluded.
