COMMISSION DECISION
of 31 May 1999
amending Decision 94/448/EC laying down special conditions governing imports of fishery and aquaculture products originating in New Zealand
(notified under document number C(1999) 1404)
(Text with EEA relevance)
(1999/402/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/493/EEC of 22 July 1991 laying down the health conditions for the production and the placing on the market of fishery products(1), as last amended by Directive 97/79/EC(2), and in particular Article 11 thereof,
(1) Whereas Article 1 of Commission Decision 94/448/EC of 20 June 1994 laying down special conditions governing imports of fishery and aquaculture products originating in New Zealand, as last amended by Decision 96/254/EC(3), states that the Ministry of Agriculture and Fisheries (MAF) shall be the competent authority in New Zealand for verifying and certifying compliance of fishery and aquaculture products with the requirements of Directive 91/493/EC;
(2) Whereas, following a restructuring of the New Zealand Government, the competent authority for health certificates for fishery products (MAF) has changed from the Ministry of Agriculture and Fisheries into the Ministry of Agriculture and Forestry; and whereas this new authority is capable of effectively verifying the application of the laws in force; whereas it is, therefore, necessary to modify the nomination of the competent authority mentioned in Decision 94/448/EC;
(3) Whereas it is convenient to harmonise the wording of Decision 94/448/EC with the wording of the more recently adopted Commission Decisions, laying down special conditions governing imports of fishery and aquaculture products originating in certain third countries;
(4) Whereas the measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
Decision 94/448/EC shall be modify as follows:
1. Article 1 shall be replace by the following: "Article 1
The 'Ministry of Agriculture and Forestry (MAF)' shall be the competent authority in New Zealand for verifying and certifying compliance of fishery and aquaculture products with the requirements of Directive 91/493/EEC."
2. Article 2 shall be replace by the following: "Article 2
Fishery and aquaculture products originating in New Zealand must meet the following conditions:
1. each consignment must be accompanied by a numbered original health certificate, duly completed, signed, dated and comprising a single sheet in accordance with the model in Annex A hereto;
2. the products must come from approved establishments, factory vessels, cold stores or registered freezer vessels listed in Annex B hereto;
3. except in the case of frozen fishery products in bulk and intended for the manufacture of preserved foods, all packages must bear the word 'NEW ZEALAND' and the approval/registration number of the establishment, factory vessel, cold store or freezer vessel of origin in indelible letters."
3. Annex A shall be replaced by the Annex hereto.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 31 May 1999.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ L 268, 24.9.1991, p. 15.
(2) OJ L 24, 30.1.1998, p. 31.
(3) OJ L 86, 4.4.1996, p. 75.
ANNEX
"ANNEX A
>PIC FILE= "L_1999151EN.003303.EPS">
>PIC FILE= "L_1999151EN.003401.EPS">"
