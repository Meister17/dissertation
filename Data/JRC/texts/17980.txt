Commission Decision
of 29 January 2004
amending Council Decision 97/788/EC as regards checks carried out by Serbia and Montenegro concerning the maintenance of varieties of fodder, beet, oil and fibre plants
(notified under document number C(2004) 147)
(Text with EEA relevance)
(2004/120/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Decision 97/788 of 17 November 1997 on the equivalence of checks on practices for the maintenance of varieties carried out in third countries(1), and in particular Article 2 thereof,
Whereas:
(1) Decision 97/788/EC determined that the official checks on practices for the maintenance of varieties carried out in certain third countries for certain species afford the same guarantees as those carried out by the Member States, pursuant to Council Directive 2002/53/EC of 13 June 2002 on the common catalogue of varieties of agriculture plant species(2).
(2) Decision 97/788 applies to the Federal Republic of Yugoslavia (now known as Serbia and Montenegro) only as regards varieties of certain species.
(3) An examination of the rules of Serbia and Montenegro and the manner in which they are applied in respect of the species listed in Council Directive 66/401/EEC of 14 June 1966 on the marketing of fodder plant seed(3), in Council Directive 2002/54/EC of 13 June 2002 on the marketing of beet seed(4) and in Council Directive 2002/57/EC of 13 June 2002 on the marketing of seed of oil and fibre plants(5) have shown that the checks on practices for the maintenance of varieties afford the same guarantees as those carried out by the Member States.
(4) The equivalence granted to Serbia and Montenegro should be extended to these additional species.
(5) Decision 97/788/EC should therefore be amended accordingly.
(6) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on Seeds and Propagating Material for Agriculture, Horticulture and Forestry,
HAS ADOPTED THIS DECISION:
Article 1
The Annex to Decision 97/788/EC is amended as follows.
In the entry beside the code "YU" relating to the Federal Republic of Yugoslavia, "66/402" is replaced by "66/401, 66/402, 2002/54 and 2002/57".
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 29 January 2004.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 322, 25.11.1997, p. 39. Decision as amended by Decision 2002/580/EC (OJ L 184, 13.7.2002, p. 26).
(2) OJ L 193, 20.7.2002, p. 1. Directive as last amended by Regulation (EC) No 1829/2003 of the European Parliament and of the Council (OJ L 268, 18.10.2003, p. 1).
(3) OJ L 125, 11.7.1966, p. 2298/66. Directive as last amended by Directive 2003/61/EC (OJ L 165, 3.7.2003, p. 23).
(4) OJ L 193, 20.7.2002, p. 12. Directive as last amended by Directive 2003/61/EC.
(5) OJ L 193, 20.7.2002, p. 60. Directive as amended by Directive 2003/61/EC.
