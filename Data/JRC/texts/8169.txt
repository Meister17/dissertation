DECISION OF THE EEA JOINT COMMITTEE
No 82/98
of 25 September 1998
amending Annex II (Technical regulations, standards, testing and certification) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as adjusted by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas Annex II to the Agreement was amended by Decision of the EEA Joint Committee No 13/94 of 28 October 1994(1);
Whereas Directive 97/23/EC of the European Parliament and of the Council of 29 May 1997 on the approximation of the laws of the Member States concerning pressure equipment(2), is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
The following point shall be inserted after point 6 (Council Directive 87/404/EEC) in Chapter VIII of Annex II to the Agreement: "6a. 397 L 0023: Directive 97/23/EC of the European Parliament and of the Council of 29 May 1997 on the approximation of the laws of the Member States concerning pressure equipment (OJ L 181, 9.7.1997, p. 1)."
Article 2
The following indent shall be added in point 2 (Council Directive 76/767/EEC) in Chapter VIII of Annex II to the Agreement: "- 397 L 0023: Directive 97/23/EC of the European Parliament and of the Council of 29 May 1997 (OJ L 181, 9.7.1997, p. 1)."
Article 3
The texts of Directive 97/23/EC in the Icelandic and Norwegian languages, which are annexed to the respective language versions of this Decision, are authentic.
Article 4
This Decision shall enter into force on 26 September 1998, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee.
Article 5
This Decision shall be published in the EEA section of, and in the EEA Supplement to, the Official Journal of the European Communities.
Done at Brussels, 25 September 1998.
For the EEA Joint Committee
The President
N. v. LIECHTENSTEIN
(1) OJ L 325, 17.12.1994, p. 64.
(2) OJ L 181, 9.7.1997, p. 1.
