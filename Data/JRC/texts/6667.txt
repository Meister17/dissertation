[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 28.11.2005
COM(2005) 606 final
GREEN PAPER
ON THE FUTURE OF THE EUROPEAN MIGRATION NETWORK
(presented by the Commission)
TABLE OF CONTENTS
1. Introduction 3
2. Policy Background 3
3. Existing Information Gaps 3
4. The Current Emn 4
4.1. Status and structure 4
4.2 Financing 5
4.3 Tasks and main achievements 5
4.4 Assessment 6
5. A Future European Migration Network 7
5.1. General objective and fields of action 7
5.2. Concrete tasks 8
5.3. Relations with other information gathering bodies and institutional stakeholders 8
5.4. Form and structure 9
6. Conclusion 11
Annex 1 12
1. INTRODUCTION
THE AIM OF THIS GREEN PAPER IS TO SEEK VIEWS from all relevant stakeholders on the future of the European Migration Network (EMN), including options for its possible tasks and structure.
The EMN was set up in 2002 as a pilot/preparatory action in response to the identified need to improve the exchange of information on all aspects of migration and asylum. Its primary objective is to provide the Community and its Member States with objective, reliable and comparable information in these fields by systematically collecting and storing existing data and information from Member States and carrying out national and European level analysis. At present, the EMN consists of national contact points designated by Member States and a scientific coordinator, both supported and monitored by the European Commission.
As the preparatory action will be drawing to a close in 2006, it is necessary to take stock of the progress made so far and launch, through this Green Paper, a reflection on the best way forward
2. POLICY BACKGROUND
Already in 1994, the European Commission stressed in its first communication on immigration and asylum policies the value of creating a mechanism to monitor migration flows on a comprehensive and EU-wide basis. In response, a feasibility study was carried out in 1996[1].
In its conclusions the Laeken European Council of December 2001 invited the Commission ‘to establish a system for exchange of information on asylum, migration and countries of origin’, further to which the EMN was set up as a pilot project in 2002.
To start implementation, it was agreed with Member States to first set up a network of national contact points that would provide the necessary information.
The European Council of Thessaloniki in June 2003 endorsed the creation of the EMN and proposed to examine the possibility of ‘creating a permanent structure in the future’.
Finally, in the Hague Programme the European Council of 4-5 November 2004 states that the ongoing development of European asylum and migration policy should be based on a common analysis of migratory phenomena in all their aspects. Reinforcing the collection, provision, exchange, and efficient use of up-to-date information and data on all relevant migratory developments is of key importance’.
3. EXISTING INFORMATION GAPS
IMPORTANT INFORMATION N eeds, as already identified by the 1996 feasibility study and confirmed by the 2005 evaluation of the EMN[2], continue to exist in the field of migration and asylum for policy makers at both EU and Member States level which any future EMN structure should contribute to satisfying.
First, despite the huge amount of information on many aspects of international migration that is being produced, access to this information is not easy to obtain, as it is dispersed and not centrally available. A single reference point for the filtering and synthesising of this information that could serve as a clearing house would therefore be very useful to EU institutions and Member States, both policy makers and the general public.
Second, there is insufficient reliable, comparable and up-to-date information and analysis of legal and policy developments with regard to asylum and migration, both at national and EU level. There is also insufficient comparable information on effects of EU and national policies in this area. In addition, there exists an increasing need for the EU and its Member States to obtain quick responses on specific issues related to migration and asylum. To fill these information gaps, some kind of network structure would be required that could reach out to all Member States, combined with a certain level of central coordination.
Third, a major problem lies in the lack of accurate, up-to-date and comparable statistical data on migration and asylum. The recently adopted Commission proposal for a regulation on Community statistics on migration and international protection[3] will tackle this problem, but there will continue to be a need for the comparative analysis of such statistical information.
4. THE CURRENT EMN
THIS SECTION DESCRIBES THE CURRENT EMN INSOFAR AS NECESSARY TO UNDERSTAND ITS FUNCTIONING, OUTPUTS AND PROBLEMS WITH A VIEW TO DETERMINING its future structure[4].
4.1. Status and structure
At present, the EMN consists of a network of 14 national contact points (NCPs) designated by Member States. The NCPs are at the core of the network as they carry out the documentation and analysis and research activities. The majority of new Member States have not yet designated NCPs and participate as observers. It is expected that some will become full members of the EMN in 2006.
NCPs range from structures within or attached to Ministries of Interior or Justice, national statistical offices and research institutes, to structures within non-governmental and welfare institutions to national offices of intergovernmental organisations. NCPs are rather frequently changed, for instance following a reorganization of responsibility between Ministries.
A scientific and administrative co-ordinator was selected in December 2003 following an open invitation to tender to assist in the development of the network.
4.2 Financing
The current network is co-financed between the Commission and Member States. Some NCPs that are part of or attached to Member States’ administrations did not receive Community funding due to constraints linked with national public finance rules or Community eligibility rules. The financial resources allocated to set up the network increased from € 1.4 million for the 2002 pilot action to € 3.0 million for the 2005 budget that will be implemented in 2006, including grants for 9 NCPs and financial provisions for the scientific and administrative coordination of the network.
4.3 Tasks and main achievements
The primary objective of the EMN as set out in its original specifications is to provide the Community, its Member States, and in the longer term the public with objective, reliable and comparable information on the migration and asylum situation at the European and national levels.
This is to be accomplished by bringing together and making accessible existing data and information as well as reviewing and analysing relevant existing information.
The EMN therefore focuses on two interrelated activities: documentation and analysis, and research. This includes information monitoring, comparison, analysis and research, through collating existing information from Member States and undertaking some own research for European-level analysis, and developing a comprehensive database for such information. Networks at national level contribute to the EMN’s analysis and research activity. EMN action is implemented on the basis of annual work programmes approved by the Commission.
Against this background, the following principal achievements –apart from the actual setting up of the NCPs in terms of staff, facilities and IT equipment- can be identified from 2002 until now:
Documentation facility
In order to comply with the objective to developing a comprehensive repository for data and information, a computer-based information system is being set up which constitutes the backbone of the EMN operation. The web-based user interface developed in 2004 facilitates data collection and documentation by offering a practical system of templates that provide for the direct upload of the collected data into the information system. The system also provides for a search and navigation mechanism.
The system is nearing completion so that its functioning will now be a matter of inputting data and information. In this respect, by mid-2005, NCP’s had uploaded more than 1700 publications; in addition legal documents and contacts of both organisations and individuals active in the field had been indexed. Efforts are currently undertaken to improve the quality of these entries through common guidelines to be followed when uploading information. It is obvious that many more inputs are needed if the EMN documentation centre is to provide a comprehensive collection of policy-relevant and up-to-date information on asylum and migration for policy makers, practitioners and eventually the general public.
Analysis and research activity
The EMN has completed several analysis and research projects and information outputs, among these the 2004 pilot research study on the impact of immigration on Europe’s societies[5]; contributions to 2001-2003 EU annual reports on statistics[6] by supplying information on national policies and practices, and analysing national figures; the 2004 policy analysis report that served to obtain an overview of national policy developments and to conduct a comparative overview, as well as other research studies. Currently, with a view to filling information gaps in two policy relevant areas, a second research study on state approaches towards illegally resident third country nationals and a small scale study on reception systems for asylum applicants are in preparation.
Even though it is still early to assess the overall quality of the EMN outputs, it can be said that NCPs produced high quality reports that give an indication of the network’s capacities, especially within the context of providing reliable and up-to-date analysis that allows for comparison between Member States.
Networking and Visibility
Bilateral contacts between NCPs have intensified as the network progresses. As regards networking at national level, most NCPs have succeeded in establishing national networks of relevant actors and setting up procedures for obtaining inputs from their partners. A national network directory that provides an overview of potential and actual network partners has been established. Efforts were also made to establish contacts with other networks and projects at EU level. However, these contacts have not led to intensified collaboration which would be required for the EMN to ensure synergies and avoid overlaps.
The visibility of the EMN to the outside world is currently limited, as very few of its products are (yet) accessible to the public. This prevents the network’s results from being taken fully into account. The lack of visibility is mainly due to the fact that the network started from scratch so that the first years of operation had to be used for its setting up. Various efforts are however currently underway with a view to improving the EMN’s visibility and spreading its research results[7].
4.4 Assessment
The 2005 evaluation report identified several difficulties that the current EMN has been confronted with in fulfilling its tasks, the two most important being the structure of national contact points and the general set-up of the project.
As to the first factor, there is a great heterogeneity in the legal status and in the specialisation of the NCPs as well as in the human, financial resources and technical equipment made available and therefore in their level of commitment. In addition, NCPs have been designated at different points in time and are also frequently changed, be it due to changes in the personnel or as a consequence of a shift of responsibility to a different ministry. The result is that NCPs are at very different stages of their development.
Concerning the second factor, the institutional arrangement and set up chosen required a rather high level of input from the Commission. In addition, the administrative process chosen for setting up and running the network- i.e. a co-financed action- proved to be resource intensive. The appointment of a scientific coordinator improved the overall situation, although there is still a need to identify more clearly the role of the Commission.
In addition, the initial tasks and functions of the EMN were rather wide, owing to the pilot nature of the project and its experimental start-up. The definition of roles and functions of both the network as a whole and the NCPs, and their relation with the coordinator, therefore lacked a certain clarity and focus.
On the whole, while progress towards the achievement of its objective can be accounted for especially in 2004, structural problems inherent to the current set-up of the EMN inhibit more and faster progress to be made. Any future structure should therefore seek to address and avoid these problems. It should also to the extent possible build on what has already been and will still be achieved by the current EMN.
5. A FUTURE EUROPEAN MIGRATION NETWORK
AGAINST THE ABOVE BACKGROUND, THIS SECTION BRINGS FORWARD A NUMBER OF ELEMENTS THAT ARE RELEVANT WHEN DECIDING ON A FUTURE STRUCTURE to be set up from 2007, it being understood that these elements are interdependent. Participants are invited to comment on these options put forward by the Commission.
5.1. General objective and fields of action
Given the existing information gaps in the field of migration and asylum as outlined above, the general objective of the current EMN to provide the Community, its Member States and the general public with objective, reliable and comparable information remains valid and relevant. This information should be made available with a view to supporting the common immigration and asylum policies.
Concerning outputs, and in particular the database, the starting point would be that these should be accessible to the general public, in line with the objective. However, there may be cases where it would prove useful or even necessary to restrict such access to a specific user group, for instance for pieces of information that Member States consider confidential.
Do you agree with such a mandate –to provide the Community, its Member States and the general public with objective, reliable and comparable information in the field of asylum and migration- for a future EMN? If not, which changes should be made to the mandate, and why? Should the data and information collected by the EMN systematically be made publicly available? |
- 5.2. Concrete tasks
It needs to be determined which concrete tasks a future structure should fulfil in order to achieve the general objective outlined above.
Tasks currently undertaken focus on the collection of existing data and information, a limited amount of research, the exchange and documentation of these data; analysis of the data and production of comparative reports, and networking.
In particular, as regards the provision of comparable information, the EMN could develop an in-depth understanding of methodological differences that may hinder data comparison, and develop an adequate, common methodology to overcome such differences.
Moreover one could envisage that a certain amount of further research be launched and implemented by a future EMN in areas of identified information gaps, either as part of its agreed work programme or upon the specific request of Community institutions, and possibly other actors[8].
Depending on the further structure to be chosen, other tasks could include the issuing of views and opinions on the basis of the information collection and analysis activities. The purpose would be to provide guidance and advice on policy developments and implementation. It may, however, be premature to charge the EMN with such tasks, in the sense that it may be preferable for a future structure to concentrate on core tasks of data collection and analysis, in order to first build up a certain authority in the field.
The current network’s low visibility combined with the need for it to liaise with other bodies active in the field calls for pro-active dissemination and awareness raising activities, including organising meetings and conferences on migration and asylum to provide a forum for key stakeholders. However, such tasks would require substantive additional resources.
Which of the above mentioned concrete tasks –collection and analysis of information, research, issuing opinions and recommendations, and awareness raising activities- should a future structure be given and why? Can you suggest other tasks that should be fulfilled? |
- 5.3. Relations with other information gathering bodies and institutional stakeholders
It is essential that maximum synergies with other actions carried out in this field by the Commission, the EU, the Member States or international organisations is ensured in order to draw a maximum benefit and to achieve complementarity.
The future EMN should be seen as part of the more general reorganisation and rationalisation of information exchange instruments that is ongoing in the area of immigration and asylum. This includes instruments such as the forthcoming proposal on a mutual information system, practical cooperation in the field of asylum or the recently adopted regulation on Community statistics on migration and international protection[9].
In order to achieve synergies, it is necessary to network with other bodies that carry out activities related to the ones the EMN pursues. Certain Community-level actors are concerned with similar information collection and analysis, such as the future Fundamental Rights Agency[10] and CARIM[11] in particular. The Fundamental Rights Agency will be an independent centre of expertise concerning fundamental rights issues. Its main tasks will include data collection, analysis, advice, networking and awareness-raising. The CARIM project offers an instrument for observing, analysing and forecasting migratory movements, their causes and consequences that originate from transit through or are destined for the countries of the Barcelona process. These actors have a different mandate and brief than the EMN, as their focus on migration is mostly concerned with migrants’ rights, or relates to specific countries. This decreases risk of overlap. Still there is a need for a future EMN to create strong and structured links with them.
As regards relations with institutional stakeholders, such as the European Parliament, the Economic and Social Committee and the Committee of the Regions, these should be, together with the Member States and the Commission, the principal beneficiaries of a future EMN’s activities. One may also consider whether these stakeholders should be able to ask for specific information to be then provided by the NCP’s, for example when preparing its opinions on proposals for legislation.
How can close cooperation with stakeholders be assured? How can a future structure build on and complement existing national and international bodies in the field of migration and asylum? |
- 5.4. Form and structure
This section addresses the question of which structures could enable the implementation of the objective and the range of possible tasks, and which advantages and disadvantages each structure would imply.
Depending on the objective, concrete tasks to be allocated and resources available, a future EMN could take different forms. Amongst them, two different structures could be considered, the fundamental choices being between a network attached to and working under the responsibility of the Commission, or a network selected and operated by an independent Community agency.
Option 1: Network attached to the Commission
A future EMN could consist of a network of national contact points. The Commission would assume responsibility for the overall direction, programming and further development of the network’s activities. A legal basis would be required that would establish the future EMN and set out its main objectives, including the criteria that NCPs should fulfil. This could take the form of a Council decision.
Under this option, it would be up to the Commission to designate appropriate management structures. This could imply the appointment of a private or public coordinating body following an open call for tenders.
As regards the selection of NCPs, one could either envisage that they are selected by Member States, in accordance with the criteria set out in the legal basis, or by the Commission, through a call for proposals.
Attaching the network to the Commission would guarantee that the Commission assumes the overall political responsibility for its activities. It would allow a seamless move from the current EMN and could also be considered as a transitional phase before shifting to a more permanent and institutionalised structure, should there prove to be a need. However, efficient management would have to be ensured, and one may question in this respect whether outsourcing of tasks to a contractor could provide sufficient continuity. Indeed, the Commission’s procurement rules foresee a certain maximum duration for such contracts.
Option 2: Agency
A Community agency could be considered as a longer term option, depending not only on the further development of the common immigration and asylum policies but also on the Financial Perspectives, taking into account the limited budget available. It should be underlined that an agency would require a legal basis to establish it; it would also imply a management board with representatives from Member States and the Commission.
Such a stable structure would provide a longer term perspective in its activities and would also allow a wider range of tasks to be fulfilled. It could, therefore, be in a position to provide more effectively the information required.
If such an agency were to be considered as appropriate for fulfilling the envisaged new tasks, there would have to be sufficient justification for its need and for its potential continuity. This would imply an impact assessment in order to analyse fully the implications of such an agency. In this context, the use of other agencies - existing or future -in the area of Justice and Home Affairs could be examined.
Role and status of national contact points
Whatever option will be chosen, and if the overall mandate as outlined above is accepted, this results in certain implications for the role and status of NCPs, as the information gathering and analysis activities at national level will be carried out by them.
NCPs therefore have to be in a position to deliver this information, i.e. to broadly cover their respective Member State, and they have to be able to ensure the permanent updating of the database. A core function is to mobilise and be at the centre of a national network of relevant actors in the field, such as researchers and research institutions, public and governmental authorities, and NGOs, that would contribute to the activities. To that end, NCPs should be independent from governments, but at the same time establish good relations to public bodies, in order to have access to public sector information.
In this respect, in order that NCPs are in a position to fulfil these tasks and to achieve a certain level of homogeneity among them, a set of minimum requirements in terms of overall experience in the field, staffing, capacity, IT and other equipment, would have to be specified.
Which structure should be put in place for the next stage of the EMN to achieve the overall objective, and why? Which role and status is needed for the NCPs? How best should the national networks be structured? |
- 6. CONCLUSION
IN THIS GREEN PAPER, THE COMMISSION HAS TRIED TO OUTLINE THE MAIN ISSUES AT STAKE AND HAS PUT FORWARD A NUMBER OF OPTIONS THAT SHOULD BE CONSIDERED WHEN SETTING UP A FUTURE EMN STRUCTURE.
The primary aim of the Green Paper is to call for reactions and launch an in-depth consultation among all relevant stakeholders. To that end, the Commission would like to receive contributions by 28 January 2006, to be sent by e-mail to the following address:
jls-migration-network@cec.eu.int
Contributions received will be published on the Commission’s website in the language in which they were submitted and with the authors’ names, unless they indicate their wish to remain anonymous or request that their entire contribution be treated as confidential.
ANNEXES
ANNEX 1: NATIONAL CONTACT POINTS AND COORDINATOR
National Contact Points:
Austria:
International Organisation for MigrationNibelungengasse 13/4,1010 Vienna, Austriancpaustria@iom.intwww.emn.at
Belgium:
SPF Intérieur, DG Office des Etrangers,59b Chaussée d´Anvers1000 Bruxelles, Belgiumemn@ibz.fgov.bewww.ibz.fgov.be
Czech Republic:
Ministry of the InteriorUnit of International RelationsAnd Information on Countries of OriginDepartment for Asylum and Migration PoliciesMV – P.O. box 21/Oam170 34 Prague 7, Czech Republicopu@mvcr.cz
Finland:Statistics FinlandP.O. Box 4B00022 Helsinki, Finlandwww.stat.fi
France:Observatoire des statistiques de l’immigrationet de l’intégration du Haut Conseil à l’Intégration (HCI-OSII)35, rue St. Dominique75007 Paris, France
Germany:Bundesamt für Migration und Flüchtlinge (BAMF)Frankenstraße 21090461 Nürnberg, Germanywww.bamf.de
Greece:Centre of Planning and Economic Research (KEPE)22 Hippokratous st.10680 Athens, Greecekepe@kepe.grwww.kepe.gr
Ireland:Economic and Social Research Institute (ESRI)4 Burlington Rd., Dublin 4, Irelandwww.esri.ie
Italy:Immigrazione Dossier Statistico (IDOS)of Caritas Diocesana die RomaViale Baldelli 4100147 Roma, Italywww.emnitaly.it
Netherlands:Immigration and Naturalisation Service,Information and Analysis Centre (INDIAC)Dr. H. Colijnlaan 3412283 XL Rijswijk, The Netherlandsemn@ind.minjus.nlwww.ind.nl
Portugal:Serviço de Estrangeiros e FronteirasRua Conselheiro José Silvestre Ribeiro 41649-007 Lisboa, Portugalemn@sef.ptwww.sef.pt
Spain:Observatorio Permanente de la InmigraciónC/Amador de los Rios 728010 Madrid, Spainopi@amador.mir.eswww.imsersomigracion.upco.es
Sweden:Statistics Sweden/Swedish Migration Board701 89 Örebro, Swedenwww.scb.se
United Kingdom:Immigration Research and Statistics Service, Home Office1305 Apollo House, 36 Wellesley Road,Croydon CR9 3RR, United Kingdomwww.homeoffice.gov.uk
Coordination Team:
Berlin Institute for Comparative Social Research (BIVS)Schliemannstr. 23,10437 Berlin, Germanyemn@emz-berlin.de
www.emz-berlin.de
Technical University BerlinComputation and Information StructuresSecr. E-N7, Einsteinufer 17,10587 Berlin, Germanyhttp:/cis.cs.tu-berlin.de/emn/web
ANNEX 2: REFERENCES
STUDIES
1996 Feasibility Study for a European Migration Observatory
2005 EMN First Activities report covering 2004
2005 Report on the Evaluation of the Activities of the European Migration Network
Website of the current EMN
www.european-migration-network.org
[1] ‘Feasibililty study for a European Migration Observatory’, 1996.
[2] ‘Evaluation of the Activities of the European Migration Network’, 2005.
[3] COM 2005 (375) final of 26 August 2005.
[4] For a more complete account see the EMN’s first annual activities report (2005), available at http://www.european-migration-network.org/
[5] 9 NCPs contributed national reports of which the scientific coordinator produced a synthesis report that draws together, summarises and evaluates the findings of the national studies.
[6] Available on the website of the Commission’s Directorate General Justice, Freedom and security at http://europa.eu.int/comm/justice_home/doc_centre/asylum/statistical/doc_annual_report_2001_en.htm
[7] These include the EMN website at http://www.european-migration-network.org, a general information leaflet , an EMN quarterly newsletter , and the organisation of enlarged EMN meetings in order to present research results to a wider audience.
[8] Migration research undertaken under the Community RTD Framework Programmes would be taken into account.
[9] COM 2005 (375) final of August 2005.
[10] Commission Proposal for a Council Regulation establishing a European Union Agency for Fundamental Rights, COM (2005)280 final of 30 June 2005. The proposed Regulation will extend the mandate of the current European Monitoring Centre on Racism and Xenophobia (EUMC) as from 2007. While the EUMC activities focus on racism, xenophobia and anti-Semitism, the Agency will deal more broadly with fundamental rights.
[11] Euro-Mediterranean Consortium for Applied Research on International Migration, www.carim.org
