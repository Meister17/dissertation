Commission Regulation (EC) No 1474/2005
of 9 September 2005
determining the extent to which applications for import rights lodged in respect of the quota for live bovine animals of a weight exceeding 160 kg and originating in Switzerland, provided for in Regulation (EC) No 1218/2005, can be accepted
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1254/1999 of 17 May 1999 on the common organisation of the market in beef and veal [1],
Having regard to Commission Regulation (EC) No 1218/2005 of 28 July 2005 laying down detailed rules for the application of an import tariff quota for live bovine animals of a weight exceeding 160 kg and originating in Switzerland provided for in Council Regulation (EC) No 1182/2005 [2], and in particular in the first sentence of Article 4(2) thereof,
Whereas:
(1) Article 1(1) of Regulation (EC) No 1218/2005 fixes at 2300 head the quantity of the quota in respect of which Community importers can lodge an application for import rights in accordance with Article 3 of that Regulation.
(2) Since the import rights applied for exceed the available quantity referred to in Article 1(1) of Regulation (EC) No 1218/2005, a unique reduction coefficient should be fixed for quantities tendered,
HAS ADOPTED THIS REGULATION:
Article 1
Each application for import rights lodged in accordance with Article 3(3) of Regulation (EC) No 1218/2005 shall be accepted at a rate of 74,074 % of the import rights applied for.
Article 2
This Regulation shall enter into force on 10 September 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 9 September 2005.
For the Commission
J. M. Silva Rodríguez
Director-General for Agriculture and Rural Development
[1] OJ L 160, 26.6.1999, p. 21. Regulation as last amended by Regulation (EC) No 1782/2003 (OJ L 270, 21.10.2003, p. 1).
[2] OJ L 199, 29.7.2005, p. 39.
--------------------------------------------------
