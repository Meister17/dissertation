*****
COMMISSION REGULATION (EEC) No 3123/85
of 6 November 1985
amending Commission Regulation (EEC) No 2237/77 on the form of farm return to be used for the purpose of determining incomes of agricultural holdings
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to the Act of Accession of the Kingdom of Spain and the Portugese Republic to the European Communities and in particuliar article 396 thereof,
Whereas Commission Regulation (EEC) No 2237/77 (1), as last amended by Regulation (EEC) No 3272/82 (2), must be supplemented by fixing the first accounting year from which that Regulation is to apply to Spain and Portugal; whereas certain other data must also be added,
HAS ADOPTED THIS REGULATION:
Article 1
Article 2 of Commission Regulation (EEC) No 2237/77 is supplemented as follows:
'These provisions shall apply for the first time in Spain and Portugal to the accountancy data of the 1986 accounting year, beginning during the period between 1 January and 1 July 1986.'
Article 2
In Annex II, Title II, under G 'Land and buildings, deadstock and circulating capital' - heading 'Depreciation of machinery and equipment' footnote 1 is supplemented as follows:
'Pta 15 000; Esc 15 000'.
Article 3
Annex II, Title II, under I 'Value Added Tax (VAT)' - heading 107 'VAT system' is supplemented as follows:
1.2 // SPAIN // Code No // Normal system // 1 // Simplified system // 2 // Agricultural system // 3 // PORTUGAL // // VAT not applicable // 0'.
Article 4
This Regulation shall enter into force on 1 January 1986 subject to the entry into force of the Treaty concerning the Accession of Spain and Portugal.
It shall apply from the 1986 accounting year.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 6 November 1985.
For the Commission
Frans ANDRIESSEN
Vice-President
(1) OJ No l 263, 17. 10. 1977, p. 1.
(2) OJ No L 347, 7. 12. 1982, p. 10.
