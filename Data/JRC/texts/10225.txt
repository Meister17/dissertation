Commission notice pursuant to Article 4(1)(a) of Council Regulation (EEC) No 2408/92
Amendment of public service obligations imposed on certain scheduled air services within the Autonomous Region of the Azores
(2006/C 49/09)
(Text with EEA relevance)
1. Pursuant to Article 4(1)(a) of Regulation (EEC) No 2408/92, the Government of the Autonomous Region of the Azores has decided to impose, as from 1 April 2006, public service obligations on the scheduled air services operated on the following routes:
- Ponta Delgada — Santa Maria — Ponta Delgada
- Ponta Delgada — Terceira — Ponta Delgada
- Ponta Delgada — Horta — Ponta Delgada
- Ponta Delgada — Pico — Ponta Delgada
- Ponta Delgada — São Jorge — Ponta Delgada
- Ponta Delgada — Flores — Ponta Delgada
- Terceira — Graciosa — Terceira
- Terceira — São Jorge — Terceira
- Terceira — Pico — Terceira
- Terceira — Horta — Terceira
- Terceira — Flores — Terceira
- Terceira — Corvo — Terceira
- Horta — Flores — Horta
- Horta — Corvo — Horta
- Corvo — Flores — Corvo
2. The public service obligations are as follows:
Minimum frequency:
- On the Ponta Delgada — Santa Maria — Ponta Delgada route, nine round trips per week from November to March, ten round trips per week from April to June and from September to October and fourteen round trips per week in July and August, with at least one trip per day throughout the year.
- On the Ponta Delgada — Terceira — Ponta Delgada route, twenty-two round trips per week from September to June and twenty-six round trips per week in July and August, with at least two trips per day throughout the year.
- On the Ponta Delgada — Horta — Ponta Delgada route, six round trips per week from November to March, eleven round trips per week from April to June and from September to October and seventeen round trips per week in July and August.
- On the Ponta Delgada — Pico — Ponta Delgada route, two round trips per week from November to March, seven round trips per week from April to June and from September to October and nine round trips per week in July and August.
- On the Ponta Delgada — São Jorge — Ponta Delgada route, three round trips per week from April to June and from September to October and six round trips per week in July and August.
- On the Ponta Delgada — Flores — Ponta Delgada route, two round trips per week from April to June and from September to October and four round trips per week in July and August.
- On the Terceira — Graciosa — Terceira route, seven round trips per week from September to June and eight round trips per week in July and August, with at least one trip per day from Monday to Saturday in the period from October to May and one trip per day from Monday to Sunday from June to September.
- On the Terceira — São Jorge — Terceira route, eight round trips per week from September to June and nine round trips per week in July and August, with at least one trip per day throughout the year.
- On the Terceira — Pico — Terceira route, seven round trips per week from September to June and eight round trips per week in July and August, with at least one trip per day throughout the year.
- On the Terceira — Horta — Terceira route, eleven round trips per week from September to June and fourteen round trips per week in July and August, with at least one trip per day throughout the year.
- On the Terceira — Flores — Terceira route, from Monday to Friday, two round trips per week from September to June and four round trips per week in July and August.
- On the Terceira — Corvo — Terceira route, from Monday to Friday, three round trips per week in July and August. They may be combined with services to Horta or Flores.
- On the Horta — Flores — Horta route, four round trips per week from Monday to Saturday from October to May and five round trips per week, including one on Sunday, from June to September.
- On the Horta — Corvo — Horta route, three round trips per week throughout the year on alternate days from Monday to Friday. They may be combined with services to Flores.
- On the Corvo — Flores — Corvo route, two round trips per week throughout the year on non-consecutive days from Monday to Friday. They may be combined with the other services to Corvo and Flores.
Linking Ponta Delgada, Terceira, Horta and Corvo flights to Flores should enable daily services to that island from Monday to Saturday from September to June and from Monday to Sunday in July and August.
Every week at least two flights must be operated to and from each island, connecting with flights from and to outside the region. A weekly service must also be operated between each island and the others. Where there is no obligation to operate a daily service to a particular island, a weekly service from and to outside the region must be operated, and a service to São Miguel, Terceira and Faial.
Capacity:
The following minimum weekly capacity must be offered:
- On the Ponta Delgada — Santa Maria — Ponta Delgada service, 1050 seats from November to March, 1200 seats from April to June and September to October and 1680 seats in July and August.
- On the Ponta Delgada — Terceira — Ponta Delgada service, 2590 seats from September to June and 3070 seats in July and August.
- On the Ponta Delgada — Horta — Ponta Delgada service, 620 seats from November to March, 1270 seats from April to June and September to October and 1960 seats in July and August.
- On the Ponta Delgada — Pico — Ponta Delgada service, 200 seats from November to March, 780 seats from April to June and September to October and 1000 seats in July and August.
- On the Ponta Delgada — São Jorge — Ponta Delgada service, 330 seats from April to June and September to October and 670 seats in July and August.
- On the Ponta Delgada — Flores — Ponta Delgada service, 190 seats from April to June and September to October and 380 seats in July and August.
- On the Terceira — Graciosa — Terceira service, 700 seats from September to June and 910 seats in July and August.
- On the Terceira — São Jorge — Terceira service, 820 seats from September to June and 1040 seats in July and August.
- On the Terceira — Pico — Terceira service, 780 seats from September to June and 890 seats in July and August.
- On the Terceira — Horta — Terceira service, 1290 seats from November to March, 1320 seats from April to June and September to October and 1680 seats in July and August.
- On the Terceira — Flores — Terceira service, 190 seats from September to June and 380 seats in July and August.
- On the Terceira — Corvo — Terceira service, 60 seats in July and August.
- On the Horta — Flores — Horta service, 380 seats from September to June and 480 seats in July and August.
- On the Horta — Corvo — Horta service, 60 seats throughout the year.
- On the Corvo — Flores — Corvo service, 40 seats throughout the year.
In the event of temporary suspension of the air services due to unforeseen circumstances, to cases of force majeure and for other reasons, the programmed capacity must be increased by at least 60 % from the moment when services can be resumed until the traffic backlog accumulated during the suspension of services has been cleared.
Where average seat sales on a particular route exceed 70 % in any IATA season, the minimum capacity to be offered in the next corresponding season must be increased by at least enough to maintain a 70 % average.
Where the number of passengers on the waiting list exceeds 5 % of the weekly capacity on a particular route, or is equivalent to the capacity of the aircraft most frequently used on that route, whichever is less, an extra service will be operated, if the passengers concerned are unable to continue their journey on flights scheduled for the following 48 hours.
Additional services will be operated to meet the extra demand created by religious holidays and sporting and cultural events on the islands. For each additional service seat sales must not be less than 70 %, both ways.
Freight, including mail, is to be carried using the capacity of the aircraft used to carry passengers.
Type of aircraft used:
The services must be operated using aircraft which have been duly certified for passenger flights and meet the technical and operational requirements which allow them to land at aerodromes with night flight restrictions. Services to and from the aerodromes and airports of the Autonomous Region of the Azores must be operated in accordance with the conditions laid down in the "Aeronautical Information of Portugal" (AIP) and the "Manual Pilot Civil" (MPC).
The aircraft used must have a certified minimum capacity of 18 seats.
Fares:
The fare structure must include:
(a) A fully flexible economy reference fare, not exceeding the values set out in Annex A;
(b) A range of special fares with levels and conditions tailored to different demand segments:
- Senior Citizens' fare,
- Youth fare,
- APEX.
(c) Reduced fares for citizens resident in the Autonomous Region of the Azores. The maximum fares shall be those set out in Annex B.
(d) Reduced fares for students living in the Autonomous Region of the Azores, for journeys within the region between their island of residence and the island where their educational establishment is located. These fares shall be 40 % less than the standard fares.
(e) A reduced fare for children which shall be 50 % of the adult fare.
(f) A reduced fare for babies which shall be 10 % of the adult fare.
In addition to the above, special fares may be offered provided they have been approved by the Regional Department of Economic Affairs.
Freight tariffs must be approved by the Regional Department of Economic Affairs.
These fares shall be revised automatically every year on 1 April, beginning in 2007, based on the rate of inflation for the previous year, as published by the Portuguese Government in the Grandes Opções do Plano (Major Planning Options) and as notified, by 28 February, to the carriers operating the routes in question. The Commission will be notified of the new fares, which shall enter into force only after publication in the Official Journal of the European Union.
The fares given in Annexes A and B are standard from the point of departure to the point of arrival with any intermediate stage — provided that the route is listed in Annex C — even if the overall route involves more than one carrier. In the latter case, the carriers providing services between islands shall apportion earnings according to the number of miles travelled. Carriers shall also agree passenger transfer conditions, specifying connection times and reciprocal arrangements for transferring tickets and luggage to the final destination, such that passengers are not unduly penalised. The conditions must be submitted to the Regional Department of Economic Affairs for approval.
Continuity of service and punctuality:
Except in cases of force majeure, the number of flights cancelled for reasons not directly attributable to the carrier must not exceed 2 % of the number of flights scheduled in any IATA season.
Except in cases of force majeure, delays of more than 15 minutes directly attributable to the carrier may not affect more than 25 % of the flights.
The planned services must be guaranteed throughout at least one calendar year and, except in the cases mentioned above, may be suspended only with six months notice.
Timetables:
Flights should be distributed as evenly as possible throughout the week and the time slots available.
Unless there are restrictions on operating timetables or operational restrictions on aerodromes and airports, flights must take place between 06.00 local time at the place of departure and 24.00 local time at the destination.
Flight bookings:
Flights must be marketed using a computerised reservation system but other distribution channels can also be used which, taking account of the nature of the services, provide sufficient information.
A no-show penalty may be set, not exceeding 10 % of the relevant economy class reference fare.
Mail service:
Mail sent by the universal postal service operator must satisfy the quality standards of the universal service and other applicable legal requirements.
3. Given the special importance of the routes in question, and the exceptional nature of the service continuity requirements, Community carriers are hereby informed of the following conditions:
- Carriers wishing to operate one or more of the services subject to these obligations must first submit a financial plan which demonstrates their ability to operate the services profitably for one year in accordance with the obligations;
- All carriers holding a valid operating license issued by a Member State in accordance with Council Regulation (EEC) No 2407/92 on licensing of air carriers, and an appropriate air operator's certificate can operate these services;
- Carriers wishing to operate the services in question must prove that their air operator certificate covers the main and reserve aircraft and the associated human resources needed for the scheduled services they propose to provide, without prejudice to their right, exceptionally, to subcontract to other carriers in order to deal with unforeseen developments;
- Carriers must prove that, by the scheduled starting date, the human, logistical and technical facilities have been set in place, or contractually agreed, on each island in the Azores to enable the public to purchase individual tickets either directly from the carrier or from an authorised agent;
- Within the Autonomous Region of the Azores, carriers must have their own, or contracted, technical organisations available (services for operating flights, and engineering and maintenance services for aircraft and associated equipment), which have been previously certified by the Instituto Nacional da Aviação Civil (Portuguese Civil Aviation Institute) in accordance with the regulations in force;
- Given the special nature of these services, air carriers will have to demonstrate that most of the cabin crew on the flights they operate in the Azores can speak and understand Portuguese and, in addition, that most of the technical crew on services to the island of Corvo can speak and understand Portuguese;
- Carriers may establish commercial agreements such as "interline" or "codeshare";
- Suspension of any of the services in question, without the advance notice specified in the public service obligations set out above, or failure to fulfil these obligations, shall be punishable by administrative penalties and fines;
- Carriers may apply to operate one or more services without claiming any compensation, and without demanding exclusive rights to the route, within one month of the date of publication of this notice in the Official Journal of the European Union;
- If several carriers apply to operate the same route without claiming compensation or demanding exclusive rights, then all the carriers must comply strictly with all the public service requirements imposed, except the frequency and capacity of services, for which the joint contributions of all the applicants will be taken into consideration. To that end, each carrier will be required to provide a minimum frequency and capacity determined by dividing the frequency and capacity for the route set out in paragraph 1 of this notice equally by the number of rival proposals.
The public service obligations set out in this notice may be amended or adjusted, for reasons of public interest, by means of an agreement to be drawn up between the carriers and the Regional Department of Economic Affairs.
Community carriers are also informed that the Regional Air and Maritime Transport Department of the Regional Department of Economic Affairs of the Regional Government of the Azores will monitor compliance with the public service obligations imposed.
Applications (proposals) must be delivered to the Regional Air and Maritime Transport Department (Direcção Regional dos Transportes Aéreos e Marítimos) of the Regional Department of Economic Affairs (Secretaria Regional da Economia) of the Government of the Autonomous Region of the Azores (Governo da Região Autónoma dos Açores).
--------------------------------------------------
ANNEX A
Standard Economy Fare (RT) (in EUR)
CVU: Corvo; FLW: Flores; GRW: Graciosa; HOR: Horta; PDL: Ponta Delgada; PIX: Pico; SJZ: São Jorge; SMA: Santa Maria; TER: Terceira
| CVU | FLW | GRW | HOR | PDL | PIX | SJZ | SMA | TER |
CVU | | 50 | 170 | 104 | 170 | 170 | 170 | 170 | 170 |
FLW | 50 | | 170 | 104 | 170 | 170 | 170 | 170 | 170 |
GRW | 170 | 170 | | 170 | 170 | 170 | 170 | 170 | 104 |
HOR | 104 | 104 | 170 | | 170 | 170 | 170 | 170 | 168 |
PDL | 170 | 170 | 170 | 170 | | 170 | 170 | 104 | 170 |
PIX | 170 | 170 | 170 | 170 | 170 | | 170 | 170 | 168 |
SJZ | 170 | 170 | 170 | 170 | 170 | 170 | | 170 | 104 |
SMA | 170 | 170 | 170 | 170 | 104 | 170 | 170 | | 170 |
TER | 170 | 170 | 104 | 168 | 170 | 168 | 104 | 170 | |
--------------------------------------------------
ANNEX B
Fare for Residents (RT) (in EUR)
CVU: Corvo; FLW: Flores; GRW: Graciosa; HOR: Horta; PDL: Ponta Delgada; PIX: Pico; SJZ: São Jorge; SMA: Santa Maria; TER: Terceira
| CVU | FLW | GRW | HOR | PDL | PIX | SJZ | SMA | TER |
CVU | | 40 | 142 | 80 | 142 | 142 | 142 | 142 | 142 |
FLW | 40 | | 142 | 80 | 142 | 142 | 142 | 142 | 142 |
GRW | 142 | 142 | | 142 | 142 | 142 | 142 | 142 | 80 |
HOR | 80 | 80 | 142 | | 142 | 142 | 142 | 142 | 136 |
PDL | 142 | 142 | 142 | 142 | | 142 | 142 | 80 | 142 |
PIX | 142 | 142 | 142 | 142 | 142 | | 142 | 142 | 136 |
SJZ | 142 | 142 | 142 | 142 | 142 | 142 | | 142 | 80 |
SMA | 142 | 142 | 142 | 142 | 80 | 142 | 142 | | 142 |
TER | 142 | 142 | 80 | 136 | 142 | 136 | 80 | 142 | |
--------------------------------------------------
ANNEX C
Fares for services between islands
The routes below are for travel between two islands in the Autonomous Region of the Azores calling at one or more intermediate points (with or without stopovers) for the standard fare from point of departure to destination.
All the routes listed in one direction only may also be taken in the other direction.
CVU-FLW [1]-HOR [1]-TER [1]-GRW
CVU-FLW-HOR
CVU-FLW-HOR-TER-PDL
CVU-FLW [1]-HOR [1]-TER [1]-PIX
CVU-FLW-HOR-TER-PDL-SMA
CVU-FLW [1]-HOR [1]-TER [1]-SJZ
CVU-FLW-HOR-TER
FLW-CVU [1]-HOR [1]-TER [1]-GRW
FLW-CVU [1]-HOR
FLW-HOR-TER-PDL
FLW-CVU [1]-HOR [1]-TER [1]-PDL
FLW-CVU [1]-HOR [1]-TER [1]-PIX
FLW-HOR-TER-PDL-SMA
FLW-CVU [1]-HOR [1]-TER [1]-PDL [1]-SMA
FLW-CVU [1]-HOR [1]-TER [1]-SJZ
FLW-HOR-TER
FLW-CVU [1]-HOR [1]-TER
GRW-TER [1]-HOR
GRW-TER-PDL
GRW-TER [1]-PIX
GRW-TER-PDL-SMA
GRW-TER [1]-SJZ
HOR-TER-PDL
HOR-TER [1]-PIX
HOR-TER-PDL-SMA
HOR-TER [1]-SJZ
PDL-TER-SJZ
PDL-TER-PIX
PIX-TER-PDL-SMA
PIX-TER [1]-SJZ
SMA-PDL-TER-SJZ
SMA-PDL-TER
[1] No stopovers allowed.
--------------------------------------------------
