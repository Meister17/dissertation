JOINT ACTION of 16 December 1996 adopted by the Council on the basis of Article K.3 of the Treaty on European Union concerning a uniform format for residence permits (97/11/JHA)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty on European Union, and in particular Article K.3 (2) (b) thereof,
Whereas Article K.1 (3) of the Treaty provides that immigration policy and policy regarding nationals of third countries constitute a matter of common interest;
Whereas it is desirable to harmonize the format of residence permits issued by Member States to third country nationals;
Whereas it is essential that the uniform format for residence permits should contain all the necessary information and meet very high technical standards, in particular as regards safeguards against counterfeiting and falsification; whereas it must also be suited to use by all the Member States and bear universally recognizable security features which are clearly visible to the naked eye;
Whereas this Joint Action only lays down such specifications as are not secret; whereas these specifications need to be supplemented by further specifications which are to remain secret in order to prevent counterfeiting and falsifications and which may not include personal data or references to such data; whereas the Council should lay down these supplementary specifications;
Whereas, to ensure that the information referred to is not made available to more persons than necessary, it is also essential that each Member State should designate not more than one body having responsibility for printing the uniform format for residence permits, with Member States remaining free to change the body, if need be; whereas, for security reasons, each Member State must communicate the name of the competent body to the Council and the Commission;
Whereas, with regard to the personal data to be entered on the uniform format for residence permits in accordance with the Annex hereto, compliance should be ensured with applicable data-protection provisions,
HAS ADOPTED THIS JOINT ACTION:
Article 1
Residence permits issued by Member States to third country nationals shall be in a uniform format and provide space for the information set out in the Annex hereto.
The uniform format can be used as a sticker or a stand-alone document.
Each Member State may add in the relevant space of the uniform format any information of importance regarding the nature of the permit and the person concerned, including information as to whether or not the person is permitted to work.
Article 2
1. The technical specifications for incorporating the information set out in the Annex on the uniform format for residence permits shall be laid down by the Council without delay.
Further technical specifications which render the residence permit difficult to counterfeit or falsify shall also be laid down by the Council without delay. These specifications shall be secret and shall not be published. They shall be made available only to bodies designated by the Member States as responsible for printing and to persons duly authorized by a Member State.
2. Each Member State shall designate one body having responsibility for printing residence permits. It shall communicate the name of that body to the Council and the Commission. The same body may be designated by two or more Member States for this purpose. Each Member State shall be entitled to change its designated body. It shall inform the Council and the Commission accordingly.
3. Each Member State shall inform the Council and the Commission of the competent authority or authorities for issuing residence permits.
Article 3
1. Without prejudice to any more extensive provisions applicable concerning data protection, an individual to whom a residence permit is issued shall have the right to verify the personal particulars entered on the residence permit and, where appropriate, to ask for any corrections or deletions to be made.
2. No information in machine-readable form shall be given on the uniform format for residence permits unless it also appears in the boxes described in points 10 and 11 of the Annex, or unless it is mentioned in the relevant travel document.
Article 4
For the purpose of this Joint Action, 'residence permit` shall mean any authorization issued by the authorities of a Member State allowing a third country national to stay legally on its territory, with the exception of:
- visas,
- permits issued for a stay whose duration is determined by national law but which may not exceed six months,
- permits issued pending examination of an application for a residence permit or for asylum.
Article 5
Where Member States use the uniform format for residence permits for purposes other than those covered by Article 4, appropriate measures must be taken to ensure that confusion with the residence permit referred to in Article 4 is not possible.
Article 6
This Joint Action applies to residence permits issued to third country nationals, with the exception of:
- members of the families of citizens of the Union exercising their right to free movement,
- nationals of Member States of the European Free Trade Association party to the Agreement on the European Economic Area and members of their families exercising their right to free movement.
Article 7
This Joint Action shall be published in the Official Journal, and shall enter into force the day after that of its publication.
The Member States shall apply Article 1 no later than five years after the adoption of the measures referred to in Article 2 (1). However, the validity of authorizations granted in documents already issued shall not be affected by the introduction of the uniform format for residence permits, unless the Member State concerned decides otherwise.
Done at Brussels, 16 December 1996.
For the Council
The President
M. D. HIGGINS
ANNEX
>REFERENCE TO A GRAPHIC>
Description
The document will be produced either as a sticker, if possible in ID 2 format, or as a stand-alone document.
1. The title of the document (Residence permit) shall appear in this space in the language(s) of the issuing Member State.
2. The document number - with special security features and preceded by an identification letter - shall appear in this space.
The section to be completed has nine sub-sections:
3. first sub-section
Name: this sub-section shall contain surname and forename(s) in that order.
4. second sub-section
Valid until: this sub-section shall indicate the relevant expiry date, or, where appropriate, a word to indicate unlimited validity.
5. third sub-section
Place/date of issue: this sub-section shall indicate the place and date of issue of the residence permit.
6. fourth sub-section
Type of permit: this sub-section shall indicate the specific type of residence permit issued to the third country national by the Member State.
7. fifth to ninth sub-section
Remarks: in the fifth to ninth sub-sections Member States may enter details and indications for national use necessary with regard to their rules on third country nationals including indications relating to any permission to work.
8. Date/Signature/Authorization: here - if necessary - the signature and seal of the issuing authority and/or the holder may appear.
9. In this space the printed area shall contain the national emblem of the Member State to distinguish the residence permit and provide a safeguard of its national origin.
10. This box shall be reserved for the machine-readable area. The machine-readable area shall conform to ICAO guidelines.
11. The printed text of this box shall contain a character indicating exclusively the respective Member State. This character may not affect the technical features of the machine-readable zone.
12. This space shall contain a metallized latent image effect including the country code of the Member State.
13. This space shall contain an OVD (kinegram or equivalent sign).
14. If the residence permit is produced as a stand-alone document, an identity photograph shall be affixed in this space and secured with OVD film (kinefilm or equivalent security laminate).
15. The following additional information boxes shall be provided on the back in the case of a stand-alone document:
- date/place of birth,
- nationality,
- sex,
- remarks.
The address of the permit holder may also be indicated.
Uniform format for residence permits (98/C 193/01)
The pictures displayed below are the result of the Council Decision of 18 December 1997 on the technical specifications referred to in Article 2(1) of the Joint Action of 16 December 1996, adopted by the Council on the basis of Article K.3 of the Treaty on European Union, on a uniform format for residence permits (97/11/JHA) (1).
At this juncture only specimens are published as examples: Austria's specimen for the adhesive stick-in version (picture No 1) and specimens from the Kingdom of the Netherlands for the card format version ID1 (picture No 2) and ID2 (picture No 3).
Specimens from the other Member States will be published at a later stage after expiry of the time limit provided for in Article 1 of the Joint Action of 15 December 1996 adopted by the Council on the basis of Article K.3 of the Treaty on European Union, on a uniform format for residence permits (97/11/JHA).
PICTURE No 1
>REFERENCE TO A FILM>
PICTURE No 2
>REFERENCE TO A FILM>
PICTURE No 3
>REFERENCE TO A FILM>
(1) OJ L 7, 10.1.1997, p. 1.
