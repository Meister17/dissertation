COMMISSION DECISION of 10 February 1999 amending the Decision on the Liaison Group on the Elderly (notified under document number C(1999) 211) (1999/141/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Whereas, in the light of developments at Community level, it is necessary to adjust the membership of the Group set up by Commission Decision 91/544/EEC (1), as amended by Decision 93/417/EEC (2); whereas at the same time, in the interests of administrative efficiency, the terms of office of the Chairman and of the Members of the Group should be reduced,
HAS DECIDED AS FOLLOWS:
Sole Article
Decision 91/544/EEC is amended as follows:
1. in Article 3(2), '25 members` is replaced by '24 members`;
2. Article 4(3) is amended as follows:
(a) in each case, 'five seats` is replaced by 'four seats`;
(b) the following indent is added:
'- ESCU-European Senior Citizens Union: four seats`;
3. in Article 5(1), '18 months` is replaced by '12 months`;
4. in Article 7(1), '18 months` is replaced by '12 months`;
5. in the Annex, the following indent is added:
'- ESCU-European Senior Citizens Union`.
Done at Brussels, 10 February 1999.
For the Commission
Pádraig FLYNN
Member of the Commission
(1) OJ L 296, 26. 10. 1991, p. 42.
(2) OJ L 187, 29. 7. 1993, p. 60.
