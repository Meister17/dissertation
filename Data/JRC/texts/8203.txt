DECISION OF THE EXECUTIVE COMMITTEE
of 28 April 1999
on general principles governing the payment of informers
(SCH/Com-ex (99)8 Rev. 2)
THE EXECUTIVE COMMITTEE,
Having regard to Article 132 of the Agreement implementing the Schengen Convention,
Having regard to Articles 70 to 76 of the abovementioned Convention,
HAS DECIDED AS FOLLOWS:
The Executive Committee hereby approves the Decision of the Central Group of 22 March 1999 on the general principles governing payment of informers (SCH/C (99)25, SCH/Stup (98)72 Rev.).
Luxembourg, 28 April 1999.
The Chairman
C. H. Schapper
Subject: General principles governing payment of informers
SCH/Stup (98)72 Rev. 2
1. Introduction
International drug-related crime, other forms of serious crime and organised crime are a growing phenomenon which also affects the Schengen States.
Criminals, particularly those involved in illicit trafficking in drugs, are adopting an increasingly professional approach and adapting flexibly to new geopolitical, legal, economic and technological circumstances, using entrepreneurial structures and interweaving illegal business dealings with legitimate commercial activities. They are also prepared to act ruthlessly to achieve their objectives, threatening or resorting to violence against people and property and seeking to manipulate politicians, businessmen and public officials, their main motivation being the maximisation of profits and the pursuit of power.
The modus operandi of criminal networks involved in drugs is characterised by specialisation, division of labour and compartmentalisation. Illegal profits are "reinvested" in new criminal activities or injected into legitimate commercial activities in order to gain influence and create a criminal monopoly.
Even special investigative methods are becoming increasingly ineffective. Actively obtaining information undercover and using operational investigative methods such as systematic evaluation have therefore become an increasingly important technique for identifying and fighting organised crime in the field of drugs. In this regard particular attention should be focused on the systematic, coordinated and targeted use of informers.
Informers must gain the trust of the criminals so that they are in a position to shed light on the structure of criminal organisations and structures.
That is why the Presidency carried out a survey in the Schengen States (see Doc. SCH/Stup (98)25). Doc. SCH/Stup (98)60 Rev. was subsequently distributed outlining the results. The survey showed that the law and, also in part, legal practice with regard to the payment of informers in the Schengen States differed widely. At its meeting of 21 October 1998 the Working Group on Drugs therefore agreed to draw up common non-binding guidelines for paying informers and guaranteeing them non-material benefits.
The general principles governing payment of informers are to be used as non-mandatory guidelines within the Schengen area and are intended to contribute to the further improvement of customs and police cooperation in this sensitive sphere. They should also serve as benchmarks for those Schengen States currently engaged in drafting or amplifying similar regulations.
2. General
Informers' motives for cooperating with police and customs authorities are frequently financial. They should therefore be provided with financial incentives that take market realities into account and correspond to their personal circumstances, reflect the skills required for the operation and are commensurate with the risk involved and the outcome of the investigation. Economic considerations are also a factor, since using informers often works out cheaper.
Ensuring that the following guidelines are observed throughout the Schengen area would in particular permit compliance with tactical and legal requirements that apply to drugs investigations while also taking account of specific bilateral and regional features and the particular nature of the offence. This would also prevent incidentally the emergence of "informer tourism", with police forces and customs authorities which run informers competing with each other on a bilateral level or with other services throughout Schengen.
3. Principles
These principles shall be without prejudice to national provisions.
Payments made to informers should be in reasonable proportion to the outcome of the investigation achieved as a result of criminal prosecution and/or the danger that is averted by the use of an informer on the one hand and the involvement of and personal risk incurred by the informer on the other. The financial incentive must not incite the informer to commit an offence.
Particular criteria are as follows:
- The quantity of information and the results it produces, for example the value and the importance of the drugs that are seized, the number and calibre of the criminals arrested and/or the value of the assets confiscated.
- The quality of the information, for example strategically or tactically useful information about methods, logistical approach used by the criminals, aims of the criminal organisation, the way in which it responds to measures taken by the criminal justice authorities.
- The personal characteristics of the informer, for example degree of involvement in the operation, particular difficulties, risks and dangers, trustworthiness and motivation.
- The importance of the criminal hierarchy/organisation, or investigating the criminal activity of the members, their influence within the criminal milieu, degree of infiltration into public life, actual or potential damage caused, social relevance of the case and the degree to which it is rooted in the local criminal environment, the information also being used for strategic purposes.
Payment for cooperating is generally case-specific. No attempt should be made to provide the informer with living expenses for an indefinite period.
Informers may also benefit from special protective or post-operation measures (so-called witness protection) and arrangements may be made to provide social protection.
Costs incurred by an informer (expenses) may be refunded in specific cases.
Payment is made after completion of the assignment. Part payments may be made after parts of the assignment have been completed. Advances should not be paid.
An informer's earnings are still subject to national tax and social security regulations.
Generally speaking, the costs of using an informer are borne by the police or customs authority. If an investigation is to be conducted jointly by several Schengen bodies, agreement should be reached at an early stage on how the costs are to be shared. Contributions from third parties should not as a rule be included in the payment made to the informer.
Non-material benefits may also be provided subject to the provisions of national law in force in the various Schengen States and counted as material contributions. The nature of the benefit, its importance to the informer and the cost to the State in providing it are the factors to be taken into account in this regard. Protecting the informer in dangerous situations, an easing of the detention regime and full or partial remission of sentence in accordance with national law also come into the category of non-material benefit.
If an informer acts improperly, for example by not keeping to the agreement, committing a criminal offence in a particular case, knowingly or recklessly giving false information, culpably failing to follow received instructions or wilfully departing from tactical directives, payments may be reduced, withheld or recovered in their totality depending on the seriousness of the informer's misconduct. If two or more Schengen States are affected or might be affected in such a situation, the relevant national agencies should give provide notification ("warning") as soon as possible.
The competent central authorities should exchange information on current criteria for payments in the different States.
