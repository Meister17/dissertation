European Maritime Safety Agency — Publication of the final accounts for the financial year 2004
(2005/C 269/03)
The complete version of the final accounts may be found at the following address:
http://emsa.eu.int
Table 1
Budget implementation for the financial year 2004
NB: Variations in totals are due to the effects of rounding.
(EUR 1000) |
Revenue | Expenditure |
Source of revenue | Revenue entered in the final budget for the financial year | Revenue received | Allocation of expenditure | Final budget appropriations | Appropriations carried over from previous year | Appropriations available (2004 budget and financial year 2003) |
entered | committed | paid | carried over | cancelled | Commitments outstanding | paid | cancelled | Appropriations | committed | paid | carried over | cancelled |
Community subsidies | 13340 | 12800 | Title I Staff | 8170 | 3736 | 3594 | 143 | 4434 | 66 | 13 | 53 | 8236 | 3802 | 3607 | 143 | 4487 |
Other revenue | | 5 | Title II Operating expenditure | 1630 | 1319 | 635 | 684 | 311 | 315 | 267 | 48 | 1945 | 1634 | 902 | 684 | 359 |
Title III Operational expenditure | 3540 | 2448 | 437 | 2074 | 1092 | 155 | 5 | 150 | 3695 | 2603 | 442 | 2074 | 1242 |
Total | 13340 | 12805 | Total | 13340 | 7503 | 4666 | 2901 | 5837 | 536 | 285 | 251 | 13876 | 8039 | 4951 | 2901 | 6089 |
Table 2
Revenue and expenditure account for the financial years 2004 and 2003
NB: Variations in totals are due to the effects of rounding.
(EUR 1000) |
| 2004 | 2003 |
Revenue
Community subsidies | 12800 | 2630 |
Other revenue | 5 | 2 |
Total budget revenue (a) | 12805 | 2632 |
Expenditure
Staff — Title I of the budget
Payments | 3594 | 647 |
Appropriations carried over | 143 | 66 |
Operating expenditure — Title II of the budget
Payments | 635 | 238 |
Appropriations carried over | 684 | 315 |
Operational expenditure — Title III of the budget
Payments | 437 | 13 |
Appropriations carried over | 2074 | 155 |
Total budget expenditure (b) | 7567 | 1434 |
Balance (c = a - b) | 5238 | 1198 |
Appropriations carried over and cancelled | 251 | — |
Exchange-rate differences | – 1 | 0 |
Balance of the budget implementation for the financial year (d) | 5488 | 1198 |
Variation in automatic carry-overs of appropriations and invoices to be received | 2089 | 399 |
Variation in investments for the financial year | 242 | 11 |
Variation in debts (Commission) | – 5489 | – 1198 |
Depreciation for the financial year | – 43 | – 3 |
Variation in advances to suppliers | 56 | — |
Outturn of the economic adjustments for the financial year (e) | 2343 | 407 |
Table 3
Balance sheets as at 31 December 2004 and 2003
NB: Variations in totals are due to the effects of rounding.
(EUR 1000) |
ASSETS | 2004 | 2003 | LIABILITIES | 2004 | 2003 |
Fixed assets | | | Own funds | | |
Computer software | 30 | 11 | Cumulative outturn | 2750 | 407 |
Plant and machinery | 27 | 0 | Subtotal | 2750 | 407 |
Computer hardware | 193 | 0 | | | |
Depreciations | – 44 | – 3 | | | |
Subtotal | 206 | 8 | | | |
Current assets | | | Long-term liabilities | | |
| | | Balance of budget to be paid back (2004) | 5488 | 1198 |
Sundry accounts receivable | 38 | 2 | Subtotal | 5488 | 1198 |
Recoverable VAT | 2 | 0 | | | |
Staff advances | 19 | 17 | Current liabilities | | |
Advances to Community bodies | 1 | 1 | Balance of budget to be paid back (2003) | 1198 | |
Advances to suppliers | 56 | 0 | Invoices to be received | 413 | 137 |
Social security advances (ONSS) | 22 | 0 | Sundry accounts due | 249 | 29 |
Subtotal | 138 | 20 | Subtotal | 1860 | 167 |
Cash in hand | | | | | |
Banks | 9754 | 1744 | | | |
Subtotal | 9754 | 1744 | | | |
Total | 10098 | 1772 | Total | 10098 | 1772 |
[1] EMSA's operating appropriations are managed as differentiated appropriations. Payment appropriations for the year amounting to EUR 64419 were used to pay outstanding commitments relating to differentiated appropriations from the previous year.
[2] All commitment appropriations carried over were paid from the appropriations for the year, except a payment of EUR 4953.
[3] The data for the financial year 2003 have been restated in order to comply with the principle of accruals-based accounting.
[4] The data for the financial year 2003 have been restated in order to comply with the principle of accruals-based accounting.
--------------------------------------------------
