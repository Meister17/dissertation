P6_TA(2005)0073
Follow-up to the Fourth World Conference on Women — Platform for Action (Beijing+10)
European Parliament resolution on the follow-up to the Fourth World Conference on Women — Platform for Action (Beijing+10)
The European Parliament,
- having regard to the celebration of International Women's Day on 8 March 2005,
- having regard to the International Convention on the Elimination of All Forms of Racial Discrimination of 21 December 1965,
- having regard to the UN Convention on the Elimination of all Forms of Discrimination against Women of 18 December 1979,
- having regard to the UN Convention against Torture and Other Cruel, Inhuman or Degrading Treatment or Punishment of 10 December 1984,
- having regard to the Beijing Declaration and Platform for Action adopted in Beijing on 15 September 1995 by the Fourth World Conference on Women: Action for Equality, Development and Peace,
- having regard to its resolutions of 15 June 1995 [1] and 21 September 1995 [2] on that Conference,
- having regard to its resolution of 4 July 1996 on the follow-up to the Cairo International Conference on Population and Development [3],
- having regard to the Council Recommendation of 2 December 1996 on the balanced participation of men and women in the decision-making process [4],
- having regard to its resolution of 16 September 1997 [5] on the Commission communication on incorporating equal opportunities for women and men into all Community policies and activities — "mainstreaming",
- having regard to its resolution of 19 May 2000 [6] on the Commission communication on further actions in the fight against trafficking in women,
- having regard to Council Directive 2000/43/EC of 29 June 2000 implementing the principle of equal treatment between persons irrespective of racial and ethnic origin [7] (Race Equality Directive),
- having regard to its positions of 17 November 1999 [8] and 9 March 2004 [9] on the adoption of programmes of Community action (Daphne and Daphne II, from 2000 to 2003 and 2004 to 2008 respectively) on preventive measures to fight violence against children, young persons and women,
- having regard to its resolutions of 20 September 2001 on equal pay for work of equal value [10] and 25 September 2002 on representation of women among the social partners of the European Union [11],
- having regard to its resolution of 9 March 2004 on reconciling professional, family and private lives [12],
- having regard to the outcome of the 49th Session of the UN Commission on the Status of Women held on 4 March 2005,
- having regard to the serious incidents that took place on 6 March 2005 in Istanbul during a demonstration in connection with International Women's Day,
- having regard to Rule 108(5) of its Rules of Procedure,
A. whereas, although women make up more than half of the European Union's population and electorates, they continue to be under-represented in economic, social and political decision-making bodies throughout the Union; and whereas, in particular, in some Member States the percentage of women parliamentarians is below the world-wide average of 15,6 %,
B. whereas women's rights continue to be violated throughout the world, even though they are an integral, inalienable and indivisible part of universal human rights, and whereas violations are particularly shocking in war-torn areas where women are victims of rape, forced pregnancy and sexual exploitation,
C. whereas sexual exploitation has not decreased and, according to estimates, every year hundreds of thousands of women and children from third countries fall victim to the trafficking of human beings in the European Union,
D. regretting that genital mutilation, which is not unknown in EU countries, is still widespread throughout the world (according to the WHO, two million women are subjected to such practices each year),
E. whereas the empowerment of women is a crucial factor in the eradication of poverty and a necessary precondition for achieving the Lisbon Strategy goals and meeting their target of 60 % of women in the work force by 2010,
F. whereas equal access to goods and services and economic independence for women are essential if equality is to be achieved throughout society,
G. whereas measures that help reconcile private and working life are a prerequisite for promoting gender equality in employment and society, in line with the conclusions of the Barcelona and Lisbon summits,
H. whereas special attention should be paid to women from minority groups,
I. whereas the UN's International Year of Microcredit 2005, from which a large number of women benefit, should be actively promoted by the Union,
J. whereas the rising unemployment rate in Europe requires the promotion of specific measures in order to facilitate access to skilled jobs for women, who are the first to be hit by unemployment, and whereas, although women are often better educated than men, they are still not in a position to benefit from this educational advantage,
K. whereas the wage gap between men and women in Europe is still, on average, between 16% and 33 % and whereas no real progress has been made on implementation of the principle of equal pay for work of equal value, which was introduced thirty years ago by Directive 75/117/EEC [13] on equal pay for men and women, and whereas 30 % of working women in the EU have a part-time job, compared with 6,6% of men, a choice often forced on them by a lack of affordable childcare facilities,
L. whereas experience shows that if full account is to be taken of women's interests in society, at least one third of those elected to all institutional bodies must be women, and whereas 50 % is the target for achieving parity democracy, a principle which is accepted in some Member States at party, local, national and European level; whereas a clear Union policy on parity democracy is still lacking,
M. whereas millions of women still lack information and education concerning their health and have no access to necessary health treatment and no information on, or access to, contraceptives; and whereas, with particular reference to sexually transmitted diseases such as HIV/AIDS, a recent World Bank report estimated that 201 million women still do not have access to contraception, causing 23 million unwanted pregnancies and 1,4 million infant deaths,
N. whereas the 1995 Platform for Action expressly mentions freedom of decision and responsibility on matters related to sexuality, full respect for the integrity of the individual and equality in matters of sexual relations and reproduction,
O. welcoming the role played by the Luxembourg Presidency and the Commission in the preparatory work for and during the negotiations at the 49th Session of the UN Commission on the Status of Women,
1. Welcomes the Declaration of the 49th Session of the UN Commission on the Status of Women, which reaffirms the Declaration and the Platform for Action adopted in Beijing in 1995;
2. Strongly condemns the brutality of the Turkish police in Istanbul during the demonstration on 6 March 2005 to mark International Women's Day, and asks the Commission to present Parliament with a full report on what happened;
3. Calls on those UN member states who have not yet done so to ratify the Convention on the Elimination of All Forms of Discrimination against Women;
4. Recalls that the human rights of women are an inalienable, integral and indivisible part of universal human rights, and states that the promotion and protection of women's rights are fundamental prerequisites for building a true democracy, and that all possible means should be used to prevent any violations of the human rights of women, including those violations taking place in the Union;
5. Requests that Member States adopt legislation, or enforce their existing laws, to put a stop to the female genital mutilation being perpetrated on their territory, and help the third countries concerned to set up programmes with local NGOs to combat such practices;
6. Calls upon Member States identified as known sites of non-consensual sterilisation to coordinate law enforcement efforts and put an end to this practice;
7. Condemns forced marriages, and calls on the Member States and, in a coordinating role, the Commission to take all necessary steps to punish the perpetrators, even when the forced marriages are contracted by Union residents acting outside the Union's territory;
8. Stresses that the situation of women in the EU has not improved substantially since 1995, despite the implementation of existing legislation such as Directive 75/117/EEC [14] and Directive 76/207/EEC [15] on the principle of equal treatment as regards access to employment and vocational training; calls on the Commission to ensure that women in the new Member States benefit fully from the acquis on women's rights, and proposes a Europe-wide campaign on this acquis to make women more aware of their rights;
9. Welcomes the Commission's proposal for a directive of the European Parliament and of the Council on the implementation of the principle of equal opportunities and equal treatment of men and women in matters of employment and occupation (recast version) (COM(2004)0279) as an important step towards certainty and clarity in the field of gender equality;
10. Welcomes the adoption of Directive 2004/113/EC [16] implementing the principle of equal treatment for women and men in the access to and supply of goods and services, and regards that directive as an important tool for ensuring gender equality; regrets, however, that not enough progress has been made towards completely eliminating the use of gender as a discriminating factor in the determination of premiums and benefits in relation to insurance and associated financial activities;
11. Recognises that some positive measures have been taken in relation to violence against women; stresses, however, the lack of a clear political commitment to addressing and eradicating domestic violence against women, sex tourism and trafficking in women, including legislative measures such as asylum rights for victims;
12. Calls, therefore, on the Commission to declare 2006 European Year against Violence towards Women and urges it, further, to include Romania, Bulgaria, Turkey and neighbouring countries fully in the preparations for, and all events, programmes and projects in connection with, the year against violence towards women;
13. Acknowledges that women's health in the EU has improved significantly over the past decade, although there are still many factors that present obstacles to gender equality in relation to health; requests, therefore, that the different patterns in women's health be taken into account when adopting European programmes and that special attention be paid to reproductive health;
14. Urges the Member States and the Commission to give priority to the gender dimension at UN world conferences, such as Cairo+10 and the World Summit for Social Development in 2005, taking into account the UN Millennium Development Goals;
15. Recalls that education and training are essential to the achievement of equality between women and men and underlines the need for lifelong learning and vocational training to promote equal access for women to skilled jobs and all levels of professional life; notes, in this connection, that of the 860 million people who are illiterate two-thirds are women;
16. Calls on the Council, acting on a proposal from the Commission, to adopt gender-specific indicators relating to the feminisation of poverty, in order to pursue a more concerted policy in the field of social protection, and to see to it that in the Union's development policies top priority is given to fighting the feminisation of poverty;
17. Reiterates that women should constitute at least 40 % of the staff of conciliation, peace-keeping, conflict-prevention and disaster aid operations, including fact-finding and observer missions acting on behalf of the EU and its Members States;
18. Recalls the importance of individual freedom of expression, but underlines the role of the media, advertising and the Internet in establishing values and gender stereotypes; welcomes, therefore, a debate with users and regulatory authorities on their role, with a view to identifying and striking the right balance between freedom of expression and the right to human dignity, especially as regards media and advertising viewable by children;
19. Calls on the Commission and the Council to propose measures to improve women's access to full participation in economic, social and political decision making, and stresses the importance of implementing gender mainstreaming in all European policies;
20. Calls on the Commission and the Council to ensure that all programmes and activities financed through EU budgets, especially under the Structural Funds, include gender-based budgeting, starting in 2006;
21. Calls on political parties, at both national and European level, to review their party structures and procedures with the aim of removing all barriers that directly or indirectly militate against the participation of women and to adopt appropriate strategies with a view to achieving a better balance between women and men in elected assemblies, including affirmative measures such as quotas;
22. Calls on the Commission to propose a follow-up strategy on the indicators put forward by the various EU Presidencies;
23. Welcomes the legal reforms that have been carried out in Turkey, but reiterates its concern that women there are still victims of honour killings and violence, and calls therefore on the Commission and the Council to continue their cooperation with the Turkish authorities and to follow closely the women's rights situation in Turkey;
24. Instructs its President to forward this resolution to the Council, the Commission, the governments of the Member States and the UN Secretary-General.
[1] OJ C 166, 3.7.1995, p. 92.
[2] OJ C 269, 16.10.1995, p. 146.
[3] OJ C 211, 22.7.1996, p. 31.
[4] OJ L 319, 10.12.1996, p. 11.
[5] OJ C 304, 6.10.1997, p. 50.
[6] OJ C 59, 23.2.2001, p. 307.
[7] OJ L 180, 19.7.2000, p. 22.
[8] OJ C 189, 7.7.2000, p. 69.
[9] OJ C 102 E, 28.4.2004, p. 159.
[10] OJ C 77 E, 28.3.2002, p. 134.
[11] OJ C 273 E, 14.11.2003, p. 169.
[12] OJ C 102 E, 28.4.2004, p. 492.
[13] OJ L 45, 19.2.1975, p. 19.
[14] OJ L 45, 19.2.1975, p. 19.
[15] OJ L 39, 14.2.1976, p. 40.
[16] OJ L 373, 21.12.2004, p. 37.
--------------------------------------------------
