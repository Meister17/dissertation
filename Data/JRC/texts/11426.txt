Commission Regulation (EC) No 990/2006
of 30 June 2006
opening standing invitations to tender for the export of cereals held by the intervention agencies of the Member States
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1784/2003 of 29 September 2003 on the common organisation of the market in cereals [1], and in particular Article 6 thereof,
Whereas:
(1) Commission Regulation (EEC) No 2131/93 [2] lays down the procedure and conditions for the disposal of cereals held by intervention agencies.
(2) Commission Regulation (EEC) No 3002/92 [3] lays down common detailed rules for verifying the use and/or destination of products from intervention.
(3) Given the current situation of the market in cereals, in view of the quantities of cereals available in intervention stocks and the prospects for export of those cereals to third countries, standing invitations to tender should be opened for the export of cereals held by the intervention agencies of the Member States. Each of them should be considered to be a separate invitation to tender.
(4) To ensure that the operations and their monitoring are properly effected, special monitoring arrangements adapted to the cereals sector should be laid down. To that end, securities should be lodged to ensure that the objectives laid down by the legislation are achieved without excessive cost to the operators.
(5) Derogations should accordingly be made to certain rules, in particular those laid down in Regulation (EEC) No 2131/93 as regards the price to be paid, the time limits for submission of tenders and the amount of the securities, and in Regulation (EEC) No 3002/92 as regards the entries to be made on the export licence, the removal orders and, where applicable, the T5 copy.
(6) To forestall reimportation, exports under this invitation to tender should be limited to certain third countries.
(7) Pursuant to Article 7(2) of Regulation (EEC) No 2131/93, the lowest transport costs between the place of storage and the place of loading at the port or place of exit are reimbursed to the successful tenderer. For Member States which do not have sea ports, pursuant to Article 7(2a) of that Regulation the successful exporting tenderer may be reimbursed the lowest transport costs between the place of storage and the actual place of exit, up to a certain ceiling. This provision should be applied for the Member States concerned and the conditions for its application should be laid down.
(8) With a view to the sound management of the system, provision should also be made for the electronic transmission of the information required by the Commission.
(9) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Cereals,
HAS ADOPTED THIS REGULATION:
Article 1
1. Subject to this Regulation, the intervention agencies of the Member States shown in Annex I shall issue standing invitations to tender in accordance with Regulation (EEC) No 2131/93 for the export of each type of cereal held by them. The maximum quantities of the different cereals covered by these invitations to tender are shown in Annex I.
2. For common wheat and rye, each invitation to tender shall cover a maximum quantity for export to third countries with the exception of Albania, Bosnia and Herzegovina, Bulgaria, Croatia, the former Yugoslav Republic of Macedonia, Liechtenstein, Montenegro, Romania, Serbia [4] and Switzerland.
For barley, each invitation to tender shall cover a maximum quantity for export to third countries with the exception of Albania, Bosnia and Herzegovina, Bulgaria, Canada, Croatia, the former Yugoslav Republic of Macedonia, Liechtenstein, Mexico, Montenegro, Romania, Serbia [4], Switzerland and the United States of America.
Article 2
1. No export refund or tax or monthly increase shall be granted on exports carried out under this Regulation.
2. Article 8(2) of Regulation (EEC) No 2131/93 shall not apply.
3. Notwithstanding the third paragraph of Article 16 of Regulation (EEC) No 2131/93, the price to be paid for the export shall be that quoted in the tender, without monthly increase.
4. For the Czech Republic, Luxembourg, Hungary, Austria and Slovakia, the lowest transport costs between the place of storage and the actual place of exit situated outside their territory shall be reimbursed to the successful tenderer, in accordance with Article 7(2a) of Regulation (EEC) No 2131/93, up to the ceiling set in the invitation to tender.
Article 3
1. Export licences shall be valid from their date of issue within the meaning of Article 9 of Regulation (EEC) No 2131/93 until the end of the fourth month thereafter.
2. Tenders submitted in response to each invitation to tender opened under this Regulation need not be accompanied by export licence applications submitted under Article 49 of Commission Regulation (EC) No 1291/2000 [5].
Article 4
1. Notwithstanding Article 7(1) of Regulation (EEC) No 2131/93, the time-limit for submission of tenders under the first partial invitation to tender shall be 09.00 (Brussels time) on 6 July 2006.
The closing dates for the submission of tenders for subsequent partial invitations to tender shall be each Thursday at 09.00 (Brussels time), with the exception of 3 August 2006, 17 August 2006, 24 August 2006, 2 November 2006, 28 December 2006, 5 April 2007 and 17 May 2007, i.e. weeks when no invitation to tender shall be made.
The last partial invitation to tender shall expire at 09.00 (Brussels time) on 28 June 2007.
2. Tenders must be lodged with the intervention agencies concerned at the addresses shown in Annex I.
Article 5
The intervention agency concerned, the storer and the successful tenderer shall, at the request of the latter and by common agreement, either before or at the time of removal from storage as the tenderer chooses, take reference samples for counter-analysis at the rate of at least one sample for every 500 tonnes and shall analyse the samples. Each intervention agency may be represented by a proxy, provided this is not the storer.
Reference samples for counter-analysis shall be taken and analysed within seven working days of the date of the successful tenderer’s request or within three working days if the samples are taken on removal from storage.
In the event of a dispute over the results of analyses, the results shall be forwarded electronically to the Commission.
Article 6
1. The successful tenderer must accept the lot as established if the final result of the sample analyses indicates a quality:
(a) higher than that specified in the notice of invitation to tender;
(b) higher than the minimum characteristics laid down for intervention but below the quality described in the notice of invitation to tender, providing that the differences do not exceed the following limits:
- one kilogram per hectolitre as regards specific weight, which must not, however, be less than 75 kg/hl for common wheat, 64 kg/hl for barley and 68 kg/hl for rye,
- one percentage point as regards moisture content,
- half a percentage point as regards the impurities referred to in points B.2 and B.4 of Annex I to Commission Regulation (EC) No 824/2000 [6],
- half a percentage point as regards the impurities referred to in point B.5 of Annex I to Regulation (EC) No 824/2000, the percentages admissible for noxious grains and ergot remaining unchanged, however.
2. If the final result of the analyses carried out on the samples indicates a quality higher than the minimum characteristics laid down for intervention but below the quality described in the notice of invitation to tender and the difference exceeds the limits set out in paragraph 1(b), the successful tenderer may:
(a) accept the lot as established, or
(b) refuse to take over the lot concerned.
In the case of (b), the successful tenderer shall be discharged of all obligations relating to the lot in question and the securities shall be released provided the Commission and the intervention agency concerned are immediately notified using the form in Annex II.
3. Where the final result of sample analyses indicates a quality below the minimum characteristics laid down for intervention, the successful tenderer may not remove the lot in question. The successful tenderer shall be discharged of all obligations relating to the lot in question and the securities shall be released provided the Commission and the intervention agency concerned are immediately notified using the form set out in Annex II.
Article 7
Should the cases mentioned in point (b) of the first subparagraph of Article 6(2) and in Article 6(3) arise, the successful tenderer may ask the intervention agency to supply an alternative lot of cereals of the requisite quality, at no extra cost. In that case, the security shall not be released. The lot must be replaced within three days of the date of the successful tenderer’s request. The successful tenderer shall immediately inform the Commission thereof electronically, using the form set out in Annex II.
If, as a result of successive replacements, the successful tenderer has not received a replacement lot of the quality laid down within one month of the date of the first request for a replacement, the successful tenderer shall be discharged of all obligations and the securities shall be released, provided the Commission and the intervention agency concerned have been immediately informed electronically using the form set out in Annex II.
Article 8
1. If the cereals are removed before the results of the analyses provided for in Article 5 are known, all risks shall be borne by the successful tenderer from the time the lot is removed, without prejudice to any means of redress the tenderer might have against the storer.
2. The costs of taking the samples and conducting the analyses provided for in Article 5, with the exception of those referred to in Article 6(3), shall be borne by the European Agricultural Guidance and Guarantee Fund (EAGGF) for up to one analysis per 500 tonnes, with the exception of the cost of inter-bin transfers. The costs of inter-bin transfers and any additional analyses requested by a successful tenderer shall be borne by that tenderer.
Article 9
Notwithstanding Article 12 of Regulation (EEC) No 3002/92, the documents relating to the sale of cereals carried out under this Regulation, and in particular the export licence, the removal order referred to in Article 3(1)(b) of Regulation (EEC) No 3002/92, the export declaration and, where applicable, the T5 copy shall carry:
(a) for common wheat, one of the entries set out in Annex III(A) to this Regulation;
(b) for barley, one of the entries set out in Annex III(B) to this Regulation;
(c) for rye, one of the entries set out in Annex III(C) to this Regulation.
Article 10
1. The security lodged under Article 13(4) of Regulation (EEC) No 2131/93 shall be released once the export licences have been issued to the successful tenderers.
2. Notwithstanding Article 17(1) of Regulation (EEC) No 2131/93, the obligation to export shall be covered by a security equal to the difference between the intervention price applying on the day of the award and the price awarded, but not less than EUR 25 per tonne. Half of this security shall be lodged when the export licence is issued and the balance shall be lodged before the cereals are removed from the place of storage.
Article 11
The intervention agencies concerned shall communicate to the Commission tenders submitted within two hours of the expiry of the deadline for the submission of tenders laid down in Article 4(1). If no tenders are submitted, the Member State concerned shall communicate this to the Commission within the same time-limits. If the Member State does not send a communication to the Commission within the given deadline, the Commission shall consider that no tender has been submitted in the Member State concerned.
The communications referred to in the first subparagraph shall be sent electronically, in accordance with the model in Annex IV. A separate form for each type of cereal shall be sent to the Commission for each invitation to tender. The tenderers shall not be identified.
Article 12
1. In accordance with the procedure referred to in Article 25(2) of Regulation (EC) No 1784/2003 the Commission shall fix, for each cereal concerned and by Member State, the minimum selling price, or decide to take no action in respect of the tenders received, in accordance with Article 10 of Regulation (EEC) No 2131/93.
2. If the fixing of a minimum price, in accordance with paragraph 1, would lead to an overrun on the maximum quantity available to a Member State, an award coefficient may be fixed at the same time for the quantities offered at the minimum price in order to comply with the maximum quantity available to that Member State.
Article 13
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 30 June 2006.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 270, 21.10.2003, p. 78. Regulation as amended by Commission Regulation (EC) No 1154/2005 (OJ L 187, 19.7.2005, p. 11).
[2] OJ L 191, 31.7.1993, p. 76. Regulation as last amended by Regulation (EC) No 749/2005 (OJ L 126, 19.5.2005, p. 10).
[3] OJ L 301, 17.10.1992, p. 17. Regulation as last amended by Regulation (EC) No 770/96 (OJ L 104, 27.4.1996, p. 13).
[4] Including Kosovo, as defined in UN Security Council Resolution 1244 of 10 June 1999.
[5] OJ L 152, 24.6.2000, p. 1.
[6] OJ L 100, 20.4.2000, p. 31.
--------------------------------------------------
ANNEX I
LIST OF INVITATIONS TO TENDER
— : no intervention stock of this cereal in this Member State.
Member State | Quantities made available for sale on external markets (tonnes) | Intervention Agency Name, address and contact details |
Common wheat | Barley | Rye |
Belgique/België | 0 | 0 | — | Bureau d'intervention et de restitution belge Rue de Trèves 82 B-1040 Bruxelles Tel. (32-2) 287 24 78 Fax (32-2) 287 25 24 E-mail: webmaster@birb.be |
Česká republika | 50000 | 150000 | — | Státní zemědělský intervenční fond Odbor Rostlinných Komodit Ve Smečkách 33 CZ-110 00, Praha 1 Tel. (420) 222 871 667 – 222 871 403 Fax (420) 296 806 404 E-mail: dagmar.hejrovska@szif.cz |
Danmark | 0 | 0 | — | Direktoratet for FødevareErhverv Nyropsgade 30 DK-1780 København Tel. (45) 33 95 88 07 Fax (45) 33 95 80 34 E-mail: mij@dffe.dk and pah@dffe.dk |
Deutschland | 0 | 0 | 300000 | Bundesanstalt für Landwirtschaft und Ernährung Deichmanns Aue 29 D-53179 Bonn Tel. (49-228) 6845 — 3704 Fax 1 (49-228) 6845 — 3985 Fax 2 (49-228) 6845 — 3276 E-mail: pflanzlErzeugnisse@ble.de |
Eesti | 0 | 30000 | — | Põllumajanduse Registrite ja Informatsiooni Amet Narva mnt. 3, 51009 Tartu Tel. (372) 7371 200 Fax (372) 7371 201 E-mail: pria@pria.ee |
Elláda | — | — | — | Payment and Control Agency for Guidance and Guarantee Community Aids (O.P.E.K.E.P.E) 241, Archarnon str., GR-104 46 Athens Tel. (30-210) 212 47 87 & 47 54 Fax (30-210) 212 47 91 E-mail: ax17u073@minagric.gr |
España | — | — | — | S. Gral. Intervención de Mercados (FEGA) C/Almagro 33 — 28010 Madrid — España Tel. (34-91) 3474765 Fax (34-91) 3474838 E-mail: sgintervencion@fega.mapa.es |
France | 0 | 0 | — | Office national interprofessionnel des grandes cultures (ONIGC) 21, avenue Bosquet F-75326 Paris Cedex 07 Tel. (33) 144 18 22 29 et 23 37 Fax (33) 144 18 20 08 — 144 18 20 80 E-mail: m.meizels@onigc.fr et f.abeasis@onigc.fr |
Ireland | — | 0 | — | Intervention Operations, OFI, Subsidies & Storage Division, Department of Agriculture & Food Johnstown Castle Estate, County Wexford Tel. 353 53 63400 Fax 353 53 42843 |
Italia | — | — | — | Agenzia per le Erogazioni in Agricoltura — AGEA Via Torino, 45, 00184 Roma Tel. (39) 0649499755 Fax (39) 0649499761 E-mail: d.spampinato@agea.gov.it |
Kypros/Kibris | — | — | — | |
Latvija | 0 | 0 | — | Lauku atbalsta dienests Republikas laukums 2, Rīga, LV – 1981 Tel. (371) 702 7893 Fax (371) 702 7892 E-mail: lad@lad.gov.lv |
Lietuva | 0 | 50000 | — | The Lithuanian Agricultural and Food Products Market regulation Agency L. Stuokos-Guceviciaus Str. 9-12, Vilnius, Lithuania Tel. (370-5) 268 5049 Fax (370-5) 268 5061 E-mail: info@litfood.lt |
Luxembourg | — | — | — | Office des licences 21, rue Philippe II Boîte postale 113 L-2011 Luxembourg Tel. (352) 478 23 70 Fax (352) 46 61 38 Telex: 2 537 AGRIM LU |
Magyarország | 500000 | 80000 | — | Mezőgazdasági és Vidékfejlesztési Hivatal Soroksári út. 22-24 H-1095 Budapest Tel. (36) 1 219 45 76 Fax (36) 1 219 89 05 E-mail: ertekesites@mvh.gov.hu |
Malta | — | — | — | |
Nederland | — | — | — | Dienst Regelingen Roermond Postbus 965, NL-6040 AZ Roermond Tel. (31) 475 355 486 Fax (31) 475 318 939 E-mail: p.a.c.m.van.de.lindeloof@minlnv.nl |
Österreich | 0 | 0 | — | AMA (Agrarmarkt Austria) Dresdnerstraße 70 A-1200 Wien Tel. (43-1) 33151 258 (43-1) 33151 328 Fax (43-1) 33151 4624 (43-1) 33151 4469 E-mail: referat10@ama.gv.at |
Polska | 400000 | 100000 | — | Agencja Rynku Rolnego Biuro Produktów Roślinnych Nowy Świat 6/12 PL-00-400 Warszawa Tel. (48) 22 661 78 10 Fax (48) 22 661 78 26 E-mail: cereals-intervention@arr.gov.pl |
Portugal | — | — | — | Instituto Nacional de Intervenção e Garantia Agrícola (INGA) R. Castilho, n.o 45-51 1269-163 Lisboa Tel. (351) 21 751 85 00 (351) 21 384 60 00 Fax (351) 21 384 61 70 E-mail: inga@inga.min-agricultura.pt edalberto.santana@inga.min-agricultura.pt |
Slovenija | — | — | — | Agencija Republike Slovenije za kmetijske trge in razvoj podeželja Dunajska 160, 1000 Ljubjana Tel. (386) 1 580 76 52 Fax (386) 1 478 92 00 E-mail: aktrp@gov.si |
Slovensko | 30000 | 0 | — | Pôdohospodárska platobná agentúra Oddelenie obilnín a škrobu Dobrovičova 12 SK-815 26 Bratislava Tel. (421-2) 58 24 3271 Fax (421-2) 57 41 2665 E-mail: jvargova@apa.sk |
Suomi/Finland | 0 | 200000 | — | Maa- ja metsätalousministeriö (MMM) Interventioyksikkö – Intervention Unit Malminkatu 16, Helsinki PL 30 FIN-00023 Valtioneuvosto Tel. (358-9) 16001 Fax (358-9) 1605 2772 (358-9) 1605 2778 E-mail: intervention.unit@mmm.fi |
Sverige | 0 | 0 | — | Statens Jordbruksverk SE-55182 Jönköping Tel. (46) 36 15 50 00 Fax (46) 36 19 05 46 E-mail: jordbruksverket@sjv.se |
United Kingdom | — | 0 | — | Rural Payments Agency Lancaster House Hampshire Court Newcastle upon Tyne NE4 7YH Tel. (44) 191 226 5882 Fax (44) 191 226 5824 E-mail: cerealsintervention@rpa.gov.uk |
--------------------------------------------------
ANNEX II
Communication to the Commission of refusal and possible replacement of lots under standing invitations to tender for the export of cereals held by the intervention agencies of the Member States
Model [1]
(Articles 6 and 7 of Commission Regulation (EC) No 990/2006)
"TYPE OF CEREAL: CN code [2]"
Name of successful tenderer:
- Date of award:
- Date of refusal of the lot by the successful tenderer:
Member State | Lot No | Quantity in tonnes | Address of the silo | Reason for refusal to take over |
| | | | specific weight (kg/hl)% of sprouted grains% of miscellaneous impurities (Schwarzbesatz)% of matter other than basic cereals of unimpaired qualityother |
[1] To be sent to DG AGRI (Unit D.2).
[2] 100190 for common wheat, 100300 for barley and 10020000 for rye.
--------------------------------------------------
ANNEX III
Part A
Entries referred to in Article 9 for common wheat
in Spanish : Trigo blando de intervención sin aplicación de restitución ni gravamen, Reglamento (CE) no 990/2006
in Czech : Intervenční pšenice obecná nepodléhá vývozní náhradě ani clu, nařízení (ES) č. 990/2006
in Danish : Blød hvede fra intervention uden restitutionsydelse eller -afgift, forordning (EF) nr. 990/2006
in German : Weichweizen aus Interventionsbeständen ohne Anwendung von Ausfuhrerstattungen oder Ausfuhrabgaben, Verordnung (EG) Nr. 990/2006
in Estonian : Pehme nisu sekkumisvarudest, mille puhul ei rakendata toetust või maksu, määrus (EÜ) nr 990/2006
in Greek : Μαλακός σίτος παρέμβασης χωρίς εφαρμογή επιστροφής ή φόρου, κανονισμός (ΕΚ) αριθ. 990/2006
in English : Intervention common wheat without application of refund or tax, Regulation (EC) No 990/2006
in French : Blé tendre d'intervention ne donnant pas lieu à restitution ni taxe, règlement (CE) no 990/2006
in Italian : Frumento tenero d'intervento senza applicazione di restituzione né di tassa, regolamento (CE) n. 990/2006
in Latvian : Intervences parastie kvieši bez kompensācijas vai nodokļa piemērošanas, Regula (EK) Nr. 990/2006
in Lithuanian : Intervenciniai paprastieji kviečiai, kompensacija ar mokesčiai netaikytini, Reglamentas (EB) Nr. 990/2006
in Hungarian : Intervenciós búza, visszatérítés, illetve adó nem alkalmazandó, 990/2006/EK rendelet
in Dutch : Zachte tarwe uit interventie, zonder toepassing van restitutie of belasting, Verordening (EG) nr. 990/2006
in Polish : Pszenica zwyczajna interwencyjna niedająca podstawy do zastosowania refundacji lub podatku, rozporządzenie (WE) nr 990/2006
in Portuguese : Trigo mole de intervenção sem aplicação de uma restituição ou imposição, Regulamento (CE) n.o 990/2006
in Slovak : Intervenčná pšenica obyčajná nepodlieha vývozným náhradám ani clu, nariadenie (ES) č. 990/2006
in Slovene : Intervencija navadne pšenice brez zahtevkov za nadomestila ali carine, Uredba (ES) št. 990/2006
in Finnish : Interventiovehnä, johon ei sovelleta vientitukea eikä vientimaksua, asetus (EY) N:o 990/2006
in Swedish : Interventionsvete, utan tillämpning av bidrag eller avgift, förordning (EG) nr 990/2006
Part B
Entries referred to in Article 9 for barley
in Spanish : Cebada de intervención sin aplicación de restitución ni gravamen, Reglamento (CE) no 990/2006
in Czech : Intervenční ječmen nepodléhá vývozní náhradě ani clu, nařízení (ES) č. 990/2006
in Danish : Byg fra intervention uden restitutionsydelse eller -afgift, forordning (EF) nr. 990/2006
in German : Interventionsgerste ohne Anwendung von Ausfuhrerstattungen oder Ausfuhrabgaben, Verordnung (EG) Nr. 990/2006
in Estonian : Sekkumisoder, mille puhul ei rakendata toetust või maksu, määrus (EÜ) nr 990/2006
in Greek : Κριθή παρέμβασης χωρίς εφαρμογή επιστροφής ή φόρου, κανονισμός (ΕΚ) αριθ. 990/2006
in English : Intervention barley without application of refund or tax, Regulation (EC) No 990/2006
in French : Orge d'intervention ne donnant pas lieu à restitution ni taxe, règlement (CE) no 990/2006
in Italian : Orzo d'intervento senza applicazione di restituzione né di tassa, regolamento (CE) n. 990/2006
in Latvian : Intervences mieži bez kompensācijas vai nodokļa piemērošanas, Regula (EK) Nr. 990/2006
in Lithuanian : Intervenciniai miežiai, kompensacija ar mokesčiai netaikytini, Reglamentas (EB) Nr. 990/2006
in Hungarian : Intervenciós árpa, visszatérítés illetve adó nem alkalmazandó, 990/2006/EK rendelet
in Dutch : Gerst uit interventie, zonder toepassing van restitutie of belasting, Verordening (EG) nr. 990/2006
in Polish : Jęczmien interwencyjny niedający podstawy do zastosowania refundacji lub podatku, rozporządzenie (WE) nr 990/2006
in Portuguese : Cevada de intervenção sem aplicação de uma restituição ou imposição, Regulamento (CE) n.o 990/2006
in Slovak : Intervenčný jačmeň, nepodlieha vývozným náhradám ani clu, nariadenie (ES) č. 990/2006
in Slovene : Intervencija ječmena brez zahtevkov za nadomestila ali carine, Uredba (ES) št. 990/2006
in Finnish : Interventio-ohra, johon ei sovelleta vientitukea eikä vientimaksua, asetus (EY) N:o 990/2006
in Swedish : Interventionskorn, utan tillämpning av bidrag eller avgift, förordning (EG) nr 990/2006
Part C
Entries referred to in Article 9 for rye
in Spanish : Centeno de intervención sin aplicación de restitución ni gravamen, Reglamento (CE) no 990/2006
in Czech : Intervenční žito nepodléhá vývozní náhradě ani clu, nařízení (ES) č. 990/2006
in Danish : Rug fra intervention uden restitutionsydelse eller -afgift, forordning (EF) nr. 990/2006
in German : Interventionsroggen ohne Anwendung von Ausfuhrerstattungen oder Ausfuhrabgaben, Verordnung (EG) Nr. 990/2006
in Estonian : Sekkumisrukis, mille puhul ei rakendata toetust või maksu, määrus (EÜ) nr 990/2006
in Greek : Σίκαλη παρέμβασης χωρίς εφαρμογή επιστροφής ή φόρου, κανονισμός (ΕΚ) αριθ. 990/2006
in English : Intervention rye without application of refund or tax, Regulation (EC) No 990/2006
in French : Seigle d'intervention ne donnant pas lieu à restitution ni taxe, règlement (CE) no 990/2006
in Italian : Segala d'intervento senza applicazione di restituzione né di tassa, regolamento (CE) n. 990/2006
in Latvian : Intervences rudzi bez kompensācijas vai nodokļa piemērošanas, Regula (EK) Nr. 990/2006
in Lithuanian : Intervenciniai rugiai, kompensacija ar mokesčiai netaikytini, Reglamentas (EB) Nr. 990/2006
in Hungarian : Intervenciós rozs, visszatérítés illetve adó nem alkalmazandó, 990/2006/EK rendelet
in Dutch : Rogge uit interventie, zonder toepassing van restitutie of belasting, Verordening (EG) nr. 990/2006
in Polish : Żyto interwencyjne niedające podstawy do zastosowania refundacji lub podatku, rozporządzenie (WE) nr 990/2006
in Portuguese : Centeio de intervenção sem aplicação de uma restituição ou imposição, Regulamento (CE) n.o 990/2006
in Slovak : Intervenčná raž, nepodlieha vývozným náhradám ani clu, nariadenie (ES) č. 990/2006
in Slovene : Intervencija rži brez zahtevkov za nadomestila ali carine, Uredba (ES) št. 990/2006
in Finnish : Interventioruis, johon ei sovelleta vientitukea eikä vientimaksua, asetus (EY) N:o 990/2006
in Swedish : Interventionsråg, utan tillämpning av bidrag eller avgift, förordning (EG) nr 990/2006
--------------------------------------------------
ANNEX IV
Communication to the Commission of tenders received under the standing invitation to tender for the export of cereals from intervention stocks
Model [1]
(Article 11 of Regulation (EC) No 990/2006)
"TYPE OF CEREAL: CN code [2]"
"MEMBER STATE [3]"
1 | 2 | 3 | 4 | 5 | 6 |
Serial numbers of tenderers | Lot No | Eligible quantity (t) | Price quoted EUR/tonne [4] | Increases (+) Reductions (–) (EUR/tonne) (p.m.) | Commercial costs [5] (EUR/tonne) |
1 | | | | | |
2 | | | | | |
3 | | | | | |
etc. | | | | | |
Indicate the total quantities offered (including rejected offers made for the same lot): […] tonnes.
[1] To be sent to DG AGRI (Unit D.2).
[2] 100190 for common wheat, 100300 for barley and 10020000 for rye.
[3] Indicate the Member State concerned.
[4] This price includes the increases and reductions relating to the lot covered by the tender.
[5] The commercial costs corresponding to insurance and services provision borne after the exit of the intervention stock up to the FOB stage at the port of export, with the exception of transport costs. The notified costs shall be established on the basis of the average real costs recorded by the intervention agency in the six months preceding the opening of the tendering period and shall be expressed in euro per tonne.
--------------------------------------------------
