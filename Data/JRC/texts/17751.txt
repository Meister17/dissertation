Statement of revenue and expenditure of the European Agency for Safety and Health at Work for the financial year 2004 - Amending Budget 1
(2004/871/EC)
REVENUE
TITLE 1
EUROPEAN COMMUNITY SUBSIDY
CHAPTER 1 0 — EUROPEAN COMMUNITY SUBSIDY
1 0 0 European Community subsidy
Budget 2004 | Amending budget | New amounts |
9375000 | 1213000 | 10588000 |
Council Regulation (EC) No 2062/94 of 18 July 1994 establishing a European Agency for Safety and Health at Work (OJ L 216, 20.8.1994, p. 1), as amended by Regulation (EC) No 1643/95 (OJ L 156, 7.7.1995, p. 1).
Pursuant to Article 12(3) of this Regulation, a subsidy for the Agency is entered in the "Commission" section of the general budget.
EXPENDITURE
TITLE 1
STAFF
CHAPTER 1 1 — STAFF IN ACTIVE EMPLOYMENT
1 1 0 Staff holding a post provided for in the establishment plan
1 1 0 0 Basic salaries
Budget 2004 | Amending budget | New amounts |
1881268 | 140854 | 2022122 |
Staff Regulations applicable to officials of the European Communities, and in particular Articles 62 and 66 thereof.
This appropriation is intended to cover basic salaries of permanent officials and temporary staff.
1 1 0 1 Family allowances
Budget 2004 | Amending budget | New amounts |
224195 | –952 | 223243 |
Staff Regulations applicable to officials of the European Communities, and in particular Articles 62, 67 and 68 thereof and section I of Annex VII thereto.
This appropriation is intended to cover the household, dependent child and education allowances for permanent officials and temporary staff.
1 1 0 2 Expatriation and foreign-residence allowances
Budget 2004 | Amending budget | New amounts |
295390 | 16445 | 311835 |
Staff Regulations applicable to officials of the European Communities, and in particular Articles 62 and 69 thereof and Article 4 of Annex VII thereto.
This appropriation is intended to cover the expatriation and foreign-residence allowances for permanent officials and temporary staff.
1 1 0 3 Secretarial allowances
Budget 2004 | Amending budget | New amounts |
6169 | 1500 | 7669 |
Staff Regulations applicable to officials of the European Communities, and in particular Article 4a of Annex VII thereto.
This appropriation is intended to cover the secretarial allowance paid to temporary staff in category C employed as shorthand-typists or typists, teleprinter operators, typesetters, executive secretaries or principal secretaries.
1 1 0 4 Directorate-General for Personnel and administration — administrative help
Budget 2004 | Amending budget | New amounts |
18700 | 4400 | 23100 |
This appropriation is intended to cover the charge made by the DG ADMIN for preparing the salaries.
1 1 1 Other staff
1 1 1 0 Auxiliary staff
Budget 2004 | Amending budget | New amounts |
166309 | 25000 | 191309 |
Conditions of employment of other servants of the European Communities, and in particular Article 3 and Title III thereof.
This appropriation is intended to cover the remuneration of, and the employer's social security contributions for, auxiliary staff.
1 1 1 2 Local staff
Budget 2004 | Amending budget | New amounts |
583369 | 45000 | 628369 |
Conditions of employment of other servants of the European Communities, and in particular Article 4 and Title IV thereof.
This appropriation is intended to cover the remuneration (including overtime) and the employer's share of social security contributions for local staff.
1 1 2 Professional training of staff
Budget 2004 | Amending budget | New amounts |
| | |
1 1 3 Employer's social security contributions
1 1 3 0 Insurance against sickness
Budget 2004 | Amending budget | New amounts |
66075 | 6104 | 72179 |
Staff Regulations applicable to officials of the European Communities, and in particular Article 72 thereof. Rules on sickness insurance for officials of the European Communities, and in particular Article 23 thereof.
This appropriation is intended to cover the employer's contribution.
1 1 3 1 Insurance against accidents and occupational disease
Budget 2004 | Amending budget | New amounts |
16908 | 1924 | 18832 |
Staff Regulations applicable to officials of the European Communities, and in particular Article 73 thereof.
This appropriation is intended to cover:
- the employer's contribution to insurance against accidents and occupational disease,
- a further provision is added to the appropriations to cover expenditure not covered by the insurance (Article 73 of the Staff Regulations).
1 1 3 2 Insurance against unemployment
Budget 2004 | Amending budget | New amounts |
15547 | 1819 | 17366 |
Council Regulation (ECSC, EEC, Euratom) No 2799/85 of 27 September 1985 amending the Staff Regulations of officials and the Conditions of employment of other servants of the European Communities (OJ L 265, 8.10.1985, p. 1).
This appropriation is intended to insure temporary staff against unemployment.
1 1 4 Miscellaneous allowances and grants
1 1 4 0 Childbirth and death allowances and grants
Budget 2004 | Amending budget | New amounts |
p.m. | 450 | 450 |
Staff Regulations applicable to officials of the European Communities, and in particular Articles 70, 74 and 75 thereof.
This appropriation is intended to cover:
- the childbirth grant, and
- in the event of the death of an official:
- payment of the deceased's full remuneration until the end of the third month after the month in which the death occurred,
- the cost of transporting the body to the official's place of origin.
1 1 4 1 Travel expenses for annual leave
Budget 2004 | Amending budget | New amounts |
56893 | 3090 | 59983 |
Staff Regulations applicable to officials of the European Communities, and in particular Article 8 of Annex VII thereto.
This appropriation is intended to cover the flat-rate travel expenses for officials or temporary staff, their spouses and dependants, from their place of employment to their place of origin in accordance with the following conditions:
- once per calendar year if the distance by rail is greater than 50 kilometres and less than 725 kilometres,
- twice per calendar year if the distance by rail is at least 725 kilometres.
1 1 7 Supplementary services
1 1 7 5 Temporary staff
Budget 2004 | Amending budget | New amounts |
p.m. | 10000 | 10000 |
This appropriation is intended to cover use of temporary staff, mainly typists.
Recourse to temporary staff is useful when certain departments require flexibility in adapting to ad hoc needs.
1 1 7 6 Officials on secondment from Member States
Budget 2004 | Amending budget | New amounts |
51188 | –10000 | 41188 |
This appropriation is intended to enable the Agency to organise a programme of exchange for civil servants from Member States.
1 1 8 Miscellaneous expenditure on staff recruitment and transfer
1 1 8 0 Miscellaneous expenditure on staff recruitment
Budget 2004 | Amending budget | New amounts |
18293 | 71707 | 90000 |
Staff Regulations applicable to officials of the European Communities, and in particular Articles 27 to 31 and 33 thereof and Annex VII thereto.
This appropriation is intended to cover the expenditure involved in recruitment procedures.
1 1 8 1 Travel expenses
Budget 2004 | Amending budget | New amounts |
16803 | 17054 | 33857 |
Staff Regulations applicable to officials of the European Communities, and in particular Articles 20 and 71 thereof and Article 7 of Annex VII thereto.
This appropriation is intended to cover the travel expenses of staff (including members of their families) entering or leaving the service.
1 1 8 2 Installation, resettlement and transfer allowances
Budget 2004 | Amending budget | New amounts |
105809 | 72824 | 178633 |
Staff Regulations applicable to officials of the European Communities, and in particular Articles 5 and 6 of Annex VII thereto.
This appropriation is intended to cover the installation allowances for staff obliged to change residence after taking up their appointment or when they definitively cease their duties and settle elsewhere.
1 1 8 3 Removal expenses
Budget 2004 | Amending budget | New amounts |
135000 | 122340 | 257340 |
Staff Regulations applicable to officials of the European Communities, and in particular Articles 20 and 71 thereof and Article 9 of Annex VII thereto.
This appropriation is intended to cover the removal costs of staff obliged to change residence after taking up their appointment or when transferred to a new place of work or when they definitively cease their duties and settle elsewhere.
1 1 8 4 Temporary daily subsistence allowances
Budget 2004 | Amending budget | New amounts |
38334 | 19622 | 57956 |
Staff Regulations applicable to officials of the European Communities, and in particular Articles 20 and 71 thereof and Article 10 of Annex VII thereto as well as Articles 25 and 67 of the Conditions of Employment of other Servants.
This appropriation is intended to cover the daily subsistence allowances due to staff able to prove that they were obliged to change their place of residence after taking up their duties (including transfer).
1 1 9 Salary weightings — Adjustments to remuneration
1 1 9 0 Salary weightings
Budget 2004 | Amending budget | New amounts |
50508 | –7887 | 42621 |
Staff Regulations applicable to officials of the European Communities, and in particular Articles 64 and 65 thereof.
This appropriation is intended to cover the impact of salary weightings applicable to the remuneration of officials and temporary staff and to overtime.
TITLE 2
BUILDINGS, EQUIPMENT AND MISCELLANEOUS OPERATING EXPENDITURE
CHAPTER 2 0 — RENTAL OF BUILDINGS AND ASSOCIATED COSTS
2 0 4 Fitting-out of premises
Budget 2004 | Amending budget | New amounts |
44000 | 50000 | 94000 |
This appropriation is intended to cover the fitting-out of the premises and repairs in the building.
CHAPTER 2 1 — INFORMATION TECHNOLOGY
2 1 3 Technical installations and electronic office equipment
2 1 3 0 New and replacement purchases
Budget 2004 | Amending budget | New amounts |
59100 | 37706 | 96806 |
This appropriation is intended to cover the purchase and installation of technical and data-processing equipment.
The purchase of replacement equipment and installations under this item is conditional upon the discarding, in accordance with established procedure, of the technical and data-processing equipment and installations to be replaced.
TITLE 3
OPERATING EXPENDITURE
CHAPTER 3 0 — OPERATING EXPENDITURE
3 0 0 Focal points activities including Expert Group activities, meetings and interpretation costs
3 0 0 1 Focal point subsidy
Budget 2004 | Amending budget | New amounts |
580000 | 360000 | 940000 |
This appropriation is intended to support the establishment and management of an Agency network of Focal Points web-sites and to stimulate European Week activities in the Member States via grant agreements between the Agency and the Focal Points.
3 0 4 Editing, publication and distribution of information, and other activities
3 0 4 0 Publication and distribution of results of studies and other information activities and corporate products
Budget 2004 | Amending budget | New amounts |
250000 | 60303 | 310303 |
This appropriation is intended to cover the publication and distribution costs (including databases and mailing) of results of studies, other information activities and corporate publications (annual report, newsletter, magazine, Agency budget, etc.) which will contribute to the implementation of the work programme.
This item is merged with former Item 2 3 9 3.
3 0 6 Translation and interpretation
3 0 6 0 Translation of studies, reports and working documents
Budget 2004 | Amending budget | New amounts |
549070 | 163697 | 712767 |
This appropriation is intended to cover the translation of studies, reports, as well as working documents for the Administrative Board and Bureau and for congresses, seminars, etc. into the different Community languages. The translation work will mainly be carried out by the Translation Centre for the bodies of the European Union in Luxembourg. Exceptionally, however, recourse may be made to freelance translators where the Translation Centre is unable for any reason to carry out the work.
ESTABLISHMENT PLAN FOR THE AGENCY
TEMPORARY PERSONNEL
Category and Grade | Budget 2004 | Amending B. | New total |
A7/6 | 6 | 3 | 9 |
Total grade A | 6 | 3 | 9 |
B5/4 | 4 | 1 | 5 |
Total grade B | 4 | 1 | 5 |
C5/4 | 2 | 1 | 3 |
Total grade C | 2 | 1 | 3 |
TOTAL | 12 | 5 | 17 |
--------------------------------------------------
