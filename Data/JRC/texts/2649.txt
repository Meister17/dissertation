Commission Directive 2005/63/EC
of 3 October 2005
correcting Directive 2005/26/EC concerning the list of food ingredients or substances provisionally excluded from Annex IIIa of Directive 2000/13/EC of the European Parliament and of the Council
(Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Directive 2000/13/EC of the European Parliament and of the Council of 20 March 2000 on the approximation of the laws of the Member States relating to the labelling, presentation and advertising of foodstuffs [1] and in particular Article 6 (11), thereof,
Whereas:
(1) Commission Directive 2005/26/EC [2] established the list of food ingredients or substances provisionally excluded from Annex IIIa of Directive 2000/13/EC, having heard the opinion of the European Food Safety Authority (EFSA).
(2) In its opinion of 2 December 2004 on certain uses of fish gelatine, the EFSA concluded that this product, when used as a carrier for vitamin and carotenoid preparations, is not likely to cause severe allergic reactions.
(3) Carotenoids were wrongly omitted from the list in annex to Directive 2005/26/EC and must therefore be added to it,
HAS ADOPTED THIS DIRECTIVE:
Article 1
In the second column of the Annex to Directive 2005/26/EC, the seventh row shall be replaced by the following:
"— Fish gelatine used as a carrier for vitamin or carotenoid preparations and flavours."
Article 2
1. The Member States shall adopt and publish, by 3 December 2005 at the latest, the rules, regulations and administrative provisions necessary to comply with the present Directive. They shall immediately communicate the text of those provisions to the Commission together with a correlation table those provisions and this Directive.
They shall apply those provisions from 25 November 2005.
When the Member States adopt those provisions, they shall contain a reference to this Directive or be accompanied by a reference at the time of their official publication. The Member States shall determine how such reference is to be made.
2. The Member States shall communicate to the Commission the text of the main provisions of national law which they adopt in the field covered by this Directive.
Article 3
This Directive shall enter into force on the day following that of its publication in the Official Journal of the European Union.
Article 4
This Directive is addressed to the Member States.
Done at Brussels, 3 October 2005.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 109, 6.5.2000, p. 29. Directive as last amended by Directive 2003/89/EC (OJ L 308, 25.11.2003, p. 15).
[2] OJ L 75, 22.3.2005, p. 33.
--------------------------------------------------
