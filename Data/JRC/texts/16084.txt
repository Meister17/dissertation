Commission Regulation (EC) No 907/2006
of 20 June 2006
amending Regulation (EC) No 648/2004 of the European Parliament and of the Council on detergents, in order to adapt Annexes III and VII thereto
(Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Regulation (EC) No 648/2004 of the European Parliament and of the Council of 31 March 2004 on detergents [1], and in particular Article 13(1) thereof,
Whereas:
(1) Regulation (EC) No 648/2004 ensures the free circulation of detergents on the internal market while at the same time providing a high level of protection to the environment and to human health by laying down rules for the ultimate biodegradation of surfactants for detergents, and for the labelling of detergent ingredients.
(2) Some of the methods laid down in Annex III to Regulation (EC) No 648/2004, e.g. the ISO 14593 reference method, are also applicable for testing substances that are poorly-soluble in water, if adequate dispersion of the substance is ensured. More guidance for testing poorly-soluble substances is given in ISO 10634. However, an additional test method should be introduced for use with surfactants that are poorly-soluble in water. The additional test method proposed is ISO standard 10708:1997 "Water quality — Evaluation in an aqueous medium of the ultimate aerobic biodegradability of organic compounds". The Scientific Committee on Health and Environmental Risks (SCHER) concluded that ISO 10708 is of an equivalent standard to the test methods already included in Annex III to that Regulation and was in favour of its use.
(3) To ensure a high level of health protection, information concerning detergent composition needs to be made more easily available to the general public. The address of a website should therefore be provided on the packaging of the detergent from which the list of ingredients mentioned in Section D of Annex VII to Regulation (EC) No 648/2004 can be easily obtained.
(4) There is a requirement to declare allergenic fragrances if they are added in the form of pure substances. However there is no requirement to declare them if they are added as constituents of complex ingredients such as essential oils or perfumes. To ensure better transparency to the consumer, allergenic fragrances in detergents should be declared irrespective of the way they are added to the detergent.
(5) The list of ingredients intended for the general public that is given in section D of Annex VII to Regulation (EC) No 648/2004 requires the use of specialised scientific nomenclature that may hinder rather than help the general public. Moreover, there are some minor inconsistencies between the information made available to the general public compared to that to be made available to medical personnel under section C of the same Annex. The ingredient information for the general public should be made more easily understandable by using the INCI nomenclature already in use for cosmetic ingredients, and sections C and D should be made compatible.
(6) The definition of "detergent" in the Regulation makes it clear that the rules on labelling apply to all detergents whether they contain surfactants or not. However, section D of Annex VII to Regulation (EC) No 648/2004 lays down different rules for those industrial and institutional detergents that contain surfactants compared with those that do not. This difference in labelling requirements serves no useful purpose and should be eliminated.
(7) Annexes III and VII to Regulation (EC) No 648/2004 should be amended accordingly. In the interest of clarity it is appropriate to replace those Annexes.
(8) The measures provided for in this Regulation are in accordance with the opinion of the detergents Committee,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 648/2004 is amended as follows:
1. Annex III is replaced by the text set out in Annex I to this Regulation.
2. Annex VII is replaced by the text set out in Annex II to this Regulation.
Article 2
Entry into force
This Regulation shall enter into force on the 20th day following its publication in the Official Journal of the European Union.
Article 1(2) shall apply from the day falling six months after the entry into force of this Regulation.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 20 June 2006.
For the Commission
Günter Verheugen
Vice-President
[1] OJ L 104, 8.4.2004, p. 1.
--------------------------------------------------
ANNEX I
"ANNEX III
ULTIMATE BIODEGRADABILITY (MINERALISATION) TEST METHODS FOR SURFACTANTS IN DETERGENTS
A. The reference method for laboratory testing of surfactant ultimate biodegradability in this Regulation is based on the EN ISO standard 14593: 1999 (CO2 headspace test).
Surfactants in detergents shall be considered as biodegradable if the level of biodegradability (mineralisation) measured according to one of the following tests [1] is at least 60 % within 28 days:
1. EN ISO Standard 14593: 1999 — Water quality — Evaluation of ultimate aerobic biodegradability of organic compounds in aqueous medium — Method by analysis of inorganic carbon in sealed vessels (CO2 headspace test). Pre-adaptation is not to be used. The 10-day window principle is not applied (reference method).
2. Directive 67/548/EEC method, Annex V.C.4-C (carbon dioxide (CO2) Eeolution modified Sturm test): pre-adaptation is not to be used. The 10-day window principle is not applied.
3. Directive 67/548/EEC method, Annex V.C.4-E (closed Bottle): pre-adaptation is not to be used. The 10-day window principle is not applied.
4. Directive 67/548/EEC method, Annex V.C.4-D (manometric respirometry): pre-adaptation is not to be used. The 10-day window principle is not applied.
5. Directive 67/548/EEC method, Annex V.C.4-F (MITI: Ministry of International Trade and Industry, Japan): pre-adaptation is not to be used. The 10-day window principle is not applied.
6. ISO 10708:1997 — Water quality — Evaluation in an aqueous medium of the ultimate aerobic biodegradability of organic compounds — Determination of biochemical oxygen demand in a two-phase closed bottle test. Pre-adaptation is not to be used. The 10-day window principle is not applied.
B. Depending on the physical characteristics of the surfactant, one of the methods listed below may be used if appropriately justified [2]. It should be noted that the pass criterion of at least 70 % of these methods is to be considered as equivalent to the pass criterion of at least 60 % referred to in methods listed in point A. The adequacy of the choice of the methods listed below shall be decided on a case-by-case confirmation, in accordance with Article 5 of this Regulation.
1. Directive 67/548/EEC method, Annex V.C.4-A (dissolved organic carbon DOC die-away): pre-adaptation is not to be used. The 10-day window principle is not applied. The pass criteria for biodegradability measured according to the test shall be at least 70 % within 28 days.
2. Directive 67/548/EEC method, Annex V.C.4-B (modified OECD screening-DOC die-away): pre-adaptation is not to be used. The 10-day window principle is not applied. The pass criteria for biodegradability measured according to the test shall be at least 70 % within 28 days.
NB: Those of the abovementioned methods that are taken from Council Directive 67/548/EEC can also be found in the publication "Classification, Packaging and Labelling of Dangerous Substances in the European Union", Part 2: "Testing Methods". European Commission 1997, ISBN 92-828-0076-8.
[1] These tests are identified as the most suitable for surfactants.
[2] The DOC methods could give results on the removal and not on the ultimate biodegradation. The manometric respirometry and the MITI and two-phase BOD methods would not be appropriate in some cases because the high initial test concentration could be inhibitory."
--------------------------------------------------
ANNEX II
"ANNEX VII
LABELLING AND INGREDIENT DATA SHEET
A. Labelling of contents
The following provisions on labelling shall apply to the packaging of detergents sold to the general public.
The following weight percentage ranges:
- less than 5 %,
- 5 % or over but less than 15 %,
- 15 % or over but less than 30 %,
- 30 % and more,
shall be used to indicate the content of the constituents listed below where they are added in a concentration above 0,2 % by weight:
- phosphates,
- phosphonates,
- anionic surfactants,
- cationic surfactants,
- amphoteric surfactants,
- non-ionic surfactants,
- oxygen-based bleaching agents,
- chlorine-based bleaching agents,
- EDTA and salts thereof,
- NTA (nitrilotriacetic acid) and salts thereof,
- phenols and halogenated phenols,
- paradichlorobenzene,
- aromatic hydrocarbons,
- aliphatic hydrocarbons,
- halogenated hydrocarbons,
- soap,
- zeolites,
- polycarboxylates.
The following classes of constituent, if added, shall be listed irrespective of their concentration:
- enzymes,
- disinfectants,
- optical brighteners,
- perfumes.
If added, preservation agents shall be listed, irrespective of their concentration, using where possible the common nomenclature established under Article 8 of Council Directive 76/768/EEC of 27 July 1976 on the approximation of laws of the Member States relating to cosmetic products [1].
If added at concentrations exceeding 0,01 % by weight, the allergenic fragrances that appear on the list of substances in Annex III, Part 1 to Directive 76/768/EEC, as a result of its amendment by Directive 2003/15/EC of the European Parliament and of the Council [2] to include the allergenic perfume ingredients from the list first established by the Scientific Committee on Cosmetics and Non-food Products (SCCNFP) in its opinion SCCNFP/0017/98, shall be listed using the nomenclature of that Directive, as shall any other allergenic fragrances that are subsequently added to Annex III, Part 1 to Directive 76/768/EEC by adaptation of that Annex to technical progress.
If individual risk-based concentration limits for fragrance allergens are subsequently established by the SCCNFP, the Commission shall propose the adoption, in accordance with Article 12(2), of such limits to replace the limit of 0,01 % mentioned above.
The website address, from which the list of ingredients mentioned in section D of Annex VII can be obtained, shall be given on the packaging.
For detergents intended to be used in the industrial and institutional sector, and not made available to members of the general public, the abovementioned requirements do not have to be fulfilled if the equivalent information is provided by means of technical data sheets, safety data sheets, or in a similar appropriate manner.
B. Labelling of dosage information
As prescribed in Article 11(4), the following provisions on labelling shall apply to the packaging of detergents sold to the general public. The packaging of detergents sold to the general public intended to be used as laundry detergents shall bear the following information:
- the recommended quantities and/or dosage instructions expressed in millilitres or grams appropriate to a standard washing machine load, for soft, medium and hard water hardness levels and making provision for one or two cycle washing processes,
- for heavy-duty detergents, the number of standard washing machine loads of "normally soiled" fabrics, and, for detergents for delicate fabrics, the number of standard washing machine loads of lightly-soiled fabrics, that can be washed with the contents of the package using water of medium hardness, corresponding to 2,5 millimoles CaCO3/l,
- the capacity of any measuring cup, if provided, shall be indicated in millilitres or grams, and markings shall be provided to indicate the dose of detergent appropriate for a standard washing machine load for soft, medium and hard water hardness levels.
The standard washing machine loads are 4,5 kg dry fabric for heavy-duty detergents and 2,5 kg dry fabric for light-duty detergents in line with the definitions of Commission Decision 1999/476/EC of 10 June 1999 establishing the Ecological Criteria for the award of the Community eco-label to Laundry Detergents [3]. A detergent shall be considered to be a heavy-duty detergent unless the claims of the manufacturer predominantly promotes fabric care i.e. low temperature wash, delicate fibres and colours.
C. Ingredient data sheet
The following provisions shall apply to the listing of ingredients on the data sheet referred to in Article 9(3).
The data sheet shall list the name of the detergent and that of the manufacturer.
All ingredients shall be listed; in order of decreasing abundance by weight, and the list shall be sub-divided into the following weight percentage ranges:
- 10 % or more,
- 1 % or over, but less than 10 %,
- 0,1 % or over, but less than 1 %,
- less than 0,1 %.
Impurities shall not be considered to be ingredients.
"Ingredient" means any chemical substance, of synthetic or natural origin, intentionally included in the composition of a detergent. For the purpose of this Annex, a perfume, an essential oil, or a colouring agent shall be considered to be a single ingredient and none of the substances that they contain shall be listed, with the exception of those allergenic fragrance substances that appear on the list of substances in Annex III, Part 1 to Directive 76/768/EEC if the total concentration of the allergenic fragrance substance in the detergent exceeds the limit mentioned in section A.
The common chemical name or IUPAC [4] name and, where available, the INCI [5] name, the CAS number, and the European Pharmacopoeia name, shall be given for each ingredient.
D. Publication of the list of ingredients
Manufacturers shall make available on a website the ingredient data sheet mentioned above except for the following:
- information on weight percentage ranges is not required
- CAS numbers are not required
- the ingredient names shall be given in INCI nomenclature, or where this is not available, the European Pharmacopoeia name, shall be given. If neither name is available, the common chemical name or IUPAC name shall be used instead. For a perfume the word "parfum" shall be used and for a colouring agent, the word "colorant". A perfume, an essential oil, or a colouring agent shall be considered to be a single ingredient and none of the substances that they contain shall be listed, with the exception of those allergenic fragrance substances that appear on the list of substances in Annex III, Part 1 to Directive 76/768/EEC if the total concentration of the allergenic fragrance substance in the detergent exceeds the limit mentioned in section A.
Access to the website shall not be subject to any restriction or condition and the content of the website shall be kept up to date. The website shall include a link to the Commission Pharmacos website or to any other suitable website that provides a table of correspondence between INCI names, European Pharmacopoeia names, and CAS numbers.
This obligation shall not apply to industrial or institutional detergents, or to surfactants for industrial or institutional detergents, for which a technical data sheet or safety data sheet is available.
[1] OJ L 262, 27.9.1976, p. 169. Directive as last amended by Commission Directive 2005/80/EC (OJ L 303, 22.11.2005, p. 32).
[2] OJ L 66, 11.3.2003, p. 26.
[3] OJ L 187, 20.7.1999, p. 52. Decision as last amended by Decision 2003/200/EC (OJ L 76, 22.3.2003, p. 25).
[4] International Union of Pure and Applied Chemistry.
[5] International Nomenclature Cosmetic Ingredient."
--------------------------------------------------
