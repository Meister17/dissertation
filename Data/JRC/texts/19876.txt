++++
COUNCIL DIRECTIVE
OF 21 DECEMBER 1978
PROHIBITING THE PLACING ON THE MARKET AND USE OF PLANT PROTECTION PRODUCTS CONTAINING CERTAIN ACTIVE SUBSTANCES
( 79/117/EEC )
THE COUNCIL OF THE EUROPEAN COMMUNITIES ,
HAVING REGARD TO THE TREATY ESTABLISHING THE EUROPEAN ECONOMIC COMMUNITY , AND IN PARTICULAR ARTICLE 100 THEREOF ,
HAVING REGARD TO THE PROPOSAL FROM THE COMMISSION ( 1 ) ,
HAVING REGARD TO THE OPINION OF THE EUROPEAN PARLIAMENT ( 2 ) ,
HAVING REGARD TO THE OPINION OF THE ECONOMIC AND SOCIAL COMMITTEE ( 3 ) ,
WHEREAS PLANT PRODUCTION HAS A VERY IMPORTANT PLACE IN THE EUROPEAN ECONOMIC COMMUNITY ;
WHEREAS THE YIELD FROM THAT PRODUCTION IS CONTINUALLY BEING AFFECTED BY HARMFUL ORGANISMS AND WEEDS ; WHEREAS IT IS ABSOLUTELY ESSENTIAL TO PROTECT PLANTS AGAINST THESE RISKS TO PREVENT A FALL IN YIELDS AND THEREBY HELP TO ENSURE THE AVAILABILITY OF SUPPLIES ;
WHEREAS ONE OF THE MOST IMPORTANT METHODS OF PROTECTING PLANTS AND PLANT PRODUCTS AND OF INCREASING THE PRODUCTIVITY OF AGRICULTURE IS TO USE PLANT PROTECTION PRODUCTS ;
WHEREAS THE EFFECTS OF THESE PLANT PROTECTION PRODUCTS MAY NOT BE WHOLLY FAVOURABLE FOR PLANT PRODUCTION ; WHEREAS THEIR USE MAY INVOLVE RISKS FOR MAN AND THE ENVIRONMENT SINCE , IN THE MAIN , THEY ARE TOXIC SUBSTANCES OR PREPARATIONS HAVING DANGEROUS EFFECTS ;
WHEREAS , IN THE CASE OF CERTAIN PLANT PROTECTION PRODUCTS , THESE RISKS ARE SO GREAT THAT THEIR USE CAN NO LONGER BE WHOLLY OR PARTIALLY TOLERATED ;
WHEREAS THE MEMBER STATES HAVE THEREFORE NOT ONLY CONTROLLED THE MARKETING OF PLANT PROTECTION PRODUCTS BUT HAVE ALSO INTRODUCED , FOR CERTAIN PLANT PROTECTION PRODUCTS , RESTRICTIONS OR PROHIBITIONS OF USE COVERING ALSO THEIR MARKETING ;
WHEREAS THESE PROVISIONS DIFFER IN THE VARIOUS MEMBER STATES AND RESULT IN OBSTACLES TO TRADE WHICH DIRECTLY AFFECT THE ESTABLISHMENT AND FUNCTIONING OF THE COMMON MARKET ;
WHEREAS IT IS THEREFORE DESIRABLE TO ELIMINATE THESE OBSTACLES BY ALIGNING THE RELEVANT PROVISIONS LAID DOWN IN THE MEMBER STATES BY LAW , REGULATION OR ADMINISTRATIVE PROVISION ;
WHEREAS IT THEREFORE SEEMS JUSTIFIED , AS A BASIC PRINCIPLE , TO INTRODUCE PROHIBITIONS OF THE USE OF ALL PLANT PROTECTION PRODUCTS CONTAINING ACTIVE SUBSTANCES WHICH , EVEN WHEN PROPERLY USED FOR THE PURPOSE INTENDED , GIVE RISE OR ARE LIKELY TO GIVE RISE TO HARMFUL EFFECTS ON HUMAN OR ANIMAL HEALTH OR TO UNREASONABLE ADVERSE EFFECTS ON THE ENVIRONMENT ;
WHEREAS FOR SOME OF THESE PLANT PROTECTION PRODUCTS NATIONAL DEROGATIONS MAY , HOWEVER , BE PERMITTED TO A LIMITED EXTENT FOR THOSE USES WHERE , FOR ECOLOGICAL REASONS , A PARTICULAR NEED EXISTS AND WHERE THE RISK IS LESS THAN IN THE CASE OF THEIR OTHER PREVIOUSLY SANCTIONED USES ;
WHEREAS EVEN THESE DEROGATIONS SHOULD BE PHASED OUT AS SOON AS LESS HAZARDOUS TREATMENTS BECOME AVAILABLE ;
WHEREAS , ON THE OTHER HAND , IT IS NECESSARY TO GRANT MEMBER STATES A LIMITED RIGHT TO SUSPEND TEMPORARILY , ON THEIR OWN AUTHORITY , PROHIBITIONS OF USE IN THE EVENT OF AN UNFORESEEABLE DANGER THREATENING PLANT PRODUCTION WHICH CANNOT BE CONTAINED BY OTHER MEANS ;
WHEREAS THE DIRECTIVE EXCLUDES PLANT PROTECTION PRODUCTS INTENDED FOR RESEARCH AND ANALYTICAL PURPOSES ;
WHEREAS , MOREOVER , IT IS NOT APPROPRIATE TO APPLY COMMUNITY PROVISIONS TO PLANT PROTECTION PRODUCTS INTENDED FOR EXPORT TO THIRD COUNTRIES , SINCE IN GENERAL THESE COUNTRIES HAVE THEIR OWN REGULATIONS ;
WHEREAS THE IMPLEMENTATION OF THIS DIRECTIVE AND THE ADAPTATION OF ITS ANNEX TO THE DEVELOPMENT OF TECHNICAL AND SCIENTIFIC KNOWLEDGE NECESSITATES A CLOSE COOPERATION BETWEEN THE COMMISSION AND MEMBER STATES ; WHEREAS THE PROCEDURE - FOR THE PRESENT , LIMITED IN TIME - OF THE STANDING COMMITTEE ON PLANT HEALTH AND INVOLVEMENT OF THE SCIENTIFIC COMMITTEE FOR PESTICIDES OFFERS A SUITABLE BASIS FOR THIS ,
HAS ADOPTED THIS DIRECTIVE :
ARTICLE 1
THIS DIRECTIVE CONCERNS PROHIBITION OF THE PLACING ON THE MARKET AND USE OF PLANT PROTECTION PRODUCTS CONTAINING CERTAIN ACTIVE SUBSTANCES .
ARTICLE 2
FOR THE PURPOSES OF THIS DIRECTIVE THE FOLLOWING DEFINITIONS SHALL APPLY :
1 . PLANT PROTECTION PRODUCTS
ACTIVE SUBSTANCES AND PREPARATIONS CONTAINING ONE OR MORE ACTIVE SUBSTANCES INTENDED :
1.1 . TO DESTROY ORGANISMS HARMFUL TO PLANTS OR PLANT PRODUCTS OR TO PROTECT THEM FROM SUCH ORGANISMS , IN SO FAR AS SUCH SUBSTANCES OR PREPARATIONS ARE NOT DEFINED IN THE FOLLOWING PROVISIONS ;
1.2 . TO INFLUENCE THE LIFE PROCESSES OF PLANTS , OTHER THAN AS A NUTRIENT ;
1.3 . TO PRESERVE PLANT PRODUCTS , IN SO FAR AS SUCH SUBSTANCES OR PRODUCTS ARE NOT SUBJECT TO SPECIAL COUNCIL OR COMMISSION PROVISIONS ON PRESERVATIVES ;
1.4 . TO DESTROY UNDESIRED PLANTS ; OR
1.5 . TO DESTROY PARTS OF PLANTS OR TO PREVENT UNDESIRED GROWTH OF PLANTS .
2 . SUBSTANCES
CHEMICAL ELEMENTS AND THEIR COMPOUNDS , AS THEY OCCUR NATURALLY OR BY MANUFACTURE .
3 . PREPARATIONS
MIXTURES OR SOLUTIONS COMPOSED OF TWO OR MORE SUBSTANCES , OR OF MICRO-ORGANISMS OR VIRUSES USED AS PLANT PROTECTION PRODUCTS .
4 . ACTIVE SUBSTANCES
SUBSTANCES , MICRO-ORGANISMS AND VIRUSES , HAVING GENERAL OR SPECIFIC ACTION :
4.1 . AGAINST HARMFUL ORGANISMS ; OR
4.2 . ON PLANTS , PARTS OF PLANTS OR PLANT PRODUCTS .
5 . PLANTS
LIVE PLANTS AND LIVE PARTS OF PLANTS , INCLUDING FRESH FRUIT AND SEED .
6 . PLANT PRODUCTS
PRODUCTS , IN THE UNPROCESSED STATE OR HAVING UNDERGONE ONLY SIMPLE PREPARATION SUCH AS MILLING , DRYING OR PRESSING , DERIVED FROM PLANTS , BUT EXCLUDING PLANTS THEMSELVES AS DEFINED IN POINT 5 .
7 . HARMFUL ORGANISMS
PESTS OF PLANTS OR OF PLANT PRODUCTS , WHICH BELONG TO THE ANIMAL OR PLANT KINGDOMS , OR WHICH ARE VIRUSES , MYCOPLASMAS OR OTHER PATHOGENS .
8 . ANIMALS
ANIMALS BELONGING TO SPECIES NORMALLY NOURISHED AND KEPT OR CONSUMED BY MAN .
9 . PLACING ON THE MARKET
ANY TRANSFER OF POSSESSION , WHETHER IN RETURN FOR PAYMENT OR FREE OF CHARGE .
10 . ENVIRONMENT
THE RELATIONSHIP OF HUMAN BEINGS WITH WATER , AIR , LAND AND ALL BIOLOGICAL FORMS .
ARTICLE 3
MEMBER STATES SHALL ENSURE THAT PLANT PROTECTION PRODUCTS CONTAINING ONE OR MORE OF THE ACTIVE SUBSTANCES LISTED IN THE ANNEX MAY BE NEITHER PLACED ON THE MARKET NOR USED .
THE FIRST PARAGRAPH SHALL NOT APPLY TO PLANT PROTECTION PRODUCTS WHICH CONTAIN NEGLIGIBLE IMPURITIES BECAUSE OF THE NATURE OF THEIR MANUFACTURING PROCESS , PROVIDED THAT THEY HAVE NO HARMFUL EFFECTS ON HUMAN BEINGS , ANIMALS OR THE ENVIRONMENT .
ARTICLE 4
1 . BY WAY OF DEROGATION FROM ARTICLE 3 , MEMBER STATES SHALL BE TEMPORARILY AUTHORIZED TO PERMIT THE PLACING ON THE MARKET OR THE USE WITHIN THEIR TERRITORY OF PLANT PROTECTION PRODUCTS CONTAINING CERTAIN OF THE ACTIVE SUBSTANCES LISTED IN THE FIRST COLUMN OF THE ANNEX IN THE CASES SET OUT IN THE SECOND COLUMN .
2 . MEMBER STATES SHALL INFORM THE OTHER MEMBER STATES AND THE COMMISSION OF ANY CASES IN WHICH PARAGRAPH 1 IS APPLIED AND , IF THE COMMISSION SO REQUESTS , SHALL PROVIDE IT WITH DETAILS OF THE EXTENT TO WHICH EACH SUCH ACTIVE SUBSTANCE IS USED .
ARTICLE 5
THE DIRECTIVE SHALL NOT APPLY TO PLANT PROTECTION PRODUCTS INTENDED FOR :
( A ) PURPOSES OF RESEARCH OR ANALYSIS ; OR
( B ) EXPORT TO THIRD COUNTRIES .
ARTICLE 6
1 . AFTER CONSULTATION BY THE COMMISSION OF THE SCIENTIFIC COMMITTEE ON PESTICIDES SET UP UNDER DECISION 78/436/EEC ( 4 ) THE FOLLOWING SHALL BE ADOPTED IN ACCORDANCE WITH THE PROCEDURE LAID DOWN IN ARTICLE 8 :
( A ) ANY AMENDMENTS NECESSARY WITHIN GROUPS A ( MERCURY COMPOUNDS ) AND B ( PERSISTENT ORGANOCHLORINE COMPOUNDS ) OF THE SUBSTANCES IN COLUMN 1 OF THE ANNEX ;
( B ) ANY AMENDMENTS NECESSARY TO COLUMN 2 OF THE ANNEX . IF A DEROGATION IS TO BE CANCELLED , PRIOR CONSULTATION OF THE SCIENTIFIC COMMITTEE SHALL NOT BE NECESSARY , PROVIDED THAT ALL MEMBER STATES HAVE INFORMED THE COMMISSION THAT THEY DO NOT INTEND OR NO LONGER INTEND TO AVAIL THEMSELVES OF THAT DEROGATION . THIS INFORMATION MAY BE SUPPLIED TO THE STANDING COMMITTEE ON PLANT HEALTH SET UP UNDER DECISION 76/894/EEC ( 5 ) .
2 . PARAGRAPH 1 SHALL APPLY FOR A PERIOD OF FIVE YEARS AS FROM 1 JANUARY 1981 .
THE COUNCIL , ACTING UNANIMOUSLY ON A PROPOSAL FROM THE COMMISSION , MAY DECIDE TO EXTEND THE PERIOD OF VALIDITY OF PARAGRAPH 1 OR TO REMOVE ANY LIMIT ON ITS APPLICATION .
3 . ANY AMENDMENTS TO THE ANNEX WHICH ARE NOT PROVIDED FOR IN PARAGRAPH 1 SHALL BE ADOPTED BY THE COUNCIL ACTING ON A PROPOSAL FROM THE COMMISSION .
4 . THE COMMISSION SHALL EXAMINE AT LEAST EVERY TWO YEARS WHETHER AND TO WHAT EXTENT THE SECOND COLUMN OF THE ANNEX SHOULD BE AMENDED .
5 . ALL AMENDMENTS TO THE ANNEX SHALL BE MADE BY REASON OF THE DEVELOPMENT OF SCIENTIFIC AND TECHNICAL KNOWLEDGE .
6 . AN ACTIVE SUBSTANCE SHALL BE INCLUDED IN THE ANNEX IF , EVEN WHEN PROPERLY APPLIED FOR THE PURPOSE INTENDED , ITS USE GIVES RISE OR IS LIKELY TO GIVE RISE TO :
( A ) HARMFUL EFFECTS ON HUMAN OR ANIMAL HEALTH ;
( B ) UNREASONABLE ADVERSE EFFECTS ON THE ENVIRONMENT .
ARTICLE 7
1 . IF IT APPEARS NECESSARY , BECAUSE OF AN UNFORESEEABLE DANGER THREATENING PLANT PRODUCTION WHICH CANNOT BE CONTAINED BY OTHER MEANS , TO USE IN A MEMBER STATE A PLANT PROTECTION PRODUCT CONTAINING ONE OR MORE OF THE ACTIVE SUBSTANCES LISTED IN THE ANNEX , THE MEMBER STATE IN QUESTION MAY PERMIT THE PLACING ON THE MARKET AND THE USE OF SUCH PRODUCT FOR A MAXIMUM PERIOD OF 120 DAYS . IT SHALL IMMEDIATELY INFORM THE OTHER MEMBER STATES AND THE COMMISSION OF ITS ACTION .
2 . IT SHALL BE ESTABLISHED WITHOUT DELAY IN ACCORDANCE WITH THE PROCEDURE LAID DOWN IN ARTICLE 8 WHETHER AND , IF SO , UNDER WHAT CONDITIONS THE ACTION TAKEN BY THE MEMBER STATE PURSUANT TO PARAGRAPH 1 MAY BE CONTINUED OR REPEATED .
ARTICLE 8
1 . WHERE THE PROCEDURE LAID DOWN IN THIS ARTICLE IS TO BE FOLLOWED , MATTERS SHALL BE REFERRED WITHOUT DELAY BY THE CHAIRMAN , EITHER ON HIS OWN INITIATIVE OR AT THE REQUEST OF A MEMBER STATE , TO THE STANDING COMMITTEE ON PLANT HEALTH , HEREINAFTER CALLED " THE COMMITTEE " .
2 . WITHIN THE COMMITTEE THE VOTES OF THE MEMBER STATES SHALL BE WEIGHTED AS PROVIDED FOR IN ARTICLE 148 ( 2 ) OF THE TREATY . THE CHAIRMAN SHALL NOT VOTE .
3 . THE COMMISSION REPRESENTATIVE SHALL SUBMIT A DRAFT OF THE MEASURES TO BE ADOPTED . THE COMMITTEE SHALL DELIVER ITS OPINION ON SUCH MEASURES WITHIN A TIME LIMIT SET BY THE CHAIRMAN HAVING REGARD TO THE URGENCY OF THE MATTER . OPINIONS SHALL BE ADOPTED BY A MAJORITY OF 41 VOTES .
4 . THE COMMISSION SHALL ADOPT THE MEASURES AND IMPLEMENT THEM FORTHWITH WHERE THEY ARE IN ACCORDANCE WITH THE OPINION OF THE COMMITTEE . WHERE THEY ARE NOT IN ACCORDANCE WITH THE OPINION OF THE COMMITTEE , OR IF NO OPINION IS DELIVERED , THE COMMISSION SHALL WITHOUT DELAY PROPOSE TO THE COUNCIL THE MEASURES TO BE ADOPTED . THE COUNCIL SHALL ADOPT THE MEASURES BY A QUALIFIED MAJORITY .
IF THE COUNCIL HAS NOT ADOPTED ANY MEASURES WITHIN THREE MONTHS OF THE PROPOSAL BEING SUBMITTED TO IT , THE COMMISSION SHALL ADOPT THE PROPOSED MEASURES AND IMPLEMENT THEM FORTHWITH .
ARTICLE 9
THE MEMBER STATES SHALL , NOT LATER THAN 1 JANUARY 1981 , BRING INTO FORCE THE LAWS , REGULATIONS AND ADMINISTRATIVE PROVISIONS NECESSARY TO COMPLY WITH THIS DIRECTIVE . THEY SHALL IMMEDIATELY INFORM THE COMMISSION THEREOF .
ARTICLE 10
THIS DIRECTIVE IS ADDRESSED TO THE MEMBER STATES .
DONE AT BRUSSELS , 21 DECEMBER 1978 .
FOR THE COUNCIL
THE PRESIDENT
OTTO GRAF LAMBSDORFF
( 1 ) OJ NO C 200 , 26 . 8 . 1976 , P . 10 .
( 2 ) OJ NO C 30 , 7 . 2 . 1977 , P . 38 .
( 3 ) OJ NO C 114 , 11 . 5 . 1977 , P . 16 .
( 4 ) OJ NO L 124 , 12 . 5 . 1978 , P . 16 .
( 5 ) OJ NO L 340 , 9 . 12 . 1976 , P . 25 .
ANNEX
NAMES OF ACTIVE SUBSTANCES OR GROUPS OF ACTIVE SUBSTANCES REFERRED TO IN ARTICLE 3*CASES IN WHICH ON THE MARKET OR USE ARE PERMITTED IN ACCORDANCE WITH ARTICLE 4*
A . MERCURY COMPOUNDS**
1 . MERCURIC OXIDE*AS A PAINT TO TREAT NECTRIA GALLIGENA ( CANKER ) ON POMACEOUS FRUIT TREES AFTER HARVESTING AND UNTIL BUDDING*
2 . MERCUROUS CHLORIDE ( CALOMEL ) * ( A ) AGAINST PLASMODIOPHORA ON BRASSICAE*
* ( B ) TREATMENT OF ONION SEEDS AND PLANTS AGAINST SCLEROTIUM*
* ( C ) TREATMENT OF ORNAMENTAL TURF AND TURF FOR SPORTS GROUNDS AGAINST SCLEROTINIA AND FUSARIUM*
3 . OTHER INORGANIC MERCURY COMPOUNDS**
4 . ALKYL MERCURY COMPOUNDS* ( A ) DIPPING OF FLOWER BULBS AND SEED POTATOES*
* ( B ) TREATMENT OF BASIC AND PREBASIC CEREAL SEED , EXCEPT MAIZE , AND SUGAR BEET SEED*
5 . ALKOXYALKYL AND ARYL MERCURY COMPOUNDS* ( A ) AS A PAINT TO TREAT NECTRIA GALLIGENA ( CANKER ) ON POMACEOUS FRUIT TREES AFTER HARVESTING AND UNTIL BUDDING*
* ( B ) IN AUTUMN TO PREVENT NECTRIA GALLIGENA ( CANKER ) IN BRAMLEY APPLE TREES IF NECESSARY IN NORTHERN IRELAND , AFTER AN EXCEPTIONALLY HUMID SUMMER*
* ( C ) DIPPING OF FLOWER BULBS AND SEED POTATOES*
* ( D ) SEED TREATMENT OF CEREALS , BEET , FLAX AND RAPE*
NAMES OF ACTIVE SUBSTANCES OR GROUPS OF ACTIVE SUBSTANCES REFERRED TO IN ARTICLE 3*CASES IN WHICH PLACING ON THE MARKET OR USE ARE PERMITTED IN ACCORDANCE WITH ARTICLE 4*
B . PERSISTENT ORGANO-CHLORINE COMPOUNDS**
1 . ALDRIN* ( A ) SOIL TREATMENT AGAINST OTIORRHYNCHUS IN NURSERIES , STRAWBERRY BEDS BEFORE PLANTING OUT , BEDS OF ORNAMENTALS AND VINEYARDS*
* ( B ) TREATMENT OF POTATOES GROWN IN FORMER PASTURELAND AGAINST AGRIOTES IN IRELAND AND THE UNITED KINGDOM*
* ( C ) TREATMENT OF NARCISSI WHICH HAVE TO REMAIN TWO OR THREE YEARS IN THE GROUND AGAINST MERODON EQUESTRIS , EUMERUS STRIGATUS AND EUMERUS TUBERCULATUS*
2 . CHLORDANE**
3 . DIELDRIN**
4 . DDT* ( A ) AS A DIP AGAINST HYLOBIUS IN CONIFER SEEDLINGS*
* ( B ) TREATMENT OF INDIVIDUAL TREES AGAINST SCOLYTIDAE TO COMBAT CERATOCYSTIS ULMI*
* ( C ) TREATMENT OF SUGAR BEET , POTATOES AND ORNAMENTAL TURF OR TURF FOR SPORTS GROUNDS AGAINST MELOLONTHA , AMPHIMALLON , PHYLLOPERTHA , CETONIA AND SERICA*
* ( D ) TREATMENT OF SUGAR BEET , POTATOES , STRAWBERRIES , CARROTS AND ORNAMENTALS AGAINST AGROTIS AND EUXOA*
* ( E ) TREATMENT OF CEREALS AGAINST TIPULA*
5 . ENDRIN* ( A ) AS AN ACARICIDE ON CYCLAMEN AND ON STRAWBERRY PROPAGATING MATERIAL*
* ( B ) TREATMENT AGAINST ARVICOLA TERRESTRIS L . IN ORCHARDS WITHOUT SUBCULTIVATION*
6 . HCH CONTAINING LESS THAN 99,0 % OF THE GAMMA ISOMER**
7 . HEPTACHLOR*TREATMENT AT THEIR PREPARATION STAGE OF BEET SEED AGAINST ATOMARIA LINEARIS , AGRIOTES SPEC . , MYRIAPODA AND COLLEMBOLA*
8 . HEXACHLOROBENZENE
