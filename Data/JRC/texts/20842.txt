Decision of the EEA Joint Committee
No 151/2002
of 8 November 2002
amending Annex XXI (Statistics) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex XXI to the Agreement was amended by Decision No 110/2002 of the EEA Joint Committee of 12 July 2002(1).
(2) Commission Decision 98/377/EC of 18 May 1998 adapting Annex I to Council Regulation (EEC) No 571/88 in view of the organisation of the Community surveys on the structure of agricultural holdings(2) is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
Point 23 (Council Regulation (EEC) No 571/88) of Annex XXI to the Agreement shall be amended as follows:
(a) the following indent shall be added:
"- 398 D 0377: Commission Decision No 98/377/EC of 18 May 1998 (OJ L 168, 13.6.1998, p. 29).";
(b) the text of adaptation (e) shall be replaced by the following:
"Annex I to Council Regulation (EEC) No 571/88 shall be supplemented by the list set out in Appendix 1 to the present Annex.";
(c) the heading "APPENDIX 1 TO ANNEX XXI" shall be inserted at the end of Annex XXI;
(d) the list in the Annex to the present Decision shall be inserted as the text of Appendix 1 to Annex XXI, under the subheading "LIST OF CHARACTERISTICS".
Article 2
The text of Decision 98/377/EC in the Icelandic and Norwegian languages, to be published in the EEA Supplement of the Official Journal of the European Communities, shall be authentic.
Article 3
This Decision shall enter into force on 9 November 2002, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee(3).
Article 4
This Decision shall be published in the EEA section of, and in the EEA Supplement to, the Official Journal of the European Communities.
Done at Brussels, 8 November 2002.
For the EEA Joint Committee
The President
Kjartan Jóhannsson
(1) OJ L 298, 31.10.2002, p. 35.
(2) OJ L 168, 13.6.1998, p. 29.
(3) No constitutional requirements indicated.
ANNEX
to Decision No 151/2002 of the EEA Joint Committee
>PIC FILE= "L_2003019EN.002702.TIF">
>PIC FILE= "L_2003019EN.002801.TIF">
>PIC FILE= "L_2003019EN.002901.TIF">
>PIC FILE= "L_2003019EN.003001.TIF">
>PIC FILE= "L_2003019EN.003101.TIF">
>PIC FILE= "L_2003019EN.003201.TIF">
>PIC FILE= "L_2003019EN.003301.TIF">
>PIC FILE= "L_2003019EN.003401.TIF">
>PIC FILE= "L_2003019EN.003501.TIF">
>PIC FILE= "L_2003019EN.003601.TIF">
>PIC FILE= "L_2003019EN.003701.TIF">
>PIC FILE= "L_2003019EN.003801.TIF">
