Council Decision
of 6 March 2003
establishing a Tripartite Social Summit for Growth and Employment
(2003/174/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 202 thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) The social partners are associated in the implementation of the coordinated employment strategy established by the Luxembourg European Council of 20 and 21 November 1997 within the Standing Committee on Employment established by Council Decision 70/532/EEC of the Council of 14 December 1970 setting up the Standing Committee on Employment of the European Communities(1).
(2) The Cologne European Council of 3 and 4 June 1999 set up a macroeconomic dialogue with the participation of representatives from the Council, the Commission, the European Central Bank and the social partners.
(3) The Lisbon European Council of 23 and 24 March 2000 set a new strategic goal for the next decade and agreed that achievement of that goal required an overall strategy designed to integrate structural reforms, a coordinated European employment strategy, social protection and macroeconomic policies in the context of coordination of the general economic policies of the Member States. In its Communication "The European social dialogue, a force for innovation and change", the Commission stressed that the Tripartite Social Summit could contribute to the debate on these topics.
(4) In their joint contribution to the Laeken European Council of 14 and 15 December 2001, the social partners pointed out that the Standing Committee on Employment had not led to such an integration of concertation and that it did not meet the need for coherence and synergy between the various processes in which they were involved. They therefore proposed that it should be abolished and that a new form of tripartite consultation should be established.
(5) In that joint contribution, the social partners proposed to formalise their meetings with the Troika at the level of Heads of State or Government and the Commission which, in the context of the Luxembourg process, have been held since 1997 on the eve of European Councils. Since December 2000, these meetings have been known as Social Summits and they are attended by the President of the Commission and the Troika of Heads of State or Government together with the Ministers for Labour and Social Affairs and the social partners represented by the Union of Industrial Employers' Confederations of Europe (UNICE), the European Centre of Enterprises with Public Participation and of Enterprises of General Economic Interest (CEEP), the European Association of Craft, Small and Medium-sized Enterprises (UEAPME), the European Trade Union Confederation (ETUC), Eurocadres and the Confédération européenne des cadres (CEC).
(6) The Laeken European Council took note of the social partners' willingness to develop and improve coordination of concertation on the various aspects of the Lisbon strategy. It agreed that such a Social Summit would in future be held before each spring European Council. This was confirmed by the Barcelona European Council of 15 and 16 March 2002.
(7) This Decision is without prejudice to the organisation and operation of the social dialogue in the various Member States.
(8) In the context of enlargement and development of an autonomous social dialogue, the widest possible representativity as well as the legitimacy and effectiveness of the consultation of social partners are of the highest importance. To that end, it is necessary to take into account the Commission study on the representativeness of the social partners and the revised list of organisations involved in all dimensions of social dialogue at European level, and that list should be updated,
HAS DECIDED AS FOLLOWS:
Article 1
Establishment
A Tripartite Social Summit for Growth and Employment (hereinafter referred to as "the Summit") is hereby established.
Article 2
Task
The task of the Summit shall be to ensure, in compliance with the Treaty and with due regard for the powers of the institutions and bodies of the Community, that there is a continuous concertation between the Council, the Commission and the social partners. It will enable the social partners at European level to contribute, in the context of their social dialogue, to the various components of the integrated economic and social strategy, including the sustainable development dimension as launched at the Lisbon European Council in March 2000 and supplemented by the Göteborg European Council in June 2001. For that purpose, it shall draw on the upstream work of and discussions between the Council, the Commission and the social partners in the different concertation forums on economic, social and employment matters.
Article 3
Membership
1. The Summit shall consist of the Council Presidency and the two subsequent Presidencies, the Commission and the social partners, represented at the highest level.
The ministers from those three Presidencies and the Commissioner, responsible for Labour and Social Affairs shall also be present.
Depending on the agenda, other ministers from these three Presidencies and other Commissioners may also be invited to take part.
2. The social partners' representatives shall be divided into two delegations of equal size comprising 10 workers' representatives and 10 employers' representatives, taking into account the need to ensure a balanced participation between men and women.
Each delegation shall consist of representatives of European cross-industry organisations either representing general interests or more specific interests of supervisory and managerial staff and small and medium-sized businesses at European level.
The technical coordination shall be provided for the workers' delegation by the European Trade Union Confederation (ETUC) and for the employers' delegation by the Union of Industrial and Employers' Confederations of Europe (UNICE). The ETUC and UNICE shall ensure that the views expressed by the specific and sectoral organisations are fully taken into account in their contributions and shall, where appropriate, include representatives from some of those organisations in their delegations.
Article 4
Preparation
1. The agenda for the Summit shall be determined jointly by the Council Presidency, the Commission and the workers' and employers' cross-industry organisations taking part in the work of the Summit during preparatory meetings.
2. The matters on the agenda shall be discussed by the Council meeting in its Employment, Social Policy, Health and Consumer Affairs configuration.
3. The Summit secretariat shall be provided by the Commission. In particular the latter shall ensure the punctual distribution of documents. For the purposes of preparing and organising meetings, the Summit secretariat shall arrange appropriate contacts with the ETUC and UNICE, which shall be responsible for the coordination of their respective delegations.
Article 5
Operation
1. The Summit shall meet at least once a year. A meeting shall be held before the spring European Council.
2. The Summit shall be chaired jointly by the President of the Council and the President of the Commission.
3. The meetings of the Summit shall be convened by the joint chairmen on their own initiative, in consultation with the social partners.
Article 6
Information
The joint chairmen shall draw up a summary of the Summit's discussions in order to inform the relevant Council configurations and the general public.
Article 7
Repeal
Decision 1999/207/EC is hereby repealed from the date of the first meeting of the Summit.
Article 8
Entry into force
This Decision shall enter into force on 6 March 2003.
Done at Brussels, 6 March 2003.
For the Council
The President
D. Reppas
(1) OJ L 273, 17.12.1970, p. 25. Decision as last amended by Decision 1999/207/EC (OJ L 72, 18.3.1999, p. 33).
