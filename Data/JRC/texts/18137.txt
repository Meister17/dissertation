P6_TA(2004)0095
Framework for the transparency of qualifications and competences (Europass) ***II
European Parliament legislative resolution on the Council common position for adopting a decision of the European Parliament and of the Council on a single Community framework for the transparency of qualifications and competences (Europass) (12242/1/2004 — C6-0158/2004 — 2003/0307(COD))
(Codecision procedure: second reading)
The European Parliament,
- having regard to the Council common position (12242/1/2004 — C6-0158/2004),
- having regard to its position at first reading [1] on the Commission proposal to Parliament and the Council (COM(2003)0796) [2],
- having regard to Article 251(2) of the EC Treaty,
- having regard to Rule 67 of its Rules of Procedure,
- having regard to the recommendation for second reading of the Committee on Culture and Education (A6-0056/2004),
1. Approves the common position;
2. Notes that the act is adopted in accordance with the common position;
3. Instructs its President to sign the act with the President of the Council pursuant to Article 254(1) of the EC Treaty;
4. Instructs its Secretary-General to sign the act, once it has been verified that all the procedures have been duly completed, and, in agreement with the Secretary-General of the Council, to have it published in the Official Journal of the European Union;
5. Instructs its President to forward its position to the Council and Commission.
[1] Texts Adopted, 22.4.2004, P5_TA(2004)0362.
[2] Not yet published in OJ.
--------------------------------------------------
