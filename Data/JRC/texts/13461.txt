Order of the Court (Fourth Chamber) of 13 July 2006 (reference for a preliminary ruling from the Tribunale civile di Bolzano — Italy) — Eurodomus srl
v Comune di Bolzano
(Case C-166/06) [1]
Referring court
Tribunale civile di Bolzano
Parties to the main proceedings
Applicant: Eurodomus srl
Defendant: Comune di Bolzano
Re:
Reference for a preliminary ruling — Tribunale civile di Bolzano — Interpretation of Article 6(2) of the Treaty on European Union — Provincial administration adopting regulations capable of impeding the application of a decision of an administrative court that is res judicata — Compatibility with Community law
Operative part of the order
The reference for a preliminary ruling made by the Tribunale civile di Bolzano, by decision of 4 January 2006, is inadmissible.
[1] OJ C 154, 01.07.2006
--------------------------------------------------
