Information communicated by Member States regarding State aid granted under Commission Regulation (EC) No 70/2001 of 12 January 2001 on the application of Articles 87 and 88 of the EC Treaty to State aid to small and medium-sized enterprises, as amended by Commission Regulation (EC) No 364/2004 of 25 February 2004 as regards the extension of its scope to include aid for research and development
(2005/C 336/03)
(Text with EEA relevance)
Aid No | XS 72/2004 |
Member State | Spain |
Region | Catalonia |
Title of aid scheme or name of company receiving individual aid | Aid for requesting agreed prior assessments or reasoned reports in relation to tax credits for RD and TI projects |
Legal basis | Orden TRI/235/2004, de 21 de junio, por la que se aprueban las bases para el otorgamiento de ayudas para la solicitud de acuerdos previos de valoración o de informes motivados en relación con la deducción fiscal de proyectos de R+D+IT y se abre la convocatoria para el año 2004. (DOGC 4174 de 14/07/04) |
Annual expenditure planned or overall amount of aid granted to the company | Aid scheme | Annual overall amount | EUR 0,3m |
Loans guaranteed | |
Individual aid | Overall aid amount | |
Loans guaranteed | |
Maximum aid intensity | In conformity with Articles 4(2)-(6) and 5 of the Regulation | Yes | |
Date of implementation | 15.7.2004 |
Duration of scheme or individual aid award | Until 31.12.2006 |
Objective of aid | Aid to SMEs | Yes | |
Economic sectors concerned | All sectors eligible for aid to SMEs | Yes |
Name and address of the granting authority | Name: Centro de Innovación y Desarrollo Empresarial |
Address: Passeig de Gràcia, 129, 6a planta E-08008 Barcelona |
Large individual aid grants | In conformity with Article 6 of the Regulation | Yes | |
Aid No | XS 82/04 |
Member State | Italy |
Region | Piedmont |
Title of aid scheme or name of company receiving individual aid | Assistance in favour of industrial-research projects and pre-competitive development |
Legal basis | Deliberazione della Giunta Regionale n 63 — 13094 del 19 luglio 2004 attuativa ed integrativa a livello regionale del regime d'aiuto previsto dall'art. 11 della Legge 598/84 e s.m.i già approvato dalla Commissione con Decisione C(2002)691cor. del 05.3.02 |
Annual expenditure planned or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount | EUR 100m |
Loans guaranteed | |
Individual aid | Overall aid amount | |
Loans guaranteed | |
Maximum aid intensity | In conformity with Articles 4(2)-(6) and 5 of the Regulation | Yes | |
Date of implementation | 13.10.2004 |
Duration of scheme or individual aid award | Until 31.12.2006 |
Objective of aid | Aid to SMEs | Yes | |
Economic sectors concerned | All sectors eligible for aid to SMEs | Yes |
Name and address of the granting authority | Name: Regione Piemonte — Assessorato all'Industria, Lavoro e Bilancio — Direzione Industria. |
Address: Regione Piemonte Direzione Industria Via Pisano, 6 — I-10152 Torino Tel. (011) 432 14 61 — Fax (011) 432 34 83 e-mail: direzione16@regione.piemonte.it |
Large individual aid grants | In conformity with Article 6 of the Regulation | Yes | |
Aid No | XS 85/04 |
Member State | United Kingdom |
Region | West Wales The Valleys Objective 1 Region |
Title of aid scheme or name of company receiving individual aid | Welsh Highland Railway Ltd. |
Legal basis | Industrial Development Act 1982 |
Annual expenditure planned under the scheme or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount | |
Loans guaranteed | |
Individual aid | Overall aid amount | £3728735 (reconstructed line costs within the meaning of Section 13 of Regulation 70/2001) |
Loans guaranteed | |
Maximum aid intensity | In conformity with Articles 4(2)-(6) and 5 of the Regulation | Yes | |
Date of implementation | From 26.8.2004 |
Duration of scheme or individual aid award | Until 31.12.2006 NB As noted above, the Grant was committed prior to 31 December 2006. Payments against this commitment will, potentially (in line with N+2), continue until 30 June 2008 |
Objective of aid | Aid to SMEs | Yes | |
Economic sectors concerned | All sectors eligible for aid to SMEs | No |
Limited to specific sectors | Yes |
Other services (Tourism: Infrastructure) | Yes |
Name and address of the granting authority | Name: Welsh European Funding Office |
Address: Cwm Cynon Business Park Mountain Ash CF45 4ER, United Kingdom |
Large individual aid grants | In conformity with Article 6 of the Regulation | Yes | |
Aid No | XS 90/04 |
Member State | United Kingdom |
Region | West Wales and the Valleys Objective 1 Area |
Title of aid scheme or name of company receiving individual aid | Broadlands Industrial Park Ltd. |
Legal basis | Industrial Development Act 1982 |
Annual expenditure planned under the scheme or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount | |
Loans guaranteed | |
Individual aid | Overall aid amount | GBP 229,375 |
Loans guaranteed | |
Maximum aid intensity | In conformity with Articles 4(2)-(6) and 5 of the Regulation | Yes | |
Date of implementation | From 1.11.2004 |
Duration of scheme or individual aid award | Until 31.3.2005 |
Objective of aid | Aid to SMEs | Yes | |
Economic sectors concerned | All sectors eligible for aid to SMEs | Yes |
Name and address of the granting authority | Name: Welsh European Funding Office |
Address: Cwm Cynon Business Park Mountain Ash CF45 4ER, United Kingdom |
Large individual aid grants | In conformity with Article 6 of the Regulation | Yes | |
Aid No | XS 92/04 |
Member State | United Kingdom |
Region | West Wales The Valleys Objective 1 Region |
Title of aid scheme or name of company receiving individual aid | Recreating Pride In Cardigan |
Legal basis | Industrial Development Act 1982 |
Annual expenditure planned under the scheme or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount | GBP 640000 |
Loans guaranteed | |
Individual aid | Overall aid amount | |
Loans guaranteed | |
Maximum aid intensity | In conformity with Articles 4(2)-(6) and 5 of the Regulation | Yes | |
Date of implementation | From 1.9.2004 |
Duration of scheme or individual aid award | Until 31.12.2006 NB As noted above, the Grant was committed prior to 31 December 2006. Payments against this commitment will, potentially (in line with N+2), continue until 31 August 2007. |
Objective of aid | Aid to SMEs | Yes | |
Economic sectors concerned | All sectors eligible for aid to SMEs | Yes |
Name and address of the granting authority | Name: Welsh European Funding Office |
Address: Cwm Cynon Business Park Mountain Ash CF45 4ER, United Kingdom |
Large individual aid grants | In conformity with Article 6 of the Regulation | Yes | |
Aid No | XS 135/04 |
Member State | Poland |
Region | All 16 provinces |
Title of aid scheme or name of company receiving individual aid | Financial assistance granted to businesses in the form of non-repayable financial aid for consultancy purposes |
Legal basis | Art. 6 ustawy z dnia 9 listopada 2000 r. o utworzeniu Polskiej Agencji Rozwoju Przedsiębiorczości. § 5 punkt 1 c, d, e Rozporządzenia Ministra Gospodarki i Pracy z dnia 17 sierpnia 2004 w sprawie udzielania przez Polską Agencję Rozwoju Przedsiębiorczości pomocy finansowej niezwiązanej z programami operacyjnymi |
Annual expenditure planned or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount | EUR 0,43 million |
Loans guaranteed | |
Individual aid | Overall aid amount | |
Loans guaranteed | |
Maximum aid intensity | In conformity with Articles 4(2)-(6) and 5 of the Regulation | Yes |
Date of implementation | 28.8.2004 — the day on which the Decree entered into force |
Duration of scheme or individual aid award | Until 30.11.2005 |
Objective of aid | Aid to SMEs | Yes | |
Economic sectors concerned | All sectors eligible for aid to SMEs | Yes |
Name and address of the granting authority | Name: Polish Agency for Enterprise Development |
Address: Ul. Pańska 81/83 PL-00-834 Warsaw |
Large individual aid grants | In conformity with Article 6 of the Regulation | Yes | |
Aid No | XS 150/04 |
Member State | Czech Republic |
Region | Czech Republic |
Title of aid scheme or name of company receiving individual aid | Aid to boost the competitiveness of SMEs |
Legal basis | Zákon č. 47/2002 Sb., o podpoře malého a středního podnikání |
Annual expenditure planned or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount | EUR 1,6m |
Loans guaranteed | |
Individual aid | Overall aid amount | EUR 0,03m This is the amount calculated on the basis of the estimated number of beneficiaries |
Loans guaranteed | |
Maximum aid intensity | In conformity with Articles 4(2)-(6) and 5 of the Regulation | Yes | |
Date of implementation | 1.1.2005 |
Duration of scheme or individual aid award | Until 31.12.2006 |
Objective of aid | Aid to SMEs | Yes | |
Economic sectors concerned | All sectors eligible for aid to SMEs | Yes |
Name and address of the granting authority | Name: Ministry of Industry and Trade |
Address: Na Františku 32 CZ-110 15 Praha 1 |
Large individual aid grants | In conformity with Article 6 of the Regulation | Yes | |
Aid No | XS 151/04 |
Member State | Czech Republic |
Region | Czech Republic |
Title of aid scheme or name of company receiving individual aid | Aid for design consultancy services |
Legal basis | Zákon č. 47/2002 Sb., o podpoře malého a středního podnikání |
Annual expenditure planned or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount | EUR 0,32m |
Loans guaranteed | |
Individual aid | Overall aid amount | EUR 0,005m This is the amount calculated on the basis of the estimated number of beneficiaries |
Loans guaranteed | |
Maximum aid intensity | In conformity with Articles 4(2)-(6) and 5 of the Regulation | Yes | |
Date of implementation | 1.1.2005 |
Duration of scheme or individual aid award | Until 31.12.2006 |
Objective of aid | Aid to SMEs | Yes | |
Economic sectors concerned | All sectors eligible for aid to SMEs | Yes |
Name and address of the granting authority | Name: Ministry of Industry and Trade |
Address: Na Františku 32 CZ-110 15 Prague 1 |
Large individual aid grants | In conformity with Article 6 of the Regulation | Yes | |
Aid No | XS 152/04 |
Member State | Czech Republic |
Region | Czech Republic |
Title of aid scheme or name of company receiving individual aid | Aid for training and consultancy services |
Legal basis | Zákon č. 47/2002 Sb., o podpoře malého a středního podnikání |
Annual expenditure planned or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount | EUR 1m |
Loans guaranteed | |
Individual aid | Overall aid amount | EUR 0,002m This is the amount calculated on the basis of the estimated number of beneficiaries |
Loans guaranteed | |
Maximum aid intensity | In conformity with Articles 4(2)-(6) and 5 of the Regulation | Yes | |
Date of implementation | 1.1.2005 |
Duration of scheme or individual aid award | Until 31.12.2006 |
Objective of aid | Aid to SMEs | Yes | |
Economic sectors concerned | All sectors eligible for aid to SMEs | Yes |
Name and address of the granting authority | Name: Ministry of Industry and Trade |
Address: Na Františku 32 CZ-110 15 Prague 1 |
Large individual aid grants | In conformity with Article 6 of the Regulation | Yes | |
Aid No | XS 153/04 |
Member State | Czech Republic |
Region | Czech Republic |
Title of aid scheme or name of company receiving individual aid | Subsidised loan |
Legal basis | Zákon č. 47/2002 Sb., o podpoře malého a středního podnikání |
Annual expenditure planned or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount | EUR 7,09m |
Loans guaranteed | |
Individual aid | Overall aid amount | EUR 0,81m This is the amount calculated on the basis of the estimated number of beneficiaries |
Loans guaranteed | |
Maximum aid intensity | In conformity with Articles 4(2)-(6) and 5 of the Regulation | Yes | |
Date of implementation | 1.1.2005 |
Duration of scheme or individual aid award | Until 31.12.2006 |
Objective of aid | Aid to SMEs | Yes | |
Economic sectors concerned | All sectors eligible for aid to SMEs | Yes |
Name and address of the granting authority | Name: Ministry for Industry and Trade |
Address: Na Františku 32 CZ-110 15 Prague 1 |
Large individual aid grants | In conformity with Article 6 of the Regulation | Yes | |
Aid No | XS 155/04 |
Member State | Czech Republic |
Region | Czech Republic |
Title of aid scheme or name of company receiving individual aid | Guarantees for bank loans, capital contribution and design of a public bidding procedure |
Legal basis | Zákon č. 47/2002 Sb., o podpoře malého a středního podnikání |
Annual expenditure planned or overall amount of individual aid granted to the company | Aid scheme Guarantees | Annual overall amount | EUR 24,3m |
Loans guaranteed (volume) | EUR 92,3m |
Individual aid — cost of subsidised guarantee | Overall aid amount | EUR 0,04m This is the amount calculated on the basis of the estimated number of beneficiaries |
Loans guaranteed | EUR 0,14m |
Maximum aid intensity | In conformity with Articles 4(2)-(6) and 5 of the Regulation | Yes | |
Date of implementation | 1.1.2005 |
Duration of scheme or individual aid award | Until 31.12.2006 |
Objective of aid | Aid to SMEs | Yes | |
Economic sectors concerned | All sectors eligible for aid to SMEs | Yes |
Name and address of the granting authority | Name: Ministry for Industry and Trade |
Address: Na Františku 32, CZ-110 15 Prague 1 |
Large individual aid grants | In conformity with Article 6 of the Regulation | Yes | |
--------------------------------------------------
