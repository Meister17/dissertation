Recommendation of the European Central Bank
of 1 February 2006
to the Council of the European Union on the external auditors of the Oesterreichische Nationalbank
(ECB/2006/1)
(2006/C 34/08)
THE GOVERNING COUNCIL OF THE EUROPEAN CENTRAL BANK,
Having regard to the Statute of the European System of Central Banks and of the European Central Bank, and in particular Article 27.1 thereof,
Whereas:
(1) The accounts of the European Central Bank (ECB) and of the national central banks of the Eurosystem are audited by independent external auditors recommended by the Governing Council of the ECB and approved by the Council of the European Union.
(2) Pursuant to Article 37(1) of the Federal Act on the Oesterreichische Nationalbank, the General Meeting of the Oesterreichische Nationalbank (OeNB) shall elect two auditors and two alternate auditors each year. The alternate auditors will be mandated only in the event that the auditors are not able to perform the audit.
(3) The mandate of the current external auditors of the OeNB cannot be renewed for a further term of office after the audit for the financial year 2005. It is therefore necessary to appoint external auditors from the financial year 2006.
(4) The OeNB has selected KPMG Alpen-Treuhand GmbH, TPA Horwath Wirtschaftsprüfung GmbH, Moore Stephens Austria Wirtschaftsprüfungsgesellschaft mbH and BDO Auxilia Treuhand GmbH as its new external auditors in accordance with Community as well as Austrian public procurement legislation, and the ECB considers that they fulfil the necessary requirements for appointment.
(5) The mandate of the external auditors may be renewed on a yearly basis, not exceeding a total term of five years,
HAS ADOPTED THIS RECOMMENDATION:
1. It is recommended that KPMG Alpen-Treuhand GmbH and TPA Horwath Wirtschaftsprüfung GmbH should be appointed jointly as the external auditors of the OeNB for the financial year 2006.
2. It is recommended that Moore Stephens Austria Wirtschaftsprüfungsgesellschaft mbH and BDO Auxilia Treuhand GmbH should be appointed jointly as the alternate auditors of the OeNB for the financial year 2006.
3. This mandate may be renewed on a yearly basis, not exceeding a total term of five years, ending with the financial year 2010 at the latest.
Done at Frankfurt am Main, 1 February 2006.
The President of the ECB
Jean-Claude Trichet
--------------------------------------------------
