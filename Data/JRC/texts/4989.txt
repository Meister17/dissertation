Commission Regulation (EC) No 736/2005
of 13 May 2005
supplementing the Annex to Regulation (EC) No 2400/96 as regards the entry of a name in the "Register of protected designations of origin and protected geographical indications" (Miel d’Alsace) — (PGI)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2081/92 of 14 July 1992 on the protection of geographical indications and designations of origin for agricultural products and foodstuffs [1], and in particular Article 7(5)(b), Article 6(3) and the first indent of Article 6(4) thereof,
Whereas:
(1) In accordance with Article 6(2) of Regulation (EC) No 2081/92, France’s application to register "Miel d’Alsace" was published in the Official Journal of the European Union [2].
(2) Germany opposed the registration under Article 7 of Regulation (EEC) No 2081/92. This opposition arose from a failure to comply with the conditions laid down in Article 2 of Regulation (EEC) No 2081/92. In Germany’s view, some details of the specification relating to proof of origin in particular were inadequate as regards the definition of a geographical indication.
(3) By letter of 6 February 2003 the Commission asked the Member States concerned to seek agreement among themselves in accordance with their internal procedures.
(4) As no agreement was reached between France and Germany within three months, the Commission must adopt a decision in accordance with the procedure provided for in Article 15 of Regulation (EEC) No 2081/92.
(5) As a result of the contacts between France and Germany on Miel d’Alsace, clarifications were made to the product specification in question, particularly as regards the honey’s chemical features and proof of origin. Point 4 of the specification summary has been amended as a result.
(6) In light of the above, the name should thus be entered in the "Register of protected designations of origin and protected geographical indications".
(7) The measures provided for in this Regulation are in accordance with the opinion of the Regulatory Committee on the Protection of Geographical Indications and Designations of Origin for Agricultural Products and Foodstuffs,
HAS ADOPTED THIS REGULATION:
Article 1
The name in Annex I to this Regulation is hereby added to the Annex to Commission Regulation (EC) No 2400/96 [3].
Article 2
A summary of the main points of the specification is given in Annex II to this Regulation.
Article 3
This Regulation shall enter into force on the 20th day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 13 May 2005.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 208, 24.7.1992, p. 1. Regulation as last amended by Regulation (EC) No 806/2003 (OJ L 122, 16.5.2003, p. 1).
[2] OJ C 71, 20.3.2002, p. 11.
[3] OJ L 327, 18.12.1996, p. 11. Regulation as last amended by Regulation (EC) No 205/2005 (OJ L 33, 5.2.2005, p. 6).
--------------------------------------------------
ANNEX I
PRODUCTS LISTED IN ANNEX I TO THE EC TREATY, INTENDED FOR HUMAN CONSUMPTION
Other animal products (eggs, honey, various milk products except butter, etc.)
FRANCE
Miel d’Alsace (PGI)
--------------------------------------------------
ANNEX II
COUNCIL REGULATION (EEC) No 2081/92
"MIEL D’ALSACE"
(EC No: FR/00150)
PDO ( ) PGI (X)
This summary has been drawn up for information purposes only. For full details, in particular the producers of the products covered by the PDO or PGI concerned, please consult the complete version of the product specification obtainable at national level or from the European Commission.
Name: Ministère de l’agriculture et de la pêche — direction des politiques économique et internationale — bureau des signes de qualité et de l'agriculture biologique
Address 3, rue Barbet de Jouy
F-75349 07 SP
Telephone: (33) 149 55 81 01
Fax: (33) 149 55 57 85
Name: Confédération régionale des apiculteurs d’Alsace
Address Espace européen de l’entreprise
2, rue de Rome
F-67300 Schiltigheim
Telephone: (33) 388 19 16 78
Fax: (33) 388 18 90 42
E-mail: alsace-qualite@alsace-qualite.com
Composition: producer/processor (X) other ( )
Type of product: Class 1.4 Honey
Name "Miel d’Alsace"
Description
A product made by bees from sugars produced by plants in the form either of exudates of flowers (nectar) or of sap gathered by aphids (honeydew). Each product is characterised by its maximum water content, conductivity, acidity, HMF, colour and distinctive taste.
Honey made from silver fir is brown in colour with green lights, smell is mildly resinous and aroma balsamic.
Honey made from chestnut is light to dark brown in colour, smell is reminiscent of overripe apples and taste is tannic and slightly stringent.
Honey made from acacia is light-coloured, smell is fragrant of acacia flowers, aroma is reminiscent of false acacia and taste reminiscent of the hive.
Honey made from lime is light to dark yellow in colour, smell is mentholated and taste slightly bitter.
Forest honey has a distinctive intense colour, subtle aromas due to the mixture of honeydew and nectars, a flavour that is intense and a taste that is slightly stringent.
Multi-flower honey is light to dark coloured, aromas are complex owing to the mixture of nectars, taste is very sweet.
Geographical area
The production hives must be installed in Alsace. In the case of honey from silver fir, the perimeter is limited to the Alsatian slopes of the Vosges and Jura mountain ranges. Chestnut honey is gathered in the hills south of the Vosges (Lower Rhine and Upper Rhine in the forests of Brumath and Haguenau). Lime honey comes from the forests of Hardt (Upper Rhine).
The honey collection areas, a determining factor for quality and specificity, are defined, but extraction can take place outside the area provided that traceability is ensured.
Proof of origin
Bee-keepers and production sites are identified, registered and checked. Production sites must be located in Alsace. Traceability of the honey is ensured through the keeping of records of labels which are checked against harvest declarations and stocks.
Proof of origin is also checked through microscopic analysis of 30 % of all Miel d’Alsace production, by designation.
Method of production Hives are placed in an area in which the forest or floral species are found that correspond to the type of honey sought: acacia, lime, pine, chestnut, forest or multi-flower honey. The honey is extracted when ripe without heating, is decanted, stored and packaged. Each batch is subjected to a physico-chemical and sensory analysis. The honey must also comply with the physico-chemical and organoleptic criteria set out in the specification.
Link Distinctive features linked to the product:
Each type of honey develops its own physico-chemical and organoleptic characteristics, which are defined in the specification and correspond to the floral diversity of the region. The diversity of Alsatian honeys stems directly from the diversity of the prevailing ecosystems. Alsace comprises several areas: an area of mountains covered with softwoods, an area made up of hills and plateaux with vines, meadows and beech and chestnut forests, and a plain consisting of cropped land and meadows. The resulting diversity of ecosystems hence allows harvesting to take place from early spring to early autumn, providing a wide variety of products.
Past and present reputation:
Records of honey production in Alsace go back as far as the seventh century.
- in the 16th century, among the various works published was a treatise on apiculture (1580) which remained a reference for almost two centuries,
- the second half of the 19th century saw a new phase in the development of Alsatian apiculture; by the beginning of the 20th century there were more than 50000 hives in Alsace,
- Alsatian apiculture today is made up of a very large number of bee-keepers (currently close to 4000) who are members of pyramid-shaped and decentralised unions. The regional show held at Colmar for the honeys from Alsace attests to the vitality of Alsatian honey production.
Name: Certiqual (Association certifying Alsace quality products)
Address Espace européen de l’entreprise
2, rue de Rome
F-67300 Schiltigheim
Telephone: (33) 388 19 16 79
Fax: (33) 388 19 55 29
E-mail: certiqual2@wanadoo.fr
Labelling:
The words Miel d’Alsace must figure on the label together with a reference to the type of honey (acacia, lime, silver fir, chestnut, forest, multi-flower).
Freshness, quality and taste guaranteed, compliance of characteristics certified by Certiqual F-67309 Schiltigheim.
National requirements: —
--------------------------------------------------
