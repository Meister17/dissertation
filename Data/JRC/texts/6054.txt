Commission Regulation (EC) No 2185/2005
of 27 December 2005
opening Community tariff quotas for 2006 for sheep, goats, sheepmeat and goatmeat
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 2529/2001 of 19 December 2001 on the common organisation of the market in sheepmeat and goatmeat [1], and in particular Article 16(1) thereof,
Whereas:
(1) Community tariff quotas for sheepmeat and goatmeat should be opened for 2006. The duties and quantities referred to in Regulation (EC) No 2529/2001 should be fixed in accordance with the respective international agreements in force during the year 2006.
(2) Council Regulation (EC) No 312/2003 of 18 February 2003 implementing for the Community the tariff provisions laid down in the Agreement establishing an association between the European Community and its Member States, of the one part, and the Republic of Chile, of the other part [2] has provided for an additional bilateral tariff quota of 2000 tonnes with a 10 % annual increase of the original quantity to be opened for product code 0204 from 1 February 2003. That quota was added to the GATT/WTO quota for Chile and both quotas should continue to be managed in the same way during 2006. Moreover, a quota calculation error occurred when attributing the quota for the year 2005 under Commission Regulation (EC) No 2202/2004 of 21 December 2004 opening Community tariff quotas for 2005 for sheep, goats, sheepmeat and goatmeat [3] and a quantity of 5417 tonnes instead of 5400 tonnes was attributed. A quantity of 17 tonnes should therefore be deducted from the quantity available for 2006.
(3) Council Regulation (EC) No 992/95 of 10 April 1995 opening and providing for the administration of Community tariff quotas for certain agricultural and fishery products originating in Norway [4], as amended by Regulation (EC) No 1329/2003 [5], grants additional bilateral trade concessions concerning agricultural products.
(4) Council Regulation (EC) No 2175/2005 of 21 December 2005 concerning the implementation of the Agreements concluded by the EC following negotiations in the framework of Article XXIV:6 of GATT 1994, amending Annex I to Regulation (EEC) No 2658/87 on the tariff and statistical nomenclature and on the Common Customs Tariff [6] grants an additional tariff quota quantity of 1154 tonnes for New Zealand as from 1 January 2006, which should be added to the quantity available for 2006.
(5) Certain tariff quotas for sheep and goat meat products have been granted to the ACP States under the Cotonou Agreement [7].
(6) Certain quotas are defined for a period running from 1 July of a given year to 30 June of the following year. Since imports under this present Regulation should be managed on a calendar-year basis, the corresponding quantities to be fixed for the calendar year 2006 with regard to the quotas concerned are the sum of half of the quantity for the period from 1 July 2005 to 30 June 2006 and half of the quantity for the period from 1 July 2006 to 30 June 2007.
(7) A carcase-weight equivalent needs to be fixed in order to ensure a proper functioning of the Community tariff quotas. Furthermore, since certain tariff quotas provide for the option of importing either the live animals or their meat, a conversion factor is required.
(8) Experience with the management of the Community tariff quotas under the first-come, first-served system in sheepmeat and goatmeat products has been positive during the year 2005. Therefore, quotas concerning those products should, by way of derogation from Commission Regulation (EC) No 1439/95 of 26 June 1995 laying down detailed rules for the application of Council Regulation (EEC) No 3013/89 as regards the import and export of products in the sheepmeat and goatmeat sector [8], be managed in conformity with Article 16(2)(a) of Regulation (EC) No 2529/2001. This should be done in accordance with Articles 308a, 308b and 308c(1) of Commission Regulation (EEC) No 2454/93 of 2 July 1993 laying down provisions for the implementation of Council Regulation (EEC) No 2913/92 establishing the Community Customs Code [9].
(9) In order to avoid any discrimination among exporting countries, and given that equivalent tariff quotas have not been quickly exhausted in the last two years, tariff quotas under this Regulation should be regarded initially as non-critical within the meaning of Article 308c of Regulation (EEC) No 2454/93 when managed under the first-come, first-served system. Therefore, customs authorities should be authorised to waive the requirement for security in respect of goods initially imported under those quotas in accordance with Articles 308c(1) and 248(4) of Regulation (EEC) No 2454/93. Due to the particularities of the transfer from one management system to the other, Article 308c(2) and (3) of that Regulation should not apply.
(10) It should be clarified which kind of proof certifying the origin of products has to be provided by operators in order to benefit from the tariff quotas under the first-come, first served system.
(11) When sheepmeat products are presented by operators to the customs authorities for import, it is difficult for those authorities to establish whether they originate from domestic sheep or other sheep, which determines the application of different duty rates. It is therefore appropriate to provide that the proof of origin contains a clarification to that end.
(12) In accordance with Chapter II of Council Directive 2002/99/EC of 16 December 2002 laying down the animal health rules governing the production, processing, distribution and introduction of products of animal origin for human consumption [10] and with Council Directive 97/78/EC of 18 December 1997 laying down the principles governing the organisation of veterinary checks on products entering the Community from third countries [11], imports may be authorised only for products meeting the requirements of the food chain procedures, rules and checks in force in the Community.
(13) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Sheep and Goats,
HAS ADOPTED THIS REGULATION:
Article 1
This Regulation opens Community tariff quotas for sheep, goats, sheepmeat and goatmeat for the period from 1 January to 31 December 2006.
Article 2
The customs duties applicable to imports into the Community of sheep, goats, sheepmeat and goatmeat falling within CN codes 01041030, 01041080, 01042090, 02109921, 02109929 and 0204 originating in the countries indicated in the Annex shall be suspended or reduced in accordance with this Regulation.
Article 3
1. The quantities, expressed in carcase-weight equivalent, for the import of meat falling within CN code 0204 and of live animals falling within CN codes 01041030, 01041080 and 01042090, as well as the customs duty applicable shall be those as laid down in the Annex.
2. For the purpose of calculating the quantities of "carcase-weight equivalent" referred to in paragraph 1 the net weight of sheep and goat products shall be multiplied by the following coefficients:
(a) for live animals: 0,47;
(b) for boneless lamb and boneless goatmeat of kid: 1,67;
(c) for boneless mutton, boneless sheep and boneless goatmeat other than of kid and mixtures of any of these: 1,81;
(d) for bone-in products: 1,00.
"Kid" shall mean goats of up to one year old.
Article 4
By way of derogation from Title II(A) and (B) of Regulation (EC) No 1439/95, the tariff quotas set out in the Annex to this Regulation shall be managed on a first-come, first-served basis in accordance with Articles 308a, 308b and 308c(1) of Regulation (EEC) No 2454/93 from 1 January to 31 December 2006. Article 308c(2) and (3) of that Regulation shall not apply. No import licences shall be required.
Article 5
1. In order to benefit from the tariff quotas set out in the Annex, a valid proof of origin issued by the competent authorities of the third country concerned together with a customs declaration for release for free circulation for the goods concerned shall be presented to the Community customs authorities. The origin of products subject to tariff quotas other than those resulting from preferential tariff agreements shall be determined in accordance with the provisions in force in the Community.
2. The proof of origin referred to in paragraph 1 shall be as follows:
(a) in the case of a tariff quota which is part of a preferential tariff agreement, it shall be the proof of origin laid down in that agreement;
(b) in the case of other tariff quotas, it shall be a proof established in accordance with Article 47 of Regulation (EEC) No 2454/93 and, in addition to the elements provided for in that Article, the following data:
- the CN code (at least the first four digits),
- the order number or order numbers of the tariff quota concerned,
- the total net weight per coefficient category as provided for in Article 3(2) of this Regulation;
(c) in the case of a country whose quota falls under points (a) and (b) and are merged, it shall be the proof referred to in point (a).
Where the proof of origin referred to in point (b) is presented as supporting document for only one declaration for release for free circulation, it may contain several order numbers. In all other cases, it shall only contain one order number.
3. In order to benefit from the tariff quota set out in the Annex for Country Group 4 in respect of products falling under CN codes ex0204, ex02109921 and ex02109929 the proof of origin shall contain in the box concerning the description of the products one of the following:
(a) "sheep product/s from the species domestic sheep";
(b) "product/s from the species other than domestic sheep".
Those indications shall correspond to the indications in the veterinary certificate accompanying those products.
Article 6
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Union.
It shall apply from 1 January 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 27 December 2005.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 341, 22.12.2001, p. 3. Regulation as last amended by Regulation (EC) No 1913/2005 (OJ L 307, 25.11.2005, p. 2).
[2] OJ L 46, 20.2.2003, p. 1. Regulation as amended by Commission Regulation (EC) No 305/2005 (OJ L 52, 25.2.2005, p. 6).
[3] OJ L 374, 22.12.2004, p. 31.
[4] OJ L 101, 4.5.1995, p. 1. Regulation as last amended by Regulation (EC) No 1920/2004 (OJ L 331, 5.11.2004, p. 1).
[5] OJ L 187, 26.7.2003, p. 1.
[6] See page 9 of this Official Journal.
[7] OJ L 317, 15.12.2000, p. 3.
[8] OJ L 143, 27.6.1995, p. 7. Regulation as last amended by Regulation (EC) No 272/2001 (OJ L 41, 10.2.2001, p. 3).
[9] OJ L 253, 11.10.1993, p. 1. Regulation as last amended by Regulation (EC) No 883/2005 (OJ L 148, 11.6.2005, p. 5).
[10] OJ L 18, 23.1.2003, p. 11.
[11] OJ L 24, 30.1.1998, p. 9. Directive as last amended by Regulation (EC) No 882/2004 of the European Parliament and of the Council (OJ L 165, 30.4.2004, p. 1. Corrected by OJ L 191, 28.5.2004, p. 1).
--------------------------------------------------
ANNEX
SHEEPMEAT AND GOATMEAT (in tonnes (t) of carcase weight equivalent) COMMUNITY TARIFF QUOTAS FOR 2006
Country group No | CN codes | Ad valorem duty % | Specific duty EUR/100 Kg | Order number under "first-come, first-served" | Origin | Annual volume in tonnes of carcase weight equivalent |
Live animals (Coefficient = 0,47) | Boneless lamb (Coefficient = 1,67) | Boneless mutton/sheep (Coefficient = 1,81) | Bone-in and carcases (Coefficient = 1,00) |
1 | 0204 | Zero | Zero | — | 09.2101 | 09.2102 | 09.2011 | Argentina | 23000 |
— | 09.2105 | 09.2106 | 09.2012 | Australia | 18650 |
— | 09.2109 | 09.2110 | 09.2013 | New Zealand | 227854 |
— | 09.2111 | 09.2112 | 09.2014 | Uruguay | 5800 |
— | 09.2115 | 09.2116 | 09.1922 | Chile | 5600 |
— | 09.2119 | 09.2120 | 09.0790 | Iceland | 1350 |
2 | 0204 | Zero | Zero | — | 09.2121 | 09.2122 | 09.0781 | Norway | 300 |
3 | 0204 | Zero | Zero | — | 09.2125 | 09.2126 | 09.0693 | Greenland | 100 |
— | 09.2129 | 09.2130 | 09.0690 | Faeroes | 20 |
— | 09.2131 | 09.2132 | 09.0227 | Turkey | 200 |
4 | 01041030, 01041080 and 01042090. For the species "other than domestic sheep" only: ex0204, ex02109921 and ex02109929 | Zero | Zero | 09.2141 | 09.2145 | 09.2149 | 09.1622 | ACP States | 100 |
For the species "domestic sheep" only: ex0204, ex02109921 and ex02109929 | Zero | 65 % reduction of specific duties | — | 09.2161 | 09.2165 | 09.1626 | ACP States | 500 |
5 | 0204 | Zero | Zero | — | 09.2171 | 09.2175 | 09.2015 | Others | 200 |
01041030 01041080 01042090 | 10 % | Zero | 09.2181 | — | — | 09.2019 | Others | 49 |
--------------------------------------------------
