COMMISSION REGULATION (EC) No 1861/95 of 27 July 1995 amending Regulation (EC) No 1162/95 laying down special detailed rules for the application of the system of import and export licences for cereals and rice
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 1766/92 of 30 June 1992 on the common organization of the market in cereals (1), as last amended by Regulation (EC) No 1664/95 (2), and in particular Articles 9 (2), 12 (4) and 13 (11) thereof,
Whereas Commission Regulation (EC) No 1162/95 (3), as last amended by Regulation (EC) No 1617/95 (4), lays down special detailed rules for the application of the system of import and export licences for cereals and rice;
Whereas where no refund has been fixed the period of validity of licences for all the products referred to in Article 1 of Council Regulation (EEC) No 1766/92 and (EEC) No 1418/76 (5), as last amended by Regulation (EC) No 1530/95 (6), should be reduced to avoid jeopardizing the sound management of the market at a time when it is particularly sensitive; whereas a precise period of validity should therefore be laid down;
Whereas it is essential for all applications for licences, with or without refund, to be notified to the Commission so that it can draw up the statistical tables which are essential for market management and the monitoring of export commitments;
Whereas Regulation (EC) No 1162/95 should be amended;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Cereals,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 1162/95 is hereby amended as follows:
1. The following paragraph 2a is added to Article 7:
'2a. In cases where no refund has been fixed, export licences for the products referred to in Article 1 of Regulation (EEC) No 1766/92 and Article 1 of Regulation (EEC) No 1418/76 shall be valid 30 days from the day of issue within the meaning of Article 21 (1) of Regulation (EEC) No 3719/88.` 2. The first indent of point 1 (a) (i) of Article 13 is replaced by the following:
'- of all applications for licences, or the absence of applications for licences.`
Article 2
The Member States shall notify the Commission as soon as possible of all licences issued between 1 July 1995 and the entry into force of this Regulation.
Article 3
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Communities.
However, Article 2 shall apply from 1 July 1995.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 27 July 1995.
For the Commission Franz FISCHLER Member of the Commission
