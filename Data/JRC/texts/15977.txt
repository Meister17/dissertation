Judgment of the Court of First Instance of 26 January 2006 — Medici Grimm v Council
(Case T-364/03) [1]
Parties
Applicant: Medici Grimm KG (Rodgau Hainhausen, Germany) (represented by: R. MacLean, Solicitor, and E. Gybels, lawyer)
Defendant: Council of the European Union (represented by: M. Bishop, Agent, assisted by G. Berrisch, lawyer)
Intervener in support of the defendant: Commission of the European Communities (represented by: N. Khan and T. Scharf, Agents)
Application for
compensation under Article 235 EC and the second paragraph of Article 288 EC for damage allegedly suffered by the applicant as a result of the absence of retroactive effect of Council Regulation (EC) No 2380/98 of 3 November 1998, amending Regulation (EC) No 1567/97 imposing a definitive anti-dumping duty on imports of leather handbags originating in the People's Republic of China (OJ 1997 L 296, p. 1), partially annulled by the judgment in Case T-7/99 Medici Grimm
v Council [2000] ECR II-2671
Operative part of the judgment
1. The action is dismissed;
2. The applicant shall bear, in addition to its own costs, the costs incurred by the Council;
3. The Commission shall bear its own costs.
[1] OJ C 21, 24.1.2004.
--------------------------------------------------
