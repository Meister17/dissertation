Council Decision
of 14 February 2006
on the signature, on behalf of the European Community, and the provisional application of the Agreement in the form of an Exchange of Letters concerning the provisional application of the Protocol amending the Protocol setting out the fishing opportunities and the financial contribution provided for in the Agreement on cooperation in the sea fisheries sector between the European Community and the Islamic Republic of Mauritania for the period 1 August 2001 to 31 July 2006
(2006/113/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 37 in conjunction with Article 300(2) thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) The Protocol setting out the fishing opportunities and the financial contribution provided for in the Agreement on cooperation in the sea fisheries sector between the European Community and the Islamic Republic of Mauritania is applicable for the period 1 August 2001 to 31 July 2006 [1].
(2) In view of the scientific opinions on the state of resources in the Mauritanian EEZ and, in particular, the results of the fourth and fifth working groups of the Mauritanian Institute of Oceanographic Research and Fisheries (IMROP) and of the joint scientific working group, and in the light of the conclusions drawn from those results at the meeting of the Joint Committee held on 10 September and 15 and 16 December 2004, the two parties have decided to amend the existing fishing opportunities.
(3) The outcome of those amendments has been the subject of an Exchange of Letters and involves a temporary reduction in the fishing effort for cephalopods (category 5), the fixing of a second closed period of one month for demersal fishing, and an increase in the number of pole-and-line tuna vessels and surface longliners (category 8) and pelagic freezer trawlers (category 9).
(4) So that those amendments to fishing opportunities can apply as soon as possible, the Agreement should, therefore, be signed in the form of an Exchange of Letters, subject to its definitive conclusion by the Council.
(5) The allocation of the new fishing opportunities among the Member States, as thus amended, should be confirmed,
HAS DECIDED AS FOLLOWS:
Article 1
The signature of the Agreement in the form of an Exchange of Letters concerning the provisional application of the Protocol amending the Protocol setting out the fishing opportunities and the financial contribution provided for in the Agreement on cooperation in the sea fisheries sector between the European Community and the Islamic Republic of Mauritania for the period 1 August 2001 to 31 July 2006 is hereby approved on behalf of the Community, subject to the conclusion of the said Agreement.
The text of the Agreement is attached to this Decision.
Article 2
The President of the Council is hereby authorised to designate the persons empowered to sign the Agreement in the form of an Exchange of Letters on behalf of the Community subject to its conclusion.
Article 3
The Agreement in the form of an Exchange of Letters shall be applied on a provisional basis by the Community as from 1 January 2005.
Article 4
Following the amendments set out in the Exchange of Letters, the new fishing opportunities for the "pole-and-line tuna vessel and surface longliner" category (Datasheet No 8 of the Protocol) and for the "pelagic freezer trawler" category (Datasheet No 9) shall be allocated among the Member States as follows:
Fishing category | Member State | Tonnage/Number of vessels which may be used |
Pole-and-line tuna vessels Surface longliners (vessels) | Spain | 20 + 3 = 23 |
Portugal | 3 + 0 = 3 |
France | 8 + 1 = 9 |
Pelagic species (vessels) | | 15 + 10 = 25 |
The temporary suspension of five fishing licences for the cephalopod fishing category shall take effect as from 1 January 2005. The future reactivation of those five licences shall be decided by common accord in a Joint Committee meeting held between the Commission and the Mauritanian authorities on the basis of the state of resources.
If licence applications from Member States do not cover all the fishing opportunities laid down by the Protocol, the Commission may take into consideration licence applications from any other Member State.
Done at Brussels, 14 February 2006.
For the Council
The President
K.-H. Grasser
[1] OJ L 341, 22.12.2001, p. 128.
--------------------------------------------------
