Commission Decision
of 18 August 2006
amending Decision 2005/734/EC as regards certain additional risk mitigating measures against the spread of avian influenza
(notified under document number C(2006) 3702)
(Text with EEA relevance)
(2006/574/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 90/425/EEC of 26 June 1990 concerning veterinary and zootechnical checks applicable in intra-Community trade in certain live animals and products with a view to the completion of the internal market [1], and in particular Article 10(4) thereof,
Whereas:
(1) In order to reduce the risk of highly pathogenic avian influenza caused by influenza A virus of subtype H5N1 being introduced into poultry farms and other premises where birds are kept in captivity via wild birds, Commission Decision 2005/734/EC of 19 October 2005 laying down biosecurity measures to reduce the risk of transmission of highly pathogenic avian influenza caused by influenza virus A subtype H5N1 from birds living in the wild to poultry and other captive birds and providing for an early detection system in areas at particular risk [2] was adopted.
(2) Pursuant to that Decision, Member States are to identify individual holdings keeping poultry or other captive birds which, according to epidemiological and ornithological data, should be considered particularly at risk from the spread of the avian influenza A virus of subtype H5N1 via wild birds.
(3) In the light of current epidemiological and ornithological developments with regard to that disease, provision should be made to review such risks on a regular and ongoing basis with a view to adjusting the areas identified as particularly at risk for the spread of the disease and the measures taken in those areas.
(4) In such areas the use of decoy birds has been prohibited, except for their use in the Member States’ programmes for avian influenza surveys in poultry and wild birds, as provided for in Commission Decision 2005/732/EC of 17 October 2005 approving the programmes for the implementation of Member States’ surveys for avian influenza in poultry and wild birds during 2005 and laying down reporting and eligibility rules for the Community financial contribution to the implementation costs of those programmes [3].
(5) Taking into account recent experiences and based on a favourable outcome of a case-by-case risk assessment, the competent authority should be given the possibility to grant further derogations from the prohibition of the use of decoy birds provided that appropriate biosecurity measures are taken.
(6) Decision 2005/734/EC should therefore be amended accordingly.
(7) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
Decision 2005/734/EC is amended as follows:
1. Article 2a is replaced by the following:
"Article 2a
Additional risk mitigating measures
1. Member States shall ensure that the following activities are prohibited in the areas of their territory that they have identified as being particularly at risk for the introduction of avian influenza, in accordance with Article 1(1):
(a) the keeping of poultry in the open air, without undue delay;
(b) the use of outdoor water reservoirs for poultry;
(c) the provision of water to poultry from surface water reservoirs accessed by wild birds;
(d) the use of birds of the orders Anseriformes and Charadriiformes as decoy birds (decoy birds) during bird-hunting.
2. Member States shall ensure that the bringingtogether of poultry and other birds at markets, shows, exhibitions and cultural events, including point-to-point races of birds, is prohibited."
2. The following Articles 2b and 2c are inserted:
"Article 2b
Derogations
1. By way of derogation from Article 2a(1), the competent authority may authorise the following activities:
(a) the keeping of poultry in the open air, provided the poultry are supplied with feed and water indoors or under a shelter which sufficiently discourages the landing of wild birds and thereby prevents contact by wild birds with the feed or water intended for the poultry;
(b) the use of outdoor water reservoirs if they are required for animal welfare reasons for certain poultry and are sufficiently screened against wild waterfowl;
(c) the provision of water from surface water accessed by wild waterfowl after treatment that would ensure inactivation of the possible avian influenza virus;
(d) the use of decoy birds during bird-hunting:
(i) by decoy bird holders registered with the competent authority, under the strict supervision of the competent authority for the attraction of wild birds intended for sampling pursuant to the Member States’ programmes for avian influenza surveys in poultry and wild birds as provided for in Decision 2005/732/EC; or
(ii) in accordance with appropriate biosecurity measures, which include:
- identification of the individual decoy birds by a ringing system,
- implementation of a specific surveillance system for decoy birds,
- the recording and reporting of the health status of decoy birds and laboratory testing for avian influenza in the case of deaths of such birds and at the end of the bird-hunting season,
- strict separation between decoy birds and domestic poultry and other captive birds circuit,
- cleansing and disinfection of the means of transport and equipment used for the transport of decoy birds and for trips in the areas where decoy birds are placed,
- restrictions and control of movements of decoy birds, in particular to prevent contacts with different open waters,
- the development and implementation of "guidelines of good biosecurity practises" detailing the measures provided for in the first to sixth indents,
- implementation of a reporting system on the data obtained from the measures referred to in the first, second, and third indents.
2. By way of derogation from Article 2a(2), the competent authority may authorise the gatherings of poultry and other captive birds.
Article 2c
Conditions for and follow-up to authorisation
1. Member States shall ensure that authorisations in accordance with Article 2b are only granted following the favourable outcome of a risk assessment and provided that biosecurity measures are in place to avoid possible spread of avian influenza.
2. Before authorising the use of decoy birds in accordance with Article 2b(1)(d)(ii), the Member State concerned shall submit a risk assessment to the Commission accompanied by information on the biosecurity measures that are to be put in place to ensure proper implementation of that Article.
3. Member States that grant derogations in accordance with Article 2(b)(1)(d)(ii) shall submit a monthly report to the Commission on the biosecurity measures adopted."
Article 2
Member States shall immediately take the necessary measures to comply with this Decision and publish those measures. They shall immediately inform the Commission thereof.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 18 August 2006.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 224, 18.8.1990, p. 29. Directive as last amended by Directive 2002/33/EC of the European Parliament and of the Council (OJ L 315, 19.11.2002, p. 14).
[2] OJ L 274, 20.10.2005, p. 105. Decision as last amended by Decision 2006/405/EC (OJ L 158, 10.6.2006, p. 14).
[3] OJ L 274, 20.10.2005, p. 95.
--------------------------------------------------
