Euro exchange rates [1]
5 July 2006
(2006/C 157/01)
| Currency | Exchange rate |
USD | US dollar | 1,2794 |
JPY | Japanese yen | 146,74 |
DKK | Danish krone | 7,4602 |
GBP | Pound sterling | 0,69370 |
SEK | Swedish krona | 9,1776 |
CHF | Swiss franc | 1,5677 |
ISK | Iceland króna | 95,98 |
NOK | Norwegian krone | 7,9545 |
BGN | Bulgarian lev | 1,9558 |
CYP | Cyprus pound | 0,5750 |
CZK | Czech koruna | 28,475 |
EEK | Estonian kroon | 15,6466 |
HUF | Hungarian forint | 279,95 |
LTL | Lithuanian litas | 3,4528 |
LVL | Latvian lats | 0,6961 |
MTL | Maltese lira | 0,4293 |
PLN | Polish zloty | 4,0250 |
RON | Romanian leu | 3,5780 |
SIT | Slovenian tolar | 239,64 |
SKK | Slovak koruna | 38,388 |
TRY | Turkish lira | 2,0160 |
AUD | Australian dollar | 1,7172 |
CAD | Canadian dollar | 1,4152 |
HKD | Hong Kong dollar | 9,9425 |
NZD | New Zealand dollar | 2,0963 |
SGD | Singapore dollar | 2,0228 |
KRW | South Korean won | 1211,34 |
ZAR | South African rand | 9,0617 |
CNY | Chinese yuan renminbi | 10,2280 |
HRK | Croatian kuna | 7,2425 |
IDR | Indonesian rupiah | 11668,13 |
MYR | Malaysian ringgit | 4,671 |
PHP | Philippine peso | 67,591 |
RUB | Russian rouble | 34,3630 |
THB | Thai baht | 48,798 |
[1] Source: reference exchange rate published by the ECB.
--------------------------------------------------
