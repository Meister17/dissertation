COUNCIL DECISION
of 2 December 1999
concerning the conclusion of the Agreement for scientific and technological cooperation between the European Community and the Argentine Republic
(2000/15/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 170, in conjunction with Article 300(2), first sentence of the first subparagraph, and Article 300(3), first subparagraph, thereof,
Having regard to the proposal from the Commission(1),
Having regard to the opinion of the European Parliament(2),
Whereas:
(1) A Framework Agreement for trade and economic cooperation between the European Economic Community and the Argentine Republic(3) was concluded on 2 April 1990;
(2) The European Community and the Argentine Republic are pursuing specific RTD programmes in areas of common interest;
(3) On the basis of past experience, both sides have expressed a desire to establish a deeper and broader framework for the conduct of collaboration in science and technology;
(4) This Cooperation Agreement in the field of science and technology forms part of the global cooperation between the European Community and the Argentine Republic;
(5) By its Decision of 25 January 1999, the Council authorised the Commission to negotiate an Agreement for scientific and technological cooperation between the European Community and the Argentine Republic;
(6) By its Decision of 29 July 1999, the Council decided that the Agreement for scientific and technological cooperation was to be signed on behalf of the European Community;
(7) The Agreement for scientific and technological cooperation was signed on 20 September 1999;
(8) The Agreement for scientific and technological cooperation between the European Community and the Argentine Republic should be approved,
HAS DECIDED AS FOLLOWS:
Article 1
The Agreement for scientific and technological cooperation between the European Community and the Argentine Republic is hereby approved on behalf of the Community.
The text of the Agreement is attached to this Decision.
Article 2
Pursuant to Article 11 of the Agreement, the President of the Council shall give notification that the procedures necessary for the entry into force of the Agreement have been completed on the part of the Community.
Done at Brussels, 2 December 1999.
For the Council
The President
E. TUOMIOJA
(1) OJ C 274 E, 28.9.1999, p. 1.
(2) Opinion delivered on 4 November 1999 (not yet published in the Official Journal).
(3) OJ L 295, 26.10.1990, p. 67.
