Judgment of the Court (Third Chamber) of 28 September 2006 — Commission of the European Communities v Republic of Austria
(Case C-128/05) [1]
Parties
Applicant: Commission of the European Communities (represented by: D. Triantafyllou, Agent)
Defendant: Republic of Austria (represented by: H. Dossi and M. Fruhmann, Agents)
Re:
Failure of a Member State to fulfil its obligations — Infringement of Articles 2, 6, 9(2)(b), 17, 18 and 22(3) to (5) of Council Directive 77/388/EEC: Sixth Council Directive of 17 May 1977 on the harmonisation of the laws of the Member States relating to turnover taxes — Common system of value added tax: uniform basis of assessment (OJ 1977 L 145, p. 1) — Specific rules for companies involved in international passenger transport established in another State and whose annual turnover in Austria does not exceed EUR 22000 — No duty to submit periodic declarations and to pay the net amount of VAT
Operative part of the judgment
The Court:
1. Declares that by allowing taxable persons not established in Austria who transport passengers there not to submit tax return forms and not to pay the net amount of VAT when their annual turnover in Austria is below EUR 22000, in that case deeming the amount of VAT due to be equal to the amount of deductible VAT and making application of the simplified rules contingent on Austrian VAT not appearing on invoices or in other documents serving as invoices, the Republic of Austria has failed to fulfil its obligations under Articles 18(1)(a) and (2) and 22(3) to (5) of Sixth Council Directive 77/388/EEC of 17 May 1977 on the harmonisation of the laws of the Member States relating to turnover taxes — Common arrangement of value added tax: uniform basis of assessment;
2. Dismisses the action as to the remainder;
3. Orders the Republic of Austria to pay the costs.
[1] OJ C 182, 23.07.2005.
--------------------------------------------------
