Authorisation for State aid pursuant to Articles 87 and 88 of the EC Treaty
Cases where the Commission raises no objections
(2005/C 262/05)
Date of adoption of the decision: 10.8.2005
Member State: The Republic of Lithuania
Aid No: N 292/2005
Title: Aid for the reimbursement of insurance premiums
Objective: The promotion of voluntary insurance cover against losses caused by natural disasters, unfavourable weather conditions and other losses caused by adverse weather conditions
- 2002 m. birželio 25 d. Lietuvos Respublikos žemės ūkio ir kaimo plėtros įstatymas Nr. IX-987 (Valstybės žinios, Nr. 72-3009),
- 2004 m. kovo 4 d. Lietuvos Respublikos žemės ūkio ministro įsakymo Nr. 3D-92 "Draudimo įmokų dalinio kompensavimo taisyklės" projektas
Budget:
Overall budget: Litas 31,1 million (around EUR 9 million).
Annual budget: Litas 5,2 million (around EUR 1,5 million)
Aid intensity or amount: 50 % of insurance premium
Duration: 6 years from the approval by the Commission
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 6.9.2005
Member State: Germany (Meklenburg-Vorpommern)
Aid No: N 363/2004
Title: Aid for the construction of a whey refining plant
Objective: Aid would be provided to the company for the construction of a whey refining plant by the Mopro-Nord GmbH in Altentreptow, Mecklenburg-Vorpommern. The new whey refining plant will refine whey to the products WPC powder (60 % protein), de-mineralised whey powder (90 % whey protein), lactose and permeate powder
Legal basis: Investitionszulagengesetz 1995
Budget: Not determined. A budget of EUR 5230000 (nationally financed) is foreseen but may have to be reduced due to the premature start of the investment
Aid intensity or amount: 12,5 % (State aid), cumulated with other support (in particular EAGGF) up to 47,5 %
Duration: One-off
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 24.2.2005
Member State: Italy (Veneto)
Aid No: N 367/2004
Title: Plan to prevent pollution and improve water in the catchment area emptying into the Venice lagoon. Agriculture and livestock measures. 2004 allocation plan
— Measure C5.1.3a — Compatible agriculture in the Venice lagoon.
An increase of EUR 6611724 is planned for this measure, which involves agri-environmental aids to compensate crop rotation and maintenance of grassland areas over five years. The increase will allow agri-environmental undertakings to be activated on a further 13200 ha UAA, in addition to the 5000 covered by aid No N 53/2003.
— Measure C5.1.3.b — Environmental buffers and set-aside in the Venetian Lagoon.
An increase of EUR 11406290 is planned for this measure, which involves agri-environmental aids for the maintenance or the creation and maintenance for five years of grassed buffer zones along rivers, and set-aside of farmland for 10 years with a ban on the use of any plant-protection products, herbicides or slurry.
— Measure C5.1.3.c — Water management and rationalisation of water resources.
An increase of EUR 10406396 is planned for this measure, which involves investment aid to agricultural undertakings and aid for infrastructural works by land improvement consortia.
— Measure C5.1.4 — Slurry management plan and structural assistance for animal production.
An increase of EUR 22264354 is planned for this measure, which involves investment aid for livestock holdings.
— Measure C5.1.5 — Management of agricultural practices: eco-friendly environmental innovation.
A increase of EUR 1500000 is planned for this measure, which involves the application and/or consolidation of new techniques, including the revival of traditional practices, and new organic farming practices
Legal basis:
Deliberazione del Consiglio Regionale 4 maggio 2004, n. 24.
Legge 29 novembre 1984, n. 798 "Nuovi interventi per la salvaguardia di Venezia"
Budget: EUR 52188764 for the 5 measures
Aid intensity or amount: Varies according to the measures
Duration: Varies according to the measures
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 29.7.2005
Member State: Italy (Apulia)
Aid No: N 374/2004
Title: Assistance in agricultural areas damaged by bad weather (heavy rains between 1 April and 30 June 2004, province of Bari)
Objective: To compensate losses in agricultural production as a result of bad weather conditions
Legal basis: Decreto legislativo n. 102/2004
Budget: In accordance with the scheme approved (NN 54/A/04)
Aid intensity or amount: 80 % of losses
Duration: Measure implementing an aid scheme approved by the Commission
Other information: Measure applying the aid scheme approved by the Commission under State aid NN 54/A/2004 (Commission letter C(2005) 1622 final of 7 June 2005)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 29.7.2005
Member State: Italy (Veneto)
Aid No: N 375/2004
Title: Assistance in agricultural areas which have suffered damage (exccessive snow and persistent rain in February and March 2004, provinces of Rovigo, Venice and Vicenza)
Objective: To compensate for material damage to farming structures as a result of bad weather
Legal basis: Decreto legislativo n. 102/2004
Budget: EUR 1300000 to be financed from the budget approved under Aid No NN 54/A/04
Aid intensity or amount: 100 % of the damage
Duration: Measure applying an aid scheme approved by the Commission
Other details: Measure applying the scheme approved by the Commission under State aid NN 54/A/2004 (Commission letter C(2005)1622 final, dated 7 June 2005)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 12.8.2005
Member State: Italy (Calabria)
Aid No: N 391/2003 and N 29/2004
Title: Guarantee Fund for Agriculture
Objective: To facilitate access to loans
Legal basis: Legge regionale n. 24 dell' 8 luglio 2002, articolo 11; legge regionale n. 3 del 26 febbraio 2003; Misura 4.19 POR Calabria
Budget: EUR 2000000 for 2004, EUR 3000000 a year as from 2005
Aid intensity or amount: Linked to the scheme to which the guarantee applies (see aid N 287/03, N 313/03 and measures 4.5, 4.6 and 4.10 of the Calabria Regional Operational Programme)
Duration: Indefinite
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 25.7.2005
Member State: Italy (Piedmont)
Aid No: N 443/2004
Title: Legislative Decree No 102/2004: Assistance in agricultural areas which have suffered damage (hail-storm of June 2004, provinces of Torino, Asti, Alessandria and Cuneo)
Objective: To provide meteorological information about storms which caused damage for which compensation is to be granted under the scheme approved under aid No NN 54/A/04
Legal basis: Decreto legislativo 102/2004: "Nuova disciplina del Fondo di solidarietà nazionale"
Budget: The amount of the aid, which has still to be fixed, will be financed from the overall budget of EUR 200 million allocated to the approved scheme (NN 54/A/04)
Aid intensity or amount: Up to 100 %
Duration: One-off aid
Other information: Measure applying the scheme approved by the Commission under State aid NN 54/A/2004 (Commission letter C(2005)1622 final, dated 7 June 2005)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 29.7.2005
Member State: Italy (Sicily)
Aid No: N 444/2004
Title: Assistance in farming areas affected by natural disasters (hail-storm of 17 June 2004 in the province of Trapani)
Objective: To compensate farmers for damage to agricultural production and farming structures as a result of bad weather
Legal basis: Decreto legislativo n. 102/2004
Budget: See the approved scheme (NN 54/A/04)
Aid intensity or amount: Up to 100 %
Duration: Measure applying an aid scheme approved by the Commission
Other details: Measure applying the scheme approved by the Commission under State aid NN 54/A/2004 (Commission letter C(2005)1622 final, dated 7 June 2005)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 12.8.2005
Member State: Italy (Molise)
Aid No: N 461/2004
Title: Legislative Decree No 102/2004: Assistance in agricultural areas which have suffered damage (hail and fierce winds of 26 August 2004, Molise region, province of Campobasso)
Objective: To provide meteorological information about the bad weather that caused the damage for which compensation is to be paid as part of the scheme approved under Aid No NN 54/A/ 04
Legal basis: Decreto legislativo n. 102/2004: "Nuova disciplina del Fondo di solidarietà nazionale"
Budget: The amount of the aid, which has still to be fixed, will be financed from the overall budget of EUR 200 million allocated to the approved scheme (NN 54/A/04)
Aid intensity or amount: Up to 100 %
Duration: One-off aid
Other information: Measure applying the scheme approved by the Commission under State aid NN 54/A/2004 (Commission letter C(2005)1622 final, dated 7 June 2005)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 11.8.2005
Member State: Italy (Liguria)
Aid No: N 464/2004
Title: Legislative Decree No 102/2004: Assistance in agricultural areas which have suffered damage (hail-storms of 12 July 2004, Liguria region, province of Imperia)
Objective: To provide meteorological information about the bad weather that caused the damage for which compensation is to be paid as part of the scheme approved under Aid No NN 54/A/04
Legal basis: Decreto legislativo n. 102/2004: "Nuova disciplina del Fondo di solidarietà nazionale"
Budget: The amount of the aid, which has still to be fixed, will be financed from the overall budget of EUR 200 million allocated to the approved scheme (NN 54/A/04)
Aid intensity or amount: Up to 100 %
Duration: One-off aid.
Other information: Measure applying the scheme approved by the Commission under State aid NN 54/A/2004 (Commission letter C(2005) 1622 final, dated 7 June 2005).
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
--------------------------------------------------
