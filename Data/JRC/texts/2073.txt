Resolution of the Council and of the Representatives of the Governments of the Member States of 24 May 2005 meeting within the Council on implementing the common objective: to increase participation by young people in the system of representative democracy
(2005/C 141/02)
THE COUNCIL OF THE EUROPEAN UNION AND THE REPRESENTATIVES OF THE GOVERNMENTS OF THE MEMBER STATES MEETING WITHIN THE COUNCIL,
Whereas:
(1) The European Commission's White Paper entitled "European governance" [1], presented on 30 July 2001, places openness and participation at the top of its list of five principles underpinning good governance.
(2) The European Commission's White Paper entitled "A new impetus for European youth" [2], presented on 21 November 2001, sets out a new framework for European cooperation on youth affairs.
(3) The Council, in its conclusions of 14 February 2002 [3], considered the White Paper to be a starting point for developing a framework for cooperation on youth affairs.
(4) In its Resolution of 27 June 2002 [4], the Council
(a) adopted the open method of coordination as the new framework for cooperation on youth policy and approved four thematic priorities: participation, information, voluntary activities and greater understanding and knowledge of youth;
(b) invited the Commission, no later than the end of the first implementation exercise for the four thematic priorities, to prepare an evaluation report in association with the Member States on the framework for cooperation, covering in particular an evaluation of the open method of coordination and, as appropriate, suggestions for its modification, and to submit that report to the Council for consideration.
(5) In its Resolution of 25 November 2003 [5], the Council:
(a) adopted common objectives for the first two priorities, namely participation by and information for young people, setting increased participation by young people in the system of representative democracy as one of the common objectives for the participation priority;
(b) recalled that implementation of the common objectives must be flexible, incremental and appropriate for the youth field, and must respect the powers of the Member States and the principle of subsidiarity;
(c) invited the Commission to convene, where appropriate, representatives of the national administrations dealing with the youth field, in order to promote the exchange of information on the progress made and on best practice.
(6) The European Union is founded on both principles of representative democracy and participatory democracy.
HAVE TAKEN NOTE OF the work accomplished under the Irish Presidency, in particular at the informal Ministerial Conference in Clare, and of the discussions launched as part of cooperation with the Council of Europe.
(a) young women and men continue to have a keen involvement and interest in societal issues;
(b) young people's readiness for active citizenship does not automatically lead them to engage with the institutions of representative democracy;
(c) young people's participation and interest in the institutions of representative democracy tend to decline in many EU Member States;
(d) this lack of interest in democratic institutions is often reflected in a reluctance to make a long-term commitment to youth organisations, low turnouts at elections, declining membership of political parties and their youth sections.
(a) representative democracy is one of the mainstays of our society;
(b) democracies need the participation of all their citizens;
(c) in particular, the participation of young women and men in the institutions of representative democracy is crucial if democracy is to function properly;
(d) young people do not form a homogenous group, and the issue of their non-participation in the institutions of representative democracy presents various challenges, depending on gender, level of education, ethnic origin or other factors.
(a) the value of an ongoing dialogue at national level between young women and men and political leaders in creating a climate conducive to participation in the institutions of representative democracy;
(b) the importance of the Commission guideline on a structured dialogue between young people and political representatives;
(c) the key role played by non-formal education and by youth-oriented information in providing quality civic education on a broad basis;
(d) the special importance of youth organisations and associations in providing opportunities for young women and men to learn about democratic mechanisms and active, critical citizenship;
(a) that action to achieve these objectives cannot target young women and men alone but must also be directed at the institutions of representative democracy themselves;
(b) that the commitment of those participating in representative democracy should be highlighted and encouraged;
(c) that when implementing the common objective of increasing the participation of young women and men in the system of representative democracy, particular attention should be paid to creating a climate which encourages young women and men to participate, taking into account the important role played by the education system, youth organisations, political parties and the family;
(d) that particular care should be taken to tailor measures to their target groups and the groups' specific characteristics;
(e) to involve young people and youth organisations in developing specific implementing measures.
- encourage political parties' awareness of the importance of increasing their youth membership, having more young women and men on their organisational bodies and more young women and men on their lists of candidates;
- encourage, where applicable, the placing of young people on the electoral register;
- mobilise the support of regional and local authorities for young people's participation in representative democracy;
- make young people aware of the importance of participating in representative democracy and, in particular, through casting their votes.
- under the common priority regarding greater understanding and knowledge of youth, to make an inventory of the existing knowledge about obstacles to young people's active participation in representative democracy;
- to pool measures already undertaken and examples of good practice for achieving the common objective of increasing participation by young people in the system of representative democracy, at both Member State and European level;
- to strengthen dialogue between young people and political leaders, for example by introducing regular meetings;
- to meet in 2006 to review progress on this objective on the basis of national reports on the participation priority.
[1] 11574/01 – COM(2001) 428 final.
[2] 14441/01 – COM(2001) 681 final.
[3] OJ C 119, 22.5.2002, p. 6.
[4] OJ C 168, 13.7.2002, p. 2.
[5] JO C 295, 5.12.2003, p. 6.
--------------------------------------------------
