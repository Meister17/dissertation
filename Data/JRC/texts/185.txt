*****
COUNCIL REGULATION (EEC) No 1808/89
of 19 June 1989
amending Regulation (EEC) No 1981/82 drawing up the list of Community regions in which production aid for hops is granted only to recognized producer groups
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 1696/71 of 26 July 1971 on the common organization of the market in hops (1), as last amended by Regulation (EEC) No 3998/87 (2), and in particular Article 12 (4) thereof,
Having regard to the proposal from the Commission,
Whereas Article 12 (3) of Regulation (EEC) No 1696/71 provides for the granting of production aid to producers' groups in regions of the Community where the said groups are capable of ensuring that their members receive a fair income and of achieving rational management of supply; whereas those regions are listed in the Annex to Regulation (EEC) No 1981/82 (3), as amended by Regulation (EEC) No 4069/87 (4);
Whereas Ireland has notified that it has no longer a recognized producer group and that the conditions for granting the aid to a producer group laid down in Article 12 of Regulation (EEC) No 1696/71 are no longer fulfilled by the region 'Ireland'; whereas Regulation (EEC) No 1981/82 should therfore be amended as from the 1988 harvest,
HAS ADOPTED THIS REGULATION:
Article 1
In the Annex to Regulation (EEC) No 1981/82 the region 'Ireland' is hereby deleted from the list.
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
It shall apply as from the 1988 harvest.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Luxembourg, 19 June 1989.
For the Council
The President
C. ROMERO HERRERA
(1) OJ No L 175, 4. 8. 1971, p. 1.
(2) OJ No L 377, 31. 12. 1987, p. 40.
(3) OJ No L 215, 23. 7. 1982, p. 3.
(4) OJ No L 380, 31. 12. 1987, p. 32.
