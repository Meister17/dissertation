Commission Regulation (EC) No 1453/2005
of 6 September 2005
amending Annex II to Council Regulation (EC) No 872/2004 concerning further restrictive measures in relation to Liberia
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 872/2004 concerning further restrictive measures in relation to Liberia [1], and in particular Article 11(b) thereof,
Whereas:
(1) Annex II to Regulation (EC) No 872/2004 lists the competent authorities to which specific functions relating to the implementation of that Regulation are attributed.
(2) Lithuania, the Netherlands and Sweden requested that the address details concerning their competent authorities be amended,
HAS ADOPTED THIS REGULATION:
Article 1
Annex II to Regulation (EC) No 872/2004 is hereby amended in accordance with the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 6 September 2005.
For the Commission
Eneko Landáburu
Director-General of External Relations
[1] OJ L 162, 30.4.2004, p. 32. Regulation as last amended by Regulation (EC) No 874/2005 (OJ L 146, 10.6.2005, p. 5).
--------------------------------------------------
ANNEX
Annex II to Regulation (EC) No 872/2004 is amended as follows:
1. The address details under the heading "Lithuania" shall be replaced with:
"Ministry of Foreign Affairs
Security Policy Department
J. Tumo-Vaizganto 2
LT-01511 Vilnius
Tel. +370 5 236 25 16
Fax +370 5 231 30 90";
2. The address details under the heading "Netherlands" shall be replaced with:
"Ministerie van Financiën
Directie Financiële Markten/Afdeling Integriteit
Postbus 20201
2500 EE Den Haag
The Netherlands
Tel. (31-70) 342 89 97
Fax (31-70) 342 79 84";
3. The address details under the heading "Sweden" shall be replaced with:
"Articles 3, 4 and 5:
Försäkringskassan
SE-103 51 Stockholm
Tel. (46-8) 786 90 00
Fax (46-8) 411 27 89
Articles 7 and 8:
Finansinspektionen
Box 6750
SE-113 85 Stockholm
Tel. (46-8) 787 80 00
Fax (46-8) 24 13 35".
--------------------------------------------------
