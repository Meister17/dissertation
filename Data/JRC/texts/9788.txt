Commission Decision
of 12 September 2006
fixing the annual breakdown by Member State of the amount for Community support to rural development for the period from 1 January 2007 to 31 December 2013
(notified under document number C(2006) 4024)
(2006/636/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1698/2005 of 20 September 2005 on support for rural development by the European Agricultural Fund for Rural Development (EAFRD) [1], and in particular Article 69(4) thereof,
Whereas:
(1) Council Decision 2006/493/EC [2] fixed the amount of Community support to rural development for the period from 1 January 2007 to 31 December 2013, its annual breakdown and the minimum amount to be concentrated in regions eligible under the Convergence Objective.
(2) Point 40 of the Financial Perspectives 2007 to 2013, agreed upon by the European Council of December 2005, fixed the maximum level of transfers from funds supporting Cohesion.
(3) In accordance with Article 69(4) of Regulation (EC) No 1698/2005 the annual breakdown by Member State of the amounts for Community support to rural development is to be made after deduction of the resources devoted to technical assistance for the Commission and taking into account the amounts reserved for regions eligible under the Convergence Objective, past performance and particular situations and needs based on objective criteria. In accordance with Article 69(3) of Regulation (EC) No 1698/2005, those amounts should be indexed at 2 % per year, while, in accordance with paragraph 5 of that Article, in addition to those amounts, the Member States have to take into account for the purpose of programming the amounts resulting from modulation as provided for in Article 12(2) of Council Regulation (EC) No 1290/2005 of 21 June 2005 on the financing of the common agricultural policy [3].
(4) The total transfers from the European Agricultural Guarantee Fund to the EAFRD pursuant to Articles 10(2), 143d and 143e of Council Regulation (EC) No 1782/2003 of 29 September 2003 establishing common rules for direct support schemes under the common agricultural policy and establishing certain support schemes for farmers [4] are set by Commission Decision 2006/410/EC [5] for the budget years 2007 to 2013. Those amounts should be added to the annual breakdown by Member State for the purpose of rural development programming, according to the methodology established in Article 10(3) and (4) of Regulation (EC) No 1782/2003. The annual breakdown by Member States of the amounts resulting from modulation provided for in Article 10(1) of Regulation (EC) No 1782/2003 have been allocated by Commission Decision 2006/588/EC [6].
(5) The annual breakdown should not include the amounts for Bulgaria and Romania since the Treaty of Accession of those countries has not yet entered into force. Upon the entry into force of the Treaty of Accession of those countries the annual breakdown should be amended accordingly to include allocations for those countries,
HAS ADOPTED THIS DECISION:
Article 1
The annual breakdown by Member State of commitment appropriations for Community support to rural development for the period from 1 January 2007 to 31 December 2013, as referred to in Article 69 of Regulation (EC) No 1698/2005, is set out in the Annex to this Decision.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 12 September 2006.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 277, 21.10.2005, p. 1.
[2] OJ L 195, 15.7.2006, p. 22.
[3] OJ L 209, 11.8.2005, p. 1. Regulation as amended by Regulation (EC) No 320/2006 (OJ L 58, 28.2.2006, p. 42).
[4] OJ L 270, 21.10.2003, p. 1. Regulation as last amended by Commission Regulation (EC) No 1156/2006 (OJ L 208, 29.7.2006, p. 3).
[5] OJ L 163, 15.6.2006, p. 10.
[6] OJ L 240, 2.9.2006, p. 6.
--------------------------------------------------
ANNEX
Breakdown by Member State of Community support for rural development 2007 to 2013
(current prices in EUR) |
| 2007 | 2008 | 2009 | 2010 | 2011 | 2012 | 2013 | 2007-2013 total | of which minimum for regions under the Convergence objective Total |
Belgium | 63991299 | 63957784 | 60238083 | 59683509 | 59267519 | 56995480 | 54476632 | 418610306 | 40744223 |
Czech Republic | 396623321 | 392638892 | 388036387 | 400932774 | 406640636 | 412672094 | 417962250 | 2815506354 | 1635417906 |
Denmark | 62592573 | 66344571 | 63771254 | 64334762 | 63431467 | 62597618 | 61588551 | 444660796 | 0 |
Germany | 1184995564 | 1186941705 | 1147425574 | 1156018553 | 1159359200 | 1146661509 | 1131114950 | 8112517055 | 3174037771 |
Estonia | 95608462 | 95569377 | 95696594 | 100929353 | 104639066 | 108913401 | 113302602 | 714658855 | 387221654 |
Greece | 461376206 | 463470078 | 453393090 | 452018509 | 631768186 | 626030398 | 619247957 | 3707304424 | 1905697195 |
Spain | 1012456383 | 1030880527 | 1006845141 | 1013903294 | 1057772000 | 1050937191 | 1041123263 | 7213917799 | 3178127204 |
France | 931041833 | 942359146 | 898672939 | 909225155 | 933778147 | 921205557 | 905682332 | 6441965109 | 568263981 |
Ireland | 373683516 | 355014220 | 329171422 | 333372252 | 324698528 | 316771063 | 307203589 | 2339914590 | 0 |
Italy | 1142143461 | 1135428298 | 1101390921 | 1116626236 | 1271659589 | 1266602382 | 1258158996 | 8292009883 | 3341091825 |
Cyprus | 26704860 | 24772842 | 22749762 | 23071507 | 22402714 | 21783947 | 21037942 | 162523574 | 0 |
Latvia | 152867493 | 147768241 | 142542483 | 147766381 | 148781700 | 150188774 | 151198432 | 1041113504 | 327682815 |
Lithuania | 260974835 | 248836020 | 236928998 | 244741536 | 248002433 | 250278098 | 253598173 | 1743360093 | 679189192 |
Luxembourg | 14421997 | 13661411 | 12655487 | 12818190 | 12487289 | 12181368 | 11812084 | 90037826 | 0 |
Hungary | 570811818 | 537525661 | 498635432 | 509252494 | 547603625 | 563304619 | 578709743 | 3805843392 | 2496094593 |
Malta | 12434359 | 11527788 | 10656597 | 10544212 | 10347884 | 10459190 | 10663325 | 76633355 | 18077067 |
Netherlands | 70536869 | 72638338 | 69791337 | 70515293 | 68706648 | 67782449 | 66550233 | 486521167 | 0 |
Austria | 628154610 | 594709669 | 550452057 | 557557505 | 541670574 | 527868629 | 511056948 | 3911469992 | 31938190 |
Poland | 1989717841 | 1932933351 | 1872739817 | 1866782838 | 1860573543 | 1857244519 | 1850046247 | 13230038156 | 6997976121 |
Portugal | 562210832 | 562491944 | 551196824 | 559018566 | 565142601 | 565192105 | 564072156 | 3929325028 | 2180735857 |
Slovenia | 149549387 | 139868094 | 129728049 | 128304946 | 123026091 | 117808866 | 111981296 | 900266729 | 287815759 |
Slovakia | 303163265 | 286531906 | 268049256 | 256310239 | 263028387 | 275025447 | 317309578 | 1969418078 | 1106011592 |
Finland | 335121543 | 316143440 | 292385407 | 296367134 | 287790092 | 280508238 | 271617053 | 2079932907 | 0 |
Sweden | 292133703 | 277225207 | 256996031 | 260397463 | 252975513 | 246760755 | 239159282 | 1825647954 | 0 |
United Kingdom | 263996373 | 283001582 | 274582271 | 276600084 | 273334332 | 270695626 | 267364152 | 1909574420 | 188337515 |
Total | 11357312403 | 11182240092 | 10734731213 | 10827092785 | 11238887764 | 11186469323 | 11136037766 | 77662771346 | 28544460460 |
--------------------------------------------------
