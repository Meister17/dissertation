Decision No 3/2005 of the ACP-EC Council of Ministers
of 8 March 2005
concerning the adoption of the Rules of Procedure of the ACP-EC Committee of Ambassadors
(2005/299/EC)
THE ACP-EC COUNCIL OF MINISTERS,
Having regard to the ACP-EC Partnership Agreement signed in Cotonou on 23 June 2000, in particular Article 16(3) thereof,
Whereas:
(1) By Decision No 2/2001 of 30 January 2001, the ACP-EC Committee of Ambassadors adopted its Rules of Procedure.
(2) Certain amendments are necessary to take account of the accession of new Member States to the European Union.
(3) At the 29th session of the ACP-EC Council of Ministers, held in Gaborone, Botswana on 6 May 2004, the decision was taken to amend the Rules of Procedure accordingly,
HAS DECIDED AS FOLLOWS:
Article 1
Dates and locations of meetings
1. As provided in Article 16(2) of the ACP-EC Partnership Agreement (hereinafter referred to as the ACP-EC Agreement), the ACP-EC Committee of Ambassadors (hereinafter referred to as "the Committee"), shall meet regularly, in particular to prepare meetings of the ACP-EC Council of Ministers (hereinafter referred to as "the Council"), and whenever it proves necessary, at the request of one of the parties.
2. The Committee shall be convened by its Chair. The dates of its meetings shall be fixed by common agreement between the parties.
3. The Committee shall meet at the seat of the Council of the European Union or at the seat of the Secretariat of the ACP Group of States. It may, however, meet in a city of an ACP State if a special decision is taken to this effect.
Article 2
The Committee’s functions
1. In accordance with Article 16(2) of the ACP-EC Agreement, the Committee shall assist the Council in carrying out its tasks and shall carry out any mandate which the Council may entrust to it. In this context, it shall monitor implementation of the ACP-EC Agreement and progress towards achieving the objectives set therein.
2. The Committee shall report to the Council, in particular on matters in which competence has been delegated.
3. It shall also submit to the Council any resolutions, recommendations or opinions that it considers necessary or appropriate.
Article 3
Agenda of meetings
1. The provisional agenda for every meeting shall be drawn up by the Chair. It shall be communicated to the other members of the Committee at least eight days before the date of the meeting.
The provisional agenda shall include those items in respect of which the Chair has received a request for inclusion 10 days before the date of the meeting. The only items to be included on the provisional agenda shall be those for which documentation has been submitted to the Secretariat of the Council in time to be forwarded to the members of the Committee at least eight days before the date of the meeting.
2. The agenda shall be adopted by the Committee at the beginning of each meeting. In urgent cases the Committee may decide, at the request of the ACP States or of the Community, to include on the agenda items in respect of which the time limits laid down in paragraph 1 have not been observed.
Article 4
Proceedings
1. The Committee shall act by common agreement between the Community on the one part and the ACP States on the other.
2. The proceedings of the Committee shall be valid only if at least half of the permanent representatives of the Member States of the Community, one representative of the Commission of the European Communities (hereinafter referred to as the Commission), and half the members of the ACP Committee of Ambassadors are present.
3. All members of the Committee unable to attend may be represented. In this case they shall inform the Chair and shall indicate the person or delegation authorised to represent them. The representative shall exercise all the rights of the member unable to attend.
4. Committee members may be accompanied by advisers.
5. A representative of the European Investment Bank (hereinafter referred to as "the Bank"), shall be present at Committee meetings when matters concerning it are on the agenda.
Article 5
Written procedures, official publications and form of acts
Articles 4, 9(3) and 14 of the Rules of Procedure of the ACP-EC Council shall apply to the acts adopted by the Committee.
Article 6
States attending with observer status
1. Representatives of signatory States to the ACP-EC Agreement which, on the date of its entry into force, have not yet completed the procedures referred to in Article 93(1) and (2) thereof, may attend Committee meetings as observers. They may in this case be authorised to take part in the Committee’s debates.
2. The same rule shall apply to the countries referred to in Article 93(6) of the ACP-EC Agreement.
3. The Committee may authorise representatives of a country applying for accession to the ACP-EC Agreement to take part in the proceedings of the Committee as observers.
Article 7
Confidentiality
1. Unless otherwise decided, meetings of the Committee shall not be public.
2. Without prejudice to such other provisions as may apply, the Committee’s deliberations shall be covered by the obligation of professional secrecy unless the Committee decides otherwise.
Article 8
Communications and minutes of meetings
1. All communications provided for in these Rules of Procedure shall be forwarded through the Secretariat of the Council to the representatives of the ACP States, the Secretariat of the ACP Group of States, the Permanent Representatives of the Member States, the General Secretariat of the Council of the European Union and the Commission.
Such communications shall also be sent to the President of the Bank when they concern the Bank.
2. Minutes shall be kept of each meeting, noting in particular the decisions taken by the Committee.
After their approval by the Committee, the minutes shall be signed by the Chair of the Committee and the Secretaries of the Council and shall be kept in the Council archives. Copies of the minutes shall be sent to the recipients listed in paragraph 1.
Article 9
The Chair
The Office of Chair of the Committee shall be held alternately, for six months at a time, by a Permanent Representative of a Member State, designated by the Community, and by a Head of Mission representing an ACP State designated by the ACP States.
Article 10
Correspondence and documentation
1. Correspondence intended for the Committee shall be sent to its Chair at the seat of the Council Secretariat.
2. Unless decided otherwise, the Committee shall conduct its deliberations on the basis of documents drafted in the official languages of the parties.
Article 11
Committees, subcommittees and working parties
1. The Committee shall be assisted by:
(i) the Customs Cooperation Committee set up by Article 37 of Protocol No 1 to Annex V to the ACP-EC Agreement;
(ii) the permanent joint group on bananas provided for in Article 3 of Protocol 5 to Annex V to the ACP-EC Agreement;
(iii) the Subcommittee on Trade Cooperation;
(iv) the Subcommittee on Sugar;
(v) the joint working party on rice referred to in paragraph 5 of Declaration XXIV of the Final Act to the ACP-EC Agreement;
(vi) the joint working party on rum referred to in paragraph 6 of Declaration XXV of the Final Act to the ACP-EC Agreement.
2. The Committee may set up other appropriate committees, subcommittees or working parties to carry out the work it considers necessary for the accomplishment of the tasks set out in Article 16(2) of the ACP-EC Agreement.
3. These committees, subcommittees and working parties shall submit reports on their work to the Committee.
Article 12
Composition of committees, subcommittees and working parties
1. With the exception of the Customs Cooperation Committee, the committees, subcommittees and working parties referred to in Article 11 shall comprise ACP ambassadors or their representatives, representatives of the Commission and representatives of the Member States.
2. A representative of the Bank shall attend committee, subcommittee and working party meetings where their agendas contain items concerning the Bank.
3. The members of these committees, subcommittees and working parties may be assisted in their tasks by experts.
Article 13
Chair of committees, subcommittees and working parties
1. The committees, subcommittees and working parties referred to in Article 11 shall be jointly chaired by an ACP Ambassador for the ACP States and a representative of the Commission or a representative of a Member State for the Community.
2. Without prejudice to paragraph 1 the Co-chairs may, in exceptional cases and by common agreement, be represented by any person they designate.
Article 14
Procedures for committee, subcommittee and working party meetings
The committees, subcommittees and working parties referred to in Article 11 shall meet at the request of one of the two parties after consultation between their Chairs and notice of seven days shall be given for such meetings, except in urgent cases.
Article 15
Rules of Procedure of committees, subcommittees and working parties
The committees, subcommittees and working parties referred to in Article 11 may establish their Rules of Procedure with the agreement of the Committee.
Article 16
Secretariat
1. Secretariat work and other work necessary for the operation of the Committee and of the committees, subcommittees and working parties referred to in Article 11 (drafting agendas and distributing the relevant documents) shall be carried out by the Council Secretariat.
2. As soon as possible after each meeting, the Secretariat shall draft minutes of the meetings of these committees, subcommittees and working parties.
The minutes shall be sent by the Secretariat of the Council to the representatives of the ACP States, the Secretariat of the ACP Group of States, the Permanent Representatives of the Member States, the General Secretariat of the Council of the European Union and the Commission.
Article 17
The present Decision annuls and replaces Decision No 2/2001 of 30 January 2001 of the ACP-EC Committee of Ambassadors concerning the adoption of its Rules of Procedure.
Done at Brussels, 8 March 2005.
For the ACP-EC Council of Ministers
The President
J. Asselborn
--------------------------------------------------
