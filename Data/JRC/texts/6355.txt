Council Decision
of 14 November 2005
concerning the conclusion of an Agreement in the form of an Exchange of Letters between the European Community and the United States of America on matters related to trade in wine
(2005/798/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 133, in conjunction with the first sentence of the first subparagraph of Article 300(2) thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) On 23 October 2000 the Council authorised the Commission to negotiate an Agreement on trade in wine between the European Community and the United States of America.
(2) The negotiations have been concluded and the Agreement between the European Community and the United States of America on trade in wine as well as an Agreement in the form of an Exchange of Letters between the European Community and the United States of America on matters related to trade in wine was initialed by both Parties on 14 September 2005.
(3) Certain derogations from the Community rules relating to wine-making practices and certain certification practices are provided for in particular in Council Regulation (EC) No 1037/2001 of 22 May 2001 authorising the offer and delivery for direct human consumption of certain imported wines which may have undergone oenological processes not provided for in Regulation (EC) No 1493/1999 [1] and in Commission Regulation (EC) No 883/2001 of 24 April 2001 laying down detailed rules for implementing Council Regulation (EC) No 1493/1999 as regards trade with third countries in products in the wine sector [2] in favour of wines originating in the United States.
(4) The derogations will expire on 31 December 2005. Articles 4 and 9 of the Agreement on trade in wine will continue to treat wines originating in the United States in the same way but, in accordance with Article 17(2) of that Agreement, these provisions will only apply from the first day of the second month following receipt of the written notice referred to in Article 6(3) of that Agreement.
(5) It was therefore necessary to negotiate a separate Agreement in the form of an Exchange of Letters to cover the period from 31 December 2005 until the date of application of Articles 4 and 9 of the Agreement on trade in wine.
(6) The Agreement in the form of an Exchange of Letters should therefore be approved.
(7) In order to facilitate the implementation of the Agreement in the form of an Exchange of Letters, the Commission should be authorised to adopt the necessary measures for its implementation, in accordance with the procedure laid down in Council Regulation (EC) No 1493/1999 of 17 May 1999 on the common organisation of the market in wine [3], including any necessary prolongation of the derogation provided for in Regulation (EC) No 1037/2001,
HAS DECIDED AS FOLLOWS:
Article 1
The Agreement in the form of an Exchange of Letters between the European Community and the United States of America on matters related to trade in wine (hereinafter referred to as the Agreement) is hereby approved on behalf of the Community.
The text of the Agreement is attached to this Decision.
Article 2
The President of the Council is hereby authorised to designate the person empowered to sign the Agreement in order to bind the Community.
Article 3
The Commission is hereby authorised to adopt the necessary measures for implementation of the Agreement, in accordance with the procedure laid down in Article 75(2) of Regulation (EC) No 1493/1999, including any necessary prolongation of the derogation provided for in Regulation (EC) No 1037/2001.
Article 4
This Decision shall be published in the Official Journal of the European Union.
Done at Brussels, 14 November 2005.
For the Council
The President
T. Jowell
[1] OJ L 145, 31.5.2001, p. 12. Regulation as amended by Regulation (EC) No 2324/2003 (OJ L 345, 31.12.2003, p. 24).
[2] OJ L 128, 10.5.2001, p. 1. Regulation as last amended by Regulation (EC) No 908/2004 (OJ L 163, 30.4.2004, p. 56).
[3] OJ L 179, 14.7.1999, p. 1. Regulation as last amended by Regulation (EC) No 1428/2004 (OJ L 263, 10.8.2004, p. 7).
--------------------------------------------------
