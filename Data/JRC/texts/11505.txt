Judgment of the Court (First Chamber) of 13 July 2006 (reference for a preliminary ruling from the Oberlandesgericht Düsseldorf — Germany) — Gesellschaft für Antriebstechnik mbH & Co. KG (GAT) v Lamellen und Kupplungsbau Beteiligungs KG (LuK)
(Case C-4/03) [1]
Referring court
Oberlandesgericht Düsseldorf
Parties to the main proceedings
Applicant: Gesellschaft für Antriebstechnik mbH & Co. KG (GAT)
Defendant: Lamellen und Kupplungsbau Beteiligungs KG (LuK)
Re:
Reference for a preliminary ruling — Oberlandesgericht Düsseldorf — Interpretation of Article 16(4) of the Brussels Convention — Exclusive jurisdiction "in proceedings concerned with the … validity of patents" — Whether that covers a declaratory action to establish that a patent has (or has not) been infringed, in the course of which one party pleads that the patent is invalid
Operative part of the judgment
The Court:
Article 16(4) of the Convention of 27 September 1968 on Jurisdiction and the Enforcement of Judgments in Civil and Commercial Matters, as last amended by the Convention of 29 November 1996 on the Accession of the Republic of Austria, the Republic of Finland and the Kingdom of Sweden, is to be interpreted as meaning that the rule of exclusive jurisdiction laid down therein concerns all proceedings relating to the registration or validity of a patent, irrespective of whether the issue is raised by way of an action or a plea in objection.
[1] OJ C 55, 8.3.2003.
--------------------------------------------------
