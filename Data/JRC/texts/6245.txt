Commission Regulation (EC) No 1686/2005
of 14 October 2005
setting the production levies and the coefficient for the additional levy in the sugar sector for the 2004/05 marketing year
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1260/2001 of 19 June 2001 on the common organisation of the markets in the sugar sector [1], and in particular the first indent of Article 15(8) and Article 16(5) thereof,
Whereas:
(1) Article 8 of Commission Regulation (EC) No 314/2002 of 20 February 2002 laying down detailed rules for the application of the quota system in the sugar sector [2] provides that the basic production levy and the B levy together with, if required, the coefficient referred to in Article 16(2) of Regulation (EC) No 1260/2001 for sugar, isoglucose and inulin syrup are to be set before 15 October in respect of the preceding marketing year.
(2) For the 2004/05 marketing year, Commission Regulation (EC) No 1462/2004 of 17 August 2004 revising the maximum amount for the B production levy and amending the minimum price for B beet in the sugar sector for the 2004/05 marketing year [3] increases the maximum amount of the B levy referred to in the first indent of Article 15(4) of Regulation (EC) No 1260/2001 to 37,5 % of the intervention price for white sugar.
(3) For the 2004/05 marketing year, the estimate of the overall loss recorded in accordance with Article 15(1) and (2) of Regulation (EC) No 1260/2001 requires, in accordance with paragraphs 4 and 5 of that Article, the adoption of the maximum amounts of 2 % for the basic levy and 37,5 % for the B levy, set in the first indent of the second subparagraph of paragraph 3 of that Article and in Article 1 of Regulation (EC) No 1462/2004 respectively.
(4) Article 16(1) of Regulation (EC) No 1260/2001 provides that an additional levy is to be charged where the overall loss recorded in accordance with Article 15(1) and (2) of that Regulation is not fully covered by the proceeds from the basic production levy and the B levy. For the 2004/05 marketing year, this uncovered overall loss amounts to EUR 133529997. The coefficient referred to in Article 16(2) of Regulation (EC) No 1260/2001 should therefore be set. For the purposes of fixing that coefficient, account should be taken of the levies set for the 2003/04 marketing year for the Member States of the Community as constituted on 30 April 2004.
(5) The Management Committee for Sugar has not delivered an opinion within the time-limit set by its chairman,
HAS ADOPTED THIS REGULATION:
Article 1
The production levies in the sugar sector for the 2004/05 marketing year shall be as follows:
(a) EUR 12,638 per tonne of white sugar as the basic production levy on A sugar and B sugar;
(b) EUR 236,963 per tonne of white sugar as the B levy on B sugar;
(c) EUR 5,330 per tonne of dry matter as the basic production levy on A isoglucose and B isoglucose;
(d) EUR 99,424 per tonne of dry matter as the B levy on B isoglucose;
(e) EUR 12,638 per tonne of dry matter sugar/isoglucose equivalent as the basic production levy on A inulin syrup and B inulin syrup;
(f) EUR 236,963 per tonne of dry matter sugar/isoglucose equivalent as the B levy on B inulin syrup.
Article 2
For the 2004/05 marketing year, the coefficient provided for in Article 16(2) of Regulation (EC) No 1260/2001 shall be 0,27033 for the Czech Republic, Latvia, Lithuania, Hungary, Poland, Slovenia and Slovakia and 0,15935 for the other Member States.
Article 3
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 14 October 2005.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 178, 30.6.2001, p. 1. Regulation as last amended by Commission Regulation (EC) No 39/2004 (OJ L 6, 10.1.2004, p. 16).
[2] OJ L 50, 21.2.2002, p. 40. Regulation as last amended by Regulation (EC) No 38/2004 (OJ L 6, 10.1.2004, p. 13).
[3] OJ L 270, 18.8.2004, p. 4.
--------------------------------------------------
