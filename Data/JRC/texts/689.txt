Commission Regulation (EC) No 1566/2000
of 18 July 2000
amending Regulation (EEC) No 94/92 laying down detailed rules for implementing the arrangements for imports from third countries provided for in Council Regulation (EEC) No 2092/91
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2092/91 of 24 June 1991 on organic production of agricultural products and indications referring thereto on agricultural products and foodstuffs(1), as last amended by Commission Regulation (EC) No 1437/2000(2), and in particular Article 11(1)(a) thereof,
Whereas:
(1) Article 11(1) of Regulation (EEC) No 2092/91 stipulates that products which are imported from a third country may be marketed only where they originate in a third country appearing on a list drawn up in accordance with the criteria laid down in paragraph 2 of that Article. That list is given in the Annex to Commission Regulation (EEC) No 94/92(3), as last amended by Regulation (EC) No 548/2000(4).
(2) The Australian authorities have asked the Commission to include a new inspection and certification body in accordance with Regulation (EEC) No 94/92.
(3) The Australian authorities have provided the Commission with all the necessary guarantees and information to satisfy it that the new inspection and certification body meets the criteria laid down in Article 11(2) of Council Regulation (EEC) No 2092/91.
(4) The measures provided for in this Regulation are in accordance with the opinion of the Committee referred to in Article 14 of Regulation (EEC) No 2092/91,
HAS ADOPTED THIS REGULATION:
Article 1
The Annex to Regulation (EEC) No 94/92 is amended as shown in the Annex hereto.
Article 2
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 18 July 2000.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 198, 22.7.1991, p. 1.
(2) OJ L 161, 1.7.2000, p. 62.
(3) OJ L 11, 17.1.1992, p. 14.
(4) OJ L 67, 15.3.2000, p. 12.
ANNEX
Point 3 of the text referring to Australia is replaced by the following: "Inspection bodies:
- Australian Quarantine and Inspection Service (AQIS) (Department of Agriculture, Fisheries and Forestry)
- Bio-dynamic Research Institute (BDRI)
- Biological Farmers of Australia (BFA)
- Organic Vignerons Association of Australia Inc. (OVAA)
- Organic Herb Growers of Australia Inc. (OHGA)
- Organic Food Chain Pty Ltd (OFC)
- National Association of Sustainable Agriculture, Australia (NASAA)"
