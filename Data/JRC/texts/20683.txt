COMMISSION DECISION of 24 October 1994 concerning questionnaires for Member States reports on the implementation of certain Directives in the waste sector (implementation of Council Directive 91/692/EEC) (94/741/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/692/EEC of 23 December 1991, on the standardization and rationalization of reports on the implementation of certain Directives relating to the Environment (1), and in particular Articles 5 and 6 and its Annex VI,
Having regard to Council Directive 75/439/EEC of 16 June 1975 on the disposal of waste oils (2), as last amended by Directive 91/692/EEC,
Having regard to Council Directive 75/442/EEC of 15 July 1975 on waste (3), as last amended by Directive 91/692/EEC,
Having regard to Council Directive 86/278/EEC of 12 June 1986 on the protection of the environment, and in particular of the soil, when sewage sludge is used in agriculture (4), amended by Directive 91/692/EEC,
Whereas, Article 18 of Directive 75/439/EEC, Article 16 of Directive 75/442/EEC, and Article 17 of Directive 86/278/EEC have been replaced by Article 5 of Directive 91/692/EEC which requires Member States to transmit to the Commission information of the implementation of certain Community Directives in the form of a sectorial report;
Whereas, this report has to be established on the basis of a questionnaire or an outline drafted by the Commission in accordance with the procedure set out in Article 6 of Directive 91/692/EEC;
Whereas the first sectoral report will cover the period 1995 to 1997 inclusive;
Whereas the measures envisaged by this Decision are in accordance with the opinion expressed by the Committee established in accordance with Article 6 of the aforementioned Directive,
HAS ADOPTED THIS DECISION:
Article 1
The questionnaires attached to this Decision, which relate to Council Directives 75/439/EEC, 75/442/EEC and 86/278/EEC are hereby adopted.
Article 2
The Member States will use these questionnaires as a basis for the drawing up of the sectorial reports they are required to submit to the Commission pursuant to Article 5 of Directive 91/692/EEC.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 24 October 1994.
For the Commission
Yannis PALEOKRASSAS
Member of the Commission
(1) OJ No L 377, 31. 12. 1991, p. 48.
(2) OJ No L 194, 25. 7. 1975, p. 23.
(3) OJ No L 194, 25. 7. 1975, p. 47.
(4) OJ No L 181, 4. 7. 1986, p. 6.
ANNEX
LIST OF QUESTIONNAIRES 1. Questionnaire relating to Council Directive 75/439/EEC of 16 June 1975 on the disposal of waste oils (1), as last amended by Directive 91/692/EEC (2).
2. Questionnaire relating to Council Directive 75/442/EEC of 15 July 1975 on waste (3), as last amended by Directive 91/692/EEC.
3. Questionnaire relating to Council Directive 86/278/EEC of 12 June 1986 on the protection of the environment, and in particular the soil, when sewage sludge is used in agriculture (4), as amended by Directive 91/692/EEC.
Application of Directive 91/692/EEC on harmonizing and rationalizing reports on the implementation of certain Directives relating to the environment
QUESTIONNAIRE for the report of the Member States on the transposition and implementation of Directive 75/439/EEC on the disposal of waste oils as last amended by Directive 91/692/EEC (5)
There is no need to repeat information which has already been provided
I. INCORPORATION INTO NATIONAL LAW 1. (a) Has the Commission been provided with details of the current laws and regulations in force to incorporate the Directive as amended into national law?
(Yes/No)
(b) If the answer to (a) above is 'No', please state the reasons why.
2. (a) Have measures been taken pursuant to Article 7?
(Yes/No)
(b) If the answer to (a) above is 'Yes', has the Commission been provided with details of these measures?
(Yes/No)
(c) If the answer to (b) above is 'No', please state the reasons why.
3. (a) Have more stringent measures been adopted pursuant to Article 16?
(Yes/No)
(b) If the answer to (a) above is 'Yes', has the Commission been provided with details of these measures?
(Yes/No)
(c) If the answer to (b) above is 'No', please state the reasons why.
II. IMPLEMENTATION OF THE DIRECTIVE 1. (a) Pursuant to Article 2 and 3, have the necessary measures been taken to ensure that waste oils are collected and disposed of without causing any avoidable damage to man and the environment?
(b) If the answer to (a) above is 'No', please state the reasons why.
(c) If the answer to (a) above is 'Yes', please complete the following tables, as far as possible, stating whether any of the information given is an estimate.
2. (a) Have any technical, economic and organizational constraints referred to in Article 3 (1) prevented the Member State from giving priority to the processing of waste oils by regeneration?
(Yes/No)
(b) If the answer to (a) above is 'Yes', please provide details.
(c) Have any technical, economic and organizational constraints referred to in Article 3 (2) affected the feasibility of the combustion of waste oils?
(Yes/No)
(d) If the answer to (c) above is 'Yes', please provide details.
(e) If the abovementioned constraints prevented the regeneration or combustion of waste oils, have any measures been taken in accordance with Article 3 (3)?
(Yes/No)
(f) If the answer to (e) above is 'Yes', please provide details.
3. (a) Have any public information and promotional campaigns, pursuant to Article 5 (1), been carried out?
(b) If the answer to (a) above is 'Yes', please provide details of national campaigns and where possible give examples of other campaigns, indicating among others the authority which launched the campaign, the nature of the campaign, the media (TV, radio, newspapers, etc.), the target groups, and any assessment of the effeciveness of the campaign if this has been carried out (it can be expressed in terms of any increase of waste oil collection for treatment or regeneration).
4. Please complete the following table, relating to undertakings collecting waste oils (state whether any of the information given is an estimate).5. (a) Has it been decided to allocate the waste oils to any of the types of processing set out in Article 3, as provided for in Article 5 (3)?
(Yes/No)
(b) If the answer to (a) above is 'Yes', please state the nature of the allocation.
(c) If the answer to (a) above is 'Yes', please state whether appropriate checks have been instituted and, if so, briefly describe them.
6. (a) Please complete the following table, relating to undertakings which dispose of waste oils, stating whether any of the information given is an estimate.
Table ATable B(b) Indicate how the competent authority satisfied itself that all appropriate environmental and health protection measures (as provided for under Article 6 (2)) have been taken.
7. (a) Please complete the following table with the limit values set for the substances listed in the Annex of the Directive (Article 8 (1) (a)) and for any other parameters or substances.
(b) Please give details of the controls that apply to plants with a thermal input of less than 3 MW (Article 8 (1) (b)), indicating any national limit values that have been set by completing the following table.(c) Please complete the following table, relating to the combustion of waste oils in plants, stating whether any of the information given is an estimate.8. Pursuant to Article 11, please complete the following table with respect to the minimum quantities of waste oil as specified by Member States.9. (a) Are indemnities, as provided for under Article 14, granted to undertakings which collect waste oils?
(Yes/No)
(b) If the answer to (a) above is 'Yes', please give the average amount of these indemnities, describe the basis on which they are calculated and describe the method(s) of financing.
10. (a) Are indemnities, as provided for under Article 14, granted to undertakings which dispose of waste oils?
(Yes/No)
(b) If the answer to (a) above is 'Yes', please give the average amount of these indemnities, describe the basis on which they are calculated and describe the method(s) of financing.
Application of Directive 91/692/EEC on harmonizing and rationalizing reports on the implementation of certain Directives relating to the environment
QUESTIONNAIRE
for the report of the Member States on the transposition and implementation of Directive 75/442/EEC on waste as last amended by Directive 91/692/EEC (6)
There is not need to repeat information which has already been provided.
I. INCORPORATION INTO NATIONAL LAW 1. (a) Has the Commission been provided with details of the current laws and regulations in force to incorporate the Directive as amended into national law?
(Yes/No)
(b) If the answer to (a) above is 'No', please state the reasons why
2. Please indicate in the table below the (estimated) number of competent authorities in each of the NUTS levels designated pursuant to Article 6 and indicate the competence by ticking the relevant boxes.II. IMPLEMENTATION OF THE DIRECTIVE
1. (a) Have waste management plans been drawn up in order to attain the objectives referred to in Articles 3, 4 and 5?
(Yes/No)
(b) If the answer to (a) above is 'No', please state the reasons why.
(c) For each waste management plan which has been drawn up, please provide details as follows (use a separate sheet if necessary)(d) (i) Has any collaboration, as referred to in Article 7 (2), taken place with other Member States or with the Commission?
(Yes/No)
(ii) If the answer to (i) above is 'Yes', please give details of the extent and form of any such collaboration.
(e) (i) Has the Commission been provided with details of any general measures taken pursuant to Article 7 (3)?
(Yes/No)
(ii) If the answer to (i) above is 'No', please state the reasons why.
2. (a) Has the Commission been provided with details of any measures intended to be taken pursuant to Article 3 (1)?
(Yes/No)
(b) If the answer to (a) above is 'No', please state the reasons why.
3. (a) Have measures been taken in fulfilment of the obligation in Article 5 (1) to establish an integrated and adequate network of disposal installations?
(Yes/No)
(b) If the answer to (a) above is 'Yes', please give details.
(c) Please give details of the extend and form of any collaboration which may have taken place with other Member States in fulfilling the obligation in Article 5 (1).
(d) What degree of self-sufficiency in waste disposal has been attained in the Member State? Please illustrate this answer with actual or estimated figures for the waste produced and disposed of within the Member State, out of the total waste produced in the Member State requiring disposal.
4. Pursuant to Article 7 (1), please give the following details, where available, indicating whether any of the figures given is an estimate:5. (a) Have general rules been adopted to provide exemptions pursuant to Article 11?
(Yes/No)
(b) If the answer to (a) above is 'Yes' but the Commission has not been informed of the general rules adopted, please state the reasons why.
6. (a) Are any of the types of establishments or undertakings referred to in Articles 9 and 10 required to keep records, pursuant to Article 14, in standard form?
(Yes/No)
If yes, please give details.
(b) Are producers required to comply with Article 14?
(Yes/No)
If yes, please give details.
Application of Directive 91/692/EEC on harmonizing and rationalizing reports on the implementation of certain Directives relating to the environment
QUESTIONNAIRE
for the report of the Member States on the transposition and implementation of Directive 86/278/EEC on the protection of the environment, and in particular of the soil, when sewage sludge is used in agriculture, amended by Directive 91/692/EEC (7)
There is no need to repeat information which has already been provided.
I. INCORPORATION INTO NATIONAL LAW 1. (a) Has the Commission been provided with details of the current laws and regulations in force to incorporate the Directive as amended into national law?
(Yes/No)
(b) If the answer to (a) above is 'No', please state the reasons why.
2. (a) If national measures have been adopted pursuant to Article 5 to ensure that sewage sludge may not be used in soils with concentrations of one or more heavy metals that exceed the agreed limit values, has the Commission been notified of these measures?
(Yes/No)
(b) If the answer to (a) above is 'No', please state the reasons why.
(c) If national measures have been adopted that are stricter than those provided for in the Directive, has the Commission been notified of these measures pursuant to Article 12?
(Yes/No)
(d) If the answer to (c) above is 'No', please state the reasons why.
II. IMPLEMENTATION OF THE DIRECTIVE 1. Please quote any specific conditions which have been deemed necessary for the protection of human health and the environment in accordance with the first indent of Article 3 (2), when using sludge residues from septic tanks and other similar installations for the treatment of waste water for agricultural purposes.
2. (a) With regard to Article 5, please complete the following table, stating whether any of the information given is an estimate:(b) If the option proposed under Article 5 (2) (a) has been chosen, please indicate the maximum quantity of sludge that may be applied to the soil per surface unit per annum (in tonnes of dry matter per hectare per annum).
(c) If any less stringent limit values for heavy-metal concentrations in soils have been permitted in accordance with Annex I A, footnote 1, please complete the following table, stating whether any of the informations given is an estimate.(d) If any less stringent limit values for heavy-metal concentrations in soils have been permitted in accordance with Annex I A, footnote 2, please complete the following table (the first three columns are not obligatory):(e) If any less stringent limit values for heavy-metal concentrations in soils have been permitted in accordance with Annex I C, footnote 1, please complete the following table, stating whether any of the information given is an estimate.3. (a) With regard to Article 6, please briefly describe the technologies employed for treating sludge.
(b) Have rules been drawn up to ensure that analyses are carried out at more frequent intervals than those provided for in Annex II A (1)?
(Yes/No)
(c) If the answer to (b) above is 'Yes', please give further details.
(d) Have conditions been laid down for authorizing the injection or working into the soil of untreated sludge (Article 6 (a))?
(Yes/No)
(e) If the answer to (d) above is 'Yes', please give further details.
4. With regard to Article 7, please indicate, where appropriate, the length of the period during which it is forbidden to use sludge on grassland before it is grazed, and on forage crops before harvest.
5. (a) Have any reduced limit values or, where appropriate, any other measures, been authorized at national level where the pH of the soil is below 6, as provided for in Article 8?
(Yes/No)
(b) If the answer to (a) above is 'Yes', please complete the following table.6. (a) If appropriate, indicate which types of analysis are carried out, pursuant to Article 9, on soil parameters in accordance with Annex II B (1), other than those mentioned in Annex II B (3) (pH and heavy metals).
(b) State the minimum frequency of soil analysis (Annex II B (2)).
7. On the basis of the data contained in the records referred to in Article 10, complete the following tables, stating whether the information given is an estimate.8. State the number of cases in which exemptions under Article 11 have been granted.
(1) OJ No L 194, 25. 7. 1975, p. 23.
(2) OJ No L 377, 31. 12. 1991, p. 48.
(3) OJ No L 194, 25. 7. 1975, p. 47.
(4) OJ No L 181, 4. 7. 1986, p. 6.
(5) OJ No L 377, 31. 12. 1991, p. 48.
(6) OJ No L 377, 31. 12. 1992, p. 48.
(7) OJ No L 377, 31. 12. 1991. p. 48.
