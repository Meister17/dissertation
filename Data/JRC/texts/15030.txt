Judgment of the Court
(First Chamber)
of 12 January 2006
in Case C-361/04 P: Claude Ruiz-Picasso, Paloma Ruiz-Picasso, Maya Widmaier-Picasso, Marina Ruiz-Picasso, Bernard Ruiz-Picasso v Office for Harmonisation in the Internal Market (Trade Marks and Designs) (OHIM) [1]
In Case C-361/04 P: appeal under Article 56 of the Statute of the Court of Justice lodged on 18 August 2004, by Claude Ruiz-Picasso, residing in Paris (France), Paloma Ruiz-Picasso, residing in London (United Kingdom), Maya Widmaier-Picasso, residing in Paris, Marina Ruiz-Picasso, residing in Geneva (Switzerland), Bernard Ruiz-Picasso, residing in Paris, (represented by C. Gielen, advocaat), the other parties to the proceedings being the Office for Harmonisation in the Internal Market (Trade Marks and Designs) (OHIM) (Agents: G. Schneider and A. von Mühlendahl), defendant at first instance, and DaimlerChrysler AG (represented by S. Völker, Rechtsanwalt), intervener at first instance — the Court (First Chamber), composed of P. Jann, President of the Chamber, K. Schiemann (Rapporteur), N. Colneric, K. Lenaerts and E. Juhász, Judges; D. Ruiz-Jarabo Colomer, Advocate General; K. Sztranc, Administrator, for the Registrar, gave a judgment on 12 January 2006, in which it:
1. Dismisses the appeal;
2. Orders Mr Claude Ruiz-Picasso, Mrs Paloma Ruiz-Picasso, Mrs Maya Widmaier-Picasso, Mrs Marina Ruiz-Picasso and Mr Bernard Ruiz-Picasso to pay the costs.
[1] OJ 262, 23.10.2004.
--------------------------------------------------
