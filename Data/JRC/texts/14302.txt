Judgment of the Civil Service Tribunal (Second Chamber) of 11 July 2006 — Tas v Commission
(Case F-12/05) [1]
Parties
Applicant: David Tas (Brussels, Belgium) (represented by: S. Orlandi, X. Martin, A. Coolen, J.-N. Louis and E. Marchal, lawyers)
Defendant: Commission of the European Communities (represented by: J. Currall and K. Herrmann, Agents)
Re:
Annulment of the Selection Board's decision not to admit the applicant to the tests in competition EPSO/A/4/03, organised to draw up a reserve list of assistant administrators at Grade A 8 in the sectors "European public administration", "Law", "Economy" and "Audit".
Operative part of the judgment
The Tribunal:
1. Dismisses the application;
2. Orders the parties to bear their own costs.
[1] OJ C 132, 28.05.2005 (case initially registered at the Court of First Instance of the European Communities under number T-124/05 and transferred to the Civil Service Tribunal of the European Union by order of 15.12.2005).
--------------------------------------------------
