Decision of the EEA Joint Committee No 78/2004 of 8 June 2004 amending Annex XIV (Competition), Protocol 21 (on the implementation of competition rules applicable to undertakings), Protocol 22 (concerning the definition of «undertaking» and «turnover» (Article 56)) and Protocol 24 (on cooperation in the field of control of concentrations) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as «the Agreement» , and in particular Article 98 thereof,
Whereas:
(1) Annex XIV to the Agreement was amended by the Agreement on the participation of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic in the European Economic Area signed on 14 October 2003 in Luxembourg(1).
(2) Protocol 21 to the Agreement was amended by the Agreement on the participation of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic in the European Economic Area signed on 14 October 2003 in Luxembourg.
(3) Protocol 22 to the Agreement has not previously been amended by the EEA Joint Committee.
(4) Protocol 24 to the Agreement has not previously been amended by the EEA Joint Committee,
(5) Article 57 of the Agreement provides the legal basis for the control of concentrations within the European Economic Area.
(6) Article 57 must be applied in accordance with Protocols 21 and 24 and Annex XIV, laying down the applicable rules on the control of concentrations.
(7) Council Regulation (EEC) No 4064/89 of 21 December 1989 on the control of concentrations between undertakings(2), as last amended by Regulation (EC) No 1310/97(3), is incorporated into Annex XIV and Protocol 21, and referred to in Protocol 24 to the Agreement.
(8) Annex XIV and Protocol 21 were amended by Decision of the EEA Joint Committee No 27/1998 of 27 March 1998 (4), incorporating Council Regulation (EC) No 1310/97 of 30 June 1997 , amending Regulation (EEC) No 4064/89, into the Agreement in line with the objective of maintaining a dynamic and homogenous EEA based on common rules and equal conditions on competition.
(9) Regulation (EC) No 1310/97 amending Regulation (EEC) No 4064/89 amends Article 5(3) of that Regulation. It is appropriate to amend Protocol 22 to the Agreement correspondingly.
(10) Council Regulation (EC) No 139/2004 of 20 January 2004 on the control of concentrations between undertakings (the EC Merger Regulation)(5) repeals and replaces Regulation (EEC) No 4064/89.
(11) Regulation (EC) No 139/2004 should be incorporated into Annex XIV and Protocol 21, and referred to in Protocol 24 to the Agreement in order to maintain equal conditions of competition within the EEA,
HAS DECIDED AS FOLLOWS:
Article 1
Annex XIV to the Agreement shall be amended as specified in Annex I to this Decision.
Article 2
Protocol 21 to the Agreement shall be amended as specified in Annex II to this Decision.
Article 3
Protocol 22 to the Agreement shall be amended as specified in Annex III to this Decision.
Article 4
Protocol 24 to the Agreement shall be replaced as specified in Annex IV to this Decision.
Article 5
The texts of Regulation (EC) No 139/2004 in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union , shall be authentic.
Article 6
This Decision shall enter into force on 9 June 2004 , provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee(6).
Article 7
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union .
Done at Brussels, 8 June 2004 .
For the EEA Joint Committee
The President
S. Gillespie
(1) OJ L 130, 29.4.2004, p. 3.
(2) OJ L 395, 30.12.1989, p. 1.
(3) OJ L 180, 9.7.1997, p. 1.
(4) OJ L 310, 19.11.1998, p. 9 and, EEA Supplement No 48, 19.11.1998 , p. 190.
(5) OJ L 24, 29.1.2004, p. 1.
(6) No constitutional requirements indicated.
ANNEX I
The text of point 1 (Council Regulation (EEC) No 4064/89) in Annex XIV to the Agreement shall be replaced by the following:
«32004 R 0139 : Council Regulation (EC) No 139/2004 of 20 January 2004 on the control of concentrations between undertakings (the EC Merger Regulation) (OJ L 24, 29.1.2004, p. 1).
The provisions of the Regulation shall, for the purposes of the Agreement, be read with the following adaptations:
(a) in Article 1(1), the phrase «or the corresponding provisions in Protocol 21 and Protocol 24 to the EEA Agreement» shall be inserted after the words «Without prejudice to Article 4(5)» ;
furthermore, the term «Community dimension» shall read «Community or EFTA dimension» ;
(b) in Article 1(2), the term «Community dimension» shall read «Community or EFTA dimension respectively» ;
furthermore, the term «Community-wide turnover» shall read «Community-wide turnover or EFTA-wide turnover» ;
in the last subparagraph, the term «Member State» shall read «EC Member State or EFTA State» ;
(c) in Article 1(3), the «Community dimension» shall read «Community or EFTA dimension respectively» ;
furthermore, the term «Community-wide turnover» shall read «Community-wide turnover or EFTA-wide turnover» ;
in Article 1(3)(b) and (c), the term «Member States» shall read «EC Member States or in each of at least three EFTA States» ;
in the last subparagraph, the term «Member State» shall read «EC Member State or EFTA State» ;
(d) Article 1(4) and (5) shall not apply;
(e) in Article 2(1), first subparagraph, the term «common market» shall read «functioning of the EEA Agreement» ;
(f) In Article 2(2), at the end, the term «common market» shall read «functioning of the EEA Agreement» ;
(g) in Article 2(3), at the end, the term «common market» shall read «functioning of the EEA Agreement» ;
(h) in Article 2(4), at the end, the term «common market» shall read «functioning of the EEA Agreement» ;
(i) in Article 3(5)(b), the term «Member State» shall read «EC Member State or EFTA State» ;
(j) in Article 4(1), first subparagraph, the term «Community dimension» shall read «Community or EFTA dimension» ;
furthermore, in the first sentence, the phrase «in accordance with Article 57 of the EEA Agreement» shall be inserted after the words «shall be notified to the Commission» ;
in Article 4(1), second subparagraph, the term «Community dimension» shall read «Community or EFTA dimension» ;
(k) in Article 5(1), the last subparagraph shall read:
«Turnover, in the Community or in an EC Member State, shall comprise products sold and services provided to undertakings or consumers, in the Community or in that EC Member State as the case may be. The same shall apply as regards turnover in the territory of the EFTA States as a whole or in an EFTA State.» ;
(l) in Article 5(3)(a), the last subparagraph shall read:
«The turnover of a credit or financial institution in the Community or in an EC Member State shall comprise the income items, as defined above, which are received by the branch or division of that institution established in the Community or the EC Member State in question as the case may be. The same shall apply as regards turnover of a credit or financial institution in the territory of the EFTA States as a whole or in an EFTA State.» ;
(m) in Article 5(3)(b), the last phrase, «... gross premiums received from Community residents and from residents of one Member State respectively shall be taken into account.» shall read:
«... gross premiums received from Community residents and from residents of one EC Member State respectively shall be taken into account. The same shall apply as regards gross premiums received from residents in the territory of the EFTA States as a whole and from residents in one EFTA State, respectively.»  » .
ANNEX II
The text of point 1(1) (Council Regulation (EEC) No 4064/89) in Article 3 of Protocol 21 to the Agreement shall be replaced by the following:
«32004 R 0139 : Article 4(4) and (5), Articles 6 to 12, Articles 14 to 21 and Articles 23 to 26 of Council Regulation (EC) No 139/2004 of 20 January 2004 on the control of concentrations between undertakings (the EC Merger Regulation) (OJ L 24, 29.1.2004, p. 1).»
ANNEX III
The text of Article 3 in Protocol 22 to the Agreement shall be replaced by the following:
«In place of turnover the following shall be used:
(a) for credit institutions and other financial institutions, the sum of the following income items as defined in Council Directive 86/635/EEC, after deduction of value added tax and other taxes directly related to those items, where appropriate:
(i) interest income and similar income;
(ii) income from securities:
- income from shares and other variable yield securities,
- income from participating interests,
- income from shares in affiliated undertakings;
(iii) commissions receivable;
(iv) net profit on financial operations;
(v) other operating income.
The turnover of a credit or financial institution in the territory covered by the Agreement shall comprise the income items, as defined above, which are received by the branch or division of that institution established in the territory covered by the Agreement;
(b) for insurance undertakings, the value of gross premiums written which shall comprise all amounts received and receivable in respect of insurance contracts issued by or on behalf of the insurance undertakings, including also outgoing reinsurance premiums, and after deduction of taxes and parafiscal contributions or levies charged by reference to the amounts of individual premiums or the total volume of premiums; as regards Article 1(2)(b) and (3)(b), (c) and (d) and the final part of Article 1(2) and (3) of Council Regulation (EC) No 139/2004, gross premiums received from residents in the territory covered by the Agreement shall be taken into account.»
ANNEX IV
Protocol 24 to the Agreement shall be replaced by the following:
PROTOCOL 24 on cooperation in the field of control of concentrations
GENERAL PRINCIPLES
Article 1
1.The EFTA Surveillance Authority and the EC Commission shall exchange information and consult each other on general policy issues at the request of either of the surveillance authorities.
2.In cases falling under Article 57(2)(a) of the Agreement, the EC Commission and the EFTA Surveillance Authority shall cooperate in the handling of concentrations as provided for in the provisions set out below.
3.For the purposes of this Protocol, the term «territory of a surveillance authority» shall mean for the EC Commission the territory of the EC Member States to which the Treaty establishing the European Community applies, upon the terms laid down in that Treaty, and for the EFTA Surveillance Authority the territories of the EFTA States to which the Agreement applies.
Article 2
1.Cooperation shall take place, in accordance with the provisions set out in this Protocol, where:
(a) the combined turnover of the undertakings concerned in the territory of the EFTA States equals 25 % or more of their total turnover within the territory covered by the Agreement; or
(b) each of at least two of the undertakings concerned has a turnover exceeding EUR 250 million in the territory of the EFTA States; or
(c) the concentration is liable to significantly impede effective competition, in the territories of the EFTA States or a substantial part thereof, in particular as a result of the creation or strengthening of a dominant position.
2.Cooperation shall also take place where:
(a) the concentration fulfils the criteria for referral pursuant to Article 6.
(b) an EFTA State wishes to adopt measures to protect legitimate interests as set out in Article 7.
INITIAL PHASE OF THE PROCEEDINGS
Article 3
1.The EC Commission shall transmit to the EFTA Surveillance Authority copies of notifications of the cases referred to in Article 2(1) and (2)(a) within three working days and, as soon as possible, copies of the most important documents lodged with or issued by the EC Commission.
2.The EC Commission shall carry out the procedures set out for the implementation of Article 57 of the Agreement in close and constant liaison with the EFTA Surveillance Authority. The EFTA Surveillance Authority and EFTA States may express their views upon those procedures. For the purposes of Article 6(1) of this Protocol, the EC Commission shall obtain information from the competent authority of the EFTA State concerned and give it the opportunity to make known its views at every stage of the procedures up to the adoption of a decision pursuant to that Article. To that end, the EC Commission shall give it access to the file.
Documents to be transmitted from the Commission to an EFTA State and from an EFTA State to the Commission pursuant to this Protocol shall be submitted via the EFTA Surveillance Authority.
HEARINGS
Article 4
In cases referred to in Article 2(1) and (2)(a), the EC Commission shall invite the EFTA Surveillance Authority to be represented at the hearings of the undertakings concerned. The EFTA States may likewise be represented at those hearings.
THE EC ADVISORY COMMITTEE ON CONCENTRATIONS
Article 5
1.In cases referred to in Article 2(1) and (2)(a), the EC Commission shall in due time inform the EFTA Surveillance Authority of the date of the meeting of the EC Advisory Committee on Concentrations and transmit the relevant documentation.
2.All documents forwarded for that purpose from the EFTA Surveillance Authority, including documents emanating from EFTA States, shall be presented to the EC Advisory Committee on Concentrations together with the other relevant documentation sent out by the EC Commission.
3.The EFTA Surveillance Authority and the EFTA States shall be entitled to be present in the EC Advisory Committee on Concentrations and to express their views therein; they shall not have, however, the right to vote.
RIGHTS OF INDIVIDUAL STATES
Article 6
1.The EC Commission may, by means of a decision notified without delay to the undertakings concerned, to the competent authorities of the EC Member States and to the EFTA Surveillance Authority, refer a notified concentration, in whole or in part, to an EFTA State where:
(a) a concentration threatens to affect significantly competition in a market within that EFTA State, which presents all the characteristics of a distinct market, or
(b) a concentration affects competition in a market within that EFTA State, which presents all the characteristics of a distinct market and which does not constitute a substantial part of the territory covered by the Agreement.
2.In cases referred to in paragraph 1, any EFTA State may appeal to the Court of Justice of the European Communities, on the same grounds and conditions as an EC Member State under Articles 230 and 243 of the Treaty establishing the European Community, and in particular request the application of interim measures, for the purpose of applying its national competition law.
3.(No text)
4.Prior to the notification of a concentration within the meaning of Article 4(1) of Regulation (EC) No 139/2004 the persons or undertakings referred to in Article 4(2) of Regulation (EC) No 139/2004 may inform the EC Commission, by means of a reasoned submission, that the concentration may significantly affect competition in a market within an EFTA State which presents all the characteristics of a distinct market and should therefore be examined, in whole or in part, by that EFTA State.
The EC Commission shall transmit all submissions pursuant to Article 4(4) of Regulation (EC) No 139/2004 and this paragraph to the EFTA Surveillance Authority without delay.
5.With regard to a concentration as defined in Article 3 of Regulation (EC) No 139/2004 which does not have a Community dimension within the meaning of Article 1 of that Regulation and which is capable of being reviewed under the national competition laws of at least three EC Member States and at least one EFTA State, the persons or undertakings referred to in Article 4(2) of that Regulation may, before any notification to the competent authorities, inform the EC Commission by means of a reasoned submission that the concentration should be examined by the Commission.
The EC Commission shall transmit all submissions pursuant to Article 4(5) of Regulation (EC) No 139/2004 to the EFTA Surveillance Authority without delay.
Where at least one such EFTA State has expressed its disagreement as regards the request to refer the case, the competent EFTA State(s) shall retain their competence, and the case shall not be referred from the EFTA States pursuant to this paragraph.
Article 7
1.Notwithstanding the sole competence of the EC Commission to deal with concentrations of a Community dimension as set out in Council Regulation (EC) No 139/2004, EFTA States may take appropriate measures to protect legitimate interests other than those taken into consideration according to the above Regulation and compatible with the general principles and other provisions as provided for, directly or indirectly, under the Agreement.
2.Public security, plurality of media and prudential rules shall be regarded as legitimate interests within the meaning of paragraph 1.
3.Any other public interest must be communicated to the EC Commission and shall be recognised by the EC Commission after an assessment of its compatibility with the general principles and other provisions as provided for, directly or indirectly, under the Agreement before the measures referred to above may be taken. The EC Commission shall inform the EFTA Surveillance Authority and the EFTA State concerned of its decision within 25 working days of that communication.
ADMINISTRATIVE ASSISTANCE
Article 8
1.When the EC Commission requires by decision a person, an undertaking or an association of undertakings located within the territory of the EFTA Surveillance Authority to supply information, it shall without delay forward a copy of the decision to the EFTA Surveillance Authority. At the specific request of the EFTA Surveillance Authority, the EC Commission shall also forward to the EFTA Surveillance Authority copies of simple requests for information relating to a notified concentration.
2.At the request of the EC Commission, the EFTA Surveillance Authority and the EFTA States shall provide the EC Commission with all necessary information to carry out the duties assigned to it by Article 57 of the Agreement.
3.When the EC Commission interviews a consenting natural or legal person in the territory of the EFTA Surveillance Authority, the EFTA Surveillance Authority shall be informed in advance thereof. The EFTA Surveillance Authority may be present during the interview, as well as officials from the competition authority on whose territory the interviews are conducted.
4.(No text)
5.(No text)
6.(No text)
7.Where the EC Commission carries out investigations within the territory of the Community, it shall, as regards cases falling under Article 2(1) and (2)(a), inform the EFTA Surveillance Authority of the fact that such investigations have taken place and on request transmit in an appropriate way the relevant results of the investigations.
PROFESSIONAL SECRECY
Article 9
1.Information acquired as a result of the application of this Protocol shall be used only for the purpose of procedures under Article 57 of the Agreement.
2.The EC Commission, the EFTA Surveillance Authority, the competent authorities of the EC Member States and of the EFTA States, their officials and other servants and other persons working under the supervision of these authorities as well as officials and civil servants of other authorities of the Member States and of the EFTA States shall not disclose information acquired by them as a result of the application of this Protocol and of the kind covered by the obligation of professional secrecy.
3.Rules on professional secrecy and restricted use of information provided for in the Agreement or the legislation of the Contracting Parties shall not prevent the exchange and use of information as set out in this Protocol.
NOTIFICATIONS
Article 10
1.Undertakings shall address their notifications to the competent surveillance authority in accordance with Article 57(2) of the Agreement.
2.Notifications or complaints addressed to the authority which, pursuant to Article 57 of the Agreement, is not competent to take decisions on a given case shall be transferred without delay to the competent surveillance authority.
Article 11
The date of submission of a notification shall be the date on which it is received by the competent surveillance authority.
LANGUAGES
Article 12
1.Undertakings shall be entitled to address and be addressed by the EFTA Surveillance Authority and the EC Commission in an official language of an EFTA State or the Community which they choose as regards notifications. This shall also cover all instances of a proceeding.
2.If undertakings choose to address a surveillance authority in a language which is not one of the official languages of the States falling within the competence of that authority, or a working language of that authority, they shall simultaneously supplement all documentation with a translation into an official language of that authority.
3.As far as undertakings are concerned which are not parties to the notification, they shall likewise be entitled to be addressed by the EFTA Surveillance Authority and the EC Commission in an appropriate official language of an EFTA State or of the Community or in a working language of one of those authorities. If they choose to address a surveillance authority in a language which is not one of the official languages of the States falling within the competence of that authority, or a working language of that authority, paragraph 2 shall apply.
4.The language which is chosen for the translation shall determine the language in which the undertakings may be addressed by the competent authority.
TIME LIMITS AND OTHER PROCEDURAL QUESTIONS
Article 13
As regards time limits and other procedural provisions, including the procedures for referral of a concentration between the EC Commission and one or more EFTA States, the rules implementing Article 57 of the Agreement shall apply also for the purpose of the cooperation between the EC Commission and the EFTA Surveillance Authority and EFTA States, unless otherwise provided for in this Protocol.
The calculation of the time limits referred to in Article 4(4) and (5) and Article 9(2) and (6) of Regulation (EC) No 139/2004 shall start, for the EFTA Surveillance Authority and the EFTA States, upon receipt of the relevant documents by the EFTA Surveillance Authority.
TRANSITION RULE
Article 14
Article 57 of the Agreement shall not apply to any concentration which was the subject of an agreement or announcement or where control was acquired before the date of entry into force of the Agreement. It shall not in any circumstances apply to a concentration in respect of which proceedings were initiated before that date by a national authority with responsibility for competition.
