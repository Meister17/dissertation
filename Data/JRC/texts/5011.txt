Commission Regulation (EC) No 1750/2005
of 25 October 2005
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 26 October 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 25 October 2005.
For the Commission
J. M. Silva Rodríguez
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 386/2005 (OJ L 62, 9.3.2005, p. 3).
--------------------------------------------------
ANNEX
to Commission Regulation of 25 October 2005 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code | Standard import value |
07020000 | 052 | 46,7 |
096 | 24,7 |
204 | 39,7 |
624 | 421,2 |
999 | 133,1 |
07070005 | 052 | 92,6 |
999 | 92,6 |
07099070 | 052 | 86,0 |
204 | 45,3 |
999 | 65,7 |
08055010 | 052 | 65,2 |
388 | 65,1 |
524 | 66,9 |
528 | 70,1 |
999 | 66,8 |
08061010 | 052 | 100,1 |
400 | 283,5 |
508 | 230,2 |
512 | 92,7 |
999 | 176,6 |
08081080 | 052 | 57,2 |
388 | 79,9 |
400 | 100,2 |
404 | 84,6 |
512 | 75,8 |
720 | 54,4 |
800 | 161,3 |
804 | 83,1 |
999 | 87,1 |
08082050 | 052 | 95,2 |
388 | 57,1 |
720 | 64,0 |
999 | 72,1 |
[1] Country nomenclature as fixed by Commission Regulation (EC) No 750/2005 (OJ L 126, 19.5.2005, p. 12). Code "999" stands for "of other origin".
--------------------------------------------------
