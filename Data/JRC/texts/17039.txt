Information relating to the entry into force of the Agreement between the European Community and the Republic of India on customs cooperation and mutual administrative assistance in customs matters
The Agreement between the European Community and the Republic of India on customs cooperation and mutual administrative assistance in customs matters [1], entered into force on 1 November 2004, the procedures provided for in Article 22 of the Agreement having been completed on 29 October 2004.
--------------------------------------------------
[1] OJ L 304, 30.9.2004, p. 25.
