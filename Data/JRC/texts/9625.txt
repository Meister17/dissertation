Imposition of public service obligations imposed in respect of scheduled air services on routes within Greece in accordance with Council Regulation (EEC) No 2408/92
(2006/C 260/05)
(Text with EEA relevance)
1. Pursuant to Article 4(1)(a) of Regulation (EEC) No 2408/92 the Greek Government has decided to impose a public service obligation in respect of scheduled services on the following routes:
- Athens — Kalimnos
- Thessaloniki — Kalamata
- Thessaloniki — Limnos — Ikaria
2. On these routes, the public service obligation is as follows:
(A) Minimum frequency of flights and minimum number of seats available each week on each route:
Athens — Kalimnos
- Six return flights a week and a total of 120 seats a week in each direction throughout the winter;
- Eight return flights a week and a total of 160 seats a week in each direction throughout the summer;
Thessaloniki — Kalamata
- Two return flights a week and a total of 30 seats a week in each direction throughout the winter;
- Three return flights a week and a total of 45 seats a week in each direction throughout the summer;
Thessaloniki — Limnos — Ikaria
- Two return flights a week and a total of 40 seats a week in each direction throughout the winter;
- Three return flights a week and a total of 60 seats a week in each direction throughout the summer;
The winter and summer periods are as determined by IATA.
If the average occupancy rate of all the flights on one route has exceeded 75 % during the previous period, the minimum weekly frequency or the minimum number of seats provided each week may be increased in proportion to the increase recorded. This increase shall be notified to the carrier operating the service by registered letter six months before it applies, and shall not come into effect until it has been published by the European Commission in the Official Journal of the European Union.
Where the aircraft used do not have the capacity required to cover the minimum weekly seat availability (laid down in paragraph 2(A)), the flight frequencies may be increased accordingly.
If flights are cancelled because of weather conditions, they must be operated on the days immediately afterwards in order to fulfil the weekly requirement, taking into account the minimum number of seats provided pursuant to paragraph 2(A).
(B) Fares
The single economy fare may not exceed the following rates:
Athens–Kalimnos: : 60 euros
Thessaloniki—Kalamata: : 70 euros
Thessaloniki—Limnos: : 60 euros
Thessaloniki—Ikaria: : 70 euros
Limnos—Ikaria: : 40 euros
The above amounts may be increased in the event of an unforeseen increase in the costs of operating the service outside the control of the carrier. This increase shall be notified to the carrier operating the service but shall not come into effect until it has been published by the European Commission in the Official Journal of the European Union.
(C) Minimum frequency of flights
In accordance with Article 4(1)(c) of Regulation (EEC) No 2408/92, a carrier intending to operate scheduled services on these routes must give a guarantee that it will operate the route for at least twelve months without interruption.
The number of flights cancelled for reasons for which the carrier is responsible may not exceed 2 % of the total annual number of flights, except in cases of force majeure.
Any intention to cease operation of any of the aforementioned routes must be notified by the air carrier to the Civil Aviation Department, Directorate for Air Operations, Section for Bilateral Air Agreements, at least six months before services are interrupted.
3. Useful information
Operation of flights by a Community carrier who does not fulfil the public service obligations imposed may result in the imposition of administrative or other sanctions.
As regards the types of aircraft to be used, air carriers are asked to consult Aeronautical Information Publications of Greece (AIP Greece) as regards technical and business data and airport procedures.
As regards timetables, arrivals and departures of aircraft must be planned within the operating times of airports as determined by decision of the Minister for Transport and Communications.
If no air carrier has declared to the Civil Aviation Authority, Directorate for Air Operations, an intention to operate scheduled services from 1 May 2007 on one or more of the abovementioned routes without financial compensation, Greece has decided, under the procedure set out in Article 4(1)(d) of Regulation (EEC) No 2408/92, to limit access to each or several of the abovementioned routes to only one carrier for three years and to grant the right to operate those services from 1 May 2007 following an invitation to tender.
--------------------------------------------------
