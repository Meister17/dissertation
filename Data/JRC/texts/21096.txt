Commission Regulation (EC) No 980/2002
of 4 June 2002
amending Regulation (EC) No 2082/2000, adopting Eurocontrol standards
(Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 93/65/EEC of 19 July 1993, on the definition and use of compatible technical specifications for the procurement of air traffic management equipment and systems(1), as last amended by Commission Directive 97/15/EC(2), and in particular Article 3 thereof,
Whereas:
(1) Directive 97/15/EC adopting Eurocontrol standards and amending Directive 93/65/EEC on the definition and use of compatible technical specification for the procurement of air traffic management equipment and systems, as amended by Regulation (EC) No 2082/2000(3), adopted the Eurocontrol standard for On-Line Data Interchange (OLDI), edition 1.0 and the Eurocontrol standard for Air Traffic Services Data Exchange Presentation (ADEXP), edition 1.0.
(2) Regulation (EC) No 2082/2000 adopted two more recent versions of those two Eurocontrol standards, namely OLDI edition 2.2 and ADEXP edition 2.0, as well as a new Eurocontrol standard named Flight Data Exchange - Interface Control Document (FDE-ICD).
(3) Eurocontrol has since adopted amendments to OLDI edition 2.2 and ADEXP edition 2.0.
(4) These amendments to the Eurocontrol standards fall within the scope of Directive 93/65/EEC and contribute to the harmonisation of Member States' air traffic management national systems, particularly concerning the transfer of flights between air traffic control centres (OLDI) and the air traffic flow management (ADEXP).
(5) These amendments include in particular an indication of equipment capabilities of the flight and are necessary to facilitate smooth and safe introduction of several programs aiming at capacity increases.
(6) Regulation (EC) No 2082/2000 should therefore be amended accordingly.
(7) The measures provided for in this Regulation are in accordance with the opinion of the Committee instituted by Article 6(1) of Directive 93/65/EEC,
HAS ADOPTED THIS REGULATION:
Article 1
Annexes I and II to Regulation (EC) No 2082/2000 are amended in accordance with the Annex of this Regulation:
Article 2
This Regulation shall enter into force on the seventh day following that of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 4 June 2002.
For the Commission
Loyola De Palacio
Vice-President
(1) OJ L 187, 29.7.1993, p. 52.
(2) OJ L 95, 10.4.1997, p. 16.
(3) OJ L 254, 9.10.2000, p. 1.
ANNEX
Annexes I and II are amended as follows:
1. Annex I is amended as follows:
(a) Point 6.2.2 is replaced by the following: "6.2.2. Message contents
The ABI message shall contain the following items of data:
- message type,
- message number,
- aircraft identification,
- SSR mode and code (if available),
- departure aerodrome,
- estimate data,
- destination aerodrome,
- aircraft number and type,
- type of flight,
- equipment capability and status,
- route (optional),
- other flight plan data (optional).
NOTE:
Data insertion rules, formats and field contents are specified at Annex A."
(b) Points 6.2.5.1 and 6.2.5.2 are replaced by the following: "6.2.5.1. I C A O
(ABIE/L001-AMM253/A7012-LMML-BNE/1221F350-EGBB-9/B757/M-15/N0480F390 UB4 BNE UB4 BPK UB3 HON-80/N-81/W/EQ Y/NO)
6.2.5.2. A D E X P
TITLE ABI -REFDATA -SENDER -FAC E -RECVR -FAC L -SEQNUM 001 -ARCID AMM253 -SSRCODE A7012 - ADEP LMML -COORDATA -PTID BNE -TO 1221 -TFL F350 -ADES EGBB -ARCTYP B757 -FLTTYP N -BEGIN EQCST -EQPT W/EQ -EQPT Y/NO -END EQCST-ROUTE N0480F390 UB4 BNE UB4 BPK UB3 HON)".
(c) Point 6.3.2 is replaced by the following: "6.3.2. Message contents
The ACT message shall contain the following items of data:
- message type,
- message number,
- aircraft identification,
- SSR mode and code,
- departure aerodrome,
- estimate data,
- destination aerodrome,
- aircraft number and type,
- type of flight,
- equipment capability and status,
- route (optional),
- other flight plan data (optional).
NOTE:
Data insertion rules, formats and field contents are specified at Annex A."
(d) Points 6.3.5.1 and 6.3.5.2 are replaced by the following: "6.3.5.1. I C A O
(ACTE/L005-AMM253/A7012-LMML-BNE/1226F350-EGBB-9/B757/M-15/N0480F390 UB4 BNE UB4 BPK UB3 HON-80/N-81/W/EQ Y/NO)
6.3.5.2. A D E X P
-TITLE ACT -REFDATA -SENDER -FAC E -RECVR -FAC L -SEQNUM 005 -ARCID AMM253 -SSRCODE A7012 -ADEP LMML -COORDATA -PTID BNE -TO 1226 -TFL F350 -ADES EGBB -ARCTYP B757 -FLTTYP N-BEGIN EQCST-EQPT W/EQ -EQPT Y/NO -END EQCST-ROUTE N0480F390 UB4 BNE UB4 BPK UB3 HON".
(e) Point 7.3.2. is replaced by the following: "7.3.2. Message contents
The REV message shall contain the following items of data:
- message type,
- message number,
- aircraft identification,
- departure aerodrome,
- estimate data and/or coordination point,
- destination aerodrome,
- message reference (optional),
- SSR mode and code (optional),
- route (optional),
- equipment capability and status (optional).
NOTE:
Data insertion rules, formats and field contents are specified at Annex A."
(f) Points 7.3.3.1.2., 7.3.3.1.3. and 7.3.3.1.4. are replaced by the following: "7.3.3.1.2. The following elements shall be subject to revisions:
- ETO at the COP,
- transfer level(s),
- SSR code,
- equipment capability and status.
7.3.3.1.3. A REV message shall be sent when:
- the ETO at the COP differs from that in the previous message by more than a value bilaterally agreed, rounded to the nearest integer value,
- there is any change to the transfer level(s), SSR code or equipment capability and status.
7.3.3.1.4. Where bilaterally agreed, a REV message shall be sent when there is any change in the following:
- COP,
- route.
NOTE:
Operational rules may require that modifications effected after ACT be subject to prior coordination between the units concerned."
(g) Points 7.3.3.2.1. and 7.3.3.2.2. are replaced by the following: "7.3.3.2.1. I C A O format
All revision messages include field types 3, 7, 13, 14 and 16. The following rules apply:
- a change to the ETO at the COP or transfer level(s) shall be incorporated by the inclusion of the revised data in field 14,
- a change to the SSR code shall be included as elements (b) and (c) of field 7,
All other modifications are included in field 22 format after the initial five fields. The following rules apply:
- fields included in field 22 format can be in any order relative to each other,
- changes to the COP shall be incorporated as field 14 data in field 22 format (see Appendix B - Special route processing requirements),
- route changes shall be incorporated as field 15 data in field 22 format. Rules for the coordination of such changes, including direct routings, are specified in Annex B Special Route Processing Requirements,
- equipment Capability and Status; only the capability or capabilities being changed shall be included. A message changing a capability that results in the need to include data concerning an additional capability as specified in Annex A paragraph A.30 shall include the additional capability.
7.3.3.2.2. A D E X P format
All revision messages in ADEXP format shall include the following primary fields: TITLE REFDATA ARCID ADEP ADES. The following rules apply:
- a change to the ETO at the COP or transfer level(s) shall be incorporated by the inclusion of the revised data in primary field COORDATA,
- the primary field COP shall be included unless a change to the ETA or transfer level(s) requires the use of primary field COORDATA. It shall contain the COP through which the flight is currently co-ordinated or, if the COP is being amended, the COP through which the flight had previously been coordinated,
- changes to the COP shall be incorporated using primary field COORDATA (see Appendix B - Special route processing requirements). Such messages shall also include primary field COP as stated above,
- changes to the route shall be incorporated using primary field ROUTE. Rules for the coordination of such changes, including direct routings, are specified in Annex B - Special route processing requirements,
- a change to the SSR Code shall be indicated by the inclusion of primary field SSRCODE,
- a change to equipment capability and status shall utilise primary field EQCST; only the capability or capabilities being changed shall be included. A message changing a capability that results in the need to include data concerning an additional capability as specified in Annex A paragraph A.30 shall include the additional capability."
(h) Points 7.3.5.1 and 7.3.5.2 are replaced by the following: "7.3.5.1. I C A O
(a) (REVE/L002-AMM253-LMML-BNE/1226F310-EGBB)
(b) (REVE/L010-AMM253/A2317-LMML-BNE/1226F310-EGBB)
(c) (REVE/L019-AMM253-LMML-BNE/1237F350-EGBB-81/W/NO)
(d) (REVBC/P873-BAF4486-EBMB-NEBUL/2201F250-LERT-81/W/NO U/EQ)
7.3.5.2. A D E X P
(a) -TITLE REV -REFDATA -SENDER -FAC E -RECVR -FAC L -SEQNUM 002 -ARCID AMM253 -ADEP LMML -COORDATA -PTID BNE -TO 1226 -TFL F310 -ADES EGBB
(b) -TITLE REV -REFDATA -SENDER -FAC E -RECVR -FAC L -SEQNUM 010 -ARCID AMM253 -ADEP LMML -COP BNE -ADES EGBB -SSRCODE A2317
(c) -TITLE REV -REFDATA -SENDER -FAC E -RECVR -FAC L -SEQNUM 019 -ARCID AMM253 -ADEP LMML -COP BNE -ADES EGBB -BEGIN EQCST -EQPT W/NO -END EQCST
(d) -TITLE REV -REFDATA -SENDER -FAC BC -RECVR -FAC P -SEQNUM 873 -ARCID BAF4486 -ADEP EBMB -COP NEBUL -ADES LERT -BEGIN EQCST -EQPT Y/NO -EQPT U/EQ -END EQCST".
(i) Annex A is amended as follows:
- In the list of contents the following points A.29 and A.30 are added: "A.29. Type of flight
A.30. Equipment capability and status"
- The following point A.2.2.a is inserted: "A.2.2.a In some instances a pseudo ICAO field type number is used where a suitable ICAO field does not exist. Such numbers contain two digit values equal to or greater than 80."
- Point A.14. is replaced by the following: "A.14. This field allows the option of including in specified messages flight plan data not normally included as part of the coordination procedure and not described elsewhere in this Appendix. The inclusion of the following items as described in Reference 1, Appendix 2, Field types 8 and 18 are permitted:
- flight rules,
- registration markings,
- name of the operator,
- reason for special handling by ATS,
- type,
- performance,
- name of departure, destination, alternate aerodromes
- plain language remarks.
A.14.1. I C A O
Field type 8, element (a) Flight rules in field type 22 format.
One or more of the following field type 18 elements in field type 22 format:
REG, OPR, STS, TYP, PER, DEP, DEST, ALTN, RALT, RMK.
A.14.2. ADEXP
Primary fields: "fltrul", "depz", "destz", "opr", "per", "reg", "rmk", "altrnt1", "altrnt2", "sts", and "typz"."
- The following points A.29. and A.30. are added: "A.29. Type of flight
This item shall be as filed in the flight plan or equivalent data originating from an alternative source. The letter X shall be inserted if the type of flight is omitted in the flight plan or it is not known for any other reason.
A.29.1. I C A O
The type of flight shall be inserted as a single letter in field type 22 format utilising the pseudo field type number 80.
A.29.2. A D E X P
Primary field "flttyp".
A.30. Equipment capability and status
This item indicates the capability and status of equipment that is either a pre-requisite for flight in certain airspaces or on specified routes or has a significant effect on the provision of ATC service. The presence of a capability is identified in the flight plan but may be found to be incorrect or to have changed during the flight. The equipment capability and status specifies the current status.
The status of the following items shall be included:
- RVSM capability,
- 8.33 kHz RTF equipage.
The status of the following shall be included for State flights as specified in the type of flight in the flight plan for which 8.33 kHz RTF equipment is not known to be available for use:
- UHF equipage.
A.30.1. I C A O
The data shall be inserted in field type 22 format utilising the pseudo field number 81.
Two elements shall be inserted for each capability:
- the equipment capability expressed as a single letter as specified in field type 10 "Equipment" in the ICAO flight plan (see Appendix 3 of Reference 1), followed immediately by,
- element separator - oblique stroke (/), followed immediately by,
- status expressed as two letters.
The status shall be expressed by the use of following indicators as applicable to the flight:
(a) EQ meaning the flight is equipped and the equipment is available for use by the flight;
(b) NO meaning the flight is not equipped or for any reason the equipment cannot be used by the flight;
(c) UN meaning compliance with the capability is unknown.
The first capability group shall be inserted directly after the oblique stroke following the field number. Subsequent groups shall be separated by a space character. The order of equipment capabilities is not significant.
A.30.2. A D E X P
Primary field "EQCST""
2. In Annex II Annex A is amended as follows:
(a) In point A.2. the following entries are added: >TABLE>
(b) In point A.3. the following entry is added: >TABLE>
(c) In point A.4. the following entry is added: >TABLE>
