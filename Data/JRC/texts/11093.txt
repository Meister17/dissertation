Judgment of the Court of First Instance of 13 July 2006 –Andrieu
v Commission
(Case T-285/04) [1]
Parties
Applicant: Michel Andrieu (Brussels, Belgium) (represented by: S. Rodrigues and Y. Minatchy, lawyers)
Defendant: Commission of the European Communities (represented by: G. Berscheid and L. Lozano Palacios, acting as Agents, and M. Genton, lawyer)
Re:
Firstly, action for annulment of the applicant's career development report for 2001/2002 and, secondly, action for damages
Operative part of the judgment
The Court:
1. Dismisses the action;
2. Orders each party to bear its own costs.
[1] OJ C 262, 23.10.2004.
--------------------------------------------------
