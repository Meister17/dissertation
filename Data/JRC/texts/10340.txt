Authorisation for State aid pursuant to Articles 87 and 88 of the EC Treaty
Cases where the Commission raises no objections
(2006/C 248/09)
(Text with EEA relevance)
Date of adoption of the decision | 25.8.2006 |
Reference number of the aid | N 235/06 |
Member State | Finland |
Region | Åland Islands |
Title | Investeringsstöd till Mariehamns Bioenergi Ab |
Legal basis | Budget för landskapet Åland 2006 / Ahvenanmaan maakuntahallituksen talousarvio 2006 |
Type of measure | Aid scheme |
Objective | Environmental protection (energy) |
Form of aid | Direct grant |
Budget | Annual expenditure planned: -; Overall aid amount granted: EUR 2,63 millions |
Intensity | 40% |
Name and address of the granting authority | Ålands landskapsstyrelse PB 1060 FIN-22111 Mariehamn |
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption of the decision | 8.2.2006 |
Reference number of the aid | N 254/05 |
Member State | Portugal |
Title | Auxilío à formação à Blaupunkt Auto-Rádio — Portugal Lda |
Legal basis | Portaria n.o 1285/2003, DR I-Série B, n.o 266, of 17 November 2003 |
Type of measure | Individual aid |
Objective | Training |
Form of aid | Direct grant |
Budget | Annual budget: -; Overall budget: EUR 2910072,50 |
Duration | 2 years |
Economic sectors | Electrical and optical equipment |
Name and address of the granting authority | API — Agência Portuguesa para o Investimento E.P.E. Ed. Península 7.a Praça do Bom Sucesso 127/131 7.a Sala 702 P-4150-146 Porto |
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption of the decision | 22.8.2006 |
Reference number of the aid | N 295/06 |
Member State | Spain |
Region | Madrid |
Title | Extensión de la ayuda N 121/2005 — Ayudas a la innovación tecnológica en el sector de la Biotecnología de la Comunidad de Madrid |
Legal basis | Orden 84/2006 de 12 de enero de la Consejería de Economía e Innovación Tecnológica, por la que se aprueban las bases reguladores y se convocan ayudas cofinanciadas por el Fondo Europeo de Desarrollo Regional para el fomento de la innovación en el sector de la Biotecnología de la Comunidad de Madrid |
Type of measure | Aid scheme |
Objective | Research and development |
Form of aid | Direct grant |
Budget | Annual budget: -; Overall budget: EUR 48 million |
Intensity | 75% |
Duration | 1 January 2005 — 31 October 2011 |
Economic sectors | Chemical and pharmaceutical industry |
Name and address of the granting authority | Dirección General de Innovación Tecnológica Comunidad de Madrid Calle Cardenal Marcelo Spínola E-14 28016 Madrid |
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
Date of adoption of the decision | 15.9.2006 |
Reference number of the aid | N 519/06 |
Member State | United Kingdom |
Title | Climate Change Levy: eligibility of further sectors to Climate change agreements cold storage and glass manufacture |
Legal basis | Finance Act 2000 |
Objective | Two further sectors/associations concluded Climate Change Agreement with the UK government. The achievement of the energy efficiency targets foreseen by the agreements entitles the concerned sectors/companies to a reduction under the Climate Change Levy |
Budget | No change in the budget foreseen for all Climate change agreements |
Duration | Until 31 March 2011 |
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://ec.europa.eu/community_law/state_aids/
--------------------------------------------------
