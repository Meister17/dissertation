COUNCIL DIRECTIVE 93/14/EEC of 5 April 1993 on the braking of two or three-wheel motor vehicles
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 100a thereof,
Having regard to Council Directive 92/61/EEC of 30 June 1992 relating to the type-approval of two or three-wheel motor vehicles(1) ,
Having regard to the proposal from the Commission(2) ,
In cooperation with the European Parliament(3) ,
Having regard to the opinion of the Economic and Social Committee(4) ,
Whereas measures should be adopted which are intended gradually to establish the internal market within a period expiring on 31 December 1992; whereas the internal market comprises an area without internal frontiers in which the free movement of goods, persons, services and capital is ensured;
Whereas, with regard to braking, in each Member State two or three-wheel vehicles must display certain technical characteristics laid down by mandatory provisions which differ from one Member State to another; whereas, as a result of their differences, such provisions constitute barriers to trade within the Community;
Whereas these barriers to the establishment and operation of the internal market may be removed if the same requirements are adopted by all the Member States in place of their national rules;
Whereas it is necessary to draw up harmonized requirements concerning the braking of two or three-wheel motor vehicles in order to enable the type-approval and component type-approval procedures laid down in Directive 92/61/EEC to be applied for each type of such vehicle;
Whereas in order to facilitate access to the markets of non-Community countries it is necessary to establish equivalence between the requirements of this Directive and those of United Nations ECE Regulation No 78,
HAS ADOPTED THIS DIRECTIVE:
Article 1
This Directive applies to the braking of all types of vehicle as defined in Article 1 of Directive 92/61/EEC.
Article 2
The procedure for the granting of component type-approval in respect of the braking of a type of two or three-wheel motor vehicle and the conditions governing the free movement of such vehicles shall be as laid down in Chapters II and III of Directive 92/61/EEC.
Article 3
In accordance with Article 11 of Directive 92/61/EEC, equivalence between the requirements laid down in this Directive and those laid down in United Nations ECE Regulation No 78 (E/ECE/324 and E/ECE(TRANS/505 REV 1 ADD 77 of 20 October 1988) is hereby acknowledged.
The authorities of the Member States which grant component type-approval shall accept approvals granted in accordance with the requirements of the abovementioned Regulation No 78 as well as component type-approval marks as an alternative to the corresponding approvals and component type-approval marks granted in accordance with this Directive.
Article 4
This Directive may be amended in accordance with Article 13 of Directive 70/156/EEC(5) in order to:
- take into account any amendments to the ECE Regulation referred to in Article 3,
- adapt the Annex to technical progress.
Article 5
1. Member States shall adopt and publish the provisions necessary to comply with this Directive before 5 October 1994 and shall forthwith inform the Commission thereof.
When the Member States adopt these provisions, they shall contain a reference to this Directive or shall be accompanied by such a reference on the occasion of their official publication. The methods of making such a reference shall be laid down by the Member States.
From the date mentioned in the first subparagraph Member States may not, for reasons connected with braking, prohibit the initial entry into service of vehicles which conform to this Directive.
They shall apply the provisions referred to in the first suparagraph as from 5 April 1995.
2. Member States shall communicate to the Commission the texts of the provisions of national law which they adopt in the field covered by this Directive.
Article 6
This Directive is addressed to the Member States.
Done at Luxembourg, 5 April 1993.
For the Council The President J. TROEJBORG
(1) OJ No L 225, 10. 8. 1992, p. 72.
(2) OJ No C 93, 13. 4. 1992, p. 24.
(3) OJ No C 305, 23. 11. 1992, p. 114; and OJ No C 72, 15. 3. 1993.
(4) OJ No C 313, 30. 11. 1992, p. 7.
(5) OJ No L 42, 23. 2. 1970, p. 1. Directive as last amended by Directive 92/53/EEC (OJ No L 225, 10. 8. 1992, p. 1).
ANNEX
1. DEFINITIONS
For the purposes of this Directive:
1.1. Type of vehicle with respect to its braking:
means vehicles which do not differ in such essential respects as:
1.1.1. the vehicle category, as defined in Article 1 of this Directive;
1.1.2. the maximum mass, as defined in 1.13;
1.1.3. the distribution of mass among the axles;
1.1.4. the maximum design speed;
1.1.5. a different type of braking system;
1.1.6. number and arrangement of the axles;
1.1.7. engine type;
1.1.8. the number of gears and their overall ratio;
1.1.8a. final drive ratios;
1.1.9. the tyre dimensions.
1.2. Braking device:
means the combination of parts other than the engine whose function is progressively to reduce the speed of a moving vehicle or to bring it to a halt, or to keep it stationary if it is already halted; these functions are specified in 2.1.2. The device consists of the control, the transmission and the brake proper.
1.3. Control:
means the part actuated directly by the driver to furnish to the transmission the energy required for braking, or for controlling it. This energy may be the muscular energy of the driver, or energy of another source controlled by the driver, or a combination of these various kinds of energy.
1.4. Transmission:
means the combination of components comprised between the control and the brakes and linking them functionally. Where the braking power is derived from or assisted by a source of energy independent of the driver but controlled by him, the reserve of energy in the device is likewise part of the transmission.
1.5. Brake:
means the parts of the braking device in which the forces opposing the movement of the vehicle are developed.
1.6. Different types of braking devices:
means devices which differ in such essential respects as:
1.6.1. components having different characteristics;
1.6.2. a component made of materials having different characteristics, or a component differing in shape or size;
1.6.3. a different assembly of the components.
1.7. Component(s) of a braking device:
means one or more of the individual parts which, when assembled, constitute the braking device.
1.8. Combined braking system means:
1.8.1. in the case of two-wheel mopeds and two-wheel motorcycles, a system whereby at least two brakes on different wheels are actuated in combination by the operation of a single control;
1.8.2. in the case of three-wheel mopeds and tricycles, a braking device which operates on all the wheels;
1.8.3. in the case of motorcycles with sidecar, a braking device which operates at least on the front and on the rear wheel. Therefore a braking device which operates simultaneously on the rear wheel and on the sidecar wheel is regarded as a rear brake.
1.9. Progressive and graduated braking:
means braking during which, within the normal operating range of the device, and whether during the application or during release of the brakes:
1.9.1. the driver can at any moment increase or decrease the braking force by acting on the control;
1.9.2. the braking force varies proportionally to the action on the control (monotonic function); and
1.9.3. the braking force can be easily regulated with sufficient precision.
1.10. Maximum design speed:
means the speed which the vehicle cannot exceed, on the level and without undue external influence, taking into account any special limitations imposed on the design and construction of the vehicle.
1.11. Laden vehicle:
means, except where otherwise stated, a vehicle so laden as to attain its maximum mass.
1.12. Unladen vehicle:
means the vehicle alone, as submitted for the tests, plus the driver alone and any necessary test equipment or instrumentation.
1.13. Maximum mass:
means the maximum mass stated by the vehicle manufacturer to be technically permissible (this mass may be greater than the permissible maximum mass laid down by the national administration).
1.14. Wet brake:
means a brake or brakes which has/have been treated in accordance with Section 1.3 of Appendix 1.
2. CONSTRUCTION AND FITTING REQUIREMENTS
2.1. General
2.1.1. Braking device
2.1.1.1. The braking device must be so designed, constructed and fitted as to enable the vehicle in normal use to comply with the provisions of this Directive, despite the vibration to which it may be subjected.
2.1.1.2. In particular, the braking device shall be so designed, constructed and fitted as to be able to resist the corroding and ageing phenomena to which it is exposed.
2.1.2. Functions of the braking device
The braking device defined in 1.2 must fulfil the following conditions:
2.1.2.1. Service braking
The service braking must make it possible to control the movement of the vehicle and to halt it safely, speedily and effectively, whatever its speed and load, on any up or down gradient. It must be possible to graduate this braking action. The driver must be able to achieve this braking action from his driving seat without removing his hands from the steering control.
2.1.2.2. Secondary (emergency) braking (where applicable)
The secondary (emergency) braking must make it possible to halt the vehicle within a reasonable distance in the event of failure of the service braking. It must be possible to graduate this braking action. The driver must be able to obtain this braking action from his driving seat while keeping at least one hand on the steering control. For the purposes of these provisions it is assumed that not more than one failure of the service braking can occur at one time.
2.1.2.3. Parking brake (if fitted)
The parking brake must make it possible to halt the vehicle stationary on up or down gradient even in the absence of the driver, the working parts being then held in the locked position by a purely mechanical device. The driver must be able to achieve this braking action from his driving seat.
2.2. Characteristics of braking devices
2.2.1. Every two-wheel moped or two-wheel motorcycle shall be equipped with two service braking devices, with independent controls and transmissions, one acting at least on the front wheel and the other at least on the rear wheel.
2.2.1.1. The two service braking devices may have a common braking so long as a failure in one braking device does not affect the performance of the other. Certain parts, such as the brake itself, the brake cylinders and their pistons (except the seals), the push rods and the cam assemblies of the brakes, shall not be regarded as liable to breakage if they are amply dimensioned, are readily accessible for maintenance and exhibit sufficient safety features.
2.2.1.2. A parking braking device is not compulsory.
2.2.2. Every motorcycle with sidecar shall be equipped with the braking devices which would be required if it had no sidecar; if these devices enable the required performance to be achieved in tests of the vehicle with sidecar, a brake on the sidecar wheel shall not be required; a parking braking device is not compulsory.
2.2.3. Every three-wheel moped must be equipped with:
2.2.3.1. either two independent service braking devices which together actuate the brakes on all of the wheels; or
2.2.3.2. a service braking device which operates on all the wheels, and a secondary (emergency) braking device which may be the parking brake.
2.2.3.3. In addition, every three-wheel moped must be equipped with a parking braking device acting on the wheel or wheels of at least one axle. The parking braking device, which may be one of the two devices specified in 2.2.3.1, must be independent of the device acting on the other axle or axles.
2.2.4. Every tricycle must be equipped with:
2.2.4.1. a foot-controlled serviced braking device which operates on all wheels, and a secondary (emergency) braking device which may be the parking brake; and
2.2.4.2. a parking braking device acting on the wheels of at least one axle. The control of the parking device must be independent of the control of the service braking device.
2.2.5. The braking devices must act on brake surfacespermanently connected to the wheels either rigidly or through components unlikely to fail.
2.2.6. The component parts of all braking devices, where attached to the vehicle, must be so secured that the braking devices do not fail in their function under normal operating conditions.
2.2.7. The braking devices shall operate freely when correctly lubricated and adjusted.
2.2.7.1. Wear of the brakes must be capable of being easily taken up by means of either manual or automatic adjustment. The brakes shall be capable of being adjusted to an efficient operating position until the brake linings have worn to the point of requiring replacement.
2.2.7.2. The control and the components of the transmission and of the brakes must possess a reserve of travel such that when the brakes become heated and the brake linings have reached maximum permitted degree of wear, effective braking is ensured without immediate adjustment being necessary.
2.2.7.3. When correctly adjusted the components of the braking device must not, when operated, contact anything other than the intended parts.
2.2.8. In braking devices where the transmission is hydraulic, the receptables containing the reserve fluid must be so designed and constructed that the level of the reserve fluid can be easily checked.
This provision does not apply to mopeds with a maximum speed of 25 km/h or lower.
Appendix 1 Braking tests and performance of braking devices
1. BRAKING TESTS
1.1. General
1.1.1. The performance prescribed for braking devices is based on the stopping distance. The performance of a braking device is determined either by measuring the stopping distance in relation to the initial speed or by the response time of the device and the mean fully-developed deceleration.
1.1.2. The stopping distance is the distance covered by the vehicle from the moment when the driver begins to actuate the control of the device until the moment when the vehicle stops. The initial speed is the speed at the moment when the driver begins to actuate the control of the device. In the formula given below, for measurement of braking performance:
V= initial speed in kilometres per hour, and
S= stopping distance, in metres.
1.1.3. For component type-approval, the braking performance must be measured during road tests conducted under the following conditions:
1.1.3.1. the vehicle's condition as regards its mass must be as prescribed for each type of test and must be specified in the test report;
1.1.3.2. the test must be carried out at the speed and in the manner prescribed for each type of test: if the maximum speed of the vehicle does not conform to the speed prescribed, the test must be carried out under the special alternative conditions provided;
1.1.3.3. the prescribed performance must be obtained without locking of the wheel(s), without deviation of the vehicle from its course, and without any abnormal vibration;
1.1.3.4. during the tests the force applied to the brake control in order to obtain the prescribed performance must not exceed the maximum laid down for the test vehicle's category.
1.1.4. Test conditions
1.1.4.1. The service braking tests must be carried out under the following conditions:
1.1.4.1.1. at the start of the test or any series of tests the tyres must be cold and at the pressure prescribed for the load actually borne by the wheels when the vehicle is stationary;
1.1.4.1.2. the vehicle must be loaded, when required to be tested in the laden condition, with the load distributed in accordance with the manufacturer's prescription;
1.1.4.1.3. for all the type-0 tests the brakes must be cold: a brake is deemed to be cold when the temperature measured on the disc or on the outside of the drum is less than 100 °C;
1.1.4.1.4. the driver must shall be seated on the saddle as for normal driving and must maintain the same position throughout the test;
1.1.4.1.5. the test area must be level, dry and have a surface affording good adhesion;
1.1.4.1.6. the test must be performed when there is no wind liable to affect the test result.
1.2. Type-0 test (performance test with brakes cold)
1.2.1. General
1.2.1.1. The limits prescribed for service braking performance are as laid down for each category of vehicle.
1.2.2. Type-0 test with engine disconnected
1.2.2.1. The test must be carried out at the speed prescribed for the category to which the vehicle belongs, the figures prescribed in this connection being subject to a certain margin of tolerance.
In the case of vehicles where the two service brakes can be applied separately, the braking devices must be tested separately. The minimum performance for each braking device for each category of vehicle must be attained.
1.2.2.1.1. In the case of a vehicle with a manual gearbox or an automatic transmission where the gearbox can be disengaged manually, the tests must be carried out with the gearbox inoperative and/or the engine disconnected by clutch disengagement or otherwise.
1.2.2.1.2. In the case of a vehicle with other types of automatic transmission, the tests must be carried out under the normal operating conditions.
1.2.3. Type-0 test with engine connected for motorcycles (with or without sidecar) and trycicles
1.2.3.1. Tests must be carried out in the unladen condition at various speeds, the lowest being equal to 30 % of the maximum speed of the vehicle and the highest being equal to 80 % of that speed or 160 km/h, whichever is the lower.
The maximum practical performance figures are measured and together with the behaviour of the vehicle must be recorded in the test report. In the case where two service braking devices can be applied separately, both devices must be tested together and simultaneously, with the vehicle unladen.
1.2.4. Type-0 test with engine disconnected: with wet brakes
1.2.4.1. This test must (subject to the exemption contained in 1.3.1) be carried out on mopeds and motorcylces (but not tricycles). The test procedure is identical to that for the type-0 test with engine disconnected, except for the provisions for wetting the brakes described in 1.3.
1.3. Special provisions relating to testing with wet brakes
1.3.1. Enclosed brakes: it is not necessary to carry out this series of type-0 tests on vehicles equipped with conventional drum brakes or with fully enclosed brakes which are not subject to water penetration under normal running conditions.
1.3.2. The test with brakes subject to wetting must be carried out under the same conditions as the test with dry brakes. There must be no adjustment or alteration of the braking device other than fitting the equipment to allow brake wetting.
1.3.3. The test equipment must continuously wet the brakes for each test run at a flow rate of 15 l/h for each brake. Two disc brakes on one wheel will be considered as two brakes.
1.3.4. For exposed or partly exposed disc brakes, the prescribed amount of water must be directed on to the rotating disc in such a manner that it is equally distributed on the surface or surfaces of the disc swept by the friction pad or pads.
1.3.4.1. For fully exposed disc brakes, the water must be directed on to the surface(s) of the disc 45° in advance of the friction pad(s).
1.3.4.2. For partly exposed disc brakes, the water must be directed on to the surface(s) of the disc 45° in advance of the shield or baffle.
1.3.4.3. The water must be directed on to the surface(s) of the disc(s) in a continuous jet, in a direction perpendicular to the surface of the disc, from single jet nozzles so positioned as to be between the inner extremity and a point two-thirds of the distance from the outer extremity of that part of the disc swept by the friction pad(s) (see Figure 1).
1.3.5. For fully enclosed disc brakes, where the provisions of 1.3.1 do not apply, the water must be directed on to both sides of the shield or baffle at a point and in a manner corresponding with that described in 1.3.4.1 and 1.3.4.3. Where the nozzle would be coincident with a ventilation or inspection port, the water must be applied one quarter of a revolution in advance of the said port.
1.3.6. Where in 1.3.3 and 1.3.4, it is not possible to apply the water in the position specified owing to the presence of some fixed part of the vehicle, the water must be applied at the first point, exceeding 45° where uninterrupted application is possible.
1.3.7. For drum brakes, where the requirements of 1.3.1 do not apply, the prescribed amount of water must be distributed equally on either side of the braking device (that is, on the stationary back plate and the rotating drum) from nozzles so positioned as to be two-thirds of the distance from the outer circumference of the rotating drum to the wheel hub.
1.3.8. Subject to the requirements of 1.3.7 and to the requirement that no nozzle shall be within 15° of or coincident with a ventilation or inspection port on the stationary back plate, the test equipment for drum brakes shall be so positioned as to obtain the optimum uninterrupted application of water.
1.3.9. To ensure the correct wetting of the brake(s), the vehicle must be driven, immediately before the commencement of the test series:
- with the wetting equipment functioning continuously, as prescribed in this Appendix,
- at the prescribed test speed,
- without the operation of the braking device(s) to be tested,
- for a distance of no less than 500 m prior to the point at which the test is to be carried out.
1.3.10. For rim brakes, as fitted to some mopeds with a maximum speed of 25 km/h or lower, the water must be directed on to the wheel rim as shown in Figure 2.
Figure 1 Method of water application for disc brakes Figure 2 Method of water application for rim brakes 1.4. Type-I test (fade test)
1.4.1. Special provisions
1.4.1.1. The service brakes of all motorcycles (with or without sidecar) and tricycles must be tested by a series of repeated stops, the vehicle being in the laden condition, in accordance with the requirements set out below. For vehicles equipped with a combined braking system, it is sufficient to submit this service braking device to the type-I test.
1.4.1.2. The type-I test is carried out in three parts.
1.4.1.2.1. A single type-0 test as prescribed by 2.1.2 or 2.2.3.1.
1.4.1.2.2. A series of 10 repeated stops carried out in accordance with the requirements of point 1.4.2.
1.4.1.2.3. A single type-0 test, carried out as soon as possible after the completion of the test specified in 1.4.1.2.2 and in any case within one minute thereof, and performed under the same conditions as those used for the test in 1.4.1.2.1, in particular at a control force as constant as possible with a mean value not exceeding the mean force actually used in that test.
1.4.2. Test conditions
1.4.2.1. The vehicle and the brake(s) to be tested must be substantially free from moisture and the brake(s) cold (& le; 100 °C).
1.4.2.2. The initial test speed is:
1.4.2.2.1. for testing the front brake(s), whichever is the lower of 70 % of the vehicle's maximum speed and 100 km/h;
1.4.2.2.2. for testing the rear brake(s), whichever is the lower of 70 % of the vehicle's maximum speed and 80 km/h;
1.4.2.2.3. for testing a combined braking system, whichever is the lower of 70 % of the vehicle's maximum speed and 100 km/h.
1.4.2.3. The distance between the initiation of one stop and the initiation of the next shall be 1 000 metres.
1.4.2.4. The use of the gearbox and/or clutch is as follows:
1.4.2.4.1. In the case of a vehicle with a manual gearbox or an automatic transmission where the gearbox can be disengaged manually, the highest gear, consistent with attaining the initial test speed, must be engaged during the stops.
When the vehicle speed has fallen to 50 % of the initial test speed, the engine must be disengaged.
1.4.2.4.2. In the case of a vehicle with a fully automatic transmission, the test must be carried out under the normal operating conditions for such equipment.
For the approach, the gear suitable to the initial test speed must be used.
1.4.2.5. After each stop, the vehicle must immediately be subjected to maximum acceleration to reach the initial test speed and maintained at that speed until the initiation of the next stop. If appropriate, the vehicle may be turned round on the test track before acceleration.
1.4.2.6. The force applied to the control shall be so adjusted as to maintain a mean deceleration of 3 m/s2 or the maximum deceleration achievable with that brake, whichever is the lower, at the first stop: this force must remain constant throughout the succeeding stops required by 1.4.1.2.2.
1.4.3. Residual performance
1.4.3.1. At the end of the type-I test the residual performance of the service braking device must be measured in the same conditions (and in particular at a control force as constant as possible with a mean value not exceeding the mean force actually used) as for the type-0 test with the engine disconnected (the temperature conditions may be different).
1.4.3.2. This residual performance must not be:
1.4.3.2.1. if expressed as a deceleration, less than 60 % of the deceleration figure achieved during the type-0 test;
or
1.4.3.2.2. if expressed as a stopping distance, more than the stopping distance figure, calculated in accordance with the following formula;
where:
S1 = the stopping distance achieved in the type-0 test,
S2 = the stopping distance as recorded in the residual performance test,
a = 0,1,
V = the initial speed at the beginning of braking as defined in 2.1.1 or 2.2.2.
2. PERFORMANCE OF BRAKING DEVICES
2.1. Provisions relating to tests of vehicles with braking devices operating on the wheel or wheels of the front or rear axle only:
2.1.1. Test speed V = 40 km/h(1) for mopeds.
Test speed V = 60 km/h(2) for motorcycles (with or without sidecar) and tricycles.
2.1.2. Braking performance with the vehicle laden
2.1.2.1. For the purposes of the type-I residual performance test (motorcycles with or without sidecar), the recorded performance levels in terms of stopping distances, mean fully developed deceleration, as well as the control force used, are recorded.
2.1.2.2. Braking with the front brake only
/* Tables: see OJ */
/* Tables: see OJ */
2.1.3.1. A practical test of the vehicle driven by the driver alone is not required if a calculation shows that the distribution of the mass on the braked wheels allows a mean fully developed deceleration of at least 2,5 m/s2 or a stopping distance
to be achieved with each of the single axle braking devices.
2.2. Provisions relating to tests of vehicles of which (at least) one of the braking devices is a combined braking system
2.2.1. For the purposes of the type-I residual performances test on motorcycles (with or without sidecar) and tricycles, the recorded performance levels in terms of stopping distance, mean fully developed deceleration, as well as the control force used, are recorded.
2.2.2. Test speed V = 40 km/h(3) for mopeds.
Test speed V = 60 km/h(4) for motorcycles (with or without sidecar and tricycles.
2.2.3. The vehicle is tested both unladen and laden.
2.2.3.1. Braking with the combined braking system only
/* Tables: see OJ */
The stopping distance is:
(corresponding mean fully developed deceleration 2,5 m/s2).
2.3. Braking performance with the parking braking device (if applicable)
2.3.1. The parking braking device must, even if it is combined with one of the other braking devices, be capable of holding the laden vehicle stationary on an 18 % up or down gradient.
2.4. Provisions relating to forces applied to brake controls
2.4.1. Forces applied to service brake controls
hand control & le; 200 N
foot control & le; 350 N (mopeds and motorcycles (with or without sidecar))
foot control & le; 500 N (tricycles).
2.4.2. Forces applied ot the parking brake control (if applicable)
with manual control & le; 400 N
with foot control & le; 500 N.
2.4.3. In the case of handbrake levers, the point of application of the manual force is assumed to be 50 mm from the outer end of the lever.
2.5. Performance levels (minimum and maximum) to be attained with wet brakes
2.5.1. The mean deceleration to be attained with wet brake(s) between 0,5 and 1,0 second after application of the brake must be at least 60 %(5) of that attained with dry brake(s) during the same time period and with the same control force applied.
2.5.2. The control force used, which must be applied as quickly as possible, must be equivalent to that required to attain a mean deceleration of 2,5 m/s2 with dry brake(s).
2.5.3. At no time during the type-0 test with wet brake(s) may the deceleration exceed 120 % of that attained with dry brake(s).
Appendix 2 Requirements applicable to two-wheel mopeds, two-wheel motorcycles and tricycles equipped with anti-lock devices
1. GENERAL
1.1. The purpose of these provisions is to define the minimum performances for braking systems with anti-lock devices fitted to two-wheel mopeds, two-wheel motorcycles and tricycles. This does not make it compulsory to fit vehicles with an anti-lock device but if such devices are fitted to a vehicle they must meet the requirements below.
1.2. The devices known at present comprise a sensor or sensors, a controller or controllers and a modulator or modulators. Any devices of a different design will be deemed to be anti-lock devices within the meaning of this Appendix if they provide performances at least equal to those prescribed by this Appendix.
2. DEFINITIONS
For the purposes of this Appendix:
2.1. Anti-lock device
means a component of a service braking system which automatically controls the degree of slip, in the direction of rotation of the wheel(s) on one or more wheels of the vehicle during braking.
2.2. Sensor
means a component designed to identify and transmit to the controller the conditions of rotation of the wheel(s) or the dynamic conditions of the vehicle.
2.3. Controller
means a component designed to evaluate the data transmitted by the sensor(s) and to transmit a signal to the modulator.
2.4. Modulator
means a component designed to vary the braking force(s) in accordance with the signal received from the controller.
3. NATURE AND CHARACTERISTICS OF THE SYSTEM
3.1. Each controlled wheel must be such that it can bring at least its own device into operation.
3.2. Any break in the supply of electricity to the device and/or in the wiring external to the electronic controller(s) must be signalled to the driver by an optical warning signal, which must be visible even in daylight; it must be easy for the driver to check that it is in working order(6) .
3.3. In the event of a failure in an anti-lock device, the braking efficiency of the laden vehicle must not be less than that prescribed for whichever is the lower of the two requirements for the vehicle defined in 2.1.2.2 or 2.1.2.3 of Appendix 1.
3.4. The operation of the device must not be affected adversely by electro-magnetic fields(7) .
3.5. Anti-lock devices must maintain their performance when the brake is fully applied for the duration of any stop.
4. UTILIZATION OF ADHESION
4.1. General
4.1.1. In the case of two-wheel motorcycles and tricycles, braking systems equipped with an anti-lock device are deemed acceptable when the condition
e
is satisfied where e represents the adhesion utilized as defined in the addendum to this Appendix(8) .
4.1.2. The coefficient of adhesion utilization e must be measured on road surfaces with a coefficient of adhesion not exceeding 0,45 and of not less than 0,8.
4.1.3. Tests must be carried out with the vehicle unladen.
4.1.4. The test procedure to determine the coefficient of adhesion (K) and the formula for calculating the adhesion utilization (e) are as prescribed in the addendum to this Appendix.
5. ADDITIONAL CHECKS
5.1. The following additional checks must be carried out with the vehicle unladen.
5.1.1. Any wheel controlled by an anti-lock device must not lock when the full force(9) is suddenly applied to its control device, on the two kinds, of road surface specified in 4.1.2 at initial speeds of up to 0,8 Vmax but not exceeding 80 km/h(10) .
5.1.2. Where a wheel controlled by an anti-lock device passes from a high-adhesion surface or a low-adhesion surface as described in 4.1.2 with the full force(11) applied to the control device, the wheel must not lock. The running speed and the instant of applying the brakes must be so calculated that, with the anti-lock device fully cycling on the high-adhesion surface, the passage from one surface to the other is made at about 0,5 Vmax not exceeding 50 km/h.
5.1.3. Where a vehicle passes from a low-adhesion surface to a high-adhesion surface as described in 4.1.2 with the full force(12) applied to the control device, the deceleration of the vehicle must rise to the appropriate high value within a reasonable time and the vehicle must not deviate from its initial course. The running speed and the instant of applying the brakes must be so calculated that, with the anti-lock device fully cycling on the low-adhesion surface, the passage from one surface to the other occurs at about 0,5 Vmax not exceeding 50 km/h.
5.1.4. Where both independent braking devices are equipped with an anti-lock device the tests prescribed in 5.1.1, 5.1.2 and 5.1.3 must also be performed using both independent braking devices together, the stability of the vehicle being maintained at all times.
5.1.5. However, in the tests provided for in 5.1.1, 5.1.2, 5.1.3 and 5.1.4, periods of wheel locking or of extreme wheel slip are allowed provided that the stability of the vehicle is not adversely affected. Below vehicle speeds of 10 km/h wheel locking is permitted.
e Addendum 1. DETERMINATION OF THE COEFFICIENT OF ADHESION (K)
1.1. The coefficient of adhesion is determined from the maximum braking rate, without wheel lock, of the vehicle with the anti-lock device(s) disconnected and braking all wheels simultaneously(13) .
1.2. The braking tests are carried out by applying the brakes at an initial speed of 60 km/h (or, in the case of vehicles which are unable to reach 60 km/h, a speed of roughly 0,9 Vmax), the vehicle being unladen (apart from the test instruments and/or the necessary safety equipment). The effort exerted on the brake control must be constant throughout the tests.
1.3. A series of tests may be carried out up to the critical point reached immediately before the wheel(s) lock by varying both the front and the rear brake forces, in order to determine the maximum braking rate of the vehicle(14) .
1.4. The braking rate (Z) shall be determined by reference to the time taken for the speed of the vehicle to reduce from 40 km/h to 20 km/h, using the formula
where t is measured in seconds.
Alternatively, for vehicles unable to attain 50 km/h, the braking rate is determined by reference to the time taken for the speed of the vehicle to reduce from 0,8 Vmax to 0,8 Vmax - 20 where Vmax is measured in km/h.
The maximum value of Z = K.
2. DETERMINATION OF THE ADHESION UTILIZATION (e).
2.1. The adhesion utilized is defined as the quotient of the maximum braking rate with the anti-lock device in operation (Zmax) and the maximum braking rate with the anti-lock disconnected (Zm). Separate tests must be carried out on each wheel equipped with an anti-lock device.
2.2. Zmax is calculated on the on the basis of the average of the three tests, using the time taken for the speed of the vehicle to achieve the reductions in speed specified in 1.4.
2.3. The adhesion utilized is given by the formula
e
Appendix 3 Information sheet in respect of the braking of a type of two or three-wheel motor vehicle
(To be attached to the component type-approval application where this is submitted separately from the vehicle type-approval application)
Reference number (allocated by the applicant): .
The application for component type-approval in respect of the braking of a type of two- or three-wheel motor vehicle must be accompanied by the information set out in Annex II to Directive 92/61/EEC, under A, in the following sections:
0.1,
0.2,
0.4 to 0.6,
2.1 to 2.2.1,
3.0 to 3.1.1,
5.2,
5.2.2,
7.1 to 7.4.
Appendix 4 Name of Administration
Approval certificate in respect of the braking of a type of two- or three-wheel motor vehicle
MODEL
Report No ....................... issued by testing body ...................... on . (date)
Approval No: ..................... Extension No: .
1. Trade mark or name of vehicle: .
2. Type of vehicle: .
3. Name and address of manufacturer: .
4. Name and address of manufacturer's authorized representative (if any): .
5. Date vehicle submitted for test: .
6. Approval has been granted/refused(15) .
7. Place: .
8. Date: .
9. Signature: .
(1) Vehicles the maximum speed (Vmax) of which is lower than 45 km/h in the case of mopeds, or 67 km/h in the case of motorcycles (with or without sidecar) and tricycles are tested at a speed equal to 0,9 Vmax.
(2) Vehicles the maximum speed (Vmax) of which is lower than 45 km/h in the case of mopeds, or 67 km/h in the case of motorcycles (with or without sidecar) must be tested at a speed equal to 0,9 Vmax.
(3) For mopeds with a maximum speed of 25 km/h or lawer this value is 40 %.
(4) The technical service should examine the electronic controller and/or any drive system with regard to possible failure modes.
(5)() Until uniform test procedures have been agreed, the manufacturers must provide the technical services with their test procedures and results.
(6) For two-wheel mopeds, until a minimum value for has been established, the measured value must be recorded in the test report.
(7) Full force means the maximum force prescribed in Section 2.4, Appendix 1 for the category of vehicle: a higher force may be used if required to actuate the anti-lock device.
(8) On low adhesion surfaces (& le; 0,35) the initial speed may be reduced for safety reasons: in such cases, the K value and the initial speed must be noted in the test report.
(9) Additional requirements may have to be established in the case of vehicles equipped with combined braking systems.
(10) As an initial step, to facilitate these preliminary tests, the maximum control force applied before the critical point may be obtained for each individual wheel.
(11) Delete as appropriate.
