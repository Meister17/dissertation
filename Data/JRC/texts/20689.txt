COMMISSION REGULATION (EC) No 2926/94 of 30 November 1994 amending Regulation (EEC) No 2177/92 laying down detailed rules for the application of the specific supply arrangements for the Azores, Madeira and the Canary Islands with regard to sugar and Regulation (EEC) No 1713/93 as regards the agricultural conversion rates in the sugar sector
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 1601/92 of 15 June 1992 concerning specific measures for the Canary Islands with regard to certain agricultural products (1), as last amended by Commission Regulation (EEC) No 1974/93 (2), and in particular
Articles 3 (4) and 7 (2) thereof,
Whereas the detailed rules for the application of the specific supply arrangements for the Azores, Madeira and the Canary Islands with regard to sugar are laid down by Commission Regulation (EEC) No 2177/92 (3), as last amended by Regulation (EC) No 1443/94 (4);
Whereas the new common detailed rules for implementation of the specific arrangements for the supply of certain agriculture products to the Canary Islands established by Commission Regulation (EC) No 2790/94 (5), as amended by Regulation (EC) No 2883/94 (6), which enter into force on 1 December 1994, no longer lay down that a security must be lodged before an aid certificate is issued and define a new operative event for the rate to be used for converting supply aid into Spanish pesetas; whereas, therefore, the corresponding amendments with regard to the Canary Islands should be made to Regulation (EEC) No 2177/92 and Regulation (EEC) No 1713/93 of 30 June 1993 establishing special detailed rules for applying the agricultural conversion rate in the sugar sector (7), as amended by Regulation (EEC) No 2627/93 (8), to take effect from 1 December 1994;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Sugar,
HAS ADOPTED THIS REGULATION:
Article 1
The following paragraph 5 is added to Article 5 of Regulation (EEC) No 2177/92:
'5. The above paragraphs shall not apply in respect of the Canary Islands.'
Article 2
In point XVI (c) of the Annex to Regulation (EEC) No 1713/93, the words 'and the Canary Islands' are deleted.
Article 3
This Regulation shall enter into force on 1 December 1994.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 30 November 1994.
For the Commission
René STEICHEN
Member of the Commission
(1) OJ No L 173, 27. 6. 1992, p. 13.
(2) OJ No L 180, 23. 7. 1993, p. 26.
(3) OJ No L 217, 31. 7. 1992, p. 71.
(4) OJ No L 157, 24. 6. 1994, p. 4.
(5) OJ No L 296, 17. 11. 1994, p. 23.
(6) OJ No L 304, 29. 11. 1994, p. 18.
(7) OJ No L 159, 1. 7. 1993, p. 94.
(8) OJ No L 240, 25. 9. 1993, p. 19.
