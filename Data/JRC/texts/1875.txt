Action brought on 17 August 2005 — De Geest
v Council
Parties
Applicant(s): Johan De Geest (Rhode-St-Genèse, Belgium) (represented by: S. Orlandi, X. Martin, A. Coolen, J.-N. Louis, E. Marchal, lawyers)
Defendant(s): Council of the European Union
Form of order sought
The applicant(s) claim(s) that the Court should:
- annul the decision of the Council of 3 January 2005 rejecting the applicant's claim to be recruited in grade A6 or A7, which have been renamed A*10 and A*8 since 1 May 2004;
- order the defendant to pay the costs.
Pleas in law and main arguments
The pleas in law and main arguments relied on by the applicant are identical to those relied on in Case T-164/05 De Geest v Council. [1]
[1] OJ C 171, 9.7.2005, p. 28.
--------------------------------------------------
