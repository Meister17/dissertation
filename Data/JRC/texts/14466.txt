[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 12.7.2006
COM(2006) 368 final
2003/0165 (COD)
OPINION OF THE COMMISSION pursuant to Article 251 (2), third subparagraph, point (c) of the EC Treaty, on the European Parliament's amendments to the Council's common position regarding the proposal for a
REGULATION OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL on nutrition and health claims made on foods
AMENDING THE PROPOSAL OF THE COMMISSIONpursuant to Article 250 (2) of the EC Treaty
2003/0165 (COD)
OPINION OF THE COMMISSION pursuant to Article 251 (2), third subparagraph, point (c) of the EC Treaty, on the European Parliament's amendments to the Council's common position regarding the proposal for a
REGULATION OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL on nutrition and health claims made on foods
1. Introduction
Article 251(2), third subparagraph, point (c) of the EC Treaty provides that the Commission is to deliver an opinion on the amendments proposed by the European Parliament at second reading. The Commission sets out its opinion below on the amendments proposed by Parliament.
2. Background
Date of transmission of the proposal to the EP and the Council (document COM(2003) 424 final – 2003/0165COD): | 17 July 2003 |
Date of the opinion of the European Economic and Social Committee: | 26 February 2004 |
Date of the opinion of the European Parliament, first reading: | 26 May 2005 |
Date of adoption of the common position | 8 December 2005 |
Date of European Parliament opinion, second reading | 16 May 2006 |
3. PURPOSE OF THE PROPOSAL
This proposal covers nutrition and health claims used in the labelling, presentation and advertising of foods. Only nutrition and health claims that are in conformity with the provisions of this Regulation will be allowed on the labelling, presentation and advertising of foods placed on the market within the Community and delivered as such to the final consumer.
The main objectives of this proposal are the following:
– to achieve a high level of consumer protection by regulating the provision of further voluntary information, beyond the mandatory information foreseen by EU legislation;
– to improve the free movement of goods within the internal market;
– to increase legal security for economic operators;
– to ensure fair competition in the area of foods; and
– to promote and protect innovation in the area of foods.
The proposed rules ensure that foods bearing nutrition and health claims are labelled and advertised in a truthful and meaningful manner. By adopting rules that regulate the information about the foods and their nutritional value appearing on the label, the consumers will be able to make informed and meaningful choices. This also contributes to a higher level of protection of human health.
Appropriate labelling can indeed point consumers in the right direction towards adopting a healthy diet, and facilitate positive and informed choices. These rules also take into account the importance for the food industry to have a regulatory environment thereby allowing them to innovate and remain competitive at Community and international level. This also gives the economic operators legal security and a more predictable environment.
This proposal was foreseen in the White Paper on Food Safety (COM(1999) 719 final– Action n° 65). Its adoption will contribute to completing the regulatory framework covering the labelling of food for human consumption, and participate in providing better nutritional information to consumers and allowing them to make informed choices about their diet and consumption habits.
4. OPINION OF THE COMMISSION ON THE AMENDMENTS BY THE EUROPEAN PARLIAMENT
4.1. Amendments accepted by the Commission
The Commission can accept amendments 50 to 89, which are all the amendments adopted by the European Parliament. Amendments 50 to 60 adapt the recitals with the amended articles. Amendments 61 to 64 adjust the scope of the Regulation. Amendments 65 and 66 are dealing with the nutrient profiles that foods will have to meet in order to bear claims. Amendments 73 to 84 are dealing with the authorisation procedures for health claims, and amendments 87 to 89 extend the transition periods.
These amendments are the result of a compromise agreement reached between the European Parliament, Council and Commission during the second reading. The amendments are in line with the Commission’s objectives for the proposal and maintain the balance of interests achieved in the common position.
5. Conclusion
Pursuant to Article 250(2) of the EC Treaty, the Commission amends its proposal as set out above.
