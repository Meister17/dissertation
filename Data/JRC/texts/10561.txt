Commission Regulation (EC) No 162/2006
of 30 January 2006
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 31 January 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 30 January 2006.
For the Commission
J. L. Demarty
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 386/2005 (OJ L 62, 9.3.2005, p. 3).
--------------------------------------------------
ANNEX
to Commission Regulation of 30 January 2006 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code | Standard import value |
07020000 | 052 | 76,8 |
204 | 43,2 |
212 | 102,0 |
624 | 120,2 |
999 | 85,6 |
07070005 | 052 | 93,4 |
204 | 102,3 |
628 | 155,5 |
999 | 117,1 |
07091000 | 220 | 80,1 |
624 | 91,7 |
999 | 85,9 |
07099070 | 052 | 122,9 |
204 | 147,5 |
999 | 135,2 |
08051020 | 052 | 43,5 |
204 | 56,8 |
212 | 48,0 |
220 | 50,6 |
624 | 58,4 |
999 | 51,5 |
08052010 | 204 | 82,4 |
999 | 82,4 |
08052030, 08052050, 08052070, 08052090 | 052 | 61,6 |
204 | 101,1 |
400 | 85,6 |
464 | 144,8 |
624 | 79,0 |
662 | 36,9 |
999 | 84,8 |
08055010 | 052 | 40,2 |
220 | 61,7 |
999 | 51,0 |
08081080 | 400 | 130,0 |
404 | 99,2 |
720 | 73,8 |
999 | 101,0 |
08082050 | 388 | 83,3 |
400 | 83,9 |
720 | 45,8 |
999 | 71,0 |
--------------------------------------------------
