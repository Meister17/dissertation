Council Regulation (EC) No 311/2002
of 18 February 2002
imposing a definitive anti-dumping duty on imports of certain magnetic disks (3,5" microdisks) originating in Hong Kong and the Republic of Korea
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 384/96 of 22 December 1995 on protection against dumped imports from countries not members of the European Community(1), and in particular Article 11(2) thereof,
Having regard to the proposal submitted by the Commission after consulting the Advisory Committee,
Whereas:
A. GENERAL INFORMATION
1. Procedure
Previous investigations
(1) In September 1994, by Regulation (EC) No 2199/94(2), the Council imposed definitive anti-dumping measures on imports of certain magnetic disks ("3,5" microdisks") originating in Hong Kong and the Republic of Korea ("Korea").
Proceedings concerning other countries
(2) Apart from measures on 3,5" microdisks originating in Hong Kong and Korea, it is recalled that definitive anti-dumping duties have also been imposed on imports of 3,5" microdisks originating in Japan, Taiwan and the People's Republic of China(3) and Indonesia(4). The measures on imports of 3,5" microdisks originating in Japan, Taiwan and the People's Republic of China are currently subject to an expiry review(5).
(3) In December 1999 the product scope of the measures was clarified. It was found that the microdisks based on optically continuous servo tracking technology or magnetic sector servo tracking technology with a storage capacity of 120 MB or more did not fall under the scope of any of the existing anti-dumping measures on 3,5" microdisks(6).
Current investigation
(4) Following the publication of the notice of impending expiry of the anti-dumping measures applicable to imports of 3,5" microdisks originating in Hong Kong and Korea, the Commission received, in June 1999, a request to review these measures pursuant to Article 11(2) of Regulation (EC) No 384/96 ("the basic Regulation"). The request was lodged by the Committee of European Diskette Manufacturers ("Diskma") on behalf of producers whose combined output constitutes a major proportion of the total Community production of 3,5" microdisks ("the product concerned"). The request was based on the grounds that the expiry of the measures would be likely to result in the continuation or recurrence of dumping and injury to the Community industry.
(5) Having determined, after consulting the Advisory Committee, that sufficient evidence existed for the initiation of an expiry review, the Commission initiated an investigation(7) pursuant to Article 11(2) of the basic Regulation.
Period of investigation
(6) The investigation of the likelihood of continuation and recurrence of dumping covered the period from 1 September 1998 to 31 August 1999 ("IP"). The examination of trends relevant for the assessment of the likelihood of continuation and recurrence of injury covered the period from 1995 up to the end of the investigation period ("the analysis period").
2. Parties concerned by the investigation
(7) The Commission officially advised the four Community producers supporting the request, the exporting producers and the importers known to be concerned as well as the representatives of the exporting countries of the initiation of the investigation, and gave the parties directly concerned the opportunity to make their views known in writing and to request a hearing.
(8) The Commission sent questionnaires to all parties known to be concerned and received full replies from three Community producers ("the cooperating Community producers") and one unrelated importer of 3,5" microdisks originating in Hong Kong. No complete replies were received from any exporting producers in either Hong Kong or Korea.
(9) The Commission sought and verified all information they deemed necessary for the purposes of determining whether there was a likelihood of a continuation or recurrence of dumping and injury and whether maintaining the measures would not be against the Community interest.
Verification visits were carried out at the premises of the following companies:
Cooperating Community producers
- Sentinel N.V., Bodem, Belgium;
- Computer Support Italcard srl, Milan, Italy;
- Datarex srl, Assemini, Italy.
Importers
- Datamatic srl, Milan, Italy.
3. Product concerned and like product
Product concerned
(10) The product concerned is the same as in the original investigation and as clarified subsequently, i.e. 3,5" microdisks used to record and store encoded digital computer information falling within CN code ex 8523 20 90, with the exception of 3,5" microdisks based on optically continuous servo tracking technology or magnetic sector servo tracking technology with a storage capacity of 120 MB or more.
(11) The 3,5" microdisks concerned are available in various types, depending on various factors, including, amongst others, their storage capacity, formatting, degree of certification (this is a measure used to test the performance of the microdisk which influences its market value) and on the way in which they are marketed, i.e. whether sold as branded products or in bulk. Despite the existence of various types of 3,5" microdisks, there are no significant differences in their basic physical characteristics and technology, and they all show a high degree of interchangeability.
(12) On this basis, the 3,5" microdisks as defined above are to be considered as one product.
Like product
(13) The various types of 3,5" microdisks, as defined above, which are manufactured and sold in the Community, and those manufactured and sold in the countries concerned or exported to the Community are alike in their essential physical characteristics and technology and show a high degree of interchangeability. Therefore, they are considered to be like products within the meaning of Article 1(4) of the basic Regulation.
B. LIKELIHOOD OF CONTINUATION OR RECURRENCE OF DUMPING
1. General
(14) The market share represented by imports of 3,5"microdisks from Hong Kong was significant at the time of the original investigation, when it represented more than 10 % of the total Community consumption. In the period following the imposition of the anti-dumping measures, this market share decreased to 5,5 % in 1997 and during the IP of this investigation, it represented 7,3 % of the total Community consumption.
(15) The market share represented by imports from Korea represented at the time of the original investigation more than 2 % of the total Community consumption. After the imposition of the anti-dumping measures, the market share of Korean imports decreased substantially, and during the IP it represented less than 0,2 % of the total Community consumption of the product concerned.
(16) As far as Hong Kong is concerned, four out of the 10 companies contacted came forward and stated that they were no longer producing or selling the product concerned to the Community. The remaining companies did not reply at all to the questionnaire.
(17) In the case of Korea, neither of the two companies named in the request for a review replied to the questionnaire. During the investigation no indication of the existence of other Korean exporting producers was found. It should be recalled that only one company cooperated in the investigation which led to the adoption of the anti-dumping measures.
(18) Under these circumstances, findings concerning Hong Kong and Korea were established on the basis of the facts available, in accordance with Article 18 of the basic Regulation.
2. Continuation of dumping
(a) Hong Kong
Normal value
(19) In view of the non-cooperation, normal value was established on the basis of the information provided in the request for review, which was the only reliable information available. The use of the normal value established in the original investigation would be inappropriate, because prices and costs for the manufacturing of the product concerned have decreased significantly over the period on all known international markets.
(20) In the request, normal value was constructed separately for bulked and branded/packed microdisks, by adding to the cost of production of the product concerned in the European Community a reasonable margin of profit (10 %, as in the original investigation). Given the nature of data available for the export price, i.e. Eurostat, which does not distinguish between different types, a single weighted average normal value was established. The weighing was done on the basis of the export shares of bulked and branded/packed microdisks to the Community, as evidenced by the few import invoices available. On the basis of this information, it was established that 3,5" microdisks marketed in bulk represented almost 80 % of the imports of microdisks in the Community, while the remaining 20 % were branded/packed microdisks.
(21) The Hong Kong Economic and Trade Office ("HKETO") objected to the possible use of the cost of production submitted in the request for review to establish the normal value for Hong Kong exports. However, the only alternatives suggested by the HKETO were the use of either the domestic sales or the cost of production in Hong Kong. As none of these methods could be applied because of the non-cooperation, the evidence provided by the complainant remained the most reliable information available to the Commission. After disclosure, the HKETO claimed that normal value should instead have been based on the exports from Hong Kong to other countries, as resulting from Hong Kong's trade statistics. This claim was rejected, notably as these trade statistics (at the 6 digit level of the international nomenclature: Harmonised System) also include products other than the product under consideration.
Export price
(22) In view of the non-cooperation, the export price was established on the basis of Eurostat data. However, Eurostat figures (on a per month and individual Member State basis) sometimes showed high unit values that were entirely irreconcilable with sales prices of 3,5" microdisks in the Community. This seems to be due to the fact that other products such as CD-Rs have been recorded under the CN code covering 3,5" microdisks. The Eurostat figures concerning the IP were therefore adjusted in order to exclude monthly data having an average unit value higher than 0,17 EUR/unit. This was a very conservative approach because:
- the export prices for Hong Kong set out in the review request were significantly below this threshold. The import prices resulting from the invoices made available by the sole cooperating unrelated importer in the Community, which however represented only a small share in the total import volume from Hong Kong were also significantly below this level;
- the threshold of 0,17 EUR/unit corresponds to the weighted average unrelated importers/Community producers' resale/selling price of 3,5" microdisks in the Community market during the IP. The weighing was carried out on the basis of sales quantities of the Community industry and the total import volumes of 3,5" microdisks from all origins. It was in fact reckoned that imports from Hong Kong should reasonably not take place at cif Community frontier average prices higher than the average resale/selling prices prevailing in the Community market.
The HKETO claimed that the abovementioned methodology was arbitrary and incorrect, and that values and quantities reported under the total CN code should have been used to calculate the export price. This claim is rejected for the reasons outlined above.
Comparison
(23) Normal value and export price were compared on an ex-works basis, in accordance with Article 2(10) of the basic Regulation. Due allowance in the form of adjustments was made for differences in transport, insurance, handling, loading and ancillary costs on the basis of the information contained in the request for a review.
Dumping margin
(24) In accordance with Article 2(11) of the basic Regulation, the weighted average normal value was compared with the weighted average export price to the Community.
(25) This comparison showed the existence of a margin of dumping of between 10 % and 15 %, the dumping margin being equal to the amount by which the normal value exceeded the export price, expressed as a percentage of the cif import price at the Community border, duty unpaid. This compares with a residual margin of dumping of 27,4 % established in the course of the original investigation (when individual dumping margins ranged from 6,7 % to 13,3 %).
(26) Should the comparison be made between the normal value calculated as described in recitals 17 to 19 and the export prices obtained from the invoices made available from the cooperating unrelated importer, the dumping margin would be even bigger. However, as mentioned above, this importer only accounted for a fairly small share of total imports from Hong Kong.
Conclusion
(27) The above findings show the existence of a continuation of dumping for imports of 3,5" microdisks originating in Hong Kong established on the basis of significant export volumes.
(b) Korea
Methodology
(28) Normal value and export price concerning Korea were established by following the same methodology described above for Hong Kong, though in the case of Korea no import invoices were available from the cooperating unrelated importer. The comparison between normal value and export price was also carried out by applying the same methodology used for Hong Kong. It should however be noted that the results of the examination of continuation of dumping were not decisive in this case in view of the low volume of imports from Korea as described at recital 49.
Dumping margin
(29) In accordance with Article 2(11) of the basic Regulation, the weighted average normal value was compared with the weighted average export price to the Community.
(30) This comparison showed the existence of a margin of dumping of between 20 % and 25 %, the dumping margin being equal to the amount by which the normal value exceeded the export price, expressed as a percentage of the cif import price at the Community border, duty unpaid. This compares with a margin of dumping margin of 8,1 % established in the course of the original investigation.
Conclusion
(31) The above findings show the existence of a continuation of dumping for imports of 3,5" microdisks originating in Korea, although the import volumes on the basis of which this conclusion had been reached were very small.
3. Likelihood of recurrence of dumping
(a) Hong Kong
(32) The market share represented by imports of 3,5" microdisks from Hong Kong was significant at the time of the original investigation, when it represented more than 10 % of the total Community consumption. In the period following the imposition of the anti-dumping measures, this market share decreased to 5,5 % in 1997, and, during the IP, it represented 7,3 % of the total Community consumption. It should also be mentioned that imports from Hong Kong have shown, after the IP, a significant decrease.
(33) The HKETO claimed, at the initiation of the investigation, that there was only one Hong Kong exporting producer of 3,5" microdisks still active in the market and that the production capacity of this company, if exported, was too limited to cause injury to the Community industry even if the anti-dumping duties were allowed to lapse. In addition, after disclosure, the HKETO claimed that after the IP production of microdisks in Hong Kong had ceased altogether and that therefore there was no likelihood of recurrence of dumping any more.
(34) In this respect, it should be pointed out that Hong Kong has held a substantial market share throughout the period analysed (mostly around 7 %). It should also be noted that imports of 3,5" microdisks from Hong Kong significantly decreased after the IP while there was a commensurate increase of imports of this product from Macao which is not subject to anti-dumping measures. Following disclosure, the HKETO has provided information claiming that production and exports to the Community of microdisks originating in Hong Kong has ceased after the IP. However, this cannot compensate for the fact that manufacturing activities for the product concerned have been found to be mobile and, if measures were repealed it is likely that certain exporting producers would relocate their production back to Hong Kong, or resume their export should they only have suspended them temporarily. It should also be noted that, because of the lack of cooperation shown by exporting producers during the IP, it was not possible to ascertain whether the source(s) of dumped exports had indeed ceased its(their) activities after the IP.
(b) Korea
(35) The market share represented by imports from Korea represented at the time of the original investigation more than 2 % of the total Community consumption. After the imposition of the anti-dumping measures, the market share of Korean imports decreased substantially, and during the IP it represented less than 0,2 % of the total Community consumption of the product concerned. It was therefore considered whether there was a likely risk of a recurrence of dumping of Korean imports at above de minimis levels.
(36) Two companies had been named in the request for a review as producers of the product concerned, however neither of them cooperated. For one of these two producers, exports to the Community represented a very substantial proportion of its total sales during the original investigation. Moreover, this company had a production capacity which used to satisfy an important part of Community consumption (close to 5 %). In the absence of cooperation by this company, it can therefore be assumed that substantial unused production capacity still exists in Korea which could be exploited, should the measures be repealed. Information available indicates that this producer is still manufacturing the product concerned.
(37) Moreover, given the abovementioned ease of relocation of production for the product concerned, it cannot be excluded that certain exporting producers would relocate their production back to Korea, should they have shifted production to locations outside Korea.
(38) As imports from Korea were made at significantly dumped prices, albeit in limited quantities, and as spare production capacity is reckoned to exist in the country and in view of the possibility of a relocation of production, it is concluded that, should the measures be repealed, there is a likelihood of recurrence of dumping at significant export volumes.
C. DEFINITION OF THE COMMUNITY INDUSTRY
(39) In the Community, during the IP, 3,5" microdisks were manufactured by:
- three Community producers, which fully cooperated with the Commission during the investigation;
- another producer which supported the request for review but did not supply data;
- other economic operators related to Japanese, Taiwanese and Chinese exporting producers.
(40) As in the previous proceedings, the Commission found that the assessment of the situation of the Community industry would be distorted if Community producers related to those producers from countries involved in prior proceedings found to be dumping the product concerned, and causing material injury to the applicant, were not excluded from the definition of the "Community production". Consequently, the production of the economic operators which are related to producers in the countries concerned have been excluded from the definition of the "Community production".
(41) The production of the three cooperating Community producers and of the other producer supporting the complaint constitute therefore the Community production within the meaning of Article 4(1) of the basic Regulation.
(42) The HKETO argued that companies related with Japanese, Taiwanese or other non-Community-owned producers should not be excluded from the definition of the Community production and stated this exclusion inflated the applicants' standing.
(43) As explained above, these producers were excluded from the definition of the Community production, because they were found to be related to parties dumping on the Community market and no reasons have been given as to why this approach should be changed in the review investigation. Furthermore, even when taking these producers into account, the cooperating Community producers would still represent more than 25 % of production taking place in the Community. The HKETO claim was, therefore, rejected.
(44) On this basis and given that the cooperating Community producers represent a major proportion, in this case more than 75 %, of the Community production, they are therefore considered to constitute the Community industry within the meaning of Article 4(1) and Article 5(4) of the basic Regulation. They are hereinafter referred to as the "Community industry".
D. THE COMMUNITY MICRODISKS' MARKET
1. General
(45) The market for 3,5" microdisks is mature, and is currently characterised by a situation of falling demand. Other products such as ZipTM discs, other high capacity microdisks such as HiFDs and optical-magnetic data storage media, such as recordable compact disks (CD-Rs) are progressively replacing the market held by 3,5" microdisks. However, given the significant base of PCs with 3,5" microdisk disk drives incorporated (currently estimated at 30 million units in the Community), it is clear that there will be a continuing need for these microdisks in the Community. Furthermore, according to recent market studies, most PC manufacturers continue to incorporate 3,5" microdisk drives in the basic configuration of their machines. It is estimated that by the year 2002 there will be 38 million microdisk drives in the Community. The Community microdisk market will therefore remain important.
2. Consumption of 3,5" microdisks on the Community market
(46) Figures for consumption in the Community are based on information contained in the request for review crossed checked by verified production and sales figures provided by the Community industry and total import volumes obtained from Eurostat.
(47) In Eurostat data however, 3,5" microdisks are only a part of the CN heading concerned. During the analysis period they were deemed to be by far the largest part of this heading since the market for other non-rigid magnetic discs was very small. It has become evident to the Commission that in 1998 and 1999 some recordable CDs (CD-Rs) were misdeclared to the customs authorities under this CN heading. Volume import figures were corrected using detailed confidential statistical information to reflect this misdeclaration.
(48) Consumption fell by 50 % during the analysis period.
>TABLE>
3. Imports from the countries concerned
Preliminary remark
(49) Due to the lack of cooperation, Eurostat and custom information (TARIC) was used to estimate imported volumes. For Hong Kong, the Commission has used custom information concerning the product concerned for 1995 to 1998. For the IP, it was considered that 20 % of the trade registered by Eurostat under the relevant heading could be linked to a misdeclaration of CD-Rs and volumes were corrected accordingly, while prices were estimated as explained at recital 20. For Korea, the custom information was used across the analysis period for volume and prices.
Volume and market share
(50) Based on the above methodology, the development of the volume of imports of 3,5" microdisks into the Community was as follows:
>TABLE>
(51) The volume of imports from Hong Kong fell throughout the analysis period from 89 million units in 1995 to 47 million units during the IP. The market share of imports originating in Hong Kong fell initially from 6,9 % in 1995 to 5,5 % in 1997 before rising again in 1998, to reach 7,3 % in the IP.
(52) The volume of imports from Korea was at low levels throughout the period analysed, falling from 6 million units in 1995 to almost zero in 1997 and rising to just over 1 million units during the IP. Korean market share remained at negligible levels throughout the analysis period.
Price behaviour of exporting producers
(53) The Commission compared average cif prices of imports originating in Hong Kong and in Korea with sales prices of the Community industry in the Community.
(54) The comparison showed, on a weighted average basis, that the prices of the imports originating in Hong Kong were 5 % lower than those of the sales of the Community industry during the IP, and that the prices of the imports originating in Korea were 14 % lower than those of the sales of the Community industry during the IP.
4. Imports from other third countries
(55) During the IP, imports from other countries develop as follow. They were originating mainly in India and Singapore (5,7 % and 8,7 % of market share respectively).
>TABLE>
5. Situation of the Community industry
Production, capacity and capacity utilisation
(56) The production volume of the Community industry decreased by 30 % over the analysis period. Over the same period, the rate of capacity utilisation by the Community industry decreased by 29 percentage points.
>TABLE>
Sales, market share and growth
(57) Sales made by the Community industry decreased by 31 % over the analysis period. Whilst the total Community consumption for 3,5" microdisks has decreased over the analysis period by 50 %, the Community industry's market share has increased by 6 percentage points. This gain reflects a consolidation of the position of the Community industry that was permitted by anti-dumping measures in force.
>TABLE>
Price development
(58) The development of prices of the Community industry for sales to unrelated customers during the analysis period is as follows. Prices fell by 44 % during the analysis period.
>TABLE>
Inventories
(59) Inventories are relatively stable over time and therefore they do not add any relevant information as to the situation of the Community industry.
Profitability, return on investments and cash flow
(60) It was found that throughout the analysis period, the Community industry had recorded financial results (i.e. losses) which were well below the profit rate deemed appropriate for the Community industry in the original investigation. Profit levels for 1995 are not available due to industry restructuring. Overall during the analysis period, losses fell from - 5,57 % in 1996 to - 1,75 % in the IP.
(61) Return on investments has been negative during the analysis period and, in general terms, in line with the trend of profitability. Cash flow has slightly improved in line with profitability.
Employment, productivity, wages
(62) Between 1995 and the IP, the Community industry reduced its labour force by 35 % from 252 to 163 employees. Accordingly, its productivity rose by 9 % during the same period, and total wages decreased by 23 % (wages per employee increased by 19 %).
Investments and ability to raise capital
(63) Production of 3,5" microdisks is capital intensive and production facilities typically operate 24 hours daily all year round. After some insignificant investments made in 1995 and 1996, there has been no new net investment in this industry.
(64) The losses experienced during the analysis period were such that no financing for new investment by the Community industry was possible during the last three years.
Exports by the Community industry
(65) The exports remained stable between 2 and 3 % of total turnover over the analysis period.
Magnitude of dumping margin and recovery from past dumping
(66) The situation of the Community industry improved to a certain extent after the imposition of measures, but it has not completely recovered from past dumping as evidenced mainly from its weak financial situation.
(67) As concerns the impact on the situation of the Community industry of the magnitude of the actual margin of dumping during the IP, this is not considered a relevant factor in the present review examination as the imposition of duties is meant to remedy the dumping found.
Submission by the Hong Kong Economic and Trade Office
(68) The HKETO claimed that the Community industry has had good economic results during the analysis period, taking into account the decrease in demand and that it was not showing any weakness. They also claimed that, because of the declining trend in the volume and value of imports from Hong Kong, it was doubtful that these imports had caused an injury to the Community industry and would do so in the future.
(69) These claims could not be accepted. Indeed, the above analysis describes the situation of the Community industry as improved in a difficult context of declining demand although it is clearly concluded that this industry has not completely recovered from past dumping from Hong Kong and from other countries subject to anti-dumping measures. Moreover, it must be underlined that imports from Hong Kong continued to be dumped as concluded at recital 25 and that Hong Kong was the largest supplier to the Community during the IP, with a market share of more than 7 %. Finally, it must be stressed that, in the context of an expiry review, it is necessary to establish whether there is a likelihood of recurrence of injury should measures expire.
Conclusions
(70) The overall picture that emerges of the situation of the Community industry from the foregoing is that of an industry still in a weak situation, despite an improved market share and a successful effort at significantly reducing costs of production (reduced by 40 % over the period examined). Production methods have been modernised and facilities are now almost entirely automated in order to improve efficiency, maintain market share and maximise profits. However, the Community industry has not yet been able to reach a satisfactory financial situation.
E. LIKELIHOOD OF CONTINUATION AND/OR RECURRENCE OF INJURY
(71) It is recalled that it was concluded at recitals 33 and 37 that a likelihood of a recurrence of dumping in significant volumes existed for both Hong Kong and Korea.
(72) It was concluded above that the Community industry was in a vulnerable situation during the IP.
(73) Should measures expire, dumped imports from Hong Kong and Korea are likely to push down the prices of the Community industry which are already depressed.
(74) In such conditions, the Community industry, already loss-making, would not be able to compete with high quantities sold at such low prices because firstly, a difference in prices in this market (microdisks are a commodity-like product) leads to immediate substitution in supplies and secondly, the Community industry has already made all the necessary efforts to restructure itself and is already operating at very low costs. It is therefore likely that the Community industry would see a further deterioration of its financial situation bringing its very survival into question.
(75) In the light of the above findings it is concluded that the expiry of the measures would be likely to lead to a continuation and/or recurrence of injury for the Community industry.
F. COMMUNITY INTEREST
1. General considerations
(76) The Commission examined whether the maintenance of the anti-dumping measures on 3,5" microdisks would be in the interest of the Community. It has been found that there is a likelihood of continuation and/or recurrence of injurious dumping. The investigation also considered whether or not there are any overriding interests against maintaining the measures and also took account of the past effects of duties on all the various interests involved.
(77) It should be recalled that, in the previous investigation, the adoption of measures was considered not to be against the interest of the Community. It should also be noted that, since this is an expiry review investigation, this investigation should also show the impact of the existing measures in particular on users, consumers and traders.
2. Interests of the Community industry
(78) In view of the conclusions on the situation of the Community industry set out at recital 68, especially in terms of its negative profitability, the Commission considers that, in the absence of measures against injurious dumping, the Community industry is likely to experience a worsening of its financial situation.
(79) The Community industry is viable and capable of supplying the market for a product which, although at a mature stage of its life cycle, constitutes the basic storage device for a large number of computer users. Indeed, the Community industry has shown a willingness to maintain a competitive presence on the Community market. Examples of such steps taken are:
(a) keeping prices at a minimum to maintain its market share;
(b) progression towards greater consolidation;
(c) closure of manufacturing units;
(d) widespread use of modern production techniques (e.g. increased mechanisation and computerisation);
(e) improvements in productivity;
(f) investing in production of other digital storage media products.
(80) It is also to be noted that the production of data storage media is an area of technological importance for the Community as a whole. The production technology and experience gained by the Community industry in 3,5" microdisk production has provided, and will continue to provide, a basis for further innovation in the manufacture of other related data storage media products. For the Community industry, remaining viable in the microdisk sector is the economic basis for participating in the growing market of other storage media.
3. Interests of unrelated importers/traders
(81) Only one unrelated importer cooperated with the investigation. It stated that imports were restricted by the existence of anti-dumping duties. However, it was obvious that imports were still possible for this importer. Should measures be maintained, this company would still be able to source 3,5" microdisks from the countries concerned and from other third countries, including countries not subject to anti-dumping measures.
(82) Furthermore, the limited cooperation of importer in this case leads to the conclusion that the measures in force on imports originating in Hong Kong and Korea did not have any significant impact on the situation of unrelated importers and traders of 3,5" microdisks in the Community.
(83) Therefore, it is concluded that the continuation of the measures is not likely to affect the situation of unrelated importers and traders of 3,5" microdisks in the Community.
4. Interests of component suppliers
(84) Any further shrinking and/or deterioration of the Community industry would not only have negative implications for employment and investment in the industry itself but may have a knock-on effect among the industry's suppliers of inter alia, shells, cookies, shutters, hubs, liners and springs.
(85) The Community producers source the large majority of their materials and components from suppliers located in the Community. Therefore, the continuation of the anti-dumping measures would clearly be in the interest of the Community component supplier industry.
5. Interests of users and consumers
(86) Major users of 3,5" microdisks include duplicators and final consumers. Neither sector has made representations in this review investigation. The Commission, therefore, considers that the findings of the original investigation in this context are still applicable, i.e. the increase in costs applicable to this sector when compared to overall costs can be considered as negligible and would have very little or no impact on prices to retail consumers.
(87) On the contrary the expiry of the measures would seriously threaten the viability of the Community industry, the disappearance of which would reduce supply and competition, to the detriment of duplicators and consumers.
6. Conclusion
(88) After weighing the interests of the various parties involved, the Commission concludes that there are no compelling reasons of Community interest against the continuation of measures.
(89) Due to the long duration of the investigation, it is considered appropriate that the measures be limited to four years.
G. PROPOSED DUTIES
(90) The anti-dumping duties imposed by Regulation (EC) No 2199/94 should be maintained at existing levels, i.e.:
>TABLE>
HAS ADOPTED THIS REGULATION:
Article 1
1. A definitive anti-dumping duty is hereby imposed on imports of 3,5" microdisks used to record and store encoded digital computer information falling within CN code ex 8523 20 90 (TARIC code 8523 20 90*40 ) and originating in Hong Kong and the Republic of Korea, with the exception of 3,5" microdisks based on optically continuous servo tracking technology or magnetic sector servo tracking technology with a storage capacity of 120 MB or more.
2. The rate of definitive anti-dumping duty applicable to the net, free-at-Community-frontier price before duty, shall be as follows for the products manufactured by:
>TABLE>
Article 2
Unless otherwise specified, the provisions in force concerning customs duties shall apply.
Article 3
The anti-dumping duties shall be imposed for a period of four years from the date of entry into force of this Regulation.
Article 4
This Regulation shall enter into force on the day following that of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 18 February 2002.
For the Council
The President
J. Piqué i Camps
(1) OJ L 56, 6.3.1996, p. 1. Regulation as last amended by Regulation (EC) No 2238/2000 (OJ L 257, 11.10.2000, p. 2).
(2) OJ L 236, 10.9.1994, p. 2. Regulation as last amended by Regulation (EC) No 2537/1999 (OJ L 307, 2.12.1999, p. 1).
(3) Regulation (EC) No 2861/93 (OJ L 262, 21.10.1993, p. 4). Regulation as last amended by Regulation (EC) No 2537/1999.
(4) Regulation (EC) No 1821/98 (OJ L 236, 22.8.1998, p. 1). Regulation as last amended by Regulation (EC) No 2537/1999.
(5) OJ C 322, 21.10.1998, p. 4.
(6) Regulation (EC) No 2537/1999 (OJ L 307, 2.12.1999, p. 1).
(7) OJ C 256, 9.9.1999, p. 3.
