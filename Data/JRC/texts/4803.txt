Commission Regulation (EC) No 2082/2005
of 19 December 2005
amending Regulation (EC) No 1497/2001 imposing provisional anti-dumping duties on imports of urea originating in Belarus, Bulgaria, Croatia, Estonia, Libya, Lithuania, Romania and the Ukraine, accepting an undertaking offered by the exporting producer in Bulgaria and terminating the proceeding as regards imports of urea originating from Egypt and Poland
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 384/96 of 22 December 1995 on protection against dumped imports from countries not members of the European Community [1] (the basic Regulation), and in particular Articles 8 and 9 thereof,
Whereas:
A. PREVIOUS PROCEDURE
(1) On 21 October 2000, by means of a notice published in the Official Journal of the European Communities, the Commission announced the initiation of an anti-dumping proceeding [2] in respect of imports of urea (the product concerned) originating in Belarus, Bulgaria, Croatia, Egypt, Estonia, Libya, Lithuania, Poland, Romania and the Ukraine.
(2) This proceeding resulted in provisional anti-dumping duties being imposed in July 2001 on imports of urea originating in Belarus, Bulgaria, Croatia, Estonia, Libya, Lithuania, Romania and the Ukraine and a termination of the proceeding concerning imports of urea originating from Egypt and Poland by Commission Regulation (EC) No 1497/2001 [3].
(3) In the same Regulation, the Commission accepted an undertaking offered by the exporting producer in Bulgaria, Chimco AD. Subject to the conditions set out in Regulation (EC) 1497/2001, imports of the product concerned into the Community from this company were exempted from the said provisional anti-dumping duties, pursuant to Article 3(1) of the same Regulation.
(4) Definitive duties were later imposed on imports of goods originating in Belarus, Bulgaria, Croatia, Estonia, Libya, Lithuania, Romania and the Ukraine by Council Regulation (EC) No 92/2002 [4] (the definitive Regulation). Subject to the conditions set out therein, this Regulation also granted goods produced and directly exported to the first independent customer in the Community by Chimco AD an exemption to the payment of the definitive anti-dumping duties as an undertaking had already been accepted definitively from this company at the provisional stage of the proceeding. As mentioned in recital 137 of the definitive Regulation, the minimum price of the undertaking was adapted due to a change in the injury elimination level.
B. BREACHES OF THE UNDERTAKING
1. Obligations of the company with an undertaking
(5) The undertaking offered by Chimco AD obliges the company concerned, inter alia, to export the product concerned to the Community at or above certain minimum import price levels (MIPs) specified therein. This minimum price level has to be respected on a quarterly weighted average. The company also undertakes not to circumvent the undertaking by making compensatory arrangements with any other party.
(6) Furthermore, in order to allow effective monitoring of the undertaking, Chimco AD is obliged to send to the European Commission a quarterly report of all of its exports sales of the product concerned to the European Community. These reports should include details of all invoices issued during the period for sales made under the terms of the undertaking for which exemption to the anti-dumping duties is sought. The data submitted in these sales reports should be complete and correct in all particulars.
(7) It is stipulated in the undertaking that the European Commission may give further technical instructions for the monitoring of this undertaking and that the company undertakes to cooperate in providing all information considered necessary by the European Commission for the purpose of ensuring compliance with this undertaking.
2. Breaches of the undertaking
(8) The company was informed in December 2003 by the European Commission that a new system would be made available for the company to report the quarterly sales reports to the European Union. This system would be used for the reports from 2004 onwards if the company agreed to it.
(9) In return, the company sent an acceptance declaration of the new reporting system for undertakings and continued offer of an undertaking. Further, the two first reports under the new system have been correctly transmitted to the European Commission.
(10) Reports for the third and fourth quarter have not been received within the framework of the reporting system. The company was informed about this and was granted a sufficient period to remedy this situation. No reaction was received from the company.
(11) Further the company no longer sent any quarterly sales reports for 2005 to the European Commission. The obligation of the company to report their sales to the European Union was therefore not met.
(12) The failure to report in conformity to the technical specifications as well as the non-transmission of the quarterly reports constituted a breach of the undertaking. The company was therefore informed in writing of the essential facts and considerations on the basis of which the Commission intended to withdraw acceptance of the undertaking and to recommend the imposition of the definitive anti-dumping duty. No reaction was received from the company, which is now under a bankruptcy procedure.
C. AMENDMENT OF REGULATION (EC) No 1497/2001
(13) In the light of the above, Article 3 of Regulation (EC) No 1497/2001 accepting an undertaking from Chimco AD should be deleted and Articles 4 and 5 of that Regulation should be renumbered accordingly,
HAS ADOPTED THIS REGULATION:
Article 1
Acceptance of the undertaking offered by Chimco AD is hereby withdrawn.
Article 2
1. Article 3 of Regulation (EC) No 1497/2001 is hereby deleted.
2. Article 4 of Regulation (EC) No 1497/2001 is hereby renumbered "Article 3".
3. Article 5 of Regulation (EC) No 1497/2001 is hereby renumbered "Article 4".
Article 3
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 19 December 2005.
For the Commission
Peter Mandelson
Member of the Commission
[1] OJ L 56, 6.3.1996, p. 1. Regulation as last amended by Regulation (EC) No 461/2004 (OJ L 77, 13.3.2004, p. 12).
[2] OJ C 301, 21.10.2000, p. 2.
[3] OJ L 197, 21.7.2001, p. 4.
[4] OJ L 17, 19.1.2002, p. 1. Regulation as amended by Regulation (EC) No 1107/2002 (OJ L 168, 27.6.2002, p. 1).
--------------------------------------------------
