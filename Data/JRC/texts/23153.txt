COMMISSION REGULATION (EC) No 708/98 of 30 March 1998 on the taking over of paddy rice by the intervention agencies and fixing the corrective amounts and the price increases and reductions to be applied
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 3072/95 of 22 December 1995 on the common organisation of the market in rice (1), as amended by Regulation (EC) No 192/98 (2), and in particular Article 8(b) thereof,
Whereas Regulation (EC) No 3072/95 lays down that the intervention price is fixed for paddy rice of a specific standard quality and that, if the quality of the rice offered for intervention differs from that standard quality, the intervention price is adjusted by applying price increases or reductions;
Whereas Council Regulation (EC) No 3073/95 (3) determines the standard quality of paddy rice for which the intervention price is fixed, tightening up the requirements under the previous arrangements;
Whereas to ensure the satisfactory management of intervention, a minimum quantity should be fixed for each offer; whereas, however, provision should be made for setting a higher limit so that account can be taken of the trading customs and conditions pertaining on the wholesale market in some Member States;
Whereas paddy rice whose quality does not permit suitable use or storage should not be accepted for intervention; whereas in fixing the minimum quality particular consideration should be given to weather conditions in the rice-growing areas of the Community; whereas, in order to allow uniform lots to be taken over; it should be specified that a lot is to be made up of the same rice variety;
Whereas, for the application of the price increases and reductions, account should be taken of the basic characteristics of the paddy rice, thereby allowing objective assessment of its quality; whereas the moisture content, the yield after processing and grain defects can be assessed by simple and effective methods, thus meeting this requirement;
Whereas, to make the intervention scheme as simple and efficient as possible, offers should be submitted to the intervention centre closest to the place where the goods are stored and the provisions on the costs of their transport to the store where the intervention agency takes them over should be laid down;
Whereas the checks to ensure that the requirements on the weight and quality of the goods offered are complied with should be precisely laid down; whereas a distinction must be made between, on the one hand, acceptance of the goods offered after the quantity and compliance with the minimum quality requirements have been checked and, on the other hand, fixing the price to be paid to the applicant after the necessary tests have been carried out to identify the precise characteristics of each lot based on representative samples;
Whereas the specific provisions applying to cases where goods are taken over in the applicant's stores should be laid down; whereas in such cases reference should be made to the applicant's stock accounts, subject to additional checks to ensure that the intervention agency's requirements on taking over are complied with;
Whereas this Regulation is to replace Commission Regulation (EC) No 1528/96 (4); whereas that Regulation should therefore be repealed;
Whereas the Management Committee for Cereals has not delivered an opinion within the time limit set by its chairman,
HAS ADOPTED THIS REGULATION:
Article 1
1. During the buying-in period fixed in Article 4(1) of Regulation (EC) No 3072/95, any holder of a lot of at least 20 tonnes of paddy rice harvested in the Community shall be entitled to offer such rice to the intervention agency. A lot shall be made up of rice of the same variety.
The Member States may fix a higher minimum quantity.
2. Where a lot is delivered in several loads (by lorry, barge, railway wagon, etc.) each load must comply with the requisite minimum characteristics, the last subparagraph of Article 8(1) notwithstanding.
Article 2
1. To be accepted for intervention, paddy rice shall be of sound and fair merchantable quality.
2. Paddy rice shall be deemed to be of sound and fair merchantable quality where it is free of odour and of live insects, and where:
- the moisture content does not exceed the percentage laid down in Annex I,
- the yield after processing is not less than 14 points below the basic yields listed in Annex II,
- the percentage of grains that are not of unimpaired quality as defined in the Annex to Regulation (EC) No 3073/95, the percentage of miscellaneous impurities, the percentage of rice grains of other types and the percentage of other rice varieties does not exceed the following maximum values:
>TABLE>
- the level of radioactivity does not exceed the maximum levels permitted by Community legislation. The rice shall be monitored for radioactive contamination only if the situation so requires and for the necessary period. Where necessary, the duration and scope of the control measures shall be determined in accordance with the procedure laid down in Article 22 of Regulation (EC) No 3072/95.
Article 3
1. Paddy rice containing more than 0,1 % of miscellaneous impurities can be bought in only at a price reduction of 0,02 % for each additional 0,01 % deviation.
Miscellaneous impurities shall mean extraneous matter other than rice consisting of inedible, non-toxic vegetable or mineral material, other edible grains or parts thereof, dead insects and bits thereof.
2. Paddy rice containing more than 3 % of other varieties can be bought into intervention only at a price reduction of 0,1 % for each additional 0,1 % deviation.
3. Where the moisture content of the paddy rice offered for intervention exceeds that laid down for the standard quality of paddy rice, the price reductions to be applied shall be as shown in Annex I.
4. Where the yield after processing of the rice offered for intervention differs from the basic yield after processing for the variety concerned as set out in Part B of Annex II, the price increases and reductions to be applied shall be as shown in Part A of Annex II.
5. Where the grain defects of the paddy rice offered for intervention exceed the tolerances allowed for the standard quality of paddy rice, the price reductions to be applied shall be as shown in Annex III.
6. The above price increases and reductions shall be calculated by applying the percentages shown in the Annexes to the intervention price valid at the beginning of the marketing year. They shall be cumulatively applied.
Article 4
1. All offers for sale into intervention shall be made in writing to an intervention agency in accordance with the form drawn up by it. To be eligible, the offer must contain the following:
- name of the applicant,
- place of storage of the rice offered,
- quantity, variety, main characteristics and year of harvest of the rice,
- intervention centre for which the offer is made.
The application shall also contain a declaration that the product is of Community origin.
For information purposes, the application shall indicate any pesticide treatments carried out, specifying the doses used.
The intervention agency may regard as admissible a written telecommunicated offer, provided that all the above particulars are contained therein. In such cases, the agency can insist, however, that the offer be followed by a written application, either delivered by post or handed in direct. This written application shall be deemed to have been submitted on the day the telecommunication was received.
2. Acceptance of an offer by the intervention agency will be notified to the applicant within 10 working days of its submission.
3. Where an offer is ineligible, the intervention agency shall inform the relevant trader of that fact within 10 working days of the offer's submission.
Article 5
1. All offers shall be made to an intervention agency in respect of the intervention centre nearest to the place where the paddy rice is when the offer is made.
The nearest intervention centre means the centre to which the paddy rice can be transported at the lowest cost. Such cost shall be determined by the intervention agency.
2. The costs of transporting the goods from the depot where they are stored at the time the offer is submitted to the nearest intervention centre as identified in accordance with paragraph 1 shall be borne by the applicant.
3. If the intervention agency does not take the paddy rice over at the nearest intervention centre identified in accordance with paragraph 1, the additional transport costs shall be borne by the intervention agency.
Article 6
1. The intervention agency shall fix the date and intervention centre of delivery and shall notify the applicant of both forthwith. These conditions may be appealed no later than two working days from the receipt of the notification.
The actual taking-over must occur not later than the end of the second month following receipt of the offer and in any case not later than 31 August of the current marketing year. In the case of staggered deliveries, the last part of the lot must be delivered in accordance with this subparagraph.
2. The intervention agency shall take the goods over in the presence of the applicant or his duly authorised agent.
3. The rice offered shall be taken over by the intervention agency where the minimum quantity and characteristics laid down in Articles 1 and 2 have been established by the intervention agency or its authorised agent for the goods delivered to the intervention warehouse, in accordance with Article 8(1).
Where Article 7 applies, the date of taking over shall coincide with the date on which the minimum characteristics mentioned in the take-over record referred to in Article 9 were checked.
4. The quantity delivered shall be established by weight in the presence of the applicant and a representative of the intervention agency, who must have no relationship to the applicant.
5. The storekeeper may represent the intervention agency.
In this case the intervention agency shall itself carry out a check within 30 days of the completion of delivery. The weight at least must be checked, using volumetric measurement.
If volumetric measurement indicates that:
(a) the weight is up to 6 % less than the quantity recorded in the storekeeper's accounts, the storekeeper shall bear all the costs relating to the missing quantities as recorded in any subsequent weighing compared to the weight recorded in the accounts (at taking over);
(b) the weight is over 6 % less than the quantity recorded in the storekeeper's accounts, the goods shall be immediately weighed. The weighing costs involved shall be borne by the storekeeper if the weight established is less than the weight recorded in the accounts. In the opposite case, the intervention agency shall bear the weighing costs.
Article 7
1. The intervention agency can take the paddy rice over at the place where it is stored when the offer is submitted rather than at the intervention centre nominated by the applicant.
2. In such cases, the quantity can be established on the basis of the stock accounts - drawn up to professional specifications and in accordance with the intervention agency's requirements - provided that:
- the accounts indicate the weight established by weighing, the quality characteristics at the moment of weighing, moisture content in particular, any inter-bin transfers, and any treatments carried out. Weighing must have occurred within the previous 10 months,
- the storekeeper declares that the lot offered corresponds in every detail to the information in the accounts.
In this case:
- the weight to be recorded shall be the weight entered in the accounts, adjusted as appropriate to take account of any difference between the moisture content established at weighing and the content established on the basis of the representative sample,
- the intervention agency shall carry out a volumetric check within 30 days of the date of taking over. Any difference between the quantity weighed and the quantity estimated using the volumetric method may not exceed 6 %.
If volumetric measurement indicates that:
- the weight is up to 6 % less than the quantity recorded in the storekeeper's accounts, the storekeeper shall bear all the costs relating to the missing quantities established as such in a subsequent weighing compared to the weight recorded in the accounts (at taking over),
- the weight is over 6 % less than the quantity recorded in the storekeeper's accounts, the storekeeper shall carry out an immediate weighing. The storekeeper shall bear the weighing costs if the weight thus established is less than the weight as recorded. The EAGGF shall bear the costs in the opposite case.
Article 8
1. The quality requirements that must be fulfilled for a product to be taken into intervention shall be checked as follows:
The intervention agency shall take samples in the presence of the applicant or his duly authorised agent. Three samples shall be taken at each sampling, intended respectively for:
- the applicant,
- the store where take over is to take place,
- the intervention agency. This sample shall undergo the agency's own test.
(a) In the case of a delivery, samples shall be taken for each part-delivery (by lorry, barge, railway wagon, etc.), at a ratio of one sample to 10 tonnes of rice.
The requisite conditions shall be checked on the basis of a representative sample of each part delivery. The representative sample shall be made up of the samples intended for the store.
(b) Where Article 7 applies, checking when taking over is at the applicant's store shall be based on a representative sample of the lot offered. This representative sample shall consist of the average result of the samples intended for the store. The number of samples to be taken is obtained by dividing the quantity of the lot offered by 20; a representative sample shall consist of no more than 20 samples, however.
The check should establish that the goods comply with the minimum quality requirements. Should this not be the case, the lot shall not be taken over.
In the case of a delivery, before the lot enters the intervention store the examination of each part-delivery can be restricted to a check on the moisture content and impurity level and verification that no live insects are present. However, if it later becomes apparent when the check is finalised that a part-delivery does not satisfy the minimum quality requirements, the lot shall be refused for take over. The entire lot shall be withdrawn at the applicant's expense.
If the intervention agency in a Member State is able to check all the minimum quality requirements for each part-delivery before it enters the store, it shall refuse take-over of any part-delivery that fails to satisfy these requirements.
2. Where the goods are accepted, once the examination in accordance with paragraph 1 has been carried out the precise characteristics of the goods shall be identified with a view to establishing the price to be paid to the applicant. This price shall be established for the lot offered on the basis of the weighted average of the test results for the representative samples as defined in paragraph 1.
The test results shall be notified to the applicant in the take-over record provided for in Article 9.
3. Should the applicant contest the result of the test carried out to determine the price under paragraph 2, a laboratory approved by the competent authorities shall carry out a further, detailed analysis of the characteristics of the goods using new representative samples made up equally of samples held by the applicant and by the store. The weighted average of the test results for these representative samples shall provide the result.
The result of these analyses shall be final and shall decide the price to be paid to the applicant. The cost of carrying out these new analyses shall be borne by the losing party.
Article 9
A take-over record shall be drawn up by the intervention agency for each lot. The applicant or his representative may be present when the record is being drawn up.
It shall indicate:
- the date on which the quantity and minimum characteristics are checked,
- the variety and the weight delivered,
- the number of samples taken to make up the representative sample,
- the physical and qualitative characteristics noted.
Article 10
1. The price to be paid to the seller shall be that fixed in accordance with Article 4(2) of Regulation (EC) No 3072/95 for goods delivered not unloaded to storage depot and valid on the date fixed as the first day of delivery, account being taken of the provisions of Article 5 on transport costs and the price increases and reductions provided for in Annexes I to III.
Where the rice is taken over at the applicant's stores pursuant to Article 7, the price to be paid shall be established on the basis of the intervention price valid on the date the offer is accepted, adjusted by the applicable price increases and reductions, and reduced by the most advantageous transport costs between the place where the paddy rice is taken over and the nearest intervention centre as defined in Article 5(1), and by the costs of removal from storage. Such costs shall be determined by the intervention agency.
2. Payment shall be made between the 32nd and 37th day following the day of taking over referred to in Article 6(3) of this Regulation. Where Article 8(3) applies, payment shall be made as soon as possible after the results of the last test are notified to the applicant.
Where applicants must submit an invoice before they can be paid and where this invoice is not submitted within the time limit laid down in the preceding subparagraph, payment shall be made within five working days of the actual submission of the invoice.
Article 11
Operators storing the products bought in on the intervention agency's behalf shall regularly monitor their presence and their state of preservation and shall inform the agency forthwith of any problems in this regard.
The intervention agency shall check the quality of the stored product at least once a year. Sampling for this purpose can be done during the annual inventory required by Commission Regulation (EC) No 2148/96 (5).
Article 12
The intervention agencies shall, where necessary, adopt additional procedures and conditions for taking over, compatible with the provisions of this Regulation, to take account of any special conditions prevailing in the Member State to which they belong.
Article 13
Regulation (EEC) No 1528/96 is hereby repealed.
Article 14
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 30 March 1998.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ L 329, 30. 12. 1995, p. 18.
(2) OJ L 20, 27. 1. 1998, p. 16.
(3) OJ L 329, 30. 12. 1995, p. 33.
(4) OJ L 190, 31. 7. 1996, p. 25.
(5) OJ L 288, 9. 11. 1996, p. 6.
ANNEX I
>TABLE>
ANNEX II
A. Price increases and reductions relating to yield after processing
>TABLE>
>TABLE>
B. Basic yield after processing
>TABLE>
ANNEX III
PRICE REDUCTIONS FOR DEFECTIVE GRAINS
1996/97 marketing year:
>TABLE>
From 1997/98:
>TABLE>
