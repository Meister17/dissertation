Decision of the EEA Joint Committee
No 137/2004
of 24 September 2004
amending Protocol 31 to the EEA Agreement, on cooperation in specific fields outside the four freedoms
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular 86 and 98 thereof,
Whereas:
(1) Protocol 31 to the Agreement was amended by Decision of the EEA Joint Committee No 90/2004 of 8 June 2004 [1].
(2) Decision No 1230/2003/EC of the European Parliament and of the Council of 26 June 2003 adopting a multiannual programme for action in the field of energy: "Intelligent Energy — Europe" (2003-2006) [2], was incorporated into the Agreement by Decision of the EEA Joint Committee No 164/2003 of 7 November 2003 [3].
(3) It is appropriate to extend the cooperation of the Contracting Parties to the Agreement to include the programme's specific field "COOPENER" and actions pursuant thereto.
(4) Protocol 31 to the Agreement should therefore be amended in order to allow for this extended cooperation to take place from 1 January 2005,
HAS DECIDED AS FOLLOWS:
Article 1
The following indent shall be added in paragraph 2e of Article 14 of Protocol 31 to the Agreement:
- "As from 1 January 2005 the EFTA States shall participate in the specific field "COOPENER" and actions pursuant thereto in the Community programme referred to in paragraph 5(g)."
Article 2
This Decision shall enter into force on 25 September 2004, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [4].
It shall apply from 1 January 2005.
Article 3
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 24 September 2004.
For the EEA Joint Committee
The President
Kjartan Jóhannsson
[1] OJ L 349, 25.11.2004, p. 52.
[2] OJ L 176, 15.7.2003, p. 29.
[3] OJ L 41, 12.2.2004, p. 67.
[4] Constitutional requirements indicated.
--------------------------------------------------
