REGULATION (ECSC, EEC, EURATOM) No 1544/73 OF THE COUNCIL of 4 June 1973 amending Regulation (EEC, Euratom, ECSC) No 260/68 laying down the conditions and procedure for applying the tax for the benefit of the European Communities
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing a Single Council and a Single Commission of the European Communities;
Having regard to the Protocol on the Privileges and Immunities of the European Communities, and in particular Article 13 thereof;
Having regard to the proposal from the Commission;
Whereas it is necessary to amend Council Regulation (EEC, Euratom, ECSC) No 260/68 (1) of 29 February 1968 laying down the conditions and procedure for applying the tax for the benefit of the European Communities, as last amended by Regulation (ECSC, EEC, Euratom) No 559/73 (2), in order to take account of Council Regulation (ECSC, EEC, Euratom) No 1543 (3) of 4 June 1973, introducing special temporary measures applicable to officials of the European Communities paid from research and investment funds;
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EEC, Euratom, ECSC) No 260/68 shall be amended as follows: (a) Article 2 shall be supplemented by a sixth and seventh indent as follows:
" - those entitled to the allowance for termination of service under Articles 3 and 4 of Regulation (ECSC, EEC, Euratom) No 1543;
- those entitled to the allowance for termination of service under Article 5 of Regulation (ECSC, EEC, Euratom) No 1543.
"
(b) Article 6 (1) (b) shall be supplemented by the following:
"These provisions shall apply also to payments made pursuant to Article 5 of Regulation (ECSC, EEC, Euratom) No 1543."
Article 2
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Luxembourg, 4 June 1973.
For the Council
The President
R. VAN ELSLANDE (1)OJ No L 56, 4.3.1968, p. 8. (2)OJ No L 55, 28.2.1973, p. 4. (3)See p. 1 of this Official Journal.
