Commission Communication
recognising the obsolescence of Commission Regulation (EEC) No 2677/75 of 6 October 1975 applying Council Regulation (EEC) No 3254/74 of 17 December 1974, applying Regulation (EEC) No 1055/72 on notifying the Commission of imports of crude oil and natural gas, to petroleum products falling within subheadings 27.10 A, B, C I and C II of the Common Customs Tariff
(2005/C 311/04)
In accordance with the guidelines on the reduction of the active Community acquis, and in the context of simplifying Commission acts, Commission Regulation (EEC) No 2677/75 is hereby removed from the active Community acquis and consequently will no longer appear in the Directory of Community legislation in force.
--------------------------------------------------
