P6_TA(2005)0028
Cooperation Agreement with the Principality of Andorra ***
European Parliament legislative resolution on the proposal for a Council decision on the signature and conclusion, on behalf of the European Community, of a Cooperation Agreement with the Principality of Andorra (COM(2004)0456 — C6-0214/2004 — 2004/0136(AVC))
(Assent procedure)
The European Parliament,
- having regard to the proposal for a Council decision (COM(2004)0456) [1],
- having regard to the draft Cooperation Agreement between the European Community and the Principality of Andorra,
- having regard to the request for assent submitted by the Council pursuant to Article 300(3), second subparagraph, in conjunction with Article 310 of the EC Treaty (C6-0214/2004),
- having regard to Rules 75 and 83(7) of its Rules of Procedure,
- having regard to the recommendation of the Committee on Foreign Affairs (A6-0014/2005),
1. Gives its assent to conclusion of the agreement;
2. Instructs its President to forward its position to the Council, the Commission and the governments and parliaments of the Member States and of the Principality of Andorra.
[1] Not yet published in OJ.
--------------------------------------------------
