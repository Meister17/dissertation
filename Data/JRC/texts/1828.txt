[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 09.09.2005
COM(2005) 418 final
Proposal for a
COUNCIL REGULATION
amending Regulation (EC) No 2604/2000 imposing definitive anti-dumping duties on imports of certain polyethylene terephthalate (PET) originating, inter alia, in India
(presented by the Commission)
EXPLANATORY MEMORANDUM
CONTEXT OF THE PROPOSAL |
110 | Grounds for and objectives of the proposal This proposal concerns the application of Council Regulation (EC) No 384/96 of 22 December 1995 on protection against dumped imports from countries not members of the European Community, as last amended by Council Regulation (EC) No 461/2004 of 8 March 2004 ("the basic Regulation") in the proceeding concerning the definitive anti-dumping measures imposed by Council Regulation (EC) No 2604/2000 on imports of certain polyethylene terephthalate (PET) originating, inter alia, in India |
120 | General context This proposal is made in the context of the implementation of the basic Regulation and is the result of an investigation which was carried out in line with the substantive and procedural requirements laid out in the basic Regulation. |
139 | Existing provisions in the area of the proposal There are no existing provisions in the area of the proposal. |
141 | Consistency with other policies and objectives of the Union Not applicable. |
CONSULTATION OF INTERESTED PARTIES AND IMPACT ASSESSMENT |
Consultation of interested parties |
219 | Interested parties concerned by the proceeding have already had the possibility to defend their interests during the investigation, in line with the provisions of the basic Regulation. |
Collection and use of expertise |
229 | There was no need for external expertise. |
230 | Impact assessment This proposal is the result of the implementation of the basic Regulation. The basic Regulation does not foresee a general impact assessment but contains an exhaustive list of conditions that have to be assessed. |
LEGAL ELEMENTS OF THE PROPOSAL |
305 | Summary of the proposed action In November 2000, the Council, by Regulation, imposed definitive anti-dumping duties of EUR 181,7 per tonne on imports into the Community of PET originating, inter alia, in India. The Commission received an application for a ‘new exporter’ review by South Asian Petrochem Limited, a new exporting producer in India. The Commission initiated on 12 January 2005 a ‘new exporter’ review with a view to determine if and to what extent the imports of PET produced and exported to the Community by South Asian Petrochem Limited should be subject to the anti-dumping duties currently in force. The investigation revealed the existence of dumping. Taking into account the findings of a parallel anti-subsidy review investigation, a specific anti-dumping duty rate of EUR 88,9 per tonne was established for South Asian Petrochem Limited. Consequently, it is proposed to amend Council regulation imposing anti-dumping measures on imports of PET originating, inter alia, in India by including the specific duty rate established for South Asian Petrochem Limited and by accepting the price undertaking offered . It is therefore proposed that the Council adopt the attached proposal for a Regulation which should be published in the Official Journal of the European Union no later than 11 October 2005. |
310 | Legal basis Council Regulation (EC) No 384/96 of 22 December 1995 on protection against dumped imports from countries not members of the European Community, as last amended by Council Regulation (EC) No 461/2004 of 8 March 2004. |
329 | Subsidiarity principle The proposal falls under the exclusive competence of the Community. The subsidiarity principle therefore does not apply. |
Proportionality principle The proposal complies with the proportionality principle for the following reason(s). |
331 | The form of action is described in the above-mentioned basic Regulation and leaves no scope for national decision. |
332 | Indication of how financial and administrative burden falling upon the Community, national governments, regional and local authorities, economic operators and citizens is minimized and proportionate to the objective of the proposal is not applicable. |
Choice of instruments |
341 | Proposed instruments: regulation. |
342 | Other means would not be adequate for the following reason(s). The above-mentioned basic Regulation does not foresee alternative options. |
BUDGETARY IMPLICATION |
409 | The proposal has no implication for the Community budget. |
1. Proposal for a
COUNCIL REGULATION
amending Regulation (EC) No 2604/2000 imposing definitive anti-dumping duties on imports of certain polyethylene terephthalate (PET) originating, inter alia, in India
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 384/96 of 22 December 1995 on protection against dumped imports from countries not members of the European Community[1] (‘the basic Regulation’), and in particular Article 11(4) thereof,
Having regard to the proposal submitted by the Commission[2] after consulting the Advisory Committee,
Whereas:
A. MEASURES IN FORCE
(1) The Council, by Regulation (EC) No 2604/2000[3], imposed a definitive anti-dumping duty on imports of certain polyethylene terephthalate (‘PET’) with a coefficient of viscosity of 78 ml/g or higher, according to DIN (Deutsche Industrienorm) 53728 (‘the product concerned’) normally declared within CN code 3907 60 20 originating, inter alia, in India. The measures take the form of a specific duty rate of EUR 181,7 per tonne with the exception of imports from several companies expressly mentioned which are subject to individual duty rates.
(2) The Council, by Regulation (EC) No 2603/2000[4], imposed at the same time a definitive countervailing duty of EUR 41,3 per tonne on imports into the Community of the same product originating in India, with the exception of imports from several companies expressly mentioned which are subject to individual duty rates.
B. CURRENT PROCEDURE
1. Request for a review
(3) Subsequent to the imposition of definitive measures, the Commission received an application to initiate a ‘new exporter’ review of Regulation (EC) No 2604/2000, pursuant to Article 11(4) of the basic Regulation, from an Indian exporting producer, i.e. South Asian Petrochem Limited (‘the company’). The company claimed that it was not related to any of the exporting producers in India subject to the anti-dumping measures in force with regard to the product concerned. Furthermore, it claimed that it had not exported the product concerned during the original period of investigation (1 October 1998 to 30 September 1999), but had exported the product concerned to the Community after that period.
2. Initiation of a ‘new exporter’ review
(4) The Commission examined the evidence submitted by the company and considered it sufficient to justify the initiation of a review in accordance with Article 11(4) of the basic Regulation. After the consultation of the Advisory Committee and after the Community industry concerned had been given the opportunity to comment, the Commission initiated, by Regulation (EC) No 33/2005[5], a review of Regulation (EC) No 2604/2000 with regard to the company and commenced its investigation.
(5) Pursuant to the Commission Regulation initiating the review, the anti-dumping duty imposed by Regulation (EC) No 2604/2000 was repealed with regard to imports of the product concerned produced and exported to the Community by the company. Simultaneously, customs authorities were directed, pursuant to Article 14(5) of the basic Regulation, to take appropriate steps to register such imports.
(6) At the same time and on the same grounds, following a request from the company, the Commission initiated an accelerated review of Council Regulation (EC) No 2603/2000[6] pursuant to Article 20 of Council Regulation (EC) No 2026/97[7].
3. Parties concerned
(7) The Commission officially informed the company and representatives of India (‘the exporting country’) of the initiation of the ‘new exporter’ review. Furthermore, it gave other parties directly concerned the opportunity to make their views known in writing and to request a hearing. However, no such request was received by the Commission.
(8) The Commission sent a questionnaire to the company and received a reply within the deadline. The Commission also sought and verified all the information deemed necessary for the determination of dumping. A verification visit was carried out at the premises of the company.
4. Investigation period
(9) The investigation of dumping covered the period from 1 October 2003 to 30 September 2004 (‘the investigation period’).
C. RESULT OF THE INVESTIGATION
1. New exporter qualification
(10) The investigation confirmed that the company had not exported the product concerned during the original period of investigation and that it had begun exporting to the Community after this period.
(11) Furthermore, the company was able to satisfactorily demonstrate that it did not have any links, direct or indirect, with any of the Indian exporting producers subject to the anti-dumping measures in force with regard to the product concerned.
(12) Accordingly, it is confirmed that the company should be considered a ‘new exporter’ in accordance with Article 11(4) of the basic Regulation, and thus an individual margin should be determined for it.
2. Dumping
Normal value
(13) In accordance with Article 2(2) of the basic Regulation, the Commission first examined whether the company’s total domestic sales of PET were representative in comparison with its total export sales to the Community. Since these sales amounted to more than 5% of its total export sales volume to the Community, they were considered representative.
(14) The Commission subsequently identified those types of PET sold domestically by the company that were identical or directly comparable to the types sold for export to the Community.
(15) The investigation revealed that only two product types exported to the Community are identical or directly comparable to the products sold on the domestic market. For each of these two product types it was then examined whether domestic sales were sufficiently representative with respect to the corresponding export sales. Since the domestic sales of each of these types were significantly above the 5% threshold, both product types were considered representative.
(16) An examination was also made as to whether the domestic sales of each product type could be regarded as having been made in the ordinary course of trade, by establishing the proportion of the sales volume of the like product sold at a net sales price equal to or above the cost of production (profitable sales) to independent customers of the type in question. Since the volume of profitable sales of the product concerned represented less than 80% but 10% or more of the total sales volume, normal value was based on the actual domestic price, calculated as a weighted average of profitable sales of each type only.
Export price
(17) Since all export sales of the product concerned to the Community were made directly to independent customers in the Community, the export price was established in accordance with Article 2(8) of the basic Regulation, namely on the basis of export prices actually paid or payable.
Comparison
(18) For the purpose of ensuring a fair comparison between normal value and export price, due allowance in the form of adjustments was made for differences affecting price comparability in accordance with Article 2(10) of the basic Regulation.
(19) All the allowances claimed by the company on export sales have been accepted. These allowances are related to commissions, inland freight, ocean freight, insurance, handling charges, packaging expenses and bank charges.
(20) On domestic sales, allowances claimed for commissions, inland freight, insurance, packaging expenses and bank charges were accepted. However, allowances claimed by the company for indirect tax and import charges on the basis of Article 2(10)(b) of the basic Regulation and branch office expenses on the basis of Article 2(10)(k) of the basic Regulation were rejected for the reasons set out below.
(21) The claimed allowance for indirect taxes is based on the argument that the company’s domestic customers paid a non-recoverable amount of excise duty when purchasing the product concerned on the domestic market, whereas the company’s export customers were not subject to such duty. This non-recoverable excise duty amount was claimed as an adjustment to normal value. However, the normal value which was compared to the export price was established on the basis of the net domestic sales price excluding all taxes. Therefore, normal value did not include any excise duty having an impact on price and price comparability within the meaning of Article 2(10) of the basic Regulation. Furthermore, it was considered that the tax liability of the exporting producer’s domestic customers does not qualify for an adjustment under Article 2(10)(b) of the basic Regulation since such duty is not ‘borne by the like product and by materials physically incorporated therein’ . The excise duty paid by the company’s domestic customers is charged upon the company’s net sales price and does not have any impact on the company’s production cost and price setting. As any difference in indirect duties on the domestic and the export market has already been taken fully into consideration by comparing the company’s net domestic sales prices with its net export sales prices, the company’s claim for allowance was rejected.
(22) Upon disclosure, the company argued that it would be irrelevant whether the comparison between normal value and export price was made on the basis of net prices, i.e. excluding all indirect taxes. It further argued that the excise duty would be borne by the like product and that it affected price comparability, insofar that domestic clients would not be fully reimbursed and had ultimately to pay a portion of the excise duty. Therefore, its clients would pay a higher price on the domestic market than customers on the export market. However, as mentioned above, the domestic price used as a normal value already excluded the excise duty and therefore, price comparability could not have been affected. Furthermore, the company did not submit any information or evidence showing that the comparability of the normal value and the export price was otherwise affected. These arguments had consequently to be rejected.
(23) The claimed allowance for import duty exemption was based on the argument that whenever the company sells the product concerned on the domestic market, import duties on raw materials would become payable in the form of an “increased” excise duty. The term “increased” excise duty refers to a different tax scheme applicable for the company as it is set up as an Export Oriented Unit (‘EOU’), compared to other Indian (non-Export Oriented Unit) companies. Under this scheme, EOU companies were exempted from all import duties on raw materials, but subject to a higher excise duty rate in case goods produced by these companies were sold on the domestic market. Since no such excise duty is payable on export sales, the company requested the normal value to be adjusted accordingly. The claim was rejected as the company purchased raw materials duty free, regardless of whether the final product is sold domestically or is being exported. Therefore, no import duties were borne by the like product and by materials physically incorporated therein when intended for consumption on the domestic market, and not collected or refunded in respect of the product exported to the Community as required by Article 2(10)(b) of the basic Regulation. Thus, price comparability between the domestic and export market was not affected. It is also noted that the company was unable to prove the payment of any additional duty or indirect tax other than the excise duty on sales of the finished product which has been described above in recital (21). It was finally not possible in any event to clearly identify whether and how much of the imported or locally purchased raw material was used in the production of the final product.
(24) The company also claimed an allowance for the expenses of their local branch offices in charge of sales on the domestic market. The claim was rejected as these branch office expenses also included selling, general and administrative expenses for the sales of products other than the product concerned and could furthermore not be directly linked to the sales of the product concerned on the domestic market. The company consequently did not show whether the expenses of the branch offices had an impact on price or price comparability. Upon disclosure the company argued that it is producing only one product which is the product concerned. However, this contradicted the findings. Moreover and in accordance with Article 2(1) of the basic Regulation, what is relevant for the purpose of determining normal value is the sales price from the branch offices to the first independent customer. As the branch offices are part of the same legal entity and company structure, the company’s arguments were rejected and the claim for this allowance not warranted.
Dumping margin
(25) In accordance with Article 2(11) of the basic Regulation, the weighted average normal value of each type of the product concerned exported to the Community was compared to the weighted average export price of each corresponding type of the product concerned.
(26) The comparison showed the existence of dumping. The weighted average dumping margin established for the company, expressed as a percentage of the CIF Community-frontier price amounts to 25,5 %.
D. AMENDMENT OF THE MEASURES BEING REVIEWED
(27) In the light of the results of the investigation, it is considered that a definitive anti-dumping duty should be imposed at the level of the dumping margin found, but, in accordance with Article 9(4) of the basic Regulation should not be higher than the county-wide injury margin established for India in the investigation which lead to the imposition of the existing measures.
(28) No individual injury margin can be established in a new exporter review since the investigation, pursuant to Article 11(4) of the basic Regulation, is limited to the examination of the individual dumping margin. Therefore, the dumping margin was compared to the country-wide injury margin as established for India by the definitive Regulation. Since the latter was higher than the dumping margin, the level of the measures should be based on the dumping margin.
(29) In accordance with Article 14(1) of the basic Regulation and Article 24(1) of Council Regulation (EC) No 2026/97, no product shall be subject to both anti-dumping and countervailing duties for the purpose of dealing with one and the same situation arising from dumping or from export subsidisation.
(30) In the parallel accelerated review of Council Regulation (EC) No 2603/2000, an individual countervailing duty rate of EUR 106,5 per tonne corresponding to an ad valorem countervailing duty rate of 13,9 % was established for the company.
(31) As all of the subsidies in the parallel accelerated review were found to be export subsidies, the anti-dumping duty needs to be adjusted to reflect the actual dumping margin remaining after the imposition of the countervailing duties offsetting the effect of these subsidies.
(32) Accordingly, the anti-dumping duty applicable to the CIF Community-frontier price and taking into account the results of the parallel anti-subsidy proceeding, shall be:
Company | Injury Margin | Dumping Margin | Counter-vailing duty rate | Anti-dumping duty rate | Proposed anti-dumping duty (euro/tonne) |
South Asian Petrochem Limited | 44,3 % | 25,5 % | 13,9 % | 11,6% | 88,9 |
E. RETROACTIVE LEVYING OF THE ANTI-DUMPING DUTY
(33) As the review has resulted in a determination of dumping in respect of the company, the anti-dumping duty applicable to the company should be levied retroactively on imports of the product concerned which have been made subject to registration pursuant to Article 3 of Commission Regulation (EC) No 33/2005.
F. UNDERTAKING
(34) The company offered a price undertaking concerning its exports of the product concerned to the Community, in accordance with Article 8(1) of the basic Regulation.
(35) After examination of the offer, the Commission considered the undertaking as acceptable since it would eliminated the injurious effects of dumping. Moreover, the regular and detailed reports which the company undertook to provide to the Commission will allow effective monitoring. Furthermore, the nature of the product and the sales structure of the company is such that the Commission considers that the risk of circumvention of the undertaking is limited.
(36) In order to ensure the effective respect and monitoring of the undertaking, when the request for release for free circulation pursuant to the undertaking is presented, exemption from the duty is conditional, upon presentation to the customs service of the Members State concerned, of a valid ‘Commercial Invoice’ issued by the company and containing information listed in the Annex to Council Regulation (EC) No 2604/2000. Where no such invoice is presented, or when it does not correspond to the product presented to customs, the appropriate rate of anti-dumping duty should be payable in order to ensure the effective application of the undertaking.
(37) In the event of a breach or withdrawal of the undertaking, an anti-dumping duty may be imposed pursuant to Article 8 (9) and (10) of the basic Regulation.
G. DISCLOSURE AND DURATION OF THE MEASURES
(38) The company was informed of the facts and considerations on the basis of which it was intended to impose a definitive anti-dumping duty on its imports into the Community and was given the opportunity to comment.
(39) This review does not affect the date on which Council Regulation (EC) No 2604/2000 will expire pursuant to Article 11(2) of the basic Regulation.
H. PROPOSED OPERATIVE PART
HAS ADOPTED THIS REGULATION:
Article 1
1. Article 1(3) of Council Regulation (EC) No 2604/2000 is hereby amended by adding the following to the section headed ‘India’:
Country | Company | Definitive duty (euro/tonne) | TARIC additional code |
India | South Asian Petrochem Limited | 88,9 | A585 |
2. The duty hereby imposed shall also be levied retroactively on imports of the product concerned which have been registered pursuant to Article 3 of Commission Regulation (EC) No 33/2005.
3. Notwithstanding paragraph 1, the definitive anti-dumping duty shall not apply to imports released for free circulation in accordance with Article 2.
4. Unless otherwise specified, the provisions in force concerning customs duties shall apply.
Article 2
Article 2(3) of Council Regulation (EC) No 2604/2000 is hereby amended by adding the following in the table:
Company | Country | TARIC additional code |
South Asian Petrochem Limited | India | A585 |
Article 3
This Regulation shall enter into force on the day following that of its publication in the Official Journal of the European Union .
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels,
For the Council
The President
[1] OJ L 56, 6.3.1996, p. 1. Regulation as last amended by Regulation (EC) No 461/2004 (OJ L 77 13.3.2004, p. 12).
[2] OJ C […] […], p. […]
[3] OJ L 301, 30.11.2000, p. 21. Regulation as last amended by Regulation (EC) No 823/2004 (OJ L 127 29.4.2004, p. 7).
[4] OJ L 301, 30.11.2000, p. 1. Regulation as last amended by Regulation (EC) No 822/2004 (OJ L 127 29.4.2004, p. 3).
[5] OJ L 8, 12.1.2005, p. 9.
[6] OJ C 8, 12.1.2005, p. 2.
[7] OJ L 288, 21.10.1997, p. 1. Regulation as last amended by Council Regulation (EC) No 461/2004 (OJ L 77, 13.3.2004, p. 12).
