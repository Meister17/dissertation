COUNCIL REGULATION (EC) No 408/97 of 24 February 1997 on the conclusion of an Agreement on cooperation in the sea fisheries sector between the European Community and the Islamic Republic of Mauritania and laying down provisions for its implementation
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 43, in conjunction with Article 228 (2) and the first subparagraph of Article 228 (3) thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament (1),
Whereas, on 20 June 1996, the Community and the Islamic Republic of Mauritania initialled an Agreement on cooperation in the sea fisheries sector which provides fishing opportunities for Community fishermen in waters over which Mauritania has sovereignty or jurisdiction;
Whereas it is in the Community's interest to approve this Agreement;
Whereas, in order to manage them efficiently, the fishing opportunities available to the Community in Mauritania's fishing zone should be allocated between the Member States;
Whereas the fishing activities covered by this Regulation are subject to the controls provided for in Council Regulation (EEC) No 2847/93 of 12 October 1993 establishing a control system applicable to the common fisheries policy (2);
Whereas, to ensure implementation of the said Agreement, it is necessary for the Member States to ensure that shipowners comply with their obligations and provide the Commission with all relevant information;
Whereas in accordance with Regulation (EC) No 3317/94 (3) and with the arrangements agreed in the aforementioned Agreement, the flag Member State and the Commission have to ensure that applications for fishing licences comply with those arrangements and the Community rules applicable,
HAS ADOPTED THIS REGULATION:
Article 1
The Agreement on cooperation in the sea fisheries sector between the European Community and the Islamic Republic of Mauritania, hereinafter referred to as 'the Agreement`, is hereby approved on behalf of the Community.
The text of the Agreement is attached to this Regulation (4).
Article 2
The fishing opportunities arising from the provisional application of the Agreement shall be allocated according to the table in the Annex to this Regulation. As far as cephalopods are concerned, the annual allocation of the opportunities between Member States as from 1 August 1997 will be decided upon by 30 June each year according to the procedure provided for in Article 18 of Regulation (EEC) No 3760/92 (5).
Where, in a fishing category, a Member State draws up licence applications for less than its allocated tonnage, the Commission shall offer shipowners from the other Member States the opportunity to submit applications.
Article 3
1. The Member States shall:
(a) check that the data given on the licence application forms provided for in Appendix 1 to Annex I to the Agreement match those in the Community register of fishing vessels established by Commission Regulation (EC) No 109/94 (6), and report to the Commission any changes in those data at the time of subsequent applications.
They shall likewise verify the assurance of the other data necessary for the drawing-up of licences;
(b) submit licence applications to the Commission in accordance with Article 3 (1) of Regulation (EC) No 3317/94, no later than two working days before the deadline laid down in point 2.1 of Chapter II of Annex I to the Agreement;
(c) provide the Commission each month with a list of vessels whose licences have been suspended with, by port, the date on which a licence was handed over and the date on which it was restored;
(d) transmit to the Commission the summaries of the inspection reports referred to in point 2 of Chapter IV of Annex II to the Agreement. The summaries shall describe the inspections carried out, the results obtained and the action taken;
(e) transmit to the Commission each month a copy of the scientific observers' reports provided for in point 14 of Chapter V of Annex II to the Agreement;
They shall notify the Commission immediately of any infringements revealed by the information contained in these reports and the action taken;
They shall enter the scientific data contained in these reports in an electronic database. The Commission shall have access to these databases;
(f) transmit to the Commission, and at the same time to Mauritania's competent authorities, a copy of the notice of the inspection missions planned under point 4 of Chapter VI of Annex II to the Agreement and, where relevant, of the notification that an observer will be taking part;
They shall transmit to the Commission a copy of the reports of the observers appointed by their supervisory authorities pursuant to point 3 of Chapter VI of Annex II to the Agreement;
(g) adopt the provisions needed to take appropriate action and initiate administrative proceedings, as provided for in point 15 of Chapter V of Annex II to the Agreement.
Article 4
The President of the Council shall, on behalf of the Community, give the notification provided for in Article 16 of the Agreement (7).
Article 5
This Regulation shall enter into force on the seventh day following that of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 24 February 1997.
For the Council
The President
H. VAN MIERLO
(1) OJ No C 380, 16. 12. 1996.
(2) OJ No L 261, 20. 10. 1993, p. 1. Regulation as last amended by Regulation (EC) No 2870/95 (OJ No L 301, 14. 12. 1995, p. 1).
(3) Council Regulation (EC) No 3317/94 of 22 December 1994 laying down general provisions concerning the authorization of fishing in the waters of a third country under a fisheries agreement (OJ No L 350, 31. 12. 1994, p. 13).
(4) For the text of the Agreement see OJ No L 334, 23. 12. 1996, p. 20.
(5) Council Regulation (EEC) No 3760/92 of 20 December 1992 establishing a Community system for fisheries and aquaculture (OJ No L 389, 31. 12. 1992, p. 1). Regulation as amended by the 1994 Act of Accession.
(6) Commission Regulation (EC) No 109/94 of 19 January 1994 concerning the fishing vessel register of the Community (OJ No L 19, 22. 1. 1994, p. 5). Regulation as amended by Regulation (EC) No 493/96 (OJ No L 72, 21. 3. 1996, p. 12).
(7) The date of entry into force of the Agreement will be published in the Official Journal of the European Communities by the General Secretariat of the Council.
ANNEX
Provisional allocation of fishing opportunities between Member States
>TABLE>
