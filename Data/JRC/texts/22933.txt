COMMISSION DIRECTIVE of 19 March 1991 amending for the fifth time the Annex to Council Directive 79/117/EEC prohibiting the placing on the market and use of plant protection products containing certain active substances (91/188/EEC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Directive 79/117/EEC of 21 December 1978 prohibiting the placing on the market and use of plant protection products containing certain active substances (1), as last amended by Directive 90/533/EEC (2), and in particular Article 6 thereof,
Whereas the development of scientific and technical knowledge makes necessary certain amendments to the Annex to Directive 79/117/EEC;
Whereas it seems desirable to delete the remaining temporary derogations from the prohibitions laid down in the Directive since less hazardous treatments are now available;
Whereas all Member States have informed the Commission that they do not intend or no longer intend to avail themselves of these derogations;
Whereas the measures provided for in this Directive are in accordance with the opinion of the Standing Committee on Plant Health,
HAS ADOPTED THIS DIRECTIVE: Article 1
In Part A of the Annex to Directive 79/117/EEC, 'Mercury compounds', the text in column 2 is hereby deleted. Article 2
Member States shall, not later than 31 March 1992, bring into force the laws, regulations and administrative provisions necessary to comply with this Directive. They shall immediately inform the Commission thereof.
When Member States adopt these measures, these shall contain a reference to this Directive or shall be accompanied by such reference at the time of their official publication. The procedure for such reference shall be laid down by Member States. Article 3
This Directive is addressed to the Member States.
Done at Brussels, 19 March 1991. For the Commission
Ray MAC SHARRY
Member of the Commission (1) OJ No L 33, 8. 2. 1979, p. 36. (2) OJ No L 296, 27. 10. 1990, p. 63.
