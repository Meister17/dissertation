Acknowledgement of receipt of complaint No 2005/4347 — SG(2005)A/4763
(2005/C 190/05)
1. The European Commission has registered a complaint, regarding the management of water in the Lago d'Idro area, with reference to a site of relevance under Council Directive 92/43/EEC of 21 May 1992 on the conservation of natural habitats and of wild fauna and flora, as number No 2005/4347 in the register of complaints.
2. The Commission has received more than 35 copies of this complaint. Therefore, in order to ensure a rapid response to and information of those concerned while making the most economical use of its administrative resources, it is publishing this acknowledgment of receipt in the Official Journal of the European Communities and on the Internet at:
http://europa.eu.int/comm/secretariat_general/sg1/receipt/
3. The complaint will be examined by the Commission in the light of the applicable Community legislation. The complainants will be kept informed, by any means, including eventually by publication in the Official Journal of the European Communities and/or on the Internet, of the results of this examination and of any follow-up action that the Commission may decide to take.
4. The Commission will endeavour to take a decision on the substance of the case (opening of infringement proceedings or closure of the case) within 12 months from the date of registration of the complaint by the Secretariat-General.
5. Should the Commission need to contact the Italian authorities, it will do so without mentioning the identity of the complainants in order to protect their rights. The complainants may, however, authorise the Commission to reveal their identity in any contacts with the Italian authorities.
--------------------------------------------------
