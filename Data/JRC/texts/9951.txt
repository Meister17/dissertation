Decision of the EEA Joint Committee
No 83/2006
of 7 July 2006
amending Annex II (Technical regulations, standards, testing and certification) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex II to the Agreement was amended by Decision of the EEA Joint Committee No 7/2006 of 27 January 2006 [1].
(2) Commission Decision 2005/718/EC of 13 October 2005 on compliance of certain standards with the general safety requirement of Directive 2001/95/EC of the European Parliament and of the Council and the publication of their references in the Official Journal [2] is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
The following point shall be inserted after point 3i (Commission Decision 2005/323/EC) of Chapter XIX of Annex II to the Agreement:
"3j. 32005 D 0718: Commission Decision 2005/718/EC of 13 October 2005 on compliance of certain standards with the general safety requirement of Directive 2001/95/EC of the European Parliament and of the Council and the publication of their references in the Official Journal (OJ L 271, 15.10.2005, p. 51)."
Article 2
The text of Decision 2005/718/EC in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 8 July 2006, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [3].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 7 July 2006.
For the EEA Joint Committee
The President
Oda Helen Sletnes
[1] OJ L 92, 30.3.2006, p. 28.
[2] OJ L 271, 15.10.2005, p. 51.
[3] No constitutional requirements indicated.
--------------------------------------------------
