Commission Regulation (EC) No 279/2005
of 18 February 2005
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 19 February 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 18 February 2005.
For the Commission
J. M. Silva Rodríguez
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 1947/2002 (OJ L 299, 1.11.2002, p. 17).
--------------------------------------------------
ANNEX
to Commission Regulation of 18 February 2005 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code | Standard import value |
07020000 | 052 | 133,2 |
204 | 79,2 |
212 | 167,7 |
624 | 181,1 |
628 | 104,0 |
999 | 133,0 |
07070005 | 052 | 180,3 |
068 | 116,3 |
204 | 68,5 |
999 | 121,7 |
07091000 | 220 | 42,9 |
999 | 42,9 |
07099070 | 052 | 213,7 |
204 | 217,9 |
999 | 215,8 |
08051020 | 052 | 38,1 |
204 | 42,3 |
212 | 53,1 |
220 | 37,9 |
421 | 30,9 |
448 | 35,8 |
624 | 64,5 |
999 | 43,2 |
08052010 | 204 | 82,6 |
624 | 80,9 |
999 | 81,8 |
08052030, 08052050, 08052070, 08052090 | 052 | 42,7 |
204 | 89,7 |
220 | 35,5 |
400 | 77,0 |
464 | 143,4 |
528 | 96,4 |
624 | 61,0 |
662 | 40,8 |
999 | 73,3 |
08055010 | 052 | 55,2 |
999 | 55,2 |
08081080 | 400 | 103,5 |
404 | 107,3 |
508 | 85,1 |
512 | 124,4 |
528 | 88,0 |
720 | 52,5 |
999 | 93,5 |
08082050 | 388 | 83,2 |
400 | 93,4 |
528 | 58,9 |
999 | 78,5 |
--------------------------------------------------
