Regulation (EC) No 1059/2003 of the European Parliament and of the Council
of 26 May 2003
on the establishment of a common classification of territorial units for statistics (NUTS)
THE EUROPEAN PARLIAMENT AND THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 285 thereof,
Having regard to the proposal from the Commission(1),
Having regard to the opinion of the European Economic and Social Committee(2),
Having regard to the opinion of the Committee of the Regions(3),
Acting in accordance with the procedure laid down in Article 251 of the Treaty(4),
Whereas:
(1) Users of statistics express an increasing need for harmonisation in order to have comparable data across the European Union. In order to function, the internal market requires statistical standards applicable to the collection, transmission and publication of national and Community statistics so that all operators in the single market can be provided with comparable statistical data. In this context, classifications are an important tool for the collection, compilation and dissemination of comparable statistics.
(2) Regional statistics are a cornerstone of the European Statistical System. They are used for a wide range of purposes. For many years European regional statistics have been collected, compiled and disseminated on the basis of a common regional classification, called "Nomenclature of territorial units for statistics" (hereinafter referred to as NUTS). It is now appropriate to fix this regional classification in a legal framework and to institute clear rules for future amendments of this classification. The NUTS classification should not preclude the existence of other subdivisions and classifications.
(3) Accordingly, all Member States' statistics transmitted to the Commission, which are broken down by territorial units, should use the NUTS classification, where applicable.
(4) In its analysis and dissemination, the Commission should use the NUTS classification for all statistics classified by territorial units, where applicable.
(5) Different levels are needed for regional statistics depending on the purpose of these statistics at national and European level. It is appropriate to have at least three hierarchical levels of detail in the European regional NUTS classification. Member States could have further levels of NUTS details, where they consider it necessary.
(6) Information on the current territorial composition of NUTS level 3 regions is necessary for the proper administration of the NUTS classification and should therefore be transmitted regularly to the Commission.
(7) Objective criteria for the definition of regions are necessary in order to ensure impartiality when regional statistics are compiled and used.
(8) Users of regional statistics need stability of the nomenclature over time. The NUTS classification should hence not be amended too frequently. This Regulation will ensure a greater stability of rules over time.
(9) Comparability of regional statistics requires that the regions be of a comparable size in terms of population. In order to achieve this goal, amendments of the NUTS classification should render the regional structure more homogeneous in terms of population size.
(10) The actual political, administrative and institutional situation must also be respected. Non-administrative units must reflect economic, social, historical, cultural, geographical or environmental circumstances.
(11) Reference should be made to the definition of the "population" on which the classification is based.
(12) The NUTS classification is restricted to the economic territory of the Member States and does not provide complete coverage of the territory to which the Treaty establishing the European Community applies. Its use for Community purposes will therefore need to be assessed on a case-by-case basis. The economic territory of each country, as defined in Commission Decision 91/450/EEC(5),also includes extraregio territory, made up of parts of the economic territory that cannot be attached to a certain region (air-space, territorial waters and the continental shelf, territorial enclaves, in particular embassies, consulates and military bases, and deposits of oil, natural gas, etc. in international waters, outside the continental shelf, worked by resident units). The NUTS classification must also provide the possibility of statistics for this extraregio territory.
(13) Amendments to the NUTS classification will require close consultations with the Member States.
(14) Since the objective of the proposed action, namely the harmonisation of regional statistics, cannot be sufficiently achieved by the Member States and can therefore be better achieved at Community level, the Community may adopt measures, in accordance with the principle of subsidiarity as set out in Article 5 of the Treaty. In accordance with the principle of proportionality, as set out in that Article, this Regulation does not go beyond what is necessary in order to achieve that objective.
(15) The NUTS classification laid down in this Regulation should replace the "Nomenclature of territorial units for statistics (NUTS)" established to date by the Statistical Office of the European Communities in cooperation with the national statistical institutes. As a consequence, all references in Community acts to the "Nomenclature of territorial units for statistics (NUTS)" should now be understood as referring to the NUTS classification laid down in this Regulation.
(16) Council Regulation (EC) No 322/97 of 17 February 1997 on Community Statistics(6) constitutes the reference framework for the provisions of this Regulation.
(17) The measures necessary for the implementation of this Regulation should be adopted in accordance with Council Decision 1999/468/EC of 28 June 1999 laying down the procedures for the exercise of implementing powers conferred on the Commission(7).
(18) The Statistical Programme Committee established by Council Decision 89/382/EEC, Euratom(8) has been consulted in accordance with Article 3 thereof,
HAVE ADOPTED THIS REGULATION:
Article 1
Subject matter
1. The purpose of this Regulation is to establish a common statistical classification of territorial units, hereinafter referred to as "NUTS", in order to enable the collection, compilation and dissemination of harmonised regional statistics in the Community.
2. The NUTS classification laid down in Annex I shall replace the "Nomenclature of territorial units for statistics (NUTS)" established by the Statistical Office of the European Communities in cooperation with the national statistical institutes of the Member States.
Article 2
Structure
1. The NUTS classification subdivides the economic territory of the Member States, as defined in Decision 91/450/EEC, into territorial units. It ascribes to each territorial unit a specific code and name.
2. The NUTS classification is hierarchical. It subdivides each Member State into NUTS level 1 territorial units, each of which is subdivided into NUTS level 2 territorial units, these in turn each being subdivided into NUTS level 3 territorial units.
3. However, a particular territorial unit may be classified at several NUTS levels.
4. At the same NUTS level, two different territorial units in the same Member State may not be identified by the same name. If two territorial units in different Member States have the same name, the country identifier is added to the territorial units' names.
5. In each Member State, there can be further hierarchical levels of detail, decided by the Member State, whereby NUTS level 3 is subdivided. Within two years from the entry into force of this Regulation, the Commission, after consulting the Member States, shall submit a communication to the European Parliament and the Council on the appropriateness of establishing rules on a Europe-wide basis for more detailed levels in the NUTS classification.
Article 3
Classification criteria
1. Existing administrative units within the Member States shall constitute the first criterion used for the definition of territorial units.
To this end, "administrative unit" shall mean a geographical area with an administrative authority that has the power to take administrative or policy decisions for that area within the legal and institutional framework of the Member State.
2. In order to establish the relevant NUTS level in which a given class of administrative units in a Member State is to be classified, the average size of this class of administrative units in the Member State shall lie within the following population thresholds:
>TABLE>
If the population of a whole Member State is below the minimum threshold for a given NUTS level, the whole Member State shall be one NUTS territorial unit for this level.
3. For the purpose of this Regulation, the population of a territorial unit shall consist of those persons who have their usual place of residence in this area.
4. The existing administrative units that are used for the NUTS classification are laid down in Annex II. Amendments to Annex II shall be adopted in accordance with the regulatory procedure referred to in Article 7(2).
5. If for a given level of NUTS no administrative units of a suitable scale exist in a Member State, in accordance with the criteria referred to in paragraph 2, this NUTS level shall be constituted by aggregating an appropriate number of existing smaller contiguous administrative units. This aggregation shall take into consideration such relevant criteria as geographical, socio-economic, historical, cultural or environmental circumstances.
The resulting aggregated units shall hereinafter be referred to as "non-administrative units". The size of the non-administrative units in a Member State for a given NUTS level shall lie within the population thresholds referred to in paragraph 2.
In accordance with the regulatory procedure referred to in Article 7(2), individual non-administrative units may however deviate from these thresholds because of particular geographical, socio-economic, historical, cultural or environmental circumstances, especially in the islands and the outermost regions.
Article 4
Components of NUTS
1. Within six months after the entry into force of this Regulation, the Commission shall publish the components of each NUTS level 3 territorial unit in terms of the smaller administrative units as laid down in Annex III, as transmitted to it by the Member States.
Amendments to Annex III shall be adopted in accordance with the regulatory procedure referred to in Article 7(2).
2. Within the first six months of each year, Member States shall transmit to the Commission all changes of the components for the previous year that may affect the NUTS level 3 boundaries and in so doing shall respect the electronic data format requested by the Commission.
Article 5
Amendments to NUTS
1. The Member States shall inform the Commission of:
(a) all changes that have occurred in administrative units, in so far as they may affect the NUTS classification, as laid down in Annex I, or the contents of Annexes II and III;
(b) all other changes at the national level that may affect the NUTS classification, in accordance with the classification criteria laid down in Article 3.
2. Changes to NUTS level 3 boundaries due to changes of smaller administrative units as laid down in Annex III:
(a) shall not be considered as amendments of NUTS if they involve a population transfer equal to or less than one percent of the NUTS 3 territorial units concerned;
(b) shall be considered as amendments of NUTS, in accordance with paragraph 3 of this Article, if they involve a population transfer of more than one percent of the NUTS 3 territorial units concerned.
3. Amendments to the NUTS for the non-administrative units in a Member State, as referred to in Article 3(5), may be made if, at the NUTS level in question, the amendment reduces the standard deviation of the size in terms of population of all EU territorial units.
4. Amendments to the NUTS classification shall be adopted in the second half of the calendar year in accordance with the regulatory procedure referred to in Article 7(2), not more frequently than every three years, on the basis of the criteria laid down in Article 3. Nevertheless, in the case of a substantial reorganisation of the relevant administrative structure of a Member State, the amendments to the NUTS classification may be adopted at intervals of less than three years.
The Commission implementing measures referred to in the first subparagraph shall enter into force, with regard to the transmission of the data to the Commission, on 1 January of the second year after their adoption.
5. When an amendment is made to the NUTS classification, the Member State concerned shall transmit to the Commission the time series for the new regional breakdown, to replace data already transmitted. The list of the time series and their length will be specified in accordance with the regulatory procedure referred to in Article 7(2) taking into account the feasibility of providing them. These time series are to be supplied within two years of the amendment to the NUTS classification.
Article 6
Management
The Commission shall take the necessary measures to ensure the consistent management of the NUTS classification. In particular, such measures may include:
(a) drafting and updating of explanatory notes on NUTS;
(b) examination of problems arising from the implementation of NUTS in the Member States' classifications of territorial units.
Article 7
Procedure
1. The Commission shall be assisted by the Statistical Programme Committee, established by Article 1 of Decision 89/382/EEC, Euratom (hereinafter referred to as the Committee).
2. Where reference is made to this paragraph, Articles 5 and 7 of Decision 1999/468/EC shall apply, having regard to the provisions of Article 8 thereof.
The period laid down in Article 5(6) of Decision 1999/468/EC shall be set at three months.
3. The Committee shall adopt its rules of procedure.
Article 8
Reporting
Three years after the entry into force of this Regulation, the Commission shall submit a report on its implementation to the European Parliament and the Council.
Article 9
Entry into force
This Regulation shall enter into force on the 20th day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 26 May 2003.
For the European Parliament
The President
P. Cox
For the Council
The President
G. Drys
(1) OJ C 180 E, 26.6.2001, p. 108.
(2) OJ C 260, 17.9.2001, p. 57.
(3) OJ C 107, 3.5.2002, p. 54.
(4) Opinion of the European Parliament of 24 October 2001 (OJ C 112 E, 9.5.2002, p. 146), Council Common Position of 9 December 2002 (OJ C 32 E, 11.2.2003, p. 26) and Decision of the European Parliament of 8 April 2003 (not yet published in the Official Journal).
(5) OJ L 240, 29.8.1991, p. 36.
(6) OJ L 52, 22.2.1997, p. 1.
(7) OJ L 184, 17.7.1999, p. 23.
(8) OJ L 181, 28.6.1989, p. 47.
ANNEX I
The NUTS classification (code - name)
>TABLE>
ANNEX II
Existing administrative units
At NUTS level 1 for Belgium "Gewesten/Régions", for Germany "Länder", for Portugal "Continente", Região dos Açores and Região da Madeira, and for United Kingdom Scotland, Wales, Northern Ireland and the Government Office Regions of England.
At NUTS level 2 for Belgium "Provincies/Provinces", for Germany "Regierungsbezirke", for Greece "periferies", for Spain "comunidades y ciudades autónomas", for France "régions", for Ireland "regions", for Italy "regioni", for the Netherlands "provincies" and for Austria "Länder".
At NUTS level 3 for Belgium "arrondissementen/arrondissements", for Denmark "Amtskommuner", for Germany "Kreise/kreisfreie Städte", for Greece "nomoi", for Spain "provincias", for France "départements", for Ireland "regional authority regions", for Italy "provincie", for Sweden "län" and for Finland "maakunnat/landskapen".
ANNEX III
Smaller administrative units
For Belgium "Gemeenten/Communes", for Denmark "Kommuner", for Germany "Gemeinden", for Greece "Demoi/Koinotites", for Spain "Municipios", for France "Communes", for Ireland "counties or county boroughs", for Italy "Comuni", for Luxembourg "Communes", for the Netherlands "Gemeenten", for Austria "Gemeinden", for Portugal "Freguesias", for Finland "Kunnat/Kommuner", for Sweden "Kommuner" and for the United Kingdom "Wards".
