AGREEMENT IN THE FORM OF EXCHANGES OF LETTERS
between the Council of the European Union and the Republic of Iceland and the Kingdom of Norway concerning committees which assist the European Commission in the exercise of its executive powers
A. Letter from the Community
Sir,
The Council refers to the negotiations concerning the Agreement concerning the association of the Republic of Iceland and the Kingdom of Norway to the implementation, application and development of the Schengen acquis and has taken good note of the request of Iceland and Norway, in the spirit of their participation in the decision-making process in the fields covered by the Agreement and in order to enhance the good functioning of the Agreement, to be fully associated with the work of the committees which assist the European Commission in the exercise of its executive powers.
The Council notes that in future, when such procedures will be applied in the fields covered by the Agreement, there will indeed be a need to associate Iceland and Norway with the work of these committees, also in order to ensure that the procedures of the Agreement have been applied to the acts or measures concerned, so that these may become binding on Iceland and Norway.
Therefore, the European Community is ready to commit itself to negotiate, as soon as the need arises, appropriate arrangements for the association of Iceland and Norway with the work of these committees.
I should be obliged if you would inform me of whether or not your Government is in agreement with the above.
Please accept, Sir, the assurance of my highest consideration.
Hecho en Bruselas, el dieciocho de mayo de mil novecientos noventa y nueve.
Udfærdiget i Bruxelles den attende maj nitten hundrede og nioghalvfems.
Geschehen zu Brüssel am achtzehnten Mai neunzehnhundertneunundneunzig.
Έγινε στις Βρυξέλλες, στις δέκα οκτώ Μαΐου χίλια εννιακόσια ενενήντα εννέα.
Done at Brussels on the eighteenth day of May in the year one thousand nine hundred and ninety-nine.
Fait à Bruxelles, le dix-huit mai mil neuf cent quatre-vingt dix-neuf.
Fatto a Bruxelles, addì diciotto maggio millenovecentonovantanove.
Gedaan te Brussel, de achttiende mei negentienhonderd negenennegentig.
Feito em Bruxelas, em dezoito de Maio de mil novecentos e noventa e nove.
Tehty Brysselissä kahdeksantenatoista päivänä toukokuuta vuonna tuhatyhdeksänsataayhdeksänkymmentäyhdeksän.
Som skedde i Bryssel den artonde maj nittonhundranittionio.
Gjört í Brussel 18. maí 1999.
Utferdiget i Brussel, attende mai nittenhundreognittini.
Por el Consejo de la Unión Europea/For Rådet for Den Europæiske Union/Für den Rat der Europäischen Union/Για το Συμβούλιο της Ευρωπαϊκής Ένωσης/For the Council of the European Union/Pour le Conseil de l'Union européenne/Per il Consiglio dell'Unione europea/Voor de Raad van de Europese Unie/Pelo Conselho da União Europeia/Euroopan unionin neuvoston puolesta/För Europeiska unionens råd/Fyrir hönd ráðs Evrópusambandsins/For Rådet for Den europeiske union
>PIC FILE= "L_1999176EN.005401.TIF">
B. Letter from Iceland
Sir,
I have the honour to acknowledge receipt of your letter of today's date which reads as follows:
"The Council refers to the negotiations concerning the Agreement concerning the association of the Republic of Iceland and the Kingdom of Norway to the implementation, application and development of the Schengen acquis and has taken good note of the request of Iceland and Norway, in the spirit of their participation in the decision-making process in the fields covered by the Agreement and in order to enhance the good functioning of the Agreement, to be fully associated with the work of the committees which assist the European Commission in the exercise of its executive powers.
The Council notes that in future, when such procedures will be applied in the fields covered by the Agreement, there will indeed be a need to associate Iceland and Norway with the work of these committees, also in order to ensure that the procedures of the Agreement have been applied to the acts or measures concerned, so that these may become binding on Iceland and Norway.
Therefore, the European Community is ready to commit itself to negotiate, as soon as the need arises, appropriate arrangements for the association of Iceland and Norway with the work of these committees.
I should be obliged if you would inform me of whether ornot your Governmet is in agreement with the above."
I am able to inform you that my Government is in agreement with the contents of your letter.
Please accept, Sir, the assurance of my highest consideration.
Hecho en Bruselas, el dieciocho de mayo de mil novecientos noventa y nueve.
Udfærdiget i Bruxelles den attende maj nitten hundrede og nioghalvfems.
Geschehen zu Brüssel am achtzehnten Mai neunzehnhundertneunundneunzig.
Έγινε στις Βρυξέλλες, στις δέκα οκτώ Μαΐου χίλια εννιακόσια ενενήντα εννέα.
Done at Brussels on the eighteenth day of May in the year one thousand nine hundred and ninety-nine.
Fait à Bruxelles, le dix-huit mai mil neuf cent quatre-vingt dix-neuf.
Fatto a Bruxelles, addì diciotto maggio millenovecentonovantanove.
Gedaan te Brussel, de achttiende mei negentienhonderd negenennegentig.
Feito em Bruxelas, em dezoito de Maio de mil novecentos e noventa e nove.
Tehty Brysselissä kahdeksantenatoista päivänä toukokuuta vuonna tuhatyhdeksänsataayhdeksänkymmentäyhdeksän.
Som skedde i Bryssel den artonde maj nittonhundranittionio.
Gjört í Brussel 18. maí 1999.
Utferdiget i Brussel, attende mai nittenhundreognittini.
Por la República de Islandia/For Republikken Island/Für die Republik Island/Για τη Δημοκρατία της Ισλανδίας/For the Republic of Iceland/Pour la République d'Islande/Per la Repubblica d'Islanda/Voor de Republiek IJsland/Pela República da Islândia/Islannin tasavallan puolesta/På Republiken Islands vägnar/Fyrir hönd Lyðveldisins Íslands/For Republikken Island
>PIC FILE= "L_1999176EN.005601.TIF">
A. Letter from the Community
Sir,
The Council refers to the negotiations concerning the Agreement concerning the association of the Republic of Iceland and the Kingdom of Norway to the implementation, application and development of the Schengen acquis and has taken good note of the request of Iceland and Norway, in the spirit of their participation in the decision-making process in the fields covered by the Agreement and in order to enhance the good functioning of the Agreement, to be fully associated with the work of the committees which assist the European Commission in the exercise of its executive powers.
The Council notes that in future, when such procedures will be applied in the fields covered by the Agreement, there will indeed be a need to associate Iceland and Norway with the work of these committees, also in order to ensure that the procedures of the Agreement have been applied to the acts or measures concerned, so that these may become binding on Iceland and Norway.
Therefore, the European Community is ready to commit itself to negotiate, as soon as the need arises, appropriate arrangements for the association of Iceland and Norway with the work of these committees.
I should be obliged if you would inform me of whether or not your Government is in agreement with the above.
Please accept, Sir, the assurance of my highest consideration.
Hecho en Bruselas, el dieciocho de mayo de mil novecientos noventa y nueve.
Udfærdiget i Bruxelles den attende maj nitten hundrede og nioghalvfems.
Geschehen zu Brüssel am achtzehnten Mai neunzehnhundertneunundneunzig.
Έγινε στις Βρυξέλλες, στις δέκα οκτώ Μαΐου χίλια εννιακόσια ενενήντα εννέα.
Done at Brussels on the eighteenth day of May in the year one thousand nine hundred and ninety-nine.
Fait à Bruxelles, le dix-huit mai mil neuf cent quatre-vingt dix-neuf.
Fatto a Bruxelles, addì diciotto maggio millenovecentonovantanove.
Gedaan te Brussel, de achttiende mei negentienhonderd negenennegentig.
Feito em Bruxelas, em dezoito de Maio de mil novecentos e noventa e nove.
Tehty Brysselissä kahdeksantenatoista päivänä toukokuuta vuonna tuhatyhdeksänsataayhdeksänkymmentäyhdeksän.
Som skedde i Bryssel den artonde maj nittonhundranittionio.
Gjört í Brussel 18. maí 1999.
Utferdiget i Brussel, attende mai nittenhundreognittini.
Por el Consejo de la Unión Europea/For Rådet for Den Europæiske Union/Für den Rat der Europäischen Union/Για το Συμβούλιο της Ευρωπαϊκής Ένωσης/For the Council of the European Union/Pour le Conseil de l'Union européenne/Per il Consiglio dell'Unione europea/Voor de Raad van de Europese Unie/Pelo Conselho da União Europeia/Euroopan unionin neuvoston puolesta/För Europeiska unionens råd/Fyrir hönd ráðs Evrópusambandsins/For Rådet for Den europeiske union
>PIC FILE= "L_1999176EN.005801.TIF">
B. Letter from Norway
Sir,
I have the honour to acknowledge receipt of your letter of today's date which reads as follows:
"The Council refers to the negotiations concerning the Agreement concerning the association of the Republic of Iceland and the Kingdom of Norway to the implementation, application and development of the Schengen acquis and has taken good note of the request of Iceland and Norway, in the spirit of their participation in the decision-making process in the fields covered by the Agreement and in order to enhance the good functioning of the Agreement, to be fully associated with the work of the committees which assist the European Commission in the exercise of its executive powers.
The Council notes that in future, when such procedures will be applied in the fields covered by the Agreement, there will indeed be a need to associate Iceland and Norway with the work of these committees, also in order to ensure that the procedures of the Agreement have been applied to the acts or measures concerned, so that these may become binding on Iceland and Norway.
Therefore, the European Community is ready to commit itself to negotiate, as soon as the need arises, appropriate arrangements for the association of Iceland and Norway with the work of these committees.
I should be obliged if you would inform me of whether or not your Government is in agreement with the above."
I am able to inform you that my Government is in agreement with the contents of your letter.
Please accept, Sir, the assurance of my highest consideration.
Hecho en Bruselas, el dieciocho de mayo de mil novecientos noventa y nueve.
Udfærdiget i Bruxelles den attende maj nitten hundrede og nioghalvfems.
Geschehen zu Brüssel am achtzehnten Mai neunzehnhundertneunundneunzig.
Έγινε στις Βρυξέλλες, στις δέκα οκτώ Μαΐου χίλια εννιακόσια ενενήντα εννέα.
Done at Brussels on the eighteenth day of May in the year one thousand nine hundred and ninety-nine.
Fait à Bruxelles, le dix-huit mai mil neuf cent quatre-vingt dix-neuf.
Fatto a Bruxelles, addì diciotto maggio millenovecentonovantanove.
Gedaan te Brussel, de achttiende mei negentienhonderd negenennegentig.
Feito em Bruxelas, em dezoito de Maio de mil novecentos e noventa e nove.
Tehty Brysselissä kahdeksantenatoista päivänä toukokuuta vuonna tuhatyhdeksänsataayhdeksänkymmentäyhdeksän.
Som skedde i Bryssel den artonde maj nittonhundranittionio.
Gjört í Brussel 18. maí 1999.
Utferdiget i Brussel, attende mai nittenhundreognittini.
Por el Reino de Noruega/For Kongeriget Norge/Für das Königreich Norwegen/Για το Βασίλειο της Νορβηγίας/For the Kingdom of Norway/Pour le Royaume de Norvège/Per il Regno di Norvegia/Voor het Koninkrijk Noorwegen/Pelo Reino da Noruega/Norjan kuningaskunnan puolesta/På Konungariket Norges vägnar/Fyrir hönd Konungsríkisins Noregs/For Kongeriket Norge
>PIC FILE= "L_1999176EN.006001.TIF">
