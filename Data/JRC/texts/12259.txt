[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 29.3.2006
COM(2006) 153 final
2006/0055 (CNS)
Proposal for a
COUNCIL REGULATION
amending Regulations (EEC) No 2771/75 and (EEC) No 2777/75 as regards the application of exceptional market support measures
(presented by the Commission)
EXPLANATORY MEMORANDUM
The extensive media coverage given to the highly pathogenic strain of bird flu (H5N1) in several Member States, mainly in wild birds, has considerably hit consumption of poultrymeat in the EU in recent weeks.
The drop in consumption in some Member States, due to an underlying loss in consumer confidence, has been in some instances more than 50%. The drop in consumption has also resulted in a very significant build-up in stocks of poultrymeat in many Member States, currently estimated at 300 000 tonnes. The situation is all the more worrying because a number of third countries have placed a ban on imports of poultrymeat from certain Member States, or from the EU as a whole.
Export refunds are the sole market management instrument available to the common organisation of the market in poultrymeat. The Commission has already used this instrument by increasing export refund levels for certain products, but it is still insufficient to re-establish market equilibrium, given that consumption has also fallen on traditional export markets. The bans imposed by some third countries as mentioned above is further limiting the effect of the export refunds.
Aside from the use of export refunds for market management purposes, Articles 14 of the Regulations on the common market organisation in eggs and poultrymeat respectively provide for exceptional market support measures where restrictions are placed on free circulation as a result of the application of measures to combat the spread of animal diseases.
Such measures concern only poultry farms directly affected by health and veterinary measures and so cannot support the market more generally when there is a significant fall in consumption.
It is therefore necessary to extend this legal basis in order to include support measures other than those provided for currently in cases where serious market disturbances due to public health or animal health risks.
This proposal amends Articles 14 of Council Regulations (EEC) No 2777/75 and (EEC) No 2771/75 to this end.
2006/0055 (CNS)
Proposal for a
COUNCIL REGULATION
amending Regulations (EEC) No 2771/75 and (EEC) No 2777/75 as regards the application of exceptional market support measures
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Articles 36 and 37 thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament[1],
Whereas:
1. Article 14 of Council Regulation (EEC) No 2771/75 of 29 October 1975 on the common organisation of the market in eggs[2], and Article 14 of Council Regulation (EEC) No 2777/75 of 29 October 1975 on the common organisation of the market in poultrymeat[3] provide for the introduction of exceptional market support measures to take account of restrictions on free circulation caused by the application of measures to combat the spread of animal diseases.
2. These exceptional market support measures are to be taken by the Commission and are directly related to or consequent upon veterinary and health measures adopted by the Member States concerned to combat the spread of animal diseases. They are taken at the request of the Member States in order to prevent serious disruption of the markets concerned.
3. Experience shows that serious market disturbances such as a significant drop in consumption or in prices may be attributed to a loss in consumer confidence due to public health, or animal health risks.
4. The exceptional market support measures laid down in Regulations (EEC) No 2771/75 and (EEC) No 2777/75 should therefore also cover market disturbances created by consumer behaviour in response to such animal or public health risks.
5. It should be clarified that veterinary or sanitary measures taken by Member States must be in conformity with Community law.
6. Regulation (EEC) No 2771/75 and Regulation (EEC) No 2777/75 should therefore be amended accordingly,
HAS ADOPTED THIS REGULATION:
Article 1
Article 14(1) of Regulation (EEC) No 2771/75 is hereby replaced by the following:
“1. Exceptional support measures for the affected market may be taken under the procedure referred to in Article 17(2) in order to take account:
(a) of restrictions on free circulation that may result from the application of measures to combat the spread of animal diseases, or
(b) serious market disturbances attributed to a loss in consumer confidence due to public health, or animal health risks.
These measures shall be taken at the request of the Member State(s) concerned.
In the case of the restrictions on free circulation referred to under (a) above, exceptional measures may be taken only if the Member State(s) concerned have taken the health and veterinary measures, in conformity with Community law, needed to stamp the disease out quickly and only to the extent and for the duration strictly necessary to support this market.”
Article 2
Article 14(1) of Regulation (EEC) No 2777/75 is hereby replaced by the following:
“1. Exceptional support measures for the affected market may be taken under the procedure referred to in Article 17(2) in order to take account:
(a) of restrictions on free circulation that may result from the application of measures to combat the spread of animal diseases, or
(b) serious market disturbances directly attributed to a loss in consumer confidence due to public health, or animal health risks.
These measures shall be taken at the request of the Member State(s) concerned.
In the case of the restrictions on free circulation referred to under (a) above, exceptional measures may be taken only if the Member State(s) concerned have taken the health and veterinary measures, in conformity with Community law, needed to stamp the disease out quickly and only to the extent and for the duration strictly necessary to support this market.”
Article 3
This Regulation shall enter into force on the seventh day following that of its publication in the Official Journal of the European Union .
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels,
For the Council
The President
FINANCIAL STATEMENT |
1. | BUDGET HEADING: 05 02 15 07 | APPROPRIATIONS: p.m. |
2. | TITLE: Proposal for a Council Regulation amending Regulations (EEC) No 2771/75 and (EEC) No 2777/75 as regards the application of exceptional market support measures |
3. | LEGAL BASIS: Article 37 of the Treaty |
4. | AIMS: To extend the scope of application of Article 14 on the introduction of “exceptional market support measures”. This proposal is intended to include market difficulties following loss of consumers’ confidence directly linked with animal or public health problems. |
5. | FINANCIAL IMPLICATIONS | 12 MONTH PERIOD (EUR million) | CURRENT FINANCIAL YEAR 2006 (EUR million) | FOLLOWING FINANCIAL YEAR 2007 (EUR million) |
5.0 | EXPENDITURE – CHARGED TO THE EC BUDGET (REFUNDS/INTERVENTIONS) – NATIONAL AUTHORITIES – OTHER | (1) | – | – |
5.1 | REVENUE – OWN RESOURCES OF THE EC (LEVIES/CUSTOMS DUTIES) – NATIONAL | – | – | – |
2008 | 2009 | 2010 | 2011 and following |
5.0.1 | ESTIMATED EXPENDITURE | (1) | (1) | (1) | (1) |
5.1.1 | ESTIMATED REVENUE |
5.2 | METHOD OF CALCULATION: – |
6.0 | CAN THE PROJECT BE FINANCED FROM APPROPRIATIONS ENTERED IN THE RELEVANT CHAPTER OF THE CURRENT BUDGET? | YES NO |
6.1 | CAN THE PROJECT BE FINANCED BY TRANSFER BETWEEN CHAPTERS OF THE CURRENT BUDGET? | YES NO |
6.2 | WILL A SUPPLEMENTARY BUDGET BE NECESSARY? | YES NO |
6.3 | WILL APPROPRIATIONS NEED TO BE ENTERED IN FUTURE BUDGETS? | YES NO |
OBSERVATIONS: (1) This proposal lays down the legal basis for a possible later introduction of specific exceptional market support measures in the poultry meat and eggs sector. At this moment it is not possible to make an estimate of the future expenditure level. It will only be possible to estimate the expenditure at the time that concrete measures are proposed. |
[1] OJ C …, …, p. ….
[2] OJ L 282, 1.11.1975, p. 49. Regulation as last amended by Regulation (EC) No 1913/2005 (OJ L 307, 25.11.2005, p. 2).
[3] OJ L 282, 1.11.1975, p. 77. Regulation as last amended by Regulation (EC) No 1913/2005.
