Council Decision
of 20 March 2000
amending Decision 97/787/EC providing exceptional financial assistance for Armenia and Georgia in order to extend it to Tajikistan
(2000/244/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 308 thereof,
Having regard to the proposals from the Commission,
Having regard to the opinion of the European Parliament(1),
Whereas:
(1) Council Decision 97/787/EC provides exceptional financial assistance for Armenia and Georgia(2).
(2) In parallel with its Decision to provide exceptional financial assistance for Armenia and Georgia, the Council agreed also to consider a similar operation for Tajikistan once circumstances allowed.
(3) Tajikistan is undertaking fundamental political and economic reforms and is making substantial efforts to implement a market economy model.
(4) In view of the initial results, particularly in terms of growth and control of inflation, these reforms should be continued with the primary aim of improving people's living conditions and creating jobs.
(5) Trade, commercial and economic links are expected to develop between the Community and Tajikistan. Tajikistan is eligible for a Partnership and Cooperation Agreement with the European Communities and their Member States and has formally requested to benefit from such an agreement as soon as possible.
(6) Tajikistan agreed with the International Monetary Fund (IMF) in June 1998 on a three-year enhanced structural adjustment facility.
(7) The Tajik authorities have formally undertaken to settle fully their outstanding financial obligations towards the Community and Tajikistan secures a minimum servicing of outstanding Community claims.
(8) The Tajik authorities have formally requested exceptional financial support from the Community.
(9) Tajikistan is a low-income country and is facing particularly critical economic, social and political circumstances. This country is eligible for highly concessional loans from the World Bank and the IMF.
(10) Concessional financial assistance from the Community in the form of a mix of a long-term loan and straight grants is an appropriate measure to help the beneficiary country at this critical juncture.
(11) This assistance, both the loan and the grant component, is highly exceptional and therefore in no way constitutes a precedent.
(12) The inclusion of a grant component in this assistance is without prejudice to the powers of the budgetary authority.
(13) This assistance should be managed by the Commission.
(14) The Commission is to ensure that financial assistance is used in accordance with the rules of budgetary control.
(15) In implementing this Decision, the Commission will take due account of progress made in the Inter-Tajik peace process and, in particular, in the holding of elections under acceptable conditions.
(16) The Commission consulted the Economic and Financial Committee before submitting its proposal.
(17) The Treaty does not provide, for the adoption of this Decision, powers other than those of Article 308,
HAS DECIDED AS FOLLOWS:
Sole Article
Decision 97/787/EC is hereby amended as follows:
1. Article 1(1) to (3) shall be replaced by the following:
"1. The Community shall make available to Armenia, Georgia and Tajikistan exceptional financial assistance, in the form of long-term loans and straight grants.
2. The total loan component of this assistance shall amount to a maximum principal of EUR 245 million, with a maximum maturity of 15 years and a grace period of 10 years. To this end, the Commission shall be empowered to borrow, on behalf of the Community, the necessary resources that will be placed at the disposal of the beneficiary countries in the form of loans.
3. The grant component of this assistance shall consist of an amount of up to EUR 130 million during the 1997 to 2004 period, with a maximum of EUR 24 million annually. The grants shall be made available in so far as the net debtor position of the beneficiary countries towards the Community has been reduced, as a rule, by at leat a similar amount."
2. Article 3(1) shall be replaced by the following:
"1. Subject to the provisions of Article 1(4) and Article 2, the total amount of the loan offered to each country shall be made available by the Commission in parallel with the first instalments of the grants. The remainder of the grant component of the assistance shall be made available by the Commission in successive instalments, subject to the same provisions."
3. Article 5(2) shall be replaced by the following text:
"2. Before 31 December 2004, the Council shall examine the application of this Decision to that date on the basis of a comprehensive report by the Commission, which shall also be submitted to the European Parliament."
Done at Brussels, 20 March 2000.
For the Council
The President
J. Gama
(1) Opinion delivered on 17 December 1999 (not yet published in the Official Journal).
(2) OJ L 322, 25.11.1997, p. 37.
