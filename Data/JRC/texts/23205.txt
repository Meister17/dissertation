COMMISSION DECISION of 24 November 1998 laying down special conditions governing imports of fishery and aquaculture products originating in Mexico (notified under document number C(1998) 3586) (Text with EEA relevance) (98/695/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/493/EEC of 22 July 1991 laying down the health conditions for the production and the placing on the market of fishery products (1), as last amended by the Council Directive 97/79/EC (2), and in particular Article 11 thereof,
Whereas a Commission expert has conducted an inspection visit to Mexico to verify the conditions under which fishery products are produced, stored and dispatched to the Community;
Whereas the provisions of legislation of Mexico on health inspection and monitoring of fishery products may be considered equivalent to those laid down in Directive 91/493/EEC;
Whereas, in Mexico the 'Dirección General de Calidad Sanitaria de Bienes y Servicios (DGCSBS) de la Secretaría de Salud` is capable of effectively verifying the application of the laws in force;
Whereas the procedure for obtaining the health certificate referred to in Article 11(4)(a) of Directive 91/493/EEC must also cover the definition of a model certificate, the minimum requirements regarding the language(s) in which it must be drafted and the grade of the person empowered to sign it;
Whereas, pursuant to Article 11(4)(b) of Directive 91/493/EEC, a mark should be affixed to packages of fishery products giving the name of the third country and the approval/registration number of the establishment, factory vessel, cold store or freezer vessel of origin;
Whereas, pursuant to Article 11(4)(c) of Directive 91/493/EEC, a list of approved establishments, factory vessels, or cold stores must be drawn up; whereas a list of freezer vessels registered in the sense of Directive 92/48/EEC (3) must be drawn up; whereas these lists must be drawn up on the basis of a communication from the DGCSBS to the Commission; whereas it is therefore for the DGCSBS to ensure compliance with the provisions laid down to that end in Article 11(4) of Directive 91/493/EEC;
Whereas the DGCSBS has provided official assurances regarding compliance with the rules set out in Chapter V of the Annex to Directive 91/493/EEC and regarding the fulfilment of requirements equivalent to those laid down by that Directive for the approval or registration of establishments, factory vessels, cold stores or freezer vessels of origin;
Whereas the measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
The 'Dirección General de Calidad Sanitaria de Bienes y Servicios (DGCSBS) de la Secretaría de Salud` shall be the competent authority in Mexico for verifying and certifying compliance of fishery and aquaculture products with the requirements of Directive 91/493/EEC.
Article 2
Fishery and aquaculture products originating in Mexico must meet the following conditions:
1. each consignment must be accompanied by a numbered original health certificate, duly completed, signed, dated and comprising a single sheet in accordance with the model in Annex A hereto;
2. the products must come from approved establishments, factory vessels, cold stores or registered freezer vessels listed in Annex B hereto;
3. except in the case of frozen fishery products in bulk and intended for the manufacture of preserved foods, all packages must bear the word 'MEXICO` and the approval/registration number of the establishment, factory vessel, cold store or freezer vessel of origin in indelible letters.
Article 3
1. Certificates as referred to in Article 2(1) must be drawn up in at least one official language of the Member State where the checks are carried out.
2. Certificates must bear the name, capacity and signature of the representative of the DGCSBS and the latter's official stamp in a colour different from that of other endorsements.
Article 4
This Decision is addressed to the Member States.
Done at Brussels, 24 November 1998.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ L 268, 24. 9. 1991, p. 15.
(2) OJ L 24, 30. 1. 1998, p. 31.
(3) OJ L 187, 7. 7. 1992, p. 41.
ANNEX A
HEALTH CERTIFICATE for fishery and aquaculture products originating in Mexico and intended for export to the European Community, excluding bivalve molluscs, echinoderms, tunicates and marine gastropods in whatever form
>START OF GRAPHIC>
Reference No
Country of dispatch:
MEXICO
Competent authority:
Dirección General de Calidad Sanitaria de Bienes y Servicios (DGCSBS) de la Secretaria de Salud
I. Details identifying the fishery products
- Description of fishery/aquaculture products (1):
- species (scientific name):
- presentation of product and type of treatment (2):
- Code number (where available):
- Type of packaging:
- Number of packages:
- Net weight:
- Requisite storage and transport temperature:
II. Origin of products
Name(s) and official approval number(s) of establishment(s), factory vessel(s) or cold store(s) approved or freezer vessel(s) registered by the DGCSBS for export to the European Community:
III. Destination of products
The products are dispatched
from:
(place of dispatch)
to:
(country and place of destination)
by the following means of transport:
Name and address of dispatcher:
Name of consignee and address at place of destination:
(1) Delete where applicable.
(2) Live, refrigerated, frozen, salted, smoked, preserved, etc.
IV. Health attestation
- The official inspector hereby certifies that the fishery or aquaculture products specified above:
1. were caught, and handled on board vessels in accordance with the health rules laid down by Directive 92/48/EEC;
2. were landed, handled and where appropriate packaged, prepared, processed, frozen, thawed and stored hygienically in compliance with the requirements laid down in Chapters II, III and IV of the Annex to Directive 91/493/EEC;
3. have undergone health controls in accordance with Chapter V of the Annex to Directive 91/493/EEC;
4. are packaged, marked, stored and transported in accordance with Chapters VI, VII and VIII of the Annex to Directive 91/493/EEC;
5. do not come from toxic species or species containing biotoxins;
6. have satisfactorily undergone the organoleptic, parasitological, chemical and microbiological checks laid down for certain categories of fishery products by Directive 91/493/EEC and in the implementing decisions thereto.
- The undersigned official inspector hereby declares that he is aware of the provisions of Directive 91/493/EEC, Directive 92/48/EEC and Decision 98/695/EC.
Done at ,
(Place)
on
(Date)
Official
stamp (1)
Signature of official inspector (1)
(Name in capital letters, capacity and qualifications of person signing)
(1) The colour of the stamp and signature must be different from that of the other particulars in the certificate.
>END OF GRAPHIC>
ANNEX B
>TABLE>
>TABLE>
