Winding-up proceedings
Decision to open winding-up proceedings in respect of Ancora Versicherungs-Aktiengesellschaft
Publication made in accordance with Article 14 of Directive 2001/17/EC of the European Parliament and of the Council of 19 March 2001 on the reorganisation and winding-up of insurance undertakings (OJ L 110, 20.4.2001, p. 28)
(2006/C 225/03)
Insurance undertaking | Ancora Versicherungs-Aktiengesellschaft Mönkedamm 15 D-20457 Hamburg |
Date, entry into force and nature of the decision | 1 September 2006 Entry into force: 1 September 2006 Opening of winding-up proceedings |
Competent authorities | Amtsgericht Hamburg — Insolvenzgericht Sievekingplatz 1 D-20355 Hamburg |
Supervisory authority | Bundesanstalt für Finanzdienstleistungsaufsicht (BaFin) Graurheindorfer Straße 108 D-53117 Bonn |
Liquidator appointed | Burckhardt Reimer Domstraße 5 D-20095 Hamburg |
Applicable law | Germany § 335 Insolvenzordnung |
--------------------------------------------------
