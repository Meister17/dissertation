Decision of the EEA Joint Committee
No 113/2004
of 9 July 2004
amending Annex XX (Environment) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex XX to the Agreement was amended by Decision of the EEA Joint Committee No 82/2004 of 8 June 2004 [1].
(2) Directive 2003/105/EC of the European Parliament and of the Council of 16 December 2003 amending Council Directive 96/82/EC on the control of major-accident hazards involving dangerous substances [2] is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
The following shall be added in point 23a (Council Directive 96/82/EC) of Annex XX to the Agreement:
", as amended by:
- 32003 L 0105: Directive 2003/105/EC of the European Parliament and of the Council of 16 December 2003 (OJ L 345, 31.12.2003, p. 97)."
Article 2
The texts of Directive 2003/105/EC in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 10 July 2004, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [3].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 9 July 2004.
For the EEA Joint Committee
The President
Kjartan Jóhannsson
--------------------------------------------------
[1] OJ L 349, 25.11.2004, p. 39.
[2] OJ L 345, 31.12.2003, p. 97.
[3] No constitutional requirements indicated.
