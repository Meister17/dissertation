Commission Decision
of 27 June 2001
on establishing the European Research Advisory Board
(notified under document number C(2001) 1656)
(2001/531/EC, Euratom)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to the Treaty establishing the European Atomic Energy Community,
Whereas:
(1) The Commission needs an independent high-level body to increase effectiveness of European RTD policies.
(2) The advisory body should be made up of high-level individuals from the scientific community, industry, and the services sector, in order to examine general research policy issues.
(3) The Presidency Conclusions of the Lisbon European Council (March 2000) state that the Union must work towards the objectives set out in the Commission Communication entitled "Towards a European research area".
(4) The Council reiterated, on 16 November 2000, the importance of the setting-up of such an independent advisory body by the Commission.
(5) Scientific knowledge and advice is increasingly central to the design and implementation of European Union policies.
(6) The Commission has consulted widely with the academic and industrial communities concerned, on the optimal form such a body should take.
(7) It is appropriate to adjust the consultative system to the policy requirements in the field of science and technology, in particular the development of a European research area, in full respect of the competencies of more specific Community advisory structures, in particular those responsible for risk assessment.
(8) The Commission supports adequate representation of women in consultative committees and has a target of 40 % for participation of women in RTD activities,
HAS DECIDED AS FOLLOWS:
Article 1
A consultative committee, hereinafter referred to as the "European Research Advisory Board" (the Committee), is established by the Commission. The Committee shall be an advisory body on research and technological development policy.
Article 2
1. The task of the Committee shall be to advise the Commission on design and implementation of Community policy in research and technological development. In this context, the Committee will pay particular attention to the realisation of the European research area and the use of policy instruments such as the Community research and technological development framework programmes.
2. In order to accomplish the tasks set out in paragraph 1, the Committee shall, on its own initiative or at the request of the Commission, deliver opinions and provide advice on various aspects of Community research policy or developments in science and technology in Europe and worldwide.
3. The Commission shall ensure that the Committee receives the information necessary to perform its task. In this context, the Committee shall be regularly informed of developments in Community research policy.
4. When formally requested by the Committee, the Commission services shall provide to the Committee, written comments on its opinion or advice. The Commission services will also provide regular information on the possible follow-up actions undertaken.
5. Whenever the Commission requests the Committee's opinion, it may set a time limit within which the opinion should be given.
6. The Committee shall hold a clearly stated independent position and shall not represent scientific, industrial, any other organisations, countries or sectoral interests. Whenever appropriate, it should consult with such organisations.
7. Without prejudice to Article 8, the Committee's advice shall be made public and known to the institutions of the European Communities, as well as to the Member States.
Article 3
1. The Committee shall be composed of 45 members, appointed by the Commission in a personal capacity. In order to ensure a proper representation of the scientific/academia, business/industrial world and other stakeholders, as well as full independence of the Committee, the nomination process will be as follows:
- 20 members with an academic focus will be nominated on the basis of a proposal from the European Science Foundation (ESF),
- 20 members with a business and industrial focus will be nominated on the basis of a proposal from the Union of Industrial and Employers' Confederations of Europe (UNICE),
- five members will be identified by the Commission.
The 45 members will be appointed in a single decision and the list of Committee members shall be published by the Commission in the Official Journal of the European Communities.
2. The final composition of the Committee shall reflect the diversity of Europe. The set of criteria to be applied throughout the nomination process is the following:
- excellence in research and/or research management,
- advisory experience on a European or international level,
- balance among S & T disciplines including persons with specific university-industry experience,
- geographical balance, taking into account the enlargement of the EU and the countries associated with the framework programmes,
- appropriate gender balance.
Article 4
The term of office of each member of the Committee shall be for three years with a possibility of one renewal. The members of the Committee shall remain in office until provision is made for their replacement or for renewal of their term of office.
The functions of the Committee members shall not be remunerated. Travel and subsistence expenses for meetings of the Committee, or any working party set up within the Committee, shall be covered by the Commission in accordance with the current administrative rules and regulations. Subject to prior approval, travel and subsistence expenses for preparing opinions or advice may be also covered by the Commission. This includes meetings between members of the Committee and external experts outside Brussels.
Article 5
1. The Committee shall elect a chairperson and two vice-chairpersons, each by a two-thirds majority of the members present. In addition, the Committee shall elect five of its members, also by two-thirds majority of the members present, who, together with the chairperson and the vice-chairpersons, will form the Bureau of the Committee. The composition of the Bureau should respect the criteria specified in Article 3(2).
The Bureau shall organise the work of the Committee.
For the purposes of preparing the Committee's opinions and advice, the Bureau may ask the Commission to undertake studies or consult outside organisations.
2. The Commission shall provide financial and administrative support to the secretariat of the Committee, ensuring that its independence is fully guaranteed.
3. To enable the Committee to accomplish its tasks, the Bureau may set up working parties consisting of members of the Committee and, where necessary, outside persons. The Committee and the working parties may invite experts to participate in their work. Such experts shall participate only in the discussions of the item for which they were invited. The Commission shall cover their travel and subsistence costs.
The Committee shall be able to hold workshops or conferences. Subject to prior approval, the Commission shall cover the cost of those events.
4. The Commission shall provide the Committee with appropriate tools for communication and for dissemination of its activities and opinions.
Article 6
The Committee shall meet an average of three times a year. The regular venue will be on the Commission's premises.
Commission representatives will attend the meetings of the Committee and of its working parties unless otherwise formally requested by the Chairperson of the Committee.
Article 7
The Committee shall adopt its rules of procedure.
Article 8
Without prejudice to the provisions of Article 287 of the EC Treaty and Article 194 of the EAEC Treaty, the members of the Committee shall sign a declaration that they shall not divulge any information they obtain through their work in the Committee or its working parties where the Commission informs them that a particular opinion or matter is confidential.
Members of the Committee should abstain from discussions on a topic on which they have a conflict of interest. For this purpose they should sign a declaration in which they certify that should an agenda item arise where a conflict of interest exists, which would be prejudicial to their independence, they will inform the Chairperson.
Article 9
Commission Decision 98/611/EC, Euratom establishing the European research forum is hereby repealed.
Done at Brussels, 27 June 2001.
For the Commission
Philippe Busquin
Member of the Commission
