Judgment of the Court (Grand Chamber) of 19 September 2006 (reference for a preliminary ruling from the Cour administrative, Luxembourg) — Graham J. Wilson v Ordre des avocats du barreau de Luxembourg
(Case C-506/04) [1]
Referring court
Cour administrative
Parties to the main proceedings
Applicant: Graham J. Wilson
Defendant: Ordre des avocats du barreau de Luxembourg
Re:
Reference for a preliminary ruling — Cour administrative (Luxembourg) — Interpretation of Directive 98/5/EC of the European Parliament and of the Council of 16 February 1998 to facilitate practice of the profession of lawyer on a permanent basis in a Member State other than that in which the qualification was obtained (OJ 1998 L 77, p. 36) — Obligation to provide for a remedy before a court or tribunal in accordance with the provisions of domestic law against a decision refusing entry on the Bar register as a lawyer practising under his home-country professional title — Appeal to the Conseil disciplinaire et adminsitratif du barreau — National legislation making registration conditional on an oral examination intended to verify knowledge of the official languages of the host Member State
Operative part of the judgment
1) Article 9 of Directive 98/5/EC of the European Parliament and of the Council of 16 February 1998 to facilitate practice of the profession of lawyer on a permanent basis in a Member State other than that in which the qualification was obtained must be interpreted as meaning that it precludes an appeal procedure in which the decision refusing registration, referred to in Article 3 of that directive, must be challenged at first instance before a body composed exclusively of lawyers practising under the professional title of the host Member State and on appeal before a body composed for the most part of such lawyers, where the appeal before the supreme court of that Member State permits judicial review of the law only and not the facts.
2) Article 3 of Directive 98/5 must be interpreted as meaning that the registration of a lawyer with the competent authority of a Member State other than the State where he obtained his qualification in order to practise there under his home-country professional title cannot be made subject to a prior examination of his proficiency in the languages of the host Member State.
[1] OJ C 31, 5.2.2005.
--------------------------------------------------
