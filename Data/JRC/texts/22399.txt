COMMON POSITION of 6 October 1997 defined by the Council on the basis of Article K.3 of the Treaty on European Union on negotiations in the Council of Europe and the OECD relating to corruption (97/661/JHA)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty on European Union, and in particular Articles K.3 (2) (a) and K.5 thereof,
Whereas the Member States regard the combating of corruption on an international level as a matter of common interest;
Having regard to the Protocol, drawn up by the Council on 27 September 1996, to the Convention on the protection of the European Communities' financial interests (1);
Having regard to the Convention on the fight against corruption involving officials of the European Communities or officials of the Member States of the European Union, drawn up by the Council on 26 May 1997 (2);
Having regard to the communication from the Commission to the European Parliament and the Council on a Union policy against corruption of 21 May 1997;
Whereas in November 1996 the Committee of Ministers of the Council of Europe adopted a programme of action against corruption which includes the drawing up of a criminal law convention on corruption as a matter of priority;
Whereas on 26 May 1997 the Council at Ministerial Level of the OECD adopted a recommendation on corruption in international commercial transactions and decided to start negotiations on an international convention to make bribery of foreign public officials a criminal offence, to be open for signature by the end of 1997;
Taking into account the proceedings under way in the Council of Europe and the OECD and mindful of the need to ensure compatibility with the work carried out in the European Union;
Desiring to protect the interests of the Union and to avoid unnecessary duplication or incompatibility between the two international instruments drawn up in the Council of Europe and the OECD;
Whereas the European Council meeting in Amsterdam approved the action plan of the High-Level Group on Organized Crime, which advocates a comprehensive plan to combat corruption,
HAS DEFINED THIS COMMON POSITION:
Article 1
1. Member States shall support the drawing up of appropriate international instruments providing for bribery of foreign officials and officials of international organizations to be made a criminal offence. Without prejudice to questions of jurisdiction the offence should cover corruption with regard to any State or international organization.
2. Member States shall ensure, if necessary by inserting a specific clause to that effect, that the provisions of instruments drawn up in the Council of Europe and the OECD are compatible with instruments drawn up between them, in particular with regard to judicial assistance, extradition, combating corruption and the protection of the Community's financial interests.
3. Member States shall advocate coordination of the work on corruption currently in progress in the Council of Europe and the OECD to ensure that there is no incompatibility between the provisions in different draft conventions and that work carried out in one organization does not jeopardize or unnecessarily duplicate work carried out in the other organization.
4. In negotiations in the Council of Europe and the OECD relating to corruption, Member States shall, as far as is practicable, coordinate their positions, at the Presidency's initiative, and seek to arrive at common standpoints on all issues which have significant implications for the interests of the European Union. The Commission shall be fully associated with this work.
Article 2
This common position shall be published in the Official Journal.
Done at Luxembourg, 6 October 1997.
For the Council
The President
J. POOS
(1) OJ C 313, 23. 10. 1996, p. 2.
(2) OJ C 195, 25. 6. 1997, p. 1.
