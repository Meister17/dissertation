Brussels, 10.12.2004
COM(2004) 806 final
.
Proposal for a
COUNCIL DECISION
modifying Council Decision 2003/631/EC of 25 August 2003 adopting measures concerning Liberia under Article 96 of the ACP-EC partnership Agreement in a case of special urgency
.
(presented by the Commission)
EXPLANATORY MEMORANDUM
1. A comprehensive peace agreement signed between the former Government of Liberia, two rebel factions, civil society and political parties in Accra in August 2003 effectively ended 15 years of civil war in Liberia and led to the forming of a transitional administration. The mandate of the transitional administration is to return security to the country with the assistance of the United Nations Mission in Liberia, to rehabilitate the economy, to reinstate social infrastructure and to prepare the ground for democratic legislative and presidential elections in October 2005
2. The European Union decided to contribute to the peace and transition process in Liberia and with Council Decision 2003/631/EC “Adopting measures concerning Liberia under Article 96 of the EC-ACP partnership Agreement in a case of special urgency”, funds were made available and conditions under which the funds could be used were defined. The decision stipulated that regular follow up will be ensured by means of an intensive political dialogue and six-monthly political reviews. This dialogue should be based on the outcome of the consultations held under articles 96 of the EC-ACP Partnership Agreement as set out in Council Decision 2002/274/EC. The decision furthermore provided that the measures shall expire on 31 December 2004.
3. Current reviews of the political situation in Liberia demonstrate that a year from the date of taking office by the transitional administration, the security situation has improved following the deployment of UNMIL forces and the implementation of the disarmament and demobilisation process. Serious concerns remain, however, over the management of public finances by the transitional administration and the State owned enterprises and over the increasing level of corruption. Furthermore, the Government Commissions created under the Comprehensive Peace Agreement have to date shown insufficient progress in the implementation of their mandates to improve governance and accountability. Further steps in addressing human rights issues are also required.
4. Given the insufficient political progress to date, the lifting of the appropriate measures is not justified and the intensive political dialogue with the transitional government with a view to improve respect for human rights, democracy, rule of law and good governance should be further pursued until after a democratically elected and accountable President and government have been installed. For this reason the Commission considers that Council Decision 2003/631/EC of 25 August 2003 should be extended for a period of eighteen months.
The Commission proposes therefore to the Council to modify its aforementioned decision and to adopt the attached decision.
Proposal for a
COUNCIL DECISION
modifying Council Decision 2003/631/EC of 25 August 2003 adopting measures concerning Liberia under Article 96 of the ACP-EC partnership Agreement in a case of special urgency
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the ACP-EC Partnership Agreement entered into force the first April 2003 and in particular Article 96,
Having regard to the internal agreement on measures to be taken and procedures to be followed for the implementation of the ACP-EC Partnership Agreement[1] and, in particular, Article 3 thereof,
Having regard to the proposal from the Commission,[2]
Whereas:
(1) Council Decision 2002/274/EC of 25 March 2002 to conclude the consultations with Liberia under Articles 96 and 97 of the ACP-EC Partnership Agreement provides for the adoption of the appropriate measures within the meaning of Article 96(2) (c) and of Article 97(3) of the ACP-EC partnership Agreement;
(2) Council Decision 2003/631/EC of 25 August 2003 adopting measures concerning Liberia under Article 96 of the EC-ACP partnership Agreement in a case of special urgency provides for the adoption of new appropriate measures within the meaning of Article 96(2) (c) and of Article 97(3) of the ACP-EC partnership Agreement;
(3) The current conditions in Liberia do not yet ensure respect for democratic principles, governance and the rule of law;
(4) It is consequently necessary to extend the validity period of the measures provided for in Council Decision 2003/631/EC and to continue the intensive political dialogue with the Government of Liberia.
HAS DECIDED AS FOLLOWS:
Article 1
The measures referred to in Article 1 of Council Decision 2003/631/EC of 25 August 2003 shall expire on 30 June 2006. This date does not preclude any specific expiry date contained in the financial instruments covered by this decision.
The letter appearing in the Annex to this Decision shall be addressed to the Minister of Foreign Affairs of Liberia.
Article 2
The outcome of the consultations as set out in the draft letter annexed to Decision 2002/274/EC shall remain unaffected.
Article 3
This Decision shall enter into force on the day of its adoption.
Article 4
This decision shall be published in the Official Journal of the European Union.
Done at Brussels, […]
For the Council
The President
ANNEX
DRAFT LETTER TO THE MINISTER OF FOREIGN AFFAIRS OF LIBERIA
H.E. Mr Thomas Nimely Yaya
Minister of Foreign Affairs
Monrovia
Liberia
Dear Minister,
With its letter No SGS3/7429 of 27 August 2003, the European Union informed the government of Liberia of its intentions to accompany the Liberian peace process on the basis of the Comprehensive Peace Agreement. The letter also stipulated that the European Union would closely follow the political development and transitional reform in your country and continue the intensive political dialogue on the basis of Article 8 of the ACP-EC Partnership Agreement and the outcome of the consultations as set out in our letter No SGS 272745 of 27 March 2002.
More than a year has passed since the National Transitional Government of Liberia took office following the signing of the Accra Comprehensive Peace Agreement in August 2003. It is now time to take stock of progress in the implementation of the comprehensive peace agreement and the undertakings that resulted from the aforementioned consultations.
The European Union is pleased to note at this juncture that security and calm have returned to the country and that some initial steps to introduce democratic change and to reform the functioning of the public sector have been made. Serious concerns remain, however, over public finance and macro-economic management by the National Transitional Government of Liberia and the state owned enterprises, and over the increasing level of corruption. Furthermore, the commissions created under the Comprehensive Peace Agreement have to date shown insufficient progress in the implementation of their mandates to improve governance and accountability. Further steps in addressing human rights issues are also required.
In the light of the above, the European Union considers that the National Transitional Government of Liberia is not fully functional and operational and the appropriate measures can therefore not yet be completely revoked. This will be done once a democratically elected and accountable President and government have been installed.
For this reason, the European Union has decided that Council Decision 2003/631/EC of 25 August 2003 shall be extended for a period of eighteen months. During this period our intensive political dialogue should continue, on the basis of Article 8 of the ACP-EC Partnership Agreement and on the basis of the outcome of the consultations as set out in our letter No SGS 272745 of 27 March 2002, with a view to continue improving respect for human rights, democracy, rule of law, and good governance. The dialogue shall involve the Presidency of the European Union and the European Commission and include six-monthly political reviews.
Meanwhile, on the basis of the appropriate measures defined in Council Decision 2003/631/EC of 25 August 2003, support for the peace process and the Results Focused Transitional Framework shall continue.
Yours faithfully,
For the Commission
F or the Council
[1] OJ L 317, 15.12.2000, p. 376.
[2] OJ C […] […], p. […]
