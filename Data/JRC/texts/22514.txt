*****
COMMISSION DIRECTIVE
of 14 December 1984
amending Annexes I and II to Council Directive 66/401/EEC on the marketing of fodder plant seed
(85/38/EEC)
THE COMMISSION OF THE EUROPEAN
COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Directive 66/401/EEC of 14 June 1966 on the marketing of fodder plant seed (1), as last amended by Directive 82/287/EEC (2), and in particular Article 21a thereof,
Whereas, in the light of the development of scientific and technical knowledge, Annexes I and II to Directive 66/401/EEC should be amended for the reasons set out hereafter;
Whereas it is well known, in the case of a certain type of varieties of smooth-stalked meadowgrass (Poapratensis L.), that the varietal purity can more easily be examined than in other varieties; whereas, therefore, specific tolerance levels for varietal purity have already been set up for this type in Annex II of the beforementioned Directive;
Whereas the varietal purity standards applicable to smooth-stalked meadowgrass as well as the procedure to examine satisfaction of them should be adjusted in such a way as to eliminate possible discrimination between the different types of varieties, to give references for field inspection in all cases and to take into account current certification practices;
Whereas the measures provided for in this Directive are in accordance with the opinion of the Standing Committee on Seeds and Propagating Material for Agriculture, Horticulture and Forestry,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Annex I to Directive 66/401/EEC is hereby amended as follows:
1. In the table in paragraph 2, 'apomictic uniclonal varieties of Poa spp' is replaced by 'varieties of Poa pratensis referred to in the second part of the third sentence of paragraph 4'.
2. In the second sentence of paragraph 4, 'or of apomictic uni-clonal varieties of Poa spp' is replaced by 'or of Poa pratensis'.
3. In paragraph 4, the following is inserted after the second sentence:
'In the case of Poa pratensis, the number of plants of the crop species which are recognizable as obviously not being true to the variety shall not exceed:
- one per 20 m2 for the production of basic seed,
- four per 10 m2 for the production of certified seed;
however, in the case of varieties which are officially classified as "apomictic uni-clonal varieties" under agreed procedures, a number of plants recognizable as not being true to the variety, which does not exceed six per 10 m2, may be regarded as satisfying the beforementioned standards for the production of certified seed. Upon application, a Member State may be authorized, in accordance with the procedure laid down in Article 21, to assess the satisfaction of the varietal purity standards in the case of crops of Poa pratensis belonging to such varieties not only on the basis of the results of the field inspection carried out in accordance with paragraph 6 of Annex I, where there is evidence that compliance with the varietal purity standards set up in Annex II is ensured by appropriate seed testing or other appropriate means.'
4. In the last sentence of paragraph 4, 'and of apomictic uni-clonal varieties of Poa spp' is deleted.
Article 2
Annex II to Directive 66/401/EEC is hereby amended as follows:
1. In the first indent of the second sentence of paragraph 1 of section I, 'Poa spp, apomictic uni-clonal varieties' is replaced by 'Poa pratensis, varieties referred to in the second part of the third sentence of paragraph 4 of Annex I'.
2. In the first sentence of paragraph 1 of section II, 'and of apomictic uni-clonal varieties of Poa spp' is
replaced by 'and of varieties of Poa pratensis referred to in the second part of the third sentence of paragraph 4 of Annex I'.
Article 3
Member States shall bring into force the laws, regulations or administrative provisions necessary to comply with this Directive not later than 1 January 1986. They shall forthwith inform the Commission and the other Member States thereof.
Article 4
This Directive is addressed to the Member States.
Done at Brussels, 14 December 1984.
For the Commission
Poul DALSAGER
Member of the Commission
(1) OJ No 125, 11. 7. 1966, p. 2298/66.
(2) OJ No L 131, 13. 5. 1982, p. 24.
