Commission Regulation (EC) No 1053/2003
of 19 June 2003
amending Regulation (EC) No 999/2001 of the European Parliament and of the Council as regards rapid tests
(Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Regulation (EC) No 999/2001 of the European Parliament and of the Council of 22 May 2001 laying down rules for the prevention, control and eradication of certain transmissible spongiform encephalopathies(1), as last amended by Commission Regulation (EC) No 260/2003(2), and in particular the first subparagraph of Article 23 thereof,
Whereas:
(1) Regulation (EC) No 999/2001 sets out a list of national reference laboratories for TSEs for the purposes of that Regulation. Greece has changed its national reference laboratory.
(2) Regulation (EC) No 999/2001 also sets out a list of rapid tests approved for TSE monitoring.
(3) The company marketing one of the rapid tests approved for TSE monitoring has informed the Commission of its intention to market the test under a new trade name.
(4) In its opinion of 6 and 7 March 2003, the Scientific Steering Committee recommended the inclusion of two new tests in the list of rapid tests approved for monitoring of bovine spongiform encephalopathy (BSE). The producers of both tests have provided data showing that their test may also be used for monitoring of TSE in sheep.
(5) In order to ensure that approved rapid tests maintain the same level of performance after approval a procedure should be laid down for possible modifications to the test or the test protocol.
(6) Regulation (EC) No 999/2001 should therefore be amended accordingly.
(7) The measures provided for in this Regulation are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS REGULATION:
Article 1
Annex X to Regulation (EC) No 999/2001 is amended in accordance with the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the 20th day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 19 June 2003.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 147, 31.5.2001, p. 1.
(2) OJ L 37, 13.2.2003, p. 7.
ANNEX
Annex X is amended as follows:
(a) In Chapter A, point 3, the text concerning Greece is replaced by the following:
">TABLE>"
(b) In Chapter C, point 4 is replaced by the following:
"4. Rapid tests
For the purposes of carrying out the rapid tests in accordance with Article 5(3) and Article 6(1), the following methods shall be used as rapid tests:
- immuno-blotting test based on a Western blotting procedure for the detection of the protease-resistant fragment PrPRes (Prionics-Check Western test),
- chemiluminescent ELISA test involving an extraction procedure and an ELISA technique, using an enhanced chemiluminescent reagent (Enfer test),
- sandwich immunoassay for PrPRes carried out following denaturation and concentration steps (Bio-Rad TeSeE test, the former Bio-Rad Platelia test). However, existing stocks bearing the name 'Bio-Rad Platelia test' may be used within nine months from the date of entry into force of this Regulation,
- microplate based immunoassay (ELISA) which detects protease-resistant PrPRes with monoclonal antibodies (Prionics-Check LIA test),
- automated conformation-dependent immunoassay comparing the reactivity of a detection antibody to the protease-sensitive and protease-resistant forms of PrPSc (some fraction of the protease-resistant PrPSc is equivalent to PrPRes) and to PrPC (InPro CDI-5 test).
The producer of the rapid tests must have a quality assurance system in place agreed by the Community reference laboratory, which ensures that the test performance does not change. The producer must provide the test protocol to the Community reference laboratory.
Modifications to the rapid test or to the test protocol may only be made following advance notification to the Community reference laboratory and provided that the Community reference laboratory finds that the modification does not reduce the sensitivity, specificity or reliability of the rapid test. That finding shall be communicated to the Commission and to the national reference laboratories."
