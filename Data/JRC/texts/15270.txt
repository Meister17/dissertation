Information communicated by Member States regarding State aid granted under Commission Regulation (EC) No 2204/2002 of 12 December 2002 on the application of Articles 87 and 88 of the EC Treaty to state aid for employment
(2006/C 236/05)
(Text with EEA relevance)
Aid No | XE 1/06 |
Member State | Estonia |
Region | Estonia |
Title of aid scheme or name of company receiving individual aid | National Development Plan Action 1.3 "Inclusive Labour Market" |
Legal basis | Sotsiaalministri 4. aprilli 2006. a määrus nr 35 "Meetme 1.3 "Võrdsed võimalused tööturul", välja arvatud riigi tööturuasutuste projektidele antava toetuse, tingimused ja toetuse kasutamise seire eeskiri" (RTL, 11.4.2006, 31, 553). |
Annual expenditure planned or overall amount of individual aid granted to the company | Overall amount for 2006 | Up to EUR 2,75 million |
Maximum aid intensity | In conformity with Articles 4(2)-(5), 5 and 6 of the Regulation | Yes |
Date of implementation | 14 April 2006 |
Duration of scheme or individual aid award | Until 31 December 2006 |
Objective of aid | Article 4: Creation of employment | Yes |
Article 5: Recruitment of disadvantaged and disabled workers | Yes |
Article 6: Employment of disabled workers | Yes |
Economic sectors concerned | All Community sectors eligible for employment aid [1] | Yes |
All manufacturing [1] | Yes |
All services [1] | Yes |
Other | Yes |
Name and address of the granting authority | Name: Tööturuamet |
Address: Gonsiori 29 EE-15156 Tallinn |
Other information | The scheme will be co-financed by the European Social Fund |
Aid subject to prior notification to the Commission | In conformity with Article 9 of the Regulation | Yes |
Aid No | XE 2/06 |
Member State | Poland |
Region | Region Południowy — Województwo Śląskie — 1.2 |
Title of aid scheme or name of company receiving individual aid | EMPLOYMENT AID SCHEME FOR BUSINESSES OPERATING IN KATOWICE IN 2005-2006 |
Legal basis | art. 18 ust. 2 pkt 8, art. 40 ust. 1, art. 42 ustawy z dnia 8 marca 1990 r. o samorządzie gminnym (Dz.U. z 2001 r. nr 142, poz. 1591 z późn. zm.),art. 7 ust. 3 ustawy z dnia 12 stycznia 1991 r. o podatkach i opłatach lokalnych (Dz.U. z 2002 r. nr 9, poz. 84 z późn. zm.) |
Annual expenditure planned or overall amount of individual aid granted to the company | Annual overall amount | EUR 0,7 million |
Loans guaranteed | |
Maximum aid intensity | In conformity with Article 4(2)-(5) of the Regulation | Yes | |
Date of implementation | 30.6.2005 |
Duration of scheme or individual aid award | Until 12.2006 |
Objective of aid | Article 4: Creation of employment | Yes |
Article 5: Recruitment of disadvantaged and disabled workers | No |
Article 6: Employment of disabled workers | No |
Economic sectors concerned | All Community sectors [2] eligible for employment aid | Yes |
Name and address of the granting authority | Name: Prezydent Miasta Katowice |
Address: PL-40-098 Katowice, ul. Młyńska 4 Tel: 2593-684 |
Other information | If the aid programme is co-financed by Community funds, please insert the following sentence: Not applicable |
Aid subject to prior notification to the Commission | In conformity with Article 9 of the Regulation | Yes | |
Aid No | XE 3/06 |
Member State | Poland |
Region | Miasto Wągrowiec, powiat wągrowiecki, Województwo wielkopolskie |
Title of aid scheme or name of company receiving individual aid | Ordinance No 27/2005 of Wągrowiec Town Council dated 30 June 2005 on exemptions from property tax within the framework of employment aid |
Legal basis | Art. 18 ust. 2 pkt 8, art. 40 ust. 1 i art. 41 ust. 1 i art. 42 ustawy z dnia 8 marca 1990 r. o samorządzie gminnym (Dz.U. z 2001 r. nr 142, poz. 1591 z zm.) i art. 7 ust. 3 ustawy z dnia 12 stycznia 1991 r. o podatkach i opłatach lokalnych (Dz.U. z 2002 r. nr 9, poz. 84 z zm.) |
Annual expenditure planned or overall amount of individual aid granted to the company | Annual overall amount | EUR 0,125 million PLN 0,5 million |
Maximum aid intensity | In conformity with Article 4 of the Regulation | Yes | |
Date of implementation | 1.9.2005 |
Duration of scheme or individual aid award | Until 31.12.2006 |
Objective of aid | Article 4: Creation of employment | Yes |
Article 5: Recruitment of disadvantaged and disabled workers | |
Article 6: Employment of disabled workers | |
Economic sectors concerned | All Community sectors [3] eligible for employment aid | Yes |
Name and address of the granting authority | Name: Rada Miejska |
Address: Ul. Kościuszki 15 A, PL-62-100 Wągrowiec Tel. (0 67) 26 80 312 e-mail; miasto@wagrowiec.um.gov.pl |
Aid subject to prior notification to the Commission | In conformity with Article 9 of the Regulation | Yes | |
Aid No | XE 4/06 |
Member State | Poland |
Region | Zachodniopomorskie |
Title of aid scheme or name of company receiving individual aid | Exemptions from property tax for businesses in Bobolice which create new jobs |
Legal basis | Uchwała nr XXV/271/05 Rady Miejskiej w Bobolicach z dnia 29 marca 2005 r. w sprawie zwolnienia z podatku od nieruchomości przedsiębiorców prowadzących działalność na terenie Gminy Bobolice tworzących nowe miejsca pracy art. 7 ust. 3 ustawy z dnia 12 stycznia 1991 r. o podatkach i opłatach lokalnych (Dz.U. z 2002 r., nr 9, poz. 84 z późn. zm.). |
Annual expenditure planned or overall amount of individual aid granted to the company | Annual overall amount | EUR 0,0125 million PLN 0,05 million |
Loans guaranteed | |
Maximum aid intensity | In conformity with Articles 4(2)-(5), 5 and 6 of the Regulation | Yes | |
Date of implementation | 15.6.2005 |
Duration of scheme or individual aid award | Until 31.12.2006 |
Objective of aid | Article 4: Creation of employment | Yes |
Article 5: Recruitment of disadvantaged and disabled workers | Yes |
Article 6: Employment of disabled workers | |
Economic sectors concerned | All Community sectors [4] eligible for employment aid | Yes |
Name and address of the granting authority | Name: Burmistrz Bobolic |
Address: Urząd Miejski w Bobolicach Ul. Ratuszowa 1 PL-76-020 Bobolice |
Other information | If the aid programme is co-financed by Community funds, please insert the following sentence: Not applicable |
Aid subject to prior notification to the Commission | In conformity with Article 9 of the Regulation | Yes | |
Aid No | XE 8/06 |
Member State | Poland |
Region | Entire country |
Title of aid scheme or name of company receiving individual aid | Job creation for the unemployed and allocation of funds to enable unemployed persons to start a business |
Legal basis | Rozporządzenie Ministra Pracy i Polityki Społecznej z dnia 21 listopada 2005 r. w sprawie szczegółowych warunków i trybu dokonywania refundacji ze środków Funduszu Pracy, kosztów wyposażenia lub wyposażenia stanowiska pracy dla skierowanego bezrobotnego, przyznawania bezrobotnemu środków na podjęcie działalności gospodarczej oraz form zabezpieczenia zwrotu otrzymanych środków (Dz.U. nr 236, poz. 2002). Rozporządzenie weszło w życie z dniem 5 grudnia 2005 r. Art. 46 ust. 1 pkt 1,2 ustawy z dnia 20 kwietnia 2004 r. o promocji zatrudnienia i instytucjach rynku pracy (Dz.U. nr 99, poz. 1001, z późn. zm.) |
Annual expenditure planned or overall amount of individual aid granted to the company | Annual overall amount | 2005 — EUR 40 million, including: EUR 30 million approx. from the national budgetEUR 10 million approx. from the ESF2006 — EUR 110 million, including: EUR 82,5 million approx. from the national budgetEUR 27,5 million approx. from the ESF |
Loans guaranteed | |
Maximum aid intensity | In conformity with Articles 4(2)-(5), 5 and 6 of the Regulation | Yes | |
Date of implementation | 5.12.2005 |
Duration of scheme or individual aid award | Until 31.12.2006 |
Objective of aid | Article 4: Creation of employment | Yes |
Article 5: Recruitment of disadvantaged and disabled workers | |
Article 6: Employment of disabled workers | |
Economic sectors concerned | All Community sectors [5] eligible for employment aid | Yes |
All manufacturing [5] | |
All services [5] | |
Other | |
Name and address of the granting authority | Name: Starostowie (Heads of District Authorities) |
Address: Around 340 offices nationwide |
Other information | The aid scheme is co-financed with the ESF under the "Development of Human Resources" Sector Operational Programme |
Aid subject to prior notification to the Commission | In conformity with Article 9 of the Regulation | Yes | |
[1] With the exception of the shipbuilding sector and of other sectors subject to special rules in regulations and directives governing all state aid within the sector.
[2] With the exception of the shipbuilding sector and of other sectors subject to special rules in regulations and directives governing all state aid within the sector.
[3] With the exception of the shipbuilding sector and of other sectors subject to special rules in regulations and directives governing all state aid within the sector.
[4] With the exception of the shipbuilding sector and of other sectors subject to special rules in regulations and directives governing all state aid within the sector.
[5] With the exception of the shipbuilding sector and of other sectors subject to special rules in regulations and directives governing all state aid within the sector.
--------------------------------------------------
