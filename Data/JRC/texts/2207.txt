Commission Regulation (EC) No 1666/2005
of 13 October 2005
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 14 October 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 13 October 2005.
For the Commission
J. M. Silva Rodríguez
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 386/2005 (OJ L 62, 9.3.2005, p. 3).
--------------------------------------------------
ANNEX
to Commission Regulation of 13 October 2005 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code | Standard import value |
07020000 | 052 | 49,2 |
204 | 52,0 |
999 | 50,6 |
07070005 | 052 | 101,8 |
999 | 101,8 |
07099070 | 052 | 98,7 |
999 | 98,7 |
08055010 | 052 | 73,8 |
382 | 63,3 |
388 | 68,1 |
524 | 57,2 |
528 | 70,3 |
999 | 66,5 |
08061010 | 052 | 88,3 |
400 | 215,8 |
999 | 152,1 |
08081080 | 388 | 85,2 |
400 | 107,5 |
512 | 89,6 |
528 | 11,2 |
720 | 48,5 |
800 | 163,1 |
804 | 77,5 |
999 | 83,2 |
08082050 | 052 | 90,7 |
388 | 56,9 |
720 | 54,1 |
999 | 67,2 |
[1] Country nomenclature as fixed by Commission Regulation (EC) No 750/2005 (OJ L 126, 19.5.2005, p. 12). Code "999" stands for "of other origin".
--------------------------------------------------
