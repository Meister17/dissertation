Commission Regulation (EC) No 473/2006
of 22 March 2006
laying down implementing rules for the Community list of air carriers which are subject to an operating ban within the Community referred to in Chapter II of Regulation (EC) No 2111/2005 of the European Parliament and of the Council
(Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Regulation (EC) No 2111/2005 of the European Parliament and the Council of 14 December 2005 on the establishment of a Community list of air carriers subject to an operating ban within the Community and on informing air transport passengers of the identity of the operating air carrier, and repealing Article 9 of Directive 2004/36/EC [1], (hereinafter referred to as "the basic Regulation"), and in particular Article 8 thereof,
Whereas:
(1) Chapter II of the basic Regulation lays down procedures for updating the Community list of air carriers which are subject to an operating ban within the Community as well as procedures allowing the Member States, in certain circumstances, to adopt exceptional measures imposing operating bans within their territory.
(2) It is appropriate to adopt certain implementing measures in order to provide detailed rules in respect of these procedures.
(3) In particular, it is appropriate to specify the information to be provided by the Member States when they request the Commission to adopt a decision under Article 4(2) of the basic Regulation to update the Community list by imposing a new operating ban, lifting an existing ban or modifying the attached conditions.
(4) It is necessary to lay down conditions for the exercise of the rights of defence of the carriers subject to the decisions adopted by the Commission in order to update the Community list.
(5) In the context of updating the list, the basic Regulation requires the Commission to take due account of the need for decisions to be taken swiftly and, where appropriate, provide a procedure for urgent cases.
(6) The Commission should receive adequate information on any operating ban imposed by the Member States as exceptional measures under Articles 6(1) and 6(2) of the basic Regulation.
(7) The measures provided for in this Regulation are in accordance with the opinion of the Air Safety Committee [2],
HAS ADOPTED THIS REGULATION:
Article 1
Subject matter
This Regulation lays down detailed rules in respect of the procedures referred to in Chapter II of the basic Regulation.
Article 2
Requests by Member States to update the Community list
1. A Member State that requests the Commission to update the Community list in accordance with Article 4(2) of the basic Regulation shall provide to the Commission the information indicated in Annex A to this Regulation.
2. The requests mentioned in paragraph 1 shall be addressed in writing to the Secretariat General of the Commission. In addition, the information described in Annex A shall be communicated simultaneously to the competent services of the Directorate General for Energy and Transport of the Commission electronically. If no suitable electronic procedure is available, the same information shall be provided by the fastest practicable alternative means.
3. The Commission shall inform the other Member States through their representatives in the Air Safety Committee in accordance with the procedures provided in the Committee’s internal rules, as well as the European Aviation Safety Agency.
Article 3
Joint consultation with the authorities with responsibility for regulatory oversight of the air carrier concerned
1. A Member State that is considering making a request to the Commission under Article 4(2) of the basic Regulation shall invite the Commission and the other Member States to participate in any consultations with the authorities with responsibility for regulatory oversight of the air carrier concerned.
2. The adoption of the decisions referred to in Article 4(2) and 5 of the basic Regulation shall be preceded, when appropriate and practicable, by consultations with the authorities with responsibility for regulatory oversight over the air carrier concerned. Whenever possible, consultations shall be held jointly by the Commission and the Member States.
3. In cases where urgency so requires, joint consultations may be held only after the adoption of the decisions referred in paragraph 2. In that case, the Authority concerned shall be informed that a decision is about to be adopted under Article 4(2) or Article 5(1).
4. Joint consultations may be conducted through correspondence and take place during on-site visits in order to permit the collection of evidence, where appropriate.
Article 4
Exercise of carriers’ right of defence
1. When the Commission is considering whether to adopt a decision under Article 4(2) or Article 5 of the basic Regulation, it shall disclose to the air carrier concerned the essential facts and considerations which form the basis for such decision. The air carrier concerned shall be given an opportunity to submit written comments to the Commission within 10 working days from the date of disclosure.
2. The Commission shall inform the other Member States through their representatives in the Air Safety Committee in accordance with the procedures provided in the Committee’s internal rules. If it so requests, the air carrier shall be permitted to present its position orally before a decision is reached. Where appropriate the oral presentation should be made to the Air Safety Committee. During the audition, the air carrier can be assisted by the authorities with responsibility for its regulatory oversight if it so requests.
3. In cases of urgency, the Commission shall not be required to comply with paragraph 1 before adopting a provisional measure in accordance with Article 5(1) of the basic Regulation.
4. When the Commission adopts a decision under Article 4(2) or Article 5 of the basic Regulation, it shall immediately inform the carrier and the authorities with responsibility for regulatory oversight over the air carrier concerned.
Article 5
Enforcement
Member States shall inform the Commission of any measures taken to implement the decisions adopted by the Commission under Articles 4(2) or 5 of the basic Regulation.
Article 6
Exceptional measures adopted by a Member State
1. When a Member State has subjected an air carrier to an immediate operating ban in its territory as permitted by Article 6(1) of the basic Regulation, it shall immediately inform the Commission of the fact and communicate the information mentioned in Annex B.
2. When a Member State has maintained or imposed an operating ban on an air carrier in its territory as permitted by Article 6(2) of the basic Regulation it shall immediately inform the Commission and communicate the information mentioned in Annex C.
3. The information mentioned in paragraphs 1 and 2 shall be addressed in writing to the Secretariat General of the Commission. In addition, the information described in Annex B or C shall be communicated simultaneously to the competent services of the Directorate General for Energy and Transport of the Commission electronically. If no suitable electronic procedure is available, the same information shall be provided by the fastest practicable alternative means.
4. The Commission shall inform the other Member States through their representatives in the Air Safety Committee in accordance with the procedures provided in the Committee’s internal rules.
Article 7
Entry into force
This Regulation shall enter into force on the first day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 22 March 2006.
For the Commission
Jacques Barrot
Vice-President
[1] OJ L 344, 27.12.2005, p. 15.
[2] Established by Article 12 of Council Regulation (EEC) No 3922/91 of 16 December 1991 on the harmonisation of technical requirements and administrative procedures in the field of civil aviation (OJ L 373, 31.12.1991, p. 4).
--------------------------------------------------
ANNEX A
Information to be provided by a Member State making a request under Article 4(2) of the basic regulation
A Member State which requests under Article 4(2) of the Basic regulation the updating of the Community list shall provide the following information to the Commission:
Member State making the request
- Name and function of official contact
- E-mail or telephone of official contact.
Carrier(s) and aircraft
- Concerned carrier(s), including legal entity name (indicated on AOC or equivalent) trading name (if different), AOC number (if available), ICAO airline designation number (if known) and full contact details
- Name(s) and full contact details of the authority or authorities with responsibility for regulatory oversight of the air carrier(s) concerned
- Details of the aircraft type(s), State(s) of registry, registration number(s) and, if available, construction serial numbers of the aircraft affected.
Decision requested
- Type of decision requested: imposition of operating ban, removal of an operating ban or modification of the conditions of an operating ban
- Scope of the requested decision (specific carrier(s) or all carriers subject to a particular overseeing authority, specific aircraft or type(s) of aircraft).
Request for the imposition of an operating ban
- Detailed description of the safety concern (eg inspection results) which led to the request for a total or partial ban (related in order to each of the relevant common criteria in the Annex to the basic Regulation)
- Broad description of recommended condition(s) allowing the proposed ban to be cancelled/waived for use as the basis for preparing a corrective action plan in consultation with the authority or authorities with responsibility for regulatory oversight of the air carrier(s) concerned.
Request for the lifting of an operating ban or the modification of attached conditions
- Date and details of agreed corrective action plan, if applicable
- Evidence of subsequent compliance with the agreed corrective plan, if applicable
- Explicit written endorsement by the authority or authorities with responsibility for regulatory oversight of the air carrier(s) concerned that the corrective action plan has been implemented.
Publicity
- Information on whether the Member State has made public its request.
--------------------------------------------------
ANNEX B
Communication by a Member State of exceptional measures taken under article 6(1) of the basic Regulation to impose an operating ban in its territory
A Member State reporting that an air carrier has been made subject to an operating ban in its territory in accordance with Article 6(1) of the basic Regulation shall provide the following information to the Commission:
Member State making the report
- Name and function of the official contact
- E-mail or telephone of the official contact.
Carrier(s) and aircraft
- Concerned carrier(s), including legal entity name (indicated on AOC or equivalent) trading name (if different), AOC number (if available), ICAO airline designation number (if known) and full contact details
- Name(s) and full contact details of the authority or authorities with responsibility for regulatory oversight of the air carrier(s) concerned
- Details of the aircraft type(s), State(s) of registry, registration number (s) and, if available construction serial number(s) of the aircraft affected.
Decision
- Date, time and duration of decision
- Description of decision to refuse, suspend, revoke or impose restrictions on an operating authorisation or technical permission
- Scope of the decision (specific carrier(s) or all carriers subject to a particular overseeing authority, specific aircraft or type(s) of aircraft)
- Description of condition(s) allowing the refusal, suspension, revocation or restrictions of the operating authorisation or technical permission delivered by the Member State to be cancelled/waived.
Safety concern
- Detailed description of safety concern (i.e. inspection results) which lead to total or partial ban decision (related to the order of each of the common criteria in the Annex to the basic Regulation).
Publicity
- Information on whether the Member State has made its ban public.
--------------------------------------------------
ANNEX C
Communication by a Member State of exceptional measures taken as permitted by article 6(2) of the basic Regulation to maintain or impose an operating ban in its territory when the Commission has decided not to include similar measures in the Community list
A Member State reporting that an operating ban on an air carrier in its territory has been maintained or imposed as permitted by Article 6(2) of the basic Regulation shall provide the following information to the Commission:
Member State making the report
- Name and function of the official contact
- E-mail or telephone of the official contact.
Carrier(s) and aircraft
- Concerned carrier(s), including legal entity name (indicated on AOC or equivalent) trading name (if different), AOC number (if available), ICAO airline designation number (if known).
Reference to the Commission decision
- date of and reference to any relevant Commission documents
- date of Commission/Air Safety Committee decision.
Safety problem specifically affecting the Member State
--------------------------------------------------
