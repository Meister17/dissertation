Commission Regulation (EC) No 892/2005
of 14 June 2005
amending Regulation (EC) No 462/2005 as regards the quantity covered by the standing invitation to tender for the export of barley held by the German intervention agency
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1784/2003 of 29 September 2003 on the common organisation of the market in cereals [1], and in particular Article 6 thereof,
Whereas:
(1) Commission Regulation (EEC) No 2131/93 [2] lays down the procedure and conditions for the disposal of cereals held by intervention agencies.
(2) Commission Regulation (EC) No 462/2005 [3] opened a standing invitation to tender for the export of 1000693 tonnes of barley held by the German intervention agency.
(3) Germany has informed the Commission of the intention of its intervention agency to increase by 300000 tonnes the quantity put out to tender for export. In view of the market situation, the request made by Germany should be granted.
(4) This increase in the quantity put out to tender makes it necessary to alter the quantity stored by region of storage referred to in Annex I to Regulation (EC) No 462/2005.
(5) Regulation (EC) No 462/2005 should therefore be amended accordingly.
(6) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Cereals,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 462/2005 is amended as follows:
1. Article 2 is replaced by the following:
"Article 2
1. The invitation to tender shall cover a maximum of 1300693 tonnes of barley for export to third countries with the exception of Albania, Bosnia and Herzegovina, Bulgaria, Canada, Croatia, the Former Yugosla
v Republic of Macedonia, Liechtenstein, Mexico, Romania, Serbia and Montenegro [4], Switzerland and the United States of America.
2. The regions in which the 1300693 tonnes of barley are stored are listed in Annex I.
2. Annex I is replaced by the text in the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 14 June 2005.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 270, 21.10.2003, p. 78.
[2] OJ L 191, 31.7.1993, p. 76. Regulation as last amended by Regulation (EC) No 749/2005 (OJ L 126, 19.5.2005, p. 10).
[3] OJ L 75, 22.3.2005, p. 27. Regulation as last amended by Regulation (EC) No 610/2005 (OJ L 101, 21.4.2005, p. 9).
[4] Including Kosovo, as defined in UN Security Council Resolution No 1244 of 10 June 1999."
--------------------------------------------------
ANNEX
--------------------------------------------------
