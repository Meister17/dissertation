Commission Decision
of 19 March 2002
laying down case definitions for reporting communicable diseases to the Community network under Decision No 2119/98/EC of the European Parliament and of the Council
(notified under document number C(2002) 1043)
(2002/253/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Decision No 2119/98/EC of the European Parliament and of the Council of 24 September 1998 setting up a network for the epidemiological surveillance and control of communicable diseases in the Community(1), and in particular Article 3(c) thereof,
Whereas:
(1) Member States should communicate information on the epidemiological development and emergence of public health threats due to communicable diseases using the Community network in a way which allows comparisons to be made for preventive and control action to be taken at Community and national level.
(2) For comparability of such information, the setting up of common case definitions is a prerequisite even where disease-specific surveillance networks have not yet been put in place. As soon as this Decision comes into effect these case definitions should be used for reporting to the Community network, and should comply with regulations on individual data protection.
(3) The case definitions which allow comparable reporting should comprise a tiered system allowing Member States' structures and/or authorities flexibility in communicating information on diseases and special health issues. In particular, these case definitions will facilitate reporting on diseases listed in Commission Decision 2000/96/EC(2).
(4) Case definitions should be constructed to enable all Member States to participate in the reporting to the greatest extent possible, using data from their existing systems. They should allow for different levels of sensitivity and specificity according to the different goals of information collection and they should be easy to amend.
(5) The measures provided for in this Decision are in accordance with the opinion of the Committee set up by Decision No 2119/98/EC,
HAS ADOPTED THIS DECISION:
Article 1
For the purposes of submitting data for the epidemiological surveillance and control of communicable diseases under the provisions of Decision No 2119/98/EC, and in particular Article 4 thereof, Member States shall apply the case definitions specified in the Annex.
Article 2
This Decision will be adapted to the extent necessary on the basis of the latest scientific data.
Article 3
This Decision shall apply as of 1 January 2003.
Article 4
This Decision is addressed to the Member States.
Done at Brussels, 19 March 2002.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 268, 3.10.1998, p. 1.
(2) OJ L 28, 3.2.2000, p. 50.
ANNEX
CASE DEFINITIONS FOR COMMUNICABLE DISEASES LISTED IN DECISION 2000/96/EC
GENERAL PRINCIPLES FOR THE APPLICATION OF THESE CASE DEFINITIONS
- Unless specifically stated, only symptomatic cases are to be reported, however, asymptomatic infections are to be regarded as cases, if the infection has therapeutic or public health implications.
- A "case with an epidemiological link" is a case that has either been exposed to a confirmed case, or has had the same exposure as a confirmed case (e.g. eaten the same food, stayed in the same hotel, etc.).
- A three-tiered system with following levels is to be used:
- confirmed case: verified by laboratory analysis,
- probable case: clear clinical picture, or linked epidemiologically to a confirmed case,
- possible case: indicative clinical picture without being a confirmed or probable case.
The classification on these different levels might vary according to the epidemiology of the individual diseases.
- Clinical symptoms listed are only given as indicative examples and not exhaustive.
- For most diseases, several "criteria for laboratory diagnosis" are listed. Unless otherwise stated, only one of these is needed to confirm a case.
- N.A. in the case definition list means "not applicable".
INTRODUCTORY NOTES
1. The information reported in this document is intended only for uniform reporting/comparability of data within the Community network. The clinical description gives a general outline of the disease and does not necessarily indicate all the features needed for clinical diagnosis of the disease.
2. The laboratory criteria for diagnosis reported here may be fulfilled with different testing methods. However, when specific techniques are indicated, their use is recommended.
CASE DEFINITIONS
ACQUIRED IMMUNODEFICIENCY SYNDROME (AIDS) AND HIV INFECTION
1. Aids
Clinical description
Includes all human immunodeficiency virus (HIV)-infected individuals who have any of the 28 clinical conditions listed in the European AIDS surveillance case definition.
Criteria for diagnosis
I. Adults and adolescents: 1993 European AIDS surveillance case definition (see Annex II).
II. Children aged &lt; 13 years: 1995 revision of the European case definition for AIDS surveillance in children (see Annex III).
Case classification
>TABLE>
2. HIV infection
Clinical description
The diagnosis is based on laboratory criteria of HIV infection or a diagnosis of AIDS.
Laboratory criteria for diagnosis
I. Adults, adolescents and children aged &gt;=18 months
- Positive result on a screening HIV antibody test confirmed by a different HIV antibody test
- Detection of HIV nucleic acid (RNA or DNA)
- Detection of HIV by HIV p24 antigen test, including neutralisation assay
- HIV isolation (viral culture)
II. Children &lt; 18 months
- Positive results on two separate determinations (excluding cord blood) from one or more of the following HIV detection tests:
- HIV nucleic acid (RNA or DNA) detection
- HIV p24 antigen test, including neutralisation assay, in a child &gt;=1 month of age
- HIV isolation (viral culture).
Case classification
>TABLE>
ANTHRAX
Clinical description
Inhalational anthrax
After inhalation of Bacillus anthracis and a brief prodrome acute febrile respiratory failure develops with hypoxia, dyspnoa and radiological evidence of mediastinal widening.
Cutaneous anthrax
A skin lesion evolving from a papule, through a vesicular stage to a depressed black eschar with surrounding oedema. The lesion is usually painless but there may be constitutional disturbance (fever and malaise).
Gastointestinal anthrax
Following consumption of raw contaminated food a syndrome of severe abdominal pain, diarrhoea, fever and septicaemia.
Laboratory criteria for diagnosis
- Isolation and confirmation of B. anthracis from specimens collected from a normally sterile site (e.g. blood or CSF) or lesion of other affected tissue (skin, lung or gut);
- both of the following:
- evidence of B. anthracis DNA (e.g. by PCR) from specimens collected from a normally sterile site (e.g. blood or CSF) or lesion of other affected tissue (skin, lung or gut),
- demonstration of B. anthracis in a clinical specimen by immunohistochemical staining of affected tissue (skin, lung or gut).
Nasal swab without indication of disease does not contribute to diagnosis of a case.
Case classification
>TABLE>
BOTULISM, FOODBORNE
Clinical description
Clinical picture compatible with botulism, e.g. symptoms such as diplopia, blurred vision and bulbar weakness. Symmetric paralysis may progress rapidly.
Laboratory criteria for diagnosis
- Detection of botulinum toxin in serum, stool, stomach content or patient's food
- Isolation of Clostridium botulinum from stool.
Case classification
>TABLE>
BRUCELLOSIS
Clinical description
Clinical picture compatible with brucellosis, e.g. acute or insidious onset of fever, night sweats, undue fatigue, anorexia, weight loss, headache and arthralgia.
Laboratory criteria for diagnosis
- Demonstration of a specific antibody response
- Demonstration by immunofluorescence of Brucella sp. in a clinical specimen
- Isolation of Brucella sp. from a clinical specimen
For probable case:
- A single high titre.
Case classification
>TABLE>
CAMPYLOBACTER INFECTION
Clinical description
Clinical picture compatible with campylobacteriosis, e.g. diarrhoeal illness of variable severity.
Laboratory criteria for diagnosis
- Isolation of Campylobacter sp. from any clinical specimen.
Case classification
>TABLE>
CHLAMYDIA TRACHOMATIS, GENITAL INFECTION
Clinical description
Clinical picture compatible with Chlamydia trachomatis infection, e.g. urethritis, epididymitis, cervicitis, acute salpingitis or other syndromes when sexually transmitted.
Laboratory criteria for diagnosis
- Isolation of C. trachomatis by culture from specimen of the uro-genital tract
- Demonstration of C. trachomatis in a clinical specimen from the uro-genital tract by detection of antigen or nucleic acid.
Case classification
>TABLE>
CHOLERA
Clinical description
Clinical picture compatible with cholera, e.g. watery diarrhoea and/or vomiting. Severity is variable.
Laboratory criteria for diagnosis
- Isolation of toxigenic (i.e. cholera toxin-producing) Vibrio cholerae O1 or O139 from stool or vomitus
- Demonstration of a specific anti-toxin and vibrocidal antibody response.
Case classification
>TABLE>
CRYPTOSPORIDIOSIS
Clinical description
Clinical picture compatible with cryptosporidiosis, characterised by diarrhoea, abdominal cramps, loss of appetite, nausea and vomiting.
Laboratory criteria for diagnosis
- Demonstration of Cryptosporidium oocysts in stool
- Demonstration of Cryptosporidium in intestinal fluid or small-bowel biopsy specimens
- Demonstration of Cryptosporidium antigen in stool.
Case classification
>TABLE>
DIPHTHERIA
Clinical description
Clinical picture compatible with diphtheria, e.g. an upper-respiratory tract illness characterised by sore throat, low-grade fever, and an adherent membrane of the tonsil(s), pharynx, and/or nose.
Laboratory criteria for diagnosis
- Isolation of toxin-producing Corynebacterium diphtheriae from a clinical specimen
- Histopathologic diagnosis of diphtheria.
Case classification
>TABLE>
Note that asymptomatic carriers, cases with non-toxigenic C. diphteriae or cutaneous diphteria should not be reported.
ECHINOCOCCOSIS
Clinical description
Clinical picture compatible with echinococcosis, which may produce any of several clinical syndromes, varying with cyst size and location.
Laboratory criteria for diagnosis
Diagnosis by:
- Histopathology
- A combination of imaging techniques and serological tests (e.g. indirect haemagglutination, immunodiffusion, immunoblot assay).
Case classification
>TABLE>
EHEC (infection with entero-haemorrhagic Escherichia coli)
Clinical description
Clinical picture compatible with EHEC infection, e.g. diarrhoea (often bloody) and abdominal cramps. Illness may be complicated by haemolytic uraemic syndrome (HUS) or thrombotic thrombocytopenic purpura (TTP).
Laboratory criteria for diagnosis
- Isolation of E. coli belonging to a sero-group known to cause enterohaemorrhagic disease
- Serological confirmation in patients with HUS or TTP
- For probable cases: detection of genes coding for St×1/St×2 production.
Case classification
>TABLE>
GIARDIASIS
Clinical description
Clinical picture compatible with infection with Giardia lamblia, characterised by diarrhoea, abdominal cramps, bloating, weight loss, or malabsorption.
Laboratory criteria for diagnosis
- Demonstration of G. lamblia cysts in stool
- Demonstration of G. lamblia trophozoites in stool, duodenal fluid, or small-bowel biopsy
- Demonstration of G. lamblia antigen in stool.
Case classification
>TABLE>
GONORRHOEA
Clinical description
Clinical picture compatible with gonorrhoea, e.g. urethritis, cervicitis, or salpingitis.
Laboratory criteria for diagnosis
- Isolation of Neisseria gonorrhoeae from a clinical specimen
- Detection of N. gonorrhoeae antigen or nucleic acid
- Demonstration of gram-negative intracellular diplococci in an urethral smear from a male.
Case classification
>TABLE>
HAEMOPHILUS INFLUENZAE TYPE B, INVASIVE
Clinical description
Clinical picture compatible with invasive disease, e.g. bacteremia, meningitis, arthritis, epiglottitis, osteomyelitis or cellulitis.
Laboratory criteria for diagnosis
- Isolation of Haemophilus influenzae type B from normally sterile site
- Detection of H. influenzae nucleic acid from normally sterile site
For probable case:
- Detection of H. influenzae antigen from normally sterile site.
Case classification
>TABLE>
HEPATITIS, VIRAL
Clinical description
In symptomatic cases clinical picture compatible with hepatitis, e.g. discrete onset of symptoms and jaundice or elevated serum aminotransferase levels.
Hepatitis A, acute
Laboratory criteria for diagnosis
- IgM antibody to hepatitis A virus (anti-HAV) positive
- Detection of antigen in stool
- Detection of nucleic acid in serum.
Case classification
>TABLE>
Hepatitis B, acute
Laboratory criteria for diagnosis
- IgM antibody to hepatitis B core antigen (anti-HBc) positive
- Detection of HBV nucleic acid in serum.
Case classification
>TABLE>
Hepatitis C
Laboratory criteria for diagnosis
- Detection of HCV-specific antibodies
- Detection of HCV nucleic acid from clinical samples.
Case classification
>TABLE>
HIV INFECTION
(See under Acquired Immunodeficiency Syndrome above).
INFLUENZA
Clinical description
Clinical picture compatible with influenza, e.g. sudden onset of disease, cough, fever &gt; 38° C, muscular pain and/or headache.
Laboratory criteria for diagnosis
- Detection of influenza antigen, or influenza virus specific RNA
- Isolation of influenza virus
- Demonstration of a specific serum antibody response to influenza A or B.
Case classification
>TABLE>
LEGIONELLOSIS
Legionnaires' disease
Clinical description
Pneumonia
Pontiac fever
Clinical description
A self-limiting influenza-like illness characterised by fever, headache, myalgia and non-productive cough. Patients recover spontaneously without therapy after 2 to 5 days. No signs of pneumonia.
Laboratory criteria for diagnosis of legionellosis
- Isolation of any Legionella organism from respiratory secretion, lung tissue or blood
- Demonstration of a specific antibody response to Legionella pneumophila serogroup 1 or other serogroups or other Legionella species by the indirect immunofluorescent antibody test or by microagglutination
- Detection of specific Legionella antigen in urine using validated reagents
For probable case:
- A single high titre in specific serum antibody to L. pneumophila serogroup 1 or other serogroups or other Legionella species
- Detection of specific Legionella antigen in respiratory secretion or direct fluorescent antibody (DFA) staining of the organism in respiratory secretion or lung tissue using evaluated monoclonal reagents.
Case classification
>TABLE>
LEPTOSPIROSIS
Clinical description
Clinical picture compatible with leptospirosis, characterised by fever, headache, chills, myalgia, conjunctival suffusion, and less frequently by meningitis, rash, jaundice or renal insufficiency.
Laboratory criteria for diagnosis
- Isolation of Leptospira from a clinical specimen
- Demonstration of a specific increase in Leptospira agglutination titre
- Demonstration of Leptospira in a clinical specimen by immunofluorescence
- Detection of Leptospira IgM antibody in serum.
Case classification
>TABLE>
LISTERIOSIS
Clinical description
Infection caused by Listeria monocytogenes, which may produce any of several clinical syndromes, including stillbirth, listeriosis of the newborn, meningitis, bacteremia or localised infections.
Laboratory criteria for diagnosis
- Isolation of L. monocytogenes from a normally sterile site (e.g. blood or cerebrospinal fluid or, less commonly, joint, pleural or pericardial fluid).
Case classification
>TABLE>
MALARIA
Clinical description
Clinical picture compatible with malaria, e.g. fever and common associated symptoms, which includes headache, back pain, chills, sweats, myalgia, nausea, vomiting, diarrhoea and cough.
Laboratory criteria for diagnosis
- Demonstration of malaria parasites in blood films
- Detection of Plasmodium nucleic acid.
Case classification
>TABLE>
MEASLES
Clinical description
Clinical picture compatible with measles, i.e. a generalised rash lasting &gt;3 days and a temperature &gt;38,0° C and one or more of the following: cough, coryza, Koplik's spots, conjunctivitis.
Laboratory criteria for diagnosis
- Detection of IgM antibodies against measles in the absence of recent vaccination
- Demonstration of a specific measles antibody response in absence of recent vaccination
- Detection of measles virus (not vaccine strains) in a clinical specimen.
Case classification
>TABLE>
MENINGOCOCCAL DISEASE
Clinical description
Clinical picture compatible with meningococcal disease, e.g. meningitis and/or meningococcemia that may progress rapidly to purpura fulminans, shock and death. Other manifestations are possible.
Laboratory criteria for diagnosis
- Isolation of Neisseria meningitidis from a normally sterile site (e.g. blood or cerebrospinal fluid (CSF) or, less commonly, joint, pleural or pericardial fluid)
- Detection of N. meningitidis nucleic acid from normally sterile site
- Detection of N. meningitidis antigen from normally sterile site
- Demonstration of gram-negative diplococci from normally sterile site by microscopy
For probable case:
- Single high titre of meningococcal antibody in convalescent serum.
Case classification
>TABLE>
Note that asymptomatic carriers should not be reported.
MUMPS
Clinical description
Clinical picture compatible with mumps, e.g. acute onset of uni- or bilateral tender, self-limited swelling of the parotid or other salivary gland, lasting &gt;2 days, and without other apparent cause.
Laboratory criteria for diagnosis
- Detection of mumps IgM antibody
- Demonstration of specific mumps antibody response in absence of recent vaccination
- Isolation of mumps virus (not vaccine strains) from clinical specimen
- Detection of mumps nucleic acid
Case classification
>TABLE>
PERTUSSIS (WHOOPING COUGH)
Clinical description
Clinical picture compatible with pertussis, e.g. a cough illness lasting at least 2 weeks with one of the following: paroxysms of coughing, inspiratory "whoop" or post-tussive vomiting without other apparent cause.
Laboratory criteria for diagnosis
- Demonstration of a specific pertussis antibody response in absence of recent vaccination
- Detection of nucleic acid
- Isolation of Bordetella pertussis from clinical specimen.
Case classification
>TABLE>
PLAGUE
Clinical description
The disease is characterized by fever, chills, headache, malaise, prostration and leukocytosis that manifests in one or more of the following principal clinical forms:
- regional lymphadenitis (bubonic plague),
- septicaemia without an evident bubo (septicemic plague),
- plague pneumonia,
- pharyngitis and cervical lymphadenitis.
Laboratory criteria for diagnosis
- Isolation of Yersinia pestis from a clinical specimen
- Demonstration of a specific antibody response to Y. pestis F1 antigen.
For probable case:
- Elevated serum antibody titre(s) to Y. pestis fraction 1 (F1) antigen (without documented specific change) in a patient with no history of plague vaccination
- Detection of F1 antigen in a clinical specimen by fluorescent assay.
Case classification
>TABLE>
POLIOMYELITIS, PARALYTIC
Clinical description
Clinical picture compatible with poliomyelitis, e.g. acute onset of a flaccid paralysis of one or more limbs with decreased or absent tendon reflexes in the affected limbs, without other apparent cause and without sensory or cognitive loss.
Laboratory criteria for diagnosis
- Isolation of poliovirus from a clinical specimen
- Detection of polio virus nucleic acid.
Case classification
>TABLE>
RABIES, HUMAN
Clinical description
Rabies is an acute encephalomyelitis that almost always progresses to coma or death within 10 days after the first symptom.
Laboratory criteria for diagnosis
- Detection by direct fluorescent antibody of viral antigens in a clinical specimen (preferably the brain or the nerves surrounding hair follicles in the nape of the neck)
- Detection of rabies nucleic acid in clinical specimen
- Isolation (in cell culture or in a laboratory animal) of rabies virus from saliva, cerebrospinal fluid (CSF), or central nervous system tissue
- Identification of a rabies-neutralising antibody titre (complete neutralization) in the serum or CSF of an unvaccinated person.
Case classification
>TABLE>
RUBELLA
Clinical description
Clinical picture compatible with rubella, e.g. acute onset of generalized maculopapular rash and arthralgia/arthritis, lymphadenopathy, or conjunctivitis.
Laboratory criteria for diagnosis
- Detection of rubella IgM antibody in absence of recent vaccination
- Demonstration of a specific rubella antibody response in absence of recent vaccination
- Isolation of rubella virus in absence of recent vaccination
- Detection of rubella nucleic acid in clinical specimen.
Case classification
>TABLE>
SALMONELLOSIS (NON-TYPHI, NON-PARATYPHI)
Clinical description
Clinical picture compatible with salmonellosis, e.g. diarrhoea, abdominal pain, nausea and sometimes vomiting. The organism may cause extraintestinal infections.
Laboratory criteria for diagnosis
- Isolation of Salmonella (non-typhi, non-paratyphi) from a clinical specimen.
Case classification
>TABLE>
SHIGELLOSIS
Clinical description
An illness of variable severity characterised by diarrhoea, fever, nausea, cramps, and tenesmus.
Laboratory criteria for diagnosis
- Isolation of Shigella sp. from a clinical specimen.
Case classification
>TABLE>
STREPTOCOCCUS PNEUMONIAE, INVASIVE DISEASE
Clinical description
Steptococcus pneumoniae causes many clinical syndromes, depending on the site of infection (e.g. acute otitis media, pneumonia, bacteremia, or meningitis).
Laboratory criteria for diagnosis
- Isolation of S. pneumoniae from a normally sterile site (e.g. blood, cerebrospinal fluid, or, less commonly, joint, pleural or pericardial fluid)
- Detection of S. pneumoniae nucleic acid from a normally sterile site
For probable case:
- Detection of S. pneumoniae antigen from a normally sterile site.
Case classification
>TABLE>
SYPHILIS
Syphilis, primary
Clinical description
A stage of infection with Treponema pallidum characterised by one or more chancres (ulcers). Chancres might differ considerably in clinical appearance.
Laboratory criteria for diagnosis
- Detection of specific IgM by EIA
- Demonstration of T. pallidum in clinical specimens by dark field microscopy, direct fluorescent antibody (DFA-TP) or equivalent methods
For probable case:
- A reactive serologic test (nontreponemal: Venereal Disease Research Laboratory (VDRL) or rapid plasma reagin (RPR); treponemal: fluorescent treponemal antibody absorbed (FTA-ABS) or microhemagglutination assay for antibody to T. pallidum (MHA-T]).
Case classification
>TABLE>
Syphilis, secondary
Clinical description
A stage of infection caused by T. pallidum and characterised by localised or diffuse mucocutaneous lesions, often with generalised lymphadenopathy. The primary chancre may still be present.
Laboratory criteria for diagnosis
- Demonstration of T. pallidum in clinical specimens by dark field microscopy, direct fluorescent antibody (DFA-TP) or equivalent methods
For probable case:
- A reactive serologic test (nontreponemal: Venereal Disease Research Laboratory (VDRL)
- Rapid plasma reagin (RPR); treponemal: fluorescent treponemal antibody absorbed (FTA-ABS)
- Microhaemagglutination assay for antibody to T. pallidum (MHA-TP).
Case classification
>TABLE>
Syphlis, latent
Clinical description
A stage of infection caused by T. pallidum in which organisms persist in the body of the infected person without causing symptoms or signs.
Laboratory criteria for diagnosis
Demonstration of a positive reaction with a specific EIA but negative for laboratory test for infectious syphilis (see primary or secondary syphilis).
Case classification
>TABLE>
TETANUS
Clinical description
Clinical picture compatible with tetanus, e.g. acute onset of hypertonia and/or painful muscular contractions (usually of the muscles of the jaw and neck) and generalised muscle spasms without other apparent medical cause.
Laboratory criteria for diagnosis
- Detection of tetanus toxoid antibody in an unvaccinated and untreated patient
- Demonstration of a specific tetanus toxoid antibody response.
Case classification
>TABLE>
TOXOPLASMOSIS
Clinical description
A protozoan disease, which presents with an acute illness with one or more of the following: lymphadenopathy, encephalitis, chorioretinitis, disfunction of the central nervous system. Congenital infections may also occur with hydrocephalus, microcephalus, intracerebral calcification, convulsions, cerebral retardation.
Laboratory criteria for diagnosis
- Demonstration of a specific toxoplasma antibody response
- Demonstration of the agent in body tissues or fluids or isolation in animals or cell culture
- Detection of toxoplasma nucleic acid.
Case classification
>TABLE>
TRICHINOSIS
Clinical description
A disease caused by ingestion of Trichinella larvae. The disease has variable clinical manifestations. Common signs and symptoms among symptomatic persons include eosinophilia, fever, myalgia and periorbital oedema.
Laboratory criteria for diagnosis
- Demonstration of Trichinella larvae in tissue obtained by muscle biopsy
- Demonstration of a specific Trichinella antibody response.
Case classification
>TABLE>
TUBERCULOSIS
Clinical criteria
- A clinician's judgement that clinical and/or radiological signs and/or symptoms are compatible with tuberculosis
and
- a clinician's decision to treat the patient with a full course of anti-tuberculosis therapy.
Laboratory criteria
- Isolation of Mycobacterium tuberculosis complex (except M. bovis BCG) from any clinical specimen by culture
- Evidence of acid-fast bacilli (AFB) at microscopic examination of spontaneous or induced sputum.
Classification according to laboratory criteria
Definite
A case with isolation of M. tuberculosis complex (except M. bovis BCG) from any clinical specimen. In countries where culture is not routinely available, a case with sputum smear examinations positive for AFB is also considered to be a definite case.
Other than definite
A case that meets the clinical criteria above but does not meet the laboratory criteria of a definite case.
Classification according to site of disease
Pulmonary tuberculosis
Tuberculosis of the lung parenchyma or the tracheo-bronchial tree.
Extrapulmonary tuberculosis
Tuberculosis affecting any site other than pulmonary as defined above.
Classification according to previous anti-tuberculosis treatment
Never treated
A case which never received a treatment for active tuberculosis in the past or which received anti-tuberculosis drugs for less than one month.
Previously treated
A case which was diagnosed with active tuberculosis in the past and received anti-tuberculosis drugs (excluding preventive therapy) for at least one month.
TYPHOID/PARATYPHOID FEVER
Clinical description
An illness caused by Salmonella typhi or paratyphii that is often characterised by insidious onset of sustained fever, headache, malaise, anorexia, relative bradycardia, constipation or diarrhoea and nonproductive cough. However, many mild and atypical infections occur.
Laboratory criteria for diagnosis
- Isolation of S. typhi or paratyphii from blood, stool or other clinical specimen.
Case classification
>TABLE>
VARIANT CREUTZFELDT-JAKOB'S DISEASE
Clinical description
I. History
- Progressive neuropsychiatric disorder,
- Duration of illness &gt; 6 months,
- Routine investigation do not suggest an alternative diagnosis,
- No history of potential iatrogenic exposure.
II. Clinical features
- Early psychiatric symptoms,
- Persistent painful sensory symptoms,
- Ataxia,
- Myoclonus or chorea or dystonia,
- Dementia.
Laboratory criteria for diagnosis
- EEG does not show typical appearance of classical CJD (or no EEG performed)
- Bilateral pulvinar high signal on MRI scan
- Characteristic neuropathological and immunopathological findings.
Case classification
>TABLE>
VIRAL HAEMORRHAGIC FEVERS
Ebola/Marburg fever
Clinical description
Begins with acute fever, diarrhoea that can be bloody and vomiting. Headache, nausea, and abdominal pain are common. Haemorrhagic manifestations may follow. Some patients may also show a maculopapular rash on the trunk.
Laboratory criteria for diagnosis
- Positive virus isolation
- Positive skin biopsy (immunohistochemistry)
- Detection of Ebola/Marburg virus nucleic acid
- Positive serology, which may appear late in the course of the disease.
Case classification
>TABLE>
Lassa fever
Clinical description
An illness of gradual onset with malaise, fever, headache, sore throat, cough, nausea, vomiting, diarrhoea, myalgia and chest pain. Haemorrhagic manifestations may follow.
Laboratory criteria for diagnosis
- Virus isolation
- Positive skin biopsy (immunohistochemistry)
- Detection of Lassa virus nucleic acid
- Positive serology, which may appear late in the course of the disease.
Case classification
>TABLE>
Congo-Crimean haemorrhagic fever
Clinical description
An illness of gradual onset with acute high fever, chills, myaliga, nausea, anorexia, vomitting, headache and backache. Haemorrhagic manifestations may follow.
Laboratory criteria for diagnosis
- Virus isolation
- Detection of CCHF virus nucleic acid
- Positive serology, which may appear late in the course of the disease.
Case classification
>TABLE>
YELLOW FEVER
Clinical description
An illness characterised by acute onset and constitutional symptoms followed by a brief remission, a recurrence of fever, hepatitis, albuminuria, and in some instances, renal failure, shock and generalised hæmorrhages.
Laboratory criteria for diagnosis
- Demonstration of a specific yellow fever antibody response in a patient who has no history of recent yellow fever vaccination and where cross-reactions to other flaviviruses have been excluded
- Virus isolation
- Detection of yellow fever antigen
- Detection of yellow fever nucleic acid.
Case classification
>TABLE>
YERSINIOSIS
Clinical description
An illness of variable severity characterised by diarrhoea, fever, nausea, cramps and tenesmus.
Laboratory criteria for diagnosis
- Isolation of Yersinia enterocolitica or pseudotubeculosis from a clinical specimen.
Case classification
>TABLE>
