COMMISSION DECISION of 25 July 1995 establishing health certification for fishery products from third countries which are not yet covered by a specific decision (Text with EEA relevance) (95/328/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/493/EEC of 22 July 1991, laying down the health conditions for the production and the placing on the market of fishery products (1) and in particular Article 11 thereof,
Whereas in accordance with Article 11 of Directive 91/493/EEC the Commission has laid down the special conditions for the importation of fishery products from a significant number of third countries;
Whereas for the importations of fishery products from third countries not yet covered by this type of Decision, it is advisable initially to establish a standardized model of health certificate to avoid disruptions to trade;
Whereas the adoption of a standard health certificate would have positive effects on operators and for the control services and would facilitate the free circulation of imported fisheries products within the Community;
Whereas the model of health certificate established by this Decision is provisional and applicable for a period of two years during which individual Decisions may be adopted; whereas the provisional certificate will no longer apply to third countries for which an individual Decision has been adopted;
Whereas the veterinary checks on imported fishery products have to be carried out in accordance with Council Directive 90/675/EEC of 10 December 1990 laying down the principles governing the organization of the veterinary checks on products entering the Community from third countries (2) as last amended by the Treaty of Accession of Austria, Finland and Sweden; whereas these checks provide for the presentation of a health certificate accompanying the imported products;
Whereas the adoption of a standard model of health certificate is without prejudice to specific import conditions adopted for a third country after on the spot assessment of the health situation by Commission experts;
Whereas in view of their particular status, the fishery products defined in Article 10 subparagraph 2 of Directive 91/493/EEC need not be accompanied by the health certificate referred to above;
Whereas, in accordance with Article 10 of Directive 91/493/EEC, the health certificate should attest that the conditions of production, preparation, processing, storage, packaging and transport of fishery products intended for the Community are at least equivalent to those laid down in Directive 91/493/EEC;
Whereas the measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
1. Consignments of fishery products introduced into the territories defined in Annex I of Directive 90/675/EEC shall come from an agreed establishment inspected by the competent authority of the third country, and shall be accompanied by a numbered original health certificate attesting that the health conditions of production, handling, processing, packaging and identification of the products are at least equivalent to those established by Directive 91/493/EEC. The model of the health certificate is laid down in the Annex to this Decision.
2. However, this certificate is not required for the fishery products referred to in Article 10 (2) of Directive 91/493/EEC.
Article 2
The health certificate referred to in Article 1 (1) shall consist of a single sheet of paper and shall be drawn up in at least in one of the official languages of the country of introduction into the Community, and, if necessary, into one of the languages of the country of destination.
Article 3
The health certificate provided for by this Decision does not apply for fishery products from a third country for which the individual conditions of importation are laid down elsewhere.
Article 4
This Decision shall apply from 1 July 1995 and for a period of two years.
Article 5
This Decision is addressed to the Member States.
Done at Brussels, 25 July 1995.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ No L 268, 24. 9. 1991, p. 15.
(2) OJ No L 373, 31. 12. 1990, p. 1.
ANNEX
>START OF GRAPHIC>
HEALTH CERTIFICATE
for fishery products intended for export to the European Community
Country of dispatch: Competent authority (1): Inspection body (1): Reference number of health certificate: I. Details identifying the fishery products
Description:
- species (scientific name): - state (2) or type of processing:
Type of packaging: Number of packages: Net weight: Temperature required during storage and transport: II. Provenance of the fishery products
Address(es) and number(s) of preparation or processing establishment(s) authorized for export by the competent authority:
III. Destination of the fishery products:
The fishery products are to be dispatched
from: (place of dispatch)
to: (country and place of destination)
by the following means of transport: Name and address of the consignor: Name of consignee and address at place of destination: (1) Name and address.
(2) Live intended for direct human consumption, prepared, processed, etc.
IV. Health attestation
The undersigned official inspector hereby certifies that:
(1) the fishery products described above have been handled, prepared or processed, identified, stored and transported under conditions at least equivalent to those laid down in Council Directive 91/493/EEC of 22 July 1991 laying down the health conditions for the production and the placing on the market of fishery products;
(2) in addition, in the case of frozen or processed bivalve molluscs, the latter have been gathered in production areas subject to conditions at least equivalent to those laid down in Council Directive 91/492/EEC of 15 July 1991 laying down the health conditions for the production and the placing on the market of live bivalve molluscs.
Done at (place) on (date)
(Signature of the official inspector)
(Name in capitals, capacity and qualifications)
>END OF GRAPHIC>
