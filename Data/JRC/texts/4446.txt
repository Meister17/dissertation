Judgment of the Court of First Instance of 21 September 2005 — Yusuf and Al Barakaat International Foundation v Council and Commission
(Case T-306/01) [1]
Parties
Applicant(s): Ahmed Ali Yusuf (Spånga, Sweden) and Al Barakaat International Foundation (represented by: L. Silbersky and T. Olsson, lawyers)
Defendant(s): Council of the European Union (represented by: M. Vitsentzatos, I. Rådestad, E. Karlsson and M. Bishop, Agents,)
Intervener(s) in support of the defendant(s): United Kingdom of Great Britain and Northern Ireland (originally represented by J. Collins, and then by R. Caudwell, Agents, and by S. Moore, Barrister)
Application
originally, for annulment of, first, Council Regulation (EC) No 467/2001 of 6 March 2001 prohibiting the export of certain goods and services to Afghanistan, strengthening the flight ban and extending the freeze of funds and other financial resources in respect of the Taliban of Afghanistan, and repealing Regulation (EC) No 337/2000 (OJ 2001 L 67, p. 1) and, second, Commission Regulation (EC) No 2199/2001 of 12 November 2001 amending, for the fourth time, Regulation No 467/2001 (OJ 2001 L 295, p. 16) and, subsequently, for an application for annulment of Council Regulation (EC) No 881/2002 of 27 May 2002 imposing certain specific restrictive measures directed against certain persons and entities associated with Usama bin Laden, the Al-Qaeda network and the Taliban, and repealing Regulation No 467/2001 (OJ 2002 L 139, p. 9),
Operative part of the judgment
The Court:
1) Declares that there is no longer any need to adjudicate on the application for annulment of Council Regulation (EC) No 467/2001 of 6 March 2001 prohibiting the export of certain goods and services to Afghanistan, strengthening the flight ban and extending the freeze of funds and other financial resources in respect of the Taliban of Afghanistan, and repealing Regulation (EC) No 337/2000 and for annulment of Commission Regulation (EC) No 2199/2001 of 12 November 2001 amending, for the fourth time, Regulation No 467/2001;
2) Dismisses the action in so far as it is brought against Council Regulation (EC) No 881/2002 of 27 May 2002 imposing certain specific restrictive measures directed against certain persons and entities associated with Usama bin Laden, the Al-Qaeda network and the Taliban, and repealing Regulation No 467/2001;
3) Orders the applicants to bear, in addition to their own costs, those of the Council and those incurred by the Commission until 10 July 2002, including the costs of the interlocutory proceedings;
4) Orders the United Kingdom of Great Britain and Northern Ireland, and the Commission for the period after 10 July 2002, to bear their own costs.
[1] OJ C 44, 16.2.2002.
--------------------------------------------------
