COMMISSION DIRECTIVE 96/20/EC of 27 March 1996 adapting to technical progress Council Directive 70/157/EEC relating to the permissible sound level and the exhaust system of motor vehicles (Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 70/156/EEC of 6 February 1970, on the approximation of the laws of the Member States relating to the type-approval of motor vehicles and their trailers (1), as last amended by Commission Directive 95/54/EC (2) and in particular Article 13 (2) thereof,
Having regard to Council Directive 70/157/EEC of 6 February 1970 on the approximation of the laws of the Member States relating to the permissible sound level and the exhaust system of motor vehicles (3), as last amended by Directive 92/97/EEC (4), and in particular Article 3 thereof,
Whereas Directive 70/157/EEC is one of the separate Directives of the EC type-approval procedure which has been established by Directive 70/156/EEC; whereas, consequently, the provisions laid down in Directive 70/156/EEC relating to vehicle systems, components and separate technical units apply to this Directive;
Whereas, in particular, Articles 3 (4) and 4 (3) of Directive 70/156/EEC necessitate that each separate Directive has attached to it an information document incorporating the relevant items of Annex I to that Directive and also a type-approval certificate based on Annex VI thereto in order that type-approval may be computerized;
Whereas, the recent developments in engine technology make it necessary to define more explicitly the testing process, particularly in the case of heavy vehicles, insofar as the feasibility and the repetition of such tests is concerned;
Whereas the provisions of this Directive are in accordance with the opinion of the Committee for Adaptation to Technical Progress established by Directive 70/156/EEC,
HAS ADOPTED THIS DIRECTIVE:
Article 1
1. The Articles of Directive 70/157/EEC shall be amended as follows:
- Article 1 to read at the end: '. . . rails and of agricultural and forestry tractors and all mobile machinery.`,
- in Article 2, second indent, and Article 2a, paragraph 2, replace 'Article 9a` by 'Article 2`,
- in Article 3, replace 'the Annex` by 'the Annexes`.
2. The Annexes to Directive 70/157/EEC shall be amended in accordance with the Annex to this Directive.
Article 2
1. With effect from 1 October 1996 Member States may not, on grounds relating to the permissible sound level or the exhaust system:
- refuse, in respect of a type of vehicle or a type of exhaust system, to grant EC type-approval or national type-approval, or
- prohibit the registration, sale or entry into service of vehicles, or the sale or entry into service of exhaust systems,
if the vehicles or exhaust systems comply with the requirements of Directive 70/157/EEC, as amended by this Directive.
2. With effect from 1 January 1997 Member States:
- shall no longer grant EC type-approval, and
- shall refuse to grant national type-approval,
for a type of vehicle on grounds relating to its permissible sound level and for a type of exhaust system, if the requirements of Directive 70/157/EEC, as amended by this Directive, are not fulfilled.
3. Notwithstanding paragraph 2, for the purposes of replacement parts, Member States shall continue to grant EC type-approval, and to permit the sale and entry into service, of exhaust systems in accordance with previous versions of Directive 70/157/EEC provided that such exhaust systems:
- are intended to be fitted to vehicles already in use, and
- comply with the requirements of that Directive which were applicable when the vehicles were first registered.
Article 3
1. Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive before 1 October 1996. They shall forthwith inform the Commission thereof.
2. When the Member States adopt these provisions, they shall contain a reference to this Directive or be accompanied by such reference on the occasion of their official publication. The methods of making such a reference shall be laid down by the Member States.
3. Member States shall communicate to the Commission the texts of the main provisions of national law which they adopt in the field governed by this Directive.
Article 4
This Directive shall enter into force the twentieth day after its publication in the Official Journal of the European Communities.
Article 5
This Directive is addressed to the Member States.
Done at Brussels, 27 March 1996.
For the Commission
Martin BANGEMANN
Member of the Commission
(1) OJ No L 42, 23. 2. 1970, p. 1.
(2) OJ No L 266, 8. 11. 1995, p. 1.
(3) OJ No L 42, 23. 2. 1970, p. 16.
(4) OJ No L 371, 19. 12. 1992, p. 1.
ANNEX
A list of Annexes shall be inserted between the Articles and Annex I to read:
'List of Annexes
ANNEX I: EC type-approval in respect of the sound level of a type of motor vehicle
Appendix 1: Information document
Appendix 2: Type-approval certificate
ANNEX II: EC type-approval of exhaust systems as separate technical units
Appendix 1: Information document
Appendix 2: Type-approval certificate
Appendix 3: Model for the EC type-approval mark
ANNEX III: Checks on conformity of production
ANNEX IV: Test track specifications`.
Amendments to Annex I:
The footnote to item 1.1.7. shall be amended to read:
'(1) In accordance with the definitions given in Annex II A to Directive 70/156/EEC.`
Item 2.1. shall be amended to read:
'2.1. The application for EC type-approval pursuant to Article 3 (4) of Directive 70/156/EEC of a vehicle type with regard to its sound level shall be submitted by the vehicle manufacturer.`
Item 2.2. shall be amended to read:
'2.2. A model for the information document is given in Appendix 1.`
Items 2.2.1. to 2.2.4. inclusive shall be deleted.
In item 2.3. the words 'or his authorized representative` shall be deleted.
Item 2.5. shall be deleted.
Item 4. shall be amended to read:
'4. Granting of EC type-approval
4.1. If the relevant requirements are satisfied, EC type-approval pursuant to Article 4 (3) and, if applicable, 4 (4) of Directive 70/156/EEC shall be granted.
4.2. A model for the EC type-approval certificate is given in Appendix 2.
4.3. An approval number in accordance with Annex VII to Directive 70/156/EEC shall be assigned to each vehicle type approved. The same Member State shall not assign the same number to another vehicle type.`
In item 5.2.1.2. 'Annex III` shall read 'Appendix 2`.
In item 5.2.2.3.1. 'Annex VI` shall read 'Annex IV`.
In item 5.2.2.3.4. the text of the second paragraph shall read:
'The tyres used for the test are selected by the vehicle manufacturer and shall comply with commercial practice and be available on the market; they shall correspond to one of the tyre-size designations (see item 2.17. of Annex II to Council Directive 92/23/EEC (*)) indicated for the vehicle by the vehicle manufacturer according to item 1.5. of the Addendum to Appendix 2 and in the case of vehicles of categories M1 and N1 meet the requirements of Directive 89/459/EEC regarding the minimum tread depth; for vehicles of other categories, the minimum tread depth specified in Directive 89/459/EEC will be applied as if the vehicles were within the scope of that Directive. The tyres must be inflated to the appropriate pressure(s) for the test mass of the vehicle
(*) OJ No L 129, 14. 5. 1992, p. 95.`
In item 5.2.2.4.3.3.1.1. at the end of the third paragraph, it shall be added:
'If the engine speed "S" is still attained with an approach engine speed corresponding to the idle speed, then the test will be performed only in third gear and the relevant results have to be evaluated.`
In item 5.2.2.4.3.3.1.2. it shall be added at the end:
'However, the vehicle is deemed representative of its type also, if, on the applicant's request, the tests are extended over more ratios than foreseen, and the highest sound level is obtained between the extreme ratios tested.`
In items 5.2.3.1. and 5.2.3.5.1. 'Annex III` shall read 'Appendix 2`.
In items 5.3.2. 'Article 8 (3)` shall read 'Article 11 (2) or 11 (3)`.
Item 6. shall read:
'6. Modifications of the type and amendments of approval
6.1. In the case of modifications of the type approved pursuant to this Directive, the provisions of Article 5 of Directive 70/156/EEC shall apply.`
The sub-items of item 7. shall read:
'7.1. Measures to ensure the conformity of production shall be taken in accordance with the requirements laid down in Article 10 of Directive 70/156/EEC.
7.2. Special provisions:
7.2.1. The tests referred to in item 2.3.5. of Annex X to Directive 70/156/EEC are those laid down in Annex III (I) to this Directive.
7.2.2. The frequency of inspections referred to in item 2.4. of Annex X to Directive 70/156/EEC is normally one every two years.`
After figure 4 the following Appendices 1 and 2 shall be added:
'Appendix 1
>START OF GRAPHIC>
Information document No . . . pursuant to Annex I of Council Directive 70/156/EEC (*) relating to EC type-approval of a vehicle with respect to the permissible sound level and the exhaust system (Directive 70/157/EEC, as last amended by Directive . . . /. . . /EC)
The following information, if applicable, must be supplied in triplicate and include a list of contents. Any drawings must be supplied in appropriate scale and in sufficient detail on size A4 or on a folder of A4 format. Photographs, if any, must show sufficient detail.
If the systems, components or separate technical units have electronic controls, information concerning their performance must be supplied.
0. General
0.1. Make (trade name of manufacturer):
0.2. Type and general commercial description(s):
0.3. Means of identification of type, if marked on the vehicle (b):
0.3.1. Location of that marking:
0.4. Category of vehicle (c):
0.5. Name and address of manufacturer:
0.8. Address(es) of assembly plant(s):
(*) The item numbers and footnotes used in this information document correspond to those set out in Annex I to Directive 70/156/EEC. Items not relevant for the purpose of this Directive are omitted.
1. General construction characteristics of the vehicle
1.1. Photographs and/or drawings of a representative vehicle:
1.3.3. Powered axles (number, position, interconnection):
1.6. Position and arrangement of the engine:
2. Masses and dimensions (e) (in kg and mm) (Refer to drawing where applicable)
2.4. Range of vehicle dimensions (overall)
2.4.1. For chassis without bodywork
2.4.1.1. Length (j):
2.4.1.2. Width (k):
2.4.2. For chassis with bodywork
2.4.2.1. Length (j):
2.4.2.2. Width (k):
2.6. Mass of the vehicle with bodywork in running order, or mass of the chassis with cab if the manufacturer does not fit the bodywork (with standard equipment, including coolant, oils, fuel, tools, spare wheel and driver) (o) (maximum and minimum):
3. Power plant (q)
3.1. Manufacturer:
3.1.1. Manufacturer's engine code: (As marked on the engine, or other means of identification)
3.2. Internal combustion engine
3.2.1.1. Working principle: positive ignition /compression ignition, four stroke/two stroke (1)
3.2.1.2. Number and arrangement of cylinders:
3.2.1.2.3. Firing order:
3.2.1.3. Engine capacity (s): .......... cm3
3.2.1.8. Maximum net power (t): .......... kW at .......... min-1 (manufacturer's declared value)
3.2.4. Fuel feed
3.2.4.1. By carburettor(s): yes/no (1)
3.2.4.1.2. Type(s):
3.2.4.1.3. Number fitted:
3.2.4.2. By fuel injection (compression ignition only): yes/no (1)
3.2.4.2.2. Working principle: Direct injection/pre-chamber/swirl chamber (1)
3.2.4.2.4. Governor
3.2.4.2.4.1. Type:
3.2.4.2.4.2.1. Cut-off point under load: .......... min -1
3.2.4.3. By fuel injection (positive ignition only): yes/no (1)
3.2.4.3.1. Working principle: Intake manifold (single-/multi-point (1))/direct injection/other (specify) (1)
(1) Delete where not applicable.
3.2.8. Intake system
3.2.8.4.2. Air filter, drawings: ..........; or
3.2.8.4.2.1. Make(s):
3.2.8.4.2.2. Type(s):
3.2.8.4.3. Intake silencer, drawings: .........., or
3.2.8.4.3.1. Make(s):
3.2.8.4.3.2. Type(s):
3.2.9. Exhaust system
3.2.9.2. Description and/or drawing of the exhaust system:
3.2.9.4. Exhaust silencer(s):
For front, centre, rear silencer: construction, type, marking; where relevant for exterior noise: reducing measures in the engine compartment and on the engine:
3.2.9.5. Location of the exhaust outlet:
3.2.9.6. Exhaust silencer containing fibrous materials:
3.2.12.2.1. Catalytic convertor: yes/no (1)
3.2.12.2.1.1. Number of catalytic convertors and elements:
3.3. Electric motor
3.3.1. Type (winding, excitation):
3.3.1.1. Maximum hourly output: .......... kW
3.3.1.2. Operating voltage:.......... V
3.4. Other engines or motors or combinations thereof (particulars regarding the parts of such engines or motors):
4. Transmission (v)
4.2. Type (mechanical, hydraulic, electric, etc.):
4.6. Gear ratios
Gear / Internal gearbox ratios (ratios of engine to gearbox output shaft revolutions) / Final drive ratio(s) (ratio of gearbox output shaft to driven wheel revolutions / Total gear ratios
Maximum for CVT (*)
1
2
3
. . .
Minimum for CVT (*)
Reverse
(*) Continuously variable transmission
(1) Delete where not applicable.
4.7. Maximum vehicle speed (and gear in which this is achieved) (in km/h) (w):
6. Suspension
6.6. Tyres and wheels
6.6.2. Upper and lower limits of rolling radii
6.6.2.1. Axle 1:
6.6.2.2. Axle 2:
6.6.2.3. Axle 3:
6.6.2.4. Axle 4:
etc.
9. Bodywork (not applicable for vehicles of category M1)
9.1. Tpe of bodywork:
9.2. Materials used and method of constriction
12. Miscellaneous
12.5. Details of any non-engine devices designed to reduce noise (if not covered by other items):
Additional information in the case of off-road vehicles
1.3. Number of axles and wheels:
2.4.1. For chassis without bodywork
2.4.1.4.1. Approach angle (na): .......... degrees
2.4.1.5.1. Departure angle (nb): .......... degrees
2.4.1.6. Ground clearance (as defined in point 4.5 of section A of Annex II to Directive 70/156/EEC)
2.4.1.6.1. Between the axles:
2.4.1.6.2. Under the front axle(s):
2.4.1.6.3. Under the rear axle(s):
2.4.1.7. Ramp angle (nc): .......... degrees
2.4.2. For chassis with bodywork
2.4.2.4.1. Approach angle (na): .......... degrees
2.4.2.5.1. Departure angle (nb): .......... degrees
2.4.2.6. Ground clearance (as defined in point 4.5 of section A of Annex II to Directive 70/156/EEC)
2.4.2.6.1. Between the axles:
2.4.2.6.2. Under the front axle(s):
2.4.2.6.3. Under the rear axle(s):
2.4.2.7. Ramp angle (nc): .......... degrees
2.15. Hill-starting ability (solo vehicle): .......... percent
4.9. Differential lock: yes/no/optional (1)
Date, File
(1) Delete where not applicable.
>END OF GRAPHIC>
Appendix 2
>START OF GRAPHIC>
MODEL
EC TYPE-APPROVAL CERTIFICATE
(Maximum Format: A4 (210 × 297 mm))
Stamp of administration
Communication concerning the
- type-approval (1)
- extension of type-approval (1)
- refusal of type-approval (1)
- withdrawal of type-approval (1)
of a type of a vehicle/component/separate technical unit (1) with regard to Directive . . . /. . . /EEC, as last amended by Directive . . . /. . . /EC.
Type-approval number:
Reason for extension:
SECTION I
0.1. Make (trade name of manufacturer):
0.2. Type and general commercial description(s):
0.3. Means of identification of type if marked on the vehicle/component/separate technical unit (1) (2)
0.3.1. Location of that marking:
0.4. Category of vehicle (3):
0.5. Name and address of manufacturer:
0.7. In the case of components and separate technical units, location and method of affixing of the EC approval mark:
0.8. Address(es) of assembly plant(s)
SECTION II
1. Additional information (where applicable): See Addendum
2. Technical service responsible for carrying out the tests:
3. Date of test report:
4. Number of test report:
5. Remarks (if any): See Addendum
6. Place:
7. Date:
8. Signature:
9. The index to the information package lodged with the approval authority, which may be obtained on request, is attached.
Addendum to EC type-approval certificate No . . .
concerning the type-approval of a vehicle with regard to Directive 70/157/EEC as last amended by Directive . . .
1. Additional information:
1.1. If necessary, list of vehicles covered by item 5.2.2.4.3.3.1.2. of Annex I:
1.2. Engine
1.2.1. Manufacturer:
1.2.2. Type:
1.2.3. Model:
1.2.4. Rated maximum power .................... kW at .................... min - 1
(1) Delete where not applicable.
(2) If the means of identification of type contains characters not relevant to describe the vehicle, component or separate technical unit types covered by this type-approval certificate such characters shall be represented in the documentation by the symbol:"?" (e.g. ABC??123??).
(3) As defined in Annex II A to Directive 70/156/EEC.
1.3. Transmission: non-automatic gearbox/automatic gearbox (1)
1.3.1. Number of gears:
1.4. Equipment
1.4.1. Exhaust silencer
1.4.1.1. Manufacturer:
1.4.1.2. Model:
1.4.1.3. Type:.................... in accordance with drawing No: ....................
1.4.2. Intake silencer
1.4.2.1. Manufacturer:
1.4.2.2. Model:
1.4.2.3. Type: .................... in accordance with drawing No: ....................
1.5. Tyre size:
1.5.1. Description of tyre type used for type-approval testing:
1.6. Measurements
1.6.1. Sound level of moving vehicle:
Measurement results
Left-hand side dB (A) (2) / Right-hand side dB (A) (2) / Position of gear lever
first measurement
second measurement
third measurement
fourth measurement
Test result: dB (A)/E (3)
1.6.2. Sound level of stationary vehicle:
dB (A) / Engine speed
first measurement
second measurement
third measurement
Test result dB (A)/E (3)
1.6.3. Sound level of compressed air noise:
Measurement results
Left-hand side dB (A) (2) / Right-hand side dB (A) (2)
first measurement
second measurement
third measurement
fourth measurement
Test result dB (A)
5. Remarks:`
(1) Delete where inapplicable.
(2) The measurement values are given with the 1 dB (A) deduction in accordance with provisions of item 5.2.2.5.1. of Annex I.
(3) 'E` indicates that the measurements in question were conducted in accordance with this Directive.
>END OF GRAPHIC>
Amendments to Annex II
In item 0. 'Article 9a` shall be replaced by 'Article 2`.
Item 2.1. shall be amended to read:
'2.1. The application of EC type-approval pursuant to Article 3 (4) of Directive 70/156/EEC in respect of a replacement exhaust system or component thereof as a separate technical unit shall be submitted by the vehicle manufacturer or the manufacturer of the separate technical unit in question.`
Item 2.2. shall be amended to read:
'2.2. A model for the information document is given in Appendix 1.`
Item 2.2.1. to 2.2.3. inclusive, 2.4. and 3.1.3. shall be deleted.
Footnote (1) to items 2.3.3. and 5.2.1. shall be amended to read:
'(1) As prescribed in the version of this Directive which was applicable to the type-approval of the vehicle.`Items 3., 3.1., 3.1.1., 3.1.2. and 3.2. shall be renumbered 2.4., 2.4.1., 2.4.1.1., 2.4.1.2. and 2.4.2., respectively.
Item 4. shall be renumbered 3. and shall be amended to read:
'3. Granting of EC type-approval
3.1. If the relevant requirements are satisfied, EC type-approval pursuant to Article 4 (3) and, if applicable, Article 4 (4) of Directive 70/156/EEC shall be granted.
3.2. A model for the EC type-approval certificate is given in Appendix 2.
3.3. A type-approval number in accordance with Annex VII to Directive 70/156/EEC shall be assigned to each type of replacement exhaust system or component thereof approved as a separate technical unit; section 3 of the type-approval number shall indicate the number of the amending Directive which was applicable at the time of the vehicle type-approval. The same Member State shall not assign the same number to another type of replacement exhaust system or component thereof.`
A new item 4. shall be added to read:
'4. EC type-approval mark
4.1. Every replacement exhaust system or component thereof, excluding fixing hardware and pipes, conforming to a type approved under this Directive shall bear an EC type-approval mark.
4.2. The EC type-approval mark shall consist of a rectangle surrounding the lower case letter "e" followed by the distinguishing letter(s) or number of the Member State which has granted the approval:
"1" for Germany
"2" for France
"3" for Italy
"4" for the Netherlands
"5" for Sweden
"6" for Belgium
"9" for Spain
"11" for the United Kingdom
"12" for Austria
"13" for Luxembourg
"17" for Finland
"18" for Denmark
"21" for Portugal
"23" for Greece
"IRL" for Ireland
It must also include in the vicinity of the rectangle the "base approval number" contained in section 4 of the type-approval number referred to in Annex VII to Directive 70/156/EEC, preceded by the two figures indicating the sequence number assigned to the most recent major technical amendment to Council Directive 70/157/EEC which was applicable at the time of the vehicle type-approval. For Directive 70/157/EEC, the sequence number is 00; for Directive 77/212/EEC the sequence number is 01; for Directive 84/424/EEC the sequence number is 02; for Directive 92/97/EEC the sequence number is 03.
4.3. The mark must be clearly legible and indelible even when the replacement exhaust system or component thereof is fitted to the vehicle.
4.4. An example of the EC type-approval mark is shown in Appendix 3.`
Item 6. shall be replaced by the following new items 6. and 7.:
'6. Modification of the type and amendments to approvals
6.1. In the case of modifications of the type approved pursuant to this Directive, the provisions of Article 5 of Directive 70/156/EEC shall apply.
7. Conformity of production
7.1. Measures to ensure the conformity of production shall be taken in accordance with the requirements laid down in Article 10 to Directive 70/156/EEC.
7.2. Special provisions:
7.2.1. The tests referred to item 2.3.5. of Annex X to Directive 70/156/EEC are those prescribed in Annex III (II) to this Directive.
7.2.2. The frequency of inspections referred to in item 2.4. of Annex X to Directive 70/156/EEC is normally one every two years.`
After Figure 3 the following Appendices 1, 2 and 3 shall be added:
'Appendix 1
>START OF GRAPHIC>
Information Document No . . . relating to EC type-approval as separate technical unit of exhaust systems for motor vehicles (Directive 70/157/EEC, as last amended by Directive . . . /. . . /EC)
The following information, if applicable, must be supplied in triplicate and include a list of contents. Any drawings must be supplied in appropriate scale and in sufficient detail on size A4 or on a folder of A4 format. Photographs, if any, must show sufficient detail.
If the systems, components or separate technical units have electronic controls, information concerning their performance must be supplied.
0. General
0.1. Make (trade name of manufacturer):
0.2. Type and general commercial description(s):
0.5. Name and address of manufacturer:
0.7. In the case of components and separate technical units, location and method of affixing of the EC approval mark:
0.8. Address(es) of assembly plant(s):
1. Description of the vehicle for which the device is intended (if the device is intended to be fitted to more than one vehicle type the information requested under this point shall be supplied for each type concerned)
1.1. Make (trade name of manufacturer):
1.2. Type and general commercial description(s):
1.3. Means of identification of type, if marked on the vehicle:
1.4. Category of vehicle:
1.5. EC type-approval number with regard to sound level:
1.6. All the information mentioned in items 1.1. to 1.5. of the type-approval certificate concerning the vehicle (Annex I, Appendix 2 to this Directive):
2. Description of the device
2.1. A description of the replacement exhaust system indicating the relative position of each system component, together with mounting instructions:
2.2. Detailed drawings of each component, so that they can be easily located and identified, and reference to the materials used. These drawings must indicate the place provided for the compulsory affixing of the EC type-approval mark:
Date, File
>END OF GRAPHIC>
Appendix 2
>START OF GRAPHIC>
MODEL
EC TYPE-APPROVAL CERTIFICATE
(Maximum Format: A4 (210 × 297 mm))
Stamp of administration
Communication concerning the
- type-approval (1)
- extension of type-approval (1)
- refusal of type-approval (1)
- withdrawal of type-approval (1)
of a type of a vehicle/component/separate technical unit (1) with regard to Directive . . ./. . ./EEC, as last amended by Directive . . ./. . ./EC.
Type-approval number:
Reason for extension:
SECTION I
0.1. Make (trade name of manufacturer):
0.2. Type and general commercial description(s):
0.3. Means of identification of type if marked on the vehicle/component/separate technical unit (1) (2):
0.3.1. Location of that marking:
0.4. Category of vehicle (3)
0.5. Name and address of manufacturer:
0.7. In the case of components and separate technical units, location and method of affixing of the EC approval mark:
0.8. Address(es) of assembly plant(s):
SECTION II
1. Additional information (where applicable): See Addendum
2. Technical service responsible for carrying out the tests:
3. Date of test report:
4. Number of test report:
5. Remarks (if any): See Addendum
6. Place:
7. Date:
8. Signature:
9. The index to the information package lodged with the approval authority, which may be obtained on request, is attached.
Addendum to EC type-approval certificate No . . .
concerning the separate technical unit type-approval of exhaust systems for motor vehicles with regard to Directive 70/157/EEC as last amended by Directive . . .
1. Additional information
1.1. Composition of the separate technical unit:
(1) Delete where not applicable.
(2) If the means of identification of type contains characters not relevant to describe the vehicle, component or separate technical unit types covered by the type-approval certificate such characters shall be represented in the documentation by the symbol: "?" (e.g. ABC??123??).
(3) As defined in Annex II A to Directive 70/156/EEC.
1.2. Trademark or trade name of the type(s) of motor vehicle to which the silencer is to be fitted (1):
1.3. Type(s) of vehicle and its/their type-approval number(s):
1.4. Engine
1.4.1. Type (positive ignition, diesel):
1.4.2. Cycles: two-stroke, four-stroke:
1.4.3. Total cylinder capacity:
1.4.4. Rated maximum engine power .................... kW at .................... min-1
1.5. Number of gear ratios:
1.6. Gear ratios employed:
1.7. Drive-axle ratios(s):
1.8. Sound-level values:
- moving vehicle: .................... dB (A), speed stabilized before acceleration at .................... km/h;
- stationary vehicle: .................... dB (A), at .................... min-1
1.9. Variation in back pressure:
1.10. Any restrictions in respect of use and mounting requirements:
>END OF GRAPHIC>
5. Remarks:
Appendix 3
MODEL FOR THE EC TYPE-APPROVAL MARK
>REFERENCE TO A FILM>
The exhaust system or component thereof bearing the above EC type-approval mark is a device which has been approved in Spain (e 9) pursuant to Directive 92/97/EEC (03) under the base approval number 0148.
The figures used are only indicative.
(1) If several types are indicated, items 1.3. to 1.10. inclusive must be completed in respect of each type.`
Amendments to Annexes III, IV, V and VI:
Annexes III and IV shall be deleted.
Annex V shall be renumbered Annex III.
In Annex III (1), item 2 shall read:
'2. Testing procedures
The methods of testing, conditions and measurements, measuring instruments and interpretation of results shall be those described in Annex I. The vehicle(s) under test shall be subject to the test for measurement of noise of vehicle in motion as described in item 5.2.2. of Annex I.`
Annex VI shall be renumbered Annex IV.
