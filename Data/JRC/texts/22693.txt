*****
COUNCIL DIRECTIVE
of 29 June 1982
amending Directives 69/169/EEC and 77/800/EEC as regards the rules governing turnover tax and excise duty applicable in international travel
(82/443/EEC)
THE COUNCIL OF THE EUROPEAN
COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 99 thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas intra-Community tax-free allowances contribute to the interpenetration of Member States' economies;
Whereas, in order to achieve this objective and in the interests of the population of the Member States, the value of the exemptions laid down by Article 2 of Council Directive 69/169/EEC of 28 May 1969 on the harmonization of provisions laid down by law, regulation or administrative action relating to exemption from turnover tax and excise duty on imports in international travel (4), as last amended by Directive 81/933/EEC (5), should be increased;
Whereas for expressions of alcoholic strength it is necessary to take account of Council Directive 76/766/EEC of 27 July 1976 on the approximation of the laws of the Member States relating to alcohol tables (6), and accordingly to amend Articles 4 and 5 of Directive 69/169/EEC and Article 1 of Council Directive 77/800/EEC of 19 December 1977 on a derogation accorded to the Kingdom of Denmark relating to the rules governing turnover tax and excise duty applicable in international travel (7);
Whereas on the grounds of the present economic situation a further period of time should be granted to the Kingdom of Denmark in which to apply the increase in value of the allowance for persons travelling from other Member States of the Community,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Articles 1 and 2 of Directive 69/169/EEC shall be amended as follows:
1. In Article 1 (3), '40 European units of account' shall be replaced by 'the amount set out in paragraph 1'.
2. In Article 2 (1), '180 European units of account' shall be replaced by '210 ECU'.
3. In Article 2 (2), '50 European units of account' shall be replaced by '60 ECU'.
4. In Article 2 (3), '180 European units of account' shall be replaced by 'the amount set out in paragraph 1'.
Article 2
In Articles 4 and 5 of Directive 69/169/EEC and Article 1 of Directive 77/800/EEC, '22°' shall be replaced by '22 % vol'.
Article 3
1. Member States shall bring into force the measures necessary to comply with this Directive with effect from 1 January 1983.
However, the Kingdom of Denmark shall put into force the necessary measures to comply with Article 1 (2) not later than 1 January 1984.
2. Member States shall inform the Commission of the provisions which they adopt to implement this Directive.
Article 4
This Directive is addressed to the Member States.
Done at Brussels, 29 June 1982.
For the Council
The President
P. de KEERSMAEKER
(1) OJ No C 318, 19. 12. 1979, p. 5.
(2) OJ No C 117, 12. 5. 1980, p. 83.
(3) OJ No C 113, 7. 5. 1980, p. 34.
(4) OJ No L 133, 4. 6. 1969, p. 6.
(5) OJ No L 338, 25. 11. 1981, p. 24.
(6) OJ No L 262, 27. 9. 1976, p. 149.
(7) OJ No L 336, 27. 12. 1977, p. 21.
