COUNCIL REGULATION (EC) No 448/98 of 16 February 1998 completing and amending Regulation (EC) No 2223/96 with respect to the allocation of financial intermediation services indirectly measured (FISIM) within the European system of national and regional accounts (ESA)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 213 thereof,
Having regard to the draft Regulation submitted by the Commission (1),
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the European Monetary Institute (3),
Whereas Council Regulation (EC) No 2223/96 of 25 June 1996 on the European System of National and Regional Accounts in the Community (4) contains the reference framework of common standards, definitions, classifications and accounting rules for drawing up the accounts of the Member States for the statistical requirements of the European Community, in order to obtain comparable results between Member States;
Whereas Article 2(3) of Regulation (EC) No 2223/96 provides that a decision on the allocation of financial intermediation services indirectly measured (FISIM) will be taken no later than 31 December 1997;
Whereas solving the problem of allocating FISIM should lead to major improvements in the methodology of the ESA and to a more accurate intra-European Union comparison of gross domestic product (GDP) levels;
Whereas the purpose of this Regulation is to introduce the principle of allocation of FISIM and the detailed rules of its implementation;
Whereas the effectiveness of the allocation of FISIM and the detailed rules of its implementation need to be evaluated by means of calculations to be carried out by the Member States according to the trial methods described in Annex III to this Regulation during a trial period which is sufficiently long in order to test whether this allocation yields more reliable results for the correct measurement of the economic activity concerned than the present zero allocation;
Whereas, it is appropriate that the Commission on the basis of the calculations carried out during the trial period, presents evaluation reports on the quality of the data, in particular, on their availability and a qualitative and quantitative analysis of the stability in time and the sensitivity of the results in respect of the different trial methods;
Whereas it is appropriate in case of a positive evaluation of reliability of the results obtained that the Commission decides on the most suitable method for the allocation of FISIM;
Whereas in case the trial methods do not yield more reliable results for the correct measurement of the economic activity concerned than the present zero allocation it is however appropriate that the Commission submits to the Council an appropriate proposal for modification of Regulation (EC) No 2223/96;
Whereas it is appropriate that the decision to allocate FISIM for the establishment of the GNP used for the purposes of the Community's budget and its own resources will be adopted by the Council, acting unanimously, on a proposal from the Commission;
Whereas it is appropriate not to allocate FISIM for the purposes of other Community policies until the Commission has decided on the method to be used for allocating FISIM in case the results obtained are judged to be more reliable;
Whereas, in accordance with the principle of subsidiarity, the objectives pursued by this Regulation can be better achieved at the Community level than at the level of Member States because only the Commission can coordinate the necessary harmonisation of the statistical methods for calculating and allocating FISIM at Community level; whereas, however, the calculation and allocation proper as well as the infrastructure required to monitor application of the methods, should be organised by the Member States; whereas for this reason it is necessary to prescribe that the competent national authorities have access to all available data at national level;
Whereas the Committee on the Statistical Programmes of the European Communities, established by Decision 89/382/EEC, Euratom (5), and the Committee on Monetary, Financial and Balance of Payments Statistics, established by Decision 91/115/EEC (6), have been respectively consulted in accordance with Article 3 of each of the aforesaid Decisions,
HAS ADOPTED THIS REGULATION:
Article 1
Purpose
1. The purpose of this Regulation is to introduce the principle, using reliable methodology, for allocating financial intermediation services indirectly measured (FISIM), described in Annex I of Annex A to Regulation (EC) No 2223/96.
2. For this purpose, Annexes I and II of Annex A to Regulation (EC) No 2223/96 shall be amended in accordance with Annexes I and II to this Regulation.
Article 2
Methods
1. The Member States shall carry out calculations in accordance with the methods described in Annex III to this Regulation during the trial period described in Article 4.
2. On the basis of an evaluation of the results of these calculations a decision on the method to be used for the allocation of FISIM shall be taken in accordance with Article 5.
Article 3
Means
1. The Member States shall ensure that the necessary data or appropriate estimates for carrying out these calculations shall forthwith be put at the disposal of the national authority in charge of conducting the calculations referred to in Article 2(1).
2. The national authority shall be responsible for the collection of supplementary data it deems necessary for the calculations.
Article 4
Submission of the results of calculations during the trial period
The Member States shall submit to the Commission the results of the calculations referred to in Article 2(1), in accordance with the following schedule:
The results for calendar years 1995, 1996, 1997 and 1998 shall be submitted no later than 1 November 1999.
The results for calendar year 1999 as well as revised results for calendar years 1995, 1996, 1997 and 1998 shall be submitted no later than 1 November 2000.
The results for calendar year 2000 as well as revised results for calendar years 1995, 1996, 1997, 1998 and 1999 shall be submitted no later than 1 November 2001.
The first estimates for calendar year 2001 as well as revised results for calendar years 1995, 1996, 1997, 1998, 1999 and 2000 shall be submitted no later than 30 April 2002.
Article 5
Evaluation of results
1. On the basis of the results referred to in Article 4, the Commission, after consulting the Statistical Programme Committee, shall submit before 31 December 2000 a mid-term report and before 1 July 2002 a final report to the European Parliament and the Council containing a qualitative and quantitative analysis of the implications of the trial methods for allocating and calculating FISIM as described in Annex III.
2. The necessary measures for the application of this Regulation, including measures to clarify and improve the trial methods described in Annex III, shall be adopted by the Commission in accordance with the procedure of Article 7.
3. Before 31 December 2002, the Commission shall, after consulting the Committee for Monetary, Financial and Balance of Payments Statistics and in accordance with the procedure of Article 7, adopt the method to be used for the allocation of FISIM in case the findings of the final evaluation report on the reliability of the results obtained during the trial period are positive.
4. In case the Commission in its final evaluation report referred to in paragraph 1 finds that none of the trial methods for allocating FISIM are more reliable for the correct measurement of the economic activity than the present zero allocation, the Commission shall submit, if necessary, an appropriate proposal to the Council for the modification of Regulation (EC) No 2223/96.
Article 6
Transmission to the Commission
From 1 January 2003 the Member States shall transmit to the Commission the results of the calculations made pursuant to this Regulation as part of the tables referred to in Article 3 of Regulation (EC) No 2223/96.
Article 7
Procedure
1. The Commission shall be assisted by the Statistical Programme Committee, hereinafter referred to as 'the Committee`.
2. The representative of the Commission shall submit to the Committee a draft of the measures to be taken. The Committee shall deliver its opinion on the draft within a time limit which the Chairman may lay down according to the urgency of the matter.
The opinion shall be delivered by the majority laid down in Article 148(2) of the Treaty establishing the European Community in the case of decisions which the Council is required to adopt on a proposal from the Commission. The votes of the representatives of the Member States within the Committee shall be weighted in the manner set out in that Article. The Chairman shall not vote.
3. (a) The Commission shall adopt the measures envisaged if they are in accordance with the opinion of the Committee.
(b) If the measures envisaged are not in accordance with the opinion of the Committee, or if no opinion is delivered, the Commission shall, without delay, submit to the Council a proposal relating to the measures to be taken. The Council shall act by a qualified majority.
If, upon expiry of a period of three months the Council has not acted, the proposed measures shall be adopted by the Commission.
Article 8
Derogations
By way of derogation from this Regulation:
1. The decision to allocate FISIM for the establishment of the GNP used for the purposes of the Community's budget and its own resources shall be adopted by the Council, acting unanimously, on a proposal from the Commission.
2. FISIM shall not be allocated for the purposes of other Community policies until the Commission has adopted the method to be used for the allocation of FISIM in accordance with Article 5(3).
Article 9
Final provisions
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 16 February 1998.
For the Council
The President
G. BROWN
(1) OJ C 124, 21. 4. 1997, p. 28.
(2) OJ C 339, 10. 11. 1997.
(3) Opinion delivered on 16 October 1997 (not yet published in the Official Journal).
(4) OJ L 310, 30. 11. 1996, p. 1.
(5) OJ L 181, 28. 6. 1989, p. 47.
(6) OJ L 59, 6. 3. 1991, p. 19. Decision as amended by Decision 96/174/EC (OJ L 51, 1. 3. 1996, p. 48).
ANNEX I
AMENDMENTS TO ANNEX A AND ANNEX I THERETO OF REGULATION (EC) No 2223/96
>TABLE>
>TABLE>
>TABLE>
ANNEX II
AMENDMENT TO ANNEX II OF ANNEX A TO REGULATION (EC) No 2223/96
The following text is deleted from the third sentence of the first paragraph of point 11:
': property income receivable less interest payable, excluding any property income receivable from the investment of their own funds`.
ANNEX III
CALCULATING FISIM
1. CALCULATION OF FISIM OUTPUT BY SECTOR S122 AND S123
(a) Statistical data required
For each of the sub-sectors S122 and S123 (1), it is necessary to use the table of average stocks of loans, deposits (split by user sectors) and the securities other than shares issued by FI for the period (average of four quarters) and the accrued interest, after reallocation of interest rate subsidies to their actual recipients as defined by the 1995 ESA.
(b) The choice of a reference rate
In the balance sheets of financial intermediaries included in S122 and S123 loans and deposits with resident units have to be broken down to differentiate between loans and deposits:
- which are interbank (i.e. within the institutional units included in sectors S122 and S123);
- which are undertaken with the user institutional sectors (S11 - S124 - S125 - S13 - S14 - S15) (except with the central banks).
In addition to that, loans and deposits with the rest of the world (S2) should also be broken down into loans and deposits with non-resident financial intermediaries and loans and deposits within other non-residents.
In the trial period of five years Member States are required to compare the results on the allocation of FISIM by using the internal reference rate calculated according to the following four methods:
Method 1
To obtain the FISIM output of the resident FIs by institutional sector, the 'internal` reference rate is calculated as the ratio of interest receivable on loans between S122 and S123 to stocks of loans between S122 and S123.
>NUM>interest receivable on loans between S122 and S123
>DEN>stock of loans between S122 and S123
Method 2
To obtain the FISIM output of the resident FIs by institutional sector, the 'internal` reference rate is calculated as the weighted average of the rates on both interbank loans and on securities other than shares issued by FIs. The weights are the level of stocks in the headings loans between resident FIs included in S122 and S123 and securities other than shares issued by the resident financial intermediairies included in S122 and S123.
>NUM>interest receivable on loans between S122 and S123 + interest on securities other than shares issued by S122 and S123
>DEN>stock of loans between S122 and S123 + securities other than shares issued by S122 and S123
If the institutional characteristics of the national banking systems do not allow to calculate this rate (e.g. because banks do not issue securities other than shares), then an alternative reference rate should be used. This rate can be calculated by using the stocks and interest flows of assets (excluding loans)/liabilities (excluding deposits) whose average time to maturity is the nearest to that of the liabilities in the balance sheets of FIs included in S122 and S123.
Method 3
To obtain the FISIM output of resident FIs by institutional sector, two reference rates can be applied, one for short term transactions (calculated as in Method 1) and one for long term transactions (using published rates for securities other than shares whose maturity reproduces that of the liabilities in the balance sheet with a long maturity).
Method 4
To obtain the FISIM output of the resident FIs by institutional sector, the 'internal` reference rate is calculated according to the three alternatives below:
(a) as an average of lending and deposits rates which are undertaken with the resident institutional sectors (S124 - S125 - S11 - S13 - S14 - S15) (except with the central banks);
(b) as an average between the average of lending and deposits rates which are undertaken with the resident user institutional sectors (S124 - S125 - S11 - S13 - S14 - S15) (except with the central banks) and the implicit interest rate calculated as in Method 1;
(c) as an average between the average of lending and deposits rates which are undertaken with the resident user institutional sectors (S124 - S125 - S11 - S13 - S14 - S15) (except with the central banks) and the implicit interest rate calculated as in Method 2.
To determine FISIM imports and exports, the reference rate used is the average interbank rate weighted by the levels of stocks in the headings 'loans between S122 and S123 on the one hand, and non-resident FIs on the other hand` and 'deposits between S122 and S123 on the one hand, and non-resident FIs on the other hand`, which are included in the balance sheet of the financial intermediaries.
This calculated rate is the 'external` reference rate which is used to calculate FISIM exports and imports.
In the trial period the calculation should be done distinguishing the internal and external reference rates both on the basis of the residence of the FIs engaged in the transactions and on the basis of the currencies in which these transactions are denominated.
Member States are required to provide Eurostat with all the statistical information used in the methodology applied.
(c) Detailed breakdown of FISIM by institutional sector
For each institutional sector it is necessary to have the following table of loans and deposits granted by the resident FIs:
>START OF GRAPHIC>
>END OF GRAPHIC>
The total FISIM by institutional sector is obtained as the sum of FISIM on loans granted to the institutional sector and of FISIM on deposits of the institutional sector.
FISIM on the loans granted to the institutional sector = interest receivable on loans - (loan stocks × 'internal` reference rate)
FISIM on the deposits of the institutional sector = (deposit stocks × 'internal` reference rate) - interest payable on deposits
Part of the output is exported; on the basis of the balance sheet of the financial intermediaries (S122 and S123) we observe:
>START OF GRAPHIC>
>END OF GRAPHIC>
Exported FISIM is calculated using the 'external` interbank reference rate as follows:
FISIM on loans granted to non-residents (including FIs) = interest receivable - (loan stock × 'external` reference rate)
FISIM on the deposits of non-residents (including FIs) = (deposit stocks × 'external` reference rate) - interest payable
(d) Breakdown into intermediate and final consumption of FISIM allocated to households
The services attributable to households must be broken down into:
- intermediate consumption of households in their capacity as owners of dwellings,
- intermediate consumption of households in their capacity as owners of unincorporated enterprises,
- final consumption of households.
It entails a breakdown of loans to households (stocks and interest) into:
- dwelling loans,
- loans to households as owners of unincorporated enterprises,
- other loans to households.
Loans to households as owners of unincorporated enterprises and dwelling loans are generally shown separately in the various breakdowns of lending in financial and monetary statistics. Other loans to households can be obtained by subtraction. FISIM for loans to households should be distributed among three items (dwelling loans, loans to households as owners of unincorporated enterprises and other loans to households) on the basis of information on stocks and interest for each of the three groups. Dwelling loans are not identical to mortgage loans, as mortgage loans can have other purposes.
Household deposits must be broken down into:
- deposits of households as owners of unincorporated enterprises,
- deposits of individuals.
In the absence of statistics on deposits of households as owners of unincorporated enterprises in the trial period of five years Member States are required to compare the results on the allocation of FISIM by using the following two methods:
Method 1
Stocks can be calculated on the basis of the ratio of deposits to value added observed for the smallest size corporations and extrapolated for unincorporated enterprises.
Method 2
Stocks can be calculated on the basis of the ratio of deposits to turnover observed for the smallest size corporations and extrapolated for unincorporated enterprises.
FISIM on the deposits of households must be distributed between FISIM on the deposits of households as owners of unincorporated enterprises and that on the deposits of households as consumers on the basis of the average stocks of these two categories, for which, owing to lack of further information, the same interest rate may be used.
As an alternative, especially in the case more detailed information on loans and deposits of households is absent, FISIM to households can be allocated to intermediate consumption and final consumption assuring that all loans are attributable to households as producer or as owners of dwellings and that all deposits are attributable to households as consumers.
2. CALCULATION OF IMPORTED FINANCIAL INTERMEDIATION
Non-resident FIs grant loans to residents and receive deposits from residents. By institutional sector it is necessary to have the following table:
>START OF GRAPHIC>
>END OF GRAPHIC>
The financial intermediation imported by each institutional sector is therefore calculated as follows:
FISIM imported for loans = interest receivable by non-resident FIs - (loan stocks × 'external` reference rate)
FISIM imported for deposits = (deposit stocks × 'external` reference rate) - interest payable by non-resident FIs
3. FISIM AT CONSTANT PRICES
The difference between the reference rate and the effective rate of interest represents the margin earned by the financial intermediary, and thus can be considered to be the price paid for the service provided. FISIM at constant prices are derived as the quotient of the value of FISIM on loans and deposits held by S122 and S123 and this price. The stocks of loans and deposits are revalued to base period prices using a general price index (e.g. the implicit price deflator for domestic final demand).
>TABLE>
Base period margin on loans = effective interest rate on loans - reference rate
Base period margin on deposits = reference rate - effective interest rate on deposits
(1) The financial intermediaries to be considered are the sectors S122 (other monetary financial institutions) and S123 (other financial intermediaries, except insurance corporations and pension funds), except investment funds.
