Commission Regulation (EC) No 1102/2005
of 13 July 2005
amending Council Regulation (EC) No 32/2000 to take account of amendments to Council Regulation (EEC) No 2658/87 on the tariff and statistical nomenclature and on the Common Customs Tariff
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 32/2000 of 17 December 1999 opening and providing for the administration of Community tariff quotas bound in GATT and certain other Community tariff quotas and establishing detailed rules for adjusting the quotas, and repealing Regulation (EC) No 1808/95 [1], and in particular Article 9(1)(a) thereof,
Whereas:
(1) Council Regulation (EEC) No 2658/87 of 23 July 1987 on the tariff and statistical nomenclature and on the Common Customs Tariff [2] as amended by Commission Regulation (EC) No 1810/2004 [3], has amended the combined nomenclature codes for certain products of Annexes III, IV and V to Regulation (EC) No 32/2000. Those Annexes should therefore be amended accordingly.
(2) This Regulation should apply from the date of entry into force of Regulation (EC) No 1810/2004.
(3) The measures provided for in this Regulation are in accordance with the opinion of the Customs Code Committee,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 32/2000 is amended as follows:
1. in Annex III, the codes for order number 09.0107 are amended as follows:
(a) CN code "ex57023990" is replaced by CN code "ex57023900";
(b) CN code "ex57024990" and TARIC subdivision "10" for that code are replaced by CN code "ex57024900" and TARIC subdivision "20" correspondingly;
(c) CN code "ex57039000" is replaced by CN codes "ex57039010" and "ex57039090";
2. Annex IV is amended as follows:
(a) the CN codes for order number 09.0106, in the second column, are amended as follows:
(i) CN code "62079190" is replaced by CN code "ex62079100";
(ii) CN code "62089119" is replaced by CN code "ex62089100";
(iii) CN code "630251" is replaced by CN code "63025100";
(iv) CN code "630291" is replaced by CN code "63029100";
(v) CN codes "63012091" and "63012099" are replaced by CN code "63012090";
(b) the codes for order number 09.0106 in the column of TARIC codes are amended as follows:
(i) in the row for CN code "62079190" code "10" is replaced by code "91";
(ii) in the row for CN code "62089119" code "10" is replaced by code "18";
(c) the CN codes for order number 09.0106 are amended as follows:
(i) CN code "62079190" is replaced by CN code "62079100";
(ii) CN code "62089119" is replaced by CN code "62089100";
(iii) CN codes "63012091" and "63012099" are replaced by CN code "63012090";
(iv) CN codes "63025110" and "63025190" are replaced by CN code "63025100";
(v) CN codes "63029110" and "63029190" are replaced by CN code "63029100";
3. in Annex V, the codes for order number 09.0103, in the list of TARIC codes, in the column "CN code", are amended as follows:
(a) CN codes "52101110" and "52101190" are replaced by CN code "52101100";
(b) CN codes "52102110" and "52102190" are replaced by CN code "52102100";
(c) CN codes "52103110" and "52103190" are replaced by CN code "52103100".
Article 2
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Union.
It shall apply from 1 January 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 13 July 2005.
For the Commission
László Kovács
Member of the Commission
[1] OJ L 5, 8.1.2000, p. 1. Regulation as last amended by Commission Regulation (EC) No 545/2004 (OJ L 87, 25.3.2004, p. 12).
[2] OJ L 256, 7.9.1987, p. 1. Regulation as last amended by Regulation (EC) No 493/2005 (OJ L 82, 31.3.2005, p. 1).
[3] OJ L 327, 30.10.2004, p. 1.
--------------------------------------------------
