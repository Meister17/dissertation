Council Joint Action 2004/794/CFSP
of 22 November 2004
prolonging Joint Action 2002/921/CFSP extending the mandate of the European Union Monitoring Mission (EUMM)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty on European Union, and in particular Article 14 thereof,
Whereas:
(1) On 26 November 2002, the Council adopted Joint Action 2002/921/CFSP extending the mandate of the European Union Monitoring Mission [1] (EUMM).
(2) On 5 December 2003, the Council adopted Joint Action 2003/852/CFSP [2] prolonging Joint Action 2002/921/CFSP and extending the mandate of the EUMM until 31 December 2004.
(3) The EUMM should continue its activities in the Western Balkans in support of the European Union's policy towards that region.
(4) The mandate of the EUMM should therefore be extended and Joint Action 2002/921/CFSP should be prolonged and amended accordingly,
HAS ADOPTED THIS JOINT ACTION:
Article 1
Joint Action 2002/921/CFSP is hereby prolonged and the mandate of the EUMM is extended.
Article 2
Joint Action 2002/921/CFSP is hereby amended as follows:
(a) in Article 3(3), the date " 30 September 2004" shall be replaced by " 30 September 2005";
(b) in Article 6(1), the financial reference amount shall be EUR 4186482;
(c) in the second paragraph of Article 8, the date " 31 December 2004" shall be replaced by " 31 December 2005".
Article 3
This Joint Action shall enter into force on the date of its adoption.
Article 4
This Joint Action shall be published in the Official Journal of the European Union.
Done at Brussels, 22 November 2004.
For the Council
The President
B. R. Bot
--------------------------------------------------
[1] OJ L 321, 26.11.2002, p. 51 and corrigendum in, OJ L 324, 29.11.2002, p. 76.
[2] OJ L 322, 9.12.2003, p. 31.
