REGULATION (EEC) No 2910/73 OF THE COUNCIL of 23 October 1973 amending Regulation No 79/65/EEC as regards the utilization of accountancy data, the field of survey and the number of returning holdings to be included in the farm accountancy data network of the European Economic Community
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 43 thereof;
Having regard to the proposal from the Commission;
Having regard to the Opinion of the European Parliament;
Whereas Council Regulation No 79/65/EEC (1) of 15 June 1965 setting up a network for the collection of accountancy data on the incomes and business operation of agricultural holdings in the European Economic Community, as amended by Regulation (EEC) No 2835/72 (2), provides, in particular, for the use of accountancy data, the product of such said network, delimits the field of survey thereof and determines the number of returning holdings which it covers;
Whereas it is opportune to extend the use of such accountancy data;
Whereas the experience gained during the first few years of operation of the data network shows the need to define the limits of the field of survey in such manner that only such holdings as are sufficiently large to enable accounting systems to reflect them should be included;
Whereas, in accordance with Regulation (EEC) No 2835/72 the data network currently covers 13 600 returning holdings ; whereas the number of returning holdings must in future be properly representative of all agricultural holdings in the field of survey;
Whereas representation based on approximately 1 % of the total number of agricultural holdings in that field is a minimum that should be attained as quickly as possible ; whereas, consequently, the number of returning holdings needs to be increased;
Whereas the increase in the number of returning holdings must be phased gradually over several years in order to allow the national and regional authorities within the data network to carry out this extension smoothly,
HAS ADOPTED THIS REGULATION:
Article 1
The following shall be substituted for Article 1 (3) of Regulation No 79/65/EEC:
"The data obtained pursuant to this Regulation shall, in particular, serve primarily as the basis for the drawing up of reports by the Commission on the situation of agriculture and of agricultural markets as well as on farm incomes in the Community ; the reports are to be submitted annually to the Council and the European Parliament, in particular for the annual fixing of prices of agricultural produce."
Article 2
The following shall be substituted for Article 4 (1) and (2) of Regulation No 79/65/EEC:
"1. The field of survey referred to in Article 1 (2) (a) shall cover those agricultural holdings, which: - are run as market-oriented holdings,
- provide the main occupation of the operator, (1)OJ No 109, 23.6.1965, p. 1859/65. (2)OJ No L 298, 31.12.1972, p. 47.
- ensure the employment, per year, of at least one worker (1 man-work unit) ; this threshold may, however, be reduced in the case of a Member State to 0 775 man-work units in accordance with the procedure laid down in Article 19.
2. For the accounting years beginning in the year 1973 and 1974, the number of retaining holdings shall be 13 600. This number shall be increased gradually at the beginning of each of the financial years commencing during the period 1975 to 1978 so as to attain eventually the number of 28 000 returning holdings."
Article 3
The following shall be substituted for Article 23 of Regulation No 79/65/EEC:
"Before 1 January 1980 the Commission shall submit to the Council a full report on the operation of the data network together with any proposal for amending this Regulation, as appropriate."
Article 4
The list of divisions referred to in Article 2 (d) of Regulation No 79/65/EEC shall be replaced, as regards Italy, by the list annexed hereto.
Article 5
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Luxembourg, 23 October 1973.
For the Council
The President
Ib FREDERIKSEN
ANNEX ITALY
>PIC FILE= "T0011387">
