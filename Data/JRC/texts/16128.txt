Agreement on Workers Health Protection through the Good Handling and Use of Crystalline Silica and Products containing it
(2006/C 279/02)
The Annexes to this Agreement may be found on www.nepsi.eu
(1) Whereas, crystalline silica is abundant in nature, it makes approximately 12 % of the earth crust. Crystalline silica is naturally contained in several other minerals and mineral products.
(2) Whereas, industry makes intensive use of two of the crystalline forms of silica, i.e. quartz and cristobalite. Both are sold as sand, which is a granular material, or as flours that consist of particles finer than 0,1 millimetres.
(3) Whereas, crystalline silica, and materials/products/raw materials containing crystalline silica are used in a large variety of industries, including but not limited to the chemical, ceramics, construction, cosmetics, detergents, electronics, foundry, glass, horticultural, leisure, metal and engineering, coatings including paint, pharmaceutical industries, and as filtration media in several industries.
(4) Whereas, the European Commission's Scientific Committee for Occupational Exposure Limits (SCOEL) [1] concluded, interalia "that the main effect in humans of the inhalation of respirable crystalline silica is silicosis. There is sufficient information to conclude that the relative lung cancer risk is increased in persons with silicosis (and apparently, not in employees without silicosis exposed to silica dust in quarries and in the ceramic industry). Therefore, preventing the onset of silicosis will also reduce the cancer risk. Since a clear threshold for silicosis development cannot be identified, any reduction of exposure will reduce the risk of silicosis."
(5) Whereas, there seems to be evidence that there is a variable potency of the effects of Respirable crystalline silica in different industries.
(6) Whereas there are a series of confounding factors in the epidemiology of lung cancer, for example smoking, radon, and Polycyclic Aromatic Hydrocarbons.
(7) Whereas, there is currently no occupational exposure limit for Respirable crystalline silica at EU level and the national occupational exposure limits vary.
(8) Whereas, Respirable crystalline silica differs in many respects — including because of its natural abundance — from the situations normally dealt with under the workers safety legislation. Therefore, the present Agreement, which is in many respects unique, is a suitable instrument for dealing with this particular substance.
(9) Whereas the Parties act in the firm belief that this Agreement will contribute to protecting jobs, and securing the economic future of the sectors and companies.
(10) Whereas, the Parties will make their best efforts to obtain application of this Agreement to all companies within the whole sectors they represent.
(11) Whereas, the Parties to this Agreement act in accordance with Article 139 (1) and (2) EC Treaty.
Having regard to the aforesaid, the Parties conclude the following Agreement on prevention and workers health protection through the good handling and use of crystalline silica and products containing it.
Article 1
Objectives
This Agreement aims at
- protection of health of Employees and other individuals occupationally exposed at the workplace to Respirable crystalline silica from materials/products/raw materials containing crystalline silica.
- minimisation of exposure to Respirable crystalline silica at the workplace by applying the Good Practices stipulated herein in order to prevent, eliminate or reduce occupational health risks related to Respirable crystalline silica.
- increasing the knowledge about potential health effects of Respirable crystalline silica and about Good Practices.
Article 2
Scope
(1) This Agreement addresses the production and use of crystalline silica, as well as materials/products/raw materials containing crystalline silica that may potentially lead to Respirable crystalline silica exposure. Descriptions of industries concerned are provided in Annex 5 hereto.
(2) The scope of the Agreement includes ancillary activities related thereto, such as handling, storage, and transport. It also applies to mobile workplaces. Mobile workplaces may be subject to specific rules hereunder.
(3) This Agreement is applicable to the Parties, Employers and Employees as defined and stipulated hereunder.
Article 3
Definitions
(1) "Employer(s)" means the individual companies directly or indirectly represented by the Parties to this Agreement representing industry.
(2) "Employees" means the workers directly or indirectly represented by the Parties to this Agreement representing employees who may regularly or from time to time be exposed to Respirable crystalline silica. Employees are understood to mean part-time, full-time, as well as fixed-term employees, and other workers that act under the direct supervision of the Employer (e.g. seconded/posted workers).
(3) "Workers Representatives" means workers' representatives with specific responsibility for the safety and health of workers: any person elected, chosen or designated in accordance with national law and/or practices to represent workers where problems arise relating to the safety and health protection of workers at work.
(4) "Parties" means the signatories to this Agreement.
(5) "Respirable crystalline silica" means the mass fraction of inhaled crystalline silica particles penetrating to the unciliated airways. The respirable convention, which is a target specification for sampling instruments, is defined according to § 5.3. of European Standard EN 481 Workplace Atmospheres — Size fraction definition for measurement of airborne particles.
(6) "Good Practices" means the general principles of Directive 89/391 and of Section II of Directive 98/24 as further developed and illustrated by Annex 1 hereto, that may be updated from time to time.
(7) "Site" means an operational entity at which Respirable crystalline silica occurs. Storage and transport are considered separate Sites unless they are connected to a production or use Site. Mobile workplaces are also considered Sites.
(8) "Non-application" means non-observance of the Agreement including the Good Practices as defined under (6) above resulting in an increased exposure of Employees to Respirable crystalline silica and resulting risk to health that could have been avoided by observing the Good Practices.
(9) "National Practices" means competent authorities issued' or industry agreed' guidelines or standards, which are neither law nor regulation.
Article 4
Principles
(1) The Parties will co-operate to enhance the knowledge about health effects of Respirable crystalline silica, in particular by research, monitoring, and dissemination of Good Practices.
(2) The Parties recognize that there is a need for a European Respirable crystalline silica prevention strategy. However, this does not mean that signature of this Agreement should be considered as acknowledgement of uncontrolled exposure in the sector concerned or actual exposure in the entire sector.
(3) The Parties acknowledge that the general principles of Directive 89/391, and of Directive 98/24 on the protection of the health and safety of workers from the risks related to chemical agents at work remain at all times applicable (including, in particular, Article 4: determination and risk assessment; Article 5: risk prevention; Article 6: specific protection and prevention measures; Article 7: arrangements to deal with accidents, incidents and emergencies; Article 8: information and training for workers).
(4) The Parties agree that crystalline silica and materials/products/raw materials containing crystalline silica are, as further described in Annex 5 hereto, basic, useful and often indispensable components/ingredients for a large number of industrial and other professional activities contributing to protecting jobs, and securing the economic future of the sectors and companies, and that their production and wide-range use should therefore continue.
(5) The Parties agree that the implementation of the "Good Practices" illustrated in Annex 1 to this Agreement will make an effective contribution to risk management by preventing or, where this is not currently achievable, minimizing exposure to Respirable crystalline silica by the application of appropriate prevention and protection measures in application of Section II of Directive 98/24.
(6) This Agreement is without prejudice to the Employers' and Employees' obligation to comply with national and EU law in the area of workers health and safety.
(7) In so far as National Practices in force are shown to be more stringent than the requirements under this Agreement, the Employers and Employees will adhere to these National Practices.
Article 5
Good Practices
(1) The Parties jointly adopt the Good Practices as illustrated in Annex 1 hereto.
(2) Employers and Employees, and the Workers Representatives, will jointly make their best endeavours to implement the Good Practices at Site level in as far as applicable thereto, including in relation to non-Employees occupationally exposed at Sites, for example contractors (e.g. by making, where applicable, the Good Practices part of the contract specifications).
(3) Annex 1 may be adapted in accordance with the procedure provided for in Annex 7.
(4) Employers undertake to organise periodic training, and all concerned Employees undertake to follow this periodic training with regard to the implementation of the Good Practices [2].
Article 6
Monitoring
(1) Each Site will install a monitoring system for the application of the Good Practices. For this purpose, an Employee (e.g. the team leader of a site) will be designated for each site by the Employer to monitor the application of the Good Practices. He/she will report to the individual designated under (2) upon request.
(2) An individual will be designated by the Employer in accordance with the provisions of Article 7 of Directive 89/391 to monitor the application or Non-application of the Good Practices regularly. He/she will liaise with the persons designated under (1) above according to a schedule/procedure set up under his/her responsibility after consultation with the works council of the company and Workers Representatives where applicable.
(3) For dust monitoring, Employers will follow the relevant Dust Monitoring Protocol(s) as outlined in Annex 2. These Protocol(s) may be adapted to the specific needs of smaller Sites and may allow for random selection of Sites in case of a multitude of smaller Sites in specific sectors.
Article 7
Reporting, Improvement
(1) The Employers and the Employees with the support of the Workers Representatives shall jointly and continuously strive to respect the Good Practices, and to improve their application.
(2) Employers will report the application/Non-application and the improvement through their individual designated under Article 6 (2) every second year, for the first time in 2008 (reporting of 2007 data).
A reporting format is jointly developed by the Parties as Annex 3 to this Agreement.
(3) The Parties agree that the number of Non-application situations shall progressively decrease per Employer during the term of this Agreement unless the number of Non-application situations is such as not to allow for further improvement at which point the Employer will make its best efforts to retain the status quo.
(4) Reporting under (2) above shall be conducted through the respective Party on a consolidated basis to the Council. However, a list of Sites which are repeatedly in a situation of Non-application shall be annexed to the consolidated report.
Article 8
The Council
(1) Principle
The main aim followed by the Council is to identify existing problems and to propose possible solutions. The Council shall be the sole and exclusive organ to supervise the implementation and interpretation of the Agreement.
(2) Tasks
The Council shall review the Article 7 reports and shall issue a Summary Report at the latest by June 30 of the following year summarizing application, Non-application and improvement, stating the level of application/Non-application per industry sector, the reasons therefore and issuing recommendations related thereto. The Summary Report will be forwarded to the Parties and their members, the European Commission and the national authorities responsible for workers safety and shall be marked "confidential/sensitive business information". An Executive Summary may be made available to the public if desirable. In June 2007, the Summary Report shall be different in format simply summarizing, based on information made available by the Parties, the status of implementation and preparations for the first reporting to take place in 2008.
In cases of repeated Non-applications where these are the result of repeated and unjustified failure to implement corrective actions, the Council will decide on the measures to be taken to address these situations.
In addition to its tasks above, the Council shall also have the following tasks: (a) discussion and resolution of any issues of importance for the working of the Agreement; (b) resolution of any conflicts and interpretation issues under this Agreement, including those brought by individual Parties, Employers and Employees; (c) issuance of recommendations about possible revisions of this Agreement; (d) communication with third parties; and (e) adaptation of the Good Practices in accordance with Annex 7.
(3) Composition
The Council shall consist of representatives of the Parties appointed by them for the first time on the day of the signature of the Agreement for terms of four years each, equal in number representing Employers and Employees. The Parties may also, at the same time or if necessary thereafter, appoint one Alternate Member for each Council member who may accompany as silent observers or replace Council members as the need arises always with a view to ensuring continuity and appropriate expertise. The size of the Council shall be such as to be workable in practice and is fixed at a maximum of 30 (i.e. 15/15) including the chairpersons of Article 3 of Annex 6. Should any Party withdraw, resign from the Agreement, or cease to exist, or should a new Party join the Agreement in the course of a term of the Council, the Parties will adjust the membership of the Council accordingly albeit respecting the above maximum number. Parties not represented in the Council as Members or Alternate Members have the right to be heard by the Council and to be present in the debate on their issue. The Council's rules are set out in Annex 6 hereto.
(4) Decision making
The Council aims at taking decisions by consensus. Failing consensus, Council decisions will be taken at a double qualified majority of 75 % of the votes respectively attributed to representatives of the Employees and to representatives of the Employers. For example, if the Council consists of 30 members (15 on the Employees' side and 15 on the Employers' side), a majority of 12 votes on each side will be required.
(5) Secretariat
The Council shall be assisted logistically by a secretariat to be set up by the Parties at the time of signature of the Agreement.
Article 9
Confidentiality
(1) All oral and written communications among and between the Parties and their members concerning the application of this Agreement shall remain confidential and will not be made available to third parties unless there is a legal obligation of disclosure.
(2) The confidentiality provision referred to in (1) does not apply to the following:
- the Summary Report which will only be forwarded to the individuals and organizations listed in Article 8 (2),
- the Executive Summary which can be made public to third parties,
- the joint necessary contacts of the Council Chairpersons with third parties,
- the necessary circulation of information by the Parties to their members as long as those members are affected by the information disclosed.
(3) The identity of companies named in reports may only be disclosed to the members of the Parties which are concerned, unless otherwise decided by the Council under Article 8 (2). The receiving persons have to be bound to the same level of confidentiality obligations as set forth by this Agreement.
(4) Breaches of (1) and (3) shall entitle the damaged Party and/or its members to undertake legal action in accordance with national civil law.
Article 10
Health Surveillance
The occupational physician/industrial hygienist or equivalent internal or external organ appointed for the Site will define in accordance with national regulations, Article 10 of Directive 98/24 and the Health Surveillance Protocol as described in Annex 8 the scope of the medical examinations to be performed.
Article 11
Research — Data Collection
The Parties will discuss gaps in research and data and make recommendations as to research, including on safer products or processes, which must be subject to a risk assessment before putting them into use. They will also make recommendations as to data collection projects that should be carried out in the future. A list of previous research projects is attached in Annex 4.
Article 12
Duration — Revision
(1) This Agreement is valid for a minimum term of four years and is automatically renewed for consecutive two year terms. Parties are entitled to withdraw from the Agreement with one year notice.
(2) This Agreement will become void as soon as all of the Parties are no longer representative of their industry sectors, or less than two Parties, one representing Employers and one representing Employees in the same industry sector, would remain Parties hereto.
(3) Parties are entitled to withdraw at any time without prior notice from this Agreement in the event that their industry sector counterpart will cease to be a party to this Agreement or no longer be representative ("Reciprocity").
(4) In case future EU legislation related to crystalline silica should be proposed, the Parties will meet to evaluate the impact of this proposed legislation on this Agreement.
Article 13
Change of Parties
(1) This Agreement is open for signature by additional parties.
(2) This Agreement will bind successors in law of the Parties.
Article 14
Miscellaneous
(1) This Agreement does not create any rights and obligations other than those stipulated herein.
(2) Any claims and disputes in relation to the interpretation and application of this Agreement shall exclusively be handled by the Council and shall, because of the unique nature of the Agreement, not be subject to jurisdiction by the local national courts. Any other claims and disputes in relation to this Agreement shall be submitted to the law and jurisdiction of the country of residence of the defendant(s), at the competent local court of residence of the defendant(s).
(3) This Agreement will be translated into all official EU languages. The English version is binding for interpretation.
(4) In as far as there is a discrepancy between the Good Practices and more stringent National Practices in a specific jurisdiction, adherence to such National Practices required under Article 4 (7) will not constitute a situation of Non-application under Article 3 (8).
Article 15
Entry into Effect
This Agreement will enter into effect six months after signature by the first two Parties, one representing Employers and one representing Employees in the same industry sector provided the Agreement has been translated into all official EU languages.
Annex 1 [Good Practices (Good Practice Guide)]
Annex 2 [Dust Monitoring Protocol]
Annex 3 [Reporting Format]
Annex 4 [List of Research Projects]
Annex 5 [Descriptions of Industries]
Annex 6 [The Council — The Secretariat]
Annex 7 [Procedure for the Adaptation of the Good Practices]
Annex 8 [Health Surveillance Protocol for Silicosis]
Entered into, April 25, 2006.
By:
APFE — European Glass Fibre Producers Association
BIBM — International Bureau for Precast Concrete
CAEF — The European Foundry Association
CEEMET — Council of European Employers of the Metal, Engineering and Technology-Based Industries
CERAME-UNIE — The European Ceramics Industries
CEMBUREAU — The European Cement Association
EMCEF — European Mine, Chemical and Energy Workers' Federation
EMF — European Metalworkers' Federation
EMO — European Mortar Industry Organization
EURIMA — European Insulation Manufacturers Association
EUROMINES — European Association of Mining Industries
EURO-ROC — European and International federation of natural stones industries
ESGA — European Special Glass Association
FEVE — European Container Glass Federation
GEPVP — European Association of Flat Glass Manufacturers
IMA-Europe — The Industrial Minerals Association
UEPG — European Aggregates Association
[1] SCOEL SUM Doc 94-final on respirable crystalline silica, June 2003.
[2] See Article 13 of Directive 89/391.
--------------------------------------------------
