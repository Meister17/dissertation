Judgment of the Court (Grand Chamber) of 3 October 2006 (reference for a preliminary ruling from the Verwaltungsgericht Frankfurt am Main — Germany) — Fidium Finanz AG v Bundesanstalt für Finanzdienstleistungsaufsicht
(Case C-452/04) [1]
Referring court
Verwaltungsgericht Frankfurt am Main
Parties to the main proceedings
Applicant: Fidium Finanz AG
Defendant: Bundesanstalt für Finanzdienstleistungsaufsicht
Re:
Reference for a preliminary ruling — Verwaltungsgericht Frankfurt am Main — Interpretation of Arts 49, 56 and 58 EC — Undertaking established in a non-member country whose activities, consisting in the granting of loans, are directed entirely or principally at the territory of a Member State — Requirement of prior authorisation in the Member State in which the service is provided
Operative part of the judgment
National rules whereby a Member State makes the granting of credit on a commercial basis, on national territory, by a company established in a non-member country subject to prior authorisation, and which provide that such authorisation must be refused, in particular, if that company does not have its central administration or a branch in that territory, affect primarily the exercise of the freedom to provide services within the meaning of Article 49 EC et seq. A company established in a non-member country cannot rely on those provisions.
[1] OJ C 6, 08.01.2005.
--------------------------------------------------
