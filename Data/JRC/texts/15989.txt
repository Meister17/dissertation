Judgment of the Court
(Sixth Chamber)
of 17 November 2005
in Case C-22/05: Commission of the European Communities v Kingdom of Belgium [1]
In Case C-22/05 Commission of the European Communities (Agents: G. Rozet and N. Yerrell) v Kingdom of Belgium (Agent: M. Wimmer) — action under Article 226 EC for failure to comply with obligations, brought on 25 January 2005 — the Court (Sixth Chamber), composed of J.-P. Puissochet acting as President of the Sixth Chamber, S. von Bahr and A. Borg Barthet (Rapporteur), Judges; J. Kokott, Advocate General; R. Grass, Registrar, gave a judgment on 17 November 2005, in which it:
1. Declares that, by excluding people working in a fairground undertaking from the scope of the national measures transposing Council Directive 93/104/EC of 23 November 1993 concerning certain aspects of the organisation of working time, the Kingdom of Belgium has failed to fulfil its obligations under Article 1(3) and Article 17 of that directive;
2. Orders the Kingdom of Belgium to pay the costs.
[1] OJ C 82 of 02.04.2005.
--------------------------------------------------
