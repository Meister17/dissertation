Commission Regulation (EC) No 1194/2006
of 4 August 2006
opening crisis distillation as provided for in Article 30 of Council Regulation (EC) No 1493/1999 for table wine in Portugal
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1493/1999 of 17 May 1999 on the common organisation of the market in wine [1], and in particular point (f) in the second subparagraph of Article 33(1),
Whereas:
(1) Article 30 of Regulation (EC) No 1493/1999 provides for the possibility of a crisis distillation measure in the event of exceptional market disturbance due to major surpluses. Such measures may be limited to certain categories of wine and/or certain areas of production, and may apply to quality wines psr at the request of the Member State concerned.
(2) Portugual has requested that crisis distillation be opened for table wine produced in its territory.
(3) Considerable surpluses have been recorded on the table wine market in Portugal, which are reflected in a fall in prices and a worrying rise in stocks towards the end of the 2005/06 marketing year. In order to reverse this negative trend, and so remedy the difficult market situation, stocks of table wine should be reduced to a level that can be regarded as normal in terms of covering market requirements.
(4) Since the conditions laid down in Article 30(5) of Regulation (EC) No 1493/1999 are satisfied, a crisis distillation measure should be opened for a maximum of 200000 hectolitres of table wine.
(5) The crisis distillation opened by this Regulation must comply with the conditions laid down by Commission Regulation (EC) No 1623/2000 of 25 July 2000 laying down detailed rules for implementing Regulation (EC) No 1493/1999 on the common organisation of the market in wine with regard to market mechanisms [2] as regards the distillation measure provided for in Article 30 of Regulation (EC) No 1493/1999. Other provisions of Regulation (EC) No 1623/2000 must also apply, in particular those concerning the delivery of alcohol to intervention agencies and the payment of advances.
(6) The price distillers must pay producers should be set at a level that permits the market disturbance to be dealt with by allowing producers to take advantage of the possibility afforded by this measure.
(7) The product of crisis distillation must be raw or neutral alcohol only, for compulsory delivery to the intervention agency in order to avoid disturbing the market for potable alcohol, which is supplied largely by the distillation provided for in Article 29 of Regulation (EC) No 1493/1999.
(8) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Wine,
HAS ADOPTED THIS REGULATION:
Article 1
Crisis distillation as provided for in Article 30 of Regulation (EC) No 1493/1999 is hereby opened for a maximum of 200000 hectolitres of table wine in Portugal, in accordance with the provisions of Regulation (EC) No 1623/2000 concerning this type of distillation.
Article 2
Producers may conclude delivery contracts as provided for in Article 65 of Regulation (EC) No 1623/2000 (hereinafter referred to as "the contract") from 16 August to 15 September 2006.
Contracts shall be accompanied by proof that a security equal to EUR 5 per hectolitre has been lodged.
Contracts may not be transferred.
Article 3
1. If the total quantity covered by the contracts submitted to the intervention agency exceeds the quantity laid down in Article 1, Portugal shall determine the rate of reduction to be applied to the above contracts.
2. Portugal shall take the administrative steps necessary to approve the contracts by 31 October 2006 at the latest. The approval shall specify any rate of reduction applied and the quantity of wine accepted per contract and shall stipulate that the producer may cancel the contract where the quantity to be distilled is reduced.
Portugal shall notify the Commission before 15 November 2006 of the quantities of wine covered by approved contracts.
3. Portugal may limit the number of contracts that individual producers may conclude under this Regulation.
Article 4
1. The quantities of wine covered by approved contracts shall be delivered to the distilleries by 15 February 2007 at the latest. The alcohol obtained must be delivered to the intervention agency in accordance with Article 6(1) by 15 May 2007 at the latest.
2. The security shall be released for the quantities delivered when the producer presents proof of delivery to a distillery.
The security shall be forfeit where no delivery is made within the time limit laid down in paragraph 1.
Article 5
The minimum price paid for wine delivered for distillation under this Regulation shall be EUR 1,914 per % volume per hectolitre.
Article 6
1. Distillers shall deliver the product obtained from distillation to the intervention agency. That product shall be of an alcoholic strength of at least 92 % volume.
2. The price the intervention agency must pay distillers for raw alcohol delivered shall be EUR 2,281 per % volume per hectolitre. The payment shall be made in accordance with Article 62(5) of Regulation (EC) No 1623/2000.
Distillers may receive an advance of EUR 1,122 per % volume per hectolitre on that amount. In that case the advance shall be deducted from the price actually paid. Articles 66 and 67 of Regulation (EC) No 1623/2000 shall apply.
Article 7
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Union.
It shall apply from 16 August 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 4 August 2006.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 179, 14.7.1999, p. 1. Regulation as last amended by Regulation (EC) No 2165/2005 (OJ L 345, 28.12.2005, p. 1).
[2] OJ L 194, 31.7.2000, p. 45. Regulation as last amended by Regulation (EC) No 1820/2005 of 8 November 2005 (OJ L 293, 9.11.2005, p. 8).
--------------------------------------------------
