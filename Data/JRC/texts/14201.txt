Commission Regulation (EC) No 1030/2006
of 6 July 2006
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 7 July 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 6 July 2006.
For the Commission
J. L. Demarty
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 386/2005 (OJ L 62, 9.3.2005, p. 3).
--------------------------------------------------
ANNEX
to Commission Regulation of 6 July 2006 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code [1] | Standard import value |
07020000 | 052 | 67,5 |
204 | 28,7 |
999 | 48,1 |
07070005 | 052 | 93,2 |
999 | 93,2 |
07099070 | 052 | 83,8 |
999 | 83,8 |
08055010 | 388 | 58,2 |
528 | 55,3 |
999 | 56,8 |
08081080 | 388 | 87,3 |
400 | 114,5 |
404 | 94,7 |
508 | 87,3 |
512 | 81,2 |
524 | 54,1 |
528 | 87,1 |
720 | 114,4 |
800 | 145,8 |
804 | 99,7 |
999 | 96,6 |
08082050 | 388 | 103,9 |
512 | 94,6 |
528 | 90,7 |
720 | 35,0 |
999 | 81,1 |
08091000 | 052 | 192,0 |
999 | 192,0 |
08092095 | 052 | 313,5 |
068 | 95,0 |
608 | 218,2 |
999 | 208,9 |
08094005 | 624 | 146,4 |
999 | 146,4 |
[1] Country nomenclature as fixed by Commission Regulation (EC) No 750/2005 (OJ L 126, 19.5.2005, p. 12). Code "999" stands for "of other origin".
--------------------------------------------------
