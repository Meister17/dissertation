Commission Regulation (EC) No 1809/2001
of 9 August 2001
amending Annexes I, II, III, V, VII, VIII and IX to Council Regulation (EEC) No 3030/93 on common rules for imports of certain textile products from third countries
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 3030/93 of 12 October 1993 on common rules for imports of certain textile products from third countries(1), as last amended by Regulation (EC) No 391/2001(2), and in particular Article 19 thereof,
Whereas:
(1) The Council has decided by decision of 29 June 2000(3) on the signing of the Agreement in the form of an Exchange of Letters amending the Agreement between the European Community and the Socialist Republic of Vietnam on trade in textile and clothing products, and authorising its provisional application.
(2) The Council has decided by decision of 9 November 2000(4) on the list of textiles and clothing products to be integrated into GATT 1994 on 1 January 2002.
(3) The Council has decided by decision of 23 November 2000(5) on the signing of an Agreement in the form of an Exchange of Letters between the European Community and the People's Republic of China concerning the extension and amendment of the Agreement between the European Community and the People's Republic of China on trade in textile products and authorising its provisional application.
(4) The Council has decided by decision of 4 December 2000(6) on the conclusion of Agreements on trade in textile products with certain third countries (Armenia, Azerbaijan, Republic of Belarus, People's Republic of China, Arab Republic of Egypt, Former Yugoslav Republic of Macedonia, Georgia, Kazakhstan, Moldova, Kingdom of Nepal, Tajikistan, Turkmenistan, Ukraine, Uzbekistan).
(5) The Council has decided by decision of 19 December 2000(7) on the signing of an Agreement in the form of an Exchange of Letters between the European Community and Ukraine concerning the extension and amendment of the Agreement between the European Community and Ukraine on trade in textile products and authorising its provisional application.
(6) The Council has decided by decision of 22 December 2000(8) on the signing and the provisional application of the Agreement on trade in textile products between the European Community and the Republic of Croatia.
(7) The Council has decided by decision of 26 February 2001(9) on the signing of an Agreement in the form of a Memorandum of Understanding between the European Community and the Democratic Socialist Republic of Sri Lanka on arrangements in the area of market access for textile and clothing products and authorising its provisional application.
(8) The Council has decided by decision of 26 February 2001(10) on the signing of an Agreement in the form of an Exchange of Letters between the European Community and Bosnia and Herzegovina concerning the extension and amendment of the Agreement between the European Community and Bosnia and Herzegovina on trade in textile products and authorising its provisional application.
(9) The textiles protocols to the Europe Agreements with Latvia and Lithuania have expired.
(10) The Council has decided by decision of 23 July 2001 on the conclusion of an Agreement in the form of an Exchange of Letters between the European Community and the Republic of Estonia on the termination of Protocol 1 on trade in textile and clothing products of the Europe Agreement establishing an association between the European Communities and their Member States, of the one part, and the Republic of Estonia, of the other part.
(11) The name of Hong Kong's Trade Department was changed to "Trade and Industry Department". New forms for licences and certificate of origin were provided by Hong Kong authorities.
(12) All the above elements make it necessary to amend the relevant portions of Annexes I, II, III, V, VII, VIII and IX to Regulation (EEC) No 3030/93 to take into account these modifications, which are applicable to the importation into the Community of certain textile products originating in certain third countries within the meaning of Article 19 of the abovementioned Regulation. For reasons of clarity, Annexes I, II, V, VII, VIII and IX were replaced by updated versions.
(13) In order to ensure that the Community complies with its international obligations the measures provided for in this Regulation should apply with effect from 1 January 2001, with the exception of the agreements with the Democratic Socialist Republic of Sri Lanka, Bosnia and Herzegovina, Ukraine and Estonia.
(14) The measures provided for in this Regulation are in accordance with the opinion of the Textile Committee,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EEC) No 3030/93 is amended as follows:
1. Annex I is replaced by Annex I to this Regulation.
2. Annex II is replaced by Annex II to this Regulation.
3. In Annex III, Article 28(6) is replaced by the following text: "6. This number shall be composed of the following elements:(11):
- two letters identifying the exporting country as follows:
- Argentina= AR
- Armenia= AM
- Azerbaijan= AZ
- Bangladesh= BD
- Belarus= BY
- Bosnia and Herzegovina= BA
- Brazil= BR
- Cambodia= KH
- China= CN
- Croatia= HR
- Egypt= EG
- Former Yugoslav Republic of Macedonia= 96(12)
- Georgia= GE
- Hong Kong= HK
- India= IN
- Indonesia= ID
- Kazakhstan= KZ
- Kyrgyzstan= KG
- Laos= LA
- Macao= MO
- Malaysia= MY
- Moldova= MD
- Mongolia= MN
- Nepal= NP
- Pakistan= PK
- Peru= PE
- Philippines= PH
- Russian Federation= RU
- Singapore= SG
- South Korea= KR
- Sri Lanka= LK
- Taiwan= TW
- Tajikistan= TJ
- Thailand= TH
- Turkmenistan= TM
- Ukraine= UA
- United Arab Emirates= AE
- Uzbekistan= UZ
- Vietnam= VN
- two letters identifying the intended Member State of destination as follows:
- AT= Austria
- BL= Benelux
- DE= Federal Republic of Germany
- DK= Denmark
- EL= Greece
- ES= Spain
- FI= Finland
- FR= France
- GB= United Kingdom
- IE= Ireland
- IT= Italy
- PT= Portugal
- SE= Sweden
- a one-digit number identifying the quota year or the year under which exports were recorded, in the case of products listed in Table A of this Annex, corresponding to the last figure in the year in question, e.g. '1' for 2001, etc. In the case of products originating in the People's Republic of China listed in Appendix C to Annex V this number should be '7' for the year 2001,
- a two-digit number identifying the issuing office in the exporting country,
- a five-digit number running consecutively from 00001 to 99999 allocated to the specific Member State of destination."
4. Table A of Annex III is replaced by Annex III to this Regulation.
5. Specimen of Hong Kong export licence Form 4 is replaced by Annex IV to this Regulation.
6. Specimen of Hong Kong export licence Form 5 is replaced by Annex V to this Regulation.
7. Specimen of Hong Kong certificate of origin is replaced by Annex VI to this Regulation.
8. Annex V is replaced by Annex VII to this Regulation.
9. Annex VII is replaced by Annex VIII to this Regulation.
10. Annex VIII is replaced by Annex IX to this Regulation.
11. Annex IX is replaced by Annex X to this Regulation.
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Communities.
It shall apply with effect from 1 January 2001. As far as the Democratic Socialist Republic of Sri Lanka and Bosnia and Herzegovina are concerned, the Regulation shall apply with effect from 1 March 2001 and for Ukraine with effect from 26 March 2001. For Estonia it shall apply as soon as the agreement enters into force.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 9 August 2001.
For the Commission
Pascal Lamy
Member of the Commission
(1) OJ L 275, 8.11.1993, p. 1.
(2) OJ L 58, 28.2.2001, p. 3.
(3) OJ L 190, 27.7.2000, p. 1.
(4) OJ L 286, 11.11.2000, p. 1.
(5) OJ L 314, 14.12.2000, p. 13.
(6) OJ L 326, 22.12.2000, p. 63.
(7) OJ L 16, 18.1.2001, p. 1.
(8) OJ L 25, 26.1.2001, p. 1.
(9) OJ L 80, 20.3.2001, p. 11.
(10) OJ L 83, 22.3.2001, p. 1.
(11) In the case of Egypt, this provision will enter into force at a later date.
(12) Two digits in the case of FYROM.
ANNEX I
"ANNEX I
TEXTILE PRODUCTS REFERRED TO IN ARTICLE 1(1)
1. Without prejudice to the rules for the interpretation of the Combined Nomenclature, the wording of the description of goods is considered to be of indicative value only, since the products covered by each category are determined, within this Annex, by CN codes. Where there is an "ex" symbol in front of a CN code, the products covered in each category are determined by the scope of the CN code and by that of the corresponding description.
2. When the constitutive material of the products of categories 1 to 114 is not specifically mentioned, these products are to be taken to be made exclusively of wool or of fine animal hair, of cotton or of man-made fibres. This applies to the following countries: Argentina, Bangladesh, Bosnia and Herzegovina, Brazil, Cambodia, China (MFA Agreement), Croatia, Egypt, Former Yugoslav Republic of Macedonia, Hong Kong, India, Indonesia, Laos, Macao, Malaysia, Nepal, Pakistan, Peru, Philippines, Russian Federation, Singapore, South Korea, Sri Lanka, Taiwan, Thailand and Vietnam.
3. Garments which are not recognisable as being garments for men or boys or as being garments for women or girls are classified with the latter.
4. Where the expression "babies' garments" is used, this is meant to cover garments up to and including commercial size 86.
GROUP IA
>TABLE>
(1) Covers only categories 1 to 114, with the exception of Armenia, Azerbaijan, Belarus, Cambodia, China (non-MFA Agreement), Georgia, Kazakhstan, Kyrgyzstan, Laos, Modova, Mongolia, Nepal, Russian Federation, Tajikistan, Turkmenistan, Ukraine, United Arab Emirates, Uzbekistan and Vietnam, for which categories 1 to 161 are covered and of Bosnia and Herzegovina, Croatia, Former Yugoslav Republic of Macedonia and Taiwan for which categories 1 to 123 are covered. In the case of Taiwan categories 115 to 123 are included in Group IIIB.
ANNEX IA
>TABLE>
ANNEX IB
1. This Annex covers textile raw materials (categories 128 and 154), textile products other than those of wool and fine animal hair, cotton and man-made fibres, as well as man-made fibres and filaments and yarns of categories 124, 125A, 125B, 126, 127A and 127B.
2. Without prejudice to the rules for the interpretation of the Combined Nomenclature, the wording of the description of goods is considered to be of indicative value only, since the products covered by each category are determined, within this Annex, by CN codes. Where there is an "ex" symbol in front of a CN code, the products covered in each category are determined by the scope of the CN code and by that of the corresponding description.
3. Garments which are not recognisable as being garments for men or boys or as being garments for women or girls are classified with the latter.
4. Where the expression "babies' garments" is used, this is meant to cover garments up to and including commercial size 86.
GROUP I
>TABLE>"
ANNEX II
"ANNEX II
EXPORTING COUNTRIES REFERRED TO IN ARTICLE 1
Argentina
Armenia
Azerbaijan
Bangladesh
Belarus
Bosnia and Herzegovina
Brazil
Cambodia
China
Croatia
Egypt
Former Yugoslav Republic of Macedonia
Georgia
Hong Kong
India
Indonesia
Kazakhstan
Kyrgyzstan
Laos
Macao
Malaysia
Moldova
Mongolia
Nepal
Pakistan
Peru
Philippines
Russian Federation
Singapore
South Korea
Sri Lanka
Taiwan
Tajikistan
Thailand
Turkmenistan
Ukraine
United Arab Emirates
Uzbekistan
Vietnam"
ANNEX III
"TABLE A
Countries and categories subject to the system of double-checking surveillance
(The complete description of the categories is shown in Annex I)
>TABLE>"
ANNEX IV
Specimen of Hong Kong export licence - Form 4
>PIC FILE= "L_2001252EN.004202.TIF">
ANNEX V
Specimen of Hong Kong export licence - Form 5
>PIC FILE= "L_2001252EN.004302.TIF">
ANNEX VI
Specimen of Hong Kong certificate of origin
>PIC FILE= "L_2001252EN.004402.TIF">
ANNEX VII
"ANNEX V
COMMUNITY QUANTITATIVE LIMITS
applicable for the years 2001 and 2002
(The complete description of the goods is shown in Annex I)
>TABLE>
Appendix A to Annex V
>TABLE>
Appendix B to Annex V
>TABLE>
Flexibilities provided for in Article 7 of and Annex VIII to Regulation (EEC) No 3030/93 for China are applicable to the above categories and amounts.
Appendix C to Annex V
COMMUNITY QUANTITATIVE LIMITS
(The complete description of the goods is shown in Annex IB)
>TABLE>"
ANNEX VIII
"ANNEX VII
REFERRED TO IN ARTICLE 5
Outward processing traffic
Article 1
Reimports into the Community of textile products listed in column 2 of the table attached to this Annex, effected in accordance with the Regulations on economic outward processing in force in the Community, shall not be subject to the quantitative limits referred to in Article 2 of the Regulation where they are subject to specific quantitative limits given in column 4 of the table and have been reimported after processing in the corresponding third country listed in column 1 for each of the quantitative limits specified.
Article 2
Reimports not covered by this Annex may be subject to specific quantitative limits in accordance with the procedure laid down in Article 17 of the Regulation, provided that the products concerned are subject to the quantitative limits laid down in Article 2 of this Regulation.
Article 3
1. Transfers between categories and advance use or carry-over of portions of specific quantitative limits from one year to another may be carried out in accordance with the procedure laid down in Article 17 of the Regulation.
2. However, automatic transfers in accordance with paragraph 1 may be carried out within the following limits:
- transfer between categories for up to 20 % of the quantitative limit established for the category to which the transfer is made,
- carry-over of a specific quantitative limit from one year to another for up to 10,5 % of the quantitative limit established for the actual year of utilisation,
- advance use of a specific quantitative limit for up to 7,5 % of the quantitative limit established for the actual year of utilisation.
3. Where there is a need for additional imports the specific quantitative limits may be adjusted in accordance with the procedure laid down in Article 17 of the Regulation.
4. The Commission shall inform the third country or countries concerned of any measures taken pursuant to the preceding paragraphs.
Article 4
1. For the purpose of applying Article 1, the competent authorities of the Member States, before issuing prior authorisations in accordance with the relevant Community Regulations on economic outward processing, shall notify the Commission of the amounts of the requests for authorisations which they have received. The Commission shall notify its confirmation that the requested amount(s) are available for reimportation within the respective Community limits in accordance with the relevant Community Regulations on economic outward processing.
2. The requests included in the notifications to the Commission shall be valid if they establish clearly in each case:
(a) the third country in which the goods are to be processed;
(b) the category of textile products concerned;
(c) the amount to be reimported;
(d) the Member State in which the reimported products are to be put into free circulation;
(e) an indication as to whether the requests relate to
(i) a past beneficiary applying for the quantities set aside under Article 3(4) or in accordance with the fifth subparagraph of Article 3(5) of Council Regulation (EC) No 3036/94(1), or to
(ii) an applicant under the third subparagraph of Article 3(4) or under Article 3(5) of that Regulation.
3. Normally the notifications referred to in the previous paragraphs of this Article shall be communicated electronically within the integrated network set up for this purpose, unless for imperative technical reasons it is necessary to use other means of communication temporarily.
4. As far as possible, the Commission shall confirm to the authorities the full amount indicated in the requests notified for each category of products and each third country concerned. Notifications presented by Member States for which no confirmation can be given because the amounts requested are no longer available within the Community quantitative limits will be stored by the Commission in the chronological order in which they have been received and confirmed in the same order as soon as further amounts become available through the application of flexibilities foreseen in Article 3.
5. The competent authorities shall notify the Commission immediately after being informed of any quantity that is not used during the duration of validity of the import authorisation. Such unused quantities shall automatically be recredited to the quantities within the Community quantitative limits not set aside pursuant to the first subparagraph of Article 3(4) or to the fifth subparagraph of Article 3(5) of Regulation (EC) No 3036/94.
The quantities for which a renunciation has been made pursuant to the third subparagraph of Article 3(4) of Regulation (EC) No 3036/94, shall automatically be added to the quantities within the Community quota that are not set aside pursuant to the first subparagraph of Article 3(4) or to the fifth subparagraph of Article 3(5) of the said Regulation.
All such quantities as outlined in the preceding subparagraphs shall be notified to the Commission in accordance with paragraph 3 above.
Article 5
The certificate of origin shall be issued by the competent governmental authorities in the supplier country concerned, in accordance with the Community legislation in force and the provisions of Annex III for all products covered by this Annex.
Article 6
The competent authorities of the Member States shall supply the Commission with the names and addresses of the authorities competent to issue the prior authorisations referred to in Article 4 together with specimens of the stamp impressions used by them.
TABLE
Community quantitative limits for goods reimported under OPT
applicable for the years 2001-02
(The complete description of the goods is shown in Annex I)
>TABLE>
(1) OJ L 322, 15.2.1994, p. 1."
ANNEX IX
"ANNEX VIII
REFERRED TO IN ARTICLE 7
Flexibility provisions
The attached table indicates for each of the supplier countries listed in column 1 the maximum amounts which, after advance notification to the Commission, it may transfer between the corresponding quantitative limits indicated in Annex V in accordance with the following provisions:
- advance utilisation of the quantitative limit for the particular category established for the following quota year shall be authorised up to the percentage of the quantitative limit for the current year indicated in column 2; the amounts in question shall be deducted from the corresponding quantitative limits for the following year,
- carry-over of amounts not utilised in a given year to the corresponding quantitative limit for the following year shall be authorised up to the percentage of the quantitative limit for the year of actual utilisation indicated in column 3,
- transfers from categories 1 to categories 2 and 3 shall be authorised up to the percentages of the quantitative limit to which the transfer is made indicated in column 4,
- transfers between categories 2 and 3 shall be authorised up to the percentages of the quantitative limit to which the transfer is made indicated in column 5,
- transfers between categories 4, 5, 6, 7 and 8 shall be authorised up to the percentages of the quantitative limit to which the transfer is made indicated in column 6,
- transfers into any of the categories in Group II or III (and where applicable Group IV) from any of the categories in Groups I, II or III shall be authorised up to the percentages of the quantitative limit to which the transfer is made indicated in column 7.
The cumulative application of the flexibility provisions referred to above shall not result in an increase in any Community quantitative limit for a given year above the percentage indicated in column 8.
The table of equivalence applicable to the abovementioned transfers is given in Annex I.
Additional conditions, possibilities for transfers and notes are given in column 9 of the table.
>TABLE>
n.a. = not applicable.
Flexibility provisions for quantitative restrictions referred to in Appendix C to Annex V
>TABLE>
n.a. = not applicable.
Appendix to Annex VIII
Flexibility provisions Hong Kong
>TABLE>
>TABLE>"
ANNEX X
"ANNEX IX
REFERRED TO IN ARTICLE 10
Safeguard clauses; basket exit thresholds
>TABLE>
>TABLE>
>TABLE>"
