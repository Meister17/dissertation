Council Decision 2005/890/CFSP
of 12 December 2005
implementing Common Position 2004/179/CFSP concerning restrictive measures against the leadership of the Transnistrian region of the Republic of Moldova
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to Common Position 2004/179/CFSP [1], and in particular Article 2(1) thereof, in conjunction with Article 23(2) of the Treaty on European Union,
Whereas:
(1) On 23 February 2004, the Council adopted Common Position 2004/179/CFSP.
(2) On 26 August 2004, the Council adopted Common Position 2004/622/CFSP in order to extend the scope of the restrictive measures imposed by Common Position 2004/179/CFSP to persons responsible for the design and implementation of the intimidation and closure campaign against Latin-script Moldovan schools in the Transnistrian region.
(3) On 21 February 2005, the Council adopted Common Position 2005/147/CFSP [2] extending and amending Common Position 2004/179/CFSP.
(4) Annex II to Common Position 2004/179/CFSP should be amended in recognition of the improvements in the situation of Latin-script schools in some areas of the Transnistrian region,
HAS DECIDED AS FOLLOWS:
Article 1
Annex II to Common Position 2004/179/CFSP shall be replaced by the Annex to this Decision.
Article 2
This Decision shall take effect on the date of its adoption.
Article 3
This Decision shall be published in the Official Journal of the European Union.
Done at Brussels, 12 December 2005.
For the Council
The President
J. Straw
[1] OJ L 55, 24.2.2004, p. 68. Common Position as amended by Common Position 2004/622/CFSP (OJ L 279, 28.8.2004, p. 47).
[2] OJ L 49, 22.2.2005, p. 31.
--------------------------------------------------
ANNEX
"ANNEX II
LIST OF PERSONS REFERRED TO IN THE SECOND INDENT OF ARTICLE 1(1)
1. PLATONOV, Yuri Mikhailovich,
known as Yury Platonov,
Head of City Administration of Rybnitsa,
born on 16 January 1948,
Russian passport No 51 NO. 0527002,
issued by the Russian Embassy in Chisinau on 4 May 2001.
2. CHERBULENKO, Alla Viktorovna,
Deputy Head of City Administration of Rybnitsa, responsible for education issues."
--------------------------------------------------
