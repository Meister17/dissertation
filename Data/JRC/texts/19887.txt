COUNCIL DIRECTIVE of 19 December 1978 amending Directive 77/391/EEC introducing Community measures for the eradication of brucellosis, tuberculosis and leucosis in cattle (79/9/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Articles 43 and 100 thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament (1),
Whereas in the framework of the national plans for the implementation of Council Directive 77/391/EEC of 17 May 1977 introducing Community measures for the eradication of brucellosis, tuberculosis and leucosis in cattle (2), it is necessary to provide for the doubling of the slaughter premium paid in respect of certain regions with a high incidence of these diseases in Belgium,
HAS ADOPTED THIS DIRECTIVE:
Article 1
By way of derogation from Article 7 (2) of Directive 77/391/EEC, the European Agricultural Guidance and Guarantee Fund, Guidance Section, may pay to the Kingdom of Belgium, at its request, for the eradication of brucellosis, an amount not exceeding 120 units of account per cow and 60 units of account per bovine other than cows, slaughtered in the framework of the measures referred to in Article 2 of the said Directive.
Article 2
This Directive is addressed to the Member States.
Done at Brussels, 19 December 1978.
For the Council
The President
J. ERTL (1)Opinion delivered on 15.12.1978 (not yet published in the Official Journal). (2)OJ No L 145, 13.6.1977, p. 44.
