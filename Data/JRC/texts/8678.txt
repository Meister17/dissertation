DECISION OF THE CENTRAL GROUP
of 22 March 1999
on general principles governing the payment of informers
(SCH/C (99)25)
Informers play a valuable role in the fight against serious cross-border crime, and drug-related crime in particular, since they usually enjoy the trust of the offenders and thus use of such persons affords an opportunity to gain a general picture of the activities of clandestine criminal organisations and structures.
The Working Group on Drugs has addressed this issue under the German Presidency and examined the laws and practices relating to the payment of informers in each Schengen State. Based on the results of this analysis, the Working Group on Drugs has devised common guiding principles for the payment of informers in the form of money or non-material benefits. These general principles are to be regarded as non-binding guidelines in the Schengen area and are intended to contribute to the further enhancement of police and customs cooperation in this sensitive sphere. They are also to serve as a possible guide for those Schengen States currently engaged in drafting or amplifying similar regulations.
The Central Group acknowledges and endorses the - non-binding - "General principles governing the payment of informers" (Doc. SCH/Stup (98)72 Rev 2), subject to approval by the Executive Committee.
