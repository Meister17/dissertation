Brussels, 06.01.2005
COM(2004) 856 final
.
Proposal for a
COUNCIL DECISION
authorising Denmark to apply a measure derogating from Article 14 (1) (d) of the Sixth Directive 77/388/EEC on the harmonisation of the laws of the Member States relating to turnover taxes
.
(presented by the Commission)
EXPLANATORY MEMORANDUM
By letter registered at the Commission’s Secretariat-General on 17 May 2004, the Danish authorities requested authorisation, under Article 27 of Council Directive 77/388/EEC of 17 May 1977 on the harmonisation of the laws of the Member States relating to turnover taxes - Common system of value added tax: uniform basis of assessment[1] (hereafter referred to as the Sixth Directive), to introduce special measures for derogation from Article 14 (1) (d) of the Directive, in order to prevent certain types of tax evasion or avoidance.
In accordance with Article 27(2) of the Sixth Directive, the Commission informed the other Member States by letter of 15 October 2004 of the request made by the Danish Government and by letter of 19 October 2004 the Commission notified Denmark that it had all the information it considers necessary for appraisal of the request.
Article 14 (1) (d) of Directive 77/388/EEC stipulates that Member States shall exempt the final importation of goods qualifying for exemption from customs duties other than as provided for in the Common Customs Tariff. Article 14 (1) (d) has been implemented by Directive 83/181/EEC[2] and in particular by its Article 22 which stipulates that goods of a total value not exceeding 10 Euro shall be exempt on admission. Member States may grant exemption for imported goods of a total value of more than 10 Euro but not exceeding 22 Euro. However, Member States may also exclude goods which have been imported on mail order from this exemption.
Denmark currently exempts imports of small consignments with a commercial nature from outside the EU from VAT. The Danish limit for VAT exemption is DKK 80 (EUR 10). However, the Danish authorities have discovered that publishing companies re-route the distribution of certain magazines and periodicals via territories not covered by the Sixth Directive. According to the investigation of the Danish authorities in the first nine months of 2003 some 3.5 million weeklies and magazines were imported from the Åland islands only, which meant an estimated loss of revenue to the State of some DKK 47 million (ca. EUR 6.2 million). There is the risk that the loss of revenue will increase unless the VAT rules are amended to prevent this tax avoidance. Denmark’s losses of VAT revenue also have a negative impact on the Community's own resources.
Since the introduction of Directive 83/181/EEC, Member States no longer have the option of not granting the exemption for these imports on the basis of the first sub-paragraph of Article 14 (1) (d) of the Sixth Directive and in any event the Danish government cannot establish whether these imports have a serious effect on conditions of competition. The request for derogation only targets the consignments and scenarios subject to the avoidance scheme and does not intend to exclude all mail order consignments on the basis of Article 22 of Directive 83/181/EEC.
It therefore appears that the envisaged derogation from Article 14 (1) (d) is in fact the most appropriate solution in the specific circumstances and for the particular tax avoidance scenario to be addressed. The derogation avoids the loss of VAT.
Proposal for a
COUNCIL DECISION
authorising Denmark to apply a measure derogating from Article 14 (1) (d) of the Sixth Directive 77/388/EEC on the harmonisation of the laws of the Member States relating to turnover taxes
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community,
Having regard to the Sixth Council Directive 77/388/CEE of 17 May 1977 on the harmonisation of the laws of the Member States relating to turnover taxes - Common system of value added tax: uniform basis of assessment(1), and in particular Article 27(1) thereof,
Having regard to the proposal from the Commission[3],
Whereas:
(1) In a letter registered by the Commission's Secretariat-General on 17 May 2004 the Danish authorities requested authorisation to introduce special measures for derogation from Article 14 (1) (d) of the Directive, in order to prevent certain types of tax evasion or avoidance. The other Member States were informed of the request on 15 October 2004,
(2) The derogating measure is intended to exclude certain magazines and periodicals imported into Denmark from benefiting from the exemption under Article 14 (1) (d) of the Directive and apply VAT on them. Article 14 (1) (d) has been implemented by Directive 83/181/EEC[4] and in particular by its Article 22 which stipulates that goods of a total value not exceeding 10 Euro shall be exempt on admission. Member States may grant exemption for imported goods of a total value of more than 10 Euro but not exceeding 22 Euro. Denmark currently exempts imports of all small consignments with a commercial nature from outside the EU from VAT. The Danish limit for VAT exemption is DKK 80 (EUR 10).
(3) The Danish authorities have discovered that some publishing companies re-route the distribution of their publications to subscribers in Denmark via territories not covered by the Sixth Directive, with a loss in revenue for Denmark and consequently with a negative impact on the Community’s own resources. There is the risk that the loss of revenue will increase unless Denmark is authorised to prevent this type of tax avoidance.
(4) Since the introduction of Directive 83/181/EEC, Member States no longer have the option of not granting the exemption for these imports on the basis of the first sub-paragraph of Article 14 (1) (d) of the Sixth Directive. In any event the Danish government cannot establish whether the imports in question have a serious effect on conditions of competition. Furthermore the request for derogation only targets the consignments and scenarios subject to the avoidance scheme and does not intend to exclude all mail order consignments from the exemption on the basis of Article 22 of Directive 83/181/EEC. It therefore appears that the envisaged derogation from Article 14 (1) (d) is in fact the most appropriate solution in the specific circumstances.
(5) The derogation avoids the loss of VAT and will therefore not adversely affect the Communities' own resources from VAT,
HAS ADOPTED THIS DECISION:
Article 1
By way of derogation from Article 14 (1) (d) of Directive 77/388/EEC Denmark is authorised to apply VAT on the importation into Denmark of magazines, periodicals or the like printed in the EU and sent to private individuals in Denmark.
Article 2
This Decision shall apply until 31 December 2010.
Article 3
This Decision is addressed to Denmark.
Done at Brussels, […]
For the Council
The President
[1] OJ L 145, 13.6.1977, p. 1. Directive last amended by Directive 2004/7/EC (OJ L 27, 30.1.2004, p. 44).
[2] OJ C […], […], p. […].
[3] OJ C […], […], p. […].
[4] OJ C […], […], p. […].
