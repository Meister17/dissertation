COMMISSION DECISION
of 14 January 2000
on amending Decision 1999/246/EEC approving certain contingency plans for the control of classical swine fever
(Text with EEA relevance)
(2000/113/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 80/217/EEC of 22 January 1980 introducing Community measures for the control of classical swine fever(1), as last amended by Council Directive 93/384/EEC(2), and in particular Article 14b thereof,
Whereas:
(1) The criteria to be applied mutatis mutandis when drawing up contingency plan for the control of classical swine fever have been laid down in Commission Decision 91/42/EEC(3);
(2) Greece and Luxembourg have submitted for approval national contingency plans;
(3) After examination these plans fulfil the criteria laid down in Decision 91/42/EEC and permit the desired objective to be attained subject to an effective implementation;
(4) The Commission approved the contingency plans submitted by certain Member States with Decision 1999/246/EEC(4);
(5) Decision 1999/246/EEC must be amended to include the plans submitted by Greece and Luxembourg;
(6) The measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
The Annex to Decision 1999/246/EEC is replaced by the Annex to this Decision.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 14 January 2000.
For the Commission
David BYRNE
Member of the Commission
(1) OJ L 47, 21.1.1980, p. 11.
(2) OJ L 166, 8.7.1993, p. 34.
(3) OJ L 23, 29.1.1991, p. 29.
(4) OJ L 93, 8.4.1999, p. 24.
ANNEX
Belgium
Denmark
Germany
Greece
Spain
France
Ireland
Italy
Luxembourg
The Netherlands
Austria
Portugal
Finland
Sweden
United Kingdom
