DECISION OF THE EXECUTIVE COMMITTEE
of 15 December 1997
on the guiding principles for means of proof and indicative evidence within the framework of readmission agreements between Schengen States
(SCH/Com-ex (97)39 rev.)
THE EXECUTIVE COMMITTEE,
Having regard to Article 132 of the Convention implementing the Schengen Agreement,
Having regard to Article 23(4) of the abovementioned Convention,
HAS DECIDED AS FOLLOWS:
Document SCH/II-Read (97)3 rev. 7 on the guiding principles for means of proof and indicative evidence under readmission agreements between Schengen States, attached hereto, is hereby approved. The application of these principles is recommended as of the date of adoption of this Decision.
Vienna, 15 December 1997.
The President
K. Schlögl
Subject: Guiding principles for means of proof and indicative evidence under readmission agreements between Schengen States
SCH/II-Read (97)3 rev. 7
Whereas problems have arisen in practice when applying readmission agreements, notably with the means of proof establishing the illegal residence in or transit through the territory of the requested Contracting Party by foreign nationals;
The Contracting Parties have adopted the following guiding principles which may be of use to the Contracting Parties when applying future readmission agreements:
1. The following documents, inter alia, may be deemed to provide proof of residence or transit:
- an entry stamp affixed to the travel document by the requested Contracting Party,
- an exit stamp of a State adjacent to a Contracting Party, taking into account the travel route and date of the frontier crossing,
- an entry stamp affixed to a false or falsified travel document by the requested Contracting Party,
- travel tickets issued by name which can formally establish entry,
- fingerprints,
- a valid residence permit,
- a valid visa issued by the requested Contracting Party,
- an embarkation/disembarkation card showing the date of entry into the territory of the requested Contracting Party.
2. A presumption of residence or transit may be established, inter alia, by the following indicative evidence:
- statements by officials,
- statements by third parties,
- statements by the person to be transferred,
- an expired residence permit issued by the requested Contracting Party, whatever the type,
- an expired visa issued by the requested Contracting Party,
- documents issued by name in the territory of the requested Contracting Party,
- travel tickets,
- hotel bills,
- cards for access to public or private amenities in the Contracting Parties,
- appointment cards for doctors, dentists, etc.,
- data showing that the person to be transferred has used the services of a facilitator or travel agency.
3. In so far as the Schengen Partners take into account the means of proof listed under point 1 when concluding future readmission agreements, these shall provide conclusive proof of residence or transit. In principle, no further investigation shall be carried out. Evidence to the contrary shall, however, be admissible (e.g. showing a document to be falsified or forged).
4. In so far as the Schengen Partners take into account the evidence listed under point 2 when concluding future readmission agreements, such evidence shall establish a presumption of residence or transit. It is by nature rebuttable by evidence to the contrary.
