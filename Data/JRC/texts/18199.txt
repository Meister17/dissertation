Commission Regulation (EC) No 746/2004
of 22 April 2004
adapting certain regulations concerning organic production of agricultural products and indications referring thereto on agricultural products and foodstuffs by reason of the accession of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia to the European Union
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to the Treaty of Accession of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia, and in particular Article 2(3) thereof,
Having regard to the Act of Accession of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia, and in particular Article 57(2) thereof,
Whereas:
(1) Certain technical amendments are necessary in several Commission Regulations concerning organic production of agricultural products and indications referring thereto on agricultural products and foodstuffs in order to carry out the necessary adaptations by reason of accession of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia (hereinafter referred to as the new Member States) to the Union.
(2) Annex V, Part B, to Council Regulation (EEC) No 2092/91 of 24 June 1991 on organic production of agricultural products and indications referring thereto on agricultural products and foodstuffs(1) establishes the models of the Community logo as well as the indications to be inserted in that logo. Annex V, Part B.2 and B.3, should be completed in order to include the language versions of the new Member States.
(3) The Annex to Commission Regulation (EEC) No 94/92 of 14 January 1992 laying down detailed rules for implementing the arrangements for imports from third countries provided for in Regulation (EEC) No 2092/91 on organic production of agricultural products and indications referring thereto on agricultural products and foodstuffs(2) lays down the list of third countries referred to in Article 11(1)(a) of Regulation (EEC) No 2092/91. The references in that Annex to the Czech Republic and to Hungary should be deleted.
(4) According to the second paragraph of Article 7 of Commission Regulation (EC) No 1788/2001 of 7 September 2001 laying down detailed rules for implementing the provisions concerning the certificate of inspection for imports from third countries under Article 11 of Council Regulation (EEC) No 2092/91 on organic production of agricultural products and indications(3), Member States shall, before 1 April 2002, inform each other and the Commission on the measures they have taken for the purpose of implementing the system of certificates, in particular as concerns the competent authorities. That date should be adapted with regard to the new Member States, taking into account the necessity of ensuring that information concerning the competent bodies in the new Member States is available throughout the Community at the date of accession.
(5) According to Article 2 of Commission Regulation (EC) No 473/2002 of 15 March 2002 amending Annexes I, II and VI to Council Regulation (EEC) No 2092/91 on organic production of agricultural products and indications referring thereto on agricultural products and foodstuffs, and laying down detailed rules as regards the transmission of information on the use of copper compounds(4), Member States applying the derogation provided for the maximum levels of copper compounds shall, before 30 June 2002, inform the Commission and the other Member States about the measures taken to implement the derogation and shall, before 31 December 2004, provide a report on the implementation and the results of these measures. It is necessary to adapt these dates with regard to the new Member States, in order to give them sufficient time to provide the required information.
(6) Article 6 of Commission Regulation (EC) No 223/2003 of 5 February 2003 on labelling requirements related to the organic production method for feedingstuffs, compound feedingstuffs and feed materials and amending Council Regulation (EEC) No 2092/91(5) lays down the conditions under which trade marks bearing an indication referring to the organic production method may still be used during a transitional period in the labelling and advertising of feed products which do not comply with that Regulation. According to these conditions, the registration of the trade mark should have been applied for before 24 August 1999. It is necessary to adapt that date with regard to the new Member States.
(7) Regulations (EEC) No 2092/91, (EEC) No 94/92, (EC) No 1788/2001, (EC) No 473/2002 and (EC) No 223/2003 should therefore be amended accordingly,
HAS ADOPTED THIS REGULATION:
Article 1
In Annex V to Regulation (EEC) No 2092/91, Part B is amended as follows:
1. Part B.2 is replaced by the following: B.2 Models
>PIC FILE= "L_2004122EN.001201.TIF">
>PIC FILE= "L_2004122EN.001301.TIF">
2. ES
"B.3.1 AGRICULTURA ECOLÓGICA
CS: EKOLOGICKÉ ZEMEDELSTVÍ
DA: ØKOLOGISK JORDBRUG
DE: BIOLOGISCHE LANDWIRTSCHAFT OR ÖKOLOGISCHER LANDBAU
ET: MAHEPÕLLUMAJANDUS VÕI ÖKOLOOGILINE PÕLLUMAJANDUS
EL: ΒΙΟΛΟΓΙΚΗ ΓΕΩΡΓΙΑ
EN: ORGANIC FARMING
FR: AGRICULTURE BIOLOGIQUE
HU: ÖKOLÓGIAI GAZDÁLKODÁS
IT: AGRICOLTURA BIOLOGICA
LT: EKOLOGINIS ZEMES UKIS
LV: BIOLOGISKA LAUKSAIMNIECIBA
MT: AGRIKULTURA ORGANIKA
NL: BIOLOGISCHE LANDBOUW
PL: ROLNICTWO EKOLOGICZNE
PT: AGRICULTURA BIOLÓGICA
SK: EKOLOGICKÉ POLNOHOSPODÁRSTVO
SL: EKOLOSKO KMETIJSTVO
FI: LUONNONMUKAINEN MAATALOUSTUOTANTO
SV: EKOLOGISKT JORDBRUK
Part B.3.1 is replaced by the following: Single indications:"
Article 2
In the Annex to Regulation (EEC) No 94/1992, the entries on the Czech Republic and Hungary are deleted.
Article 3
In Article 7 of Regulation (EC) No 1788/2001, the following paragraph is added:"For the Czech Republic, Cyprus, Estonia, Hungary, Latvia, Lithuania, Malta, Poland, Slovakia and Slovenia, the date of information referred to in the second paragraph shall be 1 May 2004."
Article 4
In Article 2 of Regulation (EC) No 473/2002, the following paragraph is inserted after the first paragraph:"For the Czech Republic, Cyprus, Estonia, Hungary, Latvia, Lithuania, Malta, Poland, Slovakia and Slovenia, the date referred to in the first indent of the first paragraph shall be 1 August 2004 and the date referred to in the second indent of the first paragraph shall be 31 December 2005."
Article 5
In Article 6 of Regulation (EC) No 223/2003, the following paragraph is added:"For the Czech Republic, Cyprus, Estonia, Hungary, Latvia, Lithuania, Malta, Poland, Slovakia and Slovenia, the date of application referred to in point (a) of the first paragraph shall be 1 May 2004 at the latest."
Article 6
This Regulation shall enter into force subject to and on the date of the entry into force of the Treaty of Accession of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 22 April 2004.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 198, 22.7.1991, p. 1. Regulation as last amended by Commission Regulation (EC) No 2277/2003 (OJ L 336, 23.12.2003, p. 68).
(2) OJ L 11, 17.1.1992, p. 14. Regulation as last amended by Regulation (EC) No 2144/2003 (OJ L 322, 9.12.2003, p. 3).
(3) OJ L 243, 13.9.2001, p. 3. Regulation as last amended by Regulation (EC) No 1918/2002 (OJ L 289, 26.10.2002, p. 15).
(4) OJ L 75, 16.3.2002, p. 21.
(5) OJ L 31, 6.2.2003, p. 3.
