Brussels, 23.2.2005
SEC(2005) 258 final
Draft
DECISION OF THE EEA JOINT COMMITTEE
amending Protocol 31 to the EEA Agreement, on cooperation in specific fields outside the four freedoms
- Draft common position of the Community -(presented by the Commission)
EXPLANATORY MEMORANDUM
1. Protocol 31 to the EEA Agreement contains specific provisions on the cooperation between the Community and the EEA EFTA States outside the four freedoms.
2. The attached draft Decision of the EEA Joint Committee aims to modify Protocol 31 to extend cooperation in the field of research and technological development. It provides a framework for cooperation and sets out the modalities for participation of the EEA EFTA States in the Community programmes and actions as from 1 January 2005, related to the following budget line of the general EU 2005 budget:
- Budget line 08.14.01: Preparatory action for the enhancement of European security research (2005)
3. Article 1.3(b) of Council Regulation (EC) No 2894/94 concerning the arrangements for implementing the EEA Agreement envisages that the Council establish the Community position for such Decisions on a proposal from the Commission.
4. The draft decision of the EEA Joint Committee is submitted for the approval of the Council, after which the Commission will put forward the position of the Community in the EEA Joint Committee at the earliest possible occasion.
Draft
DECISION OF THE EEA JOINT COMMITTEE
amending Protocol 31 to the EEA Agreement, on cooperation in specific fields outside the four freedoms
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as ‘the Agreement’, and in particular Articles 86 and 98 thereof,
Whereas:
(1) Protocol 31 to the Agreement was amended by Decision of the EEA Joint Committee No …/… of …[1].
(2) It is appropriate to extend the cooperation of the Contracting Parties to the Agreement to include the preparatory action for the enhancement of European security research (2005).
(3) Protocol 31 to the Agreement should therefore be amended in order to allow for this cooperation to take place with effect from 1 January 2005,
HAS DECIDED AS FOLLOWS:
Article 1
Article 1 of Protocol 31 to the Agreement shall be amended as follows:
1. The text of paragraph 1 shall be replaced with the following:
‘1. The EFTA States shall, from 1 January 1994, participate in the implementation of the framework programmes of Community activities in the field of research and technological development referred to in paragraph 5 and from 1 January 2005 in activities referred to in paragraph 8, through participation in their specific programmes.’
2. The text of paragraph 2 shall be replaced with the following:
‘2. The EFTA States shall contribute financially to the activities referred to in paragraphs 5 and 8 in accordance with Article 82 (1) (a) of the Agreement.’
3. The text of paragraph 6 shall be replaced with the following:
‘6. Evaluation and major redirection of activities in the framework programmes of Community activities in the field of research and technological development referred to in paragraphs 5 and 8 shall be governed by the procedure referred to in Article 79 (3) of the Agreement.’
4. The following paragraph shall be inserted after paragraph 7:
‘8. The EFTA States shall, as from 1 January 2005, participate in the Community actions related to the following budget line, entered in the general budget of the European Union for the financial year 2005:
- Budget line 08.14.01: “Preparatory action for the enhancement of European security research (2005)”.’
Article 2
This Decision shall enter into force on the day following the last notification to the EEA Joint Committee under Article 103(1) of the Agreement*.
It shall apply from 1 January 2005.
Article 3
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union .
Done at Brussels,
For the EEA Joint Committee
The President
The Secretaries to the EEA Joint Committee
[1] OJ L …
* [No constitutional requirements indicated.] [Constitutional requirements indicated.]
