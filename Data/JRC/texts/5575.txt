Decision No 1/2005 of the Joint Committee on Agriculture set up by the Agreement between the European Community and the Swiss Confederation on trade in agricultural products
of 25 February 2005
concerning point B(9) of Appendix 1 to Annex 7
(2005/394/EC)
THE JOINT COMMITTEE,
Having regard to the Agreement between the European Community and the Swiss Confederation on trade in agricultural products, and in particular Article 11 thereof,
Whereas:
(1) This Agreement entered into force on 1 June 2002.
(2) Annex 7 is aimed at facilitating trade in wine-sector products between the Parties.
(3) Under Article 27(1) of Annex 7, the Working Group considers any matter which may arise in connection with the implementation of Annex 7 and, under Article 27(2) of Annex 7, may in particular put forward proposals to the Committee with a view to adapting and updating the Appendices to Annex 7.
(4) Point B(9) of Appendix 1 to Annex 7 sets out the accompanying document for wines imported from Switzerland in accordance with point B(9) of Appendix 1 to the original version of the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
Point B(9) of Appendix 1 to Annex 7 is hereby replaced by the text contained in the Annex to this Decision.
Article 2
This Decision shall apply from 1 October 2004.
Done at Brussels, 25 February 2005.
For the Joint Committee on Agriculture
The Chair, Head of the Swiss Delegation
Christian Häberli
For the European Community,
The Head of Unit AGRI AI/2
Aldo Longo
Secretariat of the Committee
The Secretary
Remigi Winzap
--------------------------------------------------
ANNEX
--------------------------------------------------
