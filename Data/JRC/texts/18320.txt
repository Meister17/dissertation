COUNCIL DIRECTIVE of 21 January 1980 concerning the mutual recognition of diplomas, certificates and other evidence of formal qualifications in midwifery and including measures to facilitate the effective exercise of the right of establishment and freedom to provide services (80/154/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Articles 49, 57 and 66 thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas, pursuant to the Treaty, all discriminatory treatment based on nationality with regard to establishment and provision of services is prohibited as from the end of the transitional period ; whereas the principle of such treatment based on nationality applies in particular to the grant of any authorization required to practise as a midwife and also to the registration with or membership of professional organizations or bodies;
Whereas it nevertheless seems desirable that certain provisions be introduced to facilitate the effective exercise of the right of establishment and freedom to provide services in respect of the activities of midwives;
Whereas, pursuant to the Treaty, the Member States are required not to grant any form of aid likely to distort the conditions of establishment;
Whereas Article 57 (1) of the Treaty provides that Directives be issued for mutual recognition of diplomas, certificates and other evidence of formal qualifications;
Whereas it would appear advisable that, contemporaneously with the mutual recognition of diplomas, provision should be made for coordinating the conditions governing the training of midwives ; whereas such coordination is the subject of Directive 80/155/EEC (4);
Whereas in the Member States the law makes the right to take up and pursue the activities of midwife dependent upon the possession of a midwifery diploma;
Whereas, with regard to the possession of a formal certificate of training, since a Directive on the mutual recognition of diplomas does not necessarily imply equivalence in the training covered by such diplomas, the use of such qualifications should be authorized only in the language of the Member State of origin or of the Member State from which the foreign national comes;
Whereas, to facilitate the application of this Directive by the national authorities, Member States may prescribe that, in addition to formal certificates of training, the person who satisfies the conditions of (1)OJ No C 18, 12.2.1970, p. 1. (2)OJ No C 101, 4.8.1970, p. 26. (3)OJ No C 146, 11.12.1970, p. 17. (4)See page 8 of this Official Journal. training required by this Directive must provide a certificate from the competent authorities of his country of origin or of the country from which he comes stating that these certificates of training are those covered by the Directive;
Whereas, in the case of the provision of services, the requirement of registration with or membership of professional organizations or bodies, since it is related to the fixed and permanent nature of the activity pursued in the host country, would undoubtedly constitute an obstacle to the person wishing to provide the service, by reason of the temporary nature of his activity ; whereas this requirement should therefore be abolished ; whereas however, in this event, control over professional discipline, which is the responsibility of these professional organizations or bodies, should be guaranteed ; whereas, to this end, it should be provided, subject to the application of Article 62 of the Treaty, that the person concerned may be required to submit to the competent authority of the host Member State particulars relating to the provision of services;
Whereas, with regard to the requirements relating to good character and good repute, a distinction should be drawn between the requirements to be satisfied on first taking up the profession and those to be satisfied to practise it;
Whereas, as far as the activities of employed midwives are concerned, Council Regulation (EEC) No 1612/68 of 15 October 1968 on freedom of movement for workers within the Community (1) lays down no specific provisions relating to good character or good repute, professional discipline or use of title for the professions covered ; whereas, depending on the individual Member State, such rules are or may be applicable both to employed and self-employed persons ; whereas the activities of midwives are subject in all Member States to possession of a diploma, certificate or other evidence of formal qualification in midwifery ; whereas such activities are pursued by both employed and self-employed persons, or by the same persons in both capacities in the course of their professional career ; whereas, in order to encourage as far as possible the free movement of those professional persons within the Community, it therefore appears necessary to extend this Directive to employed midwives,
HAS ADOPTED THIS DIRECTIVE:
CHAPTER I
SCOPE
Article 1
1. This Directive shall apply to the activities of midwives as defined by each Member State, without prejudice to Article 4 of Directive 80/155/EEC, and pursued under the following professional titles:
in the Federal Republic of Germany: - "Hebamme";
in Belgium: - "accoucheuse/vroedvrouw";
in Denmark: - "jordemoder";
in France: - "sage-femme";
in Ireland: - midwife;
in Italy: - "ostetrica";
in Luxembourg: - "sage-femme";
in the Netherlands - "verloskundige";
in the United Kingdom: - midwife.
CHAPTER II DIPLOMAS, CERTIFICATES AND OTHER EVIDENCE OF FORMAL QUALIFICATIONS IN MIDWIFERY
Article 2
1. Each Member State shall recognize the diplomas, certificates and other evidence of formal qualifications in midwifery awarded to nationals of Member States by the other Member States and listed in Article 3 which satisfy the minimum programme mentioned in Article 1 (1), (3) and (4) of Directive 80/155/EEC and comply with one of the following conditions: - full-time training in midwifery lasting at least three years: - either subject to possession of a diploma, certificate or other evidence of formal qualifications giving right of admittance to university or higher-education establishments or, failing this, attesting an equivalent level of knowledge,
- or followed by professional practice for which the certificate referred to in Article 4 of this Directive is issued, (1)OJ No L 257, 19.10.1968, p. 2.
- full-time training in midwifery lasting at least two years or 3 600 hours subject to possession of a diploma, certificate or other evidence of formal qualifications as a nurse responsible for general care, referred to in Article 3 of Directive 77/452/EEC (1),
- full-time training in midwifery lasting at least 18 months or 3 000 hours subject to possession of a diploma, certificate or other evidence of formal qualifications as a nurse responsible for general care referred to in Article 3 of Directive 77/452/EEC and followed by professional practice for which the certificate referred to in Article 4 of this Directive is issued.
2. Each Member State shall, as far as the right to take up and pursue the activities of midwives in a self-employed capacity is concerned, give diplomas, certificates and other evidence of formal qualifications recognized by it the same effect in its territory as those which the Member State itself awards.
Article 3
The diplomas, certificates and other evidence of formal qualifications referred to in Article 2 are the following: (a) in the Federal Republic of Germany: - the "Hebammenprüfungszeugnis" awarded by the State-appointed examining board,
- the certificates issued by the competent authorities of the Federal Republic of Germany, stating that the diplomas awarded after 8 May 1945 by the competent authorities of the German Democratic Republic are recognized as equivalent to those listed in the first indent;
(b) in Belgium:
the "diplôme d'accoucheuse/vroedvrouwdiploma" awarded by schools set up or approved by the State or by the "Jury central";
(c) in Denmark:
the "bevis for bestået jordemodereksamen" awarded by "Danmarks Jordemoderskole";
(d) in France:
the "diplôme de sage-femme" awarded by the State;
(e) in Ireland:
the certificate in midwifery awarded by "An Bord Altranais";
(f) in Italy:
the "diploma d'ostetrica" awarded by schools approved by the State;
(g) in Luxembourg:
the "diplôme de sage-femme" awarded by the Minister for Health following a decision by the examining board;
(h) in the Netherlands:
the "vroedvrouwdiploma" awarded by the examining body designated by the State;
(i) in the United Kingdom:
the certificate of admission to the Roll of Midwives, awarded in England and Wales by the Central Midwives Board for England and Wales, in Scotland by the Central Midwives Board for Scotland, and in Northern Ireland by the Northern Ireland Council for Nurses and Midwives.
Article 4
The certificate provided for in Article 2 shall be issued by the competent authorities of the Member State of origin or of the Member State from which the foreign national comes. It shall certify that the holder has satisfactorily, after qualifying as a midwife, carried out all the activities of a midwife in a hospital or other health establishment approved for this purpose, for a period fixed as follows: - two years in the case provided for in the second sub-indent of the first indent of Article 2 (1),
- one year in the case provided for in the third indent of Article 2 (1).
CHAPTER III
EXISTING CIRCUMSTANCES
Article 5
1. In the case of nationals of Member States whose diplomas, certificates and other evidence of formal qualifications do not satisfy all the minimum training requirements laid down in Article 1 of Directive 80/155/EEC, each Member State shall recognize as sufficient evidence the diplomas, certificates and other evidence of the formal qualifications of midwives awarded by those Member States prior to and during a period of not more than six years after the notification of this Directive, accompanied by a certificate stating that those nationals have effectively and lawfully been engaged in the activities in question for at least three years during the five years prior to the date of issue of the certificate. (1)OJ No L 176, 15.7.1977, p. 1.
2. In the case of nationals of Member States whose diplomas, certificates and other evidence of formal qualifications satisfy all the minimum training requirements laid down in Article 1 of Directive 80/155/EEC, but, by virtue of Article 2 of the present Directive, need be recognized only if they are accompanied by the certificate of professional practice referred to in Article 4, each Member State shall recognize as sufficient evidence the diplomas, certificates and other evidence of formal qualifications of midwives awarded by those Member States prior to the entry into force of the present Directive, accompanied by a certificate stating that those nationals have effectively and lawfully been engaged in the activities in question for at least two years during the five years prior to the date of issue of the certificate.
CHAPTER IV
USE OF ACADEMIC TITLE
Article 6
1. Without prejudice to Article 15, host Member States shall ensure that nationals of Member States who fulfil the conditions laid down in Articles 2 and 5 have the right to use the lawful academic title, inasmuch as it is not identical to the professional title, or, where appropriate, the abbreviation thereof of their Member State of origin or of the Member State from which they come, in the language or languages of that State. Host Member States may require this title to be followed by the name and location of the establishment or examining board which awarded it.
2. If the academic title used in the Member State of origin, or in the Member State from which a foreign national comes, can be confused in the host Member State with a title requiring, in that State, additional training which the person concerned has not undergone, the host Member State may require such a person to use the title employed in the Member State of origin or the Member State from which he comes in a suitable form to be indicated by the host Member State.
CHAPTER V
PROVISIONS TO FACILITATE THE EFFECTIVE EXERCISE OF THE RIGHT OF ESTABLISHMENT AND FREEDOM TO PROVIDE SERVICES IN RESPECT OF THE ACTIVITIES OF MIDWIVES
A. Provisions specifically relating to the right of establishment
Article 7
1. A host Member State which requires of its nationals proof of good character or good repute when they take up for the first time any activity referred to in Article 1 shall accept as sufficient evidence, in respect of nationals of other Member States, a certificate issued by a competent authority in the Member State of origin or in the Member State from which the foreign national comes, attesting that the requirements of the Member State as to good character or good repute for taking up the activity in question have been met.
2. Where the Member State of origin or the Member State from which the foreign national comes does not require proof of good character or good repute of persons wishing to take up the activity in question for the first time, the host Member State may require of nationals of the Member State of origin or of the Member State from which the foreign national comes an extract from the judicial record or, failing this, an equivalent document issued by a competent authority in the Member State of origin or the Member State from which the foreign national comes.
3. If the host Member State has detailed knowledge of a serious matter which occurred outside its territory prior to the establishment of the person concerned in that State and which is likely to affect the taking up within its territory of the activity concerned, it may inform the Member State of origin or the Member State from which the foreign national comes.
The Member State of origin or the Member State from which the foreign national comes shall verify the accuracy of the facts if they are likely to affect in that Member State the taking up of the activity in question. The authorities in that State shall decide on the nature and extent of the investigation to be made and shall inform the host Member State of any consequential action which they take with regard to the certificates or documents they have issued.
4. Member States shall ensure the confidentiality of the information which is forwarded.
Article 8
1. Where, in a host Member State, provisions laid down by law, regulation or administrative action are in force laying down requirements as to good character or good repute, including provisions for disciplinary action in respect of serious professional misconduct or conviction for criminal offences and relating to the pursuit of any of the activities referred to in Article 1, the Member State of origin or the Member State from which the foreign national comes shall forward to the host Member State all necessary information regarding measures or disciplinary action of a professional or administrative nature taken in respect of the person concerned, or criminal penalties imposed on him when pursuing his profession in the Member State of origin or in the Member State from which he came.
2. If the host Member State has detailed knowledge of a serious matter which has occurred prior to the establishment of the person concerned in that State outside its territory and which is likely to affect the pursuit within its territory of the activity concerned, it may inform the Member State of origin or the Member State from which the foreign national comes.
The Member State of origin or the Member State from which the foreign national comes shall verify the accuracy of the facts if they are likely to affect in that Member State the pursuit of the activity in question. The authorities in that State shall decide on the nature and extent of the investigation to be made and shall inform the host Member State of any consequential action which they take with regard to the information they have forwarded in accordance with paragraph 1.
3. Member States shall ensure the confidentiality of the information which is forwarded.
Article 9
Where a host Member State requires of its own nationals wishing to take up or pursue any of the activities referred to in Article 1 a certificate of physical or mental health, that State shall accept as sufficient evidence thereof the presentation of the document required in the Member State of origin or in the Member State from which the foreign national comes.
Where the Member State of origin or the Member State from which the foreign national comes does not impose any requirements of this nature on those wishing to take up or pursue the activity in question, the host Member State shall accept from such national a certificate issued by a competent authority in that State corresponding to the certificates issued in the host Member State.
Article 10
Documents issued in accordance with Articles 7, 8 and 9 may not be presented more than three months after their date of issue.
Article 11
1. The procedure for authorizing the person concerned to take up any activity referred to in Article 1, pursuant to Articles 7, 8 and 9 must be completed as soon as possible and not later than three months after presentation of all the documents relating to such person, without prejudice to delays resulting from any appeal that may be made upon the termination of this procedure.
2. In the cases referred to in Articles 7 (3) and 8 (2), a request for re-examination shall suspend the period stipulated in paragraph 1.
The Member State consulted shall give its reply within three months.
On receipt of the reply or at the end of the period the host Member State shall continue with the procedure referred to in paragraph 1.
Article 12
Where a host Member State requires its own nationals wishing to take up or pursue one of the activities referred to in Article 1 to take an oath or make a solemn declaration and where the form of such oath or declaration cannot be used by nationals of other Member States, that Member State shall ensure that an appropriate and equivalent form of oath or declaration is offered to the person concerned.
B. Provisions specifically relating to the provision of services
Article 13
1. Where a Member State requires of its own nationals wishing to take up or pursue any of the activities referred to in Article 1 an authorization, or membership of or registration with a professional organization or body, that Member State shall in the case of the provision of services exempt the nationals of Member States from that requirement.
The person concerned shall provide services with the same rights and obligations as the nationals of the host Member State ; in particular he shall be subject to the rules of conduct of a professional or administrative nature which apply in that Member State.
Where a host Member State adopts a measure pursuant to the second subparagraph or becomes aware of facts which run counter to these provisions, it shall forthwith inform the Member State where the person concerned is established.
2. The host Member State may require the person concerned to make a prior declaration to the competent authorities concerning the provision of his services where they involve a temporary stay in its territory.
In urgent cases this declaration may be made as soon as possible after the services have been provided.
3. Pursuant to paragraphs 1 and 2, the host Member State may require the person concerned to supply one or more documents containing the following particulars: - the declaration referred to in paragraph 2,
- a certificate stating that the person concerned is lawfully pursuing the activities in question in the Member State where he is established,
- a certificate that the person concerned holds one or other of the diplomas, certificates of other evidence of formal qualification appropriate for the provision of the services in question and referred to in this Directive.
4. The document or documents specified in paragraph 3 may not be produced more than 12 months after their date of issue.
5. Where a Member State temporarily or permanently deprives, in whole or in part, one of its nationals or a national of another Member State established in its territory of the right to pursue one of the activities referred to in Article 1, it shall, as appropriate, ensure the temporary or permanent withdrawal of the certificate referred to in the second indent of paragraph 3.
Article 14
Where registration with a public social security body is required in a host Member State for the settlement with insurance bodies of accounts relating to services rendered to persons insured under social security schemes, that Member State shall exempt nationals of Member States established in another Member State from this requirement in cases of provision of services entailing travel on the part of the person concerned.
However, the persons concerned shall supply information to this body in advance, or, in urgent cases, subsequently, concerning the services provided.
C. Provisions common to the right of establishment and freedom to provide services
Article 15
Where in a host Member State the use of the professional title relating to one of the activities referred to in Article 1 is subject to rules, nationals of other Member States who fulfil the conditions laid down in Articles 2 and 5 shall use the professional title of the host Member State which, in that State, corresponds to those conditions of qualification, and shall use the abbreviated title.
Article 16
1. Member States shall take the necessary measures to enable the persons concerned to obtain information on the health and social security laws and, where applicable, on the professional ethics of the host Member State.
For this purpose, Member States may set up information centres from which such persons may obtain the necessary information. In the case of establishment, the host Member States may require the persons concerned to contact these centres.
2. Member States may set up the centres referred to in paragraph 1 within the competent authorities and bodies which they must designate within the period laid down in Article 20 (1).
3. Member States shall see to it that, where appropriate, the persons concerned acquire, in their own interest and in that of their patients, the linguistic knowledge necessary for the exercise of their profession in the host Member State.
CHAPTER VI
FINAL PROVISIONS
Article 17
In the event of justified doubts, the host Member State may require of the competent authorities of another Member State confirmation of the authenticity of the diplomas, certificates and other evidence of formal qualifications issued in that other Member State and referred to in Chapters II and III, and also confirmation of the fact that the person concerned has fulfilled all the training requirements laid down in Directive 80/155/EEC.
Article 18
Within the time limit laid down in Article 20 (1) Member States shall designate the authorities and bodies competent to issue or receive the diplomas, certificates and other evidence of formal qualifications as well as the documents and information referred to in this Directive, and shall forthwith inform the other Member States and the Commission thereof.
Article 19
This Directive shall also apply to nationals of Member States who, in accordance with Regulation (EEC) No 1612/68, are pursuing or will pursue as employed persons one of the activities referred to in Article 1.
Article 20
1. Member States shall bring into force the measures necessary to comply with this Directive within three years of its notification and shall forthwith inform the Commission thereof.
2. Member States shall communicate to the Commission the texts of the main provisions of national law which they adopt in the field covered by this Directive.
Article 21
Where a Member State encounters major difficulties in certain fields when applying this Directive, the Commission shall examine these difficulties in conjunction with that State and shall request the opinion of the Committee of Senior Officials on Public Health set up under Decision 75/365/EEC (1), as last amended by Decision 80/157/EEC (2).
Where necessary, the Commission shall submit appropriate proposals to the Council.
Article 22
This Directive is addressed to the Member States.
Done at Brussels, 21 January 1980.
For the Council
The President
G. MARCORA (1)OJ No L 167, 30.6.1975, p. 19. (2)See page 15 of this Official Journal.
