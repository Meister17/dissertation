Reference for a preliminary ruling from the Commissione tributaria provinciale di Roma lodged on 28 February 2006 — Diagram APS Applicazioni Prodotti Software
v Agenzia Entrate Ufficio Roma 6
Referring court
Commissione tributaria provinciale di Roma (Italy)
Parties to the main proceedings
Applicant: Diagram APS Applicazioni Prodotti Software
Defendant: Agenzia Entrate Ufficio Roma 6
Question(s) referred
The Commissione Tributaria provinciale di Roma (Provincial Tax Court Rome) has referred the following question to the Court of Justice for a preliminary ruling:
Must Article 33 of Directive 77/388/EEC [1] (as amended by Directive 91/680/EEC [2]) be interpreted as meaning that net output value arising from regular engagement in independent activities involving the production or exchange of goods or the rendering of services cannot be made liable to IRAP (Imposta Regionale sulle Attività Produttive — Regional tax on businesses)?
[1] OJ L 145, 13/06/1977, p. 1.
[2] OJ L 376, 31/12/1991, p. 1.
--------------------------------------------------
