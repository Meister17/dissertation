Commission Regulation (EC) No 701/2003
of 16 April 2003
laying down detailed rules for the application of Council Regulation (EC) No 2286/2002 as regards the arrangements applicable to imports of certain poultrymeat and egg products originating in the African, Caribbean and Pacific States (ACP States)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2771/75 of 29 October 1975 on the common organisation of the market in eggs(1), as last amended by Commission Regulation (EC) No 493/2002(2), and in particular Article 3 thereof,
Having regard to Council Regulation (EEC) No 2777/75 of 29 October 1975 on the common organisation of the market in poultrymeat(3), as last amended by Regulation (EC) No 493/2002, and in particular Article 3 thereof,
Having regard to Council Regulation (EC) No 2286/2002 of 10 December 2002 on the arrangements applicable to agricultural products and goods resulting from the processing of agricultural products originating in the African, Caribbean and Pacific States (ACP States) and repealing Regulation (EC) No 1706/98(4), and in particular Article 5 thereof,
Whereas:
(1) Regulation (EC) No 2286/2002 implements the amendments to the arrangements for imports from the ACP States made as a result of the ACP-EC Partnership Agreement signed in Cotonou on 23 June 2000(5). Article 1(3) of that Regulation introduces general arrangements for reducing customs duties for the products in Annex I thereto and specific arrangements for reducing customs duties, within the framework of the tariff quotas, for certain products in Annex II thereto.
(2) As a result of these new import arrangements, detailed rules of application should be laid down for the issue of import licences for products qualifying for reduced duties. Commission Regulation (EC) No 704/1999 of 31 March 1999 laying down detailed rules for the application of the arrangements for imports of eggs and poultrymeat products originating in the African, Caribbean and Pacific States (ACP States) and repealing Regulation (EEC) No 903/90(6), as amended by Regulation (EC) No 1043/2001(7), should therefore be repealed.
(3) For the purposes of managing the tariff import quotas, the general rules laid down by Commission Regulation (EC) No 1291/2000 of 9 June 2000 laying down common detailed rules for the application of the system of import and export licences and advance fixing certificates for agricultural products(8), as last amended by Regulation (EC) No 325/2003(9), should be applied where this Regulation does not lay down specific rules.
(4) In order to ensure proper administration of the quotas, a security should be required for applications for import licences and certain conditions should be laid down as regards applicants themselves. The quotas should also be staggered over the year and the term of validity of licences should be specified.
(5) To allow optimum management of the tariff quota, this Regulation should apply from 1 January 2003.
(6) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Poultrymeat and Eggs,
HAS ADOPTED THIS REGULATION:
Article 1
All imports into the Community under Regulation (EC) No 2286/2002 of products covered by CN codes listed in Annex I to this Regulation shall qualify for reduced customs duties on presentation of an import licence.
Licences shall be issued under the conditions laid down in this Regulation and within the limit of the quotas laid down in Annex II to Regulation (EC) No 2286/2002.
Article 2
The annual tariff quota referred to in Part B of Annex I shall be staggered as follows:
- 50 % in the period 1 January to 30 June,
- 50 % in the period 1 July to 31 December.
Article 3
1. Applicants for import licences for products referred to in Annex I shall be natural or legal persons who, at the time when applications are submitted, can prove to the satisfaction of the competent authorities of the Member States that they have been active in trade with third countries in the poultrymeat sector for at least the preceding 12 months.
However, retail establishments or restaurants selling their products to final consumers shall be excluded from the reduction in customs duties.
2. Import licence applications may mention only one of the quota numbers referred to in Annex I. They may involve several products covered by different CN codes. In such cases, all the CN codes shall be indicated in box 16 and their description in box 15 of licence applications and licences.
Applications must be for a minimum of one tonne and a maximum of 50 % of the quantity available for the quota concerned and the period specified in Article 2.
Article 4
1. Box 8 of licence applications and licences shall indicate the country of origin. Licences shall entail an obligation to import from the country indicated.
2. Box 20 of licence applications and licences shall carry one of the following entries:
- Producto ACP - Reglamentos (CE) n° 2286/2002 y (CE) n° 701/2003
- AVS-produkt - forordning (EF) nr. 2286/2002 og (EF) nr. 701/2003
- AKP-Erzeugnis - Verordnungen (EG) Nr. 2286/2002 und (EG) Nr. 701/2003
- Προϊόν ΑΚΕ - Κανονισμοί (ΕΚ) αριθ. 2286/2002 και (ΕΚ) αριθ. 701/2003
- ACP product - Regulations (EC) No 2286/2002 and (EC) No 701/2003
- Produit ACP - règlements (CE) n° 2286/2002 et (CE) n° 701/2003
- Prodotto ACP - regolamenti (CE) n. 2286/2002 e (CE) n. 701/2003
- ACS-product - Verordeningen (EG) nr. 2286/2002 en (EG) nr. 701/2003
- Produto ACP - Regulamentos (CE) n.o 2286/2002 e (CE) n.o 701/2003
- AKT-tuote - asetukset (EY) N:o 2286/2002 ja (EY) N:o 701/2003
- AVS-produkt - förordningarna (EG) nr 2286/2002 och (EG) nr 701/2003.
3. Box 24 of licences shall carry one of the following entries:
- Reducción del derecho de aduana en virtud del Reglamento (CE) n° 701/2003
- Toldnedsættelse, jf. forordning (EF) nr. 701/2003
- Ermäßigung des Zollsatzes gemäß der Verordnung (EG) Nr. 701/2003
- Μείωση του δασμού όπως προβλέπεται στον κανονισμό (ΕΚ) αριθ. 701/2003
- Customs duty reduction as provided for in Regulation (EC) No 701/2003
- Réduction du droit de douane comme prévu au règlement (CE) n° 701/2003
- Riduzione del dazio doganale a norma del regolamento (CE) n. 701/2003
- Douanerecht verlaagd overeenkomstig Verordening (EG) nr. 701/2003
- Redução do direito aduaneiro conforme previsto no Regulamento (CE) n.o 701/2003
- Tullialennus, josta on säädetty asetuksessa (EY) N:o 701/2003
- Nedsättning av tullavgiften enligt förordning (EG) nr 701/2003.
Article 5
1. Licence applications may only be lodged during the first seven days of the month preceding each period specified in Article 2. They must be lodged with the competent authority of the Member State where the applicant is established or has his or her registered place of business.
2. Applications shall be admissible only where applicants declare in writing that they have not lodged and will not be lodging, for the period in question, other applications for products of the same quota in the Member State where the application is lodged or another Member State. If an applicant lodges more than one application for products of one quota none of the applications shall be valid.
3. The Member States shall notify the Commission no later than the third working day following the end of the application submission period of applications lodged for each of the products of the group in question. Such notification shall comprise a list of applicants and of quantities applied for by quota.
All notifications shall be made by fax or by electronic means using the model shown in Annex II in cases where no applications have been made ("nil" returns) or the models shown in Annexes II and III in cases where applications have been made.
4. The Commission shall decide to what extent quantities may be awarded in respect of applications.
If quantities in respect of which licences have been applied for exceed the quantities available, the Commission shall fix a single percentage reducing the quantities applied for.
If the overall quantity covered by applications is less than the quantity available, the Commission shall calculate the quantity remaining, which shall be added to the quantity available in respect of the following period in the same year.
5. Licences shall be issued as soon as possible subject to the Commissions decision regarding acceptance of the applications.
6. The Member States shall communicate to the Commission, before the end of the fourth month following each annual period specified in Article 2, the quantities actually imported under this Regulation in that period.
All notifications, including notifications that there have been no imports, shall be made using the model shown in Annex IV.
Article 6
1. Import licences shall be valid for 180 days from the date of actual issue in accordance with Article 23(2) of Regulation (EC) No 1291/2000.
However, licences shall not be valid beyond 31 December of the year of issue.
2. Import licences issued pursuant to this Regulation shall not be transferable.
Article 7
A security of EUR 20 per 100 kilograms shall be lodged for import licence applications for all products.
Article 8
Import under the arrangements for a reduction in customs duties provided for in this Regulation may take place only if the origin of the products concerned is certified by the competent authorities of the exporting countries in accordance with the rules of origin applicable to the products in question pursuant to Protocol 1 to the ACP-EC Partnership Agreement signed in Cotonou on 23 June 2000.
Article 9
Unless this Regulation provides otherwise, Regulation (EC) No 1291/2000 shall apply.
Article 10
Regulation (EC) No 704/1999 is hereby repealed.
Article 11
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Union.
It shall apply from 1 January 2003.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 16 April 2003.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 282, 1.11.1975, p. 49.
(2) OJ L 77, 20.3.2002, p. 7.
(3) OJ L 282, 1.11.1975, p. 77.
(4) OJ L 348, 21.12.2002, p. 5.
(5) OJ L 317, 15.12.2000, p. 3.
(6) OJ L 89, 1.4.1999, p. 29.
(7) OJ L 145, 31.5.2001, p. 24.
(8) OJ L 152, 24.6.2000, p. 1.
(9) OJ L 47, 21.2.2003, p. 21.
ANNEX I
A. Products referred to in Article 1(3) and Annex I to Regulation (EC) No 2286/2002 qualifying for a reduction in the out-of-quota customs duty
>TABLE>
B. Products referred to in Article 1(3) and Annex II to Regulation (EC) No 2286/2002 qualifying for a reduction in customs duty under a quota
>TABLE>
ANNEX II
Regulation (EC) No 701/2003 - ACP imports
>PIC FILE= "L_2003099EN.003602.TIF">
ANNEX III
Regulation (EC) No 701/2003 - ACP imports
>PIC FILE= "L_2003099EN.003702.TIF">
ANNEX IV
Communication of quantities actually imported
>PIC FILE= "L_2003099EN.003802.TIF">
