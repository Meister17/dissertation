COUNCIL REGULATION (EEC) No 3949/92 of 21 December 1992 relating to the organization of a survey of labour costs in industry and the services sector
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 213 thereof,
Having regard to the proposal from the Commission,
Whereas, in order to carry out the tasks assigned to it by the Treaty, in particular those set out in Articles 2, 3, 117, 118, 122 and 123 thereof, the Commission must be kept informed of labour costs and workers' incomes in the Member States;
Whereas the statistical information available in each Member State does not provide a valid basis for comparisons, in particular because of the differences between the laws, regulations and administrative practices of the Member States, and whereas surveys must therefore be carried out and the results processed on the basis of uniform definitions and methods;
Whereas the best method of assessing the level, composition and trends of both labour costs and of workers' incomes is to carry out specific surveys, as was most recently done in 1989 in implementation of Council Regulation (EEC) No 1612/88 of 9 June 1988 relating to the organization of a survey of labour costs in industry, wholesale and retail distribution, banking and insurance (1), on the basis of accounting data relating to 1988;
Whereas, because of the major changes in the level and structure of expenditure by undertakings on wages and related employers' contributions, a new survey must be carried out based on accounting data for 1992 in industry, trade, banking and insurance, in order to bring up to date the results of the previous survey;
Whereas, because of the changes in the economic structures and the unemployment situation in the Member States the range of economic activities covered must be extended, particularly in the services sector;
Whereas, because of the size of the field covered, the survey must be based on a sample in order to avoid placing an excessive burden on the undertakings and the budgets of the European Communities and the Member States,
HAS ADOPTED THIS REGULATION:
Article 1
As part of its periodic surveys on labour costs and workers' incomes, the Commission shall conduct a survey on labour costs in industry and certain services sectors in 1993 on the basis of accounting data relating to 1992.
Article 2
1. The survey shall cover undertakings or local units with at least 10 employees carrying out the activities defined in sections C, D, E, F, G, H and K, divisions 65 and 66, and group 63.3 of the Statistical Classification of Economic Activities within the European Communities, NACE (Rev. 1), taking account of the special provisions set out in the Annex to this Regulation.
2. The survey shall be carried out by sampling.
Article 3
Employers shall, in respect of the undertakings or local units in the sample, provide the information needed to determine labour costs on the basis of accounting data for 1992 under the conditions set out below.
Article 4
The survey shall cover:
1. wage costs, including bonuses and allowances, and all incidental expenditure, including in particular employers' contributions to social security and supplementary schemes and other social payments, including the cost of vocational training and any taxes and subsidies directly related to labour costs;
2. the total staff employed by the undertakings or local units; and
3. working hours.
Article 5
1. The information shall be collected by the statistical offices of the Member States, which shall draw up appropriate questionnaires.
In cooperation with those offices, the Commission shall determine the list of characteristics and definitions to be used for the surveys.
The Commission shall also, under the same conditions, stipulate the starting and closing dates for the survey and deadlines for replying to the questionnaires.
2. Persons required to supply information shall reply to the questionnaires truthfully, completely and within the time limits set.
Article 6
1. The statistical offices of the Member States shall process the replies to the questionnaires.
After verification, and in accordance with the utilization programme defined by the Commission, they shall forward the results of the survey, including the data declared confidential by the Member States pursuant to domestic legislation or practice concerning statistical confidentiality, in accordance with the provisions of Council Regulation (Euratom, EEC) No 1588/90 of 11 June 1990 on the transmission of data subject to statistical confidentiality to the Statistical Office of the European Communities (1). The said Regulation governs the confidential treatment of information.
2. The results shall be broken down by sector of economic activity according to NACE (Rev. 1), by region and by size category of undertaking or local unit.
Article 7
Individual items of information supplied for purposes of the survey may be used for statistical purposes only.
They may not be used for tax or other purposes or be communicated to third parties.
Article 8
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 21 December 1992.
For the Council
The President
D. HURD
(1) OJ No L 145, 11. 6. 1988, p. 1.
(1) OJ No L 151, 15. 6. 1990, p. 1.
ANNEX
SPECIAL PROVISIONS
(Article 2 (1))
I. Exceptions to the scope of the survey
1. For all the Member States: class 65.11.
2. For Germany: section H, divisions 50, 70 and 71, groups 51.1 and 63.3 and class 51.57.
Divisions 72, 73 and 74 will be excluded for the territory of the former German Democratic Republic, including East Berlin.
3. For Greece: section F, group 51.1 and class 51.57.
4. For Ireland: section H.
II. More detailed information
Member States may provide for the supply of more detailed information, for example by making a distinction between manual and non-manual workers or by covering units with fewer than 10 employees.
III. Use of a special nomenclature
In agreement with the Commission, a Member State may forward the results of the survey on the basis of the NACE classification (version 70).
