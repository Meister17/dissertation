Commission Decision
of 2 August 2006
amending Decision 2001/844/EC, ECSC, Euratom
(2006/548/EC, Euratom)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community, and in particular Article 218(2) thereof,
Having regard to the Treaty establishing the European Atomic Energy Community, and in particular Article 131 thereof,
Having regard to the Treaty on European Union, and in particular Articles 28(1) and 41(1) thereof,
Whereas:
(1) In accordance with Article 2(1) of the Commission's provisions on security as set out in the Annex to Commission Decision 2001/844/EC, ECSC, Euratom [1], the Member of the Commission responsible for security matters is to take appropriate measures to ensure that, when handling EU classified information, the Commission's rules on security are respected within the Commission, and, inter alia, by contractors external to the Commission.
(2) Article 2(2) of the Commission provisions on security states that Member States, other institutions, bodies, offices and agencies established by virtue or on the basis of the Treaties are to be allowed to receive EU classified information on the condition that they ensure that strictly equivalent rules are respected within their services and premises, inter alia, by Member State's external contractors.
(3) The Commission provisions on security do not at present include elements on how their basic principles and minimum standards should apply where the Commission confers on external entities, by contract or grant agreement, tasks involving, entailing and/or containing EU classified information.
(4) It is therefore necessary to insert specific common minimum standards in that regard in the Commission's provisions on security and in the rules on security annexed thereto.
(5) These common minimum standards should also be complied with by Member States, for measures to be taken in accordance with national arrangements, where they confer by contract or grant agreement, tasks involving, entailing and/or containing EU classified information on the external entities referred to in Article 2(2) of the Commission provisions on security.
(6) These common minimum standards should apply without prejudice to other relevant acts, in particular Directive 2004/18/EC of the European Parliament and the Council of 31 March 2004 on the coordination of procedures for the award of public works contracts, public supply contracts and public service contracts [2], Council Regulation (EC, Euratom) No 1605/2002 of 25 June 2002 on the Financial Regulation applicable to the general budget of the European Communities [3] and Commission Regulation (EC, Euratom) No 2342/2002 [4] establishing its implementing rules and in particular to bilateral and multilateral agreements referred to in Articles 106 and 107 of Regulation (EC, Euratom) No 1605/2002,
HAS DECIDED AS FOLLOWS:
Article 1
The Commission provisions on security as set out in the Annex to Decision 2001/844/EC, ECSC, Euratom are amended as follows:
1. In Article 2(1) the following paragraph is added:
"When a contract or grant agreement between the Commission and an external contractor or beneficiary involves the processing of EU classified information in the contractor's or beneficiary's premises, the appropriate measures to be taken by the said external contractor or beneficiary to ensure that the rules referred to in Article 1 are complied with, when handling EU classified information, shall be an integral part of the contract or grant agreement."
2. The rules on security as set out in the Annex to the Commission provisions on security are amended as follows:
(a) In Section 5.1. of Part I the following sentence is added:
"Such minimum standards shall also be applied when the Commission confers by contract or grant agreement, tasks involving, entailing and/or containing EU classified information on industrial or other entities: these common minimum standards are contained in Section 27 of Part II.";
(b) In Part II, the text in the Annex to this Decision is added as Section 27;
(c) In Appendix 6, the following abbreviations are added:
"DSA : Designated Security Authority
FSC : Facility Security Clearance
FSO : Facility Security Officer
PSC : Personnel Security Clearance
SAL : Security Aspects Letter
SCG : Security Classification Guide"
.
Article 2
This Decision shall enter into force on the day of its publication in the Official Journal of the European Union.
Done at Brussels, 2 August 2006.
For the Commission
Siim Kallas
Vice-President
[1] OJ L 317, 3.12.2001, p. 1. Decision as last amended by Decision 2006/70/EC, Euratom (OJ L 34, 7.2.2006, p. 32).
[2] OJ L 134, 30.4.2004, p. 114. Directive as last amended by Commission Regulation (EC) No 2083/2005 (OJ L 333, 20.12.2005, p. 28).
[3] OJ L 248, 16.9.2002, p. 1.
[4] OJ L 357, 31.12.2002, p. 1. Regulation as last amended by Regulation (EC, Euratom) No 1261/2005 (OJ L 201, 2.8.2005, p. 3).
--------------------------------------------------
ANNEX
"27. COMMON MINIMUM STANDARDS ON INDUSTRIAL SECURITY
27.1. Introduction
This Section deals with security aspects of industrial activities that are unique to negotiating and awarding contracts or grant agreements conferring tasks involving, entailing and/or containing EU classified information and to their performance by industrial or other entities, including the release of, or access to, EU classified information during the public procurement and call for proposals procedures (bidding period and pre-contract negotiations).
27.2. Definitions
For the purpose of these common minimum standards, the following definitions shall apply:
(a) "Classified contract": any contract or grant agreement to supply products, execute works, render available buildings or provide services, the performance of which requires or involves access to or creation of EU classified information;
(b) "Classified sub-contract": a contract entered into by a contractor or a grant beneficiary with another contractor (i.e. the subcontractor) for the supply of products, execution of works, provision of buildings or services, the performance of which requires or involves access to or generation of EU classified information;
(c) "Contractor": an economic operator or legal entity possessing the legal capacity to undertake contracts or to be beneficiary of a grant;
(d) "Designated Security Authority (DSA)": an authority responsible to the National Security Authority (NSA) of an EU Member State which is responsible for communicating to industry or other entities the national policy in all matters of industrial security and for providing direction and assistance in its implementation. The function of DSA may be carried out by the NSA;
(e) "Facility Security Clearance (FSC)": an administrative determination by a NSA/DSA that, from the security viewpoint, a facility can afford adequate security protection to EU classified information of a specific security classification level and that its personnel who require access to EU classified information have been appropriately security cleared and briefed on the necessary security requirements for accessing and protecting EU classified information;
(f) "Industrial or other entity": a contractor or a subcontractor involved in supplying goods, executing works or providing services; this may involve industrial, commercial, service, scientific, research, educational or development entities;
(g) "Industrial security": the application of protective measures and procedures to prevent, detect and recover from the loss or compromise of EU classified information handled by a contractor or subcontractor in (pre)contract negotiations and classified contracts;
(h) "National Security Authority (NSA)": the Government Authority of an EU Member State with ultimate responsibility for the protection of EU classified information within that Member State;
(i) "Overall level of security classification of a contract": determination of the security classification of the whole contract or grant agreement, based on the classification of information and/or material that is to be, or may be, generated, released or accessed under any element of the overall contract or grant agreement. The overall level of security classification of a contract may not be lower than the highest classification of any of its elements, but may be higher because of the aggregation effect;
(j) "Security Aspects Letter (SAL)": a set of special contractual conditions, issued by the contracting authority, which forms an integral part of a classified contract involving access to or generation of EU classified information, and that identifies the security requirements or those elements of the classified contract requiring security protection;
(k) "Security Classification Guide (SCG)": a document which describes the elements of a programme, contract or grant agreement which are classified, specifying the applicable security classification levels. The SCG may be expanded throughout the life of the programme, contract or grant agreement, and the elements of information may be re-classified or downgraded. The SCG must be part of the SAL.
27.3. Organisation
(a) The Commission may confer by classified contract tasks involving, entailing and/or containing EU classified information on industrial or other entities registered in a Member State;
(b) The Commission shall ensure that all requirements deriving from these minimum standards are complied with when awarding classified contracts;
(c) The Commission shall involve the relevant NSA or NSAs in order to apply these minimum standards on industrial security. NSAs may refer these tasks to one or more DSAs;
(d) The ultimate responsibility for protecting EU classified information within industrial or other entities rests with the management of these entities;
(e) Whenever a classified contract or subcontract falling within the scope of these minimum standards is awarded, the Commission and/or the NSA/DSA, as appropriate, will promptly notify the NSA/DSA of the Member State in which the contractor or subcontractor is registered.
27.4. Classified contracts and grant decisions
(a) The security classification of contracts or grant agreements must take account of the following principles:
- the Commission determines, as appropriate, those aspects of the classified contract which require protection and the consequent security classification; in so doing, it must take into account the original security classification assigned by the originator to information generated before awarding the classified contract,
- the overall level of classification of the contract may not be lower than the highest classification of any of its elements,
- EU classified information generated under contractual activities is classified in agreement with the Security Classification Guide,
- where appropriate, the Commission is responsible for changing the overall level of classification of the contract, or security classification of any of its elements, in consultation with its originator, and for informing all interested parties,
- classified information released to the contractor or subcontractor or generated under contractual activity must not be used for purposes other than those defined by the classified contract and must not be disclosed to third parties without the prior written consent of the originator;
(b) The Commission and NSAs/DSAs of the relevant Member States are responsible for ensuring that contractors and subcontractors awarded classified contracts which involve information classified CONFIDENTIEL UE or above take all appropriate measures for safeguarding such EU classified information released to or generated by them in the performance of the classified contract in accordance with national laws and regulations. Non-compliance with the security requirements may result in termination of the classified contract;
(c) All industrial or other entities participating in classified contracts which involve access to information classified CONFIDENTIEL UE or above must hold a national FSC. The FSC is granted by the NSA/DSA of the Member State to confirm that a facility can afford and guarantee adequate security protection of EU classified information to the appropriate classification level;
(d) When a classified contract is awarded, a Facility Security Officer (FSO), appointed by the management of the contractor or subcontractor, shall be responsible for requesting a Personnel Security Clearance (PSC) for all persons employed in industrial or other entities registered in an EU Member State whose duties require access to information classified CONFIDENTIEL UE or above subject to a classified contract, to be granted by the NSA/DSA of that Member State in accordance with its national regulations;
(e) Classified contracts must include the SAL as defined in 27.2.(j). The SAL must contain the SCG;
(f) Before initiating a negotiated procedure for a classified contract the Commission will contact the NSA/DSA of the Member State in which the industrial or other entities concerned are registered in order to obtain confirmation that they hold a valid FSC appropriate to the level of security classification of the contract;
(g) The contracting authority must not place a classified contract with a preferred economic operator before having received the valid FSC certificate;
(h) Unless required by Member State national laws and regulations, an FSC is not required for contracts involving information classified RESTREINT UE;
(i) Invitations to tender in respect of classified contracts must contain a provision requiring that an economic operator who fails to submit a tender or who is not selected be required to return all documents within a specified period of time;
(j) It may be necessary for contractors to negotiate classified subcontracts with subcontractors at various levels. The contractor is responsible for ensuring that all subcontracting activities are undertaken in accordance with the common minimum standards contained in this Section. However, the contractor must not transmit EU classified information or material to a subcontractor without the prior written consent of the originator;
(k) The conditions under which a contractor may subcontract must be defined in the tender or call for proposals and in the classified contract. No subcontract may be awarded to entities registered in a non-EU Member State without the express written authorisation of the Commission;
(l) Throughout the life of the classified contract, compliance with all its security provisions will be monitored by the Commission, in conjunction with the relevant DSA/NSA. Any security incidents shall be reported, in accordance with the provisions laid down in Part II, Section 24 of these Rules on Security. Any change to or withdrawal of an FSC shall immediately be communicated to the Commission and to any other NSA/DSA to which it has been notified;
(m) When a classified contract or a classified subcontract is terminated, the Commission and/or the NSA/DSA, as appropriate, will promptly notify the NSA/DSA of the Member State in which the contractor or subcontractor is registered;
(n) The common minimum standards contained in this Section shall continue to be complied with, and the confidentiality of classified information shall be maintained by the contractors and subcontractors, after termination or conclusion of the classified contract or classified subcontract;
(o) Specific provisions for the disposal of classified information at the end of the classified contract will be laid down in the SAL or in other relevant provisions identifying security requirements;
(p) The obligations and conditions referred to in this Section apply mutatis mutandis to procedures where grants are awarded by decision and notably to the beneficiaries of such grants. The grant decision shall set out all obligations of the beneficiaries.
27.5. Visits
Visits by personnel of the Commission in the context of classified contracts to industrial or other entities in the Member States performing EU classified contracts must be arranged with the relevant NSA/DSA. Visits by employees of industrial or other entities within the framework of an EU classified contract must be arranged between the NSAs/DSAs concerned. However, the NSAs/DSAs involved in an EU classified contract may agree on a procedure whereby visits by employees of industrial or other entities can be arranged directly.
27.6. Transmission and transportation of EU classified information
(a) With regard to the transmission of EU classified information, the provisions of Part II, Section 21 of these Rules on Security shall apply. In order to supplement such provisions, any existing procedures in force among Member States will apply;
(b) The international transportation of EU classified material relating to classified contracts will be in accordance with Member State's national procedures. The following principles will be applied when examining security arrangements for international transportation:
- security is assured at all stages during the transportation and under all circumstances, from the point of origin to the ultimate destination,
- the degree of protection accorded to a consignment is determined by the highest classification of material contained within it,
- an FSC is obtained, where appropriate, for companies providing transportation. In such cases, personnel handling the consignment must be security cleared in compliance with the common minimum standards contained in this Section,
- journeys are point to point to the extent possible, and are completed as quickly as circumstances permit,
- whenever possible, routes should be only through EU Member States. Routes through non-EU Member States should only be undertaken when authorised by the NSA/DSA of the States of both the consignor and the consignee,
- prior to any movement of EU classified material, a Transportation Plan is made up by the consignor and approved by the NSAs/DSAs concerned."
--------------------------------------------------
