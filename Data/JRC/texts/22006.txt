COMMISSION REGULATION (EEC) No 686/78 of 6 April 1978 laying down additional provisions as regards the granting of export refunds on fishery products
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 100/76 of 19 January 1976 on the common organization of the market in fishery products (1), as last amended by Regulation (EEC) No 2560/77 (2),
Having regard to Council Regulation (EEC) No 110/76 of 19 January 1976 laying down general rules for granting export refunds on fishery products and criteria for fixing the amount of such refunds (3), and in particular Article 7 (3) thereof,
Whereas it has been found that products of Community origin are frozen and/or processed on board a vessel flying the flag of a non-member country ; whereas such products are exported after having been frozen or processed;
Whereas the economic conditions under which such operations are carried out are comparable to those of operations carried out in non-member countries where production costs are substantially lower than in the Community ; whereas the granting of export refunds on products frozen and/or processed under such conditions gives these products an unjustified economic advantage;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Fishery Products,
HAS ADOPTED THIS REGULATION:
Article 1
For the purposes of the export refund system, fishery products of Community origin frozen and/or processed on board a vessel registered or recorded in a third country and flying the flag of a third country shall be considered to be products which are not of Community origin.
Article 2
This Regulation shall enter into force on 29 April 1978.
It shall apply to exports for which customs export formalities have been completed with effect from this date.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 6 April 1978.
For the Commission
Finn GUNDELACH
Vice-President (1)OJ No L 20, 28.1.1976, p. 1. (2)OJ No L 303, 28.11.1977, p. 1. (3)OJ No L 20, 28.1.1976, p. 48.
