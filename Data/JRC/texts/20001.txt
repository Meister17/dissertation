COUNCIL REGULATION (EC) No 2964/95 of 20 December 1995 introducing registration for crude oil imports and deliveries in the Community
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 213 thereof,
Having regard to the proposal from the Commission,
Whereas the establishment of a common energy policy is one of the objectives which the Community has set itself; whereas it is for the Commission to propose the measures to be taken to that end;
Whereas security of supply at stable prices is one of the main objectives of that policy;
Whereas market transparency is desirable;
Whereas, in view of the supply situation and in order to stabilize the Community market and ensure that abnormal world market fluctuations do not have an unfavourable impact on the Community market, the Member States and the Commission should be informed, at regular intervals, about crude oil supply costs;
Whereas, by means of Regulation (EEC) No 1893/79 (1), the Council introduced arrangements for the registration of crude oil imports in the Community;
Whereas, by means of Regulation (EEC) No 2592/79 (2), the Council laid down rules for carrying out the registration of crude oil imports in the community provided for in Regulation (EEC) No 1893/79;
Whereas since those Regulations expired on 31 December 1991 the rules that they laid down should be reintroduced, while adapting them to the trading conditions prevailing on international oil markets and the objective of maintaining or improving environmental quality; whereas the reporting requirements should be aligned, as far as possible on the requirements of the national authorities and of the International Energy Agency,
HAS ADOPTED THIS REGULATION:
Article 1
Any person or undertaking importing crude oil from third countries or receiving a crude oil delivery from another Member State shall be obliged to provide information to the Member State in which he is established concerning the characteristics of the imports or deliveries.
Article 2
On the basis of the information referred to in Article 1, Member States shall, at regular intervals, forward to the Commission such information as will enable a true picture to be obtained of the developments in the conditions under which the imports or deliveries have taken place.
This information shall be circulated to the Member States.
Article 3
The information collected and forwarded pursuant to this Regulation shall be confidential.
This provision shall not prevent the publication of general information or information in summary form which does not contain details relating to individual undertakings.
Article 4
1. The information which persons or undertakings are obliged to communicate to the Member State in which they are established shall relate to each import or delivery of crude oil at a specific price.
2. 'Import` means each quantity of crude oil which enters the customs territory of the Community for purposes other than transit. 'Delivery` means each quantity of crude oil coming from another Member State for purposes other than transit. Imports or deliveries carried out on behalf of companies situated outside the importing country and intended for refining under contract and subsequent export in their entirety in the form of refined products shall be excluded.
3. However, oil extracted from the seabed over which a Member State exercises exclusive rights for the purposes of exploitation shall not be considered, when it enters the customs territory of the Community, as being an import within the meaning of paragraph 2.
Article 5
For the purposes of Article 1 the characteristics of each import or delivery of crude oil into a Member State shall include:
- the designation of the crude oil, including the API gravity,
- the quantity in barrels,
- the cif price paid per barrel,
- the percentage sulphur content.
Article 6
The information referred to in Articles 4 and 5 shall be forwarded to the Member State concerned in respect of each period not exceeding one month.
Article 7
The information which Member States are obliged to communicate to the Commission pursuant to Article 2 shall be forwarded within one month of the end of each month referred to in Article 6. This information shall consist, for each type of crude oil, of an aggregation of the data which the Member States receive from persons and undertakings. For each type of crude oil, the information shall comprise:
- the designation of the crude oil, including the average API gravity,
- the quantity in barrels,
- the average cif price,
- the number of companies reporting,
- the percentage sulphur content.
Article 8
1. The Commission shall analyse the information gathered pursuant to Article 7 and communicate it to the Member States each month.
2. The Member States and the Commission shall consult each other at regular intervals at the request of a Member State or on the initiative of the Commission. Such consultations shall relate in particular to the communications from the Commission referred to in paragraph 1.
Consultations may be organized with international organizations and third countries which have set up similar information systems.
Article 9
1. The information communicated pursuant to Article 4 and the information provided for in Article 7 shall be confidential. This provision shall not, however, prevent the distribution of information in a form which does not disclose details relating to individual undertakings, i. e. which refers to at least three undertakings.
2. The information forwarded to the Commission on the basis of Article 7 and the communications referred to in Article 8 (1) can only be used for the purposes of Article 8 (2).
3. If the Commission discovers, in the information communicated to it by the Member States in accordance with Article 7, the existence of anomalies or inconsistencies which prevent it from obtaining a true picture of developments in the conditions under which imports and deliveries have taken place, it may ask the Member States to allow it access to the relevant unaggregated information provided by the undertakings and the calculation and assessment procedures used to arrive at the aggregated information.
Article 10
The Commission shall, after consulting the Member States, adopt detailed rules for implementing this Regulation.
Article 11
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 20 December 1995.
For the Council
The President
J. M. EGUIAGARAY
