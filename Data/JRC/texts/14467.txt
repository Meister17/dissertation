Council Decision
of 27 June 2006
on the conclusion, on behalf of the European Community, of the Protocol on Soil Protection, the Protocol on Energy and the Protocol on Tourism to the Alpine Convention
(2006/516/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Articles 175(1), in conjunction with the first sentence of the first subparagraph of Article 300(2) and the first subparagraph of Article 300(3) thereof,
Having regard to the proposal from the Commission,
Having regard to the Opinion of the European Parliament [1],
Whereas:
(1) The Convention on the protection of the Alps (hereinafter "the Alpine Convention") was concluded on behalf of the European Community by Council Decision 96/191/EC [2].
(2) The Council decided on the signature, on behalf of the European Community, of the Protocol on Soil Protection, the Protocol on Energy and the Protocol on Tourism to the Alpine Convention (hereinafter "the Protocols") by Council Decision 2005/923/EC [3].
(3) The Protocols are an important step in the implementation of the Alpine Convention, and the European Community is committed to the objectives of this Convention.
(4) Economic, social and ecological cross-border problems of the Alps remain a major challenge to be addressed in this highly sensitive area.
(5) Community Policies, in particular priority areas as defined in Decision No 1600/2002/EC of the European Parliament and of the Council of 22 July 2002 laying down the Sixth Community Environment Action Programme [4], should be promoted and strengthened within the Alpine region.
(6) One of the main goals of the Protocol on Soil Protection is the safeguarding of the multifunctional role of soil based on the concept of sustainable development. Sustainable productivity of soil must be ensured in its natural function, as an archive of natural and cultural history and in order to guarantee its use for agriculture and forestry, urbanism and tourism, other economic uses, transport and infrastructure, and as a source of raw materials.
(7) Any approach to soil protection should take account of the considerable diversity of regional and local conditions that exist in the Alpine region. The Protocol on Soil Protection could help to implement appropriate measures at national and regional level.
(8) Requirements of the Protocol such as soil monitoring, identification of risk zones for erosion, flooding and landslides, an inventory of contaminated sites and the establishment of harmonised databases could be important elements for a Community policy on soil protection, as evidenced by, inter alia, Council Directive 85/337/EEC of 27 June 1985 on the assessment of the effects of certain public and private projects on the environment [5], Council Directive 86/278/EEC of 12 June 1986 on the protection of the environment, and in particular of the soil, when sewage sludge is used in agriculture [6], Council Directive 92/43/EEC of 21 May 1992 on the conservation of natural habitats and of wild fauna and flora [7], Council Directive 99/31/EC of 26 April 1999 on the landfill of waste [8], Council Regulation (EC) No 1257/1999 of 17 May 1999 on support for rural development from the European Agricultural Guidance and Guarantee Fund (EAGGF) [9] and Directive 2000/60/EC of the European Parliament and of the Council of 23 October 2000 establishing a framework for Community action in the field of water policy [10].
(9) The Protocol on Energy requires appropriate measures to be taken in the field of energy saving, energy production, including the promotion of renewable energy, energy transport, delivery and use of energy to foster conditions for sustainable development.
(10) The provisions of the Protocol on Energy are in line with the Sixth Community Environment Action Programme to combat climate change as well as to promote sustainable management and use of natural resources. The Protocol's provisions are also in line with the Community's policy on energy, as set out in the White Paper for a "Community Strategy and Action Plan", the Green Paper "Towards a European strategy for the security of energy supply", Directive 2001/77/EC of the European Parliament and of the Council of 27 September 2001 on the promotion of electricity produced from renewable energy sources in the internal electricity market [11], Directive 2002/91/EC of the European Parliament and of the Council of 16 December 2002 on the energy performance of buildings [12] and Decision No 1230/2003/EC of the European Parliament and of the Council of 26 June 2003 adopting a multiannual programme for action in the field of energy: "Intelligent Energy — Europe" (2003-2006) [13].
(11) The ratification of the Protocol on Energy would strengthen trans-border cooperation with Switzerland, Liechtenstein and Monaco. This would help to ensure that goals of the European Community are shared by regional partners and that such initiatives cover the whole Alpine eco-region.
(12) Trans European Energy Networks (TEN-E) should be given priority and coordination and implementation measures foreseen in the TEN-E guidelines in Decision No 1229/2003/EC of the European Parliament and of the Council of 26 June 2003 laying down a series of guidelines for trans European energy networks [14] should be applied when developing new crossborder connections, in particular high-voltage lines.
(13) The European Community, its Member States, Switzerland, Liechtenstein and Monaco are Parties to the United Nations Framework Convention on Climate Change (UNFCCC) and the Kyoto Protocol. The UNFCCC and the Kyoto Protocol require that Parties formulate, implement, publish and regularly update national and regional programmes containing measures to mitigate climate change by addressing anthropogenic emissions by sources and providing removals by sinks of all greenhouse gases not controlled by the Montreal Protocol.
(14) The Protocol on Energy contributes to fulfilling UNFCCC requirements to take measures to facilitate adequate adaptation to climate change.
(15) Tourism is an economically highly important sector in most parts of the Alps and is intimately linked to and dependant on environmental and social impacts.
(16) As the mountain region is a unique and ecologically very sensitive area, a balance between economic interests, local population needs and environmental concerns is extremely important for sustainable development of the region.
(17) Tourism is an increasingly global phenomenon, but at the same time it remains a sphere of primarily local and regional responsibility. As regards the Community, Directive 85/337/EEC, Directive 92/43/EEC, Regulation (EC) No 1980/2000 of the European Parliament and of the Council of 17 July 2000 on a revised Community eco-label award scheme [15], Regulation (EC) No 761/2001 of the European Parliament and of the Council of 19 March 2001 allowing voluntary participation by organisations in a Community eco-management and audit scheme (EMAS) [16] and Council Resolution of 21 May 2002 on the future of European tourism [17], inter alia, are relevant in this context. The Alpine Convention and its Protocol on Tourism, together with the other Protocols that can have influence on the tourism sector, should represent a framework instrument to stimulate and coordinate the contribution of stakeholders at regional and local level in order to make sustainability a major driver in the improvement of the quality of the Alpine region tourist offer.
(18) The overall goal of the Protocol on Tourism is to promote sustainable tourism, specifically by ensuring it is developed and managed taking into consideration its impacts on the environment. To this end, it provides specific measures and recommendations that can be used as instruments for reinforcing the environmental side of innovation and research, monitoring and training, management tools and strategies, planning and authorisation procedures linked to tourism and in particular to its qualitative development.
(19) Contracting Parties to the three Protocols should promote relevant education and training and, additionally, promote the dissemination of information to the public regarding the objectives, measures and implementation of each of these three Protocols.
(20) It is appropriate that these Protocols be approved by the European Community,
HAS DECIDED AS FOLLOWS:
Article 1
The Protocol on Soil Protection [18], the Protocol on Energy [19] and the Protocol on Tourism [20] to the Alpine Convention, signed at Salzburg on 7 November 1991, are hereby approved on behalf of the European Community.
Article 2
The President of the Council is hereby authorised to designate the person(s) empowered to deposit, on behalf of the Community, the instrument of approval with the Republic of Austria in accordance with Article 27 of the Protocol on Soil Protection, Article 21 of the Protocol on Energy and Article 28 of the Protocol on Tourism.
At the same time the designated person(s) shall deposit the Declarations related to the Protocols.
Article 3
This Decision shall be published in the Official Journal of the European Union.
Done at Luxembourg, 27 June 2006.
For the Council
The President
J. Pröll
[1] Opinion of 13 June 2006 (not yet published in the Official Journal).
[2] OJ L 61, 12.3.1996, p. 31.
[3] OJ L 337, 22.12.2005, p. 27.
[4] OJ L 242, 10.9.2002, p. 1.
[5] OJ L 175, 5.7.1985, p. 40. Directive as last amended by Directive 2003/35/EC of the European Parliament and the Council (OJ L 156, 25.6.2003, p. 17).
[6] OJ L 181, 4.7.1986, p. 6. Directive as last amended by Regulation (EC) No 807/2003 (OJ L 122, 16.5.2003, p. 36).
[7] OJ L 206, 22.7.1992, p. 7. Directive as last amended by Regulation (EC) No 1882/2003 of the European Parliament and the Council (OJ L 284, 31.10.2003, p. 1).
[8] OJ L 182, 16.7.1999, p. 1. Directive as amended by Regulation (EC) No 1882/2003.
[9] OJ L 160, 26.6.1999, p. 80. Regulation as last amended by Regulation (EC) No 2223/2004 (OJ L 379, 24.12.2004, p. 1).
[10] OJ L 327, 22.12.2000, p. 1. Directive as last amended by Decision No 2455/2001/EC (OJ L 331, 15.12.2001, p. 1).
[11] OJ L 283, 27.10.2001, p. 33. Directive as last amended by Decision of the EEA Joint Committee No 102/2005 (OJ L 306, 24.11.2005, p. 34).
[12] OJ L 1, 4.1.2003, p. 65.
[13] OJ L 176, 15.7.2003, p. 29. Decision as last amended by Decision No 787/2004/EC of the European Parliament and the Council (OL L 138, 30.4.2004, p. 12).
[14] OJ L 176, 15.7.2003, p. 11.
[15] OJ L 237, 21.9.2000, p. 1.
[16] OJ L 114, 24.4.2001, p. 1. Regulation as last amended by Commission Regulation (EC) No 196/2006 (OJ L 32, 4.2.2006, p. 4).
[17] OJ C 135, 6.6.2002, p. 1.
[18] OJ L 337, 22.12.2005, p. 29.
[19] OJ L 337, 22.12.2005, p. 36.
[20] OJ L 337, 22.12.2005, p. 43.
--------------------------------------------------
