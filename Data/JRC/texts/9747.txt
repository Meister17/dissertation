Communication from the French Government concerning Directive 94/22/EC of the European Parliament and of the Council of 30 May 1994 on the conditions for granting and using authorisations for the prospection, exploration and production of hydrocarbons [1]
(Notice regarding an application for an exclusive licence to prospect for oil and gas, designated "Permis de Mairy")
(2006/C 56/05)
(Text with EEA relevance)
On 25 November 2005, Lundin International, a company with its registered office at Maclaunay, F-51210 Montmirail, and Madison Energy France S.C.S, a company with its registered office at 13/15 boulevard de la Madeleine, F-75001 Paris, jointly and severally applied for an exclusive five-year licence, designated "Permis de Mairy", to prospect for oil and gas in an area covering some 444 km2 in the department of Marne.
The perimeter of the area covered by this licence is made up of the meridian and parallel arcs successively joining the points defined below by their geographical coordinates, the original meridian being that of Paris.
POINTS | LONGITUDE | LATITUDE |
A | 2,20° E | 54,40° N |
B | 2,40° E | 54,40° N |
C | 2,40° E | 54,30° N |
D | 2,50° E | 54,30° N |
E | 2,50° E | 54,10° N |
F | 2,48° E | 54,10° N |
G | 2,48° E | 54,13° N |
H | 2,44° E | 54,13° N |
I | 2,44° E | 54,12° N |
J | 2,43° E | 54,12° N |
K | 2,43° E | 54,10° N |
L | 2,30° E | 54,10° N |
M | 2,30° E | 54,20° N |
N | 2,20° E | 54,20° N |
Submission of applications
The initial applicant and competing applicants must prove that they comply with the requirements for obtaining the licence, as specified in Articles 3, 4 and 5 of Decree No 95-427 of 19 April 1995, as amended, concerning mining rights.
Interested companies may, within ninety days of the publication of this notice, submit a competing application in accordance with the procedure summarised in the "Notice regarding the granting of mining rights for hydrocarbons in France" published in Official Journal of the European Communities C 374 of 30.12.1994, p. 11, and established by Decree No 95-427 of 19 April 1995, as amended, concerning mining rights (Journal officiel de la République française, 22 April 1995).
Competing applications must be sent to the Minister responsible for mines at the address below. Decisions on the initial application and competing applications will be taken within two years of the date on which the French authorities received the initial application, i.e. by 30 June 2007 at the latest.
Conditions and requirements regarding performance of the activity and cessation thereof
Applicants are referred to Articles 79 and 79.1 of the Mining Code and to Decree No 95-696 of 9 May 1995, as amended, on the start of mining operations and the mining regulations (Journal officiel de la République française, 11 May 1995).
Further information can be obtained from the Ministry of Economic Affairs, Finance and Industry (Directorate-General for Energy and Raw Materials, Directorate for Energy and Mineral Resources, Bureau of Mining Legislation), 61, boulevard Vincent Auriol, Télédoc 133, F-75703 Paris Cedex 13, France, (telephone: (33) 144 97 23 02, fax: (33) 144 97 05 70).
The abovementioned laws and regulations can be consulted at http://www.legifrance.gouv.fr
[1] OJ L 164, 30.6.1994, p. 3.
--------------------------------------------------
