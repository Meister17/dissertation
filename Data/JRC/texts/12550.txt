Council Decision
of 11 December 2006
on the appointment of the Special Coordinator of the Stability Pact for South-Eastern Europe
(2006/921/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1080/2000 of 22 May 2000 on support for the United Nations Interim Mission in Kosovo (UNMIK), the Office of the High Representative in Bosnia and Herzegovina (OHR) [1] and the Stability Pact for South-Eastern Europe (SP), and in particular Article 1(a) thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) On 10 June 1999 the Foreign Ministers of the Member States of the European Union and the Commission of the European Communities, together with the other participants in the Stability Pact for South-Eastern Europe, agreed to establish a Stability Pact for South-Eastern Europe, hereinafter referred to as the "Stability Pact".
(2) Article 1(a) of Regulation (EC) No 1080/2000 provides for the Special Coordinator of the Stability Pact to be appointed on an annual basis.
(3) It is necessary to establish, together with the appointment, a mandate for the Special Coordinator. Experience has shown that the mandate laid down in Council Decision 2005/912/EC of 12 December 2005 on the appointment of the Special Coordinator of the Stability Pact for South-Eastern Europe [2] for 2006 is appropriate. Further to the conclusions of the South-Eastern Europe Regional Table which endorsed on 30 May 2006 in Belgrade a transition plan to regional ownership, and in accordance with the Council conclusions of 12 June 2006, the mandate should put a special emphasis on the requirements of this transition.
(4) It is appropriate to lay down clear lines of responsibility as well as guidance on coordination and reporting,
HAS DECIDED AS FOLLOWS:
Article 1
Dr Erhard BUSEK is hereby appointed Special Coordinator of the Stability Pact for South-Eastern Europe (the Stability Pact).
Article 2
The Special Coordinator shall carry out the functions provided for in point 13 of the Stability Pact document of 10 June 1999.
Article 3
In order to achieve the objective referred to in Article 2, the mandate of the Special Coordinator shall be to:
(a) promote achievement of the Stability Pact’s objectives within, and between, the individual countries, where the Stability Pact proves to have an added value;
(b) chair the South-Eastern Europe Regional Table;
(c) maintain close contact with all participants and facilitating States, organisations and institutions of the Stability Pact, as well as relevant regional initiatives and organisations, with a view to fostering regional cooperation and enhancing regional ownership;
(d) cooperate closely with all institutions of the European Union and its Member States in order to promote the role of the European Union in the Stability Pact in accordance with points 18, 19, and 20 of the Stability Pact document and to ensure complementarity between the work of the Stability Pact and the Stabilisation and Association Process;
(e) meet periodically and collectively as appropriate with the Chairs of the Working Tables to ensure strategic overall coordination and act as the secretariat of the South-Eastern Europe Regional Table and its instruments;
(f) work on the basis of a list, agreed in advance and in consultation with the participants in the Stability Pact, of priority actions for the Stability Pact to implement during 2007, and adjust the working methods and structures of the Stability Pact to the needs of the transition to regional ownership, ensuring consistency and efficient use of resources;
(g) facilitate the implementation of the transition to regional ownership in accordance to the conclusions of the South-Eastern Europe Regional Table on 30 May 2006 and to this purpose work closely with the South-Eastern European Cooperation Process and its Secretary General when designated. Particular attention shall be given to the establishment of the Regional Cooperation Council and of a regional cooperation secretariat as well as on the streamlining of the various Task Forces and initiatives of the Stability Pact.
Article 4
The Special Coordinator shall conclude a financing agreement with the Commission.
Article 5
The activities of the Special Coordinator shall be coordinated with those of the Secretary-General of the Council/High Representative for the CFSP, the Presidency of the Council and the Commission, notably in the framework of the Informal Consultative Committee. In the field, close liaison shall be maintained with the Presidency of the Council, the Commission, the Member States’ Heads of Mission, the Special Representatives of the European Union, as well as with the Office of the High Representative in Bosnia and Herzegovina and the United Nations Civil Administration in Kosovo.
Article 6
The Special Coordinator shall report, as appropriate, to the Council and the Commission. He will continue to inform the European Parliament regularly about his activities.
Article 7
This Decision shall take effect on the day following its publication in the Official Journal of the European Union.
It shall apply from 1 January 2007 until 31 December 2007.
Done at Brussels, 11 December 2006.
For the Council
The President
E. Tuomioja
[1] OJ L 122, 24.5.2000, p. 27. Regulation as amended by Regulation (EC) No 2098/2003 (OJ L 316, 29.11.2003, p. 1).
[2] OJ L 331, 17.12.2005, p. 32.
--------------------------------------------------
