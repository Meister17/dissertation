Lifting by France of public service obligations on scheduled air services operated on the Le Havre — Rouen — Strasbourg, Saint-Brieuc — Paris, and Saint-Brieuc — Nantes routes
(2005/C 153/10)
(Text with EEA relevance)
France has decided to lift the public service obligations imposed on scheduled air services operated between:
1. Le Havre and Strasbourg via Rouen, published in the Official Journal of the European Communities C 279 of 25 September 1996 and amended on 16 April 1998 (Official Journal of the European Communities C 116).
2. Saint-Brieuc and Paris, published in the Official Journal of the European Union C 53 of 7 March 2003.
3. Saint-Brieuc and Nantes, published in the Official Journal of the European Communities C 34 of 9 February 1999 and amended on 14 August 1999 (Official Journal of the European Communities C 233).
--------------------------------------------------
