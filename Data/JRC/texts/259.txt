Council Decision
of 17 July 2000
amending Decision 1999/311/EC adopting the third phase of the trans-European cooperation scheme for higher education (Tempus III) (2000 to 2006)
(2000/460/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 308 thereof,
Having regard to the proposal of the Commission,
Having regard to the opinion of the European Parliament(1),
Having regard to the opinion of the Economic and Social Committee(2),
Having regard to the opinion of the Committee of the Regions(3),
Whereas:
(1) By Decision 1999/311/EC(4) the Council adopted the third phase of the trans-European cooperation scheme for higher education (Tempus III) (2000 to 2006).
(2) This programme is intended for the non-associated countries of central and eastern Europe eligible for economic aid by virtue of Regulation (EEC) No 3906/89 (PHARE programme)(5) or the programme intended to replace it and the new independent states of the former Soviet Union and Mongolia as laid down in Council Regulation (EC, Euratom) No 99/2000 of 29 December 1999 concerning the provision of assistance to the partner States in Eastern Europe and Central Asia(6) (which replaces the old TACIS programme).
(3) The footnote inserted in Article 2 states that "at present" the programme relates to Albania, Bosnia-Herzegovina and the former Yugoslav Republic of Macedonia.
(4) It is important to be able to extend the Tempus III programme to other countries in the region in future, in particular Croatia,
HAS DECIDED AS FOLLOWS:
Article 1
Decision 1999/311/EC is hereby amended as follows:
1. In Article 2, the first paragraph shall be replaced by the following:"Tempus III concerns the non-associated countries of central and eastern Europe designated as eligible for economic aid by virtue of Regulation (EEC) No 3906/89 (PHARE programme) or the programme intended to replace it, and the new independent states of the former Soviet Union and Mongolia mentioned in Regulation (EC, Euratom) No 99/2000 (which replaces the old TACIS programme). These countries are hereinafter referred to as 'eligible countries'."
2. Footnote 1 shall be deleted.
Article 2
This Decision shall take effect on the day of its publication in the Official Journal of the European Communities.
Done at Brussels, 17 July 2000.
For the Council
The President
L. Fabius
(1) Opinion delivered on 14 June 2000 (not yet published in the Official Journal).
(2) Opinion delivered on 29 May 2000 (not yet published in the Official Journal).
(3) Opinion delivered on 14 June 2000 (not yet published in the Official Journal).
(4) OJ L 120, 8.5.1999, p. 30.
(5) OJ L 375, 23.12.1989, p. 11. Regulation as last amended by Regulation (EC) No 1266/1999 (OJ L 161, 20.6.1999, p. 68).
(6) OJ L 12, 18.1.2000, p. 1.
