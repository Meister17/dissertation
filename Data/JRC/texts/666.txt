COMMISSION DECISION
of 20 December 1999
amending Decision 1999/120/EC drawing up provisional lists of third country establishments from which the Member States authorise imports of animal casings
(notified under document number C(1999) 4697)
(Text with EEA relevance)
(2000/80/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Decision 95/408/EC of 22 June 1995 on the conditions for drawing up, for an interim period, provisional lists of third country establishments from which Member States are authorised to import certain products of animal origin, fishery products or live bivalve molluscs(1), as last amended by Decision 98/603/EC(2), and in particular Article 2(4) thereof,
Whereas:
(1) provisional lists of establishments in third countries producing animal casings have been drawn up by Commission Decision 1999/120/EC(3);
(2) Bangladesh, Mexico, Nicaragua and Panama have sent their lists of establishments producing animal casings and for which the responsible authorities certify that the establishments are in accordance with the Community rules;
(3) provisional lists of establishments producing animal casings can thus be drawn up for Bangladesh, Mexico, Nicaragua and Panama; whereas Decision 1999/120/EC should therefore be amended accordingly;
(4) the measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
1. In the title of Decision 1999/120/EC the words "stomachs and bladders" are inserted at the end.
2. In Article 1, first and second paragraph after the word "casings", "stomachs and bladders" are inserted.
Article 2
The text of the Annex to this Decision is added to the Annex to Decision 1999/120/EC.
Article 3
This Decision shall apply from 15 December 1999.
Article 4
This Decision is addressed to the Member States.
Done at Brussels, 20 December 1999.
For the Commission
David BYRNE
Member of the Commission
(1) OJ L 243, 11.10.1995, p. 17.
(2) OJ L 289, 28.10.1998, p. 36.
(3) OJ L 36, 10.2.1999, p. 21.
ANEXO/BILAG/ANHANG/ΠΑΡΑΡΤΗΜΑ/ANNEX/ANNEXE/ALLEGATO/BIJLAGE/ANEXO/LIITE/BILAGA
"País: BANGLADESH/Land: BANGLADESH/Land: BANGLADESCH/Χώρα: ΜΠΑΓΛΑΝΤΕΣ/Country: BANGLADESH/Pays: BANGLADESH/Paese: BANGLADESH/Land: BANGLADESH/País: BANGLADECHE/Maa: BANGLADESH/Land: BANGLADESH
>TABLE>
País: MÉXICO/Land: MEXICO/Land: MEXIKO/Χώρα: ΜΕΞΙΚΟ/Country: MEXICO/Pays: MEXIQUE/Paese: MESSICO/Land: MEXICO/País: MÉXICO/Maa: MEKSIKO/Land: MEXIKO
>TABLE>
País: NICARAGUA/Land: NICARAGUA/Land: NICARAGUA/Χώρα: ΝΙΚΑΡΑΓΟΥΑ/Country: NICARAGUA/Pays: NICARAGUA/Paese: NICARAGUA/Land: NICARAGUA/País: NICARÁGUA/Maa: NICARAGUA/Land: NICARAGUA
>TABLE>
País: PANAMÁ/Land: PANAMA/Land: PANAMA/Χώρα: ΠΑΝΑΜΑΣ/Country: PANAMA/Pays: PANAMA/Paese: PANAMA/Land: PANAMA/País: PANAMÁ/Maa: PANAMA/Land: PANAMA
>TABLE>"
