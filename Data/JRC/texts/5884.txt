Commission Regulation (EC) No 1472/2005
of 9 September 2005
determining to what extent import right applications submitted during the month of August 2005 for certain live bovine animals as part of a tariff quota provided for in Regulation (EC) No 1217/2005 may be accepted
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1254/1999 of 17 May 1999 on the common organisation of the market in beef and veal [1],
Having regard to Commission Regulation (EC) No 1217/2005 of 28 July 2005 layng down detailed rules for the application of a tariff quota for certain live bovine animals originating in Bulgaria, provided for in Council Decision 2003/286/EC [2], and in particular Article 4 thereof,
Whereas:
(1) Article 1 of Regulation (EC) No 1217/2005 fixes at 6600 the number of head of live bovine animals originating in Bulgaria which may be imported under special conditions in the period 1 July 2005 to 30 June 2006.
(2) Article 4(2) of Regulation (EC) No 1217/2005 lays down that the quantities applied for may be reduced. The applications lodged relate to total quantities which exceed the quantities available. Under these circumstances and taking care to ensure an equitable distribution of the available quantities, it is appropriate to reduce proportionally the quantities applied for,
HAS ADOPTED THIS REGULATION:
Article 1
All applications for import certificates lodged pursuant to Article 3(3) of Regulation (EC) No 1217/2005 shall be accepted at a rate of 43,5787 % of the import rates applied for.
Article 2
This Regulation shall enter into force on 10 September 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 9 September 2005.
For the Commission
J. M. Silva Rodríguez
Director-General for Agriculture and Rural Development
[1] OJ L 160, 26.6.1999, p. 21. Regulation as last amended by Regulation (EC) No 1782/2003 (OJ L 270, 21.10.2003, p. 1).
[2] OJ L 199, 29.7.2005, p. 33.
--------------------------------------------------
