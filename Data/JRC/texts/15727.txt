Council Decision
of 14 February 2006
on the conclusion of an Agreement in the form of an Exchange of Letters between the European Community and the Republic of Chile concerning amendments to the Agreement on Trade in Wines annexed to the Agreement establishing an Association between the European Community and its Member States, of the one part, and the Republic of Chile, of the other part
(2006/136/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 133, in conjunction with the first sentence of the first subparagraph of Article 300(2) thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) The Agreement establishing an Association between the European Community and its Member States, of the one part, and the Republic of Chile, of the other part [1] (hereinafter referred to as the Association Agreement), was signed on 18 November 2002, and entered into force on 1 March 2005 [2].
(2) On 24 November 2005 the Council authorised the Commission to enter into negotiations with the Republic of Chile to amend the Agreement on trade in wines attached as Annex V [3] (hereinafter referred to as Annex V) to the Association Agreement. These negotiations have been successfully concluded.
(3) The Agreement in the form of an Exchange of Letters between the European Community and the Republic of Chile concerning amendments to Annex V should be approved,
HAS DECIDED AS FOLLOWS:
Article 1
The Agreement in the form of an Exchange of Letters between the European Community and the Republic of Chile concerning amendments to the Agreement on trade in wines annexed to the Agreement establishing an Association between the European Community and its Member States, of the one part, and the Republic of Chile, of the other part, is hereby approved on behalf of the Community.
The text of the Agreement in the form of an Exchange of Letters is attached to this Decision.
Article 2
The Commissioner for Agriculture and Rural Development is hereby empowered to sign the Agreement in the form of an Exchange of Letters in order to bind the Community.
Done at Brussels, 14 February 2006.
For the Council
The President
K.-H. Grasser
[1] OJ L 352, 30.12.2002, p. 3.
[2] OJ L 84, 2.4.2005, p. 21.
[3] OJ L 352, 30.12.2002, p. 1083.
--------------------------------------------------
