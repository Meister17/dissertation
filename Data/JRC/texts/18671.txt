COMMISSION DIRECTIVE 93/111/EC of 10 December 1993 amending Directive 93/10/EEC relating to materials and articles made of regenerated cellulose film intended to come into contact with foodstuffs
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 89/109/EEC of 21 December 1988 on the approximation of the laws of the Member States relating to materials and articles intended to come into contact with foodstuffs (1), and in particular Article 3 thereof,
Whereas Article 2 of Commission Directive 92/15/EEC (2) prohibits, as from 1 July 1994, the trade in and use of regenerated cellulose film which is intended to come into contact with foodstuffs and which does not comply with Council Directive 83/229/EEC (3);
Whereas Article 5 of Commission Directive 93/10/EEC (4) prohibits, as from 1 January 1994, the trade in and use of the same products which comply neither with this Directive nor with Directive 83/229/EEC;
Whereas Article 5 of Directive 93/10/EEC should therefore be amended to eliminate the inconsistency between the dates specified in Directives 92/15/EEC and 93/10/EEC;
Whereas the measures provided for in this Directive are in accordance with the opinion of the Standing Committee on Foodstuffs,
HAS ADOPTED THIS DIRECTIVE:
Article 1
The second indent of Article 5 (1) of Directive 93/10/EEC is replaced by the following:
'- prohibit, as from 1 January 1994, the trade in and use of regenerated cellulose film which is intended to come into contact with foodstuffs and which complies with neither this Directive nor Directive 83/229/EEC, other than film which Directive 92/15/EEC prohibits as from 1 July 1994.'
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
Done at Brussels, 10 December 1993.
For the Commission
Martin BANGEMANN
Member of the Commission
(1) OJ No L 40, 11. 2. 1989, p. 38.
(2) OJ No L 102, 16. 4. 1992, p. 44.
(3) OJ No L 123, 11. 5. 1983, p. 31.
(4) OJ No L 93, 17. 4. 1993, p. 27.
