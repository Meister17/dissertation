Commission Regulation (EC) No 1541/2006
of 13 October 2006
fixing the coefficient for establishing the withdrawal threshold referred to in Article 3 of Regulation (EC) No 493/2006
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 318/2006 of 20 February 2006 on the common organisation of the markets in the sugar sector [1],
Having regard to Commission Regulation (EC) No 493/2006 of 27 March 2006 laying down transitional measures within the framework of the reform of the common organisation of the markets in the sugar sector, and amending Regulations (EC) No 1265/2001 and (EC) No 314/2002 [2], and in particular Article 3(2)(b) thereof,
Whereas:
(1) Article 3 of Regulation (EC) No 493/2006 lays down that for each undertaking, the share of the production of sugar, isoglucose or inulin syrup in the 2006/07 marketing year which is produced under the quotas set in Annex IV to that Regulation and which exceeds a certain threshold shall be considered withdrawn within the meaning of Article 19 of Regulation (EC) No 318/2006.
(2) For the purposes of establishing the threshold in question, a coefficient must be fixed no later than 15 October 2006 by dividing the sum of the quotas renounced in the 2006/07 marketing year in the Member State concerned, under Article 3 of Council Regulation (EC) No 320/2006 of 20 February 2006 establishing a temporary scheme for the restructuring of the sugar industry in the Community and amending Regulation (EC) No 1290/2005 on the financing of the common agricultural policy [3], by the sum of the quotas fixed for that Member State in Annex IV to Regulation (EC) No 493/2006.
(3) In fixing that coefficient, account should be taken of the Commission Communication of 29 September 2006 on the estimated availability of financial resources for granting of restructuring aid for the 2006/07 marketing year, in the framework of the implementation of Council Regulation (EC) No 320/2006 establishing a temporary scheme for the restructuring of the sugar industry in the Community [4], which was adopted in accordance with Article 10(2) of Commission Regulation (EC) No 968/2006 of 27 June 2006 laying down detailed rules for the implementation of Council Regulation (EC) No 320/2006 establishing a temporary scheme for the restructuring of the sugar industry in the Community [5].
(4) The coefficient for establishing the withdrawal threshold for the 2006/07 marketing year should therefore be fixed,
HAS ADOPTED THIS REGULATION:
Article 1
The coefficient referred to in Article 3(2)(b) of Regulation (EC) No 493/2006 shall be fixed by Member State as follows:
(a) Belgium: 0,1945;
(b) Spain: 0,0863;
(c) France (mainland): 0,0074;
(d) Ireland: 1,0000;
(e) Italy: 0,4936;
(f) Netherlands: 0,0848;
(g) Portugal: 0,4422;
(h) Sweden: 0,1156;
(i) other Member States: 0,0000.
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 13 October 2006.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 58, 28.2.2006, p. 1.
[2] OJ L 89, 28.3.2006, p. 11. Regulation as amended by Regulation (EC) No 769/2006 (OJ L 134, 20.5.2006, p. 19).
[3] OJ L 58, 28.2.2006, p. 42.
[4] OJ C 234, 29.9.2006, p. 9.
[5] OJ L 176, 30.6.2006, p. 32.
--------------------------------------------------
