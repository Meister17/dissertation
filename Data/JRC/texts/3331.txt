Euro exchange rates [1]
28 November 2005
(2005/C 297/01)
| Currency | Exchange rate |
USD | US dollar | 1,1726 |
JPY | Japanese yen | 140,39 |
DKK | Danish krone | 7,4570 |
GBP | Pound sterling | 0,68550 |
SEK | Swedish krona | 9,4585 |
CHF | Swiss franc | 1,5471 |
ISK | Iceland króna | 74,45 |
NOK | Norwegian krone | 7,8700 |
BGN | Bulgarian lev | 1,9560 |
CYP | Cyprus pound | 0,5735 |
CZK | Czech koruna | 28,963 |
EEK | Estonian kroon | 15,6466 |
HUF | Hungarian forint | 251,03 |
LTL | Lithuanian litas | 3,4528 |
LVL | Latvian lats | 0,6962 |
MTL | Maltese lira | 0,4293 |
PLN | Polish zloty | 3,8870 |
RON | Romanian leu | 3,6547 |
SIT | Slovenian tolar | 239,51 |
SKK | Slovak koruna | 37,751 |
TRY | Turkish lira | 1,5960 |
AUD | Australian dollar | 1,5949 |
CAD | Canadian dollar | 1,3716 |
HKD | Hong Kong dollar | 9,0928 |
NZD | New Zealand dollar | 1,6797 |
SGD | Singapore dollar | 1,9890 |
KRW | South Korean won | 1217,86 |
ZAR | South African rand | 7,6427 |
CNY | Chinese yuan renminbi | 9,4775 |
HRK | Croatian kuna | 7,4018 |
IDR | Indonesian rupiah | 11790,49 |
MYR | Malaysian ringgit | 4,432 |
PHP | Philippine peso | 63,660 |
RUB | Russian rouble | 33,8050 |
THB | Thai baht | 48,387 |
[1] Source: reference exchange rate published by the ECB.
--------------------------------------------------
