Decision of the EEA Joint Committee
No 119/2004
of 24 September 2004
amending Annex I (Veterinary and phytosanitary matters) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex I to the Agreement was amended by Decision of the EEA Joint Committee No 95/2004 of 9 July 2004 [1].
(2) Commission Regulation (EC) No 1874/2003 of 24 October 2003 approving the national scrapie control programmes of certain Member States, and defining additional guarantees, and granting derogations concerning breeding programmes for TSE resistance in sheep pursuant to Decision 2003/100/EC [2] is to be incorporated into the Agreement.
(3) Commission Decision 2003/828/EC of 25 November 2003 on protection and surveillance zones in relation to bluetongue [3] is to be incorporated into the Agreement.
(4) Commission Decision 2003/859/EC of 5 December 2003 amending Decision 2002/106/EC as regards the establishment of a classical swine fever discriminatory test [4] is to be incorporated into the Agreement.
(5) Commission Decision 2003/886/EC of 10 December 2003 laying down criteria for information to be provided in accordance with Council Directive 64/432/EEC [5] is to be incorporated into the Agreement.
(6) Decision 2003/828/EC repeals Decision 2003/218/EC [6], which is incorporated into the Agreement and which is consequently to be deleted from the Agreement.
(7) This Decision is not to apply to Iceland and Liechtenstein,
HAS DECIDED AS FOLLOWS:
Article 1
Chapter I of Annex I to the Agreement shall be amended as specified in the Annex to this Decision.
Article 2
The texts of Regulation (EC) No 1874/2003 and Decisions 2003/828/EC, 2003/859/EC and 2003/886/EC in the Norwegian language, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 25 September 2004 provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [7].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 24 September 2004.
For the EEA Joint Committee
The President
Kjartan Jóhannsson
[1] OJ L 376, 23.12.2004, p. 14.
[2] OJ L 275, 25.10.2003, p. 12.
[3] OJ L 311, 27.11.2003, p. 41.
[4] OJ L 324, 11.12.2003, p. 55.
[5] OJ L 332, 19.12.2003, p. 53.
[6] OJ L 82, 29.3.2003, p. 35.
[7] No constitutional requirements indicated.
--------------------------------------------------
ANNEX
to Decision of the EEA Joint Committee No 119/2004
Chapter I of Annex I to the Agreement shall be amended as specified below:
1. the following shall be added in point 23 (Commission Decision 2002/106/EC) in part 3.2:
", as amended by:
- 32003 D 0859: Commission Decision 2003/859/EC of 5 December 2003 (OJ L 324, 11.12.2003, p. 55).";
2. the following point shall be inserted after point 29 (Commission Decision 2003/466/EC) in part 3.2:
"30. 32003 D 0828: Commission Decision 2003/828/EC of 25 November 2003 on protection and surveillance zones in relation to bluetongue (OJ L 311, 27.11.2003, p. 41).";
3. point 27 (Commission Decision 2003/218/EC) in part 3.2. shall be deleted;
4. the following point shall be inserted after point 73 (Commission Decision 2003/466/EC) in part 4.2:
"74. 32003 D 0886: Commission Decision 2003/886/EC of 10 December 2003 laying down criteria for information to be provided in accordance with Council Directive 64/432/EEC (OJ L 332, 19.12.2003, p. 53).";
5. the following point shall be inserted after point 20 (Commission Decision 2003/100/EC) in part 7.2:
"21. 32003 R 1874: Commission Regulation (EC) No 1874/2003 of 24 October 2003 approving the national scrapie control programmes of certain Member States, and defining additional guarantees, and granting derogations concerning breeding programmes for TSE resistance in sheep pursuant to Decision 2003/100/EC (OJ L 275, 25.10.2003, p. 12)."
--------------------------------------------------
