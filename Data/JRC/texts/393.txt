Council Regulation (EC) No 1672/2000
of 27 July 2000
amending Regulation (EC) No 1251/1999 establishing a support system for producers of certain arable crops, to include flax and hemp grown for fibre
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Articles 36 and 37 thereof,
Having regard to the proposal from the Commission(1),
Having regard to the opinion of the European Parliament(2),
Having regard to the opinion of the Economic and Social Committee(3),
Having regard to the opinion of the Committee of the Regions(4),
Whereas:
(1) The common agricultural policy aims to attain the objectives referred to in the Treaty, taking account of the market situation.
(2) The flax and hemp sector has undergone profound changes since the entry into force of Council Regulation (EEC) No 1308/70 of 29 June 1970 on the common organisation of the market in flax and hemp(5). In addition to the traditional production of long flax fibre for textiles and the traditional uses of hemp fibres, flax and hemp are now also being grown for a new market in short fibres. Since these short fibres can be used for new materials, their production should be encouraged in order also to promote innovative markets offering a future.
(3) Given its attractiveness, the aid provided for in Regulation (EEC) No 1308/70 has given rise, in some Member States, to purely speculative production. Steps taken to combat this phenomenon have further complicated the legislation governing this sector and have not always had the desired success.
(4) In order to solve the problems facing the market in flax and hemp grown for fibre, the amount of aid granted to the growers concerned should be comparable to that for competitor crops. To that end, and with a view to simplifying the applicable legislation, these crops should be included in the support system for producers of certain arable crops established by Regulation (EC) No 1251/1999(6). Moreover, where there is a need to ensure continued production, Council Regulation (EC) No 1673/2000 of 27 July 2000 on the common organisation of the market in flax and hemp grown for fibre(7) provides for aid for processing flax and hemp straw. Aid for processing should lead to an increase in the purchase price of flax and hemp straw and make production more profitable for producers.
(5) To ensure a smooth transition to the level of support granted for cereals and to solve the current problems caused by the existence of different aid schemes for fibre flax and seed flax, the payments for flax and hemp grown for fibre should be the same as those granted for linseed, which must themselves be aligned with those for cereals by the 2002/2003 marketing year. Additional aid in Finland and Sweden, as envisaged for competing crops in Article 4 of Regulation (EC) No 1251/1999, is also justified in the case of flax and hemp.
(6) To take account of the recent establishment of flax and hemp production in particular near processing plants, eligibility for the scheme laid down in Regulation (EC) No 1251/1999 should be extended to cover the areas and crops in question.
(7) To avert the danger that the aims of Regulation (EC) No 1251/1999 might be circumvented, the granting of the per-hectare aid for flax and hemp grown for fibre should be made subject to certain conditions as regards cultivation.
(8) Specific measures should be laid down for hemp, to ensure that illegal crops cannot be hidden among the crops eligible for area payments, thereby disturbing the common market organisation for hemp. Provision must therefore be made for area payments to be granted only for areas sown to varieties of hemp offering certain guarantees with regard to the psychotropic substance content.
(9) In order that the quantities eligible for straw-processing aid under Regulation (EC) No 1673/2000 can be monitored, straw production must be linked to the area on which it is grown and producers should have imposed on them obligations which are reciprocal to those imposed on the producers involved,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 1251/1999 is hereby amended as follows:
1. Article 4(2) shall be replaced by the following:
"2. The calculation mentioned in paragraph 1 shall be made using the average cereals yield. However, where maize is treated separately, the 'maize' yield shall be used for maize and the 'cereals other than maize' yield shall be used for cereals, oilseeds, linseed and flax and hemp grown for fibre."
2. In the first subparagraph of Article 4(3), the words "for linseed" shall be replaced by "for linseed and flax and hemp grown for fibre".
3. Article 4(4) shall be replaced by the following:
"4. In Finland, and in Sweden north of the 62nd Parallel and some adjacent areas affected by comparable climatic conditions rendering agricultural activity particularly difficult, a supplementary amount to the area payment of EUR 19 per tonne, multiplied by the yield utilised for the area payments, shall be applied for cereals, oilseeds, linseed and flax and hemp grown for fibre."
4. The following Article shall be added:
"Article 5a
1. For flax and hemp grown for fibre, the area payment shall be made only, depending on circumstances, when the contract is concluded or commitment made as referred to in Article 2(1) of Regulation (EC) No 1673/2000.
For hemp grown for fibre, the area payment shall also be made only where the varieties used have a tetrahydrocannabinol content not exceeding 0,2 %.
2. Member States shall establish a system for verifying the tetrahydrocannabinol content of the crops grown on at least 30 % of the areas of hemp grown for fibre for which area payment applications have been made. However, if a Member State introduces a system of prior approval for such cultivation, the minimum shall be 20 %."
5. The first subparagraph of Article 7 shall be replaced by the following:"Applications for payments may not be made in respect of land which, on 31 December 1991, was under permanent pasture, permanent crops or trees or was used for non-agricultural purposes. However, applications for payments for areas used for growing flax or hemp for fibre and, if appropriate, for obligatory set-aside relating to it may be presented for land which benefited from aid granted under Council Regulation (EEC) No 1308/70 of 29 June 1970 on the common organisation of the market in flax and hemp(8) during at least one of the marketing years from 1998/1999 to 2000/2001."
6. Article 9 shall be amended as follows:
(a) in the first subparagraph, the following indents shall be inserted after the sixth indent:
"- with regard to flax and hemp grown for fibre, those relating to the arrangements for contracts and to the commitment referred to in Article 5a(1),
- with regard to hemp grown for fibre, those relating to the specific control measures and methods for determining tetrahydrocannabinol levels.";
(b) the first indent of the second subparagraph shall be replaced by the following:
"- either make the granting of payments subject to the use of:
(i) specific seeds;
(ii) certified seed in the case of durum wheat and flax and hemp grown for fibre;
(iii) certain varieties in the cases of oilseeds, durum wheat, linseed and flax and hemp grown for fibre,
- or provide for the possibility for Member States to make the grant of payments subject to such conditions,".
7. Point IV in Annex I shall be replaced by the following:
">TABLE>"
Article 2
In accordance with the third subparagraph of Article 3(6) of Regulation (EC) No 1251/1999, Member States shall submit to the Commission any revisions of their regionalisation plans required for incorporating the data on flax and hemp for fibre by 1 October 2000 at the latest.
Article 3
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Communities.
It shall apply from the 2001/2002 marketing year.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 27 July 2000.
For the Council
The President
H. Védrine
(1) OJ C 56 E, 29.2.2000, p. 17.
(2) Opinion delivered on 6 July 2000 (not yet published in the Official Journal).
(3) OJ C 140, 18.5.2000, p. 3.
(4) Opinion delivered on 14 June 2000 (not yet published in the Official Journal).
(5) OJ L 146, 4.7.1970, p. 1. Regulation as last amended by Regulation (EC) No 2702/1999 (OJ L 327, 14.12.1999, p. 7).
(6) OJ L 160, 26.6.1999, p. 1. Regulation as amended by Regulation (EC) No 2704/1999 (OJ L 327, 21.12.1999, p. 12).
(7) See page 16 of this Official Journal.
(8) OJ L 146, 4.7.1970, p. 1. Regulation as last amended by Regulation (EC) No 2702/1999 (OJ L 327, 14.12.1999, p. 7).
