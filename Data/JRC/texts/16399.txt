COUNCIL REGULATION (EC) No 540/96 of 25 March 1996 amending Regulation (EC) No 3010/95 totally or partially suspending the customs duties applicable to certain products falling within Chapters 1 to 24 and Chapter 27 of the Combined Nomenclature originating in Malta and Turkey (1995)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 113 thereof,
Having regard to the proposal from the Commission,
Whereas Regulation (EC) No 3010/95 (1) totally or partially suspended the customs duties applicable to certain agricultural goods and to certain petroleum products originating in Turkey and Malta; whereas the validity of that Regulation ended on 31 December 1995;
Whereas existing trade relations should not be interrupted; whereas accordingly it appears appropriate to continue applying the said Regulation;
Whereas the tariff advantages granted in this context should at least be equivalent to those accorded by the Community to developing countries under the Generalized System of Preferences;
Whereas the Customs Union between the European Community and Turkey took effect as from 31 December 1995; whereas, as a result, it is appropriate to amend the Annexes to the aforementioned Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 3010/95 shall be amended as follows:
1. in the title, '(1995)` shall be deleted.
2. Article 1 shall be replaced by the following:
'Article 1
The following are allowed for importation into the Community at the customs rate indicated for each:
- starting from 1 January 1995, the products of Chapters 1 to 24 of the Combined Nomenclature, originating in Malta and Turkey, as listed in Annexes I and II,
- from 1 January 1995 to 30 December 1995, the petroleum products of Chapter 27 of the Combined Nomenclature, refined in Turkey, as listed in Annex III.`
3. the Annexes shall be amended as follows:
(a) in the heading in the last column of Annex I, 'Period 1. 7 to 31. 12. 1995` shall be replaced by 'Starting from 1. 7. 1995`;
(b) in Annex I, as regards order numbers 16.0500, 15.0510, 16.0520, 16.1070 and 16.2900, the table shall be replaced by the table in Annex I to this Regulation;
(c) Annex II shall be replaced by the Annex in Annex II to this Regulation;
(d) Annex III shall be deleted with effect from 31 December 1995.
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
It shall apply from 1 January 1995, except for Annex II, which shall apply from 31 December 1995.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 25 March 1996.
For the Council
The President
S. AGNELLI
(1) OJ No L 314, 28. 12. 1995, p. 1.
ANNEX I
>TABLE>
>TABLE>
ANNEX II
'ANNEX II
>TABLE>
