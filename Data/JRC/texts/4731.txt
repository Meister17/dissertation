Commission Regulation (EC) No 36/2005
of 12 January 2005
amending Annexes III and X to Regulation (EC) No 999/2001 of the European Parliament and of the Council as regards epidemio-surveillance for transmissible spongiform encephalopathies in bovine, ovine and caprine animals
(Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Regulation (EC) No 999/2001 of the European Parliament and of the Council of 22 May 2001 laying down rules for the prevention, control and eradication of certain transmissible spongiform encephalopathies [1], and in particular Article 23 thereof,
Whereas:
(1) Regulation (EC) No 999/2001 lays down rules for the monitoring of transmissible spongiform encephalopathies (TSEs) in bovine, ovine and caprine animals.
(2) In its opinion of 4 and 5 April 2002 on a strategy to investigate the possible presence of bovine spongiform encephalopathy (BSE) in small ruminants, the Scientific Steering Committee (SSC) recommended a strategy for such investigation concerning the Community’s small ruminant population.
(3) A panel of experts on strain typing has been assembled by the Community Reference Laboratory (CRL) for TSEs for further defining the strategy recommended by the SSC. The strategy includes firstly implementing a screening method of all confirmed TSE cases in small ruminants at the level of the national reference laboratories. Secondly, a ring trial with at least three different methods in selected laboratories under the heading of the CRL to be carried out on all cases in which the first screening test could not exclude BSE. Finally, mouse strain typing is required if the outcome of the molecular typing methods needs confirmation.
(4) It is necessary to ensure that brain material of an optimal quality and in sufficient quantity from positive scrapie cases is delivered to the laboratories carrying out confirmatory examinations.
(5) When molecular typing of confirmed scrapie cases reveals a BSE-like or unusual isolate, it is desirable that the competent authority should have access to brain material from other infected animals on the holding, to further assist the investigation of the case.
(6) Four laboratories have successfully participated in a ring trial conducted by the CRL between July 2003 and March 2004 to test their proficiency in using molecular typing methods. The CRL should organise proficiency testing for other laboratories in the use of one of these molecular typing methods before April 2005.
(7) In the meantime, in view of the necessity to extend and accelerate the monitoring of caprine animals following a suspect case found in a goat, and considering the information forwarded to the CRL panel of experts by the laboratories of certain Member States on their capacity to carry out molecular testing, those laboratories should be provisionally approved for such testing pending the results of the proficiency test.
(8) Member States are submitting monthly TSE reports on a voluntary basis in addition to the annual report required by Article 6(4) of Regulation (EC) No 999/2001. The information forwarded in the annual and monthly reports should be harmonised and additional information, in particular on the age distribution of tested bovine animals, should be provided in order to evaluate the prevalence of BSE in different age groups.
(9) Regulation (EC) No 999/2001 should therefore be amended accordingly.
(10) In view of the increasing urgency to differentiate BSE from scrapie, the amendments made by this Regulation should enter into force without delay.
(11) The measures provided for in this Regulation are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS REGULATION:
Article 1
Annexes III and X to Regulation (EC) No 999/2001 are amended in accordance with the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 12 January 2005.
For the Commission
Markos Kyprianou
Member of the Commission
--------------------------------------------------
[1] OJ L 147, 31.5.2001, p. 1. Regulation as last amended by Commission Regulation (EC) No 1993/2004 (OJ L 344, 20.11.2004, p. 12).
--------------------------------------------------
+++++ ANNEX 1 +++++</br>
