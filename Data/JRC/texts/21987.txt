COUNCIL DIRECTIVE of 13 February 1978 on the performance of heat generators for space heating and the production of hot water in new or existing non-industrial buildings and on the insulation of heat and domestic hot-water distribution in new non-industrial buildings (78/170/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 103 thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament (1),
Having regard to the opinion of the Economic and Social Committee (2),
Whereas, in its resolution of 17 September 1974 concerning a new energy policy strategy for the Community (3), the Council adopted the objective of a reduction of the rate of growth of internal consumption by measures for using energy rationally and economically without jeopardizing social and economic growth objectives;
Whereas, in its resolution of 17 December 1974 on a Community action programme on the rational utilization of energy (4), the Council noted that, in its communications to the Council entitled "Rational utilization of energy", the Commission had drawn up a Community action programme in this field;
Whereas any improvement in the rational use of energy is generally beneficial to the environment;
Whereas the sector concerned with heating systems in buildings lends itself to such measures;
Whereas recommendation 76/493/EEC (5), related to the heating systems of existing buildings;
Whereas in the case of new heating systems it is necessary to achieve energy savings as soon as possible which will have an influence on total energy consumption as and when the systems are installed;
Whereas, to this end, a Directive should be adopted to provide a general framework within which the Member States would jointly explore energy saving methods designed to lessen the impact of the supply difficulties referred to in Article 103 (4) of the Treaty;
Whereas heat generators for space heating and the production of domestic hot water in new or existing non-industrial buildings should be inspected at the stage of manufacture or at the time of installation;
Whereas it should be made compulsory in new non-industrial buildings to provide, in economically justifiable conditions, thermal insulation both for generators and for the system whereby the heated fluids are distributed;
Whereas the Commission should receive regular information on the implementing measures adopted and the results obtained or anticipated;
Whereas the implementing measures adopted for this Directive should incorporate the measures adopted for the approximation of the laws of the Member States in the fields concerned by this Directive and should be directed towards facilitating the harmonization and standardization work in progress or to be undertaken in these fields both at Community and international level,
HAS ADOPTED THIS DIRECTIVE:
Article 1
1. Member States shall take all necessary measures to ensure that all new heat generators for space heating and/or the production of hot water in new or existing non-industrial buildings comply with minimum performance requirements.
In the case of generators capable of using various forms of energy, the minimum performance requirements must relate to each form of energy used. (1)OJ No C 266, 7.11.1977, p. 55. (2)OJ No C 287, 30.11.1977, p. 9. (3)OJ No C 153, 9.7.1975, p. 1. (4)OJ No C 153, 9.7.1975, p. 5. (5)OJ No L 140, 28.5.1976, p. 12.
The term heat generator shall, in particular, mean hot-water boilers, steam boilers, air heaters, including components and especially the associated firing equipment appropriate to the type of fossil fuel used. Combined electricity/heat generators used in buildings shall also be regarded as heat generators ; for these, the minimum performance requirements must relate to the full energy output.
Electric heat generators with resistances and connections to a remote heating network shall be excluded.
Those appliances for which type-testing is not practicable will be the subject of a subsequent proposal after appropriate technical study.
2. Member States shall ensure that compliance with the minimum performance requirements is assured by an inspection carried out either at the stage of manufacture of the generator or at the time of installation.
3. Heat generators subject to inspection at the time of manufacture cannot be offered for sale unless they comply with the minimum performance requirements ; compliance with the relevant rules shall be certified by means of a data plate giving the following minimum details: - manufacturer's identity,
- type of heat generator and its year of manufacture,
- heat rating in kW for every type of energy foreseen,
- type and characteristics of the energy or energies used,
- maximum temperature of the heating fluid,
- confirmation of inspection and identification of the body which carried it out,
- consumption of each heat generator when working to rated capacity.
The term "heat rating" refers to the highest output that can be continuously supplied by the heat generator.
When a heat generator of a type subject to inspection at the time of manufacture is installed, the user shall be provided with written operating and maintenance instructions to enable him to obtain optimum efficiency. These instructions must have been inspected in the same way as the generator and include the main details of the findings of the inspection.
4. In the case of heat generators subject to inspection at the time of installation, energy losses must not exceed the levels laid down by the Member States.
Article 2
Member States shall take all necessary measures to ensure that economically justifiable insulation of the distribution and storage system is made compulsory in new non-industrial buildings, both as regards heating fluid and domestic hot water.
These provisions shall also apply to systems connected to a remote-heating network.
They shall also apply to new heat generators, including electric systems for heating water, in all new or existing non-industrial buildings.
Article 3
The date from which a heat generator may no longer be installed, unless it complies with the minimum performance requirements in accordance with Article 1, shall be 1 January 1981.
The measures referred to in Article 2 shall apply from 1 July 1980.
Article 4
The Member States shall duly inform the Commission of measures taken within the scope of this Directive and of the results obtained or anticipated from such measures.
Article 5
This Directive shall in no way prejudge measures based on Article 100 of the Treaty.
Article 6
This Directive is addressed to the Member States.
Done at Brussels, 13 February 1978.
For the Council
The President
P. DALSAGER
