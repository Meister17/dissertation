Commission Decision
of 12 April 2005
authorising methods for grading pig carcases in Estonia
(notified under document number C(2005) 1099)
(Only the Estonian text is authentic)
(2005/308/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 3220/84 of 13 November 1984 determining the Community scale for grading pig carcases [1], and in particular Article 5(2) thereof,
Whereas:
(1) Article 2(3) of Regulation (EEC) No 3220/84 provides that the grading of pig carcases must be determined by estimating the content of lean meat in accordance with statistically proven assessment methods based on the physical measurement of one or more anatomical parts of the pig carcase; the authorisation of grading methods is subject to compliance with a maximum tolerance for statistical error in assessment; this tolerance was defined in Article 3 of Commission Regulation (EEC) No 2967/85 of 24 October 1985 laying down detailed rules for the application of the Community scale for grading pig carcases [2].
(2) The Government of Estonia has requested the Commission to authorise two methods for grading pig carcases and has submitted the results of its dissection trial which was executed before the day of accession, by presenting part two of the protocol provided for in Article 3 of Regulation (EEC) No 2967/85.
(3) The evaluation of this request has revealed that the conditions for authorising these grading methods are fulfilled.
(4) In Estonia commercial practice may require that the head, the fore feet and the tail are removed from the pig carcase; this should be taken into account in adjusting the weight for standard presentation.
(5) No modification of the apparata or the grading methods may be authorised except by means of a new Commission Decision adopted in the light of experience gained; for this reason, the present authorisation may be revoked.
(6) The measures provided for in this Decision are in accordance with the opinion of the Management Committee for Pigmeat,
HAS ADOPTED THIS DECISION:
Article 1
The use of the following methods is hereby authorised for grading pig carcases pursuant to Regulation (EEC) No 3220/84 in Estonia:
(a) the apparatus termed "Intrascope (Optical Probe)" and the assessment methods related thereto, details of which are given in Part 1 of the Annex;
(b) the apparatus termed "Ultra-FOM 300" and the assessment methods related thereto, details of which are given in Part 2 of the Annex.
As regards the apparatus "Ultra-FOM 300", referred to in the first paragraph, point (b), it is laid down that after the end of the measurement procedure it must be possible to verify on the carcase that the apparatus measured the values of measurement X2 and X4 on the site provided for in the Annex, Part 2, point 3. The corresponding marking of the measurement site must be made at the same time as the measurement procedure.
Article 2
Notwithstanding the standard presentation referred to in Article 2(1) of Regulation (EEC) No 3220/84, pig carcases may be presented in Estonia without the head, the fore feet and the tail before being weighed and graded. In order to establish quotations for pig carcases on a comparable basis, the recorded hot weight shall be multiplied by 1,07.
Article 3
Modifications of the apparatus or the assessment method shall not be authorised.
Article 4
This Decision is addressed to the Republic of Estonia.
Done at Brussels, 12 April 2005.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 301, 20.11.1984, p. 1. Regulation as last amended by Regulation (EC) No 3513/93 (OJ L 320, 22.12.1993, p. 5).
[2] OJ L 285, 25.10.1985, p. 39. Regulation as amended by Regulation (EC) No 3127/94 (OJ L 330, 21.12.1994, p. 43).
--------------------------------------------------
ANNEX
METHODS FOR GRADING PIG CARCASES IN ESTONIA
Part 1
INTRASCOPE (OPTICAL PROBE)
1. Grading of pig carcases shall be carried out by means of the apparatus termed "Intrascope (Optical Probe)".
2. The apparatus shall be equipped with a hexagonal-shaped probe of a maximum width of 12 millimetres (and of 19 millimetres at the blade at the top of the probe) containing a viewing window and a light source, a sliding barrel calibrated in millimetres, and having an operating distance of between 3 and 45 millimetres.
3. The lean meat content of the carcase shall be calculated according to the following formula:
= 69,09083 – 0,74785X
Where:
the estimated percentage of lean meat in the carcase,
X= the thickness of back-fat (including rind) in millimetres, measured at 7 centimetres off the midline of the carcase at the last rib.
The formula shall be valid for carcases weighing between 60 and 120 kilograms.
Part 2
ULTRA-FOM 300
1. Grading of pig carcases shall be carried out by means of the apparatus termed "Ultra-FOM 300".
2. The apparatus shall be equipped with an ultrasonic probe at 3,5 MHz (Krautkrämer MB 4 SE). The ultrasonic signal is digitised, stored and processed by a microprocessor.
The results of the measurements shall be converted into estimated lean meat content by means of the Ultra-FOM apparatus itself.
3. The lean meat content of the carcase should be calculated according to the following formula:
= 64,19701 – 0,39379X2 + 0,08082X3 – 0,33910X4
Where:
the estimated percentage of lean meat in the carcase,
X2= the thickness of back-fat (including rind) in millimetres, measured at 7 cm off the midline of the carcase, at the last rib,
X3= the thickness of muscle in millimetres, measured at the same time and in the same place as X2,
X4= the thickness of back-fat (including rind) in millimetres, measured at 7 cm off the midline of the carcase, between the third and the fourth last rib.
The formula shall be valid for carcases weighing between 60 and 120 kilograms.
--------------------------------------------------
