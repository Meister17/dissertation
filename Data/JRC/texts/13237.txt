Action brought on 21 September 2006 — Commission of the European Communities
v Kingdom of Spain
Parties
Applicant: Commission of the European Communities (represented by: N. Yerrell and R. Vidal Puig, acting as Agents)
Defendant: Kingdom of Spain
Form of order sought
- declare that, by failing to bring into force the provisions necessary to comply with Directive 2002/15/EC of the European Parliament and of the Council of 11 March 2002 on the organisation of the working time of persons performing mobile road transport activities [1] and, in any event, by failing to communicate them to the Commission, the Kingdom of Spain has failed to fulfil its obligations under that directive;
- order the Kingdom of Spain to pay the costs.
Pleas in law and main arguments
The period for transposition of Directive 2002/15/EC expired on 23 March 2005.
[1] OJ L 80 of 23.3.2002, p. 35.
--------------------------------------------------
