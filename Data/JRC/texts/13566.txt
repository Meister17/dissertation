Council Common Position 2006/755/CFSP
of 7 November 2006
concerning the temporary reception by Member States of the European Union of certain Palestinians
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty on European Union, and in particular Article 15 thereof,
Whereas:
(1) On 14 November 2005, the Council adopted Common Position 2005/793/CFSP [1] concerning the temporary reception by Member States of the European Union of certain Palestinians, which provided for an extension of the validity of their national permits for entry into, and stay in, the territory of the Member States referred to in Common Position 2002/400/CFSP [2] for a further period of 12 months.
(2) On the basis of an evaluation of the application of Common Position 2002/400/CFSP, the Council considers it appropriate that the validity of those permits be extended for a further period of 12 months,
HAS ADOPTED THIS COMMON POSITION:
Article 1
The Member States referred to in Article 2 of Common Position 2002/400/CFSP shall extend the validity of the national permits for entry and stay granted pursuant to Article 3 of that Common Position for a further period of 12 months.
Article 2
The Council shall evaluate the application of Common Position 2002/400/CFSP within six months of the adoption of this Common Position.
Article 3
This Common Position shall take effect on the day of its adoption.
Article 4
This Common Position shall be published in the Official Journal of the European Union.
Done at Brussels, 7 November 2006.
For the Council
The President
E. Heinäluoma
[1] OJ L 299, 16.11.2005, p. 80.
[2] OJ L 138, 28.5.2002, p. 33. Common Position as last amended by Common Position 2004/493/CFSP (OJ L 181, 18.5.2004, p. 24).
--------------------------------------------------
