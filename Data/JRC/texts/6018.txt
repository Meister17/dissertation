Council Decision 2005/680/CFSP
of 12 August 2005
concerning the conclusion of the Agreement between the European Union and the Democratic Republic of the Congo on the status and activities of the European Union Police Mission in the Democratic Republic of the Congo (EUPOL Kinshasa)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty on European Union, and in particular Article 24 thereof,
Having regard to the recommendation from the Presidency,
Whereas:
(1) On 9 December 2004, the Council adopted Joint Action 2004/847/CFSP on the European Union Police Mission in the Democratic Republic of the Congo (EUPOL Kinshasa) [1].
(2) Article 13 of the Joint Action provides that the status of EUPOL Kinshasa staff in the Democratic Republic of the Congo, including where appropriate the privileges, immunities and further guarantees necessary for the completion and smooth functioning of EUPOL Kinshasa shall be agreed in accordance with the procedure laid down in Article 24 of the Treaty.
(3) Following the authorisation of 24 January 2005 given by the Council to the Secretary-General/High Representative for the Common Foreign and Security Policy, assisting the Presidency, to open negotiations on its behalf, the Secretary-General/High Representative for the Common Foreign and Security Policy, negotiated an Agreement with the Government of the Democratic Republic of the Congo on the status and activities of EUPOL Kinshasa.
(4) Notwithstanding Article 11(4) of the Agreement, procurement of goods and services should comply with the principles of transparency, proportionality, equal treatment and non-discrimination.
(5) The Agreement should be approved,
HAS DECIDED AS FOLLOWS:
Article 1
The Agreement between the European Union and the Democratic Republic of the Congo on the status and activities of the European Union Police Mission in the Democratic Republic of the Congo (EUPOL Kinshasa) is hereby approved on behalf of the Union.
The text of the Agreement is attached to this Decision.
Article 2
The President of the Council is hereby authorised to designate the person(s) empowered to sign the Agreement in order to bind the Union.
Article 3
This Decision shall be published in the Official Journal of the European Union [2].
Article 4
This Decision shall take effect on the day of its adoption.
Done at Brussels, 12 August 2005.
For the Council
The President
J. Straw
[1] OJ L 367, 14.12.2004, p. 30.
[2] The date of entry into force of the Agreement will be published in the Official Journal of the European Communities by the General Secretariat of the Council.
--------------------------------------------------
