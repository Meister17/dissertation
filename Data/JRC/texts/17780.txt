Application for authorisation to extract hydrocarbons in the Akkrum Fields
(2004/C 287/03)
Notice inviting applications for authorisation to extract hydrocarbons in the area of the former Akkrum concession on Dutch territory.
The Minister of Economic Affairs of the Kingdom of the Netherlands serves notice that an application for authorisation to extract hydrocarbons has been received for the area of the former Akkrum concession as described in Article 1 of the Royal Decree of 17 February 1969, No 30 (Government Gazette 1969, No 46) as subsequently extended with the area described in Article 1 of Royal Decree of 19 August 1981, No 25 (Government Gazette 1981, No 186).
With reference to Article 3(2) of Directive 94/22/EC of the European Parliament and of the Council of 30 May 1994 on the conditions for granting and using authorisations for the prospecting, exploration and production of hydrocarbons and the publication as described in Article 15 of the Mining Act (Bulletin of Acts and Decrees 2002, 542), the Minister of Economic Affairs invites interested parties to submit an application for authorisation to extract hydrocarbons for the area of the former Akkrum concession.
Applications may be submitted for 13 weeks after publication of this notice in the Official Journal of the European Union and should be sent to the Minister of Economic Affairs, for the attention of Director of Energy Production, specifying "personal", Prinses Beatrixlaan 5-7, The Hague. Applications submitted after this deadline will not be considered.
A decision on the applications will be taken not later than nine months after this period.
Further information can be requested by telephone ((31-70) 379 66 94).
--------------------------------------------------
