Commission Regulation (EC) No 457/2006
of 20 March 2006
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 21 March 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 20 March 2006.
For the Commission
J. L. Demarty
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 386/2005 (OJ L 62, 9.3.2005, p. 3).
--------------------------------------------------
ANNEX
to Commission Regulation of 20 March 2006 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code [1] | Standard import value |
07020000 | 052 | 110,3 |
204 | 54,2 |
212 | 102,0 |
624 | 101,8 |
999 | 92,1 |
07070005 | 052 | 139,2 |
999 | 139,2 |
07099070 | 052 | 131,7 |
204 | 50,4 |
999 | 91,1 |
08051020 | 052 | 68,9 |
204 | 43,6 |
212 | 53,1 |
220 | 45,2 |
400 | 60,8 |
448 | 37,8 |
624 | 61,8 |
999 | 53,0 |
08055010 | 052 | 65,0 |
624 | 67,8 |
999 | 66,4 |
08081080 | 388 | 101,4 |
400 | 114,1 |
404 | 102,5 |
508 | 82,7 |
512 | 79,2 |
524 | 78,8 |
528 | 77,8 |
720 | 92,1 |
999 | 91,1 |
08082050 | 388 | 81,5 |
512 | 73,2 |
528 | 73,4 |
720 | 48,1 |
999 | 69,1 |
[1] Country nomenclature as fixed by Commission Regulation (EC) No 750/2005 (OJ L 126, 19.5.2005, p. 12). Code "999" stands for "of other origin".
--------------------------------------------------
