COUNCIL DECISION of 25 February 1991 establishing a Committee on monetary, financial and balance of payments statistics (91/115/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to the draft decision submitted by the Commission (1),
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas Council Decision 90/141/EEC of 12 March 1990 on the attainment of progressive convergence of economic policies and performance during stage one of economic and monetary union (4) entails the need for the availability of consistent indicators, particularly in the monetary, financial and balance of payments areas;
Whereas Council Decision 90/142/EEC of 12 March 1990 amending Decision 64/300/EEC on cooperation between the central banks of the Member States of the European Economic Community (5) aims in particular at promoting the coordination of monetary policy which necessarily depends on common surveillance indicators;
Whereas there is therefore a need to establish, as a section of the multiannual statistical programme of the Commission, a multiannual work programme in the field of monetary, financial and balance of payments statistics;
Whereas by Decision 89/382/EEC, Euratom (6) the Council set up a Committee on the Statistical Programme of the European Communities, comprising representatives of the Member States statistical institutes, to ensure close cooperation between Member States and the Commission whilst the statistical programme is established;
Whereas, in Member States, monetary and banking statistics are drawn up by central banks, and financial and balance of payments statistics are drawn up by various institutions, including central banks;
Whereas there is at present no body which ensures close cooperation between Member States and the Commission in the fields of monetary, financial and balance of payments statistics on which the principal national institutions concerned are represented;
Whereas it is necessary, in order to achieve such cooperation, in particular with a view to achieving Economic and Monetary Union, to establish a committee composed of representatives of these institutions, which is charged with aiding the Commission in drawing up and implementing the multiannual work programme relating to monetary, financial and balance of payments statistics;
Whereas, having regard to the specific role played in the Member States by the aforementioned institutions, it is necessary to allow the committee to choose its chairman;
Whereas there is, from the point of view of statistics, a close interdependence between the monetary, financial and balance of payments areas on the one hand, and certain other areas of economic statistics on the other hand;
Whereas the increased demand from the Member States and Community institutions for improved statistical information requires the enhancement of cooperation between the users and producers of monetary, financial and balance of payments statistics;
Whereas an examination of whether the Committee's tasks will meet the needs of any future European Central Bank System should be carried out in the framework of the achievement of Economic and Monetary Union,
HAS DECIDED AS FOLLOWS:
Article 1
A Committee on Monetary, Financial and Balance of Payments Statistics, hereinafter called 'the Committee', is hereby established.
Article 2
The Committee shall assist the Commission in drawing up and implementing the multiannual programme of work relating to monetary, financial and balance of payments statistics. The Committee shall, in particular, have the task of expressing opinions on the development and coordination of the monetary, financial and balance of payments statistics required for the policies implemented by the Council, the Commission, and the various committees assisting them.
The Committee may be asked to express opinions on the links between monetary, financial and balance of payments statistics on the one hand, and certain other economic statistics on the other, in particular those on which national accounts are based. The work of this Committee will be coordinated with that of the Statistical Programme Committee.
Article 3
The Commission, on its own initiative, and, should the occasion arise, following a request from the Council or from the committees which assist them, shall consult the Committee on:
(a) the establishment of multiannual Community programmes for monetary, financial and balance of payments statistics;
(b) the measures which the Commission intends to undertake to achieve the objectives referred to in the multiannual programmes for monetary, financial and balance of payments statistics and the resources and timetables involved;
(c) any other question, in particular questions of methodology, arising from the establishment or implementation of the Statistical Programme in the relevant fields.
The Committee may express opinions on its own initiative on any questions relating to the establishment or the implementation of statistical programmes in the monetary, financial and balance of payments fields.
Article 4
The Committee shall be composed of one, two or three representatives per Member State, coming from the institutions principally concerned with financial, monetary and balance of payments statistics, and of three representatives of the Commission.
In addition, one representative of the Committee of Governors of the Central Banks of the Community and one representative of the Monetary Committee may attend meetings of the Committee as observers.
Representatives of other organizations, as well as any persons able to contribute to discussions, may, on the decision of the Committee, participate in the Committee's meetings.
Article 5
The Committee shall elect its chairman by a simple majority of the Member States' votes. Its secretariat shall be provided by the Commission.
Article 6
The Committee shall draw up its rules of procedure.
Done at Brussels, 25 February 1991. For the Council
The President
J.-C. JUNCKER
(1) OJ No C 212, 25. 8. 1990, p. 5.
(2) OJ No C 324, 24. 12. 1990.
(3) OJ No C 31, 6. 2. 1991, p. 22.
(4) OJ No L 78, 24. 3. 1990, p. 23.
(5) OJ No L 78, 24. 3. 1990, p. 25.
(6) OJ No L 181, 28. 6. 1989, p. 47.
