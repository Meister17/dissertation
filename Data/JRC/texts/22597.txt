COUNCIL DIRECTIVE of 26 July 1971 on the approximation of the laws of the Member States relating to common provisions for both measuring instruments and methods of metrological control (71/316/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 100 thereof;
Having regard to the proposal from the Commission;
Having regard to the Opinion of the European Parliament 1;
Having regard to the Opinion of the Economic and Social Committee 2;
Whereas, in each Member State, mandatory provisions determine the technical characteristics of measuring instruments and the methods of metrological control ; whereas these requirements differ from one Member State to another ; whereas these differences hinder trade and may create unequal conditions of competition within the Community;
Whereas one of the objects of controls in each Member State is to give assurance to customers that quantities delivered correspond to the price paid ; whereas consequently the aim of this Directive is not to abolish these controls but to eliminate differences between rules in so far as these constitute a hindrance to trade;
Whereas these hindrances to the establishment and functioning of the common market can be reduced and eliminated if the same requirements apply in Member States, initially complementing national provisions in force and later, when the necessary conditions exist, replacing those provisions;
Whereas, even during the period when they coexist with national provisions, the Community requirements will enable firms to manufacture products which have uniform technical characteristics and can therefore be marketed and used throughout the Community after they have undergone EEC controls;
Whereas Community technical requirements for design and functioning should ensure that instruments continuously give measurements that are sufficiently accurate for their intended purpose;
Whereas compliance with technical requirements is usually confirmed by Member States before measuring instruments are placed on the market or used for the first time, and where appropriate when they are in service, such confirmation being carried out in particular by means of pattern approval and verification procedures ; whereas in order to achieve free movement of these instruments within the Community it is also necessary to provide for mutual recognition of controls among Member States and to establish appropriate EEC pattern approval and initial verification procedures for this purpose as well as EEC methods of metrological control, in accordance with this Directive and with the relevant separate directives;
Whereas the presence, on a measuring instrument or a product, of signs or marks showing that it has undergone the appropriate controls indicates that such instrument or product satisfies the relevant technical requirements of the Community, and therefore that when the instrument or product is imported or put into operation it is unnecessary to repeat the controls which have already been carried out;
Whereas national metrological rules cover numerous categories of measuring instruments and products ; whereas this Directive should lay down the general provisions dealing, in particular, with EEC pattern 1OJ No C 45, 10.5.1971, p. 26. 2OJ No C 36, 19.4.1971, p. 8.
approval and initial verification procedures, as well as EEC methods of metrological control ; whereas implementing directives covering the various categories of instruments and products will lay down the technical requirements as to design, functioning and accuracy, the control procedures and, where appropriate, the conditions under which Community technical requirements are to replace the national provisions in force;
HAS ADOPTED THIS DIRECTIVE:
CHAPTER I Basic principles
Article 1
1. No Member State may prevent, prohibit or restrict the placing on the market or entry into service of a measuring instrument (hereinafter called "instrument") or of ancillary equipment if that instrument or equipment bears the mark certifying initial verification or the sign certifying EEC pattern approval as provided for in Articles 10 and 11.
2. Member States shall attach the same value to EEC pattern approval and initial verification as to the corresponding national measures.
3. Member States may require EEC pattern approval or EEC initial verification for a category of instruments only if corresponding controls are laid down for instruments of the same category satisfying national requirements which have not been harmonised at Community level.
4. For the categories of instruments with which they are concerned, separate directives will specify the measurement characteristics of and the technical requirements for their design and functioning.
They may also specify: - whether in all Member States these instruments must undergo both EEC pattern approval and EEC initial verification or just one of these;
- the date on which national provisions conforming to the relevant separate directive are to replace completely the national provisions formerly applicable to new instruments of the same category.
CHAPTER II EEC pattern approval
Article 2
1. EEC pattern approval constitutes the admission of a manufacturer's instruments to EEC initial verification and, where the latter is not required, the authorisation to place them on the market and put them into service. If the separate directive applicable to a category of instruments exempts that category from EEC pattern approval, the instruments in that category shall be admitted directly to EEC initial verification.
2. In so far as their control equipment permits, Member States shall, at the request of the manufacturer or of his authorised representative, grant EEC pattern approval for every pattern of instrument and all ancillary equipment which satisfies the measurement characteristics and the technical design and functioning requirements laid down by the separate directive relating to the category of instrument in question.
3. Application for EEC pattern approval of a given pattern of instrument may be submitted in only one Member State.
4. A Member State which has granted EEC pattern approval shall take the necessary steps to ensure that it is kept informed of any modification or addition to the approved pattern. It shall inform the other Member States of such alterations.
Modifications or additions to an approved pattern must receive additional pattern approval from the Member State which granted EEC pattern approval, where such changes influence or might influence measurement results or the prescribed conditions for use of the instrument.
5. Member States shall grant EEC pattern approval in accordance with the provisions of this Chapter, of items 1 and 2 of Annex I, and of the relevant separate directives.
Article 3
When EEC pattern approval is granted for ancillary equipment, this approval shall specify: - the patterns of instrument to which this equipment may be attached or in which it may be incorporated;
- the general conditions for the overall functioning of the instruments for which it is approved.
Article 4
1. If the results of the examination described in item 2 of Annex I are satisfactory, the Member State which has carried out this examination shall draw up an EEC pattern approval certificate and forward it to the applicant. In the cases provided for in Article 11 or in a separate directive the applicant must affix on each instrument and all ancillary equipment conforming to the approved pattern the approval sign shown in this certificate ; in all other cases he shall be entitled to affix the approval sign in question.
2. The provisions relating to the certificate, to the approval sign, to the deposit if any of a specimen instrument, and to the announcement of the EEC pattern approval, are set out in items 3, 4 and 6 of Annex I.
Article 5
1. EEC pattern approval shall be valid for ten years. Approval can be extended for successive periods of ten years ; the number of instruments which may be manufactured in accordance with the approved pattern shall not be limited.
When EEC pattern approval is not extended, instruments already in service which conform to the provisions of this Directive shall be considered to be approved.
2. When normal approval or extension cannot be granted for certain instruments, a limited approval or extension may be granted after the other Member States have been informed and, where appropriate, consulted. In the case provided for in the third indent, prior consultation is mandatory if the place of installation is situated in a State other than the one issuing the EEC certificate of pattern approval. EEC pattern approval may be subject to the following restrictions: - limitation of the period of validity to less than ten years;
- limitation of the number of instruments which can be approved;
- an obligation to notify the competent authorities of the places of installation;
- limitation of use.
3. When new techniques which are not provided for in a separate directive are employed, limited EEC pattern approval may be granted after previous consultation with other Member States. This limited approval may be subject to the restrictions mentioned in paragraph 2 and to special conditions connected with the technique used.
This limited approval can, however, be granted only: - if the separate directive for the relevant category of instruments has already entered into force;
- if the maximum permissible errors specified in the separate directives are not exceeded.
Such limited approval shall be valid for not more than two years. It may be extended for a further period of three years.
4. If the Member State which granted limited EEC pattern approval referred to in paragraph 3 considers that a new technique has proved satisfactory, it shall apply for an adjustment of the relevant directive to technical progress, in accordance with the provisions of Articles 18 and 19.
Article 6
When EEC pattern approval is not required for a category of instruments which meets the requirements of a separate directive, the manufacturer may, on his own responsibility, affix to the instruments in this category the special sign described in item 3.3 of Annex I.
Article 7
1. The Member State which has granted EEC pattern approval may withdraw it: (a) if instruments for which this approval was granted do not conform to the approved pattern or to the provisions of the relevant separate directive;
(b) if the metrological requirements specified in the certificate of approval or the provisions of Article 5 (2) and (3) are not met.
2. The Member State which has granted EEC pattern approval must withdraw it if the instruments constructed according to an approved pattern reveal in service a defect of a general nature which makes them unsuitable for their intended use.
3. If that Member State is informed by another Member State of the occurrence of one of the cases covered by paragraphs 1 and 2, it shall likewise take the measures provided for in those paragraphs, after consulting with that other State.
4. The Member State which declares that the case referred to in paragraph 2 has arisen may forbid the placing on the market and putting into service of the instruments concerned until further notice. It shall immediately inform the other Member States and the Commission, stating the reasons on which its decision is based. The same procedure shall apply in the cases mentioned in paragraph 1, with respect to instruments which have been exempted from EEC initial verification, if the manufacturer, after due warning, does not bring the instruments into line with the approved pattern or with the requirements of the relevant separate directive.
5. If the Member State which granted the approval disputes that the case referred to in paragraph 2 of which it has been informed has arisen, or disputes that the measures taken in pursuance of paragraph 4 are justified, the Member States concerned shall endeavour to settle the dispute.
The Commission shall be kept informed and shall, where necessary, hold appropriate consultations for the purpose of reaching a settlement.
CHAPTER III Initial verification
Article 8
1. The EEC initial verification constitutes the inspection of a new or reconditioned instrument to confirm that it conforms to the approved pattern and/or to the requirements of the relevant separate directive ; it is certified by the EEC initial verification mark.
2. If they have the requisite equipment, Member States shall carry out EEC initial verification of instruments submitted as having the measurement characteristics and satisfying the technical construction and functioning requirements laid down by the separate directive on this category of instruments.
3. In the case of instruments bearing the EEC initial verification mark, the obligation imposed on Member States by Article 1 (1) shall last until the end of the year following that in which the EEC initial verification mark was affixed, unless separate directives make provision for a longer period.
Article 9
When an instrument is submitted for EEC initial verification, the Member State carrying out the examination shall determine: (a) whether the instrument belongs to a category exempt from EEC pattern approval and, if so, whether it satisfies the technical construction and functioning requirements laid down by the separate directive on this category of instruments;
(b) whether the instrument has received an EEC pattern approval and, if so, whether it conforms to the approved pattern.
The inspection carried out in the EEC initial verification relates in particular, in accordance with the relevant separate directive, to: - the measurement characteristics;
- the maximum permissible errors;
- the construction, in so far as this guarantees that the measurement characteristics are not likely to deteriorate to any great extent under normal conditions of use;
- the presence of prescribed inscriptions and the correct positioning of the stamp plates.
Article 10
1. When an instrument has successfully undergone EEC initial verification in accordance with Article 9 and items 1 and 2 of Annex II, Member States shall affix on the instrument a partial or final EEC verification mark in accordance with item 3 of Annex II.
2. The provisions relating to the designs and characteristics of the EEC verification marks are set out in item 3 of Annex II.
Article 11
When EEC initial verification is not required for a category of instruments which meet the requirements of a separate directive, the manufacturer shall, on his own responsibility, affix to the instruments in that category the special symbol described in item 3.4 of Annex I.
CHAPTER IV Provisions for both EEC pattern approval and EEC initial verification
Article 12
Member States shall take all necessary measures to prevent the use on instruments of marks or inscriptions liable to be confused with the EEC signs or marks.
Article 13
Each Member State shall notify the other Member States and the Commission of the services, agencies and institutes which are authorised to affix the marks referred to in Article 10.
Article 14
Member States may require that the prescribed inscriptions be drawn up in their official language or languages.
CHAPTER V Control of instruments in service
Article 15
1. If Member States control instruments which are in service and which bear EEC signs or marks, and if separate directives do not specify the way in which the controls are to be carried out and the maximum errors permitted in service, the control requirements, and in particular the maximum errors permitted in service, must bear the same ratio to those applied before instruments are put into service as the ratio applied to instruments complying with national technical requirements which have not been harmonised at Community level.
2. Notwithstanding the provisions of paragraph 1 of Article 1 (1), an instrument in service which bears EEC signs or marks but does not satisfy the requirements of the relevant separate Directive, in particular with regard to maximum permissible errors, may be banned from service in the same way as an instrument bearing national marks.
CHAPTER VI EEC methods of metrological control
Article 16
1. The harmonisation of methods of measurement and of metrological control and, where appropriate, of the means necessary for their application may be regulated by separate directives.
2. Harmonisation of the requirements for marketing certain products, in particular as regards the prescription, measurement and marking of pre-packed quantities, may also be covered by separate directives.
CHAPTER VII Adjustment of directives to technical progress
Article 17
The amendments necessary to keep: - Annexes I and II to this Directive ; and
- the technical annexes of separate directives dealing with the different categories of instruments, the legal units of measurement and EEC methods of metrological control,
in line with technical progress shall be made in accordance with the procedure laid down in Article 19.
Article 18
1. A committee hereinafter called the "Committee", is hereby set up to adjust to technical progress those directives which concern the elimination of technical barriers to trade in measuring instruments. It shall consist of representatives of the Member States with a representative of the Commission as Chairman.
2. The Committee shall adopt its own rules of procedure.
Article 19
1. Where the procedure laid down in this Article is to be followed, matters shall be referred to the Committee by the Chairman, either on his own initiative or at the request of the representative of a Member State.
2. The representative of the Commission shall submit to the Committee a draft of the measures to be adopted. The Committee shall deliver its Opinion of the draft within a time limit set by the Chairman having regard to the urgency of the matter. Opinions shall be adopted by a majority of twelve votes, the votes of Member States being weighted as provided in Article 148 (2) of the Treaty.
The Chairman shall not vote.
3. (a) The Commission shall adopt the measures envisaged where they are in accordance with the Opinion of the Committee.
(b) Where the measures envisaged are not in accordance with the Opinion of the Committee, or if no Opinion is adopted, the Commission shall without delay propose to the Council the measures to be adopted. The Council shall act by a qualified majority.
(c) If, within three months of the proposal being submitted to it, the Council has not acted, the proposed measures shall be adopted by the Commission.
CHAPTER VIII Final provisions
Article 20
All decisions taken pursuant to the provisions adopted in implementation of this Directive and of the separate directives on the instruments in question and refusing to grant or extend EEC pattern approval, withdrawing such approval, refusing to carry out EEC initial verification or prohibiting sale or use shall state the reasons on which they are based. Such refusal, withdrawal or prohibition shall be notified to the party concerned, who shall at the same time be informed of the remedies available to him under the laws in force in the Member States and of the time limits allowed for the exercise of such remedies.
Article 21
1. Member States shall put into force the laws, regulations and administrative provisions needed in order to comply with this Directive within eighteen months of its notification and shall forthwith inform the Commission thereof.
2. Member States shall ensure that the text of the main provisions of national law which they adopt in the field covered by this Directive are communicated to the Commission.
Article 22
This Directive is addressed to the Member States.
Done at Brussels, 26 July 1971.
For the Council
The President
A. MORO
ANNEX I EEC PATTERN APPROVAL
1. Application for EEC approval 1.1. The application and the correspondence relating to it shall be drawn up in an official language in accordance with the laws of the State to which the application is made. The Member State has the right to require the annexed documents should also be written in the same official language.
The applicant shall send simultaneously to all Member States a copy of his application.
1.2. The application shall contain the following information: - the name and address of the manufacturer or the firm, or of his or its authorised representative or of the applicant,
- the category of instrument,
- the intended use,
- the measurement characteristics,
- the commercial designation if any, or the type.
1.3. The application shall be accompanied by the documents necessary for its evaluation, in duplicate, and in particular: 1.3.1. A description in particular of: - the construction and operation of the instrument,
- the protecting arrangements ensuring correct working,
- the devices for regulation and adjustment,
- the intended locations for: - verification marks
- seals (where applicable).
1.3.2. General arrangement drawings and, where necessary, detailed drawings of important components.
1.3.3. A schematic drawing illustrating the principles of operation and, where necessary, a photograph.
1.4. The application shall be accompanied, where appropriate, by documents relevant to the national approvals already granted.
2. Examination for EEC approval 2.1. The examination shall comprise: 2.1.1. Study of the documents and an examination of the measurement characteristics of the pattern in the laboratories of the metrological service, in approved laboratories or at the place of manufacture, delivery or installation.
2.1.2. If the measurement characteristics of the pattern are known in detail, only an examination of the documents submitted.
2.2. The examination shall cover the entire performance of the instrument under normal conditions of use. Under such conditions, this instrument must maintain the measurement characteristics required.
2.3. The nature and scope of the examination mentioned in 2.1 may be specified by separate directives.
2.4. The metrological service may require the applicant to put at its disposal the standards and the appropriate means in material and assisting personnel for the performance of the approval tests.
3. EEC certificate and sign of approval 3.1. The certificate shall give the results of the examination of the pattern and shall specify the other requirements which must be complied with. It shall be accompanied by descriptions, drawings and diagrams necessary to identify the pattern and to explain its functioning. The sign of approval provided for in Article 4 of this Directive shall be a stylised letter i containing: - in the upper part, the distinguishing capital letter of the State which granted the approval (B for Belgium, D for the Federal Republic of Germany, F for France, I for Italy, L for Luxembourg, NL for the Netherlands) and the last two digits of the year of approval.
- in the lower part, a designation to be determined by the metrological service which granted approval (an identification number).
An example of this approval sign is shown in item 6.1.
3.2. In the case of EEC limited approval, a the letter P, having the same dimensions as the stylised letter i, shall be placed before this letter.
An example of this limited approval sign is shown in item 6.2.
3.3. The sign mentioned in Article 6 of this Directive is the same as the EEC approval sign, except that the stylised letter E is reversed symmetrically about a vertical axis.
An example of this sign is shown in item 6.3.
3.4. The sign mentioned in Article 11 of this Directive is the same as the EEC approval sign in a hexagon.
An example of this sign is shown in item 6.4.
3.5. The signs mentioned in the previous items and affixed by the manufacturer in accordance with the provisions of the Directive must be affixed at a visible point of each instrument and all ancillary equipment submitted for verification, and must be legible and indelible. If their affixation presents technical difficulties, exceptions may be made in separate directives or accepted after agreement among the metrological services of Member States has been reached.
4. Depositing of a sample instrument
In the cases mentioned by separate directives, the service which granted the approval may, if it considers this necessary, request the deposition of the sample instrument for which approval has been granted. Instead of this sample instrument, the service may authorise the depositing of parts of the instrument, of scale models or of drawings, and will mention this on the EEC certificate of approval.
5. Announcement of approval 5.1. EEC pattern approvals and EEC limited pattern approvals shall be published in a special Annex to the Official Journal of the European Communities. The same applies to EEC additional pattern approvals.
5.2. At the same time as the party concerned is notified, copies of the EEC certificate of approval shall be sent to the Commission and to the other Member States ; the latter can also obtain copies of the reports of the metrological examinations if they wish.
5.3. Withdrawal of EEC pattern approval and other communications concerning the extent and validity of EEC pattern approval shall also be subject to the announcement procedure mentioned in items 5.1 and 5.2.
5.4. A Member State which refuses to grant EEC pattern approval shall inform the other Member States and the Commission of its decision.
6. Signs relative to EEC pattern approval >PIC FILE= "T0011017">
ANNEX II EEC INITIAL VERIFICATION
1. General points 1.1. The EEC initial verification may be carried out in one or more stages (usually two).
1.2. Subject to the provisions of the separate directives: 1.2.1. The EEC initial verification shall be carried out in one stage on instruments which constitute a whole on leaving the factory, that is to say instruments which, theoretically, can be transferred to their place of installation without first having to be dismantled.
1.2.2. The EEC initial verification shall be carried out in two or more stages for instruments whose correct functioning depends on the conditions in which they are installed or used.
1.2.3. The first stage of the verification procedure must ensure, in particular, that the instrument conforms to the approved pattern or, in the case of instruments exempt from pattern approval, that they conform to the relevant provisions.
2. Place of the EEC initial verification 2.1. If the separate directives do not specify the place where verification is to be carried out, instruments which have to be verified in only one stage shall be verified at the place chosen by the metrological service concerned.
2.2. Instruments which have to be verified in two or more stages shall be verified by the metrological service territorially competent. 2.2.1. The last stage of a verification must be carried out at the place of installation.
2.2.2. The other verification stages of a verification shall be carried out as laid down in item 2.1.
2.3. In particular, when the verification takes place outside the office of verification the metrological service carrying out the verification may require the applicant: - to put at its disposal the standards and the appropriate means in material and assisting personnel for the performance of the verification.
- to provide a copy of the EEC certificate of approval.
3. EEC initial verification marks 3.1. Description of EEC initial verification marks. 3.1.1. Subject to the provisions of separate directives, EEC initial verification marks which are affixed in accordance with item 3.3 shall be as follows: 3.1.1.1. The final EEC verification mark shall be composed of two stamps: (a) the first consists of a letter "e" containing: - in the upper half, the distinguishing capital letter of the State where the initial verification is carried out (B for Belgium, D for the Federal Republic of Germany, F for France, I for Italy, L for Luxembourg, NL for the Netherlands), together, where necessary, with one or two numbers identifying a territorial subdivision:
- in the lower half, the identification number of the verifying agent or office;
(b) the second stamp shall consist of the last two digits of the year of the verification, in a hexagon.
3.1.1.2. The mark of EEC partial verification shall consist solely of the first stamp. It shall also serve as a seal.
3.2. Shape and dimensions of marks 3.2.1. The attached drawings show the shape, dimensions and outline of the letters and numbers for the EEC initial verification marks as laid down in item 3.1 ; the first two drawings show the various parts of the stamp, the third is an example of a stamp. The dimensions given in the drawings are relative values ; they are a function of the diameter of the circle described about the small letter "e" and about the field of the hexagon. The actual diameters of the circles described about the marks are 1 76 mm, 3 72 mm, 6 73 mm, 12 75 mm.
3.2.2. The metrological services of Member States shall mutually exchange the original drawings of the EEC initial verification marks, conforming to the models in the annexed drawings.
3.3. Affixing the marks 3.3.1. The final EEC verification mark shall be affixed at the appointed location on the instrument when the latter has been completely verified and is recognised to conform to EEC requirements.
3.3.2. The partial EEC verification mark shall be affixed: 3.3.2.1. When verification is made in several stages on the instrument or part of an instrument which fulfils the conditions laid down for operations other than those at the place of installation, at the place where the stamp-date is affixed or in any other place specified in the separate directives.
3.3.2.2. In all cases as a seal, in the places specified in the separate directives.
>PIC FILE= "T0011018">
>PIC FILE= "T0011019">
>PIC FILE= "T0011020">
