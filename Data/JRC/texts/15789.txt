Order of the President of the Court of 31 August 2006 — Commission of the European Communities
v Italian Republic
(Case C-81/06) [1]
(2006/C 294/73)
Language of the case: Italian
The President of the Court has ordered that the case be removed from the register.
[1] OJ C 86, 08.04.2006.
--------------------------------------------------
