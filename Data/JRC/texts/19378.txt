Commission Decision
of 2 October 2003
amending Decision 92/452/EEC establishing lists of embryo collection teams and embryo production teams approved in third countries for export of bovine embryos to the Community as regards the list for Canada
(notified under document number C(2003) 3427)
(Text with EEA relevance)
(2003/688/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 89/556/EEC of 25 September 1989 on animal health conditions governing intra-Community trade in and importation from third countries of embryos of domestic animals of the bovine species(1), as last amended by Regulation (EC) No 806/2003(2), and in particular Article 8 thereof,
Whereas:
(1) Commission Decision 92/452/EEC(3), as last amended by Decision 2003/391/EC(4), provides that Member States are only to import embryos from third countries where they have been collected, processed and stored by embryo collection teams and embryo production teams listed in that Decision. Canada has requested that an amendment be made to that list as regards entries for that country.
(2) Canada has provided guarantees regarding compliance with the appropriate rules set out in Directive 89/556/EEC and the team concerned has been officially approved for exports to the Community by the veterinary services of that country.
(3) Decision 92/452/EEC should therefore be amended accordingly.
(4) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
In the Annex to Decision 92/452/EEC, the row for the Canadian team No E 733 is replaced by the following:
">TABLE>"
Article 2
This Decision shall apply from 6 October 2003.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 2 October 2003.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 302, 19.10.1989, p. 1.
(2) OJ L 122, 16.5.2003, p. 1.
(3) OJ L 250, 29.8.1992, p. 40.
(4) OJ L 135, 3.6.2003, p. 25.
