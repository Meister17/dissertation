DECISION OF THE EXECUTIVE COMMITTEE
of 28 April 1999
on the Help Desk budget for 1999
(SCH/Com-ex (99) 3)
THE EXECUTIVE COMMITTEE,
Having regard to Article 132 of the Convention implementing the Schengen Agreement, hereinafter "the Schengen Convention"
Having regard to Article 119 of the abovementioned Convention,
HAS DECIDED AS FOLLOWS:
1. The draft budget for 1999 for the Help Desk shall be fixed at BEF 1880000 for 1999.
2. The contributions from the Parties shall be calculated according to the distribution key laid down in Article 119 of the Schengen Convention and pursuant to the Executive Committee Decision of 7 October 1997 (document SCH/Com-ex (97) 18).
3. This Decision shall constitute a mandate for the Benelux Economic Union, which is a party to this contract, to launch the call for contributions from the Parties.
Luxembourg, 28 April 1999.
The Chairman
C. H. Schapper
