COUNCIL REGULATION (EEC) No 561/79 of 5 March 1979 on the application of EEC-Tunisia Cooperation Council Decision No 3/78 amending the Protocol on the definition of the concept of originating products and methods of administrative cooperation to the Cooperation Agreement between the European Economic Community and the Republic of Tunisia
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 113 thereof,
Having regard to the proposal from the Commission,
Whereas the Cooperation Agreement between the European Economic Community and the Republic of Tunisia (1) was signed on 25 April 1976, and entered into force on 1 January 1979;
Whereas pursuant to Article 28 of the Protocol on the definition of the concept of originating products and methods of administrative cooperation, the EEC-Tunisia Cooperation Council has adopted Decision No 3/78 amending the Protocol as regards the rules of origin;
Whereas that Decision should be made operative in the Community,
HAS ADOPTED THIS REGULATION:
Article 1
Decision No 3/78 of the EEC-Tunisia Cooperation Council shall apply in the Community.
The text of the Decision is annexed to this Regulation.
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
It shall apply from 1 January 1979.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 5 March 1979.
For the Council
The President
J. FRANÇOIS-PONCET (1)OJ No L 265, 27.9.1978, p. 1.
