Judgement of the Court (Second Chamber) of 1 June 2006 (reference for a preliminary ruling from the Landgericht Berlin (Germany)) — innoventif Ltd
(Case C-453/04) [1]
Referring court
Landgericht Berlin
Parties to the main proceedings
Applicant: innoventif Ltd
Re:
Reference for a preliminary ruling — Landgericht Berlin — Interpretation of Article 43 EC and Article 48 EC — Entry in the register of companies of a branch of a company limited by shares established in another Member State subject to the payment of an advance on the costs of publication of the objects of the company as set out in its instrument of constitution
Operative part of the judgement
Article 43 EC and Article 48 EC do not preclude legislation of a Member State which makes registration, in the register of companies, of a branch of a limited company established in another Member State subject to the payment of an advance on the anticipated cost of the publication of the objects of the company as set out in its instrument of constitution.
[1] OJ C 6, 8.1.2005.
--------------------------------------------------
