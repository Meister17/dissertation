Commission Decision
of 20 October 2003
amending Decision 93/402/EEC as regards imports of fresh meat from Argentina
(notified under document number C(2003) 3827)
(Text with EEA relevance)
(2003/758/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 72/462/EEC of 12 December 1972 on health and veterinary inspection problems upon importation of bovine, ovine and caprine animals and swine, fresh meat or meat products from third countries(1), as last amended by Regulation (EC) No 807/2003(2), and in particular Article 14(3) thereof,
Having regard to Council Directive 2002/99/EC of 16 December 2002 laying down the animal health rules governing the production, processing, distribution and introduction of products of animal origin for human consumption(3) and in particular Article 8(4),
Whereas:
(1) Commission Decision 93/402/EEC of 10 June 1993 concerning animal health conditions and veterinary certification for imports of fresh meat from South American countries(4), as last amended by Decision 2003/658/EC(5), applies to Argentina, Brazil, Chile, Colombia, Paraguay and Uruguay.
(2) The Commission was informed of an outbreak of foot and mouth disease in Argentina in the department of General José de San Martin in the province of Salta and Decision 2003/658/EC was taken to suspend the importation of boned and matured bovine meat from the departments of General José de San Martin, Rivadavia, Oran, Iruya, and Santa Victoria in the province of Salta and from the department of Ramón Lista in the province of Formosa.
(3) However, the Argentinean veterinary authorities have informed the Commission Services on 19 September 2003 that they have enlarged the area under restriction in order to prevent a further spread of the disease in other parts of Argentina, and to create a buffer zone along the borders with other countries.
(4) The new area put under restriction by the Argentinean veterinary authorities covers the departments of Matacos and Bermejo in the province of Formosa, the department of Almirante Brown in the province of Chaco and the department of Patiño in the province of Formosa.
(5) From the information requested from and provided by the Argentinean veterinary authorities, it is not possible to fully assess the situation in the concerned areas because it is not clear exactly what measures are applied to the animals in these territories and what are the results of the sampling being carried out.
(6) In view of this uncertainty and in order to safeguard the animal health situation in the European Community it is prudent to temporarily suspend importation of boned and matured bovine meat on a regional basis from the whole territory of the provinces of Formosa, Chaco and Salta and in addition the province of Jujuy because of its geographical position.
(7) However, in the absence of clear evidence of disease in these additional areas of Argentina, the importation into the Community of boned and matured bovine fresh meat for human consumption and boned meat and offal for petfood slaughtered, produced and certified before the 8 October 2003 should be permitted.
(8) Decision 93/402/EEC should be amended accordingly.
(9) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
Decision 93/402/EEC is amended as follows:
1. Annex I is replaced by the text in the Annex I to this Decision,
2. Annex II is replaced by the text in the Annex II to this Decision.
Article 2
The Member States shall amend the measures they apply to imports so as to bring them into compliance with this Decision and they shall give immediate appropriate publicity to the measures adopted. They shall immediately inform the Commission thereof.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 20 October 2003.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 302, 31.12.1972, p. 28.
(2) OJ L 122, 16.5.2003, p. 36.
(3) OJ L 18, 23.1.2002, p. 11.
(4) OJ L 179, 22.7.1993, p. 11.
(5) OJ L 232, 18.9.2003, p. 59.
ANNEX I
"ANNEX I
Description of territories of South America established for animal health certification purposes
>TABLE>"
ANNEX II
"ANNEX II
Animal health guarantees requested on certification((The letters (A, B, C, D, E, F, G and H) appearing in the table, refer to the models of animal health guarantees as described in part 2 of Annex III, to be applied for each product and origin in accordance with Article 2; a dash (-) indicates that imports are not authorised.))
>TABLE>
HC Human consumption.
MP Destined for heat-treated meat products industry:
1= hearts.
2= livers.
3= masseter muscles.
4= tongues.
PF Destined for the pet food industry."
