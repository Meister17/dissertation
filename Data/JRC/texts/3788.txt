Commission Decision
of 13 July 2005
amending Decision 96/355/EC laying down special conditions governing the import of fishery and aquaculture products originating in Senegal, as regards the competent authority and the model of health certificate
(notified under document number C(2005) 2651)
(Text with EEA relevance)
(2005/505/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/493/EEC of 22 July 1991 laying down the health conditions for the production and the placing on the market of fishery products [1], and in particular Article 11 (1) thereof,
Whereas:
(1) In Commission Decision 96/355/EC [2], the "Ministère de la pêche et des transports maritimes — Direction de l’océanographie et des pêches maritimes — Bureau du contrôle des produits halieutiques (MPTM — DOPM — BCPH)" is identified as the competent authority in Senegal for verifying and certifying compliance of fishery and aquaculture products with Directive 91/493/EEC.
(2) Following a restructuring of the Senegal administration, the competent authority has changed to the "Ministère de l’Economie Maritime — Direction des Pêches Maritimes — Bureau de Contrôle des Produits Halieutiques (MEM — DPM — BCPH)".
(3) That new authority is capable of effectively verifying the application of the rules in force.
(4) The MEM — DPM — BCPH has provided official assurances on compliance with the standards for health controls and monitoring of fishery and aquaculture products as set out in Directive 91/493/EEC and on the fulfilment of hygienic requirements equivalent to those laid down in that Directive.
(5) Decision 96/355/EC should therefore be amended accordingly.
(6) It is appropriate for this Decision to be applied 45 days from the date of its publication in the Official Journal of the European Union thereby providing for the necessary transitional period.
(7) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
Decision 96/355/EC is amended as follows:
1. Article 1 is replaced by the following:
"Article 1
The "Ministère de l’Economie Maritime — Direction des Pêches Maritimes — Bureau de Contrôle des Produits Halieutiques (MEM — DPM — BCPH)" shall be the competent authority in Senegal for verifying and certifying compliance of fishery and aquaculture products with the requirements of Directive 91/493/EEC."
2. Article 2 is replaced by the following:
"Article 2
Fishery and aquaculture products imported from Senegal must meet the following conditions:
1. each consignment must be accompanied by a numbered original health certificate, duly completed, signed dated and comprising a single sheet in accordance with the model in Annex A hereto;
2. the products must come from approved establishments, factory vessels, cold store or registered freezer vessels listed in Annex B hereto;
3. except in the case of frozen fishery products in bulk and intended for the manufacture of preserve foods, all packages must bear the word "SENEGAL" and the approval/registration number of the establishment, factory vessel, cold store or freezer vessel of origin in indelible letters."
3. Paragraph 2 of the Article 3 is replaced by the following:
"2. Certificates must bear the name, capacity and signature of the representative of the MEM — DPM — BCPH and the latter’s official stamp in a colour different from that of other endorsements."
4. Annex A is replaced by the text in the Annex to this Decision.
Article 2
This Decision shall apply from 29 August 2005.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 13 July 2005.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 268, 24.9.1991, p. 15. Directive as last amended by Regulation (EC) No 806/2003 (OJ L 122, 16.5.2003, p. 1).
[2] OJ L 137, 8.6.1996, p. 24.
--------------------------------------------------
ANNEX
--------------------------------------------------
