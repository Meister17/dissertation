Judgment of the Court of First Instance of 13 July 2006 — Italy
v Commission
(Case T-225/04) [1]
Parties
Applicant: Italian Republic (represented by: A. Cingolo, P. Gentili and D. Del Gaizo, and subsequently by P. Gentili and D. Del Gaizo, avvocati dello Stato, acting as Agents)
Defendant: Commission of the European Communities (represented by: E. de March and J. Flynn, Agents, and A. Dal Ferro, lawyer)
Re:
Application for annulment of Commission Decision C (2003) 3971 final of 26 November 2003 establishing indicative allocations between the Member States of the commitment appropriations under Community initiatives for the period 1994-1999 and of all related prior measures
Operative part of the judgment
The Court:
1. Dismisses the application;
2. Orders the applicant to bear its own costs and the costs of the defendant.
[1] OJ C 106, 30.4.2004 (Case C-60/04).
--------------------------------------------------
