Decision of the European Parliament
of 21 April 2004
concerning discharge for the financial management of the European Coal and Steel Community (ECSC) for the financial year ended 23 July 2002
(2004/718/EC)
THE EUROPEAN PARLIAMENT,
having regard to the communication from the Commission on the financial statements of the ECSC at 23 July 2002 [1],
having regard to the Court of Auditors' report on the financial statements of the ECSC at 23 July 2002 [2], which includes the statement of assurance of 27 March 2003 concerning the reliability of the Commission's accounts in accordance with Article 45c(5) of the ECSC Treaty,
having regard to the Court of Auditors' annual report and statement of assurance concerning the ECSC of 26 June 2003, in accordance with Article 45c(1) and (4) of the ECSC Treaty, for the financial year ended 23 July 2002, together with the Commission's replies (C5-0646/2003) [3],
having regard to the ECSC Treaty, in particular Articles 78g and 97 thereof,
having regard to Rules 93 and 93a of and Annex V to its Rules of Procedure,
having regard to the Council's recommendation of 9 March 2004 (C5-0145/2004),
having regard to the report of the Committee on Budgetary Control (A5-0201/2004),
1. Gives discharge to the Commission for the financial management of the ECSC for the financial year ended 23 July 2002;
2. Records its observations in the accompanying resolution;
3. Instructs its President to forward this decision and the accompanying resolution to the Council, the Commission, the Court of Justice, the Court of Auditors, the Economic and Social Committee and the European Investment Bank and to have them published in the Official Journal of the European Union (L series);
The Secretary-General
Julian Priestley
The President
Pat Cox
[1] OJ C 127, 29.5.2003, p. 2.
[2] OJ C 127, 29.5.2003, p.2.
[3] OJ C 224, 19.9.2003, p.1.
--------------------------------------------------
