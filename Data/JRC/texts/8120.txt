DECISION OF THE EEA JOINT COMMITTEE
No 91/98
of 25 September 1998
amending Annex XI (Telecommunications services) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as adjusted by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas Annex XI to the Agreement was amended by Decision of the EEA Joint Committee No 75/98 of 17 July 1998(1);
Whereas Directive 97/67/EC of the European Parliament and of the Council of 15 December 1997 on common rules for the development of the internal market of Community postal services and the improvement of quality of service(2), is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
The following subheading shall be inserted before point 1 (Council Directive 87/372/EEC) in Annex XI to the Agreement after the heading "ACTS REFERRED TO"
"Telecommunication services"
Article 2
The following shall be inserted after point 5c (Directive 95/62/EC of the European Parliament and of the Council) in Annex XI to the Agreement: "Postal services
5d. 397 L 0067: Directive 97/67/EC of the European Parliament and of the Council of 15 December 1997 on common rules for the development of the internal market of Community postal services and the improvement of quality of service (OJ L 15, 21.1.1998, p. 14). The provisions of the Directive shall, for the purposes of the present Agreement, be read with the following adaptations:
(a) in Article 5(2) 'Treaty, in particular Articles 36 and 56 thereof' shall read 'EEA Agreement, in particular Articles 13 and 33 thereof';
(b) In Article 26 'Treaty' shall read 'EEA Agreement'."
Article 3
The texts of Directive 97/67/EC in the Icelandic and Norwegian languages, which are annexed to the respective language versions of this Decision, are authentic.
Article 4
This Decision shall enter into force on 1 October 1998, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee.
Article 5
This Decision shall be published in the EEA section of, and in the EEA Supplement to, the Official Journal of the European Communities.
Done at Brussels, 25 September 1998.
For the EEA Joint Committee
The President
N. v. LIECHTENSTEIN
(1) OJ L 172, 8.7.1999, p. 54.
(2) OJ L 15, 21.1.1998, p. 14.
