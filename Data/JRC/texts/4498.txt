Commission Decision
of 22 December 2004
determining the Community position for a decision of the Management entities under the Agreement between the Government of the United States of America and the European Community on the coordination of energy-efficient labelling programmes for office equipment on the revision of Annex C, part II, defining monitor specifications
(2005/42/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Decision 2003/269/EC of 8 April 2003 concerning the conclusion on behalf of the Community of the Agreement between the Government of the United States of America and the European Community on the coordination of energy-efficient labelling programmes for office equipment [1], and in particular Article 3(3) thereof,
Whereas:
(1) The Agreement provides for the European Commission, together with the United States Environmental Protection Agency (EPA), to reassess the specifications for label qualification of office equipment listed in Annex C to the Agreement as market conditions change. Developments in the field of computer monitors have led to a revision of the specifications.
(2) The position of the Community with regard to amendments of the specifications shall be determined by the Commission, after consultation with the special committee designated by the Council.
(3) The measures provided for in this Decision take account of the opinion given by the European Community Energy Star Board referred to in Articles 8 and 11 of Regulation (EC) No 2422/2001 of the European Parliament and of the Council of 6 November 2001 on a Community energy efficiency labelling programme for office equipment [2].
(4) With a view to adopting the Community position, the Commission has consulted the special committee designated by the Council.
(5) The monitor specifications in Annex C, part II, should be repealed and replaced by the specifications annexed to this Decision,
DECIDES:
Sole Article
The position to be adopted by the European Community for a decision by the Management entities, under the Agreement between the Government of the United States of America and the European Community on the coordination of energy-efficient labelling programmes for office equipment, on the monitor specifications in Annex C, part II, to the Agreement shall be based on the attached draft decision.
Done at Brussels, 22 December 2004.
For the Commission
Jacques Barrot
Vice-President
--------------------------------------------------
[1] OJ L 99, 17.4.2003, p. 47.
[2] OJ L 332, 15.12.2001, p. 1.
--------------------------------------------------
+++++ ANNEX 1 +++++</br>
