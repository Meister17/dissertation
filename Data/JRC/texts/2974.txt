Commission Regulation (EC) No 322/2005
of 25 February 2005
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 26 February 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 25 February 2005.
For the Commission
J. M. Silva Rodríguez
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 1947/2002 (OJ L 299, 1.11.2002, p. 17).
--------------------------------------------------
ANNEX
to Commission Regulation of 25 February 2005 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code | Standard import value |
07020000 | 052 | 121,9 |
204 | 66,1 |
212 | 151,1 |
624 | 193,8 |
999 | 133,2 |
07070005 | 052 | 173,6 |
068 | 152,0 |
204 | 115,9 |
220 | 230,6 |
999 | 168,0 |
07091000 | 220 | 36,6 |
999 | 36,6 |
07099070 | 052 | 190,8 |
204 | 176,4 |
999 | 183,6 |
08051020 | 052 | 56,3 |
204 | 46,4 |
212 | 50,5 |
220 | 39,2 |
624 | 67,5 |
999 | 52,0 |
08052010 | 204 | 87,1 |
624 | 84,0 |
999 | 85,6 |
08052030, 08052050, 08052070, 08052090 | 052 | 59,6 |
204 | 97,6 |
400 | 84,9 |
464 | 56,0 |
624 | 87,7 |
662 | 49,9 |
999 | 72,6 |
08055010 | 052 | 56,5 |
999 | 56,5 |
08081080 | 400 | 107,9 |
404 | 96,3 |
508 | 80,2 |
512 | 95,5 |
524 | 56,8 |
528 | 76,5 |
720 | 51,1 |
999 | 80,6 |
08082050 | 388 | 79,3 |
400 | 95,6 |
512 | 58,7 |
528 | 69,1 |
999 | 75,7 |
--------------------------------------------------
