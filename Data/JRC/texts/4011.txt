Corrigendum to Commission Regulation (EC) No 1255/2005 of 29 July 2005 determining the extent to which the applications for import licences submitted in July 2005 for certain dairy products under certain tariff quotas opened by Regulation (EC) No 2535/2001 can be accepted
(Official Journal of the European Union L 200 of 30 July 2005)
On page 66, in Annex I.B:
for:
"5. Products originating in Roumania",
read:
"1. Products originating in Romania";
for:
"6. Products originating in Bulgaria",
read:
"2. Products originating in Bulgaria".
--------------------------------------------------
