Euro-Mediterranean Agreement
establishing an Association between the European Communities and their Member States, of the one part, and the Hashemite Kingdom of Jordan, of the other part
The Agreement drawn up in 11 official languages of the European Union (Spanish, Danish, German, Greek, English, French, Italian, Dutch, Portuguese, Finnish, Swedish) was published in OJ L 129, 15.5.2002, p. 3. The Czech, Estonian, Latvian, Lithuanian, Hungarian, Maltese, Polish, Slovak and Slovenian versions are published in this Official Journal.
--------------------------------------------------
