COUNCIL DECISION of 4 December 1990 amending Decision 87/277/EEC on the allocation of the catch possibilities for cod in the Spitzbergen and Bear Island area and in Division 3M as defined in the NAFO Convention (90/655/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 170/83 of 25 January 1983 establishing a Community system for the conservation and management of fishery resources(1), as amended by the Act of Accession of Spain and Portugal, and in particular Article 11 of that Regulation,
Having regard to the proposal from the Commission(2),
Having regard to the opinion of the European Parliament(3),
Whereas the unification of Germany has changed the historic catches on which Decision 87/277/EEC(4) was based; whereas that Decision should therefore be amended
in order to take account of catches made by the former German Democratic Republic during the reference periods used in calculating the percentage allocations in the Annex to that decision,
HAS DECIDED AS FOLLOWS:
Sole Article
The Annex to Decision 87/277/EEC shall be replaced by the Annex to this Decision.
Done at Brussels, 4 December 1990.
For the Council The President G. DE MICHELIS
(1)OJ No L 24, 27. 1. 1983, p. 1.
(2)OJ No C 248, 2. 10. 1990, p. 11.
(3)Opinion delivered on 21 November 1990 (not yet published in the Official Journal).
(4)OJ No L 135, 23. 5. 1987, p. 29.
ANNEX
Spitzbergen Bear Island cod (ICES division II b) TAC
(tonnes)
Community
share
(tonnes)
Germany
(%)
Spain
(%)
France
(%)
Portugal
(%)
United
Kingdom
(%)
All Other
Member States
(total amount)
FIRST
INSTALMENT
Percentage of the Community share after deduction of the
standard amount allocated to other Member States
Standard amount
22 018 or less19,2449,73 8,2110,5012,32100 tonnes
SECOND
INSTALMENT
Percentage of the Community share after deduction of the first
instalment and the amount allocated to other Member States
Standard amount
22 01924 22029,7128,4516,44 4,2121,19250 tonnes
Percentage of
the Community
share
700 001 800 00024 22127 68029,5428,5416,46 4,2721,191,91
800 001 900 00027 68131 14029,5128,5616,47 4,2721,192,86
900 0011 000 00031 14134 60029,5428,5416,46 4,2721,193,82
1 000 001 or more34 601 or more29,5428,5416,46 4,2721,1 4,77
Cod NAFO 3M Germany
(%)
Spain
(%)
France
(%)
Portugal
(%)
United Kingdom
(%)
FIRST INSTALMENT
7 500 tonnes or less9,3328,674,0039,3318,67
SECOND INSTALMENT
more than 7 500 tonnes1,7637,815,3851,97 3,08
