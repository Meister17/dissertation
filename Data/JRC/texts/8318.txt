COMMISSION REGULATION (EC) No 2593/1999
of 8 December 1999
amending Annexes I, II and III of Council Regulation (EEC) No 2377/90 laying down a Community procedure for the establishment of maximum residue limits of veterinary medicinal products in foodstuffs of animal origin
(Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2377/90 of 26 June 1990 laying down a Community procedure for the establishment of maximum residue limits of veterinary medicinal products in foodstuffs of animal origin(1), as last amended by Commission Regulation (EC) No 2393/1999(2), and in particular Articles 6 and 8 thereof,
(1) Whereas, in accordance with Regulation (EEC) No 2377/90, maximum residue limits must be established progressively for all pharmacologically active substances which are used within the Community in veterinary medicinal products intended for administration to food-producing animals;
(2) Whereas maximum residue limits should be established only after the examination within the Committee for Veterinary Medicinal Products of all the relevant information concerning the safety of residues of the substance concerned for the consumer of foodstuffs of animal origin and the impact of residues on the industrial processing of foodstuffs;
(3) Whereas, in establishing maximum residue limits for residues of veterinary medicinal products in foodstuffs of animal origin, it is necessary to specify the animal species in which residues may be present, the levels which may be present in each of the relevant meat tissues obtained from the treated animal (target tissue) and the nature of the residue which is relevant for the monitoring of residues (marker residue);
(4) Whereas, for the control of residues, as provided for in appropriate Community legislation, maximum residue limits should usually be established for the target tissues of liver or kidney; whereas, however, the liver and kidney are frequently removed from carcasses moving in international trade, and maximum residue limits should therefore also always be established for muscle or fat tissues;
(5) Whereas, in the case of veterinary medicinal products intended for use in laying birds, lactating animals or honey bees, maximum residue limits must also be established for eggs, milk or honey;
(6) Whereas novobiocin, betamethasone, spiramycin, diflubenzuron and enrofloxacin should be inserted into annex I to Regulation (EEC) No 2377/90;
(7) Whereas calendulae flos, cimicifugae racemosae rhizoma, ergometrine maleate, 1-methyl-2-pyrrolidone, mepivacaine, xylazine hydrochloride, novobiocin, piperazine dihydrochloride, poyoxyl castor oil with 30 to 40 oxyethylene units and jecoris oleum should be inserted into Annex II to Regulation (EEC) No 2377/90;
(8) Whereas, in order to allow for the completion of scientific studies, piperazine, cyromazine, tilmicosin and toltrazuril should be inserted into Annex III to Regulation (EEC) No 2377/90;
(9) Whereas a period of 60 days should be allowed before the entry into force of this Regulation in order to allow Member States to make any adjustment which may be necessary to the authorisations to place the veterinary medicinal products concerned on the market which have been granted in accordance with Council Directive 81/851/EEC(3), as last amended by Directive 93/40/EEC(4), to take account of the provisions of this Regulation,
(10) Whereas the measures provided for in this Regulation are in accordance with the opinion of the Standing Committee on Veterinary Medicinal Products,
HAS ADOPTED THIS REGULATION:
Article 1
Annexes I, II and III of Regulation (EEC) No 2377/90 are hereby amended as set out in the Annex hereto.
Article 2
This Regulation shall enter into force on the 60th day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 8 December 1999.
For the Commission
Erkki LIIKANEN
Member of the Commission
(1) OJ L 224, 18.8.1990, p. 1.
(2) OJ L 290, 12.11.1999, p. 5.
(3) OJ L 317, 6.11.1981, p. 1.
(4) OJ L 214, 24.8.1993, p. 31.
ANNEX
A. Annex I to Regulation (EEC) No 2377/90 is amended as follows:
1. Anti-infectious agents
1.2. Antibiotics
1.2.3. Quinolones
">TABLE>"
1.2.4. Macrolides
">TABLE>"
1.2.11. Other antibiotics
">TABLE>"
2. Antiparasitic agents
2.2. Agents actings against ectoparasites
2.2.4. Acyl urea derivatives
">TABLE>"
5. Corticoides
5.1. Glucocorticoids
">TABLE>"
B. Annex II to Regulation (EEC) No 2377/90 is amended as follows:
2. Organic compounds
">TABLE>"
6. Substances of vegetable origin
">TABLE>"
C. Annex III to Regulation (EEC) No 2377/90 is amended as follows:
1. Anti-infectious agents
1.2. Antibiotics
1.2.2. Macrolides
">TABLE>"
2. Antiparasitic agents
2.1. Agents actings against endoparasites
2.1.5. Piperazine derivatives
">TABLE>"
2.2. Agents acting against ectoparasites
2.2.7. Triazine derivatives
">TABLE>"
2.4. Agents acting against protozoa
2.4.3. Triazinetrione derivatives
">TABLE>"
