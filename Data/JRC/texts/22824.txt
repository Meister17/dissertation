COMMISSION REGULATION (EEC) No 1057/91 of 26 April 1991 amending certain Council Regulations and Directives on agricultural statistics in connection with the unification of Germany
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 43 thereof,
Having regard to Council Regulation (EEC) No 3570/90 of 4 December 1990 on derogations in respect of agricultural statistics in Germany in connection with the unification of Germany (1), and in particular Article 1 thereof,
Whereas the implementation of Community Regulations on agricultural statistics on the territory of the former German Democratic Republic will require adjustments on the part of the institutions responsible for collecting data and on the part of the persons responsible for providing data; whereas temporary derogations will need to be introduced concerning the deadlines for the transmission of statistics relating to that territory;
Whereas, for technical reasons, the centralization of individual data as provided for in point 6 in Annex II to Council Regulation (EEC) No 571/88 (2), as amended by Regulation (EEC) No 807/89 (3), will not be possible for the territory of the former German Democratic Republic before 31 December 1992;
Whereas, on the territory of the former German Democratic Republic, it will not be possible until after the 1992 survey in Germany to make the estimates of clearing and new plantations provided for in Articles 5 and 6 of Council Directive 76/625/EEC of 20 July 1976 concerning the statistical surveys to be carried out by the Member States (4), as last amended by Directive 86/652/EEC (5), in order to determine the production potential of plantations of certain species of fruit trees;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Standing Committee on Agricultural Statistics,
HAS ADOPTED THIS REGULATION: Article 1
The following is hereby added to the first indent of Article 10 (1) of Council Regulation (EEC) No 2782/75 (6):
'Germany is authorized to delay the collection of monthly data on hatcheries on the territory of the former German Democratic Republic until after 1 January 1991 and to submit such data for 1991 not later than four months after the reference month.' Article 2 The following is hereby added to point 6 in Annex II to Regulation (EEC) No 571/88:
'The deadline for the centralization of individual data relating to holdings on the territory of the former German Democratic Republic is put back to 31 December 1992.' Article 3 The following is hereby inserted in Article 6 of Council Directive 72/280/EEC (7):
'2bis. Notwithstanding paragraph 2, Germany is authorized to delay the collection of data concerning holdings on the territory of the former German Democratic Republic until after 1 January 1991 and to transmit the data for 1991 by the following deadlines:
(a) one month after the end of the reference week for the weekly results indicated in Article 4 (1);
(b) three months after the end of the reference month for the monthly results indicated in Article 4 (2);
(c) in July of the year following the reference year for:
- the annual results indicated in Article 4 (3) (a) and (b);
- the results of the surveys indicated in Article 1 (1) (b);
(d) August of the year following the reference year for the annual results indicated in Article 4 (3) (c);
(e) November of the year following the reference year for the results indicated in Article 4 (4).' Article 4 Council Directive 73/132/EEC (8) is hereby amended as follows:
1. The following paragraph 4 is added to Article 6:
'4. Notwithstanding paragraph 3, Germany is authorized to delay transmission of forecasts concerning the territory of the former German Democratic Republic until not later than 10 weeks following the reference month for the 1990 and 1991 surveys.',
2. The following paragraph 5 is added to Article 7:
'5. Nothwithstanding paragraph 1, Germany is authorized to delay the production of monthly statistics relating to the territory of the former German Democratic Republic until 1991 and, by way of derogation from paragraph 4, to delay the transmission of the results for 1991 until not later than 10 weeks following the reference month.' Article 5 Directive 76/625/EEC is hereby amended as follows:
1. The following paragraph 4 is added to Article 5:
'4. Notwithstanding paragraph 1, Germany is authorized to delay the production of statistics relating to the territory of the former German Democratic Republic until 1993.',
2. The following paragraph 3 is added to Article 6:
'3. Notwithstanding paragraph 1, Germany is authorized to delay the production of statistics relating to the territory of the former German Democratic Republic until 1993.' Article 6 Council Directive 76/630/EEC (9) is hereby amended as follows:
1. The following indent is added to Article 1:
'Notwithstanding the first indent, Germany is authorized to carry out a survey in May 1991 instead of the survey stipulated for April 1991 in the case of the territory of the former German Democratic Republic.',
2. The following subparagraph is added to Article 4 (3):
'Notwithstanding the first indent, Germany is authorized to delay the transmission of the results of the survey to be carried out in May 1991 on the territory of the former German Democratic Republic until 31 August 1991 at the latest.',
3. The following paragraph 4 is added to Article 7:
'4. Notwithstanding paragraph 1, Germany is authorized to delay the production of monthly statistics on the territory of the former German Democratic Republic until January 1991.' Article 7 Council Directive 82/177/EEC (10) is hereby amended as follows:
1. The following paragraph 4 is added to Article 6:
'4. Notwithstanding paragraph 3, Germany is authorized to delay the transmission of the forecasts to be drawn up in 1991 until 1 April 1991 at the latest.',
2. The following paragraph 5 is added to Article 7:
'5. Notwithstanding paragraph 1, Germany is authorized to delay the production of monthly statistics on the territory of the former German Democratic Republic until January 1991 and, by way of derogation from paragraph 4, to delay the transmission of the results for 1991 until 10 weeks after the reference month at the latest.' Article 8 This Regulation shall enter into force on the 10th day following its publication in the Official Journal of the European Communities. This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 26 April 1991. For the Commission
Henning CHRISTOPHERSEN
Vice-President (1) OJ No L 353, 17. 12. 1990, p. 8. (2) OJ No L 56, 2. 3. 1988, p. 1. (3) OJ No L 86, 31. 3. 1989, p. 1. (4) OJ No L 218, 11. 8. 1976, p. 10. (5) OJ No L 382, 31. 12. 1986, p. 16. (6) OJ No L 282, 1. 11. 1975, p. 100. (7) OJ No L 179, 7. 8. 1972, p. 2. (8) OJ No L 153, 9. 6. 1973, p. 25. (9) OJ No L 223, 16. 8. 1976, p. 4. (10) OJ No L 81, 27. 3. 1982, p. 35.
