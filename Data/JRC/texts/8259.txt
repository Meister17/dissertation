COMMISSION REGULATION (EC) No 2245/1999
of 22 October 1999
amending Regulation (EC) No 1663/95 laying down detailed rules for the application of Council Regulation (EEC) No 729/70 regarding the procedure for the clearance of the accounts of the EAGGF Guarantee Section
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 729/70 of 21 April 1970 on the financing of the common agricultural policy(1), as last amended by Regulation (EC) No 1287/95(2), and in particular Articles 4(6) and 5(3) thereof,
Whereas:
(1) in the light of experience, a number of changes and clarifications should be made to Commission Regulation (EC) No 1663/95(3), as last amended by Regulation (EC) No 896/97(4), in particular with regard to the accreditation criteria for executing payments and the provisions concerning the need to avoid conflicts of interest in the work of persons occupying positions of responsibility in paying agencies;
(2) details of amounts still to be recovered should be included in the annual accounts of paying agencies;
(3) it is neither right nor fair for the Commission to give an evaluation of the expenditure it intends to exclude under Article 5(2)(c) of Regulation (EEC) No 729/70 as a result of its findings before the Member State has had the opportunity to reply;
(4) expenditure must be excluded for the whole period during which Community rules are infringed;
(5) the EAGGF Committee has delivered a favourable opinion,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 1663/95 is amended as follows:
1. Article 2(3) is replaced by the following: "The form and content of the accounting information referred to in paragraph 1 shall be established in accordance with the procedure provided for in Article 13 of Regulation (EEC) No 729/70."
2. The following point (f) is added to Article 5(1): "(f) a summary of amounts in the process of being recovered, broken down by financial year in which the recovery order was issued and a summary of the amounts recognised during the year as unrecoverable."
3. Article 8(1) is replaced by the following: "1. If, as a result of an enquiry, the Commission considers that expenditure has not been effected according to Community rules, it shall notify the Member State concerned of the results of its checks and indicate the corrective measures to be taken to ensure future compliance.
The communication shall refer to this Regulation. The Member State shall reply within two months and the Commission may modify its position in consequence. In justified cases, the Commission may extend the period allowed for reply.
After expiry of the period allowed for reply, the Commission shall invite the Member State to a bilateral discussion and the parties shall endeavour to reach agreement on the measures to be taken and on an evaluation of the gravity of the infringement and the financial loss to the Community. Following that discussion and any deadline after the discussion fixed by the Commission, after consultation of the Member States, for the provision of further information or, where the Member State does not accept the invitation to a meeting before the deadline set by the Commission, after that deadline has passed, the Commission shall formally communicate its conclusions to the Member State, referring to Commission Decision 94/442/EC(5). Without prejudice to the fourth subparagraph of this paragraph, that communication shall include an evaluation of any expenditure the Commission intends to exclude under Article 5(2)(c) of Regulation (EEC) No 729/70.
The Member State shall inform the Commission as soon as possible of the corrective measures adopted to ensure compliance with Community rules and the date of their entry into force. The Commission shall, as appropriate, adopt one or more Decisions under Article 5(2)(c) of Regulation (EEC) No 729/70 to exclude expenditure affected by non-compliance with Community rules up to the date of entry into force of the corrective measures."
4. The Annex is amended as follows:
(a) A new point 4a is added: "4a. With the prior agreement of the Commission and in the case of co-financed measures only, where a large number of small payments must be made, payments to applicants for aid may be delegated to other bodies. A written agreement must be concluded between the paying agency and that body specifying the nature of the information and the supporting documents to be submitted to the paying agency and the time limit within which they must be submitted; they must as a minimum permit the paying agency to comply with the accreditation criteria and to meet the deadlines laid down for the submission of monthly and annual accounts. The paying agency shall remain responsible for the efficient management of the funds concerned and for updating accounting records. The authorised agents of the paying agency, the certifying body and the European Union shall have the right to examine any evidence held by the abovementioned body and to carry out checks on applicants for aid."
(b) The following is added to point 5: "The administrative subunit responsible for the execution of the payments, or else a unit responsible for supervising it, shall have available documentary evidence of the authorisation of claims, and of the administrative and physical checks prescribed. The information and the evidence may be in a summary form equivalent to that described in point 4(iv) of this Annex, and it may be provided through a computerised system."
(c) The following sentence is added to the second subparagraph of point 6(ii): "Appropriate measures must be taken to avoid a conflict of interests where a person occupying a position of responsibility or a sensitive position with regard to the verification, authorisation and payment of claims on the Fund also fulfils other functions outside the paying agency."
Article 2
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 22 October 1999.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ L 94, 28.4.1970, p. 13.
(2) OJ L 125, 8.6.1995, p. 1.
(3) OJ L 158, 8.7.1995, p. 6.
(4) OJ L 128, 21.5.1997, p. 8.
(5) OJ L 182, 16.7.1994, p. 45.
