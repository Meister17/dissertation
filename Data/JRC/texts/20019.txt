COMMISSION REGULATION (EC) No 3017/95 of 20 December 1995 laying down provisions for the implementation of Council Regulation (EC) No 3036/94 establishing economic outward processing arrangements applicable to certain textile and clothing products reimported into the Community after working or processing in certain third countries
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 3036/94 of 8 December 1994 establishing economic outward processing arrangements applicable to certain textile and clothing products reimported into the Community after working or processing in certain third countries (1), and in particular Article 3 (8) and Article 12 thereof,
Whereas Article 4 of this Regulation provides that the competent authorities of the Member States shall issue prior authorizations to applicants satisfying the conditions of the economic outward processing arrangements for certain textile and clothing products;
Whereas the smooth operation of the arrangements in the single market calls for uniform procedures for issuing and checking such prior authorizations;
Whereas the introduction of a standard application and prior authorization form will facilitate the application of the arrangements in the single market, and in particular administrative cooperation between the Member States;
Whereas it should be possible for prior authorizations to be issued in any Member State and be valid throughout the Community irrespective of the Member State of issue; whereas provision should nevertheless be made for centralized control to prevent economic operators being allocated more than their entitlement, such control taking the form of consulting the competent authorities of the Member State in which the applicant is established or where his Community production takes place;
Whereas a time limit should be fixed for the completion of any consultation that may prove necessary between Member States;
Whereas applications, prior authorizations and the documents belonging to them should be kept for a prescribed period;
Whereas economic operators must be notified of the authorities competent to issue prior authorizations in each Member State; for that reason the Member States should transmit this information to the Commission in order to publish it in the 'C` series of the Official Journal of the European Communities;
Whereas the administration of quotas should be facilitated by issuing prior authorizations in respect of one category of compensating product and one processing country only;
Whereas procedures for the issue of prior authorizations and the control of such essential aspects as the origin of goods should be introduced;
Whereas procedures should be introduced for the administration of the quantitative limits;
Whereas in cases other than those provided for in Article 11 (3) of Regulation (EC) No 3036/94, the granting of prior authorizations is subject to the quantities being available in accordance with the procedures laid down in Annex VII to Council Regulation (EEC) No 3030/93 of 12 October 1993 on common rules for imports of certain textile products from third countries (2), as last amended by Commission Regulation (EC) No 1616/95 (3); whereas the quantities available are allocated in the chronological order in which applications are received by the Commission;
Whereas a time limit must be set for exporting the export goods in order to ensure that amounts charged against quantitative limits do not go unused;
Whereas the measures applicable in the event of infringements being detected must be established to ensure the smooth running of the procedure;
Whereas the procedures for temporary export and for free circulation shall follow as closely as possible the rules set down in Council Regulation (EEC) No 2913/92 of 12 October 1992 establishing the Community Customs Code (1), as well as Commission Regulation (EEC) No 2454/93 of 2 July 1993 laying down provisions for the implementation of Council Regulation (EEC) No 2913/92 establishing the Community Customs Code (2), as last amended by Regulation (EC) No 1762/95 (3);
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Committee on Economic Outward Processing Arrangements for Textiles,
HAS ADOPTED THIS REGULATION:
GENERAL
Article 1
This Regulation lays down provisions for the application of Council Regulation (EC) No 3036/94 establishing economic outward processing arrangements applicable to certain textile and clothing products reimported into the Community after working or processing in certain third countries, hereinafter referred to as the 'basic Regulation`, especially with respect to the issue and control of prior authorizations.
Article 2
Definitions
1. For the purposes of Article 3 (2) of the basic Regulation, the expression 'quantities available within the overall Community quota for the entire category and the third country concerned` is understood to mean the quantities available in the framework of the quota established for the year during which the quantity requested by an operator has been notified to the Commission by the competent authorities which have received the application for the prior authorization.
2. For the purposes of Article 3 (4) (i) of the basic Regulation, the expression 'the overall quantity for which he carried out OPT` is understood to mean the reimportation of compensating products of a specific category and country under the economic outward processing arrangements during the course of the year 1993 or the year 1994, the choice being left to the applicant. The category, quantity and the third country corresponding to the chosen year shall be the reference for the following years, an adjustment being made, if need be, of the quantities pursuant to Article 3 (4) (2) and (3) and Article 3 (5) (5) and (6) of the basic Regulation.
3. For the purposes of Article 3 (4) (3) of the basic Regulation, the equivalent quantities are to be found in the equivalence table in Annex I of Regulation (EEC) No 3030/93.
4. For the purposes of Article 3 (5) (3) of the basic Regulation the value of the Community production is calculated on the basis of the normal ex factory price, exclusive of VAT, of products, shown in Annex II of the basic Regulation that have been manufactured in the Community in the previous year, whether in the applicant's factory or on his account by another manufacturer, as much as the latter business does not also introduce a request for prior authorization for the same Community production for the same period.
5. The additional amounts, provided for in Article 3 (4) (2) and (5) (4) of the basic Regulation, are allocated according to the maximum amounts shown in Annex III on the condition that the applicant has effectively either reimported at least 50 % or exported an equivalent of at least 80 % of the quantity previously authorized in the framework of the economic outward processing arrangements for the category and the country concerned.
6. For the purposes of Article 3 (5) (5) and (6) of the basic Regulation, the 'quantities of compensatory products` which shall be allocated for each category and third country to an applicant as a past beneficiary, are those which the applicant has reimported the previous year under the economic outward processing arrangements, reduced proportionately, where appropriate, to the decline of this Community production due to economic outward processing operations carried out in the course of the reference period.
7. For the purposes of Article 5 (3) of the basic Regulation 'rate of yield` means the quantity or percentage of compensating products obtained from the processing of a given quantity of the temporary exported goods.
GRANTING OF PRIOR AUTHORIZATIONS
Article 3
Application for authorization
1. An application for the issue of a prior authorization shall be submitted to the competent authorities appointed by the Member States using the form shown in Annex I, by or on behalf of a person satisfying the conditions under Article 2 of the basic Regulation.
The competent authorities may, under the conditions fixed by them, allow the submission of applications to be transmitted or printed by electronic means which may, in order to allow for simplification, depart from the format as contained at Annex I. However all documents and evidence, required according to paragraph 3, must be available to the competent authorities.
2. The application and all further correspondence shall be completed in the language, or in one of the official languages, of the Member State in which the application is submitted and shall be used throughout the procedure as regards the applicant.
3. An application shall be accompanied by all documents or evidence, whether originals or copies, the production of which is necessary for its examination, especially the contract concluded with the undertaking responsible for carrying out the processing operations in the third country or any written evidence considered to be equivalent thereto.
Additional sheets may be appended to the application where there is a need to expand on certain particulars required. Any document, evidence or sheet appended to the application shall constitute an integral part thereof. The number of accompanying documents shall be mentioned in the application.
The competent authorities may request further information if this is required for the handling of the application. Where appropriate, the competent authorities may accept references to an earlier application in support of a new request.
4. A request for prior authorization can only concern one category of compensating products and one specific third country.
5. When the request concerns products subject, in the named third country, to specific quantitive limits with respect to economic outward processing, the applicant must declare that he has not and will not introduce another request concerning the same category and third country to another Member State's competent authorities, as long as he has not received a decision on his request for a prior authorization. When he introduces a further request having received a prior authorization for a category and a specific third country for the same quota period, the applicant must mention the prior authorization obtained.
6. The requests introduced on the basis of Article 3 (4) (1) of the basic Regulation may not cover an amount superior to the total quantity for which the applicant has carried out outward processing under the economic outward processing arrangements for the category and the country concerned in the reference period, adjustments being made, where applicable, of the quantities pursuant to Article 3 (4) (2) and (3) and Article 3 (5) (5) (6) of the basic Regulation.
The requests introduced on the basis of Article 3 (5) (1) to (3) of the basic Regulation may not cover for each country concerned, an amount superior to the one set out for the specific category in Annex 3. The total value of processing in third countries requested in the application, where applicable taking into account earlier authorizations for the same quota period, may not, however, exceed 50 % of the value of the applicant's Community production as defined in Article 2 (4).
7. The applicant must undertake to keep stock records in the Community which allow the customs supervising office to control the quantities of the temporary export goods and the compensating products reimported.
8. In lodging an application signed by the applicant or his authorized representative, the applicant expresses the intention to benefit from the arrangements, without prejudice to any sanctions that may be taken under the rules in force in the Member States, and certifies that:
- the particulars given in the application are correct,
- the documents or evidence appended are authentic, and - all obligations and conditions connected with the issuing and the utilization of the prior authorization according to the basic Regulation or in the present Regulation are met or, where appropriate, will be honoured.
Article 4
Verification of the application
1. Once they have received the application, accompanied by all documents and evidence, the competent authorities verify its content. They may request additional information if they consider the application is inexact or incomplete or does not enable them to conclude that all the conditions for granting an authorization are met.
2. When an applicant lodges a request with the competent authorities in a different Member State to where he is established or his Community production takes place, these competent authorities shall consult the competent authorities of the Member States concerned. The latter authorities shall communicate any information requested as soon as possible and not later than two weeks after the date of the receipt of the request of information. Such consultations may be conducted in other cases where appropriate.
3. Where a condition for granting a prior authorization is not satisfied, the competent authorities reject the application.
The decision to refuse an application shall be set down in writing and sent to the applicant with the grounds for refusal. Where the particulars provided by the applicant are incomplete, the competent authorities set a time limit for completion of the application, in which case the rejection may not precede the expiry of the time limit.
4. If the competent authorities consider that all conditions for granting a prior authorization are satisfied, they communicate to the Commission the quantity, category and third country concerned, and in the event that the outward processing consists of obtaining from yarn fully fashioned knitwear, the weight of yarn to be temporarily exported.
Article 5
Issue, annulment, or revocation of a prior authorization
1. The authorization shall be issued by the competent authorities to which the application has been submitted on the form shown in Annex II.
The prior authorizations form shall be made out in three copies. The first, marked 'original` and bearing the number 1, shall be issued to the applicant and the second, marked 'copy for the competent authority` and bearing the number 2, shall be kept by the issuing authority, whereas the third marked 'copy for the customs supervising office` and bearing the number 3 will be sent to the customs supervising office, as defined in Article 13. In the event that the functions of the customs supervising office are perfomed by the competent authorities to which the application has been submitted, no copy No 3 is required.
The prior authorization may be issued by electronic means as long as the customs offices involved have access to this authorization across a computer network.
2. When the application concerns a category of products whose release into free circulation in the Community is subject to the measures provided for in Article 1 (3) of the basic Regulation, the prior authorization is issued by the competent authorities only when the Commission has confirmed the availability of the quantity notified by the authorities managing the request in accordance with the procedures laid down in Annex VII of Regulation (EEC) No 3030/93. The Commission will respond to all applications immediately, wherever possible, by electronic means.
When the application concerns a category of products not subject to quantitative limits pursuant to Article 11 (3) of the basic Regulation, the prior authorization is issued once the quantities have been notified to the Commission.
3. A prior authorization can only concern one category of compensating products and one specified third country.
4. In so far as the Commission has confirmed the availability of the quantity requested within the quota limit concerned, the competent authorities shall issue the prior authorization within no more than five working days, counting from the date of receipt of confirmation from the Commission.
5. The competent authorities will register the quantities requested by and granted to the beneficiaries and ensure, if necessary in cooperation with the competent authorities of the other Member States concerned, that these quantities do not exceed those referred to in Article 3 (4) and (5) of the basic Regulation.
6. Article 497 (3) of Regulation (EEC) No 2454/93, shall apply mutatis mutandis where the applicant requests to modify a prior authorization.
7. Should the competent authorities become aware of the fact that one of the conditions for granting authorization is or has not been satisfied, Articles 8, 9 and 10 of Regulation (EEC) No 2913/92 concerning the annulment and revocation of a decision shall apply to prior authorizations.
Article 6
Partial prior authorization
1. At the request of the holder of a prior authorization and on presentation of copy No 1 of that authorization, one or more partial prior authorizatzions may be issued by the competent authorities of the Member State which have issued the prior authorization. They may authorize the customs supervising office to issue partial authorizations.
2. The authorities issuing partial authorizations shall charge the quantities concerned against copies Nos 1, 3 and possibly 2 of the initially granted prior authorization. In such cases, the entry 'partial authorization` shall be made on copies Nos 1, 3 and possibly 2 against the quantities charged and shall refer to the number in the initial authorization. In the event that copy No 3 has already been sent to the customs supervising office, the competent authorities shall inform the customs supervising office about the partial prior authorization. Where the issue of one or more partial authorizations discharges the initial prior authorization, the competent authorities shall retain copy No 1 of this prior authorization and shall inform the customs supervising office.
3. The partial authorization shall be marked 'Partial prior authorization` and shall be treated under the same rules as normal prior authorizations.
4. Without prejudice to Article 10, one partial prior authorization may not give rise to the issue of another partial authorization.
Article 7
Territorial validity
Prior authorizations shall be valid in all Member States and may be presented in any customs office competent for the handling of the economic outward processing arrangements. Prior authorizations delivered by electronic means may only be used in customs offices having access to these authorizations across a computer network.
Article 8
Transfer
Prior authorizations may not be transferred and can only be used by the person named in the authorization or by his authorized representative.
Article 9
Retention of documents
1. The competent authorities shall keep appplications and accompanying documents together with copy No 2 of any prior authorization issued.
2. In the event of a prior authorization being granted, the original or a copy of the application, the accompanying documents and the authorization shall be kept by the applicant and the competent authorities for a period of at least three years from the end of the calendar year of issue of the authorization.
3. If the application is rejected or the prior authorization is annulled or revoked, the authorization or decision rejecting the application and any accompanying documents shall be kept for at least three calendar years from the end of the calendar year in which the application was rejected or the authorization annulled or revoked.
Article 10
Loss and replacement authorization
1. If a prior authorization is lost, the competent authorities who have issued the original authorization may issue a replacement authorization, at the holder's request. The application for a replacement shall include a signed declaration, assuring the authorities that the prior authorization has been lost and will not be used if found, but returned immediately to the issuing authority.
2. The replacement authorization shall bear the same information and entries as the document replaced. It shall be issued for a quantity of goods corresponding to the quantity available on the lost document. Where this quantity cannot be proven, no replacement authorization may be issued.
The replacement authorization shall be marked either 'Replacement prior authorization` or 'Replacement partial prior authorization` and shall be treated under the same rules as normal prior authorizations.
3. In the event of a replacement authorization being lost, no further replacement authorization may be issued unless exceptional circumstances prevail.
Article 11
Competent authorities
The Member States shall designate the authorities competent for the issue of prior authorizations and shall notify the Commission accordingly. The Commission shall publish this information in the 'C` series of the Official Journal of the European Communities.
Article 12
Deadlines
1. When issuing a prior authorization, the Member States competent authorities shall fix a period during which the goods must undergo temporary export formalities. This period shall not exceed six months. Nevertheless, at the request of the holder, the competent authorities may extend the period to a total duration of nine months.
2. The competent authorities shall fix a time limit for the reimportation of the compensating products taking account of the period necessary for the processing of the goods. This period shall be calculated from the date of completion of the temporary export formalities. The period may be extended at the holder's request if circumstances so warrant.
3. The holder of a prior authorization shall return the authorization to the competent authorities as soon as it has been fully utilized or as soon as it becomes apparent that it will not be fully used. If the holder needs the authorization for outstanding reimports of compensating products, he shall contact the competent authorities who will take the appropriate measures and inform the customs supervising office.
FUNCTIONING OF THE ARRANGEMENTS
Article 13
The customs supervising office
1. Without prejudice to Article 14 (5), the customs office indicated in the prior authorization, competent for the handling of the economic outward processing arrangements and to be called the 'customs supervising office`, will control the functioning of the arrangements, notably on the basis of:
(a) copy No 3 of the prior authorization;
(b) copies of the export and import declarations;
(c) the stock records mentioned in Article 3 (7).
2. It shall inform the competent authorities if it becomes aware that less than the quantities corresponding to the allocated quantities are exported.
3. Member States may charge the authorities competent for the issuing of prior authorizations with some or all the functions of the customs supervising office.
Article 14
Entry of the goods for temporary export and free circulation
1. Where no specific provision exists in this Regulation, or the basic Regulation, the relevant provisions of Regulations (EEC) No 2913/92 and (EEC) No 2454/93 shall apply.
2. The customs office carrying out the formalities for temporary export shall inform the customs supervising office by a copy of the export declaration.
3. The customs office carrying out the formalities for entry into free circulation shall:
- charge the quantities reimported against the prior authorization, and - inform the customs supervising office by a copy of the import declaration.
4. The information required may be furnished by electronic means as long as it gives the same guarantee and on condition that the customs office involved have access to the prior authorization across a computer network.
5. Where an authorization for outward processing according to Regulation (EEC) No 2913/92 exists in parallel wih a prior authorization under the present Regulation, the competent authorities may apply the relevant provisions of Regulations (EEC) No 2913/92 and (EEC) No 2454/93 for the control of the procedure, in place of paragraphs 1 to 4 and of Article 13 (1).
INFRINGEMENTS
Article 15
1. Customs offices detecting infringements of the rules laid down in the present or the basic Regulation shall immediately notify the authority which issued the prior authorization.
2. Each Member State shall determine the sanctions applicable in cases where the competent authorities find that an application for prior authorization contains false statements, made either intentionally or as the result of serious negligence, or that any other rule laid down in the present or basic Regulation has been seriously infringed.
ADMINISTRATIVE COOPERATION
Article 16
1. The competent authorities in the Member States shall communicate to the Commission, at its request, wherever possible by electronic means, statistics on goods reimported into their territory under this Regulation.
The Commission shall communicate this information to the other Member States, wherever possible, by electronic means.
2. Where the competent authorities find that quantities have been totally, partially unused or renounced within the meaning of Article 3 (4) and (6) of the basic Regulation they shall communicate them, wherever possible via the integrated electronic network, immediately and no later than 20 days after the date of expiry of the time limit for the exportation or the expiry of the authorization.
3. In application of Article 3 (4) and Article 4 (3) of the basic Regulation, the competent authorities of the Member States shall communicate to the Commission, by category and by third country, the total quantities reserved for the past beneficiaries, as well as the quantities adjusted pursuant to Article 3 (4), (2) and (3) and Article 3 (5), (5) and (6) of the basic Regulation, before 15 Janury of each year.
4. In application of Article 2 (2) (c) of the basic Regulation, the Member States shall communicate to the Commission a list of goods for which operators benefited in the previous year of a percentage of more than 14 %, indicating each product's textile category, origin and quantity (mass and supplementary unit as appropriate).
FINAL PROVISIONS
Article 17
Regulations (EEC) No 1828/83 (1) and (EC) No 1816/95 (2) are hereby repealed. This repealing does not affect the validity of any prior authorization issued in application of these Regulations.
This Regulation shall enter into force on 1 January 1996.
Existing application and prior authorization forms may continue to be used, however, until 30 June 1996.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 20 December 1995.
For the Commission Mario MONTI Member of the Commission
(1) OJ No L 180, 5. 7. 1983, p. 16.
(2) OJ No L 175, 27. 7. 1995, p. 21.
ANNEX I
>REFERENCE TO A FILM>
>START OF GRAPHIC>
QUESTIONS 24 TO 27 AND 32, NEED ONLY BE COMPLETED ONCE PER YEAR.
(COMPLETE QUESTIONS 29 TO 31 IN EACH APPLICATION) YES NO 24. Do you manufacture goods which are similar to and at the same stage of processing in your own factory within the EU as the products to be reimported? (Article 2 (2) (a) of Regulation (EC) No 3036/94) 25. Are the main manufacturing processes of the similar goods performed in your own factory within the EU (i.e. sewing and assembly or knitting in the case of fully-fashioned garments obtained from yarn)? (Article 2 (2) (a) of Regulation (EC) No 3036/94) 26. Have you maintained your textile manufacturing activity in the EU with respect to the nature of the products and their quantities? (If not, please indicate reasons or make reference to past correspondence) (Article 3 (3) of Regulation No 3036/94) 27. Has your level of employment decreased? (If so please indicate reasons and attach statistics, if necessary, or make reference to past correspondence) (Article 5 (4) of Regulation (EC) No 3036/94) 28. Have you applied for a prior authorization in another Member State for the same quota period? (If so please attach a copy or make reference to past correspondence) (Article 3 (4) or (5) of Regulation No 3036/94) 29. Are you applying as a past beneficiary with regard to the category and country concerned? (If so please attach justification or make reference to past correspondence) (Article 3 (4) of Regulation No 3036/94) or 30. Is this a new application with regard to the category and country concerned? (If so please attach justification - or make reference to past correspondence - that the value of third country processing will not exceed 50 % of the value of your Community production in the previous year) (Article 3 (5) (2) and (3) of Regulation No 3036/94) if you have answered question 30,
31. Are you applying for a further authorization with regard to the category and country concerned? (If so please attach evidence - or make reference to past correspondence - that 50 % of your previous authorization has been reimported or that 80 % has been exported) (Article 3 (5) (4) of Regulation No 3036/94) and lasty, if you have anwered question 30,
32. Does the value of your Community production in the previous year include subcontract production? (If so, and you have not yet given this information, please attach declarations from subcontractors that they will not apply for the same quantities) (Article 2 (2) (a) of Regulation No 3036/94) I, the undersigned, hereby declare that the particulars contained in this application are accurate and the documents enclosed are authentic and I submit the following documents:
1. contracts,
2. proof of origin of the goods temporarily exported, and 3. other documents to support this application (numbered).
I also undertake:
(i) to present at the request of the competent authorities any supplementary documentation or information they consider necessary to issue the prior authorization and to accept, if need be, the control by the competent authorities of the stock records relating to the authorization;
(ii) to retain such stock records for a period of three years from the end of the calendar year to the issue of the authorization(s);
(iii) to make clearly identifiable the goods temporarily exported and reimported;
(iv) to make available all other evidence or samples the competent authorities deem necessary to control the use of this authorization; and (v) to return the prior authorization at the latest within 15 days of the expiry period.
I request the issue of a prior authorization for the goods detailed in the application.
Signed Name Date Position with company (Please state if you ar acting as a representative in the name of and on behalf of another person and attach a copy of your authority to act on their behalf).
>END OF GRAPHIC>
ANNEX II
>REFERENCE TO A FILM>
>REFERENCE TO A FILM>
>REFERENCE TO A FILM>
>REFERENCE TO A FILM>
>REFERENCE TO A FILM>
>REFERENCE TO A FILM>
ANNEX III
Maximum amounts referred to in Article 3, (5) of the basic Regulation
>TABLE POSITION>
