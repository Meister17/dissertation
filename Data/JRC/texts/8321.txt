COUNCIL REGULATION (EC) No 2704/1999
of 14 December 1999
amending Regulation (EC) No 1251/1999 establishing a support system for producers of certain arable crops
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 37 thereof,
Having regard to the proposal from the Commission(1),
Having regard to the Opinion of the European Parliament(2),
Having regard to the Opinion of the Economic and Social Committee(3),
Whereas:
(1) Article 6(3) of Council Regulation (EC) No 1251/1999(4) provides that the land set-aside may be used for the provision of materials for the manufacture within the Community of products not primarily intended for human or animal consumption, provided that effective control systems are applied;
(2) In order to ensure compliance with point 7 of the Memorandum of Understanding on certain oilseeds between the European Economic Community and the United States of America within the framework of the GATT it is necessary to provide for the possibility of reducing the amount of by-products which may be produced and put to a feed or food use, if the total quantity of such by-products would otherwise exceed 1 million metric tonnes annually expressed in soya meal equivalentes;
(3) It is therefore necessary to amend Regulation (EC) No 1251/1999,
HAS ADOPTED THIS REGULATION:
Article 1
The following subparagraph shall be added to Article 6(3) of Regulation (EC) No 1251/1999: "If the quantity of by-products for feed or food uses likely to be made available as a result of the cultivation of oilseeds on land set-aside under the first subparagraph will, on the basis of the forecast quantities covered by contracts made with producers, exceed 1 million metric tonnes annually expressed in soya bean meal equivalents, in order to limit such quantity to 1 million metric tonnes, the amount of the forecast quantity under each contract, which may be used for feed or food uses, shall be reduced."
Article 2
This Regulation shall enter into force on the seventh day following that of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 14 December 1999.
For the Council
The President
K. HEMILÄ
(1) OJ C 12, 17.1.1996, p. 11.
(2) OJ C 141, 13.5.1996, p. 277.
(3) OJ C 97, 1.4.1996, p. 30.
(4) OJ L 160, 26.6.1999, p. 1.
