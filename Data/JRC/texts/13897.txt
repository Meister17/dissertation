Commission Regulation (EC) No 928/2006
of 22 June 2006
amending Council Regulation (EC) No 32/2000 as regards certain new Community tariff quotas bound in GATT
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 32/2000 of 17 December 1999 opening and providing for the administration of Community tariff quotas bound in GATT and certain other Community tariff quotas and establishing detailed rules for adjusting the quotas, and repealing Council Regulation (EC) No 1808/95 [1], and in particular Article 9(1)(b) thereof,
Whereas:
(1) By Decision 2006/333/EC [2], the Council has approved the Agreement in the form of an Exchange of Letters between the European Community and the United States of America pursuant to Article XXIV:6 and Article XXVIII of the General Agreement on Tariffs and Trade (GATT) 1994 relating to the modification of concessions in the schedules of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic in the course of their accession to the European Union.
(2) The Agreement in the form of an Exchange of Letters between the European Community and the United States of America provides for new annual tariff quotas for certain goods.
(3) Regulation (EC) No 32/2000 opened and provided for the administration of Community tariff quotas bound in GATT designed to be used following the chronological order of dates of acceptance of customs declarations.
(4) To implement certain new annual tariff quotas for which provision is made in the Agreement in the form of an Exchange of Letters, it is necessary to amend Regulation (EC) No 32/2000.
(5) Council Regulation (EC) No 711/2006 of 20 March 2006, concerning the implementation of the Agreement in the form of an Exchange of Letters between the European Community and the United States of America pursuant to Article XXIV:6 and Article XXVIII of the General Agreement on Tariffs and Trade (GATT) 1994 relating to the modification of concessions in the schedules of the Czech Republic, the Republic of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic of Lithuania, the Republic of Hungary, the Republic of Malta, the Republic of Poland, the Republic of Slovenia and the Slovak Republic in the course of their accession to the European Union, amending and supplementing Annex I to Regulation (EEC) No 2658/87 on the tariff and statistical nomenclature and on the Common Customs Tariff [3], provides for the application of the new tariff quotas six weeks from the date of its publication in the Official Journal of the European Union, therefore this Commission implementing Regulation should apply from the same date.
(6) The measures provided for in this Regulation are in accordance with the opinion of the Customs Code Committee,
HAS ADOPTED THIS REGULATION:
Article 1
Annex I to Regulation (EC) No 32/2000 is amended as set out in the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Union.
It shall apply with effect from 22 June 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 22 June 2006.
For the Commission
László Kovács
Member of the Commission
[1] OJ L 5, 8.1.2000, p. 1. Regulation as last amended by Commission Regulation (EC) No 2158/2005 (OJ L 342, 24.12.2005, p. 61).
[2] OJ L 124, 11.5.2006, p. 13.
[3] OJ L 124, 11.5.2006, p. 1.
--------------------------------------------------
ANNEX
The following rows are inserted in the table in Annex I to Regulation (EC) No 32/2000:
"09.0084 | 17025000 | | Chemically pure fructose | From 1 January to 31 December | 1253 tonnes | 20 |
09.0085 | 1806 | | Chocolate and other food preparations containing cocoa | From 1 January to 31 December | 107 tonnes | 43 |
09.0086 | 19021100 190219 19022091 19022099 190230 190240 | | Pasta, whether or not cooked or stuffed or otherwise prepared, except stuffed pasta of CN subheadings 19022010 and 19022030; couscous, whether or not prepared | From 1 January to 31 December | 532 tonnes | 11 |
09.0087 | 19019099 19043000 19049080 19059020 | | Food preparations of cereals | From 1 January to 31 December | 191 tonnes | 33 |
09.0088 | 21069098 | | Other food preparations not elsewhere specified or included | From 1 January to 31 December | 921 tonnes | 18" |
--------------------------------------------------
