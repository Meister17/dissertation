Commission Decision
of 15 November 2005
amending its Rules of Procedure
(2005/960/EC, Euratom)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community, and in particular Article 218(2) thereof,
Having regard to the Treaty establishing the European Atomic Energy Community, and in particular Article 131 thereof,
Having regard to the Treaty on European Union, and in particular Article 28(1) and Article 41(1) thereof,
HAS DECIDED AS FOLLOWS:
Article 1
Articles 1 to 28 of the Rules of Procedure of the Commission [1] shall be replaced by the text in the Annex to this Decision.
Article 2
This Decision shall enter into force on 1 January 2006.
Article 3
This Decision shall be published in the Official Journal of the European Union.
Done at Brussels, 15 November 2005.
For the Commission
José Manuel Barroso
The President
[1] OJ L 308, 8.12.2000, p. 26. Rules of Procedure as last amended by Commission Decision 2004/563/EC, Euratom (OJ L 251, 27.7.2004, p. 9).
--------------------------------------------------
ANNEX
"CHAPTER I
THE COMMISSION
SECTION 1
General provisions
Article 1
The principle of collective responsibility
The Commission shall act collectively in accordance with these Rules of Procedure and in compliance with the political guidelines laid down by the President.
Article 2
Priorities and work programme
In compliance with the political guidelines laid down by the President, the Commission shall establish its multiannual strategic objectives and its Annual Policy Strategy, on the basis of which it shall each year adopt its work programme and the preliminary draft budget for the following year.
Article 3
The President
1. The President may assign to Members of the Commission special fields of activity with regard to which they shall be specifically responsible for the preparation of the work of the Commission and the implementation of its decisions.
He may change these assignments at any time.
2. The President may set up standing or ad hoc groups of Members of the Commission, designating their chairpersons and deciding on their membership. He shall lay down the mandate of these groups and approve their operating rules.
3. The President shall represent the Commission. He shall designate Members of the Commission to assist him in this task.
Article 4
Decision-making procedures
Commission decisions shall be taken:
(a) at Commission meetings by oral procedure; or
(b) by written procedure in accordance with Article 12; or
(c) by empowerment procedure in accordance with Article 13; or
(d) by delegation procedure in accordance with Article 14.
SECTION 2
Commission meetings
Article 5
Convening Commission meetings
1. Meetings of the Commission shall be convened by the President.
2. The Commission shall, as a general rule, meet at least once a week. It shall hold additional meetings whenever necessary.
3. Members of the Commission shall be required to attend all meetings. The President shall judge whether Members may be released from this obligation in certain circumstances.
Article 6
Agenda of Commission meetings
1. The President shall adopt the agenda of each Commission meeting.
2. Without prejudice to the prerogative of the President to adopt the agenda, any proposal involving significant expenditure must be presented in agreement with the Member of the Commission responsible for the budget.
3. If a Member of the Commission proposes the inclusion of an item on the agenda, the President must be notified as prescribed by the Commission in accordance with the implementing rules referred to in Article 28, hereinafter referred to as "the implementing rules".
4. The agenda and the necessary documents shall be circulated to the Members of the Commission as prescribed by the Commission in accordance with the implementing rules.
5. Where a Member of the Commission requests that an item be withdrawn from the agenda, the item shall, provided the President agrees, be held over for the next meeting.
6. The Commission may, on a proposal from the President, discuss any question which is not on the agenda or for which the necessary working documents have been distributed late. It may decide not to discuss an item on the agenda.
Article 7
Quorum
The number of Members whose presence is necessary to constitute a quorum shall be equal to a majority of the number of Members specified in the Treaty.
Article 8
Decision-making
1. The Commission shall take decisions on the basis of proposals from one or more of its Members.
2. A vote shall be taken if any Member so requests. The vote may be on the original draft text or on an amended draft text by the Member or Members responsible for the initiative or by the President.
3. Commission decisions shall be adopted if a majority of the number of Members specified in the Treaty vote in favour.
4. The President shall formally note the outcome of discussions, which shall be recorded in the minutes of the meeting provided for in Article 11.
Article 9
Confidentiality
Meetings of the Commission shall not be public. Discussions shall be confidential.
Article 10
Attendance of officials or other persons
1. Unless the Commission decides otherwise, the Secretary-General and the President's Head of Cabinet shall attend meetings. The circumstances in which other persons may attend Commission meetings shall be determined in accordance with the implementing rules.
2. In the absence of a Member of the Commission, his Head of Cabinet may attend the meeting and, at the invitation of the President, state the views of the absent Member.
3. The Commission may decide to hear any other person.
Article 11
Minutes
1. Minutes shall be taken of all meetings of the Commission.
2. The draft minutes shall be submitted to the Commission for approval at a subsequent meeting. The approved minutes shall be authenticated by the signatures of the President and the Secretary-General.
SECTION 3
Other decision-making procedures
Article 12
Decisions taken by written procedure
1. The agreement of the Members of the Commission to a draft text from one or more of its Members may be obtained by written procedure, provided the prior approval of the Legal Service and the agreement of the departments consulted in accordance with Article 23 has been obtained.
Such approval and/or agreement may be replaced by an agreement between the Heads of Cabinet under the finalisation written procedure as provided for in the implementing rules.
2. The draft text shall be circulated in writing to all Members of the Commission as laid down by it in accordance with the implementing rules, with a time-limit within which Members must make known any reservations they may have or amendments they wish to make.
3. Any Member of the Commission may, in the course of the written procedure, request that the draft text be discussed. He shall send a reasoned request to that effect to the President.
4. A draft text on which no Member has made and maintained a request for suspension up to the time-limit set for the written procedure shall stand adopted by the Commission.
Article 13
Decisions taken by empowerment procedure
1. The Commission may, provided the principle of collective responsibility is fully respected, empower one or more of its Members to take management or administrative measures on its behalf and subject to such restrictions and conditions as it shall impose.
2. The Commission may also instruct one or more of its Members, with the agreement of the President, to adopt the definitive text of any instrument or of any proposal to be presented to the other institutions, the substance of which has already been determined in discussion.
3. Powers conferred in this way may be subdelegated to the Directors-General and Heads of Department unless this is expressly prohibited in the empowering decision.
4. The provisions of paragraphs 1, 2 and 3 shall be without prejudice to the rules concerning delegation in respect of financial matters or the powers conferred on the appointing authority and the authority empowered to conclude contracts of employment.
Article 14
Decisions taken by delegation procedure
The Commission may, provided the principle of collective responsibility is fully respected, delegate the adoption of management or administrative measures to the Directors-General and Heads of Department, acting on its behalf and subject to such restrictions and conditions as it shall impose.
Article 15
Subdelegation for individual decisions awarding grants and contracts
A Director-General or Head of Department who has received delegated or subdelegated powers under Articles 13 and 14 for the adoption of financing decisions may decide to subdelegate the adoption of certain individual decisions on the award of grants and contracts to the competent Director or, in agreement with the Member of the Commission responsible, to the competent Head of Unit, subject to the restrictions and conditions laid down in the implementing rules.
Article 16
Information concerning decisions adopted
Decisions adopted by written procedure, empowerment procedure or delegation procedure shall be recorded in a day note which shall be recorded in the minutes of the next Commission meeting.
SECTION 4
Provisions common to all decision-making procedures
Article 17
Authentication of acts adopted by the Commission
1. Instruments adopted in the course of a meeting shall be attached, in the authentic language or languages, in such a way that they cannot be separated, to a summary note prepared at the end of the Commission meeting at which they were adopted. They shall be authenticated by the signatures of the President and the Secretary-General on the last page of the summary note.
2. Instruments adopted by written procedure or empowerment procedure in accordance with Article 12 and Article 13(1) and (2) shall be attached, in the authentic language or languages, in such a way that they cannot be separated, to the day note referred to in Article 16. They shall be authenticated by the signature of the Secretary-General on the last page of the day note.
3. Instruments adopted by delegation procedure or by subdelegation shall be attached in the authentic language or languages, in such a way that they cannot be separated, to the day note referred to in Article 16. They shall be authenticated by a certifying statement signed by the official to whom the powers have been delegated or subdelegated in accordance with Article 13(3) and Articles 14 and 15.
4. For the purposes of these Rules of Procedure, "instrument" means any instrument referred to in Article 249 of the EC Treaty and Article 161 of the Euratom Treaty.
5. For the purposes of these Rules of Procedure, "authentic languages" means all the official languages of the Communities, without prejudice to the application of Council Regulation (EC) No 930/2004 [1], in the case of instruments of general application, and the language or languages of those to whom they are addressed, in other cases.
SECTION 5
Preparation and implementation of Commission decisions
Article 18
Groups of Members of the Commission
Groups of Commission Members shall contribute to the coordination and preparation of the work of the Commission within the context of the strategic objectives and priorities laid down by the Commission and in accordance with the mandate and political guidelines laid down by the President.
Article 19
Members' cabinets and relations with departments
1. Members of the Commission shall have their own cabinet to assist them in their work and in preparing Commission decisions. The rules governing the composition of the cabinets shall be laid down by the President.
2. Members of the Commission shall approve their working arrangements with the departments for which they are responsible. In particular, these arrangements must specify the way in which Members of the Commission give instructions to the departments concerned, which will regularly provide them with all the information on their area of activity necessary for them to exercise their responsibilities.
Article 20
The Secretary-General
1. The Secretary-General shall assist the President in preparing the proceedings and conducting the meetings of the Commission. He shall also assist the Members chairing groups of Members set up under Article 3(2) in preparing and conducting their meetings.
2. He shall ensure that decision-making procedures are properly implemented and that effect is given to the decisions referred to in Article 4.
3. He shall help to ensure the necessary coordination between departments in the preparatory stages, in accordance with Article 23, and shall see that documents submitted to the Commission are of good quality in terms of substance and comply with the rules as to form.
4. Except in specific cases he shall take the necessary steps to ensure that Commission instruments are officially notified to those concerned and are published in the Official Journal of the European Union and that documents of the Commission and its departments are transmitted to the other institutions of the European Communities.
5. He shall be responsible for official relations with the other institutions of the European Communities, subject to any decisions by the Commission to exercise any function itself or to assign it to its Members or departments. He shall monitor the proceedings of the other institutions of the European Communities and keep the Commission informed.
CHAPTER II
COMMISSION DEPARTMENTS
Article 21
Structure of departments
A number of Directorates-General and equivalent departments forming a single administrative service shall assist the Commission in the performance of its tasks.
The Directorates-General and equivalent departments shall normally be divided into directorates, and directorates into units.
Article 22
Creation of specific functions and structures
In special cases the Commission may create specific functions or structures to deal with particular matters and shall determine their responsibilities and method of operation.
Article 23
Cooperation and coordination between departments
1. In order to ensure the effectiveness of Commission action, departments shall work in close cooperation and in coordinated fashion from the outset in the preparation or implementation of Commission decisions.
2. The department responsible for preparing an initiative shall ensure from the beginning of the preparatory work that there is effective coordination between all the departments with a legitimate interest in the initiative by virtue of their powers or responsibilities or the nature of the subject.
3. Before a document is submitted to the Commission, the department responsible shall, in accordance with the implementing rules, consult the departments with a legitimate interest in the draft text in sufficient time.
4. The Legal Service shall be consulted on all drafts or proposals for legal instruments and on all documents which may have legal implications.
The Legal Service must always be consulted before initiating any of the decision-making procedures provided for in Articles 12, 13 and 14, except for decisions concerning standard instruments where its agreement has already been secured (repetitive instruments). Such consultation is not required for the decisions referred to in Article 15.
5. The Secretariat-General shall be consulted on all initiatives which:
(a) are of political importance; or
(b) are part of the Commission's annual work programme or the programming instrument in force; or
(c) concern institutional issues; or
(d) are subject to impact assessment or public consultation.
6. With the exception of the decisions referred to in Article 15, the Directorates-General responsible for the budget and for personnel and administration shall be consulted on all documents which may have implications for the budget and finances or for personnel and administration respectively. The department responsible for combating fraud shall likewise be consulted where necessary.
7. The department responsible shall endeavour to frame a proposal that has the agreement of the departments consulted. In the event of a disagreement it shall append to its proposal the differing views expressed by these departments, without prejudice to Article 12.
CHAPTER III
DEPUTISING
Article 24
Continuity of service
The Members of the Commission and the departments shall ensure they take all appropriate measures to ensure continuity of service, in compliance with the provisions adopted for that purpose by the Commission or the President.
Article 25
Deputising for the President
Where the President is prevented from exercising his functions, they shall be exercised by one of the Vice-Presidents or Members in the order of precedence laid down by the President.
Article 26
Deputising for the Secretary-General
Where the Secretary-General is prevented from exercising his functions, they shall be exercised by the most senior Deputy Secretary-General present or, in the event of equal seniority, by the eldest or by an official designated by the Commission.
If there is no Deputy Secretary-General present and no official has been designated by the Commission, the most senior subordinate official present in the highest category and grade or, in the event of equal seniority, the one who is eldest, shall deputise.
Article 27
Deputising for hierarchical superiors
1. Where a Director-General is prevented from exercising his functions, they shall be exercised by the most senior Deputy Director-General present or, in the event of equal seniority, by the eldest or by an official designated by the Commission.
If there is no Deputy Director-General present and no official has been designated by the Commission, the most senior subordinate present in the highest category and grade or, in the event of equal seniority, the one who is eldest, shall deputise.
2. Where a Head of Unit is prevented from exercising his functions, they shall be exercised by the Deputy Head of Unit or an official designated by the Director-General.
If there is no Deputy Head of Unit present and no official has been designated by the Director-General, the most senior subordinate present in the highest category and grade or, in the event of equal seniority, the one who is eldest, shall deputise.
3. Where any other hierarchical superior is prevented from exercising his functions, they shall be exercised by an official designated by the Director-General in agreement with the Member of the Commission responsible. If no replacement has been designated, the most senior subordinate present in the highest category and grade, or in the event of equal seniority, the one who is eldest, shall deputise.
CHAPTER IV
FINAL PROVISIONS
Article 28
Implementing rules and supplementary measures
The Commission shall, as necessary, lay down implementing rules to give effect to these Rules of Procedure.
The Commission may adopt supplementary measures relating to the functioning of the Commission and of its departments, taking into account developments in technology and information technology.
Article 29
Entry into force
These Rules of Procedure shall enter into force on 1 January 2006."
[1] OJ L 169, 1.5.2004, p. 1.
--------------------------------------------------
