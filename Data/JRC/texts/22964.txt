COMMISSION REGULATION (EEC) No 3777/91 of 18 December 1991 opening a sale by standing invitation to tender for use in the Community of alcohol of vinous origin held by intervention agencies
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 822/87 of 16 March 1987 on the common organization of the market in wine (1), as last amended by Regulation (EEC) No 1734/91 (2),
Having regard to Council Regulation (EEC) No 3877/88 of 12 December 1988 laying down general rules on the disposal of alcohol obtained from the distillation operations referred to in Articles 35, 36 and 39 of Regulation (EEC) No 822/87 and held by intervention agencies (3), and in particular Article 1 thereof,
Whereas Commission Regulation (EEC) No 1780/89 (4), as last amended by Regulation (EEC) No 3776/91 (5), lays down detailed rules for the disposal of alcohol obtained from the distillation operations referred to in Articles 35, 36 and 39 of Regulation (EEC) No 822/87 and held by intervention agencies; whereas the rules on standing invitations to tender were amended by Regulation (EEC) No 3776/91;
Whereas, because of the cost of storing alcohol, it is desirable to open a new sale by standing invitation to tender of alcohol of vinous origin intended for new industrial uses;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Wine,
HAS ADOPTED THIS REGULATION:
Article 1
1. There shall be sale by standing invitation to tender of alcohol at 100 % vol obtained from the distillation operations referred to in Articles 35, 36 and 39 of Regulation (EEC) No 822/87 and held by the Spanish, French, Italian and Greek intervention agencies.
2. The quantity of alcohol awarded under the said tendering procedure shall not exceed 400 000 hectolitres per year.
3. The alcohol offered for sale shall be for use in the Community in accordance with Article 2 of Regulation (EEC) No 1780/89.
Article 2
The sale shall take place in accordance with Regulation (EEC) No 1780/89, and in particular Articles 2 to 9 and 29 to 38 thereof.
Article 3
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities. This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 18 December 1991. For the Commission
Ray MAC SHARRY
Member of the Commission
(1) OJ No L 84, 27. 3. 1987, p. 1. (2) OJ No L 163, 26. 6. 1991, p. 6. (3) OJ No L 346, 15. 12. 1988, p. 7. (4) OJ No L 178, 24. 6. 1989, p. 1. (5) See page 43 of this Official Journal.
