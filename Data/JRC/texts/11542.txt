Council Directive 2006/91/EC
of 7 November 2006
on control of San José Scale
(Codified version)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Articles 37 and 94 thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament [1],
Having regard to the opinion of the European Economic and Social Committee [2],
Whereas:
(1) Council Directive 69/466/EEC of 8 December 1969 on control of San José Scale [3] has been substantially amended [4]. In the interests of clarity and rationality the said Directive should be codified.
(2) The production of woody dicotyledonous plants and their fruit occupies an important place in Community agriculture.
(3) The yield of that production is constantly threatened by harmful organisms.
(4) Through the protection of such plants against such harmful organisms, not only should productive capacity be maintained but also agricultural productivity increased.
(5) Protective measures to prevent the introduction of harmful organisms into individual Member States would have only a limited effect if such organisms were not controlled simultaneously and methodically throughout the Community and were not prevented from spreading.
(6) One of the organisms most harmful to woody dicotyledonous plants is San José Scale (Quadraspidiotus perniciosus Comst.).
(7) This pest has occurred in several Member States and there are contaminated areas within the Community.
(8) There is a permanent risk to the cultivation of woody dicotyledonous plants throughout the Community if effective measures are not taken to control this pest and prevent it from spreading.
(9) To eradicate this pest, minimum provisions should be adopted for the Community. Member States should be able to adopt additional or stricter provisions where necessary.
(10) This Directive should be without prejudice to the obligations of the Member States relating to the timelimits for transposition into national law of the Directives set out in Annex I, Part B,
HAS ADOPTED THIS DIRECTIVE:
Article 1
This Directive concerns the minimum measures to be taken within the Member States to control San José Scale (Quadraspidiotus perniciosus Comst.) and to prevent it from spreading.
Article 2
For the purposes of this Directive, the following definitions shall apply:
(a) "plants" means live plants and live parts of plants with the exception of fruit and seeds;
(b) "contaminated plants or fruit" means plants or fruit on which one or more San José Scale insects are found, unless it is confirmed that they are dead;
(c) "San José Scale host plants" means plants of the genera Acer L., Cotoneaster Ehrh., Crataegus L., Cydonia Mill., Euonymus L., Fagus L., Juglans L., Ligustrum L., Malus Mill., Populus L., Prunus L., Pyrus L., Ribes L., Rosa L., Salix L., Sorbus L., Syringa L., Tilia L., Ulmus L., Vitis L.;
(d) "nurseries" means plantations in which plants intended for transplanting, multiplying or distributing as individually rooted plants are grown.
Article 3
When an occurrence of San José Scale is recorded, Member States shall demarcate the contaminated area and a safety zone large enough to ensure the protection of the surrounding areas.
Article 4
The Member States shall provide that, in contaminated areas and safety zones, San José Scale host plants shall be appropriately treated to control this pest and prevent it from spreading.
Article 5
The Member States shall provide that:
(a) all contaminated plants in nurseries shall be destroyed;
(b) all other plants which are contaminated or suspected of being contaminated and which are growing in a contaminated area shall be treated in such a way that those plants and the fresh fruit therefrom are no longer contaminated when moved;
(c) all rooted San José Scale host plants growing within a contaminated area, and parts of such plants which are intended for multiplication and are produced within that area, may be replanted within the contaminated area or transported away from it only if they have not been found to be contaminated and if they have been treated in such a way that any San José Scale insects which might still be present are destroyed.
Article 6
The Member States shall ensure that in the safety zones San José Scale host plants are subjected to official supervision and are inspected at least once a year in order to detect any occurrence of San José Scale.
Article 7
1. The Member States shall provide that in any batch of plants (other than those that are rooted in the ground) and of fresh fruit within which contamination has been found, the contaminated plants and fruit shall be destroyed and the other plants and fruit in the batch treated or processed in such a way that any San José Scale insects which might still be present are destroyed.
2. Paragraph 1 shall not apply to slightly contaminated batches of fresh fruit.
Article 8
The Member States shall revoke the measures taken to control San José Scale or to prevent it from spreading only if San José Scale is no longer found to be present.
Article 9
The Member States shall prohibit the holding of San José Scale.
Article 10
1. Member States may authorise:
(a) derogations from the measures referred to in Articles 4 and 5, Article 7(1) and Article 9 for scientific and phytosanitary purposes, tests and selection work;
(b) by way of derogation from point (b) of Article 5 and Article 7(1), the immediate processing of contaminated fresh fruit;
(c) by way of derogation from point (b) of Article 5 and Article 7(1), the movement of contaminated fresh fruit within the contaminated area.
2. The Member States shall ensure that the authorisations provided for in paragraph 1 are granted only where adequate controls guarantee that they do not prejudice the control of San José Scale and create no risk of the spread of this pest.
Article 11
Member States may adopt such additional or stricter provisions as may be required to control San José Scale or to prevent it from spreading.
Article 12
Directive 69/466/EEC is hereby repealed, without prejudice to the obligations of the Member States relating to the time limits for transposition into national law of the Directives set out in Annex I, Part B.
References to the repealed Directive shall be construed as references to this Directive and shall be read in accordance with the correlation table in Annex II.
Article 13
This Directive shall enter into force on the 20th day following its publication in the Official Journal of the European Union.
Article 14
This Directive is addressed to the Member States.
Done at Brussels, 7 November 2006.
For the Council
The President
E. Heinäluoma
[1] Opinion delivered on 12 October 2006 (not yet published in the Official Journal).
[2] Opinion delivered on 5 July 2006 (not yet published in the Official Journal).
[3] OJ L 323, 24.12.1969, p. 5. Directive as amended by Directive 77/93/EEC (OJ L 26, 31.1.1977, p. 20).
[4] See Annex I, Part A, of this Official Journal.
--------------------------------------------------
ANNEX I
PART A
Repealed Directive with its amendment
Council Directive 69/466/EEC (OJ L 323, 24.12.1969, p. 5) | |
Council Directive 77/93/EEC (OJ L 26, 31.1.1977, p. 20) | Article 19 only |
PART B
List of time-limits for transposition into national law
(referred to in Article 12)
Directive | Time-limit for transposition |
69/466/EEC [1] | 9 December 1971 |
77/93/EEC [2] [3] [4] | 1 May 1980 |
[1] For Ireland and the United Kingdom: 1 July 1973.
[2] In accordance with the procedure laid down in Article 16 of Directive 77/93/EEC, Member States may be authorised, on request, to comply with certain of the provisions of this Directive by a date later than 1 May 1980, but not later than 1 January 1981.
[3] For Greece: 1 January 1983.
[4] For Spain and Portugal: 1 March 1987.
--------------------------------------------------
ANNEX II
Correlation table
Directive 69/466/EEC | This Directive |
Articles 1-11 | Articles 1-11 |
Article 12 | — |
— | Article 12 |
— | Article 13 |
Article 13 | Article 14 |
— | Annex I |
— | Annex II |
--------------------------------------------------
