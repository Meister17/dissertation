COMMISSION REGULATION (EC) No 2714/94 of 8 November 1994 amending Regulation (EEC) No 2054/89 laying down detailed rules for the application of the minimum import price system for dried grapes
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 426/86 of 24 February 1986 on the common organization of the market in products processed from fruit and vegetables (1), as last amended by Commission Regulation (EC) No 1490/94 (2), and in particular Article 9 (6) thereof,
Whereas Article 2 (3) of Commission Regulation (EEC) No 2054/89 (3), as last amended by Regulation (EEC) No 3821/92 (4), lays down the terms under which the weighted average of resale prices of dried grapes is considered to be the import price; whereas, in order to prevent any artificial reduction in protection, provision should be made for customs import charges corresponding to entry and indirect taxes actually paid on importation to be deducted from the resale prices recorded; whereas Article 2 (6) of that Regulation defines 'end user'; whereas manufacturers who put up the product in immediate packings cannot be covered by the term since even if such packaging and presentation results in a change in the CN code, it cannot be considered processing for the purposes of this Regulation;
Whereas Article 6 of Regulation (EEC) No 2054/89 provides for a special control procedure; whereas experience shows that where that procedure is applied, the release for free circulation of the goods should only be authorized once the security laid down in Article 248 of Commission Regulation (EEC) No 2454/93 of 2 July 1993 laying down provisions for the implementation of Council Regulation (EC) No 2913/92 establishing the Community Customs Code (5) has been lodged, as last amended by Regulation (EC) No 2193/94 (6); whereas that security must be demanded if the customs authorities have doubts as to the authenticity of the import price, even prior to the checks provided for in the said Article 248; whereas, in the case of a posteriori checks, it should be made clear that steps are being taken to recover duties owed in accordance with Article 220 of Council Regulation (EEC) No 2913/92 of 12 October 1992 establishing the Communtiy Customs Code (7); whereas, in addition, it should be made clear that interest is payable on duties owed under all control procedures;
Whereas Article 7 (1) of Regulation (EEC) No 2054/89 lays down the terms under which the minimum import price may be deemed to be observed; whereas experience shows that, to avoid distortion, account must to be taken of the customs import charges actually paid and of the cost of any treatment the imported product undergoes after importation and prior to sale to the end user;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Products Processed from Fruit and Vegetables,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EEC) No 2054/89 is hereby amended as follows:
1. Article 2 is amended as follows:
(a) paragraph 3 is replaced by the following:
'3. Where it is found that prices on resale, directly or via commercial intermediaries, are, after deduction of the customs import duties actually paid, less than the minimum price as regards more than 15 % of any consignment imported, the weighted average of those adjusted prices shall be deemed the import price.';
(b) paragraph 6 is replaced by the following:
'6. For the purposes of this Regulation, the end user shall mean either a manufacturer who uses the product in question with a view to processing it, otherwise than by packaging it, into a product covered by a CN code other than that shown in the declaration of release for free circulation, or a retailer selling exclusively to consumers.';
2. Article 6 is replaced by the following:
'Article 6
1. Where the customs authorities have well-founded doubts that the price shown in the declaration of release for free circulation reflects the actual import price, they shall authorize release for free circulation only after the importer has lodged the security referred to in Article 248 (1) of Regulation (EEC) No 2454/93 plus interest for the period of six months referred to in the second subparagraph. The rate of interest applicable shall be that in force under national law for the recovery of sums due.
Importers shall have six months within which to prove that the product has been disposed of under conditions which guarantee observance of the minimum import price. Failure to comply with the time limit shall entail the loss of the security, without prejudice to paragraph 2.
2. The time limit laid down in paragraph 1 may be extended by the competent authorities by up to three months on a duly justified application by the importer and provided that the security is adjusted accordingly.';
3. Article 7 (1) is replaced by the following:
'1. The minimum import price shall be deemed to be observed if the importer furnishes proof in respect of at least 95 % of the consignment imported that, at all marketing stages including sale to end users, the product has been sold at a price at least equal to the minimum import price after deduction of customs import charges actually paid. If the product undergoes treatment after its release for free circulation and before its sale to the end user, the cost of such treatment shall be reflected in the selling price to the end user.';
4. Article 10 is replaced by the following:
'Article 10
Where, in the course of a check, the competent authorities find that the minimum import price has not been observed, they shall collect the duties owed in accordance with Article 220 of Regulation (EEC) No 2913/92. In establishing the amount of the duties to be recovered or remaining to be recovered, they shall take account of interest incurred from the date of release for free circulation of the goods until that of recovery. The rate of interest applied shall be that in force under national law for the recovery of the sums due.'
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 8 November 1994.
For the Commission
René STEICHEN
Member of the Commission
(1) OJ No L 49, 27. 2. 1986, p. 1.
(2) OJ No L 161, 29. 6. 1994, p. 13.
(3) OJ No L 195, 11. 7. 1989, p. 14.
(4) OJ No L 387, 31. 12. 1992, p. 24.
(5) OJ No L 253, 11. 10. 1993, p. 1.
(6) OJ No L 235, 9. 9. 1994, p. 6.
(7) OJ No L 302, 19. 10. 1992, p. 1.
