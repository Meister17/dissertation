COUNCIL DIRECTIVE 98/52/EC of 13 July 1998 on the extension of Directive 97/80/EC on the burden of proof in cases of discrimination based on sex to the United Kingdom of Great Britain and Northern Ireland
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 100 thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas the Council, acting in accordance with the Agreement on social policy annexed to Protocol 14 on social policy annexed to the EC Treaty, and in particular Article 2(2) thereof, adopted Directive 97/80/EC (4); whereas, as a result, the said Directive does not apply to the United Kingdom;
Whereas the Amsterdam European Council, held on 16 and 17 June 1997, noted with approval the agreement of the Intergovernmental Conference to incorporate the Agreement on social policy in the Treaty and also noted that a means had to be found to give legal effect to the wish of the United Kingdom to accept the Directives already adopted on the basis of that Agreement and those which may be adopted before the entry into force of the Treaty of Amsterdam;
Whereas, at the Council of 24 July 1997, the Council and the Commission agreed to put into effect the conclusions adopted at the Amsterdam European Council; whereas they also agreed to apply the same procedure, mutatis mutandis, to future Directives adopted on the basis of the Agreement on social policy; whereas this Directive seeks to achieve this aim by extending Directive 97/80/EC to the United Kingdom;
Whereas the fact that Directive 97/80/EC is not applicable in the United Kingdom directly affects the functioning of the common market; whereas an effective implementation of the principle of equal treatment for men and women, in particular as regards the rules organizing the burden of proof in cases of discrimination based on sex, in all the Member States will improve the functioning of the common market;
Whereas the adoption of this Directive will make Directive 97/80/EC applicable in the United Kingdom; whereas, from the date on which this Directive enters into force, the term 'Member States` in Directive 97/80/EC should be construed as including the United Kingdom;
Whereas the United Kingdom should benefit from the same period of three years that was granted to other Member States to bring into force the necessary provisions to comply with Directive 97/80/EC,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Without prejudice to Article 2, Directive 97/80/EC shall apply to the United Kingdom of Great Britain and Northern Ireland.
Article 2
The following paragraph shall be inserted after the first paragraph of Article 7 of Directive 97/80/EC:
'As regards the United Kingdom of Great Britain and Northern Ireland, the date of 1 January 2001 in paragraph 1 shall be replaced by 22 July 2001.`
Article 3
This Directive shall enter into force on the day of its publication.
Article 4
This Directive is addressed to the Member States.
Done at Brussels, 13 July 1998.
For the Council
The President
W. SCHÜSSEL
(1) OJ C 332, 7. 11. 1996, p. 11.
(2) OJ C 167, 1. 6. 1998.
(3) OJ C 157, 25. 5. 1998, p. 64.
(4) OJ L 14, 20. 1. 1998, p. 6.
