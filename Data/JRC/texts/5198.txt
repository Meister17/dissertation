Decision of the EEA Joint Committee
No 3/2005
of 8 February 2005
amending Annex II (Technical regulations, standards, testing and certification) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex II to the Agreement was amended by Decision of the EEA Joint Committee No 162/2004 of 3 December 2004 [1].
(2) Commission Directive 2004/78/EC of 29 April 2004 amending Directive 2001/56/EC of the European Parliament and of the Council relating to heating systems for motor vehicles and their trailers and Council Directive 70/156/EEC for the purposes of adapting to technical progress [2], as corrected by OJ L 231, 30.6.2004, p. 69, is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
Chapter I of Annex II to the Agreement shall be amended as follows:
1. The following indent shall be added in point 1 (Council Directive 70/156/EEC):
"— 32004 L 0078: Commission Directive 2004/78/EC of 29 April 2004 (OJ L 153, 30.4.2004, p. 100), as corrected by OJ L 231, 30.6.2004, p. 69."
2. The following shall be added in the 19th indent of point 1 (Directive 2001/56/EC of the European Parliament and the Council):
", as amended by:
- 32004 L 0078: Commission Directive 2004/78/EC of 29 April 2004 (OJ L 153, 30.4.2004, p. 100), as corrected by OJ L 231, 30.6.2004, p. 69."
Article 2
The texts of Directive 2004/78/EC in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 9 February 2005, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [3].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 8 February 2005.
For the EEA Joint Committee
The President
Richard Wright
[1] OJ L 133, 26.5.2005, p. 3.
[2] OJ L 153, 30.4.2004, p. 100.
[3] No constitutional requirements indicated.
--------------------------------------------------
