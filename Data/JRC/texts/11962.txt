Commission Decision
of 17 March 2006
concerning certain interim protection measures in relation to a suspicion of highly pathogenic avian influenza in Israel
(notified under document number C(2006) 902)
(Text with EEA relevance)
(2006/227/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/496/EEC of 15 July 1991 laying down the principles governing the organisation of veterinary checks on animals entering the Community from third countries and amending Directives 89/662/EEC, 90/425/EEC and 90/675/EEC [1], and in particular Article 18 (1) thereof,
Having regard to Council Directive 97/78/EC of 18 December 1997 laying down the principles governing the organisation of veterinary checks on products entering the Community from third countries [2], and in particular Article 22 (1) thereof,
Whereas:
(1) Avian influenza is an infectious viral disease in poultry and birds, causing mortality and disturbances which can quickly take epizootic proportions liable to present a serious threat to animal and public health and to reduce sharply the profitability of poultry farming. There is a risk that the disease agent might be introduced via international trade in live poultry and poultry products.
(2) Isarel has notified to the Commission the isolation of an H5 avian influenza virus collected from a clinical case. The clinical picture allows the suspicion of highly pathogenic avian influenza pending the determination of the neuraminidase (N) type.
(3) In view of the animal health risk of disease introduction into the Community, it is therefore appropriate as an immediate measure to suspend imports of live poultry, ratites, farmed and wild feathered game birds, live birds other than poultry and hatching eggs of these species from Israel.
(4) As Israel is authorised for imports of game trophies and eggs for human consumption, imports into the Community of these products should be suspended as well because of the animal health risk involved.
(5) Furthermore the importation into the Community from Israel should be suspended for fresh meat of poultry, ratites and wild and farmed feathered game and importation of meat preparations, minced meat, mechanically separated meat and meat products consisting of or containing meat of those species.
(6) Certain products derived from poultry slaughtered before 15 February 2006 should also continue to be authorised, taking into account the incubation period of the disease.
(7) Commission Decision 2005/432/EC [3] laying down the animal and public health conditions and model certificates for imports of meat products for human consumption from third countries and repealing Decisions 97/41/EC, 97/221/EC and 97/222/EC lays down the list of third countries from which Member States may authorise the importation of meat products and establishes treatment regimes considered effective in inactivating the respective pathogens. In order to prevent the risk of disease transmission via such products, appropiate treatment must be applied depending on the health status of the country of origin and the species the product is obtained from. It appears therefore appropriate, that imports of poultry meat products originating in Israel and treated to a temperature of at least 70 °C throughout the product should continue to be authorised.
(8) The situation shall be reviewed at the next meeting of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
1. Member States shall suspend the importation from the territory of Israel of:
- live poultry, ratites, farmed and wild feathered game, live birds other than poultry as defined in Article 1, third indent, of Decision 2000/666/EC, and hatching eggs of these species,
- fresh meat of poultry, ratites, farmed and wild feathered game,
- minced meat, meat preparations, mechanically separated meat and meat products consisting of or containing meat of those species,
- raw pet food and unprocessed feed material containing any parts of those species,
- eggs for human consumption, and
- non-treated game trophies from any birds.
2. By way of derogation from paragraph 1, Member States shall authorise the importation of the products covered by paragraph 1 first to fourth indent, which have been obtained from birds slaughtered before 15 February 2006.
3. In the veterinary certificates/commercial documents accompanying consignments of the products referred to in paragraph 2 the following words as appropriate to the species shall be included:
"Fresh poultry meat/fresh ratite meat/fresh meat of wild feathered game/fresh meat of farmed feathered game/meat product consisting of, or containing meat of poultry, ratites, farmed or wild feathered game meat/meat preparation consisting of, or containing meat of poultry, ratites, farmed or wild feathered game meat/raw pet food and unprocessed feed material containing any parts of poultry, ratites, farmed or wild feathered game [4] obtained from birds slaughtered before 15 February 2006 and in accordance with Article 1(2) of Commission Decision 2006/227/EC [5].
4. By derogation from paragraph 1, third indent, Member States shall authorise the importation of meat products consisting of or containing meat of poultry, ratites, farmed and wild feathered game under the condition that the meat of these species has undergone at least one of the specific treatments referred to under points B, C or D in Part IV of Annex II to Commission Decision 2005/432/EC.
Article 2
Member States shall amend the measures they apply to imports so as to bring them into compliance with this Decision and they shall give immediate appropriate publicity to the measures adopted. They shall immediately inform the Commission thereof.
Article 3
This Decision shall apply until 31 May 2006.
Article 4
This Decision is addressed to the Member States.
Done at Brussels, 17 March 2006.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 268, 24.9.1991, p. 56. Directive as last amended by the 2003 Act of Accession.
[2] OJ L 24, 31.1.1998, p. 9. Directive as last amended by Regulation (EC) No 882/2004 of the European Parliament and of the Council (OJ L 165, 30.4.2004, p. 1; corrected version in OJ L 191, 28.5.2004, p. 1).
[3] OJ L 151, 14.6.2005, p. 3.
[4] Delete as appropriate.
[5] OJ L 81, 18.3.2006, p. 35."
--------------------------------------------------
