Commission Regulation (EC) No 605/2006
of 19 April 2006
amending Regulation (EC) No 349/2003 suspending the introduction into the Community of specimens of certain species of wild fauna and flora
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 338/97 of 9 December 1996 on the protection of species of wild fauna and flora by regulating trade therein [1] and in particular Article 19(2) thereof,
After consulting the Scientific Review Group,
Whereas:
(1) Article 4(6) of Regulation (EC) No 338/97 provides that the Commission may establish restrictions to the introduction of certain species into the Community in accordance with the conditions laid down in points (a) to (d) thereof. Furthermore, implementing measures for such restrictions have been laid down in Commission Regulation (EC) No 1808/2001 of 30 August 2001 laying down detailed rules concerning the implementation of Council Regulation (EC) No 338/97 of the protection of species of wild fauna and flora by regulating trade therein [2].
(2) A list of species for which the introduction into the Community is suspended was established in Commission Regulation (EC) No 349/2003 of 25 February 2003 suspending the introduction into the Community of specimens of certain species of wild fauna and flora [3].
(3) On the basis of recent information, the Scientific Review Group has concluded that the conservation status of certain species listed in Annexes A and B to Regulation (EC) No 338/97 will be seriously jeopardised if their introduction into the Community from certain countries of origin is not suspended. The introduction of the following species should therefore be suspended: Ursus thibetanus from the Russian Federation; Cryptoprocta ferox, Scaphiophryne gottlebei, Euphorbia banae and E. kondoi from Madagascar; Panthera leo from Ethiopia; Balaeniceps rex, Grus carunculatus and Chamaeleo fuelleborni from the United Republic of Tanzania; Poicephalus gulielmi from Congo; Accipiter melanoleucus, A. ovampensis, Aviceda cuculoides, Hieraaetus ayresii, H. spilogaster, Macheiramphus alcinus, Spizaetus africanus, Urotriorchis macrourus, Falco chicquera, Asio capensis, Bubo lacteus, B. poensis, Glaucidium perlatum, Scotopelia peli and Python regius from Guinea; Sagittarius serpentarius and Varanus exanthematicus from Togo; Agapornis pullarius from the Democratic Republic of the Congo; Cuora galbinifrons from China; Heosemys spinosa, Leucocephalon yuwonoi, Siebenrockiella crassicollis, Liasis fuscus, Euphyllia cristata, E. divisa, E. fimbriata, Hydnophora microconos and Scolymia vitiensis from Indonesia; Geochelone pardalis from Uganda and Zambia; Uromastyx geyri from Mali and Niger; Cordylus mossambicus, C. vittifer, Tridacna maxima and T. squamosa from Mozambique; Dendrobates pumilio from Nicaragua; Hippopus hippopus from Vanuatu; Hippopus hippopus, Tridacna gigas and T. maxima from Tonga and Vietnam; Tridacna crocea, T. derasa, T. maxima and T. squamosa from Fiji and Vanuatu; Tridacna crocea from Tonga; Tridacna maxima from the Federated States of Micronesia and the Marshall Islands; Tridacna tevoroa from Tonga and Catalaphyllia jardinei from the Solomon Islands.
(4) The Scientific Review Group has also concluded that, on the basis of the most recent available information, the suspension of the introduction into the Community of the following species should no longer be required: Galago senegalensis from Djibouti; Galagoides demidoff from Kenya and Senegal; Callithrix argentata from Paraguay; Saguinus labiatus from Colombia; Callicebus torquatus from Ecuador; Cebus albifrons from Guyana; Cebus capucinus and Aratinga solstitialis from Venezuela; Cebus olivaceus from Peru; Allenopithecus nigroviridis from all range States; Colobus guereza from Equatorial Guinea; Lophocebus albigena from Kenya; Papio hamadryas and Hippopotamus amphibius from Liberia; Cynogale bennettii from Singapore; Ara ararauna from Trinidad and Tobago; Neophema splendida from Australia; Poicephalus gulielmi from Democratic Republic of the Congo; Geochelone chilensis, Homopus areolatus, H. boulengeri, H. femoralis, H. signatus, Kinixys natalensis and Psammobates spp. from all range States; Geochelone denticulata from all range States except for Bolivia and Ecuador; Geochelone elegans from all range States except for Pakistan; Kinixys belliana from all range States except for Mozambique and Benin; Kinixys erosa from all range States except for Togo; Kinixys homeana from all range States except for Benin; Manouria emys from all range States except for Bangladesh, India, Indonesia, Myanmar and Thailand; Testudo horsfieldii from all range States except for China, Pakistan and Kazakhstan; Manouria impressa from all range States except for Vietnam; Phelsuma cepediana and P. trilineata from Madagascar; Phelsuma edwardnewtonii from Mauritius; Varanus albigularis from Lesotho; Varanus rudicollis from the Philippines; Ptyas mucosus from Indonesia and Dactylorhiza incarnata from Norway.
(5) Regulation (EC) No 338/97 as last amended by Regulation (EC) No 1332/2005 provides inter alia for the transfer of Cacatua sulphurea and Pyxis arachnoides from Annex B to Annex A and the deletion from the Annexes of Agapornis roseicollis, therefore, import suspensions in respect of those species should no longer be required.
(6) The countries of origin of the species which are subject to new restrictions to introduction into the Community pursuant to this Regulation have all been consulted.
(7) The Annex to Regulation (EC) No 349/2003 should therefore be amended accordingly and, for clarity purposes, replaced.
(8) The measures provided for in this Regulation are in accordance with the opinion of the Committee on Trade in Wild Fauna and Flora,
HAS ADOPTED THIS REGULATION:
Article 1
The Annex to Regulation (EC) No 349/2003 is replaced by the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the 20th day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 19 April 2006.
For the Commission
Stavros Dimas
Member of the Commission
[1] OJ L 61, 3.3.1997, p. 1. Regulation as last amended by Commission Regulation (EC) No 1332/2005 (OJ L 215, 19.8.2005, p. 1).
[2] OJ L 250, 19.9.2001, p. 1.
[3] OJ L 51, 26.2.2003, p. 3. Regulation as last amended by Regulation (EC) No 252/2005 (OJ L 43, 15.2.2005, p. 3).
--------------------------------------------------
ANNEX
Specimens of species included in Annex A to Regulation (EC) No 338/97 whose introduction into the Community is suspended
Species | Source(s) covered | Specimen(s) covered | Countries of origin | Basis in Article 4(6), Point: |
FAUNA
CHORDATA MAMMALIA
CARNIVORA
Canidae
Canis lupus | Wild | Hunting trophies | Belarus, Kyrgyzstan, Turkey | a |
Ursidae
Ursus arctos | Wild | Hunting trophies | British Columbia | a |
Ursus thibetanus | Wild | Hunting trophies | Russian Federation | a |
Felidae
Lynx lynx | Wild | Hunting trophies | Azerbaijan, Moldova, Ukraine | a |
ARTIODACTYLA
Bovidae
Ovis ammon nigrimontana | Wild | Hunting trophies | Kazakhstan | a |
AVES
FALCONIFORMES
Accipitridae
Leucopternis occidentalis | Wild | All | Ecuador, Peru | a |
Specimens of species included in Annex B to Regulation (EC) No 338/97 whose introduction into the Community is suspended
Species | Source(s) covered | Specimen(s) covered | Countries of origin | Basis in Article 4(6), Point: |
FAUNA
CHORDATA MAMMALIA
MONOTREMATA
Tachyglossidae
Zaglossus bruijni | Wild | All | All | b |
PRIMATES
Loridae
Arctocebus aureus | Wild | All | Central African Republic, Gabon | b |
Arctocebus calabarensis | Wild | All | Nigeria | b |
Nycticebus pygmaeus | Wild | All | Cambodia, Laos | b |
Perodicticus potto | Wild | All | Togo | b |
Galagonidae
Euoticus pallidus (synonym Galago elegantulus pallidus) | Wild | All | Nigeria | b |
Galago matschiei (synonym G. inustus) | Wild | All | Rwanda | b |
Galagoides demidoff (synonym Galago demidovii) | Wild | All | Burkina Faso, Central African Republic | b |
Galagoides zanzibaricus (synonym Galago zanzibaricus) | Wild | All | Malawi | b |
Callitrichidae
Callithrix geoffroyi (synonym C. jacchus geoffroyi) | Wild | All | Brazil | b |
Cebidae
Alouatta fusca | Wild | All | All | b |
Alouatta seniculus | Wild | All | Trinidad and Tobago | b |
Ateles belzebuth | Wild | All | All | b |
Ateles fusciceps | Wild | All | All | b |
Ateles geoffroyi | Wild | All | All | b |
Ateles paniscus | Wild | All | Peru | b |
Cebus capucinus | Wild | All | Belize | b |
Chiropotes satanas | Wild | All | Brazil, Guyana | b |
Lagothrix lagotricha | Wild | All | All | b |
Pithecia pithecia | Wild | All | Guyana | b |
Cercopithecidae
Cercocebus torquatus | Wild | All | Ghana | b |
Cercopithecus ascanius | Wild | All | Burundi | b |
Cercopithecus cephus | Wild | All | Central African Republic | b |
Cercopithecus dryas (including C. salongo) | Wild | All | Democratic Republic of the Congo | b |
Cercopithecus erythrogaster | Wild | All | All | b |
Cercopithecus erythrotis | Wild | All | All | b |
Cercopithecus hamlyni | Wild | All | All | b |
Cercopithecus mona | Wild | All | Togo | b |
Cercopithecus petaurista | Wild | All | Togo | b |
Cercopithecus pogonias | Wild | All | Cameroon, Equatorial Guinea, Nigeria | b |
Cercopithecus preussi (synonym C. lhoesti preussi) | Wild | All | Cameroon, Equatorial Guinea, Nigeria | b |
Colobus polykomos | Wild | All | Côte d'Ivoire, Ghana, Nigeria, Togo | b |
Lophocebus albigena (synonym Cercocebus albigena) | Wild | All | Nigeria | b |
Macaca arctoides | Wild | All | India, Malaysia, Thailand | b |
Macaca assamensis | Wild | All | Nepal | b |
Macaca cyclopis | Wild | All | All | b |
Macaca fascicularis | Wild | All | Bangladesh, India | b |
Macaca maura | Wild | All | Indonesia | b |
Macaca nemestrina | Wild | All | China | b |
Macaca nemestrina pagensis | Wild | All | Indonesia | b |
Macaca nigra | Wild | All | Indonesia | b |
Macaca ochreata | Wild | All | Indonesia | b |
Macaca sylvanus | Wild | All | Algeria, Morocco | b |
Papio hamadryas | Wild | All | Guinea-Bissau, Liberia, Libya | b |
Procolobus badius (synonym Colobus badius) | Wild | All | All | b |
Procolobus verus (synonym Colobus verus) | Wild | All | Benin, Côte d'Ivoire, Ghana, Sierra Leone, Togo | b |
Trachypithecus phayrei (synonym Presbytis phayrei) | Wild | All | Cambodia, China, India | b |
Trachypithecus vetulus (synonym Presbytis senex) | Wild | All | Sri Lanka | b |
XENARTHRA
Myrmecophagidae
Myrmecophaga tridactyla | Wild | All | Belize, Uruguay | b |
RODENTIA
Sciuridae
Ratufa affinis | Wild | All | Singapore | b |
Ratufa bicolor | Wild | All | China | b |
CARNIVORA
Canidae
Chrysocyon brachyurus | Wild | All | Bolivia, Peru | b |
Mustelidae
Lutra maculicollis | Wild | All | United Republic of Tanzania | b |
Viverridae
Cryptoprocta ferox | Wild | All | Madagascar | b |
Cynogale bennettii | Wild | All | Brunei, China, Indonesia, Malaysia, Thailand | b |
Eupleres goudotii | Wild | All | Madagascar | b |
Fossa fossana | Wild | All | Madagascar | b |
Felidae
Leptailurus serval | Wild | All | Algeria | b |
Oncifelis colocolo | Wild | All | Chile | b |
Panthera leo | Wild | All | Ethiopia | b |
Prionailurus bengalensis | Wild | All | Macao | b |
Profelis aurata | Wild | All | Togo | b |
PERISSODACTYLA
Equidae
Equus zebra hartmannae | Wild | All | Angola | b |
ARTIODACTYLA
Hippopotamidae
Hexaprotodon liberiensis (synonym Choeropsis liberiensis) | Wild | All | Côte d'Ivoire, Guinea, Guinea-Bissau, Nigeria, Sierra Leone | b |
Hippopotamus amphibius | Wild | All | Democratic Republic of the Congo, Gambia, Malawi, Niger, Nigeria, Rwanda, Sierra Leone, Togo | b |
Camelidae
Lama guanicoe | Wild | All, except: specimens that form part of the registered stock in Argentina, provided that permits are confirmed by the Secretariat before being accepted by the Member State of import,products obtained from the shearing of live animals carried out under the approved management programme, appropriately marked and registered,non-commercial exports of limited quantities of wool for industrial testing, up to 500 kg annually. | Argentina | b |
Moschidae
Moschus berezovskii | Wild | All | China | b |
Moschus chrysogaster | Wild | All | China | b |
Moschus fuscus | Wild | All | China | b |
Moschus moschiferus | Wild | All | China, Russia | b |
Cervidae
Cervus elaphus bactrianus | Wild | All | Uzbekistan | b |
Bovidae
Saiga tatarica | Wild | All | Kazakhstan, Russia | b |
AVES
CICONIIFORMES
Balaenicipitidae
Balaeniceps rex | Wild | All | United Republic of Tanzania, Zambia | b |
ANSERIFORMES
Anatidae
Anas bernieri | Wild | All | Madagascar | b |
Oxyura jamaicensis | All | Live | All | d |
FALCONIFORMES
Accipitridae
Accipiter brachyurus | Wild | All | Papua New Guinea | b |
Accipiter gundlachi | Wild | All | Cuba | b |
Accipiter imitator | Wild | All | Papua New Guinea, Solomon Islands | b |
Accipiter melanoleucus | Wild | All | Guinea | b |
Accipiter ovampensis | Wild | All | Guinea | b |
Aviceda cuculoides | Wild | All | Guinea | b |
Buteo albonotatus | Wild | All | Peru | b |
Buteo galapagoensis | Wild | All | Ecuador | b |
Buteo platypterus | Wild | All | Peru | b |
Buteo ridgwayi | Wild | All | Dominican Republic, Haiti | b |
Erythrotriorchis radiatus | Wild | All | Australia | b |
Gyps bengalensis | Wild | All | All | b |
Gyps coprotheres | Wild | All | Mozambique, Namibia, Swaziland | b |
Gyps indicus | Wild | All | All | b |
Gyps rueppellii | Wild | All | Guinea | b |
Harpyopsis novaeguineae | Wild | All | Indonesia, Papua New Guinea | b |
Hieraaetus ayresii | Wild | All | Guinea | b |
Hieraaetus spilogaster | Wild | All | Guinea | b |
Leucopternis lacernulata | Wild | All | Brazil | b |
Lophoictinia isura | Wild | All | Australia | b |
Macheiramphus alcinus | Wild | All | Guinea | b |
Polemaetus bellicosus | Wild | All | Guinea | b |
Spizaetus africanus | Wild | All | Guinea | b |
Spizaetus bartelsi | Wild | All | Indonesia | b |
Stephanoaetus coronatus | Wild | All | Guinea | b |
Terathopius ecaudatus | Wild | All | Guinea | b |
Trigonoceps occipitalis | Wild | All | Côte d’Ivoire, Guinea | b |
Urotriorchis macrourus | Wild | All | Guinea | b |
Falconidae
Falco chicquera | Wild | All | Guinea | b |
Falco deiroleucus | Wild | All | Belize, Guatemala | b |
Falco fasciinucha | Wild | All | Botswana, Ethiopia, Kenya, Malawi, Mozambique, South Africa, Sudan, United Republic of Tanzania, Zambia, Zimbabwe | b |
Falco hypoleucos | Wild | All | Australia, Papua New Guinea | b |
Micrastur plumbeus | Wild | All | Colombia, Ecuador | b |
Sagittariidae
Sagittarius serpentarius | Wild | All | Guinea, Togo | b |
GALLIFORMES
Phasianidae
Polyplectron schleiermacheri | Wild | All | Indonesia, Malaysia | b |
GRUIFORMES
Gruidae
Balearica pavonina | Wild | All | Guinea, Mali | b |
Balearica regulorum | Wild | All | Angola, Botswana, Burundi, Democratic Republic of the Congo, Kenya, Lesotho, Malawi, Mozambique, Namibia, Rwanda, South Africa, Swaziland, Uganda, Zambia, Zimbabwe | b |
Grus carunculatus | Wild | All | South Africa, United Republic of Tanzania | b |
Grus virgo | Wild | All | Sudan | b |
COLUMBIFORMES
Columbidae
Goura cristata | Wild | All | Indonesia | b |
Goura scheepmakeri | Wild | All | Indonesia | b |
Goura victoria | Wild | All | Indonesia | b |
PSITTACIFORMES
Psittacidae
Agapornis fischeri | Wild | All | United Republic of Tanzania | b |
Ranched | All | Mozambique | b |
Agapornis lilianae | Wild | All | United Republic of Tanzania | b |
Agapornis nigrigenis | Wild | All | All | b |
Agapornis pullarius | Wild | All | Angola, Democratic Republic of the Congo, Guinea, Kenya, Mali, Togo | b |
Alisterus chloropterus chloropterus | Wild | All | Indonesia | b |
Amazona agilis | Wild | All | Jamaica | b |
Amazona autumnalis | Wild | All | Ecuador | b |
Amazona collaria | Wild | All | Jamaica | b |
Amazona mercenaria | Wild | All | Venezuela | b |
Amazona xanthops | Wild | All | Bolivia, Paraguay | b |
Ara chloroptera | Wild | All | Argentina, Panama | b |
Ara severa | Wild | All | Guyana | b |
Aratinga acuticaudata | Wild | All | Uruguay | b |
Aratinga aurea | Wild | All | Argentina | b |
Aratinga auricapilla | Wild | All | All | b |
Aratinga erythrogenys | Wild | All | Peru | b |
Aratinga euops | Wild | All | Cuba | b |
Bolborhynchus ferrugineifrons | Wild | All | Colombia | b |
Cacatua sanguinea | Wild | All | Indonesia | b |
Charmosyna amabilis | Wild | All | Fiji | b |
Charmosyna diadema | Wild | All | All | b |
Cyanoliseus patagonus | Wild | All | Chile, Uruguay | b |
Deroptyus accipitrinus | Wild | All | Peru, Surinam | b |
Eclectus roratus | Wild | All | Indonesia | b |
Forpus xanthops | Wild | All | Peru | b |
Hapalopsittaca amazonina | Wild | All | All | b |
Hapalopsittaca fuertesi | Wild | All | Colombia | b |
Hapalopsittaca pyrrhops | Wild | All | All | b |
Leptosittaca branickii | Wild | All | All | b |
Lorius domicella | Wild | All | Indonesia | b |
Nannopsittaca panychlora | Wild | All | Brazil | b |
Pionus chalcopterus | Wild | All | Peru | b |
Poicephalus cryptoxanthus | Wild | All | United Republic of Tanzania | b |
Poicephalus gulielmi | Wild | All | Côte d’Ivoire, Congo | b |
Poicephalus meyeri | Wild | All | United Republic of Tanzania | b |
Poicephalus robustus | Wild | All | Botswana, Democratic Republic of the Congo, Gambia, Guinea, Mali, Namibia, Nigeria, Senegal, South Africa, Swaziland, Togo, Uganda | b |
Poicephalus rufiventris | Wild | All | United Republic of Tanzania | b |
Polytelis alexandrae | Wild | All | Australia | b |
Prioniturus luconensis | Wild | All | Philippines | b |
Psittacula alexandri | Wild | All | Indonesia | b |
Psittacula finschii | Wild | All | Bangladesh, Cambodia | b |
Psittacula roseata | Wild | All | China | b |
Psittacus erithacus | Wild | All | Benin, Burundi, Liberia, Mali, Nigeria, Togo | b |
Psittacus erithacus timneh | Wild | All | Guinea, Guinea-Bissau | b |
Psittrichas fulgidus | Wild | All | All | b |
Pyrrhura albipectus | Wild | All | Ecuador | b |
Pyrrhura calliptera | Wild | All | Colombia | b |
Pyrrhura leucotis | Wild | All | Brazil | b |
Pyrrhura orcesi | Wild | All | Ecuador | b |
Pyrrhura picta | Wild | All | Colombia | b |
Pyrrhura viridicata | Wild | All | Colombia | b |
Tanygnathus gramineus | Wild | All | Indonesia | b |
Touit melanonota | Wild | All | Brazil | b |
Touit surda | Wild | All | Brazil | b |
Trichoglossus johnstoniae | Wild | All | Philippines | b |
Triclaria malachitacea | Wild | All | Argentina, Brazil | b |
CUCULIFORMES
Musophagidae
Musophaga porphyreolopha | Wild | All | Uganda | b |
Tauraco corythaix | Wild | All | Mozambique | b |
Tauraco fischeri | Wild | All | United Republic of Tanzania | b |
Tauraco macrorhynchus | Wild | All | Guinea | b |
STRIGIFORMES
Tytonidae
Phodilus prigoginei | Wild | All | Democratic Republic of the Congo | b |
Tyto aurantia | Wild | All | Papua New Guinea | b |
Tyto inexspectata | Wild | All | Indonesia | b |
Tyto manusi | Wild | All | Papua New Guinea | b |
Tyto nigrobrunnea | Wild | All | Indonesia | b |
Tyto sororcula | Wild | All | Indonesia | b |
Strigidae
Asio capensis | Wild | All | Guinea | b |
Asio clamator | Wild | All | Peru | b |
Bubo lacteus | Wild | All | Guinea | b |
Bubo philippensis | Wild | All | Philippines | b |
Bubo poensis | Wild | All | Guinea | b |
Bubo vosseleri | Wild | All | United Republic of Tanzania | b |
Glaucidium albertinum | Wild | All | Democratic Republic of the Congo, Rwanda | b |
Glaucidium perlatum | Wild | All | Guinea | b |
Ketupa blakistoni | Wild | All | China, Japan, Russia | b |
Ketupa ketupu | Wild | All | Singapore | b |
Nesasio solomonensis | Wild | All | Papua New Guinea, Solomon Islands | b |
Ninox affinis | Wild | All | India | b |
Ninox rudolfi | Wild | All | Indonesia | b |
Otus angelinae | Wild | All | Indonesia | b |
Otus fuliginosus | Wild | All | Philippines | b |
Otus longicornis | Wild | All | Philippines | b |
Otus magicus | Wild | All | Seychelles | b |
Otus mindorensis | Wild | All | Philippines | b |
Otus mirus | Wild | All | Philippines | b |
Otus pauliani | Wild | All | Comoros | b |
Otus roboratus | Wild | All | Peru | b |
Otus rutilus | Wild | All | Comoros | b |
Pulsatrix melanota | Wild | All | Peru | b |
Scotopelia peli | Wild | All | Guinea | b |
Scotopelia ussheri | Wild | All | Côte d’Ivoire, Ghana, Guinea, Liberia, Sierra Leone | b |
Strix davidi | Wild | All | China | b |
Strix woodfordii | Wild | All | Guinea | b |
APODIFORMES
Trochilidae
Chalcostigma olivaceum | Wild | All | Peru | b |
Heliodoxa rubinoides | Wild | All | Peru | b |
CORACIIFORMES
Bucerotidae
Buceros rhinoceros | Wild | All | Thailand | b |
PASSERIFORMES
Pittidae
Pitta nympha | Wild | All | All (except Vietnam) | b |
Pycnonotidae
Pycnonotus zeylanicus | Wild | All | Malaysia | b |
REPTILIA
TESTUDINES
Emydidae
Callagur borneoensis | Wild | All | All | b |
Chrysemys picta | All | Live | All | d |
Cuora amboinensis | Wild | All | Malaysia | b |
Cuora galbinifrons | Wild | All | China | b |
Heosemys spinosa | Wild | All | Indonesia | b |
Leucocephalon yuwonoi | Wild | All | Indonesia | b |
Siebenrockiella crassicollis | Wild | All | Indonesia | b |
Trachemys scripta elegans | All | Live | All | d |
Testudinidae
Geochelone denticulata | Wild | All | Bolivia, Ecuador | b |
Geochelone elegans | Wild | All | Pakistan | b |
Geochelone gigantea | Wild | All | Seychelles | b |
Geochelone pardalis | Wild | All | Democratic Republic of the Congo, Mozambique, Uganda, United Republic of Tanzania | b |
Ranched | All | Mozambique, Zambia | b |
Source "F" [1] | All | Zambia | b |
Geochelone platynota | Wild | All | Myanmar | b |
Gopherus agassizii | Wild | All | All | b |
Gopherus berlandieri | Wild | All | All | b |
Gopherus polyphemus | Wild | All | United States of America | b |
Indotestudo elongata | Wild | All | Bangladesh, China, India | b |
Indotestudo forstenii | Wild | All | All | b |
Kinixys belliana | Wild | All | Mozambique | b |
Ranched | All | Benin | b |
Kinixys erosa | Wild | All | Togo | b |
Kinixys homeana | Wild | All | Benin | b |
Manouria emys | Wild | All | Bangladesh, India, Indonesia, Myanmar, Thailand | b |
Manouria impressa | Wild | All | Vietnam | b |
Testudo horsfieldii | Wild | All | China, Kazakhstan, Pakistan | b |
Pelomedusidae
Erymnochelys madagascariensis | Wild | All | Madagascar | b |
Podocnemis erythrocephala | Wild | All | Colombia, Venezuela | b |
Podocnemis expansa | Wild | All | Colombia, Ecuador, Guyana, Peru, Trinidad and Tobago, Venezuela | b |
Podocnemis lewyana | Wild | All | All | b |
Podocnemis sextuberculata | Wild | All | Peru | b |
Podocnemis unifilis | Wild | All | Suriname | b |
CROCODYLIA
Alligatoridae
Caiman crocodilus | Wild | All | El Salvador, Guatemala, Mexico | b |
Palaeosuchus trigonatus | Wild | All | Guyana | b |
Crocodylidae
Crocodylus niloticus | Wild | All | Madagascar | b |
SAURIA
Gekkonidae
Phelsuma abbotti | Wild | All | Madagascar | b |
Phelsuma antanosy | Wild | All | Madagascar | b |
Phelsuma barbouri | Wild | All | Madagascar | b |
Phelsuma befotakensis | Wild | All | Madagascar | b |
Phelsuma breviceps | Wild | All | Madagascar | b |
Phelsuma chekei | Wild | All | Madagascar | b |
Phelsuma comorensis | Wild | All | Comoros | b |
Phelsuma dubia | Wild | All | Comoros, Madagascar | b |
Phelsuma flavigularis | Wild | All | Madagascar | b |
Phelsuma guttata | Wild | All | Madagascar | b |
Phelsuma klemmeri | Wild | All | Madagascar | b |
Phelsuma laticauda | Wild | All | Comoros | b |
Phelsuma leiogaster | Wild | All | Madagascar | b |
Phelsuma minuthi | Wild | All | Madagascar | b |
Phelsuma modesta | Wild | All | Madagascar | b |
Phelsuma mutabilis | Wild | All | Madagascar | b |
Phelsuma pronki | Wild | All | Madagascar | b |
Phelsuma pusilla | Wild | All | Madagascar | b |
Phelsuma seippi | Wild | All | Madagascar | b |
Phelsuma serraticauda | Wild | All | Madagascar | b |
Phelsuma standingi | Wild | All | Madagascar | b |
Phelsuma v-nigra | Wild | All | Comoros | b |
Agamidae
Uromastyx aegyptia | Source "F" [1] | All | Egypt | b |
Uromastyx dispar | Wild | All | Algeria, Mali, Sudan | b |
Uromastyx geyri | Wild | All | Mali, Niger | b |
Chamaeleonidae
Calumma boettgeri | Wild | All | Madagascar | b |
Calumma brevicornis | Wild | All | Madagascar | b |
Calumma capuroni | Wild | All | Madagascar | b |
Calumma cucullata | Wild | All | Madagascar | b |
Calumma fallax | Wild | All | Madagascar | b |
Calumma furcifer | Wild | All | Madagascar | b |
Calumma gallus | Wild | All | Madagascar | b |
Calumma gastrotaenia | Wild | All | Madagascar | b |
Calumma globifer | Wild | All | Madagascar | b |
Calumma guibei | Wild | All | Madagascar | b |
Calumma hilleniusi | Wild | All | Madagascar | b |
Calumma linota | Wild | All | Madagascar | b |
Calumma malthe | Wild | All | Madagascar | b |
Calumma nasuta | Wild | All | Madagascar | b |
Calumma oshaughnessyi | Wild | All | Madagascar | b |
Calumma parsonii | Wild | All | Madagascar | b |
Calumma peyrierasi | Wild | All | Madagascar | b |
Calumma tsaratananensis | Wild | All | Madagascar | b |
Chamaeleo deremensis | Wild | All | United Republic of Tanzania | b |
Chamaeleo eisentrauti | Wild | All | Cameroon | b |
Chamaeleo ellioti | Wild | All | Burundi | b |
Chamaeleo feae | Wild | All | Equatorial Guinea | b |
Chamaeleo fuelleborni | Wild | All | United Republic of Tanzania | b |
Chamaeleo gracilis | Wild | All | Benin | b |
Ranched | All | Benin, Togo | b |
Chamaeleo pfefferi | Wild | All | Cameroon | b |
Chamaeleo werneri | Wild | All | United Republic of Tanzania | b |
Chamaeleo wiedersheimi | Wild | All | Cameroon | b |
Furcifer angeli | Wild | All | Madagascar | b |
Furcifer antimena | Wild | All | Madagascar | b |
Furcifer balteatus | Wild | All | Madagascar | b |
Furcifer belalandaensis | Wild | All | Madagascar | b |
Furcifer bifidus | Wild | All | Madagascar | b |
Furcifer campani | Wild | All | Madagascar | b |
Furcifer labordi | Wild | All | Madagascar | b |
Furcifer minor | Wild | All | Madagascar | b |
Furcifer monoceras | Wild | All | Madagascar | b |
Furcifer pardalis | Ranched | All | Madagascar | b |
Furcifer petteri | Wild | All | Madagascar | b |
Furcifer rhinoceratus | Wild | All | Madagascar | b |
Furcifer tuzetae | Wild | All | Madagascar | b |
Furcifer willsii | Wild | All | Madagascar | b |
Iguanidae
Conolophus pallidus | Wild | All | Ecuador | b |
Conolophus subcristatus | Wild | All | Ecuador | b |
Iguana iguana | Wild | All | El Salvador | b |
Cordylidae
Cordylus mossambicus | Wild | All | Mozambique | b |
Cordylus tropidosternum | Wild | All | Mozambique | b |
Cordylus vittifer | Wild | All | Mozambique | b |
Scincidae
Corucia zebrata | Wild | All | Solomon Islands | b |
Helodermatidae
Heloderma horridum | Wild | All | Guatemala, Mexico | b |
Heloderma suspectum | Wild | All | Mexico, United States of America | b |
Varanidae
Varanus bogerti | Wild | All | Papua New Guinea | b |
Varanus dumerilii | Wild | All | Indonesia | b |
Varanus exanthematicus | Wild | All | Benin, Togo | b |
Ranched | All | Benin, Togo | b |
Varanus jobiensis (synonym V. karlschmidti) | Wild | All | Indonesia | b |
Varanus niloticus | Wild | All | Burundi, Mozambique | b |
Ranched | All | Benin, Togo | b |
Varanus prasinus beccarii | Wild | All | Indonesia | b |
Varanus salvadorii | Wild | All | Indonesia | b |
Varanus salvator | Wild | All | China, India, Singapore | b |
Varanus telenesetes | Wild | All | Papua New Guinea | b |
Varanus teriae | Wild | All | Australia | b |
Varanus yemenensis | Wild | All | All | b |
SERPENTES
Pythonidae
Liasis fuscus | Wild | All | Indonesia | b |
Morelia boeleni | Wild | All | Indonesia | b |
Python molurus | Wild | All | China | b |
Python regius | Wild | All | Guinea | b |
Python reticulatus | Wild | All | India, Malaysia (Peninsular), Singapore | b |
Python sebae | Wild | All | Mauritania, Mozambique | b |
Ranched | All | Mozambique | b |
Boidae
Boa constrictor | Wild | All | El Salvador, Honduras | b |
Calabaria reinhardtii | Ranched | All | Benin, Togo | b |
Eunectes deschauenseei | Wild | All | Brazil | b |
Eunectes murinus | Wild | All | Paraguay | b |
Gongylophis colubrinus | Wild | All | United Republic of Tanzania | b |
Elapidae
Naja atra | Wild | All | Lao People’s Democratic Republic | b |
Naja kaouthia | Wild | All | Lao People’s Democratic Republic | b |
Naja siamensis | Wild | All | Lao People’s Democratic Republic | b |
AMPHIBIA
ANURA
Dendrobatidae
Dendrobates auratus | Wild | All | Nicaragua | b |
Dendrobates pumilio | Wild | All | Nicaragua | b |
Ranched | All | Nicaragua | b |
Dendrobates tinctorius | Wild | All | Surinam | b |
Mantellidae
Mantella aurantiaca | Wild | All | Madagascar | b |
Mantella baroni (synonym Phrynomantis maculatus) | Wild | All | Madagascar | b |
Mantella aff. baroni | Wild | All | Madagascar | b |
Mantella bernhardi | Wild | All | Madagascar | b |
Mantella cowani | Wild | All | Madagascar | b |
Mantella crocea | Wild | All | Madagascar | b |
Mantella expectata | Wild | All | Madagascar | b |
Mantella haraldmeieri (synonym M. madagascariensis haraldmeieri) | Wild | All | Madagascar | b |
Mantella laevigata | Wild | All | Madagascar | b |
Mantella madagascariensis | Wild | All | Madagascar | b |
Mantella manery | Wild | All | Madagascar | b |
Mantella milotympanum (synonym M. aurantiaca milotympanum) | Wild | All | Madagascar | b |
Mantella nigricans (synonym M. cowani nigricans) | Wild | All | Madagascar | b |
Mantella pulchra | Wild | All | Madagascar | b |
Mantella viridis | Wild | All | Madagascar | b |
Microhylidae
Scaphiophryne gottlebei | Wild | All | Madagascar | b |
Ranidae
Conraua goliath | Wild | All | Cameroon | b |
Rana catesbeiana | All | Live | All | d |
ARTHROPODA
ARACHNIDA
ARANEAE
Theraphosidae
Brachypelma albopilosum | Wild | All | Nicaragua | b |
INSECTA
LEPIDOPTERA
Papilionidae
Ornithoptera croesus | Wild | All | Indonesia | b |
Ornithoptera tithonus | Wild | All | Indonesia | b |
Ornithoptera urvillianus | Wild | All | Solomon Islands | b |
Ornithoptera victoriae | Wild | All | Solomon Islands | b |
Troides andromache | Wild | All | Indonesia | b |
Ranched | All | Indonesia | b |
MOLLUSCA
BIVALVIA
VENEROIDA
Tridacnidae
Hippopus hippopus | Wild | All | New Caledonia, Tonga, Vanuatu, Vietnam | b |
Tridacna crocea | Wild | All | Fiji, Tonga, Vanuatu, Vietnam | b |
Tridacna derasa | Wild | All | Fiji, New Caledonia, Philippines, Palau, Tonga, Vanuatu | b |
Tridacna gigas | Wild | All | Fiji, Indonesia, Marshall Islands, Federated States of Micronesia, Palau, Papua New Guinea, Tonga, Vanuatu, Vietnam | b |
Tridacna maxima | Wild | All | Federated States of Micronesia, Fiji, Marshall Islands, Mozambique, New Caledonia, Tonga, Vanuatu, Vietnam | b |
Tridacna squamosa | Wild | All | Fiji, Mozambique, New Caledonia, Tonga, Vanuatu, Vietnam | b |
Tridacna tevoroa | Wild | All | Tonga | b |
MESOGASTROPODA
Strombidae
Strombus gigas | Wild | All | Antigua and Barbuda, Barbados, Dominica, Haiti, Trinidad and Tobago | b |
CNIDARIA
SCLERACTINIA
Acroporidae
Montipora caliculata | Wild | All | Tonga | b |
Caryophylliidae
Catalaphyllia jardinei | Wild | All except maricultured specimens attached to artificial substrates | Indonesia | b |
Catalaphyllia jardinei | Wild | All | Solomon Islands | b |
Euphyllia cristata | Wild | All except maricultured specimens attached to artificial substrates | Indonesia | b |
Euphyllia divisa | Wild | All except maricultured specimens attached to artificial substrates | Indonesia | b |
Euphyllia fimbriata | Wild | All except maricultured specimens attached to artificial substrates | Indonesia | b |
Plerogyra spp. | Wild | All except maricultured specimens attached to artificial substrates | Indonesia | b |
Merulinidae
Hydnophora microconos | Wild | All except maricultured specimens attached to artificial substrates | Indonesia | b |
Mussidae
Blastomussa spp. | Wild | All except maricultured specimens attached to artificial substrates | Indonesia | b |
Cynarina lacrymalis | Wild | All except maricultured specimens attached to artificial substrates | Indonesia | b |
Scolymia vitiensis | Wild | All except maricultured specimens attached to artificial substrates | Indonesia | b |
Trachyphylliidae
Trachyphyllia geoffroyi | Wild | All except maricultured specimens attached to artificial substrates | Indonesia | b |
FLORA
Amaryllidaceae
Galanthus nivalis | Wild | All | Bosnia and Herzegovina, Bulgaria, Switzerland, Ukraine | b |
Apocynaceae
Pachypodium inopinatum | Wild | All | Madagascar | b |
Pachypodium rosulatum | Wild | All | Madagascar | b |
Pachypodium rutenbergianum ssp. sofiense | Wild | All | Madagascar | b |
Euphorbiaceae
Euphorbia banae | Wild | All | Madagascar | b |
Euphorbia bulbispina | Wild | All | Madagascar | b |
Euphorbia guillauminiana | Wild | All | Madagascar | b |
Euphorbia kondoi | Wild | All | Madagascar | b |
Euphorbia millotii | Wild | All | Madagascar | b |
Orchidaceae
Anacamptis pyramidalis | Wild | All | Switzerland, Turkey | b |
Barlia robertiana | Wild | All | Turkey | b |
Cephalanthera rubra | Wild | All | Norway | b |
Cypripedium japonicum | Wild | All | China, Democratic People's Republic of Korea, Japan, Republic of Korea | b |
Cypripedium macranthos | Wild | All | Republic of Korea, Russia | b |
Cypripedium margaritaceum | Wild | All | China | b |
Cypripedium micranthum | Wild | All | China | b |
Dactylorhiza latifolia | Wild | All | Norway | b |
Dactylorhiza romana | Wild | All | Turkey | b |
Dactylorhiza russowii | Wild | All | Norway | b |
Dactylorhiza traunsteineri | Wild | All | Liechtenstein | b |
Himantoglossum hircinum | Wild | All | Switzerland | b |
Nigritella nigra | Wild | All | Norway | b |
Ophrys holoserica | Wild | All | Turkey | b |
Ophrys insectifera | Wild | All | Liechtenstein, Norway, Romania | b |
Ophrys pallida | Wild | All | Algeria | b |
Ophrys sphegodes | Wild | All | Romania, Switzerland | b |
Ophrys tenthredinifera | Wild | All | Turkey | b |
Ophrys umbilicata | Wild | All | Turkey | b |
Orchis coriophora | Wild | All | Russia, Switzerland | b |
Orchis italica | Wild | All | Turkey | b |
Orchis laxiflora | Wild | All | Switzerland | b |
Orchis mascula | Wild/Ranched | All | Albania | b |
Orchis morio | Wild | All | Turkey | b |
Orchis pallens | Wild | All | Russia | b |
Orchis papilionacea | Wild | All | Romania | b |
Orchis provincialis | Wild | All | Switzerland | b |
Orchis punctulata | Wild | All | Turkey | b |
Orchis purpurea | Wild | All | Switzerland, Turkey | b |
Orchis simia | Wild | All | Bosnia and Herzegovina, Croatia, Macedonia, Romania, Switzerland, Turkey | b |
Orchis tridentata | Wild | All | Turkey | b |
Orchis ustulata | Wild | All | Russia | b |
Serapias cordigera | Wild | All | Turkey | b |
Serapias parviflora | Wild | All | Turkey | b |
Serapias vomeracea | Wild | All | Switzerland, Turkey | b |
Spiranthes spiralis | Wild | All | Liechtenstein, Switzerland | b |
Primulaceae
Cyclamen intaminatum | Wild | All | Turkey | b |
Cyclamen mirabile | Wild | All | Turkey | b |
Cyclamen pseudibericum | Wild | All | Turkey | b |
Cyclamen trochopteranthum | Wild | All | Turkey | b |
[1] Namely animals born in captivity but for which the criteria of Chapter III of Regulation (EC) No 1808/2001 are not met, as well as parts and derivatives thereof.
--------------------------------------------------
