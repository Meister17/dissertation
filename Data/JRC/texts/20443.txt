COUNCIL DECISION of 6 June 1994 on Community membership of the European Investment Fund (94/375/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty instituting the European Community, and in particular Article 235 thereof,
Having regard to the proposal by the Commission,
Having regard to the opinion of the European Parliament (1),
Having taken into account the Statutes of the European Investment Fund (2),
Whereas the European Council meeting in Edinburgh on 11 and 12 December 1992 invited the Council and the European Investment Bank to give urgent and sympathetic consideration to the establishment as quickly as possible of a European Investment Fund, hereinafter called 'the Fund';
Whereas the Fund should constitute a cost-effective and efficient means whereby the Community, together with the European Investment Bank and other financial institutions, can provide a significant contribution to the development of trans-European networks in the areas of transport, telecommunications and energy infrastructures and to the development of small and medium-sized enterprises (SMEs);
Whereas investments in trans-European networks are essential to the proper functioning of the internal market and whereas, in accordance with procedures provided for in the Statutes of the Fund, some of these investments might involve activities to be implemented in adjacent countries where crossborder projects are involved;
Whereas investment support for SMEs is an essential factor in the reinforcement of employment creation;
Whereas the Fund will foster investment in both areas, in order to contribute to the pursuit of Community objectives;
Whereas speedy implementation of the Fund will stimulate sustained and balanced growth within the Community;
Whereas Article 30 of the Statute of the European Investment Bank empowers the Bank's Board of Governors to create the Fund; whereas the Board has decided to create the Fund and to establish its Statutes;
Whereas the Community may become a member of the Fund;
Whereas appropriate co-ordination will be ensured between the operations of the Fund and Community financial and budgetary instruments;
Whereas the Commission will address the Fund's annual report to the European Parliament and the Council, together with any further information which is of particular Community interest;
Whereas, in order to meet the objectives of the Fund, it would be appropriate to consider it as a multilateral development bank for the purposes of Council Directive 89/647/EEC (3) and Commission Directive 91/31/EEC (4);
Whereas the Treaty does not provide powers for the adoption of this Decision, other than those of Article 235,
HAS DECIDED AS FOLLOWS:
Article 1
The Community, represented by the Commission, shall hereby become a member of the European Investment Fund.
Article 2
The Commission shall keep the Council informed of the work of the bodies of the European Investment Fund. In particular, the Commission shall notify the Council, as soon as it can, of matters to be discussed at the Fund's General Meeting.
The Council may, at the request of a Member State or of the Commission, examine those matters.
Without prejudice to Article 3, the Commission shall take account of the outcome of that examination when it adopts a position on those matters in the Fund's General Meeting.
Article 3
The position of the Community on a possible increase in the capital of the Fund and on its participation in that increase shall be decided unanimously by the Council, acting on a proposal from the Commission and after consulting the European Parliament.
Done at Luxembourg, 6 June 1994.
For the Council
The President
Y. PAPANTONIOU
(1) OJ No C 115, 26. 4. 1993, p. 238.
(2) See page 1 of this Official Journal.
(3) Council Directive 89/647/EEC of 18 December 1989 on a solvency ratio for credit institutions (OJ L 386, 30. 12. 1989, p. 14). Directive as amended by Commission Directive 92/30/EEC (OJ L 110, 28. 4. 1992, p. 52).
(4) Commission Directive 91/31/EEC of 19 December 1990 adapting the technical definition of 'multilateral development banks' in Council Directive 89/647/EEC on a solvency ratio for credit institutions (OJ L 17, 23. 1. 1991, p. 20).
