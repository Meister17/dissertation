COUNCIL DECISION
of 17 December 1999
on the signing, on behalf of the European Community, of the Memorandum of Understanding between the European Community and the Government of Vietnam on the prevention of fraud in trade in footwear products, and authorising its provisional application
(2000/1/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 133 thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) The Commission has negotiated, on behalf of the European Community, a Memorandum of Understanding between the European Community and the Government of Vietnam on the prevention of fraud in trade in footwear products;
(2) The Memorandum of Understanding was initialled on 4 August 1999;
(3) The Memorandum of Understanding should be signed on behalf of the Community;
(4) It is necessary to apply the Memorandum of Understanding on a provisional basis from 1 January 2000, pending the completion of the relevant procedures for its formal conclusion, subject to reciprocity,
HAS DECIDED AS FOLLOWS:
Article 1
Subject to the conclusion thereof, the Memorandum of Understanding between the European Community and the Government of Vietnam on the prevention of fraud in trade in footwear products, shall be signed on behalf of the Community.
Article 2
The President of the Council is hereby authorised to designate the persons empowered to sign the Memorandum of Understanding, on behalf of the Community.
The text of the Memorandum of Understanding is attached to this Decision.
Article 3
The Memorandum of Understanding shall be applied on a provisional basis from 1 January 2000, subject to reciprocity(1).
Done at Brussels, 17 December 1999.
For the Council
The President
K. HEMILÄ
(1) The Commission will publish in the Official Journal of the European Communities, C Series, the date from which the Memorandum of Understanding will be applied pursuant to this Article.
