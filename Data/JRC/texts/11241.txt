Council Decision
of 21 February 2006
on the conclusion of the Agreement between the European Community and the Kingdom of Denmark extending to Denmark the provisions of Council Regulation (EC) No 343/2003 establishing the criteria and mechanisms for determining the Member State responsible for examining an asylum application lodged in one of the Member States by a third-country national and Council Regulation (EC) No 2725/2000 concerning the establishment of "Eurodac" for the comparison of fingerprints for the effective application of the Dublin Convention
(2006/188/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 63(1)(a) thereof, in conjunction with the first sentence of the first subparagraph of Article 300(2) and the first subparagraph of Article 300(3) thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament [1],
Whereas:
(1) The Commission has negotiated on behalf of the Community an Agreement between the European Community and the Kingdom of Denmark extending to Denmark the provisions of Council Regulations (EC) No 343/2003 [2] and (EC) No 2725/2000 [3].
(2) The Agreement was signed, on behalf of the European Community, on 13 March 2005, subject to its possible conclusion at a later date, in accordance with Council Decision of 13 June 2005.
(3) In accordance with Article 3 of the Protocol on the position of the United Kingdom and Ireland annexed to the TEU and the TEC, the United Kingdom and Ireland will take part in adopting and applying this Decision.
(4) In accordance with Articles 1 and 2 of the Protocol on the position of Denmark annexed tothe TEU and the TEC, Denmark is not taking part in the adoption of this Decision and is not bound by it nor subject to is application.
(5) The Agreement should be approved,
HAS DECIDED AS FOLLOWS:
Article 1
The Agreement between the European Community and the Kingdom of Denmark extending to Denmark the provisions of Council Regulation (EC) No 343/2003 establishing the criteria and mechanisms for determining the Member State responsible for examining an asylum application lodged in one of the Member States by a third-country national and Council Regulation (EC) No 2725/2000 concerning the establishment of "Eurodac" for the comparison of fingerprints for the effective application of the Dublin Convention is hereby approved on behalf of the Community.
Article 2
The President of the Council shall make the notification provided for in Article 10(2) of the Agreement [4].
Article 3
This Decision shall be published in the Official Journal of the European Union.
Done at Brussels, 21 February 2006.
For the Council
The President
K. Gastinger
[1] Opinion delivered on 13 December 2005 (not yet published in the Official Journal).
[2] OJ L 50, 25.2.2003, p. 1.
[3] OJ L 316, 15.12.2000, p. 1.
[4] The date of entry into force of the Agreement shall be the first day of the second month following notification by the contracting parties.
--------------------------------------------------
