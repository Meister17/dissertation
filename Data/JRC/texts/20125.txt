COMMISSION REGULATION (EC) No 1839/95 of 26 July 1995 laying down detailed rules for the application of tariff quotas for imports of maize and sorghum into Spain and imports of maize into Portugal
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 1766/92 of 30 June 1992 on the common organization of the market in cereals (1), as last amended by Commission Regulation (EC) No 1664/95 (2), and in particular Article 12 (1) thereof,
Whereas, under the agreements concluded during the Uruguay Round of multilateral trade negotiations, the Community has undertaken, as from the 1995/96 marketing year, to open reduced-tariff quotas for imports into Portugal of 500 000 tonnes of maize and into Spain of two million tonnes of maize and 300 000 tonnes of sorghum respectively; whereas, in the case of the quota for imports into Spain, the quantities of certain grain substitutes imported into Spain are to be deducted in proportion to the total quantities imported; whereas in the case of the quota opened for imports of maize into Portugal, the import duty actually paid may not exceed ECU 50 per tonne;
Whereas, in order to ensure that these quotas are applied, provision should be made for direct purchase on the world market or application of an import duty reduction system established pursuant to Commission Regulation (EC) No 1502/95 (3);
Whereas the combination of the advantages provided for under the arrangements established by Council Regulation (EEC) No 715/91 (4), as last amended by Regulation (EC) No 2484/94 (5), applicable to imports into the Community of sorghum and maize originating in the African, Caribbean and Pacific (ACP) States or in the overseas countries and territories (OCT) and under this Regulation is liable to create disturbances on the Spanish market in cereals; whereas that difficulty can be overcome by setting a special reduction of the levy on maize and sorghum imported under this Regulation;
Whereas, in the case of direct purchase on the world market, and with a view to enabling the operation to be carried out under optimum conditions and in particular at the lowest purchase and transport costs, an invitation to tender should be organized for supply and delivery to warehouses designated by the intervention agency concerned; whereas provision should be made for tenders to be lodged for individual lots in line with storage capacities available in certain areas of the Member State concerned and published in the notice of invitation to tender;
Whereas detailed rules should be adopted on the organization of the invitations to tender for the import duty reduction and for direct purchase on the world market, and conditions should be defined for submitting tenders and lodging and releasing securities guaranteeing compliance with the successful tenderer's obligations;
Whereas, with a view to sound economic and financial management of the purchasing operations in question and in particular to avoid disproportionate and excessive risks for operators in view of foreseeable prices on the Spanish and Portuguese markets, provision should be made for importing onto the market, subject to a reduced duty, cereals which do not meet the quality requirements laid down in the invitation to tender; whereas in that case, however, the duty reduction may not be greater than the last amount fixed for the reduction in question;
Whereas provision should be made to cover the operations arising from this Regulation according to the mechanisms laid down by Council Regulation (EEC) No 729/70 of 21 April 1970 on the financing of the common agricultural policy (6), as last amended by Regulation (EC) No 1287/95 (7), and by Council Regulation (EEC) No 1883/78 of 2 August 1978 laying down general rules for the financing of interventions by the European Agricultural Guidance and Guarantee Fund, Guarantee Section (8), as last amended by Regulation (EEC) No 1571/93 (9);
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Cereals,
HAS ADOPTED THIS REGULATION:
Article 1
1. Quotas for imports from third countries, for free circulation in Spain, of a maximum quantity in each marketing year of two million tonnes of maize and 300 000 tonnes of sorghum shall be opened annually. Imports under these quotas shall be effected as provided for in the following Articles.
2. A quota for imports from third countries, for free circulation in Portugal, of a maximum quantity in each marketing year of 500 000 tonnes of maize shall be opened annually. Imports under this quota shall be effected as provided for in the following Articles.
3. In the event of technical difficulties duly noted by the Commission a period of importation exceeding that time limit may be laid down in accordance with the procedure laid down in Article 23 of Regulation (EEC) No 1766/92.
Article 2
1. The quantities for importation into Spain referred to in Article 1 (1) shall be reduced in each marketing year in proportion to any quantities of residues of starch manufacture from maize falling within CN code 2303 10 19, brewing and distilling dregs and waste falling within CN code 2303 30 00 and citrus pulp residues falling within CN code 2308 90 30 imported into Spain from third countries during the year concerned. Where it is ascertained that the quantities for such products imported into Spain under the cover of documents establishing their Community status develop abnormally, the necessary action shall be taken in accordance with the procedure laid down in Article 23 of Regulation (EEC) No 1766/92.
2. At a frequency to be determined, the Commission shall record in the accounts:
- the quantities of maize and sorghum imported into Spain from third countries,
- the quantities of residues of starch manufacture from maize, brewing and distilling dregs and waste and residues of citrus pulp imported into Spain.
For this purpose, the Spanish authorities shall supply the Commission regularly with all necessary information.
Article 3
1. The quantities of maize and sorghum referred to in Article 1 (1) shall be allocated to processing or use in Spain.
2. The quantities of maize referred to in Article 1 (2) shall be allocated to processing or use in Portugal.
Article 4
Imports shall be effected, as part of these quotas and within the quantitative limits set out in Article 1, to Spain and Portugal by applying an import duty reduction system or by direct purchase on the world market.
CHAPTER I
Importation with import duty reduction
Article 5
1. Without prejudice to Article 14, for imports of maize and sorghum into Spain and imports of maize into Portugal, within the quantitative limits set in Article 1, a reduction shall be applied to the import duty fixed in accordance with Regulation (EC) No 1502/95.
2. The amount of the reduction shall be fixed on a flat-rate basis or by tendering procedure, at a level enabling, firstly, disturbance of the Spanish and Portuguese markets as a result of imports into those countries to be avoided and, secondly, the quantities referred to in Article 1 actually to be imported.
3. The amount of the flat-rate reduction and, if the reduction is fixed in accordance with the tendering procedure referred to in Article 7 (1), the amount of the latter reduction, shall be fixed in accordance with the procedure laid down in Article 23 of Regulation (EEC) No 1766/92.
In the case of imports into Portugal, the amount of the reduction referred to in paragraph 2 shall be fixed in such a way that the duty actually paid does not exceed ECU 50 per tonne.
The reduction may be differentiated for imports of maize and/or sorghum under Regulation (EEC) No 715/90.
The duty actually paid shall be converted into national currency by applying the conversion rate applicable for the currency concerned on the date of completion of the customs import formalities.
4. The import duty reduction provided for in paragraph 1 shall be applied on importation into Spain of maize falling within CN code 1005 90 00 and sorghum falling within CN code 1007 00 90 and on importation into Portugal of maize falling within CN code 1005 90 00, covered by licences issued by the Spanish and Portuguese authorities as provided for in this Regulation and with the consent of the Commission. These licences shall be valid only in the Member State in which they are issued.
Article 6
1. Where a tendering procedure is organized for the import duty reduction, interested parties shall reply to the invitation to tender either by lodging a written tender in exchange for an acknowledgement of receipt with the Spanish intervention agency or the General Directorate for Trade in Portugal or by forwarding the same to the latter by registered letter, telex, telefax or telegram.
2. Tenders must give:
- the reference of the invitation to tender,
- the tenderer's name and exact addresses, together with the telex or telefax number,
- the nature and quantity of the product to be imported,
- the amount per tonne of the import duty reduction proposed in ecus,
- the country of origin of the cereals to be imported.
3. Tenders must be accompanied:
(a) by evidence that the tenderer has lodged a security of ECU 20 per tonne; and (b) by a written undertaking by the tenderer that, within two days of receipt of notification of the award of contract, he will lodge with the competent body concerned an application for an import licence for the quantity awarded, and that he will import from the country of origin specified in the tender.
4. Tenders must specify only one coutnry of origin; they may not exceed the maximum quantity available for each tendering deadline.
5. Tenders not submitted in accordance with paragraphs 1, 2, 3 and 4 or containing conditions other than those laid down in the invitation to tender shall not be considered.
6. Tenders may not be withdrawn.
7. Tenders must be forwarded to the Commission by the competent body not later than two hours after the deadline for the lodging of tenders as specified in the invitation to tender. They msut be forwarded in the form shown in Annex 1.
Where no tenders are submitted, the Member State concerned shall inform the Commission within the same time limit.
Article 7
1. On the basis of the tenders lodged and forwarded under a tendering procedure for the import duty reduction the Commission shall decide, in accordance with the procedure laid down in Article 23 of Regulation (EEC) No 1766/92:
- to fix a maximum import duty reduction, or - not to pursue the tendering procedure.
Where it is decided to fix a maximum import duty reduction, any tender(s) for an amount equal to or less than the maximum reduction shall be accepted. However, where the maximum reduction fixed under the tendering procedure for a given week leads to the acceptance of quantities exceeding the quantities remaining for importation, the tenderer having lodged the tender correponding to the accepted maximum reduction shall be awarded a quantity equal to the difference between the quantities applied for in the other accepted tenders and the quantity available. Where the maximum reduction fixed corresponds to several tenders, the quantity to be awarded shall be shared between the tenderers in proportion to the respective quantities for which they have tendered.
2. The competent authorities of Spain or Portugal shall notify all tenderers in writing of the outcome of their tenders as soon as the Commission has taken the decision referred to in paragraph 1.
Article 8
1. Licence applications shall be submitted on forms printed and/or drawn up in accordance with Article 16 of Commission Regulation (EEC) No 3719/88 (1). Where a flat-rate reduction is adopted by the Commission, applications shall be lodged on the first two working days of each week. Where the duty reduction is awarded under a tendering procedure, applications shall be lodged, for the awarded quantity, within two days of receipt of the notice of award showing the reduction proposed in the tender.
2. Licence applications and the licences themselves shall carry one of the following entries in section 24:
- Reducción del derecho: certificado válido únicamente en España [Reglamento (CE) n° 1839/95] - Reducción del derecho: certificado válido únicamente en Portugal [Reglamento (CE) n° 1839/95] - Nedsaettelse af tolden: licensen er kun gyldig i Spanien (Forordning (EF) nr. 1839/95) - Nedsaettelse af tolden: licensen er kun gyldig i Portugal (Forordning (EF) nr. 1839/95) - Ermaessigte Abgabe: Lizenz nur in Spanien gueltig (Verordnung (EG) Nr. 1839/95) - Ermaessigte Abgabe: Lizenz nur in Portugal gueltig (Verordnung (EG) Nr. 1839/95) - Ìaassùóç ôïõ aeáóìïý: ðéóôïðïéçôéêue ðïõ éó÷ýaaé ìueíï óôçí Éóðáíssá [êáíïíéóìueò (AAÊ) áñéè. 1839/95] - Ìaassùóç ôïõ aeáóìïý: ðéóôïðïéçôéêue ðïõ éó÷ýaaé ìueíï óôçí Ðïñôïãáëssá [êáíïíéóìueò (AAÊ) áñéè. 1839/95] - Duty reduction: licence valid only in Spain [Regulation (EC) No 1839/95] - Duty reduction: licence valid only in Portugal [Regulation (EC) No 1839/95] - Abattement du droit: certificat valable uniquement en Espagne [règlement (CE) n° 1839/95] - Abattement du droit: certificat valable uniquement au Portugal [règlement (CE) n° 1839/95] - Riduzione del dazio: titolo valido unicamente in Spagna [regolamento (CE) n. 1839/95] - Riduzione del dazio: titolo valido unicamente in Portogallo [regolamento (CE) n. 1839/95] - Korting op het invoerrecht: certificaat uitsluitend geldig in Spanje (Verordening (EG) nr. 1839/95) - Korting op het invoerrecht: certificaat uitsluitend geldig in Portugal (Verordening (EG) nr. 1839/95) - Redução do direito: certificado válido apenas em Espanha [Regulamento (CE) nº 1839/95] - Redução do direito: certificado válido apenas em Portugal [Regulamento (CE) nº 1839/95] - Tullinalennus : todistus voimassa ainoastaan Espanjassa [Asetus (EY) N :o 1839/95] - Tullinalennus : todistus voimassa ainoastaan Portugalissa [Asetus (EY) N :o 1839/95] - Nedsaettning av tull : intyg endast gaellande i Spanien (Foerordning (EG) nr 1839/95) - Nedsaettning av tull : intyg endast gaellande i Portugal (Foerordning (EG) nr 1839/95).
3. Where a flat-rate reduction is applied, licence applications shall be taken into consideration only where evidence is provided that a security of ECU 20 per tonne has been lodged in favour of the competent authority concerned.
Article 9
1. Licence applications shall be acompanied by a written undertaking from the applicant to lodge, by the date of issue of the licence at the latest, a performance guarantee of an amount per tonne equal to the flat-rate duty reduction granted or to that of the reduction proposed in the tender.
2. The level of security provided for in Article 10 (a) of Commission Regulation (EC) No 1162/95 (1) shall apply to import licences issued under this Regulation.
3. Where a flat-rate reduction is adopted by the Commission, the rate of reduction and import duty rate applied shall be those in force on the day on which the certificate of release for free circulation is accepted by the customs office.
4. Where the reduction is fixed under a tendering procedure, the rate of duty applied shall be that in force on the day on which the certificate of release for free circulation is accepted by the customs office. In addition, the amount of the reduction granted shall be shown in section 24 of the licence.
5. Applications shall be valid only if:
- they do not exceed the maximum quantity available for each deadline for lodging applications, and - they are accompanied by evidence that the applicant's business activity includes international cereals trading in the importing Member State. For the purpose of this Article the provision of such evidence shall consist in presentation to the competent authority of either a copy of a certificate of payment of value added tax in the Member State concerned or a copy of either a customs clearance certificate issued by the Member State concerned in respect of an import or export licence or an invoice relating to intra-Community trade in the applicant's name for an operation conducted in any of the three preceding years.
Article 10
1. Where a flat-rate reduction is adopted by the Commission, licences shall be issued, within the quantities available, no later than the Friday following the last day for submission as specified in Article 8 (1). If the Friday is not a working day, they shall be issued on the first working day thereafter.
Should the applications made in respect of a week be for quantities exceeding those for maize and sorghum still available for import into Spain, the quantities for which licences are issued shall be the quantities indicated in the applications, reduced by a uniform percentage.
2. Where a duty reduction is fixed under a tendering procedure, licences shall be issued, on condition that the tenderer has lodged an application for an import licence as referred to in Article 6 (3) (b) before the specified deadline, for the quantities awarded not later than the third working day following the final date for submitting licence applications as set out in Article 8 (1).
3. The competent authorities shall notify the Commission of the quantities for which licences have been issued each week no later than the third working day of the following week.
4. Notwithstanding Article 21 (1) of Regulation (EEC) No 3719/88 import licences shall, for the purpose of determining their period of validity, be deemed to have been issued on the day of expiry of the deadline for lodging tenders or applications.
Article 11
1. The period of validity of licences shall be:
- the period specified in Article 6 of Regulation (EC) No 1162/95 in cases where a flat-rate reduction has been adopted by the Commission,
- the period specified in the Regulation opening the invitation to tender, in the case of licences issued under a tendering procedure for the duty reduction.
2. In section 8 of the import licence, a cross must be marked against the word 'yes`. Notwithstanding Article 8 (4) of Regulation (EEC) No 3719/88, the quantity released for free circulation shall not exceed a maximum of 5 % of the quantity specified in sections 17 and 18 of the import licence. The figure '0` must be entered in section 19 of the licence.
3. Notwithstanding Article 9 of Regulation (EEC) No 3719/88, the rights arising from import licences under this Regulation shall not be transferable.
Article 12
1. Without prejudice to the surveillance measures adopted pursuant to Article 13, the security referred to in Article 6 (3) (a) shall be released:
(a) forthwith, where the tender is not accepted;
(b) where the tender submitted for the tendering procedure is accepted, on the issue of the import licence. However, where the undertaking referred to in Article 6 (3) (b) is not fulfilled, the security shall be forfeit.
2. Without prejudice to the surveillance measures adopted pursuant to Article 13, the security referred to in Article 8 (3) shall be released:
(a) forthwith, in respect of quantities for which no licence has been issued; and (b) on the issue of the import licence, in respect of quantities for which a licence has been issued.
3. Without prejudice to the surveillance measures adopted pursuant to Article 13, the security referred to in Article 9 (1) shall be released where the tenderer provides evidence:
- that the imported product has been processed or used in the importing Member States; such evidence may consist in a sales invoice to a processor or consumer with headquarters in the importing Member State, or - that the product could not be imported, processed or utilized for reasons of force majeure, or - that the product has become unsuitable for any use whatsoever.
For quantities in respect of which the abovementioned evidence is not produced within 18 months of the date of acceptance of the declaration of release for free circulation the security shall be forfeit as duty.
For the purposes of this Article, the processing or utilization of the imported product shall be considered to have been effected if 95 % of the quantity released for free circulation has been processed or used.
4. Securities shall be subject to the provisions of Article 33 of Regulation (EEC) No 3719/88, except for the provision on the six-month time limit referred to in paragraph 3 (a) of the said Article.
Article 13
1. Maize and sorghum released for free circulation with a reduced duty shall remain under the customs surveillance or under administrative control of equivalent effect until such time as it is used or processed.
2. The Member State concerned shall, if need be, take all necessary measures to ensure that the surveillance referred to in paragraph 1 is carried out. These measures shall include requiring importers to submit to any check considered necessary by the competent authorities and to keep specific records enabling the authorities to make such checks.
3. The Member State concerned shall immediately notify the Commission of the measures adopted pursuant to paragraph 2.
CHAPTER II
Direct purchase on the world market
Article 14
With a view to effecting the imports referred to in Article 1, it may be decided, under the procedure laid down in Article 23 of Regulation (EEC) No 1766/92, that the Spanish or Portuguese intervention agency shall purchase on the world market quantities of maize and/or sorghum to be determined, and shall place in the Member State concerned under customs warehousing procedure as provided for in Articles 98 to 113 of Council Regulation (EEC) No 2913/92 (1) establishing the Community Customs Code and Commission Regulation (EEC) No 2454/93 (2) laying down provisions for the implementation of Regulation (EEC) No 2913/92.
2. Quantities purchased pursuant to paragraph 1 shall be put up for sale on the domestic market of the Member State concerned, in accordance with the procedure laid down in Article 23 of Regulation (EEC) No 1766/92, on terms enabling market disturbance to be avoided and in accordance with Article 13.
When the goods are put up for sale on the domestic market, the buyer shall lodge with the intervention agency of the Member State concerned on payment of the goods a security of ECU 15 per tonne. The security shall be released when the evidence referred to in Article 12 (3) is produced. For the purposes of the release of the security, the provisions of the second and third subparagraphs of Article 12 (3) and those of Article 12 (4) shall apply.
3. When the goods are placed in free circulation, an import duty shall be charged, equal to the average of the duties fixed pursuant to Regulation (EC) No 1502/95 for the cereals concerned during the month preceding the date of acceptance of the declaration of release for free circulation, minus an amount equal to 55 % of the intervention price for the same month.
Entry into free circulation shall be effected by the intervention agency of the Member State concerned.
When the purchasers of the goods make payment to the intervention agency, the selling prices, minus the duty referred to in the first subparagraph, shall correspond to revenue from sales within the meaning of Annex I to Council Regulation (EEC) No 3492/90 (1).
4. The purchasing operation provided for in paragraph 1 shall rank as intervention for the purpose of stabilizing the agricultural markets within the meaning of Article 1 (2) (b) of Regulation (EEC) No 729/70.
5. Payments by the intervention agency for purchases as provided for in paragraph 1 shall be borne by the Community as they arise and shall be treated in the same way as the expenditure referred to in Article 2 of Regulation (EEC) No 1883/78. The intervention agency of the Member State concerned shall record the value of the goods purchased at a price of 'zero` in the account referred to in Article 4 of Regulation (EEC) No 1883/78.
Article 15
1. The Spanish or Portuguese intervention agency shall arrange for the product to be bought on the world market by the award of a supply contract under a tendering procedure. The supply shall consist in the purchase of the product on the world market and the delivery, not unloaded, to warehouses designated by the abovementioned intervention agency for placing under the customs warehousing procedure provided for in Articles 98 to 113 of Regulation (EEC) No 2913/92.
The decision to purchase on the world market referred to in Article 14 (1) shall specify in particular the quantity and quality of cereals to be imported, the dates of opening and closing of the tendering procedure and the final date for delivery of the goods.
2. A notice of invitation to tender drawn up in accordance with Annex II shall in addition be published in the 'C` series of the Official Journal of the European Communities. The invitation shall relate to one or more lots. 'Lot` shall be understood as meaning the quantities to be delivered as specified in the invitation.
3. The intervention agency of the Member State concerned shall adopt, as required, additional measures for implementing the purchasing operations on the world market in question.
The agency shall notify the Commission immediately of such measures and shall inform operators thereof.
Article 16
1. Interested parties shall reply to the invitation to tender either by lodging a written tender in exchange for an acknowledgment of receipt with the intervention agency indicated in the notice of invitation to tender, or by forwarding the same to the latter by registered letter, telex, telefax or telegram.
Tenders must reach the intervention agency before 12 noon (Brussels time) on the day on which the deadline for the submission of tenders indicated in the notice of invitation to tender expires.
2. Tenders may only be submitted in respect of whole lots. They shall give:
- the reference of the invitation to tender,
- the tenderer's name and exact address, together with the telex or telefax number,
- details of the lot concerned,
- the tender price proposed, per tonne of product, in the currency of the Member State concerned,
- the country of origin of the cereals to be imported,
- separately, the cif price, per tonne of product, in the currency of the Member State concerned, corresponding to the tender.
3. Tenders must be accompanied by evidence that the security referred to in Article 17 (1) has been lodged before expiry of the deadline for the submission of tenders.
4. Tenders not submitted in accordance with the provisions of this Article or containing conditions other than those laid down in the invitation to tender shall not be considered.
5. Tenders may not be withdrawn.
Article 17
1. Tenders submitted shall be considered only where there is evidence that a security of ECU 20 per tonne has been lodged.
2. Securities shall be lodged in accordance with the criteria laid down in the invitation to tender referred to in Article 15 (2) by the Member State concerned, in accordance with Commission Regulation (EEC) No 2220/85 (1).
3. Securities shall be released immediately in the following cases:
(a) where the tender is not accepted;
(b) where the tenderer provides evidence that the supply contract has been performed in accordance with the conditions laid down in Article 15 for the accepted tender;
(c) where the tenderer provides evidence that the goods could not be imported for reasons of force majeure.
Article 18
Tenders shall be opened and read in public. This shall be done by the intervention agency immediately after the expiry of the deadline for the submission of tenders.
Article 19
1. Without prejudice to the application of paragraphs 2 and 3, the decision to award the contract to the tenderer submitting the most favourable tender shall be notified in writing to all the tenderers not later than the second working day following the day on which the tenders are opened and read.
2. Where the tender judged most favourable is submitted simultaneously by more than one tenderer, the intervention agency shall draw lots to decide which tenderer is to be selected.
3. If the tenders submitted seem not to reflect the conditions normally applying on the markets, the intervention agency may decide to make no award. Invitations to tender shall be renewed within one week until all the lots are awarded.
Article 20
1. At the time of supply the intervention agency shall check the quantity and quality of the goods.
Subject to the application of the price reductions provided for in the notice of invitation to tender, the goods shall be rejected if the quality is below the minimum quality laid down. However the goods may be imported with a reduced duty, obtained by applying a flat-rate reduction in accordance with Chapter I.
2. Where, pursuant to paragraph 1, delivery does not take place, the security referred to in Article 17 shall be forfeit without prejudice to any other financial consequences for breach of the supply contract.
CHAPTER III
Final provisions
Article 21
Commission Regulation (EC) No 675/94 (2) is hereby repealed. However, Commission Regulation (EC) No 517/95 (3) shall continue to apply for the second instalment for the sale on the Portuguese market of 250 000 tonnes of maize purchased in accordance with the Commission Decision of February 1995.
Article 22
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
It shall apply from 1 July 1995.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 26 July 1995.
For the Commission Franz FISCHLER Member of the Commission
(1) OJ No L 302, 19. 10. 1992, p. 1.
(2) OJ No L 253, 11. 10. 1993, p. 1.
(1) OJ No L 337, 4. 12. 1990, p. 3.
(1) OJ No L 205, 3. 8. 1985, p. 5.
(2) OJ No L 83, 26. 3. 1994, p. 26.
(3) OJ No L 53, 9. 3. 1995, p. 12.
ANNEX I
>START OF GRAPHIC>
Weekly tendering procedure for the reduction in the duty on imports of .................... from third countries (Regulation (EC) No 1839/95) Deadline for submission of tenders (date/time) .................... 1 2 3 4 5 6 Reference number of tenderer Quantity (in tonnes) Aggregate quantity (in tonnes) Amount of the import duty reduction Green rate fixed in advance? (yes/no) Origin of the cereal 1 2 3 4 5 etc.
>END OF GRAPHIC>
ANNEX II
>START OF GRAPHIC>
MODEL FOR NOTICE OF INVITATION TO TENDER Invitation to tender for the purchase of .................... tonnes of .................... on the world market by the .................... intervention agency (Article 15 (2) of Commission Regulation (EC) No 1839/95) 1. Product to be mobilized:
2. Total quantity:
3. List of warehouses for a lot:
4. Characteristics of goods (quality required, minimum quality, price reductions):
5. Packaging (bulk):
6. Delivery period:
7. Deadline for submission of tenders:
>END OF GRAPHIC>
