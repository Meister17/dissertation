Council Directive 2006/67/EC
of 24 July 2006
imposing an obligation on Member States to maintain minimum stocks of crude oil and/or petroleum products
(Codified version)
(Text with EEA relevance)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 100 thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament [1],
Having regard to the opinion of the European Economic and Social Committee [2],
Whereas:
(1) Council Directive 68/414/EEC of 20 December 1968 imposing an obligation on Member States of the EEC to maintain minimum stocks of crude oil and/or petroleum products [3] has been substantially amended several times [4]. In the interests of clarity and rationality the said Directive should be codified.
(2) Imported crude oil and petroleum products are of importance in providing the Community with supplies of energy. Any difficulty, even temporary, having the effect of reducing supplies of such products imported from third States, or significantly increasing the price thereof on international markets, could cause serious disturbances in the economic activity of the Community. The Community must therefore be in a position to offset or at least to diminish any harmful effects in such a case.
(3) A crisis in obtaining supplies could occur unexpectedly and it is therefore essential to establish forthwith the necessary means to make good a possible shortage.
(4) To this end, it is necessary to increase the security of supply for crude oil and petroleum products in Member States by establishing and maintaining minimum stocks of the most important petroleum products.
(5) It is necessary that the organisational arrangements for oil stocks do not prejudice the smooth running of the internal market.
(6) In Directive 73/238/EEC [5] the Council decided upon appropriate measures — including drawing on oil stocks — to be taken in the event of difficulties in the supply of crude oil and petroleum products to the Community; Member States have undertaken similar obligations in the Agreement on an "International Energy Programme".
(7) It is necessary that stocks be at the disposal of Member States should difficulties in oil supply arise. Member States should possess the powers and the capacity to control the use of stocks so that they can be made available promptly for the benefit of the areas which most need oil supplies.
(8) Organisational arrangements for the maintenance of stocks should ensure the stocks' availability and their accessibility to the consumer.
(9) It is appropriate that organisational arrangements for the maintenance of stocks are transparent, ensuring a fair and non-discriminatory sharing of the burden of the stock-holding obligation. Therefore, information relating to the cost of holding oil stocks may be made available by Member States to interested parties.
(10) In order to organise the maintenance of stocks, Member States may have recourse to a system based on a stockholding body or entity which will hold all, or part, of the stocks making up their stock-holding obligation. The balance, if any, should be maintained by refiners and other market operators. Partnership between the Government and the industry is essential to operate efficient and reliable stock-holding mechanisms.
(11) Indigenous production contributes in itself to security of supply. The oil market evolution can justify an appropriate derogation from the obligation to maintain oil stocks for Member States with indigenous oil production. In accordance with the principle of subsidiarity, Member States may exempt undertakings from the obligation to maintain stocks in respect of an amount not exceeding the quantity of products which those undertakings manufacture from indigenously produced crude oil.
(12) It is appropriate to adopt approaches which are already followed by the Community and the Member States within their international obligations and agreements. Owing to changes in the pattern of oil consumption, international aviation bunkers have become an important component of this consumption.
(13) There is a need to adapt and simplify the Community statistical reporting mechanism concerning oil stocks.
(14) Oil stocks can, in principle, be held anywhere in the Community and, therefore, it is appropriate to facilitate the establishment of stocks outside national territory. It is necessary that decisions for holding stocks outside national territory be taken by the Government of the Member State concerned according to its needs and supply security considerations. In the case of stocks held at the disposal of another undertaking, or body/entity, more detailed rules are needed to guarantee their availability and accessibility in the event of oil supply difficulties.
(15) It is desirable, in order to ensure the smooth running of the internal market, to promote the use of agreements between Member States concerning minimum stockholding in order to further the use of storage facilities in other Member States. It is for the Member States concerned to take the decision to conclude such agreements.
(16) It is appropriate to reinforce the administrative supervision of stocks and establish efficient mechanisms for the control and verification of stocks. A regime of sanctions is necessary to impose such a control.
(17) The Council should be regularly informed on the state of the security stocks in the Community.
(18) Since the objective of the action to be taken, namely the maintenance of a high level of security in the supply of crude oil within the Community, by means of reliable and transparent arrangements based on solidarity between Member States, while complying with the rules of the internal market and the competition rules, may be better achieved at Community level, the Community may adopt measures, in accordance with the principle of subsidiarity as set out in Article 5 of the Treaty. In accordance with the principle of proportionality, as set out in that Article, this Directive does not go beyond what is necessary in order to achieve that objective.
(19) This Directive should be without prejudice to the obligations of the Member States relating to the time-limits for transposition into national law of the Directives set out in Annex I, Part B,
HAS ADOPTED THIS DIRECTIVE:
Article 1
1. Member States shall adopt such laws, regulations or administrative provisions as may be appropriate in order to maintain within the Community at all times, subject to the provisions of Article 10, their stocks of petroleum products at a level corresponding, for each of the categories of petroleum products listed in Article 2, to at least 90 days average daily internal consumption in the preceding calendar year referred to in Article 4(2).
2. That part of internal consumption met by derivatives of petroleum produced indigenously by the Member State concerned may be deducted up to a maximum of 25 % of the said consumption. The domestic distribution of the result of such a deduction shall be decided by the Member State concerned.
Article 2
The following categories of product shall be taken into account in calculating internal consumption:
(a) motor spirit and aviation fuel (aviation spirit and jet-fuel of the gasoline type);
(b) gas oil, diesel oil, kerosene and jet-fuel of the kerosene type;
(c) fuel oils.
Bunker supplies for sea-going vessels shall not be included in the calculation of internal consumption.
Article 3
1. Stocks maintained in accordance with Article 1 shall be fully at the disposal of Member States should difficulties arise in obtaining oil supplies. Member States shall ensure that they have the legal powers to control the use of stocks in such circumstances.
At all other times, Member States shall ensure the availability and accessibility of these stocks. They shall establish arrangements allowing for the identification, accounting and control of the stocks.
2. Member States shall ensure that fair and non-discriminatory conditions apply in their stock-holding arrangements.
The cost burden resulting from the maintenance of stocks in accordance with Article 1 shall be identified by transparent arrangements. In this context, Member States may adopt measures to obtain appropriate information regarding the cost burden of stock-holding in accordance with Article 1 and to make such information available to interested parties.
3. To fulfil the requirements of paragraphs 1 and 2, Member States may decide to have recourse to a stock-holding body or entity which will be responsible for holding all or part of the stocks.
Two or more Member States may decide to have recourse to a joint stock-holding body or entity. In that case they shall be jointly responsible for the obligations deriving from this Directive.
Article 4
1. Member States shall submit to the Commission a statistical summary showing stocks existing at the end of each month, drawn up in accordance with Article 5(2) and (3) and Article 6 and specifying the number of days of average consumption in the preceding calendar year which those stocks represent. This summary shall be submitted at the latest by the 25th day of the second month after the month to be reported.
2. A Member State's stock-holding obligation shall be based on the previous calendar year's internal consumption. At the beginning of each calendar year, Member States shall re-calculate their stock-holding obligation at the latest by 31 March in each year and ensure that they comply with their new obligations as soon as possible and, in any event, at the latest by 31 July in each year.
3. In the statistical summary, stocks of jet-fuel of the kerosene type shall be reported separately under the category referred to in point (b) of Article 2.
Article 5
1. Stocks required to be maintained by Article 1 may be maintained in the form of crude oil and intermediate products, as well as in the form of finished products.
2. In the statistical summary of stocks existing at the end of each month:
(a) finished products shall be accounted for according to their actual tonnage;
(b) crude oil and intermediate products shall be accounted for:
(i) in the proportions of the quantities for each category of product obtained during the preceding calendar year from the refineries of the Member State concerned; or
(ii) on the basis of the production programmes of the refineries of the Member State concerned for the current year; or
(iii) on the basis of the ratio between the total quantity manufactured during the preceding calendar year in the Member State concerned of products covered by the obligation to maintain stocks and the total amount of crude oil used during that year, up to a maximum of 40 % of the total obligation for the first and second categories (petrol and gas oils), and up to a maximum of 50 % for the third category (fuel oils).
3. Blending components, when intended for processing into the finished products listed in Article 2, may be substituted for the products for which they are intended.
Article 6
1. When the level of minimum stocks provided for in Article 1 is calculated, only those quantities which would be held in accordance with Article 3(1) shall be included in the statistical summary.
2. Subject to the provisions of paragraph 1, the following may be included in the stocks:
(a) supplies on board oil tankers in port for the purpose of discharging, once the port formalities have been completed;
(b) supplies held in ports of discharge;
(c) supplies held in tanks at the entry to oil pipelines;
(d) supplies held in refinery tanks, excluding those supplies in pipes and refining plant;
(e) supplies held in storage by refineries and by importing, storage or wholesale distribution firms;
(f) supplies held in storage by large-scale consumers in compliance with the provisions of national law concerning the obligation to maintain permanent stocks;
(g) supplies held in barges and coasting-vessels engaging in transport within national frontiers, on condition that it is possible for the competent authorities to keep a check on such supplies and provided that the supplies can be made available immediately.
3. The following shall, in particular, be excluded from the statistical summary: indigenous crude oil not yet extracted; supplies intended for the bunkers of sea-going vessels; supplies in direct transit apart from the stocks referred to in Article 7(1); supplies in pipelines, in road tankers and rail tank-wagons, in the storage tanks of retail outlets, and those held by small consumers.
Quantities held by the armed forces and those held for them by the oil companies shall also be excluded from the statistical summary.
Article 7
1. For the purposes of implementing this Directive, stocks may be established, under agreements between Governments, within the territory of a Member State for the account of undertakings, or bodies/entities, established in another Member State. It is for the Government of the Member State concerned to decide whether to hold a part of its stocks outside its national territory.
The Member State on whose territory the stocks are held under the framework of such an agreement shall not oppose the transfer of these stocks to the other Member States for the account of which stocks are held under that agreement; it shall keep a check on such stocks in accordance with the procedures specified in that agreement but shall not include them in its statistical summary. The Member State on whose behalf the stocks are held may include them in its statistical summary.
Together with the statistical summary, each Member State shall send a report to the Commission concerning the stocks maintained within its own territory for the benefit of another Member State, as well as the stocks held in other Member States for its own benefit. In both cases, the storage locations and/or companies holding the stocks, quantities and product category — or crude oil — stored will be indicated in the report.
2. Drafts of the agreements mentioned in the first subparagraph of paragraph 1 shall be sent to the Commission, which may make its comments known to the Governments concerned. The agreements, once concluded, shall be notified to the Commission, which shall make them known to the other Member States.
Agreements shall satisfy the following conditions:
(a) they must relate to crude oil and to all petroleum products covered by this Directive;
(b) they must lay down conditions and arrangements for the maintenance of stocks with the aim of safeguarding control and availability of these stocks;
(c) they must specify the procedures for checking and identifying the stocks provided for, inter alia, the methods for carrying out and cooperating on inspections;
(d) they must as a general rule be concluded for an unlimited period;
(e) they must state that, where provision is made for unilateral termination, the latter shall not operate in the event of a supply crisis and that, in any event, the Commission shall receive prior information of any termination.
3. When stocks established under such agreements are not owned by the undertaking, or body/entity, which has an obligation to hold stocks, but are held at the disposal of this undertaking, or body/entity, by another undertaking, or body/entity, the following conditions shall be met:
(a) the beneficiary undertaking, or body/entity, must have the contractual right to acquire these stocks throughout the period of the contract; the methodology for establishing the price of such acquisition must be agreed between the parties concerned;
(b) the minimum period of such a contract must be 90 days;
(c) storage location and/or companies holding the stocks at the disposal of the beneficiary undertaking, or body/entity, as well as quantity and category of product, or crude oil, stored in that location must be specified;
(d) the actual availability of the stocks for the beneficiary undertaking, or body/entity, must be guaranteed, at all times throughout the period of the contract, by the undertaking or body/entity holding the stocks at the disposal of the beneficiary undertaking, or body/entity;
(e) the undertaking, or body/entity, holding the stocks at the disposal of the beneficiary undertaking, or body/entity, must be one which is subject to the jurisdiction of the Member State on whose territory the stocks are situated insofar as the legal powers of that Member State to control and verify the existence of the stocks are concerned.
Article 8
Member States shall adopt all the necessary provisions and take all the necessary measures to ensure control and supervision of stocks. They shall put in place mechanisms to verify the stocks according to the provisions of this Directive.
Article 9
Member States shall determine the penalties applicable to breaches of the national provisions adopted pursuant to this Directive and shall take any measure necessary to ensure the implementation of these provisions. The penalties shall be effective, proportionate and dissuasive.
Article 10
1. If difficulties arise with regard to Community oil supplies, the Commission shall, at the request of any Member State or on its own initiative, arrange a consultation between the Member States.
2. Save in cases of particular urgency or in order to meet minor local needs, Member States shall refrain, prior to the consultation provided for in paragraph 1, from drawing on their stocks to any extent which would reduce those stocks to below the compulsory minimum level.
3. Member States shall inform the Commission of any withdrawals from their reserve stocks and shall communicate as soon as possible:
(a) the date upon which stocks fell below the compulsory minimum;
(b) the reasons for such withdrawals;
(c) the measures, if any, taken to replenish stocks;
(d) an appraisal, if possible, of the probable development of the situation with regard to the stocks while they remain below the compulsory minimum.
Article 11
The Commission shall submit regularly to the Council a report on the situation concerning stocks in the Community, including if appropriate on the need for harmonisation in order to ensure effective control and supervision of stocks.
Article 12
Directive 68/414/EEC shall be repealed, without prejudice to the obligations of the Member States relating to the time-limits for transposition into national law and implementation of the Directives set out in Annex I, Part B.
References to the repealed Directive shall be construed as references to this Directive and shall be read in accordance with the correlation table in Annex II.
Article 13
This Directive shall enter into force on the 20th day following its publication in the Official Journal of the European Union.
Article 14
This Directive is addressed to the Member States.
Done at Brussels, 24 July 2006.
For the Council
The President
M. Pekkarinen
[1] OJ C 226 E, 15.9.2005, p. 44.
[2] OJ C 112, 30.4.2004, p. 39.
[3] OJ L 308, 23.12.1968, p. 14. Directive as last amended by Directive 98/93/EC (OJ L 358, 31.12.1998, p. 100).
[4] See Annex I, Part A.
[5] OJ L 228, 16.8.1973, p. 1.
--------------------------------------------------
ANNEX I
PART A
Repealed Directive with its amending acts
Council Directive 68/414/EEC | (OJ L 308, 23.12.1968, p. 14) |
Council Directive 72/425/EEC | (OJ L 291, 28.12.1972, p. 154) |
Council Directive 98/93/EC | (OJ L 358, 31.12.1998, p. 100) |
PART B
List of time-limits for transposition into national law
(referred to in Article 12)
Directives | Time-limit for transposition | Date of application |
68/414/EEC | 1 January 1971 | 1 January 1971 |
98/93/EC | 1 January 2000 [1] | |
[1] 1 January 2003 for the Hellenic Republic as regards the obligations of Directive 98/93/EC in relation to the inclusion of bunker supplies for international aviation in the calculation of internal consumption. See Article 4 of Directive 98/93/EC.
--------------------------------------------------
ANNEX II
Correlation table
Directive 68/414/EEC | This Directive |
Article 1 | Article 1 |
Article 2, first paragraph, introductory wording | Article 2, first subparagraph, introductory wording |
Article 2, first paragraph, first indent | Article 2, first subparagraph, point (a) |
Article 2, first paragraph, second indent | Article 2, first subparagraph, point (b) |
Article 2, first paragraph, third indent | Article 2, first subparagraph, point (c) |
Article 2, second paragraph | Article 2, second subparagraph |
Article 3 | Article 3 |
Article 4, first paragraph | Article 4(1) |
Article 4, second paragraph | Article 4(2) |
Article 4, third paragraph | Article 4(3) |
Article 5, first paragraph | Article 5(1) |
Article 5, second paragraph, introductory wording, first part of sentence | Article 5(2), introductory wording |
Article 5, second paragraph, introductory wording, second part of sentence | Article 5(2)(a) |
Article 5, second paragraph, introductory wording, third part of sentence | Article 5(2)(b) introductory wording |
Article 5, second paragraph, first indent | Article 5(2)(b)(i) |
Article 5, second paragraph, second indent | Article 5(2)(b)(ii) |
Article 5, second paragraph, third indent | Article 5(2)(b)(iii) |
Article 5, third paragraph | Article 5(3) |
Article 6(1) | Article 6(1) |
Article 6(2), first subparagraph | Article 7(1), first subparagraph |
Article 6(2), second subparagraph | Article 7(1), second subparagraph |
Article 6(2), third subparagraph | Article 7(1), third subparagraph |
Article 6(2), fourth subparagraph | Article 7(2), first subparagraph |
Article 6(2), fifth subparagraph, introductory wording | Article 7(2), second subparagraph, introductory wording |
Article 6(2), fifth subparagraph, first indent | Article 7(2), second subparagraph, point (a) |
Article 6(2), fifth subparagraph, second indent | Article 7(2), second subparagraph, point (b) |
Article 6(2), fifth subparagraph, third indent | Article 7(2), second subparagraph, point (c) |
Article 6(2), fifth subparagraph, fourth indent | Article 7(2), second subparagraph, point (d) |
Article 6(2), fifth subparagraph, fifth indent | Article 7(2), second subparagraph, point (e) |
Article 6(2), sixth subparagraph, introductory wording | Article 7(3), introductory wording |
Article 6(2), sixth subparagraph, first indent | Article 7(3)(a) |
Article 6(2), sixth subparagraph, second indent | Article 7(3)(b) |
Article 6(2), sixth subparagraph, third indent | Article 7(3)(c) |
Article 6(2), sixth subparagraph, fourth indent | Article 7(3)(d) |
Article 6(2), sixth subparagraph, fifth indent | Article 7(3)(e) |
Article 6(3), first subparagraph, introductory wording | Article 6(2), introductory wording |
Article 6(3), first subparagraph, first indent | Article 6(2)(a) |
Article 6(3), first subparagraph, second indent | Article 6(2)(b) |
Article 6(3), first subparagraph, third indent | Article 6(2)(c) |
Article 6(3), first subparagraph, fourth indent | Article 6(2)(d) |
Article 6(3), first subparagraph, fifth indent | Article 6(2)(e) |
Article 6(3), first subparagraph, sixth indent | Article 6(2)(f) |
Article 6(3), first subparagraph, seventh indent | Article 6(2)(g) |
Article 6(3), second subparagraph, first sentence | Article 6(3), first subparagraph |
Article 6(3), second subparagraph, second sentence | Article 6(3), second subparagraph |
Article 6a | Article 8 |
Article 6b | Article 9 |
Article 7, first paragraph | Article 10(1) |
Article 7, second paragraph | Article 10(2) |
Article 7, third paragraph, introductory wording | Article 10(3), introductory wording |
Article 7, third paragraph, first indent | Article 10(3)(a) |
Article 7, third paragraph, second indent | Article 10(3)(b) |
Article 7, third paragraph, third indent | Article 10(3)(c) |
Article 7, third paragraph, fourth indent | Article 10(3)(d) |
Article 8 | — |
— | Article 11 |
— | Article 12 |
— | Article 13 |
Article 9 | Article 14 |
— | Annex I |
— | Annex II |
--------------------------------------------------
