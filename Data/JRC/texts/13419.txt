Commission Regulation (EC) No 1155/2006
of 28 July 2006
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 29 July 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 28 July 2006.
For the Commission
Jean-Luc Demarty
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 386/2005 (OJ L 62, 9.3.2005, p. 3).
--------------------------------------------------
ANNEX
to Commission Regulation of 28 July 2006 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code [1] | Standard import value |
07020000 | 052 | 71,2 |
096 | 41,9 |
999 | 56,6 |
07070005 | 052 | 78,5 |
388 | 52,4 |
524 | 46,9 |
999 | 59,3 |
07099070 | 052 | 69,2 |
999 | 69,2 |
08055010 | 388 | 57,0 |
524 | 54,9 |
528 | 58,4 |
999 | 56,8 |
08061010 | 052 | 160,1 |
204 | 133,3 |
220 | 142,0 |
388 | 8,7 |
400 | 200,9 |
508 | 48,4 |
512 | 56,7 |
624 | 158,2 |
999 | 113,5 |
08081080 | 388 | 95,5 |
400 | 104,3 |
508 | 76,8 |
512 | 88,4 |
524 | 67,7 |
528 | 90,3 |
720 | 78,9 |
800 | 152,2 |
804 | 98,3 |
999 | 94,7 |
08082050 | 052 | 70,3 |
388 | 103,0 |
512 | 90,4 |
528 | 83,8 |
720 | 30,3 |
804 | 128,9 |
999 | 84,5 |
08091000 | 052 | 135,5 |
999 | 135,5 |
08092095 | 052 | 297,9 |
400 | 365,8 |
999 | 331,9 |
08093010, 08093090 | 052 | 101,7 |
999 | 101,7 |
08094005 | 093 | 64,8 |
098 | 75,9 |
624 | 131,5 |
999 | 90,7 |
[1] Country nomenclature as fixed by Commission Regulation (EC) No 750/2005 (OJ L 126, 19.5.2005, p. 12). Code "999" stands for "of other origin".
--------------------------------------------------
