P6_TA(2005)0065
Work of the ACP-EU Joint Parliamentary Assembly in 2004
European Parliament resolution on the work of the ACP-EU Joint Parliamentary Assembly in 2004 (2004/2141(INI))
The European Parliament,
- having regard to the Partnership Agreement between the members of the African, Caribbean and Pacific (ACP) Group of States of the one part, and the European Community and its Member States, of the other part, signed in Cotonou on 23 June 2000 (Cotonou Agreement) [1], which entered into force on 1 April 2003,
- having regard to the Rules of Procedure of the Joint Parliamentary Assembly (JPA) as last amended at The Hague on 25 November 2004 [2],
- having regard to the resolutions and the declaration adopted by the JPA at its 7th session in Addis Ababa (Ethiopia) from 16 to 19 February 2004 [3] on:
- Economic Partnership Agreements (EPA): problems and prospects,
- conflict prevention and resolution and the establishment of a lasting peace,
- poverty diseases and reproductive health in ACP countries,
- cotton and other commodities: problems encountered by ACP countries,
- damage caused by cyclones in the Pacific, the Indian Ocean and the Caribbean and the need for a rapid response to natural disasters,
- having regard to the resolutions adopted by the JPA at its 8th session in The Hague (Netherlands) from 22 to 25 November 2004 [2] on:
- political dialogue (Article 8 of the Cotonou Agreement),
- food aid and food security,
- the situation in Darfur,
- damage caused by cyclones in the Caribbean region,
- having regard to Council Regulation (EC) No 314/2004 of 19 February 2004 concerning certain restrictive measures in respect of Zimbabwe [4],
- having regard to Rule 45 of its Rules of Procedure,
- having regard to the report of the Committee on Development (A6-0044/2005),
A. whereas the JPA stands as a model of cooperation and development in the world and is making a major contribution to open North-South dialogue on the basis of equal rights,
B. whereas the setting-up of standing committees has contributed to the Assembly's historic development into a genuine parliamentary assembly,
C. whereas four joint missions took place in 2004: to Eritrea from 22 to 25 January, to observe the elections in Malawi from 18 to 20 May, to the ACP-EU Summit in Maputo (Mozambique) from 21 to 24 June and to the 7th regional seminar of ACP-EU economic and social interest groups, in Fiji, from 18 to 20 October,
D. whereas, for the first time, the ACP Group sent an observers' mission to The Hague (Netherlands) and to Budapest (Hungary) to observe the European elections of 10 to 13 June 2004,
E. having regard to the importance of the role of the JPA in implementing the political dialogue provided for by Article 8 of the Cotonou Agreement, particularly with regard to strengthening democracy, implementing good governance and protecting human rights,
F. welcoming the fact that on 14 November 2004, the Ethiopian Government agreed in principle on the demarcation of the boundary with Eritrea, in keeping with the decision by the International Border Commission,
G. whereas on 21 January 2004, the President of the European Parliament, speaking to the JPA, reiterated the Parliament's position on the Zimbabwean delegation,
H. whereas the situation in Zimbabwe has deteriorated still further with regard to human rights and freedom of opinion, the right of association and peaceful assembly,
I. whereas the co-rapporteurs of the ACP-EU Committee on Economic Development, Finance and Trade were unable to reach agreement in good time on a joint text on the budgetisation of the European Development Fund (EDF),
J. having regard to the addresses to the JPA by the Centre for the Development of Enterprise (CDE) and the Technical Centre for Agricultural and Rural Cooperation ACP-EU (CTA) respectively,
K. having regard to the excellent contribution made by the Dutch Presidency, and various local authorities, to the organisation and contents of the 8th session in The Hague,
1. Calls on the JPA to strengthen its role as a forum for political dialogue, in accordance with Article 8 of the Cotonou Agreement;
2. Congratulates the Bureau of the JPA for its efforts to make a positive contribution to the peaceful resolution of the border dispute between Eritrea and Ethiopia, by means of dialogue and in full respect for international law;
3. Stresses the political importance of the resolution adopted by the JPA on 25 November 2004 on the situation in Darfur, condemning the ongoing violence and calling for a political solution based on UN Security Council Resolutions 1564 and 1574 (2004);
4. Calls on the JPA to continue its work on human rights and thereby contribute to the political dialogue provided for in Article 8 of the Cotonou Agreement; calls for a greater involvement with civil society in the work of the JPA and its Standing Committees;
5. Suggests that, for matters of cross committee interest, a procedure of cooperation is put in place so that Opinions may be given by committees on Reports in the lead Committee;
6. Welcomes the work of Members of the ACP that ensured better preparation of compromise urgent resolutions in 2004, making a final vote by separate houses unnecessary;
7. Welcomes the new Commission's attitude to Question Time, answering questions in writing and supplementary questions orally; asks the Council to follow the same procedure;
8. Calls on the Commission and other addressees of the resolution to improve the follow-up of previous resolutions, in particular during meetings of Standing Committees;
9. Welcomes the sending of a delegation of ACP parliamentarians to The Hague (Netherlands) and to Budapest (Hungary) to observe the European elections of 10 to 13 June 2004, and welcomes this as a new stage in the development of the JPA into an Assembly of fully equal partners;
10. Congratulates the ACP Group for upholding its undertaking that at meetings being held on EU territory, the Zimbabwean delegation should include no members subject to the Council's restrictive measures; and is confident that they will follow this undertaking in future;
11. Calls on the JPA to adopt in plenary sitting in Bamako (Mali) its report on the budgetisation of the EDF; stresses that inclusion of the EDF in the EU budget should be accompanied by a reinforcement of the political role of the JPA in monitoring the funds committed in the framework of the ACP-EU partnership;
12. Welcomes the regular monitoring work carried out by the ACP-EU Committee on Economic Development, Finance and Trade with regard to the ACP-EU Economic Partnership Agreements, in cooperation with the Commission and representatives of civil society;
13. Welcomes the increased cooperation between the JPA and the UN and its various bodies, and calls on the JPA to continue to develop such cooperation;
14. Calls on the JPA to help strengthen the role of parliamentarians in the ACP countries in terms of exercising parliamentary control of their respective governments, particularly with regard to development aid;
15. Calls on the JPA to exercise to the full its remit of democratic supervision of the CDE and CTA as bodies set up under the Cotonou Agreement, as it did at Addis Ababa within the framework of a general debate with the CDE and in The Hague with the CTA, in the context of its consideration of the report on food aid and food security;
16. Congratulates the JPA Bureau on having rebalanced the agenda of the JPA sessions so as to leave more time for contributions from the floor and fewer and, in most cases, shorter contributions from the platform;
17. Calls on the Bureau to avoid meetings taking place at the same time as the plenary;
18. Notes with satisfaction the involvement of non-state actors in fringe meetings, including the Women's Forum;
19. Stresses the added value of holding the JPA sessions in the EU Member States by rotation, and believes that this rotation should be maintained in the future, whether following the changing Presidency of the Council or following some other arrangement if the European Constitution is ratified by all the Member States; notes however that the choice of a climatically more hospitable time of year might be welcomed by ACP Members, perhaps by switching the JPA sessions;
20. Congratulates the Dutch Presidency, the Provinces of Noord Brabant and Zeeland, and The Hague and Rotterdam city authorities on their active contribution to the 8th session, in particular the workshops;
21. Calls on the British Presidency and the Bureau of the EP to make the requisite arrangements for holding the 10th session in the United Kingdom;
22. Calls on the JPA, pursuant to Article 17(3) of the Cotonou Agreement, to organise meetings at regional or sub-regional level, based on the existing parliamentary structures within the ACP Group; in particular on regional cooperation in the sphere of conflict prevention and resolution, and on concluding and implementing Economic Partnership Agreements;
23. Welcomes the speed with which JPA documents are made available on the Internet, an essential tool for the smooth functioning of the JPA where geographical distances between the members are extremely large;
24. Calls on the JPA and the two co-secretariats to make systematic use of e-mail and the Internet to forward documents to Members, and actively to encourage use of the Internet, where it is available;
25. Welcomes the inaugural session of the Pan-African Parliament (PAP) held in Addis Ababa in March 2004 and proposes close working between EP and PAP Members and an exchange programme of EP officials with PAP officials to help them build up capacity in view of the first direct elections scheduled for March 2009;
26. Instructs its President to forward this resolution to the ACP-EU Council, the ACP-EU Joint Parliamentary Assembly, the parliaments of the ACP countries, the governors of the Provinces of Noord Brabant and Zeeland, the mayors of The Hague and of Rotterdam, and the Commission.
[1] OJ L 317, 15.12.2000, p. 3.
[2] Not yet published in OJ.
[3] OJ C 120, 30.4.2004, p. 1.
[4] OJ L 55, 24.2.2004, p. 1. Regulation amended by Commission Regulation (EC) No 1488/2004 (OJ L 273, 21.8.2004, p. 12).
--------------------------------------------------
