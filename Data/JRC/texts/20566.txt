FINAL ACT OF THE EUROPEAN ENERGY CHARTER CONFERENCE
I. The final Plenary Session of the European Energy Charter Conference was held at Lisbon on 16 to 17 December 1994. Representatives of the Republic of Albania, the Republic of Armenia, Australia, the Republic of Austria, the Azerbaijani Republic, the Kingdom of Belgium, the Republic of Belarus, the Republic of Bulgaria, Canada, the Republic of Croatia, the Republic of Cyprus, the Czech Republic, the Kingdom of Denmark, the Republic of Estonia, the European Communities, the Republic of Finland, the French Republic, the Republic of Georgia, the Federal Republic of Germany, the Hellenic Republic, the Republic of Hungary, the Republic of Iceland, Ireland, the Italian Republic, Japan, the Republic of Kazakhstan, the Republic of Kyrgyzstan, the Republic of Latvia, the Principality of Liechtenstein, the Republic of Lithuania, the Grand Duchy of Luxembourg, the Republic of Malta, the Republic of Moldova, the Kingdom of the Netherlands, the Kingdom of Norway, the Republic of Poland, the Portuguese Republic, Romania, the Russian Federation, the Slovak Republic, the Republic of Slovenia, the Kingdom of Spain, the Kingdom of Sweden, the Swiss Confederation, the Republic of Tajikistan, the Republic of Turkey, Turkmenistan, Ukraine, the United Kingdom of Great Britain and Northern Ireland, the United States of America and the Republic of Uzbekistan (hereinafter referred to as 'the representatives`) participated in the Conference, as did invited observers from certain countries and international organizations.
BACKGROUND
II. During the meeting of the European Council in Dublin in June 1990, the Prime Minister of the Netherlands suggested that economic recovery in eastern Europe and the then Union of Soviet Socialist Republics could be catalysed and accelerated by cooperation in the energy sector. This suggestion was welcomed by the Council, which invited the Commission of the European Communities to study how best to implement such cooperation. In February 1991 the Commission proposed the concept of a European Energy Charter.
Following discussion of the Commission's proposal in the Council of the European Communities, the European Communities invited the other countries of western and eastern Europe, of the Union of Soviet Socialist Republics and the non-European members of the Organization for Economic Cooperation and Development to attend a conference in Brussels in July 1991 to launch negotiations on the European Energy Charter. A number of other countries and international organizations were invited to attend the European Energy Charter Conference as observers.
Negotiations on the European Energy Charter were completed in 1991 and the Charter was adopted by signature of a Concluding Document at a conference held at The Hague on 16 to 17 December 1991. Signatories of the Charter, then or subsequently, include all those listed in Section I, other than observers.
The signatories of the European Energy Charter undertook:
- to pursue the objectives and principles of the Charter and implement and broaden their cooperation as soon as possible by negotiating in good faith a Basic Agreement and Protocols.
The European Energy Charter Conference accordingly began negotiations on a Basic Agreement - later called the Energy Charter Treaty - designed to promote east-west industrial cooperation by providing legal safeguards in areas such as investment, transit and trade. It also began negotiations on Protocols in the fields of energy efficiency, nuclear safety and hydrocarbons, although in the last case negotiations were later suspended until completion of the Energy Charter Treaty.
Negotiations on the Energy Charter Treaty and the Energy Charter Protocol on energy efficiency and related environmental aspects were successfully completed in 1994.
THE ENERGY CHARTER TREATY
III. As a result of its deliberations the European Energy Charter Conference has adopted the text of the Energy Charter Treaty (hereinafter referred to as the 'Treaty`) which is set out in Annex 1 and Decisions with respect thereto which are set out in Annex 2, and agreed that the Treaty would be open for signature at Lisbon from 17 December 1994 to 16 June 1995.
UNDERSTANDINGS
IV. By signing the Final Act, the representatives agreed to adopt the following understandings with respect to the Treaty:
1. With respect to the Treaty as a whole
(a) The representatives underline that the provisions of the Treaty have been agreed upon bearing in mind the specific nature of the Treaty aiming at a legal framework to promote long-term cooperation in a particular sector and as a result cannot be construed to constitute a precedent in the context of other international negotiations.
(b) The provisions of the Treaty do not:
(i) oblige any Contracting Party to introduce mandatory third party access; or
(ii) prevent the use of pricing systems which, within a particular category of consumers, apply identical prices to customers in different locations.
(c) Derogations from most favoured nation treatment are not intended to cover measures which are specific to an investor or group of investors, rather than applying generally.
2. With respect to Article 1 (5)
(a) It is understood that the Treaty confers no rights to engage in economic activities other than economic activities in the energy sector.
(b) The following activities are illustrative of economic activity in the energy sector:
(i) prospecting and exploration for, and extraction of, e.g., oil, gas, coal and uranium;
(ii) construction and operation of power generation facilities, including those powered by wind and other renewable energy sources;
(iii) land transportation, distribution, storage and supply of energy materials and products, e.g., by way of transmission and distribution grids and pipelines or dedicated rail lines, and construction of facilities for such, including the laying of oil, gas, and coal-slurry pipelines;
(iv) removal and disposal of wastes from energy related facilities such as power stations, including radioactive wastes from nuclear power stations;
(v) decommissioning of energy related facilities, including oil rigs, oil refineries and power generating plants;
(vi) marketing and sale of, and trade in energy materials and products, e.g., retail sales of gasoline; and
(vii) research, consulting, planning, management and design activities related to the activities mentioned above, including those aimed at improving energy efficiency.
3. With respect to Article 1 (6)
For greater clarity as to whether an investment made in the area of one Contracting Party is controlled, directly or indirectly, by an investor of any other Contracting Party, control of an investment means control in fact, determined after an examination of the actual circumstances in each situation. In any such examination, all relevant factors should be considered, including the investor's:
(a) financial interest, including equity interest, in the investment;
(b) ability to exercise substantial influence over the management and operation of the investment; and
(c) ability to exercise substantial influence over the selection of members of the board of directors or any other managing body.
Where there is doubt as to whether an investor controls, directly or indirectly, an investment, an investor claiming such control has the burden of proof that such control exists.
4. With respect to Article 1 (8)
Consistent with Australia's foreign investment policy, the establishment of a new mining or raw materials processing project in Australia with total investment of $A 10 million or more by a foreign interest, even where that foreign interest is already operating a similar business in Australia, is considered as the making of a new investment.
5. With respect to Article 1 (12)
The representatives recognize the necessity for adequate and effective protection of intellectual property rights according to the highest internationally-accepted standards.
6. With respect to Article 5 (1)
The representatives' agreement to Article 5 is not meant to imply any position on whether or to what extent the provisions of the 'Agreement on Trade-Related Investment Measures` annexed to the Final Act of the Uruguay Round of Multilateral Trade Negotiations are implicit in Articles III and XI of the General Agreement on Tariffs and Trade (GATT).
7. With respect to Article 6
(a) The unilateral and concerted anti-competitive conduct referred to in Article 6 (2) are to be defined by each Contracting Party in accordance with its laws and may include exploitative abuses.
(b) 'Enforcement` and 'enforces` include action under the competition laws of a Contracting Party by way of investigation, legal proceeding, or administrative action as well as by way of any decision or further law granting or continuing an authorization.
8. With respect to Article 7 (4)
The applicable legislation would include provisions on environmental protection, land use, safety, or technical standards.
9. With respect to Articles 9, 10 and Part V
As a Contracting Party's programmes which provide for public loans, grants, guarantees or insurance for facilitating trade or investment abroad are not connected with investment or related activities of investors from other Contracting Parties in its area, such programmes may be subject to constraints with respect to participation in them.
10. With respect to Article 10 (4)
The supplementary treaty will specify conditions for applying the treatment described in Article 10 (3). Those conditions will include, inter alia, provisions relating to the sale or other divestment of state assets (privatization) and to the dismantling of monopolies (demonopolization).
11. With respect to Articles 10 (4) and 29 (6)
Contracting Parties may consider any connection between the provisions of Article 10 (4) and Article 29 (6).
12. With respect to Article 14 (5)
It is intended that a Contracting Party which enters into an agreement referred to in Article 14 (5) ensure that the conditions of such an agreement are not in contradiction with that Contracting Party's obligations under the Articles of Agreement of the International Monetary Fund.
13. With respect to Article 19 (1) (i)
It is for each Contracting Party to decide the extent to which the assessment and monitoring of environmental impacts should be subject to legal requirements, the authorities competent to take decisions in relation to such requirements, and the appropriate procedures to be followed.
14. With respect to Articles 22 and 23
With regard to trade in energy materials and products governed by Article 29, that Article specifies the provisions relevant to the subjects covered by Articles 22 and 23.
15. With respect to Article 24
Exceptions contained in the GATT and Related Instruments apply between particular Contracting Parties which are parties to the GATT, as recognized by Article 4. With respect to trade in energy materials and products governed by Article 29, that Article specifies the provisions relevant to the subjects covered by Article 24.
16. With respect to Article 26 (2) (a)
Article 26 (2) (a) should not be interpreted to require a Contracting Party to enact Part III of the Treaty into its domestic law.
17. With respect to Articles 26 and 27
The reference to treaty obligations in the penultimate sentence of Article 10 (1) does not include decisions taken by international organizations, even if they are legally binding, or treaties which entered into force before 1 January 1970.
18. With respect to Article 29 (2) (a)
(a) Where a provision of GATT 1947 or a Related Instrument referred to in this paragraph provides for joint action by parties to the GATT, it is intended that the Charter Conference take such action.
(b) The notion 'applied on 1 March 1994 and practised with regard to energy materials and products by parties to GATT 1947 among themselves` is not intended to refer to cases where a party to the GATT has invoked Article XXXV of the GATT, thereby disapplying the GATT vis-à-vis another party to the GATT, but nevertheless applies unilaterally on a de facto basis some provisions of the GATT vis-à-vis that other party to the GATT.
19. With respect to Article 33
The provisional Charter Conference should at the earliest possible date decide how best to give effect to the goal of Title III of the European Energy Charter that Protocols be negotiated in areas of cooperation such as those listed in Title III of the Charter.
20. With respect to Article 34
(a) The provisional Secretary-General should make immediate contact with other international bodies in order to discover the terms on which they might be willing to undertake tasks arising from the Treaty and the Charter. The provisional Secretary-General might report back to the provisional Charter Conference at the meeting which Article 45 (4) requires to be convened not later than 180 days after the opening date for signature of the Treaty.
(b) The Charter Conference should adopt the annual budget before the beginning of the financial year.
21. With respect to Article 34 (3) (m)
The technical changes to Annexes might for instance include, delisting of non-signatories or of signatories that have evinced their intention not to ratify, or additions to Annexes N and VC. It is intended that the Secretariat would propose such changes to the Charter Conference when appropriate.
22. With respect to Annex TFU (1)
(a) If some of the parties to an agreement referred to in paragraph 1 have not signed or acceded to the Treaty at the time required for notification, those parties to the agreement which have signed or acceded to the Treaty may notify on their behalf.
(b) The need in general for notification of agreements of a purely commercial nature is not foreseen because such agreements should not raise a question of compliance with Article 29 (2) (a), even when they are entered into by state agencies. The Charter Conference could, however, clarify for purposes of Annex TFU which types of agreements referred to in Article 29 (2) (b) require notification under the Annex and which types do not.
DECLARATIONS
V. The representatives declared that Article 18 (2) shall not be construed to allow the circumvention of the application of the other provisions of the Treaty.
VI. The representatives also noted the following declarations that were made with respect to the Treaty:
1. With respect to Article 1 (6)
The Russian Federation wishes to have reconsidered, in negotiations with regard to the supplementary treaty referred to in Article 10 (4), the question of the importance of national legislation with respect to the issue of control as expressed in the understanding to Article 1 (6).
2. With respect to Articles 5 and 10 (11)
Australia notes that the provisions of Articles 5 and 10 (11) do not diminish its rights and obligations under the GATT, including as elaborated in the Uruguay Round Agreement on Trade-Related Investment Measures, particularly with respect to the list of exceptions in Article 5 (3), which it considers incomplete.
Australia further notes that it would not be appropriate for dispute settlement bodies established under the Treaty to give interpretations of GATT Articles III and XI in the context of disputes between parties to the GATT or between an investor of a party to the GATT and another party to the GATT. It considers that with respect to the application of Article 10 (11) between an investor and a party to the GATT, the only issue that can be considered under Article 26 is the issue of the awards of arbitration in the event that a GATT panel or the WTO dispute settlement body first establishes that a trade-related investment measure maintained by the Contracting Party is inconsistent with its obligations under the GATT or the Agreement on Trade-Related Investment Measures.
3. With respect to Article 7
The European Communities and their Member States and Austria, Norway, Sweden and Finland declare that the provisions of Article 7 are subject to the conventional rules of international law on jurisdiction over submarine cables and pipelines or, where there are no such rules, to general international law.
They further declare that Article 7 is not intended to affect the interpretation of existing international law on jurisdiction over submarine cables and pipelines, and cannot be considered as doing so.
4. With respect to Article 10
Canada and the United States of America each affirm that they will apply the provisions of Article 10 in accordance with the following considerations:
For the purposes of assessing the treatment which must be accorded to investors of other Contracting Parties and their investments, the circumstances will need to be considered on a case-by-case basis. A comparison between the treatment accorded to investors of one Contracting Party, or the investments of investors of one Contracting Party, and the investments or investors of another Contracting Party, is only valid if it is made between investors and investments in similar circumstances. In determining whether differential treatment of investors or investments is consistent with Article 10, two basic factors must be taken into account.
The first factor is the policy objectives of Contracting Parties in various fields in so far as they are consistent with the principles of non-discrimination set out in Article 10. Legitimate policy objectives may justify differential treatment of foreign investors or their investments in order to reflect a dissimilarity of relevant circumstances between those investors and investments and their domestic counterparts. For example, the objective of ensuring the integrity of a country's financial system would justify reasonable prudential measures with respect to foreign investors or investments, where such measures would be unnecessary to ensure the attainment of the same objectives in so far as domestic investors or investments are concerned. Those foreign investors or their investments would thus not be 'in similar circumstances` to domestic investors or their investments. Thus, even if such a measure accorded differential treatment, it would not be contrary to Article 10.
The second factor is the extent to which the measure is motivated by the fact that the relevant investor or investment is subject to foreign ownership or under foreign control. A measure aimed specifically at investors because they are foreign, without sufficient countervailing policy reasons consistent with the preceding paragraph, would be contrary to the principles of Article 10. The foreign investor or investment would be 'in similar circumstances` to domestic investors and their investments, and the measure would be contrary to Article 10.
5. With respect to Article 25
The European Communities and their Member States recall that, in accordance with Article 58 of the Treaty establishing the European Community:
(a) companies or firms formed in accordance with the law of a Member State and having their registered office, central administration or principal place of business within the Community shall, for the right of establishment pursuant to Part Three, Title III, Chapter 2 of the Treaty establishing the European Community, be treated in the same way as natural persons who are nationals of Member States; companies or firms which only have their registered office within the Community must, for this purpose, have an effective and continuous link with the economy of one of the Member States;
(b) 'companies and firms` means companies or firms constituted under civil or commercial law, including cooperative societies, and other legal persons governed by public or private law, save for those which are non-profitmaking.
The European Communities and their Member States further recall that:
Community law provides for the possibility to extend the treatment described above to branches and agencies of companies or firms not established in one of the Member States; and that, the application of Article 25 of the Energy Charter Treaty will allow only those derogations necessary to safeguard the preferential treatment resulting from the wider process of economic integration resulting from the Treaties establishing the European Communities.
6. With respect to Article 40
Denmark recalls that the European Energy Charter does not apply to Greenland and the Faroe Islands until notice to this effect has been received from the local governments of Greenland and the Faroe Islands.
In this respect Denmark affirms that Article 40 of the Treaty applies to Greenland and the Faroe Islands.
7. With respect to Annex G (4)
(a) The European Communities and the Russian Federation declare that trade in nuclear materials between them shall be governed, until they reach another agreement, by the provisions of Article 22 of the Agreement on Partnership and Cooperation establishing a partnership between the European Communities and their Member States, of the one part, and the Russian Federation, of the other part, signed at Corfu on 24 June 1994, the exchange of letters attached thereto and the related joint declaration, and disputes regarding such trade will be subject to the procedures of the said Agreement.
(b) The European Communities and Ukraine declare that, in accordance with the Partnership and Cooperation Agreement signed at Luxembourg on 14 June 1994 and the Interim Agreement thereto, initialled there the same day, trade in nuclear materials between them shall be exclusively governed by the provisions of a specific agreement to be concluded between the European Atomic Energy Community and Ukraine.
Until entry into force of this specific agreement, the provisions of the Agreement on Trade and Economic and Commercial Cooperation between the European Economic Community, the European Atomic Energy Community and the Union of Soviet Socialist Republics signed at Brussels on 18 December 1989 shall exclusively continue to apply to trade in nuclear materials between them.
(c) The European Communities and Kazakhstan declare that, in accordance with the Partnership and Cooperation Agreement initialled at Brussels on 20 May 1994, trade in nuclear materials between them shall be exclusively governed by the provisions of a specific agreement to be concluded between the European Atomic Energy Community and Kazakhstan.
Until entry into force of this specific agreement, the provisions of the Agreement on Trade and Economic and Commercial Cooperation between the European Economic Community, the European Atomic Energy Community and the Union of Soviet Socialist Republics signed at Brussels on 18 December 1989 shall exclusively continue to apply to trade in nuclear materials between them.
(d) The European Communities and Kyrgyzstan declare that, in accordance with the Agreement on Partnership and Cooperation initialled at Brussels on 31 May 1994, trade in nuclear materials between them shall be exclusively governed by the provisions of a specific agreement to be concluded between the European Atomic Energy Community and Kyrgyzstan.
Until entry into force of this specific agreement, the provisions of the Agreement on Trade and Economic and Commercial Cooperation between the European Economic Community, the European Atomic Energy Community and the Union of Soviet Socialist Republics signed at Brussels on 18 December 1989 shall exclusively continue to apply to trade in nuclear materials between them.
(e) The European Communities and Tajikistan declare that trade in nuclear materials between them shall be exclusively governed by the provisions of a specific agreement to be concluded between the European Atomic Energy Community and Tajikistan.
Until entry into force of this specific agreement, the provisions of the Agreement on Trade and Economic and Commercial Cooperation between the European Economic Community, the European Atomic Energy Community and the Union of Soviet Socialist Republics signed at Brussels on 18 December 1989 shall exclusively continue to apply to trade in nuclear materials between them.
(f) The European Communities and Uzbekistan declare that trade in nuclear materials between them shall be exclusively governed by the provisions of a specific agreement to be concluded between the European Atomic Energy Community and Uzbekistan.
Until entry into force of this specific agreement, the provisions of the Agreement on Trade and Economic and Commercial Cooperation between the European Economic Community, the European Atomic Energy Community and the Union of Soviet Socialist Republics signed at Brussels on 18 December 1989 shall exclusively continue to apply to trade in nuclear materials between them.
THE ENERGY CHARTER PROTOCOL ON ENERGY EFFICIENCY AND RELATED ENVIRONMENTAL ASPECTS
VII. The European Energy Charter Conference has adopted the text of the Energy Charter Protocol on energy efficiency and related environmental aspects which is set out in Annex 3.
THE EUROPEAN ENERGY CHARTER
VIII. The provisional Charter Conference and the Charter Conference provided for in the Treaty shall henceforth be responsible for making decisions on requests to sign the Concluding Document of The Hague Conference on the European Energy Charter and the European Energy Charter adopted thereby.
DOCUMENTATION
XI. The records of negotiations of the European Energy Charter Conference will be deposited with the Secretariat.
Done at Lisbon on the seventeenth day of December in the year one thousand nine hundred and ninety-four.
Fait à Lisbonne, le dix-sept décembre mil neuf cent quatre-vingt-quatorze.
Geschehen zu Lissabon am siebzehnten Dezember neunzehnhundertvierundneunzig.
Fatto a Lisbona il diciassettesimo giorno del mese di dicembre dell'anno millenovecentonovantaquattro.
>REFERENCE TO A FILM>
Hecho en Lisboa, el diecisiete de diciembre de mil novecientos noventa y cuatro.
Udfærdiget i Lissabon, den syttende december nittenhundrede og fireoghalvfems.
¸ãéíå óôç Ëéóáâüíá, óôéò äÝêá åðôÜ Äåêåìâñßïõ ôïõ Ýôïõò ÷ßëéá åíéáêüóéá åíåíÞíôá ôÝóóåñá.
Gedaan te Lissabon, de zeventiende december negentienhonderd vierennegentig.
Feito em Lisboa, aos dezassete de Dezembro de mil novecentos e noventa e quatro.
Për Republikën e Shqipërisë
>REFERENCE TO A FILM>
>REFERENCE TO A FILM>
For Australia
>REFERENCE TO A FILM>
Für die Republik Österreich
>REFERENCE TO A FILM>
>REFERENCE TO A FILM>
Pour le royaume de Belgique
Voor het Koninkrijk België
Für das Königreich Belgien
>REFERENCE TO A FILM>
Cette signature engage également la Communauté française de Belgique, la Communauté flamande, la Communauté germanophone de Belgique, la Région wallonne, la Région flamande et la région de Bruxelles-Capitale.
Deze handtekening bindt eveneens de Vlaamse Gemeenschap, de Franse Gemeenschap van België, de Duitstalige Gemeenschap van België, het Waals Gewest en het Brussels Hoofdstedelijk Gewest.
Diese Unterschrift bindet ebenso die Flämische Gemeinschaft, die Französische Gemeinschaft Belgiens, die Deutschsprachige Gemeinschaft Belgiens, die Flämische Region, die Wallonische Region und die Region Brüssel-Hauptstadt.
>REFERENCE TO A FILM>
>REFERENCE TO A FILM>
For Canada
Pour le Canada
za Republiku Hrvatsku
>REFERENCE TO A FILM>
For the Republic of Cyprus
>REFERENCE TO A FILM>
Za OCeskou Republiku
>REFERENCE TO A FILM>
For Kongeriget Danmark
>REFERENCE TO A FILM>
Eesti Vabariigi nimel
>REFERENCE TO A FILM>
Por las Comunidades Europeas
For De Europæiske Fællesskaber
Für die Europäischen Gemeinschaften
Ãéá ôéò ÅõñùðáúêÝò Êïéíüôçôåò
For the European Communities
Pour les Communautés européennes
Per le Comunità europee
Voor de Europese Gemeenschappen
Pelas Comunidades Europeias
>REFERENCE TO A FILM>
Suomen tasavallan puolesta
>REFERENCE TO A FILM>
Pour la République française
>REFERENCE TO A FILM>
>REFERENCE TO A FILM>
Für die Bundesrepublik Deutschland
>REFERENCE TO A FILM>
Ãéá ôçí ÅëëçíéêÞ Äçìïêñáôßá
>REFERENCE TO A FILM>
A Magyar Köztársaság nevében
Fyrir hönd Ly sveldisins íslands
>REFERENCE TO A FILM>
Thar cheann na hÉireann
For Ireland
>REFERENCE TO A FILM>
Per la Repubblica italiana
>REFERENCE TO A FILM>
>REFERENCE TO A FILM>
>REFERENCE TO A FILM>
>REFERENCE TO A FILM>
Latvijas Republikas varda
>REFERENCE TO A FILM>
Für das Fürstentum Liechtenstein
>REFERENCE TO A FILM>
Lietuvos Respublikos vardu
>REFERENCE TO A FILM>
Pour le grand-duché de Luxembourg
>REFERENCE TO A FILM>
For the Republic of Malta
>REFERENCE TO A FILM>
Pentru Republica Moldova
>REFERENCE TO A FILM>
Voor het Koninkrijk der Nederlanden
>REFERENCE TO A FILM>
For Kongeriket Norge
>REFERENCE TO A FILM>
Za Rzeczpospolit Na Polsk Na
>REFERENCE TO A FILM>
Pela República Portuguesa
>REFERENCE TO A FILM>
Pentru Rômania
>REFERENCE TO A FILM>
>REFERENCE TO A FILM>
Za Slovenskú republiku
>REFERENCE TO A FILM>
Za Republiko Slovenijo
>REFERENCE TO A FILM>
Por el Reino de España
>REFERENCE TO A FILM>
För Konungariket Sverige
>REFERENCE TO A FILM>
Für die Schweizerische Eidgenossenschaft
Pour la Confédération suisse
Per la Confederazione svizzera
>REFERENCE TO A FILM>
>REFERENCE TO A FILM>
Türkiye Cumhuriyeti adina
>REFERENCE TO A FILM>
>REFERENCE TO A FILM>
For the United Kingdom of Great Britain and Northern Ireland
>REFERENCE TO A FILM>
For the United States of America
>REFERENCE TO A FILM>
