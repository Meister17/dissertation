Commission Regulation (EC) No 1166/2002
of 28 June 2002
amending Regulation (EC) No 174/1999 laying down special detailed rules for the application of Council Regulation (EEC) No 804/68 as regards export licences and export refunds in the case of milk and milk products
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1255/1999 of 17 May 1999 on the common organisation of the market in milk and milk products(1), as last amended by Commission Regulation (EC) No 509/2002(2), and in particular Article 26(3) and Article 31(14) thereof,
Whereas:
(1) Commission Regulation (EC) No 174/1999(3), as last amended by Regulation (EC) No 787/2002(4), lays down special detailed rules for applying Council Regulation (EEC) No 804/68(5), as last amended by Regulation (EC) No 1587/96(6), as regards export licences and export refunds in the case of milk and milk products. In order to ensure that the export refund arrangements are properly managed and reduce the risk of speculation and disturbance in the arrangements for certain milk products, the security set in the above Regulation should be increased.
(2) Article 15 of Regulation (EC) No 174/1999 provides for refunds on exports of cheeses to be differentiated according to destination zone. In view of the trends in the refunds for the various destinations, certain zones can be abolished. In the interests of simplification, certain destination zones should be merged.
(3) Regulation (EC) No 174/1999 should therefore be amended accordingly.
(4) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Milk and Milk Products,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 174/1999 is amended as follows:
1. The first paragraph of Article 9 is replaced by the following: "The security referred to in Article 15(2) of Regulation (EC) No 1291/2000 shall be equal to a percentage of the refund fixed for each product code applicable on the day the export licence application is lodged, as follows:
(a) 10 % for products covered by CN code 0405;
(b) 30 % for products covered by CN code 0402 10;
(c) 30 % for products covered by CN code 0406;
(d) 25 % for other products."
2. Article 15(3) is replaced by the following: "3. The zones referred to in paragraph 1 shall be as follows:
- zone I: destination codes 070 and 091 to 096,
- zone III: destination code 400,
- zone VI: all other destination codes."
Article 2
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 28 June 2002.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 160, 26.6.1999, p. 48.
(2) OJ L 79, 22.3.2002, p. 15.
(3) OJ L 20, 27.1.1999, p. 8.
(4) OJ L 127, 14.5.2002, p. 6.
(5) OJ L 148, 28.6.1968, p. 13.
(6) OJ L 206, 16.8.1996, p. 21.
