Commission Decision
of 24 June 2005
concerning the financing of studies, impact assessments and evaluations covering the areas of food safety, animal health and welfare and zootechnics
(2005/472/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Decision 90/424/EEC of 26 June 1990 on expenditure in the veterinary field [1], and in particular Article 20 thereof,
Whereas:
(1) In accordance with Decision 90/424/EEC of 26 June 1990 on expenditure in the veterinary field, the Community is to undertake or assist the Member States in undertaking the technical and scientific measures necessary for the development of Community veterinary legislation and for the development of veterinary education or training.
(2) Studies, impact assessments as well as systematic and timely evaluations of its expenditure programmes are an established priority for the European Commission (EC), as a means of accounting for the management of allocated funds and as a way of promoting a lesson-learning culture throughout the organisation, particularly in a context of increased focus on results-based management.
(3) In order to carry out these tasks, a call for tender for an evaluation framework contract covering the policy areas of food safety, animal health and welfare and zootechnics has been launched following an open procedure during the last quarter 2004.
(4) This framework contract is expected to provide high quality, timely and relevant information which will serve as a basis for Community decision making.
(5) All individual tasks shall be subject to specific agreements. These agreements shall be signed between the Commission and the selected contractor as defined in the framework contract.
(6) The measure provided for in this Decision is in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS DECIDED AS FOLLOWS:
Sole Article
The actions described in the Annex to this Decision are approved for the purpose of their financing.
Done at Brussels, 24 June 2005.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 224, 18.8.1990, p. 19. Decision as last amended by Regulation (EC) No 806/2003 (OJ L 122, 16.5.2003, p. 1).
--------------------------------------------------
ANNEX
Domain: Food safety, animal health and welfare and zootechnics.
Legal basis: Decision 90/424/EEC of 26 June 1990 on expenditure in the veterinary field.
- various types of studies and other services supporting the design and preparation of Commission proposals,
- ex ante evaluations/ impact assessments,
- interim and ex-post evaluations.
The evaluation of Community Animal Health Policy (CAHP) 1995 to 2004 and possible policy options for the future (including a risk financing model for livestock epidemics) has been targeted as a priority in 2005.
- 17 04 02 — Other measures in the veterinary, animal welfare and public-health field: EUR 500000,
- 17 01 04 04 — Pilot study: risk financing model for livestock epidemics — Expenditure on administrative management: EUR 500000.
Budget: maximum of EUR 1000000 for the first year of the framework contract.
Number of specific actions envisaged: approximately six.
All actions shall be governed by common public procurement rules: in casu use of existing framework contract.
--------------------------------------------------
