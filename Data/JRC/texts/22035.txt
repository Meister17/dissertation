COMMISSION DIRECTIVE 97/32/EC of 11 June 1997 adapting to technical progress Council Directive 77/539/EEC relating to reversing lamps for motor vehicles and their trailers (Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 70/156/EEC of 6 February 1970 on the approximation of the laws of the Member States relating to the type-approval of motor vehicles and their trailers (1), as last amended by European Parliament and Council Directive 96/79/EC (2), and in particular Article 13 (2) thereof,
Having regard to Council Directive 77/539/EEC of 28 June 1977 on the approximation of the laws of the Member States relating to reversing lamps for motor vehicles and their trailers (3), as last amended by the Act of Accession of Austria, Finland and Sweden, and in particular Article 10 thereof,
Whereas Directive 77/539/EEC is one of the separate Directives of the EEC type-approval procedure which has been established by Directive 70/156/EEC; whereas, consequently, the provisions laid down in Directive 70/156/EEC relating to vehicle systems, components and separate technical units apply to this Directive;
Whereas, in particular, Articles 3 (4) and 4 (3) of Directive 70/156/EEC require each separate Directive to have attached to it an information document and also a type-approval certificate based on Annex VI to that Directive in order that type-approval may be computerized; whereas the type approval certificate(s) provided for in Directive 77/539/EEC must be amended accordingly;
Whereas the procedures need to be simplified in order to maintain the equivalence, established by Article 9 (2) of Directive 70/156/EEC, between certain separate Directives and the corresponding regulations of the United Nations' Economic Commission for Europe when the said regulations are amended; whereas, as a first step, the technical requirements of Directive 77/539/EEC need to be replaced by those of Regulation No 23 by way of cross-reference;
Whereas reference is made to Council Directive 76/756/EEC (4), as last amended by Commission Directive 97/28/EC (5), and to Council Directive 76/761/EEC (6), as last amended by the Act of Accession of Austria, Finland and Sweden;
Whereas the measures provided for in this Directive are in accordance with the opinion of the Committee for Adaptation to Technical Progress established by Directive 70/156/EEC,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Directive 77/539/EEC is hereby amended as follows:
1. Article 1 (1) is replaced by the following:
'1. Each Member State shall grant EEC component type-approval for any type of reversing lamp which satisfies the construction and testing requirements laid down in the relevant Annexes.`
2. The first paragraph of Article 2 is replaced by the following:
'Member States shall for each type of reversing lamp which they approve pursuant to Article 1, issue to the manufacturer an EEC component type-approval mark conforming to the model shown in Annex I, Appendix 3.`
3. Article 4 is replaced by the following:
'Article 4
The competent authorities of the Member States shall inform each other, by means of the procedure specified in Article 4 (6) of Directive 70/156/EEC, of each approval which they have granted, refused or withdrawn pursuant to this Directive.`
4. Article 9 is replaced by the following:
'Article 9
For the purposes of this Directive, "vehicle" means any motor vehicle intended for use on the road, with or without bodywork, having at least four wheels and a maximum design speed exceeding 25 km/h, and its trailers, with the exception of vehicles which run on rails and of agricultural and forestry tractors and all mobile machinery.`
5. The Annexes are replaced by the Annex to this Directive.
Article 2
1. From 1 January 1998, or, if publication of the texts referred to in Article 3 is delayed beyond 1 July 1997, six months after the actual date of publication of these texts, Member States may not, on grounds relating to reversing lamps:
- refuse, in respect of a type of vehicle or a type of lamp mentioned above, to grant EC type-approval or national type-approval, or
- prohibit the registration, sale or entry into service of vehicles, or the sale or entry into service of reversing lamps,
provided that the lamps comply with the requirements of Directive 77/539/EEC, as amended by this Directive, and that, as far as vehicles are concerned, they are installed in accordance with the requirements laid down in Directive 76/756/EEC.
2. From 1 October 1998 Member States:
- shall no longer grant EC type-approval, and
- may refuse to grant national type-approval
for any type of vehicle on grounds relating to reversing lamps, and for any type of reversing lamp, if the requirements of Directive 77/539/EEC, as amended by this Directive, are not fulfilled.
3. From 1 October 1999 the requirements of Directive 77/539/EEC relating to reversing lamps as components, as amended by this Directive, shall be applicable for the purposes of Article 7 (2) of Directive 70/156/EEC.
4. Notwithstanding paragraphs 2 and 3 above, for the purposes of replacement parts Member States shall continue to grant EC type-approval of reversing lamps, and to permit their sale and entry into service, in accordance with previous versions of Directive 77/539/EEC provided that such lamps
- are intended to be fitted to vehicles already in use, and
- comply with the requirements of that Directive which were applicable when the vehicles were first registered.
Article 3
The paragraphs and annexes of UN/ECE Regulation No 23 referred to in Annex II, item 2.1, shall be published in the Official Journal of the European Communities before 1 July 1997.
Article 4
1. Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive by 1 January 1998; however, if the publication of the texts referred to in Article 3 is delayed beyond 1 July 1997, the Member States shall comply with this obligation six months after the actual date of publication of these texts. They shall forthwith inform the Commission thereof.
They shall apply those provisions from 1 January 1998, or, if the publication of the texts referred to in Article 3 is delayed beyond 1 July 1997, six months after the actual date of publication of those texts.
When Member States adopt these provisions, these shall contain a reference to this Directive or shall be accompanied by such reference at the time of their official publication. The procedure for such reference shall be adopted by Member States.
2. Member States shall communicate to the Commission the texts of the main provisions of national law which they adopt in the field covered by this Directive.
Article 5
This Directive shall enter into force on the twentieth day following that of its publication in the Official Journal of the European Communities.
Article 6
This Directive is addressed to the Member States.
Done at Brussels, 11 June 1997.
For the Commission
Martin BANGEMANN
Member of the Commission
(1) OJ No L 42, 23. 2. 1970, p. 1.
(2) OJ No L 18, 21. 1. 1997, p. 7.
(3) OJ No L 220, 29. 8. 1977, p. 72.
(4) OJ No L 262, 27. 9. 1976, p. 1.
(5) See p. 1 of this Official Journal.
(6) OJ No L 262, 27. 9. 1976, p. 96.
ANNEX
'LIST OF ANNEXES
ANNEX I: Administrative provisions for type approval
Appendix 1: Information document
Appendix 2: Type-approval certificate
Appendix 3: Models of the EC component type-approval mark
ANNEX II: Scope and technical requirements
ANNEX I
ADMINISTRATIVE PROVISIONS FOR TYPE APPROVAL
1. APPLICATION FOR EC COMPONENT TYPE-APPROVAL
1.1. The application for EC component type-approval pursuant to Article 3 (4) of Directive 70/156/EEC of a type of reversing lamp shall be submitted by the manufacturer.
1.2. A model for the information document is given in Appendix 1.
1.3. The following must be submitted to the technical service responsible for conducting the type-approval tests:
1.3.1. Two samples equipped with the lamp or lamps recommended. If the devices are not identical but are symmetrical and suitable for mounting one on the left and one on the right side of the vehicle, the two samples submitted may be identical and be suitable for mounting only on the right or only on the left side of the vehicle.
2. MARKINGS
2.1. The devices submitted for EC component type-approval must bear:
2.1.1. the trade name or mark of the manufacturer;
2.1.2. if necessary in order to prevent any mistake in mounting the reversing lamp on the vehicle, bear the word 'TOP` marked horizontally on the uppermost part of the illuminating surface;
2.1.3. in the case of lamps with replaceable light sources: the type(s) of filament lamp prescribed;
2.1.4. in the case of lamps with non-replaceable light sources: the nominal voltage and wattage.
2.2. These markings shall be clearly legible and indelible and shall be affixed to the illuminating surface, or to one of the illuminating surfaces, of the device. They shall be visible from the exterior when the device is fitted to the vehicle.
2.3. Each device shall have sufficient space for the component type-approval mark. This space
3. GRANTING OF EC COMPONENT TYPE-APPROVAL
3.1. If the relevant requirements are satisfied, EC type-approval pursuant to Article 4 (3) and, if applicable, 4 (4) of Directive 70/156/EEC shall be granted.
3.2. A model for the EC type-approval certificate is given in Appendix 2.
3.3. An approval number in accordance with Annex VII to Directive 70/156/EEC shall be assigned to each type of reversing lamp approved. The same Member State shall not assign the same number to another type of reversing lamp.
3.4. Where EC component type approval is requested for a type of lighting and light-signalling device comprising a reversing lamp and other lamps, a single EC component type-approval number may be assigned provided that the reversing lamp complies with the requirements of this Directive and that each of the other lamps forming part of the lighting and light-signalling device for which EC component type approval is requested complies with the separate Directive applying to it.
4. EC COMPONENT TYPE-APPROVAL MARK
4.1. In addition to the markings referred to in item 2.1 every reversing lamp conforming to the type approved pursuant to this Directive shall bear an EC component type-approval mark.
4.2. This mark shall consist of:
4.2.1. a rectangle surrounding the letter 'e` followed by the distinguishing number or letters of the Member State which has granted type approval:
>TABLE>
4.2.2. in the vicinity of the rectangle the 'base approval number` contained in section 4 of the type-approval number referred to in Annex VII of Directive 70/156/EEC, preceded by the two figures indicating the sequence number assigned to the most recent major technical amendment to Directive 77/539/EEC on the date EEC type approval was granted. In this Directive the sequence number is 00;
4.2.3. an additional symbol consisting of the letters 'A` and 'R`, mingled as shown in figure 1 of Appendix 3.
4.2.4. On lamps of which the visibility angles are asymmetrical with regard to the reference axis in a horizontal direction, an arrow pointing towards the side on which the photometric specifications are met up to an angle of 45° H.
4.3. The EC component type-approval mark shall be affixed to the lens of the lamp or one of the lenses in such a way as to be indelible and clearly legible even when the lamps are fitted to the vehicle.
4.4. Examples of the EC component type-approval mark are given in Appendix 3, Figure 1.
4.5. Where a single EC component type-approval number is issued, as per item 3.4 above, for a type of lighting and light-signalling device comprising a reversing lamp and other lamps, a single EC component type-approval mark may be affixed, consisting of:
4.5.1. a rectangle surrounding the letter 'e` followed by the distinguishing number or letters of the Member State which has granted type approval (see item 4.2.1);
4.5.2. the base approval number (see item 4.2.2, first half-sentence);
4.5.3. if necessary, the required arrow, in so far as it relates to the lamp assembly as a whole.
4.6. This mark may be located anywhere on the lamps which are grouped, combined or reciprocally incorporated, provided that:
4.6.1. it is visible after the installation of the lamps;
4.6.2. no light-transmitting components of the grouped, combined or reciprocally incorporated lamps can be removed without simultaneously removing the approval mark.
4.7. The identification symbol for each lamp corresponding to each Directive pursuant to which EC component type approval was granted, together with the sequence number (see item 4.2.2, second half-sentence) and, where necessary, the letter 'D` and the required arrow shall be marked:
4.7.1. either on the appropriate light-emitting surface;
4.7.2. or in a group, in such a way that each of the grouped, combined or reciprocally incorporated lamps may be clearly identified.
4.8. The dimensions of the components of this mark must not be less than the minimum dimensions specified for individual marks by the various Directives pursuant to which EC component type approval was granted.
4.9. Examples of an EC component type-approval mark for a lamp that is grouped, combined or reciprocally incorporated with other lamps are given in Appendix 3, Figure 2.
5. MODIFICATIONS OF THE TYPE AND AMENDMENTS TO APPROVALS
5.1. In the case of modifications of the type approved pursuant to this Directive, the provisions of Article 5 of Directive 70/156/EEC shall apply.
6. CONFORMITY OF PRODUCTION
6.1. As a general rule, measures to ensure the conformity of production shall be taken in accordance with the provisions laid down in Article 10 of Directive 70/156/EEC.
6.2. Every reversing lamp shall comply with the photometric conditions specified in paragraphs 6 and 8 (*). Nevertheless, in the case of a reversing lamp selected at random from series production, the requirements as to minimum intensity of the light emitted (measured with a standard filament lamp as referred to in paragraph 7 (*) shall be limited in each relevant direction to 80 % of the minimum value prescribed in paragraph 6 (*),
(*) Of the documents referred to in point 2.1 of Annex II of the present Directive.
Appendix 1
Information document No . . . relating to the EC component type-approval of reversing lamps
>
START OF GRAPHIC>
(Directive 77/539/EEC, as last amended by Directive . . ./. . ./EC)
The following information, if applicable, must be supplied in triplicate and include a list of contents. Any drawings must be supplied in appropriate scale and in sufficient detail on size A4 or on a folder of A4 format. Photographs, if any, must show sufficient detail.
If the systems, components or separate technical units have electronic controls, information concerning their performance must be supplied.
0. GENERAL
0.1. Make (trade name of manufacturer): .
0.2. Type and general commercial description(s): .
0.5. Name and address of manufacturer: .
0.7. In the case of components and separate technical units, .
location and method of affixing of the EC approval mark: .
0.8. Address(es) of assembly plant(s): .
1. DESCRIPTION OF THE DEVICE
1.1. Type of device: .
1.1.1. Device function(s): .
1.1.2. Category or class of device: .
1.2.3. Colour of the light emitted or reflected: .
1.2. Drawing(s) in sufficient detail to permit identification of the type of the device and showing .
1.2.1. in what geometrical position the device is to be mounted on the vehicle (not applicable to rear registration plate lamps): .
1.2.2. the axis of observation to be taken as the axis of refernce in the tests (horizontal angle H = 0°, vertical angle V = 0°) and the point to be taken as the centre of reference in the said tests (not applicable to retro-reflecting devices and rear registration plate lamps): .
1.2.3. the position intended for the EC component type-approval mark: .
1.2.4. for rear registration plate lamps, the geometrical position in which the device is to be fitted in relation to the space to be occupied by the registration plate, and the outline of the area adequately illuminated: .
1.2.5. for headlamps and front fog lamps, a frontal view of the lamps with details of lens ribbing, if any, and the cross section: .
1.3. A brief technical description stating, in particular, with the exception of lamps with non replaceable light sources, the category or categories of light sources prescribed which shall be one or more of those contained in Directive 76/761/EEC (not applicable to retro-reflecting devices):
.
1.4. Specific information .
1.4.1. For rear registration plate lamps, a statement whether the device is intended to illuminate a wide/tall/both wide and tall plate: .
1.4.2. For headlamps, .
1.4.2.1. information whether the headlamp is intended to provide both a dipped beam and main beam or only one of those beams: .
1.4.2.2. information in case the headlamp is intended to provide a dipped beam, if it is designed for both left-hand and right-hand traffic or for either left-hand or right-hand traffic only: .
1.4.2.3. if the headlamp is equipped with an adjustable reflector, an indication of the mounting position(s) of the headlamp in relation to the ground and the longitudinal median plane of the vehicle, if the headlamp is for use in that (those) position(s) only: .
1.4.3. For position lamps, stop lamps and direction indicators, .
1.4.3.1. if the device may also be used in an assembly of two lamps of the same category: .
1.4.3.2. in the case of a device with two levels of intensity (stop lamps and category 2b direction indicators), arrangement diagram and specification of the characteristics of the system ensuring the two levels of intensity: .
1.4.4. For retro-reflecting devices, a brief description giving the technical specifications of the materials of the retro-reflecting optical unit: .
1.4.5. For reversing lamps, a statement whether the device is intended to be installed on a vehicle exclusively in a pair of devices: .
>END OF GRAPHIC>
Appendix 2
MODEL (maximum format: A4 (210 × 297 mm)) EC TYPE-APPROVAL CERTIFICATE
>START OF GRAPHIC>
Stamp of administration
Communication concerning the
- type-approval (1)
- extension of type-approval (1)
- refusal of type-approval (1)
- withdrawal of type-approval (1)
of a type of a vehicle/component/separate technical unit (1) with regard to Directive . . ./. . ./EEC, as last amended by Directive . . ./. . ./EC
Type-approval number: .
Reason for extension: .
SECTION I
0.1. Make (trade name of manufacturer): .
0.2. Type and general commercial description(s): .
0.3. Means of identification of type if marked on the vehicle/component/separate technical unit (1) (2):
.
0.3.1. Location of that marking: .
0.4. Category of vehicle (1) (3): .
0.5. Name and address of manufacturer: .
0.7. In the case of components and separate technical units, location and method of affixing of the EC approval mark: .
0.8. Address(es) of assembly plant(s): .
SECTION II
1. Additional information (where applicable): See Addendum
2. Technical service responsible for carrying out the tests: .
3. Date of test report: .
4. Number of test report: .
5. Remarks (if any): See Addendum
6. Place: .
7. Date: .
8. Signature: .
9. The index to the information package lodged with the approval authority, which may be obtained on request, is attached.
(1) Delete where not applicable.
(2) If the means of identification of type contains characters not relevant to describe the vehicle, component or separate technical unit types covered by this type-approval certificate such characters shall be represented in the documentation by the symbol "?" (e. g. ABC??123???).(3) As defined in Annex II A to Directive 70/156/EEC.Addendum to EC type-approval certificate No . . .
concerning the component type-approval of a lighting and/or light-signalling device with regard to Directive(s) 76/757/EEC; 76/758/EEC; 76/759/EEC; 76/760/EEC; 76/761/EEC; 76/762/EEC; 77/538/EEC; 77/539/EEC and 77/540/EEC (1) as last amended by Directive(s) . . .
1. ADDITIONAL INFORMATION
1.1. Where applicable, indicate for each lamp
1.1.1. The category(ies) of the device(s): .
1.1.2. The number and category of light sources (not applicable to retro reflector) (2) .
1.1.3. The colour of the light emitted or reflected: .
1.1.4. Approval granted solely for use as a replacement part on vehicles already in service: Yes/No (1) .
1.2. Specific information for certain types of lighting or light-signalling devices .
1.2.1. For retro-reflecting devices: In isolation/part of an assembly of devices (1) .
1.2.2. For rear registration plate lamps: Device for illuminating a tall plate/a wide plate (1) .
1.2.3. For headlamps: If equipped with an adjustable reflector, the mounting position(s) of the headlamp in relation to the ground and the longitudinal median plane of the vehicle, if the headlamp is for use in that (those) position(s) only: .
1.2.4. For reversing lamps: This device shall be installed on a vehicle only as part of a pair of devices: Yes/No (1) .
5. REMARKS
5.1. Drawings
5.1.1. For rear registration plate lamps: the attached drawing No . . . shows the geometric position in which the device is to be fitted in relation to the space to be occupied by the registration plate, and the outline of the area adequately illuminated;
5.1.2. For retro-reflecting devices: the attached drawing No . . . shows the geometric position in which the device is to be fitted to the vehicle;
5.1.3. For all other lighting and light signalling devices: the attached drawing No . . . shows the geometric position in which the device is to be fitted to the vehicle, and the axis of reference and centre of reference of the device.
5.2. For headlamps: operating mode used during the test (item 5.2.3.9. of Annex I to Directive 76/761/EEC): .
(1) Delete where not applicable.
(2) For lamps with non replaceable light sources, indicate the number and the total wattage of the light sources.
>END OF GRAPHIC>
Appendix 3
EXAMPLES OF THE EC COMPONENT TYPE-APPROVAL MARK Figure 1
>REFERENCE TO A GRAPHIC>
The device bearing the EC component type-approval mark shown above is a reversing lamp, type approved in Germany (e1) pursuant to this Directive (00) under the base approval number 1471. The arrow indicates the side on which the required photometric specifications are met up to an angle of 45°H.
Figure 2
Simplified marking of grouped, combined or reciprocally incorporated lamps when two or more lamps are part of the same assembly
>REFERENCE TO A GRAPHIC>
MODEL B
>REFERENCE TO A GRAPHIC>
MODEL C
>REFERENCE TO A GRAPHIC>
Note: The three examples of approval marks, models A. B. and C represent three possible variants of the marking of a lighting and light-signalling device when two or more lamps are part of the same unit of grouped, combined or reciprocally incorporated lamps. This approval marks shows that the device was approved in Germany (el) under the base approval number 1712 and comprises:
A retro reflector of class IA approved in accordance with Directive 76/757/EEC, sequence No 02;
A rear direction indicator of category 2a approved in accordance with Counsil Directive 76/759/EEC (OJ No L 262, 27. 9. 1976, p. 32);
A red rear position lamp (R) approved in accordance with Annex II to Council Directive 76/758/EEC (OJ No L 262, 27. 9. 1976, p. 70), sequence No 02;
A rear fog lamp (F) approved in accordance with Council Directive 77/538/EEC (OJ No L 262, 27. 9. 1976, p. 54);
A reversing lamp (AR) approved in accordance with Directive 77/539/EEC, sequence No 00;
A stop lamp with two levels of intensity (S2) approved in accordance with Annex II to Directive 76/758/EEC sequence No 02;
A rear registration plate lamp (L) approved in accordance with Council Directive 76/760/EEC (OJ No L 262, 27. 9. 1976, p. 85), sequence No 00.
ANNEX II
SCOPE AND TECHNICAL REQUIREMENTS
1. SCOPE
This Directive applies to reversing lamps for motor vehicles and their trailers.
2. TECHNICAL REQUIREMENTS
2.1. The technical requirements are those set out in paragraphs 1, 5 to 8 and Annexes 3 and 4 of UN-ECE Regulation No 23 which consists of a consolidation of the following documents:
- the Regulation in its original form (00) incorporating Supplements 1 to 4 to Regulation No 23 and a Correction (1);
- Supplement 5 to Regulation 23 (2).
except that:
2.1.1. Where reference is made to "Regulation No 48", this shall be understood as "Directive 76/756/EEC".
2.1.2. Where reference is made to "Regulation No 37", this shall be understood as "Annex VII to Directive 76/761/EEC".
2.1.3. In paragraph 6.4, penultimate section, the sentence "(see paragraph 2 of this Regulation)" shall be understood as "(see Appendix 1 of Annex I to this Directive)".
2.1.4. In paragraph 6.4, last section, the sentence "a statement in paragraph 11 "Comments" of the Communication form (see Annex I to this Regulation)" shall be understood as "a statement in the Addendum to the type-approval certificate (see Appendix 2 of Annex I to this Directive)".
>TABLE>
Technical requirements of Regulation No 23 of the United Nations' Economic Commission for Europe referred to in Article 3 and in Annex II, item 2.1 of Commission Directive 97/32/EC (1) adapting to technical progress Council Directive 77/539/EEC relating to reversing lamps for motor vehicles and their trailers
1. DEFINITIONS
For the purposes of this Regulation,
1.1. 'reversing lamp` means the lamp of the vehicle designed to illuminate the road to the rear of the vehicle and to warn other road users that the vehicle is reversing or about to reverse;
1.2. The definitions given in Regulation No 48 and its series of amendments in force at the time of application for type approval shall apply to this Regulation;
1.3. reversing lamps of different 'types` are reversing lamps which differ in such essential respects as,
1.3.1. the trade name or mark;
1.3.2. the characteristics of the optical system;
1.3.3. the inclusion of components capable of altering the optical effects by reflection, refraction or absorption; and
1.3.4. the category of filament lamp.
5. GENERAL SPECIFICATIONS
5.1. Each sample shall conform to the specifications set forth in the paragraphs below.
5.2. Reversing lamps shall be so designed and constructed that in normal use, despite the vibration to which they may then be subjected, they continue to function satisfactorily and retain the characteristics prescribed by this Regulation.
6. INTENSITY OF LIGHT EMITTED
6.1. The intensity of the light emitted by each of the two samples shall be not less than the minima and not greater than the maxima specified below and shall be measured in relation to the axis of reference in the directions shown below (expressed in degrees of angle with the axis of reference).
6.2. The intensity along the axis of reference shall be not less than 80 candelas.
6.3. The intensity of the light emitted in all directions in which the light can be observed shall not exceed
300 candelas in directions in or above the horizontal plane; or
600 candelas in directions below the horizontal plane.
6.4. In every other direction of measurement shown in annex 3 to this Regulation the luminous intensity shall be not less than the minima specified in that annex.
However, in the case where the reversing lamp is intended to be installed on a vehicle exclusively in a pair of devices, the photometric intensity may be verified only up to an angle of 30° inwards where a photometric value of at least 25 cd shall be satisfied.
This condition shall be clearly explained in the application for approval and relating documents (see paragraph 2 of this Regulation).
Moreover, in the case where the type approval will be granted applying the condition above, a statement in paragraph 11 'Comments` of the communication form (see annex 1 to this Regulation), will inform that the device shall only be installed in a pair.
6.5. In the case of a single lamp containing more than one light source, the lamp shall comply with the minimum intensity required when any one light source has failed and when all light sources are illuminated the maximum intensities shall not be exceeded.
7. TEST PROCEDURE
7.1. All measurements shall be carried out with uncoloured standard filament lamps of the types prescribed for the device, adjusted to produce the normal luminous flux prescribed for those types of filament lamps.
7.1.1. All measurements on lamps equipped with non-replaceable light sources (filament lamps and other) shall be made at 6.75 V, 13.5 V or 28.0 V respectively.
In the case of light sources supplied by a special power supply, the above test voltages shall be applied to the input terminals of that power supply. The test laboratory may require from the manufacturer the special power supply needed to supply the light sources.
8. COLOUR OF LIGHT EMITTED
The colour of the light emitted shall be white. In case of doubt, the colour may be checked on the basis of the definition of the colour of white light given in annex 4 to this Regulation.
(1) OJ No L 171, 30. 6. 1997, p. 63.
ANNEX 3
Photometric measurements
1. MEASUREMENT METHODS
1.1. When photometric measurements are taken, stray reflections shall be avoided by appropriate masking.
1.2. In the event that the results of measurements are challenged, measurements shall be taken in such a way as to meet the following requirements:
1.2.1. the distance of measurement shall be such that the law of the inverse of the square of the distance is applicable;
1.2.2. the measuring equipment shall be such that the angle subtended by the receiver from the reference centre of the light is between 10' and 1°;
1.2.3. the intensity requirement for a particular direction of observation shall be satisfied if the required intensity is obtained in a direction deviating by not more than one quarter of a degree from the direction of observation.
2. MEASURING POINTS EXPRESSED IN DEGREES OF ANGLE WITH THE AXIS OF REFERENCE AND VALUES OF THE MINIMUM INTENSITIES OF THE LIGHT EMITTED
TOP
>REFERENCE TO A GRAPHIC>
2.1. The directions H = 0° and V = 0° correspond to the axis of reference. On the vehicle they are horizontal, parallel to the median longitudinal plane of the vehicle and oriented in the required direction of visibility. They pass through the centre of reference. The values shown in the table give, for the various directions of measurement, the minimum intensities in cd.
2.2. If visual examination of a lamp appears to reveal substantial local variations of intensity, a check shall be made to ensure that no intensity measured between two of the directions of measurement referred to above is below 50 % of the lower minimum intensity of the two prescribed for these directions of measurement.
3. PHOTOMETRIC MEASUREMENT OF LAMPS EQUIPPED WITH SEVERAL LIGHT SOURCES
The photometric performance shall be checked:
3.1. For non-replaceable light sources (filament lamps and other): with the light sources present in the lamp, in accordance with paragraph 7.1.1. of this Regulation.
3.2. For replaceable filament lamps:
when equipped with mass production filament lamps at 6.75 V, 13.5 V or 28.0 V the luminous intensity values produced shall lie between the maximum limit given in this Regulation and the minimum limit of this Regulation increased according to the permissible deviation of the luminous flux permitted for the type of filament lamp chosen, as stated in Regulation No 37 for production filament lamps; alternatively a standard filament lamp may be used in turn, in each of the individual positions, operated at its reference flux, the individual measurements in each position being added together.
ANNEX 4
COLOUR OF WHITE LIGHT (Trichromatic coordinates)
>TABLE>
For checking these colorimetric characteristics, a source of light at a colour temperature of 2,854°K corresponding to illuminant A of the International Commission on Illumination (ICI) shall be used.
However, for lamps equipped with non-replaceable light sources (filament lamps and other), the colorimetric characteristics should be verified with the light sources present in the lamp, in accordance with paragraph 7.1.1. of this Regulation.
