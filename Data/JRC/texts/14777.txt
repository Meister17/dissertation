Action brought on 19 February 2006 — Cofira SAC v Commission of the European Communities
Parties
Applicant: Cofira SAC (Rousset Cedex, France) (represented by: Girolamo Addessi, Leonilda Mari, Daniella Magurno, lawyers)
Defendant: Commission of the European Communities
Form of order sought
The applicant(s) claim(s) that the Court should:
- annul the fine imposed on Cofira SAC;
- impose the fines jointly and severally on all the companies that came into existence upon the demerger of Cofira Sepso;
- reduce the amount of the fine;
- order the Commission to pay the costs.
Pleas in law and main arguments
Article 1 of the contested decision states that certain undertakings, including the applicant, infringed Community competition rules during the period from 24 March 1982 to 26 June 2002 by participating in agreements and concerted practices in the industrial plastic bag sector in Belgium, Germany, Spain, Luxembourg and the Netherlands. According to the defendant, the purpose of those infringements was to fix prices, implement common models, calculate prices, share markets, allocate sales quotas, customers, business and orders, collude in undermining certain invitations to tender and exchange individual information.
In support of its claims, the applicant maintains first and foremost that the decision should not have been addressed to it.
In this regard, it is pointed out that on 27 November 2003 COFIRA SEPSO, which, along with other concerns, was investigated, was split into three companies, one of them being the applicant. COFIRA SAC therefore came into existence after the occurrence of the events which gave rise to the imposition of penalties by the Commission.
The contested decision does not even state the grounds on which the fine was imposed on the applicant alone, when all of the companies which came into existence as a result of the demerger of COFIRA SEPSO should have been required to answer for the wrongful acts alleged.
Nor does the decision state the basis on which the total amount of the fine was calculated, bearing in mind the fact that fines are commensurate to turnover and that at the time of the alleged events the applicant did not have any turnover as it did not exist.
Furthermore, the Commission does not set out the elements of fact which constituted the infringement. In fact, the whole decision is based on the assumption that the meetings between the representatives of the companies amounted, subsequently and in fact, to conduct contrary to Article 81 EC, and that such practices had a significant impact on competition. However, even if the facts relied on by the Commission were accepted, the fifteen year limitation period has expired.
--------------------------------------------------
