Council Decision
of 13 December 2004
amending Decision 2002/463/EC adopting an action programme for administrative cooperation in the fields of external borders, visas, asylum and immigration (ARGO programme)
(2004/867/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 66 thereof,
Having regard to the proposal from the Commission,
Having regard to the Opinion of the European Parliament [1],
Having regard to the Opinion of the European Economic and Social Committee [2],
After consulting the Committee of the Regions,
Whereas:
(1) The principal objective of Decision 2002/463/EC [3] is to promote administrative cooperation in the fields of external borders, visas, asylum and immigration.
(2) In its Communication to the European Parliament and to the Council of 3 June 2003 on the development of a Common policy on illegal immigration, smuggling and trafficking of human beings, external borders and the return of illegal residents the Commission puts forward the possibility of revising the ARGO programme for providing financial support to national projects in the area of external borders by addressing specific structural weaknesses at strategic border points that would be identified in agreement with the Member States on the basis of objective criteria.
(3) The European Council of Thessaloniki invited the Commission to examine the possibility of appropriating funds under heading 3 of the financial perspectives in order to address, during the period 2004-2006, the most pressing structural needs and to cover a wider definition of solidarity that would include, inter alia, Community support in the management of external borders.
(4) The budgetary authority [4] has substantially increased the appropriations allocated to the ARGO programme for 2004, with a view to better management of the external borders.
(5) In order to promote the general objectives of the ARGO programme it is appropriate to increase the number of actions to be proposed in the area of external borders and to envisage new types of action.
(6) In order to make it more accessible to national administrations, Member States should be able to propose actions which do not necessarily involve other Member States, but which contribute to the general objectives and support the activities in the area of external borders as set out in Decision No 2002/463/EC.
(7) An indicative ceiling should be set on the available appropriations earmarked for actions by a single Member State.
(8) The provisions on the consultation of the ARGO Committee in Decision No 2002/463/EC must be aligned with the new Financial Regulation applicable to the general budget of the European Union [5].
(9) In accordance with Articles 1 and 2 of the Protocol on the position of Denmark annexed to the Treaty on European Union and to the Treaty establishing the European Community, Denmark is not participating in the adoption of this Decision, and is therefore not bound by it or subject to its application.
(10) In accordance with Article 3 of the Protocol on the position of the United Kingdom and Ireland annexed to the Treaty on the European Union and to the Treaty establishing the European Community, the United Kingdom has notified its wish to take part in the adoption and application of the present Decision.
(11) In accordance with Article 1 of the Protocol on the position of the United Kingdom and Ireland annexed to the Treaty on European Union and to the Treaty establishing the European Community, Ireland is not participating in the adoption of this Decision. As a result, and without prejudice to Article 4 of the said Protocol, the provisions of this Decision do not apply to Ireland.
(12) Decision No 2002/463/EC should therefore be amended accordingly,
HAS ADOPTED THIS DECISION:
Article 1
Decision 2002/463/EC is amended as follows:
1. In Article 10 the following paragraph is inserted:
"1a. Actions referred to in Article 8 and proposed by a national agency of one Member State which implement one of the activities in the policy area referred to in Article 4, shall be eligible for co-financing under the ARGO programme, provided they:
(a) pursue one of the general objectives referred to in Article 3; and
(b) contribute to integrated border management by addressing specific structural weaknesses at strategic border points, identified on the basis of objective criteria.".
2. Article 11 is amended as follows:
(a) In paragraphs 3, 4 and 6 "Article 10(1)" is replaced by "Article 10(1) and (1a)".;
(b) Paragraph 5 is replaced by the following:
"(5) The decisions to award community grants to the actions referred to in Article 10(1) and (1a) shall be subject to grant agreements between the Commission and the national agencies proposing the actions. The implementation of these award decisions and agreements shall be subject to financial control of the Commission and to audits by the Court of Auditors.".
3. Article 12 is amended as follows:
(a) Paragraph 3(a) is replaced by the following:
"(a) prepare an annual work programme comprising specific objectives, thematic priorities, the objective criteria referred to in Article 10(1a)(b), definition of the indicative maximum share of the annual budget available for the actions referred to in Article 10(1a), a description of the actions referred to in Article 10(3) which the Commission intends to undertake and, if necessary, a list of other actions.";
(b) Paragraph 4 is replaced by the following:
"4. The annual work programme including the actions proposed by the Commission and the specific actions provided for in Article 9 shall be adopted according to the procedure referred to in Article 13(2).
As regards the actions referred to in Article 10(1) and (1a), the list of selected actions shall be adopted according to the procedure referred to in Article 13(3).".
Article 2
This decision shall apply from the date of its publication in the Official Journal of the European Union.
Article 3
This Decision is addressed to the Member States in accordance with the Treaty establishing the European Community.
Done at Brussels, 13 December 2004.
For the Council
The President
B. R. Bot
--------------------------------------------------
[1] Opinion delivered on 17.11.2004 (not yet published in the Official Journal).
[2] Opinion delivered on 27.10.2004 (not yet published in the Official Journal).
[3] OJ L 161, 19.6.2002, p. 11.
[4] General budget of the EU for the financial year 2004 (OJ L 53, 23.2.2004).
[5] OJ L 248, 16.9.2002, p. 1.
