Commission Decision
of 14 December 2004
amending for the third time Decision 2004/122/EC concerning certain protection measures in relation to avian influenza in several Asian countries
(notified under document number C(2004) 4775)
(Text with EEA relevance)
(2004/851/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/496/EEC of 15 July 1991 laying down the principles governing the organisation of veterinary checks on animals entering the Community from third countries and amending Directives 89/662/EEC, 90/425/EEC and 90/675/EEC [1], and in particular Article 18(7) thereof,
Having regard to Council Directive 97/78/EC of 18 December 1997 laying down the principles governing the organisation of veterinary checks on products entering the Community from third countries [2], and in particular Article 22(6) thereof,
Whereas:
(1) By Decision 2004/122/EC [3] the Commission adopted protection measures in relation to avian influenza in several Asian countries, namely in Cambodia, Indonesia, Japan, Laos, Pakistan, the People’s Republic of China including the territory of Hong Kong, South Korea, Thailand and Vietnam.
(2) On 19 August 2004 Malaysia has reported an outbreak of avian influenza and therefore the Commission has adopted Decision 2004/606/EC amending for the second time Decision 2004/122/EC in order to apply protection measures also to Malaysia.
(3) Given the disease situation in most countries of the region, in particular with respect to the continuing outbreaks of avian influenza in Malaysia, Thailand, Vietnam, the People’s Republic of China and Indonesia, it is necessary to further prolong the protection measures in place.
(4) At present some of the countries listed above have not reported any further outbreaks; therefore their disease status in relation to avian influenza should be reviewed before December 2004.
(5) The western peninsula of Malaysia is listed in Commission Decision 94/85/EC [4] and it is therefore necessary to restrict the importation of table eggs, non-treated game trophies, raw pet food and unprocessed feed materials containing any parts of poultry from this region in Malaysia.
(6) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
Decision 2004/122/EC is amended as follows:
1. in Article 3 the words "and Malaysia" shall be inserted after the words "South Korea";
2. in Article 7 the date of " 15 December 2004" is replaced by " 31 March 2005".
Article 2
The Member States shall amend the measures they apply to imports so as to bring them into compliance with this Decision and they shall give immediate appropriate publicity to the measures adopted. They shall immediately inform the Commission thereof.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 14 December 2004.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 268, 24.9.1991, p. 56. Directive as last amended by the 2003 Act of Accession.
[2] OJ L 24, 30.1.1998, p. 9. Directive as last amended by Regulation (EC) No 882/2004 of the European Parliament and of the Council (OJ L 165, 30.4.2004, p. 1).
[3] OJ L 36, 7.2.2004, p. 59. Decision as last amended by Decision 2004/606/EC (OJ L 273, 21.8.2004, p. 21).
[4] OJ L 44, 17.2.1994, p. 31. Decision as last amended by Decision 2004/118/EC (OJ L 36, 7.2.2004, p. 34).
--------------------------------------------------
