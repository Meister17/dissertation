Council Regulation (EC) No 2528/2001
of 17 December 2001
on the conclusion of the Protocol setting out the fishing opportunities and the financial contribution provided for in the Agreement on cooperation in the sea fisheries sector between the European Community and the Islamic Republic of Mauritania for the period 1 August 2001 to 31 July 2006
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 37 in conjunction with Article 300(2) and the first subparagraph of Article 300(3) thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament(1),
Whereas:
(1) In accordance with the Agreement on cooperation in the sea fisheries sector between the European Community and the Islamic Republic of Mauritania(2), the two Parties have held negotiations to determine any amendments or additions to be made to the Agreement.
(2) As a result of those negotiations, a new Protocol setting out the fishing opportunities and the financial contribution provided for in the above Agreement for the period 1 August 2001 to 31 July 2006 was initialled on 31 July 2001.
(3) It is in the Community's interest to approve this Protocol.
(4) The method for allocating the fishing opportunities among the Member States should be defined,
HAS ADOPTED THIS REGULATION:
Article 1
The Protocol setting out the fishing opportunities and the financial contribution provided for in the Agreement on cooperation in the sea fisheries sector between the European Community and the Islamic Republic of Mauritania for the period 1 August 2001 to 31 July 2006 is hereby approved on behalf of the Community.
The text of the Protocol is attached to this Regulation(3).
Article 2
The fishing opportunities set out in the Protocol shall be allocated among the Member States as follows:
>TABLE>
If licence applications from these Member States do not cover all the fishing opportunities fixed by the Protocol, the Commission may take into consideration licence applications from any other Member State.
Article 3
The Member States whose vessels are fishing under the Protocol shall notify the Commission of the quantities of each stock taken in Mauritania's fishing zone in accordance with the arrangements laid down in Commission Regulation (EC) No 500/2001 of 14 March 2001 laying down detailed rules for the application of Council Regulation (EEC) No 2847/93 on the monitoring of catches taken by Community fishing vessels in third country waters and on the high seas(4).
Article 4
The President of the Council is hereby authorised to designate the persons empowered to sign the Protocol in order to bind the Community.
Article 5
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 17 December 2001.
For the Council
The President
A. Neyts-Uyttebroeck
(1) Opinion delivered on 13 December 2001 (not yet published in the Official Journal).
(2) OJ L 334, 23.12.1996, p. 20.
(3) See page 125 of this Official Journal.
(4) OJ L 73, 15.3.2001, p. 8.
