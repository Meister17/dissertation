EU25 ethyl alcohol balance for 2005
(established on 15 May 2006 In accordance with Article 2 of Regulation (EC) No 2336/2003)
(2006/C 158/04)
Sources: Communications from the Member States/Eurostat COMEXT
| EU25 ethyl alcohol balance [1] for 2005 established on 15 May 2006 In accordance with Article 2 of Regulation (EC) No 2336/2003 [2] | Hectolitres of pure alcohol |
1. | Initial stock | 9855008 |
—Agricultural origin |
—Non-agricultural origin |
2. | Production | 27621612 |
—Agricultural origin | 22621074 |
—Non-agricultural origin | 5000538 |
3. | Imports | 5974522 |
—0 % duty | 1535160 |
—Reduced duty | 1738446 |
—100 % duty | 2700916 |
4. | Total resources | 43451142 |
5. | Exports | 584141 |
6. | Internal consumption | 31223724 |
| Agricultural origin | Non-agricultural origin | Total |
Food use | 7352275 | 0 | 7352275 |
Industrial use | 6229751 | 5800265 | 12030016 |
Fuel | 9792141 | 71427 | 9863568 |
Other | 1838510 | 139355 | 1977865 |
7. | Final Stock | 11643277 |
—Agricultural origin |
—Non-agricultural origin |
[1] Includes only products falling within CN codes 220710, 220720, 22089091 and 22089099.
[2] Commission Regulation (EC) No 2336/2003 of 30 December 2003 introducing certain detailed rules for applying Council Regulation (EC) No 670/2003 laying down specific measures concerning the market in ethyl alcohol of agricultural origin. OJ L 346, 31.12.2003, p. 19 — 25
--------------------------------------------------
