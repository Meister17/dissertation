Commission Directive 2002/28/EC
of 19 March 2002
amending certain annexes to Council Directive 2000/29/EC on protective measures against the introduction into the Community of organisms harmful to plants or plant products and against their spread within the Community
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 2000/29/EC of 8 May 2000 on protective measures against the introduction into the Community of organisms harmful to plants or plant products and against their spread within the Community(1), as last amended by Commission Directive 2001/33/EC(2) and in particular Article 14(c) thereof,
Having regard to the agreement of the Member States concerned,
Whereas:
(1) From information supplied by the United Kingdom based on updated surveys it appears that the protected zone recognised for Dendroctonus micans Kugelan in the United Kingdom should be modified.
(2) From information supplied by the United Kingdom on the presence of beet necrotic yellow vein virus it appears that it is no longer appropriate to maintain the protected zone for the whole of the United Kingdom in respect of beet necrotic yellow vein virus but should be restricted to Northern Ireland only.
(3) From information supplied by Italy the description of the protected zones in respect of Erwinia amylovora (Burr.) Winsl. et al. should be modified to take into account the present distribution of the organism.
(4) The description of the protected zones relating to host plants of Erwinia amylovora (Burr.) Winsl. et al. as regards the special requirements to be met should be modified to take into account the present distribution of the organism.
(5) From information supplied by France on the presence of Matsucoccus feytaudi Duc. it appears that it is no longer appropriate to maintain the protected zone for this organism.
(6) Therefore, Directive 2000/29/EC should be amended accordingly.
(7) The measures provided for in this Directive are in accordance with the opinion of the Standing Committee on Plant Health,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Annexes I, II, III and IV to Directive 2000/29/EC are amended in accordance with the Annex to this Directive.
Article 2
1. Member States shall adopt and publish, by 31 March 2002 at the latest, the laws, regulations and administrative provisions necessary to comply with this Directive. They shall forthwith inform the Commission thereof.
They shall apply those provisions from 1 April 2002.
When Member States adopt those provisions, they shall contain a reference to this Directive or shall be accompanied by such reference on the occasion of their official publication. Member States shall determine how such a reference is to be made.
2. Member States shall immediately communicate to the Commission the text of the main provisions of national law which they adopt in the field covered by this Directive. The Commission shall inform the other Member States thereof.
Article 3
This Directive shall enter into force on the third day following that of its publication in the Official Journal of the European Communities.
Article 4
This Directive is addressed to the Member States.
Done at Brussels, 19 March 2002.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 169, 10.7.2000, p. 1.
(2) OJ L 127, 9.5.2001, p. 42.
ANNEX
1. In Annex I, Part B(b)(1), in the right-hand column "UK" is replaced by "UK (Northern Ireland)".
2. Annex II, Part B is amended as follows:(a) under heading (a), in point 3, the entry in the third column is replaced by the following: >TABLE>;
(b) under heading (a), point 7 is deleted;
(c) under heading (b), in point 2, the entry in the third column is replaced by the following: >TABLE>;
3. In Annex III, Part B(b)(1), the right-hand column is replaced by the following: >TABLE>;
4. Annex IV, Part B is amended as follows:(a) in points 1, 7, and 14.1, the entry in the third column is replaced by the following: >TABLE>;
(b) points 6.2, 14.7 are deleted;
(c) in points 20.1, 20.2, 22, 23, 25.1, 25.2, 26, 27.1, 27.2, and 30 in the third column "UK" is replaced by "UK (Northern Ireland)";
(d) in point 21, the entry in the second column at (a) is replaced by the following: >TABLE>;
(e) in point 21, the entry in the third column is replaced by the following: >TABLE>.
