Commission Regulation (EC) No 2135/2004
of 14 December 2004
prohibiting fishing for Northern prawn by vessels flying the flag of Poland
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Regulation (EEC) No 2847/93 of 12 October 1993 establishing a control system applicable to the common fisheries policy [1], as last amended by Regulation (EC) No 1954/2003 [2], and in particular Article 21 (3) thereof,
Whereas:
(1) Council Regulation (EC) No 2287/2003 of 19 December 2003 fixing for 2004 the fishing opportunities and associated conditions for certain fish stocks and groups of fish stocks, applicable in Community waters and, for Community vessels, in waters where catch limitations are required lays down quotas for Northern prawn for 2004 [3].
(2) In order to ensure compliance with the provisions relating to the quantity limits on catches of stocks subject to quotas, the Commission must fix the date by which catches made by vessels flying the flag of a Member State are deemed to have exhausted the quota allocated.
(3) According to the information received by the Commission, catches of Northern prawn in the waters of NAFO division 3L by vessels flying the flag of Poland or registered in Poland have exhausted the quota allocated for 2004.
HAS ADOPTED THIS REGULATION:
Article 1
Catches of Northern prawn in the waters of NAFO division 3L by vessels flying the flag of Poland or registered in Poland are hereby deemed to have exhausted the quota allocated to Poland for 2004.
Fishing for Northern prawn in the waters of NAFO division 3L by vessels flying the flag of Poland or registered in Poland is hereby prohibited, as are the retention on board, transhipment and landing of this stock caught by the above vessels after the date of application of this Regulation.
Article 2
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 14 December 2004.
For the Commission
Joe Borg
Member of the Commission
[1] OJ No L 261, 20.10.1993, p. 1.
[2] OJ No L 289, 7.11.2003, p. 1.
[3] OJ No L 344, 31.12.2003, p. 1.
--------------------------------------------------
