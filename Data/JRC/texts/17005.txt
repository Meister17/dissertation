Corrigendum to Commission Regulation (EC) No 2259/2004 of 28 December 2004 fixing the Community selling price for the fishery products listed in Annex II to Council Regulation (EC) No 104/2000 for the 2005 fishing year
(Official Journal of the European Union L 389 of 30 December 2004)
--------------------------------------------------
[1] OJ L 369, 16.12.2004, p. 1."
