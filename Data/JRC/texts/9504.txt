Reference for a preliminary ruling from the Unabhängiger Verwaltungssenat im Land Niederösterreich (Austria) lodged on 10 August 2006 — Gottfried Heinrich
Referring court
Unabhängiger Verwaltungssenat im Land Niederösterreich
Parties to the main proceedings
Applicant: Gottfried Heinrich
Questions referred
1. Do documents within the meaning of Article 2(3) of Regulation (EC) No 1049/2001 of the European Parliament and of the Council of 30 May 2001 regarding public access to European Parliament, Council and Commission documents [1] include acts which are required to be published in the Official Journal of the European Union pursuant to Article 254 EC?
2. Do regulations or parts thereof have binding force if, contrary to the requirement of Article 254(2) EC, they are not published in the Official Journal of the European Union?
[1] OJ L 145, p. 43.
--------------------------------------------------
