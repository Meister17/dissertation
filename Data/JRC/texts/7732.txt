*****
COUNCIL DIRECTIVE
of 16 December 1986
on the legal protection of topographies of semiconductor products
(87/54/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community and in particular Article 100 thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas semiconductor products are playing an increasingly important role in a broad range of industries and semiconductor technology can accordingly be considered as being of fundamental importance for the Community's industrial development;
Whereas the functions of semiconductor products depend in large part on the topographies of such products and whereas the development of such topographies requires the investment of considerable resources, human, technical and financial, while topographies of such products can be copied at a fraction of the cost needed to develop them independently;
Whereas topographies of semiconductor products are at present not clearly protected in all Member States by existing legislation and such protection, where it exists, has different attributes;
Whereas certain existing differences in the legal protection of semiconductor products offered by the laws of the Member States have direct and negative effects on the functioning of the common market as regards semiconductor products and such differences could well become greater as Member States introduce new legislation on this subject;
Whereas existing differences having such effects need to be removed and new ones having a negative effect on the common market prevented from arising;
Whereas, in relation to extension of protection to persons outside the Community, Member States should be free to act on their own behalf in so far as Community decisions have not been taken within a limited period of time;
Whereas the Community's legal framework on the protection of topographies of semiconductor products can, in the first instance, be limited to certain basic principles by provisions specifying whom and what should be protected, the exclusive rights on which protected persons should be able to rely to authorize or prohibit certain acts, exceptions to these rights and for how long the protection should last;
Whereas other matters can for the time being be decided in accordance with national law, in particular, whether registration or deposit is required as a condition for protection and, subject to an exclusion of licences granted for the sole reason that a certain period of time has elapsed, whether and on what conditions non-voluntary licences may be granted in respect of protected topographies;
Whereas protection of topographies of semiconductor products in accordance with this Directive should be without prejudice to the application of some other forms of protection;
Whereas further measures concerning the legal protection of topographies of semiconductor products in the Community can be considered at a later stage, if necessary, while the application of common basic principles by all Member States in accordance with the provisions of this Directive is an urgent necessity,
HAS ADOPTED THIS DIRECTIVE:
CHAPTER 1
Definitions
Article 1
1. For the purposes of this Directive:
(a) a 'semiconductor product' shall mean the final or an intermediate form of any product:
(i) consisting of a body of material which includes a layer of semiconducting material; and
(ii) having one or more other layers composed of conducting, insulating or semiconducting material, the layers being arranged in accordance with a predetermined three-dimensional pattern; and
(iii) intended to perform, exclusively or together with other functions, an electronic function;
(b) the 'topography' of a semiconductor product shall mean a series of related images, however fixed or encoded;
(i) representing the three-dimensional pattern of the layers of which a semiconductor product is composed; and
(ii) in which series, each image has the pattern or part of the pattern of a surface of the semiconductor product at any stage of its manufacture;
(c) 'commercial exploitation' means the sale, rental, leasing or any other method of commercial distribution, or an offer for these purposes. However, for the purposes of Articles 3 (4), 4 (1), 7 (1), (3) and (4) 'commercial exploitation' shall not include exploitation under conditions of confidentiality to the extent that no further distribution to third parties occurs, except where exploitation of a topography takes place under conditions of confidentiality required by a measure taken in conformity with Article 223 (1) (b) of the Treaty.
2. The Council acting by qualified majority on a proposal from the Commission, may amend paragraph 1 (a) (i) and (ii) in order to adapt these provisions in the light of technical progress.
CHAPTER 2
Protection of topographies of semiconductor products
Article 2
1. Member States shall protect the topographies of semiconductor products by adopting legislative provisions conferring exclusive rights in accordance with the provisions of the Directive.
2. The topography of a semiconductor product shall be protected in so far as it satisfies the conditions that it is the result of its creator's own intellectual effort and is not commonplace in the semiconductor industry. Where the topography of a semiconductor product consists of elements that are commonplace in the semiconductor industry, it shall be protected only to the extent that the combination of such elements, taken as a whole, fulfils the abovementioned conditions.
Article 3
1. Subject to paragraphs 2 to 5, the right to protection shall apply in favour of persons who are the creators of the topographies of semiconductor products.
2. Member States may provide that,
(a) where a topography is created in the course of the creator's employment, the right to protection shall apply in favour of the creator's employer unless the terms of employment provide to the contrary;
(b) where a topography is created under a contract other than a contract of employment, the right to protection shall apply in favour of a party to the contract by whom the topography has been commissioned, unless the contract provides to the contrary.
3. (a) As regards the persons referred to in paragraph 1, the right to protection shall apply in favour of natural persons who are nationals of a Member State or who have their habitual residence on the territory of a Member State.
(b) Where Member States make provision in accordance with paragraph 2, the right to protection shall apply in favour of:
(i) natural persons who are nationals of a Member State or who have their habitual residence on the territory of a Member State;
(ii) companies or other legal persons which have a real and effective industrial or commercial establishment on the territory of a Member State.
4. Where no right to protection exists in accordance with other provisions of this Article, the right to protection shall also apply in favour of the persons referred to in paragraph 3 (b) (i) and (ii) who:
(a) first commercially exploit within a Member State a topography which has not yet been exploited commercially anywhere in the world; and
(b) have been exclusively authorized to exploit commercially the topography throughout the Community by the person entitled to dispose of it.
5. The right to protection shall also apply in favour of the successors in title of the persons mentioned in paragraphs 1 to 4.
6. Subject to paragraph 7, Member States may negotiate and conclude agreements or understandings with third States and multilateral Conventions concerning the legal protection of topographies of semiconductor products whilst respecting Community law and in particular the rules laid down in this Directive.
7. Member States may enter into negotiations which third States with a view to extending the right to protection to persons who do not benefit from the right to protection according to the provisions of this Directive. Member States who enter into such negotiations shall inform the Commission thereof.
When a Member State wishes to extend protection to persons who otherwise do not benefit from the right to protection according to the provisions of this Directive or to conclude an agreement or understanding on the extension of protection with a non-Member State it shall notify the Commission. The Commission shall inform the other Member States thereof. The Member State shall hold the extension of protection or the conclusion of the agreement or understanding in abeyance for one month from the date on which it notifies the Commission. However, if within that period the Commission notifies the Member State concerned of its intention to submit a proposal to the Council for all Member States to extend protection in respect of the persons or non-Member State concerned, the Member State shall hold the extension of protection or the conclusion of the agreement or undestanding in abeyance for a period of two months from the date of the notification by the Member State.
Where, before the end of this two-month period, the Commission submits such a proposal to the Council, the Member State shall hold the extension of protection or the conclusion of the agreement or understanding in abeyance for a further period of four months from the date on which the proposal was submitted.
In the absence of a Commission notification or proposal or a Council decision within the time limits prescribed above, the Member State may extend protection or conclude the agreement or understanding.
A proposal by the Commission to extend protection, whether or not it is made following a notification by a Member State in accordance with the preceding paragraphs shall be adopted by the Council acting by qualified majority.
A Decision of the Council on the basis of a Commission proposal shall not prevent a Member State from extending protection to persons, in addition to those to benefit from protection in all Member States, who were included in the envisaged extension, agreement or understanding as notified, unless the Council acting by qualified majority has decided otherwise.
8. Commission proposals and Council decisions pursuant to paragraph 7 shall be published for information in the Official Journal of the European Communities.
Article 4
1. Member States may provide that the exclusive rights conferred in conformity with Article 2 shall not come into existence or shall no longer apply to the topography of a semiconductor product unless an application for registration in due form has been filed with a public authority within two years of its first commercial exploitation. Member States may require in addition to such registration that material identifying or exemplifying the topography or any combination thereof has been deposited with a public authority, as well as a statement as to the date of first commercial exploitation of the topography where it precedes the date of the applciation for registration.
2. Member States shall ensure that material deposited in conformity with paragraph 1 is not made available to the public where it is a trade secret. This provision shall be without prejudice to the disclosure of such material pursuant to an order of a court or other competent authority to persons involved in litigation concerning the validity or infringement of the excusive rights referred to in Article 2.
3. Member States may require that transfers of rights in protected topographies be registered.
4. Member States may subject registration and deposit in accordance with paragraphs 1 and 3 to the payment of fees not exceeding their administrative costs.
5. Conditions prescribing the fulfilment of additional formalities for obtaining or maintaining protection shall not be admitted.
6. Member States which require registration shall provide for legal remedies in favour of a person having the right to protection in accordance with the provisions of this Directive who can prove that another person has applied for or obtained the registration of a topography without his authorization.
Article 5
1. The exclusive rights referred to in Article 2 shall include the rights to authorize or prohibit any of the following acts:
(a) reproduction of a topography in so far as it is protected under Article 2 (2);
(b) commercial exploitation or the importation for that purpose of a topography or of a semiconductor product manufactured by using the topography.
2. Notwithstanding paragraph 1, a Member State pay permit the reproduction of a topography privately for non commercial aims.
3. The exclusive rights referred to in paragraph 1 (a) shall not apply to reproduction for the purpose of analyzing, evaluating or teaching the concepts, processes, systems or techniques embodied in the topography or the topography itself.
4. The exclusive rights referred to in paragraph 1 shall not extend to any such act in relation to a topography meeting the requirements of Article 2 (2) and created on the basis of an analysis and evaluation of another topography, carried out in conformity with paragraph 3.
5. The exclusive rights to authorize or prohibit the acts specified in paragraph 1 (b) shall not apply to any such act committed after the topography or the semiconductor product has been put on the market in a Member State by the person entitled to authorize its marketing or with his consent. 6. A person who, when he acquires a semiconductor product, does not know, or has no reasonable grounds to believe, that the product is protected by an exclusive right conferred by a Member State in conformity with this Directive shall not be prevented from commercially exploiting that product.
However, for acts committed after that person knows, or has reasonable grounds to believe, that the semiconductor product is so protected, Member States shall ensure that on the demand of the rightholder a tribunal may require, in accordance with the provisions of the national law applicable, the payment of adequate remuneration.
7. The provisions of paragraph 6 shall apply to the successors in title of the person referred to in the first sentence of that paragraph.
Article 6
Member States shall not subject the exclusive rights referred to in Article 2 to licences granted, for the sole reason that a certain period of time has elapsed, automatically, and by operation of law.
Article 7
1. Member States shall provide that the exclusive rights referred to in Article 2 shall come into existence:
(a) where registration is the condition for the coming into existence of the exclusive rights in accordance with Article 4, on the earlier of the following dates:
(i) the date when the topography is first commercially exploited anywhere in the world;
(ii) the date when an application or registration has been filed in due form; or
(b) when the topography is first commercially exploited anywhere in the world; or
(c) when the topography is first fixed or encoded.
2. Where the exclusive rights come into existence in accordance with paragraph 1 (a) or (b), the Member States shall provide, for the period prior to those rights coming into existence, legal remedies in favour of a person having the right to protection in accordance with the provisions of this Directive who can prove that another person has fraudulently reproduced or commercially exploited or imported for that purpose a topography. This paragraph shall be without prejudice to legal remedies made available to enforce the exclusive rights conferred in conformity with Article 2.
3. The exclusive rights shall come to an end 10 years from the end of the calendar year in which the topography is first commercially exploited anywhere in the world or, where registration is a condition for the coming into existence or continuing application of the exclusive rights, 10 years from the earlier of the following dates:
(a) the end of the calendar year in which the topography is first commercially exploited anywhere in the world;
(b) the end of the calendar year in which the application for registration has been filed in due form.
4. Where a topography has not been commercially exploited anywhere in the world within a period of 15 years from its first fixation or encoding, any exclusive rights in existence pursuant to paragraph 1 shall come to an end and no new exclusive rights shall come into existence unless an application for registration in due form has been filed within that period in those Member States where registration is a condition for the coming into existence or continuing application of the exclusive rights.
Article 8
The protection granted to the topographies of semiconductor products in accordance with Article 2 shall not extend to any concept, process, system, technique or encoded information embodied in the topography other than the topography itself.
Article 9
Where the legislation of Member States provides that semiconductor products manufactured using protected topographies may carry an indication, the indication to be used shall be a capital T as follows: T, 'T ', [ T ] , T , T * or T .
CHAPTER 3
Continued application of other legal provisions
Article 10
1. The provisions of this Directive shall be without prejudice to legal provisions concerning patent and utility model rights.
2. The provisions of this Directive shall be without prejudice:
(a) to rights conferred by the Member States in fulfilment of their obligations under international agreements, including provisions extending such rights to nationals of, or residents in, the territory of the Member State concerned;
(b) to the law of copyright in Member States, restricting the reproduction of drawing or other artistic representations of topographies by copying them in two dimensions.
3. Protection granted by national law to topographies of semiconductor products fixed or encoded before the entry into force of the national provisions enacting the Directive, but no later than the date set out in Article 11 (1), shall not be affected by the provisions of this Directive. CHAPTER 4
Final provisions
Article 11
1. Member States shall bring into force the laws, regulations or administrative provisions necessary to comply with this Directive by 7 November 1987.
2. Member States shall ensure that they communicate to the Commission the texts of the main provisions of national law which they adopt in the field covered by this Directive.
Article 12
This Directive is addressed to the Member States.
Done at Brussels, 16 December 1986.
For the Council
The President
G. HOWE
(1) OJ No C 360, 31. 12. 1985, p. 14.
(2) OJ No C 255, 13. 10. 1986, p. 249.
(3) OJ No C 189, 28. 7. 1986, p. 5.
