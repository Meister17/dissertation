Regulation (EC) No 1644/2003 of the European Parliament and of the Council
of 22 July 2003
amending Regulation (EC) No 1406/2002 establishing a European Maritime Safety Agency
THE EUROPEAN PARLIAMENT AND THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 80(2) thereof,
Having regard to the proposal from the Commission(1),
Having regard to the opinion of the Court of Auditors(2),
Having regard to the opinion of the European Economic and Social Committee(3),
Acting in accordance with the procedure laid down in Article 251 of the Treaty(4),
Whereas:
(1) Certain provisions of Regulation (EC) No 1406/2002 of the European Parliament and of the Council of 27 June 2002 setting up a European Maritime Safety Agency(5) should be brought into line with Council Regulation (EC, Euratom) No 1605/2002 of 25 June 2002 on the Financial Regulation applicable to the general budget of the European Communities(6), (hereinafter referred to as the general Financial Regulation), and in particular Article 185 thereof.
(2) The general principles and limits governing the exercise of the right of access to documents provided for in Article 255 of the Treaty have been laid down by Regulation (EC) No 1049/2001 of the European Parliament and of the Council of 30 May 2001 regarding public access to European Parliament, Council and Commission documents(7).
(3) When Regulation (EC) No 1049/2001 was adopted, the three institutions agreed in a joint declaration that the agencies and similar bodies should have rules which conform to those of that Regulation.
(4) Appropriate provisions should therefore be included in Regulation (EC) No 1406/2002 to make Regulation (EC) No 1049/2001 applicable to the European Maritime Safety Agency, as should a provision on appeals against a refusal of access to documents.
(5) Regulation (EC) No 1406/2002 should therefore be amended accordingly,
HAVE ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 1406/2002 is hereby amended as follows:
1. in Article 4:
(a) Paragraph 1 shall be replaced by the following:
"1. Regulation (EC) No 1049/2001 of the European Parliament and of the Council of 30 May 2001 regarding public access to European Parliament, Council and Commission documents(8) shall apply to documents held by the Agency.";
(b) Paragraph 3 shall be replaced by the following:
"3. The Administrative Board shall adopt the practical arrangements for implementing Regulation (EC) No 1049/2001 within six months after the entry into force of Regulation (EC) No 1644/2003 of the European Parliament and of the Council of 22 July 2003 amending Regulation (EC) No 1406/2002 establishing a European Maritime Safety Agency(9).";
(c) The following paragraph shall be added:
"5. Decisions taken by the Agency pursuant to Article 8 of Regulation (EC) No 1049/2001 may form the subject of a complaint to the Ombudsman or of an action before the Court of Justice of the European Communities, under Articles 195 and 230 of the EC Treaty respectively.";
2. Article 10(2)(b) shall be replaced by the following:
"(b) shall adopt the annual report on the Agency's activities and forward it by 15 June at the latest to the European Parliament, the Council, the Commission, the Court of Auditors and the Member States.
The Agency shall annually forward to the budgetary authority all information relevant to the outcome of the evaluation procedures.";
3. in Article 18:
(a) Paragraphs (3), (4), (5) and (6) shall be replaced by the following:
"3. The Executive Director shall draw up a draft statement of estimates of the Agency's revenue and expenditure for the following year and shall forward it to the Administrative Board, together with a draft establishment plan.
4. Revenue and expenditure shall be in balance.
5. Each year the Administrative Board, on the basis of a draft statement of estimates of revenue and expenditure, shall produce a statement of estimates of revenue and expenditure for the Agency for the following financial year.
6. This statement of estimates, which shall include a draft establishment plan together with the provisional work programme, shall by 31 March at the latest be forwarded by the Administrative Board to the Commission and to the States with which the Community has concluded agreements in accordance with Article 17.";
(b) the following paragraphs shall be added:
"7. The statement of estimates shall be forwarded by the Commission to the European Parliament and the Council (hereinafter referred to as the budgetary authority) together with the preliminary draft general budget of the European Union.
8. On the basis of the statement of estimates, the Commission shall enter in the preliminary draft general budget of the European Union the estimates it deems necessary for the establishment plan and the amount of the subsidy to be charged to the general budget, which it shall place before the budgetary authority in accordance with Article 272 of the Treaty.
9. The budgetary authority shall authorise the appropriations for the subsidy to the Agency.
The budgetary authority shall adopt the establishment plan for the Agency.
10. The budget shall be adopted by the Administrative Board. It shall become final following final adoption of the general budget of the European Union. Where appropriate, it shall be adjusted accordingly.
11. The Administrative Board shall, as soon as possible, notify the budgetary authority of its intention to implement any project which may have significant financial implications for the funding of the budget, in particular any projects relating to property such as the rental or purchase of buildings. It shall inform the Commission thereof.
Where a branch of the budgetary authority has notified its intention to deliver an opinion, it shall forward its opinion to the Administrative Board within a period of six weeks after the date of notification of the project.";
4. Article 19 shall be replaced by the following,
"Article 19
Implementation and control of the budget
1. The Executive Director shall implement the Agency's budget.
2. By 1 March at the latest following each financial year, the Agency's accounting officer shall communicate the provisional accounts to the Commission's accounting officer together with a report on the budgetary and financial management for that financial year. The Commission's accounting officer shall consolidate the provisional accounts of the institutions and decentralised bodies in accordance with Article 128 of the general Financial Regulation.
3. By 31 March at the latest following each financial year, the Commission's accounting officer shall forward the Agency's provisional accounts to the Court of Auditors, together with a report on the budgetary and financial management for that financial year. The report on the budgetary and financial management for the financial year shall also be forwarded to the European Parliament and the Council.
4. On receipt of the Court of Auditors' observations on the Agency's provisional accounts, under Article 129 of the general Financial Regulation, the Executive Director shall draw up the Agency's final accounts under his own responsibility and submit them to the Administrative Board for an opinion.
5. The Administrative Board shall deliver an opinion on the Agency's final accounts.
6. The Executive Director shall, by 1 July at the latest following each financial year, forward the final accounts to the European Parliament, the Council, the Commission and the Court of Auditors, together with the Administrative Board's opinion.
7. The final accounts shall be published.
8. The Executive Director shall send the Court of Auditors a reply to its observations by 30 September at the latest. He shall also send this reply to the Administrative Board.
9. The Executive Director shall submit to the European Parliament, at the latter's request, all information necessary for the smooth application of the discharge procedure for the financial year in question, as laid down in Article 146(3) of the general Financial Regulation.
10. The European Parliament, on a recommendation from the Council acting by a qualified majority, shall, before 30 April of year N + 2, give a discharge to the Executive Director in respect of the implementation of the budget for year N.";
5. Article 21 shall be replaced by the following:
"Article 21
Financial provisions
The financial rules applicable to the Agency shall be adopted by the Administrative Board after the Commission has been consulted. They may not depart from Commission Regulation (EC, Euratom) No 2343/2002 of 19 November 2002 on the framework Financial Regulation for the bodies referred to in Article 185 of Council Regulation (EC, Euratom) No 1605/2002 on the Financial Regulation applicable to the general budget of the European Communities(10) unless such a departure is specifically required for the Agency's operation and the Commission has given its prior consent."
Article 2
This Regulation shall enter into force on the first day of the month following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 22 July 2003.
For the European Parliament
The President
P. Cox
For the Council
The President
G. Alemanno
(1) OJ C 331 E, 31.12.2002, p. 87.
(2) OJ C 285, 21.11.2002, p. 4.
(3) OJ C 85, 8.4.2003, p. 64.
(4) Opinion of the European Parliament of 22 October 2002 (not yet published in the Official Journal), Council Common Position of 3 June 2003 (not yet published in the Official Journal) and Decision of the European Parliament of 1 July 2003.
(5) OJ L 208, 5.8.2002, p. 1.
(6) OJ L 248, 16.9.2002, p. 1; corrigendum in OJ L 25, 30.1.2003, p. 43.
(7) OJ L 145, 31.5.2001, p. 43.
(8) OJ L 145, 31.5.2001, p. 43.
(9) OJ L 245, 29.9.2003, p. 10.
(10) OJ L 357, 31.12.2002, p. 72; corrigendum in OJ L 2, 7.1.2003, p. 39.
