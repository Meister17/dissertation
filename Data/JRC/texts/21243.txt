Decision No 2045/2002/EC of the European Parliament and of the Council
of 21 October 2002
amending Decision No 1720/1999/EC adopting a series of actions and measures in order to ensure interoperability of and access to trans-European networks for the electronic interchange of data between administrations (IDA)
THE EUROPEAN PARLIAMENT AND THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 156 thereof,
Having regard to the proposal from the Commission(1),
Having regard to the opinion of the Economic and Social Committee(2),
After consulting the Committee of the Regions,
Acting in accordance with the procedure laid down in Article 251 of the Treaty(3),
Whereas:
(1) The objective of Decision No 1720/1999/EC of the European Parliament and of the Council(4) is for the Community to achieve a high degree of interoperability between the trans-European telematic networks established between the Member States and the Community institutions in order to support the establishment of economic and monetary union and the implementation of Community policies, as well as to achieve substantial benefits for Member State administrations and the Community by streamlining operations and speeding up implementation of new networks and enhancements.
(2) The benefits of trans-European telematic networks for administrations should be extended to citizens and enterprises of the Community, notably in those areas where this contributes to the objectives of the eEurope initiative and related action plan, in particular the chapter on Government online.
(3) Due account should be taken of the recommendations contained in the Declaration issued at the Ministerial Conference on eGovernment "From Policy to Practice" held in Brussels on 29 and 30 November 2001, as well as the conclusions of the Conference "eGovernment in the service of European citizens and enterprises - what is required at the European level", organised jointly by the Presidency of the Council and the Commission (IDA) in Stockholm/Sandhamn on 13 and 14 June 2001.
(4) Regarding the spread of best practice, conferences, workshops and other types of events should be organised in order to ensure general awareness of the achievements and benefits of the IDA projects and actions and encourage a broad discussion on the future direction and priorities of the IDA programme.
(5) For the implementation of the Community Actions set out in Articles 3 to 10 of Decision No 1720/1999/EC it should be clarified that proposals for any budgetary increase of more than EUR 250000 per project line within a year are subject to the procedure referred to in that Decision.
(6) Following the interest expressed by Malta and Turkey, the IDA programme may be opened to participation by these countries in the horizontal actions and measures under Decision No 1720/1999/EC. Before the IDA programme has been opened up for full participation to all candidate countries, the possibility for these countries to use at their own cost IDA generic services in order to implement a Community policy should be facilitated. This possibility should also be afforded to other non-member countries, under the same conditions.
(7) With a view to creating more flexibility in the annual budget allocation, a financial reference amount for the implementation of the Community action under Decision No 1720/1999/EC for the period 2002-2004 should be introduced, with the annual appropriations being authorised by the budgetary authority within the limit of the financial perspective.
(8) The measures necessary for the implementation of this Decision should be adopted in accordance with Council Decision 1999/468/EC of 28 June 1999 laying down the procedures for the exercise of implementing powers conferred on the Commission(5).
(9) Decision No 1720/1999/EC should be amended accordingly,
HAVE DECIDED AS FOLLOWS:
Article 1
Decision No 1720/1999/EC is hereby amended as follows:
1. In Article 1(1):
(a) point (d) shall be replaced by the following:
"(d) the extension of the benefits of such networks, as mentioned in point (c), to Community industry and citizens of the European Union, notably in those areas where this contributes to the objectives of the eEurope initiative and related action plan, in particular the chapter on Government online;"
(b) the following point shall be added:
"(f) where appropriate, the identification and deployment of pan-European electronic government services for citizens and enterprises and other relevant electronic government services to be used in line with the priorities laid down in Article 4 of Decision No 1719/1999/EC of the European Parliament and of the Council of 12 July 1999 on a series of guidelines, including the identification of projects of common interest, for trans-European networks for the electronic interchange of data between administrations (IDA)(6)."
2. The following paragraph shall be inserted in Article 3:
"1a. Where appropriate, in order to be able to identify the horizontal actions and measures to be undertaken, the Community shall establish a description of an infrastructure, which shall serve as a platform for the development of projects of common interest, as well as other sectoral networks, referred to in Decision 1719/1999/EC.
The described infrastructure shall comprise a framework of interoperability for networks, services, security, applications, contents and other relevant elements. It may also include aspects such as the required management, organisation, responsibilities and cost-sharing. The description shall also encompass a strategy to be used in the development and implementation of the infrastructure. The description shall be reviewed on a yearly basis."
3. The following paragraph shall be added to Article 10:
"3. The Community shall organise conferences, workshops and other types of events in order to ensure general awareness of the achievements and benefits of the IDA projects and actions and encourage a broad discussion on the future direction and priorities of the IDA programme."
4. Article 11(2), (3) and (4) shall be replaced by the following:
"2. The section of the IDA work programme concerning the implementation of this Decision, which the Commission shall draw up for its entire duration and which shall be reviewed at least twice a year, shall be approved, on the basis of its compliance with the relevant provisions of Articles 3 to 10, in accordance with the procedure referred to in Article 12(2).
3. The common rules and procedures for bringing about technical and administrative interoperability shall be adopted in accordance with the procedure referred to in Article 12(2).
4. The procedure referred to in Article 12(2) shall also apply in respect of the approval of the breakdown of the yearly budgetary expenditure under this Decision. Proposals for any budgetary increase of more than EUR 250000 per project line within a year shall also be subject to that procedure."
5. Article 12 shall be replaced by the following:
"Article 12
Committee
1. The Commission shall be assisted by a committee called the Telematics between Administrations Committee (TAC), composed of representatives of the Member States and chaired by the representative of the Commission.
2. Where reference is made to this paragraph, Articles 4 and 7 of Council Decision 1999/468/EC(7) shall apply, having regard to Article 8 thereof.
The period laid down in Article 4(3) of Decision 1999/468/EC shall be set at three months.
3. The TAC shall adopt its rules of procedure.
4. The Commission shall report annually to the TAC on the implementation of this Decision."
6. Article 14 shall be replaced by the following:
"Article 14
Extension to the EEA and associated countries
1. The IDA programme may be opened, within the framework of their respective agreements with the European Community, to participation by the countries of the European Economic Area and the associated countries of central and eastern Europe, Cyprus, Malta and Turkey in the horizontal actions and measures under this Decision.
2. In the course of implementing this Decision, cooperation with non-member countries and with international organisations or bodies, as appropriate, shall be encouraged.
3. Before the IDA programme has been opened up for their full participation, the associated countries of central and eastern Europe, Cyprus, Malta and Turkey may, at their own cost, use IDA generic services, in order to implement a Community policy.
4. Other non-member countries may also, at their own cost, use IDA generic services in order to implement a Community policy."
7. Article 15 shall be replaced by the following:
"Article 15
Financial reference amount
1. The financial reference amount for the implementation of the Community action under this Decision for the period 2002-2004 shall be EUR 34,2 million.
2. The annual appropriations shall be authorised by the budgetary authority within the limit of the financial perspective."
Article 2
This Decision shall enter into force on the day of its publication in the Official Journal of the European Communities.
Done at Brussels, 21 October 2002.
For the European Parliament
The President
P. Cox
For the Council
The President
P. S. Møller
(1) OJ C 332 E, 27.11.2001, p. 290.
(2) OJ C 80, 3.4.2002, p. 21.
(3) Opinion of the European Parliament of 11 June 2002 (not yet published in the Official Journal) and Decision of the Council of 23 September 2002.
(4) OJ L 203, 3.8.1999, p. 9.
(5) OJ L 184, 17.7.1999, p. 23.
(6) OJ L 203, 3.8.1999, p. 1.
(7) OJ L 184, 17.7.1999, p. 23.
