REGULATION (EEC) No 2411/75 OF THE COUNCIL of 16 September 1975 on the conclusion of the Agreement between the European Economic Community and the United Mexican States
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Articles 113 and 114 thereof;
Having regard to the recommendation from the Commission;
Whereas the Agreement negotiated between the European Economic Community and the United Mexican States should be concluded,
HAS ADOPTED THIS REGULATION:
Article 1
The Agreement between the European Economic Community and the United Mexican States, the text of which is annexed to this Regulation, is hereby concluded on behalf of the Community.
Article 2
The President of the Council shall notify the other Contracting Party in accordance with Article 12 of the Agreement of the completion, as regards the Community, of the procedures necessary for the entry into force of this Agreement.
Article 3
The Community shall be represented on the Joint Committee provided for in Article 6 of the Agreement by the Commission of the European Communities, assisted by representatives of the Member States.
Article 4
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 16 September 1975.
For the Council
The President
M. RUMOR
