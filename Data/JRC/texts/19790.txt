Commission Regulation (EC) No 497/2003
of 18 March 2003
amending Regulation (EC) No 94/2002 laying down detailed rules for applying Council Regulation (EC) No 2826/2000 on information and promotion actions for agricultural products on the internal market
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 2826/2000 of 19 December 2000 on information and promotion actions for agricultural products on the internal market(1), and in particular Article 12 thereof,
Whereas:
(1) With a view to providing correct information to and protecting consumers, Article 2 of Commission Regulation (EC) No 94/2002(2), as last amended by Regulation (EC) No 2097/2002(3), should provide that nutritional messages are to be based on recognised scientific data, the sources of which are available.
(2) In the interests of legal certainty, it should be specified that the proposed programmes must comply, in particular, with all the Community legislation covering the products concerned and their marketing.
(3) In the light of both the experience gained from the scrutiny of the programmes presented and developments in scientific knowledge, the guidelines should be adapted for some sectors.
(4) Olive oil, table olives and beef and veal are key sectors where market balance could be improved through information and/or generic promotion measures, in particular by providing adequate information to consumers.
(5) In the fibre-flax sector, high quality products must face stiff competition from flax originating outside the Community and from other types of fibre. The sector's results from the last marketing year show that information measures on Community fibre flax have had a positive impact.
(6) Olive oil, table olives, fibre flax and beef and veal should therefore be included in the list of products to be promoted, and general guidelines on campaigns should be laid down already at this stage for the purpose of managing the programmes to be implemented for these sectors.
(7) The guidelines in the Annex hereto take account of the market situation and the available results from the evaluation of the most recent promotion campaign.
(8) In view of the date on which these guidelines were adopted, specific deadlines should be fixed for the transmission and approval of programmes submitted in 2003 for olive oil, table olives and fibre flax.
(9) Annex II, which lists the competent bodies of the Member States, needs updating.
(10) Regulation (EC) No 94/2002 should be amended accordingly.
(11) The measures provided for in this Regulation are in accordance with the opinion delivered at the joint meeting of the management committees on agricultural product promotion,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 94/2002 is hereby amended as follows:
- In Article 2, the following paragraph 3 is added:
"3. Any reference in promotion and information messages to the effects on health of consuming the products concerned must be based on generally-accepted scientific data. Such messages must be accepted by the national authority responsible for public health. The trade federation or interbranch organisation making the proposal shall keep a list available for the Member State concerned and the Commission of the scientific studies and the opinions of authorised scientific institutions on which such messages are based."
- Article 5(1) is replaced by the following:
"1. For the purpose of implementing measures under the programmes referred to in Article 6 of Regulation (EC) No 2826/2000, the Member State concerned shall receive, in response to a call for proposals and no later than 31 January and 31 July each year, programmes drawn up by trade and interbranch associations in the Community which are representative of the sector or sectors concerned.
However, the Member States concerned shall receive programmes to be submitted in 2003 relating to olive oil, table olives and fibre flax in response to a call for proposals no later than 31 May 2003.
Such programmes shall comply with the Community rules governing the products concerned and their marketing, and the guidelines referred to in Article 5 of Regulation (EC) No 2826/2000 and the specifications stipulating exclusion, selection and award criteria distributed to that end by the Member States concerned.
The guidelines are laid down for the first time in Annex III to this Regulation."
- Article 7 is amended as follows:
(a) the following subparagraph is added to paragraph 1:"However, the programmes submitted in 2003 relating to olive oil, table olives and fibre flax in response to a call for proposals shall be notified to the Commission no later than 30 June 2003.";
(b) the following subparagraph is added to paragraph 3:"However, the Commission shall decide on programmes submitted in 2003 relating to olive oil, table olives and fibre flax in response to a call for proposals no later than 15 September 2003."
- Annex I is replaced by Annex I hereto.
- Annex II is replaced by Annex II hereto.
- Annex III is amended in accordance with Annex III to this Regulation.
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 18 March 2003.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 328, 21.12.2000, p. 2.
(2) OJ L 17, 19.1.2002, p. 20.
(3) OJ L 323, 28.11.2002, p. 41.
ANNEX I
(a) List of themes for which information and/or promotion measures may be carried out:
- information about protected designations of origin (PDOs), protected geographical indications (PGIs), guaranteed traditional specialities (GTSs) and the graphic symbols laid down in agricultural legislation,
- information about organic farming,
- information about agricultural production systems that guarantee product traceability and the labelling of such products,
- information on the quality and safety of food and nutritional and health aspects.
(b) List of products which may be covered by campaigns:
- fresh fruit and vegetables,
- processed fruit and vegetables,
- milk products,
- quality wines psr, table wines with a geographical indication,
- olive oil and table olives,
- flowers and live plants,
- fibre flax,
- beef and veal, fresh, chilled or frozen.
ANNEX II
LIST OF COMPETENT BODIES IN THE MEMBER STATES
(for administering Regulations (EC) No 2702/1999 and (EC) No 2826/2000)
>TABLE>
ANNEX III
Milk and milk products
1. OVERALL ANALYSIS OF THE SITUATION
There has been a drop in the consumption of liquid milk, particularly in the major consumer countries, mainly due to competition from soft drinks among young people. By contrast, there is an overall increase in the consumption of milk products expressed in milk equivalent.
2. GOALS
- To increase liquid milk consumption
- To consolidate consumption of milk products
- To encourage consumption by young people.
3. TARGET GROUPS
- Children and adolescents, especially girls aged eight to 14
- Young women and mothers aged 15 to 40
- People aged over 55.
4. MAIN MESSAGES
- Milk and milk products are healthy, natural, high-energy products suited to modern living and enjoyable to consume.
- Messages must be positive and take account of the specific nature of consumption on the different markets.
- The continuity of the main messages must be ensured during the entire programme in order to convince consumers of the benefits to be had from regularly consuming these products.
5. MAIN CHANNELS
- Electronic channels
- Telephone information lines
- PR contacts with the media (e.g. specialised journalists for women's and young people's press)
- Contacts with doctors and nutritionists
- Contacts with teachers
- Other channels (leaflets and booklets, children's games, etc.)
- Demonstration at sales points
- Visual media (cinema, specialised TV channels)
- Radio spots
- Publicity in the specialised press (young people and women).
6. DURATION OF PROGRAMMES
12 to 36 months, giving priority to multi-annual programmes that set targets for each stage.
7. INDICATIVE BUDGET
EUR 6 million.
Wine
1. OVERALL ANALYSIS OF THE SITUATION
Wine production is ample while consumption is static or even in decline for certain types of wine, and supply from third countries is on the increase.
2. GOALS
To inform consumers about the variety, quality and production conditions of European wines and the results of scientific studies.
3. TARGET GROUPS
Consumers, excluding young people and adolescents as referred to in Council Recommendation 2001/458(1).
4. MAIN MESSAGES
- Community legislation strictly regulates production, quality indications, labelling and marketing, so guaranteeing for consumers the quality and traceability of the wine on offer.
- The attraction of being able to choose from an extremely wide selection of European wines of different origins, highlighting the growing of European wines and its link to the local area.
- Drinking wine in moderation in conjunction with a balanced diet.
5. MAIN CHANNELS
Information and public relations measures:
- training for distributors and caterers,
- contacts with the medical profession and specialised press,
- other channels (Internet site, leaflets and brochures) to guide consumers in their choice and create opportunities for consumption at family events.
6. DURATION OF PROGRAMMES
12 to 36 months, giving priority to multi-annual programmes that set objectives for each stage.
7. INDICATIVE BUDGET
EUR 6 million.
Fresh fruit and vegetables
1. OVERALL ANALYSIS OF THE SITUATION
In this sector, the need to ensure a steady and regular disposal of supply is more pronounced for some products, regardless of the communication measures taken hitherto.
There is a noticeable lack of interest among consumers under 35, which is even stronger among the school-age population. This is not in the interests of a balanced diet.
2. GOALS
The aim is to restore the image of the products as being "fresh" and "natural" and to bring down the average age of consumers, chiefly by encouraging young people to consume the products concerned.
3. TARGET GROUPS
- Young households under 35
- School-age children and adolescents
- Mass caterers and school canteens
- Doctors and nutritionists.
4. MAIN MESSAGES
- The products are natural
- The products are fresh
- Quality (safety, nutritional value and taste, production methods, environmental protection, link with the product's origin)
- Enjoyment
- Balanced diet
- Variety and seasonal nature of supply of fresh products
- Ease of preparation: fresh foods require no cooking
- Traceability
- Consuming these products is good for the health.
5. MAIN CHANNELS
- Electronic channels (Internet sites presenting the products, with games for young people)
- Telephone information lines
- PR contacts with the media (e.g. specialised journalists, women's press, youth magazines and papers)
- Contacts with doctors and nutritionists
- Educational measures targeting children and adolescents by mobilising teachers and school canteen managers
- Other channels (leaflets and brochures with information on the products and recipes, children's games, etc.)
- Visual media (cinema, specialised TV channels)
- Radio spots
- Publicity in the specialised press (young people and women).
6. DURATION OF PROGRAMMES
12 to 36 months, giving priority to multi-annual programmes that set targets for each stage.
7. INDICATIVE BUDGET
EUR 6 million.
Olive oil and table olives
1. OVERALL ANALYSIS OF THE SITUATION
While the supply of olive oil and table olives is on the increase, demand for these products varies widely between traditional consumer markets and those where they are a relatively new phenomenon.
In the traditional consumer Member States (Spain, Italy, Greece and Portugal), the products concerned are generally well-known and consumption is high. In other words, these are mature markets where there is little prospect of any overall increase in demand.
In the "new consumer" Member States, per capita consumption is still low and many consumers do not yet know about the different qualities and uses of olive oil and table olives. This is the market where there is major scope for increasing demand.
2. GOALS
- To increase consumption in the "new consumer" Member States by increasing market penetration and diversifying the use of these products.
- To consolidate and, where possible, increase consumption in the traditional consumer Member States by improving information to consumers about aspects which remain little known and getting young people into the habit of buying the products.
3. TARGET GROUPS
(a) In the "new consumer" Member States:
- Chief purchasers
- Opinion formers (gastronomes, chefs, restaurants, general and specialised press (gastronomy, women's, various styles)
- Distributors.
(b) In the "traditional consumer" Member States:
- Chief purchasers aged 20 to 35
- Consumer press
- Medical and paramedical press.
4. MAIN MESSAGES
(a) In the "new consumer" Member States:
- olive oil, in particular extra virgin oil, is the natural product of an ancient tradition and know-how ideally suited to a highly-flavoured modern cuisine,
- tips for daily use (hot and cold) throughout the year,
- the different quality categories and variety of flavours of olive oil,
- the organoleptic characteristics of virgin olive oil (aroma, colour, taste) vary subtly between varieties, areas, harvests, PDOs/PGIs, etc. This diversity offers a wide range of culinary sensations and possibilities,
- the nutritional qualities of olive oil make it an important part of a balanced and health diet,
- information on the quality control, certification and labelling system for olive oil,
- table olives are a natural and healthy product, which can be eaten as snacks or in complex dishes.
(b) In the "traditional consumer" Member States:
- the different categories of olive oil and the specific features of virgin oils,
- the meaning and advantages of the Community PDO/PGI system and information about all the olive oils and/or table olives registered as PDOs/PGIs throughout the Community,
- information on the quality control, certification and labelling system for olive oil,
- despite its long history, olive oil is a modern product with both nutritional and culinary value,
- the characteristics of the different varieties of table olives.
Without prejudice to Article 2(3), information on the nutritional qualities of olive oil must be based, in particular, on the documentation produced for the seventh olive oil promotional campaign and validated by the Commission's scientific assistant.
5. MAIN CHANNELS
(a) In the "new consumer" Member States:
- The Internet
- Promotion at points of sale (tasting, recipes, information)
- Publicity (or publicity editorials) in the general, culinary, women's and lifestyle press
- Public relations with opinion formers (specialised journalists, chefs, etc.).
(b) In the "traditional consumer" Member States:
- The Internet
- Publicity (or publicity editorials) in the specialised press (women's, culinary, etc.) focusing on updating the product's image
- Promotion at points of sale
- Public relations and PR contacts with the press (events, participation in consumer shows, etc.)
- Joint actions taken with the medical and paramedical profession.
6. DURATION OF PROGRAMMES
12 to 36 months, giving priority to programmes that set a properly reasoned strategy and objectives for each stage.
However, pending all the results of the external evaluation of the seventh campaign, programmes approved in 2003 will last for 12 months.
7. INDICATIVE BUDGET
EUR 6 million, broken down as follows:
- 70 % for programmes to be implemented in one or more "new consumer" Member States,
- 30 % for programmes to be implemented in one or more "traditional consumer" Member States.
Fibre flax
1. OVERALL ANALYSIS OF THE SITUATION
The liberalisation of international trade in textiles and clothing has brought European flax into contact with sharp competition with flax from outside the Community, offered at very attractive prices, and other fibres. At the same time, consumption of textiles is tending to stabilise.
2. GOALS
- To develop the image and reputation of European flax
- To increase consumption of this product, as identified by the "Masters of Linen" mark
- To capitalise on the distinctive qualities of European flax, as identified by the mark
- To inform consumers about the characteristics of new products placed on the market.
3. TARGET GROUPS
- Prescribers (stylists, designers, makers, editors)
- Distributors
- Textiles, fashion and interior design education circles (teachers and students)
- Opinion leaders
- Consumers.
4. MAIN MESSAGES
- The quality of the product comes from the conditions in which the raw material is produced, the use of suitable varieties and the know-how brought to bear all along the production chain.
- European flax offers a wide range of products (clothing, decoration, household linen) and a wealth of creativity and innovation.
- The "Masters of Linen" mark, based on compliance with specifications, identifies top-quality European flax in the light of criteria relating to specific production and processing conditions in the European Community, from crop to cloth.
5. MAIN CHANNELS
- Electronic channels (Internet sites)
- Professional shows
- Information measures targeting users downstream (designers, makers, distributors, editors)
- Information at sales points
- Relations with the specialist press
- Educational information measures in fashion schools, textile engineer courses, etc.
6. DURATION OF PROGRAMMES
12 to 36 months, giving priority to multiannual programmes that set objectives for each stage.
7. INDICATIVE BUDGET
EUR 1 million.
Beef and veal
1. GOALS
The aim of the communication programme is to restore confidence in the beef and veal market by coordinated efforts in the Member States concerned. It is to be a flexible programme: the objectives and broad structure will be common to all, but the particular combination of elements and the timing will vary from Member State to Member State as the situation demands. There is a need for consistency, but not uniformity. The programme will cover all beef markets.
The body responsible for the programme must be identified in each Member State. A point of contact is also to be provided.
The programme should address consumers' concerns and reassure them about beef and veal.
The information campaign
The campaign will concentrate on reassuring consumers, who need to know that European and national legislation lays down rules to ensure product safety (e.g. traceability and labelling) and provides effective control throughout the production chain.
The campaign will operate at three levels: European Union, national authorities, and the private sector.
The content and meaning of any national or private labels used should be explained.
All material is to give the addresses of the European and national websites.
2. MAIN MESSAGES
- Beef and veal is nutritious and subject to regulation
- Effective safety measures are in place, including checks
- Beef and veal labelling is designed to reassure consumers
- More information is available if the consumer would like it.
3. TARGET GROUPS
A. Individual consumers
- The key consumer audience is women aged 25 to 45, with children, living in urban areas. These women are the core of the food-buying market.
- A second target group is single people and couples aged under 35 with good purchasing power, whose product choice is guided by convenience and taste.
B. The institutional market (schools, hospitals, catering providers, etc.)
The specialised press and consumer associations are also directly involved as opinion multipliers.
4. MAIN CHANNELS
- e-tools (Internet),
- telephone information line,
- contacts with media (e.g. consumer journalists, scientific and specialised press), conferences, question-and-answer sessions given by independent food safety experts. These sessions will involve traders, consumer groups and other institutional market entities,
- print media (e.g. consumer magazines, regional press, leaflets, brochures, etc.),
- visual media, such as poster advertising, point-of-sale materials, TV,
- radio.
5. INDICATIVE BUDGET
EUR 6 million.
(1) OJ L 161, 16.6.2001, p. 38.
