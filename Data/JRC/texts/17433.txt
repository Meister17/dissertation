Commission Regulation (EC) No 1985/2004
of 18 November 2004
concerning tenders notified in response to the invitation to tender for the import of sorghum issued in Regulation (EC) No 238/2004
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1784/2003 of 29 September 2003, on the common organisation of the market in cereals [1], and in particular Article 12(1) thereof,
Whereas:
(1) An invitation to tender for the maximum reduction in the duty on sorghum imported into Spain was opened pursuant to Commission Regulation (EC) No 238/2004 [2].
(2) Article 5 of Commission Regulation (EC) No 1839/95 [3], allows the Commission to decide, in accordance with the procedure laid down in Article 25 of Regulation (EC) No 1784/2003 and on the basis of the tenders notified to make no award.
(3) On the basis of the criteria laid down in Articles 6 and 7 of Regulation (EC) No 1839/95 a maximum reduction in the duty should not be fixed.
(4) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Cereals,
HAS ADOPTED THIS REGULATION:
Article 1
No action shall be taken on the tenders notified from 12 to 18 November 2004 in response to the invitation to tender for the reduction in the duty on imported sorghum issued in Regulation (EC) No 238/2004.
Article 2
This Regulation shall enter into force on 19 November 2004.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 18 November 2004.
For the Commission
Franz Fischler
Member of the Commission
--------------------------------------------------
[1] OJ L 270, 21.10.2003, p. 78.
[2] OJ L 40, 12.2.2004, p. 23.
[3] OJ L 177, 28.7.1995, p. 4. Regulation as last amended by Regulation (EC) No 2235/2000 (OJ L 256, 10.10.2000, p. 13).
