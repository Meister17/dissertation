Brussels, 21.12.2004
COM(2004)823 final
COMMUNICATION FROM THE COMMISSION TO THE EUROPEAN PARLIAMENT AND TO THE COUNCIL
Special Framework of Assistance for Traditional ACP Suppliers of Bananas (Council Regulation (EC) No 856/1999) Biennial Report from the Commission 2004
1. INTRODUCTION
1.1 In 2003, world banana production was approximately 69 million tonnes. The largest producer is India while the main exporters are Ecuador, Costa Rica, the Philippines and Colombia.
Almost all bananas imported to the US are of Latin American origin. Latin American bananas also have a more than 60% share of the EU market while ACP imports and Community production share the rest of the market between them on almost equal terms.
In 2002, almost 95% of total ACP banana exports were sold to the EU. In 2003, banana imports from the Ivory Coast and Cameroon accounted for more than 60% of total ACP imports into the EU.
1.2 Banana imports into the European Union have traditionally been regulated by a quota system with strong preferential treatment for bananas from Africa, the Caribbean and the Pacific (the so-called ACP countries). The EU’s banana import regime has not changed over the period 2002-2004. Following EU enlargement on 1 May 2004, the volume of imported bananas has been increased by an additional quantity for the period 1 May–31 December 2004 in order to ensure a sufficient supply of bananas to consumers in the enlarged Community.
The EU obtained two waivers in the WTO to cover the preferential import arrangements for the ACP. The first[1] covers the tariff preference for the import of bananas for the ACP under the Cotonou Agreement until 1 January 2008. The second[2] covers the reservation of quota C for the ACP until 1 January 2006.
The EU has agreed to introduce a “tariff only” regime for banana imports no later than 1 January 2006.[3] The level of this tariff has not yet been determined. The ACP countries will continue to benefit from a tariff preference under the new regime: their preferential advantage on the market will, however, depend on the agreed level of the tariff.
1.3 In order to make it easier for the twelve traditional ACP banana suppliers to cope with the transition to the new market conditions, a Special Framework of Assistance (SFA) was already put in place in 1999, through a dedicated budget line. Five African and seven Caribbean countries are considered to be traditional suppliers and are therefore beneficiaries of the SFA. This framework provides technical and financial support to specific projects presented by the countries concerned, based on a long-term strategy previously agreed with and approved by the Commission. The individual country allocations are calculated on the basis of two criteria, namely, both the competitiveness gaps observed when compared to third country suppliers, and the importance of the banana production to the economy of the ACP countries concerned. In general, during the first years of implementation (1999-2001) the allocations focused more on improving productivity than on diversification, whereas during the period from 2002 to 2004 the priority was to facilitate the diversification efforts of the countries concerned. Under the two Regulations,[4] a maximum reduction of 15% has started to be applied since the beginning of 2004 to all country allocations. In keeping with the Regulations, the reduction is less in the case of countries which have managed to increase their competitiveness.
2. LEGAL BASIS
On 22 April 1999, the Council adopted Regulation (EC) No 856/1999[5] launching a special framework of assistance for traditional ACP suppliers of bananas. On 22 July 1999, the Commission adopted Regulation No 1609/1999[6] stipulating the detailed rules for its implementation.
In 2002, the budget line added up to €40 million. The Commission Decision allocating the (individual) amounts available in 2003 under the special framework of assistance was adopted on 14 March 2003.[7] The relevant Commission Decision for the budget line 2004, amounting to a total of € 37.31 million, was adopted on 1 April 2004.[8]
2.1. Objectives
The overall objective is either to improve the competitiveness of traditional ACP banana production or to support diversification wherever competitiveness is no longer attainable. In summary, this goal should be achieved by funding projects designed to:
- increase productivity without causing damage to the environment,
- improve quality, including phytosanitary measures,
- adapt production, distribution and marketing to meet the Community’s quality standards,
- establish producers’ organisations focusing on improvements in marketing and on the development of environmentally friendly production methods, including fair-trade bananas,
- develop marketing and/or production strategies designed to meet the requirements of the EU common market organisation for bananas,
- assist banana producers in developing environmentally friendly production methods, including fair-trade bananas,
- assist with training and market intelligence, and improve the distribution infrastructure,
- support diversification whenever the competitiveness of the banana sector is not sustainable.
2.2. Reporting
Article 9 of the Council Regulation specifies that “by 31 December 2000, and every two years thereafter, the Commission shall present a report, accompanied if appropriate by proposals, on the operation of this Regulation to the European Parliament and the Council” . This report fulfils that obligation with regard to the years 2003 and 2004. The previous report, which dealt with the years 2001 and 2002, was issued on 23.12.2002.[9]
3. MARKET INFORMATION
In 2003, world banana production was approximately 69 million tonnes (68 million tonnes in 2002). The largest producer is India (23.8% of world production), while the main exporters are Ecuador, Costa Rica, the Philippines and Colombia, which in 2002 controlled together 63% of world banana exports.
The largest import markets for bananas are the US (3.9 million tonnes in 2002) and the EU (3.3 million tonnes in 2002). Almost all bananas imported to the US are of Latin American origin. Conversely, Latin American bananas (63%) share the EU market with ACP imports (19%) and Community production (18%).
In 2002, almost 95% of total ACP banana exports were sold to the EU. In 2003, banana imports from the Ivory Coast and Cameroon accounted for almost 63% (61% in 2002) of total ACP imports into the EU.
The EU is an attractive market for banana suppliers due to the higher prices, compared to the US market, resulting from the quota system and differences in both duties and transport costs. In 2003, the average prices of Latin American supplies were €621/t, and the average prices of ACP imports reached €616/t. However, significant differences in prices can be seen among ACP suppliers: in 2003, average prices for bananas originating from Belize amounted to €501/t, whereas for bananas from Ivory Coast the average price totalled €676/t.
4. EU TRADE REGIME
The EU trade regime for bananas has not changed since the last SFA report.
Since 1 January 2002, the import of bananas into the Community has been governed by tariff quotas and import licences distributed on the basis of past trade.
The following three tariff quotas apply: quota A of 2 200 000 tonnes at a tariff of €75/t (€0 for ACP bananas); quota B of 453 000 tonnes at a tariff of €75/t (€0 for ACP bananas); quota C of 750 000 tonnes at a tariff of €0/t (reserved for ACP bananas). Quotas A and B are open to bananas of any origin; C quota is reserved for ACP countries.
Banana imports outside these quotas are subject to a customs duty of €680/t. ACP countries benefit from a tariff preference of €300/t.
Following EU enlargement on 1 May 2004, appropriate arrangements have been made to ensure a sufficient supply of bananas to consumers in the enlarged Community. To this end, the current import volumes of bananas have been increased by an additional quantity of 300 000 tonnes for the period 1 May to 31 December 2004.
Also in the context of the EU enlargement, in the course of 2004 the EU manifested its readiness to negotiate with the relevant WTO members possible trade compensations under Article XXIV.6 of GATT for the increase in the import duties for bananas resulting from the application of the EU 15 tariff to the new Member States.
Finally, considering that the Regulation on the common market organisation for bananas, the Understandings on Bananas with Ecuador and the United States, and the WTO waivers to GATT Article I as regards the ACP-EC Partnership Agreement provide for the entry into force of a tariff-only import regime no later than 1 January 2006, the EU notified the WTO on 15 July 2004 of its intention to amend WTO concessions on bananas pursuant to GATT Article XXVIII procedures.
As far as the ACP banana suppliers are concerned, the Commission is committed to meeting its obligations stemming from the Cotonou Agreement and accordingly intends to pay particular attention to the implications of the change in its import regime for ACP banana producers, to examine appropriate ways of addressing their specific situation, including preferential access for ACP products, and to seek to maintain a level of preference for ACP countries equivalent to that afforded by the enlarged Community of 25.
5. FINANCIAL DECISIONS
5.1. Budget line 2003
The Financing Proposals prepared by the 12 ACP beneficiary countries were all approved at the November 2003 EDF Committee. Although the financial commitments were all adopted at the end of 2003, the signature of the corresponding financing agreements has only been possible at the end of 2004, due to the verification exercise under Article 164 of the Financial Regulations (except for Somalia which was signed earlier, as the management of the programme is centralised). The latter, performed in October/November 2004, checks the compliance with the criteria for decentralised management.
The total amount of the 2003 financing agreements is €40 million and has been allocated as shown in Annex I.
5.2. Budget line 2004
The 2004 indicative amounts were announced to the beneficiary states in April 2004. As all 12 countries presented financing proposals, there was no need for reallocation of unused resources.
The total amount of the 2004 allocations is slightly lower than in previous years, as per Article 7 of Council Regulation (No) No 856/1999:
“From the year 2004 […] a maximum reduction coefficient of 15% shall be applied to the level of assistance made available to the individual traditional ACP suppliers. Where programmes defined under Article 3(2)(a) [aiming at boosting productivity] are implemented, this reduction coefficient shall be reduced to the same extent that an increase in competitiveness has been observed compared to the previous year”.
The 12 financing proposals were submitted to the November EDF Committee, which gave its approval.
6. IMPLEMENTATION
Compared to the period 1999-2002, the ratios of the amounts allocated in the 2003 and 2004 financing agreements to boost banana plantation productivity continue to decrease.
The ratios of diversification to productivity in the amounts allocated in 2003 and 2004 were 171% and 178% respectively, up from 81% and 178% in 2001 and 2002 (see Annex VI).
As indicated in Article 3 of Council Regulation (No) No 856/1999, “technical and financial assistance shall be granted […] to contribute to the implementation of programmes aiming […] to support diversification where improvement in the competitiveness of the banana sector is not sustainable” .
Of the 24 financing agreements, 16 have allocated funds for diversification projects.
6.1. Diversification
Programmes to promote diversification mainly target banana farmer retraining.
- Saint Lucia, Saint Vincent & the Grenadines, Dominica, Grenada, Cape Verde, Somalia and Madagascar have planned to make use of the full amount of their 2003 and 2004 allocations for agricultural and institutional support projects.
- The 2003 and 2004 allocations for Jamaica are spread over both sectors: boosting productivity and diversification.
- Six out of the eight countries implementing diversification projects with the help of their 2003-2004 allocations have also included a specific technical assistance component in their programmes.
6.2. Boosting Productivity
Projects aimed at increasing the productivity of banana plantations are being carried out in 5 of the 12 countries. In 2003 and 2004, these projects include activities on:
- irrigation and drainage in Cameroon, Ivory Coast, Jamaica and Suriname;
- renewal of plantations in Cameroon, Ivory Coast and Belize;
- packing and storage of crops in Cameroon, Ivory Coast and Suriname;
- infrastructure and social projects in Cameroon, Ivory Coast and Belize;
- specific technical assistance in Ivory Coast, Belize, Jamaica and Suriname;
- certification of production instruments in Ivory Coast in order to meet the quality standards imposed by market conditions.
6.3. Payments and Disbursements
The tables in Annexes II, III and IV show allocations, payments and disbursements per SFA and per country, respectively, as at 15/10/2004 and 31/12/2003.
A sharp decrease in payments is to be noted in 2004, principally due to the need to adapt ongoing projects to the requirements of the new Financial Regulations that entered into force in 2003.
Indeed, project implementation via Annual Work Plans (AWP)[10] was no longer possible as the verification exercise under Article 164 of the Financial Regulations had not yet given its conclusions.
As shown in Annex VIII, the amount paid on AWPs for the projects implemented between 1999 and 2004 is €62.60 million while disbursements come to €35.67 million.
Although a considerable delay has been observed in the implementation of projects in 2003 and 2004, the situation should improve by the beginning of 2005 as the verification exercise under Article 164 of the Financial Regulations allows for decentralised management in most countries.
7. MONITORING
While no activities had been carried out at the time of the initial monitoring mission in all Caribbean countries in 2001, the relevant projects have in the meantime been successfully launched.
A consultant has been contracted by DG AGRI within the Bananas Common Market Organisation (CMO) to carry out a restricted analysis of the SFA. It has been agreed that the terms of reference will be extended to include a detailed impact analysis of the SFA from 1999 to 2004.
The monitoring mission will also focus on the current Banana Strategies and will estimate their sensitivity to the level of the single tariff to be introduced. In addition, the programmes proposed and implemented under the SFA will be assessed for their effectiveness to contribute to the Strategy of each beneficiary country.
The mission, which will visit all 12 countries, has started at the end of 2004 and should provide a report by mid-2005.
8. RECOMMENDATIONS AND CONCLUSIONS
Due to the verification exercise under Article 164 of the Financial Regulations, there has been a considerable delay in the implementation of projects during the past two years. The provision of this article allows for decentralised management in most countries concerned. The implementation situation should thus improve at the beginning of 2005.
In parallel to the reduction coefficient of 15% to the level of assistance made available and its proportional reduction to the increase in competitiveness observed, implementation and programming shifted from supporting competitiveness to supporting primarily diversification. This trend has continued in the period 2002-2004.
In 2002, an evaluation of the implementation of Regulation (EC) No 856/1999 was commissioned and carried out. Its recommendations, e.g. multi-annual programming to reduce administrative steps and consequently simplify the administrative process, were found not to correspond to the requirements of the SFA Regulation and the Financial Regulation. Other reports have been commissioned, drafted and carried out for other donors.[11] The Commission is currently in the process of examining their conclusions and recommendations with a view to further simplifying and accelerating the implementation of the SFA in the context of the existing regulatory framework. WORKING DOCUMENTS
Introduction to the Annexes:
Annexes II (1999), III (2000), IV (2001), V (2002) and VIII (recapitulative) show per country:
- payments (transfer from EC account to double signature account)
- disbursements (final payments of invoices) for annual work programmes
- payments/disbursements for specific commitments.
ANNEX I:BANANA BUDGET LINE 21-03-18 (Ex B7-8710) COUNTRY ALLOCATIONS for 2003 AND 2004
+++++ TABLE +++++
ANNEX II:SFA 1999
Financial situation as at:15/10/2004 for payments 31/12/2003 for disbursements
+++++ TABLE +++++
ANNEX III: SFA 2000
Financial situation as at:15/10/2004 for payments 31/12/2003 for disbursements
+++++ TABLE +++++
ANNEX IV: FA 2001
Financial situation as at:15/10/2004 for payments 31/12/2003 for Disbursements
+++++ TABLE +++++
ANNEX V:SFA 2002
Financial situation as at: 15/10/2004 for payments 31/12/2003 for disbursements
+++++ TABLE +++++
ANNEX VI:Overview of activities by type of assistance
[pic]
1) Irrigation and drainage 2) Renewal of plantations (3) Phyto-sanitary treatment (4) Fertiliser (5) Packing (6) Cold storage (7) Agriculture/rural development (8) Roads (9) Social infrastructure (10) Microcredit (11) Social projects (12) Training (13) Institutional support (14) Technical Assistance ANNEX VII: CHART: Global payments on AWP over Global Payments on Specific Commitments
[pic]
ANNEX VIII: CHART: Global payments over Global Disbursements on AWP only
[pic]
+++++ TABLE +++++
[1] WTO Decision of 14 November 2001: WT/MIN(01)15: “European Communities – The ACP-EC Partnership Agreement”.
[2] WTO Decision of 14 November 2001: WT/MIN(01)16: “European Communities – Transitional regime for the EC autonomous tariff rate quota on imports of bananas”.
[3] Council Regulation (EC) No 216/2001
[4] Art. 7(2) of Council Regulation (EC) No 856/1999 and Art. 5 of Commission Regulation (EC) No 1609/1999
[5] OJ L 108, 27.4.1999, p. 2
[6] OJ L 190, 23.7.1999, p. 14
[7] Decision E/2003/359 – C(2003)766
[8] Decision E/2004/644 – C(2004)1142
[9] COM(2002)763 final
[10] An Annual Work Plan (AWP), also called programme estimate, is a document laying down the human and material resources required, the budget and the detailed technical and administrative arrangements for implementation of a project over a specified period.
[11] For example, the NERA Economic Consulting report “Addressing the impact of preference erosion in bananas on Caribbean countries” carried out for DFID in August 2004
[12] Approximate amount calculated at the exchange rate of December 2003.
[13] The disbursed amount is higher than the one paid from HQ because of the exchange rates fluctuations.
[14] Approximate amount calculated at the exchange rate of December 2003.
[15] The disbursed amount is higher than the one paid from HQ because of the exchange rates fluctuations.
