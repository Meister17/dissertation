[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 2.6.2005
COM(2005) 250 final
2003/0198 (COD)
OPINION OF THE COMMISSION pursuant to Article 251 (2), third subparagraph, point (c) of the EC Treaty, on the European Parliament's amendments to the Council's common position regarding the proposal for a
REGULATION OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL amending the Convention implementing the Schengen Agreement of 14 June 1985 on the gradual abolition of checks at common borders as regards access to the Schengen Information System by the services in the Member States responsible for issuing registration certificates for vehicles
AMENDING THE PROPOSAL OF THE COMMISSION pursuant to Article 250 (2) of the EC Treaty
2003/0198 (COD)
OPINION OF THE COMMISSION pursuant to Article 251 (2), third subparagraph, point (c) of the EC Treaty, on the European Parliament's amendments to the Council's common position regarding the proposal for a
REGULATION OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL amending the Convention implementing the Schengen Agreement of 14 June 1985 on the gradual abolition of checks at common borders as regards access to the Schengen Information System by the services in the Member States responsible for issuing registration certificates for vehicles
1. HISTORY OF THE DOSSIER
The Council, in its Conclusions of 5 and 6 June 2003, invited the Commission to present a legislative proposal to give vehicle registration authorities access to the SIS (Schengen Information System) data on stolen and lost vehicles and relevant documents. The Commission presented a proposal for a Draft Regulation on 21 August 2003.
The proposal stipulates that Member States be given six months after publication of the draft Regulation to set up the necessary technical facilities to provide direct access. This implies that the functionality could be implemented under the current SIS system.
To make it possible for vehicle registration authorities to access the SIS files on stolen vehicles, the Schengen Convention must be amended because, for data protection reasons, the Convention specifically identifies the authorities that have the right to access the SIS. The regulation thus proposes to add a new article 102A to the Convention. The proposal is based on article 71 (1) (d) EC Treaty. The Parliament delivered its first reading opinion on 1 April 2004, proposing 10 amendments. In particular Parliament requested a yearly report on the implementation of this regulation. Finally, to underline that increased access should only be possible with strengthened data protection rules it introduced the requirement to record every transmission. The Commission orally presented its position on the first reading amending and on this basis the Council included seven out of ten amendments of the EP in its Common Position.
- Date of decision by the Commission: 21.8.2003
- Date of the transmission of the Proposal to the Parliament and the Council: 21.8.2003
- Date of the opinion of the European Economic and Social Committee : 25.2.2004
- First Reading of the European Parliament: 1.4.2004
- Date of the transmission of the Communication from the Commission to the European Parliament pursuant to the second subparagraph of Article 251(2) of the EC Treaty concerning the common position of the Council (COM(2005)3 final): 10.1.2005
- Council Common Position: 22.12.2004 (by unanimity)
- Date of receipt of the Council’s common position by the European Parliament: 13.1.2005
- Second reading of the European Parliament: 28.4.2005
2. OBJECTIVE OF THE PROPOSAL
The general objective of the proposal is to amend the Schengen Convention to improve co-operation between Member States and as a result the functioning of the internal market.
The specific objectives are as follows:
In the first place this proposal contributes to the realisation of the common transport policy by providing Member States with an additional operational means to assist each with re-registration of vehicles and thus to facilitate the mutual recognition of vehicle registration certificates issued by another Member State.
In the second place this proposal signifies a development of the Schengen acquis in the sense of Council Decision 1999/436/EC. It is a concrete contribution to a series of other initiatives developing the Schengen acquis aiming at augmenting the functionalities of the SIS.
In the third place the proposal will contribute to the achievement of the goals formulated by the European Council of Tampere and reaffirmed by the Hague Programme endorsed by the European Council on November 2004 in view of strengthening of an area of freedom, security and justice. Notably, the proposal also contributes to combat vehicle theft more efficiently. This will be done by allowing under certain conditions national vehicle registration authorities to consult the SIS data registers on stolen vehicles and on stolen vehicle documents prior to registering a vehicle that is already registered in another Schengen State. This information will make it more difficult for thieves to sell stolen vehicles in other Schengen States. At the same time it will increase the chances for the victims of theft to reclaim their lost property.
3. OPINION OF THE COMMISSION ON THE AMENDMENTS PROPOSED BY THE EUROPEAN PARLIAMENT
During the second reading Parliament adopted 3 amendments and the Commission fully accepts them.
Amendment 1
RECITAL 3(3)
Council Decision 2004/919/EC of 22 December 2004 on tackling vehicle crime with cross-border implications includes the use of the SIS as an integral part of the law enforcement strategy against vehicle crime.
Amendment 1 updates the text in order to take account of the fact that the Dutch initiative to which the Council Common position referred to in recital 3 has in the meantime been adopted.
Amendment 2
RECITAL 13 A (new) (13a)
As regards Switzerland, this Regulation constitutes a development of the provisions of the Schengen acquis within the meaning of the Agreement signed between the European Union, the European Community and the Swiss Confederation concerning the association of the Swiss Confederation with the implementation, application and development of the Schengen acquis, which falls in the area referred to in Article 1, point G of Decision 1999/437/EC read in conjunction with Article 4(1) of Council Decision 2004/860/EC on the signing on behalf of the European Community, and on the provisional application of certain provisions of that Agreement.
Amendment 2 is introduced to take account of Switzerland's forthcoming accession to the Schengen acquis on which this measure constitutes a development.
Amendment 3
ARTICLE 1
Article 102 a, paragraph 3 a (new) (1990 Schengen Convention) 3a. Each year, after seeking the opinion of the joint supervisory authority set up pursuant to Article 115 on the data protection rules, the Council shall submit a report to the European Parliament on the implementation of this Article. This report shall include information and statistics relating to the use and results of the implementation of this Article and shall state how the data protection rules were applied.
Amendment 3 requests an annual report from the Council to the EP about the implementation of the proposed access to vehicle registration authorities to combat vehicle crime.
Amendment 3 is now acceptable for the Commission since modifies the wording of previous AM 10 (first reading) in two aspects: first, it is proposed that the Council and not the Commission sends the report to the EP. In fact, the Commission has no responsibility for the management of the current SIS. Second, a general wording is introduced to clarify that the preparation of these statistics will not imply for the Member States to introduce technical changes at national level. In fact, the report will generally include “information and statistics relating to the use and results of the implementation of this Article”.
4. CONCLUSION
Pursuant to Article 250(2) of the EC Treaty, the Commission amends its proposal as set out above.
