Action brought on 22 May 2006 — SANOFI AVENTIS v OHIM — G.D. SEARLE ("ATURION")
Parties
Applicant: SANOFI AVENTIS, S.A. (Paris, France) (represented by: E. Armijo Chavarri and A. Castán Pérez-Gómez, lawyers)
Defendant: Office for Harmonisation in the Internal Market (Trade Marks and Designs)
Other party to the proceedings before the Board of Appeal of OHIM: G.D. SEARLE LLC
Form of order sought
- Annul the decision of the First Board of Appeal of OHIM of 3 February 2006 in Case R 227/2005-1;
- Order OHIM to pay the costs.
Pleas in law and main arguments
Applicant for a Community trade mark: G.D. SEARLE LLC.
Community trade mark concerned: Word mark "ATURION" for goods in Class 5 — application No1662311.
Proprietor of the mark or sign cited in the opposition proceedings: The applicant.
Mark or sign cited in opposition: National word mark "URION" for goods in Class 5.
Decision of the Opposition Division: Rejection of the opposition.
Decision of the Board of Appeal: Dismissal of the appeal.
Pleas in law: Infringement of Article 8(1)(b) of Regulation (EC) No 40/94, given that the marks in question may create a likelihood of confusion on the market.
--------------------------------------------------
