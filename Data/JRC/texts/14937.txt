Action brought on 16 November 2005 — Evian v OHIM
Parties
Applicant: Société Anonyme des Eaux Minérales d'Evian (Evian, France) (represented by: C. Herzt-Eichenrode, lawyer)
Defendant: Office for Harmonisation in the Internal Market (Trade Marks and Designs)
Other party to the proceedings before the Board of Appeal of OHIM: A. Racke GmbH & Co. OHG (Bingen, Germany)
Form of order sought
The applicant claims that the Court should:
- annul decision no 2754/2001 of the Opposition Division of OHIM of 23 November 2001;
- annul the decision of the Fourth Board of Appeal of the Office for Harmonisation in the Internal Market (Trade Marks and Designs) of 22 July 2005 (Case R 82/2002-4);
- order the defendant to pay the costs.
Pleas in law and main arguments
Applicant for a Community trade mark: A. Racke GmbH & Co. OHG
Community trade mark concerned: The figurative mark "REVIAN's" in gold, black and white for goods in Class 33 (Wine and sparkling wine) — Application No 945758
Proprietor of the mark or sign cited in the opposition proceedings: The applicant
Mark or sign cited in opposition: The national and international word and figurative marks "EVIAN" for goods in Classes 32 and 33 (beer, alcoholic beverages, water, lemonades…)
Decision of the Opposition Division: Rejection of the opposition
Decision of the Board of Appeal: Dismissal of the appeal
Pleas in law: Infringement of Article 8(1)(b) of Council Regulation No 40/94 as the likelihood of confusion between the marks was rejected without assessment as to the distinctive character of the mark EVIAN and as there was an error in law in appraising the similarity of the goods.
Moreover, the opposition was not unfounded as the translations required under Rule 17(2) of Commission Regulation No 2868/95 were submitted.
--------------------------------------------------
