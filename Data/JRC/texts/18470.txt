COUNCIL DECISION of 20 December 1983 on the allocation of the possibilities for catching herring in the North Sea as from 1 January 1984 (83/653/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 170/83 of 25 January 1983 establishing a Community system for the conservation and management of fishery resources (1), and in particular Articles 4 (1) and 11 thereof,
Having regard to the proposal from the Commission,
Whereas the replenishment of the herring stock in the North Sea makes it possible to begin fishing for this species again;
Whereas, in order to enable herring fishermen to organize their fishing on a stable basis, rules for the distribution of this stock should be adopted on the basis of the development thereof,
HAS ADOPTED THIS DECISION:
Article 1
The possibilities for catching herring in the North Sea shall be allocated in accordance with the table annexed to this Decision.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 20 December 1983.
For the Council
The President
N. AKRITIDIS (1) OJ No L 24, 27.1.1983, p. 1.
ANNEX
>PIC FILE= "T0023441">For Community shares between 155 000 and 251 000 tonnes, allocation would be made in accordance with the points on corresponding straight-line graphs.
