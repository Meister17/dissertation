Order of the President of the Court of 29 June 2006 — Commission of the European Communities
v Republic of Austria
(Case C-209/05) [1]
(2006/C 224/63)
Language of the case: German
The President of the Court has ordered that the case be removed from the register.
[1] OJ C 171, 9.7.2005.
--------------------------------------------------
