Commission Regulation (EC) No 220/2005
of 10 February 2005
opening and providing for the administration of an autonomous tariff quota for preserved mushrooms from 1 January 2005
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to the Treaty of Accession of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia,
Having regard to the Act of Accession of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia, and in particular the first paragraph of Article 41 thereof,
Whereas:
(1) Commission Regulation (EC) No 1864/2004 [1] opens tariff quotas for preserved mushrooms and lays down rules for the administration thereof. To that end it lays down transitional measures for importers in the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia. The aim of these measures is to make a distinction between traditional importers and new importers in the new Member States, and to adjust the quantities to which licence applications presented by traditional importers from the new Member States may relate so that these importers may benefit from this system.
(2) To ensure uninterrupted supplies to the enlarged Community market while taking account of the economic supply conditions in the new Member States prior to accession, a new autonomous and temporary import tariff quota should be opened for preserved mushrooms of the genus Agaricus falling within CN codes 07115100, 20031020 and 20031030. This new tariff quota is in addition to those opened by Commission Regulations (EC) Nos 1076/2004 of 7 June 2004 [2] and 1749/2004 of 7 October 2004 [3].
(3) This new quota must be transitional and must not prejudge the outcome of the negotiations under way in the context of the World Trade Organisation (WTO) as a result of the accession of new members.
(4) The Management Committee for Products Processed from Fruit and Vegetables has not delivered an opinion within the time limit set by its chairman,
HAS ADOPTED THIS REGULATION:
Article 1
1. An autonomous tariff quota of 1200 tonnes (drained net weight), bearing the serial number 09.4111, hereinafter the "autonomous quota", shall be opened from 1 January 2005 for Community imports of preserved mushrooms of the genus Agaricus falling within CN codes 07115100, 20031020 and 20031030.
2. The ad valorem tariff rate applicable to products imported under the autonomous quota shall be 12 % for products falling within CN code 07115100 and 23 % for products falling within CN codes 20031020 and 20031030.
Article 2
Regulation (EC) No 1864/2004 shall apply to the management of the autonomous quota, subject to the provisions of this Regulation.
However, Articles 1, 5(2) and (5), 6(2), (3) and (4), 7, 8(2), 9 and 10 of Regulation (EC) No 1864/20042 shall not apply to the management of the autonomous quota.
Article 3
Import licences issued under the autonomous quota, hereinafter "licences", shall be valid until 30 June 2005.
Box 24 of licences shall show one of the entries listed in Annex I.
Article 4
1. Importers may submit licence applications to the competent authorities of the Member States in the five working days following the date of entry into force of this Regulation.
Box 20 of licence applications shall show one of the entries listed in Annex II.
2. Licence applications submitted by a single traditional importer may not relate to a quantity exceeding 9 % of the autonomous quota.
3. Licence applications submitted by a single new importer may not relate to a quantity exceeding 1 % of the autonomous quota.
Article 5
The autonomous quota shall be allocated as follows:
- 95 % to traditional importers,
- 5 % to new importers.
If the quantity allocated to one of the categories of importers is not used in full, the balance may be allocated to the other category.
Article 6
1. Member States shall notify the Commission, on the seventh working day following the entry into force of this Regulation, of the quantities for which licence applications have been made.
2. Licences shall be issued on the twelfth working day following the entry into force of this Regulation, unless the Commission has taken special measures under paragraph 3.
3. Where the Commission finds, on the basis of the information notified under paragraph 1, that licence applications exceed the quantities available for a category of importers under Article 5, it shall adopt, by means of a regulation, a single reduction percentage for the applications in question.
Article 7
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 10 February 2005
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 325, 28.10.2004, p. 30.
[2] OJ L 203, 8.6.2004, p. 3.
[3] OJ L 312, 9.10.2004, p. 3
--------------------------------------------------
ANNEX I
Entries referred to in Article 3
--------------------------------------------------
ANNEX II
Entries referred to in Article 4(1)
--------------------------------------------------
