[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 15.12.2005
COM(2005) 663 final
Proposal for a
COUNCIL REGULATION
amending Regulation (EC) 92/2002 imposing definitive anti-dumping duty and collecting definitively the provisional anti-dumping duty imposed on imports of urea originating in Belarus, Bulgaria, Croatia, Estonia, Libya, Lithuania, Romania and the Ukraine
(presented by the Commission)
EXPLANATORY MEMORANDUM
CONTEXT OF THE PROPOSAL |
Grounds for and objectives of the proposal This proposal concerns the application of Council Regulation (EC) No 92/2002, imposing definitive anti-dumping duties on imports of urea originating, inter alia, in Bulgaria, except where this product is directly exported by the Bulgarian exporting producer Chimco AD from which the Commission, by Regulation (EC) No 1497/2001, has accepted a price undertaking. |
General context This proposal is made in the context of the implementation of the basic Regulation and is the result of an investigation which was carried out in line with the substantive and procedural requirements laid out in the basic Regulation. |
Existing provisions in the area of the proposal There are no existing provisions in the area of the proposal. |
Consistency with other policies and objectives of the Union Not applicable. |
CONSULTATION OF INTERESTED PARTIES AND IMPACT ASSESSMENT |
Consultation of interested parties |
Interested parties concerned by the proceeding have already had the possibility to defend their interests during the investigation, in line with the provisions of the basic Regulation. |
Collection and use of expertise |
There was no need for external expertise. |
Impact assessment This proposal is the result of the implementation of the basic Regulation. The basic Regulation does not foresee a general impact assessment but contains an exhaustive list of conditions that have to be assessed. |
LEGAL ELEMENTS OF THE PROPOSAL |
Summary of the proposed action For the reasons indicated in Commission Regulation (EC) No …/2005[1], it is considered that the Bulgarian exporting producer Chimco AD has breached the terms of its undertaking. The undertaking should therefore be withdrawn and Council Regulation (EC) No 92/2002 should be amended accordingly. The Member States were consulted regarding this investigation. All Member States supported the proposal. It is therefore proposed that the Council adopts the attached proposal for a Regulation which should be published in the Official Journal of the European Union as soon as possible. |
Legal basis Council Regulation (EC) No 384/96 of 22 December 1995 on protection against dumped imports from countries not members of the European Community, as last amended by Council Regulation (EC) No 461/2004 of 8 March 2004. |
Subsidiarity principle The proposal falls under the exclusive competence of the Community. The subsidiarity principle therefore does not apply. |
Proportionality principle The proposal complies with the proportionality principle for the following reasons. |
The form of action is described in the above-mentioned basic Regulation and leaves no scope for national decision. |
Indication of how financial and administrative burden falling upon the Community, national governments, regional and local authorities, economic operators and citizens is minimized and proportionate to the objective of the proposal is not applicable. |
Choice of instruments |
Proposed instruments: regulation. |
Other means would not be adequate because the basic Regulation does not foresee alternative options. |
BUDGETARY IMPLICATION |
The proposal has no implication for the Community budget. |
1. Proposal for a
COUNCIL REGULATION
amending Regulation (EC) 92/2002 imposing definitive anti-dumping duty and collecting definitively the provisional anti-dumping duty imposed on imports of urea originating in Belarus, Bulgaria, Croatia, Estonia, Libya, Lithuania, Romania and the Ukraine
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 384/96 of 22 December 1995 on protection against dumped imports from countries not members of the European Community (the ‘basic Regulation’)[2], and in particular Articles 8 and 9 thereof,
Having regard to the proposal submitted by the Commission after consulting the Advisory Committee,
Whereas:
A. PREVIOUS PROCEDURE
(1) On 21 October 2000, by means of a notice published in the Official Journal of the European Communities, the Commission announced the initiation of an anti-dumping proceeding[3] in respect of imports of urea (‘the product concerned’) originating in Belarus, Bulgaria, Croatia, Egypt, Estonia, Libya, Lithuania, Poland, Romania and the Ukraine.
(2) This proceeding resulted in provisional anti-dumping duties being imposed in July 2001 on imports of urea originating in Belarus, Bulgaria, Croatia, Estonia, Libya, Lithuania, Romania and the Ukraine and a termination of the proceeding concerning imports of urea originating from Egypt and Poland by Commission Regulation (EC) No 1497/2001[4].
(3) In the same Regulation, the Commission accepted an undertaking offered by the exporting producer in Bulgaria, Chimco AD. Subject to the conditions set out in Regulation (EC) 1497/2001, imports of the product concerned into the Community from this company were exempted from the said provisional anti-dumping duties, pursuant to Article 3 (1) of the same Regulation.
(4) Definitive duties were later imposed on imports of urea originating in Belarus, Bulgaria, Croatia, Estonia, Libya, Lithuania, Romania and the Ukraine by Council Regulation (EC) No 92/2002[5] (‘the definitive Regulation’). Subject to the conditions set out therein, this Regulation also granted goods produced and directly exported to the first independent customer in the Community by Chimco AD an exemption to the definitive anti-dumping duties as an undertaking had already been accepted definitively from this company at the provisional stage of the proceeding. As mentioned in recital 137 of the definitive Regulation, the minimum price of the undertaking was adapted due to a change in the injury elimination level.
B. FAILURE TO COMPLY WITH THE UNDERTAKING
(5) The undertaking offered by Chimco AD obliges the company concerned, inter alia , to export the product concerned to the Community at or above certain minimum import price levels (‘MIPs’) specified therein. This minimum price level has to be respected on a quarterly weighted average. The company also undertakes not to circumvent the undertaking by making compensatory arrangements with any other party. Furthermore, Chimco AD is obliged to send to the European Commission a quarterly report of all its exports sales of the product concerned to the European Community.
(6) Chimco AD failed to submit more data for two quarterly reports in a technically acceptable manner. Further, afterwards, Chimco AD no longer submitted any data in respect of the quarterly reports. It is therefore found, that the company did not respect its obligation to send to the European Commission quarterly reports of all its exports sales of the product concerned to the European Community and had therefore breached the undertaking.
(7) Commission Regulation (EC) No …/2005[6] sets out in more detail the nature of the breaches found.
(8) In view of these breaches, acceptance of the undertaking offered by Chimco AD (Taric additional code A272) has been withdrawn by Commission Regulation (EC) No …/2005 and a definitive anti-dumping duty should be imposed forthwith on imports of the product concerned when produced and exported by Chimco AD.
(9) In accordance with Article 8(9) of Regulation (EC) No 384/96, the rate of the anti-dumping duty must be determined on the basis of the facts established within the context of the investigation which led to the undertaking. As the investigation in question was concluded with a final determination as to dumping and injury, by Regulation (EC) No 92/2002, it is considered appropriate that the definitive anti-dumping rate is set at the level and in the form imposed by that Regulation, namely 21,43 Euro per ton before duty, to the net, free-at-Community frontier price.
C. AMENDMENT OF REGULATION (EC) No 92/2002
(10) In view of the above, Regulation (EC) No 92/2002 should be amended accordingly,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 92/2002 is hereby amended as follows:
1. In Article 1 (2), the row concerning Bulgaria shall be replaced by the following:
Country of origin | Produced by | Definitive anti-dumping duty (euro per ton) | TARIC additional code |
Bulgaria | All companies | 21,43 | - |
2. In Article 2 (1) the below line of the table concerning Bulgaria shall be deleted:
Country | Company | TARIC additional code |
Bulgaria | Chimco AD, Shose az Mezdra, 3037 Vratza | A272 |
Article 2
This Regulation shall enter into force on the day following that of its publication in the Official Journal of the European Union .
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels,
For the Council
The President
[1] OJ L …,….2005, p. ….
[2] OJ L 56, 6.3.1996, p. 1. Regulation as last amended by Regulation (EC) No 461/2004 (OJ L 77, 13.3.2004, p. 12).
[3] OJ C 301, 21.10.2000, p. 2.
[4] OJ L 197, 21.7.2001, p. 4.
[5] OJ L 17, 19.1.2002, p. 1. Regulation as last amended by Regulation (EC) No 1107/2002 (OJ L 168, 27.6.2002, p. 1).
[6] OJ L …, ….2005, p. ….
