Judgment of the Court of First Instance (Fifth Chamber) of 31 January 2006 — Albrecht and Others v Commission
(Case T-251/03) [1]
Parties
Applicants: Albert Albrecht GmbH & Co. KG, (Aulendorf, Germany), AniMedica GmbH (Seden-Bösensell, Germany), Ceva Tiergensundheit GmbH (Düsseldorf, Germany), Fatro SpA (Bologna, Italy), Laboratorios Syva, SA (León, Spain), Laboratorios Virbac, SA (Barcelona, Spain), Química Farmacéutica Bayer, SA (Barcelona), Univete Técnica Pecuaria Comercio Industria, Lda (Lisbon, Portugal), Vétoquinol Especialidades Veterinarias, SA (Madrid, Spain), Virbac SA (Carros, France), (represented by: D. Waelbroeck, U. Zinsmeister and N. Rampal, lawyers)
Defendant(s): Commission of the European Communities (represented by: H. Støvlbæk and M. Shotter, Agents)
Intervener(s) in support of the applicant(s): French Republic (represented by: G. de Bergues and R. Loosli-Surrans, Agents)
Application for
annulment of Commission Decision C(2003) 1404 of 22 April 2003 ordering suspension of the marketing authorisations of veterinary medicinal products containing benzathine benzylpenicillin intended to be administered by intramuscular and/or subcutaneous means to food-producing animals,
Operative part of the judgment
The Court:
1. Annuls Commission Decision C(2003) 1404 of 22 April 2003 ordering suspension of the marketing authorisations of veterinary medicinal products containing benzathine benzylpenicillin intended to be administered by intramuscular and/or subcutaneous means to food-producing animals;
2. Orders the Commission to bear its own costs and those incurred by the applicants;
3. Orders the French Republic to bear its own costs.
[1] OJ C 239, 4.10.2003.
--------------------------------------------------
