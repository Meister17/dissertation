*****
COUNCIL DIRECTIVE
of 21 November 1989
on mutual assistance between the administrative authorities of the Member States and cooperation between the latter and the Commission to ensure the correct application of legislation on veterinary and zootechnical matters
(89/608/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 43 thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas, in the agricultural sector, far-reaching rules have been introduced in the veterinary and zootechnical area;
Whereas the smooth functioning of the common agricultural policy and of the common market for agricultural products, and also the prospect of the abolition of veterinary checks at frontiers with a view to achieving the internal market for products subject to such checks, render it necessary to reinforce cooperation between the authorities responsible in each Member State for the application of veterinary and zootechnical rules;
Whereas it is therefore appropriate to establish rules according to which the competent authorities of the Member States must mutually provide each other with assistance and cooperate with the Commission so as to ensure the proper application of veterinary and zootechnical rules, in particular by the prevention and detection of infringements of such rules and detection of any activity which is or seems to be contrary thereto;
Whereas, in order to establish these rules, inspiration should be sought, as far as possible, from the Community provisions established by Council Regulation (EEC) No 1468/81 of 19 May 1981 on mutual assistance between the administrative authorities of the Member States and cooperation between the latter and the Commission to ensure the correct application of the law on customs or agricultural matters (4), as amended by Regulation (EEC) No 945/87 (5); whereas account should also be taken of the specificity of health rules,
HAS ADOPTED THIS DIRECTIVE:
Article 1
The Directive lays down the ways in which the competent authorities responsible in the Member States for monitoring legislation on veterinary and zootechnical matters shall cooperate with those in the order Member States and with the relevant Commission departments in order to ensure compliance with such legislation.
Article 2
1. For the purposes of this Directive:
- 'legislation on veterinary matters' shall mean all Community provisions and provisions which contribute to the application of Community rules governing animal health, public health as related to
the veterinary sector, health inspection of animals, meat and other products of animal origin, and animal protection.
- 'legislation on zootechnical matters' shall mean all Community provisions and provisions which contribute to the application of Community rules on zootechnical matters,
- 'applicant authority' shall mean the competent authority of a Member State which makes a request for assistance,
- 'requested authority' shall mean the competent authority of a Member State to which a request for assistance is made.
2. Each Member State shall communicate to the other Member States and to the Commission a list of the competent authorities referred to in Article 1.
Article 3
The obligation to provide assistance laid down by this Directive shall not concern the provision of information or documents obtained by the competent authorities referred to in Article 1 under powers exercised by them at the request of the judicial authority.
However, in the case of an application for assistance, such information or documents shall be provided, without prejudice to Article 14, in all cases where the judicial authority, which must be consulted to that effect, gives its consent.
TITLE I
Assistance on request
Article 4
1. At the request, which shall be duly reasoned, of the applicant authority, the requested authority shall
- communicate to the applicant authority all information, attestations, documents or certified copies thereof in its possession or which it can obtain as prescribed in paragraph 2 and which are such as to enable it to check that the provisions laid down in legislation on veterinary and zootechnical matters have been complied with,
- carry out any relevant inquiry into the accuracy of the facts notified by the applicant authority and inform it of the outcome of such inquiries including the information that was necessary for such inquiries.
2. In order to obtain this information, the requested authority or the administrative authority which it has addressed shall proceed as though it were acting on its own account or at the request of another authority in its own country.
Article 5
1. At the request of the applicant authority, the requested authority shall, while observing the rules in force in the Member State in which it is situated, notify the applicant authority or have it notified of all instruments or decisions which emanate from the competent authorities and concern the application of legislation on veterinary or zootechnical matters.
2. Requests for notification, mentioning the subject of the act or decision to be communicated, shall, at the request of the requested authority, be accompanied by a translation in the official language or one of the official languages of the Member State in which the requested authority is situated.
Article 6
At the request of the applicant authority, the requested authority shall keep a watch or arrange for a watch to be kept or to be reinforced within its operational area where such irregularities are suspected, in particular:
(a) on establishments;
(b) on places where stocks of goods have been assembled;
(c) on notified movements of goods;
(d) on means of transport.
Article 7
At the request of the applicant authority, the requested authority shall supply to it any relevant information in its possession or which it obtains in accordance with Article 4 (2), in particular in the form of reports and other documents or certified copies of or extracts from such reports or documents, concerning operations actually detected which appear to the applicant authority to be contrary to legislation on veterinary or zootechnical matters.
TITLE II
Spontaneous assistance
Article 8
1. The competent authorities of each Member State shall, as laid down in paragraph 2, spontaneously assist the competent authorities of the other Member States without prior request of the latter.
2. Where they consider it useful in connection with compliance with the legislation on veterinary or zootechnical matters, the competent authorities of each Member State shall:
(a) as far as possible keep the watch referred to in Article 6 or arrange for such watch to be kept;
(b) communicate to the competent authorities of the other Member States concerned as soon as possible all available information, in particular in the form of reports and other documents or copies of or extracts from such reports or documents, concerning operations which are or appear to them to be contrary to legislation on veterinary or zootechnical matters, and particularly the means or methods used to carry out such operations.
TITLE III
Final provisions
Article 9
1. The competent authorities of each Member State shall communicate to the Commission as soon as it is available to them: (a) any information they consider useful concerning:
- goods which have been or are suspected of having been the subject of transactions contrary to legislation on veterinary or zootechnical matters,
- the methods or processes used or suspected of having been used to contravene such legislation;
(b) any information on deficiencies of, or lacunae in, the said legislation which application thereof has revealed or suggested.
2. The Commission shall communicate to the competent authorities of each Member State, as soon as it is available to it, any information which is such as to enable compliance with legislation on veterinary or zootechnical matters to be enforced.
Article 10
1. Where the competent authorities of a Member State become aware of operations which are, or appear to be, contrary to the rules on veterinary or zootechnical matters and which are of particular interest at Community level, and in particular:
- where they have, or might have, ramifications in other Member States, or
- where it appears to the said authorities likely that similar operations have been also carried out in other Member States,
those authorities shall communicate to the Commission as quickly as possible, either on their own initiative or at the reasoned request of the Commission, all relevant information, where appropriate in the form of documents or copies or extracts from documents, necessary to determine the facts so as to enable the Commission to coordinate the action undertaken by the Member States.
The Commission shall pass this information on to the competent authorities of the other Member States.
2. Where the communications referred to in paragraph 1 concerning cases likely to create risks for human health, and in the absence of other methods of prevention, the information in question may, after contact between the parties and the Commission, be the object of a reasoned notice to the public.
3. Information relating to natural or legal persons shall be communicated as provided for in paragraph 1 only to the extent strictly necessary to enable operations which are contrary to legislation on veterinary or zootechnical matters to be noted.
4. Where the competent authorities of a Member State make use of paragraph 1, they need not communicate information as provided in Articles 8 (2) (b) and 9 to the competent authorities of the other Member States concerned.
Article 11
The Commission and the Member States meeting within the Standing Veterinary Committee or the Standing Zootechnical Committee shall:
- examine, in general terms, the operation of the mutual assistance arrangements provided for in this Directive,
- examine the relevant information communicated to the Commission pursuant to Articles 9 and 10 - and the procedures for such communication - with a view to drawing conclusions.
In the light of that examination the Commission shall, if necessary, suggest amendments to existing Community provisions or the drawing-up of additional ones.
Article 12
For the purposes of applying this Directive, Member States shall take all the necessary steps to:
(a) ensure smooth internal coordination between the competent authorities referred to in Article1;
(b) establish, in their mutual relations and as required, direct cooperation between the authorities they specially empower to this end.
Article 13
1. This Directive shall not oblige the competent authorities of the Member States to grant each other assistance where to do so would be likely to prejudice public policy or any other fundamental interests of the State in which they are situated.
2. Reasons shall be stated for any refusal to grant assistance.
Article 14
The supply of documents provided for in this Directive may be replaced by the supply of computerized information produced in any form for the same purpose.
Article 15
1. Any information communicated in whatever form pursuant to this Directive shall be of a confidential nature. It shall be covered by the obligation of professional secrecy and shall enjoy the protection extended to like information under both the national law of the Member State which received it and the corresponding provisions applying to Community bodies.
The information referred to in the first subparagraph may not, in particular, be sent to persons other than those in the Member States or within the Community institutions whose duties require that they have access to it. Nor many it be used for purposes other than those provided for in this Directive, unless the authority supplying it has expressly agreed and insofar as the provisions in force, in the Member State where the authority which received it is situated do not object to such communication or use.
The information provided for under this Directive shall nonly be communicated to the applicant authority to the extent there is nothing to the contrary in the provisions in force in the Member State or requested authority.
Member States shall ensure that information obtained under the mutual assistance scheme remains confidential, even after a matter has been closed.
2. Paragraph 1 shall not impede the use of information obtained pursuant to this Directive in any legal actions or proceedings subsequently instituted for failure to comply with legislation on veterinary or zootechnical matters on or in the prevention and discovery of irregularities detrimental to Community funds.
The competent authority of the Member State which supplied this information shall be informed forthwith of such utilization.
Article 16
Member States shall communicate to the Commission and to the other Member States the bilateral mutual assistance agreements between veterinary administrations concluded with third countries.
The Commission shall, for its part, communicate to the Member States agreements of the same nature that it concludes with third countries.
Article 17
Member States shall mutually waive claims for the reimbursement of expenses incurred pursuant to this Directive except, where appropriate, in respect of fees paid to experts.
Article 18
This Directive shall not affect the application in the Member States of the rules on mutual assistance in criminal matters.
Article 19
Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive before 1 July 1991. They shall forthwith inform the Commission thereof.
Article 20
This Directive is addressed to the Member States.
Done at Brussels, 21 November 1989.
For the Council
The President
H. NALLET
(1) OJ No C 225, 31. 8. 1988, p. 4.
(2) OJ No C 326, 19. 12. 1988, p. 28.
(3) OJ No C 56, 6. 3. 1989, p. 7.
(4) OJ No L 144, 2. 6. 1981, p. 1.
(5) OJ No L 90, 2. 4. 1987, p. 3.
