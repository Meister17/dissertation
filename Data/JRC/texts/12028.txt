Euro exchange rates [1]
30 May 2006
(2006/C 127/01)
| Currency | Exchange rate |
USD | US dollar | 1,2839 |
JPY | Japanese yen | 144,05 |
DKK | Danish krone | 7,4573 |
GBP | Pound sterling | 0,68470 |
SEK | Swedish krona | 9,2726 |
CHF | Swiss franc | 1,5585 |
ISK | Iceland króna | 92,61 |
NOK | Norwegian krone | 7,8250 |
BGN | Bulgarian lev | 1,9558 |
CYP | Cyprus pound | 0,5750 |
CZK | Czech koruna | 28,203 |
EEK | Estonian kroon | 15,6466 |
HUF | Hungarian forint | 261,51 |
LTL | Lithuanian litas | 3,4528 |
LVL | Latvian lats | 0,6959 |
MTL | Maltese lira | 0,4293 |
PLN | Polish zloty | 3,9323 |
RON | Romanian leu | 3,5280 |
SIT | Slovenian tolar | 239,64 |
SKK | Slovak koruna | 37,690 |
TRY | Turkish lira | 1,9885 |
AUD | Australian dollar | 1,6848 |
CAD | Canadian dollar | 1,4138 |
HKD | Hong Kong dollar | 9,9600 |
NZD | New Zealand dollar | 2,0054 |
SGD | Singapore dollar | 2,0227 |
KRW | South Korean won | 1214,31 |
ZAR | South African rand | 8,4320 |
CNY | Chinese yuan renminbi | 10,3077 |
HRK | Croatian kuna | 7,2690 |
IDR | Indonesian rupiah | 11799,04 |
MYR | Malaysian ringgit | 4,646 |
PHP | Philippine peso | 67,514 |
RUB | Russian rouble | 34,6520 |
THB | Thai baht | 48,981 |
[1] Source: reference exchange rate published by the ECB.
--------------------------------------------------
