Commission Decision
of 22 May 2006
amending Decision 2005/1/EC authorising methods for grading pig carcasses in the Czech Republic
(notified under document number C(2006) 1982)
(Only the Czech text is authentic)
(2006/383/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 3220/84 of 13 November 1984 determining the Community scale for grading pig carcasses [1], and in particular Article 5(2) thereof,
Whereas:
(1) By Commission Decision 2005/1/EC [2], the Czech Republic was authorised to apply four methods for grading pig carcasses.
(2) The Czech Republic has requested the Commission to authorise two new methods for grading pig carcasses and has submitted the results of its dissection trials, by presenting part two of the protocol provided for in Article 3(3) of Commission Regulation (EEC) No 2967/85 of 24 October 1985 laying down detailed rules for application of the Community scale for grading pig carcasses [3].
(3) The evaluation of this request has revealed that the conditions for authorising the new methods are fulfilled.
(4) Due to technical reasons in slaughterhouses, the Czech Republic requested to fix the limit for using the grading method "Zwei-Punkte-Messverfahren (ZP)" at the previous weekly amount expressed in annual average.
(5) Decision 2005/1/EC should therefore be amended accordingly.
(6) The measures provided for in this Decision are in accordance with the opinion of the Management Committee for Pigmeat,
HAS ADOPTED THIS DECISION:
Article 1
Decision 2005/1/EC is amended as follows:
1. Article 1 is amended as follows:
(a) In the first paragraph of Article 1, the following fifth and sixth indents are added:
- "— the apparatus termed "Ultra-sound IS-D-05" and assessment methods related thereto, details of which are given in Part 5 of the Annex,
- — the apparatus termed "Needle IS-D-15" and assessment methods related thereto, details of which are given in Part 6 of the Annex."
(b) The second paragraph is replaced by the following:
"The grading method "Zwei-Punkte-Messverfahren (ZP)" may be applied only in slaughterhouses which do not exceed a weekly slaughtering of 200 pigs in annual average."
2. The Annex is amended in accordance with the Annex to this Decision.
Article 2
This Decision is addressed to the Czech Republic.
Done at Brussels, 22 May 2006.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 301, 20.11.1984, p. 1. Regulation last amended by Regulation (EC) No 3513/93 (OJ L 320, 22.12.1993, p. 5).
[2] OJ L 1, 4.1.2005, p. 8.
[3] OJ L 285, 25.10.1985, p. 39. Regulation amended by Regulation (EC) No 3127/94 (OJ L 330, 21.12.1994, p. 43).
--------------------------------------------------
ANNEX
In the Annex to Decision 2005/1/EC the following Part 5 and Part 6 are added:
"PART 5
Ultra-sound IS-D-05
1. Grading of pig carcasses shall be carried out by use of the method termed "Ultra-sound IS-D-05".
2. The grading apparatus IS-D-05 is a device for measuring the lean meat and fat thickness on the basis of response analysis of the range of ultrasonic impulses which are transmitted in sequence into the carcass in the specified place. Ultrasonic probe scans treated carcasses with the set of 3 × 100 ultrasonic impulses at frequency of 4 MHz in the time of scanning.
3. The lean meat content of the carcass shall be calculated according to the following formula:
+++++ TIFF +++++
= 60,69798 – 0,89211 S (IS-D-05) + 0,10560 M (IS-D-05)
where:
+++++ TIFF +++++
= the estimated percentage of lean meat in the carcass,
S (IS-D-05) = backfat thickness including skin (mm) at the point of measurement at 70 mm off the midline of the carcass between the second and the third last ribs (mm),
M (IS-D-05) = muscle depth at the point of measurement (mm).
The formula shall be valid for carcasses weighing between 60 and 120 kilograms.
PART 6
Needle IS-D-15
1. Grading of pig carcasses shall be carried out by use of the method termed "Needle IS-D-15".
2. The grading apparatus IS-D-15 is a device using a sharp needle probe which is inserted into a given place of the carcass; the insertion depth is about 140 mm. A special engineered optical set is located behind the needle that by the means of an optical channel enlightens the surrounding tissue or area and scans the amount of reflected light energy with the defined wavelength. The terminal is also equipped with a precise contactless measuring device which determines the current insertion depth with 46 micrometer precision.
3. The lean meat content of the carcass shall be calculated according to the following formula:
+++++ TIFF +++++
= 60,92452 – 0,77248 S (IS-D-15) + 0,11329 M (IS-D-15)
where:
+++++ TIFF +++++
= the estimated percentage of lean meat in the carcass,
S (IS-D-15) = backfat thickness including skin (mm) at the point of measurement at 70 mm off the midline of the carcass between the second and the third last ribs (mm),
M (IS-D-15) = muscle depth at the point of measurement (mm).
The formula shall be valid for carcasses weighing between 60 and 120 kilograms"
--------------------------------------------------
