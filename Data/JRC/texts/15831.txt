Decision No 1/2006 of the EU-Chile Association Council
of 24 April 2006
eliminating customs duties on wines, spirit drinks and aromatised drinks listed in Annex II to the EU-Chile Association Agreement
(2006/462/EC)
THE ASSOCIATION COUNCIL,
Having regard to the Agreement establishing an Association between the European Community and its Member States, of the one part, and the Republic of Chile, of the other part, signed in Brussels on 18 November 2002 (hereafter referred to as the "Association Agreement"), and in particular Article 60(5) thereof,
Whereas:
(1) Article 60(5) of the Association Agreement empowers the Association Council to take decisions to accelerate the elimination of customs duties more rapidly than is provided for in Article 72, or otherwise improve the conditions of access established therein.
(2) Such decisions supersede the terms established in Article 72 for the product concerned,
HAS DECIDED AS FOLLOWS:
Article 1
Chile shall eliminate the customs duties applicable to wines, spirit drinks and aromatised drinks listed in Annex II to the Association Agreement originating in the Community, as provided for in the Annex to this Decision.
Article 2
This Decision supersedes the terms established in Article 72 of the Association Agreement with respect to imports into Chile of the products concerned.
Article 3
This Decision shall enter into force sixty days after the day of its adoption.
Done at Brussels, 24 April 2006.
For the Association Council
The Chairman
--------------------------------------------------
ANNEX
Products for which Chile eliminates customs duties on goods originating in the Community on the date of entry into force of this Decision:
Partida S.A. | Glosa | Base | Categoría |
2204 | Vino de uvas frescas, incluso encabezado, mosto de uva, excepto el de la partida 2009 | | |
22041000 | – Vino espumoso | 6 | Year 0 |
| – los demás vinos; mosto de uva en el que la fermentación se ha impedido o cortado añadiendo alcohol: | | |
220421 | – – en recipientes con capacidad inferior o igual a 2 l: | | |
| – – – Vinos blancos con denominación de origen: | | |
22042111 | – – – – Sauvignon blanc | 6 | Year 0 |
22042112 | – – – – Chardonnay | 6 | Year 0 |
22042113 | – – – – Mezclas | 6 | Year 0 |
22042119 | – – – – los demás | 6 | Year 0 |
| – – – Vinos tintos con denominación de origen: | | |
22042121 | – – – – Cabernet sauvignon | 6 | Year 0 |
22042122 | – – – – Merlot | 6 | Year 0 |
22042123 | – – – – Mezclas | 6 | Year 0 |
22042129 | – – – – los demás | 6 | Year 0 |
22042130 | – – – los demás vinos con denominación de origen | 6 | Year 0 |
22042190 | – – – los demás | 6 | Year 0 |
220429 | – – los demás: | | |
| – – – Mosto de uva fermentado parcialmente y, apagado con alcohol (incluidas las mistelas): | | |
22042911 | – – – – Tintos | 6 | Year 0 |
22042912 | – – – – Blancos | 6 | Year 0 |
22042919 | – – – – los demás | 6 | Year 0 |
| – – – los demás: | | |
22042991 | – – – – Tintos | 6 | Year 0 |
22042992 | – – – – Blancos | 6 | Year 0 |
22042999 | – – – – los demás | 6 | Year 0 |
220430 | – los demás mostos de uva: | | |
| – – Tintos: | | |
22043011 | – – – Mostos concentrados | 6 | Year 0 |
22043019 | – – – los demás | 6 | Year 0 |
| – – Blancos: | | |
22043021 | – – – Mostos concentrados | 6 | Year 0 |
22043029 | – – – los demás | 6 | Year 0 |
22043090 | – – los demás | 6 | Year 0 |
2205 | Vermut y demás vinos de uvas frescas preparados con plantas o sustancias aromáticas | | |
220510 | – en recipientes con capacidad inferior o igual a 2 l: | | |
22051010 | – – vinos con pulpa de fruta | 6 | Year 0 |
22051090 | – – los demás | 6 | Year 0 |
22059000 | – los demás | 6 | Year 0 |
22060000 | Las demás bebidas fermentadas (por ejemplo: sidra, perada, aguamiel); mezclas de bebidas fermentadas y mezclas de bebidas fermentadas y bebidas no alcohólicas, no expresadas ni comprendidas en otra parte | 6 | Year 0 |
2207 | Alcohol etílico sin desnaturalizar con grado alcohólico volumétrico superior o igual al 80 % vol; alcohol etílico y aguardiente desnaturalizados, de cualquier graduación | | |
22071000 | – Alcohol etílico sin desnaturalizar con grado alcohólico volumétrico superior o igual al 80 % vol | 6 | Year 0 |
22072000 | – Alcohol etílico y aguardiente desnaturalizados, de cualquier graduación | 6 | Year 0 |
2208 | Alcohol etílico sin desnaturalizar con grado alcohólico volumétrico inferior al 80 % vol; aguardientes, licores y demás bebidas espirituosas | | |
220820 | – Aguardiente de vino o de orujo de uvas: | | |
22082010 | – – de uva (pisco y similares) | | |
ex22082010 | – – – Cognac, Amagnac, Grappa y Brandy de Jerez | 6 | Year 0 |
ex22082010 | – – – los demás | 6 | Year 0 |
22082090 | – – los demás | | |
ex22082090 | – – – Cognac, Amagnac, Grappa y Brandy de Jerez | 6 | Year 0 |
ex22082090 | – – – los demás | 6 | Year 0 |
220830 | – Whisky: | | |
22083010 | – – de envejecimiento inferior o igual a 6 años | 6 | Year 0 |
22083020 | – – de envejecimiento superior a 6 años pero inferior o igual a 12 años | 6 | Year 0 |
22083090 | – – los demás | 6 | Year 0 |
220840 | – Ron y demás aguardientes de caña: | | |
22084010 | – – Ron | 6 | Year 0 |
22084090 | – – los demás | 6 | Year 0 |
220850 | – "Gin" y ginebra: | | |
22085010 | – – "Gin" | 6 | Year 0 |
22085020 | – – Ginebra | 6 | Year 0 |
22086000 | – Vodka | 6 | Year 0 |
22087000 | – Licores | 6 | Year 0 |
220890 | – los demás: | | |
22089010 | – – Tequila | 6 | Year 0 |
22089090 | – – los demás | 6 | Year 0 |
--------------------------------------------------
