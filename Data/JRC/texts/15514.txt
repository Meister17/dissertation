Judgment of the Civil Service Tribunal (Third Chamber) of 28 June 2006 –Beau v Commission
(Case F-39/05) [1]
Parties
Applicant(s): Yolande Beau (Paris, France) (represented by: G. Vandersanden and L. Levi, lawyers)
Defendant: Commission of the European Communities (represented by: J. Currall and K. Herrmann, agents, assisted by F. Longfils, lawyer)
Re:
Annulment of the decision of 3 August 2004 in which the Commission of the European Communities refused to accede to the request for recognition of the occupational origin of the applicant's disease and charging to her the fees and incidental costs of the doctor nominated by her and half of the fees and incidental costs of the third doctor
Operative part of the judgment
The Tribunal:
1. Dismisses the action.
2. Orders each of the parties to bear its own costs.
[1] OJ C 205 of 20.08.2005 (case initially registered before the Court of First Instance of the European Communities under number T-215/05 and transferred to the European Civil Service Tribunal by order of 15.12.2005).
--------------------------------------------------
