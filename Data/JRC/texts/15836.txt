Model cards issued by the Ministries of Foreign Affairs of Member States to accredited members of diplomatic missions and consular representations and members of their families, as referred to in Article 19(2) of Regulation (EC) No 562/2006 of the European Parliament and of the Council of 15 March 2006 establishing a Community Code on the rules governing the movement of persons across borders (Schengen Borders Code)
(2006/C 247/05)
BELGIUM
Carte d'identité diplomatique
Diplomatieke identiteitskaart
Diplomatischer Personalsausweis
Diplomatic Identity Card
Front
Back
+++++ TIFF +++++
+++++ TIFF +++++
+++++ TIFF +++++
Carte d'identité consulaire
Consulaire identiteitskaart
Konsularer Personalsausweis
Consular Identity Card
Front
Back
+++++ TIFF +++++
+++++ TIFF +++++
+++++ TIFF +++++
Carte d'identité spéciale (bleue)
Bijzondere identiteitskaart (blauw)
Besonderer Personalsausweis (blau)
Special Identity Card (blue)
Front
Back
+++++ TIFF +++++
+++++ TIFF +++++
+++++ TIFF +++++
Carte d'identité spéciale (rouge)
Bijzondere identiteitskaart (rood)
Besonderer Personalsausweis (rot)
Special Identity Card (red)
Front
Back
+++++ TIFF +++++
+++++ TIFF +++++
+++++ TIFF +++++
Pièce d'identité pour enfant d'étranger privilégié
Identiteitsbewijs voor een kind van een bevoorrecht vreemdeling
Identitätsdokument für ein Kind eines bevorrechtigten Ausländers
Identity document for children of privileged foreigners
Front
Back
+++++ TIFF +++++
+++++ TIFF +++++
+++++ TIFF +++++
CZECH REPUBLIC
ID cards are issued to privileged persons of diplomatic and consular authorities and international organisations, and their family members in the Czech Republic. The status of the privileged person is indicated on the ID card as a code.
The following codes are used:
Embassies
Status | Code |
diplomatic staff | D |
technical and administrative staff | ATP |
service staff | SP |
private domestic staff | SSO |
Consulates
Status | Code |
consular staff | K |
technical and administrative staff | KZ |
service staff | SP/K |
private domestic staff | SP/K |
Honorary consulates
Status | Code |
honorary consul | HK |
International organisations in the Czech Republic
Status | Code |
staff treated as diplomatic staff | MO/D |
official staff | MO |
technical and administrative staff | MO/ATP |
The ID card is a paper card laminated in foil (105 x 74 mm). On the front it bears a photograph of the holder and states his/her name, nationality, date of birth, sex, function, address and the date of expiry of the ID card. On the back it states that the ID card is an official document and a proof of identity, valid only in the Czech Republic.
Front
+++++ TIFF +++++
Back
+++++ TIFF +++++
It should be noted that this identity card does not constitute the entitlement to entry the territory of the Czech Republic without a visa, since this identity card is not a proof of possession of a Czech residence permit.
DENMARK
Stickers:
- Sticker E (pink/white sticker).
Diplomatisk visering/Diplomatic Residence Permit — issued to accredited diplomats and their family members, and to staff of international organisations in Denmark of equivalent rank. Valid for stay and multiple entries, so long as the individual remains diplomatically accredited in Copenhagen.
+++++ TIFF +++++
- Sticker F (pink/white sticker).
Opholdstilladelse/Residence Permit — issued to technical/administrative staff in post and to their family members, and to diplomats' household staff holding service passports from the Ministry of Foreign Affairs of the country of origin. Also issued to staff of international organisations in Denmark of equivalent rank. Valid for stay and multiple entries, for as long as the posting lasts.
+++++ TIFF +++++
- Sticker S (pink/white sticker)(combined with sticker E or F).
Residence permit for accompanying close relatives if included in the passport.
+++++ TIFF +++++
Identity cards
— Red cards
R-nr. = identity card number
D-nr. = diplomat's identity number
I-nr. = identity number for the highest ranking officials of international organisations
+++++ TIFF +++++
— Green cards
G-nr. = identity card number
T-nr. = identity number for the technical/administrative staff of an Embassy
I-nr. = identity number for the technical/administrative staff of an international organisation
+++++ TIFF +++++
— White cards
H-nr. = identity card number
L-nr. = identity number for locally employed staff at Embassies
I-nr. = dentity number for locally employed staff of international organisations
S-nr. = identity number for service staff (drivers, household staff, etc.)
+++++ TIFF +++++
It should be noted that the identity cards issued by the Ministry of Foreign Affairs to foreign diplomats, technical/administrative staff, domestic staff, etc., do not give entitlement to entry without a visa, since these identity cards are not proof of possession of a Danish residence permit.
GERMANY
1. The Ministry of Foreign Affairs (Protocol Section) issues the following ID cards to diplomatic staff on request:
(a) Red diplomatic ID cards
These ID cards are issued to diplomats and family members making up their respective households;
+++++ TIFF +++++
(b) Blue ID cards
These ID cards are issued to technical and administrative staff and departmental staff at diplomatic missions and family members making up their respective households;
+++++ TIFF +++++
(c) Green ID Cards
These ID cards are issued to private domestic staff of staff working at diplomatic missions, provided that they do not have their permanent residence in the Federal Republic of Germany;
+++++ TIFF +++++
(d) Yellow ID cards
These ID cards are issued to staff working at diplomatic missions who have their permanent residence in the Federal Republic of Germany and family members making up their respective households, provided that they do not hold German nationality within the meaning of the Basic Law (Grundgesetz). Yellow ID cards are also issued to persons who entered the Federal Republic of Germany in possession of a valid visa to work at a diplomatic mission and who were not accredited by their government;
+++++ TIFF +++++
2. The Ministry of Foreign Affairs (Protocol Section) also issues the following documents on request:
(a) Pink ID cards
These ID cards are issued to staff without privileges of the Russian Federation's trade representation and of the trade departments of the Czech and Slovak embassies as well as family members making up their respective households;
+++++ TIFF +++++
(b) Special dark red ID cards
These ID cards are issued to foreign employees of representations of international and supranational organisations and institutions, as well as intergovernmental organisations, working permanently in Germany and family members making up their respective households who are foreign nationals.
+++++ TIFF +++++
3. The competent authorities at Länder level issue the following documents on request:
(a) White ID cards
These ID cards are issued to staff of the consular corps, consular staff and family members making up their respective households;
+++++ TIFF +++++
(b) Grey ID cards
These ID cards are issued to other employees and family members making up their respective households;
+++++ TIFF +++++
(c) White ID cards crossed with green lines
These ID cards are issued to honorary consular officials;
+++++ TIFF +++++
(d) Yellow ID cards
These ID cards are intended for members of consular representations (local staff) who have their permanent residence in the Federal Republic of Germany and family members making up their respective households, provided that these persons do not hold German nationality within the meaning of the Basic Law (Grundgesetz);
+++++ TIFF +++++
(e) Green ID cards
These ID cards are issued to private domestic staff of accredited consular officials, provided that they do not have their permanent residence in the Federal Republic of Germany.
+++++ TIFF +++++
Given that the abovementioned categories of persons do not have their permanent residence in the Federal Republic of Germany, to enter German territory they must be in possession of a valid residence permit in the form of a visa authorising them solely to work for an accredited consular official. A residence permit is issued solely on this basis and for a duration of one year. It can be extended several times, but may not exceed the duration of the employer's assignment. When the residence permit expires, the private domestic employee must leave the country and may not change employers without having lodged a new request abroad.
- New residence permits in card format (ID card format) issued by the Ministry of Foreign Affairs:
- Diplomatenausweis (diplomatic ID card) and — Diplomatenausweis Art. 38 WÜD (diplomatic ID card pursuant to Article 38 of the Vienna Convention on Diplomatic Relations)
- These documents are equivalent to the old red diplomat's passes and bear the letter "D" on the back.
- Protokollausweis für Verwaltungspersonal (protocol pass for administrative staff)
- This document is equivalent to the old blue pass for seconded members of the administrative and technical staff of the missions and bears the letters "VB" on the back.
- Protokollausweis für dienstliches Hauspersonal (protocol pass for service staff)
- This document is equivalent to the old blue pass for seconded members of the service staff of the missions and bears the letters "DP" on the back.
- Protokollausweis für Ortskräfte (protocol pass for local staff)
- This document is equivalent to the old yellow pass for locally hired employees of the missions and bears the letters "OK" on the back.
- Protokollausweis für privates Hauspersonal (protocol pass for private domestic staff)
- This document is equivalent to the old green pass for private domestic staff of seconded members of the missions and bears the letters "PP" on the back.
- Sonderausweis für Mitarbeiter internationaler Organisationen (Special card issued to members of staff of international organisations)
- This document is equivalent to the old dark red special pass for members of staff of international organisations and bears the letters "IO" on the back.
The respective privileges are indicated on the back of the pass.
ESTONIA
Diplomatic and service cards
1) to diplomats and consular officers and their family members — BLUE:
- Cat A — Head of Mission,
- Cat. B — Members of the diplomatic staff,
+++++ TIFF +++++
2) to members of the administrative and technical staff and their family members — RED:
- Cat. C — Members of the administrative and technical staff
+++++ TIFF +++++
3) to members of the service staff, private servants and their family members and to local employees — GREEN:
- Cat. D — Members of service staff; Cat. E — Private servants; Cat. F — Local employees (Estonian citizens or residents)
+++++ TIFF +++++
+++++ TIFF +++++
4) to honorary consular officers of foreign countries in the Republic of Estonia — GREY:
- Cat. HC — Honorary consular officers.
+++++ TIFF +++++
The data on the front side of the diplomatic and service card is the following:
- title of the card (diplomatic or service card)
- name of the bearer
- date of birth
- photo
- signature
- stamp of the Protocol Department.
The reverse side contains the following data:
- issuing authority (Ministry of Foreign Affairs)
- name of the embassy
- position of the bearer
- extent of the immunity
- date of issue
- valid until
- serial no.
General features of all cards issued by Estonia:
The card is laminated in plastic foil. The photo and the signature are scanned onto the front side. Watermark of the national coat of arms is on the reverse side.
Family members are the following dependants of a diplomat and sharing a common household with him/her:
1. spouse;
2. an unmarried child of up to 21 years of age;
3. an unmarried child of up to 23 years of age, studying in a higher educational institution;
4. another family member in special cases.
A diplomatic and service card shall not be issued if the assignment period is shorter than six (6) months.
GREECE
1. Diplomatic staff
+++++ TIFF +++++
2. Administrative and technical Staff of the diplomatic missions
+++++ TIFF +++++
3. Service Staff
+++++ TIFF +++++
4. Consular officers
+++++ TIFF +++++
5. Consular Employees
+++++ TIFF +++++
6. Honorary consular Officers
+++++ TIFF +++++
7. Staff of international Organisations
+++++ TIFF +++++
SPAIN
Pass 1 (red)
Special pass with the following inscription
"Cuerpo Diplomático" "Diplomatic Corps"
"Embajador" "Ambassador"
"Documento de Identidad" "Identity Document"
issued by the Ministry of Foreign Affairs to all ambassadors accredited to the Kingdom of Spain.
+++++ TIFF +++++
Passes 2 and 3 (red)
Special passes with the following inscription
"Cuerpo Diplomático" "Diplomatic Corps"
"Documento de Identidad" "Identity Document"
issued by the Ministry of Foreign Affairs to staff accredited to all diplomatic missions who have diplomatic status.
+++++ TIFF +++++
The indication "F" is included on the pass of the spouse and children aged between 12 and 23.
+++++ TIFF +++++
Passes 4 and 5 (yellow)
Special passes with the following inscription
"Misiones Diplomáticas" "Diplomatic Missions"
"Personal administrativo y técnico" "Administrative and Technical Staff"
"Documento de Identidad" "Identity Document"
issued by the Ministry of Foreign Affairs to administrative officials in all accredited diplomatic missions, excluding Spanish nationals and ordinary residents.
+++++ TIFF +++++
The indication "F" is included on the pass of the spouse and children aged between 12 and 23.
+++++ TIFF +++++
Passes 6 and 7 (red)
Special passes with the following inscription
"Tarjeta de Identidad" "Identity Document"
issued by the Ministry of Foreign Affairs to accredited staff with certain privileges in the Office of the Palestinian General Mission.
+++++ TIFF +++++
The indication "F" is included on the pass of the spouse and children aged between 12 and 23.
+++++ TIFF +++++
Passes 8 and 9 (red)
Special passes with the following inscription
"Tarjeta Diplomática de Identidad" "Diplomatic Identity Pass"
issued by the Ministry of Foreign Affairs to staff who have diplomatic status at the Office of the League of Arab Nations.
+++++ TIFF +++++
The indication "F" is included on the pass of the spouse and children aged between 12 and 23.
+++++ TIFF +++++
Passes 10 and 11 (red)
Special passes with the following inscription
"Organismos Internacionales" "International Organisations"
"Estatuto Diplomático" "Diplomatic Status"
"Documento de Identidad" "Identity Document"
issued by the Ministry of Foreign Affairs to staff who have diplomatic status accredited to international organisations.
+++++ TIFF +++++
The indication "F" is included on the pass of the spouse and children aged between 12 and 23.
+++++ TIFF +++++
Passes 12 and 13 (blue)
Special passes with the following inscription
"Organismos Internacionales" "International Organisations"
"Personal administrativo y técnico" "Administrative and Technical Staff"
"Documento de Identidad" "Identity Document"
issued by the Ministry of Foreign Affairs to administrative officials accredited to international organisations.
+++++ TIFF +++++
The indication "F" is included on the pass of the spouse and children aged between 12 and 23.
+++++ TIFF +++++
Passes 14 and 15 (green)
Special passes with the following inscription
"Funcionario Consular de Carrera" "Career Consular Official"
"Documento de Identidad" "Identity Document"
issued by the Ministry of Foreign Affairs to career consular officials accredited in Spain.
+++++ TIFF +++++
The indication "F" is included on the pass of the spouse and children aged between 12 and 23.
+++++ TIFF +++++
Passes 16 and 17 (green)
Special passes with the following inscription
"Empleado consular" "Consular Employee"
"Expedido a favor de …" "Issued to ..."
"Documento de Identidad" "Identity Document"
issued by the Ministry of Foreign Affairs to consular administrative officials accredited in Spain.
+++++ TIFF +++++
The indication "F" is included on the pass of the spouse and children aged between 12 and 23.
+++++ TIFF +++++
Passes 18 and 19 (grey)
Special passes with the following inscription
"Personal de Servicio" "Service Staff"
"Misiones Diplomáticas, Oficinas Consulares y Organismos Internacionales" "Diplomatic Missions, Consular Posts and International Organisations"
"Expedido a favor de …" "Issued to ..."
"Documento de Identidad" "Identity Document"
issued by the Ministry of Foreign Affairs to service staff in diplomatic missions, consular posts and international organisations and career diplomatic or consular staff (private domestic staff).
+++++ TIFF +++++
The indication "F" is included on the pass of the spouse and children aged between 12 and 23.
+++++ TIFF +++++
GENERAL FEATURES
1. Documents 1 to 15:
Hard cover (imitation leather effect) with a three-part fold away section for extensions.
On the front the Spanish coat of arms is towards the top and the various inscriptions, all of which are gold-blocked, are in the centre and towards the bottom.
The usual size of the coat of arms is 25 x 25 mm, except for documents 4, 5, 12 and 13, where the size is 17 x 17 mm.
These documents are issued manually and filled in by hand. The photograph of the holder is glued in and bears the stamp of the Dirección General de Protocolo (Directorate-General for Protocol) in one corner.
The documents are valid for two to three years (printed on the document) and can be extended annually up to three times after the first date of expiry.
There are no special security features.
Document sizes:
Documents 1 to 5 and 7 to 15 measure 115 x 77 mm.
They may also be issued to minors below the age of 12 who hold their own individual passports.
2. Documents 16 to 19
Card, folded down the centre, four sections in total.
The front displays the Spanish coat of arms (17 x 17 mm) with the printed text below. The holder's name is on a dotted line. All entries are in black ink.
These documents are issued manually and either filled in by hand or typewritten. The photograph of the holder is glued in and bears the stamp of the Dirección General de Protocolo (Directorate-General for Protocol).
The documents are valid for two years (printed on the document) and can be extended annually no more than twice.
Space is reserved on the back for extensions.
Documents 18 and 19 are issued both to private domestic staff and to service staff. The status of the holder is indicated on the lefthand inside page.
Document sizes:
Documents 16 to 19 measure 115 x 75 mm.
FRANCE
White
+++++ TIFF +++++
Orange
+++++ TIFF +++++
White
+++++ TIFF +++++
Blue
+++++ TIFF +++++
Green
+++++ TIFF +++++
Green
+++++ TIFF +++++
Beige
+++++ TIFF +++++
Grey
+++++ TIFF +++++
Grey
+++++ TIFF +++++
Blue-grey
+++++ TIFF +++++
ITALY
ID card No 1
ID card — diplomatic corps
This is the ID card issued by the Ministry of Foreign Affairs (Protocol Section) to members of the diplomatic corps.
This ID card, which bears a photograph of the holder and states its validity on the back, is a document providing identification in all the situations laid down by law and exempts the holder from the compulsory registration of residence with the authorities.
+++++ TIFF +++++
ID card No 2
ID card — international organisations and special foreign missions
This is the ID card issued by the Ministry of Foreign Affairs (Protocol Section) to staff of international organisations and special foreign missions.
This ID card, which bears a photograph of the holder and states its validity on the back, covers the duration of the assignment and is issued for a maximum period of five years; it is a document providing identification in all the situations laid down by law and exempts the holder from the compulsory registration of residence with the authorities.
+++++ TIFF +++++
ID card No 3
ID card — diplomatic representations
This is the ID card issued by the Ministry of Foreign Affairs (Protocol Section) to staff of diplomatic representations.
This ID card, which bears a photograph of the holder and states its validity on the back, covers the duration of the assignment and is issued for a maximum period of two years; it is a document providing identification in all the situations laid down by law and exempts the holder from compulsory registration of residence with the authorities.
+++++ TIFF +++++
ID card No 4
ID card — consular corps
This is the ID card issued by the Ministry of Foreign Affairs (Protocol Section) to staff of the consular corps.
This ID card, which bears a photograph of the holder and states its validity on the back, covers the duration of the assignment and is issued for a maximum period of five years; it is a document providing identification in all the situations laid down by law and exempts the holder from compulsory registration of residence with the authorities.
+++++ TIFF +++++
ID card No 5
ID card — Consulates (consular staff)
This is the ID card issued by the Ministry of Foreign Affairs (Protocol Section) to staff working at consulates abroad.
This ID card, which bears a photograph of the holder and states its validity on the back, is a document providing identification in all the situations laid down by law and exempts the holder from the compulsory registration of residence with the authorities.
+++++ TIFF +++++
Carta d'identità M.A.E.
(Identity card issued by the Ministry of Foreign Affairs)
- Mod. 1 (blu) Corpo diplomatico accreditato e consorti titolari di passaporto diplomatico
(Model 1 (blue) Accredited members of the diplomatic corps and their spouses who hold a diplomatic passport)
- Mod. 2 (verde) Corpo consolare titolare di passaporto diplomatico
(Model 2 (green) Members of the consular corps who hold a diplomatic passport)
- Mod. 3 (arancione) Funzionari II FAO titolari di passaporto diplomatico, di servizio o ordinario
(Model 3 (orange) Category II FAO officials who hold a diplomatic, service or ordinary passport)
- Mod. 4 (arancione) Impiegati tecnico-amministrativi presso Rappresentanze diplomatiche titolari di passaporto di servizio
(Model 4 (orange) Technical and administrative staff of diplomatic representations who hold a service passport)
- Mod. 5 (arancione) Impiegati consolari titolari di passaporto di servizio
(Model 5 (orange) Consular staff who hold a service passport)
- Mod. 7 (grigio) Personale di servizio presso Rappresentanze diplomatiche titolare di passaporto di servizio
(Model 7 (grey) Domestic staff of diplomatic representations who hold a service passport)
- Mod. 8 (grigio) Personale di servizio presso Rappresentanze Consolari titolare di passaporto di servizio
(Model 8 (grey) Domestic staff of consular representations who hold a service passport)
- Mod. 11 (beige) Funzionari delle Organizzazioni internazionali, Consoli Onorari, impiegati locali, personale di servizio assunto all'estero e venuto al seguito, familiari Corpo Diplomatico e Organizzazioni Internazionali titolari di passaporto ordinario
(Model 11 (beige) Officials of international organisations, honorary consuls, local employees, domestic staff recruited abroad who have followed their employer, families of members of the diplomatic corps and international organisations who hold an ordinary passport)
NB: Models 6 (orange) and 9 (green) for, respectively, staff of international organisations who have no immunity and for foreign honorary consuls, are no longer issued and have been replaced by specimen 11. However, these documents remain valid until the expiry date stated on them.
CYPRUS
+++++ TIFF +++++
The Identity Card for Members of the Diplomatic Corps of Cyprus is issued to diplomats and their family members, it is folded in the middle (letters on the inside), its colour is dark blue and the size is 11 cm x 14,5 cm. The reverse side of the document is a dark blue cover.
+++++ TIFF +++++
The Identity Card for Members of Other Foreign Missions in Cyprus is issued to United Nations personnel in Cyprus and their family members, it is folded in the middle (the front side appears in the upper part of the graphic displayed above and the reverse side appears in the lower part), its colour is light green and its size is 20,5 cm x 8 cm.
+++++ TIFF +++++
The Identity Card for Non-Diplomatic Personnel of Foreign Missions in Cyprus is issued to technical and administrative personnel of foreign diplomatic missions in Cyprus and their family members, it is folded in the middle (the front side appears in the upper part of the graphic displayed above and the reverse side appears in the lower part), its colour is light blue and its size is 20,5 cm x 8 cm.
LATVIA
1. Card of identification for Ambassador
Front
Back
+++++ TIFF +++++
According to the Vienna Convention on Diplomatic Relations ( 18 April 1961) this card includes all the privileges and immunities.
2. Card of identification for driver
Front
Back
+++++ TIFF +++++
According to the Vienna Convention on Diplomatic Relations ( 18 April 1961), Article 37(3), this card includes partial privileges and immunities provided to embassy's service staff.
3. Card of identification for consul general
Front
Back
+++++ TIFF +++++
According to the Vienna Convention on Diplomatic Relations ( 18 April 1961) this card includes all the privileges and immunities.
4. Card of identification for honorary Consul general
Front
Back
+++++ TIFF +++++
This card includes the following privileges and immunities: juridical immunity; the right to contact citizens of the represented country; the right to contact institutions of the country, in accordance with the law of the country of residence; the right to collect fees according to the law of the represented country and of the country of residence. In addition, all the goods imported for the needs of the consular institution should be imported duty-free and the building of the consular institution should be tax-free.
5. Card of identification for domestic servant
Front
Back
+++++ TIFF +++++
According to the Vienna Convention on Diplomatic Relations ( 18 April 1961), Article 37(4), this card includes partial privileges and immunities provided to embassy's service staff.
6. Card of identification for Secretary
Front
Back
+++++ TIFF +++++
According to the Vienna Convention on Diplomatic Relations ( 18 April 1961), Article 37(2), this card includes partial privileges and immunities provided to administrative and technical employees.
7. Card of identification for Head (International Organisations)
Front
Back
+++++ TIFF +++++
This card includes all the privileges and immunities according to the agreement between the Republic of Latvia and the appropriate international organisation.
LITHUANIA
+++++ TIFF +++++
+++++ TIFF +++++
LUXEMBOURG
+++++ TIFF +++++
+++++ TIFF +++++
+++++ TIFF +++++
+++++ TIFF +++++
+++++ TIFF +++++
+++++ TIFF +++++
HUNGARY
The Ministry of Foreign Affairs issues the following ID cards to members of the staff of foreign representations. The coloured stripe at the middle of the ID card indicates the different categories:
(a) Blue stripe
These ID cards are issued to diplomats and their family members and to officials of international organisations holding diplomatic rank and to their family members
+++++ TIFF +++++
(b) Green stripe
These ID cards are issued to technical and administrative staff of the diplomatic missions and to their family members and to officials of international organisations holding a technical and administrative rank and to their family members
+++++ TIFF +++++
(c) Pink stripe
These ID cards are issued to members of consular representations and to their family members
+++++ TIFF +++++
(d) Yellow stripe
These ID cards are issued to members of the service staff of diplomatic or consular missions or representations of international organisations and to their family members
+++++ TIFF +++++
(e) Light green stripe
These ID cards are issued to members of representations of foreign countries and international organisations and to their family members, certifying limited diplomatic privileges and immunities
+++++ TIFF +++++
The data on the front side of the ID cards is the following:
- Name
- Representation
- Rank
- Serial Number
- Validity
- Photo
The reverse side contains the following data:
- Identity Card
- Date of birth
- Address
- Notes:
1. Note regarding immunity
2. Date of entry into service
- Space for BAR code (No BAR code used yet)
- Data registry identification number
- General features of the cards:
The document is laminated in foil (the image of the foil can be seen below). It is waterproof, cannot be altered without causing damage and it has the following security elements: uv content, micro-letter protection, metallised paper.
+++++ TIFF +++++
MALTA
+++++ TIFF +++++
+++++ TIFF +++++
+++++ TIFF +++++
+++++ TIFF +++++
+++++ TIFF +++++
Applications for the identity cards should be submitted by Note Verbale to the Protocol Office, Consular and Information Directorate, Ministry of Foreign Affairs accompanied by two recent photos of the person concerned. The Note Verbale should include the designation of the applicant, clearly indicating whether the applicant is a diplomat or technical staff. Identity cards should be returned to the Protocol Office, Consular and Information Directorate on termination of the tour of duty of the person concerned.
Five different identity cards are used by the Protocol Office, Consular and Information Directorate to indicate whether the bearer is a diplomat, technical staff member, Honorary Consul of Malta, Honorary Consul of another country represented in Malta or a staff member of an International Organisation. Each identity card is valid for a period of four years.
1. Diplomats
Black identity cards indicate that the bearer is a Diplomat. This identity card is issued to the diplomatic staff of a foreign mission and their spouses as well as to their children over 18 years of age still forming part of the same household.
2. Consular
There are two kinds of consular identity cards, one green and one brown. The Green identity card is issued to Honorary Consuls of Malta abroad whilst the Brown identity card is issued to Honorary Consuls representing foreign countries in Malta.
3. Administrative and technical staff
A Blue identity card is issued to the technical and administrative staff and family members of a mission accredited to Malta.
4. Staff of International Organisations
A Maroon card is issued to staff of International Organisations and their family members who form part of the same household.
The technical features of the identity cards issued by the Ministry of Foreign Affairs are:
(a) Identity card numbers which are registered;
(b) Signature of the authorised Protocol officer; and
(c) Lamination of the identity cards so as to reduce the possibility of forgery.
NETHERLANDS
Statuses
Each privileged person is assigned a status indicating to which category of privileged persons he or she belongs. This status is shown on the Privileged Persons document by means of a code.
The following codes are used:
Embassies
STATUS | CODE |
diplomatic staff | AD |
technical and administrative staff | BD |
service staff | ED |
private domestic staff | PD |
Consulates
STATUS | CODE |
consular staff | AC |
technical and administrative staff | BC |
service staff | EC |
private domestic staff | PC |
International organisations in the Netherlands
STATUS | CODE |
staff treated as diplomatic staff | AO |
technical and administrative staff | BO |
service staff | EO |
private domestic staff | POS |
pecial cases
Where identity documents are issued to Dutch nationals or aliens permanently resident in the Netherlands, the following codes are added to the indication of residence status:
- NL for Dutch nationals;
- DV for permanent residents.
+++++ TIFF +++++
AUSTRIA
ID cards for persons who enjoy special privileges and immunities
The Federal Ministry of Foreign Affairs issues ID cards in the following colours:
- red ID cards for persons who have diplomatic status in Austria and their family members;
- yellow ID cards for consuls and their family members;
- blue ID cards for all other persons who enjoy special privileges and immunities in Austria and their family members.
Red ID card for persons who have diplomatic status in Austria and their family members
+++++ TIFF +++++
Yellow ID card for consuls and their family members
+++++ TIFF +++++
Blue ID card for all other persons who enjoy special privileges and immunities in Austria and their family members.
+++++ TIFF +++++
New card formats (gray, brown, blue, green, yellow, orange and red):
+++++ TIFF +++++
+++++ TIFF +++++
+++++ TIFF +++++
+++++ TIFF +++++
+++++ TIFF +++++
+++++ TIFF +++++
+++++ TIFF +++++
POLAND
+++++ TIFF +++++
General features
DOCUMENT: DIPLOMATIC IDENTITY CARD ISSUED BY THE MINISTRY OF FOREIGN AFFAIRS (MSZ)
SIZE: 100 mm x 71 mm
NUMBER OF PAGES: 2 (two)
PERSONAL DATA:
- PROTECTION OF PHOTOGRAPH AGAINST REPLACEMENT:
the safeguards are not disclosed; the photograph is to be attached in the left bottom corner of the front side of the document.
OTHER FEATURES:
- The Ministry of Foreign Affairs initials, MSZ, are displayed in outline against the background of horizontal lines drawn within a circle, 29 mm across.
- The outline of the "M" is devoid of the guilloche pattern. The spaces within the contours of the "S" and the "Z" are lined, the lines within the "S" slanting leftwards, and those within the "Z" slanting rightwards.
LETTERING:
All titles of the fields and information on the back of the document are offset-printed in black ink.
PORTUGAL
+++++ TIFF +++++
+++++ TIFF +++++
+++++ TIFF +++++
+++++ TIFF +++++
+++++ TIFF +++++
+++++ TIFF +++++
+++++ TIFF +++++
+++++ TIFF +++++
SLOVENIA
+++++ TIFF +++++
+++++ TIFF +++++
+++++ TIFF +++++
+++++ TIFF +++++
+++++ TIFF +++++
+++++ TIFF +++++
SLOVAKIA
+++++ TIFF +++++
Identity cards for authorised persons accredited in the Slovak Republic are issued by the Diplomatic Protocol of the Ministry of Foreign Affairs of the Slovak Republic.
Types of identity cards:
1. Type "D" — (red) identity cards issued for diplomats and their family members.
2. Type "ATP" — (blue) identity cards issued for administrative and technical staff and their family members.
3. Type "SP" — (green) identity cards issued for service staff and their family members and for private staff .
4. Type "MO" — (violet) identity cards issued for employees of international organisations and their family members.
5. Type "HK" — (grey) identity cards issued for honorary consular officers.
The front side of the identity card contains:
(a) name, type, number and validity of the identity card,
(b) first name, surname, date of birth, citizenship, sex and position of the identity card holder,
(c) bar code readable by a machine (similar to citizenship cards and passports used by police authorities for identification purposes).
The reverse side of the identity card contains:
(a) explanatory text,
(b) address of the identity card holder,
(c) date of issue,
(d) signature of the card holder,
(e) signature of the director of the diplomatic protocol,
(f) stamp of the Ministry of Foreign Affairs of the Slovak Republic.
The size of its printed form is 99 x 68 mm and it is printed on white security paper with a precisely located multitone watermark as well as an incorporated protective plastic strip and protective fibres. The card is protected by protective thermoplastic foil of 105 x 74 mm, by hot-process lamination.
The following protective elements are incorporated in the card:
(a) watermark,
(b) plastic strip,
(c) protective fibres,
(d) protective under-prints,
(e) protection in infra-red sphere,
(f) optically variable element,
(g) numbering.
FINLAND
The Ministry for Foreign Affairs issues identity cards to the members of missions and to private servants for the term of office or residence in Finland, provided that the period is not shorter than six months. An identity card is not considered to be an official document of identity in Finland. Its purpose is to certify the position and status of the person assigned to serve at a mission. The different types of cards are as follows:
- A-card to diplomatic agents;
- B-card to members of the administrative and technical staff;
- C-card to members of the service staff of missions (such as drivers, housekeepers and cooks);
- D-card to private servants of members of missions, and locally engaged drivers of missions.
In connection with a valid residence permit:
A temporary residence permit (B) in the form of a sticker issued by the Ministry for Foreign Affairs, bearing the indication "diplomaattileimaus" (diplomatic) or "virkaleimaus" (service) or bearing no specific indication.
A. Members of the diplomatic staff and their family members (blue)
Front
+++++ TIFF +++++
Back
+++++ TIFF +++++
B. Members of the administrative and technical staff of missions and their family members (red)
Front
+++++ TIFF +++++
Back
+++++ TIFF +++++
C. Members of the service staff of missions (drivers, housekeepers, cooks, etc.) (yellow)
Front
+++++ TIFF +++++
Back
+++++ TIFF +++++
D. Others (brown)
Front
+++++ TIFF +++++
Back
+++++ TIFF +++++
SWEDEN
+++++ TIFF +++++
+++++ TIFF +++++
ICELAND
The Ministry of Foreign Affairs issues the following identity cards to members of diplomatic and consular authorities:
Blue identity card (see specimen)
This ID card is issued to administrative, technical and service staff working in foreign embassies in Iceland and their family members (spouse and children aged between 12 and 18 years).
The ID card is a laminated plastic card (8,2 cm x 5,4 cm). On the front it bears a photograph of the holder and states his/her name, nationality, and the date of expiry. On the back it states both in Icelandic and English that the ID card must be returned to the Ministry of Foreign Affairs on departure of the holder.
Front
+++++ TIFF +++++
Back
+++++ TIFF +++++
Yellow diplomatic identity card (see specimen)
This ID card is issued to foreign diplomats and their family members (spouse and children aged between 12 and 18 years.
The ID card is a laminated plastic card (8,2 cm x 5,4 cm). On the front it bears a photograph of the holder and states his/her name, nationality, and the date of expiry. On the back it states in Icelandic that the holder of the ID has diplomatic immunity and therefore his travel cannot be restricted in any way. It also states in Icelandic and English that the ID card must be returned to the Ministry of Foreign Affairs on the departure of the holder.
Front
+++++ TIFF +++++
Back
+++++ TIFF +++++
NORWAY
+++++ TIFF +++++
+++++ TIFF +++++
--------------------------------------------------
