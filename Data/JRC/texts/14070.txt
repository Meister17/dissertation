Action brought on 8 March 2006 — Commission of the European Communities v Hellenic Republic
Parties
Applicant: Commission of the European Communities (represented by: G. Zavvos and H. Støvlbæk)
Defendant: Hellenic Republic
Form of order sought
The applicant asks the Court to
- declare that, by failing to bring into force the laws, regulations and administrative provisions, as regards the profession of veterinary surgeon, that are necessary to comply with Directive 2001/19/EC of the European Parliament and of the Council of 14 May 2001 amending Council Directives 89/48/EEC and 92/51/EEC on the general system for the recognition of professional qualifications and Council Directives 77/452/EEC, 77/453/EEC, 78/686/EEC, 78/687/EEC, 78/1026/EEC, 78/1027/EEC, 80/154/EEC, 80/155/EEC, 85/384/EEC, 85/432/EEC, 85/433/EEC and 93/16/EEC concerning the professions of nurse responsible for general care, dental practitioner, veterinary surgeon, midwife, architect, pharmacist and doctor [1], or in any event by failing to inform the Commission thereof, the Hellenic Republic has failed to fulfil its obligations under Article 16 of that Directive;
- order the Hellenic Republic to pay the costs.
Pleas in law and main arguments
In this case Article 16(1) of Directive 2001/19/EC of the European Parliament and of the Council of 14 May 2001 provides that Member States are to bring into force the laws, regulations and administrative provisions necessary to comply with the Directive before 1 January 2003 and that they are to inform the Commission thereof forthwith.
The Commission considers that Greece has not yet taken the measures necessary as regards the profession of veterinary surgeon.
[1] OJ L 206 of 31/07/2001
--------------------------------------------------
