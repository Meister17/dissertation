Commission Regulation (EC) No 1929/2000
of 12 September 2000
amending Regulation (EC) No 2603/1999 laying down rules for the transition to the rural development support provided for by Council Regulation (EC) No 1257/1999 as regards transformation of agri-environmental commitments entered into under Council Regulation (EEC) No 2078/92
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1257/1999 of 17 May 1999 on support for rural development from the European Agricultural Guidance and Guarantee Fund (EAGGF) and amending and repealing certain regulations(1), and in particular Article 53(1) thereof,
Whereas:
(1) Current rules do not permit the transformation of an agri-environment commitment entered into under Council Regulation (EEC) No 2078/92(2), as last amended by Commission Regulation (EC) No 2772/95(3), into a new commitment under new Regulation (EC) No 1257/1999. Article 13 of Commission Regulation (EC) No 746/96(4), as amended by Regulation (EC) No 435/97(5), laying down detailed rules for the application of Regulation (EEC) No 2078/92 states that a commitment may be transformed into another commitment only within the same five-year period.
(2) To guarantee better implementation of the new programming period as regards agri-environmental measures, Member States should be allowed to authorise the transformation of an agri-environmental commitment contracted on the basis of the old rules into a new commitment of five years or more under Regulation (EC) No 1257/1999 provided that the new commitment is definitely beneficial to the environment.
(3) The measures provided for in this Regulation are in accordance with the opinion of the Committee on Agricultural Structures and Rural Development,
HAS ADOPTED THIS REGULATION:
Article 1
At the end of Article 3, the following paragraph is added to Commission Regulation (EC) No 2603/1999(6):
"4. Before the end of the period for performing a commitment entered into under Regulation (EEC) No 2078/92, Member States may authorise the transformation of that commitment into a new commitment for five years or more under Regulation (EC) No 1257/1999 provided that:
(a) any such a transfer is of unequestionable benefit to the environment, and
(b) the existing commitment is significantly reinforced."
Article 2
This Regulation shall enter into force on the 20th day following its publication in the Official Journal of the European Communities.
It shall apply from 1 January 2000.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 12 September 2000.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 160, 26.6.1999, p. 80.
(2) OJ L 215, 30.7.1992, p. 91.
(3) OJ L 288, 1.12.1995, p. 35.
(4) OJ L 102, 25.4.1996, p. 19.
(5) OJ L 67, 7.3.1997, p. 2.
(6) OJ L 316, 10.12.1999, p. 26.
