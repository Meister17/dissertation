Commission Regulation (EC) No 1721/2006
of 21 November 2006
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 22 November 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 21 November 2006.
For the Commission
Jean-Luc Demarty
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 386/2005 (OJ L 62, 9.3.2005, p. 3).
--------------------------------------------------
ANNEX
to Commission Regulation of 21 November 2006 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code [1] | Standard import value |
07020000 | 052 | 71,7 |
204 | 28,5 |
999 | 50,1 |
07070005 | 052 | 144,7 |
204 | 66,2 |
628 | 171,8 |
999 | 127,6 |
07099070 | 052 | 148,5 |
204 | 135,1 |
999 | 141,8 |
08052010 | 204 | 67,5 |
999 | 67,5 |
08052030, 08052050, 08052070, 08052090 | 052 | 73,3 |
400 | 77,8 |
999 | 75,6 |
08055010 | 052 | 45,9 |
388 | 46,4 |
528 | 25,4 |
999 | 39,2 |
08081080 | 388 | 93,6 |
400 | 103,6 |
404 | 99,2 |
720 | 66,8 |
800 | 152,5 |
999 | 103,1 |
08082050 | 052 | 106,4 |
720 | 54,8 |
999 | 80,6 |
[1] Country nomenclature as fixed by Commission Regulation (EC) No 750/2005 (OJ L 126, 19.5.2005, p. 12). Code "999" stands for "of other origin".
--------------------------------------------------
