Commission Decision
of 15 March 2006
on setting up a high level expert group to advise the European Commission on the implementation and the development of the i2010 strategy
(2006/215/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Whereas:
(1) As indicated in the conclusions of the Communication from the Commission entitled "i2010 — A European Information Society for growth and employment" [1] (hereafter i2010), "the Commission will strengthen dialogue with stakeholders and work with Member States to address ICT issues, notably through the open method of coordination". Therefore, the Commission may need to call upon the guidance and expertise of an advisory body with Member State officials specialising in the ICT policy area.
(2) The group must contribute to the efficient implementation of i2010.
(3) The group must be made up of high level representatives from the Member States, and open to observers from acceding and EEA countries.
(4) The "i2010 high level group" has therefore to be set up and its terms of reference and structures must be detailed,
HAS DECIDED AS FOLLOWS:
Article 1
A group of experts, the "i2010 High Level Group", hereinafter referred to as "the Group", is hereby set up by the Commission.
Article 2
Task
The Commission may consult the group on any matter relating to the implementation of the i2010 strategy.
The Group’s task is to:
- discuss strategic ICT policy issues in the context of i2010 and in the wider context of the Lisbon agenda, review the effectiveness of i2010, and give input and advice on possible improvements and adjustments on i2010 actions, on the basis of the monitoring of i2010 implementation and the evolution of policy;
- offer a forum for strategic discussions and for the exchange of experiences, with all Commission services involved; and
- exchange views on issues arising from the national reform plans, in the areas covered by i2010, in relation to achieving the objectives of the Lisbon Strategy.
Article 3
Composition — Appointment
1. The Group shall be composed of one representative of each Member State and the Commission. Member State representatives may be accompanied by appropriate colleagues according to the subject matter discussed, without prejudice to the applicable rules for reimbursement of meeting expenses. The representatives shall be high level civil servants dealing with information society issues at national level, able to ensure appropriate coordination between the national public authorities involved in the various areas covered by the i2010 strategy.
2. The Commission may authorise the participation of observers from EEA countries and acceding countries. These observers are nominated according to the same criteria as referred to in paragraph 1.
3. Member States, EEA countries and acceding countries shall notify the Commission of the names and contact details of their nominees and of any subsequent changes.
4. The names of the appointed members shall be published on the i2010 website (www.europa.eu.int/i2010). The membership list will be updated by the Information Society and Media Directorate-General each time changes are notified by Member States.
Article 4
Operation
1. The group is chaired by the Commission.
2. The Commission may, after consultation of the group, establish subgroups to examine specific questions under the terms of reference established by the group; they shall be disbanded as soon as these terms of reference have been fulfilled.
3. The Commission may ask experts or observers with specific competence on a subject on the agenda to participate in the group’s or subgroup’s deliberations if this is seen as useful and/or necessary.
4. Information obtained by participating in the group’s or subgroup’s deliberations shall not be divulged if the Commission indicates that this relates to confidential matters.
5. The group shall adopt its rules of procedure on the basis of the standard rules of procedure [2].
6. The Commission may publish any résumé, conclusion, partial conclusion or working document of the group.
Article 5
Meeting expenses
The Commission shall reimburse travel and subsistence expenses for members, experts and observers in connection with the group’s activities in accordance with the provisions in force in the Commission. The members shall not be paid for their duties.
Meeting expenses are reimbursed within the limits of the appropriations allocated to the department concerned under the annual procedure for allocating resources.
Article 6
Entry into force
The decision shall take effect on the day of its publication in the Official Journal of the European Union. It is applicable until 31 December 2010. The Commission shall decide on a possible extension before that date.
Done at Brussels, 15 March 2006.
For the Commission
Viviane Reding
Member of the Commission
[1] COM(2005) 229 final.
[2] Annex III of SEC(2005) 1004 of 27 July 2005.
--------------------------------------------------
