Council Regulation (EC) No 813/2000
of 17 April 2000
supplementing the Annex to Commission Regulation (EC) No 1107/96 on the registration of geographical indications and designations of origin under the procedure laid down in Article 17 of Regulation (EEC) No 2081/92
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community,
Having regard to the proposal from the Commission,
Having regard to Council Regulation (EEC) No 2081/92 of 14 July 1992 on the protection of geographical indications and designations of origin for agricultural products and foodstuffs(1), and in particular Article 17(2) thereof,
Whereas:
(1) Additional information was requested on certain names notified by the Member States under Article 17 of Regulation (EEC) No 2081/92 in order to ensure that they comply with Articles 2 and 4 of that Regulation. That additional information shows that the names do comply with those Articles. They should therefore be registered and added to the Annex to Commission Regulation (EC) No 1107/96(2).
(2) The Committee provided for in Article 15 of Regulation (EEC) No 2081/92 has not given a favourable opinion,
HAS ADOPTED THIS REGULATION:
Article 1
The names in the Annex to this Regulation are hereby added to the Annex to Commission Regulation (EC) No 1107/96.
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Luxembourg, 17 April 2000.
For the Council
The President
L. Capoulas Santos
(1) OJ L 208, 24.7.1992, p. 1. Regulation as last amended by Regulation (EC) No 1068/97 (OJ L 156, 13.6.1997, p. 10).
(2) OJ L 148, 21.6.1996, p. 1. Regulation as last amended by Regulation (EC) No 1070/1999 (OJ L 130, 26.5.1999, p. 18).
ANNEX
"OTHER PRODUCTS LISTED IN ANNEX I TO THE TREATY
Vinegars (other than wine vinegars)
ITALY
- Aceto balsamico tradizionale di Modena (PDO)
- Aceto balsamico tradizionale di Reggio Emilia (PDO)"
