Communication from the French Government concerning Directive 94/22/EC of the European Parliament and of the Council of 30 May 1994 on the conditions for granting and using authorisations for the prospection, exploration and production of hydrocarbons [1]
(Notice regarding an application for an exclusive licence to prospect for oil and gas, designated "Permis de Ferrières")
(2006/C 239/04)
(Text with EEA relevance)
By application of 16 August 2005, Essence de Paris, a company with registered offices at 26bis, rue des Cornouillers, F-780, Verneuil sur Seine (France), applied for an exclusive licence, designated the "Montargis Licence", to prospect for liquid and gaseous hydrocarbons in an area of approximately 538 km2 covering part of the department of Loiret. The initial application was the subject of a competition notice published in Official Journal of the European Union No C 27 of 3 February 2006.
By application of 28 February 2006, Lundin International, a company with registered offices at MacLaunay 51210, Montmirail (France), submitted a competing tender to the "Montargis Licence" for a three-year exclusive licence, designated the "Ferrières Licence", to prospect for liquid and gaseous hydrocarbons in an area of approximately 403 km2 covering parts of the departments of Loiret and Seine et Marne. The perimeter of the area covered by this licence consists of the meridian and parallel arcs successively connecting the vertices defined below by their geographical coordinates, the original meridian being that of Paris.
VERTICES | LONGITUDE | LATITUDE |
A | 0,3 degrees E | 53,5 degrees N |
B | 0,6 degrees E | 53,5 degrees N |
C | 0,6 degrees E | 53,3 degrees N |
D | 0,3 degrees E | 53,3 degrees N |
Part of this application overlaps with the "Montargis" application, for which the competition notice deadline has passed, and part is separate. This competition notice only concerns the separate part. The perimeter concerned consists of the meridian and parallel arcs successively connecting the vertices defined below by their geographical coordinates, the original meridian being that of Paris.
VERTICES | LONGITUDE | LATITUDE |
A | 0,3 degrees E | 53,5 degrees N |
B | 0,6 degrees E | 53,5 degrees N |
C | 0,6 degrees E | 53,4 degrees N |
D | 0,3 degrees E | 53,4 degrees N |
Submission of applications
The initial applicant and competing applicants must prove that they comply with the requirements for obtaining the licence, specified in Articles 3, 4 and 5 of Decree No 95-427 of 19 April 1995, as amended, concerning mining rights, and as extended by Article 63 of Decree No 2006-648 of 2 June 2006 on mining rights and underground storage rights.
Interested companies may, within a period of ninety days of the publication of this notice, submit a competing application in accordance with the procedure summarised in the "Notice regarding the granting of mining rights for hydrocarbons in France" published in Official Journal of the European Communities No C 374 of 30 December 1994, p. 11, and established by the aforementioned Decree No 95-427.
Competing applications must be sent to the Minister responsible for mines at the address below. Decisions on the initial application and competing applications will be taken within two years of the date on which the French authorities received the initial application, i.e. by 16 August 2007 at the latest.
Conditions and requirements regarding performance of the activity and cessation thereof
Applicants are referred to Articles 79 and 79.1 of the Mining Code and to Decree No 95-696 of 9 May 1995, as amended, on the start of mining operations and the mining regulations (Journal officiel de la République française, 11 May 1995).
Further information can be obtained from the Ministry of Economic Affairs, Finance and Industry (Directorate-General for Energy and Raw Materials, Directorate for Energy and Mineral Resources, Bureau of Mining Legislation), 61, boulevard Vincent Auriol, Télédoc 133, F-75703 Paris Cedex 13, France [telephone: (33) 144 97 23 02, fax: (33) 144 97 05 70].
The abovementioned laws and regulations can be consulted at
http://www.legifrance.gouv.fr
[1] OJ L 164, 30.6.1994, p. 3.
--------------------------------------------------
