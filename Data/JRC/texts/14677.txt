Order of the President of the Court of 31 May 2006 — Commission of the European Communities
v Republic of Estonia
(Case C-351/05) [1]
(2006/C 224/68)
Language of the case: Estonian
The President of the Court has ordered that the case be removed from the register.
[1] OJ C 281, 12.11.2005.
--------------------------------------------------
