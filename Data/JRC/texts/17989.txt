Commission Directive 2004/77/EC
of 29 April 2004
amending Directive 94/54/EC as regards the labelling of certain foods containing glycyrrhizinic acid and its ammonium salt
(Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Directive 2000/13/EC of the European Parliament and of the Council of 20 March 2000 on the approximation of the laws of the Member States relating to the labelling, presentation and advertising of foodstuffs(1), and in particular Article 4(2) thereof,
After consulting the Scientific Committee on Food,
Whereas:
(1) Commission Directive 94/54/EC of 18 November 1994 concerning the compulsory indication on the labelling of certain foodstuffs of particulars other than those provided for in Directive 2000/13/EC(2) contains a list of foodstuffs for which the labelling must include one or more additional particulars.
(2) The purpose of this Directive is to supplement that list with regard to certain foods containing glycyrrhizinic acid and its ammonium salt.
(3) Glycyrrhizinic acid naturally occurs in the liquorice plant Glycyrrhiza glabra while its ammonium salt is manufactured from aqueous extracts of liquorice plant Glycyrrhiza glabra. Glycyrrhizinic acid and its ammonium salt are included in the Community register of flavouring substances laid down by Commission Decision 1999/217/EC of 23 February 1999 adopting a register of flavouring substances used in or on foodstuffs drawn up in application of Regulation (EC) No 2232/96 of the European Parliament and of the Council of 28 October 1996(3). Exposure to glycyrrhizinic acid and its ammonium salt occur mostly via consumption of liquorice confectionery, including chewing gum, herbal teas and other beverages.
(4) The Scientific Committee on Food, in its opinion of 4 April 2003 on glycyrrhizinic acid and its ammonium salt, concluded that an upper limit for regular ingestion of 100 mg/day provides a sufficient level of protection for the majority of the population, consumption above this level may give rise to hypertension. However the Committee noted that within the human population there are subgroups for which this upper limit might not offer sufficient protection. These subgroups comprise people with medical conditions related to disturbed water- and electrolyte homeostasis.
(5) These findings make it necessary to provide labelling which gives the consumers clear information on the presence of glycyrrhizinic acid or its ammonium salt in confectionery and beverages. In the case of high contents of glycyrrhizinic acid or its ammonium salt in these products, the consumers, and in particular those suffering from hypertension, should in addition be informed that excessive intake should be avoided. To ensure a good understanding of these information by the consumers, the well known term "liquorice extracts" should be preferably used.
(6) Directive 94/54/EC should therefore be amended accordingly.
(7) The measures provided for in this Directive are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Annex to Directive 94/54/EC is amended in accordance with the text set out in the Annex to this Directive.
Article 2
1. The Member States shall permit trade in products which comply with this Directive from 20 May 2005 at the latest.
2. The Member States shall prohibit trade in products which do not comply with this Directive from 20 May 2006.
However, products which do not comply with this Directive and which were labelled before 20 May 2006 shall be authorised while stocks last.
Article 3
1. Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive by 20 May 2005 at the latest. They shall forthwith communicate to the Commission the text of those provisions and a correlation table between those provisions and this Directive.
When Member States adopt those provisions, they shall contain a reference to this Directive or be accompanied by such a reference on the occasion of their official publication. Member States shall determine how such reference is to be made.
2. Member States shall communicate to the Commission the text of the main provisions of national law which they adopt in the field covered by this Directive.
Article 4
This Directive shall enter into force on the 20th day following that of its publication in the Official Journal of the European Union.
Article 5
This Directive is addressed to the Member States.
Done at Brussels, 29 April 2004.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 109, 6.5.2000, p. 29. Directive as amended by Directive 2003/89/EC of the European Parliament and of the Council of 10 November 2003 (OJ L 308, 25.11.2003, p. 15).
(2) OJ L 300, 23.11.1994, p. 14. Directive as amended by Council Directive 96/21/EC (OJ L 88, 5.4.1996, p. 5).
(3) OJ L 84, 27.3.1999, p. 1. Decision as amended by Decision 2002/113/EC (OJ 49, 20.2.2002, p. 1).
ANNEX
In Annex to Directive 94/54/EC, the following text is added:
>TABLE>
