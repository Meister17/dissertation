Commission Regulation (EC) No 307/2006
of 21 February 2006
amending Regulation (EC) No 80/2006 opening a standing invitation to tender for the resale on the Community market of rye held by the German intervention agency
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1784/2003 of 29 September 2003 on the common organisation of the market in cereals [1], and in particular Article 6 thereof,
Whereas:
(1) Commission Regulation (EC) No 80/2006 [2] opens a standing invitation to tender for the resale on the Community market of rye held by the German intervention agency.
(2) In view of market requirements and the quantities held by the German intervention agency, Germany has informed the Commission that its intervention agency intends to increase the amount put out to tender by 50000 tonnes. In view of the market situation, the request made by Germany should be granted.
(3) Regulation (EC) No 80/2006 should be amended accordingly.
(4) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Cereals,
HAS ADOPTED THIS REGULATION:
Article 1
In Article 1 of Regulation (EC) No 80/2006, "50000 tonnes" is replaced by "100000 tonnes".
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 21 February 2006.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 270, 21.10.2003, p. 78. Regulation as amended by Commission Regulation (EC) No 1154/2005 (OJ L 187, 19.7.2005, p. 11).
[2] OJ L 14, 19.1.2006, p. 5.
--------------------------------------------------
