Opinion of the European Economic and Social Committee on the Proposal for a Regulation of the European Parliament and of the Council on international rail passengers' rights and obligations
(COM(2004) 143 final — 2004/0049 (COD))
(2005/C 221/02)
On 28 April 2004 the Council decided to consult the European Economic and Social Committee, under Article 71 of the Treaty establishing the European Community, on the abovementioned proposal.
The Section for Transport, Energy, Infrastructure and the Information Society, which was responsible for preparing the Committee's work on the subject, adopted its opinion on 17 January 2005. The rapporteur was Mr Chagas.
At its 414th plenary session (meeting of 9 February 2005), the European Economic and Social Committee adopted the following opinion by 119 votes to one with four abstentions
1. Introduction
1.1 The current proposal for a Regulation of the European Parliament and of the Council on international rail passengers' rights and obligations (hereinafter "Quality Directive on Passenger Transport") is part of what is known as the third railway package, which the Commission adopted on 3 March 2004. The other elements are:
- Amending Directive 91/440/EEC on the development of the Community's railways (COM(2004) 139 final);
- Proposal for a Directive on the certification of train crews (COM(2004) 142 final);
- Proposal for a Regulation on compensation in cases of non-compliance with contractual quality requirements for rail freight services (COM(2004) 144 final);
and
- Communication from the Commission on further integration of the European rail system (COM(2004) 140);
- Commission Staff Working Paper on gradually opening up the market for international passenger services by rail (SEC(2004) 236).
1.2 The First Railway Package (also known as the Infrastructure Package) entered into force on 15 March 2001 and was required to be transposed into national law by 15 March 2003. It contains the following elements:
- Amendment of Directive 91/440/EEC: among other things, free market access for international rail freight traffic on the Trans-European Rail Freight Network by 15 March 2003 and total liberalisation of international rail freight traffic by 15 March 2008 [1];
- Extension of the scope of a European Rail Operators' Licence (Amendment of Directive 95/18/EC) [2];
- Harmonisation of the provisions for the allocation of railway infrastructure capacity and the charging of infrastructure fees and safety certification (replaces Directive 95/19/EC) [3].
1.3 In October 2003, the European Commission took nine Member States to the European Court of Justice for failure to notify the transposition of the First Railway Package into national law. In May 2004, five countries still had yet to prove notification, and two Member States had only partially implemented the provisions in national law.
1.4 The Second Railway Package was published in the Official Journal of the European Communities on 30 April 2004, and must be transposed into national law by 30 April 2006. It contains the following elements:
- Amendment of Directive 91/440/EEC: Bringing forward free market access for international rail freight traffic to 1 January 2006 and liberalisation of domestic rail freight traffic including cabotage from 1 January 2007 [4];
- Directive on safety on the Community's railways [5];
- Regulation establishing a European Railway Agency [6];
- Amendment of the Directives on the interoperability of the high-speed rail system (Directive 96/48/EC) and of the conventional rail system (Directive 2001/16/EC) [7].
1.5 The first and second railway packages created the legal basis for an internal market in rail freight transport. The measures cover market access, licensing and safety certification of rail operators, access to infrastructure and the calculation of access charges, the creation of a legal framework for railway safety, and measures to ensure technical interoperability of the rail system.
1.6 This legal framework, put in place by the first and second packages, requires, as the EESC stated in its opinion on the second railway package [8], a complete restructuring of the sector and the creation of new authorities and competences.
1.7 In the current proposal, the Commission suggests passing legislation to afford similar protection for international rail passengers to that already enjoyed by air travellers, whose rights in the event of overbooking and delays are better protected.
2. The Commission proposal
2.1 Liability and compensation
2.1.1 The draft regulation governs liability of rail operators in the event of death or injury of passengers or of loss of, or damage to, their luggage.
2.1.2 Minimum compensation payments are set for delays (Annex III); these payments do not affect the passenger's right to transport.
Annex III
Type of service | Journey time | 50% compensation in the event of | 100 % compensation in the event of |
International journeys in whole or in part by high-speed scheduled rail service | Up to two hours | Delays of between 30 and 60 minutes | Delays of more than 60 minutes |
Over two hours | Delays of between 60 and 120 minutes | Delays of more than 120 minutes |
International journeys by scheduled trains other than high-speed trains | Up to four hours | Delays of between 60 and 120 minutes | Delays of more than 120 minutes |
Over four hours | Delays of between 120 and 240 minutes | Delays of more than 240 minutes |
2.1.3 The draft regulation comprehensively governs the rights of passengers who miss their connections or whose trains are cancelled as well as the handling of customers in the event of delays or missed connections.
2.2 Availability of information and sale of tickets
2.2.1 Annex I governs the minimum information to be provided by rail operators before, during and after the journey. Annex II sets out the minimum information to be provided on the ticket.
2.2.2 Rail operators must sell tickets and/or through tickets for hub stations and the surrounding areas. Several rail operators must work together for this purpose and sign contracts in order to provide for the sale of through tickets. These tickets must be offered for sale at ticket counters, ticket machines, by telephone or on the Internet. If the ticket counters are closed or the machines are out of order, international tickets must be available on the train. System vendors must be open to all rail operators for the provision of information and the sale of tickets.
2.2.3 Rail operators must inform the public about plans to suspend international services.
2.3 People with reduced mobility
2.3.1 The proposed regulation governs assistance for people with reduced mobility at the station and on the train, as well as when embarking, disembarking or changing trains. The need for assistance must be notified 24 hours in advance.
2.4 Service quality standards and complaints procedures
2.4.1 Rail operators are required to set service quality standards (defined in Annex IV) and to put a quality management system in place. Quality performance must be published in the annual report.
2.4.2 A complaints procedure is to be set up, according to which a reply must be sent to the customer in the language in which the complaint was made. The complaint may be submitted in any language of any country through which the international train passes. English, French and German are in any case permissible. This also applies to complaints made in person at ticket counters.
3. Assessment of the proposal
3.1 Basic observations
3.1.1 The Commission's proposal deals with two areas at once. The proposals for operators' liability and compensation in the case of delays, cancellations, injury or damage to property, and assistance to persons with limited mobility have broadly the same scope as the Regulation on compensation and assistance to passengers in the event of denied boarding and of cancellation or long delay of flights [9]. This proposal deals with the rights of passengers in a second transport sector, i.e. the railways.
3.1.2 The second area covers a separate range of matters. This requires operators to cooperate to ensure that passengers can obtain information about timetables and fares and buy tickets at a one-stop shop in a competitive system. This covers both connections between hubs, and the stations in the area surrounding the nearest hub station. This proposal is closely connected with the proposal to amend Directive 91/440/EEC and with the liberalisation of international passenger transport.
3.1.3 The standards for timetable and fares information and with a few exemptions (e.g. Thalys, Eurostar) for the issue of tickets are fulfilled under the current system, in which international transport of passengers by rail is carried out under cooperation agreements between rail operators or by international groups. In the frame of a system of competing undertakings these provisions need to be maintained and improved by regulation or legislation.
3.2 Scope
3.2.1 The proposal applies to the international transport of passengers by rail. However, the provisions also apply to connections from hubs to stations in the surrounding area.
3.2.2 The EESC points out that connecting services may be subject to public service contracts.
3.2.3 However, the scope of the regulation is limited by its definition of "railway undertaking" (Article 2.1). This includes only undertakings whose principal business is to transport passengers. This could be seen to imply that railway undertakings that also carry freight are excluded from the provisions of the regulation. This is not acceptable.
3.3 Liability and compensation
3.3.1 The EESC welcomes the principle of introducing European legislation on compensation of passengers when services are not provided satisfactorily or at all, and on the liability of rail operators.
3.3.2 However, it is important to ensure that binding legislation treats the various competing transport operators equally.
3.3.3 It is conspicuous that compensation for international rail passengers becomes payable earlier than that for air passengers, despite the fact that land-bound rail traffic often involves longer journey times and has more potential for disruption. For example, fare refunds are payable to air passengers only after delays of five hours.
3.3.4 In the event that a train is delayed, free meals and refreshments must be offered to passengers in a reasonable relation to the waiting time. In the case of air transport, the threshold is two hours or more.
3.3.5 In the case of cancelled flights, compensation need not be paid where the cancellation takes place due to "extraordinary circumstances." This basis for exemption from liability does not exist in the case of transport of passengers by rail.
3.3.6 Similarly, the regulation on air travel does not provide for compensation for consequential damages arising as a result of delays or cancellations, whereas the regulation on rail travel does so. Furthermore, the proposed regulation does not set an upper limit for consequential damages.
3.3.7 The maximum limit for liability is set at different levels for hand luggage and other luggage: EUR 1800 for hand luggage and EUR 1300 for other luggage. It appears from the explanatory memorandum that the Commission has drawn on different but comparable agreements (CIV for the rail sector and the Montreal Convention for the air sector). From the passenger's point of view, this difference is incomprehensible.
3.3.8 The proposed regulation contains varying provisions for liability of the operator to the customer depending on whether or not the operator was at fault. Thus, the operator is responsible for loss or damage to a passenger's hand luggage only if the operator is at fault. In other cases, the operator is liable whether or not it is at fault.
3.3.9 The operator is not liable for delays when these were caused by exceptional weather conditions, natural disasters, acts of war or terrorism. The operator is liable for delays due to any other cause, whether or not it was at fault.
3.3.10 As a general principle, the EESC is in favour of liability regardless of fault in the event of delay in any transport sector. This is not a matter of compensation for damages in the narrowest sense, but rather compensation for services that were not provided. To the consumer/customer, it does not matter whether the operator is at fault. The proposed limitations are appropriate.
3.3.11 It is not clear from the proposed regulation that a passenger can cancel his journey in the event of a delay and receive a full refund of his fare. Particularly in the case of business trips, a delay can make the journey pointless.
3.4 Assistance to persons with limited mobility and other passengers
3.4.1 The EESC welcomes the provisions for assistance to people with limited mobility.
3.4.2 Rail operators should provide accessible information to all passengers — including people with reduced mobility as defined in Article 2, paragraph 21; this could be done by locating windows and information stands at an appropriate height and by preparing text in bigger fonts and in an easy-to-read format.
3.5 Information to passengers and on tickets
3.5.1 The EESC welcomes the provisions on passenger information before, during and after the journey (Annex I). In particular, lack of information before and during the journey in the event of a delay regularly causes considerable annoyance to customers.
3.5.2 With regard to minimum information on tickets (Annex II), it should be stated whether and when the ticket can be surrendered and the fare refunded. Because of wide variations in reservation systems, this is often unclear to the passenger.
3.5.3 The Regulation provides (Articles 3, 5 and 6):
- that rail operators and/or tour operators must provide information about journey times, fares, carriage of bicycles, etc. for all services, including those offered by other operators, in all sales systems (ticket counters, telephone, Internet or other systems that may become available in the future);
- that operators must work together to sell through tickets to customers through all sales systems.
3.5.4 Fundamentally, the EESC considers it desirable that passengers should be offered one-stop-shop booking and information systems for all rail transport and associated services.
3.5.5 However, it draws attention to the peculiarities of rail travel:
- the dependency on the network and the interdependency of international long-distance, domestic long-distance and local services, including public service routes that are subject to other contractual obligations;
- the advantages of short-term bookings (spontaneous journeys), the ability to board along the route, and, in many cases, the absence of a requirement to reserve a seat;
- tickets that are transferable between persons.
3.5.6 A directly applicable regulation cannot make appropriate provision for the complexity that arises from the connection between international passenger rail services and regional services in a network that includes the integration of competing scheduled operators. The number of railway stations affected (hubs and stations in the area surrounding them) is, for example, considerable.
3.5.7 The Committee underlines that for international rail passenger services, these standards are presently fulfilled to a large extent. In a system of competing undertakings however, these provisions need to be maintained and improved by adequate legislation.
3.6 Effects of the regulation on employees
3.6.1 Article 21 of the proposed regulation states that the railway undertaking shall be liable for its staff. Article 22, on the other hand, mentions the possibility of aggregate claims, and includes claims against staff. It must be made absolutely clear that railway staff are not exposed to liability claims by passengers or other third parties but that the employer remains responsible.
3.6.2 High compensation for delays shall not, as a consequence, lead to rail operators to accept higher risks on the safety level in order to avoid compensation claims. Also, it shall be excluded that railway undertakings put excessive pressure on their employees with the risk to neglect working, driving and rest time. Furthermore, it is necessary to ensure that sufficient, well-trained staff are available to fulfil the quality requirements.
3.6.3 Annex IV on minimum service quality standards must therefore cover the skills of the relevant staff. This applies not only to train crews, but also to station staff and staff who process complaints.
4. Conclusions
4.1 The EESC welcomes the proposal for a Regulation on international rail passengers' rights and obligations. This extends consumer protection provisions that currently apply only to air travel to another mode of transport.
4.2 However, the EESC is against unequal treatment of competing modes of transport. The provisions that apply to the railway sector must not be stricter than those for air travel.
4.3 The EESC sees room for improvement in the regulation with regard to individual provisions for liability for consequential damages, the setting of upper limits for liability, and to exclusions in the event of train cancellations.
4.4 Fundamentally the EESC would be in favour of a refund of fare in the event of a service not being provided or being provided inadequately, whether or not the operator was at fault, as long as this applies to all modes of transport.
4.5 The EESC supports the concept of one-stop-shop information and booking systems that guarantee good service quality for passengers. However, it has reservations about this being dealt with in the same regulation on compensation and liability.
4.6 The EESC points out that establishing one-stop-shop information and booking systems in an interconnected system with network providers in international, national, regional and public service transport, with the added complication of competing scheduled operators, is very complex, particularly if this system is to be available through all sales systems.
4.7 It points out that the presentation of legislation on passenger information and the issue of tickets for the international transport of passengers by rail should be considered in conjunction with the amendment of Directive 91/440/EEC on the liberalisation of international passenger transport by rail.
Brussels, 9 February 2005.
The President
of the European Economic and Social Committee
Anne-Marie Sigmund
[1] Directive 2001/12/EC – OJ L 75, 15.3.2001, p. 1 – EESC Opinion – OJ C 209, 22.7.1999, p. 22.
[2] Directive 2001/13/EC – OJ L 75, 15.3.2001, p. 26 – EESC Opinion – OJ C 209, 22.7.1999, p. 22.
[3] Directive 2001/14/EC – OJ L 75, 15.3.2001, p. 29 – EESC Opinion – OJ C 209, 22.7.1999, p. 22.
[4] Directive 2004/51/EC – OJ L 164, 30.4.2004, p. 164 – EESC Opinion – OJ C 61, 14.3.2003, p. 131.
[5] Directive 2004/49/EC – OJ L 164, 30.4.2004, p. 44 – EESC Opinion – OJ C 61, 14.3.2003, p. 131.
[6] Directive 2004/881/EC – OJ L 164, 30.4.2004, p. 1 – EESC Opinion – OJ C 61, 14.3.2003, p. 131.
[7] Directive 2004/50/EC – OJ L 164, 30.4.2004, p. 114 – EESC Opinion – OJ C 61, 14.3.2003, p. 131.
[8] OJ C 61, 14.3.2003, p. 131.
[9] Regulation (EC) No. 261/2004 of the European Parliament and of the Council of 11 February 2004 establishing common rules on compensation and assistance to passengers in the event of denied boarding and of cancellation or long delay of flights, and repealing Regulation (EEC) No. 295/91.
--------------------------------------------------
