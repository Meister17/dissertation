Judgment of the Court of First Instance of 11 July 2006 — Torres v OHIM — Bodegas Muga (Torre Muga)
(Case T-247/03) [1]
Parties
Applicant: Miguel Torres (Vilafranca del Penedés, Spain) (represented by: E. Armijo Chávarri, M.A. Baz de San Ceferino and A. Castán Pérez-Gómez, lawyers)
Defendant: Office for Harmonisation in the Internal Market (Trade Marks and Designs) (represented by: I. de Medrano Caballero and S. Laitinen, acting as Agents)
Other party to the proceedings before the Board of Appeal of OHIM intervening before the Court of First Instance: Bodegas Muga, SA (Haro, Spain) (represented by: L.M. Polo Flores and F. Porcuna de la Rosa, lawyers)
Re:
Action brought against the decision of the First Board of Appeal of Office for Harmonisation in the Internal Market (Trade Marks and Designs) of 7 April 2003 (Case R 998/2001-1) concerning opposition proceedings between Miguel Torres, SA, and Bodegas Muga, SA
Operative part of the judgment
The Court:
1. Dismisses the action;
2. Orders the applicant to pay the costs incurred by the Office for Harmonisation in the Internal Market (Trade Marks and Designs) (OHIM);
3. Orders the intervener to bear its own costs.
[1] OJ C 213, 6.9.2003.
--------------------------------------------------
