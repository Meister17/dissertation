Commission Regulation (EC) No 865/2003
of 19 May 2003
supplementing the Annex to Regulation (EC) No 2400/96 (Cítricos Valencianos or Cítrics Valencians)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2081/92 of 14 July 1992 on the protection of geographical indications and designations of origin for agricultural products and foodstuffs(1), as last amended by Regulation (EC) No 692/2003(2), and in particular Article 6(3) and (4) thereof,
Whereas:
(1) In accordance with Article 5 of Regulation (EEC) No 2081/92, Spain has sent the Commission an application for registration of the name "Cítricos Valencianos" or "Cítrics Valencians" as a geographical indication.
(2) In accordance with Article 6(1) of that Regulation, the application has been found to meet all the requirements laid down therein and in particular to contain all the information required in accordance with Article 4 thereof.
(3) No statement of objection under Article 7 of Regulation (EEC) No 2081/92 has been received by the Commission in respect of the name given in the Annex hereto following its publication in the Official Journal of the European Communities(3).
(4) The name should therefore be entered in the "Register of protected designations of origin and protected geographical indications" and hence be protected throughout the Community as a protected geographical indication.
(5) The Annex to this Regulation supplements the Annex to Commission Regulation (EC) No 2400/96(4), as last amended by Regulation (EC) No 617/2003(5),
HAS ADOPTED THIS REGULATION:
Article 1
The name in the Annex hereto is added to the Annex to Regulation (EC) No 2400/96 and entered as a protected geographical indication (PGI) in the "Register of protected designations of origin and protected geographical indications" provided for in Article 6(3) of Regulation (EEC) No 2081/92.
Article 2
This Regulation shall enter into force on the 20th day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 19 May 2003.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 208, 24.7.1992, p. 1.
(2) OJ L 99, 17.4.2003, p. 1.
(3) OJ C 204, 28.8.2002, p. 6 (Cítricos Valencianos or Cítrics Valencians).
(4) OJ L 327, 18.12.1996, p. 11.
(5) OJ L 89, 5.4.2003, p. 3.
ANNEX
PRODUCTS LISTED IN ANNEX I TO THE EC TREATY INTENDED FOR HUMAN CONSUMPTION
Fruit, vegetables and cereals
SPAIN
Cítricos Valencianos or Cítrics Valencians. (PGI)
