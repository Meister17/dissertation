Commission Decision
of 29 March 2001
amending Decision 93/52/EEC recording the compliance by certain Member States of regions with the requirements relating to brucellosis (Brucella melitensis) and according them the status of a Member State or region officially free of the disease
(notified under document number C(2001) 952)
(Text with EEA relevance)
(2001/292/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/68/EEC of 28 January 1991 on animal health conditions governing intra-Community trade in ovine and caprine animals(1), as last amended by Commission Decision 94/953/EC(2), and in particular Annex A, Chapter 1(II) thereof,
Whereas:
(1) By Decision 93/52/EEC(3), as last amended by Decision 97/315/EC(4), the Commission recorded compliance by certain Member States or regions with the requirements relating to brucellossis (Brucella melitensis) and accorded them the status of a Member State or region officially free of the disease.
(2) In Austria and the French regions of Ardèche, Aveyron, Cantal, Corrèze, Gers, Gironde, Haute-Loire, Lot, Lozère, Morbihan, Puy-de-Dôme and Vosges, brucellosis has been a notifiable disease for at least five years. No case has been confirmed there officially for at least five years and vaccination has been banned for at least three years. It should therefore be recorded that these regions comply with the requirements laid down in Annex A, Chapter 1(II)(1)(b) to Directive 91/68/EEC.
(3) Austria and the regions Ardèche, Aveyron, Cantal, Corrèze, Gers, Gironde, Haute-Loire, Lot, Lozère, Morbihan, Puy-de-Dôme and Vosges in France therefore satisfy the conditions to be recognised officially free of brucellosis.
(4) Austria and France furthermore undertake to comply with Annex A Chapter 1(II)(2) to Directive 91/68/EEC. The status of Austria and the regions in France of Ardèche, Aveyron, Cantal, Corrèze, Gers, Gironde, Haute-Loire, Lot, Lozère, Morbihan, Puy-de-Dôme and Vosges should therefore be recognised as officially free of brucellosis (Brucella melitensis) and Decision 93/52/EEC should be amended accordingly.
(5) Sheep and goats introduced onto holdings in Austria and in the regions of France of Ardèche, Aveyron, Cantal, Corrèze, Gers, Gironde, Haute-Loire, Lot, Lozère, Morbihan, Puy-de-Dôme and Vosges must therefore comply with the conditions laid down in Annex A Chapter 1(I)(D) to Directive 91/68/EEC.
(6) The measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
Annex I ("Member States") to Decision 93/52/EEC is replaced by Annex I to this Decision.
Article 2
Annex II ("Regions") to Decision 93/52/EEC is replaced by Annex II to this Decision.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 29 March 2001.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 46, 19.2.1991, p. 19.
(2) OJ L 371, 31.12.1994, p. 14.
(3) OJ L 13, 21.1.1993, p. 14.
(4) OJ L 137, 28.5.1997, p. 20.
ANNEX I
"ANNEX I
- Austria
- Belgium
- Denmark
- Germany
- Finland
- Ireland
- Luxembourg
- Netherlands
- Sweden
- United Kingdom."
ANNEX II
"ANNEX II
In France:
Ain, Aisne, Allier, Ardèche, Ardennes, Aube, Aveyron, Cantal, Charente, Charente-Maritime, Cher, Corrèze, Côte-d'Or, Côtes-d'Armor, Creuse, Deux-Sèvres, Dordogne, Doubs, Essonne, Eure, Eure-et-Loire, Finistère, Gers, Gironde, Hauts-de-Seine, Haute-Loire, Haute-Vienne, Ille-et-Vilaine, Indre, Indre-et-Loire, Jura, Loir-et-Cher, Loire, Loire-Atlantique, Loiret, Lot-et-Garonne, Lot, Lozère, Maine-et-Loire, Manche, Marne, Mayenne, Morbihan, Nièvre, Nord, Oise, Orne, Pas-de-Calais, Puy-de-Dôme, Rhône, Haute-Saône, Saône-et-Loire, Sarthe, Seine-Maritime, Seine-Saint-Denis, Territoire de Belfort, Val-de-Marne, Val-d'Oise, Vendée, Vienne, Yonne, Yvelines, Ville de Paris, Vosges.
In Spain:
Santa Cruz de Tenerife, Las Palmas."
