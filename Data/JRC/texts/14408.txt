Commission Decision
of 6 November 2006
establishing the lists of third countries and territories from which imports of bivalve molluscs, echinoderms, tunicates, marine gastropods and fishery products are permitted
(notified under document number C(2006) 5171)
(Text with EEA relevance)
(2006/766/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Regulation (EC) No 854/2004 of the European Parliament and of the Council of 29 April 2004 laying down specific rules for the organisation of official controls on products of animal origin intended for human consumption [1], and in particular Article 11(1) thereof,
Whereas:
(1) The special conditions for the import of bivalve molluscs, tunicates, echinoderms and marine gastropods and of fishery products from third countries have been laid down in Regulation (EC) No 854/2004.
(2) Commission Decision 97/20/EC [2] established the list of third countries fulfilling the equivalence conditions for the production and placing on the market of bivalve molluscs, echinoderms, tunicates and marine gastropods and Commission Decision 97/296/EC [3] drew up the list of third countries from which the import of fishery products is authorised for human consumption.
(3) Lists should be drawn up containing the third countries and territories which satisfy the criteria referred to in Article 11(4) of Regulation (EC) No 854/2004 and which are therefore able to guarantee that bivalve molluscs, tunicates, echinoderms and marine gastropods and fishery products exported to the Community meet the sanitary conditions laid down to protect the health of consumers. Nevertheless, imports of adductor muscles of pectinidae other than aquaculture animals, completely separated from the viscera and gonads, should also be permitted from third countries not appearing on such a list.
(4) The competent authorities of Australia, New Zealand and Uruguay have provided appropriate guarantees that the conditions applicable to live bivalve molluscs, echinoderms, tunicates and marine gastropods are equivalent to those provided for in the relevant Community legislation.
(5) The competent authorities of Armenia, Belarus and Ukraine have provided appropriate guarantees that the conditions applicable to fishery products are equivalent to those provided for in the relevant Community legislation.
(6) Decisions 97/20/EC and 97/296/EC should therefore be repealed and replaced by a new Decision.
(7) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
Imports of bivalve molluscs, tunicates, echinoderms and marine gastropods
1. The list of third countries from which bivalve molluscs, tunicates, echinoderms and marine gastropods may be imported, as referred to in Article 11(1) of Regulation (EC) No 854/2004, is established in Annex I to this Decision.
2. Notwithstanding Article 11(1) of Regulation (EC) No 854/2004 , Paragraph 1 shall not apply to the adductor muscles of pectinidae other than aquaculture animals, completely separated from the viscera and gonads, that may be imported also from third countries not appearing on the list referred to in paragraph 1.
Article 2
Imports of fishery products
The list of third countries and territories from which fishery products may be imported, as referred to in Article 11(1) of Regulation (EC) No 854/2004, is established in Annex II to this Decision.
Article 3
Repeal
Decisions 97/20/EC and 97/296/EC are repealed.
References to the repealed Decisions shall be construed as references to this Decision.
Article 4
This Decision is addressed to the Member States.
Done at Brussels, 6 November 2006.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 139, 30.4.2004, p. 206; corrected by OJ L 226, 25.6.2004, p. 83. Regulation as last amended by Commission Regulation (EC) No 2076/2005 (OJ L 338, 22.12.2005, p. 83).
[2] OJ L 6, 10.1.1997, p. 46. Decision as last amended by Decision 2002/469/EC (OJ L 163, 21.6.2002, p. 16).
[3] OJ L 122, 14.5.1997, p. 21. Decision as last amended by Decision 2006/200/EC (OJ L 71, 10.3.2006, p. 50).
--------------------------------------------------
ANNEX I
List of third countries from which imports of bivalve molluscs, echinoderms, tunicates and marine gastropods in any form for human consumption are permitted
(Countries and territories referred to in Article 11 of Regulation (EC) No 854/2004)
AU — AUSTRALIA
CL — CHILE [1]
JM — JAMAICA [2]
JP — JAPAN [1]
KR — SOUTH KOREA [1]
MA — MOROCCO
NZ — NEW ZEALAND
PE — PERU [1]
TH — THAILAND [1]
TN — TUNISIA
TR — TURKEY
UY — URUGUAY
VN — VIETNAM [1]
[1] Only frozen or processed bivalve molluscs, echinoderms, tunicates and marine gastropods.
[2] Only marine gastropods.
--------------------------------------------------
ANNEX II
List of third countries and territories from which imports of fishery products in any form for human consumption are permitted
(Countries and territories referred to in Article 11 of Regulation (EC) No 854/2004)
AE — UNITED ARAB EMIRATES
AG — ANTIGUA AND BARBUDA [1]
AL — ALBANIA
AM — ARMENIA [2]
AN — NETHERLANDS ANTILLES
AR — ARGENTINA
AU — AUSTRALIA
BD — BANGLADESH
BG — BULGARIA [3]
BR — BRAZIL
BS — THE BAHAMAS
BY — BELARUS
BZ — BELIZE
CA — CANADA
CH — SWITZERLAND
CI — IVORY COAST
CL — CHILE
CN — CHINA
CO — COLOMBIA
CR — COSTA RICA
CU — CUBA
CV — CAPE VERDE
DZ — ALGERIA
EC — ECUADOR
EG — EGYPT
FK — FALKLAND ISLANDS
GA — GABON
GD — GRENADA
GH — GHANA
GL — GREENLAND
GM — GAMBIA
GN — GUINEA CONAKRY [4] [5]
GT — GUATEMALA
GY — GUYANA
HK — HONG KONG
HN — HONDURAS
HR — CROATIA
ID — INDONESIA
IN — INDIA
IR — IRAN
JM — JAMAICA
JP — JAPAN
KE — KENYA
KR — SOUTH KOREA
KZ — KAZAKHSTAN
LK — SRI LANKA
MA — MOROCCO [6]
MG — MADAGASCAR
MR — MAURITANIA
MU — MAURITIUS
MV — MALDIVES
MX — MEXICO
MY — MALAYSIA
MZ — MOZAMBIQUE
NA — NAMIBIA
NC — NEW CALEDONIA
NG — NIGERIA
NI — NICARAGUA
NZ — NEW ZEALAND
OM — OMAN
PA — PANAMA
PE — PERU
PG — PAPUA NEW GUINEA
PH — PHILIPPINES
PF — FRENCH POLYNESIA
PM — ST PIERRE & MIQUELON
PK — PAKISTAN
RO — ROMANIA [3]
RU — RUSSIA
SA — SAUDI ARABIA
SC — SEYCHELLES
SG — SINGAPORE
SN — SENEGAL
SR — SURINAME
SV — EL SALVADOR
TH — THAILAND
TN — TUNISIA
TR — TURKEY
TW — TAIWAN
TZ — TANZANIA
UA — UKRAINE
UG — UGANDA
US — UNITED STATES OF AMERICA
UY — URUGUAY
VE — VENEZUELA
VN — VIETNAM
XM — MONTENEGRO [7]
XS — SERBIA [7] [8]
YE — YEMEN
YT — MAYOTTE
ZA — SOUTH AFRICA
ZW — ZIMBABWE
[1] Only live crustaceans.
[2] Only for live non farmed crayfish.
[3] Only applicable until this Acceding State becomes a Member State of the Community.
[4] Only fish that has not undergone any preparation or processing operation other than heading, gutting, chilling or freezing.
[5] The reduced frequency of physical checks, provided for by Commission Decision 94/360/EC (OJ L 158, 25.6.1994, p. 41), shall not be applied.
[6] Processed bivalve molluscs belonging to the species Acanthocardia tuberculatum must be accompanied by: (a) an additional health attestation in accordance with the model set out in Part B of Appendix V of Annex VI to Commission Regulation (EC) No 2074/2005 (OJ L 338, 22.12.2005, p. 27); and (b) the analytical results of the test demonstrating that the molluscs do not contain a paralytic shellfish poison (PSP) level detectable by the bioassay method.
[7] Only whole fresh fish from wild seawater catches.
[8] Not including Kosovo as defined by the United Nations Security Council Resolution 1244 of 10 June 1999.
--------------------------------------------------
