Commission Regulation (EC) No 2127/2005
of 22 December 2005
concerning the classification of certain goods in the Combined Nomenclature
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2658/87 of 23 July 1987 on the tariff and statistical nomenclature and on the Common Customs Tariff [1], and in particular Article 9(1)(a) thereof,
Whereas:
(1) In order to ensure uniform application of the Combined Nomenclature annexed to Regulation (EEC) No 2658/87, it is necessary to adopt measures concerning the classification of the goods referred to in the Annex to this Regulation.
(2) Regulation (EEC) No 2658/87 has laid down the general rules for the interpretation of the Combined Nomenclature. Those rules apply also to any other nomenclature which is wholly or partly based on it or which adds any additional subdivision to it and which is established by specific Community provisions, with a view to the application of tariff and other measures relating to trade in goods.
(3) Pursuant to those general rules, the goods described in column 1 of the table set out in the Annex to this Regulation should be classified under the CN code indicated in column 2, by virtue of the reasons set out in column 3 of that table.
(4) It is appropriate to provide that binding tariff information which has been issued by the customs authorities of Member States in respect of the classification of goods in the Combined Nomenclature but which is not in accordance with this Regulation can, for a period of three months, continue to be invoked by the holder, under Article 12(6) of Council Regulation (EEC) No 2913/92 of 12 October 1992 establishing the Community Customs Code [2].
(5) The measures provided for in this Regulation are in accordance with the opinion of the Customs Code Committee,
HAS ADOPTED THIS REGULATION:
Article 1
The goods described in column 1 of the table set out in the Annex shall be classified within the Combined Nomenclature under the CN code indicated in column 2 of that table.
Article 2
Binding tariff information issued by the customs authorities of Member States, which is not in accordance with this Regulation, can continue to be invoked for a period of three months under Article 12(6) of Regulation (EEC) No 2913/92.
Article 3
This Regulation shall enter into force on the twentieth day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 22 December 2005.
For the Commission
László Kovács
Member of the Commission
[1] OJ L 256, 7.9.1987, p. 1. Regulation as last amended by Commission Regulation (EC) No 1719/2005 (OJ L 286, 28.10.2005, p. 1).
[2] OJ L 302, 19.10.1992, p. 1. Regulation as last amended by Regulation (EC) No 648/2005 of the European Parliament and of the Council (OJ L 117, 4.5.2005, p. 13).
--------------------------------------------------
ANNEX
Description of the goods | Classification (CN-code) | Reasons |
(1) | (2) | (3) |
An incomplete and unassembled new four-wheeled vehicle, of the "pick-up" type with a diesel engine of a cylinder-capacity of 2500 cm3, a gross vehicle weight of 2650 kg and a total cargo-capacity of 1000 kg. The vehicle has one row of seats for two persons (including the driver) and an open cargo area with a length of 2,28 m. All parts are presented and declared to customs at the same place and time. The radiator, windows, tyres, battery, shock absorbers, seat and door upholstery are not present. | 87042191 | Classification is determined by General Rules 1, 2 (a) and 6 for the interpretation of the Combined Nomenclature and by the wording of CN codes 8704, 870421 and 87042191. The vehicle is classified under heading 8704 because, as presented, it has the essential character of a complete or finished vehicle (General Rule 2 (a), first sentence). See also the HS Explanatory Notes to Chapter 87, General. The fact that the vehicle is presented unassembled does not affect the classification as a complete or finished product (General Rule 2 (a), second sentence). |
--------------------------------------------------
