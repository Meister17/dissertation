*****
COMMISSION DIRECTIVE
of 27 April 1989
amending for the second time the Annexes to Council Directive 77/96/EEC on the examination for Trichinae (Trichinelle spiralis) upon importation from third countries of fresh meat derived from domestic swine
(89/321/EEC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Directive 77/96/EEC of 21 December 1976 on the examination for trichinae (trichinella spiralis) upon importation from third countries of fresh meat derived from domestic swine (1), as last amended by Regulation (EEC) No 3768/85 (2), and in particular Article 8 thereof,
Whereas recent studies have enabled certain methods to be devised for detecting trichinae in pigmeat; whereas the reliability of these methods from the point of view of health protection is equivalent to that of existing methods; whereas appropriate additions should, therefore, be made to Annex I to Directive 77/96/EEC;
Whereas, in order to facilitate the work of examination for trichinae, non-member countries and the Member States should be permitted to choose between the examination methods provided for;
Whereas the measures laid down by this Directive are in accordance with the opinion of the Standing Veterinary Committee;
HAS ADOPTED THIS DIRECTIVE:
Article 1
Directive 77/96/EEC is hereby amended as set out in the Annex.
Article 2
Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive not later than 1 September 1989. They shall forthwith inform the Commission thereof.
Article 3
This Directive is addressed to the Member States.
Done at Brussels, 27 April 1989.
For the Commission
Ray MAC SHARRY
Member of the Commission
(1) OJ No L 26, 31. 1. 1977, p. 67.
(2) OJ No L 362, 31. 12. 1985, p. 8.
ANNEX
The following point VII is added to Annex I to Directive 77/96/EEC:
'VII THE AUTOMATIC DIGESTION METHOD FOR POOLED SAMPLES OF UP TO 35 g
(a) Apparatus and reagents
- Knife or scissors for cutting specimens,
- trays marked off with 50 squares each of which can hold samples of approximately 2 g of meat,
- a Trichomatic 35 blender with filtration insert,
- hydrochloric acid solution 8,5 % ± 0,5 weight,
- transparent polycarbonate membrane filters with a diameter of 50 mm and a pore size of 14 microns,
- pepsin strength 1: 10 000 NF (US National Formulary)
corresponding to 1: 12 5000 BP (British Pharmacopoea)
corresponding to 2 000 FIP (Fédération Internationale de Pharmacie),
- a balance, accurate to 0,1 g,
- tweezers with a flat tip,
- a number of microscope slides with a side-length of at least 5 cm or a number of at least 6 cm diameter Petri dishes marked on their underside equipped into 10 × 10 mm large areas using a pointed instrument,
- a (stereo-) microscope with transmitted light (magnification 15 to 60 times) or a trichinoscope with a horizontal table,
- a bin for collection of waste liquids,
- a number of 10 litre bins to be used when applying de-contamination, such as formol treatment, to the apparatus and for the remaining digestive juice in the case of positive results.
(b) Collection of specimens
1. In the case of whole carcases a specimen to be taken of approximately 2 g from a pillar of the diaphragm at the transition to the sinewy part. In the absence of diaphragm pillars, a specimen of the same size to be taken from the rib part of the breastbone, part of the diaphragm, from the jaw muscle or the abdomina muscle.
2. For cuts of meat, a sample of approximately 2 g of skeletal muscle to be taken, containing little fat and, where possible, near to bones or tendons.
(c) Method
1. Digestion Procedure
- Place the blender with filtration-insert, connect the waste tube and lead the tube to the waste bin.
- When the blender is switched on, the heat-up will start.
- Before start, the bottom valve, located below the reaction chamber, should be opened and closed.
- Up to 35 samples of approximately 1 g each (at 25 to 30°C) taken from each of the individual samples, in accordance with point b, are then added. Make sure that larger pieces of tendons are removed as this may clot the membrane filter.
- Pour water to the edge of a liquid chamber connected to the blender (approximately 400 ml).
- Pour about 30 ml hydrochloric acid (8,5 %) to the edge of the smaller, connected liquid chamber.
- Place a membrane filter under the coarse filter in the filter holder in the filter insert.
- 5 g of pepsin is added last. The order of addition should be strictly adhered to in order to avoid decomposition of the pepsin.
- Close the lids to the reaction- and liquid chambers.
- Select the period of digestion. Short digestion period (5 minutes) for pigs at normal age of slaughtering and extended digestion time (8 minutes) for other samples. - The automatic dispensing starts when the start button on the blender is activated and digestion with following filtration will proceed automatically. After 10 to 13 minutes the process is completed and stops automatically.
- The lid to the reaction chamber is opened once it is checked that the chamber is emptied. If there is foam or remains of digestion liquid in the chamber repeat the procedure according to point 4.
2. Recovery of larvae
- Dismount the filter holder and transfer the membrane filter to a slide or a Petri dish.
- The membrane filter is examined by means of a microscope or a trichinoscope.
3. Cleaning of equipment
- In the case of a positive result, fill the reaction chamber in the blender 2 / 3 with boiling water. Ordinary tap-water is poured into the connecting liquid chamber until the lower level sensor is covered. The automatic cleaning programme is then carried out. De-contaminate the filter-holder together with the remaining equipment, for example by means of formal treatment.
- After the day's work fill the liquid chamber in the blender with water and carry out a standard programme.
4. Method to be used when digestion is incomplete and filtration cannot therefore be carried out
When the automatic process in the blender is carried out according to point 1, open the lid to the reaction chamber and check whether there is foam or liquid remaining in the chamber. It this is the case, carry out the following procedure:
- Close the bottom valve below the reaction chamber.
- Dismount the filter holder and transfer the membrane filter to a slide or a Petri dish.
- Put a new membrane filter in the filter holder and mount the filter holder.
- Fill water into the liquid chamber in the blender until the lower level-sensor is covered.
- Carry out the automatic cleaning programme.
- After the cleaning programme has been completed open the lid to the reaction chamber and check for liquid remains.
- If the chamber is empty, dismount the filter holder and transfer the membrane filter with a tweezer to a slide or a Petri dish.
- The two membrane filters are examined according to point 2. If the filters cannot be examined repeat the entire digestion process with extended digestion time according to point 1.
5. In the case of a positive or doubtful result, following the result of a collective sample, a further 20 g sample should be taken from each pig in accordance with point b above. These samples shall be investigated individually according to the abovementioned method.'
