Council Regulation (EC) No 2549/2000
of 17 November 2000
establishing additional technical measures for the recovery of the stock of cod in the Irish Sea (ICES Division VIIa)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community and in particular Article 37 thereof,
Having regard to the proposal from the Commission(1),
Having regard to the opinion of the European Parliament(2),
Whereas:
(1) The stock of mature cod in the Irish Sea is currently depleted and urgent and temporary technical measures for fishing in the Irish Sea have been laid down in Commission Regulation (EC) No 304/2000 of 9 February 2000 establishing measures for the recovery of the stock of cod in the Irish Sea (ICES Division VII a)(3).
(2) Greater protection of juvenile cod in the Irish Sea is required so that more juveniles survive to become adults.
(3) Additional technical measures intended to ensure the survival of juvenile cod are required within the Irish Sea.
(4) Also, conditions defined in footnote 6 of Annex I of Council Regulation (EC) No 850/98 of 30 March 1998 for the conservation of fishery resources through technical measures for the protection of juveniles of marine organisms(4) currently allow, during 2000, relaxed percentage by-catch conditions to prevail for various fishing gears both within and in waters adjacent to the Irish Sea (ICES Division VIIa). The application of these conditions during 2000 is not desirable. The aforementioned footnote 6 therefore should not be applicable in the Irish Sea,
HAS ADOPTED THIS REGULATION:
Article 1
This Regulation lays down technical measures additional to those defined in Regulation (EC) No 850/98 which shall apply exclusively to the Irish Sea (ICES Division VIIa as defined in Council Regulation (EEC) No 3880/91 of 17 December 1991 on the submission of nominal catch statistics by Member States fishing in the north-east Atlantic)(5).
Article 2
When fishing in the Irish Sea, it shall be prohibited to use:
1. any demersal towed net other than beam trawls incorporating a cod-end and/or extension piece made entirely or partly of multiple-twine netting materials;
2. any demersal towed net other than beam trawls incorporating a cod-end and/or extension piece of which the thickness of the twine exceeds 6 mm;
3. any demersal towed net other than beam trawls incorporating a cod-end of mesh size range 70 to 79 mm or of mesh size range 80 to 89 mm having more than 120 meshes in any circumference of said cod-end excluding the joinings and selvedges;
4. any demersal towed net which includes any individual quadrilateral mesh of which the bars of the mesh are not of approximately equal length;
5. any demersal towed net other than beam trawls of mesh size range 70 to 79 mm or of mesh size range 80 to 99 mm unless the entire upper half of the anterior part of such a net consists of a panel of netting material attached directly to the headline of the net, extending towards the posterior of the net for at least 15 meshes and constructed of diamond-meshed netting material of which no individual mesh is of mesh size less than 140 mm;
6. any beam trawl of mesh size range 70 to 79 mm or of mesh size range 80 to 99 mm unless the entire upper half of the anterior part of such a net consists of a panel of netting material attached directly to the headline of the net, extending towards the posterior of the net for at least 30 meshes and constructed of diamond-meshed netting material of which no individual mesh is of mesh size less than 180 mm;
7. any demersal towed net other than beam trawls of mesh size range 80 to 99 mm unless a square-meshed panel of mesh size of at least 80 mm is included in such a net, in accordance with the conditions laid down in Article 7 of Regulation (EC) No 850/98;
8. any demersal towed net to which a cod-end of mesh size less than 100 mm is attached by any means other than being sewn into the net, anterior to the cod-end.
Article 3
Footnote 6 of Annex I of Regulation (EC) No 850/98 shall not apply to ICES Division VII a.
Article 4
This Regulation shall enter into force on the twentieth day following that of its publication in the Official Journal of the European Communities.
The conditions of Article 2 shall apply from 1 January 2001.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 17 November 2000.
For the Council
The President
J. Glavany
(1) OJ C 248 E, 29.8.2000, p. 120.
(2) Opinion of 10 October 2000 (not yet published in the Official Journal).
(3) OJ L 35, 10.2.2000, p. 10.
(4) OJ L 125, 27.4.1998, p. 1. Regulation as last amended by Regulation (EC) No 1298/2000 (OJ L 148, 22.6.2000, p. 1).
(5) OJ L 365, 31.12.1991, p. 1.
