*****
COMMISSION REGULATION (EEC) No 1586/89
of 7 June 1989
concerning the classification of certain goods in the combined nomenclature
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 2658/87 (1) on the tariff and statistical nomenclature and on the Common Customs Tariff, as last amended by Regulation (EEC) No 1495/89 (2), and in particular Article 9 thereof,
Whereas in order to ensure uniform application of the combined nomenclature annexed to Regulation (EEC) No 2658/87, it is necessary to adopt measures concerning the classification of the goods referred to in the Annex to this Regulation;
Whereas Regulation (EEC) No 2658/87 has set down the general rules for the interpretation of the combined nomenclature and these rules also apply to any other nomenclature which is wholly or partly based on it or which adds any additional subdivisions to it and which is established by specific Community provisions, with a view to the application of tariff or other measures relating to trade in goods;
Whereas, pursuant to the said general rules, the goods described in column 1 of the table annexed to this Regulation must be classified under the appropriate CN codes indicated in column 2, by virtue of the reasons set out in column 3;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Nomenclature Committee,
HAS ADOPTED THIS REGULATION:
Article 1
The goods described in column 1 of the annexed table are now classified within the combined nomenclature under the appropriate CN codes indicated in column 2 of the said table.
Article 2
This Regulation shall enter into force on the 21st day after its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 7 June 1989.
For the Commission
Christiane SCRIVENER
Member of the Commission
(1) OJ No L 256, 7. 9. 1987, p. 1.
(2) OJ No L 148, 1. 6. 1989, p. 1.
ANNEX
1.2.3 // // // // Description of the goods // CN code classification // Reasons // // // // (1) // (2) // (3) // // // // 1. Lumpwood charcoal impregnated with a heavy (waxy) hydrocarbon, the hydrocarbon content being approximately 25 % by weight of the whole and 2. Briquettes of wood charcoal agglomerated with a binder and containing sodium nitrate These goods are used as barbecue fuel and put up in 2 to 10 kg bags // 3606 90 90 // Classification is determined by the provisions of general rules 1 and 6, note 2 (a) to Chapter 36 and the texts of CN codes 3606 and 3606 90 90 As prepared fuels in solid form, these products cannot be classified under CN code 4402 (see the HS explanatory notes to code 3606, part II (C)) // // //
