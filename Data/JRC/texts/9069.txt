Commission Regulation (EC) No 923/2006
of 22 June 2006
amending Regulations (EC) No 1164/2005, (EC) No 1165/2005, (EC) No 1168/2005, (EC) No 1700/2005 and (EC) No 1845/2005 opening standing invitations to tender for the resale on the Community market of maize held by the Czech, Hungarian, Austrian, Polish and Slovak intervention agencies
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1784/2003 of 29 September 2003 on the common organisation of the market in cereals [1], and in particular Article 6 thereof,
Whereas:
(1) Commission Regulations (EC) No 1164/2005 [2], (EC) No 1165/2005 [3], (EC) No 1168/2005 [4], (EC) No 1700/2005 [5] and (EC) No 1845/2005 [6] open standing invitations to tender for the resale on the Community market of maize held by the Czech, Hungarian, Austrian, Polish and Slovak intervention agencies. Those invitations to tender expire on 28 June 2006.
(2) In order to guarantee livestock farmers and the livestock-feed industry supplies at competitive prices at the beginning of the 2006/2007 marketing year, the stocks of maize held by the Czech, Hungarian, Austrian, Polish and Slovak intervention agencies should continue to be made available on the cereal market.
(3) In the context of this extension, however, the weeks from 28 June 2006 when no invitation to tender will be made were not specified. Tenders could thus be lodged in good faith by traders during those weeks, although no Management Committee meetings are scheduled.
(4) Regulations (EC) No 1164/2005, (EC) No 1165/2005, (EC) No 1168/2005, (EC) No 1700/2005 and (EC) No 1845/2005 should be amended accordingly.
(5) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Cereals,
HAS ADOPTED THIS REGULATION:
Article 1
1. The second subparagraph of Article 4(1) of Regulations (EC) No 1164/2005, (EC) No 1165/2005, (EC) No 1168/2005, (EC) No 1700/2005 and (EC) No 1845/2005 is hereby replaced by the following:
"The time limit for the submission of tenders for subsequent partial invitations to tender shall be 15.00 (Brussels time) each Wednesday, with the exception of 2 August 2006, 16 August 2006 and 23 August 2006, i.e. weeks when no invitation to tender shall be made."
2. In the third subparagraph of Article 4(1) of Regulations (EC) No 1164/2005, (EC) No 1165/2005, (EC) No 1168/2005, (EC) No 1700/2005 and (EC) No 1845/2005, " 28 June 2006" is replaced by " 13 September 2006".
Article 2
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 22 June 2006.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 270, 21.10.2003, p. 78. Regulation as amended by Commission Regulation (EC) No 1154/2005 (OJ L 187, 19.7.2005, p. 11).
[2] OJ L 188, 20.7.2005, p. 4. Regulation as last amended by Commission Regulation (EC) No 714/2006 (OJ L 124, 11.5.2006, p. 11).
[3] OJ L 188, 20.7.2005, p. 7. Regulation as last amended by Commission Regulation (EC) No 1990/2005 (OJ L 320, 8.12.2005, p. 23).
[4] OJ L 188, 20.7.2005, p. 16. Regulation as last amended by Commission Regulation (EC) No 800/2006 (OJ L 144, 31.5.2006, p. 7).
[5] OJ L 273, 19.10.2005, p. 3.
[6] OJ L 296, 12.11.2005, p. 3. Regulation as amended by Commission Regulation (EC) No 703/2006 (OJ L 122, 9.5.2006, p. 7).
--------------------------------------------------
