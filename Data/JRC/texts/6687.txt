Commission Decision
of 23 March 2005
amending Decision 2004/832/EC as regards the eradication and emergency vaccination plan for feral pigs against classical swine fever in Northern Vosges, France
(notified under document number C(2005) 917)
(Only the French text is authentic)
(Text with EEA relevance)
(2005/264/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 2001/89/EC of 23 October 2001 on Community measures for the control of classical swine fever [1], and in particular the second subparagraph of Article 16(1) and the fourth subparagraph of Article 20(2) thereof,
Whereas:
(1) Commission Decision 2004/832/EC of 3 December 2004 approving the plans for the eradication of classical swine fever in feral pigs and the emergency vaccination of such pigs in the Northern Vosges, France [2] was adopted as one of a number of measures to combat classical swine fever.
(2) France has informed the Commission about the recent evolution of that disease in feral pigs in the Northern Vosges area of France. In the light of the epidemiological information, the eradication plan should be extended to the area of the Northern Vosges located west of the Rhine and the channel Rhine Marne, north of the motorway A 4, east of the river Sarre and south of the border with Germany. In addition, the emergency vaccination plan for feral pigs against classical swine fever should be amended to cover that area.
(3) Decision 2004/832/EC should therefore be amended accordingly.
(4) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
The Annex to Decision 2004/832/EC is replaced by the text in the Annex to this Decision.
Article 2
This Decision is addressed to the French Republic.
Done at Brussels, 23 March 2005.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 316, 1.12.2001, p. 5. Directive as amended by the 2003 Act of Accession.
[2] OJ L 359, 4.12.2004, p. 62.
--------------------------------------------------
ANNEX
--------------------------------------------------
