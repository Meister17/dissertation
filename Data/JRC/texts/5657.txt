Authorisation for State aid pursuant to Articles 87 and 88 of the EC Treaty
Cases where the Commission raises no objections
(2005/C 329/02)
(Text with EEA relevance)
Date of adoption : 27.7.2005
Member State : Lithuania
Aid No : N 44/2005
Title : Excise tax reduction on biofuels
Objective (sector) : Environmental protection. Tax advantage in favour of biofuel producers
Legal basis - Lietuvos Respublikos akcizų įstatymas (Žin., 2001, Nr. 98-3482; 2004, Nr. 26-802);
- Lietuvos Respublikos biokuro, biodegalų ir bioalyvų įstatymas (Žin., 2000, 64-1940; 2004, Nr. 28-870);
Budget : EUR 72000000 (LTL 250000000) in the period 2006-2010
Duration : 5 years
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision : 20.10.2005
Member State : Austria
Aid No : N 263/2005
Title : Broadband for Kärnten
Objective : To support the provision of basic and advanced broadband services, at conditions similar to urban areas, in certain areas of Kärnten which are currently not served and where there are no plans for coverage in the near future
Legal basis : The measure is based on the tender documentation for a broadband service concession by the Land of Kärnten and linked to the general development policy of the government of Kärnten as well as to the Austrian national broadband strategy
Budget : The maximum amount of public funds is EUR […] [1]
Aid intensity : only known after signature of procurement contract
Duration of project : 3 years subject to a possible extension of 1 year
Other information : The selected provider(s) will be mandated to provide non-discriminatory wholesale access to third party operators
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
[1] Business secret.
--------------------------------------------------
