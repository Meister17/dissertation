Authorisation for State aid pursuant to Articles 87 and 88 of the EC Treaty
Cases where the Commission raises no objections
(2005/C 250/03)
(Text with EEA relevance)
Date of adoption of the decision: 22.9.2004
Member State: Portugal
Aid No: N 161/04
Title: Stranded costs in Portugal
Objective: To compensate for stranded costs on the Portuguese electricity market
Legal basis: Projecto de Decreto-Lei CMEC
Budget: EUR 9216074579
Duration: Until 2027
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 9.3.2005
Member State: Italy
Aid No: N 173/2003
Title: Regione Campania — Risk capital fund for SMEs
Objective: To develop and widen the venture capital market in Campania and to support small and medium-sized enterprises
Legal basis:
Legge Regionale n. 10 dell' 11.10.2001, articolo 3; Misura 4.2, lettera g) del Complemento di Programmazione del Programma Operativo Regionale (P.O.R.) Regione Campania 2000-2006
Convenzione tra la Regione Campania e la Società di Gestione del Risparmio SGR Aggiudicataria;
Regolamento Quadro del Fondo Chiuso Regione Campania
Duration: 10 years
Other information: Annual report
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 26.7.2004
Member State: Ireland
Aid No: N 218/2004
Title: All-Island Collaborative R&D Pilot Scheme
Objective: To promote and develop R&D on a collaborative basis between Northern Ireland and the Republic of Ireland, in the sectors of biotechnology, medical devices and pharmaceuticals.
Legal basis: The British /Irish Agreement Act 1999
Budget: GBP 3 million (about EUR 4,1 million) of which approximately EUR 570000 for the first year, EUR 3 million for the second year and EUR 500000 for the third year, amounting in total EUR 4106665. Maximum grant per company: EUR 200000.
- 10 percentage points, when aid is to be given to SMEs;
- 10 percentage points, when aid is to be given to enterprises in assisted Areas 87(3)(a);
- 5 percentage points, when aid is to be given to enterprises in assisted Areas 87(3)(c).
- 10 percentage points where a research project is not in accordance with a specific project or programme undertaken as part of the Community's framework programme for R&D but has specific cooperation elements or spread of results.
Including top-ups, aid intensities must not exceed 75 % for industrial research and 50 % for pre-competitive development.
Duration: 3 years from the date of the Commission's approval
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption: 9.12.2004
Member State: Sweden
Aid No: N 312/2004
Title: Support for establishment of waste sorting facilities
Objective: To stimulate the establishment of waste sorting facilities in residences with several households in order to create better conditions for sorting of waste by the households
Legal basis: Förslag till lag om kreditering på skattekonto av belopp som beviljats för inrättande av källsorteringsutrymme
Budget: In total SEK 400 million (ca. EUR 44 million).
Intensity or amount: 30 % of the costs for the establishment of a waste sorting facility, but with a ceiling of SEK 100000 (ca EUR 11000) per waste sorting facility
Duration: 1.1.2005 until 30.6.2006
Other information: Annual report
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption: 16.3.2004
Member State: Denmark
Aid No: N 342/2003
Title: Support for windpower plants
Objective: Aid to contribute to the development of windpower with the environmental advantages thereof
Legal basis: Lov om ændring af lov om elforsyning og lov om tilskud til elproduktion (lov nr. 1091 af 17.12.2002)
Budget: In total, less than DKK 200 million (ca. EUR 26,8 million) during 2003-2008
- General price supplement: DKK 0,10 per kWh
- Compensation: DKK 0,023 per kWh
- Extra price supplement: DKK 0,10 per kWh
Duration: Maximum 20 years
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption: 16.3.2005
Member States: Spain
Aid No: N 423/04
Title: Aid to Shipbuilding companies/Grant
1. Regional
2. Research and development
3. Innovation
Legal basis: Real Decreto 442/1994
Budget: EUR 20000000 per annum
Aid intensity or amount: 12,5 % — 100 %
Duration: Until 31.12.2006
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption of the decision: 1.12.2004
Member State: Italy
Aid No: N 490/2000
Title: Stranded costs of the electricity sector
Objective: to offset those commitments and guarantees of operation which may no longer be honoured on account of the provisions of Directive 96/92/EC of 19 December 1996; State aid granted to companies operating in the electricity sector in order to cover the eligible "stranded costs" is designed to facilitate the transition from a regulated market dominated by a monopoly in production and distribution to a competitive electricity market
Legal basis: Decreti: 26 gennaio 2000; 17 aprile 2001; 4 agosto 2004; Legge 17 aprile 2003 n. 83; lettera dei Ministri Marzano (Attività produttive) e Siniscalco (Economia e Finanze) al Commissario Monti del 29.9.2004
Duration: Four years (2000-2003) for the stranded impianti; ten years (2000-2009) for the stranded GLN Nigeriano
Other information: Annual report
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption: 20.7.2005
Member States: Czech Republic
Aid No: N 597/2004
Title: Regional investment aid to Lignit Hodonín, s.r.o./grant
Objective: Regional
Legal basis: Nařízení vlády č. 974 z 6. října 2004
Budget: CZK 324 million (EUR 10,2 million)
Aid intensity or amount: CZK 155,5 million (EUR 5 million)
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
Date of adoption: 19.5.2004
Member State: Denmark
Aid No: N 618/2003
Title: "Prolongation of N 1037/95 for certain CHPs"
Objective: To promote the use of renewable energy
Legal basis: Forslag til ændring af lov om elforsyning (særligt ændringspunkt nr. 16)
- DKK 5,15/kW (for industrial CHPs),
- DKK 8,67/kW (for decentralised CHPs) and DKK 10,51/kW (for waste-based CHPs)
- "Bigger" CHPs (at least 10 MW): 1 year (until 31.12.2004)
- "Smaller" CHPs (< 10 MW): max. 3 years (until 31.12.2006)
- "Very small" CHPs (< 5 MW): unlimited
The authentic text(s) of the decision, from which all confidential information has been removed, can be found at:
http://europa.eu.int/comm/secretariat_general/sgb/state_aids/
--------------------------------------------------
