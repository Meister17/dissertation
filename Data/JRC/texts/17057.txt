Commission Regulation (EC) No 2066/2004
of 1 December 2004
fixing the definitive rate of refund and the percentage of system B export licences to be issued in the fruit and vegetables sector (tomatoes, oranges, table grapes and apples)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 2200/96 of 28 October 1996 on the common organisation of the market in fruit and vegetables [1],
Having regard to Commission Regulation (EC) No 1961/2001 of 8 October 2001 on detailed rules for implementing Council Regulation (EC) No 2200/96 as regards export refunds on fruit and vegetables [2], and in particular Article 6(7) thereof,
Whereas:
(1) Commission Regulation (EC) No 1425/2004 [3] fixed the indicative quantities for the issue of B system export licences.
(2) The definitive rate of refund for tomatoes, oranges, table grapes and apples covered by licences applied for under system B between 17 September and 15 November 2004 should be fixed at the indicative rate, and the percentage of licences to be issued for the quantities applied for should be laid down,
HAS ADOPTED THIS REGULATION:
Article 1
For applications for system B export licences submitted pursuant to Article 1 of Regulation (EC) No 1425/2004 between 17 September and 15 November 2004, the percentages of licences to be issued and the rates of refund applicable are fixed in the Annex hereto.
Article 2
This Regulation shall enter into force on 2 December 2004.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 1 December 2004.
For the Commission
J. M. Silva Rodríguez
Director-General for Agriculture and Rural Development
--------------------------------------------------
[1] OJ L 297, 21.11.1996, p. 1. Regulation as last amended by Commission Regulation (EC) No 47/2003 (OJ L 7, 11.1.2003, p. 1).
[2] OJ L 268, 9.10.2001, p. 8. Regulation as amended by Regulation (EC) No 1176/2002 (OJ L 170, 29.6.2002, p. 69).
[3] OJ L 262, 7.8.2004, p. 5.
--------------------------------------------------
+++++ ANNEX 1 +++++</br>
