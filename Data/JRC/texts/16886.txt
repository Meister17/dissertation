Political and Security Committee Decision Proxima/2/2004
of 30 November 2004
concerning the appointment of the Head of Mission of the EU Police Mission in the former Yugosla
v Republic of Macedonia, EUPOL Proxima
(2004/846/EC)
THE POLITICAL AND SECURITY COMMITTEE,
Having regard to the Treaty on European Union and in particular Article 25(3) thereof,
Having regard to Council Joint Action 2004/789/CFSP of 22 November 2004 on the extension of the European Union Police Mission in the former Yugoslav Republic of Macedonia (EUPOL Proxima), and in particular Article 8(1) thereof,
Whereas:
(1) Article 8(1) of Joint Action 2004/789/CFSP provides that the Council authorises the Political and Security Committee to take the relevant decisions in accordance with Article 25 of the TEU, including the powers to appoint, upon a proposal by the Secretary General/High Representative, a Head of Mission.
(2) The Secretary General/High Representative has proposed the appointment of Mr Jürgen SCHOLZ,
HAS DECIDED AS FOLLOWS:
Article 1
Mr Jürgen SCHOLZ is hereby appointed Head of Mission of the European Union Police Mission in the former Yugoslav Republic of Macedonia (EUPOL Proxima) from 15 December 2004.
Article 2
This Decision shall take effect on the day of its adoption.
It shall apply until 14 December 2005.
Done at Brussels, 30 November 2004.
For the Political and Security Committee
The President
A. Hamer
--------------------------------------------------
