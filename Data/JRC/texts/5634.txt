P6_TA(2005)0038
Statistics relating to vocational training in enterprises ***I
European Parliament legislative resolution on the proposal for a regulation of the European Parliament and of the Council on the statistics relating to vocational training in enterprises (COM(2004)0095 — C5-0083/2004 — 2004/0041(COD))
(Codecision procedure: first reading)
The European Parliament,
- having regard to the Commission proposal to the European Parliament and the Council (COM(2004)0095) [1],
- having regard to Articles 251(2) and 285(1) of the EC Treaty, pursuant to which the Commission submitted the proposal to Parliament (C5-0083/2004),
- having regard to Rule 51 of its Rules of Procedure,
- having regard to the report of the Committee on Employment and Social Affairs (A6-0033/2005),
1. Approves the Commission proposal as amended;
2. Calls on the Commission to refer the matter to Parliament again if it intends to amend the proposal substantially or replace it with another text;
3. Instructs its President to forward its position to the Council and Commission.
[1] Not yet published in OJ.
--------------------------------------------------
