COMMISSION DIRECTIVE of 24 May 1978 amending Directive 74/268/EEC laying down special conditions concerning the presence of "Avena fatua" in fodder plant and cereal seed (78/511/EEC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Directive 66/401/EEC of 14 June 1966 on the marketing of fodder plant seed (1), as last amended by Council Directive 78/55/EEC (2), and in particular Article 11 thereof,
Whereas the said Directive laid down tolerances in respect of the presence of Avena fatua in fodder plant seed;
Whereas these tolerances appeared to be too high for certain requirements;
Whereas Commission Directive 74/268/EEC of 2 May 1974 (3) therefore laid down special conditions in respect of the presence of Avena fatua in fodder plant seed;
Whereas the First Commission Directive 78/386/EEC of 18 April 1978 amending the Annexes to Council Directive 66/401/EEC on the marketing of fodder plant seed (4) laid down new conditions at Community level as regards the presence of Avena fatua in fodder plant seed;
Whereas because of these amendments the special conditions contained in Directive 74/268/EEC are no longer necessary;
Whereas these special conditions should therefore be repealed as far as fodder plant seed is concerned;
Whereas the measures provided for in this Directive are in accordance with the opinion of the Standing Committee on Seeds and Propagating Material for Agriculture, Horticulture and Forestry,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Article 1 of Directive 74/268/EEC is hereby repealed.
Article 2
Member States shall bring into force, not later than 1 July 1980, the laws, regulations or administrative provisions necessary to comply with this Directive. They shall forthwith inform the Commission thereof, who shall inform the other Member States.
Article 3
This Directive is addressed to the Member States.
Done at Brussels, 24 May 1978.
For the Commission
Finn GUNDELACH
Vice-President (1)OJ No 125, 11.7.1966, p. 2298/66. (2)OJ No L 16, 20.1.1978, p. 23. (3)OJ No L 141, 24.5.1974, p. 19. (4)OJ No L 113, 25.4.1978, p. 1.
