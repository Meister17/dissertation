Commission Decision
of 27 December 2000
amending Decision 1999/465/EC establishing the officially enzootic-bovine-leukosis-free status of bovine herds of certain Member States or regions of Member States
(notified under document number C(2000) 4146)
(Text with EEA relevance)
(2001/28/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 64/432/EEC of 26 June 1964 on health problems affecting intra-Community trade in bovine animals and swine(1), as last amended by Directive 2000/20/EC(2), and in particular Annex DI(E) thereof,
Whereas:
(1) Commission Decision 1999/465/EC of 13 July 1999 establishing the officially enzootic-bovine-leukosis-free status of bovine herds of certain Member States or regions of Member States(3) granted this status to certain Member States and regions thereof.
(2) The competent authorities of Sweden submitted to the Commission documentation demonstrating compliance with all of the conditions provided for in Annex DI(E) of Directive 64/432/EEC.
(3) It appears therefore appropriate to consider Sweden officially enzootic-bovine-leukosis-free in accordance with the provisions of the above Directive, and to amend Decision 1999/465/EC accordingly.
(4) The measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
Decision 1999/465/EC is amended by adding the word "Sweden" to the list of Member States in Annex I.
Article 2
This Decision is adressed to the Member States.
Done at Brussels, 27 December 2000.
For the Commission
David Byrne
Member of the Commission
(1) OJ 121, 29.7.1964, p. 1977/64.
(2) OJ L 163, 4.7.2000, p. 35.
(3) OJ L 181, 16.7.1999, p. 32.
