*****
COMMISSION REGULATION (EEC) No 411/88
of 12 February 1988
on the method and the rate of interest to be used for calculating the costs of financing intervention measures comprising buying-in, storage and disposal
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 1883/78 of 2 August 1978 laying down general rules for the financing of interventions by the European Agricultural Guidance and Guarantee Fund, Guarantee Section (1), as last amended by Regulation (EEC) No 2095/87 (2), and in particular Article 5 thereof,
Whereas the provisions concerning the calculation of the costs of financing intervention measures comprising buying-in, storage and disposal, initially laid down in Commission Regulation (EEC) No 467/77 (3), as last amended by Regulation (EEC) No 331/87 (4), have been amended a number of times since their adoption; whereas, by reason of their number, complexity and dispersal among various issues of the Official Journal of the European Communities, the relevant texts are difficult to use and thus lack the clarity which should be an essential feature of all legislation; whereas they should therefore be consolidated;
Whereas, under Article 4 (1) of Regulation (EEC) No 1883/78, an account is drawn up for each Member State and for each financial year showing the net losses sustained by the intervention agencies concerned;
Whereas these accounts include financing costs which are to be calculated by a method and at a rate of interest adopted in accordance with the procedure laid down in Article 13 of Council Regulation (EEC) No 729/70 (5), as last amended by Regulation (EEC) No 3769/85 (6);
Whereas the financing costs should be calculated according to a method which takes account of the volume of the stocks, of the different ways in which the goods are presented for intervention, of the depreciation of goods in storage since the beginning of the financial year and also of changes in the intervention prices of products during the financial year; whereas this method should be easy to apply;
Whereas the interest rate must be representative of the cost of money prevailing in the Community;
Whereas, with a view to ensuring that the Community budget continues to be implemented under appropriate conditions, Regulation (EEC) No 1883/78 authorizes the Commission to fix the uniform interest rate at a level below its representative level for the 1986, 1987 and 1988 budget years; whereas the budget year includes, for this type of expenditure, expenditure on material operations from 1 October of the previous year to 30 September; whereas, in these circumstances, the interest rate has been reduced to 7 %;
Whereas the second paragraph of Article 5 of Regulation (EEC) No 1883/78 empowered the Commission to fix the uniform interest rate at a lower level in the case of Member States which have interest costs below those which result from the application of the uniform interest rate for calculating financial costs; whereas the uniform interest rate has been fixed at 7 % since 1 December 1985; whereas in comparison with the situation in 1985 the interest rates in two Member States fell below the uniform interest rate during 1986; whereas the specific interest rate to be applied in those Member States should be fixed;
Whereas Article 7 of Council Regulation (EEC) No 1883/78 provides that where the products in question experience a depreciation as a result of storage, the financial effect thereof is to be recorded and taken into account at the time of entry into intervention; whereas the basis for calculating the financing costs which are used in determining the net losses of the intervention agencies is thereby changed; whereas the corresponding depreciation must therefore be taken into account when calculating the average value per tonne of the product;
Whereas, under the rules governing common market organizations, provision may be made for payment for a product bought in by an intervention agency to be effected only after a certain period; whereas the method used for calculating interest charges should therefore be adjusted to take account of the period for payment where this is stipulated in the rules;
Whereas, where payment for a product bought in by an intervention agency is effected after a certain period, the quantities to be used for the calculation of interest charges are to be reduced; whereas it transpires that, as a
consequence of the extension of periods for payment and of major buying-in in certain sectors at the end of the financial year, this reduction may produce a negative result; whereas the method of calculation should be adjusted to take account of this effect;
Whereas the rules governing the common market organizations or Community invitations to tender governing the sale of agricultural products in public intervention may allow the purchaser of such products a period in which to remove them, once payment has been made; whereas the method for calculating interest charges should be altered to take account of such removal period;
Whereas, on account of the exceptionally high levels of stocks of certain agricultural products in intervention storage, the Commission has, in certain instances, provided for a period in which payment may be made after the purchaser has removed the products from store; whereas it is thus appropriate, as an exceptional measure, to adjust the method of calculating interest charges for the sales concerned, in order to take account of such period for payment;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the EAGGF Committee,
HAS ADOPTED THIS REGULATION:
Article 1
1. The interest costs referred to in Article 1 (1) (b) of Council Regulation (EEC) No 3247/81 (1) shall be calculated by applying the rate fixed in Articles 3 and 4 (2) thereof to the average value per tonne of product which has been the subject of intervention and by multiplying the figure obtained by the average stock over the financial year in question.
2. The average value per tonne of product shall be calculated by dividing the sum of the value of products in store on the first day of the financial year and the value of products bought in during that year by the sum of the quantity of products in store on the first day of the financial year and the quantity of products bought in during the financial year.
3. The average stock for the financial year shall be calculated by adding the total stock at the beginning of each month to the total stock at the end of each month and dividing the total thus obtained by twice the number of months in the financial year.
Article 2
1. Where a coefficient of depreciation is fixed for a product pursuant to Article 7 of Regulation (EEC) No 1883/78, the value of the products bought in during the financial year shall be calculated by multiplying the buying-in price by this coefficient.
In the case of products for which a reduction in value has been fixed pursuant to the second subparagraph of Article 8 of Regulation (EEC) No 1883/78, the calculation of average stocks shall be made before the effective date of each reduction in value taken into account for the purposes of the average value.
2. Where it is stipulated, in the rules governing common market organizations, that payment for a product bought in by an intervention agency may not be effected until a minimum of one month has elapsed after the date of acceptance of delivery, the average stock calculated in accordance with paragraph 3 shall be reduced by a quantity resulting from the following calculation:
Q x N
12
1.2 // Q = // quantities bought in during the financial year, // N = // number of months of minimum period for payment.
For the purposes of this calculation, the minimum period given in the rules shall be taken as the period for payment. A month shall be considered as consisting of 30 days. Any part of a month longer than 15 days shall be considered a whole month; any part of a month equal to or less than 15 days shall not be taken into account for this calculation.
Where the calculation of average stock at the end of the year gives a negative result once the reduction referred to in the first subparagraph has been effected, that amount shall be deducted from the average stock calculated for the following financial year.
3. Where, for the sale of products by intervention agencies, the rules governing the common market organization or notices of invitation to tender allow the purchaser of such products a period in which to remove them, once payment has been made, and where such period exceeds 30 days, the financing costs calculated in accordance with the preceding paragraphs shall be reduced by the amount produced by the following calculation:
V x J x i
365
1.2 // V = // amount paid by the purchaser, // J = // number of days between receipt of payment and removal of product, less 30, // i = // interest rate specified in
(1) OJ No L 327, 14. 11. 1981, p. 1.
4. In respect of sales of agricultural products by intervention agencies based on specific Community regulations, where the actual period for payment after removal of such products exceeds 30 days, the financing costs calculated in accordance with the preceding paragraphs shall be increased by the amount produced by the following calculation:
M x D x i
365
1.2 // M = // amount to be paid by purchaser, // D = // number of days between removal of product and receipt of payment, less 30, // i = // interest rate specified in Article 3.
Article 3
The interest rate referred to in Article 5 of Regulation (EEC) No 1883/78 shall be 7 %.
Article 4
1. If the rate of interest costs borne by a Member State is lower for at least 6 months than the uniform interest rate fixed for the Community, a specific interest rate shall be fixed for that Member State.
2. For the period beginning 1 December 1986 the specific interest rate shall be:
- 6 % for Germany,
- 6 % for the Netherlands.
Article 5
Regulation (EEC) No 467/77 is hereby repealed.
Article 6
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 12 February 1988.
For the Commission
Frans ANDRIESSEN
Vice-President
(1) OJ No L 216, 5. 8. 1978, p. 1.
(2) OJ No L 196, 17. 7. 1987.
(3) OJ No L 62, 8. 3. 1977, p. 9.
(4) OJ No L 32, 3. 2. 1987, p. 10.
(5) OJ No L 94, 28. 4. 1970, p. 13.
(6) OJ No L 362, 31. 12. 1985, p. 17. Article 3.
