Information communicated by Member States regarding State aid granted under Commission Regulation (EC) No 68/2001 of 12 January 2001 on the application of Articles 87 and 88 of the EC Treaty to training aid, as amended by Commission Regulation (EC) No 363/2004 of 25 February 2004
(2006/C 239/07)
(Text with EEA relevance)
Aid No | XT 1/06 |
Member State | Estonia |
Region | Estonia |
Title of aid scheme or name of company receiving individual aid | National Development Plan Action 1.3 RAK meede 1.3 "Inclusive Labour Market" |
Legal basis | Sotsiaalministri 4. aprilli 2006.a määrus nr 35 "Meetme 1.3 "Võrdsed võimalused tööturul", välja arvatud riigi tööturuasutuste projektidele antava toetuse, tingimused ja toetuse kasutamise seire eeskiri" (RTL, 11.04.2006, 31, 553) |
Annual expenditure planned or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount | Up to EUR 0,19 million (2006) |
Loans guaranteed | |
Individual aid | Overall aid amount | |
Loans guaranteed | |
Maximum aid intensity | In conformity with Article 4(2)-(7) of the Regulation | Yes | |
Date of implementation | 14 April 2006 |
Duration of scheme or individual aid award | Until 31 December 2006 |
Objective of aid | General training | Yes |
Specific training | Yes |
Economic sectors concerned | Limited to specific sectors | |
Agriculture | Yes |
Fisheries and aquaculture | Yes |
Coalmining | No |
All manufacturing | Yes |
All services | Yes |
Name and address of the granting authority | Tööturuamet |
Gonsiori 29 EE-15156 Tallinn |
Large individual aid grants | In conformity with Article 5 of the Regulation | Yes | |
--------------------------------------------------
