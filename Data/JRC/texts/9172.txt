Commission Regulation (EC) No 1297/2006
of 31 August 2006
amending the representative prices and additional duties for the import of certain products in the sugar sector fixed by Regulation (EC) No 1002/2006 for the 2006/2007 marketing year
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 318/2006 of 20 February 2006 on the common organisation of the markets in the sugar sector [1],
Having regard to Commission Regulation (EC) No 951/2006 of 30 June 2006 laying down detailed rules for the implementation of Council Regulation (EC) No 318/2006 as regards trade with third countries in the sugar sector [2], and in particular of the Article 36,
Whereas:
(1) The representative prices and additional duties applicable to imports of white sugar, raw sugar and certain syrups for the 2006/2007 marketing year are fixed by Commission Regulation (EC) No 1002/2006 [3]. These prices and duties have been last amended by Commission Regulation (EC) No 1251/2006 [4].
(2) The data currently available to the Commission indicate that the said amounts should be changed in accordance with the rules and procedures laid down in Regulation (EC) No 951/2006,
HAS ADOPTED THIS REGULATION:
Article 1
The representative prices and additional duties on imports of the products referred to in Article 36 of Regulation (EC) No 951/2006, as fixed by Regulation (EC) No 1002/2006 for the 2006/2007 marketing year are hereby amended as set out in the Annex to this Regulation.
Article 2
This Regulation shall enter into force on 1 September 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 31 August 2006.
For the Commission
Jean-Luc Demarty
Director-General for Agriculture and Rural Development
[1] OJ L 55, 28.2.2006, p. 1.
[2] OJ L 178, 1.7.2006, p. 24.
[3] OJ L 178, 1.7.2006, p. 36.
[4] OJ L 227, 19.8.2006, p. 38.
--------------------------------------------------
ANNEX
Amended representative prices and additional duties applicable to imports of white sugar, raw sugar and products covered by CN code 17029099 applicable from 1 September 2006
(EUR) |
CN code | Representative price per 100 kg of the product concerned | Additional duty per 100 kg of the product concerned |
17011110 [1] | 22,86 | 4,87 |
17011190 [1] | 22,86 | 10,10 |
17011210 [1] | 22,86 | 4,68 |
17011290 [1] | 22,86 | 9,67 |
17019100 [2] | 29,66 | 10,40 |
17019910 [2] | 29,66 | 5,88 |
17019990 [2] | 29,66 | 5,88 |
17029099 [3] | 0,30 | 0,35 |
[1] Fixed for the standard quality defined in Annex I.III to Council Regulation (EC) No 318/2006 (OJ L 58, 28.2.2006, p. 1).
[2] Fixed for the standard quality defined in Annex I.II to Regulation (EC) No 318/2006.
[3] Fixed per 1 % sucrose content.
--------------------------------------------------
