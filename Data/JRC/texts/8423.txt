COMMISSION DIRECTIVE 1999/76/EC
of 23 July 1999
establishing a Community method of analysis for the determination of lasalocid sodium in feedingstuffs
(Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 70/373/EEC of 20 July 1970 on the introduction of Community methods of sampling and analysis for the official control of feedingstuffs(1), as last amended by the Act of Accession of Austria, Finland and Sweden, and in particular Article 2 thereof,
(1) Whereas Directive 70/373/EEC stipulates that official controls of feedingstuffs for the purpose of checking compliance with the requirements arising under the laws, regulations and administrative provisions governing their quality and composition must be carried out using Community methods of sampling and analysis;
(2) Whereas Council Directive 70/524/EEC of 23 November 1970 concerning additives in feedingstuffs(2), as last amended by Commission Regulation (EC) No 866/1999 of 26 April 1999(3) stipulates that the lasalocid sodium content must be indicated on the labelling where these substances are added to premixtures and feedingstuffs;
(3) Whereas Community methods of analysis must be established for checking these substances;
(4) Whereas the measures provided for in this Directive are in accordance with the opinion of the Standing Committee for Feedingstuffs,
HAS ADOPTED THIS DIRECTIVE:
Article 1
The Member States shall provide that analyses conducted with a view to official controls of the lasalocid content of feedingstuffs and premixtures are carried out using the method set out in Annex hereto.
Article 2
The Member States shall bring into force, not later than 31 January 2000, the laws, regulations or administrative provisions necessary to comply with the provisions of this Directive. They shall immediately inform the Commission thereof.
They shall apply the measures from 1 February 2000.
When Member States adopt these measures, they shall contain a reference to this Directive or shall be accompanied by such reference at the time of their official publication. The procedure for such reference shall be adopted by Member States.
Article 3
This Directive shall enter into force on the 20th day following its publication in the Official Journal of the European Communities.
Article 4
This Directive is addressed to the Member States.
Done at Brussels, 23 July 1999.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ L 170, 3.8.1970, p. 2.
(2) OJ L 270, 14.12.1970, p. 1.
(3) OJ L 108, 27.4.1999, p. 20.
ANNEX
DETERMINATION OF LASALOCID SODIUM
Sodium salt of a polyether monocarboxylic acid produced by Streptomyces lasaliensis
1. Purpose and scope
The method is for the determination of lasalocid sodium in feedingstuffs and premixtures. The limit of detection is 5 mg/kg, the limit of determination is 30 mg/kg.
2. Principle
Lasalocid sodium is extracted from the sample into acidified methanol and determined by reversed-phase high performance liquid chromatography (HPLC) using a spectrofluorimetric detector.
3. Reagents
3.1. Potassium dihydrogen phosphate (KH2PO4)
3.2. Orthophosphoric acid, w = 85 %
3.3. Orthophosphoric acid solution, σ = 20 %
Dilute 23,5 ml of orthophosphoric acid (3.2) to 100 ml with water.
3.4. 6-Methyl-2-heptylamine (1,5-dimethylhexylamine), w = 99 %
3.5. Methanol, HPLC grade
3.6. Hydrochloric acid, ρ20 1,19 g/ml
3.7. Phosphate buffer solution, c = 0,01 mol/l
Dissolve 1,36 g of KH2PO4 (3.1) in 500 ml of water (3.11 ), add 3,5 ml of orthophosphoric acid (3.2) and 10,0 ml of 6-methyl-2-heptylamine (3.4). Adjust the pH to 4,0 with orthophosphoric acid solution (3.3) and dilute to 1000 ml with water (3.11).
3.8. Acidified methanol
Transfer 5,0 ml of hydrochloric acid (3.6) into a 1000 ml graduated flask, make up to the mark with methanol (3.5) and mix. This solution must be prepared freshly before use.
3.9. HPLC mobile phase, phosphate buffer-methanol solution 5 + 95 (V + V)
Mix 5 ml of phosphate buffer solution (3.7) with 95 ml of methanol (3.5).
3.10. Lasalocid sodium standard substance with guaranteed purity, C34H53O8Na (sodium salt of a polyether monocarboxylic acid produced by Streptomyces lasaliensis), E763
3.10.1. Lasalocid sodium stock standard solution, 500 μg/ml
Weigh to the nearest 0,1 mg, 50 mg of lasalocid sodium (3.10) into a 100 ml graduated flask, dissolve in acidified methanol (3.8), make up to the mark with the same solvent and mix. This solution must be freshly prepared before use.
3.10.2. Lasalocid sodium intermediate standard solution, 50 μg/ml
Pipette 10,0 ml of stock standard solution (3.10.1) into a 100 ml graduated flask, make up to the mark with acidified methanol (3.8) and mix. This solution must be prepared freshly before use.
3.10.3. Calibration solutions
Into a series of 50 ml graduated flasks transfer 1.0, 2.0, 4.0, 5.0 and 10.0 ml of the intermediate standard solution (3.10.2). Make up to the mark with acidified methanol (3.8) and mix. These solutions correspond to 1.0, 2.0, 4.0, 5.0 and 10.0 μg of lasalocid sodium per ml respectively. These solutions must be prepared freshly before use.
3.11. Water, HPLC grade
4. Apparatus
4.1. Ultrasonic bath (or shaking water-bath) with temperature control
4.2. Membrane filters, 0,45 μm
4.3. HPLC equipment with injection system, suitable for injecting volumes of 20 μl.4.3.1. Liquid chromatographic column 125 mm x 4 mm, reversed-phase C18, 5 μm packing or equivalent
4.3.2. Spectrofluorimeter with variable wavelength adjustment of excitation and emission wavelengths
5. Procedure
5.1. General
5.1.1. Blank feed
For the performance of the recovery test (5.1.2) a blank feed should be analysed to check that neither lasalocid sodium nor interfering substances are present. The blank feed should be similar in type to that of the sample and lasalocid sodium or interfering substances should not be detected.
5.1.2. Recovery test
A recovery test should be carried out by analysing the blank feed which has been fortified by addition of a quantity of lasalocid sodium, similar to that present in the sample. To fortify at a level of 100 mg/kg, transfer 10,0 ml of the stock standard (3.10.1) to a 250 ml conical flask and evaporate the solution to approximately 0,5 ml. Add 50 g of the blank feed, mix thoroughly and leave for 10 minutes mixing again several times before proceeding with the extraction step (5.2).
Alternatively, if a blank feed similar in type to that of the sample is not available (see 5.1.1), a recovery test can be performed by means of the standard addition method. In this case the sample to be analysed is fortified with a quantity of lasalocid sodium similar to that already present in the sample. This sample is analysed together with the unfortified sample and the recovery calculated by subtraction.
5.2. Extraction
5.2.1. Feedingstuffs
Weigh to the nearest 0,01 g, from 5 g to 10 g of the sample into a 250 ml conical flask with stopper. Add 100,0 ml of acidified methanol (3.8) by pipette. Stopper loosely and swirl to disperse. Place the flask in an ultrasonic bath (4.1) at approximately 40° C for 20 minutes, then remove and cool to room temperature. Allow to stand for about 1 hour until the suspended matter has settled, then filter an aliquot portion through a 0,45 μm membrane filter (4.2) into a suitable vessel. Proceed to the HPLC determination (5.3).
5.2.2. Premixtures
Weigh to the nearest 0,001 g about 2 g of the unground premix into a 250 ml graduated flask. Add 100,0 ml of acidified methanol (3.8) and swirl to disperse. Place the flask and contents in an ultrasonic bath (4.1) at approximately 40° C for 20 minutes, then remove and cool to room temperature. Dilute to the mark with acidified methanol (3.8) and mix thoroughly. Allow to stand for one hour until the suspended matter has settled, then filter an aliquot portion through a 0,45 μm membrane filter (4.2). Dilute an appropriate volume of the clear filtrate with acidified methanol (3.8) to produce a final test solution containing about 4 μg/ml of lasalocid sodium. Proceed to the HPLC determination (5.3).
5.3. HPLC Determination
5.3.1. Parameters
The following conditions are offered for guidance; other conditions may be used, provided they yield equivalent results:
>TABLE>
Check the stability of the chromatographic system, injecting the calibration solution (3.10.3) containing 4,0 μg/ml several times, until constant peak heights (or areas) and retention times are achieved.
5.3.2. Calibration graph
Inject each calibration solution (3.10.3) several times and determine the mean peak heights (areas) for each concentration. Plot a calibration graph using the mean peak heights (areas) as the ordinates and the corresponding concentrations in μg/ml as the abscissae.
5.3.3. Sample solution
Inject the sample extracts obtained in 5.2.1 or 5.2.2 several times, using the same volume as taken for the calibration solution and determine the mean peak heights (areas) of the lasalocid sodium peaks.
6. Calculation of results
From the mean peak height (area) produced by injection of the sample solution (5.3.3) determine the concentration of lasalocid sodium (μg/ml) by reference to the calibration graph.
6.1. Feedingstuffs
The lasalocid sodium content, w (mg/kg) in the sample is given by the following formula:
>REFERENCE TO A GRAPHIC>
where:
ß= lasalocid sodium concentration of the sample solution (5.2.1) in μg/ml
V1= volume of the sample extract according to 5.2.1 in ml (i.e. 100)
m= mass of the test portion in g
6.2. Premixtures
The lasalocid sodium content, w (mg/kg) in the sample is given by the following formula:
>REFERENCE TO A GRAPHIC>
where:
ß= lasalocid sodium concentration of the sample solution (5.2.2) in μg/ml
V2= volume of the sample extract according to 5.2.2 in ml (i.e. 250)
f= dilution factor according to 5.2.2
m= mass of the test portion in g
7. Validation of the results
7.1. Identity
Methods based on spectrofluorimetry are less subject to interference than those in which UV detection is used. The identity of the analyte can be confirmed by co-chromatography.
7.1.1. Co-chromatography
A sample extract (5.2.1 or 5.2.2) is fortified by the addition of an appropriate amount of a calibration solution (3.10.3). The amount of added lasalocid sodium should be similar to the amount of lasalocid sodium found in the sample extract. Only the height of the lasalocid sodium peak should be enhanced to an extent based on the amount of lasalocid sodium added and the dilution of the extract. The peak width, at half height, must be within ± 10 % of the original peak width produced by the unfortified sample extract.
7.2. Repeatability
The difference between the results of two parallel determinations carried out on the same sample must not exceed:
- 15 % relative to the higher value for lasalocid sodium contents from 30 mg/kg to 100 mg/kg;
- 15 mg/kg for lasalocid sodium contents from 100 mg/kg to 200 mg/kg;
- 7,5 % relative to the higher value for lasalocid sodium contents of more than 200 mg/kg.
7.3. Recovery
For the fortified (blank) feed sample, the recovery should be at least 80 %. For the fortified premixture samples, the recovery should be at least 90 %.
8. Results of a collaborative study
A collaborative study(1) was arranged in which 2 premixtures (samples 1 and 2) and 5 feeds (samples 3-7) were analysed by 12 laboratories. Duplicate analyses were performed on each sample. The results are given in the following table:
>TABLE>
L= number of laboratories
n= number of single results
Sr= standard deviation of repeatability
SR= standard deviation of reproducibility
CVr= coefficient of variation of repeatability, %
CVR= coefficient of variation of reproducibility, %
(1) Analyst, 1995, 120, 2175-2180.
