Appeal brought on 14 September 2006 by Ocean Trawlers Ltd against the judgment of the Court of First Instance (First Chamber) delivered on 13 June 2006 in Joined Cases T-218/03 to T-240/03: Cathal Boyle and others v Commission of the European Communities
Parties
Appellant: Ocean Trawlers Ltd (represented by: P. Gallagher SC, A. Collins SC, D. Barry, Solicitor)
Other parties to the proceedings: Ireland, Commission of the European Communities
Form of order sought
The applicant claims that the Court should:
- Set aside the judgment of the Court of First Instance of June 13, 2006 in so far as it dismissed the application in Case T-226/03, Ocean Trawlers Ltd. V. Commission for the annulment of Commission Decision 2003/245/EC [1] of 4 April 2003 on the requests received by the Commission to increase MAGP IV objectives to take into account improvements on safety, navigation at sea, hygiene, product quality and working conditions for vessels of more than 12 m in length overall as it applied to the safety capacity application for proposed new RSW vessel MFV Golden Rose and ordered Ocean Trawlers Ltd. to bear its own costs.
- Annul Commission Decision 2003/245/EC of 4 April 2003 on the requests received by the Commission to increase MAGP IV objectives to take into account improvements on safety, navigation at sea, hygiene, product quality and working conditions for vessels of more than 12 m in length overall as it applied to the safety capacity application for a proposed new RSW vessel MFV Golden Rose.
- Order the Commission to pay the costs of the entirety of these proceedings.
Pleas in law and main arguments
The appellant submits that the judgment of the Court of First Instance should be set aside on the following grounds:
By determining the appellant's interest in bringing the proceedings by reference to the date of the adoption of decision 2003/245 and not the date on which the application was lodged the Court of First Instance applied an incorrect legal test;
The Court made a substantive error apparent from the documents submitted to it, namely as to the appellant's ownership of the MFV "Golden Rose" at all times material to the application;
The finding that the appellant was not individually concerned by decision 2003/245 "since the vessels in question are fictitious" has no basis in law and is, moreover, contradicted by the reasoning of the Court of First Instance in its judgment;
The appellant is, and at all material times has been, the owner of the MFV "Golden Rose". It therefore cannot be said to have lost the interest it unquestionably had at the commencement of its action for the annulment of decision 2003/245 in so far as it impacted upon its application for safety tonnage in respect of the proposed MFV "Golden Rose";
The Court of First Instance erred in finding that the appellant was deprived of standing to seek the annulment of decision 2003/245 by reason of the steps it took to mitigate the loss and damage sustained as a result of that measure.
[1] OJ L 90, P. 48
--------------------------------------------------
