COMMISSION REGULATION (EC) No 1983/98 of 17 September 1998 amending Regulation (EEC) No 2273/93 determining the intervention centres for cereals
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 1766/92 of 30 June 1992 on the common organisation of the market in cereals (1), as last amended by Commission Regulation (EC) No 923/96 (2), and in particular Article 5 thereof,
Whereas the intervention centres are determined in the Annex to Commission Regulation (EEC) No 2273/93 (3), as last amended by Regulation (EC) No 1877/97 (4); whereas some Member States have asked for that Annex to be amended; whereas those requests should be acceded to;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Cereals,
HAS ADOPTED THIS REGULATION:
Article 1
The Annex to Regulation (EEC) No 2273/93 is amended as follows:
1. in the 'France` section, the amendments are as follows:
>TABLE>
2. in the 'Österreich` section:
- the 'Absdorf-Hippersdorf` centre becomes 'Absdorf`,
- the 'Palterndorf-Dobermannsdorf` centre becomes 'Dobermannsdorf`,
- the 'Siebenbrunn-Leopoldsdorf` centre becomes 'Untersiebenbrunn`;
3. in the 'Suomi` section, the 'Mustio` centre becomes 'Mustio-kirkniemi`.
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 17 September 1998.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ L 181, 1. 7. 1992, p. 21.
(2) OJ L 126, 24. 5. 1996, p. 37.
(3) OJ L 207, 18. 8. 1993, p. 1.
(4) OJ L 265, 27. 9. 1997, p. 33.
