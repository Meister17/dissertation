Council Regulation (EC) No 1801/2006
of 30 November 2006
on the conclusion of the Fisheries Partnership Agreement between the European Community and the Islamic Republic of Mauritania
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 37 in conjunction with Article 300(2) and the first subparagraph of Article 300(3), thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament [1],
Whereas:
(1) The European Community and the Islamic Republic of Mauritania have negotiated and initialled a Fisheries Partnership Agreement providing Community fishermen with fishing opportunities in the waters falling within the sovereignty of the Islamic Republic of Mauritania, (hereinafter referred to as the Partnership Agreement).
(2) It is in the Community's interest to approve the Partnership Agreement.
(3) The method for allocating the fishing opportunities among the Member States should be defined,
HAS ADOPTED THIS REGULATION:
Article 1
The Fisheries Partnership Agreement between the European Community and the Islamic Republic of Mauritania is hereby approved on behalf of the Community.
The text of the Partnership Agreement is attached to this Regulation.
Article 2
1. The fishing opportunities set out in the Protocol setting out the fishing opportunities and financial contribution provided for in the Fisheries Partnership Agreement (hereafter referred to as the Protocol), shall be allocated among the Member States as follows:
Fishing category | GT or maximum number of licences per licence period | Member State | GT, licences or annual catch ceiling by Member State |
Category 1: Fishing vessels specialising in crustaceans other than crawfish and crab | 9440 GT | Spain | 7183 GT |
Italy | 1371 GT |
Portugal | 886 GT |
Category 2: Black hake trawlers and bottom longliners | 3600 GT | Spain | 3600 GT |
Category 3: Vessels fishing for demersal species other than black hake with gear other than trawls | 2324 GT | Spain | 1500 GT |
United Kingdom | 800 GT |
Malta | 24 GT |
Category 4: Pelagic freezer trawlers fishing for demersal species | 750 GT | Greece | 750 GT |
Category 5: Cephalopods | 18600 GT 43 licences | Spain | 39 licences |
Italy | 4 licences |
Category 6: Crawfish | 300 GT | Portugal | 300 GT |
Category 7: Freezer tuna seiners | 36 licences | Spain | 15 licences |
France | 20 licences |
Malta | 1 licence |
Category 8: Pole-and-line tuna vessels and surface longliners | 31 licences | Spain | 23 licences |
France | 5 licences |
Portugal | 3 licences |
Category 9: Pelagic freezer trawlers | 22 licences for a maximum ceiling of 440000 tonnes | Netherlands | 190000 tonnes |
Lithuania | 120500 tonnes |
Latvia | 73500 tonnes |
Germany | 20000 tonnes |
United Kingdom | 10000 tonnes |
Portugal | 6000 tonnes |
France | 10000 tonnes |
Poland | 10000 tonnes |
Category 10: Crab fishing | 300 GT | Spain | 300 GT |
Category 11: Non-freezer pelagic vessels | 15000 GT per month, averaged over the year | | |
2. Under the Protocol, unused category 11 fishing opportunities (non-freezer pelagic vessels) may be used by category 9 (pelagic freezer trawlers) at a rate of a maximum 25 licences per month.
3. If licence applications for category 9 (pelagic freezer trawlers) exceed the maximum permitted number per reference period, the Commission shall give priority to forwarding applications from vessels which made most use of licences in the ten months preceding that licence application.
4. For category 11 (non-freezer pelagic vessels), the Commission shall forward licence applications once it has received an annual fishing plan detailing applications by vessel (specifying the number of GT planned for each month of activity, for every month of the year) and sent to the Commission not later than 1 March of the year during which the fishing plan applies.
In the event of applications for more than 15000 GT per month averaged over the year, the award shall be carried out on the basis of the table of applications and of the fishing plans referred to in the first subparagraph.
5. The management of fishing opportunities shall be conducted in full accordance with Article 20 of Council Regulation (EC) No 2371/2002 of 20 December 2002 on the conservation and sustainable exploitation of fisheries resources under the Common Fisheries Policy [2].
If licence applications from these Member States do not cover all the fishing opportunities laid down by the Protocol, the Commission may take into consideration licence applications from any other Member State.
Article 3
The Member States whose vessels fish under the Partnership Agreement shall notify the Commission of the quantities of each stock caught within the Mauritanian fishing zone in accordance with Commission Regulation (EC) No 500/2001 of 14 March 2001 laying down detailed rules for the application of Council Regulation (EEC) No 2847/93 on the monitoring of catches taken by Community fishing vessels in third country waters and on the high seas [3].
Article 4
The President of the Council is hereby authorised to designate the person(s) empowered to sign the Partnership Agreement in order to bind the Community [4].
Article 5
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 30 November 2006.
For the Council
The President
L. Hyssälä
[1] Opinion delivered on 16 November 2006 (not yet published in the Official Journal).
[2] OJ L 358, 31.12.2002, p. 59.
[3] OJ L 73, 15.3.2001, p. 8.
[4] Plus the sum of the contributions to be paid by shipowners as provided for in Chapter III of Annex I, paid directly to Mauritania into the account provided for in Chapter IV of Annex I, which is estimated at EUR 22 million per year.
--------------------------------------------------
