DECISION OF THE EXECUTIVE COMMITTEE
of 22 December 1994
on bringing into force the Convention implementing the Schengen Agreement of 19 June 1990
(SCH/Com-ex (94)29 rev. 2)
THE EXECUTIVE COMMITTEE,
Having regard to Article 2 of the Convention implementing the Schengen Agreement,
Having regard to Article 131 of the abovementioned Convention,
Having regard to Article 132 of the abovementioned Convention,
Having regard to Article 139(2) in conjunction with paragraphs 1 and 2 of Joint Declaration I on Article 139 contained in the Final Act of the abovementioned Convention,
HAS DECIDED AS FOLLOWS:
The Convention implementing the Schengen Agreement (hereinafter the "Convention") shall be applied irreversibly:
1. Bringing into force the rules and regulations
The Convention shall be brought into force in its entirety on 26 March 1995 for the initial Signatory States namely Belgium, Germany, France, Luxembourg and the Netherlands and the acceding States, Spain and Portugal.
From that date onwards all provisions of the Convention shall apply to relations between the Schengen Contracting Parties in accordance with the decisions of the Executive Committee, in particular those relating to:
- the abolition of checks on persons at internal borders, in particular the removal of obstacles to and restrictions on traffic at road crossings on the internal borders
(SCH/Com-ex (94)1 rev. 2),
- the introduction and application of Schengen arrangements at airports and aerodromes
(SCH/Com-ex (94)17 rev. 4),
- the performance of checks at the external borders and measures to further enhance security at the external borders
(SCH/Com-ex (93)4 rev. 2 corr(1); SCH/Com-ex (94)decl. 8 corr(2); SCH/Com-ex (94)12; SCH/Com-ex (94)16 rev.; SCH/Com-ex (94)23 rev.),
- the common policy on visas
(SCH/Com-ex (93)6; SCH/Com-ex (93)7; SCH/Com-ex (93)19; SCH/Com-ex (93)24; SCH/Com-ex (93)21; SCH/Com-ex (94)15 rev.; SCH/Com-ex (94)2; SCH/Com-ex (94)5;SCH/Com-ex (94)6; SCH/Com-ex (94)7; SCH/Com-ex (94)20 rev.; SCH/Com-ex (94)24)),
- the fight against illegal trafficking in narcotic drugs and psychotropic substances
(SCH/Com-ex (93)9; SCH/Com-ex (94)28 rev.),
- responsibility for examining asylum applications
(SCH/Com-ex (93)15 corr.(3); SCH/Com-ex (94)3; SCH/Com-ex (94)11,
- international mutual judicial assistance
(SCH/Com-ex (93)14).
A decision shall be adopted at a later date for the other States which have acceded to the Convention, namely Italy and Greece, as soon as they have satisfied the preconditions for the Convention to be brought into force.
2. Declaring the Schengen Information System (SIS) operational
The SIS shall be declared operational and accessible to the authorities entitled to consult directly the data contained therein on 26 March 1995.
The Sirene Manual (SCH/Com-ex (93)8(4)) supplementing the SIS shall apply in its entirety with effect as of that date.
By reason of the conclusions of the report by the SIS Steering Committee, the Executive Committee considers that the SIS will be operational on that date and that the existing national data deemed essential within the meaning of its declaration of 18 October 1993 (SCH/Com-ex (93)decl. 1(5)) shall then have been loaded in accordance with its declaration of 27 June 1994 (SCH/Com-ex (94)decl. 4 rev. 2).
As of that date the Joint Supervisory Authority provided for under Article 115 of the Convention shall assume its functions.
The provisions of the Convention on data protection shall apply in their entirety. The Executive Committee refers to the notification by the Provisional Joint Supervisory Authority for data protection that the Contracting Parties which have successfully completed the tests satisfy the data protection conditions required for the putting into operation of the SIS.
3. Arrangements for the preparatory period (22 December 1994 to 26 March 1995)
The Executive Committee requests the Contracting Parties which have successfully completed the tests to adopt the following measures before 26 March 1995:
- to consolidate in terms of organisational and human resources the measures necessary for the full application of the Schengen rules and regulations, particularly in the fields of consular, judicial and police cooperation as well as in the fight against narcotic drugs; to continue to ensure that the competent staff are trained in the application of the Schengen rules and regulations and
- to complete the preparations in terms of technical, organisational and human resources with a view to bringing the N.SIS into operation in relation to the C.SIS and to complete preparations for end-user access to this system.
The Executive Committee instructs the SIS Steering Committee to confirm in good time before the abovementioned date that the SIS is ready for operation in terms of technical, organisational and human resources.
The Executive Committee requests the Contracting States to confirm that the system is accessible to the authorities entitled to direct consultation already notified to the Executive Committee (SCH/OR.SIS (94)18 rev. 3).
The Executive Committee requests the Contracting Parties to load retroactively during this period other data on persons or objects which go beyond the data deemed essential (SCH/Com-ex (94)decl. 4 rev. 2(6)). The SIS databases must be constantly updated.
The Executive Committee calls on the Contracting Parties to ensure that airlines make the changes necessary for the free movement of persons at the latest by the change in flight schedules on 26 March 1995 and that airport operators complete the measures provided for to this end in the decision on the introduction and application of the Schengen arrangements at airports and aerodromes (SCH/Com-ex (94)17 rev. 4) and create the necessary organisational and technical preconditions for the free movement of persons by that date.
The Contracting Parties are requested to inform the airlines and airport operators accordingly as soon as possible.
4. Organisation of the application of the Convention after its entry into force, particularly in the initial phase of application
The purpose of implementing the Convention is to enhance the security of citizens in Europe whilst creating the preconditions necessary to achieve the free movement of persons within the meaning of Article 7a of the Treaty establishing the European Community.
The Executive Committee therefore attaches particular importance to the initial phase of the application of the Convention in its entirety during the first three months after 26 March 1995.
Each Contracting Party shall be responsible for the application of the Convention, particularly the abolition of checks at the internal borders during the initial phase of application. The Contracting Parties shall keep each other mutually informed, shall consult whenever necessary and shall work in close cooperation.
With a view to creating the instrument necessary for the administration of the Convention, the Executive Committee hereby decides to set up a permanent follow-up structure composed of the existing Central Group and its working groups and sub-groups.
The Executive Committee instructs the permanent follow-up structure during the initial phase of application to monitor particularly closely the application of the Schengen rules and regulations, to identify, analyse and resolve rapidly any technical problems and, if necessary, to take measures with a view to the more effective application of the Convention.
The Executive Committee instructs the Presidency as of 1 January 1995 to prepare the work of this follow-up structure and to ensure particularly that the working groups identify any difficulties and rapidly devise solutions.
During the initial three-month application period, the working groups of the follow-up structure shall convene regularly and as often as necessary.
Should urgent decisions be required in particular cases, the Central Group may convene a select meeting at short notice as the follow-up committee. The follow-up committee shall be composed of either the heads of delegation of each Contracting Party or a high-ranking official designated by each of the Contracting Parties assisted by the representatives of the working groups which must be consulted to resolve any problems arising.
At the request of one Contracting Party, the Central Group shall also undertake a general analysis of the difficulties arising and shall propose solutions devised in collaboration with the working groups and sub-groups.
In the absence of agreement within the Central Group, the matter shall be referred to the Executive Committee. In this respect, the Contracting Parties concerned must be granted the opportunity to give an opinion on its conclusions.
Each Contracting Party may also request the Central Group to assess situations which have only arisen on its own national territory.
Three months after the Convention has been brought into force, the Central Group shall submit a preliminary report to the Executive Committee dealing with the functioning of the SIS, the efficacy of controls at the external borders, the efficiency of the fight against narcotic drugs and the results of police and judicial cooperation.
The Central Group shall submit a general report to the Executive Committee by 31 March 1996.
Bonn, 22 December 1994.
The Chairman
Bernd Schmidbauer
(1) See SCH/Com-ex (99)13.
(2) This document is not taken over in the acquis.
(3) "Asylum" provisions taken over in the Bonn Protocol (SCH/Com-ex (94)3).
(4) Replaced by SCH/Com-ex (99)5.
(5) This document is not taken over in the acquis.
(6) This document is not taken over in the acquis.
