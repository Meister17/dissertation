Prior notification of a concentration
(Case COMP/M.4422 — Bertelsmann/Vodafone/Moconta)
Candidate case for simplified procedure
(2006/C 267/05)
(Text with EEA relevance)
1. On 25 October 2006, the Commission received a notification of a proposed concentration pursuant to Article 4 of Council Regulation (EC) No 139/2004 [1] by which the undertaking Bertelsmann Aktiengesellschaft ("Bertelsmann", Germany), by means of its 100 % subsidiary Arvato Mobile GmbH ("Arvato", Germany), and Vodafone D2 GmbH ("Vodafone D2", Germany) acquire within the meaning of Article 3(1)(b) of the Council Regulation joint control of Moconta GmbH Co. KG ("Moconta", Germany) by way of purchase of shares constituting a joint venture.
2. The business activities of the undertakings concerned are:
- Bertelsmann: Media company in the areas of television/radio, print media publishing, music as well as printing and other services;
- Arvato: Conceptual design and realisation of technical services in the area of mobile telecommunication marketing and customer loyalty;
- Vodafone D2: Mobile telecommunications network operator in Germany as well as service provider for direct internet access, mobile fax- and data communication and picture messages (MMS);
- Moconta: Conceptual design and marketing of services in the area of customer loyalty on the basis of mobile telecommunication services
3. On preliminary examination, the Commission finds that the notified transaction could fall within the scope of Regulation (EC) No 139/2004. However, the final decision on this point is reserved. Pursuant to the Commission Notice on a simplified procedure for treatment of certain concentrations under Council Regulation (EC) No 139/2004 [2] it should be noted that this case is a candidate for treatment under the procedure set out in the Notice.
4. The Commission invites interested third parties to submit their possible observations on the proposed operation to the Commission.
Observations must reach the Commission not later than 10 days following the date of this publication. Observations can be sent to the Commission by fax (fax No (32-2) 296 43 01 or 296 72 44) or by post, under reference number COMP/M.4422 — Bertelsmann/Vodafone/Moconta, to the following address:
European Commission
Directorate-General for Competition
Merger Registry
J-70
B-1049 Bruxelles/Brussel
[1] OJ L 24, 29.1.2004, p. 1.
[2] OJ C 56, 05.3.2005, p. 32.
--------------------------------------------------
