COUNCIL DIRECTIVE of 24 June 1988 for the implementation of Article 67 of the Treaty (88/361/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Articles 69 and 70 (1) thereof,
Having regard to the proposal from the Commission, submitted following consultation with the Monetary Committee (1),
Having regard to the opinion of the European Parliament (2),
Whereas Article 8a of the Treaty stipulates that the internal market shall comprise an area without internal frontiers in which the free movement of capital is ensured, without prejudice to the other provisions of the Treaty;
Whereas Member States should be able to take the requisite measures to regulate bank liquidity; whereas these measures should be restricted to this purpose;
Whereas Member States should, if necessary, be able to take measures to restrict, temporarily and within the framework of appropriate Community procedures, short-term capital movements which, even where there is no appreciable divergence in economic fundamentals, might seriously disrupt the conduct of their monetary and exchange-rate policies;
Whereas, in the interests of transparency, it is advisable to indicate the scope, in accordance with the arrangements laid down in this Directive, of the transitional measures adopted for the benefit of the Kingdom of Spain and the Portuguese Republic by the 1985 Act of Accession in the field of capital movements;
Whereas the Kingdom of Spain and the Portuguese Republic may, under the terms of Articles 61 to 66 and 222 to 232 respectively of the 1985 Act of Accession, postpone the liberalization of certain capital movements in derogation from the obligations set out in the First Council Directive of 11 May 1960 for the implementation of Article 67 of the Treaty (3), as last amended by Directive 86/566/EEC (4); whereas Directive 86/566/EEC also provides for transitional arrangements to be applied for the benefit of those two Member States in respect of their obligations to liberalize capital movements; whereas it is appropriate for those two Member States to be able to postpone the application of the new liberalization obligations resulting from this Directive;
Whereas the Hellenic Republic and Ireland are faced, albeit to differing degrees, with difficult balance-of-payments situations and high levels of external indebtedness; whereas the immediate and complete liberalization of capital movements by those two Member States would make it more difficult for them to continue to apply the measures they have taken to improve their external positions and to reinforce the capacity of their financial systems to adapt to the requirements of an integrated financial market in the Community; whereas it is appropriate, in accordance with Article 8c of the Treaty, to grant to those two Member States, in the light of their specific circumstances, further time in which to comply with the obligations arising from this Directive;
Whereas, since the full liberalization of capital movements could in some Member States, and especially in border areas, contribute to difficulties in the market for secondary residences; whereas existing national legislation regulating these purchases should not be affected by the entry into effect of this Directive;
Whereas advantage should be taken of the period adopted for bringing this Directive into effect in order to enable the Commission to submit proposals designed to eliminate or reduce risks of distortion, tax evasion and tax avoidance resulting from the diversity of national systems for taxation and to permit the Council to take a position on such proposals;
Whereas, in accordance with Article 70 (1) of the Treaty, the Community shall endeavour to attain the highest possible degree of liberalization in respect of the movement of capital between its residents and those of third countries;
Whereas large-scale short-term capital movements to or from third countries may seriously disturb the monetary or financial situation of Member States or cause serious stresses on the exchange markets; whereas such developments may prove harmful for the cohesion of the European Monetary System, for the smooth operation of the internal market and for the progressive achievement of economic and monetary union; whereas it is therefore appropriate to create the requisite conditions for concerted action by Member States should this prove necessary;
Whereas this Directive replaces Council Directive 72/156/EEC of 21 March 1972 on regulating international capital flows and neutralizing their undesirable effects on domestic liquidity (5); whereas Directive 72/156/EEC should accordingly be repealed,
HAS ADOPTED THIS DIRECTIVE:
Article 1
1. Without prejudice to the following provisions, Member States shall abolish restrictions on movements of capital taking place between persons resident in Member States. To facilitate application of this Directive, capital movements shall be classified in accordance with the Nomenclature in Annex I.
2. Transfers in respect of capital movements shall be made on the same exchange rate conditions as those governing payments relating to current transactions.
Article 2
Member States shall notify the Committee of Governors of the Central Banks, the Monetary Committee and the Commission, by the date of their entry into force at the latest, of measures to regulate bank liquidity which have a specific impact on capital transactions carried out by credit institutions with non-residents.
Such measures shall be confined to what is necessary for the purposes of domestic monetary regulation. The Monetary Committee and the Committee of Governors of the Central Banks shall provide the Commission with opinions on this subject.
Article 3
1. Where short-term capital movements of exceptional magnitude impose severe strains on foreign-exchange markets and lead to serious disturbances in the conduct of a Member State's monetary and exchange rate policies, being reflected in particular in substantial variations in domestic liquidity, the Commission may, after consulting the Monetary Committee and the Committee of Governors of the Central Banks, authorize that Member State to take, in respect of the capital movements listed in Annex II, protective measures the conditions and details of which the Commission shall determine.
2. The Member State concerned may itself take the protective measures referred to above, on grounds of urgency, should these measures be necessary. The Commission and the other Member States shall be informed of such measures by the date of their entry into force at the latest. The Commission, after consulting the Monetary Committee and the Committee of Governors of the Central Banks, shall decide whether the Member State concerned may continue to apply these measures or whether it should amend or abolish them.
3. The decisions taken by the Commission under paragraphs 1 and 2 may be revoked or amended by the Council acting by a qualified majority.
4. The period of application of protective measures taken pursuant to this Article shall not exceed six months.
5. Before 31 December 1992, the Council shall examine, on the basis of a report from the Commission, after delivery of an opinion by the Monetary Committee and the Committee of Governors of the Central Banks, whether the provisions of this Article remain appropriate, as regards their principle and details, to the requirements which they were intended to satisfy.
Article 4
This Directive shall be without prejudice to the right of Member States to take all requisite measures to prevent infringements of their laws and regulations, inter alia in the field of taxation and prudential supervision of financial institutions, or to lay down procedures for the declaration of capital movements for purposes of administrative or statistical information.
Application of those measures and procedures may not have the effect of impeding capital movements carried out in accordance with Community law.
Article 5
For the Kingdom of Spain and the Portuguese Republic, the scope, in accordance with the Nomenclature of capital movements contained in Annex I, of the provisions of the 1985 Act of Accession in the field of capital movements shall be as indicated in Annex III.
Article 6
1. Member States shall take the measures necessary to comply with this Directive no later than 1 July 1990. They shall forthwith inform the Commission thereof. They shall also make known, by the date of their entry into force at the latest, any new measure or any amendment made to the provisions governing the capital movements listed in
Annex I.
2. The Kingdom of Spain and the Portuguese Republic, without prejudice for these two Member States to Articles 61 to 66 and 222 to 232 of the 1985 Act of Accession, and the Hellenic Republic and Ireland may temporarily continue to apply restrictions to the capital movements listed in Annex IV, subject to the conditions and time limits laid down in that Annex.
If, before expiry of the time limit set for the liberalization of the capital movements referred to in Lists III and IV of Annex IV, the Portuguese Republic or the Hellenic Republic considers that it is unable to proceed with liberalization, in particular because of difficulties as regards its balance of payments or because the national financial system is insufficiently adapted, the Commission, at the request of one or other of these Member States, shall in collaboration with the Monetary Committee, review the economic and financial situation of the Member State concerned. On the basis of the outcome of this review, the Commission shall propose to the Council an extension of the time limit set for liberalization of all or part of the capital movements referred to. This extension may not exceed three years. The Council shall act in accordance with the procedure laid down in Article 69 of the Treaty.
3. The Kingdom of Belgium and the Grand Duchy of Luxembourg may temporarily continue to operate the dual exchange market under the conditions and for the periods laid down in Annex V.
4. Existing national legislation regulating purchases of secondary residences may be upheld until the Council adopts further provisions in this area in accordance with Article 69 of the Treaty. This provision does not affect the applicability of other provisions of Community law.
5. The Commission shall submit to the Council, by 31 December 1988, proposals aimed at eliminating or reducing risks of distortion, tax evasion and tax avoidance linked to the diversity of national systems for the taxation of savings and for controlling the application of these systems.
The Council shall take a position on these Commission proposals by 30 June 1989. Any tax provisions of a Community nature shall, in accordance with the Treaty, be adopted unanimously.
Article 7
1. In their treatment of transfers in respect of movements of capital to or from third countries, the Member States shall endeavour to attain the same degree of liberalization as that which applies to operations with residents of other Member States, subject to the other provisions of this Directive.
The provisions of the preceding subparagraph shall not prejudice the application to third countries of domestic rules or Community law, particularly any reciprocal conditions, concerning operations involving establishment, the provisions of financial services and the admission of securities to capital markets.
2. Where large-scale short-term capital movements to or from third countries seriously disturb the domestic or external monetary or financial situation of the Member States, or of a number of them, or cause serious strains in exchange relations within the Community or between the Community and third countries, Member States shall consult with one another on any measure to be taken to counteract such difficulties. This consultation shall take place within the Committee of Governors of the Central Banks and the Monetary Committee on the initiative of the Commission or of any Member State.
Article 8
At least once a year the Monetary Committee shall examine the situation regarding free movement of capital as it results from the application of this Directive. The examination shall cover measures concerning the domestic regulation of credit and financial and monetary markets which could have a specific impact on international capital movements and on all other aspects of this Directive. The Committee shall report to the Commission on the outcome of this examination.
Article 9
The First Directive of 11 May 1960 and Directive 72/156/EEC shall be repealed with effect from 1 July 1990.
Article 10
This Directive is addressed to the Member States.
Done at Luxembourg, 24 June 1988.
For the Council
The President
M. BANGEMANN
(1) OJ N° C 26, 1. 2. 1988, p. 1.
(2) Opinion delivered on 17 June 1988 (not yet published in the Official Journal).
(3) OJ N° 43, 12. 7. 1960, p. 921/60.
(4) OJ N° L 332, 26. 11. 1986, p. 22.
(5) OJ N° L 91, 18. 4. 1972, p. 13.
ANNEX I
NOMENCLATURE OF THE CAPITAL MOVEMENTS REFERRED TO IN ARTICLE 1 OF THE DIRECTIVE
In this Nomenclature, capital movements are classified according to the economic nature of the assets and liabilities they concern, denominated either in national currency or in foreign exchange.
The capital movements listed in this Nomenclature are taken to cover:
- all the operations necessary for the purposes of capital movements: conclusion and performance of the transaction and related transfers. The transaction is generally between residents of different Member States although some capital movements are carried out by a single person for his own account (e.g. transfers of assets belonging to emigrants),
- operations carried out by any natural or legal person (¹), including operations in respect of the assets
or liabilities of Member States or of other public administrations and agencies, subject to the provisions of
Article 68 (3) of the Treaty,
- access for the economic operator to all the financial techniques available on the market approached for the purpose of carrying out the operation in question. For example, the concept of acquisition of securities and other financial instruments covers not only spot transactions but also all the dealing techniques available: forward transactions, transactions carrying an option or warrant, swaps against other assets, etc. Similarly, the concept of operations in current and deposit accounts with financial institutions, includes not only the opening and placing of funds on accounts but also forward foreign exchange transactions, irrespective of whether these are intended to cover an exchange risk or to take an open foreign exchange position,
- operations to liquidate or assign assets built up, repatriation of the proceeds of liquidation thereof (¹) or immediate use of such proceeds within the limits of Community obligations,
- operations to repay credits or loans.
This Nomenclature is not an exhaustive list for the notion of capital movements - whence a heading XIII - F. 'Other capital movements - Miscellaneous'. It should not therefore be interpreted as restricting the scope of the principle of full liberalization of capital movements as referred to in Article 1 of the Directive.
I - DIRECT INVESTMENTS (¹)
1. Establishment and extension of branches or new undertakings belonging solely to the person providing the capital, and the acquisition in full of existing undertakings.
2. Participation in new or existing undertaking with a view to establishing or maintaining lasting economic links.
3. Long-term loans with a view to establishing or maintaining lasting economic links.
4. Reinvestment of profits with a view to maintaining lasting economic links.
A - Direct investments on national territory by non-residents (¹)
B - Direct investments abroad by residents (¹)
II - INVESTMENTS IN REAL ESTATE (not included under I) (¹)
A - Investments in real estate on national territory by non-residents
B - Investments in real estate abroad by residents
III - OPERATIONS IN SECURITIES NORMALLY DEALT IN ON THE CAPITAL MARKET (not included under I, IV and V)
(a) Shares and other securities of a participating nature (¹).
(b) Bonds (¹).
(¹) See Explanatory Notes below.
A - Transactions in securities on the capital market 1. Acquisition by non-residents of domestic securities dealt in on a stock exchange (¹).
2. Acquisition by residents of foreign securities dealt in on a stock exchange.
3. Acquisition by non-residents of domestic securities not dealt in on a stock exchange (¹).
4. Aquisition by residents of foreign securities not dealt in on a stock exchange.
B - Admission of securities to the capital market (¹)
(i) Introduction on a stock exchange (¹).
(ii) Issue and placing on a capital market (*).
1. Admission of domestic securities to a foreign capital market.
2. Administration of foreign securities to the domestic capital market.
IV - OPERATIONS IN UNITS OF COLLECTIVE INVESTMENT UNDERTAKINGS (¹)
(a) Units of undertakings for collective investment in securities normally dealt in on the capital market (shares, other equities and bonds).
(b) Units of undertakings for collective investment in securities or instruments normally dealt in on the money market.
(c) Units of undertakings for collective investment in other assets.
A - Transactions in units of collective investment undertakings
1. Acquisition by non-residents of units of national undertakings dealt in on a stock exchange.
2. Acquisition by residents of units of foreign undertakings dealt in on a stock exchange.
3. Acquisition by non-residents of units of national undertakings not dealt in on a stock exchange.
4. Acquisition by residents of units of foreign undertakings not dealt in on a stock exchange.
B - Administration of units of collective investment undertakings to the capital market
(i) Introduction on a stock exchange.
(ii) Issue and placing on a capital market.
1. Admission of units of national collective investment undertakings to a foreign capital market.
2. Admission of units of foreign collective investment undertakings to the domestic capital market.
V OPERATIONS IN SECURITIES AND OTHER INSTRUMENTS NORMALLY DEALT IN ON THE MONEY MARKET (¹)
A - Transactions in securities and other instruments on the money market
1. Acquisition by non-residents of domestic money market securities and instruments.
2. Acquisition by residents of foreign money market securities and instruments.
B - Admission of securities and other instruments to the money market
(i) Introduction on a recognized money market (*).
(ii) Issue and placing on a recognized money market.
1. Admission of domestic securities and instruments to a foreign money market.
2. Admission of foreign securities and instruments to the domestic money market.
(¹) See Explanatory Notes below.
VI - OPERATIONS IN CURRENT AND DEPOSIT ACCOUNTS WITH FINANCIAL INSTITUTIONS (¹)
A - Operations carried out by non-residents with domestic financial institutions
B - Operations carried out by residents with foreign financial institutions
VII - CREDITS RELATED TO COMMERCIAL TRANSACTIONS OR TO THE PROVISION OF SERVICES IN WHICH A RESIDENT IS PARTICIPATING (¹)
1. Short-term (less than one year).
2. Medium-term (from one to five years).
3. Long-term (five years or more).
A - Credits granted by non-residents to residents
B - Credits granted by residents to non-residents
VIII - FINANCIAL LOANS AND CREDITS (not included under I, VII and XI) (¹)
1. Short-term (less than one year).
2. Medium-term (from one to five years).
3. Long-term (five years or more).
A - Loans and credits granted by non-residents to residents
B - Loans and credits granted by residents to non-residents
IX - SURETIES, OTHER GUARANTEES AND RIGHTS OF PLEDGE
A - Granted by non-residents to residents
B - Granted by residents to non-residents
X - TRANSFERS IN PERFORMANCE OF INSURANCE CONTRACTS
A - Premiums and payments in respect of life assurance
1. Contracts concluded between domestic life assurance companies and non-residents.
2. Contracts concluded between foreign life assurance companies and residents.
B - Premiums and payments in respect of credit insurance
1. Contracts concluded between domestic credit insurance companies and non-residents.
2. Contracts concluded between foreign credit insurance companies and residents.
C - Other transfers of capital in respect of insurance contracts
XI - PERSONAL CAPITAL MOVEMENTS
A - Loans
B - Gifts and endowments
C - Dowries
D - Inheritances and legacies
E - Settlement of debts by immigrants in their previous country of residence
F - Transfers of assets constituted by residents, in the event of emigration, at the time of their installation or during their period of stay abroad
G - Transfers, during their period of stay, of immigrants' savings to their previous country of residence
(¹) See Explanatory Notes below.
XII - PHYSICAL IMPORT AND EXPORT OF FINANCIAL ASSETS
A - Securities
B - Means of payment of every kind
XIII - OTHER CAPITAL MOVEMENTS
A - Death duties
B - Damages (where these can be considered as capital)
C - Refunds in the case of cancellation of contracts and refunds of uncalled-for payments (where these can be considered as capital)
D - Authors' royalties: patents, designs, trade marks and inventions (assignments and transfers arising out of such assignments)
E - Transfers of the monies required for the provision of services (not included under VI)
F - Miscellaneous
EXPLANATORY NOTES
For the purposes of this Nomenclature and the Directive only, the following expressions have the meanings assigned to them respectively:
Direct investments
Investments of all kinds by natural persons or commercial, industrial or financial undertakings, and which serve to establish or to maintain lasting and direct links between the person providing the capital and the entrepreneur to whom or the undertaking to which the capital is made available in order to carry on an economic activity. This concept must therefore be understood in its widest sense.
The undertakings mentioned under I-1 of the Nomenclature include legally independent undertakings (wholly-owned subsidiaries) and branches.
As regards those undertakings mentioned under I-2 of the Nomenclature which have the status of companies limited by shares, there is participation in the nature of direct investment where the block of shares held by a natural person of another undertaking or any other holder enables the shareholder, either pursuant to the provisions of national laws relating to companies limited by shares or otherwise, to participate effectively in the management of the company or in its control.
Long-term loans of a participating nature, mentioned under I-3 of the Nomenclature, means loans for a period of more than five years which are made for the purpose of establishing or maintaining lasting economic links. The main examples which may be cited are loans granted by a company to its subsidiaries or to companies in which it has a share and loans linked with a profit-sharing arrangement. Loans granted by financial institutions with a view to establishing or maintaining lasting economic links are also included under this heading.
Investments in real estate
Purchases of buildings and land and the construction of buildings by private persons for gain or personal use. This category also includes rights of usufruct, easements and building rights.
Introduction on a stock exchange or on a recognized money market
Access - in accordance with a specified procedure - for securities and other negotiable instruments to dealings, whether controlled officially or unofficially, on an officially recognized stock exchange or in an officially recognized segment of the money market.
Securities dealt in on a stock exchange (quoted or unquoted)
Securities the dealings in which are controlled by regulations, the prices for which are regularly published, either by official stock exchanges (quoted securities) or by other bodies attached to a stock exchange - e.g. committees of banks (unquoted securities).
Issue of securities and other negotiable instruments
Sale by way of an offer to the public.
Placing of securities and other negotiable instruments
The direct sale of securities by the issuer of by the consortium which the issuer has instructed to sell them, with no offer being made to the public.
Domestic or foreign securities and other instruments
Securities according to the country in which the issuer has his principal place of business. Acquisition by residents of domestic securities and other instruments issued on a foreign market ranks as the acquisition of foreign securities.
Shares and other securities of a participating nature
Including rights to subscribe to new issues of shares.
Bonds
Negotiable securities with a maturity of two years or more from issue for which the interest rate and the terms for the repayment of the principal and the payment of interest are determined at the time of issue.
Collective investment undertakings Untertakings:
- the object of which is the collective investment in transferable securities or other assets of the capital they raise and which operate on the principle of risk-spreading, and
- the units of which are, at the request of holders, under the legal, contractual or statutory conditions governing them, repurchased or redeemed, directly or indirectly, out of those undertakings' assets. Action taken by a collective investment undertaking to ensure that the stock exchange value of its units does not significantly vary from their net asset value shall be regarded as equivalent to such repurchase or redemption.
Such undertakings may be constituted according to law either under the law of contract (as common funds managed by management companies) or trust law (as unit trusts) or under statute (as investment companies).
For the purposes of the Directive, 'common funds' shall also include unit trusts.
Securities and other instruments normally dealt in on the money market
Treasury bills and other negotiable bills, certificates of deposit, bankers' acceptances, commercial paper and other like instruments.
Credits related to commercial transactions or to the provision of services
Contractual trade credits (advances or payments by instalment in respect of work in progress or on order and extended payment terms, whether or not involving subscription to a commercial bill) and their financing by credits provided by credit institutions. This category also includes factoring operations.
Financial loans and credits
Financing of every kind granted by financial institutions, including financing related to commercial transactions or to the provision of services in which no resident is participating.
This category also includes mortgage loans, consumer credit and financial leasing, as well as back-up facilities and other note-issuance facilities.
Residents or non-residents
Natural and legal persons according to the definitions laid down in the exchange control regulations in force in each Member State.
Proceeds of liquidation (of investments, securities, etc.)
Proceeds of sale including any capital appreciation, amount of repayments, proceeds of execution of judgements, etc.
Natural or legal persons
As defined by the national rules.
Financial institutions
Banks, savings banks and institutions specializing in the provision of short-term, medium-term and long-term credit, and insurance companies, building societies, investment companies and other institutions of like character.
Credit institutions
Banks, savings banks and institutions specializing in the provision of short-term, medium-term and long-term credit.
ANNEX II
>TABLE>
ANNEX III
REFERRED TO IN ARTICLE 5 OF THE DIRECTIVE
>TABLE>
>TABLE>
>TABLE>
ANNEX IV
REFERRED TO IN ARTICLE 6 (2) OF THE DIRECTIVE
I. The Portuguese Republic may continue to apply or reintroduce, until 31 December 1990 restrictions existing on the date of notification of the Directive on capital movements given in List I below:
>TABLE>
II. The Kingdom of Spain and the Portuguese Republic may continue to apply or reintroduce, until 31 December 1990 and 31 December 1992 respectively, restrictions existing on the date of notification of the Directive on capital movements given in List II below:
>TABLE>
Nature of operation
Heading
III. The Hellenic Republic, the Kingdom of Spain, Ireland and the Portuguese Republic may, until 31 December 1992, continue to apply or reintroduce restrictions existing at the date of notification of the Directive on capital movements given in List III below:
>TABLE>
IV. The Hellenic Republic, the Kingdom of Spain, Ireland and the Portuguese Republic may, until 31 December 1992, defer liberalization of the capital movements given in List IV below:
>TABLE>
ANNEX V
Since the dual exchange market system, as operated by the Kingdom of Belgium and the Grand Duchy of Luxembourg, has not had the effect of restricting capital movements but nevertheless constitutes an anomaly in the EMS and should therefore be brought to an end in the interests of effective implementation of the Directive and with a view to strengthening the European Monetary System, these two Member States undertake to abolish it by 31 December 1992. They also undertake to administer the system, until such time as it is abolished, on the basis of procedures which will still ensure the de facto free movement of capital on such conditions that the exchange rates ruling on the two markets show no appreciable and lasting differences.
