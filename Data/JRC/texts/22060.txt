COMMISSION REGULATION (EC) No 1826/97 of 22 September 1997 adapting the combined nomenclature codes of certain products mentioned in Article 1 of Council Regulation (EC) No 1577/96 introducing a specific measure in respect of certain grain legumes
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 234/79 of 5 February 1979 on the procedure for adjusting the Common Customs Tariff nomenclature used for agricultural products (1), as last amended by Regulation (EC) No 3290/94 (2), and in particular Article 2 thereof,
Whereas Commission Regulation (EC) No 1734/96 of 9 September 1996 amending Annex I to Council Regulation (EEC) No 2658/87 on the tariff and statistical nomenclature and on the Common Customs Tariff (3) made some amendments to the nomenclature for legumes; whereas Council Regulation (EC) No 1577/96 (4) should be adjusted accordingly;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Joint Management Committee for Cereals, Oils and Fats and Dried Fodder,
HAS ADOPTED THIS REGULATION:
Article 1
Article 1 (a) and (b) of Regulation (EC) No 1577/96 is hereby replaced by the following:
'(a) lentils other than for sowing covered by CN code ex 0713 40 00;
(b) chick peas other than for sowing covered by CN code ex 0713 20 00;`.
Article 2
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 22 September 1997.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ L 34, 9. 2. 1979, p. 2.
(2) OJ L 349, 31. 12. 1994, p. 105.
(3) OJ L 238, 19. 9. 1996, p. 1.
(4) OJ L 206, 30. 7. 1996, p. 4.
