Agreement
between the European Community and the government of Georgia on certain aspects of air services
THE EUROPEAN COMMUNITY,
of the one part, and
THE GOVERNMENT OF GEORGIA
of the other part,
(hereinafter referred to as "the Parties")
NOTING that bilateral air service agreements have been concluded between several Member States of the European Community and Georgia containing provisions contrary to Community law,
NOTING that the European Community has exclusive competence with respect to several aspects that may be included in bilateral air service agreements between Member States of the European Community and third countries,
NOTING that under European Community law Community air carriers established in a Member State have the right to non-discriminatory access to air routes between the Member States of the European Community and third countries,
HAVING REGARD to the agreements between the European Community and certain third countries providing for the possibility for the nationals of such third countries to acquire ownership in air carriers licensed in accordance with European Community law,
RECOGNISING that certain provisions of the bilateral air service agreements between Member States of the European Community and Georgia, which are contrary to European Community law, must be brought into full conformity with it in order to establish a sound legal basis for air services between the European Community and Georgia and to preserve the continuity of such air services,
NOTING that provisions of the bilateral air service agreements between Member States of the European Community and Georgia, which are not contrary to European Community law, do not need to be amended or replaced,
NOTING that it is not a purpose of the European Community, as part of these negotiations, to increase the total volume of air traffic between the European Community and Georgia, to affect the balance between Community air carriers and air carriers of Georgia, or to negotiate amendments to the provisions of existing bilateral air service agreements concerning traffic rights,
HAVE AGREED AS FOLLOWS:
Article 1
General provisions
1. For the purposes of this Agreement, "Member States" shall mean Member States of the European Community.
2. References in each of the Agreements listed in Annex I to nationals of the Member State that is a party to that Agreement shall be understood as referring to nationals of the Member States of the European Community.
3. References in each of the Agreements listed in Annex I to air carriers or airlines of the Member State that is a party to that Agreement shall be understood as referring to air carriers or airlines designated by that Member State.
4. The granting of traffic rights continues to be carried out through bilateral arrangements.
Article 2
Designation by a Member State
1. The provisions in paragraphs 2 and 3 of this Article shall supersede the corresponding provisions in the Articles listed in Annex II(a) and (b) respectively, in relation to the designation of an air carrier by the Member State concerned, its authorisations and permissions granted by Georgia, and the refusal, revocation, suspension or limitation of the authorisations or permissions of the air carrier, respectively.
2. On receipt of a designation by a Member State, Georgia shall grant the appropriate authorisations and permissions with minimum procedural delay, provided that:
(i) the air carrier is established, under the Treaty establishing the European Community, in the territory of the designating Member State and has a valid Operating Licence in accordance with European Community law;
(ii) effective regulatory control of the air carrier is exercised and maintained by the Member State responsible for issuing its Air Operators Certificate and the relevant aeronautical authority is clearly identified in the designation; and
(iii) the air carrier is owned and shall continue to be owned directly or through majority ownership by Member States and/or nationals of Member States, or by other States listed in Annex III and/or nationals of such other States, and shall at all times be effectively controlled by such States and/or such nationals.
3. Georgia may refuse, revoke, suspend or limit the authorisations or permissions of an air carrier designated by a Member State where:
(i) the air carrier is not established, under the Treaty establishing the European Community, in the territory of the designating Member State or does not have a valid Operating Licence in accordance with European Community law;
(ii) effective regulatory control of the air carrier is not exercised or not maintained by the Member State responsible for issuing its Air Operators Certificate, or the relevant aeronautical authority is not clearly identified in the designation; or
(iii) the air carrier is not owned and effectively controlled directly or through majority ownership by Member States and/or nationals of Member States, or by other states listed in Annex III and/or nationals of such other States.
In exercising its right under this paragraph, Georgia shall not discriminate between Community air carriers on the grounds of nationality.
Article 3
Rights with regard to regulatory control
1. The provisions in paragraph 2 of this Article shall complement the Articles listed in Annex II(c).
2. Where a Member State has designated an air carrier whose regulatory control is exercised and maintained by another Member State, the rights of Georgia under the safety provisions of the Agreement between the Member State that has designated the air carrier and Georgia shall apply equally in respect of the adoption, exercise or maintenance of safety standards by that other Member State and in respect of the operating authorisation of that air carrier.
Article 4
Taxation of aviation fuel
1. The provisions in paragraph 2 of this Article shall complement the corresponding provisions in the Articles listed in Annex II(d).
2. Notwithstanding any other provision to the contrary, nothing in each of the Agreements listed in Annex II(d) shall prevent a Member State from imposing taxes, levies, duties, fees or charges on fuel supplied in its territory for use in an aircraft of a designated air carrier of Georgia that operates between a point in the territory of that Member State and another point in the territory of that Member State or in the territory of another Member State.
Article 5
Tariffs for carriage within the European Community
1. The provisions in paragraph 2 of this Article shall complement the articles listed in Annex II(e).
2. The tariffs to be charged by the air carrier(s) designated by Georgia under an agreement listed in Annex I containing a provision listed in Annex II(e) for carriage wholly within the European Community shall be subject to European Community law.
Article 6
Annexes to the Agreement
The Annexes to this Agreement shall form an integral part thereof.
Article 7
Revision or amendment
The Parties may, at any time, revise or amend this Agreement by mutual consent.
Article 8
Entry into force and provisional application
1. This Agreement shall enter in force when the Parties have notified each other in writing that their respective internal procedures necessary for its entry into force have been completed.
2. Notwithstanding paragraph 1, the Parties agree to provisionally apply this Agreement from the first day of the month following the date on which the Parties have notified each other of the completion of the procedures necessary for this purpose.
3. Agreements and other arrangements between Member States and Georgia which, at the date of signature of this Agreement, have not yet entered into force and are not being applied provisionally are listed in Annex I(b). This Agreement shall apply to all such Agreements and arrangements upon their entry into force or provisional application.
Article 9
Termination
1. In the event that an Agreement listed in Annex I is terminated, all provisions of this Agreement that relate to the Agreement listed in Annex I concerned shall terminate at the same time.
2. In the event that all Agreements listed in Annex I are terminated, this Agreement shall terminate at the same time.
IN WITNESS WHEREOF, the undersigned, being duly authorised, have signed this Agreement.
Done at Brussels in duplicate, on this third day of May in the year two thousand and six, in the Czech, Danish, Dutch, English, Estonian, Finnish, French, German, Greek, Hungarian, Italian, Latvian, Lithuanian, Maltese, Polish, Portuguese, Slovak, Slovenian, Spanish, Swedish and Georgian languages.
Por la Comunidad Europea
Za Evropské společenství
For Det Europæiske Fællesskab
Für die Europäische Gemeinschaft
Euroopa Ühenduse nimel
Για την Ευρωπαϊκή Κοινότητα
For the European Community
Pour la Communauté européenne
Per la Comunità europea
Eiropas Kopienas vārdā
Europos bendrijos vardu
Az Európai Közösség részéről
Għall-Komunità Ewropea
Voor de Europese Gemeenschap
W imieniu Wspólnoty Europejskiej
Pela Comunidade Europeia
Za Európske spoločenstvo
Za Evropsko skupnost
Euroopan yhteisön puolesta
För Europeiska gemenskapen
+++++ TIFF +++++
+++++ TIFF +++++
+++++ TIFF +++++
Por el Gobierno de Georgia
Za vládu Gruzie
For Georgiens regering
Für die Regierung von Georgien
Gruusia valitsuse nimel
Για την κυβέρνηση της Γεωργίας
For the Government of Georgia
Pour le gouvernement de la Géorgie
Per il Governo della Georgia
Gruzijas valdības vārdā
Gruzijos Vyriausybės vardu
Grúzia Kormánya részéről
Għall-Gvern tal-Ġeorġja
Voor de Regering van Georgië
W imieniu Rządu Gruzji
Pelo Governo da Geórgia
Za vládu Gruzínska
Za vlado Gruzije
Georgian hallituksen puolesta
För Georgiens regering
+++++ TIFF +++++
+++++ TIFF +++++
--------------------------------------------------
ANNEX I
List of Agreements referred to in Article 1 of this Agreement
(a) Air service agreements between the Government of Georgia and Member States of the European Community which, at the date of signature of this Agreement, have been concluded, signed and/or are being applied provisionally:
- Air Transport Agreement between the Austrian Federal Government and the Government of Georgia signed at Vienna on 15 December 1997 (date of entry into force: 1 October 2001), hereinafter referred to as the "Georgia-Austria Agreement" in Annex II,
- Agreement between the Government of the Republic of Cyprus and the Government of Georgia relating to Air Services signed at Tbilisi on 30 June 1997 (date of entry into force: 5 November 1998), hereinafter referred to as the "Georgia-Cyprus Agreement" in Annex II,
- Air Transport Agreement between the Federal Republic of Germany and the Republic of Georgia signed at Bonn on 25 June 1993 (date of entry into force: 27 November 1994), hereinafter referred to as the "Georgia-Germany Agreement" in Annex II,
- Agreement between the Government of the Hellenic Republic and the Government of the Republic of Georgia on Air Transport signed at Tbilisi on 10 April 1997 (date of entry into force: 27 May 1998), hereinafter referred to as the "Georgia-Greece Agreement" in Annex II,
- Agreement between the Government of Ireland and the Government of the Republic of Georgia on Air Transport signed at Dublin on 2 March 1995 (date of entry into force: 2 March 1995), hereinafter referred to as the "Georgia-Ireland Agreement" in Annex II,
- Agreement between the Government of the Republic of Lithuania and the Government of the Republic of Georgia relating to Air Services signed at Tbilisi on 12 April 1996 (date of entry into force: 12 January 1999), hereinafter referred to as the "Georgia-Lithuania Agreement" in Annex II,
- Agreement between the Kingdom of the Netherlands and the Republic of Georgia for Air Services between and beyond their respective territories signed at Wassenaar on 3 April 1995 (date of entry into force: 1 May 1997), hereinafter referred to as the "Georgia-Netherlands Agreement" in Annex II,
- Agreement between the Government of the United Kingdom of Great Britain and Northern Ireland and Government of the Executive Authority (Government) of Georgia relating to Air Services signed at Tbilisi on 17 September 2003, hereinafter referred to as "Georgia-United Kingdom Agreement" in Annex II;
Supplemented by Memorandum of Understanding done at Tbilisi on 17 September 2003, and by the Agreed Record signed in Tbilisi on 2 November 2004.
(b) Air service agreements and other arrangements initialled or signed between the Government of the Republic of Georgia and Member States of the European Community which, at the date of signature of this Agreement, have not yet entered into force and are not being applied provisionally:
- Agreement between the Government of Belgium and the Government of the Republic of Georgia on Air Transport initialled on 24 February 1995, hereinafter referred to as the "Georgia-Belgium Agreement" in Annex II,
- Agreement between the Government of the Republic of Hungary and the Government of the Republic of Georgia on Air Transport initialled on 29 June 1995, hereinafter referred to as the "Georgia-Hungary Agreement" in Annex II,
- Agreement between the Government of the Republic of Latvia and the Government of Georgia concerning Civil Air Transport signed at Tbilisi on 5 October 2005, hereinafter referred to as the "Georgia-Latvia Agreement" in Annex II,
- Agreement between the Government of the Republic of Poland and the Government of the Republic of Georgia concerning Civil Air Transport initialled at Warsaw on 26 April 1993, hereinafter referred to as the "Georgia-Poland Agreement" in Annex II.
--------------------------------------------------
ANNEX II
List of Articles in the Agreements listed in Annex I and referred to in Articles 2 to 5 of this Agreement
(a) Designation by a Member State:
- Article 3 of the Georgia-Austria Agreement,
- Articles 3 and 4 of the Georgia-Belgium Agreement,
- Article 4 of the Georgia-Cyprus Agreement,
- Article 3 of the Georgia-Germany Agreement,
- Article 3 of the Georgia-Greece Agreement,
- Article 3 of the Georgia-Hungary Agreement,
- Article 3 of the Georgia-Ireland Agreement,
- Article 3 of the Georgia-Latvia Agreement,
- Article 3 of the Georgia-Lithuania Agreement,
- Article 4 of the Georgia-Netherlands Agreement,
- Article 3 of the Georgia-Poland Agreement,
- Article 4 of the Georgia-UK Agreement.
(b) Refusal, Revocation, Suspension or Limitation of Authorisations or Permissions:
- Article 4 of the Georgia-Austria Agreement,
- Article 5 of the Georgia-Belgium Agreement,
- Article 5 of the Georgia-Cyprus Agreement,
- Article 4 of the Georgia-Germany Agreement,
- Article 4 of the Georgia-Greece Agreement,
- Article 4 of the Georgia-Hungary Agreement,
- Article 3, paragraphs 5 and 6, of the Georgia-Ireland Agreement,
- Article 4 of the Georgia-Latvia Agreement,
- Article 4 of the Georgia-Lithuania Agreement,
- Article 5 of the Georgia-Netherlands Agreement,
- Article 4 of the Georgia-Poland Agreement,
- Article 5 of the Georgia-UK Agreement.
(c) Regulatory control:
- Article 7 of the Georgia-Belgium Agreement.
(d) Taxation of Aviation Fuel:
- Article 7 of the Georgia-Austria Agreement,
- Article 10 of the Georgia-Belgium Agreement,
- Article 7 of the Georgia-Cyprus Agreement,
- Article 6 of the Georgia-Germany Agreement,
- Article 9 of the Georgia-Greece Agreement,
- Article 9 of the Georgia-Hungary Agreement,
- Article 11 of the Georgia-Ireland Agreement,
- Article 6 of the Georgia-Latvia Agreement,
- Article 11 of the Georgia-Lithuania Agreement,
- Article 10 of the Georgia-Netherlands Agreement,
- Article 6 of the Georgia-Poland Agreement,
- Article 8 of the Georgia-UK Agreement.
(e) Tariffs for Carriage within the European Community:
- Article 11 of the Georgia-Austria Agreement,
- Article 13 of the Georgia-Belgium Agreement,
- Article 17 of the Georgia-Cyprus Agreement,
- Article 10 of the Georgia-Germany Agreement,
- Article 12 of the Georgia-Greece Agreement,
- Article 8 of the Georgia-Hungary Agreement,
- Article 6 of the Georgia-Ireland Agreement,
- Article 11 of the Georgia-Latvia Agreement,
- Article 9 of the Georgia-Lithuania Agreement,
- Article 6 of the Georgia-Netherlands Agreement,
- Article 10 of the Georgia-Poland Agreement,
- Article 7 of the Georgia-UK Agreement.
--------------------------------------------------
ANNEX III
List of other States referred to in Article 2 of this Agreement
(a) The Republic of Iceland (under the Agreement on the European Economic Area);
(b) The Principality of Liechtenstein (under the Agreement on the European Economic Area);
(c) The Kingdom of Norway (under the Agreement on the European Economic Area);
(d) The Swiss Confederation (under the Agreement between the European Community and the Swiss Confederation on Air Transport).
--------------------------------------------------
