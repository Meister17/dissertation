Order of the President of the Court of 7 March 2006 — Commission of the European Communities
v United Kingdom of Great Britain and Northern Ireland
(Case C-323/05) [1]
(2006/C 154/38)
Language of the case: English
The President of the Court has ordered that the case be removed from the register.
[1] OJ C 271, 29.10.2005.
--------------------------------------------------
