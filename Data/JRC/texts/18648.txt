COMMISSION REGULATION (EEC) No 251/93 of 4 February 1993 amending Regulation (EEC) No 3807/92 on certain provisions in the beef sector following the replacement of Regulation (EEC) No 569/88 by Regulation (EEC) No 3002/92
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 805/68 of 27 June 1968 on the common organization of the market in beef and veal (1), as last amended by Regulation (EEC) No 125/93 (2), and in particular Article 7 (3) thereof,
Whereas Commission Regulation (EEC) No 569/88 (3), was replaced by Commission Regulation (EEC) No 3002/92 laying down common detailed rules for verifying the use and/or destination of products from intervention (4), as last amended by Regulation (EEC) No 75/93 (5), to ensure that the latter are used for the purpose laid down or sent to the destination specified; whereas certain amendments have consequently been laid down in the beef sector by Commission Regulation (EEC) No 3807/92 (6); whereas, further to those amendments, Commission Regulation (EEC) No 2824/85 of 9 October 1985 laying down detailed rules for the sale of frozen boned beef from intervention stocks for export (7) should also be amended, whereas it is appropriate to insert that amendment into Commission Regulation (EEC) No 3807/92;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Beef and Veal,
HAS ADOPTED THIS REGULATION:
Article 1
In Regulation (EEC) No 3807/92 the following Article 2 (a) is inserted:
'Article 2 (a) Article 6 of Regulation (EEC) No 2824/85 is replaced by the following:
"Article 6 Where Article 4 is applied, the removal order referred to in Article 3 (1) (b) and the documents referred to in Article 4 of Regulation (EEC) No 3002/92 as well as the export declaration and where appropriate, box 106 of the control copy T 5, shall show one or more of the following endorsements:
Productos de intervención sin restitución [Reglamento (CEE) n° 2824/85],
Interventionsprodukter uden restitution (forordning (EOEF) nr. 2824/85),
Interventionserzeugnisse ohne Erstattung (Verordnung (EWG) Nr. 2824/85),
Ðñïúueíôá ðáñaaìâUEóaaùò ÷ùñssò aaðéóôñïoeÞ [êáíïíéóìueò (AAÏÊ) áñéè. 2824/85],
Intervention products without refund [Regulation (EEC) No 2824/85],
Produits d'intervention sans restitution [règlement (CEE) n° 2824/85],
Prodotti d'intervento senza restituzione [regolamento (CEE) n. 2824/85],
Interventieprodukten zonder restitutie (Verordening (EEG) nr. 2824/85),
Produtos de intervenção sem restituição [Regulamento (CEE) n° 2824/85]."`
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 4 February 1993.
For the Commission René STEICHEN Member of the Commission
