COUNCIL REGULATION (EEC) No 3680/93 of 20 December 1993 laying down certain conservation and management measures for fishery resources in the Regulatory Area as defined in the Convention on Future Multilateral Cooperation in the North West Atlantic Fisheries
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 3760/92 of 20 December 1992 establishing a Community system for fisheries and aquaculture(1) , and in particular Article 8 (4) thereof,
Having regard to the proposal from the Commission,
Whereas the Community has signed the United Nations' Convention on the Law of the Sea, which contains principles and rules relating to the conservation and management of the living resources within the exclusive economic zones of the coastal States and in the high seas;
Whereas the Convention on Future Multilateral Cooperation in the North West Atlantic Fisheries, hereinafter referred to as the NAFO Convention, was approved by the Council in Regulation (EEC) No 3179/78(2) and entered into force on 1 January 1979; whereas the Regulatory Area as defined consists of that part of the Convention Area which lies beyond the areas in which coastal States exercise fisheries jurisdiction;
Whereas the NAFO Convention establishes a suitable framework for the rational conservation and management of the fishery resources of the Regulatory Area with a view to achieving the optimum utilization thereof; whereas, to this end, the Contracting Parties undertake to carry out joint measures;
Whereas, in the light of the available scientific advice, the catches of certain species in certain parts of the Regulatory Area should be limited; whereas, in accordance with Articles 8 of Regulation (EEC) No 3760/92, it falls to the Council to establish the total allowable catches (TACs) by stock or group of stocks, the share available for the Community and the specific conditions under which catches must be made and to allocate the share available to the Community among the Member States;
Whereas, in order to ensure the conservation of fishery resources and their balanced exploitation, technical conservation measures must be defined, inter alia for mesh sizes, the level of by-catches and authorized fish sizes;
Whereas to enable controls to be carried out on catches from the Regulatory Area while supplementing the monitoring measures provided for in Regulation (EEC) No 2847/93(3) certain specific control measures are to be defined, inter alia, for the declaration of catches, the communication of information, the holding of non-authorized nets and information and assistance relating to the storage and processing of catches,
HAS ADOPTED THIS REGULATION:
Article 1
Scope
1. Community vessels operating in the Regulatory Area and retaining on board fish from resources of that area shall do so in furtherance of the objectives and principles of the NAFO Convention.
2. With a view to ensuring through the joint action of the Contracting Parties the rational conservation and management of the fishery resources of the Regulatory Area for the purpose of achieving the optimum utilization thereof, this Regulation lays down:
- limits on catches,
- technical conservation measures,
- international control measures,
- provisions relating to the processing and transmission of certain scientific and statistical data.
Article 2
Community participation
1. Member States shall forward to the Commission a list of all vessels registered in their ports or flying their flag which intend to take part in the fishing activities in the Regulatory Area at least 30 days before the intended commencement of such activity or, as the case may be, not later than the 20th day following the date of entry into force of this Regulation. The information forwarded shall include:
(a) name of vessel;
(b) official registration number of the vessel assigned by the competent national authorities;
(c) home port of the vessel;
(d) name of owner or charterer of the vessel;
(e) a declaration that the master has been provided with a copy of the regulations in force in the Regulatory Area;
(f) the principal species fished by the vessel in the Regulatory Area;
(g) the sub-areas where the vessel may be expected to fish.
Article 3
Limits on catches
Catches in 1994 of the species set out in Annex I hereto by fishing vessels registered in the ports of Member States or flying their flag shall be limited, within the divisions of the Regulatory Area referred to in that Annex, to the quotas set out therein.
Article 4
Technical measures
1. Mesh sizes
The use of trawl net having in any section thereof net meshes of dimensions less than 130 mm shall be prohibited for direct fishing of the species referred to in Annex II hereto. This mesh size shall be reduced to 60 mm for direct fishing of short-finned squid.
However, up to 1 June 1994, the use of a trawl net or such section of any trawl net as is made of hemp, polyamide fibres or polyester fibres with a minimum mesh size of 120 mm shall be authorized for the fishing of the species listed in Annex II.
Vessels fishing for shrimp (Pandalus borealis) shall use nets with a minimum mesh size of 40 mm.
2. Attachments to nets
The use of any means or device other than those described in this paragraph which obstructs the meshes of a net or which diminishes their size shall be prohibited.
Canvas, netting or any other material may be attached to the underside of the cod-end in order to reduce or prevent damage.
Devices may be attached to the upper side of the cod-end provided that they do not obstruct the meshes of the cod-end. The use of top-side chafers shall be limited to those mentioned in Annex III hereto.
Vessels fishing for shrimp (Pandalus borealis) shall use sorting grids or grates with a maximum spacing between bars of 28 mm.
3. By-catches
By-catches of the species listed in Annex I for which no quotas have been fixed by the Community for a part of the Regulatory Area and taken in that part when fishing directly for:
- one or more of the species listed in Annex I, or
- one or more of species other than those listed in Annex I,
may not exceed for each species on board 2 500 kg or 10 % by weight of all fish on board, whichever is the greater. However, in a part of the Regulatory Area where directed fishing of certain species is banned, by-catches of each of the species listed in Annex I may not exceed 1 250 kg or 5 % respectively.
For vessels fishing for shrimp (Pandalus borealis), in the event that total by-catches of all species listed in Annex I, in any haul exceed 10 % by weight, vessels shall immediately change fishing area (minimum 5 nautical miles) in order to seek to avoid further by-catches of these species.
4. Minimum size of fish
Fish from the Regulatory Area which do not have the size required as set out in Annex IV may not be retained on board or be transhipped, landed, transported, stored, sold, displayed or offered for sale, but shall be returned immediately to the sea. Where the quantity of caught fish not having the required size exceeds in certain fishing waters 10 % of the total quantity, the vessel must move away to a distance of at least five nautical miles before continuing fishing.
Article 5
Control measures
1. In addition to complying with Articles 6, 8, 11 and 12 of Regulation (EEC) No 2847/93, masters of vessels shall enter in the logbook the information listed in Annex V hereto.
In complying with Article 15 of the said Regulation, Member States shall also inform the Commission of catches of species not subject to quota.
2. When fishing directly for one or more of the species listed in Annex II, vessels may not carry nets the mesh size of which is smaller than that laid down in Article 4 (1).
However, vessels fishing in the course of the same voyage in areas other than the Regulatory Area may keep such nets on board provided these nets are securely lashed and stowed and are not available for immediate use, that is to say:
(a) nets shall be unshackled from their boards and their hauling or trawling cables and ropes;
(b) nets carried on or above the deck must be lashed securely to a part of the superstructure.
3. The masters of fishing vessels flying the flag of a Member State or registered in one of its ports shall, in respect of catches of the species listed in Annex I, keep:
(a) a logbook stating, by species and by processed product, the aggregate output; or
(b) a storage plan, by species, of products processed, indicating where they are located in the hold.
Masters of vessels must provide the necessary assistance to enable the quantities declared in the logbook and the processes products stored on board to be verified.
Article 6
Statistical and scientific data
1. In order to secure advice on localized and seasonal concentrations of juvenile American plaice and yellowtail flounder in division 3LNO of the Regulatory Area:
(a) Member States shall provide, based upon the relevant entries in the logbook, as provided for by Article 5 (1), nominal catch and discard statistics, broken down by unit areas no larger than 1° latitude and 1° longitude, summarized on a monthly basis;
(b) length sampling shall be provided for both nominal catches and discards, with a sampling intensity on the same scale as adopted in (a) and summarized on a monthly basis.
2. In order to assess the effects of cod by-catches in the redfish and flatfish fisheries on the Flemish Cap:
(a) Member States shall supply, based upon the relevant entries in the logbook as provided for by Article 5 (1), statistics on discards of cod taken in the redfish and flatfish fisheries in the above area, in addition the normal reports, summarized on a monthly basis;
(b) length sampling of cod taken in the redfish and flatfish fisheries in the above area, shall be provided for the two components separately, with depth information accompanying each sample, summarized on a monthly basis.
3. Length samples shall be taken from all parts of the respective catch of each species concerned in such a manner that at least one statistically significant sample is taken from the first haul taken each day. The size of a fish shall be measured from the tip of the snout to the end of the tail fin.
For the purposes set out in paragraphs 1 and 2, length samples taken as described in this Regulation shall be deemed to be representative of all catches of the species concerned.
Article 7
This Regulation shall enter into force on 1 January 1994.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 20 December 1993.
For the Council
The President
A. BOURGEOIS
(1) OJ No L 389, 31. 12. 1992, p. 1.
(2) OJ No L 378, 30. 12. 1978, p. 1.
(3) OJ No L 261, 20. 10. 1993, p. 1.
ANNEX I
"" ID="1">Cod> ID="2">North-west Atlantic> ID="3">NAFO 2 J - 3 KL> ID="4">Belgium
Denmark
Germany
Greece
Spain
France
Ireland
Italy
Luxembourg
Netherlands
Portugal
United Kingdom
Available for Member States
EC total> ID="5">0
"> ID="1">Cod> ID="2">North-west Atlantic> ID="3">NAFO 3 NO> ID="4">Belgium
Denmark
Germany> ID="5">5"> ID="1"" ID="2"" ID="3"" ID="4">Greece
Spain> ID="5">1 832"> ID="1"" ID="2"" ID="3"" ID="4">France> ID="5">28"> ID="1"" ID="2"" ID="3"" ID="4">Ireland
Italy
Luxembourg
Netherlands
Portugal> ID="5">345"> ID="1"" ID="2"" ID="3"" ID="4">United Kingdom> ID="5">3"> ID="1"" ID="2"" ID="3"" ID="4">Available for Member States
EC total> ID="5">2 213
"> ID="1">Cod> ID="2">North-west Atlantic> ID="3">NAFO 3 M> ID="4">Belgium
Denmark
Germany> ID="5">513"> ID="1"" ID="2"" ID="3"" ID="4">Greece
Spain> ID="5">1 574"> ID="1"" ID="2"" ID="3"" ID="4">France> ID="5">221"> ID="1"" ID="2"" ID="3"" ID="4">Ireland
Italy
Luxembourg
Netherlands
Portugal> ID="5">2 155"> ID="1"" ID="2"" ID="3"" ID="4">United Kingdom> ID="5">1 022"> ID="1"" ID="2"" ID="3"" ID="4">Available for Member States
EC total> ID="5">5 485
"> ID="1">Atlantic redfish> ID="2">North-west Atlantic> ID="3">NAFO 3 M> ID="4">Belgium
Denmark
Germany
Greece
Spain
France
Ireland
Italy
Luxembourg
Netherlands
Portugal
United Kingdom
Available for Member States
EC total> ID="5">4 030
"> ID="1">Atlantic redfish> ID="2">North-west Atlantic> ID="3">NAFO 3 LN> ID="4">Belgium
Denmark
Germany> ID="5">476"> ID="1"" ID="2"" ID="3"" ID="4">Greece
Spain
France
Ireland
Italy
Luxembourg
Netherlands
Portugal
United Kingdom
Available for Member States
EC total> ID="5">476
"> ID="1">American plaice> ID="2">North-west Atlantic> ID="3">NAFO 3 M(1) > ID="4">Belgium
Denmark
Germany
Greece
Spain
France
Ireland
Italy
Luxembourg
Netherlands
Portugal
United Kingdom
Available for Member States
EC total> ID="5">175
"> ID="1">American plaice> ID="2">North-west Atlantic> ID="3">NAFO 3 LNO(2) > ID="4">Belgium
Denmark
Germany
Greece
Spain
France
Ireland
Italy
Luxembourg
Netherlands
Portugal
United Kingdom
Available for Member States
EC total> ID="5">61
"> ID="1">Yellow tail flounder> ID="2">North-west Atlantic> ID="3">NAFO 3 LNO(3) > ID="4">Belgium
Denmark
Germany
Greece
Spain
France
Ireland
Italy
Luxembourg
Netherlands
Portugal
United Kingdom
Available for Member States
EC total> ID="5">140
"> ID="1">Witch flounder> ID="2">North-west Atlantic> ID="3">NAFO 3 NO> ID="4">Belgium
Denmark
Germany
Greece
Spain
France
Ireland
Italy
Luxembourg
Netherlands
Portugal
United Kingdom
Available for Member States
EC total> ID="5">0
"> ID="1">Capelin> ID="2">North-west Atlantic> ID="3">NAFO 3 NO> ID="4">Belgium
Denmark
Germany
Greece
Spain
France
Ireland
Italy
Luxembourg
Netherlands
Portugal
United Kingdom
Available for Member States
EC total> ID="5">0
"> ID="1">Squid> ID="2">North-west Atlantic> ID="3">NAFO-subzones 3+4> ID="4">Belgium
Denmark
Germany
Greece
Spain
France
Ireland
Italy
Luxembourg
Netherlands
Portugal
United Kingdom
Available for Member States
EC total> ID="5">p.m.
">
(1) There will be no direct fishing on this species, which will be caught only as a by-catch, without prejudice to the rules set out in Article 4 (3) hereof.
ANNEX II
"" ID="1">Principal groundfish (except flatfish)
" ID="1">Atlantic cod
Haddock
Atlantic redfishes
Golden redfish
Silver hake
Red hake
Pollock (= saithe)
> ID="2">Gadus morhua
Melanogrammus aeglefinus
Sebastes spp.
Sebastes marinus
Sebastes mentella
Merluccius bilinearis
Urophucis chuss
Pollachius virens"" ID="1">Flatfish
" ID="1">American plaice
Witch flounder
Yellowtail flounder
Greenland halibut
Atlantic halibut
Winter flounder
Summer flounder
Windowpane flounder
Flatfish (NS)
> ID="2">Hippoglossoides platessoides
Glyptocephalus cynoglossus
Limanda ferruginea
Reinhardtius hippoglossoides
Hippoglossus hippoglossus
Pseudopleuronectes americanus
Paralichthys dentatus
Scophthalmus aquosus
Pleuronectiformes"" ID="1">Other groundfish
" ID="1">American angler (= goosefish)
Atlantic searobins
Atlantic tomcod
Blue whiting
Cunner
Cusk (= tusk)
Greenland cod
Blue ling
Ling
Lumpfish (= lumpsucker)
Northern kingfish
Northern puffer
Eelpouts (NS)
Ocean pout
Polar cod
Roundnose grenadier
Roughhead grenadier
Sandeels (= sand lances)
Sculpins
Scup
Tautog
Tilefish
White hake
Wolffishes (NS)
Atlantic wolffish
Spotted wolffish
Groundfish (NS)
> ID="2">Lophius americanus
Prionotus spp.
Microgadus tomcod
Micromesistius poutassou
Tautogolabrus adspersus
Brosme brosme
Gadus ogac
Molva dypterygia
Molva molva
Cyclopterus lumpus
Menticirrhus saxatilis
Sphoeroides maculatus
Lycodes spp.
Macrozoarces americanus
Boreogadus saida
Coruphaenoides rupestris
Macrouris berglax
Ammodytes spp.
Myoxocephalus spp.
Stenotomus chrysops
Tautoga onitis
Lopholatilus chamaeleonticeps
Urophycis tenuis
Anarhichas spp.
Anarhichas lupus
Anarhichas minor
. . .""
ANNEX III
AUTHORIZED TOP-SIDE CHAFERS 1. ICNAF-type top-side chafer
A rectangular piece of netting attached to the upper side of a cod-end to reduce or prevent damage and complying with the following requirements:
(a) the netting shall not have a mesh size less than that specified for the net itself;
(b) the netting may be fastened to the cod-end only along the forward and lateral edges of the netting and shall be fastened in such a manner that it extends forward of the splitting strap no more than four meshes and ends not less than four meshes in front of the codline mesh; where a splitting strap is not used, the netting shall not extend to more than one-third of the cod-end measured from not less than four meshes in front of the codline mesh;
(c) the number of meshes in the width of the netting shall be at least one and a half times the number of meshes in the width of the part of the cod-end which is covered, both widths being taken at right angles to the longitudinal axis the cod-end.
2. 'Multiple flap' top-side chafer
Pieces of netting having in all their parts meshes the dimensions of which, whether the pieces of netting are wet or dry, are not less than those of the meshes of the net to which they are attached, provided that:
(i) each piece of netting:
(a) is fastened only by its forward edge across the cod-end at right angles to its longitudinal axis;
(b) is at least equal in width to the width of the cod-end (such width being measured at right angles to the longitudinal axis of the cod-end at the point of attachment);
(c) is not more than 10 meshes long;
(ii) the aggregate length of all the pieces of netting so attached does not exceed two-thirds of the length of the cod-end.
3. Large mesh (modified Polish-type) top-side chafer
A rectangular piece of netting made of the same twine material as the cod-end, or of a single, thick, knotless twine material, attached to the rear portion of the upper side of the cod-end and extending over all or any part of the upper side of the cod-end, having in all its parts a mesh size twice that of the cod-end when measured wet and fastened to the cod-end along the forward, lateral and rear edges only of the netting in such a way that each mesh of the netting exactly coincides with four meshes of the cod-end.
ANNEX IV
"" ID="1">Atlantic cod> ID="2">41 cm> ID="3">fork length"> ID="1">American plaice> ID="2">25 cm> ID="3">total length"> ID="1">Yellowtail flounder> ID="2">25 cm> ID="3">total length">
ANNEX V
Particulars to be contained in the logbook "" ID="1">Name of vessel> ID="2">01
"> ID="1">Nationality of vessel> ID="2">02
"> ID="1">Registration number of vessel> ID="2">03
"> ID="1">Port of registration> ID="2">04
"> ID="1">Type of fishing gear used (daily)> ID="2">10
"> ID="1">Type of fishing gear> ID="2"> 2(1)
"> ID="1">Date:
"> ID="1">- day> ID="2">20
"> ID="1">- month> ID="2">21
"> ID="1">- year> ID="2">22
"> ID="1">Position:
"> ID="1">- latitude> ID="2">31
"> ID="1">- longitude> ID="2">32
"> ID="1">- statistical zone> ID="2">33
"> ID="1">Number of fishing operations per period of 24 hours(2) > ID="2">40
"> ID="1">Number of hours fishing with gear per period of 24 hours(3) > ID="2">41
"> ID="1">Name of species> ID="2"> 2(4)
"> ID="1">Daily catches by species (in tonnes live weight)> ID="2">50
"> ID="1">Daily catches, per species, for human consumption> ID="2">61
"> ID="1">Quantities discarded daily per species> ID="2">63
"> ID="1">Place of transhipment> ID="2">70
"> ID="1">Date(s) of transhipment> ID="2">71
"> ID="1">Master's signature> ID="2">80
"">
Standard abbreviations for the principal species in the NAFO area "" ID="1">ALE> ID="2">Alewife> ID="3">Alosa pseudoharengus
"> ID="1">ARG> ID="2">Atlantic argentine> ID="3">Argentina silus
"> ID="1">BUT> ID="2">American butterfish> ID="3">Peprilus triacanthus
"> ID="1">CAP> ID="2">Capelin> ID="3">Mallotus villosus
"> ID="1">COD> ID="2">Cod> ID="3">Gadus morhua
"> ID="1">GHL> ID="2">Greenland halibut> ID="3">Reinhardtius hippoglossoides
"> ID="1">HAD> ID="2">Haddock> ID="3">Melanogrammus aeglefinus
"> ID="1">HER> ID="2">Herring> ID="3">Clupea harengus
"> ID="1">HKR> ID="2">Red hake> ID="3">Urophycis chuss
"> ID="1">HKS> ID="2">Silver hake> ID="3">Merluccius bilinearis
"> ID="1">MAC> ID="2">Mackerel> ID="3">Scomber scombrus
"> ID="1">PLA> ID="2">American plaice> ID="3">Hippoglossoides platessoides
"> ID="1">POK> ID="2">Saithe> ID="3">Pollachius virens
"> ID="1">RED> ID="2">Redfish> ID="3">Sebastes marinus
"> ID="1">RMG> ID="2">Roundnose grenadier> ID="3">Macrourus rupestris
"> ID="1">SHR> ID="2">Shrimps and prawns> ID="3">Pandalus spp.
"> ID="1">SQU> ID="2">Squid> ID="3">Loligo pealei - Illex illecebrosus
"> ID="1">WIT> ID="2">Witch flounder> ID="3">Glyptocephalus cynoglossus
"> ID="1">YEL> ID="2">Yellowtail flounder> ID="3">Limanda ferruginea
">
Standard abbreviations for fishing gear "" ID="1">OTB> ID="2">Bottom otter trawl (side or stern, not specified)
"> ID="1">OTB 1> ID="2">Bottom otter trawl (side)
"> ID="1">OTB 2> ID="2">Bottom otter trawl (stern)
"> ID="1">OTM> ID="2">Midwater otter trawl (side or stern, not specified)
"> ID="1">OTM 1> ID="2">Midwater otter trawl (side)
"> ID="1">OTM 2> ID="2">Midwater otter trawl (stern)
"> ID="1">PTB> ID="2">Bottom pair trawl (2 vessels)
"> ID="1">PTM> ID="2">Midwater pair trawl (2 vessels)
"> ID="1">GM> ID="2">Gillnets (not specified)
"> ID="1">GNS> ID="2">Set gillnets
"> ID="1">LL> ID="2">Longlines (set or drifting, not specified)
"> ID="1">LLS> ID="2">Longlines (set)
"> ID="1">LLD> ID="2">Longlines (drifting)
"> ID="1">MIS> ID="2">Miscellaneous fishing gear
"> ID="1">NK> ID="2">Fishing gear not known
">
(1) To be indicated together with one of the abbreviations listed in the second part of this Annex.
(2) Where two or more types of fishing gear are used in a given 24-hour period, separate records must be supplied for each type of gear.
