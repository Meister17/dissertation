Commission Regulation (EC) No 1746/2005
of 24 October 2005
amending Regulation (EEC) No 2342/92 as regards the pedigree certificates to be presented for granting export refunds on female pure-bred breeding animals of the bovine species
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1254/1999 of 17 May 1999 on the common organisation of the market in beef and veal [1], and in particular Articles 31(4) and 33(12) thereof,
Whereas:
(1) Article 3 of Commission Regulation (EEC) No 2342/92 of 7 August 1992 on imports of pure-bred breeding animals of the bovine species from third countries and the granting of export refunds thereon and repealing Regulation (EEC) No 1544/79 [2] lays down the criteria for granting export refunds on female pure-bred breeding animals of the bovine species. Those criteria include the presentation of a pedigree certificate at the time when the customs export formalities are completed.
(2) Certain requirements regarding the particulars to be contained in pedigree certificates are set out in Article 3(a) of Regulation (EEC) No 2342/92. Those requirements derive directly from Commission Decision 86/404/EEC [3], which was repealed and replaced by Commission Decision 2005/379/EC of 17 May 2005 on pedigree certificates and particulars for pure-bred breeding animals of the bovine species, their semen, ova and embryos [4].
(3) Some of the requirements regarding the content of pedigree certificates were clarified in Decision 2005/379/EC. The provisions on the particulars to be contained in pedigree certificates for the purpose of granting export refunds on female pure-bred breeding animals should, therefore, be brought into line with that Decision.
(4) Regulation (EEC) No 2342/92 should therefore be amended accordingly.
(5) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Beef and Veal,
HAS ADOPTED THIS REGULATION:
Article 1
In the first paragraph of Article 3 of Regulation (EEC) No 2342/92, point (a) is hereby replaced by the following:
"(a) the pedigree certificate drawn up in accordance with Article 2(1) of Commission Decision 2005/379/EC [5] or any other document drawn up in accordance with paragraph 2 of that Article.
Article 2
This Regulation shall enter into force on the seventh day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 24 October 2005.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 160, 26.6.1999, p. 21. Regulation last amended by Regulation (EC) No 1782/2003 (OJ L 270, 21.10.2003, p. 1).
[2] OJ L 227, 11.8.1992, p. 12. Regulation last amended by Regulation (EC) No 774/98 (OJ L 111, 9.4.1998, p. 65).
[3] OJ L 233, 20.8.1986, p. 19.
[4] OJ L 125, 18.5.2005, p. 15.
[5] OJ L 125, 18.5.2005, p. 15."
--------------------------------------------------
