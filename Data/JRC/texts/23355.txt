DECISION OF THE EXECUTIVE COMMITTEE
of 21 April 1998
on the activities of the Task Force
(SCH/Com-ex (98)1 rev. 2)
THE EXECUTIVE COMMITTEE,
Having regard to Article 132 of the Convention implementing the Schengen Agreement,
Having regard to Article 6 of the abovementioned Convention,
HAS DECIDED AS FOLLOWS:
Given the increase in the number of foreign nationals immigrating into the Schengen States, in particular nationals of Iraq and other States, it is necessary, in the context of recommendations either already made or still under discussion in the European Union, to step up external border checks and take practical steps to underpin the effectiveness of such checks in accordance with a joint plan.
In accordance with the rule of specific powers, it is possible within the bounds of the Schengen remit to focus particular attention on the pull factors underlying this irregular immigration flow without losing sight of the fact that other bodies are to deal with the causes of this phenomenon in the regions of origin and transit.
The Executive Committee proposes that the Schengen States immediately begin to implement the following measures for checks at the external borders, whilst taking into account the recommendations contemplated within the European Union in other areas and whilst stressing the need for appropriate steps also to be taken in implementation of the Dublin Convention:
- the reinforcement of entry checks at the external borders by deploying extra personnel and modern technology,
- securing the areas at airports not accessible to the public with regard to flights from outside the Schengen area and transfer passengers; parallel measures at ports serving international traffic,
- the provision of mutual assistance in the initial and further training both of staff responsible for carrying out checks at airports and ports and of airline personnel, for instance by means of bilateral exchange programmes; greater use and the reciprocal provision of modern technology and an increase in the number of staff deployed,
- the inspection of ferries both during loading and when putting out to sea,
- implementing and fostering the harmonisation of sanctions on and arrangements with carriers transporting illegal immigrants to the Schengen area,
- the performance of pre-boarding checks at high-risk points of departure still to be specified,
- the exchange of information on routes and modus operandi used for smuggling illegal immigrants, stepping up practical cooperation between police authorities and border protection authorities, stepping up cooperation between these authorities and liaison officers from Schengen States working in third countries; the exchange of officials from the Contracting Parties by mutual agreement in order to monitor the effectiveness of measures to prevent illegal immigration,
- in compliance with the relevant national law, the fingerprinting of every foreign national entering the Schengen area illegally whose identity cannot be established with certainty on the basis of valid documents; retention of fingerprints for the purpose of informing the authorities in other Schengen States; the principles of personal data protection law applicable in the European Union are to be observed,
- in compliance with the relevant national law, preventing foreign nationals who have entered the Schengen area illegally and whose identity cannot be established with certainty from absconding, either until such time as their identity has been clearly established or until the measures required by the aliens police have been ordered and implemented,
- the immediate expulsion of foreign nationals who have entered the territory of the Contracting States illegally in so far as they have no right to remain,
- supporting negotiations on a readmission agreement between the Schengen States on the one hand and Turkey, the Czech Republic, the Slovak Republic, Hungary and Slovenia on the other,
- improving practical cooperation between Schengen States in the application of the Dublin Convention.
These measures shall be implemented:
- whilst respecting the national sovereignty of each of the States,
- in accordance with the law of the Contracting States, in other words, if national law so permits,
- without prejudice to the provisions of existing bilateral agreements,
- in accordance with the Schengen Implementing Convention, and in particular Articles 134 and 142 thereof, in particular regarding the Dublin Convention.
In view of the need to steer the implementation of these measures and build on them, the Presidency is requested to set up a Task Force composed at least of representatives of the six most affected States. This Task Force should meet at short intervals and report to the next Executive Committee.
This Decision is to be implemented as a complement to the EU action plan. The necessary coordination is to take place at the level of the K4 Committee and the Central Group.
Brussels, 21 April 1998.
The Chairman
J. Vande Lanotte
