Political and Security Committee Decision Darfur/3/2006
of 25 July 2006
appointing a Military Advisor to the European Union Special Representative for Sudan
(2006/634/CFSP)
THE POLITICAL AND SECURITY COMMITTEE,
Having regard to the Treaty on European Union, and in particular to Article 25(3) thereof,
Having regard to Council Joint Action 2005/557/CFSP of 18 July 2005 on the European Union civilian-military supporting action to the African Union mission in the Darfur region of Sudan [1], and in particular Article 4 thereof,
Whereas:
(1) On 5 July 2006 the Council adopted Joint Action 2006/468/CFSP [2], renewing and revising the mandate of the Special Representative of the European Union for Sudan.
(2) The Special Representative of the European Union (EUSR) for Sudan inter alia ensures the coordination and coherence of the Union's contributions to the African Union mission in the Darfur region of Sudan (AMIS). In accordance with Article 5(2) of Joint Action 2005/557/CFSP, an EU Coordination Cell in Addis Ababa (ACC) under the authority of the EUSR, comprised of a political advisor, a military advisor and a police advisor, manages day-to-day coordination with all relevant EU actors and with the Administrative Control and Management Centre (ACMC) within the chain of command of the African Union in Addis Ababa in order to ensure coherence and timely EU support to AMIS.
(3) Under Article 4 of Joint Action 2005/557/CFSP the Council authorised the Political and Security Committee to appoint the military advisor to the EUSR upon a proposal from the Secretary-General/High Representative (SG/HR) based on a recommendation from the EUSR.
(4) The SG/HR, following the recommendation of the EUSR, has proposed that Colonel Marc BOILEAU be appointed as the new Military Advisor to the EUSR.
(5) In accordance with Article 6 of the Protocol on the position of Denmark annexed to the Treaty on European Union and to the Treaty establishing the European Community, Denmark does not take part in the elaboration and implementation of decisions and actions of the European Union which have defence implications,
HAS DECIDED AS FOLLOWS:
Article 1
Colonel Marc BOILEAU is hereby appointed Military Advisor to the EUSR for Sudan.
Article 2
This Decision shall take effect on 28 July 2006.
Done at Brussels, 25 July 2006.
For the Political and Security Committee
The Chairperson
T. Tanner
[1] OJ L 188, 20.7.2005, p. 46.
[2] OJ L 184, 6.7.2006, p. 38.
--------------------------------------------------
