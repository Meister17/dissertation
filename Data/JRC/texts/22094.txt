DECISION OF THE EXECUTIVE COMMITTEE
of 15 December 1997
amending the Financial Regulation on C.SIS
(SCH/Com-ex (97) 35)
THE EXECUTIVE COMMITTEE,
Having regard to Article 132 of the Convention implementing the Schengen Agreement,
Having regard to Articles 92 and 119 of the abovementioned Convention,
Having regard to Articles 2 and 3 of the Cooperation Agreement between the Contracting Parties to the Schengen Agreement and the Schengen Convention of the one part and the Republic of Iceland and the Kingdom of Norway of the other,
HAS DECIDED AS FOLLOWS:
The version of the Financial Regulation on the costs of installing and operating the Schengen C.SIS (SCH/Com-ex (93) 16 rev.), dated 20 December 1996, is hereby amended as follows.
Vienna, 15 December 1997.
The Chairman
K. Schlögl
SCH/Com-ex (93)16 Rev. 2
THE EXECUTIVE COMMITTEE,
Having regard to Article 132 of the Convention implementing the Schengen Agreement,
Having regard to Articles 92 and 119 of the abovementioned Convention,
Having regard to Articles 2 and 3 of the Cooperation Agreement between the Contracting Parties to the Schengen Agreement and the Schengen Convention of the one part and the Republic of Iceland and the Kingdom of Norway of the other,
HAS DECIDED AS FOLLOWS:
The Financial Regulation for the costs of installing and operating for the technical support function for the Schengen Information System (C.SIS), attached hereto, is hereby adopted.
FINANCIAL REGULATION
FOR THE INSTALLATION AND OPERATION OF THE SCHENGEN C.SIS
TITLE I
GENERAL PROVISIONS
The budget for the technical support function of the Schengen Information System in Strasbourg provided for in Articles 92 and 119 of the Schengen Agreement of 14 June 1985, hereinafter the "C.SIS", shall be made up of:
- the installation budget for the central information system, for which expenditure shall be approved by the Executive Committee, after receiving the Central Group's opinion,
- the operating budget, for which the annual amount of expenditure shall be approved by the Executive Committee, after receiving the Central Group's opinion.
The installation and operating budgets for the C.SIS shall as far as possible take into account the multiannual table for the SIS installation and operating budgets.
The multiannual table for the SIS installation and operating budgets, covering at least three years, shall contain an estimate of predicted expenditure.
The multiannual table for the SIS installation and operating budgets shall be updated each year by the SIS Steering Committee and approved by the Central Group during the first quarter of the calendar year.
1. C.SIS own resources
C.SIS own resources for both the installation and operating budgets shall be composed of the contributions of the Contracting Parties and the States of the Cooperation Agreement, hereinafter "the cooperating States". The cooperating States' contributions shall be determined on the basis of their share in the GDP aggregate of all the Contracting Parties and cooperating States. The Contracting Parties' contributions shall be determined on the basis of each Contracting Party's share in the uniform VAT assessment base within the meaning of Article 2(1)(c) of the Council Decision of 24 June 1988 on the system of the Communities' own resources.
The breakdown of contributions among the Contracting Parties of the one part and the cooperating States of the other shall be determined on the basis of the share of each Contracting Party and cooperating State in the GDP aggregate of all the Contracting Parties and cooperating States for the preceding year. The breakdown of contributions among the Contracting Parties shall be determined each year, taking into account the cooperating States' contributions, on the basis of each Contracting Party's share of own resources in the European Communities' VAT resources, as established by the last amendment to the Community budget for the preceding financial year.
The contributions of the Contracting Parties and cooperating States to each budget shall be calculated and laid down in French francs by the French Contracting Party.
2. Payment of contributions
Each Contracting Party and cooperating State shall transfer its contributions to the following account:
Compte Tresor Public
Banque de France
No 9000-3
(agence centrale comptable du trésor)
Each payment shall be lodged in a support fund set up under the French Republic's budget (fonds de concours No 09.1.4.782) with the Ministry of the Interior as beneficiary.
3. Accession of new States
If a new Contracting Party accedes, the following shall apply as of the date of accession:
- the contributions of the Contracting Parties and the cooperating States shall be adjusted pursuant to Title I.1 of this Financial Regulation;
- the contributions of the Contracting Parties and the cooperating States shall be adjusted with a view to fixing the new Contracting Party's contribution to C.SIS operating costs as of the year of accession;
- the contributions of the Contracting Parties and the cooperating States shall be adjusted with a view to allocating to the new Contracting Party a proportion of the costs previously incurred for the installation of the C.SIS. This amount shall be calculated according to the share of the new Contracting Party's VAT resources in the total European Communities' VAT resources for the years in which costs were incurred for the installation of the C.SIS prior to the new Contracting Party's accession. This amount shall be reimbursed to the other States in proportion to their contribution, calculated pursuant to Title I.1 of this Regulation.
TITLE II
INSTALLATION BUDGET
The French Republic shall bear all the advance costs of C.SIS installation in accordance with the rules of law governing French public finances. The amounts fixed as the contribution of each Member State and cooperating State shall be calculated and laid down in French francs by the French Contracting Party pursuant to Title I.1 of this Financial Regulation.
1. Forecast expenditure
During the year before the budget is due to be implemented, the French Contracting Party shall draw up an annual draft budget for C.SIS installation expenditure taking into account as far as possible the provisional multiannual table for SIS installation and operations. This draft budget shall be submitted to the Central Group for its opinion and to the Executive Committee for adoption at least six months before the beginning of the financial year.
If the draft budget is rejected, the French Contracting Party shall prepare a new draft within one month which, following the Central Group's opinion, shall be submitted immediately to the Executive Committee for adoption.
At the end of each quarter of the financial year, the Central Group shall, after receiving the SIS Steering Committee's opinion, authorise C.SIS installation expenditure as well as any unforeseen expenditure, which shall be justified in a supporting document.
In the six months following the closure of the financial year, the French Contracting Party shall draw up a multiannual table of the C.SIS installation expenditure that is authorised by the Central Group until the end of the financial year.
This table shall be submitted to the Executive Committee for approval at the same time as the annual draft budget for C.SIS installation expenditure.
The contributions of the respective States shall fall due for payment upon the Executive Committee's approval of expenditure and shall be paid pursuant to the procedure laid down in Title II.2.
The Contracting Parties and cooperating States undertake to cover all installation expenditure up to the amount approved by the Executive Committee.
The Contracting Parties and cooperating States may choose to pay their contributions for C.SIS installation in the form of an advance covering part or all of their forecast contribution.
2. Method of payment
As a rule, the contributions of the Contracting Parties and the cooperating States shall fall due on the date on which the French Contracting Party makes the payments.
Nevertheless, and with a view to restricting the number of calls for payment, the French Contracting Party shall send calls for payment to the States twice a year, on 30 April and 31 October, to take into account the States' deadlines for operating expenditure commitments.
The French Contracting Party shall send a letter containing a call for payment to the States via the designated administrative authorities, details of which have been given to it.
This letter shall state:
- the legal bases for the call for payment,
- the amount of the C.SIS installation budget approved,
- the amount to be paid for the period in question,
- the necessary information for payment of the contribution, as stipulated under Title I.2 of this Financial Regulation.
The following documents shall be attached to this letter:
- a table showing the shares of the cooperating States, calculated on the basis of GDP, and a table showing each State's share of the C.SIS operating budget for the expenditure incurred during the given period, calculated on the basis of its VAT share in the SIS;
- copies of documents warranting the amount to be transferred.
To ensure the smooth transfer of payments, each State should attach to its transfer a note containing the following information:
OBJET: versement de la quote-part 199... de l'Etat ... au budget d'installation du système informatique Schengen
MONTANT: ... francs
BENEFICIAIRE: Ministère de l'Intérieur, Direction des transmissions et de l'informatique
(SUBJECT: payment of the 199... contribution from ... (State) to the installation budget of the Schengen Information System
AMOUNT: ... francs
BENEFICIARY: Ministry of the Interior, Department of data transmission and informatics)
3. Financing by a State other than the French Republic
If, in agreement with the other Contracting Parties and the cooperating States, a Contracting Party or cooperating State directly bears part of the C.SIS installation costs, this expenditure shall be apportioned to the Contracting States in accordance with the distribution key laid down by the French Contracting Party for the financial year in which the expenditure is made.
The Contracting Party or cooperating State having directly borne this expenditure shall inform the French Contracting Party, which shall call in the contributions of the contracting Parties and the Cooperating States, calculated pursuant to this Financial Regulation.
The French Contracting Party shall reimburse the payment made as soon as the contributions owing have been received from the other Contracting Parties and cooperating States.
TITLE III
OPERATING BUDGET
The French Republic shall bear the advance costs of C.SIS operations in accordance with the rules of law governing French public finances. The amounts fixed as the contribution of each Contracting Party and cooperating State shall be calculated and laid down in French francs by the French Contracting Party pursuant to Title I.1 of this Financial Regulation.
1. Draft operating budget
During the year before the budget is due to be implemented, the French Contracting Party shall draw up the draft budget for C.SIS operating expenditure. The draft budget shall be submitted to the Central Group for its opinion and to the Executive Committee for adoption at least six months before the beginning of the financial year.
This draft budget shall take into account as far as possible the multiannual table on SIS installation and operations.
Documents on forecast expenditure shall be annexed to the draft budget.
The budget shall be adopted unanimously by the Contracting Parties.
If the draft budget is rejected, the French Contracting Party shall prepare a new draft within one month which, following the Central Group's opinion, shall be immediately submitted to the Executive Committee for adoption.
During the period between the two consultations or failing adoption of the draft budget, the French Contracting Party may call in the contributions of the Contracting Parties and the cooperating States and initiate the implementation of the budget by provisional twelfths until such time as the budget for the current financial year is adopted.
The French Contracting Party may submit an amending draft budget to the Executive Committee. This shall be submitted to the latter for adoption following the Central Group's opinion.
Any deficit or surplus arising during the financial year must be cleared the following year in the course of the budget's implementation.
2. Method of payment
The Executive Committee decision adopting the budget shall be duly notified to all the Contracting Parties and cooperating States by the Presidency-in-office; the contributions of the Contracting Parties and cooperating States shall fall due for payment immediately thereafter.
To this end, the French Contracting Party shall send each Contracting Party and cooperating State a call for payment of contributions owing and shall forward the Presidency a copy thereof.
The Contracting Parties and the cooperating States shall pay their contributions in full by 30 April of the current financial year.
If a Contracting Party does not honour its financial obligations by that date, the Community regulations in force governing the interest to be paid in default in payment of contributions to the Community budget shall apply. These regulations shall apply mutatis mutandis in cases where a cooperating State does not honour its financial obligations in due time.
The French Contracting Party shall send a letter containing a call for payment to the States via the designated administrative authorities, details of which have been given to it, at the beginning of the financial year in which the adopted operating budget is to be implemented.
The letter shall state:
- the legal bases for the call for payment,
- the amount of the operating budget adopted by the Executive Committee for the year in question.
A table showing the contributions of the cooperating States, calculated on the basis of GDP, and a table showing each Contracting Party's contribution to the C.SIS operating budget calculated on the basis of its VAT share in the SIS, shall be attached to this letter. A table showing the calculation of the GDP share and the VAT share in the SIS for the year in which the expenditure is to be made shall also be annexed.
To ensure the smooth transfer of payments, each State should attach to its transfer a note containing the following information:
OBJET: versement de la quote-part 199... de l'Etat ... au budget de fonctionnement du système informatique Schengen
MONTANT: ... francs
BENEFICIAIRE: Ministère de l'Intérieur, Direction des transmissions et de l'informatique
(SUBJECT: payment of the 199... contribution from ... (State) to the operating budget of the Schengen Information System
AMOUNT: ... francs
BENEFICIARY: Ministry of the Interior, Department of data transmission and informatics)
The Contracting Parties and cooperating States may choose to advance an amount to cover their estimated contributions for a number of financial years.
TITLE IV
APPROVAL OF ACCOUNTS
At the beginning of each financial year, the French Contracting Party shall send the States a document, drawn up on the basis of the provisions of this Financial Regulation, required for the Executive Committee to give final discharge for the preceding financial year following the Central Group's opinion.
The document shall contain:
1. For the installation budget
- a statement of the expenditure made by the French Contracting Party and, where appropriate, by the other Contracting Parties or the cooperating States pursuant to the provisions of Title II.3 of this Financial Regulation;
- the amount and breakdown of the contributions paid into the support fund (fonds de concours) by each State and, where appropriate, any outstanding amounts to be recovered.
2. For the operating budget
- a statement of expenditure made during the preceding financial year. This table shall indicate the deficit or surplus as compared with the adopted budget pursuant to Title III.1 of this Financial Regulation, so that the States may be charged or reimbursed the appropriate amounts;
- the amount and breakdown of the contributions paid into the support fund and, where appropriate, any amounts owing by the States.
The document shall be certified by a financial controller of the French Ministry of the Interior and sent to all the Contracting Parties and cooperating States by the Presidency-in-office.
The Executive Committee's approval of the said document shall constitute the final discharge of the accounts presented by the French Republic for the financial year in question. Approval shall be given during the first quarter of the year following the budgetary year in question.
A table showing the contributions of each State for the following financial year, calculated pursuant to Title I.1 of this Financial Regulation, shall be attached to the document.
If a State decides to pay its contributions partly or wholly in the form of an advance, the document shall indicate the outstanding balance following deduction of amounts owing for the budgetary year in question.
This decision shall enter into force once all the Contracting Parties to the Schengen Convention have given notification that the procedures required by their legal systems for these decisions to be binding on their territory have been completed.
