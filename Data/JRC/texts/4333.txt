Action brought on 20 October 2005 — GHK Consulting/Commission
Parties
Applicant(s): GHK Consulting Limited (London, United Kingdom) [represented by: J-E. Svensson, M. Dittmer, lawyers]
Defendant(s): Commission of the European Communities
Form of order sought
- Annul the European Commission's Decision of 12 October 2005 excluding the candidacy and the offer of the consortium headed by the applicant, whereby the Commission revoked its decision on allocating the framework contract to the consortium, in relation to Tender EuropeAid//119860/C/ — Lot No. 7;
- annul any decision by the Commission following the Commission's Decision of 12 October 2005 and, in particular, any decision by the Commission to enter into contract with other tenderers;
- order the Commission to pay all costs related to the case.
Pleas in law and main arguments
The Commission issued, under reference EuropeAid//119860/C — Lot No. 7, an invitation to tender for a multiple framework contract to recruit technical assistance for short-term expertise for the exclusive benefit of third countries benefiting from European Commission external aid. The applicant, acting as leader of a consortium, submitted a bid.
By the contested Decision the Commission excluded the applicant's consortium on the grounds that the Danish Institute of International Studies ("DIIS"), a member of the applicant's consortium, was part of the same group as the Danish Institute of Human Rights ("DIHR"), which participated in another consortium bidding for the same contract. Article 13 of the public procurement notice in question prohibited legal persons within the same legal group from submitting more than one application per lot.
In support of its request to annul the contested Decision the applicant disputes the Commission's conclusion that DIIS, DIHR and a third institute constitute a legal group. According to the applicant, none of these entities controls the others as the institutes are completely self-governing and each have a separate statute, share no academic staff, have their own management and board elected by completely different bodies, and share no common economic interests or objectives. The applicant further submits that any unclear points in the procurement notice must be interpreted in favour of the tenderers and that the Commission is responsible for not making the conditions for participation clear beforehand.
--------------------------------------------------
