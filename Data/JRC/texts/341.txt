Commission Regulation (EC) No 1575/2000
of 19 July 2000
implementing Council Regulation (EC) No 577/98 on the organisation of a labour force sample survey in the Community concerning the codification to be used for data transmission from 2001 onwards
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 577/98 of 9 March 1998 on the organisation of a labour force sample survey in the Community(1), as amended by Commission Regulation (EC) No 1571/98(2), and in particular Article 4(3) thereof,
Whereas:
(1) In accordance with Article 4(3) of Regulation (EC) No 577/98 implementing measures are necessary for defining the codification of the variables to be used for the data transmission.
(2) The measures provided for in this Regulation are in accordance with the opinion of the Statistical Programme Committee established by Council Decision 89/382/EEC, Euratom(3),
HAS ADOPTED THIS REGULATION:
Article 1
The codification of the variables to be used for the data transmission for the years 2001 and onwards is laid down in the Annex to the present Regulation.
Article 2
This Regulation shall enter into force on the twentieth day following that of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 19 July 2000.
For the Commission
Pedro Solbes Mira
Member of the Commission
(1) OJ L 77, 14.3.1998, p. 3.
(2) OJ L 205, 22.7.1998, p. 40.
(3) OJ L 181, 28.6.1989, p. 47.
ANNEX
>TABLE>
