Commission Regulation (EC) No 1707/2003
of 26 September 2003
fixing the coefficients applicable to cereals exported in the form of Scotch whisky for the period 2003/2004
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 1766/92 of 30 June 1992 on the common organisation of the market in cereals(1), as last amended by Regulation (EC) No 1104/2003(2),
Having regard to Commission Regulation (EEC) No 2825/93 of 15 October 1993 laying down certain detailed rules for the application of Council Regulation (EEC) No 1766/92 as regards the fixing and granting of adjusted refunds in respect of cereals exported in the form of certain spirit drinks(3), as last amended by Regulation (EC) No 1633/2000(4), and in particular Article 5 thereof,
Whereas:
(1) Article 4(1) of Regulation (EEC) No 2825/93 provides that the quantities of cereals eligible for the refund are to be the quantities placed under control and distilled, weighted by a coefficient to be fixed annually for each Member State concerned. That coefficient expresses the ratio between the total quantities exported and the total quantities marketed of the spirituous beverage concerned on the basis of the trend noted in those quantities during the number of years corresponding to the average ageing period of the spirituous beverage in question. In view of the information provided by the United Kingdom on the period 1 January to 31 December 2002, the average ageing period in 2002 was seven years for Scotch whisky. The coefficients for the period 1 October 2003 to 30 September 2004 should be fixed.
(2) Article 10 of Protocol 3 to the Agreement on the European Economic Area(5) precludes the grant of refunds in respect of exports to Liechtenstein, Iceland and Norway. Moreover, the Community has concluded with certain third countries agreements abolishing export refunds. According to Article 7(2) of Regulation (EEC) No 2825/93, this should be taken into account in the calculation of the coefficients for the period 2003/2004.
(3) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Cereals,
HAS ADOPTED THIS REGULATION:
Article 1
For the period 1 October 2003 to 30 September 2004 the coefficients provided for in Article 4 of Regulation (EEC) No 2825/93 applying to cereals used in the United Kingdom for manufacturing Scotch whisky shall be as set out in the Annex.
Article 2
This Regulation shall enter into force on the day of its publication in the Official Journal of the European Union.
It shall apply from 1 October 2003.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 26 September 2003.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 181, 1.7.1992, p. 21.
(2) OJ L 158, 27.6.2003, p. 1.
(3) OJ L 258, 16.10.1993, p. 6.
(4) OJ L 187, 26.7.2000, p. 29.
(5) OJ L 1, 3.1.1994, p. 1.
ANNEX
COEFFICIENTS APPLICABLE IN THE UNITED KINGDOM
>TABLE>
