COMMISSION DECISION
of 20 December 1994
laying down animal health conditons and veterinary certificates for the importation of fresh poultrymeat from certain third countries
(Text with EEA relevance)
(94/984/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/494/EEC of 26 June 1991 on animal health conditions governing intra-Community trade in and imports from third countries of fresh poultrymeat (1), as last amended by Directive 93/121/EC (2), and in particular Articles 11 and 12 thereof,
Whereas Commission Decision 94/85/EC (3), as last amended by Decision 94/453/EC (4), established a list of third countries from where importation of fresh poultrymeat is authorized;
Whereas Commission Decision 94/438/EC (5) has laid down the general requirements for the classification of third countries with regard to avian influenza and Newcastle disease in relation to imports of fresh poultrymeat;
Whereas it is appropriate to restrict the scope of this Decision to poultry species covered by Council Directive 71/118/EEC (6), as last amended and updated by Directive 92/116/EEC (7) and to lay down the animal health conditions and veterinary certification for other poultry species in a separate Decision;
Whereas therefore the animal health conditions and the veterinary certificates have to be laid down; whereas, since there are different groups of similar health situations between two or more third countries, it is appropriate to establish different health certificates in the light of those situations;
Whereas it is now possible, in accordance with information received from the third countries concerned and with the results of inspections carried out by the Commission services in some of these countries, to lay down two categories of certification;
Whereas the situation of the other third countries for which it is not yet possible to lay down a certificate is being studied attentively in order to see if they comply with the Community criteria or not; whereas this Decision shall be reviewed at the latest on 31 October 1995 to authorize or prohibit imports from those countries;
Whereas this Decision applies without prejudice to measures taken for poultrymeat imported for other purposes than human consumption;
Whereas, since a new certification scheme is being established, a period of time should be provided for its implementation;
Whereas the measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
Member States shall authorize the importation of fresh poultrymeat from third countries or from parts of third countries listed in Annex I, provided that it meets the requirements of the corresponding animal health certificate set out in Annex II and that it is accompanied by such a certificate, duly completed and signed. The certificate shall include the general part conforming to Annex II, Part 1, and one of the specific health certificates conforming to Annex II, Part 2, in accordance with the model referred to in Annex I.
Article 2
This Decision shall apply from 1 May 1995.
Article 3
This Decision shall be reviewed no later than 31 October 1995.
Article 4
This Decision is addressed to the Member States.
Done at Brussels, 20 December 1994.
For the Commission
René STEICHEN
Member of the Commission
(1) OJ No L 268, 24. 9. 1991, p. 35.
(2) OJ No L 340, 31. 12. 1993, p. 39.
(3) OJ No L 44, 17. 2. 1994, p. 31.
(4) OJ No L 187, 22. 7. 1994, p. 11.
(5) OJ No L 181, 15. 7. 1994, p. 35.
(6) OJ No L 55, 8. 3. 1971, p. 23.
(7) OJ No L 62, 15. 3. 1993, p. 1.
ANNEX I
Third countries or parts of third countries which are allowed to use the certificates laid down in Annex II for imports of fresh poultry meat into the Community
>TABLE POSITION>
ANNEX II
PART 1 ANIMAL HEALTH CERTIFICATE FOR FRESH POULTRYMEAT FOR HUMAN CONSUMPTION (1)
Note for the importer: This certificate is only for veterinary purposes and the original has to accompany the consignment until it reaches the border inspection post.
>START OF GRAPHIC>
1. Consigner (name and address in full):
4. Consignee (name and address in full):
8. Place of loading:
9.1. Means of transport (3):
9.2. Number of the seal (4): 10.1. Member State of destination:
10.2. Final destination: 12. Poultry species:
13. Nature of cuttings:
14. Consignment identification details:
Notes: A separate certificate must be provided for each consignment of fresh poultrymeat.
2. HEALTH CERTIFICATE No ORIGINAL 2.1. No of relevant public health certificate: 3.1. Country of origin: 3.2. Region of of origin (2):
5. COMPETENT AUTHORITY:
6. COMPETENT AUTHORITY (LOCAL LEVEL):
7. Address of establishment(s):
7.1. Slaughterhouse:
7.2. Cutting plant (5):
7.3. Cold store (5): 11. Approval number(s) of the establishment(s):
11.1. Slaughterhouse:
11.2. Cutting plant (5):
11.3. Cold store (5):
15. Quantity:
15.1. Net weight (kgs):
15.2. No of packages: (1) Fresh poultrymeat means any parts of domestic fowl, turkeys, guinea fowl, geese and ducks, which are fit for human consumption and which have not undergone any treatment other than cold treatment to ensure its preservation; vacuum wrapped meat or meat wrapped in a controlled atmosphere must also be accompanied by a certificate according to this model. (2) Only to be completed if the authorization to export to the Community is restricted to certain regions of the third country concerned. (3) Indicate means of transport and registration marks or registered name, as appropriate. (4) Optional. (5) Delete if not applicable. >END OF GRAPHIC>
PART 2
Model A
16. Health attestation:
I, the undersigned official veterinarian, hereby certify, in accordance with the provisions of Directive 91/494/EEC:
1. that . . . . . . . . . . . . (1), region . . . . . . . . . . . . (2) is free from avian influenza and Newcastle disease, as defined in the International Animal Health Code of OIE;
2. that the meat described above is obtained from poultry which:
(a) have been held in the territory of . . . . . . . . . . . . (1), region . . . . . . . . . . . . (2) since hatching or have been imported as day-old chicks;
(b) came from holdings:
- which have not been placed under animal health restrictions in connection with a poultry disease,
- around which, within a radius of 10 km, there have been no outbreaks of avian influenza or Newcastle disease for at least 30 days;
(c) have not be slaughtered in the context of any animal health scheme for the control or eradication of poultry diseases;
(d) have/have not (3) been vaccinated against Newcastle disease using a live vaccine during the 30 days preceding slaughter;
(e) during transport to the slaughterhouse did not come into contact with poultry suffering from avian influenza or Newcastle disease;
3. that the meat described above:
(a) comes from slaughterhouses which, at the time of slaughter, are not under restrictions due to a suspect or actual outbreak of avian influenza or Newcastle disease and around which, within a radius of 10 km, there have been no outbreaks of avian influenza or Newcastle disease for at least 30 days;
(b) has not been in contact, at any time of slaughter, cutting, storage or transport with meat which does not fulfil the requirements of Directive 91/494/EEC;
Done at . on . Seal (4) . (signature of official veterinarian) (4) . (name in capital letters, qualifications and title)
Model B
16. Health attestation:
I, the undersigned official veterinarian, hereby certify, in accordance with the provisions of Directive 91/494/EEC:
1. that . . . . . . . . . . . . (1), region . . . . . . . . . . . . (2) is free from avian influenza and Newcastle disease, as defined in the International Animal Health Code of OIE;
2. that the meat described above is obtained from poultry which:
(a) have been held in the territory of . . . . . . . . . . . . (1), region . . . . . . . . . . . . (2), since hatching or have been imported as day-old chicks;
(b) come from holdings:
- which have not been placed under animal health restrictions in connection with a poultry disease,
- around which, within a radius of 10 km, there have been no outbreaks of avian influenza or Newcastle disease for at least 30 days;
(c) have not been slaughtered in the context of any animal health scheme for the control or eradication of poultry diseases;
(d) have/have not (3) been vaccinated against Newcastle disease using a live vaccine during the 30 days preceding slaughter;
(e) during transport to the slaughterhouse did not come into contact with poultry suffering from avian influenza or Newcastle disease;
3. that the commercial slaughter poultry flock from which the meat is issued,
(a) has not been vaccinated with vaccines prepared from a Newcastle disease virus Master Seed which shows a higher pathogenicity than lentogenic strains of the virus; and
(b) has undergone at slaughter, on the basis of an at random sample of cloacal swabs of at least 60 birds of each flock concerned, a virus isolation test for Newcastle disease, carried out in an official laboratory, in which no avian paramyxoviruses with an Intracerebral Pathogenicity Index (ICPI) of more than 0,4 have been found; and
(c) has not been in contact during the period of 30 days preceding slaughter with poultry which do not fulfil the guarantees mentioned under (a) and (b).
4. that the meat described above:
(a) comes from slaughterhouses which, at the time of slaughter, are not under resrictions due to a suspect or actual outbreak of avian influenza or Newcastle disease and around which, within a radius of 10 km, there have been no outbreaks of avian influenza of Newcastle disease for at least 30 days;
(b) has not been in contact, at any time of slaughter, cutting, storage or transport with meat which does not fulfil the requirements of Directive 91/494/EEC;
Done at . on . Seal (4) . (signature of official veterinarian) (4) . (name in capital letters, qualifications and title)
(1) Name of the country of origin.
(2) Only to be completed if the authorization to export to the Community is restricted to certain regions of the third country concerned.
(3) Delete the unneccessary reference. If the poultry have been vaccinated within 30 days before slaughter, the consignment cannot be sent to Member States or regions thereof which have been recognized in accordance with Article 12 of Council Directive 90/539/EEC (currently Denmark, Ireland and, in the United Kingdom, Northern Ireland).
(4) Stamp and signature in a colour different to that of the printing.
(1) Name of the country of origin.
(2) Only to be completed if the authorization to export to the Community is restricted to certain regions of the third country concerned.
(3) Delete the unnecessary reference. If the poultry have been vaccinated within 30 days before slaughter, the consignment cannot be sent to Member States or regions thereof which have been recognized in accordance with Article 12 of Directive 90/539/EEC (Currently Denmark, Ireland and, in the United Kingdom, Northern Ireland).
(4) Stamp and signature in a colour different to that of the printing.
