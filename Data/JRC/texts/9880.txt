Commission Regulation (EC) No 469/2006
of 22 March 2006
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 23 March 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 22 March 2006.
For the Commission
J. L. Demarty
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 386/2005 (OJ L 62, 9.3.2005, p. 3).
--------------------------------------------------
ANNEX
to Commission Regulation of 22 March 2006 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code [1] | Standard import value |
07020000 | 052 | 94,2 |
204 | 53,3 |
212 | 102,0 |
624 | 101,8 |
999 | 87,8 |
07070005 | 052 | 134,8 |
999 | 134,8 |
07091000 | 624 | 103,6 |
999 | 103,6 |
07099070 | 052 | 110,8 |
204 | 48,5 |
999 | 79,7 |
08051020 | 052 | 71,7 |
204 | 42,8 |
212 | 53,8 |
220 | 43,2 |
400 | 60,8 |
448 | 37,8 |
624 | 57,1 |
999 | 52,5 |
08055010 | 052 | 42,2 |
624 | 73,8 |
999 | 58,0 |
08081080 | 388 | 77,0 |
400 | 121,8 |
404 | 103,9 |
508 | 82,7 |
512 | 84,8 |
524 | 62,5 |
528 | 88,1 |
720 | 82,6 |
999 | 87,9 |
08082050 | 388 | 80,8 |
512 | 73,3 |
524 | 58,2 |
528 | 83,8 |
720 | 122,5 |
999 | 83,7 |
[1] Country nomenclature as fixed by Commission Regulation (EC) No 750/2005 (OJ L 126, 19.5.2005, p. 12). Code "999" stands for "of other origin".
--------------------------------------------------
