Commission Regulation (EC) No 1102/2006
of 17 July 2006
determining to what extent import right applications submitted during the month of June 2006 for certain live bovine animals as part of a tariff quota provided for in Regulation (EC) No 1241/2005 may be accepted
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1254/1999 of 17 May 1999 on the common organisation of the market in beef and veal [1],
Having regard to Commission Regulation (EC) No 1241/2005 of 29 July 2005 laying down detailed rules for the application of a tariff quota for certain live bovine animals originating in Romania, provided for in Council Decision 2003/18/EC [2], and in particular Article 4 thereof,
Whereas:
(1) Article 1 of Regulation (EC) No 1241/2005 fixes at 46000 the number of head of live bovine animals originating in Romania which may be imported under special conditions in the period 1 July 2006 to 30 June 2007.
(2) Article 4(2) of Regulation (EC) No 1241/2005 lays down that the quantities applied for may be reduced. The applications lodged relate to total quantities which exceed the quantities available. Under these circumstances and taking care to ensure an equitable distribution of the available quantities, it is appropriate to reduce proportionally the quantities applied for,
HAS ADOPTED THIS REGULATION:
Article 1
All applications for import certificates lodged pursuant to Article 3(3) of Regulation (EC) No 1241/2005 shall be accepted at a rate of 7,664 % of the import rights applied for.
Article 2
This Regulation shall enter into force on 18 July 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 17 July 2006.
For the Commission
Jean-Luc Demarty
Director-General for Agriculture and Rural Development
[1] OJ L 160, 26.6.1999, p. 21. Regulation as last amended by Regulation (EC) No 1782/2003 (OJ L 270, 21.10.2003, p. 1).
[2] OJ L 200, 30.7.2005, p. 38.
--------------------------------------------------
