State aid — Germany
State aid No C 24/06 (ex NN 75/2000) — Animal Health Service
Invitation to submit comments pursuant to Article 88(2) of the EC Treaty
(2006/C 244/05)
By letter dated 7.7.2006 reproduced in the authentic language on the pages following this summary, the Commission notified Germany of its decision to initiate the procedure laid down in Article 88(2) of the EC Treaty concerning the abovementioned aid/measure.
Interested parties may submit their comments on the aid/measure in respect of which the Commission is initiating the procedure within one month of the date of publication of this summary and the following letter to:
European Commission
Directorate-General for Agriculture and Rural Development
Directorate H
Rue de la Loi/Wetstraat 200
B-1049 Brussels
Fax No: (32-2) 296 21 51
These comments will be communicated to Germany. Confidential treatment of the identity of the interested party submitting the comments may be requested in writing, stating the reasons for the request.
TEXT OF SUMMARY
PROCEDURE
By letter of 21 February 2000 a complaint was lodged with the Commission regarding measures taken by the Bavarian Animal Health Service (Tiergesundheitsdienst Bayern). Further to that complaint, the Directorate-General for Agriculture wrote several letters to the Federal Republic of Germany in response to which additional information was received from the German authorities by letters of 4 July 2000, 22 December 2000, 22 November 2002, 10 April 2003, 1 December 2003 and 27 June 2005.
A meeting with representatives from the German authorities was held on 17 July 2003.
The measures had been taken since 1974. Despite being asked, it was not possible to furnish proof of the measure having been notified. The aid was therefore transferred to the register of unnotified aid.
DESCRIPTION OF THE MEASURES IN RESPECT OF WHICH THE COMMISSION IS INITIATING THE PROCEDURE
Legal basis : Article 14(1) of the Law on support for Bavarian agriculture (Gesetz zur Förderung der bayerischen Landwirtschaft — LwFöG).
Purpose of the measure : To safeguard and improve the hygiene of foodstuffs originating from animals.
Recipients : Farmers and possibly also the Bavarian Animal Health Service.
Form and scope of the aid a) For the benefit of farmers:
Measures of a precautionary nature relating to the production of milk and meat and to the rearing of pigs, poultry, sheep and fish.
Measures of the following types have been taken:
- continuous monitoring by means of tests and/or precautionary examinations;
- sampling and (laboratory/serial) analyses;
- veterinary advice;
- drawing up prevention and/or eradication plans;
- developing vaccination programmes.
These so-called general measures are free of charge for farmers.
b) Possibly for the benefit of the Bavarian Animal Health Service:
The Commission has no evidence whatsoever that in the past in each case only 100 % and not possibly more of the actual (and necessary) expenditure has been reimbursed.
Duration of the measure : Unlimited
COMPLAINTS LODGED AGAINST THE MEASURE
The complainant states that, through the veterinary surgeons which it employs, the Bavarian Animal Health Service not only takes precautionary measures but also provides veterinary care services.
According to the complainant, this distorts competition because the support provided for the "general measures" enables the Bavarian Animal Health Service to offer competitive (i.e. care) services more cheaply.
ASSESSMENT OF THE AID/MEASURE
In the Commission's opinion, the farmers are benefiting from aid within the meaning of Article 87(1) of the EC Treaty. That aid may possibly be compatible with the common market under Article 87(3)(c) of the EC Treaty if, in particular, points 11.2 and 11.4 of the Community guidelines for state aid in the agriculture sector has been complied with. In order to assess this, however, the Commission still needs certain further information from the German authorities.
The Bavarian Animal Health Service might also be receiving aid within the meaning of Article 87(1) of the EC Treaty if the costs incurred by that Service in carrying out the general measures were being overcompensated by budgetary resources from the Land Bavaria and its animal diseases fund. Up until 2004 the Bavarian Animal Health Service was not selected by means of a procedure for awarding public contracts. Under these circumstances, the Commission does not rule out the possibility of there being operating aid which is incompatible with the common market. There is a risk that the Bavarian Animal Health Service's independent economic activity may be receiving cross-subsidies.
The Commission therefore calls upon the German authorities to submit accounts for each financial year showing the costs and their reimbursement, so that it can assess whether overcompensation and/or cross-subsidy might be involved.
Since financing through mandatory contributions to the animal diseases fund is also involved, the Commission likewise needs all relevant information on how resources are provided for that fund.
In accordance with Article 14 of Council Regulation (EC) No 659/1999, all unlawful aid may be subject to recovery from the recipient. Furthermore, expenditure on national measures directly affecting Community measures may not be eligible for cover by the EAGGF budget.
TEXT OF LETTER
"Ich beehre mich, Ihnen mitzuteilen, dass die Kommission nach Prüfung der von Ihren Behörden übermittelten Informationen zu den im Betreff genannten Maßnahmen beschlossen hat, das Verfahren gemäß Artikel 88 Absatz 2 EG-Vertrag einzuleiten.
Der Entscheidung der Kommission liegen folgende Erwägungen zugrunde:
I. VERFAHREN
Der Kommission ist mit Brief vom 21.2.2000 eine Beschwerde hins. von Maßnahmen des Tiergesundheitsdienstes Bayern (im folgenden: "TGD") vorgetragen worden. Es folgten von demselben Beschwerdeführer weitere Schreiben mit der selben Beschwer.
Die Generaldirektion Landwirtschaft hat sich im Anschluß daran mit mehreren Schreiben an die Bundesrepublik Deutschland gewendet, woraufhin ergänzende Informationen mit Schreiben der deutschen Behörden vom 4.7.2000, 22.12.2000, 22.11.2002, 10.4.2003, 1.12.2003, sowie vom 27.6.2005 eingegangen sind.
Eine Sitzung mit Vertretern der deutschen Behörden hat am 17.7.2003 stattgefunden.
Die Maßnahmen wurden seit 1974 ergriffen. Trotz Nachfrage konnte eine Notifizierung der Maßnahme nicht nachgewiesen werden. Die Beihilfe wurde daher in das Verzeichnis der nicht notifizierten Beihilfen übertragen.
II. BESCHREIBUNG
1. Rechtsgrundlage
Art. 14 Abs. 1 Gesetz zur Förderung der bayerischen Landwirtschaft (im folgenden: "LwFöG")
2. Ziel der Maßnahme
Sicherung und Verbesserung der hygienischen Wertigkeit der vom Tier stammenden Nahrungsmittel
3. Begünstigte
a) Die Landwirte
b) Möglicherweise auch der TGD e.V.
4. Art und Ausmaß der Beihilfe
Die Maßnahme wird aus Mitteln des Landes Bayern finanziert.
a) zugunsten der Landwirte:
Obwohl als "Globalmaßnahmen" bezeichnet, handelt es sich um Maßnahmen, die lediglich den in Bayern ansässigen Bauern zugute kommen, zudem handelt es sich nach Angaben der Behörden um ganz bestimmte Maßnahmen mit lediglich vorsorglichen Charakter. Dies betrifft die Milch- und Fleischerzeugung, die Schweinezucht, die Geflügel- und Schafhaltung, sowie die Fischzucht.
Folgende typisierte Maßnahmen auf landwirtschaftlichen Betrieben wurden getroffen:
- lfde. Überwachung durch Tests bzw. Vorsorgeuntersuchungen
- Erhebungen und (Labor-/Reihen-)Untersuchungen
- tierärztliche Beratung, sowie zusätzlich
- Erarbeitung von Plänen zur Prophylaxe bzw. Sanierungspläne
- Entwicklung von Impfprogrammen
Diese sog. Globalmaßnahmen sind für die Bauern kostenfrei. Sie können diese Dienstleistung aber nicht anfordern, sondern der mit diesen Maßnahmen beauftragte TGD ergreift sie aus eigener Initiative.
Die Behörden des Freistaates Bayern haben nach mehrmaligen Nachfragen von Seiten der Kommission in Ihrem Schreiben vom 22.11.2002, S. 7, des weiteren mitgeteilt:
"Kurative Leistungen, d.h. Leistungen, die üblicherweise von niedergelassenen Tierärzten angeboten werden (z.B. Behandlungen mit Arzneimitteln oder Schutzimpfungen) finden nicht statt."
Die Kosten dieser Maßnahmen werden dem TGD durch das Land ersetzt. Nach Art. 14 Abs. Satz 2 LwFöG werden nur "staatliche Leistungen von 50 v.H. des notwendigen Aufwandes gewährt".
In Wirklichkeit werden nochmals 50 % der Personal- und Sachkosten des TGD aus anderen staatlichen Mitteln, nämlich der Tierseuchenkasse, ersetzt. Zusammen macht das eine Kostenerstattung von 100 %.
b) möglicherweise zugunsten des TGD:
Nach dem Wortlaut des Gesetzes wird dem TGD lediglich die Hälfte des "notwendigen Aufwandes" aus staatlichen Mitteln ersetzt. Tatsächlich kann, nach Angaben der Behörden, aber ein Ersatz in Höhe von 100 % erlangt werden.
Die Kommission hat keinerlei Evidenz darüber, dass in der Vergangenheit in jedem Falle lediglich 100 % und nicht etwa mehr der effektiven (und notwendigen) Auslagen ersetzt worden sind.
Die deutschen Behörden haben der Kommission keine gesicherte Übersicht über die getätigten Ausgaben für die Finanzierung der genannten Maßnahmen übermittelt. Sie werden gebeten, dies für die Geschäftsjahre ab 1990 [1] zu tun.
5. Dauer der Maßnahme
unbegrenzt
6. Gegen die Maßnahme vorliegende Beschwerden
Der Beschwerdeführer trägt vor, dass der TGD mit seinen angestellten Tierärzten nicht nur Vorsorgemaßnahmen trifft, sondern auch kurative Dienstleistungen erbringt.
Er macht zwar nicht geltend, dass diese direkt subventioniert würden. Gleichwohl sei aber eine wettbewerbsverzerrende Situation gegeben, insoweit nämlich, als durch die Subventionierung der "Globalmaßnahmen" es dem TGD ermöglicht würde, die wettbewerblichen (d.h.kurativen) Leistungen günstiger anzubieten. Des weiteren weist er auf die günstige Vertragsabschlußopportunität der bereits am Ort (vorsorglich) wirkenden angestellten Tierärzte des TGD. Das sei für ihn und andere frei praktizierende Tierärzte existenzbedrohend.
Im übrigen meint der Beschwerdeführer, dass die Beihilfen zugunsten der Landwirte nicht mit dem Gemeinschaftsrahmen für staatliche Beihilfen im Agrarsektor (Abl. C 28/2 vom 1.2.2000) im Einklang stünden.
III. BEWERTUNG
1. Vorliegen einer Beihilfe gem. Art. 87 Abs. 1 EG-Vertrag
Gemäß Artikel 87 Absatz 1 EG-Vertrag sind staatliche oder aus staatlichen Mitteln gleich welcher Art gewährte Beihilfen, die durch die Begünstigung bestimmter Unternehmen oder Produktionszweige den Wettbewerb verfälschen oder zu verfälschen drohen, verboten, soweit sie den Handel zwischen Mitgliedstaaten beeinträchtigen.
Die vorliegende Beihilfemaßnahme wird aus staatlichen Mitteln finanziert.
Was die Landwirte betrifft, so erleichtern die kostenfreien Globalmaßnahmen deren Produktion und stellen somit eine Begünstigung dar.
Was den TGD anlangt, so kann die Kommission nach dem derzeitigen Verfahrensstand nicht ausschließen, dass er durch die Budgetzahlungen und die Zahlungen durch die Tierseuchenkasse begünstigt ist.
Wenn es sich bei den Zahlungen aus dem Haushalt des Bundeslandes Bayern und seiner Tierseuchenkasse, wie von den deutschen Behörden vorgetragen, lediglich um Ausgleichszahlungen für die Erbringung öffentlicher Dienstleistungen handelt, so sind dies nach der Rechtsprechung des Gerichtshofs der Europäischen Gemeinschaften [2] nur dann keine Beihilfen i.S.d Art. 87 Abs. 1 EGV, wenn sie bestimmte Voraussetzungen erfüllen:
a) Das begünstigte Unternehmen muß tatsächlich mit der Erfüllung gemeinwirtschaftlicher Verpflichtungen betraut sein. Diese Verpflichtungen müssen klar definiert sein.
b) Die Parameter, anhand deren der Ausgleich berechnet wird, müssen zuvor objektiv und transparent aufgestellt worden sein, um zu verhindern, dass der Ausgleich einen wirtschaftlichen Vorteil mit sich bringt, der das Unternehmen gegenüber konkurrierenden Unternehmen begünstigt.
c) Der Ausgleich darf nicht über die Kosten der Erfüllung dieser gemeinwirtschaftlichen Verpflichtungen hinausgehen, unter Berücksichtigung der dabei erzielten Einnahmen und eines angemessenen Gewinns.
d) Wird die Auswahl des Unternehmens nicht im Rahmen eines Verfahrens zur Vergabe öffentlicher Aufträge vorgenommen, ist die Höhe des erforderlichen Ausgleichs auf der Grundlage einer Analyse der Kosten zu bestimmen.
Diese Bedingungen müssen kumulativ vorliegen. Sollte auch nur eine dieser Bedingungen nicht erfüllt sein, kann das Vorliegen einer Begünstigung nicht ausgeschlossen werden.
Zum gegenwärtigen Zeitpunkt scheinen die genannten Bedingungen nicht vollumfänglich erfüllt zu sein:
Für Bedingung d) gilt, dass sich die bayerischen Behörden erst im Zuge der Verhandlungen mit der Kommission im Jahr 2004 entschließen konnten, den Zusammenschluß i.S.d. Art. 14 LwFöG zur Ausführung der sog. Globalmaßnahmen mittels eines Verfahrens zur Vergabe öffentlicher Aufträge auszuwählen.
Da der erforderliche Ausgleich auch nicht auf Grundlage einer Kostenanalyse erfolgt bestimmt worden ist, ist schon Bedingung d) nicht erfüllt.
Daraus folgt, dass die Kommission nach der Rechtsprechung im Fall Altmark das Vorliegen einer Begünstigung i.S.d. Art. 87 EGV nicht ausschließen kann.
Der Kommission liegen aber auch Indizien tatsächlicher Art vor:
Der Beschwerdeführer trägt vor, dass gewisse klinisch-diagnostische und therapeutische Leistungen der angestellten Tierärzte des TGD bis zu 90 % billiger als die Gestehungskosten angeboten würden.
Dies ist der Bereich, in dem der TGD eigenwirtschaftlich handelt.
Dies sei, so der Beschwerdeführer, nur möglich, weil der TGD Subventionen für die sog. Globalmaßnahmen (nicht-konkurrentielles Handeln) bekommt und ihn damit in eine Lage versetzt, die klinisch-diagnostischen bzw. therapeutischen Dienstleistungen wesentlich günstiger anzubieten, als es den frei praktizierenden Tierärzten, die ohne Subventionen auskommen müssen, möglich sei.
In Anbetracht dieser Rechts- und Sachlage, kommt die Kommission nicht umhin vorläufig zu schließen, dass bis zum gegenwärtigen Zeitpunkt die Gefahr der faktischen Quersubventionierung des eigenwirtschaftlichen Handelns des TGD, welches durch die Subventionierung der sog. Globalmaßnahmen ermöglicht erscheint, besteht.
Darüber hinaus ist bemerkenswert, dass dahingehende Klagen noch formuliert worden sind, nachdem die bayerischen Behörden ein System der rechnungsmäßigen Trennung des geförderten und eigenwirtschaftliche Geschäftsbereichs analog der Transparenzrichtlinie 2000/52/EG im Jahr 2002 eingeführt haben.
Nach all dem fordert die Kommission die deutschen Behörden auf, für die Geschäftsjahre bis einschließlich 2004 eine Kostenanalyse als Grundlage des erforderlichen Ausgleichs zu erstellen und den tatsächlichen gezahlten Ausgleich aus den staatlichen Mitteln (Landeshaushalt sowie Tierseuchenkasse) zu beziffern.
Der Vergleich der Lasten des TGD mit seinen Einnahmen sollte der Kommission dann die Feststellung erlauben, ob eine Überkompensation vorliegt oder nicht, und ob eine etwaige Quersubventionierung des eigenwirtschaftlichen Handelns des TGD zu befürchten ist.
Die Landwirtschaftsbetriebe und der TGD, soweit er (eigen-) wirtschaftlich handelt, sind Unternehmen i.S.d. Art. 87 EGV.
Die Maßnahmen begünstigen die bayerischen Landwirte in direkter Weise und den TGD möglicherweise in indirekter Weise.
Die Landwirtschaft im Land Bayern ist für sich allein ein großer Hersteller von Agrarprodukten, und darüber hinaus intensiv im grenzüberschreitenden Handel mit Agrarprodukten tätig. Beim vorliegenden Stand der Informationen ist die Maßnahme somit geeignet, den Wettbewerb zu verfälschen [3] und den Handel mit Agrarprodukten zwischen Mitgliedstaaten zu beeinträchtigen [4].
Schlussfolgerung:
Die Kommission ist daher beim derzeitigen Stand des Verfahrens der Auffassung, dass den Landwirten Beihilfen gewährt werden und möglicherweise auch dem TGD, und somit der Tatbestand des Artikels 87 Absatz 1 EG-Vertrag erfüllt ist, weil diese Beihilfen durch die Begünstigung bestimmter Unternehmen und Produktionszweige den Wettbewerb verfälschen oder zu verfälschen drohen und den Handel zwischen Mitgliedstaaten beeinträchtigen.
2. Anwendbarkeit von Artikel 87 Absatz 2 und 3 EG-Vertrag
Es ist daher zu prüfen, ob eine der Ausnahmen bzw. Freistellungen von dem grundsätzlichen Beihilfeverbot gemäß Artikel 87 Absatz 1 EG-Vertrag zur Anwendung kommen kann.
Die Ausnahmetatbestände der Artikel 87 Absatz 2 und Artikel 87 Absatz 3 Buchstaben a, b und d scheinen aus derzeitiger Sicht nicht anwendbar zu sein, da es sich weder um
- Beihilfen zur Förderung der wirtschaftlichen Entwicklung von Gebieten, in denen die Lebenshaltung außergewöhnlichen niedrig ist oder eine erhebliche Unterbeschäftigung herrscht, noch um
- Beihilfen zur Förderung wichtiger Vorhaben von gemeinsamem europäischen Interesse oder zur Behebung einer beträchtlichen Störung im Wirtschaftsleben eines Mitgliedstaates oder
- Beihilfen zur Förderung der Kultur und der Erhaltung des kulturellen Erbes, soweit sie die Handels- und Wettbewerbsbedingungen in der Gemeinschaft nicht in einem Maß beeinträchtigen, das dem gemeinsamen Interesse zuwiderläuft, handelt.
Einzig der Ausnahmetatbestand unter Artikel 87 Absatz 3 Buchstabe c) könnte Anwendung finden
a) die Beihilfen an die Landwirte
Gemäß Art. 20 Abs. 2 Unterabs. 2 i.V.m. Art. 1 Abs. 1 unter Berücksichtigung von Art. 2 Ziff. 4 der Verordnung (EG) Nr. 1/2004 der Kommission [5] kommt, da diese Maßnahmen nicht auf kleine und mittlere Unternehmen begrenzt sind, keine Freistellungsmöglichkeit von der Anmeldepflicht nach Art. 3 Abs. 2 aufgrund Vereinbarkeit mit dem Gemeinsamen Markt in Betracht.
Soweit ersichtlich, könnten für die Beihilfen ab 1.1.2004 die gewährten Beihilfen allenfalls auf der Grundlage von Kapitel 11.4 des Gemeinschaftsrahmens für staatliche Beihilfen im Agrarsektor [6] genehmigt werden. Für die älteren Beihilfen ist Anwendungsmaßstab das Arbeitsdokument Nr. VI/5934/86 der Kommission vom 10.11.1986 [7]. Die dort formulierte Politik ist, was die gegenständlichen Maßnahmen betrifft, identisch mit den Bestimmungen des Gemeinschaftsrahmens, weswegen die Bewertung sich an den Punkten des Gemeinschaftsrahmens orientieren wird.
Ziff. 11.4.2. zu Folge ist ein Warnsystem einzurichten, gegebenenfalls verbunden mit Beihilfen, um die einzelnen Betroffenen zur freiwilligen Teilnahme an präventiven Maßnahme zu bewegen. Dementsprechend können nur Seuchen, deren Bekämpfung im Interesse der Behörden liegt, Gegenstand von Beihilfemaßnahmen sein, und nicht etwa Maßnahmen für die die Landwirte nach allg. Ermessen selbst die Verantwortung zu übernehmen haben.
Ein solches Warnsystem scheint zwar ab dem Jahr 2002 lediglich für bestimmte Krankheiten bzw. Krankheiterreger errichtet worden zu sein, wofür auch in vielen Fällen das entspr. Bekämpfungsinteresse der Behörden (veterinärfachliches Konzept des zuständigen Ministeriums) dargetan ist. Für andere Krankheiten bzw. Krankheiterreger ist ein solches Warnsystem bzw. ein Bekämpfungsinteresse nicht vollständig nachgewiesen worden. Darum werden die deutschen Behörden ersucht, die entsprechenden Nachweise vorzulegen, insbes. was den Zeitraum vor 2002 anlangt.
Die Kommission nimmt die Beteuerung der deutschen Behörden zur Kenntnis, dass es sich bei den sog. Globalmaßnahmen nicht um Maßnahmen handelt, die nach allg. Ermessen von den Landwirten zu verantworten sind. Auch hat der Beschwerdeführer dies nicht vorgetragen.
Diese sog. Globalmaßnahmen erscheinen in der Regel die Charakteristiken von Vorsorgemaßnahmen zu haben und würden somit auch die Bedingungen unter Ziff. 11.4.3. erster Anstrich erfüllen. Diese Schlussfolgerung kann zum gegenwärtigen Zeitpunkt (noch) nicht insbesondere für die Sanierungspläne getroffen werden. Die deutschen Behörden müssten darlegen, inwiefern solche Maßnahmen Präventivcharakter haben können, wie von ihnen geltendgemacht.
Die deutschen Behörden sollten für jede einzelne Krankheit bzw. zu bekämpfenden Erreger nachweisen, dass sie mit dem gemeinschaftlichen Veterinärrecht in Einklang stehen (Ziff. 11.4.4.).
Gemäß Ziff. 11.4.5. ist die völlige Kostenfreiheit zulässig. Auch ist die Gefahr einer Kumulierung mit anderen Beihilfen nicht gegeben.
Die Kommission weist darauf hin, dass gem. Ziff. 11.4.5. keine Beihilfen gewährt werden dürfen, wenn das Gemeinschaftsrecht vorsieht, dass diese Kosten von den Landwirten selbst zu tragen sind.
Dieser Punkt müsste von den bayerischen Behörden ebenfalls nachgewiesen werden.
In Folge der Rechtssprechung des Gerichtshofs der Europäischen Gemeinschaften [8] kann eine Beihilfe, die im eigentlichen Sinne als zulässig anerkannt werden kann, durch die Art der Finanzierung ihre störende Wirkung auf dem Gemeinsamen Markt dergestalt verstärken und dem gemeinsamen Interesse widersprechen, dass die gesamte Regelung als mit dem Gemeinsamen Markt unvereinbar anzusehen ist.
Um dies beurteilen zu können müsste die Kommission alle sachdienlichen Informationen darüber erhalten, wie sich die Tierseuchenkasse, aus der die restlichen 50 % der Beihilfe finanziert werden, speist.
In Bezug auf Wirtschaftsbeteiligte, die in der Aufzucht oder Haltung von Wasserorganismen tätig sind und die Beihilfe erhalten, sei darauf hingewiesen, dass alle Staatlichen Beihilfen im Fischerei- und Aquakultursektor anhand der Leitlinien für die Prüfung Staatlicher Beihilfen im Fischerei- und Aquakultursektor [9] bewertet werden müssen.
Im Gegensatz zu den Leitlinien von 2001 [10] enthalten die derzeitigen Leitlinien keine besonderen Bestimmungen über Beihilfen im tierärztlichen und gesundheitlichen Bereich.
Gemäß Abschnitt 3.10 der Leitlinien gilt Folgendes: Beihilfen für Maßnahmenkategorien im Rahmen der Verordnung (EG) Nr. 1595/2004, die anderen Unternehmen als KMU dienen sollen oder den Höchstbetrag nach Artikel 1 Absatz 3 der Verordnung überschreiten, werden anhand dieser Leitlinien und der Kriterien für die Maßnahmenkategorien nach den Artikeln 4 bis 13 der Verordnung geprüft.
Nach der Verordnung (EG) Nr. 1595/2004 dürfen Beihilfen für den Aquakultursektor genehmigt werden, aber nur, wenn es sich dabei um Investitionsbeihilfen handelt. Da es sich bei der vorliegenden Beihilfe anscheinend nicht um eine solche Investitionsbeihilfe handelt, fällt sie nicht in den Anwendungsbereich von Abschnitt 3.10 der Leitlinien.
Bei Beihilfemaßnahmen, die nicht in den Geltungsbereich der Leitlinien oder der Verordnung (EG) Nr. 1595/2004 fallen, bewertet die Kommission gemäß Abschnitt 3.2 der Leitlinien die Beihilfen einzeln, und zwar unter Berücksichtigung der in den Artikeln 87, 88 und 89 EG-Vertrag festgelegten Grundsätze sowie der gemeinsamen Fischereipolitik.
Da die Beihilfe den Begünstigten anscheinend keine Verpflichtung hinsichtlich der Erreichung der Ziele der gemeinsamen Fischereipolitik auferlegt und die Situation solcher Unternehmen verbessert, scheint es, daß die Beihilfemaßnahme, bei der es sich um eine Betriebsbeihilfe handelt, nicht als eine mit den allgemeinen Grundsätzen für Staatliche Beihilfen und im Besonderen mit den Grundsätzen für Staatliche Beihilfen im Fischereisektor gemäß Abschnitt 3.7 der Leitlinien zu vereinbarende Beihilfe betrachtet werden.
In dieser Phase der Untersuchungen hat die Kommission Zweifel hinsichtlich der Vereinbarkeit der Beihilfemaßnahmen mit dem Gemeinsamen Markt.
Für den Zeitraum vor dem 1. Januar 2005 sind nach Abschnitt 2.8 der Leitlinien von 2001 und nach Abschnitt 2.9 der Leitlinien von 1997 und von 1994 [11] Beihilfen im tierärztlichen und gesundheitlichen Bereich gestattet. Die Beihilfe im Rahmen dieser Regelung ist mit diesen Bedingungen vereinbar und daher kann eine Beihilfe, die nach dieser Regelung Wirtschaftsbeteiligten des Fischerei- und Aquakultursektors vor dem 1. Januar 2005 gewährt worden ist, als mit dem Gemeinsamen Markt vereinbar betrachtet werden.
b) die mögliche Beihilfe an den TGD
Es ist zum derzeitigen Zeitpunkt, insbesondere wegen des Fehlens einer Auswahl mittels eines Verfahrens zur Vergabe öffentlicher Aufträge bis zum Jahr 2000, nicht klar, ob und in welchem Ausmaß die Subventionen des Landes Bayern die dem TGD effektiv entstandenen Kosten durch die Ausführung der sog. Globalmaßnahmen überkompensieren und damit einen Zuschuss zu Betriebskosten darstellen. Es ist der Kommission ebenso wenig klar, ob diese etwaigen Zuschüsse in den Anwendungsbereich der Verordnung (EG) Nr. 69/2001 vom 12. Januar 2001 über die Anwendung der Artikel 87 und 88 EG-Vertrag auf "de-minimis"-Beihilfen [12] fallen.
Um die Frage klären zu können, ob diese Beihilfen als Betriebsbeihilfen zu gelten haben, die mit dem Gemeinsamen Markt unvereinbar sind [13], müssten die deutschen Behörden für Berechnungen vorlegen, aus denen unzweideutig hervorgeht, ob es eine Überkompensation gegeben hat, und falls ja, in welcher Höhe.
Gemäß Artikel 15 Absatz 1 der Verordnung (EG) Nr. 659/99 [14] gelten die Befugnisse der Kommission zur Rückforderung von Beihilfen für eine Frist von zehn Jahren. Aus diesen Gründen bittet die Kommission um die Vorlage dieser Berechnungen seit dem Geschäftsjahr 1990. Darüber hinaus fordert die Kommission die deutschen Behörden auf, alle sonstigen Informationen, die für eine Beurteilung dieser Maßnahme notwendig sind, vorzulegen.
Schlussfolgerung
Zum gegenwärtigen Zeitpunkt kann die Europäische Kommission ernsthafte Bedenken betreffend der Vereinbarkeit der gegenständlichen Beihilfe mit dem Gemeinsamen Markt nicht ausräumen.
IV. BESCHLUSS
Aus den oben dargelegten Gründen fordert die Kommission Deutschland gemäß dem Verfahren nach Artikel 88 Absatz 2 EG-Vertrag auf, innerhalb einer Frist von einem Monat nach Eingang dieses Schreibens Stellung zu nehmen und ihr alle sachdienlichen Informationen zu übermitteln, die eine Beurteilung der Maßnahme ermöglichen.
Die Kommission fordert die deutschen Behörden auf, detaillierte Informationen insbesondere zu den unter Abschnitt III der vorliegenden Entscheidung genannten Punkten zu übermitteln.
Die Kommission verweist Deutschland auf die aussetzende Wirkung von Artikel 88 Absatz 3 EG-Vertrag sowie auf Artikel 14 der Verordnung (EG) Nr. 659/1999, wonach alle zu Unrecht gewährten Beihilfen vom Empfänger zurückzufordern sind.
Die Kommission weist Deutschland darauf hin, dass sie die anderen Beteiligten durch Veröffentlichung dieses Schreibens und einer ausführlichen Zusammenfassung im Amtsblatt der Europäischen Gemeinschaften informieren wird. Alle Beteiligten werden aufgefordert, sich innerhalb eines Monats ab dem Datum der Veröffentlichung zu äußern."
[1] Gemäß Artikel 15 Absatz 1 der Verordnung (EG) Nr. 659/99 (Verordnung (EG) Nr. 659/1999 des Rates über besondere Vorschriften für die Anwendung von Artikel 93 des EG-Vertrages, ABl. L 83 vom 27. März 1999, S. 1) gelten die Befugnisse der Kommission zur Rückforderung von Beihilfen für eine Frist von zehn Jahren, zurückgerechnet ab dem Zeitpunkt der ersten Kontakaufnahme durch die Kommission (im Jahr 2000).
[2] Urteil vom 24.7.2003, Rs. C-280/00 ("Altmark"), Slg. 2003, S.I-7747
[3] Nach der Rechtsprechung des Europäischen Gerichtshofs deutet die Verbesserung der Wettbewerbsposition eines Unternehmens aufgrund einer staatlichen Beihilfe im allgemeinen auf eine Wettbewerbsverzerrung gegenüber konkurrierenden Unternehmen hin, die keine solche Unterstützung erhalten (Rs. C-730/79, Slg. 1980, S. 2671, Rn. 11 und 12).
[4] Der innergemeinschaftliche Handel Deutschlands mit Agrarerzeugnissen betrug im Jahr 1999 28329 Mio EUR. (Importe) und 18306 Mio EUR. (Exporte). Für das Land Bayern sind keine Daten verfügbar. (QUELLE: EUROSTAT und GD AGRI)
[5] Abl. Nr. L 1/1 vom 3.1.2004
[6] Gemeinschaftsrahmen für staatliche Beihilfen im Agrarsektor, ABl. C 232 vom 12. August 2000, S. 19.
[7] Insbes. die Ziff. 3.2.1. und 3.2.2.
[8] Urt. Vom 26.6.1970, Rs. 47/69, Slg. XVI, S. 487
[9] ABl. C 229 vom 14.9.2004, S. 5
[10] ABl. C 19 vom 20.1.2001, S. 7
[11] ABl. C 100 vom 27.3.1997, S. 12; ABl. C 260 vom 17.9.1994, S. 3
[12] ABl. L 10 vom 13. Januar 2001, S. 30
[13] Wie der Gerichtshof und das Gericht ausgeführt haben, verfälschen Betriebsbeihilfen, also Beihilfen, mit denen ein Unternehmen von Kosten befreit werden soll, die es normalerweise im Rahmen seines laufenden Betriebes oder seiner üblichen Tätigkeiten hätte tragen müssen, grundsätzlich die Wettbewerbsbedingungen (Urteil des Gerichts vom 8. Juni 1995 in der Rechtssache T-459/93, Siemens/Kommission, Slg. 1995, II-1675, Randnrn. 48 und 77, und die dort genannte Rechtsprechung).
[14] Verordnung (EG) Nr. 659/1999 des Rates über besondere Vorschriften für die Anwendung von Artikel 93 des EG-Vertrages, ABl. L 83 vom 27. März 1999, S. 1.
--------------------------------------------------
