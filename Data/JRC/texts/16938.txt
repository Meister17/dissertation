Opinion of the Committee of the Regions on the Communication from the Commission to the Council, the European Parliament, the European Economic and Social Committee and the Committee of the Regions on Connecting Europe at High Speed: National Broadband Strategies
(2005/C 71/13)
THE COMMITTEE OF THE REGIONS,
Having regard to the Communication from the Commission to the Council the European Parliament, the European Economic and Social Committee of the Regions on Connecting Europe at high speed: National broadband strategies COM(2004) 369 final;
Having regard to the decision of the European Commission of 13 May 2004 to consult it on this subject, under the first paragraph of Article 265 of the Treaty establishing the European Community;
Having regard to the decision of its Bureau of 20 April 2004 to instruct its Commission for Culture and Education to draw up an Opinion on the subject;
Having regard to the Lisbon strategy to make the European Union "the most competitive and dynamic knowledge-based economy in the world by 2010, capable of sustainable economic growth, with more and better jobs and greater social cohesion";
Having regard to the Barcelona European Council call on the Commission to draw up an eEurope action plan focusing on "the widespread availability and use of broadband networks throughout the Union by 2005 and the development of Internet protocol IPv6 and the security of networks and information, eGovernment, eLearning, eHealth and eBusiness" [1];
Having regard to the resulting eEurope 2005 Action Plan adopted in May 2002. Stimulating use and creating new services were its new central goals endorsed by the Seville European Council [2]. The overall aims are that, by the end of 2005, Europe should have modern online public services (e-government, e-learning, e-health) and a dynamic e-business environment, based on a widespread availability of broadband access at competitive prices and a secure information infrastructure;
Having regard to the preliminary draft opinion of the European Economic and Social Committee on Connecting Europe at high speed: recent developments in the sector of electronic communications - COM(2004) 61 final [3];
Having regard to the Communication from the Commission on Interoperable Delivery of Pan-European eGovernment Services to Public Administrations, Businesses and Citizens (IDABC) COM(2003) 406 final – 2003/0147 (COD);
Having regard to its Opinion on the Evaluation of the IDA programme and a second phase of the IDA programme (CdR 44/98 fin) [4];
Having regard to its Draft Opinion CdR 257/2004 rev. 2 adopted on 22 September 2004 by its Commission for Culture and Education (Rapporteur: Mr Tomaž Štebe, Mayor of the Municipality Mengeš (SI/EPP));
Whereas:
1. An ambitious high speed connectivity, taking into account equal rights, non-discriminatory and digital opportunity for a European wide information infrastructure must play the major role in bringing together the whole of Europe, the EU Member States and all future European applicant states, i.e. their town and rural municipalities, businesses and citizens;
2. It is crucial to deploy an effective and modern information infrastructure for new and existing businesses, modernised public services;
3. Equal information society opportunities should be among European citizens' rights regarding connectivity and services independent of the type of user, social status and location;
4. Information infrastructure should be understood and managed in the society the same way as the water and electricity supply;
adopted the following opinion at its 57th plenary session, held on 17- 18 November 2004 (Meeting of 18 November).
1. The Committee of the Regions' Views
THE COMMITTEE OF THE REGIONS
1.1 welcomes the conciseness and clarity of the analysis and the conclusions made by the Commission in the Communication "Connecting Europe at High Speed: National Broadband Strategies" which is strongly related to the previous analysis and recommendations in "Connecting Europe at high speed: recent developments in the sector of electronic communications" [5], and the proposed actions in "eEurope 2005 Action Plan: An Update";
1.2 recognises that the benefits of broadband play a major role in economies and societies. Past experiences of significant increases in deployment and take-up of broadband are highly encouraging. The number of broadband connections doubled in EU-15 in 2003. However, there is still a gap compared to the eEurope plan and its targets outside urban centres and institutions, and particularly in rural and in disadvantaged or less favoured areas;
1.3 is convinced that the eEurope Action plan update and EU-25 extended High Speed National Broadband Strategies should stimulate progress to the most advanced, trusted and immediate European Information Infrastructure (EII) for Administrations, Business and Citizens;
1.4 requests that strategies and actions concerning the information infrastructure, especially backbone communications construction - broadband highways in towns and rural municipalities - and supporting infrastructure eServices development should be highly ambitious, taking into account technological and commercial interests and should be financed through local, national and European public funds in the same way as (national) roads and highways or other basic infrastructures;
1.5 requests that the Commission continues to prosecute the infringements of silent regulators not acting effectively, or not acting within a reasonable timeframe, against dominant networks or services, and whose behaviour inhibits equal opportunity provision and effective competition within the information infrastructure on national and local level;
1.6 welcomes the European Initiative for Growth, endorsed by the European Council in December 2003, that highlights the opportunity of using public funds, including the Structural Funds to ensure widespread availability of broadband as already proposed in eEurope 2005 [6]. New Digital Divide "Quick-start projects" will accelerate provision of broadband in less favoured areas where commercial grounds do not apply;
1.7 supports the Commission's suggestions for the Union's structural funds in support of electronic communications in rural or disadvantaged urban areas [7];
1.8 requests that the Commission's information society and information infrastructure policy promote and support developments of innovative services and procedures to replace obsolete technologies and introduce new competitive and advanced commercial and public eServices for businesses, citizens and administrations;
1.9 N.B.: the Commission is preparing a Green Paper on public-private partnerships in the European Union.
[8];
1.10 supports the education and encouragement of the general public for new services and technologies; Broadband should be used to deliver new and better services to the people;
1.11 welcomes public intervention for construction and development of a European Information Infrastructure in favour of Europe's competitive base for commercial and widespread public services. eServices developed with the help of public funds should play an important complementary role for commercial investment and ease private initiatives and support EU companies to compete worldwide;
1.12 welcomes the importance the European Commission is attributing to the issue of a secure information infrastructure and the establishment of the European Network and Information Security Agency (ENISA);
1.13 is in favour of a (re)definition of broadband according to an ambitious understanding of the information infrastructure as outlined in this Opinion. Consequently, existing activities and programmes (eEurope, IST, IDA, eTEN,..., and DRM, IPR [9]) should be adequately clarified, simplified and redefined as well as complemented;
1.14 thinks that the definition of "broadband" in the report is a good starting point, namely: "... a wide range of technologies that have been developed to support the delivery of innovative interactive services, equipped with an always-on functionality, providing sufficient bandwidth capacity that evolves over time, and allowing the simultaneous use of both voice and data services";
1.15 suggests adding to the definition of broadband, as an enabling infrastructure and high speed communication network, the elements of important tools, utilities and mechanisms (EII driver services platform) in support of eServices for on-line, secure and trusted real-time exchange of data;
1.16 is in favour of added mandatory specifications for high speed internet/broadband connections such as delay, fault recovery;
1.17 suggests starting a new initiative The Digital Opportunity Information Technology (DOIT) for all, with special attention on less developed areas, to deploy the information infrastructure and support the present or introduce new economic activities through education, promotion, and information infrastructure funding;
1.18 encourages local authorities to design and build communication cables, underground ducts when (re)constructing streets and roads or installing them together with other cabling and communal pipelines (street lights, power cables) as a long term investment;
1.19 encourages local authorities and the European Commission to evaluate the model where a single company (or local authority or authorities) builds, owns and maintains the information infrastructure. Multiple service providers would then be enabled to use the infrastructure on an equal base. The main aim of this strategy would be to strengthen the competition of costs and quality of service providers, by giving all of them the same, non-discriminatory chances of reaching their customers;
1.20 is in favour of increased competition by: enabling an easy and fast acquisition of necessary licenses for infrastructure and frequency ranges;
1.21 Is in favour of standards and basic applications (common eServices) with common (compatible) data models and data attributes, for e.g.: GIS, 3D and VR visualisation technologies for spatial planning and regeneration / Space, real estate property and communal infrastructure management; distributed and registered public data bases access, update; public office services at home or at the work place; Traffic management: congestion, toll, ticket paying with smart non contact (mobile) IDs;
1.22 is in favour of more ambitious EII targets for communications / connectivity / throughput / availability / affordability of:
a) fibre optic connecting every European final user with minimal 10 Mbps or more, two way communication, except in the cases where economy and optimal communication (geography, user needs) requests other solutions, e.g. (high speed) copper wires or fixed wireless access (WFA) / satellite transmission:
25 % - at the end of 2006;
70 % - at the end of 2010;
b) bandwidth and availability to ensure IP telephony for nomadic users - at the end of 2006;
c) reasonable pricing of monthly broadband connection costs with 10 Mbps throughput, secure internet and trusted transactions, multimedia IP "telephony", digital TV/Radio (multimedia) broadcasting (digital rights not in the price) - at the end of 2006;
1.23 is in favour of the following European information infrastructure (EII) strategies:
a) on-line, reliable, secure, authentic and trusted exchange of multimedia data and documents – at the end of 2006;
b) distributed, interoperable, complex, hierarchical data access and update – at the end of 2007;
c) virtual or simulated environment and real processes access and control in real-time – at the end of 2008;
1.24 is in favour of EII services platform:
a) interoperability of certification and verification systems used in the EU;
b) eMoney/ePay with very affordable transaction and management fees;
c) Telemedicine (eHealth), eLearning;
d) wired up community: Tele-metrics, process control, building and facility management;
1.25 is concerned that with most of the traditional "telephony" copper wire communication is becoming obsolete or entering into very heavy competition (unbundling of local hoops). National governments do not undertake the necessary restructuring and engage in discriminatory activities with the consequences of slowing down the implementation of modern information infrastructure.
2. The Committee of the Regions' recommendations
THE COMMITTEE OF THE REGIONS
2.1 recommends that the Commission enforces the implementation of regulations:
a) Supervision and enforcing of a shared infrastructure competition on a non-discriminatory, equal cost basis for all operators or providers;
b) NRA (National Regulatory Authority) to allow frequency selections for FWA and decrease the licence fees for rural or less favoured areas;
2.2 recommends that the Commission supports the funding of an EII with the following orientations and priorities:
a) for ducts construction;
b) for services platform development;
c) for cabling and networks equipment in rural and less favoured areas;
d) NRA to decrease the licence fees for less favoured areas;
2.3 recommends that the European Commission emphasises in the forthcoming document on High Speed Communication Strategies (EU 25 including the new Candidate countries) to be issued in October 2004 the importance of ambitious and far-reaching technological advances in the European Information Infrastructure.
Brussels, 18 November 2004.
The President
of the Committee of the Regions
Peter Straub
[1] Barcelona European Council, Presidency Conclusions, paragraph 40,http://europa.eu.int/en/Info/eurocouncil/index.htm.
[2] COM(2002) 263 final "eEurope 2005 Action Plan: An information society for all."
[3] R/CESE 880/2004 – TEN 189/2004
[4] OJ C 251 of 10.8.1998, p. 1
[5] "Space: a new European frontier for an expanding union - An action plan for implementing the European Space Policy", COM(2003) 673 final and "Connecting Europe at high speed: recent developments in the sector of electronic communications," COM(2004) 61 final.
[6] See COM(2003) 65 final "Road to the Knowledge Economy" and COM(2003) 690 final "A European Initiative for Growth".
[7] "Guidelines on Criteria and Modalities of Implementation of Structural Funds in Support of Electronic Communications", SEC(2003) 895http://europa.eu.int/comm/regional_policy/sources/docoffic/working/doc/telecom_en.pdf
[8]
[9] COM(2004) 261 final The Management of Copyright and Related Rights in the Internal Market.
--------------------------------------------------
