Commission Decision
of 30 March 2004
amending Annex I to Decision 2003/804/EC laying down the animal health conditions and certification requirements for imports of molluscs, their eggs and gametes for further growth, fattening, relaying or human consumption
(notified under document number C(2004) 1076)
(Text with EEA relevance)
(2004/319/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/67/EEC of 28 January 1991 concerning the animal health conditions governing the placing on the market of aquaculture animals and products(1), and in particular Article 19(1) thereof,
Whereas:
(1) A list of third countries from which Member States are authorised to import live molluscs, their eggs and gametes for further growth, fattening, relaying or human consumption in the Community, as well as model certificates that must accompany such consignments were drawn up by Decision 2003/804/EC(2).
(2) At the time of adoption of Decision 2003/804/EC, no third countries could be listed in Annex I to the Decision.
(3) Since the entering into force of Directive 91/67/EEC, the animal health requirements for import of aquaculture animals into the Community from third countries have been unchanged. Pending the establishment of harmonised certification requirements, the Member States have been responsible for ensuring that imports of aquaculture animals and products thereof from third countries be subjected to conditions at least equivalent to those applying to placing on the market of Community products according to Article 20(3) of Directive 91/67/EEC.
(4) There is therefore an ongoing trade in live bivalve molluscs for the purpose of human consumption between certain third countries and certain Member States. This trade would be blocked from 1 May 2004, when Decision 2003/804/EC will be implemented.
(5) In order not to interrupt, unnecessarily, ongoing trade from third countries that Member States have found to comply with conditions at least equivalent to those applicable for placing on the market within the Community, certain third countries should be included in Annex I to this Decision for a interim period of time, pending the completion of the on-the-spot inspections provided for by Community rules.
(6) Such temporary listing should be limited to imports of live bivalve molluscs for the purpose of human consumption only, from areas authorised according to Council Directive 91/492/EEC(3).
(7) The measures provided for in this Decision are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS DECISION:
Article 1
Annex I to Decision 2003/804/EC is replaced by the Annex to this Decision.
Article 2
This Decision shall apply from 1 May 2004.
Article 3
This Decision is addressed to the Member States.
Done at Brussels, 30 March 2004.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 46, 19.2.1991, p. 1. Directive as last amended by Regulation (EC) No 806/2003 (OJ L 122, 16.5.2003, p. 1).
(2) OJ L 302, 21.11.2003, p. 22.
(3) OJ L 268, 24.9.1991, p. 1. Directive as last amended by Regulation (EC) No 806/2003.
ANNEX
"ANNEX I
Territories from which importation of certain species of live molluscs, their eggs and gametes intended for further growth, fattening, or relaying in European Community waters are authorised
>TABLE>"
