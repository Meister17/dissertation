*****
COMMISSION REGULATION (EEC) No 2882/83
of 13 October 1983
on the classification of goods in heading No 97.04, subheading C of the Common Customs Tariff
THE COMMISSION OF THE EUROPEAN
COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 97/69 of 16 January 1969 on measures to be taken for uniform application of the nomenclature of the Common Customs Tariff (1), as last amended by the Act of Accession of Greece, and in particular Article 3 thereof,
Whereas, in order to ensure uniform application of the nomenclature of the Common Customs Tariff, provisions must be laid down concerning the tariff classification of an electronic game (110 × 73,5 × 8 mm), consisting of a metal and plastic case, with a liquid crystal display screen, and containing a monolithic integrated circuit programmed for the game (with a display of the player's score), the clock and alarm functions and a calculator;
Whereas the Common Customs Tariff annexed to Council Regulation (EEC) No 950/68 (2), as last amended by Regulation (EEC) No 604/83 (3) refers under heading No 97.03 to (inter alia) toys and under heading No 97.04 to (inter alia) equipment for parlour games; whereas these two headings merit consideration for the classification of the abovementioned goods;
Whereas, on the basis of their design and use, the goods have the essential character of an electronic game;
Whereas, in addition, these games, because they provide for the player's score to be displayed on the screen, are designed for competition against other players on the basis of the number of points scored; whereas, consequently they should be considered as equipment for parlour games and thus must be classified under heading No 97.04, subheading C of the Common Customs Tariff;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Committee on Common Customs Tariff Nomenclature,
HAS ADOPTED THIS REGULATION:
Article 1
Electronic games, consisting of a plastic and metal case (110 × 73,5 × 8 mm) with a liquid crystal display screen and containing a monolithic integrated circuit programmed for the game (with a display of the player's score), clock, alarm and calculator functions, are classified, in the Common Customs Tariff, under subheading:
97.04 Equipment for parlour, table and funfair games for adults or children (including billiard tables and table-tennis requisites);
C. Other.
Article 2
This Regulation shall enter into force on the 21st day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 13 October 1983.
For the Commission
Karl-Heinz NARJES
Member of the Commission
(1) OJ No L 14, 21. 1. 1969, p. 1.
(2) OJ No L 172, 22. 7. 1968, p. 1.
(3) OJ No L 72, 18. 3. 1983, p. 3.
