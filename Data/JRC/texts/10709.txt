Legislative proposals adopted by the Commission
(2006/C 70/05)
Document | Part | Date | Title |
COM(2005) 498 | | 17.10.2005 | Proposal for a Council Regulation amending and updating Regulation (EC) No 1334/2000 setting up a Community regime for the control of exports of dual-use items and technology |
COM(2005) 510 | 1 | 22.12.2005 | Proposal for a Council Decision on the conclusion of an Agreement in the form of an Exchange of Letters between the European Community and the Kingdom of Thailand |
COM(2005) 510 | 2 | 22.12.2005 | Proposal for a Council Regulation concerning the implementation of the Agreement concluded by the EC following negotiations in the framework of Article XXIV.6 of GATT 1994, amending Annex I to Regulation (EEC) No 2658/87 on the tariff and statistical nomenclature and on the Common Customs Tariff |
COM(2005) 539 | | 17.2.2006 | Proposal for a Council Regulation laying down detailed rules for the implementation of Council Regulation (EC) No 2494/95 as regards the temporal coverage of price collection in the harmonised index of consumer prices |
COM(2005) 586 | | 23.11.2005 | Proposal for a Directive of the European Parliament and of the Council on compliance with flag State requirements |
COM(2005) 590 | | 23.11.2005 | Proposal for a Directive of the European Parliament and of the Council establishing the fundamental principles governing the investigation of accidents in the maritime transport sector and amending Directives 1999/35/EC and 2002/59/EC |
COM(2005) 592 | | 23.11.2005 | Proposal for a Regulation of the European Parliament and of the Council on the liability of carriers of passengers by sea and inland waterways in the event of accidents |
COM(2005) 634 | | 21.12.2005 | Proposal for a Directive of the European Parliament and of the Council on the promotion of clean road transport vehicles |
COM(2005) 650 | | 15.12.2005 | Proposal for a Regulation of the European Parliament and the Council on the law applicable to contractual obligations (Rome I) |
COM(2005) 667 | | 21.12.2005 | Proposal for a Directive of the European Parliament and of the Council on waste |
COM(2005) 674 | | 20.12.2005 | Proposal for a Council Regulation amending Regulations (EC) No 1975/2004 and (EC) No 1976/2004 extending definitive anti-dumping and countervailing duties on imports of polyethylene terephthalate (PET) film originating in India, to imports of polyethylene terephthalate (PET) film consigned from Brazil and from Israel, whether declared as originating in Brazil or Israel or not |
COM(2005) 687 | | 22.12.2005 | Proposal for a Council Decision approving the accession of the European Community to the Geneva Act of the Hague Agreement concerning the international registration of industrial designs, adopted in Geneva on 2 July 1999 |
COM(2005) 704 | | 10.1.2006 | Proposal for a Council Decision authorising Lithuania to apply a measure derogating from Article 21 of the Sixth Council Directive 77/388/EEC on the harmonisation of the laws of the Member States relating to turnover taxes |
COM(2005) 711 | | 26.1.2006 | Proposal for a Council Regulation imposing a definitive anti-dumping duty and collecting definitively the provisional duty imposed on imports of tartaric acid originating in the People's Republic of China |
These texts are available on EUR-Lex: http://europa.eu.int/eur-lex/lex/
--------------------------------------------------
