COUNCIL DIRECTIVE of 16 December 1991 on the approximation of the laws of the Member States relating to compulsory use of safety belts in vehicles of less than 3,5 tonnes (91/671/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 75 thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas the national laws on compulsory use of safety belts differ widely from one Member State to another; whereas it is therefore necessary to harmonize that compulsory use;
Whereas the obligation to use safety belts should be harmonized in vehicles of less than 3,5 tonnes in order to guarantee road users a greater degree of safety;
Whereas Directives 76/115/EEC (4) and 77/541/EEC (5) concern the technical requirements relating to safety belts with which motor vehicles must comply, but not the use of safety belts;
Whereas in the resolution of 19 December 1984 (6), the Council and the Representatives of the Governments of the Member States, meeting within the Council, undertook to ensure the rapid adoption of road safety measures, and invited the Commission to submit proposals;
Whereas resolutions of the European Parliament on road safety (7) have recommanded that the wearing of safety belts by all passengers, including children, on all roads and in all seats in passenger vehicles (except public service vehicles), should be compulsory;
Whereas provision should be made for the compulsory use of child restraint systems on seats fitted with safety belts;
Whereas, pending harmonized Community standards for child restraint systems, those standards which correspond to Member States' national requirements must be recognized throughout the Member States;
Whereas studies have also shown that rear seats are almost as hazardous as front seats for unbelted passengers and that rear-seat passengers not wearing belts increase the risk of injuries to front seat passengers; whereas deaths and injuries could thus be further reduced if the wearing of belts in rear seats was made compulsory;
Whereas the fixing of the date of entry into force of the measures referred to in this Directive should allow time for the drafting of the implementing provisions required, particularly in those Member States where as yet no provisions on this matter exist,
HAS ADOPTED THIS DIRECTIVE:
Article 1
1. This Directive shall apply to all motor vehicles in categories M1, M2 (except for rear seats and vehicles of a maximum permissible weight exceeding 3,5 tonnes and those which include places specially designed for standing passengers) and N1 (except for rear seats), as defined in Annex I to Directive 70/156/EEC (1), intended for use on the road, having at least four wheels and a maximum design speed exceeding 25 km/h.
2. for the purposes of this Directive:
1. 'safety belt (seat belt, belt)` means an assembly of straps with a securing buckle, adjusting devices and attachments which is capable of being anchored inside a power-driven vehicle and is designed to diminish the risk of injury to its wearer, in the event of collision or of abrupt vehicle deceleration, by limiting the mobility of the wearer's body. Such an assembly is generally referred to as a 'belt assembly`, a term also embracing any devices for energy absorption or belt retraction;
2. 'restraint system` means a system combining a seat, fixed to the structure of the vehicle by appropriate means and a safety belt for which at least one anchorage point is located on the seat structure;
3. 'seat` means a structure which may or may not be integral with the vehicle structure complete with trim, intended to seat one adult person. The term covers both an individual seat and part of a bench seat intended to seat one person.
Article 2
Member States shall ensure that the driver and passengers occupying the seats of vehicles on the road as referred to in Article 1 wear safety belts or are restrained by an approved restraint system provided the occupied seats are fitted with such equipment. Rear-seat occupants must use equipped seats before others.
Member States shall ensure that children under 12 years of age and who are less than 150 cm tall, travelling in such vehicles and occupying belted seats, are restrained by an approved restraint system suitable for the child's height and weight. These seats must be occupied before the others.
The use of a restraint system approved by the competent authority of a Member State shall be permitted by the other Member States.
Article 3
The provisions of this Directive shall also apply to drivers and passengers of vehicles being used on the road in the Community which are registered in a third country.
Article 4
1. By way of derogation from the second paragraph of Article 2, Member States may, on their national territory, permit children aged three years and over occupying the seats of vehicles referred to in Article 1 to be restrained by a safety belt or other restraint system approved for adult use.
2. Member States shall also, on their territory and under conditions specified in their national law, allow that children under three years of age occupying rear seats need not be restrained by a restraint system suitable for their height and weight if such children are transported in a vehicle where such a system is unavailable.
Article 5
Persons provided by the competent authorities with an exemption certificate for serious medical reasons shall be exempt from the obligations laid down in Article 2. Any medical certificate issued by the competent authorities in one Member State shall also be valid in any other Member State; the medical certificate must indicate its period of validity and be shown to all authorized persons upon request in accordance with the relevant provisions in force in each Member State. It must bear the following symbol:
>REFERENCE TO A FILM>
Article 6
Member States may, with the Commission's agreement, grant exemptions other than those laid down in Article 5, in order to:
- take account of specific physical conditions, or particular circumstances of limited duration,
- allow certain types of occupation to be carried out effectively,
- ensure that the police, security services or emergency services can perform their duties properly.
Article 7
The Commission shall, by 1 August 1994, submit a report on the implementation of this Directive for the particular purpose of establishing whether or not more stringent safety measures and much closer harmonization are needed. If appropriate, the report shall be accompanied by proposals, on which the Council shall act by a qualified majority at the earliest opportunity.
Article 8
1. After consulting the Commission, Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive before 1 January 1993. They shall forthwith inform the Commission thereof.
2. When Member States adopt these provisions, they shall contain a reference to this Directive or be accompanied by such reference on the occasion of their official publication. The methods of making such a reference shall be laid down by the Member States.
3. Member States shall communicate to the Commission the texts of the main provision of national law, which they adopt in the field governed by this Directive.
Article 9
This Directive is addressed to the Member States.
Done at Brussels, 16 December 1991.
For the CouncilThe PresidentH. MAIJ-WEGGEN
(1)OJ N° C 298, 23. 11. 1988, p. 8; and OJ N° C 308,
8. 12. 1990, p. 11.
(2)OJ N° C 96, 17. 4. 1989, p. 220; and OJ N° C 240,
16. 9. 1991, p. 74.
(3)OJ N° C 159, 26. 6. 1989, p. 52; and OJ N° C 159,
17. 6. 1991, p. 56.
(4)Council Directive 76/115/EEC of 18 December 1975 on the approximation of the laws of the Member States relating to anchorages for motor vehicle safety belts (OJ N° L 24, 30. 1. 1976, p. 6), as last amended by Directive 90/629/EEC (OJ N° L 341, 6. 12. 1990, p. 14).
(5)Council Directive 77/541/EEC of 28 June 1977 on the approximation of the laws of the Member States relating to safety belts and restraint systems of motor vehicles (OJ N° L 220, 29. 8. 1977, p. 95), as last amended by Directive 90/628/EEC (OJ N° L 341, 6. 12. 1990, p. 1).
(6)OJ N° C 341, 21. 12. 1984, p. 1.
(7)OJ N° C 104, 16. 4. 1984, p. 38; and OJ N° C 68,
24. 3. 1986, p. 35.
(1)Council Directive 70/156/EEC of 6 February 1970 on the approximation of the laws of the Member States relating to the type-approval of motor vehicles and their trailers (OJ N° L 42, 23. 2. 1970, p. 1), as last amended by Directive 87/403/EEC (OJ N° L 220, 8. 8. 1987, p. 44).
