Council Decision
of 13 June 2005
appointing a Member of the European Economic and Social Committee
(2005/456/EC, Euratom)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 259 thereof,
Having regard to the Treaty establishing the European Atomic Energy Community, and in particular Article 167 thereof,
Having regard to Council Decision 2002/758/EC, Euratom of 17 September 2002 appointing the Members of the Economic and Social Committee for the period from 21 September 2002 to 20 September 2006 [1],
Having regard to the nomination submitted by the Italian Government,
Having regard to the opinion of the Commission,
Whereas:
HAS DECIDED AS FOLLOWS:
Article 1
Mr Angelo GRASSO is hereby appointed a Member of the European Economic and Social Committee in place of Mr Giacomino TARICCO for the remainder of the latter’s term of office, which runs until 20 September 2006.
Article 2
This Decision shall be published in the Official Journal of the European Union.
It shall take effect on the date of its adoption.
Done at Luxembourg, 13 June 2005.
For the Council
The President
J. Asselborn
[1] OJ L 253, 21.9.2002, p. 9.
--------------------------------------------------
