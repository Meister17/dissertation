Commission Directive 2006/27/EC
of 3 March 2006
amending for the purposes of adapting to technical progress Council Directives 93/14/EEC on the braking of two- or three-wheel motor vehicles and 93/34/EEC on statutory markings for two- or three-wheel motor vehicles, Directives of the European Parliament and of the Council 95/1/EC on the maximum design speed, maximum torque and maximum net engine power of two- or three-wheel motor vehicles and 97/24/EC on certain components and characteristics of two- or three-wheel motor vehicles
(Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Directive 2002/24/EC of the European Parliament and of the Council of 18 March 2002 relating to the type-approval of two- and three-wheel motor vehicles and repealing Council Directive 92/61/EEC [1], and in particular Article 17 thereof,
Having regard to Council Directive 93/14/EEC of 5 April 1993 on the braking of two- or three-wheel motor vehicles [2], and in particular Article 4 thereof,
Having regard to Council Directive 93/34/EEC of 14 June 1993 on statutory markings for two- or three-wheel motor vehicles [3], and in particular Article 3 thereof,
Having regard to Directive 95/1/EC of the European Parliament and of the Council of 2 February 1995 on the maximum design speed, maximum torque and maximum net engine power of two- or three-wheel motor vehicles [4], and in particular Article 4 thereof,
Having regard to Directive 97/24/EC of the European Parliament and of the Council of 17 June 1997 on certain components and characteristics of two- or three-wheel motor vehicles [5], and in particular Article 7 thereof,
Whereas:
(1) Directives 93/14/EEC, 93/34/EEC, 95/1/EC and 97/24/EC are separate Directives for the purposes of the EC type-approval procedure laid down by Directive 2002/24/EC.
(2) It is necessary to introduce the last amendment to United Nations ECE Regulation No 78 into the European type approval requirements in order to maintain equivalence between the requirements laid down in Directive 93/14/EEC and those laid down in United Nations ECE Regulation No 78.
(3) The requirements on statutory markings and maximum speed for two- and three-wheel motor vehicles as laid down in Directives 93/34/EEC and 95/1/EC can be simplified for reasons of better regulation.
(4) In order to ensure the proper functioning of the type-approval system as a whole, it is necessary to clarify which provisions concerning external projections, safety belt anchorages and safety belts shall apply to bodied vehicles and to unbodied vehicles.
(5) In Directive 97/24/EC, the requirements for marking of original catalytic converters and original silencers need to be clarified and completed.
(6) Directives 93/14/EEC, 93/34/EEC, 95/1/EC and 97/24/EC should therefore be amended accordingly.
(7) The measures provided for this Directive are in accordance with the opinion of the Committee for Adaptation to Technical Progress,
HAS ADOPTED THIS DIRECTIVE:
Article 1
The Annex to Directive 93/14/EEC is amended in accordance with the text set out in Annex I to this Directive.
Article 2
The Annex to Directive 93/34/EEC is amended in accordance with the text set out in Annex II to this Directive.
Article 3
Annex I to Directive 95/1/EC is amended in accordance with the text set out in Annex III to this Directive.
Article 4
Annex III to Chapter 1, Annexes I and II to Chapter 3, Annex I to Chapter 4, Annexes I, II, VI, and VII to Chapter 5, the Annex to Chapter 7, Annexes II, III and IV to Chapter 9, the title and Annex I to Chapter 11, and Annexes I and II to Chapter 12 of Directive 97/24/EC are amended in accordance with the text set out in Annex IV to this Directive.
Article 5
1. With effect from 1 January 2007, with respect to two- or three- wheel vehicles which comply with the provisions laid down in Directives 93/14/EEC, 93/34/EC, 95/1/EC, and 97/24/EC respectively, as amended by this Directive, Member States shall not, on grounds relating to the subject matter of the Directive concerned, refuse to grant EC type-approval or prohibit the registration, sale or entry into service of such a vehicle.
2. With effect from 1 July 2007, Member States shall refuse, on grounds relating to the subject matter of the Directive concerned, to grant EC type-approval to any new type of two- or three-wheel motor vehicle which does not comply with the provisions laid down in Directives 93/14/EEC, 93/34/EC, 95/1/EC and 97/24/EC respectively, as amended by this Directive.
Article 6
1. Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive by 31 December 2006 at the latest. They shall forthwith communicate to the Commission the text of those provisions and a correlation table between those provisions and this Directive.
When Member States adopt those provisions, they shall contain a reference to this Directive or be accompanied by such a reference on the occasion of their official publication. Member States shall determine how such reference is to be made.
2. Member States shall communicate to the Commission the text of the main provisions of national law which they adopt in the field covered by this Directive.
Article 7
This Directive shall enter into force on the 20th day following its publication in the Official Journal of the European Union.
Article 8
This Directive is addressed to the Member States.
Done at Brussels, 3 March 2006.
For the Commission
Günter Verheugen
Vice-President
[1] OJ L 124, 9.5.2002, p. 1. Directive as last amended by Commission Directive 2005/30/EC (OJ L 106, 27.4.2005, p. 17).
[2] OJ L 121, 15.5.1993, p. 1.
[3] OJ L 188, 29.7.1993, p. 38. Directive as amended by Commission Directive 1999/25/EC (OJ L 104, 21.4.1999, p. 19).
[4] OJ L 52, 8.3.1995, p. 1. Directive as amended by Commission Directive 2002/41/EC (OJ L 133, 18.5.2002, p. 17).
[5] OJ L 226, 18.8.1997, p. 1. Directive as last amended by Directive 2005/30/EC.
--------------------------------------------------
ANNEX I
The Annex to Directive 93/14/EEC is amended as follows:
(1) The following section 2.1.1.3 is added:
"2.1.1.3. Brake linings shall not contain asbestos.";
(2) Appendix 1 is amended as follows:
(a) Sections 1.1.1 and 1.1.2 are replaced by the following:
"1.1.1. The performance prescribed for braking devices shall be based on the stopping distance and/or the mean fully developed deceleration. The performance of a braking device shall be determined by measuring the stopping distance in relation to the initial speed of the vehicle and/or measuring the mean fully developed deceleration during the test.
1.1.2. The stopping distance shall be the distance covered by the vehicle from the moment when the driver begins to actuate the control of the braking system until the moment when the vehicle stops; the initial vehicle speed, v1, shall be the speed at the moment when the driver begins to actuate the control of the braking system; the initial speed shall not be less than 98 % of the prescribed speed for the test in question. The mean fully developed deceleration, (dm), shall be calculated as the deceleration averaged with respect to distance over the interval vb to ve according to the following formula:
dm = v2b v2e25,92se - sb m/s2
Where:
dm = means fully developed deceleration
v1 = as defined above
vb = vehicle speed at 0,8 v1 in km/h
ve = vehicle speed at 0,1 v1 in km/h
sb = distance travelled between v1 and vb in metres
se = distance travelled between v1 and ve in metres
The speed and distance shall be determined using instrumentation having an accuracy of ± 1 % at the prescribed speed for the test. The "dm" may be determined by other methods than the measurement of speed and distance; in this case, the accuracy of the "dm" shall be within ± 3 %.";
(b) In Section 1.1.3, the word "component" is replaced by "vehicle";
(c) Section 1.2.1.1 is replaced by the following:
"1.2.1.1. The limits prescribed for minimum performance are those laid down hereunder for each category of vehicle; the vehicle shall satisfy both the prescribed stopping distance and the prescribed mean fully developed deceleration for the relevant vehicle category, but it may not be necessary to measure both parameters.";
(d) Section 1.4.2.1 is replaced by the following:
"1.4.2.1. The vehicle and the brake(s) to be tested must be substantially free from moisture and the brake(s) cold. A brake is deemed to be cold when the temperature measured on the disc or on the outside of the drum is below 100 °C;".
--------------------------------------------------
ANNEX II
The Annex to Directive 93/34/EEC is amended as follows:
Section 3.1.1.2 is replaced by the following:
"3.1.1.2. The second part consists of six characters (letters or digits) for the purpose of describing the general characteristics of the vehicle (type, variant, and in the case of mopeds, version); each characteristic may be represented by several characters. If its manufacturer does not use one or more of those characters the unused spaces must be filled by alphabetical or numerical characters, the choice being left to the manufacturer;".
--------------------------------------------------
ANNEX III
Annex I to Directive 95/1/EC is amended as follows:
Section 7 is replaced by the following:
"7. MAXIMUM SPEED
The maximum speed of the vehicle is expressed in kilometres per hour by the figure corresponding to the closest whole number to the arithmetical mean of the values for the speeds measured during the two consecutive tests, which must not diverge by more than 3 %. When this arithmetical mean lies exactly between two whole numbers it is rounded up to the next highest number. In the case of vehicles where the maximum speed is not limited by the relevant definition given in Article 1, sections 2 and 3 of Directive 2002/24/EC, no type-approval test is needed and the maximum speed shall be accepted as being that declared by the vehicle manufacturer in the information document given in Annex II to Directive 2002/24/EC.".
--------------------------------------------------
ANNEX IV
Directive 97/24/EC is amended as follows:
(1) In section II.1 of Appendix 2 to Annex III to Chapter 1, the fifth indent is deleted.
(2) Chapter 3 is amended as follows:
(a) Annex I is amended as follows:
(i) The title is replaced by the following:
"REQUIREMENTS APPLYING TO EXTERNAL PROJECTIONS FROM TWO-WHEEL MOTOR VEHICLES";
(ii) The following section 3.6 is added:
"3.6. In the case of two-wheel vehicles that are fitted with a form of structure or panels intended to enclose, or partially enclose, the driver or passengers or to cover components of the vehicle, the type-approval authority or technical service may, at its discretion and in discussion with the vehicle manufacturer, apply the requirements of this Annex, or of Annex II, to all or part of the vehicle based on an assessment of the worst case condition.";
(iii) Section 6.2 is replaced by the following:
"6.2. The end of the clutch and brake levers shall be perceptibly spherical and have a radius of curvature of at least 7 mm. The outer edges of these levers shall have a radius of curvature of not less than 2 mm. The verification is done with the levers in non-applied position."
(b) The title and "GENERAL" section of ANNEX II are replaced by the following:
"REQUIREMENTS APPLYING TO EXTERNAL PROJECTIONS FROM THREE-WHEEL MOTOR VEHICLES, LIGHT QUADRICYCLES AND QUADRICYCLES
GENERAL
The requirements set out in Directive 74/483/EEC [1] relating to the external projections of (category M1) motor vehicles shall apply to three-wheel motor vehicles intended for the carriage of passengers.
However, bearing in mind the variety of forms of construction of these vehicles, the type approval authority or technical service may, at its discretion and in discussion with the vehicle manufacturer, apply the requirements of this Annex, or of Annex I to all or part of the vehicle, based on an assessment of the worst case condition.
This shall also apply to the requirements given below with regard to the requirements for three-wheel vehicles, light quadricycles and quadricycles.
The following requirements shall apply to three-wheel motor vehicles, light quadricycles and quadricycles intended for the carriage of goods.
(3) In Annex I to Chapter 4 the following sections 14 and 15 are added:
"14. "Unbodied vehicle" means a vehicle for which the passenger compartment is not bounded by at least four of all of the following features: windscreen, floor, roof and side and rear walls or doors.
15. "Bodied vehicle" means a vehicle for which the passenger compartment is bounded or may be bounded by at least four of the following elements: windscreen, floor, roof and side and rear walls or doors."
(4) Chapter 5 is amended as follows:
(a) Annex I is amended as follows:
(i) Section 2.3.2 is replaced by the following:
"2.3.2. All original equipment catalytic converter(s) shall bear at least the following identifications:
- the "e" mark followed by the identification of the country which granted the type-approval,
- the vehicle manufacturer's name or trade mark,
- the make and identifying part number.
This reference must be legible and indelible and also visible, in the position at which it is to be fitted."
(ii) Section 5.2.1 is replaced by the following and sections 5.2.1.1 and 5.2.1.2 are deleted:
"5.2.1. Markings
Original replacement catalytic converters shall bear at least the following identifications:
- the "e" mark followed by the identification of the country which granted the type-approval,
- the vehicle manufacturer's name or trade mark,
- the make and identifying part number.
This reference must be legible and indelible and also visible, in the position at which it is to be fitted.";
(b) Annex II is amended as follows:
(i) Section 2.4.2 is replaced by the following:
"2.4.2. All original equipment catalytic converter(s) shall bear at least the following identifications:
- the "e" mark followed by the identification of the country which granted the type-approval,
- the vehicle manufacturer's name or trade mark,
- the make and identifying part number.
This reference must be legible and indelible and also visible, in the position at which it is to be fitted."
(ii) Section 5.2.1 is replaced by the following and sections 5.2.1.1 and 5.2.1.2 are deleted:
"5.2.1. Markings
Original replacement catalytic converters shall bear at least the following identifications:
- the "e" mark followed by the identification of the country which granted the type-approval,
- the vehicle manufacturer's name or trade mark,
- the make and identifying part number.
This reference must be legible and indelible and also visible, in the position at which it is to be fitted.";
(c) Section 4a of Annex VI is replaced by the following:
"4a. Catalytic converters
4a.1. Original equipment catalytic converter tested to all the requirements of this directive
4a.1.1. Make and type of original equipment catalytic converter as listed in item 3.2.12.2.1 of Annex V (the information document).
4a.2. Original replacement catalytic converter tested to all the requirements of this directive
4a.2.1. Make(s) and type(s) of original replacement catalytic converter as listed in item 3.2.12.2.1 of Annex V (the information document)";
(5) Figure 1 of the Annex to chapter 7 is replaced by the following:
Figure 1
+++++ TIFF +++++
Trade mark:Vehicle category:Note: Not applicable rows can be omitted.1.2.3.4.5.6.7.7a8.9.10.11.12.
(6) Chapter 9 is amended as follows:
(a) Section 2.3.2.2 of Annex II is replaced by the following:
"2.3.2.2. All original silencers shall bear at least the following identifications:
- the "e" mark followed by the identification of the country which granted the type-approval,
- the vehicle manufacturer's name or trade mark,
- the make and identifying part number.
This reference must be legible and indelible and also visible, in the position at which it is to be fitted.";
(b) Section 2.3.2.2 of Annex III is replaced by the following:
"2.3.2.2. All original silencers shall bear at least the following identifications:
- the "e" mark followed by the identification of the country which granted the type-approval,
- the vehicle manufacturer's name or trade mark,
- the make and identifying part number.
This reference must be legible and indelible and also visible, in the position at which it is to be fitted.";
(c) Section 2.4.2.2 of Annex IV is replaced by the following:
"2.4.2.2. All original silencers shall bear at least the following identifications:
- the "e" mark followed by the identification of the country which granted the type-approval,
- the vehicle manufacturer's name or trade mark,
- the make and identifying part number.
This reference must be legible and indelible and also visible, in the position at which it is to be fitted.";
(7) Chapter 11 is amended as follows:
(a) The title is replaced by the following:
"SAFETY-BELT ANCHORAGES AND SAFETY-BELTS OF THREE-WHEEL MOPEDS, TRICYCLES AND QUADRICYCLES";
(b) Annex I is amended as follows:
(i) Section 1.6 is replaced by the following and the following section 1.6a. is inserted:
"1.6. "seat" means a structure, whether or not forming an integral part of the vehicle structure and including its trim, which offers a seated position for an adult, the term designating both an individual seat and part of a bench corresponding to a seating position. A saddle is not considered to be a seat for item 2.1.
1.6a. "saddle" means a seating position where the rider or passenger sits astride.";
(ii) Section 2 is replaced by the following:
"2. GENERAL REQUIREMENTS
2.1. Whenever safety belt anchorages are fitted, these must comply with the prescriptions in this Chapter.
2.1.1. Safety belt anchorages must be fitted for all seats of three-wheeled mopeds, tricycles, light quadricycles and quadricycles.
2.1.1.1. Anchorage points suitable for three-point belts are required for all seats that meet both of the following conditions:
- when the seat has a back or when a support helps to determine the back rest angle of the dummy and may be considered as a seatback, and
- when there is a lateral or transversal structural element behind the H point at a height of more than 450 mm measured from the vertical plane of the H point.
2.1.1.2. For all other seats, anchorages suitable for lap belts are acceptable.
2.1.2. Safety belt anchorages are not mandatory for three-wheeled mopeds or quadricycles having an unladen mass of not more than 250 kg."
(8) Chapter 12 is amended as follows:
(a) After the title of Annex I to Chapter 12 the following sentence is inserted:
"For the purpose of this Chapter "bodied vehicle" means a vehicle for which the passenger compartment is bounded or may be bounded by at least four of the following elements: windscreen, floor, roof, side and rear walls or doors.";
(b) Section 2.3.1 of Annex II is replaced by the following:
"2.3.1. All vehicles must be equipped with a windscreen de-icer and de-mister enabling any ice or frost covering the windscreen and any mist covering the inner surface of the windscreen to be removed.
However, this device is not required for bodied three-wheel mopeds having an engine developing not more than 4 kW or for vehicles where the windscreen is fitted such that there is no any supporting or other structure or panel attached to the windscreen extending rearwards for more than 100 mm. The device is required for any vehicle having a roof that is either permanent or detachable or retractable."
[1] OJ L 266, 2.10.1974, p. 4."
--------------------------------------------------
