COUNCIL AND COMMISSION DECISION
of 29 April 1999
concerning the conclusion of the Agreement between the European Communities and the Government of Canada regarding the application of their competition laws
(1999/445/EC, ECSC)
THE COUNCIL OF THE EUROPEAN UNION,
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community, and in particular Articles 87 and 235, in conjunction with the first subparagraph of Article 228(3) thereof,
Having regard to the Treaty establishing the European Coal and Steel Community, and in particular Articles 65 and 66 thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament(1),
(1) Whereas Article 235 of the Treaty must be invoked owing to the inclusion in the text of the Agreement of mergers and acquisitions which are covered by Council Regulation (EEC) No 4064/89 of 21 December 1989 on the control of concentrations between undertakings(2), which is essentially based on Article 235;
(2) Whereas, given the increasingly pronounced international dimension to competition problems, international cooperation in this field should be strengthened;
(3) Whereas, to this end, the Commission has negotiated an Agreement with the Government of Canada on the application of the competition rules of the European Communities and of Canada;
(4) Whereas the Agreement, including the Exchange of Letters, should be approved,
HAS DECIDED AS FOLLOWS:
Article 1
The Agreement between the European Communities and the Government of Canada regarding the application of their competition laws, including the Exchange of Letters, is hereby approved on behalf of the European Community and the European Coal and Steel Community.
The text of the Agreement and of the Exchange of Letters, drawn up in the Danish, German, English, Spanish, Finnish, French, Greek, Italian, Dutch, Portuguese and Swedish languages are attached do this Decision.
Article 2
The President of the Council is hereby authorised to designate the person(s) empowered to sign the Agreement in order to bind the European Community.
The President of the Commission is hereby authorised to designate the person(s) empowered to sign the Agreement in order to bind the European Coal and Steel Community.
Done at Luxembourg, 29 April 1999.
For the Council
The President
W. MÜLLER
For the Commission
The President
J. SANTOR
(1) OJ C 150, 28.5.1999.
(2) OJ L 395, 30.12.1989, p. 1 (corrected version: OJ L 257, 21.9.1990, p. 13).
