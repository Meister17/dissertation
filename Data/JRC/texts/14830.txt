Judgment of the Court (Second Chamber) of 28 September 2006 (reference for a preliminary ruling from the College van Beroep voor het bedrijfsleven — the Netherlands) — NV Raverco (C-129/05), Coxon & Chatterton Ltd (C-130/05) v Minister van Landbouw, Natuur en Voedselkwaliteit
(Joined Cases C-129/05 and C-130/05) [1]
Referring court
College van Beroep voor het bedrijfsleven
Parties to the main proceedings
Applicants: NV Raverco, Coxon & Chatterton Ltd
Defendant: Minister van Landbouw, Natuur en Voedselkwaliteit
Re:
Reference for a preliminary ruling — College van Beroep voor het bedrijfsleven — Interpretation of Articles 17(2) and 22(2) of and Annex I to Council Directive 97/78/EC of 18 December 1997 laying down the principles governing the organisation of veterinary checks on products entering the Community from third countries (OJ 1998 L 24, p. 9) — Interpretation of Council Regulation No 2377/90 of 26 June 1990 laying down a Community procedure for the establishment of maximum residue limits of veterinary medicinal products in foodstuffs of animal origin (OJ 1990 L 224, p. 1) — Veterinary checks — Redispatch of products that do not satisfy the import conditions — Seizure and destruction — Protection of the interests of third countries even in the absence of Community interest
Operative part of the judgment
1. Article 17(2)(a) of Council Directive 97/78/EC of 18 December 1997 laying down the principles governing the organisation of veterinary checks on products entering the Community from third countries is to be interpreted as meaning that objection to the redispatch of a consignment that does not satisfy the import conditions must be based on the failure to meet Community requirements.
2. Article 22(2) of Directive 97/78, read in conjunction with Article 5 of Council Regulation (EEC) No 2377/90 of 26 June 1990 laying down a Community procedure for the establishment of maximum residue limits of veterinary medicinal products in foodstuffs of animal origin, is to be interpreted as meaning that it imperatively requires the competent veterinary authorities to seize and destroy products which, following veterinary inspections carried out pursuant to that directive, are revealed to contain a substance listed in Annex IV to that regulation.
[1] OJ C 143, 11.06.2005.
--------------------------------------------------
