Order of the President of the Civil Service Tribunal of 31 May 2006 — Bianchi
v European Training Foundation
(Case F-38/06 R)
Parties
Applicant: Irène Bianchi (Turin, Italy) (represented by: M.-A. Lucas, lawyer)
Defendant: European Training Foundation (represented by: M. Dunbar, Director, assisted by G. Vandersanden, lawyer)
Re:
First, suspension of execution of the decision of 24 October 2005 by which the European Training Foundation refused to renew the applicant's temporary agent contract and, second, the granting of interim measures.
Operative part of the order
1. The application for interim measures is dismissed.
2. Costs are reserved.
--------------------------------------------------
