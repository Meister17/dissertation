Commission Regulation (EC) No 388/2005
of 8 March 2005
adopting the specifications of the 2006 ad hoc module on transition from work into retirement provided for by Council Regulation (EC) No 577/98 and amending Regulation (EC) No 246/2003
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 577/98 of 9 March 1998 on the organisation of a labour force sample survey in the Community [1], and in particular Article 4(2) thereof,
Whereas:
(1) Commission Regulation (EC) No 246/2003 of 10 February 2003 adopting the programme of ad hoc modules, covering the years 2004 to 2006, to the labour force sample survey provided by Council Regulation (EC) No 577/98 [2] includes an ad hoc module on transition from work into retirement.
(2) There is a need for a comprehensive and comparable set of data on transition from work into retirement in order to monitor progress towards the common objectives of the Community’s Employment Strategy and of the open method of coordination in the area of pensions that was launched by the Laeken European Council in December 2001. Both processes identify the promotion of active ageing and prolongation of working life as priorities for action, in particular through guideline 5 of the Employment Guidelines 2003 "Increase labour supply and promote active ageing" as adopted by the Council on 22 July 2003 [3] and through objective 5 of the pensions process as developed in the Joint report on objectives and working methods in the area of pensions approved by the Council of Laeken of 14 and 15 December of 2001 and in the Joint report by the Commission and the Council on adequate and sustainable pensions adopted by the Council of Brussels, 20 and 21 March 2003.
(3) In accordance with Decision No 1145/2002/EC [4] of the European Parliament and of the Council of 10 June 2002 on Community incentive measures in the field of employment, Community activities concerning analysis, research and cooperation among the Member States in the field of employment and the labour market shall be carried out in the period from 1 January 2002 to 31 December 2006 and one of the objectives of these activities is to develop, follow up and evaluate the European Employment Strategy with a strong forward-looking emphasis.
(4) It is also necessary to update the specification of the sample set out in section 3 of the Annex to Regulation (EC) No 246/2003, in order to maximise the potentiality of the sample for the ad hoc module in terms of analysis.
(5) The measures provided for in this Regulation are in accordance with the opinion of the Statistical Programme Committee,
HAS ADOPTED THIS REGULATION:
Article 1
The detailed list of information to be collected in 2006 by the ad hoc module on transition from work into retirement shall be as set out in the Annex.
Article 2
In Section 3 of the Annex to Regulation (EC) No 246/2003, the point "Sample" is replaced by the following:
"Sample : The target age group for the sample for this module consists of persons aged 50 to 69. The complete set of variables of the labour force survey shall be collected for the sub-sample used for the ad hoc module. When the sample unit is the individual, no data on the other members of the household are required."
Article 3
This Regulation shall enter into force on the seventh day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 8 March 2005.
For the Commission
Joaquín Almunia
Member of the Commission
[1] OJ L 77, 14.3.1998, p. 3. Regulation as last amended by Regulation (EC) No 2257/2003 of the European Parliament and of the Council (OJ L 336, 23.12.2003, p. 6).
[2] OJ L 34, 11.2.2003, p. 3.
[3] OJ L 197, 5.8.2003, p. 13.
[4] OJ L 170, 29.6.2002, p. 1. Decision as amended by Decision No 786/2004/EC (OJ L 138, 30.4.2004, p. 7).
--------------------------------------------------
ANNEX
LABOUR FORCE SURVEY
Specifications of the 2006 ad hoc module on transition from work into retirement
1. Member States and regions concerned: all.
2. The variables will be coded as follows:
The numbering of the variables of the labour force survey in the column "Filter" (C11/14, C24 and C67/70) refers to Commission Regulation (EC) No 1575/2000.
Column | Code | Description | Filter |
240 | | Person reduced his/her working hours in a move to full retirement | Everybody aged 50-69 and ((C24 = 3, 5 and (C67/70 – C11/14) > 49) or (C24 = 1, 2)) |
1 | Yes, in a progressive retirement scheme/part-time pension |
2 | Yes, but not in a progressive retirement scheme/part-time pension |
3 | No, but plans to do so within the next 5 years |
4 | No, and plans not to do so within the next 5 years/did not do so |
5 | No, and does not know about plans for the next 5 years or plans are not relevant |
9 | Not applicable (not included in the filter) |
Blank | No answer |
241/242 | | Planned age for stopping all work for pay or profit | Everybody aged 50-69 and ((C24 = 3, 5 and (C67/70 – C11/14) > 49) or (C24 = 1, 2)) |
50-93 | 2 digits |
94 | No exact planned age, but it will be before 60 years old |
95 | No exact planned age, but it will be between 60 and 64 years old |
96 | No exact planned age, but it will be at 65 years old or after or plans to work as long as possible |
97 | No exact planned age and does not know at all when it will be |
98 | Has already stopped all work for pay or profit |
99 | Not applicable (not included in the filter) |
Blank | No answer |
243 | | Main labour status just after leaving last job or business | Everybody aged 50-69 and C24 = 3, 5 and (C67/70 – C11/14) > 49 |
1 | Unemployed |
2 | In retirement or early retirement |
3 | Long term sick or disabled |
4 | Other |
9 | Not applicable (not included in the filter) |
Blank | No answer |
244 | | Main reason for retirement or early retirement | C243 = 2 |
1 | Job lost |
2 | Had reached compulsory retirement age |
3 | Own health or disability |
4 | Care responsibilities |
5 | Problems related to job |
6 | Favourable financial arrangements to leave |
7 | Preference to stop working other than previous codes |
8 | Other |
9 | Not applicable (not included in the filter) |
Blank | No answer |
245 | | More flexible working time arrangements would have contributed to person staying longer at work/would contribute to person staying longer at work | Everybody aged 50-69 and ((C24 = 3, 5 and (C67/70 – C11/14) > 49) or (C24 = 1, 2)) |
1 | Yes |
2 | No |
9 | Not applicable (not included in the filter) |
Blank | No answer |
246 | | More opportunities to update skills would have contributed to person staying longer at work/would contribute to person staying longer at work | Everybody aged 50-69 and ((C24 = 3, 5 and (C67/70 – C11/14) > 49) or (C24 = 1, 2)) |
1 | Yes |
2 | No |
9 | Not applicable (not included in the filter) |
Blank | No answer |
247 | | Better health and/or safety at workplace would have contributed to person staying longer at work/would contribute to person staying longer at work | Everybody aged 50-69 and ((C24 = 3, 5 and (C67/70 – C11/14) > 49) or (C24 = 1, 2)) |
1 | Yes |
2 | No |
9 | Not applicable (not included in the filter) |
Blank | No answer |
248/249 | | Age at which person started to receive an individual retirement pension | Everybody aged 50-69 and ((C24 = 3, 5 and (C67/70 – C11/14) > 49) or (C24 = 1, 2)) |
| 2 digits |
97 | Does not receive an individual retirement pension even though is entitled to |
98 | Is not/not yet entitled to an individual retirement pension |
99 | Not applicable (not included in the filter) |
Blank | No answer |
250 | | Person receives an individual pension or individual benefits, other than a retirement pension and unemployment benefits, such as a disability pension, a sick pension or an early retirement scheme allowance | Everybody aged 50-69 and C24 = 3, 5 and (C67/70 – C11/14) > 49 |
1 | Yes, a disability pension or a sick pension |
2 | Yes, an early retirement scheme allowance |
3 | Yes, another individual benefit not elsewhere classified |
4 | Yes, combination of codes 1, 2 or 3 |
5 | No |
9 | Not applicable (not included in the filter) |
Blank | No answer |
251 | | Main financial incentive to stay at work | Everybody aged 50-69 and C24 = 1, 2 and C248/249 < 98 |
1 | To increase retirement pension entitlements |
2 | To provide sufficient household income |
3 | No financial incentive |
9 | Not applicable (not included in the filter) |
Blank | No answer |
252/253 | | Number of years spent working for pay or profit (during working life) | Everybody aged 50-69 and ((C24 = 3, 5 and (C67/70 – C11/14) > 49) or (C24 = 1, 2)) |
| 2 digits |
99 | Not applicable (not included in the filter) |
Blank | No answer |
254/259 | | Weighting factor for the LFS module 2006 (optional) | Everybody aged 50-69 and ((C24 = 3, 5 and (C67/70 – C11/14) > 49) or (C24 = 1, 2)) |
0000-9999 | Columns 254-257 contain whole numbers |
00-99 | Columns 258-259 contain decimal places |
--------------------------------------------------
