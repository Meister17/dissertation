Reference for a preliminary ruling from the Audiencia Nacional, Sala de lo Contencioso-Administrativo (España) of 15 May 2006 — Asociacion Profesional de Empresas de Reparto y Manipulado de Correspondencia. v Administración del Estado (Ministerio de Educación y Ciencia)
Referring court
Audiencia Nacional, Sala de lo Contencioso-Administrativo (National High Court, Chamber for Contentious Administrative Proceedings)
Parties to the main proceedings
Applicant: Asociacion Profesional de Empresas de Reparto y Manipulado de Correspondencia
Defendant: Ministerio de Educación y Ciencia
Question(s) referred
Are Articles 43 and 49 of the EC Treaty in conjunction with Article 86 thereof, as applied within the framework of the liberalisation of the postal services established by Directives 1997/67/EC [1] and 2002/39/EC [2] and within the framework of the rules governing public procurement introduced by the ad hoc Directives, to be interpreted as precluding an agreement whose subject-matter includes the provision of postal services, both reserved and unreserved and, therefore, liberalised, concluded between a department of the State Administration and a state company whose capital is wholly state-owned and which is furthermore, the universal postal service provider?
[1] Directive 97/67/EC of the European Parliament and the Council of 15 December 1997 on common rules for the development of the internal market of Community postal services and the improvement of quality of service. (OJ L 1998 15, p. 14).
[2] Directive 2002/39/EC of the European Parliament and the Council of 10 June 2002 amending Directive 97/67/EC with regard to the further opening to competition of Community postal services. (OJ L 2002 176, p. 21).
--------------------------------------------------
