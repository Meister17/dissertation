Euro exchange rates [1]
9 August 2006
(2006/C 187/01)
| Currency | Exchange rate |
USD | US dollar | 1,2879 |
JPY | Japanese yen | 148,17 |
DKK | Danish krone | 7,4606 |
GBP | Pound sterling | 0,67530 |
SEK | Swedish krona | 9,1655 |
CHF | Swiss franc | 1,5754 |
ISK | Iceland króna | 90,91 |
NOK | Norwegian krone | 7,9450 |
BGN | Bulgarian lev | 1,9558 |
CYP | Cyprus pound | 0,5760 |
CZK | Czech koruna | 28,093 |
EEK | Estonian kroon | 15,6466 |
HUF | Hungarian forint | 269,58 |
LTL | Lithuanian litas | 3,4528 |
LVL | Latvian lats | 0,6960 |
MTL | Maltese lira | 0,4293 |
PLN | Polish zloty | 3,8679 |
RON | Romanian leu | 3,5138 |
SIT | Slovenian tolar | 239,67 |
SKK | Slovak koruna | 37,355 |
TRY | Turkish lira | 1,8720 |
AUD | Australian dollar | 1,6846 |
CAD | Canadian dollar | 1,4420 |
HKD | Hong Kong dollar | 10,0163 |
NZD | New Zealand dollar | 2,0449 |
SGD | Singapore dollar | 2,0252 |
KRW | South Korean won | 1235,68 |
ZAR | South African rand | 8,6907 |
CNY | Chinese yuan renminbi | 10,2738 |
HRK | Croatian kuna | 7,2802 |
IDR | Indonesian rupiah | 11707,01 |
MYR | Malaysian ringgit | 4,718 |
PHP | Philippine peso | 66,134 |
RUB | Russian rouble | 34,3690 |
THB | Thai baht | 48,393 |
[1] Source: reference exchange rate published by the ECB.
--------------------------------------------------
