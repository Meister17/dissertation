Commission Regulation (EC) No 1389/2006
of 21 September 2006
determining the extent to which applications lodged in September 2006 for import licences under the regime provided for by tariff quotas for certain products in the pigmeat sector for the period 1 October to 31 December 2006 can be accepted
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 1458/2003 of 18 August 2003 opening and providing for the administration of tariff quotas for certain products in the pigmeat sector [1], and in particular Article 5(6) thereof,
Whereas:
(1) The applications for import licences lodged for the fourth quarter of 2006 are for quantities less than the quantities available and can therefore be met in full.
(2) The surplus to be added to the quantity available for the following period should be determined,
HAS ADOPTED THIS REGULATION:
Article 1
1. Applications for import licences for the period 1 October to 31 December 2006 submitted pursuant to Regulation (EC) No 1458/2003 shall be met as referred to in Annex I.
2. For the period 1 January to 31 March 2007, applications may be lodged pursuant to Regulation (EC) No 1458/2003 for import licences for a total quantity as referred to in Annex II.
Article 2
This Regulation shall enter into force on 1 October 2006.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 21 September 2006.
For the Commission
Jean-Luc Demarty
Director-General for Agriculture and Rural Development
[1] OJ L 208, 19.8.2003, p. 3. Regulation as amended by Regulation (EC) No 341/2005 (OJ L 53, 26.2.2005, p. 28).
--------------------------------------------------
ANNEX I
"—" : No application for a licence has been sent to the Commission.
Order No | Percentage of acceptance of import licences submitted for the period 1 October to 31 December 2006 |
09.4038 | 100 |
09.4039 | 100 |
09.4071 | — |
09.4072 | — |
09.4073 | — |
09.4074 | 100 |
--------------------------------------------------
ANNEX II
(t) |
Order No | Total quantity available for the period 1 January to 31 March 2007 |
09.4038 | 21494,498 |
09.4039 | 2780,0 |
09.4071 | 2251,5 |
09.4072 | 4620,75 |
09.4073 | 11300,25 |
09.4074 | 3966,950 |
--------------------------------------------------
