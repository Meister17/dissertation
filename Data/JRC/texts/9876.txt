Commission Regulation (EC) No 1644/2006
of 7 November 2006
amending Regulation (EC) No 1483/2006 as regards the quantities covered by the standing invitation to tender for the resale on the Community market of cereals held by the intervention agencies of the Member States
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1784/2003 of 29 September 2003 on the common organisation of the market in cereals [1], and in particular Article 6 thereof,
Whereas:
(1) Commission Regulation (EC) No 1483/2006 [2] opened standing invitations to tender for the resale on the Community market of cereals held by the intervention agencies of the Member States.
(2) In view of the situation on the Community markets for common wheat and barley and of the changes in demand for cereals in various regions in recent weeks, new quantities of cereals held in intervention should be made available in some Member States. The intervention agencies in the Member States concerned should therefore be authorised to increase the quantities of common wheat put out to tender by 51859 tonnes in Belgium, 44440 tonnes in Poland and 27020 tonnes in Latvia and in the case of barley by 100000 tonnes in France, 100000 tonnes in Germany, 75000 tonnes in Finland, 58004 tonnes in Sweden, 41927 tonnes in Poland, 28830 tonnes in Denmark, 24825 tonnes in the United Kingdom, 25787 tonnes in Lithuania, 22461 tonnes in Austria and 6340 tonnes in Belgium.
(3) Regulation (EC) No 1483/2006 should be amended accordingly.
(4) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Cereals,
HAS ADOPTED THIS REGULATION:
Article 1
Annex I to Regulation (EC) No 1483/2006 is replaced by the Annex hereto.
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 7 November 2006.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 270, 21.10.2003, p. 78. Regulation as amended by Commission Regulation (EC) No 1154/2005 (OJ L 187, 19.7.2005, p. 11).
[2] OJ L 276, 7.10.2006, p. 58. Regulation as amended by Regulation (EC) No 1586/2006 (OJ L 294, 25.10.2006, p. 21).
--------------------------------------------------
ANNEX
"ANNEX I
LIST OF INVITATIONS TO TENDER
"—" means no intervention stock of this cereal in this Member State."
Member State | Quantities of cereals made available for sale on the Community market (tonnes) | Intervention Agency Name, address and contact details |
Common wheat | Barley | Maize | Rye |
Belgique/België | 51859 | 6340 | — | — | Bureau d'intervention et de restitution belge/Belgisch Interventie- en Restitutiebureau Rue de Trèves/Trierstraat 82 B-1040 Bruxelles/Brussel Tél. (32-2) 287 24 78 Fax (32-2) 287 25 24 E-mail: webmaster@birb.be |
Česká republika | 0 | 0 | 0 | — | Státní zemědělský intervenční fond Odbor rostlinných komodit Ve Smečkách 33 CZ-110 00, Praha 1 Tél. (420) 222 87 16 67/222 87 14 03 Fax (420) 296 80 64 04 e-mail: dagmar.hejrovska@szif.cz |
Danmark | 174021 | 28830 | — | — | Direktoratet for FødevareErhverv Nyropsgade 30 DK-1780 København V Tél. (45) 33 95 88 07 Fax (45) 33 95 80 34 E-mail: mij@dffe.dk & pah@dffe.dk |
Deutschland | 350000 | 100000 | — | 336565 | Bundesanstalt für Landwirtschaft und Ernährung Deichmanns Aue 29 D-53179 Bonn Tél. (49-228) 6845-3704 Fax 1 (49-228) 6845-3985 Fax 2 (49-228) 6845-3276 E-Mail: pflanzlErzeugnisse@ble.de |
Eesti | 0 | 0 | — | — | Põllumajanduse Registrite ja Informatsiooni Amet Narva mnt 3, 51009 Tartu Tel: (372) 7371 200 Faks: (372) 7371 201 E-post: pria@pria.ee |
Tél. | (30-210) 21 24 787 (30-210) 21 24 754 |
Fax (30-210) 21 24 791 e-mail: ax17u073@minagric.gr |
España | — | — | — | — | Secretaría General de Intervención de Mercados (FEGA) Almagro, 33 E-28010 Madrid Tél. (34) 913 47 47 65 Fax (34) 913 47 48 38 Correo electrónico: sgintervencion@fega.mapa.es |
France | 0 | 100000 | — | — | Office national interprofessionnel des grandes cultures (ONIGC) 21, avenue Bosquet F-75326 Paris Cedex 07 Tél. (33-1) 44 18 22 29 et 23 37 Fax (33-1) 44 18 20 08 et 20 80 e-mail: m.meizels@onigc.fr et f.abeasis@onigc.fr |
Ireland | — | 0 | — | — | Intervention Operations, OFI, Subsidies & Storage Division, Department of Agriculture & Food Johnstown Castle Estate, County Wexford Tel. (353-53) 916 34 00 Fax (353-53) 914 28 43 |
Italia | — | — | — | — | Agenzia per le erogazioni in agricoltura — AGEA Via Torino, 45 I-00184 Roma Tel.: (39) 06 49 49 97 55 Fax: (39) 06 49 49 97 61 E-mail: d.spampinato@agea.gov.it |
Kypros/Kibris | — | — | — | — | |
Latvija | 27020 | 0 | — | — | Lauku atbalsta dienests Republikas laukums 2 Rīga, LV-1981 Tālr.: (371) 702 78 93 Fakss: (371) 702 78 92 E-pasts: lad@lad.gov.lv |
Lietuva | 0 | 25787 | — | — | The Lithuanian Agricultural and Food Products Market regulation Agency L. Stuokos-Guceviciaus Str. 9–12, Vilnius, Lithuania Tél. (370-5) 268 5049 Fax (370-5) 268 5061 e-mail: info@litfood.lt |
Luxembourg | — | — | — | — | Office des licences 21, rue Philippe II, Boîte postale 113 L-2011 Luxembourg Tél. (352) 478 23 70 Fax (352) 46 61 38 Télex: 2 537 AGRIM LU |
Magyarország | 350000 | 0 | 100000 | — | Mezőgazdasági és Vidékfejlesztési Hivatal Soroksári út. 22–24. H-1095 Budapest Tél. (36-1) 219 45 76 Fax (36-1) 219 89 05 e-mail: ertekesites@mvh.gov.hu |
Malta | — | — | — | — | |
Nederland | — | — | — | — | Dienst Regelingen Roermond Postbus 965 6040 AZ Roermond Nederland Tel. (31-475) 35 54 86 Fax (31-475) 31 89 39 E-mail: p.a.c.m.van.de.lindeloof@minlnv.nl |
Tél. | (43-1) 33151 258 (43-1) 33151 328 |
Fax | (43-1) 33151 4624 (43-1) 33151 4469 |
E-Mail: referat10@ama.gv.at |
Polska | 44440 | 41927 | 0 | — | Agencja Rynku Rolnego Biuro Produktów Roślinnych Ul. Nowy Świat 6/12 PL-00-400 Warszawa Tel. (48) 22 661 78 10 Faks (48) 22 661 78 26 e-mail: cereals-intervention@arr.gov.pl |
e-mail: | inga@inga.min-agricultura.pt edalberto.santana@inga.min-agricultura.pt |
|
Slovenija | — | — | — | — | Agencija Republike Slovenije za kmetijske trge in razvoj podeželja Dunajska 160, 1000 Ljubljana Tel. (386) 1 580 76 52 Faks (386) 1 478 92 00 E-pošta: aktrp@gov.si |
Slovensko | 0 | 0 | 100000 | — | Pôdohospodárska platobná agentúra Oddelenie obilnín a škrobu Dobrovičova 12 815 26 Bratislava Slovensko Tél. (421-2) 58 24 32 71 Fax (421-2) 53 41 26 65 e-mail: jvargova@apa.sk |
Faksi | (358-9) 1605 2772 (358-9) 1605 2778 |
Sähköposti: intervention.unit@mmm.fi |
Sverige | 172272 | 58004 | — | — | Statens Jordbruksverk SE-55182 Jönköping Tél. (46) 36 15 50 00 Fax (46) 36 19 05 46 e-mail: jordbruksverket@sjv.se |
United Kingdom | — | 24825 | — | — | Rural Payments Agency Lancaster House Hampshire Court Newcastle upon Tyne NE4 7YH Tel. (44-191) 226 58 82 Fax (44-191) 226 58 24 E-mail: cerealsintervention@rpa.gov.uk |
--------------------------------------------------
