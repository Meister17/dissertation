Publication of an application for registration pursuant to Article 6(2) of Regulation (EEC) No 2081/92 on the protection of geographical indications and designations of origin
(2005/C 137/11)
This publication confers the right to object to the application pursuant to Articles 7 and 12d of the abovementioned Regulation. Any objection to this application must be submitted via the competent authority in a Member State, in a WTO member country or in a third country recognized in accordance with Article 12(3) within a time limit of six months from the date of this publication. The arguments for publication are set out below, in particular under 4.6, and are considered to justify the application within the meaning of Regulation (EEC) No 2081/92.
SUMMARY
COUNCIL REGULATION (EEC) No. 2081/92
"FICO BIANCO DEL CILENTO"
EC No: IT/00282/ 14.03.2003
PDO (X) PGI ( )
This note is a summary produced for information. For full details, interested parties and in particular the producers of the products covered by the PDO and the PGI in question are invited to consult the full version of the specifications at national services and associations or at the competent services of the European Commission [1].
1. Competent department in the Member State
Name: | Ministero delle Politiche Agricole e Forestali |
Address: | Via XX Settembre, 20 — I-00187 ROME |
Tel. | (39-06) 481 99 68 |
Fax | (39-06) 42 01 31 26 |
e-mail: | qtc3@politicheagricole.it |
2. Group
2.1.Name: | Consorzio per la Tutela e la Valorizzazione del Fico Bianco del Cilento |
2.2.Address: | Via S. Marco, 118 — I-84043 Agropoli (Sa)Tel. (39-08) 28 72 27 99 |
2.3.Composition: | producers/processors (X) other ( ) |
3. Type of product
Class 1.6: Natural and processed fruit, vegetables and cereals.
4. Specification
(summary of requirements under Article 4(2))
Name: "Fico bianco del Cilento"
- figs with skin: uniform colour from light yellow to yellow;
- figs with skin that have been cooked: dark yellow;
- fichi mondi: very light in colour, bordering on white;
- flesh: doughy consistency with largely empty achenes, the centre almost entirely filled and of an amber yellow colour;
- size: number of dried figs with skin no greater than 70 per kg, number of fichi mondi no greater than 85 per kg;
- humidity: maximum permitted 26 %;
- sugar content — minimum value per 100 g of dry weight:
- glucose: 21.8 g
- fructose: 23.2 g
- saccharose: 0.1 g
flaws: the product should not be affected by damage from insects, mould or other sources; suberisation is acceptable to a maximum of 5 % of the surface of the fruit.
Filling with other ingredients such as almonds, walnuts, hazelnuts, fennel seeds or citrus fruit rind is permitted. The filling may not exceed 10 % of the total product as sold and the origin of those ingredients within the area of production defined in the following point 4.3 must be proved.
Dried figs may be packed loose and without preservatives in various types of packaging (cylindrical, with crown corks, circular, bags), weighing from 125 g to 1 kg. They may be packed in bulk, in baskets made from matter of vegetable origin, with a total weight of between 1 kg and 20 kg. The figs may be open and joined together on the flesh side in packs of 125 g to 1 kg; they may also be threaded onto wooden skewers and stuffed with the ingredients mentioned above. The packages may be decorated with bay leaves.
Geographical area: Cilento is the geographical area of Campania by the Tyrrhenian Sea between the mouth of the Sele in the gulf of Salerno and the mouth of the Bussento in the gulf of Policastro; it is bordered to the north by the Alburno and Cervati massifs. The municipalities involved in production are listed in the product specification.
Proof of origin:
Every phase of the production process should be monitored, in each case documenting the inputs and outputs. In this way, and through registration of producers, plots of land where the figs are grown, processors and packagers on special lists maintained by the regulatory body, the traceability of the product (from one end of the production chain to the other in both directions) is guaranteed.
All persons, both individuals and legal entities, on the relevant lists will be regulated by the regulatory body as established in the product specification and the relevant regulation plan. Whenever the regulatory body establishes non-conformity, even if it is only in one phase of the production chain, the product may not be sold under the protected designation of origin "Fico Bianco del Cilento".
Method of production:
The specification provides amongst other things that planting density may not exceed 700 trees per hectare. The fruit should be dried through direct exposure to the sun, possibly also using techniques such as protecting the fruits exposed to the sun with plastic tunnels with a minimum height of two metres and/or soaking the fruits in a solution of hot water and 2 % salt.
The production, processing and packing phases must take place within the production area described at point 4.3.
Link:
Soil and weather factors (the moderating effect of the sea, the barrier to cold winter air currents from the north-east provided by the Apennines, the fertility of the soil and an excellent rainfall pattern), together with the simplicity of growing methods resulting from millennia of experience and the tree's full adaptation to the natural environment in the area, contribute to giving Cilento dried figs organoleptic characteristics that are particularly appreciated by the consumer. Moreover, this tree is a characteristic feature of the rural landscape of Cilento.
Cultivation of the white fig in Cilento is very ancient and probably dates back to pre-Greek times when it was introduced into Italy as a result of the first trade voyages made by the civilisations of the near east. First Cato and later Varro described how dried figs were commonly used in Cilento and Lucania as a staple food by the labourers who worked in the fields.
Name: | IS.ME.CERT. |
Address: | Via G. Porzio Centro Direzionale Isola G1 scala C — I-80143 NAPOLI |
- name, business name and address of the packer;
- year of production of the figs;
- net weight at the point of origin;
- logo — whose details may be found in the product specification — relating to the image that must always be used with the Protected Designation of Origin. This depicts three stylised mature figs showing the typical progressive colouring of the fruit as it dries, on a green surface to evoke a field. Beside the fruit, on the right-hand side of the design, part of a stylised Greek column in the Doric style is visible. A strip of blue sky appears in the background with the sun and its rays on the left at the top, pointing to the figs.
- "Fico Bianco Del Cilento" PDO, certified as such, is the only type of fig contained in the product;
- users of the "Fico Bianco Del Cilento" PDO are authorised by the holders of the intellectual property right conferred by registration of the designation "Fico Bianco Del Cilento" PDO, organised in an association for the protection of the PDO recognised by the Ministry of Agricultural and Forestry Policy. This association will also enter the users in special registers and monitor the correct use of the protected designation. In the absence of a recognised association, the above tasks will be carried out by the Ministry of Agricultural and Forestry Policy as the national authority responsible for implementing Regulation (EEC) No 2081/92.
Where "Fico Bianco Del Cilento" PDO is used along with other ingredients, under existing regulations it may be mentioned only as one of the ingredients of the product containing it or into which it is processed.
National requirements: —
[1] European Commission, Directorate-General for Agriculture and Rural Development, Agricultural Products Quality Policy Unit, B-1049 Brussels.
--------------------------------------------------
