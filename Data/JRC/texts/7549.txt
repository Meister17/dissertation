COUNCIL DECISION
of 15 September 1986
on the conclusion of the Agreements in the form of an Exchange of Letters between the European Economic Community and the Kingdom of Norway concerning agriculture and fisheries
(86/557/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 113, thereof,
Having regard to the recommendation from the Commission,
Whereas it is necessary to approve the Agreements in the form of Exchange of Letters between the European Economic Community and the Kingdom of Norway concerning agriculture and fisheries, to take account of the accession of the Kingdom of Spain and the Portuguese Republic to the Community,
HAS DECIDED AS FOLLOWS:
Article 1
The Agreements in the form of an Exchange of Letters
between the European Economic Community and the
Kingdom of Norway concerning agriculture and fisheries is hereby approved on behalf of the Community.
The texts of the Exchange of Letters are attached to this Decision.
Article 2
The President of the Council is hereby authorized to designate the person empowered to sign the Agreements in order to bind the Community.
Done at Brussels, 15 September 1986.
For the Council
The President
G. HOWE
SPA:L666UMBE45.95
FF: 6UEN; SETUP: 01; Hoehe: 318 mm; 35 Zeilen; 1340 Zeichen;
Bediener: MARL Pr.: C;
Kunde: ................................
