COUNCIL REGULATION (EC) No 1541/98 of 13 July 1998 on proof of origin for certain textile products falling within Section XI of the Combined Nomenclature and released for free circulation in the Community, and on the conditions for the acceptance of such proof
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 113 thereof,
Having regard to the proposal from the Commission,
Whereas Council Regulation (EEC) No 616/78 of 20 March 1978 on proof of origin for certain textile products falling within Chapter 51 or Chapters 53 to 62 of the Common Customs Tariff and imported into the Community, and on the conditions for the acceptance of such proof (1), laid down conditions for preventing any deflection of trade or any abuse which would be detrimental to the application of the rules on textiles by means of a system for origin based on the requirement of presentation of a certificate of origin for some textile products and a declaration of origin on the invoice for others;
Whereas since the adoption of Regulation (EEC) No 616/78, changes have taken place in certain areas of the customs and textile fields; whereas, in particular, the textile products concerned are those listed in Section XI of the Combined Nomenclature classified in categories as defined in Annex I to Council Regulation (EEC) No 3030/93 of 12 October 1993 on common rules for imports of certain textile products from third countries (2);
Whereas the provisions relating to mutual assistance and administrative cooperation laid down in Articles 4, 4a and 4b of Regulation (EEC) No 616/78 are already covered by Council Regulation (EC) No 515/97 of 13 March 1997 on mutual assistance between the administrative authorities of the Member States and cooperation between the latter and the Commission to ensure the correct application of the law on customs and agricultural matters (3);
Whereas, in the interests of clarity, Regulation (EEC) No 616/78 should be re-cast;
Whereas in the interests of efficiency, management of the measures provided for in this Regulation should be entrusted to the Customs Code Committee,
HAS ADOPTED THIS REGULATION:
Article 1
1. The release for free circulation in the Community of textile products falling within Section XI of the Combined Nomenclature, listed in Annex I to Regulation (EEC) No 3030/93, shall be subject to proof of their origin in one of the forms and in accordance with the procedures set out in this Regulation.
2. The proof of origin referred to in paragraph 1 shall not be required for goods accompanied by a certificate of origin corresponding to the specimens and satisfying the conditions laid down for the purposes of the implementation of bilateral textile agreements, protocols or other arrangements.
3. Imports of a totally non-commercial nature are exempted from the provisions of this Regulation.
Article 2
The products listed in groups IA, IB, IIA and IIB of Annex I to Regulation (EEC) No 3030/93 must be accompanied by a certificate of origin complying with the conditions set out in Article 47 of Commission Regulation (EEC) No 2454/93 of 2 July 1993 laying down provisions for the implementation of Council Regulation (EEC) No 2913/92 establishing the Community Customs Code (4).
The certificates of origin may be accepted only if the products concerned fulfil the criteria for determining origin laid down by the relevant Community provisions in force.
Article 3
1. Products other than those referred to in Article 2 must be accompanied by a declaration by the exporter or supplier on the invoice or, if there is no invoice, on another commercial document relating to the said products, to the effect that the products in question originate in the third country where the declaration was drawn up and comply with the criteria for determining origin set out in the relevant Community provisions. The text of that declaration must correspond to the model set out in Annex I.
Notwithstanding the first subparagraph, certificates of origin may be issued for such products in conformity with Article 47 of Regulation (EEC) No 2454/93.
2. Notwithstanding the production of the declaration of origin referred to in paragraph 1, the competent authorities in the Community may, if there is cause for serious doubt, demand any additional proof with the object of ensuring that the declaration of origin complies with the criteria for determining origin set out in the relevant Community provisions.
3. Member States shall inform the Commission of any significant abuse or irregularity they detect in the use of declarations of origin.
At the request of a Member State or on the initiative of the Commission, presentation of a certificate of origin may, in accordance with the procedure laid down in Article 249 of Council Regulation (EEC) No 2913/92 of 12 October 1992 establishing the Community Customs Code (5), be demanded in respect of the products and countries concerned by such abuse or irregularity.
Article 4
In accordance with the procedure laid down in Article 249 of Council Regulation (EEC) No 2913/92, derogations from the obligation to present one of the proofs of origin referred to in Articles 2 and 3 may be granted to textile and clothing products that are not subject to specific Community commercial policy measures.
The provisions establishing derogations from the obligation to present a certificate of origin in accordance with Article 2 shall state, in particular, whether or not a declaration of origin in compliance with Article 3 must be presented for the products in question.
Article 5
EUR 1 movement certificates, EUR 2 forms, Form A certificates of origin and invoice declarations issued for the purpose of obtaining a tariff preference shall be accepted in place of the proof of origin referred to in Articles 2 and 3.
Article 6
1. Each consignment of goods must be accompanied by a certificate of origin or invoice declaration.
2. Member States may accept a certificate of origin relating to more than one consignment, provided that the products can be clearly identified on the said certificate and that the total quantities concerned are not greater than the quantities shown on the certificate.
Article 7
Where different criteria for determining origin are laid down for products falling within a single heading of the Combined Nomenclature or a single category listed in Annex I to Regulation (EC) No 3030/93, certificates or declarations of origin must contain a sufficiently detailed description of the goods to identify the criterion on the basis of which the certificate was issued or the declaration drawn up.
Article 8
1. Certificates of origin shall be issued and invoice declarations drawn up in the country in which the goods originate.
2. Where goods are not imported direct from the country of origin but arrive via another country, certificates of origin issued in the latter country shall be accepted subject to checking that such certificates are admissible on the same basis as those issued by the country of origin.
3. Paragraph 2 shall not apply if quantitative limits have been fixed or agreed for the products in question with respect to the country of origin.
Article 9
Member States shall notify the Commission of any relevant information relating to the application of this Regulation.
The Commission shall forward any such information to the other Member States.
The provisions of Regulation (EC) No 515/97 shall apply.
Article 10
The provisions for the implementation of this Regulation shall be laid down in accordance with the procedure laid down in Article 249 of Regulation (EEC) No 2913/92.
Article 11
Regulation (EEC) No 616/78 shall be repealed.
References to the repealed Regulation shall be construed as references to this Regulation and should be read in accordance with the correlation table in Annex II.
Article 12
This Regulation shall enter into force on the seventh day following that of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 13 July 1998.
For the Council
The President
W. SCHÜSSEL
(1) OJ L 84, 31. 3. 1978, p. 1. Regulation as last amended by Regulation (EEC) No 3626/83 (OJ L 360, 23. 12. 1983, p. 5).
(2) OJ L 275, 8. 11. 1993, p. 1. Regulation as last amended by Commission Regulation (EC) No 339/98 (OJ L 45, 16. 2. 1998, p. 1).
(3) OJ L 82, 22. 3. 1997, p. 1.
(4) OJ L 253, 11. 10. 1993, p. 1. Regulation as last amended by Regulation (EC) No 1427/97 (OJ L 196, 24. 7. 1997, p. 31).
(5) OJ L 302, 19. 10. 1992, p. 1. Regulation as last amended by Regulation (EC) No 82/97 (OJ L 17, 21. 1. 1997, p. 1).
ANNEX I
DECLARATION BY THE SUPPORTER OR SUPPLIER ON THE INVOICE OR, IF THERE IS NO INVOICE, ON ANOTHER COMMERCIAL DOCUMENT
Declaration of origin
>START OF GRAPHIC>
>END OF GRAPHIC>
ANNEX II
>TABLE>
