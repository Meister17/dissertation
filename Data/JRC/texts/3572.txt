Status of EC accession to UN/ECE regulations in the area of vehicle approval as of 31 December 2004
(2005/C 188/04)
The Commission publishes hereafter a table summarising the state of UN/ECE regulations as last amended (annexed in the 1958 Agreement concerning the adoption of uniform technical prescriptions for wheeled vehicles, equipment and parts which can be fitted and/or be used on wheeled vehicles and the conditions for reciprocal recognition of approvals granted on the basis of these prescriptions) to which the EC has acceded as of 31 December 2004.
Regulation number | Series of amendments | Supplements to the series | Short title of Regulation |
1 | 2 | — | Asymmetric headlamps (R2 and/or HS1) |
3 | 2 | 9 | Retro-reflecting devices |
4 | 0 | 10 | Rear registration-plate lamp |
5 | 2 | — | Asymmetric headlamps (Sealed Beam) |
6 | 1 | 12 | Direction indicators |
7 | 2 | 8 | End-outline marker-, front/rear position-,side-,stop- lamps (M, N and O) |
8 | 5 | — | Headlamps (H1, H2, H3, HB3, HB4, H7, H8, H9, HIR1, HIR2 and/or H11) |
10 | 2 | 2 | Electro-magnetic compatibility |
11 | 2 | — | Door latches and hinges |
12 | 3 | 3 | Behaviour of steering device under impact |
13 | 10 | — | Braking (categories M, N and O) |
13H | 0 | 3 | Braking (passenger cars) |
14 | 6 | 1 | Seat-belt anchorages |
16 | 4 | 16 | Seat belts |
17 | 7 | 1 | Seat strength |
18 | 3 | — | Anti-theft |
19 | 2 | 9 | Front fog lamps |
20 | 3 | — | Asymmetric headlamps (H4) |
21 | 1 | 3 | Interior fittings |
22 | 5 | 1 | Protective helmets and visors for motorcyclists |
23 | 0 | 10 | Reversing lamps |
24 | 3 | 2 | Diesel smoke and power |
25 | 4 | — | Head restraints |
26 | 3 | — | External projections |
27 | 3 | — | Advance warning triangles |
28 | 0 | 3 | Audible warning devices |
30 | 2 | 13 | Tyres (motor vehicles and their trailers) |
31 | 2 | — | Asymmetric headlamps (halogen sealed beam) |
34 | 2 | 1 | Fire risks |
37 | 3 | 25 | Filament lamps |
38 | 0 | 9 | Rear fog lamps |
39 | 0 | 4 | Speedometer |
43 | 0 | 8 | Safety glazing |
44 | 4 | — | Child restraint system |
45 | 1 | 4 | Headlamp cleaners |
46 | 2 | — | Rear-view mirrors |
48 | 2 | 10 | Installation of lighting and light-signalling devices (M, N and O) |
49 | 4 | — | Emissions (diesel, NG & LPG) |
50 | 0 | 7 | Front/rear position-, stop-lamps, direction indicators, rear registration-plate lamps (L) |
51 | 2 | 3 | Sound levels (M and N) |
53 | 1 | 5 | Installation of lighting and light-signalling devices (L3) |
54 | 0 | 16 | Tyres (commercial vehicles and their trailers) |
56 | 1 | — | Headlamps (mopeds) |
57 | 2 | — | Headlamps (motorcycles) |
58 | 1 | — | Rear underrun protective device |
59 | 0 | — | Replacement silencing systems |
60 | 0 | 2 | Driver operated controls — identification of controls, tell-tales and indicators (moped/motorcycles) |
62 | 0 | 1 | Anti-theft (moped/motorcycles) |
64 | 0 | 2 | Tyres (temporary use spare wheels/tyres) |
66 | 0 | — | Strength of superstructure (buses) |
67 | 1 | 5 | LPG equipment |
69 | 1 | 2 | Rear marking plates for slow moving vehicles |
70 | 1 | 3 | Rear marking plates for heavy and long vehicles |
71 | 0 | — | Field of vision, agricultural tractors |
72 | 1 | — | Headlamps (HS1) (motorcycles) |
73 | 0 | — | Lateral protection (goods vehicles and their trailers) |
74 | 1 | 3 | Installation of lighting and light-signalling devices (L1) |
75 | 0 | 11 | Tyres (motorcycles/mopeds) |
77 | 0 | 8 | Parking lamps |
78 | 2 | 3 | Braking (category L) |
79 | 1 | 3 | Steering equipment |
80 | 1 | 2 | Strength of seats and their anchorages (large passenger vehicles) |
81 | 0 | — | Rear-view mirrors (motorcycles/mopeds) |
82 | 1 | — | Headlamps (HS2 moped) |
83 | 5 | 5 | Emissions |
85 | 0 | 4 | Power — internal combustion and electric (M and N) |
86 | 0 | 2 | Installation of lighting and light-signalling devices (agricultural tractors) |
87 | 0 | 6 | Daytime running lamps |
89 | 0 | 1 | Speed limitation devices |
90 | 1 | 5 | Replacement brake linings and their assemblies |
91 | 0 | 7 | Side marker lamps |
93 | 1 | 3 | Front underrun protective devices |
96 | 1 | 2 | Diesel emission (agricultural tractors) |
97 | 1 | 2 | Alarm systems |
98 | 0 | 5 | Headlamps with gas-discharge light sources |
99 | 0 | 2 | Gas-discharge light sources |
100 | 0 | 1 | Electric vehicle safety |
101 | 0 | 6 | CO2 emission/fuel consumption (M1) and electric energy consumption and range (M1 and N1) |
102 | 0 | — | Close coupling devices |
103 | 0 | 2 | Replacement catalytic converters |
104 | 0 | 2 | Retro-reflective markings (heavy and long vehicles) |
105 | 3 | — | Carriage of dangerous goods — construction of vehicles |
106 | 0 | 3 | Tyres (agricultural vehicles) |
108 | 0 | 2 | Retreaded tyres (motor vehicles and their trailers) |
109 | 0 | 2 | Retreaded tyres (commercial vehicles and their trailers) |
110 | 0 | 3 | Compressed natural gas systems |
111 | 0 | 1 | Roll-over stability of tank vehicles (N and O) |
112 | 0 | 4 | Asymmetrical headlamps (filament lamps) |
113 | 0 | 3 | Symmetrical headlamps (filament lamps) |
114 | 0 | — | Replacement airbags |
115 | 0 | — | LPG-CNG retrofit systems |
[116] | 0 | — | Unauthorised use (anti-theft and alarm systems) |
[117] | 0 | — | Tyres — rolling resistance |
[118] | 0 | — | Fire resistance of interior materials |
[119] | 0 | — | Cornering lamps |
[120] | 0 | — | Power — internal combustion (agricultural tractors and mobile machinery) |
--------------------------------------------------
