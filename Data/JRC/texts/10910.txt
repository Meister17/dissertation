Judgment of the Court of First Instance of 12 July 2006 — Vitakraft-Werke Wührmann v OHIM — Johnson's Veterinary Products (VITACOAT)
(Case T-277/04) [1]
Parties
Applicant: Vitakraft-Werke Wührmann & Sohn GmbH & Co. KG (Bremen, Germany) (represented by: U. Sander, lawyer)
Defendant: Office for Harmonisation in the Internal Market (Trade Marks and Designs) (represented by: J. Novais Gonçalves, Agent)
Other party to the proceedings before the Board of Appeal of OHIM intervening before the Court of First Instance: Johnson's Veterinary Products Ltd (Sutton Coldfield, United Kingdom) (represented by: M. Edenborough, Barrister)
Re:
Action brought against the decision of the First Board of Appeal of OHIM, of 27 April 2004 (Case R 560/2003-1) regarding opposition proceedings between Vitakraft-Werke Wührmann & Sohn GmbH & Co. KG and Johnson's Veterinary Products Ltd
Operative part of the judgment
The Court:
1. Dismisses the action;
2. Orders the applicant to bear its own costs, and pay the costs incurred by the Office for Harmonisation in the Internal Market (Trade Marks and Designs) and the intervener before the Court of First Instance.
[1] OJ C 300, 4.12.2004.
--------------------------------------------------
