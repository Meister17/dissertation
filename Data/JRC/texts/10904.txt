Action brought on 26 April 2006 — Diy-Mar Insaat Sanayi ve Ticaret and Akar v Commission
Parties
Applicants: Diy-Mar Insaat Sanayi ve Ticaret Limited Sirketi (Cankaya/Ankara, Turkey) and M. Akar (Cankaya/Ankara, Turkey) (represented by: C. Sahin, lawyer)
Defendant: Commission of the European Communities
Form of order sought
- Reservation of rights to bring a claim for damages;
- Suspension, as a matter of priority, of implementation of the procedure with regard to the subject-matter of the present proceedings;
- Annulment of the procedure of 23 December 2005 with the number MK/KS/DELTUR/(2005)/SecE/D/1614 which is the subject-matter of the present proceedings;
- Order the defendant to pay the costs.
Pleas in law and main arguments
The applicants are contesting the decision of the European Commission Delegation in Turkey of 23 December 2005 which was addressed to the applicants with regard to the call for tenders in respect of the construction of educational establishments in the provinces of Diyarbakir and Siirt.
The applicants submit, inter alia, that their tender was the lowest and that the file in respect thereof was complete and therefore the contract should have been awarded to them. Furthermore, they submit that the contested decision infringes European Union law.
--------------------------------------------------
