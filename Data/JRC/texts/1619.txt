Decision of the EEA Joint Committee
No 111/2005
of 30 September 2005
amending Annex II (Technical regulations, standards, testing and certification) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex II to the Agreement was amended by Decision of the EEA Joint Committee No 76/2005 of 10 June 2005 [1].
(2) Commission Directive 2005/21/EC of 7 March 2005 adapting to technical progress Council Directive 72/306/EEC on the approximation of the laws of the Member States relating to the measures to be taken against the emission of pollutants from diesel engines for use in vehicles [2] is to be incorporated into the Agreement.
(3) Commission Directive 2005/27/EC of 29 March 2005 amending, for the purposes of its adaptation to technical progress, Directive 2003/97/EC of the European Parliament and of the Council, concerning the approximation of the laws of the Member States relating to the type-approval of devices for indirect vision and of vehicles equipped with these devices [3] is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
Chapter I of Annex II to the Agreement shall be amended as follows:
1. the following indent shall be added in point 12 (Council Directive 72/306/EEC):
- "— 32005 L 0021: Commission Directive 2005/21/EC of 7 March 2005 (OJ L 61, 8.3.2005, p. 25).";
2. the following shall be added in point 45zc (Directive 2003/97/EC of the European Parliament and of the Council):
", as amended by:
- 32005 L 0027: Commission Directive 2005/27/EC of 29 March 2005 (OJ L 81, 30.3.2005, p. 44)."
Article 2
The texts of Directives 2005/21/EC and 2005/27/EC in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 1 October 2005, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [4].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 30 September 2005.
For the EEA Joint Committee
The President
HSH Prinz Nikolaus von Liechtenstein
[1] OJ L 268, 13.10.2005, p. 5.
[2] OJ L 61, 8.3.2005, p. 25.
[3] OJ L 81, 30.3.2005, p. 44.
[4] No constitutional requirements indicated.
--------------------------------------------------
