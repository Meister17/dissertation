Commission Regulation (EC) No 218/2006
of 8 February 2006
amending Regulation (EC) No 1262/2001 laying down detailed rules for implementing Council Regulation (EC) No 1260/2001 as regards the buying in and sale of sugar by intervention agencies
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1260/2001 of 19 June 2001 on the common organisation of the markets in the sugar sector [1], and in particular Articles 7(5) and 9(3) thereof,
Whereas:
(1) Commission Regulation (EC) No 1262/2001 [2] lays down detailed rules for applying the intervention arrangements in the sugar sector. Experience has shown that adjustments are needed in order to simplify the arrangements and harmonise them with current practice for other products such as cereals and milk powder.
(2) Regulation (EC) No 1260/2001 guarantees prices and disposal only for sugar produced under quota. Access to intervention should therefore be restricted to manufacturers who are holders of a quota and who, in return for the price guarantee, are required to pay the minimum price for beet, while respecting the legitimate expectations of specialist traders who have already been granted the necessary approval to offer sugar for intervention.
(3) Recent experience in sugar intervention operations has shown that the criteria for the intervention storage of sugar and the approval of warehouses and silos should be made more stringent, in particular by giving the intervention agencies greater discretionary powers. It is also accepted that sugar can be stored for a very long time without risk of deterioration in quality where the storage conditions are right. Therefore the rules on the final dates for removal should be amended, whilst, to take account of legitimate expectations, the rules on sugar offered for intervention before a certain date should be maintained.
(4) Intervention procedures for sugar must be brought into line with those followed in other sectors such as cereals and milk powder, in particular as regards time limits for payment from the submission of tenders for intervention.
(5) Regulation (EC) No 1262/2001, as amended by Regulation (EC) No 1498/2005, lays down the requirements to be met by certain forms of packaging in which sugar bought in must be delivered. Certain clarifications are needed to ensure that this provision is properly applied.
(6) In order to facilitate the day-to-day management of intervention, in particular by creating uniform lots, the minimum quantity below which the intervention agency is not obliged to accept a tender should be increased.
(7) Regulation (EC) No 1262/2001 should therefore be amended accordingly.
(8) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Sugar,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 1262/2001 is hereby amended as follows:
1. Article 1 is amended as follows:
(a) paragraph 1 is replaced by the following:
"1. The intervention agency shall buy in the sugar only if it is offered by:
(a) a manufacturer with a production quota:
(b) a specialist sugar trader who is approved before 1 March 2006 by the Member State on whose territory they are established.";
(b) paragraph 3 is replaced by the following:
"3. Sugar may only be taken over where it is under quota and at the time of the offer is stored separately in an approved store or silo that has not been used most recently to store products other than sugar.";
2. Article 2 is amended as follows:
(a) the following subparagraph is added to paragraph 1:
"Additional conditions for approval of silos and warehouses may be laid down by the intervention agencies.";
(b) paragraph 2(c) is replaced by the following:
"(c) a total quantity, in the case of silos and warehouses for the bulk storage of sugar, not exceeding 50 times the daily removal capacity for bulk sugar that the applicant undertakes to place at the disposal of the intervention agency concerned for the removal of the sugar.";
(c) in the first subparagraph of paragraph 3, the second sentence is deleted;
3. Article 3 is amended as follows:
(a) paragraph 2 is replaced by the following:
"2. In order to offer sugar for intervention, specialised traders as referred to in paragraph 1 must have been approved by the Member State concerned. Approval shall be granted by the Member State on whose territory the trader’s establishment is located before 1 March 2006, to all applicants meeting or deemed able to meet the conditions laid down in paragraph 1 of this Article for the marketing year in question and, where applicable, any additional conditions which the Member State might attach to the grant of approval.";
(b) the second subparagraph of paragraphs 3 and 4 is deleted;
(c) paragraph 5 is replaced by the following:
"5. Approval shall be withdrawn if it is found that the person concerned no longer satisfies, or is no longer capable of satisfying, any of the conditions laid down in paragraphs 1 and 2. Withdrawal of approval may take place in the course of the marketing year. It shall not have retroactive effect.";
(d) paragraph 6 is replaced by the following:
"6. Approval granted or withdrawn under this Article shall be notified in writing to the person concerned.";
4. Article 4(1) is replaced by the following:
"1. Sugar offered to intervention shall meet the following requirements:
(a) it must have been produced within a quota during the marketing year in which the offer is made;
(b) it must be in crystal form.";
5. the second paragraph of Article 6 is replaced by the following:
"For the purposes of this Regulation "lot" means at least 2000 tonnes of sugar of uniform quality and packing, all of which is stored in the same place.";
6. Article 9 is amended as follows:
(a) paragraph 2 is replaced by the following:
"2. Storage contracts shall take effect five weeks after the date of acceptance of the offer referred to in Article 8(2) and shall expire at the end of the 10-day period during which removal of the quantity of sugar concerned is completed.";
(b) paragraph 4 is replaced by the following:
"4. The intervention agency shall bear the storage costs from the beginning of the 10-day period within which the contract takes effect as referred to in paragraph 2 until the expiry of the contract.";
(c) the second subparagraph of paragraph 5 is deleted;
7. Article 10(1) is replaced by the following:
"1. The transfer of ownership of sugar covered by a storage contract shall take place when the sugar in question is paid for.";
8. Article 16 is replaced by the following:
"Article 16
The intervention agency shall make payment no earlier than 120 days from the day on which the offer is accepted, provided the checks to verify the weight and quality of the offered lots have been conducted.";
9. Article 17(4) is replaced by the following:
"4. Sugar bought in shall be removed:
(a) in the case of offers accepted before 30 September 2005, not later than the end of the seventh month following that in which the offer was accepted, without prejudice to Article 34;
(b) in the case of offers accepted from 1 October 2005 to 9 February 2006, not later than 30 September 2006, without prejudice to Article 34;
(c) in the case of offers accepted from 10 February 2006, not later than the date of removal laid down in Article 34.";
10. Article 18 is amended as follows:
(a) the third subparagraph of paragraph 3 is replaced by the following:
"The flat-rate amount to cover the cost of the packing required or accepted by the intervention agency referred to in the second subparagraph of paragraph 2 shall be EUR 15,70 per tonne of sugar.";
(b) paragraph 4 is deleted;
11. the first sentence of Article 19(1) is replaced by the following:
"1. At the time of removal in the case of sugar as referred to in Article 17(4)(a) and (b) and within the time limit referred to in Article 16 in the case of sugar as referred to in Article 17(4)(c), four samples shall be taken for analysis either by experts approved by the competent authorities of the Member State concerned or by experts agreed upon by the intervention agency and the seller.";
12. Article 23(2) is replaced by the following:
"2. The price to be paid by the successful tenderer shall be:
(a) in the case of paragraph 1(a), the price indicated in the tender;
(b) in the case of paragraph 1(b) and (c), the price indicated in the terms of the invitation to tender."
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Union.
Article 1(1)(b) and (4) to (8) shall apply to sugar offered to intervention from the date of entry into force.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 8 February 2006.
For the Commission
Mariann Fischer Boel
Member of the Commission
[1] OJ L 178, 30.6.2001, p. 1. Regulation as last amended by Commission Regulation (EC) No 39/2004 (OJ L 6, 10.1.2004, p. 16).
[2] OJ L 178, 30.6.2001, p. 48. Regulation as amended by Regulation (EC) No 1498/2005 (OJ L 240, 16.9.2005, p. 39).
--------------------------------------------------
