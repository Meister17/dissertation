Commission Regulation (EC) No 575/2006
of 7 April 2006
amending Regulation (EC) No 178/2002 of the European Parliament and of the Council as regards the number and names of the permanent Scientific Panels of the European Food Safety Authority
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Regulation (EC) No 178/2002 of the European Parliament and of the Council of 28 January 2002 laying down the general principles and requirements of food law, establishing the European Food Safety Authority and laying down procedures in matters of food safety [1], and in particular the second paragraph of Article 28(4) thereof,
Whereas:
(1) The protection of plant health is an essential factor in the security of the food chain, and recent developments entail an increasing number of scientific assessments of plant health risks.
(2) The expertise currently available in the European Food Safety Authority’s Scientific Panel responsible for opinions concerning plant health, plant protection products and their residues allows for scientific assessments in the area of plant health to be carried out on a limited, case-by-case basis only. Accordingly, so as to deal with the increasing number of requests for scientific opinions in the area of plant health, the European Food Safety Authority has formally requested the Commission to establish a new permanent Scientific Panel bringing together a wide range of expertise in the various fields relevant to plant health, such as entomology, mycology, virology, bacteriology, botany, agronomy, plant quarantine and epidemiology of plant diseases.
(3) As a result of the creation of an additional Scientific Panel on plant health, the name of the "Panel on plant health, plant protection products and their residues" must be changed.
(4) Regulation (EC) No 178/2002 should therefore be amended accordingly.
(5) The measures provided for in this Regulation are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS REGULATION:
Article 1
Article 28(4) of Regulation (EC) No 178/2002 is amended as follows:
1. point (c) is replaced by the following:
"(c) the Panel on plant protection products and their residues;"
2. a new point (i) is added as follows:
"(i) the Panel on plant health."
Article 2
This Regulation shall enter into force on the 20th day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 7 April 2006.
For the Commission
Markos Kyprianou
Member of the Commission
[1] OJ L 31, 1.2.2002, p. 1. Regulation as amended by Regulation (EC) No 1642/2003 (OJ L 245, 29.9.2003, p. 4).
--------------------------------------------------
