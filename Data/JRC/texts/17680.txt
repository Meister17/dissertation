Council Decision
of 22 December 2004
on tackling vehicle crime with cross-border implications
(2004/919/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty on European Union, and in particular Article 30(1)(a) and Article 34(2)(c) thereof,
Having regard to the initiative of the Kingdom of the Netherlands,
Having regard to the Opinion of the European Parliament,
Whereas:
(1) An estimated 1,2 million motor vehicles are stolen each year in the Member States of the European Union.
(2) These thefts involve considerable damage amounting to at least EUR 15 billion per year.
(3) A large proportion of these vehicles, estimated at 30 to 40 %, are stolen by organised crime and then converted and exported to other States within and outside the European Union.
(4) Besides causing material damage, this is also seriously damaging to citizens' sense of justice and feeling of security. Vehicle crime may be accompanied by serious forms of violence.
(5) Consequently, attainment of the objective in Article 29 of the Treaty, to provide citizens with a high level of safety within an area of freedom, security and justice, is hampered.
(6) The Council has adopted a Resolution of 27 May 1999 on combating international crime with fuller cover of the routes used [1].
(7) Vehicle crime may also be linked internationally to other forms of crime, such as trafficking in drugs, firearms and human beings.
(8) Tackling vehicle crime is a matter for the law enforcement agencies of the Member States. However, a common approach involving — wherever practicable and necessary — cooperation between the Member States and law enforcement authorities of the Member States is necessary and proportional in order to address the cross-border aspects of this form of crime.
(9) Cooperation between law enforcement authorities and vehicle registration authorities is of particular importance, as is the provision of information to the relevant parties.
(10) Cooperation with Europol is equally important as Europol can provide analyses and reports on the matter.
(11) The European Police College offers police forces in the Member States, via the European Police Learning Net (EPLN), a library function in the field of vehicle crime for consulting information and expertise. Via its discussion function, EPLN also provides the possibility of exchanging knowledge and experience.
(12) The fight against vehicle crime will be intensified by an increase in the number of Member States acceding to the Treaty concerning a European Vehicle and Driving Licence Information System (EUCARIS) of 29 June 2000.
(13) A number of specific measures will need to be taken if vehicle crime with an international dimension is to be combated effectively,
HAS DECIDED AS FOLLOWS:
Article 1
Definitions
For the purposes of this Decision, the following definitions apply:
1. "Vehicle" shall mean any motor vehicle, trailer or caravan as defined in the provisions relating to the Schengen Information System (SIS).
2. "National competent authorities" shall mean any national authorities designated by the Member States for the purposes of this Decision, and may include, as appropriate, police, customs, border guards and judicial authorities.
Article 2
Objective
1. The objective of this Decision is to achieve improved cooperation within the European Union with the aim of preventing and combating cross-border vehicle crime.
2. Particular attention shall be given to the relationship between vehicle theft and the illegal car trade and forms of organised crime, such as trafficking in drugs, firearms and human beings.
Article 3
Cooperation between national competent authorities
1. Member States shall take the necessary steps, in accordance with national law, to enhance mutual cooperation between national competent authorities in order to combat cross-border vehicle crime, such as by means of cooperation agreements.
2. Specific attention shall be given to cooperation with respect to export control, taking into account respective competences in the Member States.
Article 4
Cooperation between competent authorities and the private sector
1. Member States shall take the necessary steps to organise periodic consultations, as appropriate, among national competent authorities, in accordance with national law, and may involve representatives of the private sector (such as holders of private registers of missing vehicles, insurers and the car trade) in such consultations with a view to coordination of information and mutual alignment of activities in this area.
2. Member States shall facilitate procedures, in accordance with national law, for a quick repatriation of vehicles released by national competent authorities following their seizure.
Article 5
Vehicle crime contact points
1. By 30 March 2005, Member States shall designate, within their law enforcement authorities, a contact point for tackling cross-border vehicle crime.
2. Member States shall authorise the contact points to exchange experience, expertise as well as general and technical information concerning vehicle crime on the basis of existing applicable legislation. Information exchange shall extend to methods and best practices of prevention of vehicle crime. Such exchanges shall not include exchanges of personal data.
3. Information concerning the designated national contact points, including subsequent changes, shall be notified to the General Secretariat of the Council for publication in the Official Journal of the European Union.
Article 6
Issuing alerts for stolen vehicles and registration certificates
1. Whenever a vehicle is reported stolen, Member States' competent authorities shall immediately enter a stolen vehicle alert in the SIS, in accordance with national law, and, where possible, in Interpol's Stolen Motor Vehicle database.
2. An alert in the search register shall, in accordance with national law, be immediately withdrawn by the Member State which issued it as soon as the reason for issuing an alert on the vehicle ceases to exist.
3. Whenever registration certificates are reported stolen, Member States' competent authorities shall immediately enter an alert thereon in the SIS, in accordance with national law.
Article 7
Registration
1. Each Member State shall ensure that its competent authorities shall take the necessary steps to prevent abuse and theft of vehicle registration documents.
2. The national vehicle registration authorities shall be informed by law enforcement authorities whether a vehicle, that is in the process of being registered, is known as having been stolen. Access to databases to that end shall take place with due respect to provisions of Community law.
Article 8
Preventing abuse of vehicle registration certificates
1. In order to prevent abuse of vehicle registration certificates, each Member State shall, in accordance with national law, ensure that its competent authorities take the necessary steps to recover a vehicle owner's or vehicle holder's registration certificate if the vehicle has been seriously damaged in an accident (total loss).
2. A registration certificate shall also be recovered, in accordance with national law, where, during a check by the law enforcement agency, it is suspected that there has been an infringement concerning the vehicle's identity markings, such as the vehicle identification number.
3. Registration certificates shall be returned only following examination and positive verification of the vehicle's identity, and in accordance with national law.
Article 9
Europol
Each Member State shall ensure that its law enforcement authorities keep Europol informed on vehicle crime perpetrators as necessary, within the scope of that mandate and tasks.
Article 10
Promotion of expertise and training
Member States shall take the necessary steps to ensure that national institutes responsible for the training of relevant law enforcement authorities promote in their curricula, where appropriate in cooperation with the European Police College, specialist training in the field of vehicle crime prevention and detection. Such training may include input from Europol, in accordance with its sphere of competence.
Article 11
Meeting of contact points and annual report to the Council
Vehicle crime contact points shall hold a meeting at least once a year under the auspices of the Member State holding the Presidency of the Council. Europol shall be invited to participate in that meeting. The Presidency shall report to the Council on the progress of relevant practical cooperation among law enforcement authorities.
Article 12
Evaluation
The Council shall evaluate the implementation of this Decision by 30 December 2007.
Article 13
Entry into effect
This Decision shall take effect on the day of its publication in the Official Journal of the European Union.
For those Member States in which the provisions of the Schengen acquis relating to the SIS have not yet been put into effect, the obligations of this Decision relating to the SIS shall take effect on the date on which those provisions start applying, as specified in a Council Decision adopted to that effect in accordance with the applicable procedures.
Done at Brussels, 22 December 2004
For the Council
The President
C. Veerman
--------------------------------------------------
[1] OJ C 162, 9.6.1999, p. 1.
