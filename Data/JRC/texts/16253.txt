COMMISSION DECISION of 26 March 1996 amending Commission Decision 95/190/EC laying down specific conditions for importing fishery and aquaculture products from the Philippines (Text with EEA relevance) (96/256/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 91/493/EEC of 22 July 1991 (1), laying down the health conditions for the production and the placing on the market of fishery products, as last amended by the Act of Accession of Austria, Finland and Sweden, and in particular Article 11 (5) thereof,
Whereas the list of establishments approved by the Philippines for importing fishery and aquaculture products into the Community has been drawn up in Commission Decision 95/190/EC (2); whereas this list may be amended following the communication of a new list by the competent authority in the Philippines;
Whereas the competent authority in the Philippines has communicated a new list adding 17 establishments and amending the date of 2 establishments;
Whereas it is necessary to amend the list of approved establishments accordingly;
Whereas the measures provided for in this Decision have been drawn up in accordance with the procedure laid down by Commission Decision 90/13/EEC (3),
HAS ADOPTED THIS DECISION:
Article 1
Annex B of Decision 95/190/EC is replaced by the Annex to this Decision.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 26 March 1996.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ No L 268, 24. 9. 1991, p. 15.
(2) OJ No L 123, 3. 6. 1995, p. 20.
(3) OJ No L 8, 11. 1. 1990, p. 70.
ANNEX
'ANNEX B
LIST OF APPROVED ESTABLISHMENTS
>TABLE>
