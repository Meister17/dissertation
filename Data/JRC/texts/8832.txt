DECISION OF THE COUNCIL AND OF THE REPRESENTATIVES OF THE GOVERNMENTS OF THE MEMBER STATES OF THE EUROPEAN COMMUNITIES, MEETING WITHIN THE COUNCIL of 20 September 1976 on practical measures of economic cooperation within the EC-Canada Joint Committee (76/755/ECSC, EEC, Euratom)
THE COUNCIL OF THE EUROPEAN COMMUNITIES AND THE REPRESENTATIVES OF THE GOVERNMENTS OF THE MEMBER STATES, MEETING WITHIN THE COUNCIL,
HEREBY DECIDE:
That practical measures of economic cooperation may not be initiated by the Community within the EC-Canada Joint Committee unless a common position has been agreed beforehand under the Community's usual procedures ; this proviso will apply in all circumstances, irrespective of the extent to which a common policy already exists for the sector concerned.
Done at Brussels, 20 September 1976.
The President
M. van der STOEL
