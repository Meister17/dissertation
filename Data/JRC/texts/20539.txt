COMMISSION DECISION of 30 November 1994 laying down special conditions for the import of live bivalve molluscs, echinoderms, tunicates and marine gastropods originating in Turkey (Text with EEA relevant) (94/777/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council directive 91/492/EEC of 15 July 1991 laying down the health conditions for the production and placing on the market of live bivalve molluscs (1), and in particular Article 9 thereof,
Whereas a group of Commission experts has conducted an inspection visit to Turkey to verify the conditions under which live bivalve molluscs, echinoderms, tunicates and marine gastropods are produced and placed on the market;
Whereas Turkish legislation makes the Ministry of Agriculture and Rural Affairs responsible for inspecting the health of live bivalve molluscs, echinoderms, tunicates and marine gastropods and monitoring the hygiene and sanitary conditions of production; whereas the same legislation empowers the Ministry of Agriculture and Rural Affairs to authorize or prohibit the harvesting of bivalve molluscs, echinoderms, tunicates and marine gastropods from certain zones;
Whereas the Ministry of Agriculture and Rural Affairs and its laboratories are capable of effectively verifying the application of the laws in force in Turkey;
Whereas the competent Turkish authorities have undertaken to communicate regularly and quickly to the Commission data on the presence of plankton containing toxins in the harvesting areas;
Whereas the competent Turkish authorities have provided official assurances regarding compliance with the requirements specified in Chapter V of the Annex to Directive 91/492/EEC and with requirements equivalent to those prescribed in that Directive for the classification of producting and relaying zones, approval of dispatch centres and public health control and production monitoring; whereas in particular any possible change in harvesting zones will be communicated to the Community;
Whereas Turkey is eligible for inclusion in the list of third countries fulfilling the conditions of equivalence referred to in Article 9 (3) (a) of Directive 91/492/EEC;
Whereas the procedure for obtaining a health certificate referred to in Article 9 (3) (b) (i) of Directive 91/492/EEC must include the definition of a model certificate, the language(s) in which it must be drawn up, the qualifications of the signatory and the health mark to be affixed to packaging;
Whereas, accordance with Article 9 (3) (b) (ii) of Directive 91/492/EEC, the production areas from which bivalve molluscs, echinoderms, tunicates and marine gastropods may be harvested and exported to the Community must be designated;
Whereas, in accordance with Article 9 (3) (c) of Directive 91/492/EEC, a list of the establishments from which the import of bivalve molluscs, echinoderms, tunicates and marine gastropods is authorized should be established; whereas such establishments may appear on the list only if they are officially approved by the competent Turkish authorities; whereas it is the duty of the competent Turkish authorities to ensure that the provisions laid down to this end in Article 9 (3) (c) of Directive 91/492/EEC are complied with;
Whereas the special import conditions apply without prejudice to decisions taken pursuant to Council Directive 91/67/EEC of 28 January 1991 concerning the animal health conditions governing the placing on the market of aquaculture animals and products (2);
Whereas the measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
The General Directorate of Protection and Control, of the Ministry of Agriculture and Rural Affairs shall be the competent authority in Turkey for verifying and certifying that live bivalve molluscs, echinoderms, tunicates and marine gastropods fulfil the requirements of Directive 91/492/EEC.
Article 2
Live bivalve molluscs, echinoderms, tunicates and marine gastropods originating in Turkey and intended for human consumption must meet the following conditions:
1. each consignment must be accompanied by a numbered original health certificate, duly completed, signed, dated and comprising a single sheet in accordance with the model in Annex A hereto;
2. consignments must originate in the authorized production areas listed in Annex B hereto;
3. they must be packed in sealed packages by approved dispatch centre included in the list in Annex C hereto;
4. each package must bear an indelible health mark containing at least the following information:
- country of dispatch: TURKEY,
- the species (common and scientific namens),
- the identification of the production area and the dispatch centre by their approval number,
- the date of packing, comprising at least the day and month.
Article 3
1. Certificates as referred to in Article 2 (1) must be drawn up in at least one official language of the Member State in which the check is carried out.
2. Certificates must bear the name, capacity and signature of the veterinarian of the Ministry of Agriculture and Rural Affairs and its official seal, in a coulour different from that of other endorsements.
Article 4
This Decision is addressed to the Member States.
Done at Brussels, 30 November 1994.
For the Commission
René STEICHEN
Member of the Commission
(1) OJ No L 268, 24. 9. 1991, p. 1.
(2) OJ No L 46, 19. 2. 1991, p. 1.
ANNEX A
HEALTH CERTIFICATE covering live
- bivalve molluscs (1)
- echinoderms (1)
- tunicates (1)
- marine gastropods (1)
originating in Turkey and intended for human consumption in the European Community
Reference No:
Country of dispatch: Turkey
Competent authority: Ministry of Agriculture and Rural Affairs, General Directorate of Protection and Control
I. Details identifying the products
- Species (scientific name):
- Code No (where available):
- Type of packaging:
- Number of packages:
- Net weight:
- Analysis report number (where available):
II. Origin of products
- Authorized production area:
- Name and official approval number of dispatch centre:
III. Destination of products
The products are dispatched
from:
(place of dispatch)
to:
(country and place of destination)
by the following means of transport:
Name and address of consignor:
Name of consignee and address at place of destination:
IV. Health attestation
The official veterinary inspector hereby certifies that the live products specified above:
1) were harvested, where necessary relayed, and transported in accordance with the health rules laid down in Chapters I, II and III of the Annex to Directive 91/492/EEC;
2) were handled, where necessary purified, and packaged in accordance with the health rules laid down in Chapter IV of the Annex to Directive 91/492/EEC;
3) have undergone controls in accordance with Chapter V of the Annex to Directive 91/492/EEC;
4) are in compliance with Cahpters V, VII, VIII, IX and X of the Annex to Directive 91/492/EEC and therefore fit for immediate human consumption.
Done at ,
(place) on
(date)
Official stamp
signature of official inspector
(name in capitals, capacity and qualifications of person signing)
(1) Delete where inapplicable.
ANNEX B
PRODUCTION AREAS FULFILLING THE REQUIREMENTS LAID DOWN IN CHAPTER I (1) (a) OF THE ANNEX TO DIRECTIVE 91/492/EEC "" ID="1">Karaburun> ID="2">I"> ID="1">Bosphorus> ID="2">II"> ID="1">Northern Marmara Sea> ID="2">III"> ID="1">Dardanelles> ID="2">IV"> ID="1">Saroz> ID="2">V"> ID="1">Ayvalik> ID="2">VI">
ANNEX C
LIST OF ESTABLISHMENTS APPROVED FOR EXPORT TO THE EUROPEAN COMMUNITY "" ID="1">Marsan - Eceabat> ID="2">110 - 31. 12. 1995"> ID="1">Dardanel Onentas - Çanakkale> ID="2">181 - 31. 12. 1995"> ID="1">Yavuz Mildon - Gelibolu> ID="2">183 - 31. 12. 1995"> ID="1">Real - Ayvalik> ID="2">203 - 31. 12. 1995"> ID="1">Artur I - Ayvalik> ID="2">205 - 31. 12. 1995"> ID="1">Tuna - Istanbul> ID="2">206 - 31. 12. 1995"> ID="1">Kerevitas Mersu Ancoker - Bursa> ID="2">301 - 31. 12. 1995">
