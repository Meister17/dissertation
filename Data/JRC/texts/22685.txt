COMMISSION DIRECTIVE of 2 April 1982 adapting to technical progress Council Directive 77/541/EEC on the approximation of the laws of the Member States relating to safety belts and restraint systems of motor vehicles (82/319/EEC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Directive 70/156/EEC of 6 February 1970 on the approximation of the laws of the Member States relating to the type-approval of motor vehicles and their trailers (1), as last amended by Directive 80/1267/EEC (2), and in particular Article 11 thereof,
Having regard to Council Directive 77/541/EEC of 28 June 1977 on the approximation of the laws of the Member States relating to safety belts and restraint systems of motor vehicles (3), as last amended by Directive 81/576/EEC (4), and in particular Article 10 thereof,
Whereas, in the interests of road safety, the Council, by Directive 81/576/EEC, extended the scope of Directive 77/541/EEC, which until then had been limited to vehicles of Category M1 as defined in Annex I to Directive 70/156/EEC, to cover all classes of motor vehicles ; whereas this extension of scope had been made possible by the technical progress which had been achieved in the meantime ; whereas implementation of this measure will, however, necessitate the alignment of the requirements and tests specified in the Directive with the enlarged scope ; whereas experience gained in applying the Directive has revealed a need for certain provisions to be brought more into line with actual test conditions;
Whereas the provisions of this Directive are in accordance with the opinion of the Committee on the Adaptation to Technical Progress of the Directives on the removal of technical barriers to trade in motor vehicles,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Annexes I, III, VI, VII, VIII, IX, X and XIV to Directive 77/541/EEC are hereby amended in accordance with the Annex to this Directive.
Article 2
1. With effect from 1 October 1982 no Member State may: (a) on grounds relating to safety belts and restraint systems: - refuse to grant EEC type-approval, to issue the certificate referred to in the last indent of Article 10 (1) of Directive 70/156/EEC or to grant national type-approval in respect of a type of motor vehicle of category M1, or
- prohibit the entry into service of vehicles of category M1,
if the safety belts and restraint systems of this type of vehicle or of these vehicles comply with the requirements of Directive 77/541/EEC, as amended by this Directive;
(b) - refuse to grant EEC component type-approval in respect of a type of safety belt or restraint system intended for installation in a vehicle of category M1 which complies with the requirements of Directive 77/541/EEC, as amended by this Directive,
- prohibit the placing on the market of such safety belts and restraint systems which bear the EEC component type-approval marks prescribed in this Directive.
2. With effect from 1 October 1983 Member States: (a) - shall no longer issue the certificate referred to in the last indent of Article 10 (1) of (1) OJ No L 42, 23.2.1970, p. 1. (2) OJ No L 375, 31.12.1980, p. 34. (3) OJ No L 220, 29.8.1977, p. 95. (4) OJ No L 209, 29.7.1981, p. 32. Directive 70/156/EEC in respect of a type of motor vehicle of category M1, or
- may refuse national type-approval in respect of a type of motor vehicle of category M1,
in which the safety belts and restraint system do not comply with the requirements of Directive 77/541/EEC, as amended by this Directive;
(b) may refuse to grant EEC component type-approval in respect of a type of safety belt or restraint system intended for installation in a vehicle of category M1 which does not comply with the requirements of Directive 77/541/EEC, as amended by this Directive.
3. With effect from 1 October 1990, Member States: - may prohibit the initial entry into service of motor vehicles of category M1 in which the safety belts and restraint systems do not comply with the requirements of Directive 77/541/EEC, as amended by this Directive,
- may prohibit the placing on the market of safety belts and restraint systems intended for installation in a vehicle of category M1 and which do not bear the EEC component type-approval marks prescribed in this Directive.
Article 3
With effect from 1 October 1982 no Member State may: (a) on grounds relating to safety belts and restraint systems: - refuse to grant EEC type-approval, to issue the certificate referred to in the last indent of Article 10 (1) of Directive 70/156/EEC or to grant national type-approval in respect of a type of motor vehicle in a category other than M1, or
- prohibit the entry into service of vehicles of such categories,
if the safety belts and restraint systems of this type of vehicle or of these vehicles comply with the requirements of Directive 77/541/EEC, as amended by this Directive;
(b) - refuse to grant EEC component type-approval in respect of a type of safety belt or restraint system intended for installation in vehicles in categories other than M1 which complies with the requirements of Directive 77/541/EEC, as amended by this Directive,
- prohibit the placing on the market of safety belts and restraint systems intended for installation in vehicles of such categories which bear the EEC component type-approval marks prescribed in this Directive.
Article 4
Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive not later than 30 September 1982. They shall forthwith inform the Commission thereof.
Article 5
This Directive is addressed to the Member States.
Done at Brussels, 2 April 1982.
For the Commission
Karl-Heinz NARJES
Member of the Commission
ANNEX Amendments to the Annexes to Directive 77/541/EEC
ANNEX I - SCOPE, DEFINITIONS, EEC COMPONENT TYPE-APPROVAL, INSTALLATION REQUIREMENTS
Item 0 shall read as follows:
"0. SCOPE
This Directive applies to safety belts and restraint systems which are designed for installation in vehicles conforming to the definition given in Article 9 and are intended for separate use, i.e. as individual fittings, by persons of adult build occupying forward-facing seats."
Item 1.1.3 shall read as follows:
"1.1.3. "three-point belt" means a belt which is essentially a combination of a lap strap and a diagonal strap;"
Items 1.1.4, 1.2.3, 1.6 and 1.7. In the French and Italian versions, the words "ensemble" and "complesso" shall be replaced by the words "ceinture" and "cintura" respectively.
Item 1.5. The last sentence shall read as follows:
"The adjusting device may be part of the buckle, the retractor or any other part of the safety belt;"
After Item 1.8.4.2, the following new Item 1.8.5 shall be added:
"1.8.5. "Emergency-locking retractor with higher response threshold" (type 4N) means a retractor of the type defined in Item 1.8.4, but having special properties as regards use in vehicles of categories M2, M3, N1, N2 and N3 (1);"
After Item 1.19, the following new Items 1.20 and 1.21 shall be added:
" 1.20 "recessed buckle-release button" : it must not be possible to release the buckle using a sphere having a diameter of 40 mm;
1.21 "non-recessed buckle-release button" : it must be possible to release the buckle using a sphere having a diameter of 40 mm.
"
Item 2.1.2.1. The first sentence shall read as follows:
"A technical description in triplicate of the belt type, providing details of the straps and rigid parts used, together with appropriate drawings and, in the case of retractors, instructions for installation thereof and installation of the sensors."
Items 2.1.2.2, 2.1.2.3 and 2.1.2.4 shall read as follows:
" 2.1.2.2. five samples of a belt type, and
2.1.2.3. a 10-m length of each type of strap used in the type of belt.
2.1.2.4. The technical service conducting the component type-approval tests shall be entitled to request further samples.
"
Item 2.4.1.2. In the French and Italian versions, the words "ensemble" and "complesso" shall be replaced by the words "ceinture" and "cintura" respectively. " (1) Vehicle categories defined in Annex I to Directive 70/156/EEC (OJ No L 42, 23.2.1970)."
Item 2.4.1.4. In the French and Italian versions, the words "ensemble" and "complesso" shall be replaced by the words "ceinture" and "cintura" respectively. In the English version, the words "plastic parts" shall be replaced by the words "parts made of plastics".
Item 2.4.2.2. The second sentence of the third paragraph shall read as follows:
"The surface to which this pressure is applied must have the following dimensions with the button in the release position and when projected into a plane perpendicular to the button's initial direction of motion:"
Item 2.4.2.3 shall read as follows:
"2.4.2.3. The buckle must be capable of withstanding repeated operation and, before the dynamic test referred to in Item 2.7.8, must undergo 5 000 opening and closing cycles under normal conditions of use."
Item 2.4.5.2.1.1 shall read as follows:
"2.4.5.2.1.1. it shall have locked when the vehicle deceleration reaches a value of 0 745 g in the case of type 4 or 0 785 g in the case of type 4N retractors;"
Item 2.4.5.2.1.2 shall read as follows:
"2.4.5.2.1.2. it shall not lock at strap accelerations, measured in the direction of unreeling, of less than 0 78 g in the case of type 4 or less than 1 70 g in the case of type 4N retractors;"
Item 2.4.5.2.1.3 shall read as follows:
"2.4.5.2.1.3. in addition, it shall not lock when the sensor is inclined at an angle not exceeding 12º in any direction from the installation position specified by the manufacturer;"
Item 2.4.5.2.1.4 shall read as follows:
"2.4.5.2.1.4. it shall lock when the sensor is inclined at an angle of not less than 27º in the case of type 4 or not less than 40º in the case of type 4N retractors in any direction from the installation position specified by the manufacturer;"
After Item 2.4.5.2.1.4, the following new Item 2.4.5.2.1.5 shall be added:
"2.4.5.2.1.5. if operation of the retractor is dependent on an external signal or energy source, the device shall ensure automatic locking of the retractor in the event of the failure or interruption of the signal or energy source."
Item 2.4.5.2.2 shall read as follows:
"2.4.5.2.2. when tested in accordance with Item 2.7.7.2, an emergency locking retractor with multiple sensitivity, including strap-sensitivity, shall comply with the specified requirements and also lock when strap acceleration measured in the direction of unreeling is not less than 1 75 g in the case of type 4 or not less than 2 70 g in the case of type 4N retractors;"
Item 2.6. In the French and Italian versions, the words "ensemble" and "complesso" shall be replaced by the words "ceinture" and "cintura" respectively.
Item 2.6.1.2 shall read as follows:
"2.6.1.2. The dynamic test shall be performed on two belt assemblies which have not previously been under load, except in the case of belt assemblies forming part of restraint systems, when the dynamic test shall be performed on the restraint systems intended for one group of seats which have not previously been under load. The buckles of the belts to be tested shall satisfy the requirements set out in Item 2.4.2.3." Item 2.6.1.2.1 shall read as follows:
"2.6.1.2.1. The belts shall have undergone the corrosion test defined in Item 2.7.2, after which the buckles shall be subjected to a further 500 opening and closing cycles under normal conditions of use."
Item 2.6.1.2.2 shall read as follows:
"2.6.1.2.2. In the case of safety belts with retractors, the latter shall have been subjected to the tests described in Items 2.4.5.1 or 2.4.5.2. If, however, a retractor has already been subjected to the corrosion test pursuant to the provisions of Item 2.6.1.2.1, this test need not be repeated."
Item 2.6.1.3. The following new item shall be added after Item 2.6.1.2:
" 2.6.1.3. During this test, the following requirements shall be met: 2.6.1.3.1. no part of a belt assembly or a restraint system securing the occupant shall break and no buckle or locking or displacement system shall unlock ; and
2.6.1.3.2. the forward displacement of the manikin shall be between 80 and 200 mm at pelvic level in the case of lap belts. In the case of other types of belt, the forward displacement shall be between 80 and 200 mm at pelvic level and between 100 and 300 mm at torso level. These displacements are the displacements in relation to the measurement points shown in Annex VIII, Figure 6.
"
Item 2.6.1.3 becomes Item 2.6.1.4.
Item 2.6.1.3.1 becomes Item 2.6.1.4.1.
Item 2.6.1.3.2 becomes Item 2.6.1.4.2 and shall read as follows:
"2.6.1.4.2. In vehicles where such devices are used, the displacement and locking systems enabling the occupants of all seats to leave the vehicle shall still be operable by hand after the dynamic test."
Item 2.6.2.1. The following shall be added:
"In the type 1 and type 2 procedures, the tensile-strength test shall be conducted on the strap samples only (Item 2.7.5). In type 3 procedures, the tensile-strength test shall be conducted on the strap and relevant rigid parts (Item 2.7.6)."
Item 2.6.2.2. In the table, the type 2 procedure shall not apply to adjusting devices.
Item 2.7.1.1 shall read as follows:
"2.7.1.1. Two belts or restraint systems are required for the buckle inspection, the low-temperature buckle test, the low-temperature test described in Item 2.7.6.4 where necessary, the buckle durability test, the belt corrosion test, the retractor operating tests and the buckle-opening test after the dynamic test. One of these two samples shall be used for the inspection of the belt or restraint system."
Item 2.7.1.2 shall read as follows:
"2.7.1.2. One belt or restraint system is required for the inspection of the buckle and the strength tests on the buckle, the attachment mountings, the belt adjusting devices and, where necessary, the retractors." Item 2.7.1.3 shall read as follows:
"2.7.1.3. Two belts or restraint systems are required for the inspection of the buckle, the micro-slip test and the abrasion test. The belt adjustment device operating test shall be conducted on one of the two samples."
Item 2.7.1.4 shall read as the present Item 2.7.1.5.
Items 2.7.1.5 and 2.7.1.6 shall be deleted.
Item 2.7.2. In the French and Italian versions, the words "ensemble" and "complesso" shall be replaced by the words "ceinture" and "cintura" respectively.
Item 2.7.3.2.1. The first sentence shall read as follows:
"The provisions of recommendation ISO/R 105-B 02-1978 shall apply."
Item 2.7.3.2.2 shall read as follows:
"2.7.3.2.2. The strap shall then be kept for a minimum of 24 hours at an air temperature of 20 ± 5 ºC and a relative humidity of 65 ± 5 %. If the test cannot be carried out immediately after the conditioning, the sample shall be kept in an hermetically sealed container until the start of the test. The tensile strength of the strap shall be determined within five minutes of its removal from the conditioning atmosphere or receptacle."
Item 2.7.3.6.4.2 shall read as follows:
"2.7.3.6.4.2. Type 2 procedure : in cases where the strap changes direction once on passing through a rigid part.
The angles which both straps ends make with each other must be as shown in Annex XII, Figure 2.
A permanent load of 0 75 daN must be applied. If the strap changes direction more than once on passing through a rigid part, the 0 75 daN load may be increased so as to achieve the prescribed 300 mm of strap movement through that rigid part."
Item 2.7.6. In the French and Italian versions, the words "ensemble" and "complesso" shall be replaced by the words "ceinture" and "cintura" respectively.
Item 2.7.6.1 shall read as follows:
"2.7.6.1. The buckle and strap adjusting device must be connected to the tensile-testing machine by their normal attachments and a load of 980 daN must be applied. If the buckle or adjusting device is part of the attachment or of the common component of a three-point strap, the buckle or adjusting device must be tested together with the attachment in accordance with Item 2.7.6.2, except in the case of retractors with a return pulley at the upper strap anchorage. In this case the test load must be 980 daN and the length of strap remaining on the reel at the moment of locking must be as close as possible to 450 mm."
Item 2.7.6.3 shall read as follows:
"2.7.6.3. Two samples of a belt assembly shall be placed in a low-temperature chamber at a temperature of - 10 ± 1 ºC for two hours. Immediately after being removed from the chamber, the mating parts of the buckle shall be locked together manually."
Item 2.7.6.4. In the English version, the words "plastic parts" shall be replaced by the words "parts made of plastics".
Item 2.7.7.2.2. In the second sentence, the value of "10 g" shall be replaced by "25 g".
Item 2.7.7.4 shall read as follows:
"2.7.7.4. Retracting force".
Item 2.7.7.4.1. The first sentence shall begin as follows:
"The retracting force must ...".
The second sentence shall end as follows:
"..., while the strap is being retracted at a speed of approximately 0 76 m per minute."
In the French and Italian versions, the words "ensemble" and "complesso" shall be replaced by the words "ceinture" and "cintura" respectively.
Item 2.7.8. In the French and Italian versions, the words "ensemble" and "complesso" shall be replaced by the words "ceinture" and "cintura" respectively.
Item 2.7.8.1.1 shall read as follows:
"2.7.8.1.1. If a safety belt forms part of an assembly which is the subject of an application for component type-approval as a restraint system, this safety belt must be mounted on that part of the vehicle structure to which it is normally fitted and that part must be attached to the test trolley as follows:"
Item 2.7.8.1.4. The last sentence shall read as follows:
"If the seat back is adjustable, it must be locked as specified by the manufacturer or, in the absence of any specification, locked in such a manner as to form an effective angle as close as possible to 25º in the case of vehicles in categories M1 and N1 and as close as possible to 15º in the case of vehicles of all other categories."
Item 2.7.8.1.5. "2.6.1.3.1" shall be replaced by "2.6.1.4.1".
Item 2.7.8.2 shall read as follows:
"2.7.8.2. The belt assembly must be attached to the manikin described in Annex VIII. A board 25 mm thick must be placed between the back of the manikin and the seat back. The belt must be firmly fastened around the manikin. The board must then be removed and the manikin so positioned that the whole length of its back is in contact with the seat back."
Item 2.7.8.6. "2.6.1.3.1" shall be replaced by "2.6.1.4.1".
Item 2.7.9. In the French and Italian versions, the words "ensemble" and "complesso" shall be replaced by the words "ceinture" and "cintura" respectively.
Item 2.7.9.2 shall read as follows:
"2.7.9.2. The belt assembly must be detached from the test-trolley without the buckle being opened. A direct tensile load of 30 daN must then be applied to the buckle. If the buckle is connected to a rigid part, account must be taken, when the force is applied, of the angle formed by the buckle and the rigid part during the dynamic test. At a speed of 400 ± 20 mm/min., a load must be applied to the geometric centre of the buckle-release button along a fixed axis running parallel to the initial direction of motion of the button. The buckle must be held in place by a rigid support when the force required to open the buckle is applied. The abovementioned load must not exceed the limit specified in Item 2.4.2.5. The point of contact of the test assembly must be spherical, with a radius of 2 75 ± 0 71 mm. It must have a smooth metal surface." Item 2.7.9.3 shall be deleted.
Item 2.7.9.4 becomes Item 2.7.9.3.
Item 2.7.9.5 becomes Item 2.7.9.4.
Item 2.7.10. "2.6.1.3.1" shall be replaced by "2.6.1.4.1". In the French and Italian versions, the words "ensemble" and "complesso" shall be replaced by the words "ceinture" and "cintura" respectively.
Items 2.8.1.1 and 2.8.1.2 (d). The expression "belt assemblies" shall be replaced by "belts and restraint systems".
Item 2.8.1.4.1 shall read as follows:
"2.8.1.4.1. All belts and restraint systems incorporating an emergency-locking retractor must be checked in respect of their compliance with: - the provisions of Item 2.4.5.2.1.1 in accordance with the test conditions defined in Item 2.4.5.2.3, or
- the provision set out in Item 2.4.5.2.1.4.
If the latter provision is fulfilled, at least 10 % of the production batch must also be tested in accordance with the provisions of Item 2.4.5.2.1.1."
Item 2.8.2.1. In the German version, the words "eine Anordnung" shall be replaced by "ein Exemplar". In the French version, the word "ensemble" shall be replaced by "exemplaire".
Item 3 shall read as follows:
"3. REQUIREMENTS CONCERNING INSTALLATION IN THE VEHICLE"
Item 3.2.2.2 shall read as follows:
"3.2.2.2. that the danger of a correctly positioned belt slipping from the shoulder of a wearer as a result of his/her forward movement is reduced to a minimum."
ANNEX III - EEC COMPONENT TYPE-APPROVAL MARKS
Item 1.1.1. Add "GR" for Greece.
The following new Item 1.1.4 shall be added:
"1.1.4. Belts fitted with a type 4N retractor shall also bear a symbol consisting of a rectangle with a vehicle of category M1 crossed out, indicating that the use of this type of retractor is prohibited in vehicles of that category." >PIC FILE= "T0021513">
After Item 2.3, the following new Item 2.4 shall be added:
"2.4 >PIC FILE= "T0021514">
ANNEX VI - EXAMPLE OF AN APPARATUS FOR TESTING THE DUST RESISTANCE OF RETRACTORS
The existing diagram shall be replaced by that shown below: >PIC FILE= "T0021515">
ANNEX VII - DESCRIPTION OF TROLLEY, SEAT, ANCHORAGES AND STOPPING DEVICE
Item 2. The first sentence shall read as follows:
"Except in the case of tests on restraint systems, the seat shall be of rigid construction and present a smooth surface."
Item 3 shall read as follows:
"3. ANCHORAGES
The anchorages shall be positioned as shown in Figure 1. The circular marks which correspond to the arrangement of the anchorages, show where the ends of the belt are to be connected to the trolley or to the load transducer, as the case may be. The anchorages for normal use are the points A, B and K if the strap length between the upper edge of the buckle and the hole for the attachment of the strap support is not more than 250 mm. Otherwise, the points A1 and B1 shall be used. The structure carrying the anchorages shall be rigid. The upper anchorage must not be displaced by more than 0 72 mm in the longitudinal direction when a load of 98 daN is applied to it in that direction. The trolley shall be so constructed that no permanent deformation shall occur in the parts bearing the anchorages during the test.
If a fourth anchorage is necessary in order to attach the retractor, this anchorage: - shall be located in the vertical longitudinal plane passing through K,
- shall enable the retractor to be tilted to the angle prescribed by the manufacturer,
- shall be located on the arc of a circle with centre K and with radius KB1 = 790 mm if the length between the upper strap guide and the strap outlet at the retractor is not less than 540 mm or, in all other cases, on the arc of a circle with centre K and radius 350 mm."
Item 4. The third paragraph shall read as follows:
"The dimensions of the various parts of this energy absorber are shown in Figures 2, 3 and 4. The characteristic values of the energy-absorbing material are given below. Immediately before each test, the tubes must be conditioned at a temperature of between 15 and 25 ºC for at least 12 hours without being used. The temperature of the stopping device during the dynamic testing of safety-belts and restraint systems must be the same as during the calibration test to within ± 2 ºC.
The requirements relating to the stopping device are set out in Annex IX. Any other device giving equivalent results is acceptable."
TABLE - CHARACTERISTIC VALUES OF THE ENERGY-ABSORBING MATERIAL
The first line shall read:
"- Shore hardness A : 95 ± 2 at 20 ± 5 ºC".
Figure 1 shall be replaced by the following: >PIC FILE= "T0021516">
ANNEX VIII - DESCRIPTION OF MANIKIN
Figure 6 shall be replaced by the following:
Figure 6 Manikin in sitting position as shown in Annex VII, Figure 1
>PIC FILE= "T0021517">
ANNEX IX - DESCRIPTION OF CURVE OF TROLLEY DECELERATION AS A FUNCTION OF TIME
The last sentence of the first paragraph to read as follows:
"The stopping distance during calibration of the stopping device shall be 400 ± 20 mm and the speed of the trolley shall be 50 ± 1 km/h."
Footnote (1) shall read as follows:
"(1) These requirements are in accordance with Recommendation ISO R 6478/1980."
ANNEX X - INSTRUCTIONS FOR USE
After Item 2 the following new Item 3 shall be added:
"3. In the case of safety belts fitted with a type 4N retractor, it shall be indicated in the installation instructions and on any packaging, that this belt is not suitable for installation in passenger vehicles with not more than nine seats, including that of the driver."
ANNEX XIV - CHRONOLOGICAL ORDER OF TESTS
The existing table shall be replaced by the following : >PIC FILE= "T0021518">
>PIC FILE= "T0021519">
