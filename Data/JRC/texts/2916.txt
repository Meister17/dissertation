First amending budget of the European Medicines Agency (EMEA) for 2005
(2005/651/EC, Euratom)
Pursuant to Article 26(2) of the Financial Regulation of the European Medicines Agency (EMEA), adopted by the Management Board on 10 June 2004, "the budget and amending budgets, as finally adopted, shall be published in the Official Journal of the European Union".
The first amending budget of the EMEA for 2005 was adopted by the Management Board on 14 July 2005 (MB/226064/2005).
(in EUR) |
Item | Description | Budget 2003 | Budget 2004 | Budget 2005 | Amendments | Revised budget 2005 |
Revenue
201 | Special contribution for orphan medicinal products | 2709700 | 4000000 | 3700000 | 1300000 | 5000000 |
520 | Revenue from bank interest | 450003 | 520000 | 625000 | 125000 | 750000 |
600 | Contributions to Community Programmes and revenue from services | 1117618 | 103 | p.m. | 250000 | 250000 |
| | | | 1675000 | |
| Total Budget | 84362701 | 99089103 | 110160000 | 1675000 | 111835000 |
Expenditure
2040 | Fitting out of premises | 1273314 | 1265000 | 1562000 | 125000 | 1687000 |
3011 | Evaluation of designated orphan medicinal products | 1999780 | 2837000 | 3700000 | 1300000 | 5000000 |
3050 | Community programmes | 1385034 | 103 | p.m. | 250000 | 250000 |
| | | | 1675000 | |
| Total Budget | 81691485 | 99089103 | 110160000 | 1675000 | 111835000 |
--------------------------------------------------
