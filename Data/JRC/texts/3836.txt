Decision of the EEA Joint Committee No 57/2005
of 29 April 2005
amending Annex II (Technical regulations, standards, testing and certification) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex II to the Agreement was amended by Decision of the EEA Joint Committee No 43/2005 of 11 March 2005 [1].
(2) Commission Regulation (EC) No 655/2004 of 7 April 2004 amending Regulation (EC) No 466/2001 as regards nitrate in foods for infants and young children [2] is to be incorporated into the Agreement.
(3) Commission Regulation (EC) No 683/2004 of 13 April 2004 amending Regulation (EC) No 466/2001 as regards aflatoxins and ochratoxin A in foods for infants and young children [3] is to be incorporated into the Agreement.
(4) Commission Regulation (EC) No 684/2004 of 13 April 2004 amending Regulation (EC) No 466/2001 as regards dioxins [4] is to be incorporated into the Agreement.
(5) Council Directive 2004/84/EC of 10 June 2004 amending Directive 2001/113/EC relating to fruit jams, jellies and marmalades and sweetened chestnut purée intended for human consumption [5] is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
Chapter XII of Annex II to the Agreement shall be amended as follows:
1. the following indents shall be added in point 54zn (Commission Regulation (EC) No 466/2001):
"— 32004 R 0655: Commission Regulation (EC) No 655/2004 of 7 April 2004 (OJ L 104, 8.4.2004, p. 48),
— 32004 R 0683: Commission Regulation (EC) No 683/2004 of 13 April 2004 (OJ L 106, 15.4.2004, p. 3),
— 32004 R 0684: Commission Regulation (EC) No 684/2004 of 13 April 2004 (OJ L 106, 15.4.2004, p. 6).";
2. the following shall be added in point 54zr (Council Directive 2001/113/EC):
", as amended by:
- 32004 L 0084: Council Directive 2004/84/EC of 10 June 2004 (OJ L 219, 19.6.2004, p. 8)."
Article 2
The texts of Regulations (EC) No 655/2004, (EC) No 683/2004, (EC) No 684/2004 and Directive 2004/84/EC in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 30 April 2005, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [6].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 29 April 2005.
For the EEA Joint Committee
The President
Richard Wright
[1] OJ L 198, 28.7.2005, p. 45.
[2] OJ L 104, 8.4.2004, p. 48.
[3] OJ L 106, 15.4.2004, p. 3.
[4] OJ L 106, 15.4.2004, p. 6.
[5] OJ L 219, 19.6.2004, p. 8.
[6] No constitutional requirements indicated.
--------------------------------------------------
