*****
COUNCIL DECISION
of 26 March 1985
concerning the conclusion of an exchange of letters extending the arrangement relating to clause 2 of the Agreement between the European Economic Community and the Socialist Republic of Romania on trade in sheepmeat and goatmeat
(85/211/EEC)
THE COUNCIL OF THE EUROPEAN
COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 113 thereof,
Having regard to the proposal from the Commission,
Whereas, in the voluntary restraint Agreement concluded with the Community in the sector of sheepmeat and goatmeat and of live sheep and goats, Romania undertook, by an exchange of letters, to limit its exports to certain Community markets regarded as sensitive areas; whereas, however, this undertaking did not extend beyond 31 March 1984;
Whereas the conditions which led to the designation of the said areas have not changed and the arrangement regarding the restriction of exports to these areas ought therefore to be extended;
Whereas the Commission has conducted negotiations to this end with Romania and these negotiations have resulted in agreement,
HAS DECIDED AS FOLLOWS:
Article 1
The exchange of letters extending the arrangements relating to clause 2 of the Agreement between the European Economic Community and the Socialist Republic of Romania on trade in sheepmeat and goatmeat is hereby approved on behalf of the Community.
The text of the exchange of letters is attached to this Decision.
Article 2
The President of the Council is hereby authorized to designate the person empowered to sign the exchange of letters referred to in Article 1 in order to bind the Community.
Done at Brussels, 26 March 1985.
For the Council
The President
F. M. PANDOLFI
