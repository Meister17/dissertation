Commission Regulation (EC) No 494/2003
of 18 March 2003
amending Council Regulation (EC) No 297/95 on the fees payable to the European Agency for the Evaluation of Medicinal Products
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 297/95 of 10 February 1995 on fees payable to the European Agency for the Evaluation of Medicinal Products(1), as amended by Regulation (EC) No 2743/98(2), and in particular Article 12 thereof,
Whereas:
(1) On the basis of Article 57(1) of Council Regulation (EEC) No 2309/93 of 22 July 1993, laying down Community procedures for the authorisation and supervision of medicinal products for human and veterinary use and establishing a European Agency for the Evaluation of Medicinal Products(3), as last amended by Commission Regulation (EC) No 649/98(4), the revenues of the Agency consist of a contribution and the fees paid by undertakings for obtaining and maintaining a Community marketing authorisation and for other services provided by the Agency.
(2) The Agency derives the majority of its revenue from fees.
(3) Since the 1998 reform of the fee system, the Agency has faced a relative decline in fee revenue due to inflation, while the prevailing market conditions have led to a rise in its expenditure.
(4) Figures from the Statistical Office of the European Communities (Eurostat) support the necessity for an increase in all fees by 10 % in order to arrive at the same level of purchasing power as was the case for the fees set in 1998.
(5) In view of the difficult economic conditions that the Agency has had to face and which are affecting its revenue and putting at risk the maintenance of its resource infrastructure and therefore its ability to carry out its tasks, an additional adjustment of all fees, except the annual fee, of 6 % is necessary.
(6) In view of the growing importance of post-authorisation surveillance activities in the work of the Agency and in order to improve the capacity of the Community to identify and manage risks arising from the use of, in particular, innovative medicines, the annual fee should be adjusted by 16 %.
(7) The general principles and overall structure of the fees will be reviewed in the context of a general reflection on the fee system, and in particular, on the basis of the modification of Council Regulation (EEC) No 2309/93, once adopted. These new amounts will therefore be applied during a limited period of time.
(8) The measures provided for in this Regulation are in accordance with the opinion of the Standing Committees,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 297/95 is amended as follows:
1. Article 3 is amended as follows:
(a) in the first subparagraph of paragraph 1(a), "ECU 200000" is replaced by "EUR 232000";
(b) in the second subparagraph of paragraph 1(a), "ECU 20000" is replaced by "EUR 23200";
(c) in the third subparagraph of paragraph 1(a), "ECU 5000" is replaced by "EUR 5800";
(d) in the first subparagraph of paragraph 1(b), "ECU 100000" is replaced by "EUR 116000";
(e) in the second subparagraph of paragraph 1(b), "ECU 20000" is replaced by "EUR 23200";
(f) in the third subparagraph of paragraph 1(b), "ECU 5000" is replaced by "EUR 5800";
(g) in the first subparagraph of paragraph 1(c), "ECU 50000" is replaced by "EUR 58000";
(h) in the second subparagraph of paragraph 1(c), "ECU 10000" is replaced by "EUR 11600";
(i) in the first subparagraph of paragraph 2(a), "ECU 5000" is replaced by "EUR 5800";
(j) in the first subparagraph of paragraph 2(b), "ECU 60000" is replaced by "EUR 69600";
(k) in paragraph 3, "ECU 10000" is replaced by "EUR 11600";
(l) in paragraph 4, "ECU 15000" is replaced by "EUR 17400";
(m) in paragraph 5, "ECU 5000" is replaced by "EUR 5800";
(n) in paragraph 6, "ECU 60000" is replaced by "EUR 75600".
2. Article 4 is amended as follows:
(a) in the first subparagraph, "ECU 10000" is replaced by "EUR 11600";
(b) in the second subparagraph, "ECU 40000" is replaced by "EUR 46400".
3. Article 5 is amended as follows:
(a) in the first subparagraph of paragraph 1(a), "ECU 100000" is replaced by "EUR 116000";
(b) in the second subparagraph of paragraph 1(a), "ECU 10000" is replaced by "EUR 11600";
(c) in the third subparagraph of paragraph 1(a), "ECU 5000" is replaced by "EUR 5800";
(d) in the fourth subparagraph of paragraph 1(a), "ECU 50000" is replaced by "EUR 58000" and "ECU 5000" is replaced by "EUR 5800";
(e) in the first subparagraph of paragraph 1(b), "ECU 50000" is replaced by "EUR 58000";
(f) in the second subparagraph of paragraph 1(b), "ECU 10000" is replaced by "EUR 11600";
(g) in the third subparagraph of paragraph 1(b), "ECU 5000" is replaced by "EUR 5800";
(h) in the fourth subparagraph of paragraph 1(b), "ECU 25000" is replaced by "EUR 29000" and "ECU 5000" is replaced by "EUR 5800";
(i) in paragraph 1(c), first subparagraph, "ECU 25000" is replaced by "EUR 29000";
(j) in paragraph 1(c), second subparagraph, "ECU 5000" is replaced by "EUR 5800";
(k) in paragraph 1(c), third subparagraph, "ECU 5000" is replaced by "EUR 5800";
(l) in the first subparagraph of paragraph 2(a) "ECU 5000" is replaced by "EUR 5800";
(m) in the first subparagraph of paragraph 2(b), "ECU 30000" is replaced by "EUR 34800";
(n) in the second subparagraph of paragraph 2(b), "ECU 5000" is replaced by "EUR 5800";
(o) in paragraph 3, "ECU 5000" is replaced by "EUR 5800";
(p) in paragraph 4, "ECU 15000" is replaced by "EUR 17400";
(q) in paragraph 5, "ECU 5000" is replaced by "EUR 5800";
(r) in paragraph 6, "ECU 20000" is replaced by "EUR 25200".
4. Article 6 is amended as follows:
(a) in the first subparagraph, "ECU 10000" is replaced by "EUR 11600";
(b) in the second subparagraph, "ECU 20000" is replaced by "EUR 23200".
5. Article 7 is amended as follows:
(a) in the first subparagraph of paragraph 1, "ECU 50000" is replaced by "EUR 58000";
(b) in the second subparagraph of paragraph 1, "ECU 15000" is replaced by "EUR 17400";
(c) in the first subparagraph of paragraph 2, "ECU 15000" is replaced by "EUR 17400".
6. Article 8 is amended as follows:
(a) in the first indent of paragraph 1, "ECU 60000" is replaced by "EUR 69600";
(b) in the second indent of paragraph 1, "ECU 30000" is replaced by "EUR 34800";
(c) in paragraph 2, "ECU 5000" is replaced by "EUR 5800".
Article 2
This Regulation shall enter into force on the day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 18 March 2003.
For the Commission
Erkki Liikanen
Member of the Commission
(1) OJ L 35, 15.2.1995, p. 1.
(2) OJ L 345, 19.12.1998, p. 3.
(3) OJ L 214, 24.8.1993, p. 1.
(4) OJ L 88, 24.3.1998, p. 7.
