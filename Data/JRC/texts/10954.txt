Action brought on 21 February 2006 — Marc Vereecken v Commission of the European Communities
Parties
Applicant(s): Marc Vereecken (Brussels, Belgium) (represented by: S. Rodrigues and A. Jaume)
Defendant: Commission of the European Communities
Form of order sought
- Annul the decisions of the Appointing Authority (AA) dismissing the applicant's complaint, together with the decision of the AA of 19 October 2004 and the pay slips for the months of February 2005 et seq. in so far as they change the applicant's grade to A*8, and the decision awarding merit points, priority points and compensation points for leave on personal grounds (CCP) adopted by the AA;
- Inform the AA of the consequences of annulling the contested decisions, and in particular: (i) the promotion of the applicant to grade A*10 (ex A6) with retroactive effect from 2001, or at least from 1 October 2004, when the applicant was re-employed; (ii) at least the promotion of the applicant to grade A*9 with effect from 1 October 2004; (iii) the award to the applicant of the points to which he is entitled with effect from his promotion, including merit points, priority points and transitional points for the CDRs 2003, 2004 and 2005;
- Order the defendant to pay compensation for the pecuniary loss suffered by the applicant as a result of the fact that he was not promoted to grade A*10 with effect from the 2001 promotion round or at least from 1 October 2004, including the consequences for his pension;
- Order the defendant to pay compensation for the non-pecuniary loss sustained by the applicant by reason of the failure to establish Staff Reports for 1997-1999 and the excessively late establishment of the Staff Reports for 1999-2001 and of the Career Development Reports (CDR) for 2003 and 2004;
- Order the Commission of the European Communities to pay the costs.
Pleas in law and main arguments
The applicant, an Official of the Commission under the former grade A7 was reemployed on 1 October 2004 under grade A*8 following a CCP of three years.
In his application he sets out four pleas in law, the first of which alleges the wrongful failure to establish or the late establishment of his Staff Reports for 1997-1999 and 1999-2001 and of his CDRs for 2003 and 2004.
By his second plea, the applicant submits that his classification under grade A*8 following his CCP was contrary to Article 6 of the Staff Regulations. That decision also infringes the principle of equivalence between the old and new career structures and the principle of equal treatment and the protection of legitimate expectations.
By his third plea, the applicant alleges that he was the victim of discrimination as compared with Officials in active employment in so far as, because he was on CCP, he did not benefit from the transitional measures which were applied to those Officials in relation to promotion.
Lastly, by his fourth plea, the applicant challenges the failure to take account of his seniority attained before and during his CCP, in particular as regards the award of compensation points, merit points and priority points. He thus considers that he was worse off than Officials on secondment.
--------------------------------------------------
