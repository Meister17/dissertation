Decision of the EEA Joint Committee No 45/2005
of 29 April 2005
amending Annex I (Veterinary and phytosanitary matters) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex I to the Agreement was amended by Decision of the EEA Joint Committee No 43/2005 of 11 March 2005 [1].
(2) Commission Decision 2004/517/EC of 21 June 2004 amending Commission Decision 2001/881/EC as regards the list of border inspection posts agreed for veterinary checks on animals and animal products from third countries [2] is to be incorporated into the Agreement.
(3) Commission Decision 2004/608/EC of 19 August 2004 amending Commission Decision 2001/881/EC as regards the list of border inspection posts agreed for veterinary checks on animals and animal products from third countries [3] is to be incorporated into the Agreement.
(4) This Decision is not to apply to Liechtenstein,
HAS DECIDED AS FOLLOWS:
Article 1
The following indents shall be added in point 39 (Commission Decision 2001/881/EC) in Part 1.2 of Chapter I of Annex I to the Agreement:
"— 32004 D 0517: Commission Decision 2004/517/EC of 21 June 2004 (OJ L 221, 22.6.2004, p. 18),
— 32004 D 0608: Commission Decision 2004/608/EC of 19 August 2004 (OJ L 274, 24.8.2004, p. 15)."
Article 2
The texts of Decisions 2004/517/EC and 2004/608/EC in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 30 April 2005, provided that all the notifications under Article 103(1) of the Agreement have been made to the EEA Joint Committee [4].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 29 April 2005.
For the EEA Joint Committee
The President
Richard Wright
[1] OJ L 198, 28.7.2005, p. 45.
[2] OJ L 221, 22.6.2004, p. 18.
[3] OJ L 274, 24.8.2004, p. 15.
[4] No constitutional requirements indicated.
--------------------------------------------------
