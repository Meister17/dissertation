Council Regulation (EC) No 1182/2005
of 18 July 2005
adopting autonomous and transitional measures to open a Community tariff quota for the import of live bovine animals originating in Switzerland
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 133 thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) Following the accession of the Czech Republic, Estonia, Cyprus, Latvia, Lithuania, Hungary, Malta, Poland, Slovenia and Slovakia to the European Union, the European Community and the Swiss Confederation agreed at the bilateral Summit on 19 May 2004 on the principle that the trade flows in accordance with the preferences granted previously under the bilateral arrangements between the new Member States and Switzerland should be maintained after the enlargement of the European Union. Parties therefore agreed to proceed with the adaptation of tariff concessions within the framework of the Agreement between the European Community and the Swiss Confederation on trade in agricultural products [1] (hereinafter called the Agreement), which entered into force on 1 June 2002. The adaptation of these concessions, which are listed in Annexes 1 and 2 of the Agreement, includes notably the opening of a Community tariff quota for the import of live bovine animals of a weight exceeding 160 kg.
(2) Parties have put in place autonomous measures, open-ended in the Swiss Confederation and until 30 June 2005 in the European Community, as regards imports of live bovine animals. However, the procedures for adopting bilaterally a decision to amend the Annexes 1 and 2 of the Agreement are not yet completed in the Swiss Confederation. In order to ensure that quota benefit is available until the entry into force of the said decision by the end of 2005, it is appropriate to open, on an autonomous and transitional basis, another tariff-quota concession until 31 December 2005 under the same conditions as those foreseen by Council Regulation (EC) No 1922/2004 of 25 October 2004 adopting autonomous and transitional measures to open a Community tariff quota for the import of live bovine animals originating in Switzerland [2].
(3) Detailed rules for the implementation of this Regulation and, in particular, the provisions required for quota management should be adopted in accordance with the provisions laid down in Article 32 of Council Regulation (EC) No 1254/1999 of 17 May 1999 on the common organisation of the market in beef and veal [3].
(4) To be eligible for the benefit of these tariff quotas, products should originate in Switzerland in conformity with the rules referred to in Article 4 of the Agreement.
(5) Given the urgency of the matter, it is imperative to grant an exception to the six-week period referred to in paragraph I(3) of the Protocol on the role of national parliaments in the European Union, annexed to the Treaty on European Union and to the Treaties establishing the European Communities,
HAS ADOPTED THIS REGULATION:
Article 1
1. A duty-free Community tariff quota is hereby opened on an autonomous and transitional basis for the period from the date of the entry into force of this regulation until 31 December 2005 for the import of 2300 heads of any live bovine animal originating in Switzerland weighting more than 160 kg, falling within CN code 01029041, 01029049, 01029051, 01029059, 01029061, 01029069, 01029071 or 01029079.
2. The rules of origin applicable to the products referred to in paragraph 1 shall be those provided for in Article 4 of the Agreement.
Article 2
The detailed rules for the implementation of this Regulation shall be adopted in accordance with the provisions laid down in Article 32 of Regulation (EC) No 1254/1999.
Article 3
This Regulation shall enter into force on the day following that of its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 18 July 2005.
For the Council
The President
M. Beckett
[1] OJ L 114, 30.4.2002, p. 132.
[2] OJ L 331, 5.11.2004, p. 7.
[3] OJ L 160, 26.6.1999, p. 21. Regulation as last amended by Commission Regulation (EC) No 1899/2004 (OJ L 328, 30.10.2004, p. 67).
--------------------------------------------------
