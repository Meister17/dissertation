Commission Regulation (EC) No 1965/2005
of 1 December 2005
establishing the standard import values for determining the entry price of certain fruit and vegetables
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Commission Regulation (EC) No 3223/94 of 21 December 1994 on detailed rules for the application of the import arrangements for fruit and vegetables [1], and in particular Article 4(1) thereof,
Whereas:
(1) Regulation (EC) No 3223/94 lays down, pursuant to the outcome of the Uruguay Round multilateral trade negotiations, the criteria whereby the Commission fixes the standard values for imports from third countries, in respect of the products and periods stipulated in the Annex thereto.
(2) In compliance with the above criteria, the standard import values must be fixed at the levels set out in the Annex to this Regulation,
HAS ADOPTED THIS REGULATION:
Article 1
The standard import values referred to in Article 4 of Regulation (EC) No 3223/94 shall be fixed as indicated in the Annex hereto.
Article 2
This Regulation shall enter into force on 2 December 2005.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 1 December 2005.
For the Commission
J. M. Silva Rodríguez
Director-General for Agriculture and Rural Development
[1] OJ L 337, 24.12.1994, p. 66. Regulation as last amended by Regulation (EC) No 386/2005 (OJ L 62, 9.3.2005, p. 3).
--------------------------------------------------
ANNEX
to Commission Regulation of 1 December 2005 establishing the standard import values for determining the entry price of certain fruit and vegetables
(EUR/100 kg) |
CN code | Third country code | Standard import value |
07020000 | 052 | 60,8 |
204 | 33,2 |
999 | 47,0 |
07070005 | 052 | 103,8 |
204 | 33,6 |
220 | 147,3 |
999 | 94,9 |
07099070 | 052 | 116,3 |
204 | 102,4 |
999 | 109,4 |
08052010 | 204 | 67,5 |
624 | 79,3 |
999 | 73,4 |
08052030, 08052050, 08052070, 08052090 | 052 | 69,1 |
624 | 107,8 |
999 | 88,5 |
08055010 | 052 | 66,9 |
220 | 47,3 |
999 | 57,1 |
08081080 | 052 | 78,2 |
388 | 68,7 |
400 | 93,6 |
404 | 89,9 |
720 | 60,6 |
999 | 78,2 |
08082050 | 052 | 101,8 |
400 | 92,7 |
720 | 49,3 |
999 | 81,3 |
[1] Country nomenclature as fixed by Commission Regulation (EC) No 750/2005 (OJ L 126, 19.5.2005, p. 12). Code "999" stands for "of other origin".
--------------------------------------------------
