DIRECTIVE 96/74/EC OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL of 16 December 1996 on textile names
THE EUROPEAN PARLIAMENT AND THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 100a thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the Economic and Social Committee (2),
Acting in accordance with the procedure laid down in Article 189b of the Treaty (3),
(1) Whereas Council Directive 71/307/EEC of 26 July 1971 on the approximation of the laws of the Member States relating to textile names (4) has been amended frequently and substantially; whereas, for reasons of clarity and rationality the said Directive should be consolidated;
(2) Whereas, if the provisions of the Member States with regard to the names, composition and labelling of textile products were to vary from one Member State to another, this would create hindrances to the proper functioning of the internal market;
(3) Whereas these hindrances can be eliminated if the placing of textile products on the market at Community level is subject to uniform rules; whereas it is therefore necessary to harmonize the names of textile fibres and the particulars appearing on labels, markings and documents which accompany textile products at the various stages of their production, processing and distribution; whereas the term 'textile fibre` is to be extended to include strips or tubes with an apparent width of not more than 5 mm, which are cut out from sheets manufactured by extrusion of the polymers listed under items 19 to 38 and 41 of Annex I, and subsequently drawn out lengthwise;
(4) Whereas provision should also be made in respect of certain products which are not made exclusively of textile materials but have a textile content which constitutes an essential part of the product or to which attention is specifically drawn by the producer, processor or trader; whereas in item 30 in Annex II there is no need to distinguish between the different types of polyamide or nylon, the agreed allowances for which are therefore to be brought into line;
(5) Whereas the tolerance in respect of 'other fibres`, which has already been laid down for pure products, is also to be applied to mixtures;
(6) Whereas, in order to attain the objective underlying national provisions in this field, labelling should be compulsory;
(7) Whereas, in cases where it is technically difficult to specify the composition of a product at the time of manufacture, any fibres known at that time may be stated on the label provided that they account for a certain percentage of the finished product;
(8) Whereas it is expedient, in order to avoid the differences of application that have become apparent in this connection in the Community, to specify the exact methods of labelling certain textile products consisting of two or more components, and also the components of textile products that need not be taken into account for purposes of labelling and analysis;
(9) Whereas textile products subject only to the requirements of inclusive labelling, and those sold by the metre or in cut lengths, are to be offered for sale in such a way that the consumer can fully acquaint himself with the particulars affixed to the overall packaging or the roll; whereas it is for the Member States to decide on the measures to be adopted for this purpose;
(10) Whereas the use of descriptions or names which enjoy particular prestige among users and consumers should be made subject to certain conditions;
(11) Whereas it was necessary to lay down methods for the sampling and analysis of textiles in order to exclude any possibility of objections to the methods used; whereas however, the provisional retention of the national methods currently in force does not prevent the application of uniform rules;
(12) Whereas Annex II to this Directive which sets out the agreed allowances to be applied to the anhydrous mass of each fibre during the determination by analysis of the fibre content of textile products, gives, in items 1, 2 and 3, two different agreed allowances for calculating the composition of carded or combed fibres containing wool and/or animal hair; whereas laboratories cannot always tell whether a product is carded or combed, and consequently inconsistent results can arise from the application of this provision during checks on the conformity of textile products carried out in the Community; whereas laboratories should therefore be authorized to apply a single agreed allowance in doubtful cases;
(13) Whereas it is not advisable in a separate Directive on textile products, to harmonize all the provisions applicable to such products;
(14) Whereas, Annexes III and IV, because of the exceptional nature of the items referred to therein, should also cover other products exempt from labelling, in particular 'disposable` products or products for which only inclusive labelling is required;
(15) Whereas the provisions necessary for the determination and the adaptation to technical progress of the methods of analysis are implementing measures of a strictly technical nature; whereas it is therefore necessary to apply to those measures, and to the measures for adapting Annexes I and II to this Directive to technical progress, the committee procedure already laid down in Article 6 of Directive 96/73/EC of the European Parliament and of the Council of 16 December 1996 on certain methods for the quantitative analysis of binary textile fibre mixtures (5);
(16) Whereas the provisions laid down in this Directive are in accordance with the opinion of the Committee for Directives relating to Textile Names and Labelling;
(17) Whereas this Directive is not to affect the obligations of the Member States concerning the deadlines for transposition of the Directives set out in Annex V, Part B,
HAVE ADOPTED THIS DIRECTIVE:
Article 1
Textile products may be marketed within the Community, either before or during their industrial processing or at any of the distribution stages, only where such products comply with the provisions of this Directive.
Article 2
1. For the purposes of this Directive, 'textile products` means any raw, semi-worked, worked, semi-manufactured, manufactured, semi-made-up or made-up products which are exclusively composed of textile fibres, regardless of the mixing or assembly process employed.
2. For the purpose of this Directive 'textile fibre` means:
- a unit of matter characterized by its flexibility, fineness and high ratio of length to maximum transverse dimension, which render it suitable for textile applications,
- flexible strips or tubes, of which the apparent width does not exceed 5 mm, including strips cut from wider strips or films, produced from the substances used for the manufacture of the fibres listed under items 19 to 41 in Annex I and suitable for textile applications; the apparent width is the width of the strip or tube when folded, flattened, compressed or twisted, or the average width where the width is not uniform.
3. The following shall be treated in the same way as textile products and shall be subject to the provisions of this Directive:
- products containing at least 80 % by weight of textile fibres,
- furniture, umbrella and sunshade coverings containing at least 80 % by weight of textile components; similarly, the textile components of multi-layer floor coverings, of mattresses and of camping goods, and warm linings of footwear, gloves, mittens and mitts, provided such parts or linings constitute at least 80 % by weight of the complete article,
- textiles incorporated in other products and forming an integral part thereof, where their composition is specified.
Article 3
1. The names and descriptions of fibres referred to in Article 2 are listed in Annex I.
2. Use of the names appearing in the table in Annex I shall be reserved for fibres whose nature is specified under the same item of that table.
3. None of these names may be used for any other fibre, whether on their own or as a root or as an adjective, in any language whatsoever.
4. The word 'silk` may not be used to indicate the shape or particular presentation in continuous yarn of textile fibres.
Article 4
1. No textile product may be described as '100 %`, 'pure` or 'all` unless it is exclusively composed of the same fibre; no similar term may be used.
2. A textile product may contain up to 2 % by weight of other fibres, provided this quantity is justified on technical grounds and is not added as a matter of routine. This tolerance shall be increased to 5 % in the case of textile products which have undergone a carding process.
Article 5
1. A wool product may be described as:
- 'lana virgen` or 'lana de esquilado`,
- 'ren, ny uld`,
- 'Schurwolle`,
- 'ðáñèÝíï ìáëëss`,
- 'fleece wool` or 'virgin wool`,
- 'laine vierge` or 'laine de tonte`,
- 'lana vergine` or 'lana di tosa`,
- 'scheerwol`,
- 'lã virgem`,
- 'uusi villa`,
- 'ren ull`,
only if it is composed exclusively of a fibre which has not previously been incorporated in a finished product, which has not been subjected to any spinning and/or felting processes other than those required in the manufacture of that product, and which has not been damaged by treatment or use.
2. By way of derogation from the provisions of paragraph 1, the names referred to therein may be used to describe wool contained in a fibre mixture when:
(a) all the wool contained in that mixture satisfies the requirements defined in paragraph 1;
(b) this wool accounts for not less than 25 % of the total weight of the mixture;
(c) in the case of a scribbled mixture, the wool is mixed with only one other fibre.
In the case referred to in this paragraph, the full percentage composition must be given.
3. The tolerance justified on technical grounds connected with manufacture shall be limited to 0,3 % of fibrous impurities in the case of the products referred to in paragraphs 1 and 2, including wool products which have undergone a carding process.
Article 6
1. A textile product composed of two or more fibres, one of which accounts for at least 85 % of the total weight, shall be designated:
- by the name of the latter fibre followed by its percentage by weight, or
- by the name of the latter fibre followed by the words '85 % minimum`, or
- by the full percentage composition of the product.
2. A textile product composed of two or more fibres, none of which accounts for as much as 85 % of the total weight, shall be designated by the name and percentage by weight of at least the two main fibres, followed by the names of the other constituent fibres in descending order of weight, with or without an indication of their percentage by weight. However:
(a) fibres which separately account for less than 10 % of the total weight of a product may be collectively designated by the term 'other fibres`, followed by the total percentage by weight;
(b) where the name of a fibre which accounts for less than 10 % of the total weight of a product is specified, the full percentage composition of that product shall be given.
3. Products having a pure cotton warp and a pure flax weft, in which the percentage of flax accounts for not less than 40 % of the total weight of the unsized fabric may be given the name 'cotton linen union` which must be accompanied by the composition specification 'pure cotton warp - pure flax weft`.
4. In the case of textile products intended for the end consumer, in the percentage compositions specified in paragraph 1, 2, 3 and 5:
(a) a quantity of extraneous fibres of up to 2 % of the total weight of the textile product shall be tolerated, provided that this quantity is justified on technical grounds and is not added as a matter of routine; this tolerance shall be increased to 5 % in the case of products which have undergone a carding process and shall be without prejudice to the tolerance referred to in Article 5 (3);
(b) a manufacturing tolerance of 3 % shall be permitted between the stated fibre percentages and the percentages obtained from analysis, in relation to the total weight of fibres shown on the label; such tolerance shall also be applied to fibres which, in accordance with paragraph 2, are listed in descending order of weight with no indication of their percentage. This tolerance shall also apply to Article 5 (2) (b).
On analysis, the tolerances shall be calculated separately; the total weight to be taken into account in calculating the tolerance referred to in (b) shall be that of the fibres of the finished product less the weight of any extraneous fibres found when applying the tolerance referred to in (a).
The addition of the tolerances referred to in (a) and (b) shall be permitted only if any extraneous fibres found by analysis, when applying the tolerance referred to in (a), prove to be of the same chemical type as one or more of the fibres shown on the label.
In the case of particular products for which the manufacturing process requires tolerances higher than those given in (a) and (b), higher tolerances may be authorized when the conformity of the product is checked pursuant to Article 13 (1) only in exceptional cases and where adequate justification is provided by the manufacturer. Member States shall immediately inform the Commission thereof.
5. The term 'mixed fibres` or the term 'unspecified textile composition` may be used for any product the composition of which cannot easily be stated at the time of manufacture.
Article 7
Without prejudice to the tolerances laid down in Article 4 (2), Article 5 (3) and Article 6 (4), visible, isolable fibres which are purely decorative and do not exceed 7 % of the weight of the finished product need not be mentioned in the fibre compositions provided for in Articles 4 and 6; the same shall apply to fibres (e.g. metallic fibres) which are incorporated in order to obtain an antistatic effect and which do not exceed 2 % of the weight of the finished product. In the case of the products referred to in Article 6 (3), such percentages shall be calculated not on the weight of the fabric but on the weight of the warp and that of the weft separately.
Article 8
1. Textile products within the meaning of this Directive shall be labelled or marked whenever they are put on the market for production or commercial purposes; this labelling or marking may be replaced or supplemented by accompanying commercial documents when the products are not being offered for sale to the end consumer, or when they are delivered in performance of an order placed by the State or by some other legal person governed by public law or, in those Member States where this concept is unknown, by an equivalent entity.
2. (a) The names, descriptions and particulars as to textile fibre content referred to in Articles 3 to 6 and in Annex I shall be clearly indicated in the commercial documents. This requirement shall, in particular, preclude the use of abbreviations in sales contracts, bills and invoices; however, a mechanized processing code may be used, provided that code is explained in the same document.
(b) The names, descriptions and particulars as to textile fibres content referred to in Articles 3 to 6 and in Annex I shall be indicated in clear, legible and uniform print when textile products are offered for sale or sold to the consumer, and in particular in catalogues and trade literature, on packagings, on labels and on markings.
Particulars and information other than those provided for by this Directive shall be quite separate. This provision shall not apply to trade marks or to the name of the undertaking which may be given immediately before or after particulars provided for by this Directive.
If, however, when a textile product is offered for sale or is sold to the consumer as referred to in the first subparagraph, a trade mark or a name of an undertaking is indicated which contains, on its own or as an adjective or as a root, one of the names listed in Annex I or a name liable to be confused therewith, the trade mark or the name of an undertaking must be immediately preceded or followed by the names, descriptions and particulars as to textile fibre content referred to in Articles 3 to 6 and in Annex I, in clear and legible print.
(c) Member States may require that, when textile products are offered for sale or are sold to the end consumer in their territory, their national languages should also be used for the labelling and marking required by this Article.
In the case of bobbins, reels, skeins, balls or any other small quantity of sewing, mending and embroidery yarns, the option provided for in the first subparagraph may be exercised by the Member States only in the case of inclusive labelling on packaging or displays. Without prejudice to the cases referred to in item 18 of Annex IV, individual items may be labelled in any one of the Community languages.
(d) Member States may not prohibit the use of descriptions or particulars other than those referred to in Articles 3, 4 and 5 which relate to characteristics of products where such descriptions or particulars are consistent with their fair trade practices.
Article 9
1. Any textile product composed of two or more components which have different fibre contents shall bear a label stating the fibre content of each component. Such labelling shall not be compulsory for components representing less than 30 % of the total weight of the product, excluding main linings.
2. Where to or more textile products have the same fibre content and normally form a single unit, they need bear only one label.
3. Without prejudice to the provisions of Article 12:
(a) the fibre composition of the following corsetry articles shall be indicated by stating the composition of the whole product or that of the components listed below either inclusively or separately:
- for brassières: the outside and inside fabric of the cups and back,
- for corsets: the front, rear and side stiffening panels,
- for corselets: the outside and inside fabric of the cups, the front and rear stiffening panels and the side panels.
The fibre composition of corsetry articles other than those listed in the first subparagraph shall be indicated by stating the composition of the whole product or, either inclusively or separately, the composition of the various components of the articles; such labelling shall not be compulsory for components representing less than 10 % of the total weight of the product.
The separate labelling of the various parts of the said corsetry articles shall be carried out in such a way that the end consumer can easily understand to which part of the product the particulars on the label refer;
(b) the fibre composition of etch-printed textiles shall be given for the product as a whole and may be indicated by stating, separately, the composition of the base fabric and that of the etched parts. These components must be mentioned by name;
(c) the fibre composition of embroidered textiles shall be given for the product as a whole and may be indicated by stating, separately, the composition of the base fabric and that of the embroidery yarn; these components must be mentioned by name; if the embroidered parts amount to less than 10 % of the surface area of the product, only the composition of the base fabric need be stated;
(d) the fibre composition of yarns consisting of a core and a cover made up of different fibres, and offered for sale as such to the consumer, shall be given for the product as a whole and may be indicated by stating the composition of the core and the cover separately; these components must be mentioned by name;
(e) the fibre composition of velvet and plush textiles, or of textiles resembling velvet or plush, shall be given for the whole product and, where the product comprises a distinct backing and a use-surface composed of different fibres, may be stated separately for these two parts, which must be mentioned by name;
(f) the composition of floor coverings and carpets of which the backing and the use-surface are composed of different fibres may be stated for the use-surface alone, which must be mentioned by name.
Article 10
1. By way of derogation from the provisions of Articles 8 and 9:
(a) in the case of textile products listed in Annex III which are at one of the stages referred to in Article 2 (1), Member States may not require any labelling or marking bearing the name or composition. However, the provisions of Articles 8 and 9 shall apply where such products bear a label or marking giving the name or composition, or a trade mark or name of an undertaking which incorporates, on its own or as an adjective or as a root, either one of the names listed in Annex I or a name liable to be confused therewith;
(b) where textile products listed in Annex IV are of the same type and composition, they may be offered for sale together under an inclusive label giving the composition particulars laid down by this Directive;
(c) the composition of textile products sold by the metre need by shown only on the length or roll offered for sale.
2. Member States shall take all necessary steps to ensure that the products referred to in (b) and (c) of paragraph 1 are offered for sale in such a way that the end consumer can fully acquaint himself with the composition of these products.
Article 11
Member States shall take all necessary measures to ensure that any information supplied when textile products are placed on the market cannot be confused with the names and particulars laid down by this Directive.
Article 12
For the purposes of applying Article 8 (1) and the other provisions of this Directive relating to the labelling of textile products, the fibre percentages referred to in Articles 4, 5 and 6 shall be determined without taking account of the following items:
1. for all textile products:
non-textile parts, selvedges, labels and badges, edgings and trimmings not forming an integral part of the product, buttons and buckles covered with textile materials, accessories, decorations, non-elastic ribbons, elastic threads and bands added at specific and limited points of the product and, subject to the conditions specified in Article 7, visible, isolable fibres which are purely decorative and antistatic fibres;
2. (a) for floor coverings and carpets: all components other than the use-surface;
(b) for upholstery fabrics: binding and filling warps and wefts which do not form part of the use-surface;
for hangings and curtains: binding and filling warps and wefts which do not form part of the right side of the fabric;
(c) for other textile products: base or underlying fabrics, stiffenings and reinforcements, inter-linings and canvas backings, stitching and assembly threads unless they replace the warp and/or weft of the fabric, fillings not having an insulating function and, subject to Article 9 (1), linings.
For the purposes of this provision:
- the base or underlying material of textile products which serve as a backing for the use-surface, in particular in blankets and double fabrics, and the backings of velvet or plush fabrics and kindred products shall not be regarded as backings to be removed.
- 'stiffenings and reinforcements` mean the yarns or materials added at specific and limited points of the textile products to strengthen them or to give them stiffness or thickness.
3. fatty substances, binders, weightings, sizings and dressings, impregnating products, additional dyeing and printing products and other textile processing products. In the absence of Community provisions, Member States shall take all necessary measures to ensure that these items are not present in quantities liable to mislead the consumer.
Article 13
1. Checks on whether the composition of textile products is in conformity with the information supplied in accordance with this Directive shall be carried out by the methods of analysis specified in the Directives referred to in paragraph 2.
For this purpose, the fibre percentages in Articles 4, 5 and 6 shall be determined by applying to the anhydrous mass of each fibre the appropriate agreed allowance laid down in Annex II, after having removed the items referred to in Article 12 (1), (2) and (3).
2. Separate directives will specify the methods of sampling and analysis to be used in Member States to determine the fibre composition of products covered by this Directive.
Article 14
1. No Member State may, for reasons connected with names or composition specifications, prohibit or impede the placing on the market of textile products which satisfy the provisions of this Directive.
2. The provisions of this Directive shall not preclude the application of the provisions in force in each Member State on the protection of industrial and commercial property, on indications of provenance, marks of origin and the prevention of unfair competition.
Article 15
The provisions of this Directive shall not apply to textile products which:
1. are intended for export to third countries;
2. enter Member States, under customs control, for transit purposes;
3. are imported from third countries for inward processing;
4. are contracted out to persons working in their own homes, or to independent firms that make up work from materials supplied without the property therein being transferred for consideration.
Article 16
1. The additions to Annex I and the additions and amendments to Annex II to this Directive which are necessary for adapting these Annexes to technical progress shall be adopted in accordance with the procedure laid down in Article 6 of Directive 96/73/EC.
2. The new method of quantitative analysis for binary and ternary mixtures other than those referred to in Directive 96/73/EC and Council Directive 73/44/EEC of 26 February 1973 on the approximation of the laws of the Member States relating to the quantitative analysis of ternary fibre mixtures (6) shall also be determined by that procedure.
3. The Committee referred to in Article 5 of Directive 96/73/EC is called the 'Committee for Directives relating to Textile Names and Labelling`.
Article 17
Member States shall communicate to the Commission the texts of the main provisions of national law which they adopt in the field covered by this Directive.
Article 18
The Directive listed in Annex V, part A are hereby repealed, without prejudice to the obligations of the Member States concerning the time limits for transposition set out in Annex V, part B.
References to the repealed Directives shall be construed as references to this Directive and should be read in accordance with the correlation table set out in Annex VI.
Article 19
This Directive is addressed to the Member States.
This Directive shall enter into force on the 20th day following that of its publication in the Official Journal of the European Communities.
Done at Brussels, 16 December 1996.
For the European Parliament
The President
K. HAENSCH
For the Council
The President
I. YATES
(1) OJ No C 96, 6. 4. 1994, p. 1.
(2) OJ No C 195, 18. 7. 1994, p. 9.
(3) Opinion of the European Parliament of 15 February 1995, (OJ No C 56, 6. 3. 1995, p. 53) Council common position of 26 February 1996 (OJ No C 196, 6. 7. 1996, p. 1), Decision of the European Parliament of 18 June 1996 (OJ No C 198, 8. 7. 1996, p. 25) and Council Decision of 7 October 1996.
(4) OJ No L 185, 16. 8. 1971, p. 16. Directive as last amended by Directive 87/140/EEC, (OJ No L 56, 26. 2. 1987, p. 24).
(5) See page 1 of this Official Journal.
(6) OJ No L 83, 30. 3. 1973, p. 1.
ANNEX I
>TABLE>
ANNEX II
>TABLE>
ANNEX III
PRODUCTS WHICH CANNOT BE MADE SUBJECT TO MANDATORY LABELLING OR MARKING (Article 10 (1) (a))
1. Sleeve-supporting armbands
2. Watch straps of textile materials
3. Labels and badges
4. Stuffed pan-holders of textile materials
5. Coffee cosy covers
6. Tea cosy covers
7. Sleeve protectors
8. Muffs other than in pile fabric
9. Artificial flowers
10. Pin cushions
11. Painted canvas
12. Textile products for base and underlying fabrics and stiffenings
13. Felts
14. Old made-up textile products, where explicitly stated to be such
15. Gaiters
16. Packagings, not new and sold as such
17. Felt hats
18. Containers which are soft and without foundation, saddlery, of textile materials
19. Travel goods of textile materials
20. Hand-embroidered tapestries, finished or unfinished, and materials for their production, including embroidery yarns, sold separately from the canvas and specially presented for use in such tapestries
21. Slide fasteners
22. Buttons and buckles covered with textile materials
23. Book covers of textile materials
24. Toys
25. Textile parts of footwear, excepting warm linings
26. Table mats having several components and a surface area of not more than 500 cm²
27. Oven gloves and cloths
28. Egg cosies
29. Make-up cases
30. Tobacco pouches of textile fabric
31. Spectacle, cigarette and cigar, lighter and comb cases of textile fabric
32. Protective requisites for sports with the exception of gloves
33. Toilet cases
34. Shoe-cleaning cases
35. Funeral items
36. Disposable articles, with the exception of wadding
For the purposes of this Directive, textile articles designed to be used once only or for a limited time, and the normal use of which precludes any restoring for subsequent use for the same or a similar purpose, are to be regarded as disposable
37. Textile articles subject to the rules of the European Pharmacopoeia and covered by a reference to those rules, non-disposable bandages for medical and orthopaedic use and orthopaedic textile articles in general
38. Textile articles including cordage, ropes and string, subject to item 12 of Annex IV, normally intended:
(a) for use as equipment components in the manufacture and processing of goods;
(b) for incorporation in machines, installations (e.g. for heating, air conditioning or lighting), domestic and other appliances, vehicles and other means of transport, or for their operation, maintenance or equipment, other than tarpaulin covers and textile motor vehicle accessories sold separately from the vehicle
39. Textile articles for protection and safety purposes such as safety belts, parachutes, life-jackets, emergency chutes, fire-fighting devices, bulletproof waistcoats and special protective garments (e.g. protection against fire, chemical substances or other safety hazards)
40. Air-supported structures (e.g. sports halls, exhibition stands or storage facilities), provided that particulars of the performances and technical specifications of these articles are supplied
41. Sails
42. Animal clothing
43. Flags and banners
ANNEX IV
PRODUCTS FOR WHICH ONLY INCLUSIVE LABELLING OR MARKING IS OBLIGATORY (Article 10 (1) (b))
1. Floorcloths
2. Cleaning cloths
3. Edgings and trimmings
4. Passementerie
5. Belts
6. Braces
7. Suspenders and garters
8. Shoe and boot laces
9. Ribbons
10. Elastic
11. New packaging sold as such
12. Packing string and agricultural twine; string, cordage and ropes other than those falling within item 38 of Annex III (1).
13. Table mats
14. Handkerchiefs
15. Bun nets and hair nets
16. Ties and bow ties for children
17. Bibs; washgloves and face flannels
18. Sewing, mending and embroidery yarns presented for retail sale in small quantities with a net weight of 1 gram or less
19. Tape for curtains and blinds and shutters
(1) For the products falling within this item and sold in cut lengths, the inclusive labelling shall be that of the reel. The cordage and ropes falling within this item include those used in mountaineering and water sports.
ANNEX V
PART A
REPEALED DIRECTIVES (referred to in Article 18)
- Council Directive 71/307/EEC (OJ No L 185, 16. 8. 1971, p. 16), and its successive amendments:
- Council Directive 75/36/EEC (OJ No L 14, 20. 1. 1975, p. 15)
- Council Directive 83/623/EEC (OJ No L 353, 15. 12. 1983, p. 8).
- Commission Directive 87/140/EEC (OJ No L 56, 26. 2. 1987, p. 24).
PART B
TIME LIMITS FOR TRANSPOSITION
>TABLE>
ANNEX VI
>TABLE>
