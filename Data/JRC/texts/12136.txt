[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 4.12.2006
COM(2006) 753 final
2006/0257 (CNS)
Proposal for a
COUNCIL DECISION
on the signature of a Protocol between the European Community, Switzerland and Liechtenstein to the Agreement between the European Community and the Swiss Confederation concerning the criteria and mechanisms for establishing the State responsible for examining a request for asylum lodged in a Member State, in Switzerland or Liechtenstein
Proposal for a
COUNCIL DECISION
on the conclusion of a Protocol between the European Community, Switzerland and Liechtenstein to the Agreement between the European Community and the Swiss Confederation concerning the criteria and mechanisms for establishing the State responsible for examining a request for asylum lodged in a Member State, in Switzerland or Liechtenstein
(presented by the Commission)
EXPLANATORY MEMORANDUM
I. INTRODUCTION
On 26 October 2004, the European Community signed an Agreement with the Swiss Confederation regarding criteria and mechanisms for establishing the State responsible for examining a request for asylum lodged in a Member State or in Switzerland (the "Dublin/Eurodac agreement with Switzerland").
That agreement provides in its Article 15 for the possibility for Liechtenstein to accede to the agreement. The accession of Liechtenstein has been negotiated and a draft protocol on the accession of Liechtenstein to the Dublin/Eurodac agreement with Switzerland has been initialled.
In accordance with Articles 1 and 2 of the Protocol on the position of Denmark annexed to the Treaty of the European Union and the Treaty establishing the European Community, Denmark is not participating to Council Regulation (EC) N° 343/2003 establishing the criteria and mechanisms for determining the Member State responsible for examining an asylum application lodged in one of the Member States by a third-country national (“the Dublin Regulation”) and Council Regulation (EC) N° 2725/2000 concerning the establishment of “Eurodac” for the comparison of fingerprints for the effective application of the Dublin Convention (“the Eurodac Regulation”)[1] .
The Dublin/Eurodac agreement with Switzerland provides for the possibility that Denmark may ask to participate. In that event, the Agreement provides that the contracting parties, with the consent of Denmark, set out the conditions for Denmark's participation in a protocol to the Agreement.
By letter dated 8 November 2004, the Kingdom of Denmark asked to participate in the Dublin/Eurodac Agreement with Switzerland. As Liechtenstein will now accede to this Agreement, it is appropriate that the participation of Denmark should be established in relation to both Switzerland and Liechtenstein.
Following the authorisation given by the Council to the Commission on 27.2.2006, negotiations were held with Liechtenstein and Switzerland. On 21.6.2006, negotiations were finalised and the draft protocol on the participation of Denmark in the Dublin/Eurodac Agreement with Switzerland and Liechtenstein was initialled[2].
The negotiations were held and the draft protocol has been initialled on the basis that Liechtenstein would become a contracting party after the conclusion of the protocol on its accession to the Dublin/Eurodac agreement with Switzerland. It has therefore to be born in mind that Liechtenstein can only become a contracting party to the protocol on the participation of Denmark when it has ratified the protocol on its accession to the Dublin/Eurodac agreement with Switzerland[3].
The attached proposals are the legal bases for the decisions on the signature and the conclusion of the Protocol. The legal basis of this Protocol is Article 63(1)a, in conjunction with the first sentence of the first subparagraph of Article 300(2) of the Treaty establishing the European Community.
The Council will decide by qualified majority. The European Parliament will be consulted on the conclusion of the Protocol, in accordance with Article 300(3) of the Treaty establishing the European Community.
II. RESULTS OF THE NEGOTIATIONS
The Commission considers that the objectives set by the Council in its negotiating directives were attained and that the draft Protocol is acceptable to the Community.
The final content of the Protocol can be summarised as follows:
- It makes the Dublin and Eurodac regulations and their implementing rules applicable to the relations between Denmark on the one hand and the Confederation of Switzerland and the Principality of Liechtenstein on the other. It also renders future amendments or new implementing measures applicable to these relations.
- It gives Switzerland and Liechtenstein the right to present written pleadings or observations to the Court of Justice when a court in Denmark applies to the Court of Justice for a preliminary ruling on the interpretation of a provision of the Agreement between the European Community and Denmark.
- It provides for a conciliation mechanism in the event of disagreement between Denmark on the one hand and Switzerland or Lichtenstein on the other hand on its interpretation or application.
- It lays down provisions relating to the end of its applicability.
III. CONCLUSIONS
In the light of the above considerations, the Commission proposes that the Council:
- decides that the Protocol be signed on behalf of the Community and authorises the President of the Council to appoint the person duly empowered to sign on behalf of the Community;
- approves, after consulting the European Parliament, the attached Protocol to the Agreement between the European Community and the Swiss Confederation concerning the criteria and mechanisms for establishing the State responsible for examining a request for asylum lodged in a Member State or in Switzerland, including the protocol concerning the accession of Liechtenstein.
Proposal for a
COUNCIL DECISION
on the signature of a Protocol between the European Community, Switzerland and Liechtenstein to the Agreement between the European Community and the Swiss Confederation concerning the criteria and mechanisms for establishing the State responsible for examining a request for asylum lodged in a Member State, in Switzerland or Liechtenstein
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 63(1)(a) thereof, in conjunction with the first sentence of the first subparagraph of Article 300(2),
Having regard to the proposal from the Commission,[4]
Whereas:
1. Following the authorisation given to the Commission on 27 February 2006, negotiations with the Swiss Confederation and the Principality of Liechtenstein of a Protocol on the participation of Denmark in the Agreement between the European Community and the Swiss Confederation concerning the criteria and mechanisms for establishing the State responsible for examining a request for asylum lodged in a Member State or in Switzerland, including the protocol concerning the accession of Liechtenstein, have been finalised;
2. Subject to its conclusion at a later date, the Protocol initialled in Brussels on 21 June 2006 should be signed;
3. The United Kingdom and Ireland, in accordance with Article 3 of the Protocol on the position of the United Kingdom and Ireland annexed to the Treaty on the European Union and the Treaty establishing the European Community will take part in the adoption and application of this Decision;
4. In accordance with Articles 1 and 2 of the Protocol on the position of Denmark annexed to the Treaty on the European Union and the Treaty establishing the European Community, Denmark is not participating to the adoption of this decision and is not bound by or subject to its application;
HAS DECIDED AS FOLLOWS:
Sole Article
Subject to its conclusion at a later date, the President of the Council is aut horised to appoint the person empowered to sign, on behalf of the Community, the Protocol between the European Community, the Swiss Confederation and the Principality of Liechtenstein to the Agreement between the European Community and the Swiss Confederation concerning the criteria and mechanisms for establishing the State responsible for examining a request for asylum lodged in a Member State or in Switzerland, including the protocol concerning the accession of Liechtenstein
The text of the Protocol is attached to this Decision.
Done at Brussels,
For the Council
The President
2006/0257 (CNS)
Proposal for a
COUNCIL DECISION
on the conclusion of a Protocol between the European Community, Switzerland and Liechtenstein to the Agreement between the European Community and the Swiss Confederation concerning the criteria and mechanisms for establishing the State responsible for examining a request for asylum lodged in a Member State, in Switzerland or Liechtenstein
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 63(1)(a) thereof, in conjunction with the first sentence of the first subparagraph of Article 300(2) and the first subparagraph of Article 300(3) thereof,
Having regard to the Commission proposal,[5]
Having regard to the opinion of the European Parliament[6]
Whereas:
5. Following the authorisation given to the Commission on 27 February 2006, negotiations with the Swiss Confederation and the Principality of Liechtenstein of a Protocol on the participation of Denmark in the Agreement between the European Community and the Swiss Confederation concerning the criteria and mechanisms for establishing the State responsible for examining a request for asylum lodged in a Member State or in Switzerland, including the protocol concerning the accession of Liechtenstein, have been finalised
6. According to Decision .../....../EC of the Council of [..........], and subject to its final conclusion at a later date, this Protocol has been signed, on behalf of the European Community, on .......... 2006;
7. This Protocol should be approved;
8. The United Kingdom and Ireland, in accordance with Article 3 of the Protocol on the position of the United Kingdom and Ireland annexed to the Treaty on the European Union and the Treaty establishing the European Community, are taking part in the adoption and application of this Decision;
9. In accordance with Articles 1 and 2 of the Protocol on the position of Denmark annexed to the Treaty on the European Union and the Treaty establishing the European Community, Denmark is not participating to the adoption of this decision and is not bound by or subject to its application;
HAS DECIDED AS FOLLOWS:
Article 1
The Protocol to the Agreement between the European Community and the Swiss Confederation concerning the criteria and mechanisms for establishing the State responsible for examining a request for asylum lodged in a Member State or in Switzerland, including the protocol concerning the accession of Liechtenstein, is approved on behalf of the Community.
The text of the Protocol is annexed to this Decision.
Article 2
The President of the Council is hereby authorised to designate the person empowered to deposit on behalf of the European Community the instrument of approval provided for in Article 5 of the Protocol, in order to express the consent of the Community to be bound.
Article 3
This Decision shall be published in the Official Journal of the European Union .
Done at Brussels,
For the Council
The President
Annex
Protocol between the European Community, Switzerland and Liechtenstein to the Agreement between the European Community and the Swiss Confederation concerning the criteria and mechanisms for establishing the State responsible for examining a request for asylum lodged in a Member State, in Switzerland or Liechtenstein
The European Community
and
The Swiss Confederation
and
The Principality of Liechtenstein
Hereinafter referred to as “the Contracting Parties”
BEARING IN MIND that the Protocol on the position of Denmark, annexed to the Treaty on the European Union and to the Treaty establishing the European Community, provides that no measure adopted pursuant to Title IV of the Treaty establishing the European Community shall be binding upon or applicable in Denmark;
REFERRING to Article 11(1) of the Agreement between the European Community and the Swiss Confederation concerning the criteria and mechanisms for establishing the State responsible for examining a request for asylum lodged in a Member State or in Switzerland[7] (hereinafter referred to as “the Agreement between the European Community and Switzerland”), which states that the Kingdom of Denmark may request to participate in this Agreement;
CONSIDERING the Protocol between the European Community, the Swiss Confederation and the Principality of Liechtenstein to the Agreement between the European Community and Switzerland on the accession of the Principality of Liechtenstein to that Agreement, in accordance with its Article 15;
NOTING that Denmark by letter dated 8 November 2004 requested to participate in the Agreement between the European Community and Switzerland;
RECALLING that according to Article 11(1) of the Agreement between the European Community and Switzerland to which the Principality of Liechtenstein has acceded, the conditions for such participation by the Kingdom of Denmark shall be determined by the Contracting Parties, acting with the consent of Denmark, in a protocol to that Agreement;
CONSIDERING that it was appropriate, in the first place, for Denmark and the Community to conclude an Agreement in order to settle, in particular, matters relating to the jurisdiction of the Court of Justice and to the coordination between the Community and Denmark regarding international agreements;
CONSIDERING the Agreement between Denmark and the Community on the criteria and mechanisms for establishing the State responsible for examining a request for asylum lodged in Denmark or any other Member State of the European Union and “Eurodac” for the comparison of fingerprints for the effective application of the Dublin Convention[8];
CONSIDERING that it is therefore necessary to fix the conditions whereby Denmark participates in the Agreement between the European Community and Switzerland, to which the Principality of Liechtenstein has acceded, and in particular it is necessary to establish rights and obligations between Switzerland, Liechtenstein and Denmark;
NOTING that the entry into force of this Protocol is based on the consent of Denmark, in accordance with its constitutional requirements,
HAVE AGREED AS FOLLOWS:
Article 1
The Kingdom of Denmark shall participate in the Agreement between the European Community and the Swiss Confederation concerning the criteria and mechanisms for establishing the State responsible for examining a request for asylum lodged in a Member State and Switzerland (hereinafter referred to as "the Agreement between the European Community and Switzerland") to which the Principality of Liechtenstein has acceded by a protocol to that Agreement (hereinafter referred to as "the Liechtenstein Protocol") in accordance with Article 15 thereof, under the conditions set out in the Agreement between the European Community and Denmark on the criteria and mechanisms for establishing the State responsible for examining a request for asylum lodged in Denmark or any other Member State of the European Union and “Eurodac” for the comparison of fingerprints for the effective application of the Dublin Convention (hereinafter referred to as “the Agreement between the European Community and Denmark”) and the present Protocol.
Article 2
1. The provisions of the "Dublin Regulation”[9] , which is annexed to this Protocol and forms part thereof, together with its implementing measures adopted pursuant to Article 27 (2) of the "Dublin Regulation" shall, under international law, apply to the relations between Denmark on the one hand and Switzerland and Liechtenstein on the other hand.
2. The provisions of the "Eurodac Regulation"[10], which is annexed to this Protocol and forms part thereof, together with its implementing measures adopted pursuant to Article 22 or 23(2) of the "Eurodac Regulation" shall, under international law, apply to the relations between Denmark on the one hand and Switzerland and Liechtenstein on the other hand.
3. Amendments to the Acts referred to under paragraph 1 and 2 which are notified by Denmark to the Commission in accordance with Article 3 of the Agreement between the European Community and Denmark and which are notified by Switzerland and Liechtenstein to the Commission in accordance respectively with Article 4 of the Agreement between the European Community and Switzerland, and Article 5 of the Liechtenstein Protocol shall under international law apply to the relations between Denmark on the one hand and Switzerland and Liechtenstein on the other hand.
4. Implementing measures adopted pursuant to Article 27 (2) of the "Dublin Regulation" and implementing measures adopted pursuant to Article 22 or 23(2) of the "Eurodac Regulation" which are notified by Denmark to the Commission in accordance with Article 4 of the Agreement between the European Community and Denmark and which are notified by Switzerland and Liechtenstein to the Commission in accordance respectively with Article 4 of the Agreement between the European Community and Switzerland and Article 5 of the Liechtenstein Protocol shall under international law apply to the relations between Denmark on the one hand and Switzerland and Liechtenstein on the other hand.
Article 3
Switzerland and Liechtenstein shall be entitled to submit statements of case or written observations to the Court of Justice in cases where a question has been referred to it by a Danish court or tribunal for a preliminary ruling in accordance with Article 6 (1) of the Agreement between the European Community and Denmark.
Article 4
1. In the case of a complaint by Switzerland or Liechtenstein concerning the application or the interpretation by Denmark of this Protocol, Switzerland or Liechtenstein may ask that the matter be officially entered as a matter of dispute on the agenda of the Mixed Committee.
2. In the case of a complaint by Denmark concerning the application or the interpretation by Switzerland or Liechtenstein of this Protocol, Denmark shall be entitled to ask the Commission to enter officially the matter as a matter of dispute on the agenda of the Mixed Committee. The matter shall be placed on the agenda by the Commission.
3. The Mixed Committee shall have 90 days from the date of the adoption of the agenda on which the dispute has been entered within which to settle the dispute. For this purpose, Denmark shall be entitled to make observations to the Mixed Committee.
4. In case where a dispute is settled by the Mixed Committee in a manner that it requires implementation in Denmark, Denmark shall, within the timeframe envisaged in paragraph 3, notify the Parties whether or not to implement the content of the settlement. In case where Denmark notifies its decision not to implement the content of the settlement, paragraph 5 shall apply.
5. In a case where the dispute cannot be settled by the Mixed Committee within the period envisaged in paragraph 3, a further period of 90 days shall be observed for reaching a final settlement. If the Mixed Committee has not taken a decision at the end of the period this Protocol shall be considered terminated at the end of the last day of that period.
Article 5
This Protocol is subject to ratification or approval by the Contracting Parties. Instruments of ratification or approval shall be deposited with the Secretary-General of the Council who shall act as depositary.
As far as Liechtenstein is concerned, this Protocol shall enter into force on the first day of the second month following the notification by the European Community and the Principality of Liechtenstein of the completion of their respective procedures required for this purpose.
As far as Switzerland is concerned, this Protocol shall enter into force on the first day of the second month following the notification by the European Community and the Swiss Confederation of the completion of their respective procedures required for this purpose.
The entry into force of this Protocol for the European Community and the Principality of Liechtenstein on the one hand and for the European Community and the Swiss Confederation on the other hand, is also subject to the prior receipt by the depositary of a Note from the Kingdom of Denmark to the effect that the Kingdom of Denmark assents to the provisions contained in the present Protocol and declares that it shall apply the provisions referred to in Article 2 in its mutual relations with Switzerland and Liechtenstein.
Article 6
Each Contracting Party may terminate this Protocol by written declaration to the depositary. Such declaration shall take effect six months after its deposition.
This Protocol shall cease to be effective if the Agreement between the European Community and Denmark is terminated.
This Protocol shall cease to be effective if either the European Community or both Switzerland as well as Liechtenstein have denounced it.
Done at Brussels,
Annex to the Protocol
COUNCIL REGULATION (EC) No 343/2003 of 18 February 2003 adopted by the Council of the European Union establishing the criteria and mechanisms for determining the Member State responsible for examining an asylum application lodged in one of the Member States by a third-country national, O.J. L 50, 25.2.2003
COUNCIL REGULATION (EC) No 2725/2000 of 11 December 2000 adopted by the Council of the European Union concerning the establishment of “Eurodac” for the comparison of fingerprints for the effective application of the Dublin Convention, O.J. L316, 15.12.2000
[1] It should be recalled that on 21 February 2006, the European Community concluded an agreement with the Kingdom of Denmark on the criteria and mechanisms for establishing the State responsible for examining a request for asylum lodged in Denmark or any other Member State of the European Union and “Eurodac” for the comparison of fingerprints for the effective application of the Dublin Convention (Agreement between the European Community and Denmark).
[2] as well as the draft protocols on the accession of Liechtenstein to the Schengen Agreement with Switzerland and on the accession of Liechtenstein to the Dublin/Eurodac Agreement with Switzerland
[3] Article 15 of the Dublin/Eurodac agreement with Switzerland says that the accession of Liechtenstein shall be the subject of a protocol to this Agreement
[4] OJ C …
[5] OJ C …
[6] OJ C …
[7] ASILE 54, 13049/04
[8] OJ L66/38, 8.3.2006
[9] COUNCIL REGULATION (EC) No 343/2003 of 18 February 2003 adopted by the Council of the European Union establishing the criteria and mechanisms for determining the Member State responsible for examining an asylum application lodged in one of the Member States by a third-country national, O.J. L 50, 25.2.2003.
[10] COUNCIL REGULATION (EC) No 2725/2000 of 11 December 2000 adopted by the Council of the European Union concerning the establishment of “Eurodac” for the comparison of fingerprints for the effective application of the Dublin Convention, O.J. L316, 15.12.2000.
