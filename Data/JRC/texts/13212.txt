Council Joint Action 2006/304/CFSP
of 10 April 2006
on the establishment of an EU Planning Team (EUPT Kosovo) regarding a possible EU crisis management operation in the field of rule of law and possible other areas in Kosovo
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty on European Union and, in particular Article 14 and the third subparagraph of Article 25 thereof,
Whereas:
(1) Pursuant to United Nations Security Council Resolution (UNSCR) 1244, a process to determine the future status of Kosovo was launched at the beginning of November 2005 with the appointment of the UN Status Envoy, Mr Martti Ahtisaari. The success of this process is essential not only for providing a clearer perspective for the people of Kosovo but also for the overall stability of the region.
(2) The United Nations will remain fully engaged in Kosovo until the end of UNSCR 1244. However, the UN has indicated that it will no longer take the lead in a post-Status presence. The EU has a vital interest in a positive result of this process, as well as the responsibility and the means to contribute to such an outcome. It is likely that the EU together with other partners will make a major contribution. The EU will thus have to undertake an important role in Kosovo in a complex environment. It could take on responsibility for significant operations, particularly in the police and rule of law area.
(3) The Stabilisation and Association Process (hereinafter referred to as "SAP") is the strategic framework for the EU's policy towards the Western Balkan region, and its instruments are open to Kosovo, including a European Partnership, political and technical dialogue under the SAP Tracking Mechanism, inter alia, regarding standards in the field of rule of law, and related Community assistance programmes.
(4) In June 2005 the European Council stressed that Kosovo would, in the medium term, continue to need a civilian and military presence to ensure security and in particular protection for minorities, to help with the continuing implementation of standards and to exercise appropriate supervision of compliance with the provisions contained in the status agreement. In this respect, the European Council stressed the EU's willingness to play a full part, in close cooperation with the relevant partners and international organisations.
(5) On 7 November 2005 the Council welcomed Ambassador Kai Eide's Comprehensive Review of the situation in Kosovo and expressed its full support for the UN Secretary General's intention to start a political process to determine Kosovo's future status.
(6) In view of the possibility of the EU enhancing its engagement in Kosovo, the Council on 7 November 2005 also invited the Secretary-General/High Representative (hereinafter referred to as "SG/HR") together with the Commission to continue their work in identifying the possible future EU role and contribution, including in the areas of police, rule of law and the economy, and to submit joint proposals to the Council in the near future.
(7) On 6 December 2005 the SG/HR and the Commission submitted their report on "The Future EU Role and Contribution in Kosovo" to the Council. The report suggested an outline for the EU's future involvement in Kosovo. It stressed the desire to normalise the EU's relations with Kosovo as far as possible by using all the instruments available within the SAP. In addition it stressed the need to prepare for a future ESDP mission, including by creating and deploying a regular Planning Team soon enough to initiate EU planning for an integrated EU mission inter alia in the areas of rule of law and police.
(8) On 12 December 2005 the Council reiterated its full support for the political process to determine Kosovo's future status and for Mr Martti Ahtisaari. It also reiterated its determination to participate fully in the definition of the status of Kosovo and its readiness to be closely involved in the negotiations and implementation of Kosovo's future status, through the EU representative to the Kosovo future status process. The Council stressed again the paramount importance of the ongoing implementation of standards now and in the future to help progress towards European standards. In particular, the Provisional Institutions of Self-Government need to make further progress on protection of minorities, full respect for the rule of law, a transparent public administration free from political interference, a climate conducive to returns, and the protection of cultural and religious sites.
(9) On 12 December 2005 the Council also "welcomed the joint report by the SG/HR and the Commission on the EU's future role and contribution in Kosovo. It asked the SG/HR and the Commission to continue the examination of these issues in coordination with other international actors, particularly in the areas of police and the rule of law (including contingency planning for a possible ESDP mission), economic development and fostering Kosovo's European perspective, and to keep the relevant Council bodies actively engaged in order to ensure continuing timely preparation of an EU role in Kosovo."
(10) A Joint Council-Commission Fact Finding Mission to Kosovo took place between 19 and 27 February 2006 regarding possible future ESDP and Community engagement in the broader field of the rule of law. In its report, the Fact Finding Mission, recommended, inter alia, that the EU establish a Planning Team tasked to ensure that EU decision-making could be based on a solid and well analysed basis that is in step with the future status process.
(11) In a letter to the SG/HR on 4 April 2006, the UN SRSG Jessen-Petersen welcomed the EU's engagement in the discussions on the future international engagement in Kosovo and invited the EU to deploy an EU Planning Team for Kosovo (EUPT Kosovo) to Pristina.
(12) During the Fact Finding Mission and other consultations with the EU, the Provisional Institutions of Self Government indicated that they would welcome an EU Planning Team tasked to take forward contingency planning for a possible ESDP mission in the field of rule of law.
(13) The establishment of an EUPT Kosovo will not in any way or form prejudge the outcome of the Future Status process or any subsequent decision by the EU to launch an ESDP mission in Kosovo.
(14) In accordance with the guidelines of the European Council meeting at Nice on 7 to 9 December 2000, this Joint Action should determine the role of the SG/HR, in accordance with Articles 18(3) and 26 of the Treaty.
(15) Article 14(1) of the Treaty calls for the indication of a financial reference amount for the whole period of implementation of the Joint Action. The indication of amounts to be financed by the general budget of the European Union illustrates the will of the legislative authority and is subject to the availability of commitment appropriations during the respective budget year.
(16) Recourse should be made to the extent possible to redeployment of equipment left over from other current or terminated EU operational activities, especially EUPOL PROXIMA, EUPAT and EUPM, taking into account operational needs and the principles of sound financial management.
(17) The mandate of EUPT Kosovo will be implemented in the context of a situation where the rule of law is not fully secured and which could harm the objectives of the Common Foreign and Security Policy as set out in Article 11 of the Treaty,
HAS ADOPTED THIS JOINT ACTION:
Article 1
Objective
1. The European Union hereby establishes a European Union Planning Team (EUPT Kosovo) regarding a possible EU crisis management operation in Kosovo.
2. The objective of the EUPT Kosovo shall be:
- to initiate planning, including necessary procurement processes, to ensure a smooth transition between selected tasks of UNMIK and a possible EU crisis management operation, in the field of rule of law and other areas that might be identified by the Council in the context of the future status process,
- to provide technical advice as necessary in order for the EU to contribute to support and maintain the dialogue with UNMIK as regards its plans for downsizing and transferral of competencies to the local institutions.
Article 2
Tasks
In carrying out its objective the EUPT Kosovo shall focus on the following tasks:
1. Initiating a dialogue with the international community, the Kosovo institutions and local stakeholders on their views and considerations regarding operational issues linked to future arrangements.
2. Following closely and analysing UNMIK planning towards the end of its mandate, and actively providing advice.
3. Initiating planning to allow the smooth transfer of authority from selected tasks of UNMIK to a future EU crisis management operation, in the field of rule of law and other areas that might be identified by the Council in the context of the future status process.
4. Initiating work on identifying possible elements for mandates, objectives, specific tasks and programmes and personnel strength for a possible EU crisis management operation, including a draft budget, which can be used as a basis for later decision making by the EU. In this context the EUPT Kosovo shall initiate reflections on the development of exit strategies.
5. Drafting and preparing all possible aspects of procurement requirements for the possible EU crisis management operation.
6. Ensuring appropriate logistical support for a possible EU crisis management operation, including through the establishment of a warehouse capacity enabling it to store, maintain and service equipment, including transferred from other present or former EU crisis management operation, where this will contribute to the overall effectiveness and efficiency of the possible EU crisis management operation.
7. Drafting and preparing threat and risk analysis, under the guidance of the EU SITCEN and the Council Security Office, for the various component parts of a possible EU crisis management operation in Kosovo and devising an indicative budget (drawing on the experience of OMIK and UNMIK) for the cost of security.
8. Contributing to a comprehensive and integrated EU approach, taking into account assistance in the police and judiciary area provided in the framework of the SAP.
9. In the context of contingency planning for a possible EU crisis management operation in Kosovo, exchanging, as appropriate, specific assistance with EU crisis management operations or Fact Finding/Preparatory missions for the establishment of EU crisis management operations. Such assistance shall be explicitly agreed upon by the Head of EUPT Kosovo and shall be for a limited period of time.
Article 3
Structure
1. EUPT Kosovo shall in principle be structured as follows:
- an office of the Head of EUPT Kosovo,
- a police team,
- a justice team,
- an administration team.
2. EUPT Kosovo shall establish:
- an office in Pristina,
- a coordinating office in Brussels.
Article 4
Head of the EUPT Kosovo and staff
1. The Head of the EUPT Kosovo shall be responsible for managing and coordinating EUPT Kosovo activities.
2. The Head of EUPT Kosovo shall assume the day-to-day management of EUPT Kosovo and shall be responsible for staff and disciplinary matters. For seconded personnel, disciplinary action shall be exercised by the national or EU authority concerned.
3. The Head of EUPT Kosovo shall sign a contract with the Commission.
4. EUPT Kosovo shall primarily consist of civilian staff seconded by Member States or EU Institutions. Each Member State or EU institution shall bear the costs related to any of the staff seconded by it including salaries, medical coverage, travel expenses to and from Kosovo, and allowances other than per diems.
5. EUPT Kosovo may also recruit international staff and local staff on a contractual basis, as required.
6. While remaining under the authority of their sending Member States or EU institutions, all staff in the EUPT Kosovo shall carry out their duties and act in the sole interest of the EU supporting action. All staff shall respect the security principles and minimum standards established by Council Decision 2001/264/EC of 19 March 2001 adopting the Council's security regulations [1] (hereinafter referred to as "Council's security regulations").
7. EUPT Kosovo shall be deployed gradually beginning with a core team from the end of April 2006 with the intention of having the full team in place before 1 September 2006.
Article 5
Chain of command
1. The structure of EUPT Kosovo shall have a unified chain of command.
2. The PSC shall provide the political control and strategic direction to EUPT Kosovo.
3. The SG/HR shall give guidance to the Head of EUPT Kosovo.
4. The Head of EUPT Kosovo shall lead EUPT Kosovo and assume its day-to-day management.
5. The Head of EUPT Kosovo shall report to the SG/HR.
Article 6
Political control and strategic direction
1. The PSC shall exercise, under the responsibility of the Council, the political control and strategic direction of EUPT Kosovo.
2. The Council hereby authorises the PSC to take the relevant decisions in accordance with Article 25 of the Treaty. This authorisation shall include the powers to appoint a Head of EUPT Kosovo, upon a proposal from the SG/HR. The powers of decision with respect to the objectives and termination of EUPT Kosovo shall remain vested in the Council.
3. The PSC shall receive reports at regular intervals and may request specific reports by the Head of EUPT Kosovo on the implementation of the tasks mentioned in Article 2 and on the coordination with other actors referred to in Article 10. The PSC may invite the Head of EUPT Kosovo to its meetings, as appropriate.
4. The PSC shall report to the Council at regular intervals.
Article 7
Participation of third States
Without prejudice to the decision-making autonomy of the EU and its single institutional framework, acceding States shall be invited to contribute to EUPT Kosovo provided that they bear the cost of the staff seconded by them, including salaries, medical coverage, allowances, high-risk insurance and travel expenses to and from the mission area, and contribute to the running costs of EUPT Kosovo, as appropriate.
Article 8
Security
1. The Head of EUPT Kosovo shall be responsible for the security of EUPT Kosovo and shall, in consultation with the Security Office of the General Secretariat of the Council, be responsible for ensuring compliance with minimum security requirements applicable to the mission.
2. EUPT Kosovo shall have a dedicated Security Officer reporting to the Head of EUPT Kosovo.
Article 9
Financial arrangements
1. The financial reference amount intended to cover the expenditure related to EUPT Kosovo shall be EUR 3005000.
2. The expenditure financed by the amount referred to in paragraph 1 shall be managed in accordance with the rules and procedures applicable to the general budget of the EU, with the exception that any pre-financing shall not remain the property of the Community.
3. The Head of EUPT Kosovo shall report fully to, and be supervised by, the Commission on the activities undertaken in the framework of his contract.
4. The financial arrangements shall respect the operational requirements of EUPT Kosovo, including compatibility of equipment and interoperability of its teams.
5. Expenditure shall be eligible as from the date of entry into force of this Joint Action.
Article 10
Coordination with other actors
1. Close coordination between the EU and all relevant actors, including the UN/UNMIK, the OSCE, NATO/KFOR as well as other key actors such as the US and Russia, shall continue to ensure complementarity and synergy of the efforts of the international community. All EU Member States shall be kept fully informed on the coordination process.
2. In performing his duties the Head of EUPT Kosovo shall participate in the EU coordination mechanisms established in Pristina, Kosovo.
Article 11
Status of staff of EUPT Kosovo
1. Where required, the status of EUPT Kosovo staff in Kosovo, including where appropriate the privileges, immunities and further guarantees necessary for the completion and smooth functioning of EUPT Kosovo shall be agreed in accordance with the procedure laid down in Article 24 of the Treaty. The SG/HR assisting the Presidency may negotiate such an agreement on its behalf.
2. The Member State or EU institution having seconded a staff member shall be responsible for answering any claims linked to the secondment, from or concerning the staff member. The Member State or EU institution in question shall be responsible for bringing any action against the secondee.
3. The conditions of employment and the rights and obligations of international and local contracted staff shall be laid down in the contracts between the Head of EUPT Kosovo and the staff member.
Article 12
Community action
The Council and the Commission shall, each in accordance with its respective powers, ensure consistency between the implementation of this Joint Action and external activities of the Community in accordance with the second subparagraph of Article 3 of the Treaty. The Council and the Commission shall cooperate to this end.
Article 13
Release of classified information
1. The SG/HR shall be authorised to release to NATO/KFOR EU classified information and documents up to the level "CONFIDENTIEL UE" generated for the purposes of the action, in accordance with the Council's security regulations.
2. The SG/HR shall be authorised to release to the UN/UNMIK and the OSCE, in accordance with the operational needs of the EUPT Kosovo, EU classified information and documents up to the level "RESTREINT UE" generated for the purposes of the action, in accordance with the Council's security regulations. Local arrangements shall be drawn up for this purpose.
3. The SG/HR shall be authorised to release to third parties associated with this Joint Action EU non-classified documents related to the deliberations of the Council with regard to the action covered by the obligation of professional secrecy pursuant to Article 6(1) of Council Decision 2004/338/EC, Euratom of 22 March 2004 adopting the Council's Rules of Procedure [2].
Article 14
Review
By 31 October 2006 the Council shall evaluate whether the EUPT Kosovo should be continued after 31 December 2006, taking into account the necessity of a smooth transition to a possible EU crisis management operation in Kosovo.
Article 15
Entry into force and expiry
1. This Joint Action shall enter into force on the date of its adoption.
2. It shall expire on 31 December 2006.
Article 16
Publication
This Joint Action shall be published in the Official Journal of the European Union.
Done at Brussels, 10 April 2006.
For the Council
The President
U. Plassnik
[1] OJ L 101, 11.4.2001, p. 1. Decision as last amended by Decision 2005/952/EC (OJ L 346, 29.12.2005, p. 18).
[2] OJ L 106, 15.4.2004, p. 22. Decision as last amended by Decision 2006/34/EC, Euratom (OJ L 22, 26.1.2006, p. 32).
--------------------------------------------------
