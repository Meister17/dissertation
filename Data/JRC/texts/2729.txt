Council Decision
of 22 November 2004
concerning the signing of a Framework Agreement between the European Community and Bosnia and Herzegovina on the general principles for the participation of Bosnia and Herzegovina in Community programmes
(2005/518/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Articles 13, 71, 80, 95, 127, 137, 149, 150, 151, 152, 153, 157, 166, 175, 280 and 308 in conjunction with the second sentence of the first subparagraph of Article 300(2) thereof,
Having regard to the proposal from the Commission,
Whereas:
(1) The Commission has negotiated on behalf of the Community a Framework Agreement with Bosnia and Herzegovina on the general principles for its participation in Community programmes.
(2) Subject to its conclusion at a later date, the Agreement initialled on 30 September 2004 should be signed,
HAS DECIDED AS FOLLOWS:
Sole Article
Subject to its conclusion at a later date, the President of the Council is hereby authorised to designate the person empowered to sign, on behalf of the European Community, the Framework Agreement between the European Community and Bosnia and Herzegovina on the general principles for the participation of Bosnia and Herzegovina in Community programmes.
Done at Brussels, 22 November 2004.
For the Council
The President
B. R. Bot
--------------------------------------------------
