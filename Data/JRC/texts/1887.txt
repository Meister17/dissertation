Opinion of the Advisory Committee on restrictive practices and dominant positions given at its 386th meeting on 6 December 2004 concerning a preliminary draft decision in Case COMP/C.37.533 — Choline chloride
(2005/C 180/05)
1. The Advisory Committee agrees with the Commission on the basic amount of the fines for Akzo Nobel, BASF and UCB.
2. The Advisory Committee agrees with the Commission on the increase of the basic amount due to aggravating circumstances.
3. The Advisory Committee agrees with the Commission on the reduction of the basic amount due to attenuating circumstances.
4. The Advisory Committee agrees with the Commission on the reduction of the fines based on the 1996 Commission Notice on the non-imposition or reduction of fines in cartel cases.
5. The Advisory Committee agrees with the Commission on the final amounts of the fines.
6. The Advisory Committee recommends the publication of its opinion in the Official Journal of the European Union.
7. The Advisory Committee asks the Commission to take into account all the other points raised during the discussion.
--------------------------------------------------
