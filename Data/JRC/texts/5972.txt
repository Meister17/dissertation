[pic] | COMMISSION OF THE EUROPEAN COMMUNITIES |
Brussels, 10.10.2005
COM(2005) 480 final
2005/0204 (CNS)
Proposal for a
COUNCIL DECISION
on the establishment of a mutual information procedure concerning Member States’ measures in the areas of asylum and immigration {SEC(2005)1233}
(presented by the Commission)
EXPLANATORY MEMORANDUM
1. Context and objective of the proposal
One of the most important objectives of the European Union is the establishment of an area of freedom, security and justice. In this context, it is essential to develop common immigration and asylum policies, as stated in the Tampere programme adopted by the European Council in 1999 and confirmed in 2004 in The Hague programme ("Strengthening Freedom, Security and Justice in the European Union"). These common policies must be based on the adoption of common legislative instruments and on the enhancement of mutual confidence through an improved coordination of national policies, closer practical cooperation and regular information exchange between Member States and with the Commission.
Since the entry into force of the Treaty of Amsterdam, a large number of common measures have been adopted in the areas of asylum and immigration, as the Community and the Member States share the competence to legislate in those areas. Nevertheless, Member States keep an important role in this area and are continually adopting new national measures, which may in some cases have an impact on other Member States or on the Community as a whole.
Indeed, the absence of border checks in the Schengen area, the common visa policy, the tight economic and social relations between EU Member States and the development of common immigration and asylum policies in recent years have had as an indirect consequence that asylum and immigration measures taken by one Member State are more likely to have an impact on other Member States. For instance, a very restrictive migratory policy in one Member State may deviate migration flows to its neighbours; and a regularisation procedure may attract illegal immigration into one Member State, from which regularised migrants could afterwards more easily move to other Member States. Other national asylum and immigration measures, including, among others, changes in procedures for granting international protection; determination of safe countries of origin; admission programmes for third country nationals, including quotas; and integration measures may also have an impact on other Member States or on the Community as a whole.
These developments fully justify the establishment of a formal information procedure between Member States and with the Commission which will increase the possibilities for information exchange on and discussion of national measures in the areas of asylum and immigration.
The Commission and the Luxembourg Presidency of the Council sent a letter to Justice and Home Affairs Ministers on 11 February 2005 on the need to establish an early-warning information system between Member States' administrations in the field of immigration and asylum, which could lead to a more coordinated approach to immigration and asylum policy between Member States. A first exchange of views on the establishment of such a system took place during the JHA Council of 24 February 2005. All Member States welcomed in principle the creation of such a system and the Council adopted on 14 April 2005 conclusions on the establishment of a “System of mutual information between those in charge of migration and asylum policy in the Member States”, inviting the Commission to present a formal legislative proposal.
The Commission has always stressed the need for an enhanced exchange of information on migratory phenomena. Member States will indeed benefit from the proposed information procedure, as they will be able to obtain a better knowledge of other Member States' policies and will be in a position to improve coordination between them. Member States could have the possibility to know other Member States' views, if an exchange of views takes place on a given draft national measure, before the latter becomes adopted legislation. Finally, the negotiation of new EU legislation will also be enhanced, as a result of better coordination of national policies and increased mutual knowledge and confidence.
Finally, the proposed mutual information procedure must be viewed in the wider framework of the cooperation and information mechanisms and structures between the Member States and the Commission. The Commission wishes to simplify and merge existing systems, structures and networks at Community level in order not to increase the administrative burden on Member States and on the Commission itself.
2. Existing provisions in the area of the proposal
- On 8 June 1988 the Commission adopted a Decision “setting up a prior communication and consultation procedure on migration policies in relation to non-member countries” (OJ 1988, L 183). The information and consultation mechanism established by this Decision was never effectively used by Member States. The new community framework for immigration and asylum policies renders obsolete the provisions of this Commission Decision.
- Certain provisions of Community legislation[1] impose an obligation on Member States to communicate to the Commission the text of the provisions of national law which they adopt in the fields covered by those Directives. Given the similar nature of the information to be provided under these Directives and under the proposed draft Decision, duplication of efforts can be avoided by using the information procedure established by the draft Decision as the channel for Member States to fulfil their obligations under the above mentioned Directives.
3. Consultation
An informal ad-hoc discussion on the establishment of a prior information and consultation system took place during the JHA Council of 24 February 2005. Most Member States reacted positively to the joint Presidency/Commission proposals to set up such a system. An ad-hoc meeting of Member State's experts took place in Brussels on 17 March 2005 to discuss a non-paper prepared by the Commission services which contained the main elements of the proposed system.
4. Summary of the proposed action
The mutual information procedure would request Member States to communicate to the other Member States and to the Commission measures which they intend to take in the areas of asylum and immigration, at the latest when they are made public. Only measures susceptible of having an impact on other Member States or on the Community as a whole must be communicated. This includes as well certain judicial and administrative decisions.
Member States providing the above mentioned information are requested to prepare an executive summary of this information in another official language of the Community.
The information will be transmitted through a web-based network run by the Commission. This web-based network will be used as well for the transmission to the Commission of the information required under the Directives mentioned in point 2 above.
A Member State or the Commission may request additional information on a particular measure. A particular national measure may also be the object of an exchange of views, with the presence of the Member State whose measure is the object of an exchange of views, the Commission and all other Member States wishing to participate. The purpose of such an exchange of views is the identification of problems of common interest; therefore, discussions will not lead to any voting nor will they result in any kind of recommendations to the Member State concerned.
5. Legal basis
The Council Decision is based on Art. 66 of the EC Treaty. Since 1 May 2004, pursuant to the Protocol on article 67 of the Nice Treaty, these measures are taken by qualified majority voting in the Council and consultation of the European Parliament.
Title IV of the EC Treaty is not applicable to the United Kingdom and to Ireland, unless those Member States decide otherwise in accordance with the procedure laid down in the Protocol on the position of the United Kingdom and Ireland annexed to the Treaties. Title IV is likewise not applicable to Denmark, by virtue of the Protocol on the position of Denmark annexed to the Treaties.
6. Subsidiarity
In accordance with the principle of subsidiarity, the objective of the proposed action, namely to provide a forum for mutual information and exchange of views on national asylum and immigration measures, cannot be sufficiently achieved by the Member States and can therefore, by reason of the effects of the envisaged action, be better achieved by the Community.
The success of the common legislative instruments in the areas of asylum and immigration depends on a more coordinated approach between national policies. This coordination needs new tools like those proposed in the Decision, consisting on a mechanism allowing for an exchange of information and views between Member States and with the Commission, which cannot take place only at national level. If Member States do not inform each other on the development of their asylum and immigration policies, the risk exists that divergent and even contradictory policies will lead to distortions of the migratory flows, including asylum-seekers, and will damage their ability to effectively pursue common goals in these areas.
7. Proportionality
The proposal is limited to enhancing information exchange on national measures in the areas of asylum and immigration between Member States and with the Commission
The proposed system does not create a disproportionate burden on Member States as their main duty will consist in posting a number of documents in the web-based system established by the Decision. In the case that an exchange of views on a national measure is called for, the only obligation for a Member State will be to send a representative to the exchange of views meeting in order to explain in detail the elements of the national measure being the object of an exchange of views and hear other Member States’ views on it.
The financial burden is minimised as the exchanges of views provided for by the proposed measure will be merged with meetings of other meetings of Commission advisory groups, thus reducing travel and accommodation expenses for Member States. An overall objective of the Commission with the proposed Council Decision, is to simplify existing structures for Member States’ cooperation in the areas of asylum and immigration
The information system will be run by the Commission using the existing IDA telematic network, which allows for the creation of separate communication channels at a minimum cost without any substantial investment being needed.
2005/0204 (CNS)
Proposal for a
COUNCIL DECISION
on the establishment of a mutual information procedure concerning Member States’ measures in the areas of asylum and immigration
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 66 thereof,
Having regard to the proposal from the Commission[2],
Having regard to the opinion of the European Parliament[3],
Whereas:
(1) On 4 November 2005 the European Council endorsed a multi-annual programme, known as the Hague Programme, for strengthening the area of freedom, security and justice, which calls for the development of the second phase of a common policy in the field of asylum, migration, visas and borders, starting on 1 May 2004, based, inter alia, on closer practical cooperation between Member States and an improved exchange of information;
(2) The development of common asylum and immigration policies since the entry into force of the Treaty of Amsterdam has resulted in closer interdependency between Member States’ policies in these areas, making the need for a more coordinated approach of national policies essential for strengthening the area of freedom, security and justice;
(3) In conclusions adopted at its meeting of 14 April 2005, the Justice and Home Affairs Council called for the establishment of a system of mutual information between those in charge of migration and asylum policy in the Member States, based on the necessity to communicate information on measures considered likely to have a significant impact on several Member States or on the European Union as a whole and allowing for an exchange of views between Member States and the Commission at the request of either one of them;
(4) The information procedure should be based on solidarity, transparency and mutual confidence;
(5) For reasons of efficiency and accessibility, a web-based network should be the essential element of the information procedure concerning national measures in the areas of asylum and immigration;
(6) Certain Directives adopted by the Community in the areas of asylum and immigration require the Member States to communicate to the Commission the text of the provisions of national law which they adopt in the fields covered by those Directives in addition to the implementing measures themselves. In order to simplify administrative procedures, it should be possible for the Member States to provide such information using the network established by this Decision;
(7) Since the objectives of this Decision, namely secure information exchange and consultation between Member States, cannot be sufficiently achieved by the Member States and can therefore, by reason of the effects of the envisaged action, be better achieved at Community level, the Community may adopt measures in accordance with the principle of subsidiarity as set out in Article 5 of the Treaty. In accordance with the principle of proportionality, as set out in that Article, this Decision does not go beyond what is necessary in order to achieve those objectives;
(8) In accordance with Articles 1 and 2 of the Protocol on the position of Denmark annexed to the Treaty on European Union and the Treaty establishing the European Community, Denmark is not taking part in the adoption of this Decision and is not bound by it or subject to its application.
HAS ADOPTED THIS DECISION:
Article 1
Subject matter and scope
This Decision establishes a procedure for the mutual exchange of information concerning national measures in the areas of asylum and immigration using a web-based network and allowing for an exchange of views on such measures.
Article 2
Information to be submitted
1. Member States shall communicate to the Commission and the other Member States the following measures which they intend to take in the areas of asylum and immigration if these measures are susceptible of having an impact on other Member States or on the Community as a whole:
(a) Draft legislation, at the latest at the time of submission for adoption; and
(b) Draft international agreements, at the latest at the time when they are initialled.
2. Member States shall communicate to the Commission and the other Member States:
(a) The final texts of the measures referred to in paragraph 1(a) at the time when they are adopted or immediately thereafter;
(b) The final texts of the measures referred to in paragraph 1(b) at the time when the Member State expresses its consent to be bound by such a measure or immediately thereafter.
3. Member States shall communicate to the Commission and the other Member States the following decisions if they are susceptible of having an impact on other Member States or on the Community as a whole:
(a) Final decisions of courts or tribunals which apply or interpret measures of national law in the areas of asylum or immigration, at the time when they are delivered or immediately thereafter; and
(b) Administrative decisions in the areas of asylum and immigration, at the time when they are adopted or immediately thereafter.
4. The measures referred to in paragraph 1 and 2 and the decisions referred to in paragraph 3 shall be communicated to the Commission and to the other Member States through the network referred to in Article 4 (“the network”)
5. The Commission or a Member State may request additional information concerning a particular measure or decision communicated by another Member State through the network. In such a case, the Member State concerned shall provide additional information concerning that measure or decision, within two weeks of the request being made through the network. The additional information shall be made available to the Commission and the other Member States through the network.
6. Each Member State shall ensure that a summary of the text of every measure or decision it transmits through the network is available in an official language of the Community other than its own. This summary shall at least include the objectives and scope of the concerned measure or decision, its main provisions and an estimation of the impact it could have on other Member States or on the Community as a whole.
Article 3
Obligation to provide information under existing Directives
Whenever Member States have an obligation to inform the Commission of the provisions of national law which they adopt in the fields covered by the Directives based on Article 63 of the Treaty, such obligations shall be deemed to be fulfilled if the information is transmitted through the network.
Article 4
The network
1. The network for the exchange of information in accordance with this Decision shall be web-based.
2. The Commission shall be responsible for the development and management of the network, including the structure and content of the network and access to it. The network shall include appropriate measures to guarantee its confidentiality.
3. For the practical set up of the network, the Commission shall make use of the existing technical platform within the Community framework of the trans-European telematic network for the interchange of data between administrations.
4. Member States shall provide access to the network in compliance with the measures adopted by the Commission in accordance with paragraph 2.
5. Member States shall designate national contact points having access to the network and notify the Commission thereof
Article 5
The exchange of views
1. The Commission may, on its initiative or on demand of a Member State, organise an exchange of views with Member States’ experts on a particular national measure submitted under articles 2 and 3 of this Decision. The Member State whose measure is the object of discussion shall be represented at the exchange of views.
2. The purpose of the exchange of views shall be the identification of issues of common interest.
Article 6
Evaluation and Review
The Commission shall evaluate the functioning of the system 3 years after the entry into force of this Decision and regularly thereafter.
The Commission shall periodically report to the European Parliament and the Council on the application of this Decision and, if appropriate, propose amendments to it.
Article 7
Entry into force
This Decision shall enter into force on the twentieth day following that of its publication in the Official Journal of the European Union.
Article 8
Addressees
This Decision is addressed to the Member States in accordance with the Treaty establishing the European Community.
Done at Brussels,
For the Council
The President
LEGISLATIVE FINANCIAL STATEMENT
1. NAME OF THE PROPOSAL :
Proposal for a Council Decision on the establishment of a mutual information procedure on Member States’ measures in the areas of asylum and immigration
2. ABM / ABB FRAMEWORK
Policy Area(s) concerned and associated Activity/Activities:
18 03 Immigration and asylum policy
3. BUDGET LINES
3.1. Budget lines (operational lines and related technical and administrative assistance lines (ex- B..A lines)) including headings :
N/A
3.2. Duration of the action and of the financial impact:
The proposed legal instrument does indicate neither duration nor revision period. Application of the instrument should start in 2007.
3.3. Budgetary characteristics ( add rows if necessary ) :
N/A
Budget line | Type of expenditure | New | EFTA contribution | Contributions from applicant countries | Heading in financial perspective |
4. SUMMARY OF RESOURCES
4.1. Financial Resources
4.1.1. Summary of commitment appropriations (CA) and payment appropriations (PA)
EUR million (to 3 decimal places)
Expenditure type | Section no. | Year 2007 | n + 1 | n + 2 | n + 3 | n + 4 | n + 5 and later | Total |
Operational expenditure[4] |
Commitment Appropriations (CA) | 8.1 | a |
Payment Appropriations (PA) | b |
Administrative expenditure within reference amount[5] |
Technical & administrative assistance (NDA) | 8.2.4 | c |
TOTAL REFERENCE AMOUNT |
Commitment Appropriations | a+c |
Payment Appropriations | b+c |
Administrative expenditure not included in reference amount[6] |
Human resources and associated expenditure (NDA) | 8.2.5 | d | 0.108 | 0.108 | 0.108 | 0.108 | 0.108 | 0.108 | 0.648 |
Administrative costs, other than human resources and associated costs, not included in reference amount (NDA) | 8.2.6 | e | 0.054 | 0.054 | 0.054 | 0.054 | 0.054 | 0.054 | 0.324 |
Total indicative financial cost of intervention
TOTAL CA including cost of Human Resources | a+c+d+e | 0.162 | 0.162 | 0.162 | 0.162 | 0.162 | 0.162 | 0.972 |
TOTAL PA including cost of Human Resources | b+c+d+e | 0.162 | 0.162 | 0.162 | 0.162 | 0.162 | 0.162 | 0.972 |
Co-financing details
N/A
If the proposal involves co-financing by Member States, or other bodies (please specify which), an estimate of the level of this co-financing should be indicated in the table below (additional lines may be added if different bodies are foreseen for the provision of the co-financing):
EUR million (to 3 decimal places)
Co-financing body | Year n | n + 1 | n + 2 | n + 3 | n + 4 | n + 5 and later | Total |
…………………… | f |
TOTAL CA including co-financing | a+c+d+e+f |
4.1.2. Compatibility with Financial Programming
( Proposal is compatible with existing financial programming.
( Proposal will entail reprogramming of the relevant heading in the financial perspective.
( Proposal may require application of the provisions of the Interinstitutional Agreement[7] (i.e. flexibility instrument or revision of the financial perspective).
4.1.3. Financial impact on Revenue
( Proposal has no financial implications on revenue
( Proposal has financial impact – the effect on revenue is as follows:
NB: All details and observations relating to the method of calculating the effect on revenue should be shown in a separate annex.
EUR million (to one decimal place)
Prior to action [Year n-1] | Situation following action |
Total number of human resources | 1 | 1 | 1 | 1 | 1 | 1 |
5. CHARACTERISTICS AND OBJECTIVES
Details of the context of the proposal are required in the Explanatory Memorandum. This section of the Legislative Financial Statement should include the following specific complementary information:
5.1. Need to be met in the short or long term
Better coordination of national asylum and immigration policies through the establishment of a mutual information procedure on national asylum and immigration measures and the possibility of holding meetings for the exchange of views on such measures. For such purpose, a web-based network using the CIRCA platform will be set up, and a number of meetings organised to provide the framework for the exchange of views.
5.2. Value-added of Community involvement and coherence of the proposal with other financial instruments and possible synergy
The Community involvement is needed as this kind of transnational cooperation exercise cannot take place only at national level.
Objectives, expected results and related indicators of the proposal in the context of the ABM framework
The proposed measure will lead to improved coordination, exchange of information and enhancement of the common policies on asylum and immigration
5.3. Method of Implementation (indicative) N/A
Show below the method(s)[9] chosen for the implementation of the action.
ٱ Centralised Management
ٱ Directly by the Commission
ٱ Indirectly by delegation to:
ٱExecutive Agencies
ٱ Bodies set up by the Communities as referred to in art. 185 of the Financial Regulation
ٱ National public-sector bodies/bodies with public-service mission
ٱ Shared or decentralised management
ٱ With Member states
ٱ With Third countries
ٱ Joint management with international organisations (please specify)
Relevant comments:
6. MONITORING AND EVALUATION
6.1. Monitoring system
The Commission will evaluate the functioning of the system 3 years after the entry into force of the Council decision and regularly thereafter
6.1.1. Evaluation
6.1.2. Ex-ante evaluation
See impact assessment
6.1.3. Measures taken following an intermediate/ex-post evaluation (lessons learned from similar experiences in the past)
N/A
6.1.4. Terms and frequency of future evaluation
An evaluation of the functioning of the system 3 years after the entry into force of the Council decision
7. ANTI-FRAUD MEASURES
N/A
8. DETAILS OF RESOURCES
8.1. Objectives of the proposal in terms of their financial cost
Commitment appropriations in EUR million (to 3 decimal places)
8.2.2. Description of tasks deriving from the action
The official will be in charge of managing the information system, animate the network and organise when necessary the meetings for the exchange of views on national measures transmitted through the network.
8.2.3. Sources of human resources (statutory)
(When more than one source is stated, please indicate the number of posts originating from each of the sources)
( Posts currently allocated to the management of the programme to be replaced or extended
( Posts pre-allocated within the APS/PDB exercise for year n
( Posts to be requested in the next APS/PDB procedure
( Posts to be redeployed using existing resources within the managing service (internal redeployment)
( Posts required for year n although not foreseen in the APS/PDB exercise of the year in question
Other Administrative expenditure included in reference amount (XX 01 04/05 – Expenditure on administrative management)
EUR million (to 3 decimal places)
Budget line (number and heading) | Year n | Year n+1 | Year n+2 | Year n+3 | Year n+4 | Year n+5 and later | TOTAL |
Other technical and administrative assistance |
intra muros |
extra muros |
Total Technical and administrative assistance |
8.2.5. Financial cost of human resources and associated costs not included in the reference amount
EUR million (to 3 decimal places)
Type of human resources | Year n (2007) | Year n+1 | Year n+2 | Year n+3 | Year n+4 | Year n+5 and later |
Officials and temporary staff (XX 01 01) | 0.108 | 0.108 | 0.108 | 0.108 | 0.108 | 0.108 |
Staff financed by Art XX 01 02 (auxiliary, END, contract staff, etc.) (specify budget line) |
Total cost of Human Resources and associated costs (NOT in reference amount) | 0.108 | 0.108 | 0.108 | 0.108 | 0.108 | 0.108 |
Calculation– Officials and Temporary agents
Reference should be made to Point 8.2.1, if applicable
1 B Official = 108,000 € per year
Calculation– Staff financed under art. XX 01 02
Reference should be made to Point 8.2.1, if applicable
8.2.6 Other administrative expenditure not included in reference amount EUR million (to 3 decimal places) |
XX 01 02 11 02 – Meetings & Conferences | 0.054 | 0.054 | 0.054 | 0.054 | 0.054 | 0.054 | 0.324 |
XX 01 02 11 03 – Committees[15] |
XX 01 02 11 04 – Studies & consultations |
XX 01 02 11 05 - Information systems |
2. Total Other Management Expenditure (XX 01 02 11) |
3. Other expenditure of an administrative nature (specify including reference to budget line) |
Total Administrative expenditure, other than human resources and associated costs (NOT included in reference amount) | 0.054 | 0.054 | 0.054 | 0.054 | 0.054 | 0.054 | 0.324 |
Calculation - Other administrative expenditure not included in reference amount
4 meetings for exchange of views on national measures * 27 MS * 500 €
[1] - article 8.2. of Council Directive 2001/40/EC [mutual recognition of expulsion decisions]- article 7.3. of Council Directive 2001/51/EC [supplementing provisions Schengen art 26]- article 27.2. of Council Directive 2001/55/EC [temporary protectionढ़ⴋ]- article 4.2. of Council Directive 2002/90/EC [facilitation of unauthorised entry]- article 10.2. of Council Directive 2003/110/EC [assistance in removal by air]- article 26.2 of Council Directive 2003/9/EC [standards for reception of asylum-seekers],- article 38.2 of Council Directive 2004/83/EC [qualifications directive],- article 19.2. of Council Directive 200x/xx/EC [researchers’ directive, to be formally adopted yet]- article 43 of Council Directive 200x/xx/EC [asylum procedures, to be formally adopted yet]
[2] OJ C [...], [...], p. [...].
[3] OJ C [...], [...], p. [...].
[4] Expenditure that does not fall under Chapter xx 01 of the Title xx concerned.
[5] Expenditure within article xx 01 04 of Title xx.
[6] Expenditure within chapter xx 01 other than articles xx 01 04 or xx 01 05.
[7] See points 19 and 24 of the Interinstitutional agreement.
[8] Additional columns should be added if necessary i.e. if the duration of the action exceeds 6 years
[9] If more than one method is indicated please provide additional details in the "Relevant comments" section of this point
[10] As described under Section 5.3
[11] Cost of which is NOT covered by the reference amount
[12] Cost of which is NOT covered by the reference amount
[13] Cost of which is included within the reference amount
[14] Reference should be made to the specific legislative financial statement for the Executive Agency(ies) concerned.
[15] Specify the type of committee and the group to which it belongs.
