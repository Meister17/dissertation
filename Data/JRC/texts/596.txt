Commission Decision
of 10 November 2000
establishing the European Union Eco-labelling Board and its rules of procedure
(notified under document number C(2000) 3280)
(Text with EEA relevance)
(2000/730/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community and in particular Article 175(1) thereof,
Having regard to Regulation (EC) No 1980/2000 of the European Parliament and of the Council of 17 July on a revised Community Eco-label Award Scheme(1), and in particular Article 13 thereof,
Whereas:
(1) Article 13 of Regulation (EC) No 1980/2000 provides that the Commission shall establish a European Union Eco-labelling Board, hereinafter referred to as the "EUEB", consisting of the competent bodies referred to in Article 14 and of the Consultation Forum referred to in Article 15.
(2) Article 13 of Regulation (EC) No 1980/2000 provides that the rules of procedure of the EUEB shall be established by the Commission in accordance with the procedure laid down in Article 17 and taking into account the procedural principles set out in Annex IV.
(3) Article 13 of Regulation (EC) No 1980/2000 provides that the EUEB shall in particular contribute to the setting and review of Eco-label criteria as well as the assessment and verification requirements.
(4) Article 15 of Regulation (EC) No 1980/2000 provides that the Commission shall ensure that in the conduct of its activities the EUEB observes, in respect of each product group, a balanced participation of all relevant interested parties concerned with that product group such as industry and service suppliers, including SMEs, crafts and their business organisations, trade unions, traders, retailers, importers, environmental protection groups and consumer organisations.
(5) Recital 5 of Regulation (EC) No 1980/2000 provides that for the acceptance by the general public of the Community Eco-label Award System it is essential that environmental NGOs and consumer organisations play an important role in the development and setting of criteria for Community Eco-labels.
(6) Article 6(2) of Regulation (EC) No 1980/2000 provides that the EUEB may request the Commission to initiate the procedure for setting the ecological criteria.
(7) Article 6(2) of Regulation (EC) No 1980/2000 provides that the Commission shall give mandates to the EUEB to develop and periodically review the Eco-label criteria as well as the assessment and verification requirements relating to those criteria, applying to the product groups coming within the scope of this Regulation.
(8) Article 6(3) of Regulation (EC) No 1980/2000 provides that on the basis of the mandate the EUEB shall draft the Eco-label criteria in respect of the product group and the assessment and verification requirements related to those criteria, as outlined in Article 4 and Annex IV of this Regulation, by taking into account the results of feasibility and market studies, life cycle considerations and the improvement analysis referred to in Annex II of this Regulation.
(9) Annex IV(1) of Regulation (EC) No 1980/2000 provides that a specific ad hoc working group involving the interested parties referred to in Article 15 and the competent bodies referred to in Article 14 will be established within the EUEB for the development of Eco-label criteria for each product group.
(10) Article 5 of Regulation (EC) No 1980/2000 provides that the EUEB shall be consulted by the Commission on the Community Eco-label working plan.
(11) Article 10 of Regulation (EC) No 1980/2000 provides that the members of the EUEB shall cooperate with the Member States and the Commission in promoting the use of the Community Eco-label.
(12) The measures provided for in this Decision are in accordance with the opinion of the Committee set up in accordance with Article 17 of Regulation (EC) No 1980/2000,
HAS ADOPTED THIS DECISION:
Article 1
The rules establishing the European Union Eco-labelling Board and its rules of procedure, set out in the Annex hereto, are hereby adopted.
Article 2
This decision is addressed to the Member States.
Done at Brussels, 10 November 2000.
For the Commission
Margot Wallström
Member of the Commission
(1) OJ L 237, 21.9.2000, p. 1.
ANNEX
RULES OF PROCEDURE OF THE EUROPEAN UNION ECO-LABELLING BOARD (EUEB)
ROLE OF THE EUEB
1. The EUEB referred to in Article 13 of Regulation (EC) No 1980/2000 is established and shall operate in accordance with the said Regulation.
2. The EUEB shall, in particular:
- request the Commission to initiate the procedure for setting the ecological criteria as well as the related assessment and compliance verification requirements for product groups,
- contribute to the setting and review of Eco-label criteria as well as the related assessment and compliance verification requirements for product groups,
- be consulted by the Commission on the Community Eco-label working plan,
3. The members of the EUEB shall cooperate with the Member States and the Commission in promoting the use of the Community Eco-label.
COMPOSITION
4. The EUEB shall consist of the competent bodies referred to in Article 14 of Regulation (EC) No 1980/2000, including the competent bodies of the Member States of the European Economic Area, and the Consultation Forum referred to in Article 15 of the said Regulation.
5. Amongst others, the following organisations, representing the interested parties, shall be members of the EUEB:
- Coface (consumers, representing also BEUC, Eurocoop and AEC),
- EEB (environmental)
- ETUC (trade unions),
- UNICE (industry),
- UEAPME (SMEs, crafts),
- Eurocommerce (commerce)
In order to ensure a balanced participation of all relevant interested parties, the EUEB may adapt this membership as appropriate, either at the request of the Commission or on its own initiative, subject to approval by the Commission.
6. Each member of the EUEB shall designate a contact person.
CHAIR, VICE-CHAIRS AND SECRETARIAT
7. The chair and the two vice-chairs of the EUEB shall be held in turn by the EU Ecolabel competent bodies referred to in Article 14 of Regulation (EC) No 1980/2000.
8. The chair shall firstly be held in turn by a competent body from each Member State of the European Union, during the period of their presidency, and then from each Member State of the European Economic Area, for the same duration and in alphabetical order.
9. The two vice-chairs shall be held by the competent body that is next in line to hold the chair and the competent body that previously held the chair.
10. The competent body holding the chair or the vice-chairs may, exceptionally, be replaced by one of the vice-chairs or by another member of the EUEB.
11. At any time, however, the EUEB may, subject to agreement by the Commission, adopt a different way of determining the chair and vice-chairs.
12. The secretariat of the EUEB shall be held by the Commission.
MEETINGS
13. Meetings of the EUEB shall be convened by the chair, who shall, with the assistance of the vice-chairs and of the secretariat, be responsible for preparing and circulating the invitations, agendas and supporting papers, as well as drafting and circulating the corresponding minutes.
14. The Commission shall on request make a meeting room available should the meeting be scheduled to take place in Brussels.
15. In general no more than three representatives of a member of the EUEB should participate in a given meeting.
16. Representatives of the Member States of the European Union and of the European Economic Area may participate in the EUEB meetings. Representatives of the Commission shall participate in the EUEB meetings. The Chairperson or the Commission may, where appropriate, invite other interested parties to participate.
EXPENSES
17. Common expenses necessary for the meetings, developing and reviewing ecological criteria and other activities shall be met by the Commission subject to agreement of an annual budget for such expenditure.
GENERAL RULES
(Pertaining amongst others to lead competent bodies and ad hoc working groups, and applicable when acting in relation to setting or reviewing the ecological criteria and related assessment and compliance verification requirements for product groups)
18. When acting in relation to setting or reviewing the ecological criteria as well as the related assessment and compliance verification requirements for a product group, the EUEB shall select one or more of the competent bodies referred to in Article 14 of Regulation (EC) No 1980/2000 who are willing to take a lead role. These shall be referred to as the lead competent bodies.
19. The lead competent body shall, with the assistance of members of the EUEB, form an ad hoc working group, as referred to in Annex IV(1) of the said Regulation. It shall actively seek the balanced participation of, amongst others, appropriate representatives of the interested parties, of the competent bodies, and of the Commission. European Union and non-European Union interested parties shall be treated on an equal footing. The representatives of the interested parties referred to above shall as far as possible be expert and knowledgeable of the product group in question, and shall be referred to as the technical representatives.
20. All competent bodies shall actively seek the opinion of all interested parties in their country in relation to the product group in question, and shall communicate these opinions to the ad hoc working group and to the EUEB.
21. The lead competent body shall organise and chair at least one meeting of this ad hoc working group. The Commission shall on request make a meeting room available should the meeting be scheduled to take place in Brussels.
22. The technical representatives of the interested parties referred to above shall as far as possible also participate in meetings of the EUEB where the product group in question is to be fully discussed.
23. The EUEB, the lead competent body and the ad hoc working group shall act in compliance with the objectives and principles laid down in Article 1 of the said Regulation as well as the procedural principles laid down in Annex IV of the said Regulation.
24. The EUEB, the lead competent body and the ad hoc working group shall take into account relevant Community environmental policies and work done on other related product groups.
25. All reasonable efforts shall be made by the EUEB and its members and by the lead competent body and the ad hoc working group to achieve a consensus throughout their work, while aiming at high levels of environmental protection.
PREPARATORY WORK
(Procedure to be followed before requesting the Commission to initiate the procedure for setting the ecological criteria for product groups)
26. The EUEB may request the Commission to initiate the procedure for setting the ecological criteria for product groups.
27. The EUEB shall take due account of the Community Eco-label working plan referred to in Article 5 of Regulation (EC) No 1980/2000, and in particular the nonexhaustive list of product groups considered as priorities for Community action.
28. The EUEB shall carry out preparatory work to determine whether the product group under consideration falls within the scope of the Community Eco-label award scheme as laid down in Article 2 of the said Regulation, and in particular satisfies the conditions laid down in Article 2(2). The Commission may also request the EUEB to carry out this preparatory work.
29. In order to carry out this preparatory work, the EUEB shall select a lead competent body or bodies willing to take a lead role, who shall form an ad hoc working group. The general rules detailed above shall apply.
30. The lead competent body with the assistance of the ad hoc working group shall, amongst others and to an appropriate degree, carry out the feasibility and market study, the life cycle considerations and the improvement analysis referred to in Annex II of the said Regulation, taking into account the requirements and applicable provisions laid down in Article 3, Annex I and Annex IV(1).
In doing so, the lead competent body with the assistance of the ad hoc working group shall, amongst others and insofar as is appropriate and feasible, carry out the following tasks:
(a) analysing the nature of the market, including the industrial and economic segmentation of the sector (main manufacturers, market shares, imports, etc.), characterising the different types of the product, analysing the possibilities of successfully marketing the Eco-labelled products, and proposing an appropriate marketing and communication strategy;
(b) seeking the opinion of all interested parties (competent bodies, interest groups, etc.), and identifying those parties that would cooperate in the process of establishing criteria;
(c) analysing the key environmental impacts, the environmental best practice in the sector, looking also at design for environment issues, and identifying the major areas for improvement of the product group and how these might feasibly be addressed in ecological criteria;
(d) analysing the key elements relating to the product's fitness in meeting the needs of consumers and how these might feasibly be addressed in Eco-label criteria;
(e) making an inventory and obtaining copies of existing Eco-labels, standards, test methods and studies that are relevant for the establishment of an Eco-label for the product group, taking into account the work done on related product groups, and estimating the costs of testing;
(f) reviewing relevant national, European and international legislation;
(g) identifying potential barriers for successfully establishing this product group;
(h) drafting and distributing in due time before related meetings all necessary working papers summarising the main findings of each of the three steps referred to above and including other relevant information and analyses.
(i) drafting a comprehensive final report on the result of the above analyses and investigations, written in English and optionally in one of the other official languages of the Community as well. This final report shall be made available in both printed and electronic format, and shall be made open for consultation, if possible on the Eco-label web-site. It shall inlude in annex a summary list of all documents circulated in the course of this work, together with an indication of the date of circulation of each document and to whom it has been circulated, and a copy of the documents in question. It shall also include in annex a list of the interested parties involved in the work or that have been consulted or have expressed an opinion, together with their contact information. It shall include an executive summary and where relevant annexes with detailed inventory computations. Any observations received on the report will be taken into consideration, and, upon request, information on the followup to the comments will be provided.
(j) presenting the findings to one or more EUEB meetings and, on the basis of these consultations and of the likelihood of the overall success of an Eco-label for the candidate product group, recommending whether or not the product group should be further developed and Eco-label criteria established.
31. When satisfied with the above preparatory work for a candidate product group and with the positive recommendation of the ad hoc working group, the EUEB shall forward the final report together with the proposals concerning the drafting of the mandate to the Commission, and shall request the Commission to initiate the procedure for setting the ecological criteria for the product group in question and to give a mandate to the EUEB that takes into account the abovementioned proposals. The members of the EUEB representing the interested parties referred to in Article 15 of the said Regulation may individually or collectively annex their specific opinion or opinions to this final report.
MANDATE TO DEVELOP OR REVISE CRITERIA
(Procedure to be followed when carrying out a mandate from the Commission to develop or revise Eco-label criteria and related assessment and compliance verification requirements for a product group)
32. The EUEB shall, on the basis of a mandate received from the Commission, develop a proposal for the Eco-label criteria and related assessment and compliance verification requirements for the product group in question. The EUEB shall respect the deadline for completion of the work provided in the mandate.
33. In order to do so, the EUEB shall select a lead competent body or bodies willing to take a lead role, who shall form an ad hoc working group. The general rules detailed above shall apply.
34. The lead competent body with the assistance of the ad hoc working group shall firstly determine if all of the necessary analyses, investigations and other preparatory work, detailed above, have been carried out. This includes, in particular, the feasibility and market study, the life cycle considerations and the improvement analysis referred to in Annex II of Regulation (EC) No 1980/2000. The lead competent body with the assistance of the ad hoc working group should take all appropriate actions to complete and update this preparatory work as necessary, taking into account the procedures detailed in the aforementioned section.
35. The lead competent body shall present the draft proposal and related reports and analyses to one or more EUEB meetings and, on the basis of these consultations, shall when appropriate indicate to the EUEB that the mandate may be considered to be completed. In this respect, particular regard will be paid to the likelihood of the draft proposal receiving a high level of support.
36. Similarly the EUEB, on the basis of the work carried out by the lead competent body and the ad hoc working group, shall when appropriate inform the Commission of the draft criteria and indicate to the Commission that the mandate may be considered to be completed. In this respect, particular regard will be paid to the likelihood of the draft proposal receiving a high level of support. The members of the EUEB representing the interested parties referred to in Article 15 of said Regulation (EC) No 1980/2000, may individually or collectively annex their specific opinion or opinions to the draft criteria.
37. Should the Commission indicate that the mandate has not been fulfilled, the EUEB shall continue its work on the draft criteria, taking into account the procedures and requirements detailed in this section. The Commission shall indicate the reasons for its position.
38. Should at any point in time the Commission indicate that the mandate has been fulfilled, the EUEB shall consider the mandate to have been completed. The EUEB shall nevertheless recommence work on the mandate should at a later date the Commission so request.
39. Should at any point in time the EUEB conclude that it is unable to fulfil the mandate, it shall inform the Commission accordingly without delay, indicating in detail the reasons.
MANDATE TO REVIEW CRITERIA
(Procedure to be followed when carrying out a mandate from the Commission to review the Eco-label criteria and related assessment and compliance verification requirements for a product group)
40. The EUEB shall on the basis of a mandate received from the Commission, review the Eco-label criteria and related assessment and compliance verification requirements for the product group in question.
41. The EUEB shall take particular care to complete its work in due time before the expiry of the validity of the existing criteria.
42. In order to do so, the EUEB shall select a lead competent body or bodies willing to take a lead role, who shall form an ad hoc working group. The general rules detailed above shall apply.
43. The lead competent body with the assistance of the ad hoc working group shall review the current Eco-label criteria and related assessment and compliance verification requirements, as well as reviewing and where necessary completing and updating the various analyses, reports, inventories and other work detailed in the section entitled "Preparatory work".
44. The lead competent body with the assistance of the ad hoc working group shall in particular evaluate the past, current and likely future success of this product group, including the related environmental benefits, taking into account the success of related product groups, and taking into account the Community Eco-label working plan.
45. The lead competent body shall present the results of these evaluations and analyses to one or more EUEB meetings and, on the basis of these consultations, shall when appropriate recommend to the EUEB whether the ecological criteria and related assessment and compliance verification requirements should be either prolonged, withdrawn or revised. In this respect, particular regard will be paid to the likelihood of the recommendation receiving a high level of support.
46. Similarly, the EUEB, on the basis of the work carried out by the lead competent body and the ad hoc working group, shall when appropriate recommend to the Commission that the ecological criteria and related assessment and compliance verification requirements should be either prolonged, withdrawn or revised. In this respect, particular regard will be paid to the likelihood of the recommendation receiving a high level of support. The members of the EUEB representing the interested parties referred to in Article 15 of the Regulation (EC) No 1980/2000, may individually or collectively annex their specific opinion or opinions on this recommendation.
47. Should the Commission indicate that the mandate has not been fulfilled, the EUEB shall continue accordingly, taking into account the procedures and requirements detailed in this section. The Commission shall indicate the reasons for its position.
48. Should the Commission indicate its agreement with a recommendation that the ecological criteria and related assessment and compliance verification requirements should be revised, the EUEB shall proceed accordingly, taking into account the procedure and requirements detailed in the section entitled "Mandate to develop or revise criteria".
49. Should the Commission agree with a recommendation that the ecological criteria and related assessment and compliance verification requirements should be withdrawn or prolonged, the EUEB shall consider the mandate to have been completed. The EUEB shall nevertheless recommence work on the mandate should at a later date the Commission so request.
50. Should at any point in time the EUEB conclude that it is unable to fulfil the mandate, it shall inform the Commission accordingly without delay, indicating in detail the reasons.
INPUT REGARDING THE WORKING PLAN
(Procedure to be followed when consulted by the Commission on the Community Eco-label working plan)
51. The EUEB may provide input to the Commission on the proposed Community Eco-label working plan referred to in Article 5 of Regulation (EC) No 1980/2000.
52. In order to do so the EUEB should take all necessary and appropriate steps, acting in compliance with the objectives and principles laid down in Article 1(1) and (4) of the said Regulation.
53. Before proposing new product groups which could be a priority for Community action, the EUEB shall first make a preliminary and indicative determination as to whether the product groups in question fall within the scope of the Community Eco-label award scheme as laid down in Article 2 of the said Regulation, and in particular satisfy the conditions laid down in Article 2(2) of the said Regulation. In doing so it shall take into account, to an appropriate degree, the various considerations detailed above in the section entitled "Preparatory work".
54. All reasonable efforts shall be made by the EUEB and its members to achieve a high level of consensus throughout this work.
OTHER ACTIONS BY THE MEMBERS OF THE EUEB
55. The members of the EUEB shall act in the general interest of the Community Eco-label Scheme, and may take any initiatives they consider pertinent and useful to this end. They may similarly act at the request of the Commission. Such initiatives may include, amongst others:
- promotional activities, as referred to in Article 10 of Regulation (EC) No 1980/2000 or otherwise,
- the formation of ad hoc working groups,
- actions to promote the harmonised application of Eco-label criteria and related assessment and compliance verification requirements, including the regular adaptation and improvement of the related user manuals,
- drafting guidelines, for example to facilitate the development of ecological criteria,
- adopting internal procedures as appropriate.
REVIEW
56. The EUEB shall regularly review its functioning, and, if necessary, may make recommendations to the Commission to adapt these procedures as appropriate. The first review should be completed before the end of 2002.
