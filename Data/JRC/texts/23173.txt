COMMISSION DECISION of 8 July 1998 concerning certain measures necessary for carrying out activities related to communication and information exchange systems and to linguistic training tools under the Fiscalis programme (European Parliament and Council Decision No 888/98/EC establishing a programme of Community action to improve the functioning of the indirect taxation systems of the internal market) (notified under document number C(1998) 1866) (98/532/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to European Parliament and Council Decision No 888/98/EC of 30 March 1998 establishing a programme of Community action to improve the functioning of the indirect taxation systems of the internal market (Fiscalis programme) (1), and in particular Article 10 thereof,
Whereas the Commission and the Member States are required to ensure the functioning of such existing communication and information-exchange systems that they consider necessary in accordance with Article 4(1) of Decision No 888/98/EC;
Whereas the Commission and the Member States are required to establish and maintain the functioning of such new communication and information-exchange systems that they consider necessary in accordance with Article 4(1) of Decision No 888/98/EC;
Whereas the Member States, in cooperation with the Commission, are required to develop the necessary common linguistic training tools in accordance with Article 6 of Decision No 888/98/EC;
Whereas it is necessary to identify the necessary existing and new comunication and information exchange systems and common linguistic training tools;
Whereas it is necessary to make the arrangements for the Community to meet its obligations with regard to these necessary systems and tools in accordance with Articles 4(1), 4(2), 6(1)(c) and 8(2)(b) and (c) of Decision No 888/98/EC;
Whereas it is necessary, in order to ensure the effective and efficient establishment and functioning of these necessary systems and tools, to specify the obligations of the Member States with regard to these necessary systems and tools in accordance with Articles 4(2), 4(3), 6(1) and 8(3)(b);
Whereas each individual Member State shall ensure that none of their actions will jeopardise the establishment and functioning of these necessary systems or harm the interests of the Community and the other Member States;
Whereas it is necessary, in order to ensure the effective and efficient establishment and functioning of these necessary systems and tools, to coordinate the actions of the Community and the Member States in fulfilling their obligations with regard to these systems and tools; whereas it is appropriate for the Commission to ensure this coordination through the drawing up of management plans for the establishment and functioning of each system and tool;
Whereas the measures provided for in this decision are in accordance with the opinion of the Committee referred to in Article 11 of European Parliament and Council Decision No 888/98/EC,
HAS ADOPTED THIS DECISION:
Article 1
Work programme
1. The functioning of the following existing communication and information exchange systems and infrastructure shall be ensured:
- the VAT information exchange system (VIES),
- Common Communication Network/Common System Interface (CCN/CSI) to the extent necessary to support the functioning of the systems set out in this paragraph and in paragraph two,
- the system for exchange of excise data (SEED),
- the excise movement verification system,
- the excise duty tables system,
- FiscalSCENT (System Customs Enforcement NeTwork).
2. The functioning of the following new communication and information exchange system shall be ensured:
- the excise early warning system.
3. Feasibility studies and pilot projects into the establishment of the following new communication and information exchange systems and common linguistic training tools shall be carried out:
- excise movement and control system,
- indirect taxation communication system,
- multi-language support and training tools.
Article 2
Community obligations
1. To execute the Community's obligations, as set down in Article 4(1) and (2), Article 6(1)(c) and Article 8(2)(b) and (c) of Decision No 888/98/EC, for the systems referred to above in Article 1, the Commission shall conclude the necessary contracts in the name of the Community.
2. The Commission shall co-ordinate those aspects of the establishment and functioning of the Community and non-Community elements of the systems, infrastructure and tools referred to in Article 1 necessary to ensure the overall interconnection and interoperability of the systems. To this end, the Commission shall in co-operation with the Member States draw up management plans for the proper establishment and subsequent functioning of these systems and tools.
These management plan shall specify the initial and ongoing tasks to be completed both by the Commission and each Member State; the deadlines for completion of these tasks and any proof required of their completion.
Article 3
Member State obligations
1. Member States shall ensure that they complete within the deadline set the initial and ongoing tasks attributed to them in the management plans referred to in Article 2. They shall report to the Commission on their completion of these tasks, providing the required proof of their completion.
2. The Member States shall not take any action in relation to the establishment or functioning of the systems referred to in Article 1 that may have an effect either on the overall interconnection and interoperability of the systems or on their overall functioning. Any action that a Member State wishes to take which may affect the overall interconnection and interoperability of the systems or their overall functioning may only be taken with the Commission's prior agreement.
3. The Member States shall regularly inform the Commission of any action they have taken in accordance with Article 4(3) of Decision No 888/98/EC as appropriate for the full exploitation of these systems throughout their administration.
Article 4
Entry into force
This decision shall enter into force on the day of its publication in the Official Journal of the European Communities.
It shall apply from 1 January 1998.
Article 5
Addressees
This Decision is addressed to the Member States.
Done at Brussels, 8 July 1998.
For the Commission
Mario MONTI
Member of the Commission
(1) OJ L 126, 28. 4. 1998, p. 1.
