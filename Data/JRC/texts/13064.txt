Judgment of the Court of First Instance of 23 February 2006 — Cementbouw Handel & Industrie BV
v Commission
(Case T-282/02) [1]
Parties
Applicant: Cementbouw Handel & Industrie BV (Le Cruquius, Netherlands) (represented by: W. Knibbeler, O. Brouwer and P. Kreijger, lawyers)
Defendant: Commission of the European Communities (represented by: initially, A. Nijenhuis, K. Wiedner and W. Mölls, and subsequently A. Nijenhuis, E. Gippini Fournier and A. Whelan, acting as Agents)
Application for
annulment of Commission Decision 2003/756/EC of 26 June 2002, relating to a procedure pursuant to Council Regulation (EEC) No 4064/89, declaring a merger to be compatible with the common market and the EEA Agreement (Case COMP/M.2650 — Haniel/Cementbouw/JV (CVK)) (OJ 2003 L 282, p. 1, corrigendum published in OJ 2003 L 285, p. 52)
Operative part of the judgment
The Court:
1. Dismisses the application;
2. Orders the applicant to pay the costs.
[1] OJ C 274 of 9.11.2002.
--------------------------------------------------
