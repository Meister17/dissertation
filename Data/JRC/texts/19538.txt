Council Regulation (EC) No 694/2003
of 14 April 2003
on uniform formats for Facilitated Transit Documents (FTD) and Facilitated Rail Transit Documents (FRTD) provided for in Regulation (EC) No 693/2003
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 62(2) thereof,
Having regard to the proposal from the Commission(1),
Having regard to the opinion of the European Parliament(2),
Whereas:
(1) In order to prepare accession of new Member States, the Community should take into account specific situations, which may occur as a result of the enlargement and set out the relevant legislation in order to avoid future problems in relation with the crossing of the external border.
(2) Council Regulation (EC) No 693/2003(3) establishes a Facilitated Transit Document (FTD) and Facilitated Rail Transit Document (FRTD) for the case of a specific transit by land of third country nationals who must necessarily cross the territory of one or several Member States in order to travel between two parts of their own country which are not geographically contiguous. Uniform formats for these documents should be established.
(3) These uniform formats should contain all the necessary information and meet high technical standards, in particular as regards safeguards against counterfeiting and falsification. The formats should also be suited to use by all Member States and bear universally recognisable harmonised security features which are clearly visible to the naked eye.
(4) Powers to adopt such common standards should be conferred on the Commission, which should be assisted by the Committee established by Article 6 of Council Regulation (EC) No 1683/95 of 29 May 1995 laying down a uniform format for visas(4).
(5) To ensure that the information in question is not divulged more widely than is necessary, it is also essential that each Member State issuing the FTD/FRTD designate a single body for printing the uniform format for FTD/FRTD, while retaining the possibility of changing that body, if necessary. For security reasons, each such Member State should communicate the name of the competent body to the Commission and to the other Member States.
(6) The measures necessary for the implementation of this Regulation should be adopted in accordance with Council Decision 1999/468/EC of 28 June 1999 laying down the procedures for the exercise of implementing powers conferred to the Commission(5).
(7) In accordance with Articles 1 and 2 of the Protocol on the position of Denmark annexed to the Treaty on European Union and to the Treaty establishing the European Community, Denmark is not taking part in the adoption of this Regulation, and is not bound by it or subject to its application. Given that this Regulation builds upon the Schengen acquis under the provisions of Title IV of Part Three of the Treaty establishing the European Community, Denmark shall, in accordance with Article 5 of the said Protocol, decide within a period of six months after the Council has adopted this Regulation whether it will implement it in its national law.
(8) As regards Iceland and Norway, this Regulation constitutes a development of provisions of the Schengen acquis within the meaning of the Agreement concluded by the Council of the European Union and the Republic of Iceland and the Kingdom of Norway concerning the association of those two States with the implementation, application and development of the Schengen acquis(6), which fall within the area referred to in Article 1, point B of Council Decision 1999/437/EC of 17 May 1999 on certain arrangements for the application of that Agreement(7).
(9) This Regulation constitutes a development of provisions of the Schengen acquis in which the United Kingdom does not take part, in accordance with Council Decision 2000/365/EC of 29 May 2000 concerning the request of the United Kingdom of Great Britain and Northern Ireland to take part in some of the provisions of the Schengen acquis(8); the United Kingdom is therefore not taking part in its adoption and is not bound by it or subject to its application.
(10) This Regulation constitutes a development of provisions of the Schengen acquis in which Ireland does not take part, in accordance with Council Decision 2002/192/EC of 28 February 2002 concerning Ireland's request to take part in some of the provisions of the Schengen acquis(9); Ireland is therefore not taking part in its adoption and is not bound by it or subject to its application.
(11) This Regulation constitutes an act building on the Schengen acquis or otherwise related to it within the meaning of Article 3(1) of the Act of Accession,
HAS ADOPTED THIS REGULATION:
Article 1
1. Facilitated Transit Documents (FTD) issued by the Member States as referred to in Article 2(1) of Regulation (EC) No 693/2003 shall be produced in the form of a uniform format (sticker) and shall have the same value as transit visas. They shall conform to the specifications set out in Annex I to this Regulation.
2. Facilitated Rail Transit Documents (FRTD) issued by the Member States as referred to in Article 2(2) of Regulation (EC) No 693/2003 shall be produced in the form of a uniform format (sticker) and shall have the same value as transit visas. They shall conform to the specifications set out in Annex II to this Regulation.
Article 2
1. Further technical specifications for the uniform format for FTD and FRTD relating to the following shall be established in accordance with the procedure referred to in Article 4(2):
(a) additional security features and requirements including enhanced anti-forgery, counterfeiting and falsification standards;
(b) technical processes and rules for the filling in of the uniform FTD/FRTD;
(c) other rules to be observed for the filling in of the uniform FTD/FRTD.
2. The colours of the uniform FTD and FRTD may be changed in accordance with the procedure referred to in Article 4(2).
Article 3
1. The specifications referred to in Article 2 shall be secret and not be published. They shall be made available only to the bodies designated by the Member States as responsible for printing and to the persons duly authorised by a Member State or the Commission.
2. Each Member State which has decided to issue the FTD/FRTD shall designate one body having responsibility for printing them. It shall communicate the name of that body to the Commission and the other Member States. The same body may be designated by two or more Member States for this purpose. Each Member State shall be entitled to change its designated body. It shall inform the Commission and the other Member States accordingly.
Article 4
1. The Commission shall be assisted by the Committee set up by Article 6(2) of Regulation (EC) No 1683/95.
2. Where reference is made to this paragraph, Articles 5 and 7 of Decision 1999/468/EC shall apply.
The period laid down in Article 5(6) of Decision 1999/468/EC shall be two months.
3. The Committee shall adopt its Rules of Procedure.
Article 5
Without prejudice to data protection rules, persons to whom the FTD and FRTD is issued shall have the right to verify the personal particulars contained in the FTD/FRTD and, where appropriate, to have them corrected or deleted. No information in machine-readable form shall be included in the FTD and FRTD, unless provided for in the Annexes to this Regulation or unless it is mentioned in the relevant travel document.
Article 6
Member States which have decided to do so shall issue the uniform format for FTD and FRTD as referred to in Article 1 no later than one year after the adoption of the additional security features and requirements referred to in Article 2(1)(a).
The need for the incorporation of the photograph referred to in point 2 of Annex I and point 2 of Annex II may be determined by the end of 2005.
Article 7
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in the Member States in accordance with the Treaty establishing the European Community.
Done at Luxembourg, 14 April 2003.
For the Council
The President
A. Giannitsis
(1) Not yet published in the Official Journal.
(2) Opinion delivered on 8 April 2003 (not yet published in the Official Journal).
(3) See page 8 of this Official Journal.
(4) OJ L 164, 14.7.1995, p.1. Regulation as last amended by Regulation (EC) No 334/2002 (OJ L 53, 23.2.2002, p. 23).
(5) OJ L 184, 17.7.1999, p. 23.
(6) OJ L 176, 10.7.1999, p. 36.
(7) OJ L 176, 10.7.1999, p. 31.
(8) OJ L 131, 1.6.2000, p. 43.
(9) OJ L 64, 7.3.2002, p. 20.
ANNEX I
FACILITATED TRANSIT DOCUMENT (FTD)
Security features
1. An optically variable device (OVD), which provides a quality of identification and a level of security not less than the device used in the current uniform format for visas, shall appear in this space. Depending on the angle of view, 12 stars, the letter "E" and a globe become visible in various sizes and colours.
2. An integrated photograph produced according to high security standards.
3. The logo consisting of a letter or letters indicating the issuing Member State with a latent image effect shall appear in this space. This logo shall appear light when held flat and dark when turned by 90°. The logos shall be used following Regulation (EC) No 1683/95.
4. The word "FTD" in capital letters shall appear in the middle of this space in optically variable colouring. Depending on the angle of view, it shall appear in green or red.
5. This box shall contain the number of the FTD, which shall be pre-printed and shall begin with the letter or letters indicating the issuing country as described in point 3. A special type shall be used.
Sections to be completed
6. This box shall begin with the words "valid for". The issuing authority shall indicate the territory or territories for which the FTD is valid.
7. This box shall begin with the word "from" and the word "until" shall appear further along the line. The issuing authority shall indicate here the period of validity of the FTD.
8. This box shall begin with the words "number of entries" and further along the line the words "duration of transit" and again "days" shall appear.
9. This box shall begin with the words "issued in" and shall be used to indicate the place of issue.
10. This box shall begin with the word "on" (after which the date of issue shall be filled in by the issuing authority) and further along the line the words "number of passport" shall appear (after which the holder's passport number shall appear).
11. This box shall indicate the name and the forename of the holder.
12. This box shall begin with the word "remarks". It shall be used by the issuing authority to indicate any further information, which is considered necessary, provided that it complies with Article 5 of this Regulation. The following two-and-a-half lines shall be left empty for such remarks.
13. This box shall contain the relevant machine-readable information to facilitate external border controls.
The paper shall not be coloured (basic white shade).
The words designating the boxes shall appear in English, French and in the language of the issuing State.
Model of the FTD
>PIC FILE= "L_2003099EN.001901.TIF">
ANNEX II
FACILITATED RAIL TRANSIT DOCUMENT (FRTD)
Security features
1. An optically variable device (OVD), which provides a quality of identification and a level of security not less than the device used in the current uniform format for visas, shall appear in this space. Depending on the angle of view, 12 stars, the letter "E" and a globe become visible in various sizes and colours.
2. An integrated photograph produced according to high security standards.
3. The logo consisting of a letter or letters indicating the issuing Member State with a latent image effect shall appear in this space. This logo shall appear light when held flat and dark when turned by 90°. The logos shall be used following Regulation (EC) 1683/95.
4. The word "FRTD" in capital letters shall appear in the middle of this space in optically variable colouring. Depending on the angle of view, it shall appear in green or red.
5. This box shall contain the number of the FRTD, which shall be pre-printed and shall begin with the letter or letters indicating the issuing country as described in point 3 above. A special type shall be used.
Sections to be completed
6. This box shall begin with the words "valid for". The issuing authority shall indicate the territory or territories for which the FRTD is valid.
7. This box shall begin with the word "from" and the word "until" shall appear further along the line. The issuing authority shall indicate here the period of validity of the FRTD.
8. In this box shall be stated "single entry and return" and further along the line the word "hours".
9. This box shall begin with the words "issued in" and shall be used to indicate the place of issue.
10. This box shall begin with the word "on" (after which the date of issue shall be filled in by the issuing authority) and further along the line the words "number of passport" shall appear (after which the holder's passport number shall appear).
11. This box shall indicate the name and the forename of the holder.
12. This box shall begin with the word "remarks". It shall be used by the issuing authority to indicate any further information, which is considered necessary, provided that it complies with Article 5 of this Regulation. The following two-and-a-half lines shall be left empty for such remarks.
13. This box shall contain the relevant machine-readable information to facilitate external border controls.
The paper shall not be coloured (basic white shade).
The words designating the boxes shall appear in English, French and in the language of the issuing State.
Model of the FRTD
>PIC FILE= "L_2003099EN.002101.TIF">
