Reference for a preliminary ruling from the Tribunale Amministrativo Regionale del Lazio (Italy) lodged on 3 July 2006 — Telecom Italia SpA
v Ministero dell'Economia e delle Finanze, Ministero delle Comunicazioni
Referring court
Tribunale Amministrativo Regionale del Lazio
Parties to the main proceedings
Applicant: Telecom Italia SpA
Defendants: Ministero dell'Economia e delle Finanze, Ministero delle Comunicazioni
Question referred
Is Article 20(3) of Law No 448/1998 in conjunction with Article 4(9) of Law No 249/1997 compatible with Articles 11, 22 and 25 of Directive 97/13/EC? [1]
[1] OJ 1997 L 117, p. 15.
--------------------------------------------------
