Commission Regulation (EC) No 1020/2002
of 13 June 2002
amending Regulation (EEC) No 2958/93 laying down detailed rules for the application of Council Regulation (EEC) No 2019/93 as regards the specific arrangements for the supply of certain agricultural products
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EEC) No 2019/93 of 19 July 1993 introducing specific measures for the smaller Aegean islands concerning certain agricultural products(1), as last amended by Regulation (EC) No 442/2002(2), and in particular Article 3a thereof,
Whereas:
(1) Regulation (EEC) No 2019/93 has been substantially amended by Regulation (EC) No 442/2002. It is therefore necessary to adapt the detailed rules for the application of that Regulation laid down by Commission Regulation (EEC) No 2958/93(3), as last amended by Regulation (EC) No 1802/95(4).
(2) The amounts of the aid granted for the supply to islands of groups A and B should be adapted to the new monetary system. The aid for consignments to the islands in group A should be increased in order to make it more attractive for traders. Furthermore, an additional aid should be granted to cover costs of re-loading and transport from islands of transit or loading to islands of final destination belonging to group A or group B where direct shipment from the mainland is not possible or regular.
(3) Monitoring of operations qualifying under the specific supply arrangements requires prohibiting the transfer of the rights and obligations conferred on the holder of the certificate. The period for presentation of the proof of utilisation of the aid certificate should be increased to allow traders time to fulfil their obligation.
(4) One of the objectives of the administration of the specific supply arrangements is to ensure that the benefits are actually passed on to the stage at which the products destined for the end-users are placed on the market. To that end, national authorities should be permitted to verify margins and prices applied by traders.
(5) Regulation (EEC) No 2019/93 stipulates that products covered by the specific supply arrangements may not be re-exported to third countries or re-dispatched to the rest of the Community. However, it provides for derogations for traditional exports or traditional shipments to the rest of the Community of processed products. Detailed rules should be laid down for checking how those derogations are used.
(6) It is therefore necessary to amend Regulation (EEC) No 2019/93 accordingly.
(7) The measures provided for in this Regulation are in accordance with the opinions of all the Management Committees concerned,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EEC) No 2958/93 is hereby amended as follows:
1. Article 1 is amended as follows:
(a) Paragraph 1 is replaced by the following: "1. The flat-rate aid referred to in Article 3(1) of Regulation (EEC) No 2019/93 is hereby fixed, for all products referred to in the Annex to that Regulation, at:
- EUR 22/tonne for consignments to the islands in group A referred to in Annex I to this Regulation,
- EUR 36/tonne for consignments to the islands in group B referred to in Annex II to this Regulation.
In addition, EUR 9/tonne shall be granted to cover re-loading and transport costs from islands of transit or loading to islands of final destination belonging to group A or group B where direct shipment from the mainland is not possible or regular."
(b) Paragraph 2 is deleted.
(c) Paragraph 10 is replaced by the following: "10. Proof of utilisation of the aid certificate shall be furnished within two months following expiry of the period of validity of the certificate, except in cases of force majeure."
2. Article 2 is replaced by the following: "Article 2
Certificates shall not be transferable."
3. Article 3 is amended as follows:
(a) Paragraph 1 is deleted.
(b) Paragraph 2 is replaced by the following: "2. The Greek authorities shall take all appropriate steps to check that the benefits derived from the grant of the aid are passed on to the end user. In doing so they may assess the trading margins and prices applied by the various traders concerned.
These measures, and any amendments made, shall be notified to the Commission."
4. Article 4 is replaced by the following: "Article 4
1. Traditional exports and traditional dispatches to the rest of the Community of processed products containing raw materials that have benefited from the specific supply arrangements are permitted within the limits of annual quantities to be determined by the Commission in accordance with the procedure referred to in Article 13a(2) of Regulation (EEC) No 2019/93. The competent authorities shall take the necessary steps to ensure that those operations do not exceed the annual quantities laid down.
2. The competent authorities shall authorise the export or dispatch to the rest of the Community of quantities of processed products other than those referred to in paragraph 1 only where it is attested that the products concerned do not contain raw materials introduced under the specific supply arrangements.
The competent authorities shall carry out the necessary checks to ensure the accuracy of the attestations referred to in the first subparagraph and shall recover, where appropriate, the aid granted under the specific supply arrangements.
3. For the purpose of paragraphs 1 and 2, dispatch of products to destinations outside Group A islands or outside Group B islands shall constitute dispatch to the rest of the Community."
5. Article 5 is replaced by the following: "Article 5
The Greek authorities shall notify the Commission no later than the last day of each month of the following data relating to the previous month but two, by product:
- the quantities for which aid certificates were applied for broken down by recipient island-group,
- the number of cases of non-utilisation of aid certificates and the quantities involved broken down by recipient island-group,
- any quantities exported after processing in the context of the traditional exports broken down by destination,
- any quantities dispatched after processing in the context of the traditional dispatches broken down by destination."
6. Article 6 is deleted.
7. Annex II is replaced by the text set out in the Annex to this Regulation.
Article 2
This Regulation shall enter into force on the third day following that of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 13 June 2002.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 184, 27.7.1993, p. 1.
(2) OJ L 68, 12.3.2002, p. 4.
(3) OJ L 267, 28.10.1993, p. 4.
(4) OJ L 174, 26.7.1995, p. 27.
ANNEX
"ANNEX II
List of islands and "nomos" belonging to Group B:
(Article 1)
- nomos of Dodekanisa (Dodecanese),
- nomos of Khios,
- nomos of Lesvos (Lesbos),
- nomos of Samos,
- islands of the nomos of Kiklades not included in Group A,
- island of Gavdos."
