Euro exchange rates [1]
19 October 2006
(2006/C 254/01)
| Currency | Exchange rate |
USD | US dollar | 1,2561 |
JPY | Japanese yen | 149,08 |
DKK | Danish krone | 7,4552 |
GBP | Pound sterling | 0,67230 |
SEK | Swedish krona | 9,2485 |
CHF | Swiss franc | 1,5897 |
ISK | Iceland króna | 85,62 |
NOK | Norwegian krone | 8,4760 |
BGN | Bulgarian lev | 1,9558 |
CYP | Cyprus pound | 0,5767 |
CZK | Czech koruna | 28,363 |
EEK | Estonian kroon | 15,6466 |
HUF | Hungarian forint | 263,37 |
LTL | Lithuanian litas | 3,4528 |
LVL | Latvian lats | 0,6961 |
MTL | Maltese lira | 0,4293 |
PLN | Polish zloty | 3,8832 |
RON | Romanian leu | 3,5171 |
SIT | Slovenian tolar | 239,68 |
SKK | Slovak koruna | 36,635 |
TRY | Turkish lira | 1,8300 |
AUD | Australian dollar | 1,6603 |
CAD | Canadian dollar | 1,4266 |
HKD | Hong Kong dollar | 9,7826 |
NZD | New Zealand dollar | 1,8895 |
SGD | Singapore dollar | 1,9776 |
KRW | South Korean won | 1202,46 |
ZAR | South African rand | 9,4560 |
CNY | Chinese yuan renminbi | 9,9360 |
HRK | Croatian kuna | 7,3925 |
IDR | Indonesian rupiah | 11505,88 |
MYR | Malaysian ringgit | 4,6168 |
PHP | Philippine peso | 62,824 |
RUB | Russian rouble | 33,8120 |
THB | Thai baht | 46,873 |
[1] Source: reference exchange rate published by the ECB.
--------------------------------------------------
