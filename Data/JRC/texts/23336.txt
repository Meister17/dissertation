COUNCIL DECISION of 7 October 1997 on the conclusion of the Convention for the protection of the marine environment of the north-east Atlantic (98/249/EC)
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 130s(1), in conjunction with Article 228(2) and (3), first subparagraph thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Parliament (1),
Whereas the Commission, on behalf of the Community, took part in the negotiations on the drafting of the Convention for the protection of the marine environment of the north-east Atlantic;
Whereas the Convention was signed on behalf of the Community on 22 September 1992;
Whereas the Convention aims to prevent and eliminate pollution and to protect the maritime area against the adverse effects of human activities;
Whereas the Community has adopted measures in the area covered by the Convention and should therefore undertake international commitments in that area; whereas the Community's action is a necessary complement to that of the Member States directly concerned and its participation in the Convention would appear to comply with the principle of subsidiarity;
Whereas pursuant to Article 130r of the Treaty, Community policy on the environment contributes, inter alia, to pursuit of the objectives of preserving, protecting and improving the quality of the environment, protecting human health, and prudent and rational utilisation of natural resources; whereas, moreover, with their respective spheres of competence, the Community and the Member States cooperate with third countries and with the competent international organisations,
HAS ADOPTED THIS DECISION:
Article 1
The Convention for the protection of the marine environment of the north-east Atlantic, as signed in Paris on 22 September 1992, is hereby approved on behalf of the Community.
The text of the Convention is attached to this Decision.
Article 2
The President of the Council is hereby authorised to designate the person or persons empowered to deposit the instrument of approval with the Government of the French Republic in accordance with the provisions of Article 26 of the Convention.
Article 3
The Community shall be represented by the Commission as regards matters within the sphere of Community competence, in the commission established under Article 10 of the Convention.
Done at Luxembourg, 7 October 1997.
For the Council
The President
J.-C. JUNCKER
(1) OJ C 89, 10.4.1995, p. 199.
