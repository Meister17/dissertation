COUNCIL DIRECTIVE 97/22/EC of 22 April 1997 amending Directive 92/117/EEC concerning measures for protection against specified zoonoses and specified zoonotic agents in animals and products of animal origin in order to prevent outbreaks of food-borne infections and intoxications
THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 43 thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas, in the light of the experience acquired and bearing in mind the importance accorded to the prevention and control of zoonoses, it is necessary to carry out a substantial review of Directive 92/117/EEC (4);
Whereas, pending the aforementioned review, it is appropriate to provide for a postponement of the provisions concerning new rules for the reporting system for zoonoses, the establishment of methods for collecting samples and carrying out examinations, the implementation and approval of certain national measures, and the plans to be submitted by third countries,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Directive 92/117/EEC is hereby amended as follows:
1. Article 5 shall be amended as follows:
(a) in paragraph 1, '31 March` shall be replaced by '31 May`;
(b) in paragraph 2, '1 October` shall be replaced by '1 November`;
(c) paragraph 3 shall be deleted;
2. in Article 6 (b), the words 'In the case of salmonella this shall be done before the date laid down in Article 17;` shall be deleted;
3. Article 8 shall be amended as follows:
(a) in paragraph 1, '1 October 1993` shall be replaced by '1 March 1998`;
(b) the following subparagraph shall be added to paragraph 2:
'However, pending the review provided for in Article 15a, the obligation to submit plans to the Commission shall be suspended in the case of those Member States which have not yet submitted such plans with regard to salmonella in poultry.`;
4. Article 10 shall be amended as follows:
(a) in the first and second subparagraphs of paragraph 1, '1 January 1994` shall be replaced by '1 January 1998`;
(b) the third and fourth subparagraphs of paragraph 1 shall be deleted;
(c) paragraph 2 shall be deleted;
5. in Article 14 (2), '31 December 1995` shall be replaced by '31 December 1998`;
6. in Article 15 the second subparagraph shall be deleted;
7. The following Article shall be inserted:
'Article 15a
1. The Commission shall, before 1 November 97, submit a report to the Council concerning the measures to be implemented for the control and prevention of zoonoses. This report shall refer in particular to:
- the new rules for the reporting system for zoonoses,
- the methods for collecting samples and for examinations in approved national laboratories,
- the control of salmonella in poultry laying flocks,
- the control of salmonella in poultry breedingflocks and in compound feedingstuffs for poultry,
- any measures to combat zoonoses other than salmonellosis.
2. The report referred to in paragraph 1 shall be accompanied by proposals concerning zoonoses, especially in the context of the review of this Directive. The Council shall act on the proposals by a qualified majority before 1 June 1998.`;
8. The following point shall be inserted in Annex III, Section I:
'Va Pending the report provided for in Article 15a, Member States may waive the compulsory destruction provided for in point V.1 (b) and the slaughter requirement laid down in point V.1 (c) if they can ensure that:
(i) no non-incubated eggs from a flock covered by the first paragraph of V.1 (b) are marketed, except for the purpose of treatment in accordance with Directive 89/437/EEC (*);
(ii) no live poultry - including day-old chicks - can move from the flock unless they are going for immediate slaughter in accordance with the abovementioned point (c),
and this until it has been established to the satisfaction of the competent authority that the infection due to Salmonella enteritidis or Salmonella typhimurium is no longer present.
Member States availing themselves of the option provided for in the first subparagraph shall not be eligible for the Community financial contribution provided for in Article 29 of Decision 90/424/EEC (**).
(*) OJ No L 212, 22. 7. 1989, p. 87. Directive as last amended by Directive 96/23/EC (OJ No L 125, 23. 5. 1996, p. 10).
(**) OJ No L 224, 18. 8. 1990, p. 19. Decision as last amended by Decision 94/370/EC (OJ No L 168, 2. 7. 1994, p. 31).`
Article 2
1. Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive before 1 September 1997. They shall immediately inform the Commission thereof.
When Member States adopt these measures, they shall contain a reference to this Directive or shall be accompanied by such reference on the occasion of their official publication. The methods of making such reference shall be laid down by Member States.
2. Member States shall communicate to the Commission the texts of the provisions of national law which they adopt in the field covered by this Directive.
Article 3
This Directive shall enter into force on the third day following that of its publication in the Official Journal of the European Communities.
Article 4
This Directive is addressed to the Member States.
Done at Luxembourg, 22 April 1997.
For the Council
The President
J. VAN AARTSEN
(1) OJ No C 13, 18. 1. 1996, p. 23.
(2) OJ No C 320, 28. 10. 1996, p. 261.
(3) OJ No C 97, 1. 4. 1996, p. 29.
(4) OJ No L 62, 15. 3. 1993, p. 38. Directive as amended by the 1994 Act of Accession.
