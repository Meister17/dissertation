Commission Regulation (EC) No 1041/2002
of 14 June 2002
concerning the provisional authorisation of a new additive in feedingstuffs
(Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 70/524/EEC of 23 November 1970 concerning additives in feedingstuffs(1), as last amended by Commission Regulation (EC) No 2205/2001(2), and in particular Articles 3 and 9a thereof,
Whereas:
(1) Directive 70/524/EEC provides that new additives may be authorised following the review of an application made in accordance with Article 4 of the Directive.
(2) Article 2(aaa) of Directive 70/524/EEC requires authorisations for coccidiostats to be linked to the person responsible for putting them into circulation.
(3) Article 9a of Directive 70/524/EEC provides that provisional authorisation of such substances, which are listed in part I of Annex C to that Directive, may be given for a period of up to four years from the date on which the authorisation takes effect, if the conditions laid down in Article 3a(b) to (e) of the Directive are met and if it is reasonable to assume, in view of the available results, that when used in animal nutrition it has one of the effects referred to in Article 2(a).
(4) The assessment of the dossier submitted in respect of the coccidiostat "Semduramicin sodium" described in the Annex shows that this additive satisfies the above mentioned requirements when used in the animal category and under the conditions described in that Annex.
(5) The assessment of the dossier shows that certain procedures may be required to protect workers from exposure to the additives. Such protection should however be assured by the application of Council Directive 89/391/EEC of 12 June 1989 on the introduction of measures to encourage improvements in the safety and health of workers at work(3).
(6) The Scientific Committee for Animal Nutrition has delivered a favourable opinion with regard to the safety of the above mentioned coccidiostat, under the conditions described in the Annex.
(7) The measures provided for in this Regulation are in accordance with the opinion of the Standing Committee on the Food Chain and Animal Health,
HAS ADOPTED THIS REGULATION:
Article 1
The additive "Semduramicin sodium" belonging to the group "Coccidiostats and other medicinal substances" listed in the Annex to the present Regulation is provisionally authorised for use as an additive in animal nutrition under the conditions laid down in that Annex.
Article 2
This Regulation shall enter into force on the seventh day following that of its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 14 June 2002.
For the Commission
David Byrne
Member of the Commission
(1) OJ L 270, 14.12.1970, p. 1.
(2) OJ L 297, 15.11.2001, p. 3.
(3) OJ L 183, 29.6.1989, p. 1.
ANNEX
">TABLE>"
