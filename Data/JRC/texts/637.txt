Commission Regulation (EC) No 2858/2000
of 27 December 2000
amending Regulation (EC) No 2125/95 opening and providing for the administration of tariff quotas for preserved mushrooms
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulations (EC) No 2290/2000(1), (EC) No 2435/2000(2) and 2851/2000(3), establishing certain concessions in the form of Community tariff quotas for certain agricultural products and providing for an adjustment, as an autonomous and transitional measure, of certain agricultural concessions provided for in the Europe Agreement with Bulgaria, Romania and Poland respectively, and in particular Articles 1(3) thereof,
Whereas:
(1) Commission Regulation (EC) No 2125/95(4), as last amended by Regulation (EC) No 2493/98(5), provides for the administration of the quotas for preserved mushrooms allocated to Poland, Romania and Bulgaria in Annexes II, V and VI respectively to Council Regulation (EC) No 3066/95 of 22 December 1995 establishing certain concessions in the form of Community tariff quotas for certain agricultural products and providing for the adjustment, as an autonomous and transitional measure, of certain agricultural concessions provided for in the Europe Agreements to take account of the Agreement on Agriculture concluded during the Uruguay Round Multilateral Trade Negotiations(6), as last amended by Regulation (EC) No 2435/98(7).
(2) Regulation (EC) No 3066/95 was repealed by Regulation (EC) No 2851/2000 and replaced by Regulations (EC) No 2290/2000, (EC) No 2435/2000 and (EC) No 2851/2000 as regards Bulgaria, Romania and Poland respectively. The abovementioned tariff concessions for preserved mushrooms have been included as they stand in Regulations (EC) No 2290/2000 and (EC) No 2435/2000 as regards products originating in Bulgaria and Romania on the one hand, and they are granted subject to no limit on quantity by Regulation (EC) No 2851/2000 as regards products originating in Poland on the other hand. Regulation (EC) No 2125/95 should accordingly be amended to strike out any reference to Poland, except the reference in Article 5 relating to licence applications by traditional importers, in order to adapt it to those new provisions.
(3) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Processed Fruit and Vegetables,
HAS ADOPTED THIS REGULATION:
Article 1
Regulation (EC) No 2125/95 is hereby amended as follows:
1. Article 1 is replaced by the following:
"Article 1
1. The tariff quotas for preserved mushrooms of the genus Agaricus covered by CN codes 0711 90 40, 2003 10 20 and 2003 10 30 set out in Annex I are hereby opened subject to the rules of application laid down herein.
2. The rate of duty applicable shall be 12 % ad valorem in the case of products covered by CN code 0711 90 40 (serial No 09.4062) and 23 % in the case of products covered by CN codes 2003 10 20 and 2003 10 30 (serial No 09.4063). However, a single rate of 8,4 % shall apply where the above products originate in Bulgaria (serial No 09.4725) or Romania (serial No 09.4726).".
2. The word "Poland" is deleted from Article 2(2) and Article 4(1) and (5).
3. The word "Poland" is deleted from Article 10(3).
4. Annex I is replaced by the Annex hereto.
Article 2
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
It shall apply from 1 January 2001.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 27 December 2000.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 262, 17.10.2000, p. 1.
(2) OJ L 280, 4.11.2000, p. 17.
(3) See page 7 of this Official Journal.
(4) OJ L 212, 7.9.1995, p. 16.
(5) OJ L 309, 19.11.1998, p. 38.
(6) OJ L 328, 30.12.1995, p. 31.
(7) OJ L 303, 13.11.1998, p. 1.
ANNEX
"ANNEX I
ALLOCATION AS REFERRED TO IN ARTICLE 2 IN TONNES (NET DRAINED WEIGHT)
>TABLE>"
