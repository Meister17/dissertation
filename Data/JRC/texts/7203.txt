Information communicated by Member States regarding State aid granted under Commission Regulation (EC) No 68/2001 of 12 January 2001, as amended by Commission Regulation (EC) No 363/2004 of 25 February 2004 on the application of Articles 87 and 88 of the EC Treaty to training aid
(2005/C 270/12)
(Text with EEA relevance)
Aid No | XT 55/04 |
Member State | United Kingdom |
Region | Northwest England |
Title of aid scheme or name of company receiving individual aid | Training Support for BAE Systems Marine Submarines |
Legal basis | Regional Development Agencies Act 1998 |
Annual expenditure planned under the scheme or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount | |
Loans guaranteed | |
Individual aid | Overall aid amount | GBP 435000 over 2 years |
Loans guaranteed | |
Maximum aid intensity | In conformity with Article 4(2)-(7) of the Regulation | Yes | |
Date of implementation | From 1.7.2004 |
Duration of scheme or individual aid award | Until 31.3.2006 |
Objective of aid | General training | Yes |
Specific training | No |
Economic sectors concerned | All sectors eligible for training aid | No |
Limited to specific sectors | Yes |
—Agriculture | |
—Fisheries and aquaculture | |
—Coalmining | |
—All manufacturing | |
or | |
Steel | |
Shipbuilding (Warships) | Yes |
Synthetic fibres | |
Motor vehicles | |
Other manufacturing | |
—All services | |
or | |
Maritime transport services | |
Other transport services | |
Financial services | |
Other services | |
Name and address of the granting authority | Name: Northwest Development Agency |
Address: Rennaissance House, PO Box 37, Centre Park, Warrington, UK-Cheshire WA1 1XB |
Large individual aid grants | In conformity with Article 5 of the Regulation | Yes | |
Aid No | XT 40/03 |
Member State | Belgium |
Region | Flanders |
Title of aid scheme or name of company receiving individual aid | Bombardier Transportation Belgium N
V Vaartdijkstraat 5 BE-8200 Brugge |
Legal basis | Besluit van de Vlaamse regering van 4.7.2003 |
Annual expenditure planned or overall amount of aid granted to the company | Aid scheme | Annual overall amount | |
Loans guaranteed | |
Individual aid | Overall aid amount | EUR 0,9 million |
Loans guaranteed | |
Maximum aid intensity | In conformity with Article 4(2)-(6) of the Regulation | Yes | |
Date of implementation | 4.7.2003 |
Duration of scheme or individual aid | Until 31.12.2004 |
Objective of aid | General training | Yes |
Specific training | Yes |
Economic sectors concerned | All sectors eligible for training aid | No |
Limited to specific sectors | Ad hoc case |
—Agriculture | |
—Fisheries and aquaculture | |
—Coalmining | |
—All manufacturing | |
or | |
Steel | |
Shipbuilding | |
Synthetic fibres | |
Motor vehicles | |
Other manufacturing | Production of rolling stock and trams |
—All services | |
or | |
Maritime transport services | |
Other transport services | |
Financial services | |
Other services | |
Name and address of the granting authority | Name: Ministerie van de Vlaamse Gemeenschap Administratie Economie Afdeling Economisch Ondersteuningsbeleid |
Address: Markiesstraat 1 BE-1000 Brussels |
Large individual aid grants | In conformity with Article 5 of the Regulation The measure excludes awards of aid or requires prior notification to the Commission of awards of aid, if the amount of aid granted to one enterprise for a single training project exceeds EUR 1 million. | Yes | |
--------------------------------------------------
