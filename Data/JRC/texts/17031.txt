P6_TA(2004)0024
Generalised system of preferences
European Parliament resolution on the Communication from the Commission to the Council, the European Parliament and the European Economic and Social Committee "Developing countries, international trade and sustainable development: the function of the Community's generalised system of preferences (GSP) for the ten-year period from 2006 to 2015" (COM(2004)0461)
The European Parliament,
- having regard to the Commission communication (COM(2004)0461),
- having regard to Council Regulation (EC) No 2501/2001 of 10 December 2001 applying a scheme of generalised tariff preferences for the period from 1 January 2002 to 31 December 2004 [1],
- having regard to Council Regulation (EC) No 2211/2003 of 15 December 2003 amending Regulation (EC) No 2501/2001 and extending it to 31 December 2005 [2],
- having regard to its position of 29 November 2001 on a scheme of generalised tariff preferences for the period 1 January 2002 to 31 December 2004 [3],
- having regard to its position of 4 December 2003 on a scheme of generalised tariff preferences for the period from 1 January 2002 to 31 December 2004 and extending it to 31 December 2005 [4],
- having regard to the Communication from the Commission "Towards a reinforced culture of consultation and dialogue — General principles and minimum standards for consultation of interested parties by the Commission" (COM(2002)0704),
- having regard to the Doha Declaration, adopted by the Fourth WTO Ministerial Conference, held from 9 to 14 November 2001,
- having regard to the outcome of the World Summit on Sustainable Development (WSSD), held in Johannesburg (South Africa) from 26 August to 4 September 2002,
- having regard to the Monterrey Consensus adopted at the International Conference on Financing for Development, which was held in Monterrey (Mexico) from 18 to 22 March 2002,
- having regard to Rule 108(5) of the Rules of Procedure,
A. whereas, since its creation in 1971, the Generalised System of Preferences (GSP) has been a key instrument of the EU's development policy as well as its trade policy,
B. whereas the Commission's Communication sets out new ten-year guidelines for the GSP, on the basis of which a series of three-year regulations will ensue,
C. whereas any change in the current GSP scheme will directly affect, positively and negatively, a wide variety of stakeholders,
D. whereas the EU has committed itself to undertake meaningful Sustainability Impact Assessments of the major political initiatives ex ante, but still has to do so in the case of the proposed reformed GSP scheme,
E. whereas the GSP utilisation rate, defined as the volume of imports actually benefiting from tariff preferences as a percentage of the total volume of trade eligible under the GSP, has increased in recent years to not more than 52,5 % in 2002, which has to be improved, particularly in the ACP countries,
F. whereas the WTO India Appellate Body, in its ruling of 7 April 2004 on the Community's GSP scheme, stated that developed countries may grant different tariffs to products originating from different GSP beneficiaries, provided that such differential tariff treatment meets the remaining conditions in the Enabling Clause and thus ensures non-discrimination against countries with the same development, financial and trade needs which the benefits are intended to address,
G. whereas the GSP is the broadest preferential system offered by the developed countries and whereas, moreover, this leading position will be consolidated over the coming years, following the accession of the ten new Member States,
1. Welcomes the objectives set out by the Communication and endorses the Commission's resolve to improve the current GSP through the simplification, stabilisation and clarification of the arrangements, the concentration of preferences on those developing countries most in need, and the enhancement of the sustainable development component;
2. Notes that, in order to achieve its declared objectives, the Commission proposes far-reaching reforms of the current GSP system, such as a reduction in the arrangements from five to three, a new graduation system based only on market share criteria and the simplification of rules of origin;
3. Regrets that this important Communication, which sets out new ten-year guidelines for the GSP, does not refer to any prior thorough evaluation of the functioning and impact of the current GSP, and requests the Commission to provide decision-makers with solid elements of information on both the need for reform and the potential for improvement;
4. Regrets that the Commission's Communication was not the result of a proper stakeholder consultation and does not come sufficiently in advance to allow for meaningful consultations in anticipation of the expected release of the proposal for the next regulation;
5. Recalls that customs duties have steadily fallen to an all-time low and that further tariff reductions should be agreed upon as a result of WTO negotiations on the Doha Development Agenda, and notes that the effects of these reductions notably on the Least Developed Countries should be addressed by the new GSP system;
6. Stresses that, in order to improve its impact on the special needs of developing countries, the next GSP Regulation should:
(a) grant preferences in conformity with the comparative advantage and export interests of developing countries themselves, especially taking account of the most vulnerable sectors of society,
(b) extend preferential access to a wide range of new products and transfer a significant number of products currently classified as "sensitive" to the "non-sensitive" category,
(c) consider the importance of food sovereignty and the right of the countries concerned to protect their agriculture in the implementation of the GSP,
(d) ensure that the new graduation system applied does not have a significant negative impact on the developing countries concerned;
7. Calls on the Commission to explore the possibility of increasing the preferential margin for sensitive products;
8. Welcomes the overall objective of simplifying the graduation mechanism, but notes with concern that the new graduation system outlined by the Commission is based solely on market share criteria, disregarding development and poverty indicators, and believes that such a system could discriminate against large yet poor exporters;
9. Endorses the promotion of sustainable development as a key component of the GSP; insists on the fundamental importance of a single system of additional concessions (GSP plus) being a simple and predictable arrangement that is compatible with the WTO Enabling Clause and thus provides for objective criteria for the selection and evaluation of beneficiary countries;
10. Insists that it is crucial that, to qualify for "GSP Plus", beneficiary countries must have both ratified and implemented the relevant international conventions, and that the forthcoming Regulation must specify credible procedures by which this can be assessed, involving the European Parliament and other stakeholders, e.g. social partners, and by which investigations can be initiated in case of evidence of inadequate implementation; stresses that the implementation level of human rights within the addressed countries must become a decision-making criterion;
11. Suggests that the Regulation should provide for the possibility of withdrawing "GSP Plus" preferences in respect of specific sectors in the event of serious and persistent violations of the eligibility criteria;
12. Insists that the EU should commit itself to providing development assistance towards capacity building in order to help developing countries to qualify for "GSP Plus", failing which the standards set could act as non-tariff barriers and a large number of countries could miss out on the potential benefits of the system;
13. Welcomes the Communication's emphasis on simplification, but draws attention to the complexity of implementing the proposed formula ("free-trade agreement (FTA) exclusion clause");
14. Notes that rules of origin and related administrative procedures have been demonstrated to be one of the main reasons for the under-utilisation of GSP trade preferences, particularly by Least Developed Countries;
15. Welcomes the Commission's determination to reform the system of origin rules in form, substance and procedures;
16. Calls on the Commission to explore the merits, particularly with regard to LDCs, of the expansion of partial regional cumulation of origin to cross-regional cumulation and full or global cumulation;
17. Calls on the Commission to make progress in the harmonisation of the different systems of origin rules applied by the existing trade agreements (i.e. GSP/EBA, FTAs, EPAs);
18. Requests that, as established in the WTO Doha Declaration, in the Monterrey Consensus and the conclusions of the World Summit on Sustainable Development in Johannesburg, developing countries be provided with adequate technical assistance, especially regarding the self-policing arrangements of the GSP plus, for the purpose of building the institutional and regulatory capacity required to capture the benefits from international trade and preferential arrangements;
19. Welcomes the proposal for a detailed GSP evaluation every three years, taking into consideration multilateral negotiations, and stresses that this must include an impact assessment on stakeholders, in particular LDCs and the populations of developing countries;
20. Concludes that, although full support is given to the objectives stated by the Commission, further clarification as to the exact details and implementation mechanisms of the different arrangements is required for Parliament to be able to carry out a more informed and definitive assessment;
21. Calls on the Council and the Commission to start the consultation process on the new GSP first implementing regulation sufficiently in advance to allow for proper involvement and consultation of the European Parliament, partner countries and other interested parties;
22. Instructs its President to forward this resolution to the Council and the Commission.
[1] OJ L 346, 31.12.2001, p. 1.
[2] OJ L 332, 19.12.2003, p. 1.
[3] OJ C 153 E, 27.6.2002, p. 234.
[4] OJ C 89 E, 14.4.2004, p. 101.
--------------------------------------------------
