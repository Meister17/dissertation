COMMISSION DECISION of 20 February 1995 on the list of establishments in the former Yugoslav Republic of Macedonia approved for the purpose of importing fresh meat into the Community (Text with EEA relevance) (95/45/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Directive 72/462/EEC of 12 December 1972 on health and veterinary inspection problems upon importation of bovine, ovine and caprine animals and swine and fresh meat or meat products from third countries (1), as last amended by the Act of Accession of Austria, Finland and Sweden, and in particular Articles 4 (1) and 18 (1) (a) and (b) thereof,
Whereas establishments in third countries cannot be authorized to export fresh meat to the Community unless they satisfy the general and special conditions laid down in that Directive;
Whereas, in accordance with Article 4 (3) of Directive 72/462/EEC, the former Yugoslav Republic of Macedonia has forwarded a list of the establishments authorized to export to the Community;
Whereas Community on-the-spot inspections have shown that the hygiene standards of these establishments are sufficient and they may therefore be entered on a first list of establishments, drawn up in accordance with Article 4 (1) of that Directive, from which imports of fresh meat may be authorized;
Whereas imports of fresh meat from the establishments on the list in the Annex hereto continue to be subject to provisions already laid down, the general provisions of the Treaty and in particular the other Community veterinary regulations, particularly as regards health protection;
Whereas the measures provided for in this Decision are in accordance with the opinion of the Standing Veterinary Committee,
HAS ADOPTED THIS DECISION:
Article 1
1. The establishments in the former Yugosla
v Republic of Macedonia listed in the Annex are hereby approved for the purposes of exporting fresh meat to the Community.
2. Imports from those establishments shall remain subject to the Community veterinary provisions laid down elsewhere, and in particular those concerning health protection.
Article 2
This Decision is addressed to the Member States.
Done at Brussels, 20 February 1995.
For the Commission
Franz FISCHLER
Member of the Commission
(1) OJ No L 302, 31. 12. 1972, p. 28.
ANNEX
LIST OF ESTABLISHMENTS
"" ID="1">1> ID="2">KOKO 'GODEL' AD, Skopje> ID="3">×> ID="7">×"> ID="1">2> ID="2">AD ZIK 'KUMANOVO', Kumanovo> ID="3">×> ID="7">×"> ID="1">3> ID="2">KLANICA 'KOMERC', Prilep> ID="3">×> ID="7">×"> ID="1">4> ID="2">AD ZIK 'CRVENA ZVEZDA' DOO Klanica so ladilnik, Stip> ID="3">×> ID="7">×"> ID="1">5> ID="2">ADMS ZIK 'STRUMICA' DOO 'Mosa Pijade', Strumica> ID="3">×> ID="7">×"> ID="1">6> ID="2">OP 'GORNI POLOG', Gostivar> ID="3">×> ID="7">×"> ID="1">7> ID="2">DOO 'STOKOKOMERC', Bitola> ID="3">×> ID="7">×"> ID="1">9> ID="2">AD 'MALINA', Kriva Palanka> ID="3">×> ID="7">×""
>
(1)() SL: Slaughterhouse
CP: Cutting premises
CS: Cold store
B: Bovine meat
S/G: Sheepmeat/Goatmeat
P: Pigmeat
SP: Meat from solipeds
SR: Special remarks
