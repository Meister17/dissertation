COMMISSION DECISION of 17 June 1998 on a common technical Regulation for public land-based enhanced radio message system (ERMES) receiver requirements (second edition) (notified under document number C(1998) 1615) (Text with EEA relevance) (98/522/EC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Directive 98/13/EC of the European Parliament and of the Council of 12 February 1998 relating to telecommunications terminal equipment and satellite earth station equipment, including the mutual recognition of their conformity (1),
Whereas the Commission has adopted the measure identifying the type of terminal equipment for which a common technical regulation is required, as well as the associated scope statement;
Whereas the corresponding harmonised standards, or parts thereof, implementing the essential requirements which are to be transformed into common technical regulations should be adopted;
Whereas in order to ensure continuity of access to markets for manufacturers, it is necessary to allow for transitional arrangements regarding equipment approved according to Commission Decision 95/290/EC (2);
Whereas the common technical Regulation adopted in this Decision is in accordance with the opinion of ACTE,
HAS ADOPTED THIS DECISION:
Article 1
1. This Decision shall apply to terminal equipment intended to be connected to the pan-European land-based radio paging system known as enhanced radio message system (ERMES) and falling within the scope of the harmonised standard identified in Article 2(1).
2. This Decision establishes a common technical regulation covering the receiver requirements for ERMES terminal equipment.
Article 2
1. The common technical Regulation shall include the harmonised standard prepared by the relevant standardisation body implementing to the extent applicable the essential requirements referred to in Article 5(d), (e), (f) and (g) of Directive 98/13/EC. The reference to the standard is set out in the Annex.
2. Terminal equipment covered by this Decision shall comply with the common technical Regulation referred to in paragraph 1, shall meet the essential requirements referred to in Article 5(a) and (b) of Directive 98/13/EC, and shall meet the requirements of any other applicable Directives, in particular Council Directives 73/23/EEC (3) and 89/336/EEC (4).
Article 3
Notified bodies designated for carrying out the procedures referred to in Article 10 of Directive 98/13/EC shall, as regards terminal equipment covered by Article 1(1) of this Decision, use or ensure the use of the harmonised standard referred to in the Annex within 12 months after the coming into force of this Decision.
Article 4
1. Decision 95/290/EC shall be repealed with effect from 12 months after the coming into force of this Decision.
2. Terminal equipment, approval under Decision 95/290/EC may continue to be placed on the market and put into service.
Article 5
This Decision is addressed to the Member States.
Done at Brussels, 17 June 1998.
For the Commission
Martin BANGEMANN
Member of the Commission
(1) OJ L 74, 12. 3. 1998, p. 1.
(2) OJ L 181, 2. 8. 1995, p. 21.
(3) OJ L 77, 26. 3. 1973, p. 29.
(4) OJ L 139, 23. 5. 1989, p. 19.
ANNEX
Reference to the harmonised standard applicable
The harmonised standard referred to in Article 2 of the Decision is:
Radio equipment and systems (RES);
Enhanced Radio MEssage System (ERMES);
Receiver requirements
ETSI
European Telecommunications Standards Institute
ETSI Secretariat
TBR 7 - second edition - October 1997
(excluding the foreword)
Additional information
The European Telecommunications Standards Institute is recognised according to Council Directive 83/189/EEC (1).
The harmonised standard referred to above has been produced according to a mandate issued in accordance with the relevant procedures of Directive 83/189/EEC.
The full text of the harmonised sstandard referenced above can be obtained from:
European Telecommunications Standards Institute
650, route des Lucioles
F-06921 Sophia Antipolis Cedex
European Commission
DG XIII/A/2 - (BU 31, 1/7)
Rue de la Loi/Wetstraat 200
B-1049 Brussels
or from any other organisation responsible for making ETSI standards available, of which a list can be found on the Internet under address www.ispo.cec.be.
(1) OJ L 109, 26. 4. 1983, p. 8.
