COMMISSION REGULATION (EEC) N° 4001/87
of 23 December 1987
amending Regulation (EEC) N° 2783/75 laying down special measures for ovalbumin and lactalbumin
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) N° 2658/87
of 23 July 1987 on the tariff and statistical nomenclature
and on the Common Customs Tariff (1), as amended
by Regulation (EEC) N° 3985/87 (2), and in particular
Article 15 thereof,
Whereas Council Regulation (EEC) N° 2658/87 establishes, with effect from 1 January 1988, a combined goods nomenclature based on the Harmonized System which will meet the requirements both of the Common Customs Tariff and the nomenclature of goods for the external trade statistics of the Community;
Whereas, as a consequence, it is necessary to express the descriptions of goods and tariff heading numbers which appear in Council Regulation (EEC) N° 2783/75 (3), according to the terms of the combined nomenclature; whereas these adaptations do not call for any amendment of substance,
HAS ADOPTED THIS REGULATION:
Article 1
Article 1 of Regulation (EEC) N° 2783/75 is replaced by the following:
'Article 1
In trade between the Community and third countries, import duties shall be applied to the following products:
>TABLE>
Article 2
This Regulation shall enter into force on 1 January 1988.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 23 December 1987.
For the Commission
Frans ANDRIESSEN
Vice-President
SPA:L377UMBE20.96
FF: 5UEN; SETUP: 01; Hoehe: 472 mm; 96 Zeilen; 2261 Zeichen;
Bediener: JUTT Pr.: B;
Kunde: 40800 Montan England 20
(1) OJ N° L 256, 7. 9. 1987, p. 1.
(2) OJ N° L 376, 31. 12. 1987, p. 1.
(3) OJ N° L 282, 1. 11. 1975, p. 104.
