COMMISSION REGULATION (EEC) No 822/76 of 7 April 1976 on the conditions and the procedures for the recognition of producer groups of silkworm rearers
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Regulation (EEC) No 707/76 of 25 March 1976 on the recognition of producer groups of silkworm rearers (1), and in particular Article 3 thereof,
Whereas Article 2 of Regulation (EEC) No 707/76 provides for the recognition of producer groups which provide evidence of economic activity on an adequate scale ; whereas this condition should be defined in greater detail and allowance made for possible derogations in regions with low production;
Whereas the provisions relating to the procedure for the granting and withdrawal of recognition should be amplified by specifying the information to be supplied when the application is lodged;
Whereas, for the information of Member States and parties concerned, a list should be published at the beginning of each year, of the groups which were recognized during the previous year and of those from whom recognition was withdrawn during the same period;
Whereas the measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Flax and Hemp,
HAS ADOPTED THIS REGULATION:
Article 1
1. In order to obtain recognition, a group must include at least 500 producers who use or undertake to use at least 2 500 boxes during the marketing year in which recognition is granted.
2. However, in accordance with the procedure laid down in Article 12 of Council Regulation (EEC) No 1308/70 of 29 June 1970 on the common organization of the market in flax and hemp (2), at last amended by Regulation (EEC) No 814/76 (3), a Member State may be authorized, on request, to recognize in a region of low production a group which does satisfy the condition defined in paragraph 1.
Article 2
The following documents and information shall accompany any application for recognition: (a) the statute,
(b) the names of persons authorized to act in the name of and on behalf of the group,
(c) a list of the activities justifying the application for recognition,
(d) proof that the provisions of Article 1 have been complied with,
(e) the rules adopted pursuant to the second indent of Article 1 (b) of Regulation (EEC) No 707/76, and in particular those relating to the drying of cocoons.
Article 3
1. Member States shall decide on the application for recognition within two months of its receipt.
2. Recognition of a group shall be withdrawn if the conditions laid down for recognition are no longer satisfied or if such recognition is based on incorrect information. Recognition shall be withdrawn with retroactive effect if it has been obtained or enjoyed fraudulently.
3. Member States shall exercise continuous supervision to ensure that the recognized groups observe the conditions for recognition.
Article 4
1. Where a Member State grants, refuses or withdraws recognition of a group it shall inform the Commission thereof within two months and, when refusing an application for recognition or withdrawing recognition, indicate its reasons therefore. (1)OJ No L 84, 31.3.1976, p. 1. (2)OJ No L 146, 4.7.1970, p. 1. (3)See page 4 of this Official Journal.
2. At the beginning of each year the Commission shall ensure publication in the Official Journal of the European Communities of the list of groups which were recognized during the previous year and of those from whom recognition was withdrawn during the same period.
Article 5
This Regulation shall enter into force on the third day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 7 April 1976.
For the Commission
P.J. LARDINOIS
Member of the Commission
