*****
COUNCIL DIRECTIVE
of 18 March 1986
amending Directive 80/232/EEC on the approximation of the laws of the Member States relating to the ranges of nominal quantities and nominal capacities permitted for certain prepackaged products
(86/96/EEC)
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community, and in particular Article 100 thereof,
Having regard to the proposal from the Commission (1),
Having regard to the opinion of the European Parliament (2),
Having regard to the opinion of the Economic and Social Committee (3),
Whereas, since the adoption on 15 January 1980 of Directive 80/232/EEC (4), difficulties have arisen, particularly as regards the designation of certain products in Annex I and the interpretation of the introductory paragraph of Annex III;
Whereas the said Annexes should be amended as a result,
HAS ADOPTED THIS DIRECTIVE:
Article 1
Annexes I and III to Directive 80/232/EEC are hereby amended in accordance with the Annex to this Directive.
Article 2
Member States shall take the measures necessary to comply with this Directive within 18 months of its notification (5). They shall forthwith inform the Commission thereof.
Article 3
This Directive is addressed to the Member States.
Done at Brussels, 18 March 1986
For the Council
The President
W. F. EEKELEN
(1) OJ No C 18, 25. 1. 1984, p. 7.
(2) OJ No C 46, 18. 2. 1985, p. 92.
(3) OJ No C 206, 6. 8. 1984, p. 17.
(4) OJ No L 51, 25. 2. 1980, p. 1.
(5) This Directive was notified to the Member States on 18 March 1986.
ANNEX
1. In Annex I, points 1.5.4. and 4. shall be replaced by the following:
'1.5.4. Prepared foods obtained similar the swelling or roasting of cereals or cereal products (puffed rice, corn flakes and smilar products) (Common Customs Tariff heading No 19.05)
250 - 375 - 500 - 750 - 1 000 - 1 500 - 2 000.'.
4. READY-TO-USE PAINTS AND VARNISHES (with or without added solvents; Common Customs Tariff subheading 32.09 A II excluding dispersead pigments and solutions) (quantity in ml)
25 - 50 - 125 - 250 - 375 - 500 - 750 - 1 000 - 2 000 - 2 500 - 4 000 - 5 000 - 10 000.'.
2. In Annex III, the introductory paragraph shall be replaced by the following:
'By way of derogation from Article 8 (1) (e) of Council Directive 75/324/EEC of 20 May 1975 on the approximation of the laws of the Member States relating to aerosol dispensers (1), products sold in aerosols complying with the requirements of this Directive need not be marked with the nominal weight of their contents.'.
