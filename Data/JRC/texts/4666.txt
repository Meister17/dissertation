Directive 2005/88/EC of the European Parliament and of the Council
of 14 December 2005
amending Directive 2000/14/EC on the approximation of the laws of the Member States relating to the noise emission in the environment by equipment for use outdoors
(Text with EEA relevance)
THE EUROPEAN PARLIAMENT AND THE COUNCIL OF THE EUROPEAN UNION,
Having regard to the Treaty establishing the European Community, and in particular Article 95 thereof,
Having regard to the proposal from the Commission,
Having regard to the opinion of the European Economic and Social Committee [1],
After consulting the Committee of the Regions,
Acting in accordance with the procedure laid down in Article 251 of the Treaty [2],
Whereas:
(1) Directive 2000/14/EC of the European Parliament and of the Council [3] has been the subject of a review by the Working Group on Outdoor Equipment, set up by the Commission.
(2) In its report dated 8 July 2004, this Working Group concluded that a number of the stage II limits due to be mandatorily applied as from 3 January 2006 were not technically feasible. However, the intention was never to restrict the placing on the market or putting into service of equipment solely based on technical feasibility.
(3) It is therefore necessary to provide that certain types of equipment listed in Article 12 of Directive 2000/14/EC, which would not be able to meet the stage II limits by 3 January 2006 solely for technical reasons, can still be placed on the market and/or put into service as from that date.
(4) The experience of the first five years of application of Directive 2000/14/EC has demonstrated that more time is needed to fulfil the provisions under Articles 16 and 20 thereof and highlighted the need to review that Directive with a view to its possible amendment, in particular with respect to the stage II limits referred to therein. It is therefore necessary to extend by two years the deadline for submission of the report to the European Parliament and to the Council on the Commission’s experience in implementing and administering Directive 2000/14/EC as referred to in Article 20(1) of that Directive.
(5) Article 20(3) of Directive 2000/14/EC provides for the submission, by the Commission, of a report to the European Parliament and to the Council on whether, and to what extent, technical progress allows a reduction of limit values for lawnmowers and lawn trimmers/lawn-edge trimmers. In view of the fact that the obligations contained in Article 20(1) of that Directive are more prescriptive than those in Article 20(3), and in order to avoid duplication of effort, it is appropriate to include these types of equipment in the general report provided for in Article 20(1) of that Directive. Consequently, the separate reporting obligation in Article 20(3) of that Directive should be deleted.
(6) Since the objective of this Directive, namely, to ensure the ongoing functioning of the internal market by requiring equipment used outdoors to comply with harmonised environmental noise provisions, cannot be sufficiently achieved by Member States and can therefore, by reason of the scale and effects of the proposed action, be better achieved at Community level, the Community may adopt measures, in accordance with the principle of subsidiarity as set out in Article 5 of the Treaty. In accordance with the principle of proportionality, as set out in that Article, this Directive does not go beyond what is necessary in order to achieve this objective, in that its scope is confined to those types of equipment for which compliance with the stage II limits is presently impossible for technical reasons.
(7) In accordance with point 34 of the Interinstitutional agreement on better law making [4], Member States are encouraged to draw up, for themselves and in the interest of the Community, their own tables illustrating, as far as possible, the correlation between this Directive and the transposition measures, and to make them public.
(8) Directive 2000/14/EC should therefore be amended accordingly,
HAVE ADOPTED THIS DIRECTIVE:
Article 1
Directive 2000/14/EC is hereby amended as follows:
1. The table in Article 12 shall be replaced by the following:
The permissible sound power level shall be rounded up or down to the nearest integer number (less than 0,5, use lower number; greater than or equal to 0,5, use higher number)"
"Type of equipment | Net installed power P (in kW) Electric power Pel [5] in kW Mass of appliance m in kg Cutting width L in cm | Permissible sound power level in dB/1 pW |
| | Stage I as from 3 January 2002 | Stage II as from 3 January 2006 |
Compaction machines (vibrating rollers, vibratory plates, vibratory rammers) | P ≤ 8 | 108 | 105 [6] |
8 < P ≤ 70 | 109 | 106 [6] |
P > 70 | 89 + 11 lg P | 86 + 11 lg P [6] |
Tracked dozers, tracked loaders, tracked excavator-loaders | P ≤ 55 | 106 | 103 [6] |
P > 55 | 87 + 11 lg P | 84 + 11 lg P [6] |
Wheeled dozers, wheeled loaders, wheeled excavator-loaders, dumpers, graders, loader-type landfill compactors, combustion-engine driven counterbalanced lift trucks, mobile cranes, compaction machines (non-vibrating rollers), paver-finishers, hydraulic power packs | P ≤ 55 | 104 | 101 [6] [7] |
P > 55 | 85 + 11 lg P | 82 + 11 lg P [6] [7] |
Excavators, builders’ hoists for the transport of goods, construction winches, motor hoes | P ≤ 15 | 96 | 93 |
P > 15 | 83 + 11 lg P | 80 + 11 lg P |
Hand-held concrete-breakers and picks | m ≤ 15 | 107 | 105 |
15 < m < 30 | 94 + 11 lg m | 92 + 11 lg m [6] |
m ≥ 30 | 96 + 11 lg m | 94 + 11 lg m |
Tower cranes | | 98 + lg P | 96 + lg P |
Welding and power generators | Pel ≤ 2 | 97 + lg Pel | 95 + lg Pel |
2 < Pel ≤ 10 | 98 + lg Pel | 96 + lg Pel |
10 > Pel | 97 + lg Pel | 95 + lg Pel |
Compressors | P ≤ 15 | 99 | 97 |
P > 15 | 97 + 2 lg P | 95 + 2 lg P |
Lawnmowers, lawn trimmers/lawn-edge trimmers | L ≤50 | 96 | 94 [6] |
50 < L ≤ 70 | 100 | 98 |
70 < L ≤ 120 | 100 | 98 [6] |
L > 120 | 105 | 103 [6] |
2. Article 20 shall be amended as follows:
(a) In the first sentence of paragraph 1, the words "Not later than 3 January 2005" shall be replaced by "Not later than 3 January 2007";
(b) Paragraph 3 shall be deleted.
Article 2
1. Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive by 31 December 2005 at the latest. They shall forthwith inform the Commission thereof.
They shall apply those measures from 3 January 2006.
When Member States adopt those measures, they shall contain a reference to this Directive or be accompanied by such a reference on the occasion of their official publication. The methods of making such reference shall be laid down by Member States.
2. Member States shall communicate to the Commission the text of the main provisions of national law which they adopt in the field covered by this Directive.
Article 3
This Directive shall enter into force on the day of its publication in the Official Journal of the European Union.
Article 4
This Directive is addressed to the Member States.
Done at Strasbourg, 14 December 2005.
For the European Parliament
The President
J. Borrell Fontelles
For the Council
The President
C. Clarke
[1] Opinion delivered on 27 October 2005 (not yet published in the Official Journal).
[2] Opinion of the European Parliament of 26 October 2005 (not yet published in the Official Journal) and Council Decision of 8 December 2005.
[3] OJ L 162, 3.7.2000, p. 1.
[4] OJ C 321, 31.12.2003, p. 1.
[5] Pel for welding generators: conventional welding current multiplied by the conventional load voltage for the lowest value of the duty factor given by the manufacturer.Pel for power generators: prime power according to ISO 8528-1:1993, clause 13.3.2
[6] The figures for stage II are merely indicative for the following types of equipment:
- walk-behind vibrating rollers;
- vibratory plates (> 3kW);
- vibratory rammers;
- dozers (steel tracked);
- loaders (steel tracked > 55 kW);
- combustion-engine driven counterbalanced lift trucks;
- compacting screed paver-finishers;
- hand-held internal combustion-engine concrete-breakers and picks (15<m<30)
- lawnmowers, lawn trimmers/lawn-edge trimmers.Definitive figures will depend on amendment of the Directive following the report required in Article 20(1). In the absence of any such amendment, the figures for stage I will continue to apply for stage II.
[7] For single-engine mobile cranes, the figures for stage I shall continue to apply until 3 January 2008. After that date, stage II figures shall apply.
--------------------------------------------------
