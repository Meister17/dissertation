REGULATION (EEC) No 1674/72 OF THE COUNCIL of 2 August 1972 laying down general rules for granting and financing aid for seed
THE COUNCIL OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community;
Having regard to Council Regulation (EEC) No 2358/711 of 26 October 1971 on the common organization of the market in seeds, and in particular Article 3 (4) thereof;
Having regard to Council Regulation No 729/702 of 21 April 1970 on the financing of the common agricultural policy, and in particular Article 3 (2) thereof;
Having regard to the proposal from the Commission;
Whereas Article 3 of Regulation (EEC) No 2358/71 provides for the possibility of aid being granted for the production of certain seeds and whereas general rules should be laid down for implementing that provision;
Whereas the aid may be granted only for the production of basic seed or certified seed and whereas these products should be clearly defined;
Whereas, to make supervision possible, basic seed and certified seed must be produced under duly registered growing contracts and whereas seed establishments and breeders must be officially approved or registered;
Whereas, for administrative reasons, aid should in each Member State, be granted only in respect of products harvested on the territory of that State;
Whereas, if the system of aid is to function properly, a supervision system is needed to ensure that aid is granted only in respect of products qualifying for it;
Whereas such aid involves expenditure ; whereas under Article 14 of Regulation (EEC) No 2358/71 the provisions governing the financing of the common agricultural policy apply to this aid ; whereas it should therefore be specified that the financing of the aid in question is governed by Article 3 of Regulation (EEC) No 729/70;
HAS ADOPTED THIS REGULATION:
Article 1
1. Aid fixed in accordance with Article 3 of Regulation (EEC) No 2358/71 shall be granted as specified in the following articles in respect of basic seed and certified seed: - as defined in the Council Directive of 14 June 19663 on the marketing of fodder plant seed, as last amended by the Directive of 30 March 19714;
- which satisfies the standards and conditions laid down by those Directives and
- which are officially certified.
2. From 1 February 1973, as a transitional measure for new Member States, aid shall also be granted in respect of basic seed and certified seed which are covered by a Council Decision on equivalence.
Article 2
The seed must be produced: (a) either under a growing contract concluded between a seed establishment or a breeder and a seed grower; 1 OJ No L 246, 5.11.1971, p. 1.
2 OJ No L 94, 28.4.1970, p. 13.
3 OJ No L 125, 11.7.1966, p. 2298/66 (66/403).
4 OJ No L 87, 17.4.1971, p. 24 (71/162).
(b) or directly by the seed establishment or the breeder ; such production shall be attested by a growing declaration.
Article 3
The seed establishments and breeders referred to in Article 2 shall be approved or registered by Member States.
Approval or registration by a Member State shall be valid throughout the Community.
Article 4
Each Member State shall grant aid only in respect of seed harvested on its territory during the calendar year in which the marketing year for which the aid was fixed begins.
The aid shall be granted to all seed growers under conditions which ensure equal treatment for beneficiaries irrespective of the place of their establishment within the Community.
Article 5
Member States shall introduce a system of administrative supervision to ensure that the conditions required for the granting of aid are fulfilled ; they shall in particular ensure that the growing contracts and declarations referred to in Article 2 are registered.
Article 6
1. The aid referred to in Article 3 of Regulation (EEC) No 2358/71 falls within the definition of intervention contained in Article 3 (1) of Regulation (EEC) No 729/70.
2. Expenditure resulting from the aid referred to in paragraph 1 shall be equal to the amounts paid in accordance with Article 3 of Regulation (EEC) No 2358/71 and with the provisions made pursuant to that Article.
Article 7
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Communities.
It shall apply from 1 July 1972.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 2 August 1972.
For the Council
The President
T. WESTERTERP
