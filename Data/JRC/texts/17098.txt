Decision of the EEA Joint Committee
No 105/2004
of 9 July 2004
amending Annex XI (Telecommunication services) to the EEA Agreement
THE EEA JOINT COMMITTEE,
Having regard to the Agreement on the European Economic Area, as amended by the Protocol adjusting the Agreement on the European Economic Area, hereinafter referred to as "the Agreement", and in particular Article 98 thereof,
Whereas:
(1) Annex XI to the Agreement was amended by Decision of the EEA Joint Committee No 75/2004 of 8 June 2004 [1].
(2) Commission Decision 2003/821/EC of 21 November 2003 on the adequate protection of personal data in Guernsey [2] is to be incorporated into the Agreement,
HAS DECIDED AS FOLLOWS:
Article 1
The following point shall be inserted after point 5eg (Commission Decision 2003/490/EC) of Annex XI to the Agreement:
"5eh. 32003 D 0821: Commission Decision 2003/821/EC of 21 November 2003 on the adequate protection of personal data in Guernsey (OJ L 308, 25.11.2003, p. 27)."
Article 2
The texts of Decision 2003/821/EC in the Icelandic and Norwegian languages, to be published in the EEA Supplement to the Official Journal of the European Union, shall be authentic.
Article 3
This Decision shall enter into force on 10 July 2004, provided that all the notifications under Article 103 (1) of the Agreement have been made to the EEA Joint Committee [3].
Article 4
This Decision shall be published in the EEA Section of, and in the EEA Supplement to, the Official Journal of the European Union.
Done at Brussels, 9 July 2004.
For the EEA Joint Committee
The President
Kjartan Jóhannsson
--------------------------------------------------
[1] OJ L 349, 25.11.2004, p. 33.
[2] OJ L 308, 25.11.2003, p. 27.
[3] No constitutional requirements indicated.
