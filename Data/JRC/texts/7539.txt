*****
COMMISSION DIRECTIVE
of 27 February 1986
limiting the marketing of seed of certain species of fodder plants and oil and fibre plants to seed which has been officially certified as 'basic seed' or 'certified seed'
(86/109/EEC)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Economic Community,
Having regard to Council Directive 66/401/EEC of 14 June 1966 on the marketing of fodder plant seed (1), as last amended by Commission Directive 85/38/EEC (2), and in particular Article 3 (3) thereof,
Having regard to Council Directive 69/208/EEC of 30 June 1969 on marketing the seed of oil and fibre plants (3), as last amended by Commission Directive 82/859/EEC (4), and in particular Article 3 (3) thereof,
Whereas Directive 66/401/EEC allows the marketing of basic seed, certified seed and commercial seed of certain species of fodder plants;
Whereas Directive 69/208/EEC allows the marketing of basic seed, certified seed of all kinds and commercial seed of certain species of oil and fibre plants;
Whereas Article 3 (3) of each of the aforementioned Directives authorizes the Commission to prohibit the marketing of seed unless it has been officially certified as 'basic seed' or 'certified seed';
Whereas it has been established, on the basic of the information available at present, that Member States will be in a position to produce sufficient basic seed and certified seed to satisfy within the Community the demand for seed of some of the species referred to above with seed of those categories as from 1 July 1987 in the case of certain species, 1 July 1989 in the case of certain other species and 1 July 1991 in the case of certain further species;
Whereas the measures provided for in this Directive are in accordance with the opinion of the Standing Committee on Seeds and Propagating Material for Agriculture, Horticulture and Forestry,
HAS ADOPTED THIS DIRECTIVE:
Article 1
1. Member States shall provide that from 1 July 1987 it shall not be permitted to place on the market seed of:
1.2 // - Vicia faba L. (partim) // - Field bean // - Papaver somniferum L. // - Opium poppy
unless it has been officially certified as 'basic seed' or 'certified seed'.
2. Member States shall provide that from 1 July 1987 it shall not be permitted to place on the market seed of:
1.2 // - Glycine max (L.) Merr. // - Soya bean // - Linum usitatissimum L. // - Linseed
Article 2
Member States shall provide that from 1 July 1989 it shall not be permitted to place on the market seed of:
1.2 // - Agrostis canina L. // - Velvet bent // - Agrostis gigantea Roth // - Redtop // - Agrostis stolonifera L. // - Creeping bent grass // - Agrostis tenuis Sibth. // - Brown top // - Alopecurus pratensis L. 357, 18. 12. 1982, p. 31.
// - Arrhenatherum elatius (L.) Beauv. ex J. et K. Presl. // - Tall oatgrass // - Phleum bertolinii DC // - Timothy // - Poa nemoralis L. // - Wood meadowgrass // - Poa palustris L. // - Swamp meadowgrass // - Poa trivilialis L. // - Rough-stalked meadowgrass // - Trisetum flavescens (L.) Beauv. // - Golden oatgrass // - Lotus corniculatus L. // - Birdsfoot trefoil // - Lupinus albus L. // - White lupin // - Lupinus angustifolius L. // - Blue lupin // - Lupinus luteus L. // - Yellow lupin // - Medicago lupulina L. // - Black medick // - Trifolium hybridum L. // - Alsike clover // - Brassica juncea L. Czern. et Coss. in Czern. // - Brown mustard
unless it has been officially certified as 'basic seed' or 'certified seed'.
Article 3
Member States shall provide that from 1 July 1991 it shall not be permitted to place on the market seed of:
1.2 // - Festuca ovina L. // - Sheep's fescue // - Trifolium incarnatum L. // - Crimson clover // - Trifolium resupinatum L. // - Persian clover // - Vicia sativa L. // - Common vetch // - Vicia villosa Roth. // - Hairy vetch // - Sinapis alba L. // - White mustard
unless it has been officially certified as 'basic seed' or 'certified seed'.
Article 4
Member States shall bring into force:
- not later than 1 July 1987 the laws, regulations or administrative provisions necessary to comply with Article 1,
- not later than 1 July 1989 the laws, regulations or administrative provisions necessary to comply with Article 2, and
- not later than 1 July 1991 the laws, regulations or administrative provisions necessary to comply with Article 3.
They shall forthwith inform the Commission thereof.
Article 4
This Directive is addressed to the Member States.
Done at Brussels, 27 February 1986.
For the Commission
Frans ANDRIESSEN
Vice-President // - Meadow foxtail
(1) OJ No 125, 11. 7. 1966, p. 2298/66. (2) OJ No L 16, 19. 1. 1985, p. 41. (3) OJ No L 169, 10. 7. 1969, p. 3. (4) OJ No L
