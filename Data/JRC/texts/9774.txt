Information communicated by Member States regarding State aid granted under Commission Regulation (EC) No 70/2001 of 12 January 2001 on the application of Articles 87 and 88 of the EC Treaty to State aid to small and medium-sized enterprises
(2006/C 239/03)
(Text with EEA relevance)
Aid No | XS 3/06 |
Member State | Republic of Poland |
Region | Północno zachodni (1.4) |
Title of aid scheme or name of company receiving individual aid | Regional aid scheme for SMEs investing in Sulęcin municipality |
Legal basis | Uchwała Rady Miejskiej w Sulęcinie w sprawie zwolnień od podatku od nieruchomości Art. 7 ust. 3 ustawy z dnia 12 stycznia 1991 r. o podatkach i opłatach lokalnych (tj. z 2002 r. Dz.U. nr 9 poz. 84 ze zm.) |
Annual expenditure planned or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount | EUR 0,6 million |
Loans guaranteed | |
Individual aid | Overall aid amount | |
Loans guaranteed | |
Maximum aid intensity | In conformity with Articles 4(2)-(6) and 5 of the Regulation | Yes | |
Date of implementation | 22.9.2005. |
Duration of scheme or individual aid award | Until 31.12.2006 |
Objective of aid | Aid to SMEs | Yes | |
Economic sectors concerned | All sectors eligible for aid to SMEs | Yes |
Name and address of the granting authority | Burmistrz Sulęcina |
ul. Lipowa 18 PL-69-200 Sulęcin |
Large individual aid grants | In conformity with Article 6 of the Regulation | Yes | |
Aid No | xs 4/06 |
Member State | poland |
Region | Małopolska |
Title of aid scheme or name of company receiving individual aid | Aid scheme for SMEs in Alwernia |
Legal basis | Uchwała Rady Miejskiej w Alwerni w sprawie programu pomocy dla Małych i Średnich Przedsiębiorstw w Gminie Alwernia. Art. 7 ust. 3 ustawy z dnia 12 stycznia 1991 r. o podatkach i opłatach lokalnych (tekst jedn. Dz.U. z 2002 r., nr 9, poz. 84 z późn. zm.) |
Annual expenditure planned or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount | EUR 0,45 million |
Loans guaranteed | |
Individual aid | Overall aid amount | |
Loans guaranteed | |
Maximum aid intensity | In conformity with Articles 4(2)-(6) and 5 of the Regulation | Yes | |
Date of implementation | 1.12.2005 |
Duration of scheme or individual aid award | Until 31.12.2006 |
Objective of aid | Aid to SMEs | Yes | |
Economic sectors concerned | All sectors eligible for aid to SMEs | Yes |
Name and address of the granting authority | Burmistrz Gminy Alwernia |
ul. Gęsikowskiego 7 PL-32-566 Alwernia |
Large individual aid grants | In conformity with Article 6 of the Regulation | Yes | |
Aid No | XS 33/06 |
Member State | Spain |
Region | Castilla y León |
Title of aid scheme or name of company receiving individual aid | Aid to help SMEs adapt to the information society |
Legal basis | Resolución de 14 de febrero de 2006, del Presidente de la Agencia de Inversiones y Servicios de Castilla y León (ADE), por la que se aprueba la convocatoria, así como las disposiciones comunes y específicas que la regulan, para la concesión de determinadas subvenciones de la Agencia de Inversiones y Servicios de Castilla y León para 2006 cofinanciadas con fondos estructurales (Línea 6) |
Annual expenditure planned or overall amount of aid granted to the company | Aid scheme | Annual overall amount | EUR 2 million |
Loans guaranteed | |
Individual aid | Overall aid amount | |
Loans guaranteed | |
Maximum aid intensity | In conformity with Articles 4(2)-(6) and 5 of the Regulation | Yes | |
Date of implementation | 18.2.2006 |
Duration of scheme or individual aid award | Until 31.12.2006 |
Objective of aid | Aid to SMEs | Yes | |
Economic sectors concerned | All sectors eligible for aid to SMEs | Yes |
Name and address of the granting authority | Agencia de Inversiones y Servicios de Castilla y León |
C/ Duque de la Victoria, no 23 E-47001 Valladolid |
Large individual aid grants | In conformity with Article 6 of the Regulation | Yes | |
Aid No | XS 44/06 |
Member State | Spain |
Region | Pais Vasco |
Title of aid scheme or name of company receiving individual aid | BIZKAIBERRI LAGUNTZAK, Programme to promote innovation within SMEs in Vizcaya |
Legal basis | Decreto Foral 38/2006, de 28 de febrero, por el que se regula Bizkaiberri laguntzak, Programa de impulso a la innovación de las pymes de Bizkaia del Departamento de Innovación y Promoción Económica de la Diputación Foral de Bizkaia |
Annual expenditure planned or overall amount of aid granted to the company | Aid scheme | Annual overall amount | 2006: EUR 2500000 2007: EUR 3500000 |
Maximum aid intensity | In conformity with Articles 4(2)-(6) and 5 of the Regulation | Yes | |
Date of implementation | 9.3.2006– 31.12.2007 |
Duration of scheme or individual aid award | Until 31.12.2007 |
Objective of aid | Aid to SMEs | Yes | |
Economic sectors concerned | All sectors eligible for aid to SMEs | Yes |
Name and address of the granting authority | Departamento de Innovación y Promoción Económica de la Diputación Foral de Bizkaia |
Obispo Orueta no 6 E-48009 Bilbao (Bizkaia) |
Large individual aid grants | In conformity with Article 6 of the Regulation | Yes | |
Aid No | XS 46/06 |
Member State | Italy |
Region | Veneto — Chambers of Commerce, Industry, Agriculture and Crafts (CCIAA) in Veneto either directly or indirectly via their special agencies, the Regional Union and the External Centre |
Title of aid scheme or name of company receiving individual aid | Aid for research and development by SMEs in Veneto, including via cooperatives, associations and companies representing the producing districts |
Legal basis | Atti amministrativi quali regolamenti, delibere e/o provvedimenti delle CCIAA e/o delle loro aziende speciali, Unione regionale e del loro Centro Estero Veneto |
Annual expenditure planned or overall amount of individual aid granted to the company | Individual aid | Annual overall amount | 3900000 (three million nine hundred thousand) EUR If this total expenditure were to increase by 20 % per year, the CCIAA in Veneto undertake formally to notify the modifications to the scheme to the Commission |
Maximum aid intensity | In conformity with Articles 4(2)-(6) and 5, 5a, 5b and 5c of Regulation No 70/2001 as supplemented by Regulation No 364/2004 | Yes |
Date of implementation | 1.1.2005 |
Duration of scheme or individual aid award | Until 31.12.2009 |
Objective of aid | Aid to SMEs in Veneto for fundamental research, industrial research and pre-competitive development work (Article 2(h), (i) and (j) of Regulation No 70/2001 as amended by Regulation No 364/2004), aid for research and development (Article 5a), aid for feasibility studies (Article 5b) and aid for patenting costs (Article 5c) | Yes |
Economic sectors concerned | All sectors eligible for aid to SMEs | Yes |
Name and address of the granting authority | The individual Chambers of Commerce, Industry, Agriculture and Crafts in Veneto. The agency coordinating this scheme is |
Unione regionale: via delle Industrie, 19/D I-30175 Marghera (VE) tel: 0039 041 0999311 fax 0039 041 0999303 e-mail uvcamcom@ven.camcom.it |
Website | www.ven.camcom.it |
Other information | The Chambers of Commerce, Industry, Agriculture and Crafts in Veneto have already communicated an aid scheme under Regulation No 70/2001 (aid No XS 76/02, published in OJ C 18, 23.1.2004, p. 3). |
Note: Where aid is granted in the form of a guarantee for a project that includes the acquisition of equipment as part of the eligible expenses, the intensity of this aid may not under any circumstances exceed those stipulated in Commission Regulation 70/01 for this type of aid.
Aid No | XS 51/06 |
Member State | Spain |
Region | Cataluña |
Title of aid scheme or name of company receiving individual aid | Aid for subcontracting innovation activities |
Legal basis | Orden TRI/97/2006, de 28 de febrero, por la que se aprueban las bases reguladoras para la concesión de ayudas para la subcontratación de actividades de investigación, desarrollo e innovación (DOGC 4595 de 17.3.2006) |
Annual expenditure planned or overall amount of aid granted to the company | Aid scheme | Annual overall amount | EUR 1 million |
Loans guaranteed | |
Individual aid | Overall aid amount | |
Loans guaranteed | |
Maximum aid intensity | In conformity with Articles 4(2)-(6) and 5 of the Regulation | Yes | |
Date of implementation | Since 17.3.2006 |
Duration of scheme or individual aid award | Until 31 December 2006 |
Objective of aid | Aid to SMEs | Yes | |
Economic sectors concerned | All sectors eligible for aid to SMEs | Yes |
Name and address of the granting authority | Centro de Innovación y Desarrollo Empresarial (CIDEM) |
Paseo de Grácia, 129 E-08008 Barcelona |
Large individual aid grants | In conformity with Article 6 of the Regulation | Yes | |
Aid No | XS 56/06 |
Member State | United Kingdom |
Region | Lancashire (including the unitary authority areas of Blackpool and Blackburn). |
Title of aid scheme or name of company receiving individual aid | Lancashire Rural Recovery Grant Fund |
Legal basis | Section 5 of the Regional Development Agencies Act 1998 |
Annual expenditure planned under the scheme or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount | GBP 0,75 million |
Loans guaranteed | |
Individual aid | Overall aid amount | |
Loans guaranteed | |
Maximum aid intensity | In conformity with Articles 4(2)-(6) and 5 of the Regulation | Yes | |
Date of implementation | From 1.4.2006 |
Duration of scheme or individual aid award | Until 30.6.2007 |
Objective of aid | Aid to SMEs | Yes | |
Economic sectors concerned | All sectors eligible for aid to SMEs | Yes |
Name and address of the granting authority | Lancashire County Developments Ltd |
Robert House Starkie Street Preston PR1 3LU United Kingdom |
Large individual aid grants | In conformity with Article 6 of the Regulation | Yes | |
Aid No | XS 77/06 |
Member State: | Cyprus |
Region | All regions |
Title of aid scheme or name of company receiving individual aid | Bilateral Cooperation between Cyprus and Greece |
Legal basis | Απόφαση του Διοικητικού Συμβουλίου της 14ης Μαρτίου 2006 |
Annual expenditure planned or overall amount of individual aid granted to the company | Aid scheme | Annual overall amount | EUR 0,34 million |
Loans guaranteed | |
Individual aid | Overall aid amount | |
Loans guaranteed | |
Maximum aid intensity | In conformity with Articles 4(2)-(6) and 5 of the Regulation | Yes: 75 % | |
Date of implementation | 27.4.2006 |
Duration of scheme or individual aid award | Until 31.12.2006 |
Objective of aid | Aid to SMEs | Yes | |
Economic sectors concerned | All sectors eligible for aid to SMEs | Yes |
Name and address of the granting authority | Ίδρυμα Προώθησης Έρευνας |
Γωνία Απελλή και Νιρβάνα Άγιοι Ομολογητές, Λευκωσία Τ.Κ. 23422, 1683 Λευκωσία |
Large individual aid grants | In conformity with Article 6 of the Regulation | Yes: the plan does not apply to large individual aid grants | |
--------------------------------------------------
