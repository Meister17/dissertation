COMMISSION REGULATION (EC) No 2702/98 of 17 December 1998 concerning the technical format for the transmission of structural business statistics (Text with EEA relevance)
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC, Euratom) No 58/97 of 20 December 1996 (1) concerning structural business statistics, and in particular Article 12(viii) thereof,
Whereas Council Regulation (EC, Euratom) No 58/97 established a common framework for the production of Community statistics on the structure, activity, competitiveness and performance of businesses in the Community;
Whereas it is necessary to specify the technical format for the transmission of structural business statistics;
Whereas the envisaged measures are in accordance with the opinion of the Statistical Programme Committee,
HAS ADOPTED THIS REGULATION:
Article 1
The technical format referred to in Article 9 of Regulation (EC, Euratom) No 58/97 concerning structural business statistics is defined in the Annex to this Regulation.
Article 2
Member States shall apply this format for the data concerning the 1996 reference year and subsequent years.
Article 3
This Regulation shall enter into force on the 20th day following its publication in the Official Journal of the European Communities.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 17 December 1998.
For the Commission
Yves-Thibault de SILGUY
Member of the Commission
(1) OJ L 14, 17.1.1997, p. 1.
ANNEX
TECHNICAL FORMAT
1. The form of the data
The data is sent as a set of records of which a large part describes the characteristics of the data (country, year, economic activity, etc.). The data itself is a number which can be linked to flags and explanatory footnotes used for example to describe aggregations of NACE codes. Confidential data should be sent with the true value being recorded in the value field and a flag indicating the nature of the confidential data being added to the record.
In order to be precise about the nature of the data, it is necessary to distinguish the following special cases:
- data equal to zero (coded '0`): real values of zero only,
- confidential data (coded 'x`): indicates data which the Member State does not transmit to Eurostat because the data is confidential,
- missing data (coded 'm`): this is data which is currently missing but the Member State intends to supply when available,
- data not available: this is data which is not collected in a Member State. In this case the corresponding record is not sent.
By default, if an entire dimension (a variable, a NUTS code, a NACE code, etc.) is not collected then the corresponding records will not exist except for those which are missing because they form part of a regrouping of NACE codes. This is why it is important to distinguish data which is really missing by supplying a record (one per missing item) in which the data value is coded as 'm`, and data which is really equal to zero by supplying the corresponding records in which the data value is set to 0.
2. Record structure
Records are made up of fields of variable length separated by semi-colons (;). The maximum expected length is shown in the table for information. In order from left to right they are:
>TABLE>
3. Description of the fields
3.1. The type of series
>TABLE>
3.2. Territorial unit
This code corresponds to the country for national series or to the region for regional series (series 1C, 2F, 3E, 3F, 4F). It is based on the NUTS 95 code. For regions, two characters are added to the two characters for the country (see NUTS 95).
>TABLE>
3.3. Size classes
Each size class corresponds to a code. Most series cover all size categories and hence code 30 is the most appropriate.
All codes from 01 up to 53 refer to the number of persons employed. Codes 80 to 85 refer to gross premiums written.
>TABLE>
>TABLE>
3.4. Economic activity
This field is used for the NACE Rev.1 heading. A number of standard aggregates are foreseen by the SBS Regulation and should be coded as follows.
>TABLE>
Non-standard aggregates should be indicated in the 'Aggregation of NACE codes field`.
3.5. Form of ownership or FATS identification
This field is used either for breakdowns according to form of ownership (only used in industry and construction) or FATS identification (only used in services). There are no series that breakdown by both classifications.
The requirements concerning the breakdown of characteristics following the location of ownership or control (FATS) are part of Annex 1, section 10, paragraph 3 of Regulation (EC, Euratom) No 58/97. Thus data collection is subject to testing.
The data concern a breakdown of totals by the country or geographical region where ownership or control is located. Three pieces of information are required:
- the method used for the collection: ultimate beneficial owner or 'first shot` information,
- the kind of FATS: inward/resident FATS or outward FATS,
- the country where ownership or control is located or for outward FATS where, for example, turnover is generated or employment is located.
>TABLE>
3.6. Location of ownership - FATS
Only used in services.
The codes to be used are those also used for balance of payments statistics (the same as the above codes in section 3.2). Country codes and other area codes (an incomplete list) are shown at the end of this format.
3.7. Variable
>TABLE>
3.8. Data value
Monetary data is expressed in thousands of national currency units except for:
- Ireland: data is given in units (IEP),
- Spain, Greece and Italy : data is given in millions of national currency units.
In order to be precise about the nature of the data, it is necessary to distinguish the following cases:
- data equal to zero (coded '0`): real values of zero only,
- confidential data (coded 'X`): indicates data which the Member State does not transmit to Eurostat because the data is confidential. A flag should also be used to indicate that the data is missing due to confidentiality (see 3.10 below),
- missing data (coded 'm`): this is data which is currently missing but the Member State intends to supply when available,
- data not available: this is data, which is not collected in a Member State. In this case the corresponding record is not sent.
By default, if an entire dimension (a variable, a NUTS code, a NACE code, etc.) is not collected then the corresponding records will not exist excepts for those which are missing because they form part of a regrouping of NACE codes. This is why it is important to distinguish data which is really missing by supplying a record (one per missing item) in which the data value is coded as 'm`.
3.9. Quality flag
>TABLE>
Revised data refers to data which are sent for a second (or more) time and are corrections of data which have been previously sent. Updated data concerns data which was not previously available and was coded as missing in the data value field (see 3.8) but has since become available. The flag indicating provisional data should be used in order to indicate that it is likely that that the data being transmitted may be corrected.
3.10. Confidentiality
Member States are asked to clearly indicate confidential data using the flags listed below.
Countries which cannot send confidential data are asked to set the value to 'x` (see 3.8) and to indicate using a flag that the data is missing due to confidentiality.
>TABLE>
3.11. Turnover size classes
The size classes below are measured in million ECU.
>TABLE>
3.12. Sales area size classes
>TABLE>
3.13. Units of data values
This field is optional and allows Member States to specify which units have been used for the data values used in the data transmission. The correct implementation of section 3.8 makes it unnecessary to use this field.
>TABLE>
4. Examples of records
Example 1: Aggregation of NACE-codes
Series 2A data for 1995 for Germany. The NACE code 231 contains the aggregation of codes 231 + 232 + 233 for variable 16 11 0: number of persons employed. It is not confidential.
2A;1995;DE;30;231;;;16110;2700;M;;;231 = 231 + 232 + 233;
Example 2: Confidential data: too few enterprises
Total wages and salaries (DEM 12 124 million) for the manufacture of iron and steel (NACE 271) in 1995 in the region of Fribourg (series C) is confidential because there are too few enterprises. This is revised data.
2C;1995;DE13;30;271;;;13320;12124000;R;A;;;
Example 3: Confidential data: dominance by one enterprise
The number of persons employed (2 700) in France in 1996 for enterprises of 250 to 499 employees for NACE 141 is confidential. One enterprise dominates the data and represents 92 % of employment. This is provisional data.
2D;1996;FR;14;141;;;16110;2700;P;B;92;;
Example 4: Size class data: sales area in retail trade
The turnover of the retail sale of foods (NACE Rev.1 52.2) in Luxembourg in 1996 for a sales space of 120 to 399 m² was LUF 1 000 million. It is updated data.
3C;1996;LU;30;522;;;12110;1000000;M;;;;;;02
Example 5: Inward FATS in road freight transport
In 1995 the number of enterprises (code 11110) in the Netherlands in road freight transport owned or controlled by an enterprise in the USA, following the concept of ultimate beneficial ownership (UBO), is 4. The data is provisional. It concerns Annex 1 data and the specification on the data value is added.
1G;1995;NL;30;6024;30;US;11110;4;P;;;;UNIT
5. Type of magnetic media
In order to facilitate reading the data, the data must be supplied on 3,5&Prime; diskette.
6. Other methods
Upon written request Eurostat can provide Member States with electronic questionnaires for the service sectors using Microsoft Excel. The request should be made well in advance of the planned date of data delivery.
>TABLE>
>TABLE>
