Commission Regulation (EC) No 1577/2003
of 8 September 2003
derogating from Council Regulation (EC) No 1251/1999 as regards area payments for certain arable crops and payments for set-aside for the 2003/04 marketing year to producers in certain regions of the Community
THE COMMISSION OF THE EUROPEAN COMMUNITIES,
Having regard to the Treaty establishing the European Community,
Having regard to Council Regulation (EC) No 1251/1999 of 17 May 1999 establishing a support system for producers of certain arable crops(1), as last amended by Regulation (EC) No 1038/2001(2), and in particular the fourth indent of the second paragraph of Article 9 thereof,
Whereas:
(1) Article 8(1) of Regulation (EC) No 1251/1999 provides that area payments are to be made from 16 November following the harvest.
(2) Certain regions of the Community have been adversely affected by extreme weather in 2003, resulting in exceptionally low average yields.
(3) Some producers are experiencing severe financial difficulties as a result.
(4) In the light of the situation in certain regions of the Community and the budgetary situation, the Member States concerned should be authorised to make, from 16 October 2003 onwards and subject to a limit of 50 %, advance area payments for arable crops and advance payments for set-aside for the 2003/04 marketing year.
(5) In view of the urgent nature of the measures to be adopted, provision should be made for this Regulation to enter into force immediately.
(6) The measures provided for in this Regulation are in accordance with the opinion of the Management Committee for Cereals,
HAS ADOPTED THIS REGULATION:
Article 1
1. By way of derogation from Article 8(1) of Regulation (EC) No 1251/1999, an advance payment in respect of the 2003/04 marketing year of up to 50 % of the area payments for arable crops and of up to 50 % of the payments for set-aside may be made from 16 October 2003 to producers in the Member States or regions listed in the Annex.
2. The advance payment provided for in paragraph 1 may be made only where, on the date of payment, the producer concerned is found to be eligible.
3. When calculating the final area payment to producers who receive the advance provided for in paragraph 1 of this Article, the competent authority shall take account of:
(a) any reduction in the producer's eligible area;
(b) any advance paid under this Regulation.
Article 2
This Regulation shall enter into force on the day following its publication in the Official Journal of the European Union.
This Regulation shall be binding in its entirety and directly applicable in all Member States.
Done at Brussels, 8 September 2003.
For the Commission
Franz Fischler
Member of the Commission
(1) OJ L 160, 26.6.1999, p. 12.
(2) OJ L 145, 31.5.2001, p. 16.
ANNEX
France: Departments concerned
01 AIN
03 ALLIER
04 ALPES-DE-HAUTE-PROVENCE
05 HAUTES-ALPES
07 ARDÈCHE
09 ARIÈGE
11 AUDE
12 AVEYRON
13 BOUCHES-DU-RHÔNE
15 CANTAL
16 CHARENTE
17 CHARENTE-MARITIME
18 CHER
19 CORRÈZE
21 CÔTE-D'OR
23 CREUSE
24 DORDOGNE
25 DOUBS
26 DRÔME
28 EURE-ET-LOIRE
30 GARD
31 HAUTE-GARONNE
32 GERS
36 INDRE
37 INDRE-ET-LOIRE
38 ISÈRE
39 JURA
40 LANDES
41 LOIR-ET-CHER
42 LOIRE
43 HAUTE-LOIRE
44 LOIRE-ATLANTIQUE
45 LOIRET
46 LOT
47 LOT-ET-GARONNE
48 LOZÈRE
49 MAINE-ET-LOIRE
52 HAUTE-MARNE
54 MEURTHE-ET-MOSELLE
55 MEUSE
57 MOSELLE
58 NIÈVRE
63 PUY-DE-DÔME
64 PYRÉNÉES-ATLANTIQUES
65 HAUTES-PYRÉNÉES
67 BAS-RHIN
68 HAUT-RHIN
69 RHÔNE
70 HAUTE-SAÔNE
71 SAÔNE-ET-LOIRE
73 SAVOIE
74 HAUTE-SAVOIE
79 DEUX-SÈVRES
81 TARN
82 TARN-ET-GARONNE
83 VAR
84 VAUCLUSE
85 VENDÉE
86 VIENNE
87 HAUTE-VIENNE
88 VOSGES
89 YONNE
90 TERRITOIRE DE BELFORT
The whole of Italy, the Netherlands, Portugal, Germany and Spain
